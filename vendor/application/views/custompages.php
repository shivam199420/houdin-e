<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
 
 <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
          <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Custom-Page</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Store Setting</a></li>
                  <li class="active">Custom-Page</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                 
            </div>
           <!--END Page-Title --> 

            <div class="row m-t-20">
              <div class="col-lg-12">
                <div class="card-box">
                 
                                            
                  <form action="#" data-parsley-validate novalidate>
                    <div class="form-group">
                      <label for="userName">Name</label>
                      <input type="text" name="nick" parsley-trigger="change" required placeholder="" class="form-control" id="userName">
                    
                    </div>
                    <div class="form-group">
                      <label for="userName">URL Path</label>
                      <input type="text" name="nick" parsley-trigger="change" required placeholder="" class="form-control" id="userName">
                      <p>Example: '/my-custom-page/'. Make sure to have leading and trailing slashes. You will access it at yoursite.com/p/my-custom-page/</p>
                    </div>
                   
                    <div class="form-group">
                      <label for="userName">Page Title</label>
                      <input type="text" name="nick" parsley-trigger="change" required placeholder="" class="form-control" id="userName">
                     
                    </div>
                     
                    

                    <div class="form-group">
                      <label for="userName">Content</label>
                      <div class="summernote">
                                <p></p>
                               </div>
                    </div>

                    <div class="form-group">
                      <input type="checkbox" name=""> <b>Login required</b> (If this is checked, only logged-in users will be able to view the page.)
                    </div>
                    <div class="form-group">
                     <input type="checkbox" name=""> <b>Use theme base</b> (Check this, if you want your content to be rendered inside the store's theme's header and footer)
                    </div>


                   
                   
                    <div class="form-group m-b-0">
                      <button class="btn btn-success waves-effect waves-light" type="Save">
                        Save
                      </button>
                      
                    </div>
                    
                  </form>
                </div>
              </div>
              
              
            </div>


      </div>
    </div>
  </div>


<?php $this->load->view('Template/footer.php') ?>

<script>
$(document).ready(function(){

    $(".select2").select2();
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
