<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <div class="btn-group pull-right m-t-15">
                                    <a href="javascript:;" class="btn btn-default btn-sm m-l-5" data-toggle="modal" data-target="#Add_plugin"> <i class="fa fa-plus m-r-5"></i>Add Plugin</a>
                                </div>
                                <h4 class="page-title">Plugin Management</h4>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-sm-12">
                        <?php
                        if($this->session->flashdata('success'))
                        {
                            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                        }
                        if($this->session->flashdata('error'))
                        {
                            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                        }
                        ?>
                        </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="card-box table-responsive">
                              <table class="table table-striped table-bordered table_shop_custom">
                                <thead>
                                  <tr>
                                    <th>Piugin Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(count($pluginList) > 0)
                                {
                                    foreach($pluginList as $pluginListValue)
                                    {
                                        if($pluginListValue->houdin_plugin_active_status == 'active')
                                        {
                                            $setStatusText = 'Active';
                                            $setStatusClass = 'success';
                                        }
                                        else
                                        {
                                            $setStatusText = 'Deactive';
                                            $setStatusClass = 'warning';
                                        }
                                     ?>
                                     <tr>
                                       <?php
                                       $varr=$pluginListValue->houdin_plugin_countries;
                                       $VAlll=json_decode($varr);
                                       ?>
                                        <td><?php echo $pluginListValue->houdin_plugin_name ?></td>
                                        <td><button type="button" class="btn btn-<?php echo $setStatusClass; ?> btn btn-xs"><?php echo $setStatusText; ?></button></td>
                                        <td>
                                            <button data-status="<?php echo $pluginListValue->houdin_plugin_active_status ?>" data-description="<?php echo $pluginListValue->houdin_plugin_description ?>" data-id="<?php echo $pluginListValue->houdin_plugin_id ?>" data-name="<?php echo $pluginListValue->houdin_plugin_name ?>" class="btn btn-warning btn btn-sm editPluginBtnData"><i class="fa fa-pencil m-r-5"></i>Edit</button>
                                            <button data-id="<?php echo $pluginListValue->houdin_plugin_id ?>" type="button" class="btn btn-warning btn btn-sm deletePluginButtonData"><i class="fa fa-trash m-r-5"></i>Delete</button>
                                           <button data-id="<?php echo $pluginListValue->houdin_plugin_id ?>" data-array='<?php echo $pluginListValue->houdin_plugin_countries ?>' type="button" class="btn-warning btn btn-sm price_Add" data-toggle="modal" data-target="#myModal">Add price</button>

                                        </td>
                                      </tr>
                                    <?php } }
                                    ?>

                                </tbody>
                              </table>
                              <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
                            </div>
                          </div>
                        </div>

                    </div> <!-- container -->
                </div> <!-- content -->
                <!-- Add plugin data -->
<div id="Add_plugin" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Package/plugin' ), array( 'id' => 'addPluginForm','enctype'=>'multipart/form-data' ));?>
              <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 class="modal-title">Add Plugin</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                      <label for="field-7" class="control-label">Plugin Name</label>
                                      <input type="text" name="pluginName" class="form-control required_validation_for_add_plugin name_validation" placeholder="Plugin Name" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                      <label for="field-7" class="control-label">Plugin Description</label>
                                      <textarea name="plugindescription" class="form-control required_validation_for_add_plugin name_validation" rows="4"></textarea>
                                </div>
                            </div>
                        </div>
                      <!-- <div class="row">
                          <div class="col-md-12">
                              <div class="form-group no-margin">
                                  <label for="field-7" class="control-label">Image</label>
                                    <input type="file" name="name" class="form-control" />
                              </div>
                          </div>
                    </div> -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="field-7" class="control-label">Status</label>
                                  <select class="form-control required_validation_for_add_plugin" name="pluginStatus">
                                    <option value="">Choose Status</option>
                                    <option value="active">Active</option>
                                    <option value="deactive">Deactive</option>
                                  </select>
                            </div>
                        </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                  <input type="submit" class="btn btn-info" name="addPluginBtn" value="Submit">
                </div>
          </div>
          <?php echo form_close();?>
    </div>
</div>
<div id="editPluginModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Package/plugin' ), array( 'id' => 'editPluginForm','enctype'=>'multipart/form-data' ));?>
              <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 class="modal-title">Edit Plugin</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                      <label for="field-7" class="control-label">Plugin Name</label>
                                      <input type="hidden" class="editPluginId" name="editPluginId"/>
                                      <input type="text" class="form-control required_validation_for_edit_plugin name_validation editPluginName" name="editPluginName" placeholder="Plugin Name" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                      <label for="field-7" class="control-label">Plugin Description</label>
                                      <textarea class="form-control required_validation_for_edit_plugin name_validation editPluginDescription" rows="4" name="editPluginDescription"></textarea>
                                </div>
                            </div>
                        </div>
                      <!-- <div class="row">
                          <div class="col-md-12">
                              <div class="form-group no-margin">
                                  <label for="field-7" class="control-label">Image</label>
                                    <input type="file" class="form-control" name="name" />
                              </div>
                          </div>
                    </div> -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group no-margin">
                                <label for="field-7" class="control-label">Status</label>
                                  <select class="form-control required_validation_for_edit_plugin editPluginStatus" name="editPluginStatus">
                                    <option value="">Choose Status</option>
                                    <option value="active">Active</option>
                                    <option value="deactive">Deactive</option>
                                  </select>
                            </div>
                        </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                  <input type="submit" class="btn btn-info" name="editPluginData" value="Submit">
                </div>
          </div>
          <?php echo form_close();?>
    </div>
</div>
<div id="deletePluginModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
<div class="modal-dialog">
<?php echo form_open(base_url( 'Package/plugin' ));?>
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
<h4 class="modal-title">Delete Entry</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-12">
<input type="hidden" class="deletePluginId" name="deletePluginId"/>
<h4><b>Do you really want to delete this entry ?</b></h4>
</div>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
<input type="submit" class="btn btn-info" name="deletePluginBtn" value="Delete">
</div>
</div>
<?php echo form_close();?>
</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
     <?php echo form_open(base_url( 'Package/plugin' ), array( 'id' => 'addPricePluginForm','enctype'=>'multipart/form-data','method'=>'post' ));?>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Price</h4>
      </div>
      <div class="modal-body">
      <table style="width: 100%;border: none;
    border: 0px;" cellpadding="10" border="1" class="table table-striped table-bordered table_shop_custom">
    <thead>
      <th style="border:1px solid #ebeff2"></th>
      <th style="border:1px solid #ebeff2">Name of product</th>
       <th style="border:1px solid #ebeff2">Description of product</th>

    </thead>
<input type="hidden" name="hidden_id"  class="hidden_id"/>
        <?php 
        
        foreach($country as $value)
        {
           /* if(in_array($value['houdin_country_name'],$params))
            {
                $this->db->where('houdin_country_status','active');
            }*/
            ?>
        <tr class="checkboxClass">
          <td><input type="checkbox" class="oncheck" name="country[]" value="<?php echo $value['houdin_country_name']; ?>"></td>
          <td class="country_name"><?php echo $value['houdin_country_name']; ?></td>
          <td style="padding: 0px;
    line-height: 34px;border:none!important">
    <input class="input_price required_validation_for_register" disabled="disabled" style="width: 100%;border-right: 1px solid #e5e5e5;border-left: none;border-top: 1px solid #e5e5e5;border-bottom: 0px;padding-left:10px" type="text" name="price[]"></td>
        </tr>
        <?php } ?>

      </table>

      </div>
      <div class="modal-footer">
        <button type="submit" name="saveCountries" value="yes" class="btn btn-default">Submit</button>
      </div>
    </div>
<?php echo form_close();?>
  </div>
</div>
<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addPluginForm',function(){
			var check_required_field='';
			$(".required_validation_for_add_plugin").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#editPluginForm',function(){
			var check_required_field='';
			$(".required_validation_for_edit_plugin").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});



    	$(document).ready(function(){
		$(document).on('change','.oncheck',function(){


          if($(this).is(':checked')){
            $(this).parents('.checkboxClass').find('.input_price').prop("disabled",false);
            }
            else
            {
             $(this).parents('.checkboxClass').find('.input_price').prop("disabled",true);
            }
          });
          });


 $(document).on('submit','#addPricePluginForm',function(c){

                               var rep_image_val='';
                 $(this).find(".required_validation_for_register").each(function()
               {
                       if($(this).prop("disabled")==false)
                       {
                        var val22 = jQuery(this).val();

                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");

3
                        }
                        }
                });

                $(this).find('.required_validation_for_register').on('keyup blur',function()
                                {

                                        $(this).css("border-color","#ccc");
                                });

                            if(rep_image_val)
                {
                      //  c.preventDefault();
                      //  return false;
                }
                else
                {

                }

          });

            $(document).on('click','.price_Add',function()
          {
            var ids = $(this).attr('data-array');
            $('#addPricePluginForm')[0].reset();
             $('.checkboxClass').find('.input_price').prop("disabled",true);
            
            $.each($.parseJSON(ids), function(idx, obj) {
            	  var counob=obj.country;
                var coun=$('.oncheck[value="'+counob+'"]').prop('checked',true).trigger('change');
               
               $('.oncheck[value="'+counob+'"]').parents('.checkboxClass').find('.input_price').val(obj.price);
                  // input_price
                console.log("counob",counob);
                console.log("coun",coun);
                

            });


            $('.hidden_id').val($(this).attr('data-id'));
            });

             var names_arr = ['sonarika','yogesh','sumit','vijay','anil'];
             console.log( 'Index : ' + jQuery.inArray('sumit', names_arr) );
	</script>
