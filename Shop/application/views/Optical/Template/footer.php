 <!-- Footer Part Start -->
  <!--  FOOTER START  -->
  <?php 
  $getDBName = getDBName();
  $con = mysqli_connect("localhost","root","houdine123",$getDB);
  $getShopData = mysqli_query($con,"select houdinv_shop_business_name,houdinv_shop_address,houdinv_shop_communication_email,houdinv_shop_contact_info from houdinv_shop_detail where houdinv_shop_id=1");
  $getShopValue = mysqli_fetch_array($getShopData);
  // get footer social links
  $getShopSocialLink = mysqli_query($con,'select * from houdinv_social_links');
  $getSocialLinks = mysqli_fetch_array($getShopSocialLink);
  // get footer url
  $getUrl = mysqli_query($con,"select * from houdinv_navigation_store_pages");
  $setFooterLinks = array();
  while($getFooterUrl = mysqli_fetch_array($getUrl))
  {
    array_push($setFooterLinks,$getFooterUrl['houdinv_navigation_store_pages_name']);
  }
  ?>
  <?php
  $getCurrency = getShopCurrency();
  $currencysymbol=$getCurrency[0]->houdin_users_currency;
  ?>
   <div class="modal fade" id="add_Quotation_data" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title">Ask Quotation</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>

        </div>
            <form class="modal-body" id="add_Quotation_dataform" method="post" action="<?php echo base_url(); ?>Orders/quatationAdd">

		        <div class="row">
             <div class="col-md-12">

                <div class="card-box" style="float:left;width: 100%;">
                    <div class="form-group">
                        <div class="col-md-12">
                          <div class="form-group">
                        <label>Name</label>
                        <input type="text"  class="form-control require_quote" name="name" placeholder="name" />
                      </div>
                       </div>
                      <div class="col-md-12">
                      <div class="form-group">
                        <label>Email</label>
                         <input type="email"  class="form-control require_quote email_validation" name="email" placeholder="email" />
                      </div>
                        </div>

                      <div class="col-md-12">
                      <div class="form-group">
                        <label>Phone</label>
                         <input type="number"  pattern="^[-+]?\d+$"  class="phone_telephone form-control require_quote" name="phone" placeholder="phone" />
                      </div>
                        </div>

                        <div class="col-md-12">
                      <div class="form-group">
                        <label>For product count</label>
                         <input type="number"  class="form-control require_quote number_validation" name="count" placeholder="product count" />
                      </div>
                        </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Description</label>
                         <textarea class="form-control require_quote" name="comment"></textarea>
                      </div>
                        </div>


                    <div class="col-md-12">
                      <div class="form-group">
                      <input type="hidden" value="" id="product_quete_id" name="product_id"/>
                        <input type="submit" style="background: #33d286;color: #fff;z-index: 99999999;top: 15px;left: 8px;padding: 3px 12px;border-radius: 2px;" class="form-control"  value="submit" name="quote_submit"  />
                        </div>
                        </div>


                </div>
              </div>
        </div>
</div>

          </form>




      </div>
    </div>
  </div>
        <section id="footer">

<div class="footer-bg">

<div class="container">

<div class="row">

<div class="footer-main">



<div class="col-md-3 col-sm-6">

<div class="account">

<h2>INFORMATION</h2>
<?php
if(in_array('about',$setFooterLinks))
{
echo '<a href="'.base_url().'About">About Us</a>';
}
if(in_array('terms',$setFooterLinks))
{
echo '<a href="'.base_url().'Terms">Terms & Conditions</a>';
}
if(in_array('privacy',$setFooterLinks))
{
echo '<a href="'.base_url().'Privacy">Privacy Policy</a>';
}
if(in_array('faq',$setFooterLinks))
{
echo '<a href="'.base_url().'Faq">FAQs</a>';
}
if(in_array('contact',$setFooterLinks))
{
echo '<a href="'.base_url().'Contact">Contact Us</a>';
}
?>
</div>

</div>

<div class="col-md-3 col-sm-6">

<div class="account">

<h2>SERVICES</h2>



<?php
if(in_array('disclaimer',$setFooterLinks))
{
echo '<a href="'.base_url().'Disclaimer">Disclaimer</a>';
}
if(in_array('shipping',$setFooterLinks))
{
echo '<a href="'.base_url().'Shippingpolicy">Shipping & Delivery</a>';
}
if(in_array('refund',$setFooterLinks))
{
echo '<a href="'.base_url().'Refund">Cancellation & Refund</a>';
}
?>




</div>

</div>


<div class="col-md-3 col-sm-6">

<div class="contact contact-detail">

<h2>Contact us</h2>

<p><i class="fa fa-map-marker"></i> <a href="javascript:;"><?php if($getShopValue['houdinv_shop_address']) { echo $getShopValue['houdinv_shop_address']; } else { echo "--"; } ?></a> </p>

<p><i class="fa fa-phone"></i> <?php if($getShopValue['houdinv_shop_contact_info']) { echo $getShopValue['houdinv_shop_contact_info']; } else { echo "--"; } ?> </p>

<p><i class="fa fa-envelope"></i> <?php if($getShopValue['houdinv_shop_communication_email']) { echo $getShopValue['houdinv_shop_communication_email']; } else { echo "--"; } ?> </p>

</div>

</div>

<div class="col-md-3 col-sm-6">

<div class="contact social-contact">

<h2>Stay Connected</h2>

<ul >
  <?php
  if($getSocialLinks['facebook_url'] && $getSocialLinks['facebook_url'] != "")
  {
  ?>
  <li><a href="https://<?php echo $getSocialLinks['facebook_url'] ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
  <?php }
  ?>
  <?php
  if($getSocialLinks['instagram_url'] && $getSocialLinks['instagram_url'] != "")
  {
  ?>
  <li><a href="https://<?php echo $getSocialLinks['instagram_url'] ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
  <?php }
  ?>
  <?php
  if($getSocialLinks['linkedin_url'] && $getSocialLinks['linkedin_url'] != "")
  {
  ?>
  <li><a href="https://<?php echo $getSocialLinks['linkedin_url'] ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
  <?php }
  ?>
  <?php
  if($getSocialLinks['twitter_url'] && $getSocialLinks['twitter_url'] != "")
  {
  ?>
  <li><a href="https://<?php echo $getSocialLinks['twitter_url'] ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
  <?php }
  ?>
</ul>



</div>

</div>




</div>

</div>

</div>

</div>
</section>
<!-- Footer Part End -->
<!-- Footer Bottom Start -->
<section id="footer-btm">

<div class="container">

<div class="row text-center">

<div class="col-md-12 col-xs-12">

<div class="copywright">

<p>Copyright &copy; <?php echo date('Y') ?>. All right reserved by <?php if($getShopValue['houdinv_shop_business_name']) { echo $getShopValue['houdinv_shop_business_name']; } else  { echo "Houdin-e"; } ?> </p>

</div>

</div>

</div>

</div>
</section>




<!-- Footer Bottom End -->
<script src="<?php echo base_url() ?>Extra/optical/js/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url() ?>Extra/optical/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>Extra/optical/js/slick.js"></script>
<script src="<?php echo base_url() ?>Extra/optical/js/jquery.countdown.min.js"></script>
<script src="<?php echo base_url() ?>Extra/optical/js/jquery.meanmenu.min.js"></script>
<script src="<?php echo base_url() ?>Extra/optical/js/main.js"></script>
<script src="<?php echo base_url() ?>Extra/optical/js/validation.js"></script>
<script src="<?php echo base_url() ?>Extra/optical/js/maininit.js"></script>
<script src="<?php echo base_url() ?>Extra/optical/js/intlTelInput.js"></script>

<script type="text/javascript">
    var My_variable='<?php echo  $this->session->userdata('userAuth'); ?>';
     var base_url='<?php echo base_url(); ?>';
          var setCurrency = '<?php echo $currencysymbol; ?>';
    </script>
    <script type="text/javascript">


                $('#searchboxsuggestionproduct #searchfiltertext').on('keyup',function(){


                       var val = $(this).val();
                       val = val.replace(/^\s|\s+$/, "");
                       if (val) {
                        $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-spinner fa-spin"></i>');
       $.ajax({
       type: "Get",
       url: '<?=base_url();?>search?key='+val,
       dataType: "json",
       success: function(data) {
         if(data){
         var entery = data['entries'];
         var cartstatus = data['cart'];
         $('#searchfilterdataheader').html('').show();
         $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
         if(entery){
           var allcat='';
           var cat='';

           for (var i=0;i<entery.length;i++) {
             if(cartstatus=='yes'){
               var cartbtn='<a   data-variant="'+entery[i]['clientLevelId']+'" data-cart="'+entery[i]['id']+'" title="Add cart" class="linkcat_search_cart Add_to_cart_button"><i class="fa fa-shopping-bag" aria-hidden="true"></i></a>';
             }

           if(entery[i]['category']=="Category"){
                   cat+=' <li class="desktop-suggestion null"><a  href="'+entery[i]['action']+'" class="linkcat_search" title="'+entery[i]['name']+'">'+entery[i]['name']+'</a></li>'
               }
               else{
                 allcat+=' <li class="desktop-suggestion null"><a href="'+entery[i]['action']+'" class="linkcat_search" title="'+entery[i]['name']+'">'+entery[i]['name']+'</a>'+cartbtn+'</li>'
               }

             }
             // console.log(cat);
             if(cat || allcat){

                 if(allcat){
                     // <ul class="desktop-group">
                     allcat = ' <li class="desktop-suggestionTitle">All Others</li>'+allcat;

                 }
                 if(cat){
                   cat = ' <li class="desktop-suggestionTitle">Categories</li>'+cat;
                 }

                   $('#searchfilterdataheader').html('<ul class="desktop-group">'+allcat+cat+'</ul>');

             }
             else{
               $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
             }

         }
         else{
           $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
         }
       //  console.log(data['entries']);

          }
       },
       error: function(){
         $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
         $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
       }
   });



                       } else {
                         $('#searchfilterdataheader').html('').hide();
       $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
                       }
   });


               function searchForData(value, isLoadMoreMode) {
                       // create the ajax object
                       ajax = new XMLHttpRequest();
                       // the function to execute on ready state is changed
                       ajax.onreadystatechange = function() {
                               if (this.readyState === 4 && this.status === 200) {
                                       try {
                                               var json = JSON.parse(this.responseText)
                                       } catch (e) {
                                               noUsers();
                                               return;
                                       }

                                       if (json.length === 0) {
                                               if (isLoadMoreMode) {
                                                       alert('No more to load');
                                               } else {
                                                       noUsers();
                                               }
                                       } else {
                                               showUsers(json);
                                       }


                               }
                       }
                       // open the connection
                       ajax.open('GET', '<?=base_url();?>search?key=' + value + '&startFrom=' + loadedUsers , true);
                       // send
                       ajax.send();
               }

               function showUsers(data) {
                       // the function to create a row
     //console.log(data);


                       // loop through the data

               }



 </script>

 <!-- update customer visit -->
 <script type="text/javascript">
 $(document).ready(function(){
   $.ajax({
               type: "POST",
               url: base_url+"Logout/visitorCount",
               success: function(data) {

               }
               });
 })
 </script>

<!--<script>
$(document).ready(function() {
//Preloader
$(window).on("load", function() {
preloaderFadeOutTime = 500;
function hidePreloader() {
var preloader = $('.spinner-wrapper');
preloader.fadeOut(preloaderFadeOutTime);
}
hidePreloader();
});
});
</script>-->
<script type="text/javascript">

  jQuery(document).ready(function($) {

// site preloader -- also uncomment the div in the header and the css style for #preloader
$(window).load(function(){
  $('#preloader').fadeOut('slow',function(){$(this).remove();});
});

});
</script>
<script>
    $(".phone_telephone").intlTelInput({
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: "body",
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "auto",
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: "<?php echo base_url() ?>Extra/apparels/js/utils.js"
    });
  </script>
</div>
</body>


</html>
