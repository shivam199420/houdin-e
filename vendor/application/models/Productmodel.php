<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productmodel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
        $this->productTbl = 'houdinv_products';
        $this->productVariantTbl = 'houdinv_products_variants';
    }

/**
 * InsertProductData()
 * @purpose: to insert product and variant data
 * @update by: shivam singh sengar
 * @updated at: 30 Nov 2019 07:22 PM
 * @param DATA $data ARRAY
 * @param SUPPLIERS $suppliers ARRAY
 * @param VARIANT $variant ARRAY
 * @param SALESTAX $salestax ARRAY
 * @return json ( mixed response )
 */
public function InsertProductData($data=false,$suppliers,$variant,$salestax = false)
{
    // get product table
    $this->db->insert($this->productTbl,$data);
    $id =  $this->db->insert_id();
    
    // add product suppliers if exist
    if($suppliers) { 
        $this->AddSupliers($id,$suppliers);  
    }
    // if product id exist 
    if($id) {
        // add accounting from inventory asset account
        // add Accounting for products
        $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','2')->where('houdinv_accounts_detail_type_id','7')->get()->result();    
        if (count($getUndepositedAccount) > 0) {
            if ($data['houdinv_products_total_stocks'] > 0) {
                // get admin id
                $getMaster = $this->load->database('master',true);
                $getVendorId = $getMaster->select('houdin_vendor_auth_vendor_id')->from('houdin_vendor_auth')->where('houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))->get()->result();
                if ($getVendorId[0]->houdin_vendor_auth_vendor_id) {
                    $setVendorData = $getVendorId[0]->houdin_vendor_auth_vendor_id;
                } else {
                    $setVendorData = 0;
                }
                // get amount data
                $setProductDesc = array('type'=>'product','id'=>$id);
                $getPriceData = json_decode($data['houdin_products_price'],true);
                $getFinalAmount = $data['houdinv_products_total_stocks']*$getPriceData['cost_price'];
                $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                'houdinv_accounts_balance_sheet_ref_type'=>'Inventory Asset Account',
                'houdinv_accounts_balance_sheet_pay_account'=>$setVendorData,
                'houdinv_accounts_balance_sheet_payee_type'=>'Vendor',
                'houdinv_accounts_balance_sheet_account'=>0,
                'houdinv_accounts_balance_sheet_order_id'=>0,
                'houdinv_accounts_balance_sheet_purchase_id'=>0,
                'houdinv_accounts_balance_sheet_product_desc'=>json_encode($setProductDesc),
                'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                'houdinv_accounts_balance_sheet__increase'=>0,
                'houdinv_accounts_balance_sheet_decrease'=>$getFinalAmount,
                'houdinv_accounts_balance_sheet_deposit'=>0,
                'houdinv_accounts_balance_sheet_final_balance'=>0,
                'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                // get amount
                $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                if (count($getTotalAmount) > 0) {
                    $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                } else {
                    $setFinalAmount = 0;
                }
                $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
            }
        }
        // end here
        if (count ($variant)) {
            // add prodcut variant
            for ($index = 0; $index < count($variant['variantData']); $index++) {
                $getMainProductPrice = json_decode($data['houdin_products_price'],true);

                // set sales tax
                if ($salestax) {
                    $getTaxDataValue = $this->getSalesTax($salestax);
                    $getsalePrice = $variant['new_sp'][$index];
                    $getTaxData = ($getsalePrice*$getTaxDataValue)/100;
                } else {
                        $getTaxData = 0;
                }

                // set varint discount
                if ($getMainProductPrice['discount']) {
                    $finalPrice = ($variant['new_sp'][$index]-$getMainProductPrice['discount'])+$getTaxData;
                } else {
                    $finalPrice = $variant['new_sp'][$index]+$getTaxData;
                }

                // set product stock
                $getProductStock = json_decode($data['houdinv_products_main_stock'],true);
                for ($set = 0; $set < count($getProductStock); $set++) {
                    $setArrayData[] = array('main'=>$getProductStock[$set]['main'],'value'=>$variant['new_qty'][$index],'date'=>$getProductStock['date']);
                }

                // set variant price
                $setPrice = array('price'=>$variant['new_sp'][$index],'discount'=>$getMainProductPrice['discount'],'cost_price'=>$variant['new_cp'][$index],'sale_price'=>$variant['new_mrp'][$index],'margin'=>$variant['new_margin'][$index]);

                // set product variant
                $setInsertArray = array('houdinv_products_variants_name'=>$variant['variantData'][$index],
                'houdinv_products_variants_barcode'=>$variant['barcode_data'][$index],
                'houdinv_products_variants_new_price'=>json_encode($setPrice),
                'houdin_products_variants_title'=>$variant['new_title'][$index],
                'houdin_products_variants_prices'=>$variant['new_mrp'][$index],
                'houdinv_products_variants_final_price'=>$finalPrice,
                'houdin_products_variants_sku'=>'test',
                'houdin_products_variants_stock'=>json_encode($setArrayData),
                'houdinv_products_variants_total_stocks'=>$variant['new_qty'][$index],
                'houdin_products_variants_image'=>$variant['pictureDataVariant'][$index],
                'houdin_products_variants_product_id'=>$id);
                $this->db->insert('houdinv_products_variants',$setInsertArray);
                $getInventoryProductId = $this->db->insert_id();
                
                // add Accounting for invenotry
                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','2')->where('houdinv_accounts_detail_type_id','7')->get()->result();    
                if (count($getUndepositedAccount) > 0) {
                    if ($variant['new_qty'][$index] > 0) {
                        // get admin id
                        $getMaster = $this->load->database('master',true);
                        $getVendorId = $getMaster->select('houdin_vendor_auth_vendor_id')->from('houdin_vendor_auth')->where('houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))->get()->result();
                        if ($getVendorId[0]->houdin_vendor_auth_vendor_id) {
                        $setVendorData = $getVendorId[0]->houdin_vendor_auth_vendor_id;
                        } else {
                            $setVendorData = 0;
                        }
                        // get amount data
                        $setProductDesc = array('type'=>'inventory','id'=>$getInventoryProductId);
                    
                        $getFinalAmount = $variant['new_qty'][$index]*$variant['new_cp'][$index];
    
                        $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                            'houdinv_accounts_balance_sheet_ref_type'=>'Inventory Asset Account',
                            'houdinv_accounts_balance_sheet_pay_account'=>$setVendorData,
                            'houdinv_accounts_balance_sheet_payee_type'=>'Vendor',
                            'houdinv_accounts_balance_sheet_account'=>0,
                            'houdinv_accounts_balance_sheet_order_id'=>0,
                            'houdinv_accounts_balance_sheet_purchase_id'=>0,
                            'houdinv_accounts_balance_sheet_product_desc'=>json_encode($setProductDesc),
                            'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                            'houdinv_accounts_balance_sheet__increase'=>0,
                            'houdinv_accounts_balance_sheet_decrease'=>$getFinalAmount,
                            'houdinv_accounts_balance_sheet_deposit'=>0,
                            'houdinv_accounts_balance_sheet_final_balance'=>0,
                            'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                            $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                            // get amount
                        $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                        ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                        if (count($getTotalAmount) > 0) {
                            $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                        } else {
                            $setFinalAmount = 0;
                        }
                        $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                    }
                }
            // end here
            }
        }
    }
    return $id;
}
    public function fetchAllSupplier()
    {
      $data =   $this->db->select("*")
        ->from("houdinv_suppliers")
        ->where("houdinv_supplier_active_status","active")
        ->get()->result();
        
        return $data;
    }
    
    
    public function UpdateProductData($data=false,$Id=false,$variant=false,$suppliers)
    {
        // get old product quantity
        $getProductOldQuantity = $this->db->select('houdinv_products_total_stocks')->from($this->productTbl)->where('houdin_products_id',$Id)->get()->result();
        if(count($getProductOldQuantity) > 0)
        {
            $setQuantity = $getProductOldQuantity[0]->houdinv_products_total_stocks;
        }
        else
        {
            $setQuantity = 0;
        }
        $this->db->where("houdin_products_id",$Id);
        $this->db->update($this->productTbl,$data);
        // add Accounting for products
        $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','2')->where('houdinv_accounts_detail_type_id','7')->get()->result();    
        if(count($getUndepositedAccount) > 0)
        {
            // get admin id
            $getMaster = $this->load->database('master',true);
            $getVendorId = $getMaster->select('houdin_vendor_auth_vendor_id')->from('houdin_vendor_auth')->where('houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))->get()->result();
            if($getVendorId[0]->houdin_vendor_auth_vendor_id)
            {
                $setVendorData = $getVendorId[0]->houdin_vendor_auth_vendor_id;
            }
            else
            {
                $setVendorData = 0;
            }
            // get amount data
            $setProductDesc = array('type'=>'product','id'=>$Id);
            $getPriceData = json_decode($data['houdin_products_price'],true);
            if($setQuantity)
            {
                $getFinalQuantity = $data['houdinv_products_total_stocks']-$setQuantity;
            }
            else
            {
                $getFinalQuantity = $data['houdinv_products_total_stocks'];
            }
            if($getFinalQuantity > 0)
            {
                // add data to value
                $getFinalAmount = $getFinalQuantity*$getPriceData['cost_price'];
                $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                'houdinv_accounts_balance_sheet_ref_type'=>'Inventory Asset Account',
                'houdinv_accounts_balance_sheet_pay_account'=>$setVendorData,
                'houdinv_accounts_balance_sheet_payee_type'=>'Vendor',
                'houdinv_accounts_balance_sheet_account'=>0,
                'houdinv_accounts_balance_sheet_order_id'=>0,
                'houdinv_accounts_balance_sheet_purchase_id'=>0,
                'houdinv_accounts_balance_sheet_product_desc'=>json_encode($setProductDesc),
                'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                'houdinv_accounts_balance_sheet__increase'=>0,
                'houdinv_accounts_balance_sheet_decrease'=>$getFinalAmount,
                'houdinv_accounts_balance_sheet_deposit'=>0,
                'houdinv_accounts_balance_sheet_final_balance'=>0,
                'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                // get amount
                $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                if(count($getTotalAmount) > 0)
                {
                    $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                }
                else
                {
                    $setFinalAmount = 0;
                }
                $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
            }
        }
        // end here
        // update variant data
        if(count($variant['variantData']) > 0)
        {
            for($index = 0; $index < count($variant['variantData']); $index++)
            {
                $getDiscount = json_decode($data['houdin_products_price'],true);
                if($getDiscount['discount'])
                {
                    $finalPrice = $variant['all_new_price'][$index]-$getDiscount['discount'];
                }
                else
                {
                    $finalPrice = $variant['all_new_price'][$index];
                }
                // set variant stock
                $getStockdata = json_decode($data['houdinv_products_main_stock'],true);
                for($set = 0; $set < count($getStockdata); $set++)
                {
                    $setStockArray[] = array('main'=>$getStockdata[$set]['main'],'value'=>$variant['all_new_quantity'][$index],'date'=>$getStockdata[$set]['date']);
                }

                $setPriceArray = array("price"=>$variant['all_new_price'][$index],'discount'=>$getDiscount['discount'],'cost_price'=>$variant['all_new_cp'][$index],
                'sale_price'=>$variant['all_new_sp'][$index],'margin'=>$variant['all_new_margin'][$index]);
                
                if($variant['pictureVariantData'][$index])
                {
                    $setInsertArray = array('houdinv_products_variants_name'=>$variant['variantData'][$index],
                    'houdinv_products_variants_barcode'=>$variant['all_new_barcode_data'][$index],
                    'houdinv_products_variants_new_price'=>json_encode($setPriceArray),
                    'houdin_products_variants_title'=>$variant['all_new_title'][$index],
                    'houdin_products_variants_prices'=>$variant['all_new_price'][$index],
                    'houdinv_products_variants_final_price'=>$finalPrice,
                    'houdin_products_variants_sku'=>'test',
                    'houdin_products_variants_stock'=>json_encode($setStockArray),
                    'houdinv_products_variants_total_stocks'=>$variant['all_new_quantity'][$index],
                    'houdin_products_variants_image'=>$variant['pictureVariantData'][$index]);
                }
                else
                {
                    $setInsertArray = array('houdinv_products_variants_name'=>$variant['variantData'][$index],
                    'houdinv_products_variants_barcode'=>$variant['all_new_barcode_data'][$index],
                    'houdinv_products_variants_new_price'=>json_encode($setPriceArray),
                    'houdin_products_variants_title'=>$variant['all_new_title'][$index],
                    'houdin_products_variants_prices'=>$variant['all_new_price'][$index],
                    'houdinv_products_variants_final_price'=>$finalPrice,
                    'houdin_products_variants_sku'=>'test',
                    'houdin_products_variants_stock'=>json_encode($setStockArray),
                    'houdinv_products_variants_total_stocks'=>$variant['all_new_quantity'][$index]);
                }
                $this->db->where('houdin_products_variants_id',$variant['id'][$index])->update('houdinv_products_variants',$setInsertArray);
                 // add Accounting for variants
                //  get inventory 
                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','2')->where('houdinv_accounts_detail_type_id','7')->get()->result();    
                if(count($getUndepositedAccount) > 0)
                {
                    // get admin id
                    $getMaster = $this->load->database('master',true);
                    $getVendorId = $getMaster->select('houdin_vendor_auth_vendor_id')->from('houdin_vendor_auth')->where('houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))->get()->result();
                    if($getVendorId[0]->houdin_vendor_auth_vendor_id)
                    {
                        $setVendorData = $getVendorId[0]->houdin_vendor_auth_vendor_id;
                    }
                    else
                    {
                        $setVendorData = 0;
                    }
                    // get amount data  

                    $getTotalVariantStock = $this->db->select('houdinv_products_variants_total_stocks')->from('houdinv_products_variants')->where('houdin_products_variants_id',$variant['id'][$index])->get()->result();
                    if($getTotalVariantStock[0]->houdinv_products_variants_total_stocks)
                    $setQuantity = $getTotalVariantStock[0]->houdinv_products_variants_total_stocks;
                    $setProductDesc = array('type'=>'variant','id'=>$variant['id'][$index]);
                    $getPriceData = json_decode($variant['all_new_price'][$index],true);
                    if($setQuantity)
                    {
                        $getFinalQuantity = $variant['all_new_quantity'][$index]-$setQuantity;
                    }
                    else
                    {
                        $getFinalQuantity = $data['houdinv_products_total_stocks'];
                    }
                    if($getFinalQuantity > 0)
                    {
                        // add data to value
                        $getCostPrice = $setPriceArray['cost_price'];

                        $getFinalAmount = $getFinalQuantity*$getCostPrice;
                        $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                        'houdinv_accounts_balance_sheet_ref_type'=>'Inventory Asset Account',
                        'houdinv_accounts_balance_sheet_pay_account'=>$setVendorData,
                        'houdinv_accounts_balance_sheet_payee_type'=>'Vendor',
                        'houdinv_accounts_balance_sheet_account'=>0,
                        'houdinv_accounts_balance_sheet_order_id'=>0,
                        'houdinv_accounts_balance_sheet_purchase_id'=>0,
                        'houdinv_accounts_balance_sheet_product_desc'=>json_encode($setProductDesc),
                        'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                        'houdinv_accounts_balance_sheet__increase'=>0,
                        'houdinv_accounts_balance_sheet_decrease'=>$getFinalAmount,
                        'houdinv_accounts_balance_sheet_deposit'=>0,
                        'houdinv_accounts_balance_sheet_final_balance'=>0,
                        'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                        $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                        // get amount
                        $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                        ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                        if(count($getTotalAmount) > 0)
                        {
                            $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                        }
                        else
                        {
                            $setFinalAmount = 0;
                        }
                        $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                    }
                }
                // end here
            }
        }
        if($suppliers)
        {
          $this->AddSupliers($Id,$suppliers);  
        }
        return $Id;
    }
    public function AddSupliers($Id,$suppliers)
    {
    
      
        $date = strtotime(date("Y-m-d"));
        
        foreach($suppliers as $sub)
        {
          $data =   $this->db->select("*")
                    ->from("houdinv_supplier_products")
                    ->where("houdinv_supplier_products_product_id",$Id)
                     ->where("houdinv_supplier_products_supplier_id",$sub)
                    ->get()->row();  
                    
                    if(!$data)
                    {
                        $array = array("houdinv_supplier_products_supplier_id"=>$sub,
                                        "houdinv_supplier_products_product_id"=>$Id,
                                        "houdinv_supplier_products_created_at"=>$date,
                                        "houdinv_supplier_products_updated_at"=>$date   
                                          );
             $this->db->insert("houdinv_supplier_products",$array);
                        }
        }
        
        $this->db->where('houdinv_supplier_products_product_id',$Id);
        $this->db->where_not_in('houdinv_supplier_products_supplier_id',$suppliers);
         $this->db->delete("houdinv_supplier_products"); 
       
    }

/**
 * FetchProductData()
 * @purpose: to fetch product detail for listing
 * @updated by: Shivam Singh Sengar
 * @updated at: 30 Nov 2019 20:15 PM
 * @param PARAM $param
 * @retrun JSON ( mixed response )
 */
public function FetchProductData($params=false)
{
    // get warehoue list
    $WAREHOUSE= $this->session->userdata("WAREHOUSE");
    $this->db->select("houdin_products_id,houdinv_products_main_images,houdin_products_price,houdinv_products_total_stocks,houdin_products_status,houdin_products_id,houdin_products_title")
        ->from($this->productTbl)
        ->order_by("houdin_products_id","desc");
    if (!empty($WAREHOUSE->id)) {
        $this->db->where("FIND_IN_SET('".$WAREHOUSE->id."', houdin_products_warehouse)"); 
    }          
    if (array_key_exists("start",$params) && array_key_exists("limit",$params)) {
        $this->db->limit($params['limit'],$params['start']);
    } elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)) {
        $this->db->limit($params['limit']);
    }
    if (array_key_exists("returnType",$params) && $params['returnType'] == 'count') {
        $result = $this->db->count_all_results();
    } else {
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    return $result;
    }

/**
 * fetchAllProductStatus()
 * @purpose: to fetch product status
 * @updated by: shivam singh sengar
 * @updated at: 30 Nov 2019 20:18
 * @param NONE
 * @return JSON ( mixed json )
 */
public  function fetchAllProductStatus()
{        
    $WAREHOUSE= $this->session->userdata("WAREHOUSE");
    if (!empty($WAREHOUSE->id)) {
        $this->db->where("FIND_IN_SET('".$WAREHOUSE->id."', houdin_products_warehouse)"); 
    } 
    $data['AllProduct'] = $this->db->select("*")->from($this->productTbl)->count_all_results();
    if (!empty($WAREHOUSE->id)) {
        $this->db->where("FIND_IN_SET('".$WAREHOUSE->id."', houdin_products_warehouse)"); 
    } 
    $data['AllActive'] = $this->db->select("*")->from($this->productTbl)
                            ->where("houdin_products_status",1)->count_all_results();  
    if (!empty($WAREHOUSE->id)) {
        $this->db->where("FIND_IN_SET('".$WAREHOUSE->id."', houdin_products_warehouse)"); 
    } 
    $data['AllDective'] = $this->db->select("*")->from($this->productTbl)
      ->where("houdin_products_status",0)->count_all_results(); 
      return $data;
}
    
    
    
    
  public function FetchSingleProductData($id=false)
    {
       $sub =  $this->db->select("*")
       ->from("houdinv_products_variants")
       ->where("houdin_products_variants_product_id",$id)
       ->get()->result();
       
    $data['product'] = $this->db->select("*")
    ->from($this->productTbl)
      ->where("houdin_products_id",$id)
    ->get()->row();
    
    
   $final  =  $this->db->select("houdinv_supplier_products_supplier_id")
       ->from("houdinv_supplier_products")
       ->where("houdinv_supplier_products_product_id",$id)
       ->get()->result();
       
       foreach($final as $gh)
       {
        $data['supplierAdd'][] = $gh->houdinv_supplier_products_supplier_id;
       }
   
  
     $data['variant']=  $sub;
  
    return $data;           
    }
    
    
    public function ChangeStatus($id=false,$status=false)
    {
        $datac = array("houdin_products_status"=>$status);
        $this->db->where("houdin_products_id",$id);
        $this->db->update($this->productTbl,$datac);
        return $id;
        
    }
    
    public function FetchSkuPrefix()
    {
      $sub =  $this->db->select("*")
       ->from("houdinv_skusetting")
      
       ->get()->row();  
       return $sub;
    }
    
    
     public function deleteProduct($id=false)
    {
     $this->db->delete($this->productTbl,
      array('houdin_products_id' => $id)); 
      $this->db->delete('houdinv_products_variants',
      array('houdin_products_variants_product_id' => $id)); 
      
    }
    
    
      
      
      public function DeleteProductDatavarint($id)
      {
       
       // exit;
        $this->db->delete('houdinv_products_variants', array('houdin_products_variants_id' => $id)); 
      } 
      public function getTaxData()
      {
          return $this->db->select('*')->from('houdinv_taxes')->get()->result();
      }
      public function getSalesTax($data)
      {
          $getTax = $this->db->select('houdinv_tax_percenatge')->from('houdinv_taxes')->where('houdinv_tax_id',$data)->get()->result();
          return $getTax[0]->houdinv_tax_percenatge;
      }
    
    public function missingProductData($params = false)
    {
        // get warehoue list
        $this->db->select("*")
            ->from($this->productTbl)
            ->where('houdin_products_category','')
            ->order_by("houdin_products_id","desc");
        if (array_key_exists("start",$params) && array_key_exists("limit",$params)) {
            $this->db->limit($params['limit'],$params['start']);
        } elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)) {
            $this->db->limit($params['limit']);
        }
        if (array_key_exists("returnType",$params) && $params['returnType'] == 'count') {
            $result = $this->db->count_all_results();
        } else {
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        return $result;
    }

    public function updateMissingCategory($data) {
        foreach ($data as $category) {
            $category_list = [
                "houdin_products_category"=>$category['houdin_products_category'],
                "houdin_products_sub_category_level_1"=>$category['houdin_products_sub_category_level_1'],
                "houdin_products_sub_category_level_2"=>$category['houdin_products_sub_category_level_2'],
            ];
            $this->db->where('houdin_products_id',$category['product_id']);
            $this->db->update('houdinv_products',$category_list);    
        }
    }
    public function missingImagesData($params = false)
    {
        // get warehoue list
        $this->db->select("*")
            ->from($this->productTbl)
            ->order_by("houdin_products_id","desc");
        if (array_key_exists("start",$params) && array_key_exists("limit",$params)) {
            $this->db->limit($params['limit'],$params['start']);
        } elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)) {
            $this->db->limit($params['limit']);
        }
        if (array_key_exists("returnType",$params) && $params['returnType'] == 'count') {
            $result = $this->db->count_all_results();
        } else {
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        return $result;
    }
    public function updateMissingImages($data) {
        foreach ($data as $image) {
            if ($image['image']) {
                $image_list = [
                    'houdinv_products_main_images' => json_encode($image['image'])
                ];
                $this->db->where('houdin_products_id',$image['id']);
                $this->db->update('houdinv_products',$image_list);      
            }
        }
    }
 }