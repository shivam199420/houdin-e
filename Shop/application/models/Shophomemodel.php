<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shophomemodel extends CI_Model{
    function __construct() 
    {
    }
    public function fetchShopCategory($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getHomeCategoryData = $getDynamicDB->select('*')->from('houdinv_categories')->where('houdinv_category_status','active')->order_by('houdinv_category_id','DESC')->limit('4')->get()->result();
        if(count($getHomeCategoryData) > 0)
        {
            foreach($getHomeCategoryData as $getHomeCategoryDataList)
            {
                if($getHomeCategoryDataList->houdinv_category_thumb)
                {
                    $setImage = $this->session->userdata('vendorURL')."images/category/".$getHomeCategoryDataList->houdinv_category_thumb;
                }
                else
                {
                    $getShop = shopurl();
                    $setImage = "".$getShop."Extra/noimage.jpg";
                }
                $setCategoryList[] = array('categoryId'=>$getHomeCategoryDataList->houdinv_category_id,'categoryName'=>$getHomeCategoryDataList->houdinv_category_name,'categoryImage'=>$setImage);
            }
            return array('message'=>'Success','data'=>$setCategoryList);
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function fetchShoplatestPorudct($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getStoreSetting = $getDynamicDB->select('display_out_stock')->from('houdinv_storesetting')->where('id',1)->get()->result();
        $getProductList = $getDynamicDB->select('*')->from('houdinv_products')
        ->group_start()
        ->where('houdinv_products_show_on','Both')
        ->or_where('houdinv_products_show_on','App')
        ->group_end()
        ->order_by('houdin_products_id','DESC')->limit('4')->get()->result();
        if(count($getProductList) > 0)
        {
            foreach($getProductList as $getProductListData)
            {
                // get Product image
                $getImage = json_decode($getProductListData->houdinv_products_main_images,true);
                if($getImage[0])
                {
                    $setImage = $this->session->userdata('vendorURL')."upload/productImage/".$getImage[0];
                }
                else
                {
                    $setImage = "";
                }
                // set price
                $getPrice = json_decode($getProductListData->houdin_products_price,true);
                if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
                {
                    $getOriginalPrice = $getPrice['sale_price'];
                    $getDiscountPrice = $getProductListData->houdin_products_final_price;
                }
                else
                {
                    $getOriginalPrice = $getProductListData->houdin_products_final_price;
                    $getDiscountPrice = 0;
                }
                $getQuantity = $getProductListData->houdinv_products_total_stocks;
                if($getStoreSetting[0]->display_out_stock == 1)
                {
                    if($getQuantity > 0)
                    {
                        $setProductArray[] = array('productId'=>$getProductListData->houdin_products_id,'variantId'=>0,'productImage'=>$setImage,'productTitle'=>$getProductListData->houdin_products_title,'productPrice'=>$getOriginalPrice,'productdiscountPrice'=>$getDiscountPrice,'quotation'=>$getProductListData->houdinv_products_main_quotation,'stock'=>'yes');
                    }
                    else
                    {
                        $setProductArray[] = array('productId'=>$getProductListData->houdin_products_id,'variantId'=>0,'productImage'=>$setImage,'productTitle'=>$getProductListData->houdin_products_title,'productPrice'=>$getOriginalPrice,'productdiscountPrice'=>$getDiscountPrice,'quotation'=>$getProductListData->houdinv_products_main_quotation,'stock'=>'no');
                    }
                }
                else
                {
                    if($getQuantity > 0)
                    {
                        $setProductArray[] = array('productId'=>$getProductListData->houdin_products_id,'variantId'=>0,'productImage'=>$setImage,'productTitle'=>$getProductListData->houdin_products_title,'productPrice'=>$getOriginalPrice,'productdiscountPrice'=>$getDiscountPrice,'quotation'=>$getProductListData->houdinv_products_main_quotation,'stock'=>'yes');
                    }
                }
                
            }
            
            return array('message'=>'Success','data'=>$setProductArray);
        }
        else
        {
            return array('message'=>'No data');
        }   
    }
    public function fetchShopName($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getShopName = $getDynamicDB->select('houdinv_shop_title')->from('houdinv_shop_detail')->where('houdinv_shop_id',1)->get()->result();
        if(count($getShopName) > 0)
        {
            return array('message'=>'success','data'=>$getShopName[0]->houdinv_shop_title);
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function fetchSliderData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);
        $setSliderData = array();
        $getSliderData = $getDynamicDB->select('*')->from('houdinv_custom_slider')->get()->result();
        if(count($getSliderData) > 0)
        {
            foreach($getSliderData as $getSliderDataList)
            {
                $setImageData = $this->session->userdata('vendorURL')."upload/Slider/".$getSliderDataList->houdinv_custom_slider_image;
                $setSliderData[] = array('image'=>$setImageData);
            }
            if(count($setSliderData) > 0)
            {
                return array('message'=>'Success','data'=>$setSliderData);
            }
            else
            {
                return array('message'=>'No data');    
            }
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function fetchBaseCategory($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);
        
        $getBaseCategory = $getDynamicDB->select('*')->from('houdinv_sub_categories_one')->where('houdinv_sub_category_one_main_id',$data['categoryId'])->get()->result();
        if(count($getBaseCategory) > 0)
        {   
            foreach($getBaseCategory as $getBaseCategoryList)
            {
                $setBaseCategory[] = array('categoryId'=>$getBaseCategoryList->houdinv_sub_category_one_id,'categoryName'=>$getBaseCategoryList->houdinv_sub_category_one_name);
            }
            if(count($setBaseCategory) > 0)
            {
                return array('message'=>'Success','data'=>$setBaseCategory);
            }
            else
            {
                return array('message'=>'No data');    
            }
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function fetchFinalSubCategory($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);
        
        $getBaseCategory = $getDynamicDB->select('*')->from('houdinv_sub_categories_two')->where('houdinv_sub_category_two_sub1_id',$data['subCategoryId'])->where('houdinv_sub_category_two_main_id',$data['categoryId'])->get()->result();
        if(count($getBaseCategory) > 0)
        {   
            foreach($getBaseCategory as $getBaseCategoryList)
            {
                $setBaseCategory[] = array('subCategoryId'=>$getBaseCategoryList->houdinv_sub_category_two_id,'subCategoryName'=>$getBaseCategoryList->houdinv_sub_category_two_name);
            }
            if(count($setBaseCategory) > 0)
            {
                return array('message'=>'Success','data'=>$setBaseCategory);
            }
            else
            {
                return array('message'=>'No data');    
            }
        }
        else
        {
            return array('message'=>'No data');
        }
    }
 } 
?>