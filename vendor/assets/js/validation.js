// Number validation
var setBaseData = "http://3.22.189.222/houdin-e/vendor/";
$(document).ready(function() {
    $(".number_validation").keydown(function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
    (e.keyCode >= 35 && e.keyCode <= 40)) {
    return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
    }
    });
    // Name validation
    $(document).on('keydown', '.name_validation', function(e) {
    if (e.which === 32 &&  e.target.selectionStart === 0) {return false;}  });
    });
    //Email Validation
    jQuery(document).ready(function(){
    function ValidateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
    };
    jQuery(".email_validation").blur(function () {
    if (!ValidateEmail(jQuery(this).val())) {
    jQuery(this).val("");
    }
    else {
    return  true;
    }
    });
    });
    // DatePicker Value
    $(document).ready(function(){
        $('.date_picker').focus(function(){
            $(this).daterangepicker({                    
            singleDatePicker: true,
            showDropdowns: false,    
            locale: { 
                format: 'YYYY-MM-DD',
            }
            });      
        });   
    });
    // customer data
    $(document).on('click','.deleteCustomerBtn',function(){
        $('.deleteCustomerId').val($(this).attr('data-id'));
        $('#deleteCustomerModal').modal('show');
    });
    $(document).on('click','.addCustomerTagBtn',function(){
        $('.customerTagId').val($(this).attr('data-id'));
        $('#addCustomerTag').modal('show');
    });
    $(document).on('click','.changeCustomerStatusBtn',function(){
        $('.changeCustomerStatusId').val($(this).attr('data-id'));
        $('.chooseStatus').val($(this).attr('data-status'));
        $('#changeCustomerStatusModal').modal('show');
    });
    
    $(document).on('click','.changeCustomerPrivilageBtn',function(){
        $('.changeCustomerPrivilageId').val($(this).attr('data-id'));
        $('.choosePrivilage').val($(this).attr('data-privilage'));
        $('.setmaxValue').attr('data-max',$(this).attr('data-max'));
        $('#changeCustomerPrivilageModal').modal('show');
    });
    $(document).on('keyup','.setmaxValue',function(){
        if($(this).attr('data-max'))
        {
            if(parseInt($(this).attr('data-max')) < parseInt($(this).val()))
            {
                $(this).val($(this).attr('data-max'));
            }
        }
    })
    $(document).on('click','.changeCustomerPendingAmountBtn',function(){
        $('.changeCustomerPendingId').val($(this).attr('data-id'));
        $('.choosePendingAmount').val($(this).attr('data-pending'));
        $('#changeCustomerPendingModal').modal('show');
    });

     $(document).on('click','.notify_model',function(){
        $('#field-4').val($(this).attr('data-email'));
          $('#field-5').val($(this).attr('data-name'));
     
  
    });   
    $(document).on('click','.sendCustomerEmailBtn',function(){
        $('.customerEmail').val($(this).attr('data-email'));
        $('#sendEmailModal').modal('show');
    });
    $(document).on('click','.masterCheck',function(){
        if($(this).prop('checked') == true)
        {
            $('.showdataOnCheck').show();
            $('.showdataOnCheckEmail').show();
            $('.childCheck').prop('checked',true);
            // set id data
            var setIdData = "";
            var setEmailData = "";
            $('.childCheck').each(function(){
                if(setIdData)
                {
                    setIdData = setIdData+","+$(this).attr('data-id');
                }
                else
                {
                    setIdData = $(this).attr('data-id');
                }
                // email data
                if(setEmailData)
                {
                    setEmailData = setEmailData+","+$(this).attr('data-email');
                }
                else
                {
                    setEmailData = $(this).attr('data-email');
                }
            });
            $('.showdataOnCheckEmail').attr('data-set',setEmailData);
            $('.showdataOnCheck').attr('data-set',setIdData);
        }
        else
        {
            $('.showdataOnCheck').hide();
            $('.showdataOnCheckEmail').hide();
            $('.childCheck').prop('checked',false);
            $('.showdataOnCheck').attr('data-set','');
            $('.showdataOnCheckEmail').attr('data-set','');
        }
    });
    $(document).on('click','.childCheck',function(){
        var setData = 0;
        var checkData = 0;
       $('.childCheck').each(function(){
           if($(this).prop('checked') == true)
           {
                checkData++;
           }
           else
           {
            setData++;
           }
       });
       if(setData == 0)
       {
           $('.masterCheck').prop('checked',true);
       } 
       else
       {
        $('.masterCheck').prop('checked',false);
       }
       if(checkData != 0)
       {
        var setIdData = "";
        var setEmailData = "";
        $('.childCheck').each(function(){
            if($(this).prop('checked') == true)
            {
                if(setIdData)
                {
                    setIdData = setIdData+","+$(this).attr('data-id');
                }
                else
                {
                    setIdData = $(this).attr('data-id');
                }
                // email data
                if(setEmailData)
                {
                    setEmailData = setEmailData+","+$(this).attr('data-email');
                }
                else
                {
                    setEmailData = $(this).attr('data-email');
                }
            }
        });
        $('.showdataOnCheck').attr('data-set',setIdData);
        $('.showdataOnCheckEmail').attr('data-set',setEmailData);
        $('.showdataOnCheck').show();
        $('.showdataOnCheckEmail').show();
       }
       else
       {
        $('.showdataOnCheck').attr('data-set','');
        $('.showdataOnCheckEmail').attr('data-set','');
        $('.showdataOnCheck').hide();
        $('.showdataOnCheckEmail').hide();
       }
    });
    $(document).on('click','.sendEmailMultipleCustomer',function(){
        $('.customerEmail').val($(this).attr('data-set'));
        $('#sendEmailModal').modal('show');
    });
    $(document).on('click','.addTagMultipleCustomer',function(){
        $('.customerTagId').val($(this).attr('data-set'));
        $('#addCustomerTag').modal('show');
    });
    $(document).on('click','.deleteMultipleCustomer',function(){
        $('.deleteCustomerId').val($(this).attr('data-set'));
        $('#deleteCustomerModal').modal('show');
    });
    $(document).on('click','.deleteCustomerGroupBtn',function(){
        $('.deleteGroupId').val($(this).attr('data-id'));
        $('#deleteCustomerGroupModal').modal('show');
    });
    $(document).on('click','.editCustomerGroupData',function(){
        $('.editGroupId').val($(this).attr('data-id'));
        $('.editCustomerGroupName').val($(this).attr('data-name'));
        $('.editCustomerGroupStatus').val($(this).attr('data-status'));
        $('#editCustomerGroupModal').modal('show'); 
    });
    $(document).on('click','.deleteMultipleroupCustomer',function(){
        $('.deleteCustomerGroupId').val($(this).attr('data-id'));
        $('#deleteCustomerFromGroup').modal('show');
    })
    $(document).on('click','.singleDeleteBtnData',function(){
        $('.deleteCustomerGroupId').val($(this).attr('data-id'));
        $('#deleteCustomerFromGroup').modal('show');
    });
    $(document).on('click','.sendPaymentReminderEmailBtn',function(){
        $('.paymentReminderEmail').val($(this).attr('data-email'));
        $('.paymentReminderAmount').val($(this).attr('data-payment'));
        $('#sendPaymentReminder').modal('show');
    });
    // setting module
    $(document).on("click",'.checkData',function(){
        if($(this).prop('checked') == true)
        {
            $(this).parents('.parentClass').find('.setTextData').attr('readonly',false);
        }
        else
        {
            $(this).parents('.parentClass').find('.setTextData').val('');
            $(this).parents('parentClass').find('.setTextData').attr('readonly',true);
        }
    })
    // add supplier module
    $(document).on('click','.selectProductData',function(){
        var getProductName = $(this).text();
        var getProductId = $(this).attr('data-id');
        var setData = 0;
        $('.getProiductIdData').each(function(){
            if($(this).val() == getProductId)
            {
                setData++;
            }
        });
        if(setData == 0)
        {
            $('.setProductIdData').append("<input type='hidden' value='"+getProductId+"' class='getProiductIdData' name='supplierProduct[]'/>");
            $('.setProductTag').append('<div class="tag setParentData label label-info alert alert-dismissable m-r-5 pointer"><span class="tag_text">'+getProductName+'</span><span data-id="'+getProductId+'" class="removeProduct" style="cursor:pointer;" type="button" class="" data-dismiss="alert" aria-hidden="true">×</span></div>');
            $('.searh-suggestionboxlistboxdata').hide();
            $('.searh-suggestionboxlistboxdata').html('');  
            $('.searh-suggestionboxinput').val(''); 
        }
        else
        {
            $('.searh-suggestionboxlistboxdata').hide();
            $('.searh-suggestionboxlistboxdata').html('');  
            $('.searh-suggestionboxinput').val(''); 
            alert('This product is already tag with supplier');
        }
    });
    $(document).on('click','.removeProduct',function(){
        var getProductId = $(this).attr('data-id');
        $('.getProiductIdData').each(function(){
            if($(this).val() == getProductId)
            {
                $(this).remove();
            }
        })
    });
    $(document).on('click','.deleteSupplierBtn',function(){
        $('.deleteSupplierId').val($(this).attr('data-id'));
        $('#deletSupplierModal').modal('show');
    });
    $(document).on('click','.updateSupplierStatus',function(){
        $('.changeStatusSupplierId').val($(this).attr('data-id'));
        $('.supplierStatus').val($(this).attr('data-status'));
        $('#statusModalData').modal('show');
    });
    $(document).on('click','.sendEmailSupplierBtn',function(){
        $('.supplierEmailData').val($(this).attr('data-email'));
        $('#sendEmailModal').modal('show');
    });
    $(document).on('click','.masterSupplierCheck',function(){
        var setIdData = "";
        if($(this).prop('checked') == true)
        {
            $('.childSupplierCheck').each(function(){
                if(setIdData)
                {
                    setIdData = setIdData+","+$(this).attr('data-id');
                }
                else
                {
                    setIdData = $(this).attr('data-id');
                }
            });
            $('.setSupplierMultiBtn').attr('data-id','');
            $('.setSupplierMultiBtn').attr('data-id',setIdData);
            $('.setSupplierMultiBtn').show();
            $('.childSupplierCheck').prop('checked',true);
        }
        else
        {
            $('.setSupplierMultiBtn').attr('data-id','');
            $('.setSupplierMultiBtn').hide();
            $('.childSupplierCheck').prop('checked',false);
        }
    });
    $(document).on('click','.childSupplierCheck',function(){
        var checkData = 0;
        var setData = 0;
        var setIdData = "";
        $('.childSupplierCheck').each(function(){
            if($(this).prop('checked') == false)
            {
                checkData++;
            }
            else if($(this).prop('checked') == true)
            {
                if(setIdData)
                {
                    setIdData = setIdData+","+$(this).attr('data-id');
                }
                else
                {
                    setIdData = $(this).attr('data-id');
                }
                setData++;
            }
        });
        $('.setSupplierMultiBtn').attr('data-id','');
        $('.setSupplierMultiBtn').attr('data-id',setIdData);
        if(checkData == 0)
        {
            $('.masterSupplierCheck').prop('checked',true);
        }
        else
        {
            $('.masterSupplierCheck').prop('checked',false);
        }
        if(setData == 0)
        {
            $('.setSupplierMultiBtn').hide();
        }
        else
        {
            $('.setSupplierMultiBtn').show();
        }
    });
    $(document).on('click','.chnageMultipleStatusData',function(){
        $('.changeStatusMultipleSupplierId').val($(this).attr('data-id'));
       $('#changeMultipleststatus').modal('show'); 
    });
    $(document).on('click','.deleteMultipleSupplier',function(){
        $('.deleteMultipleSupplierId').val($(this).attr('data-id'));
        $('#deletMultipleSupplierModal').modal('show'); 
    });
    // Coupons module
    $(document).on('click','.genearatePromoCodeCoupons',function(){
        if($(this).prop('checked') == true)
        {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
            var string_length = 6;
            var randomstring = '';
            for (var i=0; i<string_length; i++) {
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum,rnum+1);
            }
            $('.setPromocodeCoupons').val(randomstring);
        }
        else
        {
            $('.setPromocodeCoupons').val('');
        }
    });
    $(document).on('click','.deleteCopounBtn',function(){
        $('.deleteCouponId').val($(this).attr('data-id'));
        $('#deleteCouponsModal').modal('show');
    });
    // GIft Voucher
    $(document).on('click','.deleteMultipleVoucher',function(){
        $('.deleteVoucherId').val($(this).attr('data-id'));
        $('#deleteGiftVoucher').modal('show');
    });
    $(document).on('click','.deleteSingleGiftVoucher',function(){
        $('.deleteVoucherId').val($(this).attr('data-id'));
        $('#deleteGiftVoucher').modal('show');
    })
    $(document).on('click','.changeStatusMultippleVoucher',function(){
        $('.updateStatusId').val($(this).attr('data-id'));
        $('#updateGiftVouchermodal').modal('show');
    });
    // purchase order
    $(document).on('click','.masterSupplierProductCheck',function(){
        if($(this).prop('checked') == true)
        {
            $('.setProductQuantity').prop('readonly',false);
            $('.childSupplierProductCheck').prop('checked',true);
            // set data value
            $('.childSupplierProductCheck').each(function(){
                $(this).parents('.parentRowClass').find('.setSelectedProductId').val($(this).attr('data-id'));
                $(this).parents('.parentRowClass').find('.setSelectedProductVariantId').val($(this).attr('data-variant'));
            
                $(this).parents('.parentRowClass').find('.setProductQuantity').prop('readonly',false);
            });
        }
        else
        {
            $('.setProductQuantity').prop('readonly',true);
            $('.setProductQuantity').val('');
            $('.childSupplierProductCheck').prop('checked',false);
            // set data value
            $('.childSupplierProductCheck').each(function(){
                $(this).parents('.parentRowClass').find('.setSelectedProductId').val('');
                  $(this).parents('.parentRowClass').find('.setSelectedProductId').val('');
                $(this).parents('.parentRowClass').find('.setProductQuantity').val('').prop('readonly',true);
            });
        }
    });
    $(document).on('click','.childSupplierProductCheck',function(){
        var checkData = 0;
        if($(this).prop('checked') == true)
        {
            $(this).parents('.parentRowClass').find('.setSelectedProductId').val($(this).attr('data-id'));
            
             $(this).parents('.parentRowClass').find('.setSelectedProductVariantId').val($(this).attr('data-variant'));
            
            $(this).parents('.parentRowClass').find('.setProductQuantity').prop('readonly',false);
        }
        else
        {
            $(this).parents('.parentRowClass').find('.setSelectedProductId').val('');
              $(this).parents('.parentRowClass').find('.setSelectedProductVariantId').val('');
            $(this).parents('.parentRowClass').find('.setProductQuantity').val('').prop('readonly',true);
        }
        $('.childSupplierProductCheck').each(function(){
            if(!$(this).prop('checked') == true)
            {
                checkData++;
            }
        });
        if(checkData == 0)
        {
            $('.masterSupplierProductCheck').prop('checked',true);
        }
        else
        {
            $('.masterSupplierProductCheck').prop('checked',false);
        }
    });
    $(document).on('click','.deleteMultiplePurchase',function(){
        $('.deletePurchaseId').val($(this).attr('data-id'));
        $('#deletePurchaseModal').modal('show');
    });
    $(document).on('click','.deletePurcahseBtn',function(){
        $('.deletePurchaseId').val($(this).attr('data-id'));
        $('#deletePurchaseModal').modal('show');
    });
    $(document).on('change','.changeCreditPeriod',function(){
        if($(this).val() == 'day')
        {
            $('.showPeriodTime').show();
            $('.setPeriodLabel').text("").text('Period Time(Day)');
        }
        else if($(this).val() == 'week')
        {
            $('.showPeriodTime').show();
            $('.setPeriodLabel').text("").text('Period Time(Week)');
        }
        else if($(this).val() == 'month')
        {
            $('.showPeriodTime').show();
            $('.setPeriodLabel').text("").text('Period Time(Month)');
        }
        else
        {
            $('.showPeriodTime').hide();
        }
    })
    // Store navigation 
    $(document).on('click','.updateCustomNavigationData',function(){
        var setCustomNavigation = "";
        var setData = 0;
        $('.customNavigation').each(function(){
            if($(this).prop('checked') == true)
            {
                setData++;
                if(setCustomNavigation)
                {
                    setCustomNavigation = setCustomNavigation+","+$(this).attr('data-id');
                }
                else
                {
                    setCustomNavigation = $(this).attr('data-id');
                }
            }
        }); // End each
        if(setData > 0)
        {
            $('.showspin').show();
            $('.updateCustomNavigationData').attr('disabled',true);
            jQuery.ajax({
                type: "POST",
                url: setBaseData+"Storesetting/updateStaticNavigation",
                data: {"navData": setCustomNavigation},
                success: function(data) {
                    // console.log(data);
                  if($.trim(data) == 'nonav')
                  {
                    $('.showspin').hide();
                    $('.updateCustomNavigationData').attr('disabled',false);
                    alert('Please choose atleast one page');
                  }
                  else if($.trim(data) == 'no')
                  {
                    $('.showspin').hide();
                    $('.updateCustomNavigationData').attr('disabled',false);
                    alert('Something went wrong. Please try again');  
                  }
                  else
                  {
                    $('.showspin').hide();
                    $('.updateCustomNavigationData').attr('disabled',false);
                    $('.appendDataValue').html('');
                    $('.appendDataValue').html(data);
                  }
                }
                }); 
        }
        else
        {
            alert('Please choose atleast one page');
        }
    });
    $(document).on('click','.sethomeData',function(){
        var datavalue = $(this).attr('data-id');
        if($(this).prop('checked') == true)
        {
            $('.commonheadingclass').each(function(){
                if($(this).attr('data-id') == datavalue)
                {
                    $(this).prop('disabled',false);
                }
            })
        }
        else
        {
            $('.commonheadingclass').each(function(){
                if($(this).attr('data-id') == datavalue)
                {
                    $(this).val('');
                    $(this).prop('disabled',true);
                }
            })
        }
    });	
    $(document).on('click','.deleteSliderImageBtn',function(){
        $('.deleteSliderImageId').val($(this).attr('data-id'));
        $('#deleteSliderImagesmodal').modal('show');
    });
    $(document).on('click','.removeMenu',function(){
        $('.deleteNavigationId').val($(this).attr('data-id'));
        $('#deleteNavigationModal').modal('show');
    });
    $(document).on('click','.updatedynamicCategory',function(){
        var setData = 0;
        var setcategoryId = "";
        var setcategoryName = "";
        $('.storeCategoryNav').each(function(){
            if($(this).prop('checked') == true)
            {
                if(setcategoryId)
                {
                    setcategoryName = setcategoryName+","+$(this).attr('data-name');
                    setcategoryId = setcategoryId+","+$(this).attr('data-id');
                }
                else
                {
                    setcategoryName = $(this).attr('data-name');
                    setcategoryId = $(this).attr('data-id');
                }
                setData++;
            }
        });
        if(setData == 0)
        {
            alert('Please choose atleast one category')
        }
        else
        {
            $('.showspinCat').show();
            $('.updatedynamicCategory').attr('disabled',true);
            jQuery.ajax({
                type: "POST",
                url: setBaseData+"Storesetting/updateCategoryNavigation",
                data: {"navDataId": setcategoryId,"navDataName": setcategoryName},
                success: function(data) {
                    if($.trim(data) == 'nonav')
                    {
                        $('.showspinCat').hide();
                        $('.updatedynamicCategory').attr('disabled',false);
                      alert('Please choose atleast one page');
                    }
                    else if($.trim(data) == 'no')
                    {
                        $('.showspinCat').hide();
                        $('.updatedynamicCategory').attr('disabled',false);
                        alert('Something went wrong. Please try again');  
                    }
                    else
                    {
                        $('.showspinCat').hide();
                        $('.updatedynamicCategory').attr('disabled',false);
                      $('.appendDataValue').html('');
                      $('.appendDataValue').html(data);
                    }
                }
                }); 
        }
    });
    $(document).on('submit','#addCustomLinkFormData',function(e){
        var check_required_field='';
        $(this).find(".required_validation_for_add_custom_links").each(function(){
            var val22 = $(this).val();
            if (!val22){
                check_required_field =$(this).size();
                $(this).css("border-color","#ccc");
                $(this).css("border-color","red");
            }
            $(this).on('keypress change',function(){
                $(this).css("border-color","#ccc");
            });
        });
        if(check_required_field)
        {
            return false;
        }
        else {
            e.preventDefault();
            $('.setCustomLinkBtn').attr('disabled',true);
            $('.showspinCustomLinks').show();
            var formData = $('#addCustomLinkFormData').serialize();
            jQuery.ajax({
                type: "POST",
                url: setBaseData+"Storesetting/updateCustomLinks",
                data: formData,
                success: function(data) {
                    console.log(data);
                    if($.trim(data) == 'all')
                    {
                        $('.setCustomLinkBtn').attr('disabled',false);
                        $('.showspinCustomLinks').hide();
                        alert('All fields are mandatory');
                    }
                    else if($.trim(data) == 'no')
                    {
                        $('.setCustomLinkBtn').attr('disabled',false);
                        $('.showspinCustomLinks').hide();
                        alert('Something went wrong. Please try again');  
                    }
                    else
                    {
                        $('.setCustomLinkBtn').attr('disabled',false);
                        $('.showspinCustomLinks').hide();
                        $('.appendDataValue').html('');
                        $('.appendDataValue').html(data);
                    }
                }
                }); 
        }
    });
    $(document).on('click','.removeCustomLinkMenu',function(){
        $('.deleteCustomMenuNavigationId').val($(this).attr('data-id'));
        $('#deleteCustomNavigationModal').modal('show');
    })
    // Stock transfer module
    $(document).on('click','.stockTransferBtn',function(){
        $('#stockTransferModal').modal('show');
        $('.setProductId').attr('data-id',$(this).attr('data-id'));
    });
    $(document).on('change','.setProductId',function(){
        var getWarehouseId = $(this).val();
        var productId = $(this).attr('data-id');
        if(getWarehouseId && productId)
        {
            $('.transferData').prop('disabled',true);
            $('.setProductId').prop('disabled',true);
            jQuery.ajax({
                type: "POST",
                url: setBaseData+"Inventory/fetchStockTransferProduct",
                data: {"productId": productId,"getWarehouseId":getWarehouseId},
                success: function(data) {  
                    $('.setProductId').prop('disabled',false);
                    if($.trim(data) == 'all')
                    {
                        alert('All fields are mandatory');
                    }
                    else if($.trim(data) == 'no')
                    {
                        var setDataValue = "<tr><td colspan='3'>Stock is not available for selected warehouse</td></tr>";
                        $('.setStockData').html('');
                        $('.setStockData').append(setDataValue);
                    }
                    else
                    {
                        $('.transferData').prop('disabled',false);
                        $('.setWarehouseTo').prop('disabled',false);
                        $('.setStockData').html('');
                        $('.setStockData').append(data);
                    }
                    setNumberValidation(); 
                }
            }); 
        }
        else
        {
            alert('Something went wrong. Please try again')
        }
    });
    function setNumberValidation()
    {
        $(".number_validation").keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
            }
            });
    }
    $(document).on('click','.masterStockCheck',function(){
        if($(this).prop('checked') == true)
        {
            $('.childStockCheck').prop('checked',true);
            $('.setQunatityData').prop('readonly',false);
            $('.childStockCheck').each(function(){
                var productId = $(this).attr('data-product');
                var inventoryId = $(this).attr('data-variant');
                $(this).parents('.mainParentRow').find('.setProduct').val(productId);
                $(this).parents('.mainParentRow').find('.setVariant').val(inventoryId);
            })
        }
        else
        {
            $('.setProduct').val('');
            $('.setVariant').val('');
            $('.setQunatityData').val('');
            $('.setQunatityData').prop('readonly',true);
            $('.childStockCheck').prop('checked',false);
        }
    });
    $(document).on('click','.childStockCheck',function(){
        var setData = 0;
        if($(this).prop('checked') == true)
        {
            var productId = $(this).attr('data-product');
            var inventoryId = $(this).attr('data-variant');
            $(this).parents('.mainParentRow').find('.setProduct').val(productId);
            $(this).parents('.mainParentRow').find('.setVariant').val(inventoryId);
            $(this).parents('.mainParentRow').find('.setQunatityData').prop('readonly',false);
        }
        else
        {
            $(this).parents('.mainParentRow').find('.setProduct').val('');
            $(this).parents('.mainParentRow').find('.setVariant').val('');
            $(this).parents('.mainParentRow').find('.setQunatityData').prop('readonly',true).val('');
        }
        $('.childStockCheck').each(function(){
            if($(this).prop('checked') == true)
            {

            }
            else
            {
                setData++;
            }
        });
        if(setData == 0)
        {
            $('.masterStockCheck').prop('checked',true);
        }
        else
        {
            $('.masterStockCheck').prop('checked',false);
        }
    });
    $(document).on('keyup','.setQunatityData',function(){
        if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
        {
            $(this).val($(this).attr('data-max'));
        }
    });
    // billed confirm order
    $(document).on('keyup','.getEnteredCount',function(){
        if(parseInt($(this).attr('data-max')) < parseInt($(this).val()))
        {
            $(this).val($(this).attr('data-max'));
        }
    });

    //  all orders
    $(document).on('click','.updateOrderStatus',function(){
        $('.updateOrderStatusId').val($(this).attr('data-id'));
        $('.updateOrderStatusDataValue').val($(this).attr('data-status'));
        $('#UpdateOrderStatus').modal('show');
    });
    $(document).on('click','.DeleteOrderBtn',function(){
        $('.deleteOrderId').val($(this).attr('data-id'));
        $('#deleteOrderModal').modal('show');
    })
    $(document).on('click','.tagDeliveryBoydata',function(){
        $('.deliveryOrderId').val($(this).attr('data-id'));
        $('#deliveryTagModal').modal('show');
    });
    $(document).on('change','.setDeliveryType',function(){
        $(this).prop('disabled',true);
        $('.setErrorMessage').html('');
        $('.setAllOrdersdata').html('');
        var getTagValue = $(this).val();
        jQuery.ajax({
    type: "POST",
    url: setBaseData+"order/getDeliveryBoysDetails",
    data: {"type": getTagValue},
    success: function(data) {
        if($.trim(data) == 'no')
        {
            $('.setErrorMessage').html('');
            $('.setErrorMessage').html('<div class="alert alert-danger">Something went wrong. Please try again</div>');
            $('.setDeliveryType').prop('disabled',false);
        }
        else if($.trim(data) == 'no staff')
        {
            $('.setErrorMessage').html('');
            $('.setErrorMessage').html('<div class="alert alert-danger">Please add staff member</div>');
            $('.setDeliveryType').prop('disabled',false);
        }
        else
        {
            $('.setErrorMessage').html('');
            $('.setDeliveryType').prop('disabled',false);
            $('.setAllOrdersdata').html('');
            $('.setAllOrdersdata').append(data);
        }
    }
    }); 
    });
    $(document).on('change','.setAllOrdersdata',function(){
        var getUserEmail = $(this).children('option:selected').attr('data-email');
        $('.deliveryboyEmail').val(getUserEmail);
    });
    // update payment status
    $(document).on('click','.updatePaymentData',function(){
        $('.setErrorMessageData').hide().text('');
        $('.updatePaymentData').prop('disabled',true);
        var getOrderId = $(this).attr('data-id');
        jQuery.ajax({
            type: "POST",
            url: setBaseData+"order/getPaymentStatus",
            data: {"getOrderId": getOrderId},
            success: function(data) {
                var getData = $.parseJSON(data);
                if(getData.message == 'Success')
                {
                    console.log(getData);
                    var totalOrderAmount = parseFloat(getData.data[0].houdinv_orders_total_Amount);
                    var totalAmountPaid = parseFloat(getData.data[0].houdinv_orders_total_paid);
                    var totalRemaining = parseFloat(getData.data[0].houdinv_orders_total_remaining);
                    var totalUserPending = parseFloat(getData.data[0].houdinv_users_pending_amount);
                    var totalPriv = getData.data[0].houdinv_users_privilage;
                    if(totalAmountPaid == 0 || totalAmountPaid == "")
                    {
                        totalRemaining = totalOrderAmount;
                    }
                    else
                    {
                        totalRemaining = totalRemaining;
                    }
                    if(totalPriv == 0 || totalPriv == "")
                    {
                        $('.AmountPriv').val(0);
                        $('.AmountPaid').attr('data-priv',0);
                    }
                    else
                    {
                        $('.AmountPriv').val(totalPriv);
                        $('.AmountPaid').attr('data-priv',totalPriv);
                    }
                    $('.totalOrderAmount').val(totalOrderAmount);
                    $('.totalPaidAmount').val(totalAmountPaid);
                    $('.totalRemainingAmount').val(totalRemaining);
                    $('.customerId').val(getData.data[0].houdinv_user_id)
                    $('.totalPendingAmount').val(totalUserPending);
                    $('.paymentOrderId').val(getData.orderId);
                    $('.updatePaymentData').prop('disabled',false);
                    $('#updatePaymentStatusModal').modal('show');
                }
                else
                {
                    $('.updatePaymentData').prop('disabled',false);
                    $('.setErrorMessageData').show().text(getData.message);
                    $('#updatePaymentStatusModal').modal('hide');
                }
            }
            });
    });
    $(document).on('keyup','.AmountPaid',function(){
        var getRemainingAmount = parseFloat($('.totalRemainingAmount').val());
        if( parseFloat($(this).val()) > getRemainingAmount)
        {
            $(this).val(getRemainingAmount);
        }
        var newReaminingAmount = getRemainingAmount-parseFloat($(this).val());
        $('.newRemainingAmount').val(newReaminingAmount);
    });
    $(document).on('submit','#updatePaymentStatusForm',function(){
        $('.setFooterErrorMessage').hide().text('');
        var check_required_field='';
        $(this).find(".required_validation_for_all_paymentStatus").each(function(){
            var val22 = $(this).val();
            if (!val22 || val22 < 0){
                check_required_field =$(this).size();
                $(this).css("border-color","#ccc");
                $(this).css("border-color","red");
            }
            $(this).on('keypress change',function(){
                $(this).css("border-color","#ccc");
            });
        });
        if(check_required_field)
        {
            return false;
        }
        else {
            var getPrivAmount = parseFloat($('.AmountPriv').val());
            var paidAmount = parseFloat($('.AmountPaid').val());
            var totalRemainignAmount = parseFloat($('.totalRemainingAmount').val());
            if(getPrivAmount <= 0)
            {
                if(paidAmount == totalRemainignAmount)
                {
                    return true;
                }   
                else
                {
                    $('.setFooterErrorMessage').show().text('').text('Customer does not have any privilege amount. Please ask him/her for full payment');
                    return false;
                }
            }
            else
            {
                var privDataValue = totalRemainignAmount-paidAmount;
                if(privDataValue > getPrivAmount)
                {
                    $('.setFooterErrorMessage').show().text('').text('The remainig amount is crossed the privilege amount');
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    });
    $(document).on('click','.deleteQuotationBtn',function(){
        $('.deleteQuotationId').val($(this).attr('data-id'));
        $('#deleteQuotationModal').modal('show');
    });
    $(document).on('click','.sendEmailData',function(){
        $('.emailQuotationData').val($(this).attr('data-email'));
        $("#sendQuotationemailModal").modal('show');
    });
    // add payee for expence
    $(document).on('change','.checkPayeeData',function(){
        if($(this).val() == 'customer')
        {
          $('.customerFormData').show();
          $('.staffMemberData').hide();
          $('.supplierMemberData').hide();
        }
        else if($(this).val() == 'staff')
        {
          $('.staffMemberData').show();
          $('.customerFormData').hide();
          $('.supplierMemberData').hide();
        }
        else if($(this).val() == 'supplier')
        {
          $('.supplierMemberData').show();
          $('.staffMemberData').hide();
          $('.customerFormData').hide();
        }
        else
        {
          $('.supplierMemberData').hide();
          $('.staffMemberData').hide();
          $('.customerFormData').hide();
        }
    });
    $(document).on('click','.showJournalEntry',function(){
        $('#add_journal_entry').modal('show');
    });

    