<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Vendorloginmodel');
        $this->load->library('form_validation');
        $this->from = fromemail();
        $getSubDomainData = checkVenodrUrl();
        if($getSubDomainData['message'] == 'yes')
        {
            redirect($getSubDomainData['url'], 'refresh');
        }
    }
    public function index()
    {
        if($this->session->userdata('vendorAuth') != "")
        {
            $this->load->helper('url');
            redirect(base_url()."dashboard", 'refresh');
        }
        // login data
        // echo $this->input->post('vendorLogin'); exit();
        if($this->input->post('vendorLogin'))
        {
            $this->form_validation->set_rules('email','text','required|valid_email');
            $this->form_validation->set_rules('password','text','required');
            if($this->form_validation->run() == true)
            {
                $setArrayData = array('email'=>$this->input->post('email'),'pass'=>$this->input->post('password'));
                $getVendorLoginStatus = $this->Vendorloginmodel->loginVendor($setArrayData);
                if($getVendorLoginStatus['message'] == 'email')
                {
                    $this->session->set_flashdata('error','Please enter correct email address');
                    $this->load->helper('url');
                    redirect(base_url(), 'refresh');
                }
                else if($getVendorLoginStatus['message'] == 'pass')
                {
                    $this->session->set_flashdata('error','Please enter correct password');
                    $this->load->helper('url');
                    redirect(base_url(), 'refresh');
                }
                else if($getVendorLoginStatus['message'] == 'package')
                {
                    setcookie('tempUserPackageId', $getVendorLoginStatus['userID'], time() + (18000), "/"); // 86400 = 1 day
                    $this->load->helper('url');
                    $getUrl = SetDynamicUrl();
                    redirect(''.$getUrl.'/home/plans', 'refresh');
                }
                else if($getVendorLoginStatus['message'] == 'yes')
                {
                     
                    $this->session->set_userdata('vendorAuth',$getVendorLoginStatus['token']);
                    $this->session->set_userdata('vendorRole',$getVendorLoginStatus['role']);
                    $this->session->set_userdata('vendorOutlet',$getVendorLoginStatus['outlet']);
                    $this->session->set_userdata('shop',$getVendorLoginStatus['shopname']);
                    $this->load->helper('warehouse');
                    $getWarehouse = checkwarehouse($getVendorLoginStatus['shopname']);
                    if($getVendorLoginStatus['outlet']>0){
                    $Setwarehouse = Setwarehouse($getVendorLoginStatus['outlet'],$getVendorLoginStatus['shopname']);
                         }
                    $this->load->helper('url');
                    redirect(base_url()."dashboard", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url(), 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url(), 'refresh');
            }
        }
        $this->load->view('login');
    }
    public function setLoginAuth()
    {
        if($this->session->userdata('vendorAuth') != "")
        {
            $this->load->helper('url');
            $this->session->set_flashdata('success','You are already logged in  this is not your account please Logout first then login');
            redirect(base_url()."dashboard", 'refresh');

        }
        if($_COOKIE['tempAuth'] != "")
        {
            $getAuthStatus = $this->Vendorloginmodel->checkLandinUserAuth($_COOKIE['tempAuth']);
            if($getAuthStatus['message'] == 'wrong')
            {
                unset($_COOKIE['tempAuth']);
                $this->session->set_flashdata('error','Unauthorized access');
                $this->load->helper('url');
                redirect(base_url(), 'refresh');
            }
            else if($getAuthStatus['message'] == 'package')
            {
                unset($_COOKIE['tempAuth']);
                $this->session->set_flashdata('error','Your package has expired. Please upgrade your package');
                $this->load->helper('url');
                redirect(base_url(), 'refresh');
            }
            else if($getAuthStatus['message'] == 'yes')
            {
                unset($_COOKIE['tempAuth']);
                $this->session->set_userdata('vendorAuth',$getAuthStatus['vendorAuth']);
                $this->session->set_userdata('vendorRole',0);
                $this->session->set_userdata('vendorOutlet',0);
                $this->session->set_userdata('shop',$getAuthStatus['dbData']);  
                $this->load->helper('warehouse');
                $getWarehouse = checkwarehouse();
                $this->load->helper('url');
                redirect(base_url()."Dashboard", 'refresh');
            }
            else
            {
                unset($_COOKIE['tempAuth']);
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url(), 'refresh');
            }
        }
        else
        {
            $this->load->helper('url');
            $getUrl = SetDynamicUrl();
            redirect($getUrl, 'refresh');
        }
    }
    public function logoutVendor()
    {
        $getauthData = $this->session->userdata('vendorAuth');
         $getauthDataRole = $this->session->userdata('vendorRole');
    
        if($getauthData != "")
        {
            $this->Vendorloginmodel->logoutVendorData($getauthData,$getauthDataRole);
            $this->session->unset_userdata('vendorAuth');
            $this->session->unset_userdata('vendorRole');
            $this->session->unset_userdata('vendorOutlet');
            $this->session->unset_userdata('shop');
            $this->session->set_flashdata('success','You are logout successfully');
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        else
        {
            $this->session->set_flashdata('success','You are logout successfully');
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
    }
    // forget Password  
    public function forgetCheckData()
    { 
        $getSmsEmail = smsemailcredentials();
        $this->form_validation->set_rules('checkEmail','text','required|valid_email');
        if($this->form_validation->run() == true)
        {
            $getForgetToken = $this->Vendorloginmodel->checkForgetEmail($this->input->post('checkEmail'));
            if($getForgetToken['message'] == 'yes')
            {
                // send email to admin user
                $url = 'https://api.sendgrid.com/';
                $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                $json_string = array(
                'to' => array(
                    $this->input->post('checkEmail')
                ),
                'category' => 'test_category'
                );
                $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                <img src="'.base_url().'assets/images/main-logo.png" style="max-width: 53%;width:100%"/></td></tr>
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Your Security Pin is:</strong></td></tr>
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$getForgetToken['token'].'</td></tr>
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                <table width="100%" ><tr >
                <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e 2018--All right reserverd </td></tr>
                <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
                </tr></table>
                </div>
                </div></td></tr></table>';
                
                
                $params = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'x-smtpapi' => json_encode($json_string),
                    'to'        => $this->input->post('checkEmail'),
                    'fromname'  => 'Houdin-e',
                    'subject'   => 'Recover Your Password',
                    'html'      => $htm,
                    'from'      => $this->from,
                );
                $request =  $url.'api/mail.send.json';
                $session = curl_init($request);
                curl_setopt ($session, CURLOPT_POST, true);
                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                curl_setopt($session, CURLOPT_HEADER, false);
                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($session);
                curl_close($session);
                $getResponse = json_decode($response);
                if($getResponse->message == 'success')
                {
                    $this->session->set_flashdata('success','Please check your email');
                    $this->session->set_userdata('tempCode',$getForgetToken['token']);
                    echo 'yes';
                }
                else
                {
                    echo 'no';
                }
            }
            else if($getForgetToken['message'] == 'email')
            {
                echo 'email';
            }
            else
            {
                echo 'no';
            }
        }
        else
        {
            echo 'all';
        }
    }
}

