<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Webservicecategory extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopcategorymodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function index()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getCategoryData = $this->Shopcategorymodel->fetchCategoryData($this->input->post('shopName'));
            print_r(json_encode($getCategoryData)); 
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray)); 
        }
    }
}
