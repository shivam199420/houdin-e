<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">Login / Register</h2>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- login area end -->
    <div class="login-area ptb-100">
        <div class="container">
            <div class="row">
            <?php 
            if($this->session->flashdata('error'))
            {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
            }
            if($this->session->flashdata('success'))
            {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
            }
            ?>
                <div class="col-md-6">
                    <div class="login-content">
                        <div class="login-title">
                            <h4>login</h4>
                            <p>Please login using account detail bellow.</p>
                        </div>
                        <div class="login-form">
                        <?php echo form_open(base_url( 'Register/checkUserAuth' ), array( 'id' => 'addShopLogin', 'method'=>'post' ));?>
                        <input type="email"  name="userEmail" class="required_validation_for_user name_validation email_validation" placeholder="Enter Email Address"/>
                        <input type="password" name="userPass" class="required_validation_for_user name_validation" placeholder="Enter Password"/>
                                <div class="button-remember">
                                    <div class="checkbox-remember">
                                      
                                        <a href="javascript:;" data-toggle="modal" data-target="#myModal">Forgot your Password?</a>
                                    </div>
                                    <button class="login-btn" type="submit">Login</button>
                                </div>
                           <?php echo form_close(); ?>
                        </div>
                        <!-- Modal -->
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="login-content">
                        <div class="login-title">
                            <h4>register</h4>
                            <p>Please sign up using account detail bellow.</p>
                        </div>
                        <div class="login-form">
                        <?php echo form_open(base_url( 'Register/addShopUsers' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
                        <input type="text" class="required_validation_for_user name_validation" name="userFirstName" placeholder="Enter Firstname" />
                        <input type="text" class="required_validation_for_user name_validation" name="userLastName" placeholder="Enter Lastname"/>
                        <input type="email" class="required_validation_for_user name_validation email_validation" name="userEmail" placeholder="Enter Emailaddress"/>
                        <input type="text" class="required_validation_for_user name_validation" name="userContact" placeholder="Enter Contact Number"/>
                        <input type="password" class="required_validation_for_user name_validation opass" name="userPass" placeholder="Enter password"/>
                        <input type="password" class="required_validation_for_user name_validation cpass" name="userRetypePass" placeholder="Conform password" />
                                 
                                <div class="button-remember">
                                    <button class="login-btn setDisableData" type="submit">register</button>
                                </div>
                                <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <?php echo form_open(base_url( 'Register/checkUserForgot' ), array( 'id' => 'addShopUserForgot', 'method'=>'post' )); ?>
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Forgot your password</h4>
                              </div>
                              <div class="modal-body">
                              <input placeholder="Please enter your Email address" name="emailForgot" class="required_validation_for_user name_validation email_validation" type="text" style="width:100%;">
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="login-btn" style="float: left">Submit</button>
                              </div>
                            </div>
                            <?php echo form_close(); ?>
                          </div>
                        </div>
    <?php 
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer') 
  ?>

  <script type="text/javascript">
        $(document).ready(function(){
                $(document).on('submit','#addShopUsers,#addShopLogin,#addShopUserForgot',function(){
                        var check_required_field=0;
                        $(this).find(".required_validation_for_user").each(function(){
                                var val22 = $(this).val();
                                if (!val22){
                                        check_required_field++;
                                        $(this).css("border-color","#ccc");
                                        $(this).css("border-color","red");
                                }
                                $(this).on('keypress change',function(){
                                        $(this).css("border-color","#ccc");
                                });
                        });
                        if(check_required_field != 0)
                        {
                                return false;
                        }
                        else {
                                return true;
                        }
                });
        });
                </script>