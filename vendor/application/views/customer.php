﻿        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">

        <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">Customer</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                 <li><a href="<?php echo base_url(); ?>customer/Customerlanding">Customer Management</a></li>
                  <li class="active">Customer</li>
                  </ol>
                  </div>
                  
            </div>
           <!--END Page-Title --> 
        <div class="row m-t-20">

        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
         <img src="<?php echo base_url(); ?>assets/images/allpageicon/Customer.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($totalCustomer) { echo $totalCustomer; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total customer</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
         <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Customer.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($totalActiveCustomer) { echo $totalActiveCustomer; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total active customer</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
         <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Customer.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($totalDeactivateCustomer) { echo $totalDeactivateCustomer; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total deactive customer</div>
        </div>
        </div>
        </div>
    
       
        <!-- show messages -->
        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
        <!-- End Here -->
        
        <div class="row">
        <div class="col-md-12">

        <div class="card-box table-responsive">
        <div class="btn-group pull-right m-t-10 m-b-20">      
        <a style="float: left;margin-right: 3px;" href="<?php echo base_url(); ?>Customer/add"><button type="button" class="btn btn-default" title="Add"><i class="fa fa-plus"></i></button></a> 
        <button type="button" class="btn btn-default m-r-5 showdataOnCheckEmail sendEmailMultipleCustomer" style="display:none" title="Send Email"><i class="fa fa-envelope"></i></button>
        <button type="button" class="btn btn-default m-r-5 showdataOnCheck addTagMultipleCustomer" style="display:none" title="Add tag"><i class="fa fa-tag"></i></button>
        <button type="button" class="btn btn-default m-r-5 showdataOnCheck deleteMultipleCustomer" style="display:none" title="Delete"><i class="fa fa-trash"></i></button>
        <a href="<?php echo base_url(); ?>Customer/customerDetailPdf" target="_blank" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
        <button type="button" data-toggle="modal" data-target="#uploded_customer_csv" class="btn btn-default m-r-5 " title="upload csv"><i class="fa fa-file"></i></button>
       
        </div>
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th><input class="masterCheck" type="checkbox"></th>
        <th>Name</th>
        <th>Contact</th>
        <th>Email</th>
        <th>Payment pending</th>
        <th>Status</th>
        <th>Action</th>

        </tr>

        </thead>
        <tbody>
        <?php 
        foreach($customerList as $customerListData)
        {
                //print_r($customerListData);
        ?>
        <tr>
        <td><input data-email="<?php echo $customerListData->houdinv_user_email ?>" data-id="<?php echo $customerListData->houdinv_user_id ?>" class="childCheck" type="checkbox"></td>
        <td><a href="<?php echo base_url(); ?>Customer/view/<?php echo $customerListData->houdinv_user_id ?>"><?php echo $customerListData->houdinv_user_name ?></a></td>
        <td><?php echo $customerListData->houdinv_user_contact ?></td>
        <td><?php echo $customerListData->houdinv_user_email ?></td>
        <td>Payment</td>
        <td>
        <?php 
        if($customerListData->houdinv_user_is_active == 'active')
        {
                $setText = 'Active';
                $setClass = 'success';
        }
        else if($customerListData->houdinv_user_is_active == 'deactive')
        {
                $setText = 'Deactive';
                $setClass = 'warning';
        }
        ?>
        <button class="btn btn-<?php echo $setClass ?> btn-xs"><?php echo $setText; ?></button>
        </td>
                <td>
                <!--<a href="<?php //echo base_url(); ?>Customer/edit/<?php //echo $customerListData->houdinv_user_id ?>"><button class="btn btn-success waves-effect waves-light" >Edit</button></a> -->
        <button class="btn btn-success waves-effect waves-light deleteCustomerBtn" data-id="<?php echo $customerListData->houdinv_user_id ?>">Delete</button>
        <button class="btn btn-success waves-effect waves-light addCustomerTagBtn" data-id="<?php echo $customerListData->houdinv_user_id ?>">Add Tag</button>
        <button class="btn btn-success waves-effect waves-light sendCustomerEmailBtn" data-email="<?php echo $customerListData->houdinv_user_email ?>">Send Email</button>

        <button class="btn btn-success waves-effect waves-light changeCustomerStatusBtn" data-status="<?php echo $customerListData->houdinv_user_is_active; ?>" data-id="<?php echo $customerListData->houdinv_user_id ?>">Change status</button>


        <?php 
        if(count($customerSetting) > 0)
        {
                

        if($customerSetting[0]->allow_pay_panding == 1)
        {
        ?>
        <button class="btn btn-success waves-effect waves-light changeCustomerPrivilageBtn" data-max="<?php echo $customerSetting[0]->pay_panding_amount ?>" data data-privilage="<?php echo $customerListData->houdinv_users_privilage; ?>" data-id="<?php echo $customerListData->houdinv_user_id ?>">Change Privilage</button>
        <?php } 
        }
        else
        {
        ?>
        <button class="btn btn-success waves-effect waves-light changeCustomerPrivilageBtn" data data-privilage="<?php echo $customerListData->houdinv_users_privilage; ?>" data-id="<?php echo $customerListData->houdinv_user_id ?>">Change Privilage</button>
        <?php }
        ?>
        <?php 
        if($customerListData->houdinv_users_pending_amount)
        {
        ?>
        <button class="btn btn-success waves-effect waves-light changeCustomerPendingAmountBtn" data-pending="<?php echo $customerListData->houdinv_users_pending_amount; ?>" data-id="<?php echo $customerListData->houdinv_user_id ?>">Update Pending amount</button>
        <button type="button" class="btn btn-success m-r-5 sendPaymentReminderEmailBtn" data-email="<?php echo $customerListData->houdinv_user_email ?>" data-payment="<?php echo $customerListData->houdinv_users_pending_amount; ?>" title="Send payment reminder">Send payment reminder</button>
        <?php }
        ?>

                
        </td>
        </tr>
        <?php }
        ?>      
        </tbody>
        </table>
        <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <!--Delete-->

        <div id="deleteCustomerModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Customer/deleteCustomer' ), array( 'method'=>'post' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Customer</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleteCustomerId" name="deleteCustomerId"/>
        <h4><b>Do you really want to Delete this customer ?</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="deleteCustomerData" value="Delete">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>

        </div>
<!---Start Change Status -->
        <div id="changeCustomerStatusModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <?php echo form_open(base_url( 'Customer/changeStatus' ), array( 'method'=>'post', 'id'=>'changeStatusModalData' ));?>
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Change Status</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group"> 
        <input type="hidden" class="changeCustomerStatusId" name="changeCustomerStatusId"/>
        <label for="field-1" class="control-label">Choose Status</label> 
        <select class="form-control required_validation_for_add_single_tag chooseStatus" name="chooseStatus">
        <option value="">Choose Status</option>
        <option value="active">Active</option>
        <option value="deactive">Deactive</option>
        </select>
        </div> 
        </div> 
        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <input type="submit" class="btn btn-info" value="Change Status" name="updateCustomerStatus" />
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- End Status -->



<!---Start Change Privilage -->
        <div id="changeCustomerPrivilageModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <?php echo form_open(base_url( 'Customer/changePrivilage' ), array( 'method'=>'post', 'id'=>'changePrivilageModalData' ));?>
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Change Privilaged amount</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group"> 
        <input type="hidden" class="changeCustomerPrivilageId" name="changeCustomerPrivilageId"/>
        <label for="field-1" class="control-label">Choose Privilage amount</label> 
        <input class="form-control required_validation_for_add_single_tag choosePrivilage setmaxValue number_validation" name="choosePrivilage"/>
      
        </div> 
        </div> 
        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <input type="submit" class="btn btn-info" value="Change Privilage" name="updateCustomerPrivilage" />
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- End privilage -->

<!---Start Change Pending amount -->

        <div id="changeCustomerPendingModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <?php echo form_open(base_url( 'Customer/changePendingAmount' ), array( 'method'=>'post', 'id'=>'changePendingModalData' ));?>
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Change Pending amount</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group"> 
        <input type="hidden" class="changeCustomerPendingId" name="changeCustomerPendingId"/>
        <label for="field-1" class="control-label">Choose Pending amount</label> 
        <input class="form-control required_validation_for_add_single_tag choosePendingAmount number_validation" name="choosePendingAmount"/>
      
        </div> 
        </div> 
        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <input type="submit" class="btn btn-info" value="Change Pending amount" name="updateCustomerPendingAmount" />
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- End privilage -->


 
        <!---Start Tag -->
        <div id="addCustomerTag" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <?php echo form_open(base_url( 'Customer/addTag' ), array( 'method'=>'post', 'id'=>'singleTagModalData' ));?>
        <div class="modal-content">  
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Add Tag</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12 form-horizontal"> 
        <div class="form-group" style="margin-bottom: 0px!important;">
        <label class="col-md-3 control-label">Add Tag</label>
        <div class="col-md-9">
        <input type="hidden" class="customerTagId" name="customerTagId"/>
        <input type="text" class="form-control required_validation_for_add_single_tag name_validation" name="addCustomerTagData" placeholder="Add Tag">
        </div>
        </div>
        </div> 
        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <input type="submit" class="btn btn-info" value="Add Tag" name="addSingleTag"/>
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- End Tag -->

        <!---Start Send -->
        <div id="sendEmailModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <?php echo form_open(base_url( 'Customer/sendEmail' ), array( 'method'=>'post', 'id'=>'sendEmailModalData' ));?>
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Send Email</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12 form-horizontal"> 
        <div class="form-group">
        <label class="col-md-3 control-label">Subject</label>
        <div class="col-md-9">
        <input type="hidden" class="customerEmail" name="customerEmail"/>
        <input type="text" name="emailSubject" class="form-control required_validation_for_add_single_tag name_validation" placeholder="Subject">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-3 control-label">Message</label>
        <div class="col-md-9">
        <textarea name="emailBody" class="form-control setEditor required_validation_for_add_single_tag name_validation" rows="6"></textarea>
        </div>
        </div>  
        </div> 

        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <input type="submit" class="btn btn-info" value="Send Email" name="sendCustomerEmail"/>
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- End Send -->
        

         <!--Send payment reminder-->

        <div id="sendPaymentReminder" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Customer/sendPaymentReminderEmail' ), array( 'method'=>'post' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="paymentReminderEmail" name="paymentReminderEmail"/>
        <input type="hidden" class="paymentReminderAmount" name="paymentReminderAmount"/>
        <h4><b>Are you sure you want to send payment reminder?</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="" value="Yes">
        </div>
        </div>
        </form>
        </div>
        </div>

 <div id="uploded_customer_csv" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >

<div class="modal-dialog">
 <?php echo form_open(base_url('csv/importcsv'), array( 'id' => 'importcsv', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

<h4 class="modal-title">Upload bulk Customer</h4>

</div>

<div class="modal-body">


<div class="row">
<div class="col-md-8">

<div class="form-group">

<label for="field-7" class="control-label">Choose CSV File</label>

 <input type="file" name="userfile" required class="required_validation_for_add_single_tag" > 
                 
                

</div>




</div>
<div class="col-md-4"> <label for="field-7" class="control-label">Customer sample CSV</label>
 <a href="upload/Customer sample.csv" download >Download</a> </div>
</div>

</div>

<div class="modal-footer">

<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

<input type="submit" class="btn btn-info "  name="submit" value="UPLOAD">

</div>

</div><?php echo form_close(); ?>

</div>

</div>

        <?php $this->load->view('Template/footer.php') ?>


        <script>
        $(document).ready(function(){
        $(".click_show").click(function(){
        $(".hide_div").show();
        $(".click_show").hide();
        });
        $(".click_hide").click(function(){
        $(".hide_div").hide();
        $(".click_show").show();
        });
        });
        </script>
        <script type="text/javascript">
        $(document).ready(function(){
                $(document).on('submit','#changePrivilageModalData','#changePendingAmount','#singleTagModalData,#changeStatusModalData,#sendEmailModalData',function(c){
                        var rep_image_val='';
                        $(this).find(".required_validation_for_add_single_tag").each(function()
                        {
                                var val22 = jQuery(this).val();
                                if (!val22)
                                {
                                        rep_image_val = 'error form';
                                        $(this).css("border-color","red");
                                }
                        });
                        $(this).find('.required_validation_for_add_single_tag').on('keyup blur',function()
                        {
                                $(this).css("border-color","#ccc");
                        });
                        if(rep_image_val)
                        {
                                c.preventDefault();
                                return false;
                        }
                        else
                        {
                                return true;
                        }        
                });
        })
        </script>