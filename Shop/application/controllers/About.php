<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class About extends CI_Controller
{
    function __construct()
	{ 
        parent::__construct();
        $this->load->model('Shopuserextrapages');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $getAbout = $this->Shopuserextrapages->fetchAboutData();

        
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/about',$getAbout);
    }
     
}
