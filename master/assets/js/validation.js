// Number validation
$(document).ready(function() {
$(".number_validation").keydown(function (e) {
if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
(e.keyCode >= 35 && e.keyCode <= 40)) {
return;
}
if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
e.preventDefault();
}
});
// Name validation
$(document).on('keydown', '.name_validation', function(e) {
if (e.which === 32 &&  e.target.selectionStart === 0) {return false;}  });
});
//Email Validation
jQuery(document).ready(function(){
function ValidateEmail(email) {
var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
return expr.test(email);
};
jQuery(".email_validation").blur(function () {
if (!ValidateEmail(jQuery(this).val())) {
jQuery(this).val("");
}
else {
return  true;
}
});
});
// DatePicker Value
$(document).ready(function(){
    $('.date_picker').focus(function(){
        $(this).daterangepicker({                    
        singleDatePicker: true,
        showDropdowns: false,    
        locale: { 
            format: 'YYYY-MM-DD',
        }
        });      
    });   
});
// on reset forget password
$(document).on('keyup','.conPass',function(){
    var getOriginalPass = $('.orgPass').val();
    if(getOriginalPass != $(this).val())
    {
        $(this).css('border','1px solid red');
        $('.resetButton').attr('disabled',true);
    }
    else
    {
        $('.resetButton').attr('disabled',false);
        $(this).css('border','1px solid #d3d3d3');
    }
});
$(document).on('keyup','.orgPass',function(){
    var getOriginalPass = $('.conPass').val();
    if(getOriginalPass != $(this).val())
    {
        $('.resetButton').attr('disabled',true);
    }
    else
    {
        $('.resetButton').attr('disabled',false);
        $(this).css('border','1px solid #d3d3d3');
    }
});
// plugin data delete
$(document).on('click','.deletePluginButtonData',function(){
    $('.deletePluginId').val($(this).attr('data-id'));
    $('#deletePluginModal').modal('show');
});
$(document).on('click','.editPluginBtnData',function(){
    $('.editPluginId').val($(this).attr('data-id'));
    $('.editPluginName').val($(this).attr('data-name'));
    $('.editPluginDescription').text($(this).attr('data-description'));
    $('.editPluginStatus').val($(this).attr('data-status'));
    $('#editPluginModal').modal('show');
});
// add package data
$(document).on('click','.masterCheckPackage',function(){
    if($(this).prop('checked') == true)
    {
        var setPluginIdData = "";
        $('.childCheckPackage').each(function(){
          if(setPluginIdData)
          {
            setPluginIdData = setPluginIdData+","+$(this).attr('data-id');
          }  
          else
          {
            setPluginIdData = $(this).attr('data-id');
          }
        });
        $('.pluginIddata').val('');
        $('.pluginIddata').val(setPluginIdData);
        $('.childCheckPackage').prop('checked',true);
    }
    else
    {
        $('.pluginIddata').val('');
        $('.childCheckPackage').prop('checked',false);
    }
});
$(document).on('click','.childCheckPackage',function(){
    var uncheckData = 0;
    var setPluginIdData = "";
    $('.childCheckPackage').each(function(){
        if($(this).prop('checked') == true)
        {
            if(setPluginIdData)
            {
            setPluginIdData = setPluginIdData+","+$(this).attr('data-id');
            }  
            else
            {
            setPluginIdData = $(this).attr('data-id');
            }   
        }
        if(!$(this).prop('checked') == true)
        {
            uncheckData++;
        }
    }); 
    $('.pluginIddata').val('');
    $('.pluginIddata').val(setPluginIdData);
    if(uncheckData > 0) 
    {
        $('.masterCheckPackage').prop('checked',false);
    }
    else
    {
        $('.masterCheckPackage').prop('checked',true);
    }
});
$(document).on('click','.deleteSMSPackageBtn',function(){
    $('.deleSMSPackageId').val($(this).attr('data-id'));
   $('#deleteSMSPackageModal').modal('show'); 
});
$(document).on('click','.editSMSPackageBtn',function(){
    $('.editSMSPackageId').val($(this).attr('data-id'));
    $('.editSMSPackageName').val($(this).attr('data-name'));
    $('.editSMSPackagePrice').val($(this).attr('data-price'));
    $('.editSMSPackageCount').val($(this).attr('data-count'));
    $('.editSMSPackageStatus').val($(this).attr('data-status'));
    $('#editSMSPackageModal').modal('show');
});
$(document).on('click','.deleteEmailPackageBtn',function(){
    $('.deleteEmailPackageId').val($(this).attr('data-id'));
    $('#deleteEmailPackageModal').modal('show');
});
$(document).on('click','.editEmailPackageData',function(){
    $('.EditEmailpackageId').val($(this).attr('data-id'));
    $('.editemailPackageName').val($(this).attr('data-name'));
    $('.editemailPackagePrice').val($(this).attr('data-price'));
    $('.editemailPackageCount').val($(this).attr('data-count'));
    $('.editemailPackageStatus').val($(this).attr('data-status'));
    $('#editEmailPackageModal').modal('show');
});
// main package page 
$(document).on('click','.deletePackageBtn',function(){
    $('.deletePackageId').val($(this).attr('data-id'));
    $('#deletePackageModal').modal('show');
});
// Setting language data
$(document).on('change','.changeLangaugeData',function(){
    $('.languageNameData').val($(this).children('option:selected').text());
});
// delete languge
$(document).on('click','.deleleLanguageBtnData',function(){
    $('.deleteLangugaeId').val($(this).attr('data-id'));
    $('#deleteLanguageModal').modal('show');
});
// edit language
$(document).on('click','.editLangaugeBtnData',function(){
    $('.editLanguageId').val($(this).attr('data-id'));
    $('.editLangugaeStatus').val($(this).attr('data-status'));
    $('.editLanguageName').val($(this).attr('data-language'));
    $('#editLanguageData').modal('show');
});
// setting template module
$(document).on('click','.deleteTemplateDataBtn',function(){
    $('.deleteTemplateId').val($(this).attr('data-id'));
    $('#deleteTemplateModal').modal('show');
});
$(document).on('click','.editTemplateBtnData',function(){
    $('.editTemplateName').val($(this).attr('data-name'));
    $('.editTemplateCategory').val($(this).attr('data-category'));
    $('.editTemplateDirectoryUrl').val($(this).attr('data-file'));
    $('.editTemplateDescription').val($(this).attr('data-description'));
    $('.editTemplateStatus').val($(this).attr('data-status'));
    $('.editTemplateId').val($(this).attr('data-id'));
    $('#editTempateModal').modal('show');
});
//  shop management
$(document).on('click','.deleteShopBtn',function(){
    $('.deleteShopId').val($(this).attr('data-id'));
    $('#deleteShopModal').modal('show');
});
$(document).on('click','.editShopLoginDetail',function(){
    $('.setuemail').val($(this).attr('data-email'));
    $('.setuiddata').val($(this).attr('data-id'));
    $('#editShopModal').modal('show');
})
// change shop status
$(document).on('click','.changeShopStatusBtn',function(){
    $('.chnageShopStatusId').val($(this).attr('data-id'));
    $('.changeShopStatus').val($(this).attr('data-status'));
    $('#changeShopStatusModal').modal('show');
});
$(document).on('click','.sendShopUserEmailBtn',function(){
    $('.shopUserEmail').val($(this).attr('data-email'));
    $('#sendShopUserEmail').modal('show');
});
