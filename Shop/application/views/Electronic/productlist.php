<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
  <!-- Linking -->
  <div class="linking">
    <div class="container">
      <ol class="breadcrumb">
        <li class="active">Products</li>
      </ol>
    </div>
  </div>

  <!-- Content -->
  <div id="content">

    <!-- Products -->
    <section class="padding-top-40 padding-bottom-60">
      <div class="container">
        <div class="row">
        <?php
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
          <!-- Products -->
          <div class="col-md-12">
            <?php
            $count_product=count($productList);
            if($count_product==0)
            {?>
              <div class="container"><div class="col-sm-12 text-center"><h3>Products are not available</h3></div></div>
            <?php } else { ?>
            <!-- Short List -->
            <div class="short-lst">
              <h2>Product List</h2>
              <ul>
                <!-- Short List -->
                <li>
                  <p>Total Products: <?php echo $count; ?></p>
                </li>

                <li>
                <?php echo form_open(base_url( 'Productlist/'.$this->uri->segment('2').'/'.$this->uri->segment('3').'/'.$this->uri->segment('4')), array( 'id' => 'Productlist', 'method'=>'get' ));?>
                  <select class="sort-select selectpicker" name="sort">
                  <option value="">select</option>
                <option value="name" <?php if($_REQUEST['sort'] =="name") { echo "selected"; }?> >Name Descending</option>
                <option value="date" <?php if($_REQUEST['sort'] =="date") { echo "selected"; }?>>Date Descending</option>
                <option value="price" <?php if($_REQUEST['sort'] =="pricehl") { echo "selected"; }?>>Price High to Low</option>
                <option value="price" <?php if($_REQUEST['sort'] =="pricelh") { echo "selected"; }?>>Price Low to High</option>
                  </select>
                  <?php echo form_close(); ?>
                </li>

              </ul>
            </div>
          <?php } ?>
            <!-- Items -->
            <div class="item-col-3 appendProductList">
            <?php

            foreach($productList as $productListData)
            {
                $getProductImage = json_decode($productListData->houdinv_products_main_images,true);
                $getProductPrice = json_decode($productListData->houdin_products_price,true);
            ?>
              <!-- Product -->
              <div class="product">
                <article> 
                <?php 
              if($getProductImage[0])
              {
                  $setMainData = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
              }
              else
              {
                  $setMainData = base_url()."Extra/noPhotoFound.png";
              }
              ?>
                <img class="img-responsive" src="<?php echo $setMainData; ?>" alt="" >
                <?php if($productListData->houdinv_products_total_stocks <= 0) { ?> <span <?php  if($productListData->houdinv_products_main_quotation == 1) { ?> style="top:41px" <?php  } ?> class="sale-tag">Out of Stock</span><?php } ?>
                <?php
                if($productListData->houdinv_products_main_quotation == 1)
                {
                ?>
                <span class="model_qute_show new-tag" data-id="<?php echo $productListData->houdin_products_id ?>"  data-toggle="modal">Ask Quotation</span>
                <?php
                }
                ?>
                <a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>" class="tittle"><?php echo substr($productListData->houdin_products_title,0, 35).".." ; ?></a>
                  <!-- Reviews -->
                <br/>
                  <div class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $productListData->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $productListData->houdin_products_final_price; ?> <?php  } ?></div>
                  <?php
                    if($productListData->houdinv_products_total_stocks <= 0)
                    {
                    if($storeSetting[0]->receieve_order_out == 1)
                    {
                        ?>
                        <a class="Add_to_cart_button cart-btn" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><i class="icon-basket-loaded"></i></a>
                    <?php  }
                    }
                    else
                    {
                    ?>
                    <a class="Add_to_cart_button cart-btn" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><i class="icon-basket-loaded"></i></a>

                    <?php }
                    ?>
                  </article>
              </div>
            <?php } ?>
        </div>
        <?php
        if($count > 0)
        {
            if($count >= 12)
            {
                $remain = $count-12;
            }
            else
            {
                $remain = 0;
            }
        ?>
        <div class="row">

        <?php
        if($remain > 0)
        {
        ?>
        <div class="col-sm-12"><button type="button" data-remain="<?php echo $remain ?>" data-last="12" class="btn-round btn-default acc_btn setLoadMoredata" style="width:100%;margin-top: 0px;padding: 4px;border-radius: 4px; background:#0088cc">Load More&nbsp;<i class="fa fa-spinner fa-spin setSpinnerData" style="display:none;"></i></button></div>
        <?php }
        ?>
        </div>
        <?php }
        ?>
          </div>
        </div>
      </div>
    </section>


  <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>


  <script>
        $('.sort-select').on('change',function()
        {
          document.getElementById('Productlist').submit();
        });
        </script>
        <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('click','.setLoadMoredata',function(){
                $(this).prop('disabled',true);
                $('.setSpinnerData').show();
                var getCategory = '<?php echo $this->uri->segment('2') ?>';
                var getSubCategory = '<?php echo  $this->uri->segment('3') ?>';
                var getSubSubCategory = '<?php echo $this->uri->segment('4') ?>';
                var sortBy = '<?php echo $_REQUEST['sort'] ?>';
                var dataLast = $(this).attr('data-last');
                var dataRemain = $(this).attr('data-remain');
                $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>Ajaxcontroller/fetchProductPaginationData",
                data: {"dataLast": dataLast,"dataRemain":dataRemain,"getCategory":getCategory,"getSubCategory":getSubCategory,"getSubSubCategory":getSubSubCategory,"sortBy":sortBy},
                success: function(datas) {
                    $('.setSpinnerData').hide();
                    $('.setLoadMoredata').prop('disabled',false);
                    var setData = jQuery.parseJSON(datas);

                       var grid = "";
                    for(var i=0; i < setData.main.length ; i++)
                    {
                        var $productListData = setData.main[i];
                        var textsplit = $productListData.houdin_products_title;
                        var $setCartDesign = "";
                          var $setStockBadge = "";
                          var $settingData = setData.settingData;
                            if($productListData.houdinv_products_total_stocks <= 0)
                                {
                                    if($settingData[0].receieve_order_out == 1)
                                    {
                                        $setCartDesign = '<a class="Add_to_cart_button cart-btn" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><i class="icon-basket-loaded"></i></a>';
                                    }
                                }
                                else
                                {
                                    $setCartDesign = '<a class="Add_to_cart_button cart-btn" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><i class="icon-basket-loaded"></i></a>';
                                }
                                if($productListData.houdinv_products_total_stocks <= 0)
                                {
                                    $setStockBadge = '<span class="sale-tag">Out of Stock</span>';
                                }

                          var   $getProductImage = jQuery.parseJSON($productListData.houdinv_products_main_images);
                           var     $getProductPrice = jQuery.parseJSON($productListData.houdin_products_price);
                              console.log($getProductPrice);
                                if($getProductPrice.discount != 0 && $getProductPrice.discount  != "")
                                {
                                   var $DiscountedPrice = $getProductPrice.price-$getProductPrice.discount;
                                   var $originalPrice = $getProductPrice.price;
                                }
                                else
                                {
                                  var  $DiscountedPrice = 0;
                                  var  $originalPrice = $getProductPrice.price;
                                }
                                if($DiscountedPrice == 0)
                                {
                                  var  $setPrice = $originalPrice;
                                }
                                else
                                {
                                   var $setPrice = "<strike>"+$originalPrice+"</strike>&nbsp;"+$DiscountedPrice+"";
                                }
                                if($getProductImage[0])
                                {
                                    var setMainImageData = '<?php echo $this->session->userdata('vendorURL'); ?>upload/productImage/'+$getProductImage[0]+'';
                                }
                                else
                                {
                                    var setMainImageData = '<?php echo base_url() ?>Extra/noPhotoFound.png';
                                }
                          grid+='<div class="product"><article><img class="img-responsive" src="'+setMainImageData+'" alt="" >'
                          +''+$setStockBadge+''
                        +'<a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'" class="tittle">'+textsplit.substring(0,35)+'</a><br/>'
                        +'<div class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;'+$setPrice+'</div>'+$setCartDesign+'</article></div>'

                    }
                    $('.appendProductList').append(grid);


                    $('.setLoadMoredata').attr('data-last',setData.last);
                }
                });
            });
        });
        </script>
