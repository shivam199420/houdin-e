<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); ?>
<style type="text/css">
	.heading_content ul{list-style: none;    padding-left: 0px;}
	.heading_content ul li a{    padding: 7px;
    display: block;}
	.heading_content>ul>li>ul>li> a{ padding-left: 10px;}
	.heading_content>ul>li>ul>li>ul>li> a{ padding-left: 25px;}
	.heading_content>ul>li>ul>li>ul>li>ul>li a{ padding-left: 40px;}

	.border-t{border-top: 1px solid #ddd;}
	.border-b{border-bottom: 1px solid #ddd;}
	.text-gray{color: #797979;}
</style>
<?php 
$getCurrency = getVendorCurrency();
if($getCurrency[0]->houdin_users_currency=="USD")
{
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="AUD"){
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="Euro"){
	$currencysymbol= "£";
}else if($getCurrency[0]->houdin_users_currency=="Pound"){
	$currencysymbol= "€";
}else if($getCurrency[0]->houdin_users_currency=="INR"){
	$currencysymbol= "₹";
}
?>
<div class="content-page">
<div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">

                <div class="col-md-12">
                   <h4 class="page-title">Report</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li class="active">Report Sheet</li>
                  </ol>
                  </div>
            </div>
            <div class="row m-t-20">
            <?php echo form_open(base_url( '' ),array( 'id' => 'filterReportData', 'method'=>'post' ));?>
           				<div class="col-sm-12">
						   <div class="col-md-4">
												<div class="form-group m-r-10">
												<label for="exampleInputEmail2">From</label>
													<input type="text" name="dateFrom" class="form-control date_picker dateFromText required_validation_for_filterAccount" id="" placeholder="Date From">
												</div>
												</div>
												<div class="col-md-4">
												<div class="form-group m-r-10">
                        <input type="hidden" name="accountId" value="<?php  echo $this->uri->segment('3'); ?>"/>
													<label for="exampleInputEmail2">To</label>
													<input type="text" name="dateTo" value="<?php echo date('Y-m-d'); ?>" name="to" class="form-control date_picker dateToText required_validation_for_filterAccount" id="">
												</div>
												</div>
                        <div class="col-md-4">
												<div class="form-group m-r-10">
													<label for="exampleInputEmail2"></label>
                          <input type="submit"  class="btn btn-default submitButtonData" value="Run Report"/>
												</div>
												</div>
                          </div>
                          <?php echo form_close(); ?>
                      </div>
                      <div class="m-t-40 ">
					  <div class="setErrorMessage"></div>
					  <!-- set final amount -->
					                        <div class="col-md-12 bg-white">
                      <div class="col-md-2"></div>
                      <div class="col-lg-12 m-t-30">
								<div class="panel panel-default" style="border: 1px solid #ddd;">
									<div class="panel-heading pull-left" style="width: 100%;">
										<h3 class="panel-title pull-left">Accounts Report</h3>
										<div class="icon-right pull-right">
										<!-- <a href="https://hawkscode.org/vendor/Accounts/balancesheetpdf/2018-10-26/2018-11-02" class="btn setPdfUrl btn-default"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a> -->

										</div>
									</div>
									<div class="panel-body">
									<div class="heading_sheet text-center">
																		<h4><?php echo $accountName; ?></h4>
									<h5><strong>Account Quickreport</strong></h5>
									<p class="setDateRange">Till <?php echo date('d-m-Y'); ?></p>
									</div>
									<div class="heading_content">
									<ul class="">
									<li class=" border-b border-t" style="width: 100%;">
                                     <div class="card-box table-responsive">
              
              <table class="table table-hover table-bordered table_shop_custom">
                <thead>
                  <tr>

                    <th style="">Date</th>
                    <th>Transaction Type</th>
                    <th>Payee Name</th>
                    <th>Memo</th>
                    <th>Account</th>
                    <th>CLR</th>
                    <th>Amount</th>
                    <th>Tax</th> 
                    <th>Balance</th>
                  </tr>
                </thead>
                <tbody class="border-t border-b appendRowData">
                <?php 
                if(count($all) > 0)
                {
                  foreach($all as $values)
                  {                                    
                  ?>
                  <tr>
                  <td><?php if($values['main']['houdinv_accounts_balance_sheet_date']) { echo date('d-m-Y',strtotime($values['main']['houdinv_accounts_balance_sheet_date'])); } else { echo "--"; }  ?></td>
                  <td><?php if($values['main']['houdinv_accounts_balance_sheet_ref_type']) { echo $values['main']['houdinv_accounts_balance_sheet_ref_type']; } else { echo "--"; } ?></td>
                  <td><?php if($values['name']) { echo $values['name']; } else { echo "--"; } ?></td>
                  <td><?php if($values['main']['houdinv_accounts_balance_sheet_memo']) { echo $values['main']['houdinv_accounts_balance_sheet_memo']; } else { echo "--"; } ?></td>
                  <td><?php if($values['account']) { echo $values['account']; } else { echo "--"; } ?></td>
                  <td><?php if($values['main']['houdinv_accounts_balance_sheet_transaction_added_status']) { echo $values['main']['houdinv_accounts_balance_sheet_transaction_added_status']; } else { echo "--"; } ?></td>
                  <td><?php if($values['main']['houdinv_accounts_balance_sheet__increase']) { echo $values['main']['houdinv_accounts_balance_sheet__increase']; } else if($values['main']['houdinv_accounts_balance_sheet_decrease']) { echo "-".$values['main']['houdinv_accounts_balance_sheet_decrease']; } else { echo "--"; } ?></td>
                  <td><?php if($values['main']['houdinv_accounts_balance_sheet_tax']) { echo $values['main']['houdinv_accounts_balance_sheet_tax']; } else { echo "--"; } ?></td>
                  <td><?php if($values['balance']) { echo $values['balance']; } else { echo "--"; } ?></td>
                  </tr>
                  <?php $setFinalBalance = $values['balance']; }
                  
                }
                else
                {
                ?>
                <tr>
                <td colspan="9" class="text-center">No Record Found</td>
                </tr>
                <?php }
                ?>
                
                </tbody>
                </table>
                </div>
                </li>
                                    
                                    </ul>
                                    <?php 
                                    if(count($all) > 0)
                                    {
                                    ?>
									<ul class="setRecordData">
						
	
									
									<li class="border-t border-b"><a class="text-gray"><strong><span class="">Total for <?php echo $accountName; ?> </span><span class="pull-right totalProfitData setFinalAmountData"><?php echo $currencysymbol." ".$setFinalBalance; ?></span></strong></a></li>
									
									</li>
									<li class="border-b"><a class="text-gray"><strong><span class="">Total</span><span class="pull-right totalAccountRecievableData setFinalAmountData"><?php echo $currencysymbol." ".$setFinalBalance; ?></span></strong></a></li>
									<li class="border-b"></li>
									</ul>
                                    <?php } ?>
									</div>
									</div>
								</div>
							</div>
							<div class="col-md-2"></div>
                      </div>
                      </div>
                    </div>
                  </div>
 <?php $this->load->view('Template/footer.php') ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#filterReportData',function(e){
      var setBaseData = '<?php echo base_url(); ?>';
			var check_required_field='';
			$(".required_validation_for_filterAccount").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
      e.preventDefault();
      var getFormValue = $('#filterReportData').serializeArray();
      $('.submitButtonData').prop('disabled',true);
      jQuery.ajax({
        type: "POST",
        url: setBaseData+"Accounts/filterAccountData",
        data: getFormValue,
        success: function(data) {
          $('.submitButtonData').prop('disabled',false);
          if(data)
          {
            var jsonData = $.parseJSON(data);
            if(jsonData.length > 0)
            {
              $('.setRecordData').show();
              var setHtmlData = "";
            for(var index = 0; index < jsonData.length; index++)
            {
              var setArrayData = jsonData[index];
              if(setArrayData['main'].houdinv_accounts_balance_sheet_date)
              {
                var date    = new Date(setArrayData['main'].houdinv_accounts_balance_sheet_date),
                  yr      = date.getFullYear(),
                  month   = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
                  month = month+1;
                  day     = date.getDate()  < 10 ? '0' + date.getDate()  : date.getDate(),
                newDate = day+'-'+month+'-'+yr;
              }
              else
              {
                newDate = '--';
              }
              if(setArrayData['main'].houdinv_accounts_balance_sheet_ref_type)
              {
                var setRef = setArrayData['main'].houdinv_accounts_balance_sheet_ref_type;
              }
              else
              {
                var setRef = '--';
              }
              if(setArrayData.name)
              {
                var nameData = setArrayData.name;
              }
              else
              {
                var nameData = '--';
              }
              if(setArrayData['main'].houdinv_accounts_balance_sheet_memo)
              {
                var memoData = setArrayData['main'].houdinv_accounts_balance_sheet_memo;
              }
              else
              {
                var memoData = '--';
              }
              if(setArrayData.account)
              {
                var accountData = setArrayData.account;
              }
              else
              {
                var accountData = '--';
              }
              if(setArrayData['main'].houdinv_accounts_balance_sheet_transaction_added_status)
              {
                var statusData = setArrayData['main'].houdinv_accounts_balance_sheet_transaction_added_status;
              }
              else
              {
                var statusData = '--';
              }
              if(setArrayData['main'].houdinv_accounts_balance_sheet__increase)
              {
                var amountData = setArrayData['main'].houdinv_accounts_balance_sheet__increase;
              }
              else if(setArrayData['main'].houdinv_accounts_balance_sheet_decrease)
              {
                var amountData = "-"+setArrayData['main'].houdinv_accounts_balance_sheet_decrease;
              }
              else
              {
                var amountData = '--';
              }
              if(setArrayData['main'].houdinv_accounts_balance_sheet_tax)
              {
                var taxData = setArrayData['main'].houdinv_accounts_balance_sheet_tax;
              }
              else
              {
                var taxData = '--';
              }
              if(setArrayData.balance)
              {
                var balanceData = setArrayData.balance;
              }
              else
              {
                var balanceData = '--';
              }
              var setBalaanceDataValue = balanceData;
              setHtmlData +='<tr>'
                  +'<td>'+newDate+'</td>'
                  +'<td>'+setRef+'</td>'
                  +'<td>'+nameData+'</td>'
                  +'<td>'+memoData+'</td>'
                  +'<td>'+accountData+'</td>'
                  +'<td>'+statusData+'</td>'
                  +'<td>'+amountData+'</td>'
                  +'<td>'+taxData+'</td>'
                  +'<td>'+balanceData+'</td></tr>';
            }
            var setText = '<?php echo $currencysymbol ?>'+setBalaanceDataValue;
            $('.setFinalAmountData').text('').text(setText);
            $('.appendRowData').html('').html(setHtmlData);
            }
            else
            {
              $('.setRecordData').hide();
              $('.appendRowData').html('').html('<tr><td colspan="9" class="text-center">No Record Found</td></tr>');
            }
            var dateFrom = $('.dateFromText').val();
            var dateTo = $('.dateToText').val();
            $('.setDateRange').text('').text(dateFrom+' - '+dateTo);
          }
        }
        }); 
			}
		});
	});
	</script>