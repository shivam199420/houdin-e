//  validate form data
$(document).on('submit','#Addproductn',function(c){
    if ($('.card-box.has_cusm .card-box').length <= 0) {
        $('.variant_hide').find('.form-control.ware_house').addClass('required_validation_for_product');
        $('.variant_hide').find('.form-control.without_variant_data').addClass('required_validation_for_product');
      } else {
        $('.variant_hide').find('.form-control.ware_house').removeClass('required_validation_for_product');
        $('.variant_hide').find('.form-control.without_variant_data').removeClass('required_validation_for_product');
      }
    var rep_image_val='';
    $(this).find(".required_validation_for_product").each(function()
    {
        var val22 = jQuery(this).val();
        if (!val22) {
            rep_image_val = 'error form';
            $(this).css("border-color","red");
            if ($(this).hasClass('ware_house')) {
                $(this).siblings('span').css("border","1px solid red");
            }
        } else {
            $(this).css("border","1px solid #e6f2ff");
            if ($(this).hasClass('ware_house')) {
                $(this).siblings('span').css("border","1px solid #e6f2ff");
            }
        }
    });
    // if (!jQuery('.radio_valid:checked').val()) {
    //     rep_image_val = 'error form';
    //     $(this).css("border-color","red");  
    // }
    // $(this).find(".first_image").each(function() {
    //     var val_image = jQuery(this).val(); 
    //     if (!val_image) {
    //         rep_image_val = 'error form';
    //         $(this).css("border-color","red");
    //         $(this).css("border-width","1px");
    //         $(this).siblings('.message_text').text('Please generate  barcode');
    //     }  
    // });
    // $(".first_image").on("change",function()
    // {
    //       $(this).css("border-color","ccc");
    //       $(this).css("border-width","0px");
    // });
    $('.validated_numeric,.validated_integer,.barcode_input').on('keyup blur change',function()
    {     
        $(this).css("border-color","#ccc");
        $(this).siblings('.message_text').text('');
    });
    if(rep_image_val) {
        c.preventDefault();
        return false;
    }
});