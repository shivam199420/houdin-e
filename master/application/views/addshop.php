<?php $this->load->view('Template/header.php') ?>
    <?php $this->load->view('Template/sidebar.php') ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/intlTelInput.css">
    <style>
        .intl-tel-input
        {
            width:100%;    
        }
    </style>
    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">
    <!-- Page-Title -->
    <div class="row">
    <div class="col-sm-12 m-b-15">
    <h4 class="page-title">Add Shop</h4>

    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
    <?php 
        if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        else if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        ?>
  </div>
  </div>
    <div class="row">
    <?php echo form_open(base_url( 'Shop/add' ), array( 'id' => 'addpayumoneyCredentials' ));?>
    <div class="col-md-12">
    <div class="card-box row">
    <div class="form-group">
    <label>User Name</label>
    <input type="text" name="userName" class="form-control required_validation_for_add_payumoney_credentials name_validation" placeholder="Enter User Name" />
    </div>
    <div class="form-group">
    <label>User Contact</label>
    <input type="text" name="userContact" class="form-control Internationphonecodedata required_validation_for_add_payumoney_credentials name_validation" placeholder="Enter User Contact" />
    </div>
    <div class="form-group">
    <label>Store Name</label>
    <input type="text" name="stroeName" class="form-control required_validation_for_add_payumoney_credentials name_validation" placeholder="Enter Store Name" />
    </div>
    <div class="form-group">
    <label>Email</label>
    <input type="text" name="useremail" class="form-control required_validation_for_add_payumoney_credentials email_Validation name_validation" placeholder="Enter Email" />
    </div>
    <div class="form-group">
    <label>Address</label>
    <textarea name="address" class="form-control required_validation_for_add_payumoney_credentials name_validation" rows="5"></textarea>
    </div>
    <div class="form-group">
    <label>City</label>
    <input type="text" name="city" class="form-control required_validation_for_add_payumoney_credentials name_validation" placeholder="Enter City" />
    </div>
    <div class="form-group">
    <label>Country</label>
    <select name="country" class="form-control required_validation_for_add_payumoney_credentials">
    <option value="">Choose Country</option>
    <?php 
    foreach($country as $countryList)
    {
    ?>
    <option value="<?php echo $countryList->houdin_country_id ?>"><?php echo $countryList->houdin_country_name ?></option>
    <?php }
    ?>
    </select>
    </div>
    <div class="form-group">
    <label>Are you already selling?</label>
    <select name="sellingStatus" class="form-control required_validation_for_add_payumoney_credentials">
    <option value="">Choose</option>
    <option value="yes">Yes</option>
    <option value="no">No</option>
    </select>
    </div>
    <div class="form-group">
    <label>Choose Business Category</label>
    <select name="businessCategory" class="form-control required_validation_for_add_payumoney_credentials">
    <option value="">Choose Business Category</option>
    <?php 
    foreach($shopCategory as $shopCategoryList)
    {
    ?>
    <option value="<?php echo $shopCategoryList->houdin_business_category_id ?>"><?php echo $shopCategoryList->houdin_business_category_name ?></option>
    <?php }
    ?>
    </select>
    </div>
    <div class="form-group">
    <label>Choose Business Language</label>
    <select name="language" class="form-control required_validation_for_add_payumoney_credentials">
    <option value="">Choose Business Language</option>
    <?php foreach($language as $languageList) 
    {
    ?>
    <option value="<?php echo $languageList->houdin_language_id ?>"><?php echo $languageList->houdin_language_name_value ?></option>
    <?php }
    ?>
    </select>
    </div>
    <div class="form-group">
    <label>Choose Plan</label>
    <select name="plan" class="form-control required_validation_for_add_payumoney_credentials">
    <option value="">Choose Plan</option>
    <option value="0">Trial</option>
    <?php foreach($plan as $planList) 
    {
    ?>
    <option value="<?php echo $planList->houdin_package_id ?>"><?php echo $planList->houdin_package_name."(".$planList->houdin_package_price.")" ?></option>
    <?php }
    ?>
    </select>
    </div>
    <div class="form-group">
    <label>Choose Business Currency</label>
    <select name="currency" class="form-control required_validation_for_add_payumoney_credentials">
    <option value="">Choose Business Currency</option>
    <option value="INR">INR</option>
    <option value="USD">USD</option>
    <option value="AUD">AUD</option>
    <option value="EURO">EURO</option>
    <option value="POUND">POUND</option>
    </select>
    </div>
     <div class="form-group">
    <label>Choose Account Type</label>
    <select name="accounttype" class="form-control required_validation_for_add_payumoney_credentials">
    <option value="">Choose Account Type</option>
    <option value="live">Live</option>
    <option value="testing">Testing</option>
    </select>
    </div>
    <div class="col-md-12 ">
    <input type="submit" class="btn btn-default pull-right m-r-10" name="addShopData" value="Submit">
    <button type="reset" class="btn btn-info pull-right m-r-10">Reset</button>
    </div>
    </div>
    </div>
    <?php echo form_close(); ?>
    </div>


    </div> <!-- container -->

    </div> <!-- content -->

    <?php $this->load->view('Template/footer.php') ?>
    <script src="<?php echo base_url(); ?>assets/js/intlTelInput.min.js"></script>
<!-- CLient side form validation -->
<script type="text/javascript">
            $(document).ready(function(){
                $(document).on('submit','#addpayumoneyCredentials',function(){
                    var check_required_field='';
                    $(this).find(".required_validation_for_add_payumoney_credentials").each(function(){
                        var val22 = $(this).val();
                        if (!val22){
                            check_required_field =$(this).size();
                            $(this).css("border-color","#ccc");
                            $(this).css("border-color","red");
                        }
                        $(this).on('keypress change',function(){
                            $(this).css("border-color","#ccc");
                        });
                    });
                    if(check_required_field)
                    {
                        return false;
                    }
                    else {
                        return true;
                    }
                });
            });
            </script>
            
            <script>
	$(document).ready(function(){
    $(".Internationphonecodedata").intlTelInput({
          initialCountry: "auto",
          geoIpLookup: function(callback) {
            $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              callback(countryCode);
            });
          },
          hiddenInput: "full_number",
    // allowDropdown: false,
    // autoHideDialCode: false,
    // autoPlaceholder: "off",
    // dropdownContainer: "body",
    // excludeCountries: ["us"],
    // formatOnDisplay: false,
    // geoIpLookup: function(callback) {    
    //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    //     var countryCode = (resp && resp.country) ? resp.country : "";
    //     callback(countryCode);
    //   });
    // },
    // hiddenInput: "full_number",
    // initialCountry: "auto",
    // nationalMode: false,
    // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    // placeholderNumberType: "MOBILE",
    preferredCountries: ['in', 'us'],
    // separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/js/utils.js"
    });
});	
  </script>