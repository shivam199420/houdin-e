<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        if($this->session->userdata('userAuth') != "")
        {
            redirect(base_url()."Orders", 'refresh');
        }
        // $this->load->library('google');
        $this->load->model('Shopusermodel');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
        $this->from = fromemail();
    }
    public function index()
    {
       // print_r($this->Daynamic_templat);
        $config_app = switchDynamicDatabase();
        // print_r($config_app);
        $Template_include_path['Template']=$this->Daynamic_templat;
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view($getShopName.'/register',$Template_include_path);
    }
    public function addShopUsers()
    {
        $getPassword = $this->input->post('userPass');
        $getConfirmPass = $this->input->post('userRetypePass');
        if($getPassword == $getConfirmPass)
        {
            $this->form_validation->set_rules('userFirstName','First Name','required');
            $this->form_validation->set_rules('userLastName','Last Name','required');
            $this->form_validation->set_rules('userEmail','Email','required|valid_email');
            $this->form_validation->set_rules('userContact','Contact Number','required');
            $this->form_validation->set_rules('userPass','Password','required');
            $this->form_validation->set_rules('userRetypePass','Confirm Password','required');
            if($this->form_validation->run() == true)
            {
                    $letters1='abcdefghijklmnopqrstuvwxyz';
                    $string1='';
                    for($x=0; $x<3; ++$x)
                    {
                        $string1.=$letters1[rand(0,25)].rand(0,9);
                    }
                    $saltdata = password_hash($string1,PASSWORD_DEFAULT);
                    $pass = crypt($getPassword,$saltdata);
                    $setUserNameData = $this->input->post('userFirstName')." ".$this->input->post('userLastName');
                    $setData = strtotime(date('Y-m-d H:i:s'));
                    $setInsertArray = array('houdinv_user_name'=>$setUserNameData,'houdinv_user_email'=>$this->input->post('userEmail'),'houdinv_user_password'=>$pass,'houdin_user_password_salt'=>$saltdata,'houdinv_user_contact'=>$this->input->post('userContact'),'houdinv_user_is_verified'=>'1','houdinv_user_is_active'=>'active','houdinv_user_created_at'=>$setData);
                    $getUserRegistrationStatus = $this->Shopusermodel->addUserData($setInsertArray);
                    if($getUserRegistrationStatus['message'] == 'yes')
                    {
                        // get email count
                        $getEmailCount = fetchEmailCount($this->session->userdata('shopName'));
                        if($getEmailCount[0]->houdinv_emailsms_stats_remaining_credits > 0)
                        {
                            $getEmailTemplate = fetchRegisterTemplate($this->session->userdata('shopName'));
                            if(count($getEmailTemplate) > 0)
                            {
                                $setSubject = $getEmailTemplate[0]->houdinv_email_template_subject;
                                $getMessage = $getEmailTemplate[0]->houdinv_email_template_message;
                                $getFindString = strpos($getMessage,"{user_name}");
                                if($getFindString)
                                {
                                    $setMessage = str_replace('{user_name}',$this->input->post('userFirstName'),$getMessage);
                                }
                                else
                                {
                                    $setMessage = $getFindString;
                                }
                                // send email to admin user
                                $url = 'https://api.sendgrid.com/';
                                $user = 'anuankita';
                                $pass = 'india@123';
                                $json_string = array(
                                'to' => array(
                                    $this->input->post('userEmail')
                                ),
                                'category' => 'test_category'
                                );
                                $htm = '
                                <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                                <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                                <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                                <img src="'.base_url().'assets/images/main-logo.png" style="max-width: 53%;width:100%"/></td></tr>
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$setMessage.'</td></tr>

                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                                <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                                <table width="100%" ><tr >
                                <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e 2018--All right reserverd </td></tr>
                                <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
                                </tr></table>
                                </div>
                                </div></td></tr></table>';
                                $params = array(
                                    'api_user'  => $user,
                                    'api_key'   => $pass,
                                    'x-smtpapi' => json_encode($json_string),
                                    'to'        => $this->input->post('userEmail'),
                                    'fromname'  => 'Registration',
                                    'subject'   => $setSubject,
                                    'html'      => $htm,
                                    'from'      => $this->from,
                                );
                                $request =  $url.'api/mail.send.json';
                                $session = curl_init($request);
                                curl_setopt ($session, CURLOPT_POST, true);
                                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                                curl_setopt($session, CURLOPT_HEADER, false);
                                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                                $response = curl_exec($session);
                                curl_close($session);
                                $getResponse = json_decode($response);
                                if($getResponse->message == 'success')
                                {
                                    $getRemaining = $getEmailCount[0]->houdinv_emailsms_stats_remaining_credits-1;
                                    updateSuceessEmailCount($getRemaining,$this->session->userdata('shopName'));
                                    $this->session->set_flashdata('success','You are successfully registered with us');
                                    redirect(base_url()."register", 'refresh');
                                }
                                else
                                {
                                    updateErrorEmailLog($this->session->userdata('shopName'));
                                    $this->session->set_flashdata('success','You are successfully registered with us');

                                    redirect(base_url()."register", 'refresh');
                                }
                            }
                            else
                            {
                                $this->session->set_flashdata('success','You are successfully registered with us');
                                redirect(base_url()."register", 'refresh');
                            }
                        }
                        $this->session->set_flashdata('success','You are successfully registered with us');
                        redirect(base_url()."register", 'refresh');
                    }
                    else if($getUserRegistrationStatus['message'] == 'email')
                    {
                        $this->session->set_flashdata('error','Email address is already taken');
                        redirect(base_url()."register", 'refresh');
                    }
                    else if($getUserRegistrationStatus['message'] == 'contact')
                    {
                        $this->session->set_flashdata('error','Contact number is already taken');
                        redirect(base_url()."register", 'refresh');
                    }
                    else
                    {
                        $this->session->set_flashdata('error','Something went wrong. Please try again');
                        redirect(base_url()."register", 'refresh');
                    }

            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url()."register", 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('error','Passwords are not matched');
            redirect(base_url()."register", 'refresh');
        }
    }
    public function checkUserAuth()
    {
        $this->form_validation->set_rules('userPass','Password','required');
        $this->form_validation->set_rules('userEmail','Email','required|valid_email');
        if($this->form_validation->run() == true)
        {
            $setAuthArray = array('email'=>$this->input->post('userEmail'),'password'=>$this->input->post('userPass'));
            $getAuthData = $this->Shopusermodel->checkUserAuth($setAuthArray);
            if($getAuthData['message'] == 'email')
            {
                $this->session->set_flashdata('error','Please enter correct email address');
                redirect(base_url()."register", 'refresh');
            }
            else if($getAuthData['message'] == 'pass')
            {
                $this->session->set_flashdata('error','Please enter correct password');
                redirect(base_url()."register", 'refresh');
            }
            else if($getAuthData['message'] == 'yes')
            {
                $this->session->set_flashdata('success','Login successfully');
                $this->session->set_userdata('userAuth',$getAuthData['userId']);
                redirect(base_url()."register", 'refresh');
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url()."register", 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."register", 'refresh');
        }
    }
    public function checkUserForgot()
    {
        $this->form_validation->set_rules('emailForgot','Email','required|valid_email');
        if($this->form_validation->run() == true)
        {
            $getForgotStatus = $this->Shopusermodel->forgotPass($this->input->post('emailForgot'));
            if($getForgotStatus['message'] == 'no')
            {
                $this->session->set_flashdata('error','Something went wrong. Please contact to support');
                redirect(base_url()."register", 'refresh');
            }
            else if($getForgotStatus['message'] == 'credit')
            {
                $this->session->set_flashdata('error','Something went wrong. Please contact to support');
                redirect(base_url()."register", 'refresh');
            }
            else if($getForgotStatus['message'] == 'email')
            {
                $this->session->set_flashdata('error','Please enter correct email address');
                redirect(base_url()."register", 'refresh');
            }
            else if($getForgotStatus['message'] == 'yes')
            {
                // send email to admin user
                $url = 'https://api.sendgrid.com/';
                $user = 'anuankita';
                $pass = 'india@123';
                $json_string = array(
                'to' => array(
                    $this->input->post('emailForgot')
                ),
                'category' => 'test_category'
                );
                $htm = '
                <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                <img src="'.base_url().'assets/images/main-logo.png" style="max-width: 53%;width:100%"/></td></tr>
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Your pin is:</strong></td></tr>
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$getForgotStatus['token'].'</td></tr>
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                <table width="100%" ><tr >
                <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e 2018--All right reserverd </td></tr>
                <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
                </tr></table>
                </div>
                </div></td></tr></table>';
                $params = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'x-smtpapi' => json_encode($json_string),
                    'to'        => $this->input->post('emailForgot'),
                    'fromname'  => 'Houdin-e',
                    'subject'   => 'Recover Your Passoword',
                    'html'      => $htm,
                    'from'      => $this->from,
                );
                $request =  $url.'api/mail.send.json';
                $session = curl_init($request);
                curl_setopt ($session, CURLOPT_POST, true);
                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                curl_setopt($session, CURLOPT_HEADER, false);
                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($session);
                curl_close($session);
                $getResponse = json_decode($response);
                if($getResponse->message == 'success')
                {
                    $this->Shopusermodel->updateEmailLog($getForgotStatus['credit']);
                    $this->session->set_userdata('tempToken',$getForgotStatus['token']);
                    $this->session->set_flashdata('success','Please check your email');
                    redirect(base_url()."Forgot", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url()."register", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url()."register", 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."register", 'refresh');
        }
    }

    // function gregisterlogin()
    //   {
    //
    //       // Fill CLIENT ID, CLIENT SECRET ID, REDIRECT URI from Google Developer Console
    //       $client_id = '440438588350-3u3pcfts5ueto8oplr012dkccbt24dva.apps.googleusercontent.com';
    //       $client_secret = 'NwzDyOl3wigQnVYbG_ZdN2i9';
    //       $redirect_uri = base_url('Register/gregistercallback');
    //
    //       //Create Client Request to access Google API
    //       $client = new Google_Client();
    //       $client->setApplicationName("Houdin-e");
    //       $client->setClientId($client_id);
    //       $client->setClientSecret($client_secret);
    //       $client->setRedirectUri($redirect_uri);
    //       $client->addScope("email");
    //       $client->addScope("profile");
    //
    //       //Send Client Request
    //       $objOAuthService = new Google_Service_Oauth2($client);
    //
    //       $authUrl = $client->createAuthUrl();
    //
    //       header('Location: '.$authUrl);
    //   }
    //
    //   function gregistercallback()
    //   {
    //           // Fill CLIENT ID, CLIENT SECRET ID, REDIRECT URI from Google Developer Console
    //    $client_id = '440438588350-3u3pcfts5ueto8oplr012dkccbt24dva.apps.googleusercontent.com';
    //    $client_secret = 'NwzDyOl3wigQnVYbG_ZdN2i9';
    //    $api_keys='AIzaSyCHnU90e1p6PyqPw5e4hQeM0zRKmzOdHQ0';
    //    $redirect_uri = base_url('Register/gregistercallback');
    //
    //   //Create Client Request to access Google API
    //   $client = new Google_Client();
    //   $client->setApplicationName("Houdin-e");
    //
    //   $client->setClientId($client_id);
    //   $client->setDeveloperKey($api_keys);
    //   $client->setClientSecret($client_secret);
    //   $client->setRedirectUri($redirect_uri);
    //   $client->addScope("email");
    //   $client->addScope("profile");
    //
    //   //Send Client Request
    //   $service = new Google_Service_Oauth2($client);
    //
    //   $client->authenticate($_GET['code']);
    //   $_SESSION['access_token'] = $client->getAccessToken();
    //
    //   // User information retrieval starts..............................
    //
    //   $user = $service->userinfo->get(); //get user info
    //   print_r($user);
    //   exit;
    //   // echo "</br> User ID :".$user->id;
    //   // echo "</br> User Name :".$user->name;
    //   // echo "</br> Gender :".$user->gender;
    //   // echo "</br> User Email :".$user->email;
    //   // echo "</br> User Link :".$user->link;
    //   // echo "</br><img src='$user->picture' height='200' width='200' > ";
    //
    //   }




}
