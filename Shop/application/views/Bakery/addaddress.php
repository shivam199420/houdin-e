        <?php
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/Template/header'); ?>
        <div id="back2top"><i class="fa fa-angle-up"></i></div>

        <div class="loader"></div>

        <div class="page-wrap">

          <!--section-->

          <div class="ps-section--hero"><img src="images/hero/01.jpg" alt="">

            <div class="ps-section__content text-center">

              <h3 class="ps-section__title">New Address</h3>

            </div>

          </div>

          <div class="ps-section pt-80 pb-80">

            <?php
            if($this->session->flashdata('error'))
            {
                echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
            }
            else if($this->session->flashdata('success'))
            {
                echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
            }
            ?>
            <?php echo form_open(base_url( 'Address/adduseraddress' ), array( 'id' => 'addaddress', 'method'=>'post' ));?>

            <div class="container">

              <div class="ps-contact ps-contact--2">

                <div class="row">

                      <div class="col-lg-3 col-md-3"></div>

                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

                        <h3 class="text-center">Add Details</h3>

                          <hr>

                          <p class="text-center">Add Your New Address here..!!</p>

                        <div class="ps-contact__form">

                          <div class="form-group">

                            <input name="username" placeholder="Name" class="form-control required_validation_for_address name_validation" type="text">

                          </div>

                          <div class="form-group">

                            <input  name="userphone" placeholder="Phone number" class="form-control required_validation_for_address name_validation" type="text">

                          </div>

                          <div class="form-group">

                            <textarea rows="3" name="usermainaddress" placeholder="Street address. Apartment, suite, unit etc. (optional)" class="form-control required_validation_for_address name_validation"></textarea>

                          </div>

                          <div class="form-group">

                            <input name="usercity" placeholder="Town/City*" class="form-control required_validation_for_address name_validation" type="text">

                          </div>

                          <div class="form-group">

                            <input name="userpincode" placeholder="Post code / Zip" class="form-control required_validation_for_address name_validation" type="text">

                          </div>

                          <div class="form-group">

                            <select class="form-control required_validation_for_address" name="usercountry">
                            <option value="">Choose Country</option>
                            <option value="<?php echo $countryList[0]->houdin_user_shop_country ?>"><?php echo $countryList[0]->houdin_country_name ?></option>
                            </select>

                          </div>

                          <div class="form-group text-center mt-30">

                            <input type="submit" class="ps-btn ps-btn--sm ps-contact__submit" value="Add Address"/>

                          </div>

                        </div>

                      </div>

                </div>

              </div>

            </div>
            <?php echo form_close(); ?>

          </div>

        <?php
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/Template/footer'); ?>

        <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#addaddress',function(){
                var check_required_field='';
                $(this).find(".required_validation_for_address").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>
