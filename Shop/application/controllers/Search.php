<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Search extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        //$this->load->model('Searchmodel');
        $this->perPage = 12;
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $keyword=$_GET['key'];
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => base_url().'Webserviceproductsearch',
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => array(
            'shopName' => 'category1',
            'keyword' => $keyword
             
        )
    ));
    $resp = curl_exec($curl);
    curl_close($curl);

    $getJsonEditVehicle = json_decode($resp);
   // print_r($getJsonEditVehicle->cartShow);
    //die;
$productList= $getJsonEditVehicle->productList;

foreach($productList as $productLists){

//  print_r($productLists);
 if($productLists->productId!='0')
{
    $setActionData = base_url()."Productlistview/".$productLists->productId;
}
else
{
    $setActionData = base_url()."Productlistview/variant/".$productLists->variantId;
}
    $products[]=array("name"=>$productLists->productName,    
    "action"=>$setActionData,
    "id"=> $productLists->productId,
    "clientLevelId"=>$productLists->variantId,
    "category"=> "All Others");

}

//print_r($getJsonEditVehicle);
$categoryList=$getJsonEditVehicle->categoryList;
foreach($categoryList as $categoryLists){

   // print_r($categoryLists);
    $setActionData = base_url()."Productlist/".$categoryLists->categoryId;
    if($categoryLists->subCategoryId != "")
    {
        $setActionData = base_url()."Productlist/".$categoryLists->categoryId."/".$categoryLists->subCategoryId;
    }
    else if($categoryLists->subSubCategoryId != "")
    {
        $setActionData =base_url()."Productlist/".$categoryLists->categoryId."/".$categoryLists->subCategoryId."/".$categoryLists->subSubCategoryId;
    }
    //echo $setActionData."<br/>";
        $products[]=array("name"=>$categoryLists->categoyrName,    
        "action"=> $setActionData,
       "id"=> $categoryLists->categoryId,
        "subCategoryId"=>$categoryLists->subCategoryId,
       "category"=> "Category");
   
   }
$product=array('entries'=>$products,'cart'=>$getJsonEditVehicle->cartShow);
  echo json_encode($product);      

    }

}