<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <div class="btn-group pull-right m-t-15">
                                    <button type="button" class="btn btn-default btn-sm m-l-5" data-toggle="modal" data-target="#addLanguageMaster"> <i class="fa fa-plus m-r-5"></i>Add language</button>
                                </div>
                                <h4 class="page-title">Language</h4>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-sm-12">
                        <?php
                        if($this->session->flashdata('success'))
                        {
                            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                        }
                        if($this->session->flashdata('error'))
                        {
                            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                        }
                        ?>
                        </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="card-box table-responsive">
                              <table class="table table-striped table-bordered table_shop_custom">
                                <thead>
                                  <tr>
                                    <th>Language Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <?php 
                                foreach($languageList as $languageListData)
                                {
                                 ?>
                                  <tr>
                                    <td><?php echo $languageListData->houdin_language_name_value."(".$languageListData->houdin_language_name.")" ?></td>
                                    <td>
                                    <?php 
                                    if($languageListData->houdin_language_status == 'active')
                                    {
                                        $setText =  'Active';
                                        $setClass = "success";
                                    }
                                    else
                                    {
                                        $setText =  'Deactive';
                                        $setClass = "warning";
                                    }
                                    ?>
                                    <button type="button" class="btn btn-<?php echo $setClass; ?> btn btn-xs"><?php echo $setText ?></button></td>
                                    <td>
                                        <button type="button" data-status="<?php echo $languageListData->houdin_language_status ?>" data-language="<?php echo $languageListData->houdin_language_name ?>" data-id="<?php echo $languageListData->houdin_language_id ?>" class="btn btn-warning btn btn-sm editLangaugeBtnData"><i class="fa fa-pencil m-r-5"></i>Edit</button>
                                        <button data-id="<?php echo $languageListData->houdin_language_id ?>" type="button" class="btn btn-warning btn btn-sm deleleLanguageBtnData"><i class="fa fa-trash m-r-5"></i>Delete</button>
                                    </td>
                                  </tr>   
                                <?php }
                                ?>
                                </tbody>
                              </table>
                              <ul class="pagination pull-right"> <?php echo $this->pagination->create_links(); ?> </ul>
                            </div>
                          </div>
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->

<div id="addLanguageMaster" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Setting/language' ),array( 'id' => 'addLangugaeData','class'=>'m-t-30','method'=>'post'));?>
              <div class="modal-content">  
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 class="modal-title">Add language</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                      <label for="field-7" class="control-label">Select language</label>
                                      <input type="hidden" class="languageNameData" name="languageNameData"/>
                                      <select class="form-control required_validation_for_add_langugae changeLangaugeData" name="selectMasterLanguage"><option value="">Select Language</option><option value="af">Afrikaans</option><option value="sq">Albanian</option><option value="am">Amharic</option><option value="ar">Arabic</option><option value="hy">Armenian</option><option value="az">Azerbaijani</option><option value="eu">Basque</option><option value="be">Belarusian</option><option value="bn">Bengali</option><option value="bs">Bosnian</option><option value="bg">Bulgarian</option><option value="ca">Catalan</option><option value="ceb">Cebuano</option><option value="ny">Chichewa</option><option value="zh-CN">Chinese (Simplified)</option><option value="zh-TW">Chinese (Traditional)</option><option value="co">Corsican</option><option value="hr">Croatian</option><option value="cs">Czech</option><option value="da">Danish</option><option value="nl">Dutch</option><option value="en">English</option><option value="eo">Esperanto</option><option value="et">Estonian</option><option value="tl">Filipino</option><option value="fi">Finnish</option><option value="fr">French</option><option value="fy">Frisian</option><option value="gl">Galician</option><option value="ka">Georgian</option><option value="de">German</option><option value="el">Greek</option><option value="gu">Gujarati</option><option value="ht">Haitian Creole</option><option value="ha">Hausa</option><option value="haw">Hawaiian</option><option value="iw">Hebrew</option><option value="hi">Hindi</option><option value="hmn">Hmong</option><option value="hu">Hungarian</option><option value="is">Icelandic</option><option value="ig">Igbo</option><option value="id">Indonesian</option><option value="ga">Irish</option><option value="it">Italian</option><option value="ja">Japanese</option><option value="jw">Javanese</option><option value="kn">Kannada</option><option value="kk">Kazakh</option><option value="km">Khmer</option><option value="ko">Korean</option><option value="ku">Kurdish (Kurmanji)</option><option value="ky">Kyrgyz</option><option value="lo">Lao</option><option value="la">Latin</option><option value="lv">Latvian</option><option value="lt">Lithuanian</option><option value="lb">Luxembourgish</option><option value="mk">Macedonian</option><option value="mg">Malagasy</option><option value="ms">Malay</option><option value="ml">Malayalam</option><option value="mt">Maltese</option><option value="mi">Maori</option><option value="mr">Marathi</option><option value="mn">Mongolian</option><option value="my">Myanmar (Burmese)</option><option value="ne">Nepali</option><option value="no">Norwegian</option><option value="ps">Pashto</option><option value="fa">Persian</option><option value="pl">Polish</option><option value="pt">Portuguese</option><option value="pa">Punjabi</option><option value="ro">Romanian</option><option value="ru">Russian</option><option value="sm">Samoan</option><option value="gd">Scots Gaelic</option><option value="sr">Serbian</option><option value="st">Sesotho</option><option value="sn">Shona</option><option value="sd">Sindhi</option><option value="si">Sinhala</option><option value="sk">Slovak</option><option value="sl">Slovenian</option><option value="so">Somali</option><option value="es">Spanish</option><option value="su">Sundanese</option><option value="sw">Swahili</option><option value="sv">Swedish</option><option value="tg">Tajik</option><option value="ta">Tamil</option><option value="te">Telugu</option><option value="th">Thai</option><option value="tr">Turkish</option><option value="uk">Ukrainian</option><option value="ur">Urdu</option><option value="uz">Uzbek</option><option value="vi">Vietnamese</option><option value="cy">Welsh</option><option value="xh">Xhosa</option><option value="yi">Yiddish</option><option value="yo">Yoruba</option><option value="zu">Zulu</option></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <label for="field-7" class="control-label">Select Status</label>
                                        <select class="form-control required_validation_for_add_langugae" name="slectMasterLanguageStatus">
                                              <option value="">Choose Status</option>
                                              <option value="active">Active</option>
                                              <option value="deactive">Deactive</option>
                                        </select>
                                </div>
                            </div>
                      </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                  <input type="submit" class="btn btn-info" name="addLanguageBtn" value="Submit">
                </div>
          </div>
          <?php echo form_close();?>
    </div>
</div>
<div id="editLanguageData" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <?php echo form_open(base_url( 'Setting/language' ),array( 'id' => 'editLangugaeData','class'=>'m-t-30','method'=>'post'));?>
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                  <h4 class="modal-title">Edit language</h4>
          </div> 
          <div class="modal-body">
              <div class="row">
                  <div class="col-md-12">
                     <div class="form-group no-margin">
                        <label for="field-7" class="control-label">Select language</label>
                        <input type="hidden" class="languageNameData" name="languageNameData"/>
                        <input type="hidden" class="editLanguageId" name="editLanguageId"/>
                        <select class="form-control required_validation_for_edit_langugae changeLangaugeData editLanguageName" name="editLanguageName"><option value="">Select Language</option><option value="af">Afrikaans</option><option value="sq">Albanian</option><option value="am">Amharic</option><option value="ar">Arabic</option><option value="hy">Armenian</option><option value="az">Azerbaijani</option><option value="eu">Basque</option><option value="be">Belarusian</option><option value="bn">Bengali</option><option value="bs">Bosnian</option><option value="bg">Bulgarian</option><option value="ca">Catalan</option><option value="ceb">Cebuano</option><option value="ny">Chichewa</option><option value="zh-CN">Chinese (Simplified)</option><option value="zh-TW">Chinese (Traditional)</option><option value="co">Corsican</option><option value="hr">Croatian</option><option value="cs">Czech</option><option value="da">Danish</option><option value="nl">Dutch</option><option value="en">English</option><option value="eo">Esperanto</option><option value="et">Estonian</option><option value="tl">Filipino</option><option value="fi">Finnish</option><option value="fr">French</option><option value="fy">Frisian</option><option value="gl">Galician</option><option value="ka">Georgian</option><option value="de">German</option><option value="el">Greek</option><option value="gu">Gujarati</option><option value="ht">Haitian Creole</option><option value="ha">Hausa</option><option value="haw">Hawaiian</option><option value="iw">Hebrew</option><option value="hi">Hindi</option><option value="hmn">Hmong</option><option value="hu">Hungarian</option><option value="is">Icelandic</option><option value="ig">Igbo</option><option value="id">Indonesian</option><option value="ga">Irish</option><option value="it">Italian</option><option value="ja">Japanese</option><option value="jw">Javanese</option><option value="kn">Kannada</option><option value="kk">Kazakh</option><option value="km">Khmer</option><option value="ko">Korean</option><option value="ku">Kurdish (Kurmanji)</option><option value="ky">Kyrgyz</option><option value="lo">Lao</option><option value="la">Latin</option><option value="lv">Latvian</option><option value="lt">Lithuanian</option><option value="lb">Luxembourgish</option><option value="mk">Macedonian</option><option value="mg">Malagasy</option><option value="ms">Malay</option><option value="ml">Malayalam</option><option value="mt">Maltese</option><option value="mi">Maori</option><option value="mr">Marathi</option><option value="mn">Mongolian</option><option value="my">Myanmar (Burmese)</option><option value="ne">Nepali</option><option value="no">Norwegian</option><option value="ps">Pashto</option><option value="fa">Persian</option><option value="pl">Polish</option><option value="pt">Portuguese</option><option value="pa">Punjabi</option><option value="ro">Romanian</option><option value="ru">Russian</option><option value="sm">Samoan</option><option value="gd">Scots Gaelic</option><option value="sr">Serbian</option><option value="st">Sesotho</option><option value="sn">Shona</option><option value="sd">Sindhi</option><option value="si">Sinhala</option><option value="sk">Slovak</option><option value="sl">Slovenian</option><option value="so">Somali</option><option value="es">Spanish</option><option value="su">Sundanese</option><option value="sw">Swahili</option><option value="sv">Swedish</option><option value="tg">Tajik</option><option value="ta">Tamil</option><option value="te">Telugu</option><option value="th">Thai</option><option value="tr">Turkish</option><option value="uk">Ukrainian</option><option value="ur">Urdu</option><option value="uz">Uzbek</option><option value="vi">Vietnamese</option><option value="cy">Welsh</option><option value="xh">Xhosa</option><option value="yi">Yiddish</option><option value="yo">Yoruba</option><option value="zu">Zulu</option></select>
                      </div>
                    </div>
            </div>
                  <div class="row">
                          <div class="col-md-12">
                          <div class="form-group no-margin">
                              <label for="field-7" class="control-label">Select Status</label>
                              <select class="form-control required_validation_for_edit_langugae editLangugaeStatus" name="editLangugaeStatus">
                              <option value="">Choose Status</option>
                              <option value="active">Active</option>
                              <option value="deactive">Deactive</option>
                              </select>
                          </div>
                        </div>
                  </div>
         </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-info" name="editLangaugeBtn" value="Submit">
        </div>
    </div>
  </form>
 </div>
</div>


<div id="deleteLanguageModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >

<div class="modal-dialog">
<?php echo form_open(base_url( 'setting/language' ),  array( 'method' => 'post' ));?>
<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

<h4 class="modal-title">Delete Entry</h4>

</div>

<div class="modal-body">
<div class="row">
<div class="col-md-12">
<input type="hidden" class="deleteLangugaeId" name="deleteLangugaeId"/>
<h4><b>Do you really want to delete this entry ?</b></h4>
</div>
</div>
</div>

<div class="modal-footer">

<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

<input type="submit" class="btn btn-info" name="deleteLngaugeBtn" value="Delete">

</div>

</div>
<?php echo form_close(); ?>
</div>

</div>
<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addLangugaeData',function(){
			var check_required_field='';
			$(".required_validation_for_add_langugae").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>
    <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#editLangugaeData',function(){
			var check_required_field='';
			$(".required_validation_for_edit_langugae").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>