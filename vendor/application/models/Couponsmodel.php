<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Couponsmodel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
    }
    public function fetchBasicCouponsDetails()
    {
        // fetch product list
        $this->db->select('houdin_products_id,houdin_products_title')->from('houdinv_products')->where('houdin_products_status','active');
        $getProductList = $this->db->get()->result();
        // fetch product category
        $this->db->select('houdinv_category_id,houdinv_category_name')->from('houdinv_categories')->where('houdinv_category_status','active');
        $getCategoryList = $this->db->get()->result();
        // fetch customer list
        $this->db->select('houdinv_user_id,houdinv_user_name')->from('houdinv_users')->where('houdinv_user_is_active','active');
        $getCustomerList = $this->db->get()->result();
        // fethch Customer Group
        $this->db->select('houdinv_users_group_id,houdinv_users_group_name')->from('houdinv_users_group')->where('houdinv_users_group_status','active');
        $getCustomerGroupList = $this->db->get()->result();
        return array('productList'=>$getProductList,'categoryList'=>$getCategoryList,'customerList'=>$getCustomerList,'customerGroupLits'=>$getCustomerGroupList);
    }
    public function insertCouponData($data,$couponsData)
    {
        $getInsertData = $this->db->insert('houdinv_coupons',$data);
        $getCoupinId = $this->db->insert_id();
        if($getInsertData == 1)
        {
            if(count($couponsData['userList']) > 0)
            {
                for($index = 0; $index < count($couponsData['userList']); $index++)
                {
                    $setInsertCustomer = array('houdinv_coupons_procus_coupon_id'=>$getCoupinId,'houdinv_coupons_procus_type'=>'customer','houdinv_coupons_procus_user_id'=>$couponsData['userList'][$index],'houdinv_coupons_procus_product_id'=>0);
                    $this->db->insert('houdinv_coupons_procus',$setInsertCustomer);
                }
            }
            if(count($couponsData['productList']) > 0)
            {
                for($index = 0; $index < count($couponsData['productList']); $index++)
                {
                    $setInsertProduct = array('houdinv_coupons_procus_coupon_id'=>$getCoupinId,'houdinv_coupons_procus_type'=>'product','houdinv_coupons_procus_user_id'=>0,'houdinv_coupons_procus_product_id'=>$couponsData['productList'][$index]);
                    $this->db->insert('houdinv_coupons_procus',$setInsertProduct);
                }
            }
            if(count($couponsData['userGroup']) > 0)
            {
                $this->db->select('houdinv_users_group__user_group_id,houdinv_users_group_users_id')->from('houdinv_users_group_users_list')->where_in('houdinv_users_group__user_group_id',$couponsData['userGroup']);
                $getUserList = $this->db->get()->result();
                foreach($getUserList as $getUserListData)
                {
                    $setUserInsertArray = array('houdinv_coupons_procus_coupon_id'=>$getCoupinId,'houdinv_coupons_procus_type'=>'customer','houdinv_coupons_procus_user_id'=>$getUserListData->houdinv_users_group_users_id,'houdinv_coupons_procus_product_id'=>0,'houdinv_coupons_procus_user_group'=>$getUserListData->houdinv_users_group__user_group_id);
                    $this->db->insert('houdinv_coupons_procus',$setUserInsertArray);
                }
            }
            if(count($couponsData['productCategory']) > 0)
            {
                $this->db->select('houdin_products_category,houdin_products_id')->from('houdinv_products')->where_in('houdin_products_category',$couponsData['productCategory']);
                $getProdctList = $this->db->get()->result();
                foreach($getProdctList as $getProdctListData)
                {
                    $setUserInsertArray = array('houdinv_coupons_procus_coupon_id'=>$getCoupinId,'houdinv_coupons_procus_type'=>'product','houdinv_coupons_procus_user_id'=>0,'houdinv_coupons_procus_product_id'=>$getProdctListData->houdin_products_id,'houdinv_coupons_procus_produdct_category'=>$getProdctListData->houdin_products_category);
                    $this->db->insert('houdinv_coupons_procus',$setUserInsertArray);
                }
            }
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function updateCouponData($updateData,$couponsData,$id)
    {
        $this->db->where('houdinv_coupons_id',$id);
        $getCouponUpdate = $this->db->update('houdinv_coupons',$updateData);
        if($getCouponUpdate == 1)
        {
            $getCoupinId = $id;
            $this->db->where('houdinv_coupons_procus_coupon_id',$id);
            $this->db->delete('houdinv_coupons_procus');
            if(count($couponsData['userList']) > 0)
            {
                for($index = 0; $index < count($couponsData['userList']); $index++)
                {
                    $setInsertCustomer = array('houdinv_coupons_procus_coupon_id'=>$getCoupinId,'houdinv_coupons_procus_type'=>'customer','houdinv_coupons_procus_user_id'=>$couponsData['userList'][$index],'houdinv_coupons_procus_product_id'=>0);
                    $this->db->insert('houdinv_coupons_procus',$setInsertCustomer);
                }
            }
            if(count($couponsData['productList']) > 0)
            {
                for($index = 0; $index < count($couponsData['productList']); $index++)
                {
                    $setInsertProduct = array('houdinv_coupons_procus_coupon_id'=>$getCoupinId,'houdinv_coupons_procus_type'=>'product','houdinv_coupons_procus_user_id'=>0,'houdinv_coupons_procus_product_id'=>$couponsData['productList'][$index]);
                    $this->db->insert('houdinv_coupons_procus',$setInsertProduct);
                }
            }
            if(count($couponsData['userGroup']) > 0)
            {
                $this->db->select('houdinv_users_group__user_group_id,houdinv_users_group_users_id')->from('houdinv_users_group_users_list')->where_in('houdinv_users_group__user_group_id',$couponsData['userGroup']);
                $getUserList = $this->db->get()->result();
                foreach($getUserList as $getUserListData)
                {
                    $setUserInsertArray = array('houdinv_coupons_procus_coupon_id'=>$getCoupinId,'houdinv_coupons_procus_type'=>'customer','houdinv_coupons_procus_user_id'=>$getUserListData->houdinv_users_group_users_id,'houdinv_coupons_procus_product_id'=>0,'houdinv_coupons_procus_user_group'=>$getUserListData->houdinv_users_group__user_group_id);
                    $this->db->insert('houdinv_coupons_procus',$setUserInsertArray);
                }
            }
            if(count($couponsData['productCategory']) > 0)
            {
                $this->db->select('houdin_products_category,houdin_products_id')->from('houdinv_products')->where_in('houdin_products_category',$couponsData['productCategory']);
                $getProdctList = $this->db->get()->result();
                foreach($getProdctList as $getProdctListData)
                {
                    $setUserInsertArray = array('houdinv_coupons_procus_coupon_id'=>$getCoupinId,'houdinv_coupons_procus_type'=>'product','houdinv_coupons_procus_user_id'=>0,'houdinv_coupons_procus_product_id'=>$getProdctListData->houdin_products_id,'houdinv_coupons_procus_produdct_category'=>$getProdctListData->houdin_products_category);
                    $this->db->insert('houdinv_coupons_procus',$setUserInsertArray);
                }
            }
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function getCouponsTotalCount()
    {
        $this->db->select('COUNT(houdinv_coupons_id) AS TotalCoupons')->from('houdinv_coupons');
        $getTotalCouponCount = $this->db->get()->result();
        return array('totalRows'=>$getTotalCouponCount[0]->TotalCoupons);
    }
    public function fetchCouponsData($data)
    {
        $this->db->select('*')->from('houdinv_coupons');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getCouponsData = $this->db->get();
        $resultCouponsdata = ($getCouponsData->num_rows() > 0)?$getCouponsData->result():FALSE;
        // get total coupons
        $this->db->select('COUNT(houdinv_coupons_id) AS totalCoupons')->from('houdinv_coupons');
        $getTotalCoupons = $this->db->get()->result();
        // get total Active coupons
        $this->db->select('COUNT(houdinv_coupons_id) AS totalActiveCoupons')->from('houdinv_coupons')->where('houdinv_coupons_status','active');
        $getTotalActiveCoupons = $this->db->get()->result();
        // get total coupons
        $this->db->select('COUNT(houdinv_coupons_id) AS totalDeactiveCoupons')->from('houdinv_coupons')->where('houdinv_coupons_status','deactive');
        $getTotalDeactiveCoupons = $this->db->get()->result();
        return array('couponsLsit'=>$resultCouponsdata,'totalCouponsData'=>$getTotalCoupons[0]->totalCoupons,'totalActiveCoupons'=>$getTotalActiveCoupons[0]->totalActiveCoupons,'totalDeactiveCoupons'=>$getTotalDeactiveCoupons[0]->totalDeactiveCoupons);
    }
    public function deleteCouponData($data)
    {
        $expldoeData = explode(',',$data);
        $this->db->where_in('houdinv_coupons_id',$expldoeData);
        $getDeleteStatus = $this->db->delete('houdinv_coupons');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function fetchCouponsPdfData()
    {
        $this->db->select('*')->from('houdinv_coupons');
        $resultCouponsdata = $this->db->get()->result();
        // get total coupons
        $this->db->select('COUNT(houdinv_coupons_id) AS totalCoupons')->from('houdinv_coupons');
        $getTotalCoupons = $this->db->get()->result();
        // get total Active coupons
        $this->db->select('COUNT(houdinv_coupons_id) AS totalActiveCoupons')->from('houdinv_coupons')->where('houdinv_coupons_status','active');
        $getTotalActiveCoupons = $this->db->get()->result();
        // get total coupons
        $this->db->select('COUNT(houdinv_coupons_id) AS totalDeactiveCoupons')->from('houdinv_coupons')->where('houdinv_coupons_status','deactive');
        $getTotalDeactiveCoupons = $this->db->get()->result();
        return array('couponsLsit'=>$resultCouponsdata,'totalCouponsData'=>$getTotalCoupons[0]->totalCoupons,'totalActiveCoupons'=>$getTotalActiveCoupons[0]->totalActiveCoupons,'totalDeactiveCoupons'=>$getTotalDeactiveCoupons[0]->totalDeactiveCoupons);
    }
    public function fetchCouponsDetails($data)
    {
        // fetch product list
        $this->db->select('houdin_products_id,houdin_products_title')->from('houdinv_products')->where('houdin_products_status','active');
        $getProductList = $this->db->get()->result();
        // fetch product category
        $this->db->select('houdinv_category_id,houdinv_category_name')->from('houdinv_categories')->where('houdinv_category_status','active');
        $getCategoryList = $this->db->get()->result();
        // fetch customer list
        $this->db->select('houdinv_user_id,houdinv_user_name')->from('houdinv_users')->where('houdinv_user_is_active','active');
        $getCustomerList = $this->db->get()->result();
        // fethch Customer Group
        $this->db->select('houdinv_users_group_id,houdinv_users_group_name')->from('houdinv_users_group')->where('houdinv_users_group_status','active');
        $getCustomerGroupList = $this->db->get()->result();
        $getBasicDetail = $this->db->select('*')->from('houdinv_coupons')->where('houdinv_coupons_id',$data)->get()->result();
        $getCustomerDetails = $this->db->select('houdinv_coupons_procus_user_id')->from('houdinv_coupons_procus')->where('houdinv_coupons_procus_type','customer')->where('houdinv_coupons_procus_coupon_id',$data)->where('houdinv_coupons_procus_user_group',0)->get()->result();
        $getCustomerGroupDetails = $this->db->select('houdinv_coupons_procus_user_group')->from('houdinv_coupons_procus')->where('houdinv_coupons_procus_type','customer')->where('houdinv_coupons_procus_coupon_id',$data)->where('houdinv_coupons_procus_user_group !=',0)->get()->result();
        $getProductListData = $this->db->select('houdinv_coupons_procus_product_id')->from('houdinv_coupons_procus')->where('houdinv_coupons_procus_produdct_category',0)->where('houdinv_coupons_procus_type','product')->where('houdinv_coupons_procus_coupon_id',$data)->get()->result();
        $getProductCategory = $this->db->select('houdinv_coupons_procus_produdct_category')->from('houdinv_coupons_procus')->where('houdinv_coupons_procus_produdct_category !=',0)->where('houdinv_coupons_procus_type','product')->where('houdinv_coupons_procus_coupon_id',$data)->get()->result();
        return array('basicDetail'=>$getBasicDetail,'customerDetails'=>$getCustomerDetails,'customerGroup'=>$getCustomerGroupDetails,'productDetails'=>$getProductListData,'prodictCategory'=>$getProductCategory,'productList'=>$getProductList,'categoryList'=>$getCategoryList,'customerList'=>$getCustomerList,'customerGroupLits'=>$getCustomerGroupList);
    }

    public function fetchmembership()
    {
        $this->db->select('*')->from('houdinv_users')->where(array('houdinv_user_is_active'=>'active'));
   $houdinv_users=$this->db->get()->result();
 $this->db->select('*')->from('houdinv_viewmembership');
   $viewmembership=$this->db->get()->result();
   $this->db->select('count(*) as active')->from('houdinv_viewmembership')->where(array('status'=>1));
   $count_active=$this->db->get()->result();
   $this->db->select('count(*) as deactive')->from('houdinv_viewmembership')->where(array('status !='=>1));
   $count_deactive=$this->db->get()->result();

   $this->db->select('count(*) as total')->from('houdinv_viewmembership');
   $count_total=$this->db->get()->result();


 return array('List_users'=>$houdinv_users,'viewmembership'=>$viewmembership,'count_active'=>$count_active[0],'count_deactive'=>$count_deactive[0],'count_total'=>$count_total[0]);
    }
 
  public function viewmembership_save($value)
 {
    $getInsertData = $this->db->insert('houdinv_viewmembership',$value); 
 if($getInsertData == 1)
            { return ture;}else{return false;}}


public function viewmembership_update($value)
{                     $this->db->where('id',$value['id']);
    $getInsertData = $this->db->update('houdinv_viewmembership',$value); 
 if($getInsertData == 1)
            { return ture;}else{return false;} 
    }
 public function viewmembership_delete($value)
 {
                       $this->db->where($value);
     $getInsertData = $this->db->delete('houdinv_viewmembership');

 if($getInsertData == 1)
            { return ture;}else{return false;} 
 }

  }   
