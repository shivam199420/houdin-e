<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopregistermodel extends CI_Model{
    function __construct() {
        $this->userTbl = '';
    }
    public function insertFirstProcess($data)
    {
        $this->db->select('houdin_user_contact')->from('houdin_users')->where('houdin_user_contact',$data['phone']);
        $getUserCount = $this->db->get()->result();
        if(!count($getUserCount) > 0)
        {
            $setDate = strtotime(date('Y-m-d h:i:s'));
            $getExpiryDate = date('Y-m-d', strtotime('+20 days', strtotime(date('Y-m-d'))));
            $setInsertArray = array('houdin_user_name'=>$data['name'],'houdin_user_contact'=>$data['phone'],'houdin_user_registration_status'=>'0',
            'houdin_user_is_verified'=>'0','houdin_user_is_active'=>'active','houdin_user_created_at'=>$setDate,'houdin_users_package_id'=>'0','houdin_users_package_expiry'=>$getExpiryDate);
            $this->db->insert('houdin_users',$setInsertArray);
            $getId = $this->db->insert_id();
            if($getId)
            {
                $randomString = rand(1000,99999);
                // insert user auth data
                $setArray = array('houdin_user_second_auth_user_id'=>$getId,'houdin_user_second_auth_token'=>$randomString);
                $this->db->insert('houdin_user_second_auth',$setArray);
                return array('message'=>'success','id'=>$getId,'randomString'=>$randomString);
            }
            else
            {
                return array('message'=>'no');
            }
        }
        else
        {
            return array('message'=>'phone');
        }
    }
    public function checkUserOtpData($data)
    {
        $this->db->select('*')->from('houdin_user_second_auth')->where('houdin_user_second_auth_token',$data);
        $getOtpData = $this->db->get()->result();
        if(count($getOtpData) > 0)
        {
            $getUserId = $getOtpData[0]->houdin_user_second_auth_user_id;
            $setDate = strtotime(date('Y-m-d h:i:s'));
            $setUpdatedArray = array('houdin_user_is_verified'=>'1','houdin_user_updated_at'=>$setDate);
            $this->db->where('houdin_user_id',$this->session->userdata('tempId'));
            $getUpdateStatus = $this->db->update('houdin_users',$setUpdatedArray);
            if($getUpdateStatus == 1)
            {
                $this->db->where('houdin_user_second_auth_id',$getOtpData[0]->houdin_user_second_auth_id);
                $this->db->delete('houdin_user_second_auth');
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // update user phone
    public function updateUserPhone($data)
    {
        $this->db->select('houdin_user_contact')->from('houdin_users')->where('houdin_user_contact',$data['phone']);
        $getPhoneData = $this->db->get()->result();
        if(!count($getPhoneData) > 0)
        {
            $setUpdateData = array('houdin_user_contact'=>$data['phone']);
            $this->db->where('houdin_user_id',$data['userId']);
            $getUpdateStatus = $this->db->update('houdin_users',$setUpdateData);
            if($getUpdateStatus == 1)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }
        }
        else
        {
            return array('message'=>'phone');
        }
    }
    // get complete registration data
    public function getExtraData()
    {
        // get business category
        $this->db->select('*')->from('houdin_business_categories')->where('houdin_business_categories_status','active');
        $getBusinessCategory = $this->db->get()->result();
        // get country list
        $this->db->select('*')->from('houdin_countries')->where('houdin_country_status','active');
        $getCountryList = $this->db->get()->result();
        // get langugae list
        $getLanguageData = $this->db->select('houdin_language_id,houdin_language_name_value')->from('houdin_languages')->where('houdin_language_status','active')->get()->result();
        return array('businessCategory'=>$getBusinessCategory,'countryList'=>$getCountryList,'langugae'=>$getLanguageData);
    }
    // update shop detail
    public function updateShopDetail($data)
    {
        $this->db->select('houdin_user_shop_shop_name')->from('houdin_user_shops')->where('houdin_user_shop_shop_name',$data['storeName']);
        $getShopDetails = $this->db->get()->result();
        if(!count($getShopDetails) > 0)
        {
            $getShopPassword = $data['password'];
            $letters1='abcdefghijklmnopqrstuvwxyz'; 
            $string1=''; 
            for($x=0; $x<3; ++$x)
            {  
                $string1.=$letters1[rand(0,25)].rand(0,9); 
            }
            $saltdata = password_hash($string1,PASSWORD_DEFAULT);
            $pass = crypt($getShopPassword,$saltdata);
            $setDate = strtotime(date('Y-m-d h:i:s'));
            // update houdin_user table details
            $insertUserDetails = array('houdin_user_email'=>$data['email'],'houdin_user_password'=>$pass,'houdin_user_password_salt'=>$saltdata,'houdin_user_address'=>$data['address'],'houdin_user_city'=>$data['City'],'houdin_user_country'=>$data['country'],'houdin_user_registration_status'=>'1','houdin_user_updated_at'=>$setDate,'houdin_users_currency'=>$data['currency']);
            $this->db->where('houdin_user_id',$data['userId']);
            $getUserUpdateStatus = $this->db->update('houdin_users',$insertUserDetails);
            if($getUserUpdateStatus == 1)
            {
                // update shop details
                $setDbName = "houdin_".$data['storeName']."_db";
                $shopInsertArray = array('houdin_user_shop_user_id'=>$data['userId'],'houdin_user_shop_shop_name'=>$data['storeName'],
                'houdin_user_shop_country'=>$data['country'],'houdin_user_shop_db_name'=>$setDbName,'houdin_user_shop_active_status'=>'active',
                'houdin_user_shop_created_at'=>$setDate,'houdin_user_shops_category'=>$data['category'],'houdin_user_shops_language'=>$data['businessLanguage']);
                $getInsertStatus = $this->db->insert('houdin_user_shops',$shopInsertArray);
                if($getInsertStatus == 1)
                {
                    $this->fetchDynamicPackage($setDbName);
                 

                    return array('message'=>'yes');
                }
                else
                {
                    return array('message'=>'noshop');
                }
            }
            else
            {
                return array('message'=>'no');
            }
        }
        else
        {
            return array('message'=>'shop');
        }
    }
    public function upgradeUserPackage($user,$transaction,$id)
    {
        $this->db->where('houdin_user_id',$id);
        $getUpdateStatus = $this->db->update('houdin_users',$user);
        if($getUpdateStatus)
        {
            $getTransaction = $this->db->insert('houdin_admin_transaction',$transaction);
            if($getTransaction)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'trans');    
            }
        }
        else
        {
            return array('message'=>'user');
        }
    }





    public function fetchDynamicPackage($setDbName)
    {
        $this->load->dbforge();
        if ($this->dbforge->create_database($setDbName)) {
            $getQuery = $this->db->query("GRANT ALL ON ".$setDbName.".* TO 'hawksind_houdinv'@'localhost'");
            if ($getQuery) {
                $this->load->helper('createdb_helper');
                $getNewDatabase = createDynamicDatabase($setDbName);
                $getDB = $this->load->database($getNewDatabase, true);

                $getDB->query("CREATE TABLE cities (city_id int(11) NOT NULL,city_name varchar(30) COLLATE utf8_unicode_ci NOT NULL,state_id int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
               // $getDB->query("CREATE TABLE houdinv_coupons (houdinv_coupons_id int(11) NOT NULL,houdinv_coupons_name varchar(50) NOT NULL,houdinv_coupons_tagline varchar(50) NOT NULL,houdinv_coupons_valid_from date NOT NULL,houdinv_coupons_valid_to date NOT NULL,houdinv_coupons_order_amount int(11) NOT NULL,houdinv_coupons_code varchar(50) NOT NULL,houdinv_coupons_discount_precentage varchar(20) NOT NULL,houdinv_coupons_limit int(11) NOT NULL,houdinv_coupons_payment_method enum('cod','online payment','both') NOT NULL,houdinv_coupons_status enum('active','deactive') NOT NULL,houdinv_coupons_applicable enum('app','both') NOT NULL,houdinv_coupons_created_at varchar(30) NOT NULL,houdinv_coupons_modified_at varchar(30) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1");
                $getDB->query("INSERT INTO cities ( city_id ,  city_name ,  state_id ) VALUES
    (1652, 'South Hill', 240),
    (1653, 'The Valley', 240),
    (1654, 'Oranjestad', 240),
    (1655, 'Douglas', 240),
    (1656, 'Gibraltar', 240),
    (1657, 'Tamuning', 240),
    (1658, 'AgaÃ±a', 240),
    (1659, 'Flying Fish Cove', 240),
    (1660, 'Monte-Carlo', 240),
    (1661, 'Monaco-Ville', 240),
    (1662, 'Yangor', 240),
    (1663, 'Yaren', 240),
    (1664, 'Alofi', 240),
    (1665, 'Kingston', 240),
    (1666, 'Adamstown', 240),
    (1667, 'Singapore', 240),
    (1668, 'NoumÃ©a', 240),
    (1669, 'CittÃ  del Vaticano', 240),
    (1670, 'Mazar-e-Sharif', 241),
    (1671, 'Herat', 242),
    (1672, 'Kabul', 243),
    (1673, 'Qandahar', 244),
    (1674, 'Lobito', 245),
    (1675, 'Benguela', 245),
    (1676, 'Huambo', 246),
    (1677, 'Luanda', 247),
    (1678, 'Namibe', 248),
    (1679, 'South Hill', 249),
    (1680, 'The Valley', 249),
    (1681, 'Oranjestad', 249),
    (1682, 'Douglas', 249),
    (1683, 'Gibraltar', 249),
    (1684, 'Tamuning', 249),
    (1685, 'AgaÃ±a', 249),
    (1686, 'Flying Fish Cove', 249),
    (1687, 'Monte-Carlo', 249),
    (1688, 'Monaco-Ville', 249),
    (1689, 'Yangor', 249),
    (1690, 'Yaren', 249),
    (1691, 'Alofi', 249),
    (1692, 'Kingston', 249),
    (1693, 'Adamstown', 249),
    (1694, 'Singapore', 249),
    (1695, 'NoumÃ©a', 249),
    (1696, 'CittÃ  del Vaticano', 249),
    (1697, 'Tirana', 250),
    (1698, 'Andorra la Vella', 251),
    (1699, 'Willemstad', 252),
    (1700, 'Abu Dhabi', 253),
    (1701, 'al-Ayn', 253),
    (1702, 'Ajman', 254),
    (1703, 'Dubai', 255),
    (1704, 'Sharja', 256),
    (1705, 'La Matanza', 257),
    (1706, 'Lomas de Zamora', 257),
    (1707, 'Quilmes', 257),
    (1708, 'Almirante Brown', 257),
    (1709, 'La Plata', 257),
    (1710, 'Mar del Plata', 257),
    (1711, 'LanÃºs', 257),
    (1712, 'Merlo', 257),
    (1713, 'General San MartÃ­n', 257),
    (1714, 'Moreno', 257),
    (1715, 'Avellaneda', 257),
    (1716, 'Tres de Febrero', 257),
    (1717, 'MorÃ³n', 257),
    (1718, 'Florencio Varela', 257),
    (1719, 'San Isidro', 257),
    (1720, 'Tigre', 257),
    (1721, 'Malvinas Argentinas', 257),
    (1722, 'Vicente LÃ³pez', 257),
    (1723, 'Berazategui', 257),
    (1724, 'San Miguel', 257),
    (1725, 'BahÃ­a Blanca', 257),
    (1726, 'Esteban EcheverrÃ­a', 257),
    (1727, 'JosÃ© C. Paz', 257),
    (1728, 'Hurlingham', 257),
    (1729, 'ItuzaingÃ³', 257),
    (1730, 'San Fernando', 257),
    (1731, 'San NicolÃ¡s de los Arroyos', 257),
    (1732, 'Escobar', 257),
    (1733, 'Pilar', 257),
    (1734, 'Ezeiza', 257),
    (1735, 'Tandil', 257),
    (1736, 'San Fernando del Valle de Cata', 258),
    (1737, 'CÃ³rdoba', 259),
    (1738, 'RÃ­o Cuarto', 259),
    (1739, 'MonterÃ­a', 259),
    (1740, 'Resistencia', 260),
    (1741, 'Comodoro Rivadavia', 261),
    (1742, 'Corrientes', 262),
    (1743, 'Buenos Aires', 263),
    (1744, 'BrasÃ­lia', 263),
    (1745, 'Ciudad de MÃ©xico', 263),
    (1746, 'Caracas', 263),
    (1747, 'Catia La Mar', 263),
    (1748, 'ParanÃ¡', 264),
    (1749, 'Concordia', 264),
    (1750, 'Formosa', 265),
    (1751, 'San Salvador de Jujuy', 266),
    (1752, 'La Rioja', 267),
    (1753, 'LogroÃ±o', 267),
    (1754, 'Godoy Cruz', 268),
    (1755, 'GuaymallÃ©n', 268),
    (1756, 'Las Heras', 268),
    (1757, 'Mendoza', 268),
    (1758, 'San Rafael', 268),
    (1759, 'Posadas', 269),
    (1760, 'NeuquÃ©n', 270),
    (1761, 'Salta', 271),
    (1762, 'San Juan', 272),
    (1763, 'San Juan', 272),
    (1764, 'San Luis', 273),
    (1765, 'Rosario', 274),
    (1766, 'Santa FÃ©', 274),
    (1767, 'Santiago del Estero', 275),
    (1768, 'San Miguel de TucumÃ¡n', 276),
    (1769, 'Vanadzor', 277),
    (1770, 'Yerevan', 278),
    (1771, 'Gjumri', 279),
    (1772, 'Tafuna', 280),
    (1773, 'Fagatogo', 280),
    (1774, 'Saint JohnÂ´s', 281),
    (1775, 'Canberra', 282),
    (1776, 'Sydney', 283),
    (1777, 'Newcastle', 283),
    (1778, 'Central Coast', 283),
    (1779, 'Wollongong', 283),
    (1780, 'Brisbane', 284),
    (1781, 'Gold Coast', 284),
    (1782, 'Townsville', 284),
    (1783, 'Cairns', 284),
    (1784, 'Adelaide', 285),
    (1785, 'Hobart', 286),
    (1786, 'Melbourne', 287),
    (1787, 'Geelong', 287),
    (1788, 'Perth', 288),
    (1789, 'Klagenfurt', 289),
    (1790, 'Linz', 290),
    (1791, 'Salzburg', 291),
    (1792, 'Graz', 292),
    (1793, 'Innsbruck', 293),
    (1794, 'Wien', 294),
    (1795, 'Baku', 295),
    (1796, 'GÃ¤ncÃ¤', 296),
    (1797, 'MingÃ¤Ã§evir', 297),
    (1798, 'Sumqayit', 298),
    (1799, 'Bujumbura', 299),
    (1800, 'Antwerpen', 300),
    (1801, 'Bruxelles [Brussel]', 301),
    (1802, 'Schaerbeek', 301),
    (1803, 'Gent', 302),
    (1804, 'Charleroi', 303),
    (1805, 'Mons', 303),
    (1806, 'LiÃ¨ge', 304),
    (1807, 'Namur', 305),
    (1808, 'Brugge', 306),
    (1809, 'Djougou', 307),
    (1810, 'Cotonou', 308),
    (1811, 'Parakou', 309),
    (1812, 'Porto-Novo', 310),
    (1813, 'Koudougou', 311),
    (1814, 'Bobo-Dioulasso', 312),
    (1815, 'Ouagadougou', 313),
    (1816, 'Barisal', 314),
    (1817, 'Chittagong', 315),
    (1818, 'Comilla', 315),
    (1819, 'Brahmanbaria', 315),
    (1820, 'Dhaka', 316),
    (1821, 'Narayanganj', 316),
    (1822, 'Mymensingh', 316),
    (1823, 'Tungi', 316),
    (1824, 'Tangail', 316),
    (1825, 'Jamalpur', 316),
    (1826, 'Narsinghdi', 316),
    (1827, 'Gazipur', 316),
    (1828, 'Khulna', 317),
    (1829, 'Jessore', 317),
    (1830, 'Rajshahi', 318),
    (1831, 'Rangpur', 318),
    (1832, 'Nawabganj', 318),
    (1833, 'Dinajpur', 318),
    (1834, 'Bogra', 318),
    (1835, 'Pabna', 318),
    (1836, 'Naogaon', 318),
    (1837, 'Sirajganj', 318),
    (1838, 'Saidpur', 318),
    (1839, 'Sylhet', 319),
    (1840, 'Burgas', 320),
    (1841, 'Sliven', 320),
    (1842, 'Sofija', 321),
    (1843, 'Stara Zagora', 322),
    (1844, 'Pleven', 323),
    (1845, 'Plovdiv', 324),
    (1846, 'Ruse', 325),
    (1847, 'Varna', 326),
    (1848, 'Dobric', 326),
    (1849, 'Å umen', 326),
    (1850, 'al-Manama', 327),
    (1851, 'Nassau', 328),
    (1852, 'Sarajevo', 329),
    (1853, 'Zenica', 329),
    (1854, 'Banja Luka', 330),
    (1855, 'Brest', 331),
    (1856, 'BaranovitÅ¡i', 331),
    (1857, 'Pinsk', 331),
    (1858, 'Gomel', 332),
    (1859, 'Mozyr', 332),
    (1860, 'Grodno', 333),
    (1861, 'Lida', 333),
    (1862, 'Minsk', 334),
    (1863, 'Borisov', 335),
    (1864, 'Soligorsk', 335),
    (1865, 'MolodetÅ¡no', 335),
    (1866, 'Mogiljov', 336),
    (1867, 'Bobruisk', 336),
    (1868, 'Vitebsk', 337),
    (1869, 'OrÅ¡a', 337),
    (1870, 'Novopolotsk', 337),
    (1871, 'Belize City', 338),
    (1872, 'Belmopan', 339),
    (1873, 'Hamilton', 340),
    (1874, 'Hamilton', 340),
    (1875, 'Saint George', 341),
    (1876, 'Sucre', 342),
    (1877, 'Cochabamba', 343),
    (1878, 'La Paz', 344),
    (1879, 'El Alto', 344),
    (1880, 'Oruro', 345),
    (1881, 'PotosÃ­', 346),
    (1882, 'Santa Cruz de la Sierra', 347),
    (1883, 'Tarija', 348),
    (1884, 'Rio Branco', 349),
    (1885, 'MaceiÃ³', 350),
    (1886, 'Arapiraca', 350),
    (1887, 'MacapÃ¡', 351),
    (1888, 'Manaus', 352),
    (1889, 'Salvador', 353),
    (1890, 'Feira de Santana', 353),
    (1891, 'IlhÃ©us', 353),
    (1892, 'VitÃ³ria da Conquista', 353),
    (1893, 'Juazeiro', 353),
    (1894, 'Itabuna', 353),
    (1895, 'JequiÃ©', 353),
    (1896, 'CamaÃ§ari', 353),
    (1897, 'Barreiras', 353),
    (1898, 'Alagoinhas', 353),
    (1899, 'Lauro de Freitas', 353),
    (1900, 'Teixeira de Freitas', 353),
    (1901, 'Paulo Afonso', 353),
    (1902, 'EunÃ¡polis', 353),
    (1903, 'Jacobina', 353),
    (1904, 'Fortaleza', 354),
    (1905, 'Caucaia', 354),
    (1906, 'Juazeiro do Norte', 354),
    (1907, 'MaracanaÃº', 354),
    (1908, 'Sobral', 354),
    (1909, 'Crato', 354),
    (1910, 'Buenos Aires', 355),
    (1911, 'BrasÃ­lia', 355),
    (1912, 'Ciudad de MÃ©xico', 355),
    (1913, 'Caracas', 355),
    (1914, 'Catia La Mar', 355),
    (1915, 'Cariacica', 356),
    (1916, 'Vila Velha', 356),
    (1917, 'Serra', 356),
    (1918, 'VitÃ³ria', 356),
    (1919, 'Cachoeiro de Itapemirim', 356),
    (1920, 'Colatina', 356),
    (1921, 'Linhares', 356),
    (1922, 'GoiÃ¢nia', 357),
    (1923, 'Aparecida de GoiÃ¢nia', 357),
    (1924, 'AnÃ¡polis', 357),
    (1925, 'LuziÃ¢nia', 357),
    (1926, 'Rio Verde', 357),
    (1927, 'Ãguas Lindas de GoiÃ¡s', 357),
    (1928, 'SÃ£o LuÃ­s', 358),
    (1929, 'Imperatriz', 358),
    (1930, 'Caxias', 358),
    (1931, 'Timon', 358),
    (1932, 'CodÃ³', 358),
    (1933, 'SÃ£o JosÃ© de Ribamar', 358),
    (1934, 'Bacabal', 358),
    (1935, 'CuiabÃ¡', 359),
    (1936, 'VÃ¡rzea Grande', 359),
    (1937, 'RondonÃ³polis', 359),
    (1938, 'Campo Grande', 360),
    (1939, 'Dourados', 360),
    (1940, 'CorumbÃ¡', 360),
    (1941, 'Belo Horizonte', 361),
    (1942, 'Contagem', 361),
    (1943, 'UberlÃ¢ndia', 361),
    (1944, 'Juiz de Fora', 361),
    (1945, 'Betim', 361),
    (1946, 'Montes Claros', 361),
    (1947, 'Uberaba', 361),
    (1948, 'RibeirÃ£o das Neves', 361),
    (1949, 'Governador Valadares', 361),
    (1950, 'Ipatinga', 361),
    (1951, 'DivinÃ³polis', 361),
    (1952, 'Sete Lagoas', 361),
    (1953, 'Santa Luzia', 361),
    (1954, 'PoÃ§os de Caldas', 361),
    (1955, 'IbiritÃ©', 361),
    (1956, 'TeÃ³filo Otoni', 361),
    (1957, 'Patos de Minas', 361),
    (1958, 'Barbacena', 361),
    (1959, 'Varginha', 361),
    (1960, 'SabarÃ¡', 361),
    (1961, 'Itabira', 361),
    (1962, 'Pouso Alegre', 361),
    (1963, 'Passos', 361),
    (1964, 'Araguari', 361),
    (1965, 'Conselheiro Lafaiete', 361),
    (1966, 'Coronel Fabriciano', 361),
    (1967, 'Ituiutaba', 361),
    (1968, 'JoÃ£o Pessoa', 362),
    (1969, 'Campina Grande', 362),
    (1970, 'Santa Rita', 362),
    (1971, 'Patos', 362),
    (1972, 'Curitiba', 363),
    (1973, 'Londrina', 363),
    (1974, 'MaringÃ¡', 363),
    (1975, 'Ponta Grossa', 363),
    (1976, 'Foz do IguaÃ§u', 363),
    (1977, 'Cascavel', 363),
    (1978, 'SÃ£o JosÃ© dos Pinhais', 363),
    (1979, 'Colombo', 363),
    (1980, 'Guarapuava', 363),
    (1981, 'ParanaguÃ¡', 363),
    (1982, 'Apucarana', 363),
    (1983, 'Toledo', 363),
    (1984, 'Pinhais', 363),
    (1985, 'Campo Largo', 363),
    (1986, 'BelÃ©m', 364),
    (1987, 'Ananindeua', 364),
    (1988, 'SantarÃ©m', 364),
    (1989, 'MarabÃ¡', 364),
    (1990, 'Castanhal', 364),
    (1991, 'Abaetetuba', 364),
    (1992, 'Itaituba', 364),
    (1993, 'CametÃ¡', 364),
    (1994, 'Recife', 365),
    (1995, 'JaboatÃ£o dos Guararapes', 365),
    (1996, 'Olinda', 365),
    (1997, 'Paulista', 365),
    (1998, 'Caruaru', 365),
    (1999, 'Petrolina', 365),
    (2000, 'Cabo de Santo Agostinho', 365),
    (2001, 'Camaragibe', 365),
    (2002, 'Garanhuns', 365),
    (2003, 'VitÃ³ria de Santo AntÃ£o', 365),
    (2004, 'SÃ£o LourenÃ§o da Mata', 365),
    (2005, 'Teresina', 366),
    (2006, 'ParnaÃ­ba', 366),
    (2007, 'Rio de Janeiro', 367),
    (2008, 'SÃ£o GonÃ§alo', 367),
    (2009, 'Nova IguaÃ§u', 367),
    (2010, 'Duque de Caxias', 367),
    (2011, 'NiterÃ³i', 367),
    (2012, 'SÃ£o JoÃ£o de Meriti', 367),
    (2013, 'Belford Roxo', 367),
    (2014, 'Campos dos Goytacazes', 367),
    (2015, 'PetrÃ³polis', 367),
    (2016, 'Volta Redonda', 367),
    (2017, 'MagÃ©', 367),
    (2018, 'ItaboraÃ­', 367),
    (2019, 'Nova Friburgo', 367),
    (2020, 'Barra Mansa', 367),
    (2021, 'NilÃ³polis', 367),
    (2022, 'TeresÃ³polis', 367),
    (2023, 'MacaÃ©', 367),
    (2024, 'Cabo Frio', 367),
    (2025, 'Queimados', 367),
    (2026, 'Resende', 367),
    (2027, 'Angra dos Reis', 367),
    (2028, 'Barra do PiraÃ­', 367),
    (2029, 'Natal', 368),
    (2030, 'MossorÃ³', 368),
    (2031, 'Parnamirim', 368),
    (2032, 'Porto Alegre', 369),
    (2033, 'Caxias do Sul', 369),
    (2034, 'Pelotas', 369),
    (2035, 'Canoas', 369),
    (2036, 'Novo Hamburgo', 369),
    (2037, 'Santa Maria', 369),
    (2038, 'GravataÃ­', 369),
    (2039, 'ViamÃ£o', 369),
    (2040, 'SÃ£o Leopoldo', 369),
    (2041, 'Rio Grande', 369),
    (2042, 'Alvorada', 369),
    (2043, 'Passo Fundo', 369),
    (2044, 'Uruguaiana', 369),
    (2045, 'BagÃ©', 369),
    (2046, 'Sapucaia do Sul', 369),
    (2047, 'Santa Cruz do Sul', 369),
    (2048, 'Cachoeirinha', 369),
    (2049, 'GuaÃ­ba', 369),
    (2050, 'Santana do Livramento', 369),
    (2051, 'Bento GonÃ§alves', 369),
    (2052, 'Porto Velho', 370),
    (2053, 'Ji-ParanÃ¡', 370),
    (2054, 'Boa Vista', 371),
    (2055, 'Joinville', 372),
    (2056, 'FlorianÃ³polis', 372),
    (2057, 'Blumenau', 372),
    (2058, 'CriciÃºma', 372),
    (2059, 'SÃ£o JosÃ©', 372),
    (2060, 'ItajaÃ­', 372),
    (2061, 'ChapecÃ³', 372),
    (2062, 'Lages', 372),
    (2063, 'JaraguÃ¡ do Sul', 372),
    (2064, 'PalhoÃ§a', 372),
    (2065, 'SÃ£o Paulo', 373),
    (2066, 'Guarulhos', 373),
    (2067, 'Campinas', 373),
    (2068, 'SÃ£o Bernardo do Campo', 373),
    (2069, 'Osasco', 373),
    (2070, 'Santo AndrÃ©', 373),
    (2071, 'SÃ£o JosÃ© dos Campos', 373),
    (2072, 'RibeirÃ£o Preto', 373),
    (2073, 'Sorocaba', 373),
    (2074, 'Santos', 373),
    (2075, 'MauÃ¡', 373),
    (2076, 'CarapicuÃ­ba', 373),
    (2077, 'SÃ£o JosÃ© do Rio Preto', 373),
    (2078, 'Moji das Cruzes', 373),
    (2079, 'Diadema', 373),
    (2080, 'Piracicaba', 373),
    (2081, 'Bauru', 373),
    (2082, 'JundÃ­aÃ­', 373),
    (2083, 'Franca', 373),
    (2084, 'SÃ£o Vicente', 373),
    (2085, 'Itaquaquecetuba', 373),
    (2086, 'Limeira', 373),
    (2087, 'GuarujÃ¡', 373),
    (2088, 'TaubatÃ©', 373),
    (2089, 'Embu', 373),
    (2090, 'Barueri', 373),
    (2091, 'TaboÃ£o da Serra', 373),
    (2092, 'Suzano', 373),
    (2093, 'MarÃ­lia', 373),
    (2094, 'SÃ£o Carlos', 373),
    (2095, 'SumarÃ©', 373),
    (2096, 'Presidente Prudente', 373),
    (2097, 'Americana', 373),
    (2098, 'Araraquara', 373),
    (2099, 'Santa BÃ¡rbara dÂ´Oeste', 373),
    (2100, 'JacareÃ­', 373),
    (2101, 'AraÃ§atuba', 373),
    (2102, 'Praia Grande', 373),
    (2103, 'Rio Claro', 373),
    (2104, 'Itapevi', 373),
    (2105, 'Cotia', 373),
    (2106, 'Ferraz de Vasconcelos', 373),
    (2107, 'Indaiatuba', 373),
    (2108, 'HortolÃ¢ndia', 373),
    (2109, 'SÃ£o Caetano do Sul', 373),
    (2110, 'Itu', 373),
    (2111, 'Itapecerica da Serra', 373),
    (2112, 'Moji-GuaÃ§u', 373),
    (2113, 'Pindamonhangaba', 373),
    (2114, 'Francisco Morato', 373),
    (2115, 'Itapetininga', 373),
    (2116, 'BraganÃ§a Paulista', 373),
    (2117, 'JaÃº', 373),
    (2118, 'Franco da Rocha', 373),
    (2119, 'RibeirÃ£o Pires', 373),
    (2120, 'Catanduva', 373),
    (2121, 'Botucatu', 373),
    (2122, 'Barretos', 373),
    (2123, 'GuaratinguetÃ¡', 373),
    (2124, 'CubatÃ£o', 373),
    (2125, 'Araras', 373),
    (2126, 'Atibaia', 373),
    (2127, 'SertÃ£ozinho', 373),
    (2128, 'Salto', 373),
    (2129, 'Ourinhos', 373),
    (2130, 'Birigui', 373),
    (2131, 'TatuÃ­', 373),
    (2132, 'Votorantim', 373),
    (2133, 'PoÃ¡', 373),
    (2134, 'Aracaju', 374),
    (2135, 'Nossa Senhora do Socorro', 374),
    (2136, 'Palmas', 375),
    (2137, 'AraguaÃ­na', 375),
    (2138, 'Bridgetown', 376),
    (2139, 'Bandar Seri Begawan', 377),
    (2140, 'Thimphu', 378),
    (2141, 'Francistown', 379),
    (2142, 'Gaborone', 380),
    (2143, 'Bangui', 381),
    (2144, 'Calgary', 382),
    (2145, 'Edmonton', 382),
    (2146, 'Vancouver', 383),
    (2147, 'Surrey', 383),
    (2148, 'Burnaby', 383),
    (2149, 'Richmond', 383),
    (2150, 'Abbotsford', 383),
    (2151, 'Coquitlam', 383),
    (2152, 'Saanich', 383),
    (2153, 'Delta', 383),
    (2154, 'Kelowna', 383),
    (2155, 'Winnipeg', 384),
    (2156, 'Saint JohnÂ´s', 385),
    (2157, 'Cape Breton', 386),
    (2158, 'Halifax', 386),
    (2159, 'Toronto', 387),
    (2160, 'North York', 387),
    (2161, 'Mississauga', 387),
    (2162, 'Scarborough', 387),
    (2163, 'Etobicoke', 387),
    (2164, 'London', 387),
    (2165, 'Hamilton', 387),
    (2166, 'Ottawa', 387),
    (2167, 'Brampton', 387),
    (2168, 'Windsor', 387),
    (2169, 'Kitchener', 387),
    (2170, 'Markham', 387),
    (2171, 'York', 387),
    (2172, 'Vaughan', 387),
    (2173, 'Burlington', 387),
    (2174, 'Oshawa', 387),
    (2175, 'Oakville', 387),
    (2176, 'Saint Catharines', 387),
    (2177, 'Richmond Hill', 387),
    (2178, 'Thunder Bay', 387),
    (2179, 'Nepean', 387),
    (2180, 'East York', 387),
    (2181, 'Cambridge', 387),
    (2182, 'Gloucester', 387),
    (2183, 'Guelph', 387),
    (2184, 'Sudbury', 387),
    (2185, 'Barrie', 387),
    (2186, 'MontrÃ©al', 388),
    (2187, 'Laval', 388),
    (2188, 'QuÃ©bec', 388),
    (2189, 'Longueuil', 388),
    (2190, 'Gatineau', 388),
    (2191, 'Saskatoon', 389),
    (2192, 'Regina', 389),
    (2193, 'Bantam', 390),
    (2194, 'West Island', 391),
    (2195, 'Basel', 392),
    (2196, 'Bern', 393),
    (2197, 'Geneve', 394),
    (2198, 'Lausanne', 395),
    (2199, 'ZÃ¼rich', 396),
    (2200, 'Antofagasta', 397),
    (2201, 'Calama', 397),
    (2202, 'CopiapÃ³', 398),
    (2203, 'Talcahuano', 399),
    (2204, 'ConcepciÃ³n', 399),
    (2205, 'ChillÃ¡n', 399),
    (2206, 'Los Angeles', 399),
    (2207, 'Coronel', 399),
    (2208, 'San Pedro de la Paz', 399),
    (2209, 'Coquimbo', 400),
    (2210, 'La Serena', 400),
    (2211, 'Ovalle', 400),
    (2212, 'Temuco', 401),
    (2213, 'Puerto Montt', 402),
    (2214, 'Osorno', 402),
    (2215, 'Valdivia', 402),
    (2216, 'Punta Arenas', 403),
    (2217, 'Talca', 404),
    (2218, 'CuricÃ³', 404),
    (2219, 'Rancagua', 405),
    (2220, 'Santiago de Chile', 406),
    (2221, 'Puente Alto', 406),
    (2222, 'San Bernardo', 406),
    (2223, 'Melipilla', 406),
    (2224, 'Santiago de los Caballeros', 406),
    (2225, 'Arica', 407),
    (2226, 'Iquique', 407),
    (2227, 'ViÃ±a del Mar', 408),
    (2228, 'ValparaÃ­so', 408),
    (2229, 'QuilpuÃ©', 408),
    (2230, 'Hefei', 409),
    (2231, 'Huainan', 409),
    (2232, 'Bengbu', 409),
    (2233, 'Wuhu', 409),
    (2234, 'Huaibei', 409),
    (2235, 'MaÂ´anshan', 409),
    (2236, 'Anqing', 409),
    (2237, 'Tongling', 409),
    (2238, 'Fuyang', 409),
    (2239, 'Suzhou', 409),
    (2240, 'LiuÂ´an', 409),
    (2241, 'Chuzhou', 409),
    (2242, 'Chaohu', 409),
    (2243, 'Xuangzhou', 409),
    (2244, 'Bozhou', 409),
    (2245, 'Huangshan', 409),
    (2246, 'Chongqing', 410),
    (2247, 'Fuzhou', 411),
    (2248, 'Amoy [Xiamen]', 411),
    (2249, 'Nanping', 411),
    (2250, 'Quanzhou', 411),
    (2251, 'Zhangzhou', 411),
    (2252, 'Sanming', 411),
    (2253, 'Longyan', 411),
    (2254, 'YongÂ´an', 411),
    (2255, 'FuÂ´an', 411),
    (2256, 'Fuqing', 411),
    (2257, 'Putian', 411),
    (2258, 'Shaowu', 411),
    (2259, 'Lanzhou', 412),
    (2260, 'Tianshui', 412),
    (2261, 'Baiyin', 412),
    (2262, 'Wuwei', 412),
    (2263, 'Yumen', 412),
    (2264, 'Jinchang', 412),
    (2265, 'Pingliang', 412),
    (2266, 'Kanton [Guangzhou]', 413),
    (2267, 'Shenzhen', 413),
    (2268, 'Shantou', 413),
    (2269, 'Zhangjiang', 413),
    (2270, 'Shaoguan', 413),
    (2271, 'Chaozhou', 413),
    (2272, 'Dongwan', 413),
    (2273, 'Foshan', 413),
    (2274, 'Zhongshan', 413),
    (2275, 'Jiangmen', 413),
    (2276, 'Yangjiang', 413),
    (2277, 'Zhaoqing', 413),
    (2278, 'Maoming', 413),
    (2279, 'Zhuhai', 413),
    (2280, 'Qingyuan', 413),
    (2281, 'Huizhou', 413),
    (2282, 'Meixian', 413),
    (2283, 'Heyuan', 413),
    (2284, 'Shanwei', 413),
    (2285, 'Jieyang', 413),
    (2286, 'Nanning', 414),
    (2287, 'Liuzhou', 414),
    (2288, 'Guilin', 414),
    (2289, 'Wuzhou', 414),
    (2290, 'Yulin', 414),
    (2291, 'Qinzhou', 414),
    (2292, 'Guigang', 414),
    (2293, 'Beihai', 414),
    (2294, 'Bose', 414),
    (2295, 'Guiyang', 415),
    (2296, 'Liupanshui', 415),
    (2297, 'Zunyi', 415),
    (2298, 'Anshun', 415),
    (2299, 'Duyun', 415),
    (2300, 'Kaili', 415),
    (2301, 'Haikou', 416),
    (2302, 'Sanya', 416),
    (2303, 'Shijiazhuang', 417),
    (2304, 'Tangshan', 417),
    (2305, 'Handan', 417),
    (2306, 'Zhangjiakou', 417),
    (2307, 'Baoding', 417),
    (2308, 'Qinhuangdao', 417),
    (2309, 'Xingtai', 417),
    (2310, 'Chengde', 417),
    (2311, 'Cangzhou', 417),
    (2312, 'Langfang', 417),
    (2313, 'Renqiu', 417),
    (2314, 'Hengshui', 417),
    (2315, 'Harbin', 418),
    (2316, 'Qiqihar', 418),
    (2317, 'Yichun', 418),
    (2318, 'Jixi', 418),
    (2319, 'Daqing', 418),
    (2320, 'Mudanjiang', 418),
    (2321, 'Hegang', 418),
    (2322, 'Jiamusi', 418),
    (2323, 'Shuangyashan', 418),
    (2324, 'Tieli', 418),
    (2325, 'Suihua', 418),
    (2326, 'Shangzi', 418),
    (2327, 'Qitaihe', 418),
    (2328, 'BeiÂ´an', 418),
    (2329, 'Acheng', 418),
    (2330, 'Zhaodong', 418),
    (2331, 'Shuangcheng', 418),
    (2332, 'Anda', 418),
    (2333, 'Hailun', 418),
    (2334, 'Mishan', 418),
    (2335, 'Fujin', 418),
    (2336, 'Zhengzhou', 419),
    (2337, 'Luoyang', 419),
    (2338, 'Kaifeng', 419),
    (2339, 'Xinxiang', 419),
    (2340, 'Anyang', 419),
    (2341, 'Pingdingshan', 419),
    (2342, 'Jiaozuo', 419),
    (2343, 'Nanyang', 419),
    (2344, 'Hebi', 419),
    (2345, 'Xuchang', 419),
    (2346, 'Xinyang', 419),
    (2347, 'Puyang', 419),
    (2348, 'Shangqiu', 419),
    (2349, 'Zhoukou', 419),
    (2350, 'Luohe', 419),
    (2351, 'Zhumadian', 419),
    (2352, 'Sanmenxia', 419),
    (2353, 'Yuzhou', 419),
    (2354, 'Wuhan', 420),
    (2355, 'Huangshi', 420),
    (2356, 'Xiangfan', 420),
    (2357, 'Yichang', 420),
    (2358, 'Shashi', 420),
    (2359, 'Shiyan', 420),
    (2360, 'Xiantao', 420),
    (2361, 'Qianjiang', 420),
    (2362, 'Honghu', 420),
    (2363, 'Ezhou', 420),
    (2364, 'Tianmen', 420),
    (2365, 'Xiaogan', 420),
    (2366, 'Zaoyang', 420),
    (2367, 'Jinmen', 420),
    (2368, 'Suizhou', 420),
    (2369, 'Xianning', 420),
    (2370, 'Laohekou', 420),
    (2371, 'Puqi', 420),
    (2372, 'Shishou', 420),
    (2373, 'Danjiangkou', 420),
    (2374, 'Guangshui', 420),
    (2375, 'Enshi', 420),
    (2376, 'Changsha', 421),
    (2377, 'Hengyang', 421),
    (2378, 'Xiangtan', 421),
    (2379, 'Zhuzhou', 421),
    (2380, 'Yueyang', 421),
    (2381, 'Changde', 421),
    (2382, 'Shaoyang', 421),
    (2383, 'Yiyang', 421),
    (2384, 'Chenzhou', 421),
    (2385, 'Lengshuijiang', 421),
    (2386, 'Leiyang', 421),
    (2387, 'Loudi', 421),
    (2388, 'Huaihua', 421),
    (2389, 'Lianyuan', 421),
    (2390, 'Hongjiang', 421),
    (2391, 'Zixing', 421),
    (2392, 'Liling', 421),
    (2393, 'Yuanjiang', 421),
    (2394, 'Baotou', 422),
    (2395, 'Hohhot', 422),
    (2396, 'Yakeshi', 422),
    (2397, 'Chifeng', 422),
    (2398, 'Wuhai', 422),
    (2399, 'Tongliao', 422),
    (2400, 'Hailar', 422),
    (2401, 'Jining', 422),
    (2402, 'Ulanhot', 422),
    (2403, 'Linhe', 422),
    (2404, 'Zalantun', 422),
    (2405, 'Manzhouli', 422),
    (2406, 'Xilin Hot', 422),
    (2407, 'Nanking [Nanjing]', 423),
    (2408, 'Wuxi', 423),
    (2409, 'Xuzhou', 423),
    (2410, 'Suzhou', 423),
    (2411, 'Changzhou', 423),
    (2412, 'Zhenjiang', 423),
    (2413, 'Lianyungang', 423),
    (2414, 'Nantong', 423),
    (2415, 'Yangzhou', 423),
    (2416, 'Yancheng', 423),
    (2417, 'Huaiyin', 423),
    (2418, 'Jiangyin', 423),
    (2419, 'Yixing', 423),
    (2420, 'Dongtai', 423),
    (2421, 'Changshu', 423),
    (2422, 'Danyang', 423),
    (2423, 'Xinghua', 423),
    (2424, 'Taizhou', 423),
    (2425, 'HuaiÂ´an', 423),
    (2426, 'Qidong', 423),
    (2427, 'Liyang', 423),
    (2428, 'Yizheng', 423),
    (2429, 'Suqian', 423),
    (2430, 'Kunshan', 423),
    (2431, 'Zhangjiagang', 423),
    (2432, 'Nanchang', 424),
    (2433, 'Pingxiang', 424),
    (2434, 'Jiujiang', 424),
    (2435, 'Jingdezhen', 424),
    (2436, 'Ganzhou', 424),
    (2437, 'Fengcheng', 424),
    (2438, 'Xinyu', 424),
    (2439, 'Yichun', 424),
    (2440, 'JiÂ´an', 424),
    (2441, 'Shangrao', 424),
    (2442, 'Linchuan', 424),
    (2443, 'Changchun', 425),
    (2444, 'Jilin', 425),
    (2445, 'Hunjiang', 425),
    (2446, 'Liaoyuan', 425),
    (2447, 'Tonghua', 425),
    (2448, 'Siping', 425),
    (2449, 'Dunhua', 425),
    (2450, 'Yanji', 425),
    (2451, 'Gongziling', 425),
    (2452, 'Baicheng', 425),
    (2453, 'Meihekou', 425),
    (2454, 'Fuyu', 425),
    (2455, 'Jiutai', 425),
    (2456, 'Jiaohe', 425),
    (2457, 'Huadian', 425),
    (2458, 'Taonan', 425),
    (2459, 'Longjing', 425),
    (2460, 'DaÂ´an', 425),
    (2461, 'Yushu', 425),
    (2462, 'Tumen', 425),
    (2463, 'Shenyang', 426),
    (2464, 'Dalian', 426),
    (2465, 'Anshan', 426),
    (2466, 'Fushun', 426),
    (2467, 'Benxi', 426),
    (2468, 'Fuxin', 426),
    (2469, 'Jinzhou', 426),
    (2470, 'Dandong', 426),
    (2471, 'Liaoyang', 426),
    (2472, 'Yingkou', 426),
    (2473, 'Panjin', 426),
    (2474, 'Jinxi', 426),
    (2475, 'Tieling', 426),
    (2476, 'Wafangdian', 426),
    (2477, 'Chaoyang', 426),
    (2478, 'Haicheng', 426),
    (2479, 'Beipiao', 426),
    (2480, 'Tiefa', 426),
    (2481, 'Kaiyuan', 426),
    (2482, 'Xingcheng', 426),
    (2483, 'Jinzhou', 426),
    (2484, 'Yinchuan', 427),
    (2485, 'Shizuishan', 427),
    (2486, 'Peking', 428),
    (2487, 'Tong Xian', 428),
    (2488, 'Xining', 429),
    (2489, 'XiÂ´an', 430),
    (2490, 'Xianyang', 430),
    (2491, 'Baoji', 430),
    (2492, 'Tongchuan', 430),
    (2493, 'Hanzhong', 430),
    (2494, 'Ankang', 430),
    (2495, 'Weinan', 430),
    (2496, 'YanÂ´an', 430),
    (2497, 'Qingdao', 431),
    (2498, 'Jinan', 431),
    (2499, 'Zibo', 431),
    (2500, 'Yantai', 431),
    (2501, 'Weifang', 431),
    (2502, 'Zaozhuang', 431),
    (2503, 'TaiÂ´an', 431),
    (2504, 'Linyi', 431),
    (2505, 'Tengzhou', 431),
    (2506, 'Dongying', 431),
    (2507, 'Xintai', 431),
    (2508, 'Jining', 431),
    (2509, 'Laiwu', 431),
    (2510, 'Liaocheng', 431),
    (2511, 'Laizhou', 431),
    (2512, 'Dezhou', 431),
    (2513, 'Heze', 431),
    (2514, 'Rizhao', 431),
    (2515, 'Liangcheng', 431),
    (2516, 'Jiaozhou', 431),
    (2517, 'Pingdu', 431),
    (2518, 'Longkou', 431),
    (2519, 'Laiyang', 431),
    (2520, 'Wendeng', 431),
    (2521, 'Binzhou', 431),
    (2522, 'Weihai', 431),
    (2523, 'Qingzhou', 431),
    (2524, 'Linqing', 431),
    (2525, 'Jiaonan', 431),
    (2526, 'Zhucheng', 431),
    (2527, 'Junan', 431),
    (2528, 'Pingyi', 431),
    (2529, 'Shanghai', 432),
    (2530, 'Taiyuan', 433),
    (2531, 'Datong', 433),
    (2532, 'Yangquan', 433),
    (2533, 'Changzhi', 433),
    (2534, 'Yuci', 433),
    (2535, 'Linfen', 433),
    (2536, 'Jincheng', 433),
    (2537, 'Yuncheng', 433),
    (2538, 'Xinzhou', 433),
    (2539, 'Chengdu', 434),
    (2540, 'Panzhihua', 434),
    (2541, 'Zigong', 434),
    (2542, 'Leshan', 434),
    (2543, 'Mianyang', 434),
    (2544, 'Luzhou', 434),
    (2545, 'Neijiang', 434),
    (2546, 'Yibin', 434),
    (2547, 'Daxian', 434),
    (2548, 'Deyang', 434),
    (2549, 'Guangyuan', 434),
    (2550, 'Nanchong', 434),
    (2551, 'Jiangyou', 434),
    (2552, 'Fuling', 434),
    (2553, 'Wanxian', 434),
    (2554, 'Suining', 434),
    (2555, 'Xichang', 434),
    (2556, 'Dujiangyan', 434),
    (2557, 'YaÂ´an', 434),
    (2558, 'Emeishan', 434),
    (2559, 'Huaying', 434),
    (2560, 'Tianjin', 435),
    (2561, 'Lhasa', 436),
    (2562, 'UrumtÅ¡i [ÃœrÃ¼mqi]', 437),
    (2563, 'Shihezi', 437),
    (2564, 'Qaramay', 437),
    (2565, 'Ghulja', 437),
    (2566, 'Qashqar', 437),
    (2567, 'Aqsu', 437),
    (2568, 'Hami', 437),
    (2569, 'Korla', 437),
    (2570, 'Changji', 437),
    (2571, 'Kuytun', 437),
    (2572, 'Kunming', 438),
    (2573, 'Gejiu', 438),
    (2574, 'Qujing', 438),
    (2575, 'Dali', 438),
    (2576, 'Kaiyuan', 438),
    (2577, 'Hangzhou', 439),
    (2578, 'Ningbo', 439),
    (2579, 'Wenzhou', 439),
    (2580, 'Huzhou', 439),
    (2581, 'Jiaxing', 439),
    (2582, 'Shaoxing', 439),
    (2583, 'Xiaoshan', 439),
    (2584, 'RuiÂ´an', 439),
    (2585, 'Zhoushan', 439),
    (2586, 'Jinhua', 439),
    (2587, 'Yuyao', 439),
    (2588, 'Quzhou', 439),
    (2589, 'Cixi', 439),
    (2590, 'Haining', 439),
    (2591, 'Linhai', 439),
    (2592, 'Huangyan', 439),
    (2593, 'Abidjan', 440),
    (2594, 'BouakÃ©', 441),
    (2595, 'Daloa', 442),
    (2596, 'Korhogo', 443),
    (2597, 'Yamoussoukro', 444),
    (2598, 'YaoundÃ©', 445),
    (2599, 'Tours', 445),
    (2600, 'OrlÃ©ans', 445),
    (2601, 'Maroua', 446),
    (2602, 'Douala', 447),
    (2603, 'Nkongsamba', 447),
    (2604, 'Le-Cap-HaÃ¯tien', 448),
    (2605, 'Garoua', 448),
    (2606, 'Bamenda', 449),
    (2607, 'Port-au-Prince', 450),
    (2608, 'Carrefour', 450),
    (2609, 'Delmas', 450),
    (2610, 'Bafoussam', 450),
    (2611, 'Kikwit', 451),
    (2612, 'Matadi', 452),
    (2613, 'Boma', 452),
    (2614, 'Mbuji-Mayi', 453),
    (2615, 'Mwene-Ditu', 453),
    (2616, 'Mbandaka', 454),
    (2617, 'Kisangani', 455),
    (2618, 'Kinshasa', 456),
    (2619, 'Butembo', 457),
    (2620, 'Goma', 457),
    (2621, 'Lubumbashi', 458),
    (2622, 'Kolwezi', 458),
    (2623, 'Likasi', 458),
    (2624, 'Kalemie', 458),
    (2625, 'Bukavu', 459),
    (2626, 'Uvira', 459),
    (2627, 'Kananga', 460),
    (2628, 'Tshikapa', 460),
    (2629, 'Brazzaville', 461),
    (2630, 'Pointe-Noire', 462),
    (2631, 'Avarua', 463),
    (2632, 'MedellÃ­n', 464),
    (2633, 'Bello', 464),
    (2634, 'ItagÃ¼Ã­', 464),
    (2635, 'Envigado', 464),
    (2636, 'Barranquilla', 465),
    (2637, 'Soledad', 465),
    (2638, 'Cartagena', 466),
    (2639, 'Ciudad Guayana', 466),
    (2640, 'Ciudad BolÃ­var', 466),
    (2641, 'Tunja', 467),
    (2642, 'Sogamoso', 467),
    (2643, 'Manizales', 468),
    (2644, 'Florencia', 469),
    (2645, 'PopayÃ¡n', 470),
    (2646, 'CÃ³rdoba', 471),
    (2647, 'RÃ­o Cuarto', 471),
    (2648, 'MonterÃ­a', 471),
    (2649, 'Valledupar', 472),
    (2650, 'Soacha', 473),
    (2651, 'Girardot', 473),
    (2652, 'Neiva', 474),
    (2653, 'Maicao', 475),
    (2654, 'Santa Marta', 476),
    (2655, 'Villavicencio', 477),
    (2656, 'Pasto', 478),
    (2657, 'CÃºcuta', 479),
    (2658, 'Armenia', 480),
    (2659, 'Pereira', 481),
    (2660, 'Dos Quebradas', 481),
    (2661, 'SantafÃ© de BogotÃ¡', 482),
    (2662, 'Bucaramanga', 483),
    (2663, 'Floridablanca', 483),
    (2664, 'Barrancabermeja', 483),
    (2665, 'Giron', 483),
    (2666, 'Sincelejo', 484),
    (2667, 'CumanÃ¡', 484),
    (2668, 'CarÃºpano', 484),
    (2669, 'IbaguÃ©', 485),
    (2670, 'Cali', 486),
    (2671, 'Palmira', 486),
    (2672, 'Buenaventura', 486),
    (2673, 'TuluÃ¡', 486),
    (2674, 'Cartago', 486),
    (2675, 'Buga', 486),
    (2676, 'Moroni', 487),
    (2677, 'Praia', 488),
    (2678, 'San JosÃ©', 489),
    (2679, 'CamagÃ¼ey', 490),
    (2680, 'Ciego de Ãvila', 491),
    (2681, 'Cienfuegos', 492),
    (2682, 'Bayamo', 493),
    (2683, 'Manzanillo', 493),
    (2684, 'GuantÃ¡namo', 494),
    (2685, 'HolguÃ­n', 495),
    (2686, 'La Habana', 496),
    (2687, 'Victoria de las Tunas', 497),
    (2688, 'Matanzas', 498),
    (2689, 'Pinar del RÃ­o', 499),
    (2690, 'Sancti-SpÃ­ritus', 500),
    (2691, 'Santiago de Cuba', 501),
    (2692, 'Santa Clara', 502),
    (2693, 'South Hill', 503),
    (2694, 'The Valley', 503),
    (2695, 'Oranjestad', 503),
    (2696, 'Douglas', 503),
    (2697, 'Gibraltar', 503),
    (2698, 'Tamuning', 503),
    (2699, 'AgaÃ±a', 503),
    (2700, 'Flying Fish Cove', 503),
    (2701, 'Monte-Carlo', 503),
    (2702, 'Monaco-Ville', 503),
    (2703, 'Yangor', 503),
    (2704, 'Yaren', 503),
    (2705, 'Alofi', 503),
    (2706, 'Kingston', 503),
    (2707, 'Adamstown', 503),
    (2708, 'Singapore', 503),
    (2709, 'NoumÃ©a', 503),
    (2710, 'CittÃ  del Vaticano', 503),
    (2711, 'George Town', 504),
    (2712, 'Limassol', 505),
    (2713, 'Nicosia', 506),
    (2714, 'Praha', 507),
    (2715, 'CeskÃ© Budejovice', 508),
    (2716, 'Brno', 509),
    (2717, 'Liberec', 510),
    (2718, 'ÃšstÃ­ nad Labem', 510),
    (2719, 'Ostrava', 511),
    (2720, 'Olomouc', 511),
    (2721, 'Hradec KrÃ¡lovÃ©', 512),
    (2722, 'Pardubice', 512),
    (2723, 'Plzen', 513),
    (2724, 'Halle/Saale', 514),
    (2725, 'Magdeburg', 514),
    (2726, 'Stuttgart', 515),
    (2727, 'Mannheim', 515),
    (2728, 'Karlsruhe', 515),
    (2729, 'Freiburg im Breisgau', 515),
    (2730, 'Heidelberg', 515),
    (2731, 'Heilbronn', 515),
    (2732, 'Pforzheim', 515),
    (2733, 'Ulm', 515),
    (2734, 'Reutlingen', 515),
    (2735, 'Esslingen am Neckar', 515),
    (2736, 'Munich [MÃ¼nchen]', 516),
    (2737, 'NÃ¼rnberg', 516),
    (2738, 'Augsburg', 516),
    (2739, 'WÃ¼rzburg', 516),
    (2740, 'Regensburg', 516),
    (2741, 'Ingolstadt', 516),
    (2742, 'FÃ¼rth', 516),
    (2743, 'Erlangen', 516),
    (2744, 'Berlin', 517),
    (2745, 'Potsdam', 518),
    (2746, 'Cottbus', 518),
    (2747, 'Bremen', 519),
    (2748, 'Bremerhaven', 519),
    (2749, 'Hamburg', 520),
    (2750, 'Frankfurt am Main', 521),
    (2751, 'Wiesbaden', 521),
    (2752, 'Kassel', 521),
    (2753, 'Darmstadt', 521),
    (2754, 'Offenbach am Main', 521),
    (2755, 'Rostock', 522),
    (2756, 'Schwerin', 522),
    (2757, 'Hannover', 523),
    (2758, 'Braunschweig', 523),
    (2759, 'OsnabrÃ¼ck', 523),
    (2760, 'Oldenburg', 523),
    (2761, 'GÃ¶ttingen', 523),
    (2762, 'Wolfsburg', 523),
    (2763, 'Salzgitter', 523),
    (2764, 'Hildesheim', 523),
    (2765, 'KÃ¶ln', 524),
    (2766, 'Essen', 524),
    (2767, 'Dortmund', 524),
    (2768, 'DÃ¼sseldorf', 524),
    (2769, 'Duisburg', 524),
    (2770, 'Bochum', 524),
    (2771, 'Wuppertal', 524),
    (2772, 'Bielefeld', 524),
    (2773, 'Bonn', 524),
    (2774, 'Gelsenkirchen', 524),
    (2775, 'MÃ¼nster', 524),
    (2776, 'MÃ¶nchengladbach', 524),
    (2777, 'Aachen', 524),
    (2778, 'Krefeld', 524),
    (2779, 'Oberhausen', 524),
    (2780, 'Hagen', 524),
    (2781, 'Hamm', 524),
    (2782, 'Herne', 524),
    (2783, 'MÃ¼lheim an der Ruhr', 524),
    (2784, 'Solingen', 524),
    (2785, 'Leverkusen', 524),
    (2786, 'Neuss', 524),
    (2787, 'Paderborn', 524),
    (2788, 'Recklinghausen', 524),
    (2789, 'Bottrop', 524),
    (2790, 'Remscheid', 524),
    (2791, 'Siegen', 524),
    (2792, 'Moers', 524),
    (2793, 'Bergisch Gladbach', 524),
    (2794, 'Witten', 524),
    (2795, 'Iserlohn', 524),
    (2796, 'GÃ¼tersloh', 524),
    (2797, 'Marl', 524),
    (2798, 'LÃ¼nen', 524),
    (2799, 'DÃ¼ren', 524),
    (2800, 'Ratingen', 524),
    (2801, 'Velbert', 524),
    (2802, 'Mainz', 525),
    (2803, 'Ludwigshafen am Rhein', 525),
    (2804, 'Koblenz', 525),
    (2805, 'Kaiserslautern', 525),
    (2806, 'Trier', 525),
    (2807, 'SaarbrÃ¼cken', 526),
    (2808, 'Leipzig', 527),
    (2809, 'Dresden', 527),
    (2810, 'Chemnitz', 527),
    (2811, 'Zwickau', 527),
    (2812, 'Kiel', 528),
    (2813, 'LÃ¼beck', 528),
    (2814, 'Erfurt', 529),
    (2815, 'Gera', 529),
    (2816, 'Jena', 529),
    (2817, 'Djibouti', 530),
    (2818, 'Roseau', 531),
    (2819, 'Saint GeorgeÂ´s', 531),
    (2820, 'Kingstown', 531),
    (2821, 'Ã…rhus', 532),
    (2822, 'Frederiksberg', 533),
    (2823, 'Odense', 534),
    (2824, 'KÃ¸benhavn', 535),
    (2825, 'Aalborg', 536),
    (2826, 'Santo Domingo de GuzmÃ¡n', 537),
    (2827, 'San Francisco de MacorÃ­s', 538),
    (2828, 'La Romana', 539),
    (2829, 'San Felipe de Puerto Plata', 540),
    (2830, 'San Pedro de MacorÃ­s', 541),
    (2831, 'Santiago de Chile', 542),
    (2832, 'Puente Alto', 542),
    (2833, 'San Bernardo', 542),
    (2834, 'Melipilla', 542),
    (2835, 'Santiago de los Caballeros', 542),
    (2836, 'Alger', 543),
    (2837, 'Annaba', 544),
    (2838, 'Batna', 545),
    (2839, 'BÃ©char', 546),
    (2840, 'BÃ©jaÃ¯a', 547),
    (2841, 'Biskra', 548),
    (2842, 'Blida (el-Boulaida)', 549),
    (2843, 'Ech-Chleff (el-Asnam)', 550),
    (2844, 'Constantine', 551),
    (2845, 'GhardaÃ¯a', 552),
    (2846, 'Mostaganem', 553),
    (2847, 'Oran', 554),
    (2848, 'SÃ©tif', 555),
    (2849, 'Sidi Bel AbbÃ¨s', 556),
    (2850, 'Skikda', 557),
    (2851, 'TÃ©bessa', 558),
    (2852, 'Tiaret', 559),
    (2853, 'Tlemcen (Tilimsen)', 560),
    (2854, 'Cuenca', 561),
    (2855, 'RÃ­obamba', 562),
    (2856, 'Machala', 563),
    (2857, 'Esmeraldas', 564),
    (2858, 'Guayaquil', 565),
    (2859, 'Duran [Eloy Alfaro]', 565),
    (2860, 'Milagro', 565),
    (2861, 'Ibarra', 566),
    (2862, 'Loja', 567),
    (2863, 'Quevedo', 568),
    (2864, 'Portoviejo', 569),
    (2865, 'Manta', 569),
    (2866, 'Quito', 570),
    (2867, 'Santo Domingo de los Colorados', 570),
    (2868, 'Ambato', 571),
    (2869, 'Kafr al-Dawwar', 572),
    (2870, 'Damanhur', 572),
    (2871, 'al-Mansura', 573),
    (2872, 'Mit Ghamr', 573),
    (2873, 'Talkha', 573),
    (2874, 'al-Faiyum', 574),
    (2875, 'al-Mahallat al-Kubra', 575),
    (2876, 'Tanta', 575),
    (2877, 'Shibin al-Kawm', 576),
    (2878, 'al-Minya', 577),
    (2879, 'Mallawi', 577),
    (2880, 'Shubra al-Khayma', 578),
    (2881, 'Bahtim', 578),
    (2882, 'Banha', 578),
    (2883, 'Qalyub', 578),
    (2884, 'Zagazig', 579),
    (2885, 'Bilbays', 579),
    (2886, 'al-Dammam', 579),
    (2887, 'al-Hufuf', 579),
    (2888, 'al-Mubarraz', 579),
    (2889, 'al-Khubar', 579),
    (2890, 'Jubayl', 579),
    (2891, 'Hafar al-Batin', 579),
    (2892, 'al-Tuqba', 579),
    (2893, 'al-Qatif', 579),
    (2894, 'Alexandria', 580),
    (2895, 'Assuan', 581),
    (2896, 'Asyut', 582),
    (2897, 'Bani Suwayf', 583),
    (2898, 'Giza', 584),
    (2899, 'Bulaq al-Dakrur', 584),
    (2900, 'Warraq al-Arab', 584),
    (2901, 'al-Hawamidiya', 584),
    (2902, 'Ismailia', 585),
    (2903, 'Kafr al-Shaykh', 586),
    (2904, 'Disuq', 586),
    (2905, 'Cairo', 587),
    (2906, 'Luxor', 588),
    (2907, 'Port Said', 589),
    (2908, 'Qina', 590),
    (2909, 'Idfu', 590),
    (2910, 'Sawhaj', 591),
    (2911, 'Jirja', 591),
    (2912, 'al-Arish', 592),
    (2913, 'Suez', 593),
    (2914, 'Asmara', 594),
    (2915, 'El-AaiÃºn', 595),
    (2916, 'Sevilla', 596),
    (2917, 'MÃ¡laga', 596),
    (2918, 'CÃ³rdoba', 596),
    (2919, 'Granada', 596),
    (2920, 'Jerez de la Frontera', 596),
    (2921, 'AlmerÃ­a', 596),
    (2922, 'CÃ¡diz', 596),
    (2923, 'Huelva', 596),
    (2924, 'JaÃ©n', 596),
    (2925, 'Algeciras', 596),
    (2926, 'Marbella', 596),
    (2927, 'Dos Hermanas', 596),
    (2928, 'Zaragoza', 597),
    (2929, 'GijÃ³n', 598),
    (2930, 'Oviedo', 598),
    (2931, 'Palma de Mallorca', 599),
    (2932, 'Bilbao', 600),
    (2933, 'Vitoria-Gasteiz', 600),
    (2934, 'Donostia-San SebastiÃ¡n', 600),
    (2935, 'Barakaldo', 600),
    (2936, 'Las Palmas de Gran Canaria', 601),
    (2937, 'Santa Cruz de Tenerife', 601),
    (2938, '[San CristÃ³bal de] la Laguna', 601),
    (2939, 'Santander', 602),
    (2940, 'Valladolid', 603),
    (2941, 'Burgos', 603),
    (2942, 'Salamanca', 603),
    (2943, 'LeÃ³n', 603),
    (2944, 'Badajoz', 604),
    (2945, 'Vigo', 605),
    (2946, 'A CoruÃ±a (La CoruÃ±a)', 605),
    (2947, 'Ourense (Orense)', 605),
    (2948, 'Santiago de Compostela', 605),
    (2949, 'Albacete', 606),
    (2950, 'Barcelona', 607),
    (2951, 'LÂ´Hospitalet de Llobregat', 607),
    (2952, 'Badalona', 607),
    (2953, 'Sabadell', 607),
    (2954, 'Terrassa', 607),
    (2955, 'Santa Coloma de Gramenet', 607),
    (2956, 'Tarragona', 607),
    (2957, 'Lleida (LÃ©rida)', 607),
    (2958, 'MatarÃ³', 607),
    (2959, 'La Rioja', 608),
    (2960, 'LogroÃ±o', 608),
    (2961, 'Madrid', 609),
    (2962, 'MÃ³stoles', 609),
    (2963, 'LeganÃ©s', 609),
    (2964, 'Fuenlabrada', 609),
    (2965, 'AlcalÃ¡ de Henares', 609),
    (2966, 'Getafe', 609),
    (2967, 'AlcorcÃ³n', 609),
    (2968, 'TorrejÃ³n de Ardoz', 609),
    (2969, 'Murcia', 610),
    (2970, 'Cartagena', 610),
    (2971, 'Pamplona [IruÃ±a]', 611),
    (2972, 'Valencia', 612),
    (2973, 'Alicante [Alacant]', 612),
    (2974, 'Elche [Elx]', 612),
    (2975, 'CastellÃ³n de la Plana [Castel', 612),
    (2976, 'Tallinn', 613),
    (2977, 'Tartu', 614),
    (2978, 'Addis Abeba', 615),
    (2979, 'Gonder', 616),
    (2980, 'Dese', 616),
    (2981, 'Bahir Dar', 616),
    (2982, 'Dire Dawa', 617),
    (2983, 'Nazret', 618),
    (2984, 'Mekele', 619),
    (2985, 'Helsinki [Helsingfors]', 620),
    (2986, 'Espoo', 620),
    (2987, 'Vantaa', 620),
    (2988, 'Lahti', 621),
    (2989, 'Tampere', 622),
    (2990, 'Oulu', 623),
    (2991, 'Turku [Ã…bo]', 624),
    (2992, 'Suva', 625),
    (2993, 'Nyeri', 625),
    (2994, 'Kathmandu', 625),
    (2995, 'Lalitapur', 625),
    (2996, 'Birgunj', 625),
    (2997, 'San Lorenzo', 625),
    (2998, 'LambarÃ©', 625),
    (2999, 'Fernando de la Mora', 625),
    (3000, 'Kabwe', 625),
    (3001, 'Kandy', 625),
    (3002, 'Kampala', 625),
    (3003, 'Stanley', 626),
    (3004, 'Strasbourg', 627),
    (3005, 'Mulhouse', 627),
    (3006, 'Bordeaux', 628),
    (3007, 'Clermont-Ferrand', 629),
    (3008, 'Paris', 630),
    (3009, 'Boulogne-Billancourt', 630),
    (3010, 'Argenteuil', 630),
    (3011, 'Montreuil', 630),
    (3012, 'Caen', 631),
    (3013, 'Dijon', 632),
    (3014, 'St-Ã‰tienne', 633),
    (3015, 'Brest', 633),
    (3016, 'YaoundÃ©', 634),
    (3017, 'Tours', 634),
    (3018, 'OrlÃ©ans', 634),
    (3019, 'Le Havre', 635),
    (3020, 'BesanÃ§on', 636),
    (3021, 'Rennes', 637),
    (3022, 'Rouen', 637),
    (3023, 'Montpellier', 638),
    (3024, 'NÃ®mes', 638),
    (3025, 'Perpignan', 638),
    (3026, 'Limoges', 639),
    (3027, 'Metz', 640),
    (3028, 'Nancy', 640),
    (3029, 'Toulouse', 641),
    (3030, 'Reims', 642),
    (3031, 'Roubaix', 642),
    (3032, 'Tourcoing', 642),
    (3033, 'Nantes', 643),
    (3034, 'Angers', 643),
    (3035, 'Le Mans', 643),
    (3036, 'Amiens', 644),
    (3037, 'Marseille', 645),
    (3038, 'Nice', 645),
    (3039, 'Toulon', 645),
    (3040, 'Aix-en-Provence', 645),
    (3041, 'Lyon', 646),
    (3042, 'Lille', 646),
    (3043, 'Grenoble', 646),
    (3044, 'Villeurbanne', 646),
    (3045, 'TÃ³rshavn', 647),
    (3046, 'Weno', 648),
    (3047, 'Palikir', 649),
    (3048, 'Libreville', 650),
    (3049, 'South Hill', 651),
    (3050, 'The Valley', 651),
    (3051, 'Oranjestad', 651),
    (3052, 'Douglas', 651),
    (3053, 'Gibraltar', 651),
    (3054, 'Tamuning', 651),
    (3055, 'AgaÃ±a', 651),
    (3056, 'Flying Fish Cove', 651),
    (3057, 'Monte-Carlo', 651),
    (3058, 'Monaco-Ville', 651),
    (3059, 'Yangor', 651),
    (3060, 'Yaren', 651),
    (3061, 'Alofi', 651),
    (3062, 'Kingston', 651),
    (3063, 'Adamstown', 651),
    (3064, 'Singapore', 651),
    (3065, 'NoumÃ©a', 651),
    (3066, 'CittÃ  del Vaticano', 651),
    (3067, 'London', 652),
    (3068, 'Birmingham', 652),
    (3069, 'Liverpool', 652),
    (3070, 'Sheffield', 652),
    (3071, 'Manchester', 652),
    (3072, 'Leeds', 652),
    (3073, 'Bristol', 652),
    (3074, 'Coventry', 652),
    (3075, 'Leicester', 652),
    (3076, 'Bradford', 652),
    (3077, 'Nottingham', 652),
    (3078, 'Kingston upon Hull', 652),
    (3079, 'Plymouth', 652),
    (3080, 'Stoke-on-Trent', 652),
    (3081, 'Wolverhampton', 652),
    (3082, 'Derby', 652),
    (3083, 'Southampton', 652),
    (3084, 'Northampton', 652),
    (3085, 'Dudley', 652),
    (3086, 'Portsmouth', 652),
    (3087, 'Newcastle upon Tyne', 652),
    (3088, 'Sunderland', 652),
    (3089, 'Luton', 652),
    (3090, 'Swindon', 652),
    (3091, 'Southend-on-Sea', 652),
    (3092, 'Walsall', 652),
    (3093, 'Bournemouth', 652),
    (3094, 'Peterborough', 652),
    (3095, 'Brighton', 652),
    (3096, 'Blackpool', 652),
    (3097, 'West Bromwich', 652),
    (3098, 'Reading', 652),
    (3099, 'Oldbury/Smethwick (Warley)', 652),
    (3100, 'Middlesbrough', 652),
    (3101, 'Huddersfield', 652),
    (3102, 'Oxford', 652),
    (3103, 'Poole', 652),
    (3104, 'Bolton', 652),
    (3105, 'Blackburn', 652),
    (3106, 'Preston', 652),
    (3107, 'Stockport', 652),
    (3108, 'Norwich', 652),
    (3109, 'Rotherham', 652),
    (3110, 'Cambridge', 652),
    (3111, 'Watford', 652),
    (3112, 'Ipswich', 652),
    (3113, 'Slough', 652),
    (3114, 'Exeter', 652),
    (3115, 'Cheltenham', 652),
    (3116, 'Gloucester', 652),
    (3117, 'Saint Helens', 652),
    (3118, 'Sutton Coldfield', 652),
    (3119, 'York', 652),
    (3120, 'Oldham', 652),
    (3121, 'Basildon', 652),
    (3122, 'Worthing', 652),
    (3123, 'Chelmsford', 652),
    (3124, 'Colchester', 652),
    (3125, 'Crawley', 652),
    (3126, 'Gillingham', 652),
    (3127, 'Solihull', 652),
    (3128, 'Rochdale', 652),
    (3129, 'Birkenhead', 652),
    (3130, 'Worcester', 652),
    (3131, 'Hartlepool', 652),
    (3132, 'Halifax', 652),
    (3133, 'Woking/Byfleet', 652),
    (3134, 'Southport', 652),
    (3135, 'Maidstone', 652),
    (3136, 'Eastbourne', 652),
    (3137, 'Grimsby', 652),
    (3138, 'Saint Helier', 653),
    (3139, 'Belfast', 654),
    (3140, 'Glasgow', 655),
    (3141, 'Edinburgh', 655),
    (3142, 'Aberdeen', 655),
    (3143, 'Dundee', 655),
    (3144, 'Cardiff', 656),
    (3145, 'Swansea', 656),
    (3146, 'Newport', 656),
    (3147, 'Sohumi', 657),
    (3148, 'Batumi', 658),
    (3149, 'Kutaisi', 659),
    (3150, 'Rustavi', 660),
    (3151, 'Tbilisi', 661),
    (3152, 'Kumasi', 662),
    (3153, 'Accra', 663),
    (3154, 'Tema', 663),
    (3155, 'Tamale', 664),
    (3156, 'Jaffna', 664),
    (3157, 'Sekondi-Takoradi', 665),
    (3158, 'Pokhara', 665),
    (3159, 'Freetown', 665),
    (3160, 'Colombo', 665),
    (3161, 'Dehiwala', 665),
    (3162, 'Moratuwa', 665),
    (3163, 'Sri Jayawardenepura Kotte', 665),
    (3164, 'Negombo', 665),
    (3165, 'South Hill', 666),
    (3166, 'The Valley', 666),
    (3167, 'Oranjestad', 666),
    (3168, 'Douglas', 666),
    (3169, 'Gibraltar', 666),
    (3170, 'Tamuning', 666),
    (3171, 'AgaÃ±a', 666),
    (3172, 'Flying Fish Cove', 666),
    (3173, 'Monte-Carlo', 666),
    (3174, 'Monaco-Ville', 666),
    (3175, 'Yangor', 666),
    (3176, 'Yaren', 666),
    (3177, 'Alofi', 666),
    (3178, 'Kingston', 666),
    (3179, 'Adamstown', 666),
    (3180, 'Singapore', 666),
    (3181, 'NoumÃ©a', 666),
    (3182, 'CittÃ  del Vaticano', 666),
    (3183, 'Conakry', 667),
    (3184, 'Basse-Terre', 668),
    (3185, 'Les Abymes', 669),
    (3186, 'Banjul', 670),
    (3187, 'Serekunda', 671),
    (3188, 'Bissau', 672),
    (3189, 'Malabo', 673),
    (3190, 'Athenai', 674),
    (3191, 'Pireus', 674),
    (3192, 'Peristerion', 674),
    (3193, 'Kallithea', 674),
    (3194, 'Thessaloniki', 675),
    (3195, 'Herakleion', 676),
    (3196, 'Larisa', 677),
    (3197, 'Patras', 678),
    (3198, 'Roseau', 679),
    (3199, 'Saint GeorgeÂ´s', 679),
    (3200, 'Kingstown', 679),
    (3201, 'Nuuk', 680),
    (3202, 'Ciudad de Guatemala', 681),
    (3203, 'Mixco', 681),
    (3204, 'Villa Nueva', 681),
    (3205, 'Quetzaltenango', 682),
    (3206, 'Cayenne', 683),
    (3207, 'South Hill', 684),
    (3208, 'The Valley', 684),
    (3209, 'Oranjestad', 684),
    (3210, 'Douglas', 684),
    (3211, 'Gibraltar', 684),
    (3212, 'Tamuning', 684),
    (3213, 'AgaÃ±a', 684),
    (3214, 'Flying Fish Cove', 684),
    (3215, 'Monte-Carlo', 684),
    (3216, 'Monaco-Ville', 684),
    (3217, 'Yangor', 684),
    (3218, 'Yaren', 684),
    (3219, 'Alofi', 684),
    (3220, 'Kingston', 684),
    (3221, 'Adamstown', 684),
    (3222, 'Singapore', 684),
    (3223, 'NoumÃ©a', 684),
    (3224, 'CittÃ  del Vaticano', 684),
    (3225, 'Georgetown', 685),
    (3226, 'Victoria', 686),
    (3227, 'Kowloon and New Kowloon', 687),
    (3228, 'La Ceiba', 688),
    (3229, 'San Pedro Sula', 689),
    (3230, 'Tegucigalpa', 690),
    (3231, 'Zagreb', 691),
    (3232, 'Osijek', 692),
    (3233, 'Rijeka', 693),
    (3234, 'Split', 694),
    (3235, 'Le-Cap-HaÃ¯tien', 695),
    (3236, 'Garoua', 695),
    (3237, 'Port-au-Prince', 696),
    (3238, 'Carrefour', 696),
    (3239, 'Delmas', 696),
    (3240, 'Bafoussam', 696),
    (3241, 'PÃ©cs', 697),
    (3242, 'KecskemÃ©t', 698),
    (3243, 'Miskolc', 699),
    (3244, 'Budapest', 700),
    (3245, 'Szeged', 701),
    (3246, 'SzÃ©kesfehÃ©rvÃ¡r', 702),
    (3247, 'GyÃ¶r', 703),
    (3248, 'Debrecen', 704),
    (3249, 'NyiregyhÃ¡za', 705),
    (3250, 'Banda Aceh', 706),
    (3251, 'Lhokseumawe', 706),
    (3252, 'Denpasar', 707),
    (3253, 'Bengkulu', 708),
    (3254, 'Semarang', 709),
    (3255, 'Surakarta', 709),
    (3256, 'Pekalongan', 709),
    (3257, 'Tegal', 709),
    (3258, 'Cilacap', 709),
    (3259, 'Purwokerto', 709),
    (3260, 'Magelang', 709),
    (3261, 'Pemalang', 709),
    (3262, 'Klaten', 709),
    (3263, 'Salatiga', 709),
    (3264, 'Kudus', 709),
    (3265, 'Surabaya', 710),
    (3266, 'Malang', 710),
    (3267, 'Kediri', 710),
    (3268, 'Jember', 710),
    (3269, 'Madiun', 710),
    (3270, 'Pasuruan', 710),
    (3271, 'Waru', 710),
    (3272, 'Blitar', 710),
    (3273, 'Probolinggo', 710),
    (3274, 'Taman', 710),
    (3275, 'Mojokerto', 710),
    (3276, 'Jombang', 710),
    (3277, 'Banyuwangi', 710),
    (3278, 'Jakarta', 711),
    (3279, 'Jambi', 712),
    (3280, 'Pontianak', 713),
    (3281, 'Banjarmasin', 714),
    (3282, 'Palangka Raya', 715),
    (3283, 'Samarinda', 716),
    (3284, 'Balikpapan', 716),
    (3285, 'Bandar Lampung', 717),
    (3286, 'Ambon', 718),
    (3287, 'Mataram', 719),
    (3288, 'Kupang', 720),
    (3289, 'Pekan Baru', 721),
    (3290, 'Batam', 721),
    (3291, 'Tanjung Pinang', 721),
    (3292, 'Ujung Pandang', 722),
    (3293, 'Palu', 723),
    (3294, 'Kendari', 724),
    (3295, 'Manado', 725),
    (3296, 'Gorontalo', 725),
    (3297, 'Padang', 726),
    (3298, 'Palembang', 727),
    (3299, 'Pangkal Pinang', 727),
    (3300, 'Medan', 728),
    (3301, 'Pematang Siantar', 728),
    (3302, 'Tebing Tinggi', 728),
    (3303, 'Percut Sei Tuan', 728),
    (3304, 'Binjai', 728),
    (3305, 'Sunggal', 728),
    (3306, 'Padang Sidempuan', 728),
    (3307, 'Jaya Pura', 729),
    (3308, 'Bandung', 730),
    (3309, 'Tangerang', 730),
    (3310, 'Bekasi', 730),
    (3311, 'Depok', 730),
    (3312, 'Cimahi', 730),
    (3313, 'Bogor', 730),
    (3314, 'Ciputat', 730),
    (3315, 'Pondokgede', 730),
    (3316, 'Cirebon', 730),
    (3317, 'Cimanggis', 730),
    (3318, 'Ciomas', 730),
    (3319, 'Tasikmalaya', 730),
    (3320, 'Karawang', 730),
    (3321, 'Sukabumi', 730),
    (3322, 'Serang', 730),
    (3323, 'Cilegon', 730),
    (3324, 'Cianjur', 730),
    (3325, 'Ciparay', 730),
    (3326, 'Citeureup', 730),
    (3327, 'Cibinong', 730),
    (3328, 'Purwakarta', 730),
    (3329, 'Garut', 730),
    (3330, 'Majalaya', 730),
    (3331, 'Pondok Aren', 730),
    (3332, 'Sawangan', 730),
    (3333, 'Yogyakarta', 731),
    (3334, 'Depok', 731),
    (3335, 'Hyderabad', 732),
    (3336, 'Vishakhapatnam', 732),
    (3337, 'Vijayawada', 732),
    (3338, 'Guntur', 732),
    (3339, 'Warangal', 732),
    (3340, 'Rajahmundry', 732),
    (3341, 'Nellore', 732),
    (3342, 'Kakinada', 732),
    (3343, 'Nizamabad', 732),
    (3344, 'Kurnool', 732),
    (3345, 'Ramagundam', 732),
    (3346, 'Eluru', 732),
    (3347, 'Kukatpalle', 732),
    (3348, 'Anantapur', 732),
    (3349, 'Tirupati', 732),
    (3350, 'Secunderabad', 732),
    (3351, 'Vizianagaram', 732),
    (3352, 'Machilipatnam (Masulipatam)', 732),
    (3353, 'Lalbahadur Nagar', 732),
    (3354, 'Karimnagar', 732),
    (3355, 'Tenali', 732),
    (3356, 'Adoni', 732),
    (3357, 'Proddatur', 732),
    (3358, 'Chittoor', 732),
    (3359, 'Khammam', 732),
    (3360, 'Malkajgiri', 732),
    (3361, 'Cuddapah', 732),
    (3362, 'Bhimavaram', 732),
    (3363, 'Nandyal', 732),
    (3364, 'Mahbubnagar', 732),
    (3365, 'Guntakal', 732),
    (3366, 'Qutubullapur', 732),
    (3367, 'Hindupur', 732),
    (3368, 'Gudivada', 732),
    (3369, 'Ongole', 732),
    (3370, 'Guwahati (Gauhati)', 733),
    (3371, 'Dibrugarh', 733),
    (3372, 'Silchar', 733),
    (3373, 'Nagaon', 733),
    (3374, 'Patna', 734),
    (3375, 'Gaya', 734),
    (3376, 'Bhagalpur', 734),
    (3377, 'Muzaffarpur', 734),
    (3378, 'Darbhanga', 734),
    (3379, 'Bihar Sharif', 734),
    (3380, 'Arrah (Ara)', 734),
    (3381, 'Katihar', 734),
    (3382, 'Munger (Monghyr)', 734),
    (3383, 'Chapra', 734),
    (3384, 'Sasaram', 734),
    (3385, 'Dehri', 734),
    (3386, 'Bettiah', 734),
    (3387, 'Chandigarh', 735),
    (3388, 'Raipur', 736),
    (3389, 'Bhilai', 736),
    (3390, 'Bilaspur', 736),
    (3391, 'Durg', 736),
    (3392, 'Raj Nandgaon', 736),
    (3393, 'Korba', 736),
    (3394, 'Raigarh', 736),
    (3395, 'Delhi', 737),
    (3396, 'New Delhi', 737),
    (3397, 'Delhi Cantonment', 737),
    (3398, 'Ahmedabad', 738),
    (3399, 'Surat', 738),
    (3400, 'Vadodara (Baroda)', 738),
    (3401, 'Rajkot', 738),
    (3402, 'Bhavnagar', 738),
    (3403, 'Jamnagar', 738),
    (3404, 'Nadiad', 738),
    (3405, 'Bharuch (Broach)', 738),
    (3406, 'Junagadh', 738),
    (3407, 'Navsari', 738),
    (3408, 'Gandhinagar', 738),
    (3409, 'Veraval', 738),
    (3410, 'Porbandar', 738),
    (3411, 'Anand', 738),
    (3412, 'Surendranagar', 738),
    (3413, 'Gandhidham', 738),
    (3414, 'Bhuj', 738),
    (3415, 'Godhra', 738),
    (3416, 'Patan', 738),
    (3417, 'Morvi', 738),
    (3418, 'Vejalpur', 738),
    (3419, 'Faridabad', 739),
    (3420, 'Rohtak', 739),
    (3421, 'Panipat', 739),
    (3422, 'Karnal', 739),
    (3423, 'Hisar (Hissar)', 739),
    (3424, 'Yamuna Nagar', 739),
    (3425, 'Sonipat (Sonepat)', 739),
    (3426, 'Gurgaon', 739),
    (3427, 'Sirsa', 739),
    (3428, 'Ambala', 739),
    (3429, 'Bhiwani', 739),
    (3430, 'Ambala Sadar', 739),
    (3431, 'Srinagar', 740),
    (3432, 'Jammu', 740),
    (3433, 'Ranchi', 741),
    (3434, 'Jamshedpur', 741),
    (3435, 'Bokaro Steel City', 741),
    (3436, 'Dhanbad', 741),
    (3437, 'Purnea (Purnia)', 741),
    (3438, 'Mango', 741),
    (3439, 'Hazaribag', 741),
    (3440, 'Purulia', 741),
    (3441, 'Bangalore', 742),
    (3442, 'Hubli-Dharwad', 742),
    (3443, 'Mysore', 742),
    (3444, 'Belgaum', 742),
    (3445, 'Gulbarga', 742),
    (3446, 'Mangalore', 742),
    (3447, 'Davangere', 742),
    (3448, 'Bellary', 742),
    (3449, 'Bijapur', 742),
    (3450, 'Shimoga', 742),
    (3451, 'Raichur', 742),
    (3452, 'Timkur', 742),
    (3453, 'Gadag Betigeri', 742),
    (3454, 'Mandya', 742),
    (3455, 'Bidar', 742),
    (3456, 'Hospet', 742),
    (3457, 'Hassan', 742),
    (3458, 'Cochin (Kochi)', 743),
    (3459, 'Thiruvananthapuram (Trivandrum', 743),
    (3460, 'Calicut (Kozhikode)', 743),
    (3461, 'Allappuzha (Alleppey)', 743),
    (3462, 'Kollam (Quilon)', 743),
    (3463, 'Palghat (Palakkad)', 743),
    (3464, 'Tellicherry (Thalassery)', 743),
    (3465, 'Indore', 744),
    (3466, 'Bhopal', 744),
    (3467, 'Jabalpur', 744),
    (3468, 'Gwalior', 744),
    (3469, 'Ujjain', 744),
    (3470, 'Sagar', 744),
    (3471, 'Ratlam', 744),
    (3472, 'Burhanpur', 744),
    (3473, 'Dewas', 744),
    (3474, 'Murwara (Katni)', 744),
    (3475, 'Satna', 744),
    (3476, 'Morena', 744),
    (3477, 'Khandwa', 744),
    (3478, 'Rewa', 744),
    (3479, 'Bhind', 744),
    (3480, 'Shivapuri', 744),
    (3481, 'Guna', 744),
    (3482, 'Mandasor', 744),
    (3483, 'Damoh', 744),
    (3484, 'Chhindwara', 744),
    (3485, 'Vidisha', 744),
    (3486, 'Mumbai (Bombay)', 745),
    (3487, 'Nagpur', 745),
    (3488, 'Pune', 745),
    (3489, 'Kalyan', 745),
    (3490, 'Thane (Thana)', 745),
    (3491, 'Nashik (Nasik)', 745),
    (3492, 'Solapur (Sholapur)', 745),
    (3493, 'Shambajinagar (Aurangabad)', 745),
    (3494, 'Pimpri-Chinchwad', 745),
    (3495, 'Amravati', 745),
    (3496, 'Kolhapur', 745),
    (3497, 'Bhiwandi', 745),
    (3498, 'Ulhasnagar', 745),
    (3499, 'Malegaon', 745),
    (3500, 'Akola', 745),
    (3501, 'New Bombay', 745),
    (3502, 'Dhule (Dhulia)', 745),
    (3503, 'Nanded (Nander)', 745),
    (3504, 'Jalgaon', 745),
    (3505, 'Chandrapur', 745),
    (3506, 'Ichalkaranji', 745),
    (3507, 'Latur', 745),
    (3508, 'Sangli', 745),
    (3509, 'Parbhani', 745),
    (3510, 'Ahmadnagar', 745),
    (3511, 'Mira Bhayandar', 745),
    (3512, 'Jalna', 745),
    (3513, 'Bhusawal', 745),
    (3514, 'Miraj', 745),
    (3515, 'Bhir (Bid)', 745),
    (3516, 'Gondiya', 745),
    (3517, 'Yeotmal (Yavatmal)', 745),
    (3518, 'Wardha', 745),
    (3519, 'Achalpur', 745),
    (3520, 'Satara', 745),
    (3521, 'Imphal', 746),
    (3522, 'Shillong', 747),
    (3523, 'Aizawl', 748),
    (3524, 'Bhubaneswar', 749),
    (3525, 'Kataka (Cuttack)', 749),
    (3526, 'Raurkela', 749),
    (3527, 'Brahmapur', 749),
    (3528, 'Raurkela Civil Township', 749),
    (3529, 'Sambalpur', 749),
    (3530, 'Puri', 749),
    (3531, 'Pondicherry', 750),
    (3532, 'Ludhiana', 751),
    (3533, 'Amritsar', 751),
    (3534, 'Jalandhar (Jullundur)', 751),
    (3535, 'Patiala', 751),
    (3536, 'Bhatinda (Bathinda)', 751),
    (3537, 'Pathankot', 751),
    (3538, 'Hoshiarpur', 751),
    (3539, 'Moga', 751),
    (3540, 'Abohar', 751),
    (3541, 'Lahore', 751),
    (3542, 'Faisalabad', 751),
    (3543, 'Rawalpindi', 751),
    (3544, 'Multan', 751),
    (3545, 'Gujranwala', 751),
    (3546, 'Sargodha', 751),
    (3547, 'Sialkot', 751),
    (3548, 'Bahawalpur', 751),
    (3549, 'Jhang', 751),
    (3550, 'Sheikhupura', 751),
    (3551, 'Gujrat', 751),
    (3552, 'Kasur', 751),
    (3553, 'Rahim Yar Khan', 751),
    (3554, 'Sahiwal', 751),
    (3555, 'Okara', 751),
    (3556, 'Wah', 751),
    (3557, 'Dera Ghazi Khan', 751),
    (3558, 'Chiniot', 751),
    (3559, 'Kamoke', 751),
    (3560, 'Mandi Burewala', 751),
    (3561, 'Jhelum', 751),
    (3562, 'Sadiqabad', 751),
    (3563, 'Khanewal', 751),
    (3564, 'Hafizabad', 751),
    (3565, 'Muzaffargarh', 751),
    (3566, 'Khanpur', 751),
    (3567, 'Gojra', 751),
    (3568, 'Bahawalnagar', 751),
    (3569, 'Muridke', 751),
    (3570, 'Pak Pattan', 751),
    (3571, 'Jaranwala', 751),
    (3572, 'Chishtian Mandi', 751),
    (3573, 'Daska', 751),
    (3574, 'Mandi Bahauddin', 751),
    (3575, 'Ahmadpur East', 751),
    (3576, 'Kamalia', 751),
    (3577, 'Vihari', 751),
    (3578, 'Wazirabad', 751),
    (3579, 'Jaipur', 752),
    (3580, 'Jodhpur', 752),
    (3581, 'Kota', 752),
    (3582, 'Bikaner', 752),
    (3583, 'Ajmer', 752),
    (3584, 'Udaipur', 752),
    (3585, 'Alwar', 752),
    (3586, 'Bhilwara', 752),
    (3587, 'Ganganagar', 752),
    (3588, 'Bharatpur', 752),
    (3589, 'Sikar', 752),
    (3590, 'Pali', 752),
    (3591, 'Beawar', 752),
    (3592, 'Tonk', 752),
    (3593, 'Chennai (Madras)', 753),
    (3594, 'Madurai', 753),
    (3595, 'Coimbatore', 753),
    (3596, 'Tiruchirapalli', 753),
    (3597, 'Salem', 753),
    (3598, 'Tiruppur (Tirupper)', 753),
    (3599, 'Ambattur', 753),
    (3600, 'Thanjavur', 753),
    (3601, 'Tuticorin', 753),
    (3602, 'Nagar Coil', 753),
    (3603, 'Avadi', 753),
    (3604, 'Dindigul', 753),
    (3605, 'Vellore', 753),
    (3606, 'Tiruvottiyur', 753),
    (3607, 'Erode', 753),
    (3608, 'Cuddalore', 753),
    (3609, 'Kanchipuram', 753),
    (3610, 'Kumbakonam', 753),
    (3611, 'Tirunelveli', 753),
    (3612, 'Alandur', 753),
    (3613, 'Neyveli', 753),
    (3614, 'Rajapalaiyam', 753),
    (3615, 'Pallavaram', 753),
    (3616, 'Tiruvannamalai', 753),
    (3617, 'Tambaram', 753),
    (3618, 'Valparai', 753),
    (3619, 'Pudukkottai', 753),
    (3620, 'Palayankottai', 753),
    (3621, 'Agartala', 754),
    (3622, 'Kanpur', 755),
    (3623, 'Lucknow', 755),
    (3624, 'Varanasi (Benares)', 755),
    (3625, 'Agra', 755),
    (3626, 'Allahabad', 755),
    (3627, 'Meerut', 755),
    (3628, 'Bareilly', 755),
    (3629, 'Gorakhpur', 755),
    (3630, 'Aligarh', 755),
    (3631, 'Ghaziabad', 755),
    (3632, 'Moradabad', 755),
    (3633, 'Saharanpur', 755),
    (3634, 'Jhansi', 755),
    (3635, 'Rampur', 755),
    (3636, 'Muzaffarnagar', 755),
    (3637, 'Shahjahanpur', 755),
    (3638, 'Mathura', 755),
    (3639, 'Firozabad', 755),
    (3640, 'Farrukhabad-cum-Fatehgarh', 755),
    (3641, 'Mirzapur-cum-Vindhyachal', 755),
    (3642, 'Sambhal', 755),
    (3643, 'Noida', 755),
    (3644, 'Hapur', 755),
    (3645, 'Amroha', 755),
    (3646, 'Maunath Bhanjan', 755),
    (3647, 'Jaunpur', 755),
    (3648, 'Bahraich', 755),
    (3649, 'Rae Bareli', 755),
    (3650, 'Bulandshahr', 755),
    (3651, 'Faizabad', 755),
    (3652, 'Etawah', 755),
    (3653, 'Sitapur', 755),
    (3654, 'Fatehpur', 755),
    (3655, 'Budaun', 755),
    (3656, 'Hathras', 755),
    (3657, 'Unnao', 755),
    (3658, 'Pilibhit', 755),
    (3659, 'Gonda', 755),
    (3660, 'Modinagar', 755),
    (3661, 'Orai', 755),
    (3662, 'Banda', 755),
    (3663, 'Meerut Cantonment', 755),
    (3664, 'Kanpur Cantonment', 755),
    (3665, 'Dehra Dun', 756),
    (3666, 'Hardwar (Haridwar)', 756),
    (3667, 'Haldwani-cum-Kathgodam', 756),
    (3668, 'Calcutta [Kolkata]', 757),
    (3669, 'Haora (Howrah)', 757),
    (3670, 'Durgapur', 757),
    (3671, 'Bhatpara', 757),
    (3672, 'Panihati', 757),
    (3673, 'Kamarhati', 757),
    (3674, 'Asansol', 757),
    (3675, 'Barddhaman (Burdwan)', 757),
    (3676, 'South Dum Dum', 757),
    (3677, 'Barahanagar (Baranagar)', 757),
    (3678, 'Siliguri (Shiliguri)', 757),
    (3679, 'Bally', 757),
    (3680, 'Kharagpur', 757),
    (3681, 'Burnpur', 757),
    (3682, 'Uluberia', 757),
    (3683, 'Hugli-Chinsurah', 757),
    (3684, 'Raiganj', 757),
    (3685, 'North Dum Dum', 757),
    (3686, 'Dabgram', 757),
    (3687, 'Ingraj Bazar (English Bazar)', 757),
    (3688, 'Serampore', 757),
    (3689, 'Barrackpur', 757),
    (3690, 'Naihati', 757),
    (3691, 'Midnapore (Medinipur)', 757),
    (3692, 'Navadwip', 757),
    (3693, 'Krishnanagar', 757),
    (3694, 'Chandannagar', 757),
    (3695, 'Balurghat', 757),
    (3696, 'Berhampore (Baharampur)', 757),
    (3697, 'Bankura', 757),
    (3698, 'Titagarh', 757),
    (3699, 'Halisahar', 757),
    (3700, 'Santipur', 757),
    (3701, 'Kulti-Barakar', 757),
    (3702, 'Barasat', 757),
    (3703, 'Rishra', 757),
    (3704, 'Basirhat', 757),
    (3705, 'Uttarpara-Kotrung', 757),
    (3706, 'North Barrackpur', 757),
    (3707, 'Haldia', 757),
    (3708, 'Habra', 757),
    (3709, 'Kanchrapara', 757),
    (3710, 'Champdani', 757),
    (3711, 'Ashoknagar-Kalyangarh', 757),
    (3712, 'Bansberia', 757),
    (3713, 'Baidyabati', 757),
    (3714, 'Dublin', 758),
    (3715, 'Cork', 759),
    (3716, 'Ardebil', 760),
    (3717, 'Bushehr', 761),
    (3718, 'Shahr-e Kord', 762),
    (3719, 'Tabriz', 763),
    (3720, 'Maragheh', 763),
    (3721, 'Marand', 763),
    (3722, 'Esfahan', 764),
    (3723, 'Kashan', 764),
    (3724, 'Najafabad', 764),
    (3725, 'Khomeynishahr', 764),
    (3726, 'Qomsheh', 764),
    (3727, 'Shiraz', 765),
    (3728, 'Marv Dasht', 765),
    (3729, 'Jahrom', 765),
    (3730, 'Rasht', 766),
    (3731, 'Bandar-e Anzali', 766),
    (3732, 'Gorgan', 767),
    (3733, 'Hamadan', 768),
    (3734, 'Malayer', 768),
    (3735, 'Bandar-e-Abbas', 769),
    (3736, 'Ilam', 770),
    (3737, 'Kerman', 771),
    (3738, 'Sirjan', 771),
    (3739, 'Rafsanjan', 771),
    (3740, 'Kermanshah', 772),
    (3741, 'Mashhad', 773),
    (3742, 'Sabzevar', 773),
    (3743, 'Neyshabur', 773),
    (3744, 'Bojnurd', 773),
    (3745, 'Birjand', 773),
    (3746, 'Torbat-e Heydariyeh', 773),
    (3747, 'Ahvaz', 774),
    (3748, 'Abadan', 774),
    (3749, 'Dezful', 774),
    (3750, 'Masjed-e-Soleyman', 774),
    (3751, 'Andimeshk', 774),
    (3752, 'Khorramshahr', 774),
    (3753, 'Sanandaj', 775),
    (3754, 'Saqqez', 775),
    (3755, 'Khorramabad', 776),
    (3756, 'Borujerd', 776),
    (3757, 'Arak', 777),
    (3758, 'Sari', 778),
    (3759, 'Amol', 778),
    (3760, 'Babol', 778),
    (3761, 'Qaemshahr', 778),
    (3762, 'Gonbad-e Qabus', 778),
    (3763, 'Qazvin', 779),
    (3764, 'Qom', 780),
    (3765, 'Saveh', 780),
    (3766, 'Shahrud', 781),
    (3767, 'Semnan', 781),
    (3768, 'Zahedan', 782),
    (3769, 'Zabol', 782),
    (3770, 'Teheran', 783),
    (3771, 'Karaj', 783),
    (3772, 'Eslamshahr', 783),
    (3773, 'Qarchak', 783),
    (3774, 'Qods', 783),
    (3775, 'Varamin', 783),
    (3776, 'Urmia', 784),
    (3777, 'Khoy', 784),
    (3778, 'Bukan', 784),
    (3779, 'Mahabad', 784),
    (3780, 'Miandoab', 784),
    (3781, 'Yazd', 785),
    (3782, 'Zanjan', 786),
    (3783, 'al-Ramadi', 787),
    (3784, 'al-Najaf', 788),
    (3785, 'al-Diwaniya', 789),
    (3786, 'al-Sulaymaniya', 790),
    (3787, 'Kirkuk', 791),
    (3788, 'al-Hilla', 792),
    (3789, 'Baghdad', 793),
    (3790, 'Basra', 794),
    (3791, 'al-Nasiriya', 795),
    (3792, 'Baquba', 796),
    (3793, 'Irbil', 797),
    (3794, 'Karbala', 798),
    (3795, 'al-Amara', 799),
    (3796, 'Mosul', 800),
    (3797, 'al-Kut', 801),
    (3798, 'ReykjavÃ­k', 802),
    (3799, 'Beerseba', 803),
    (3800, 'Ashdod', 803),
    (3801, 'Ashqelon', 803),
    (3802, 'Rishon Le Ziyyon', 804),
    (3803, 'Petah Tiqwa', 804),
    (3804, 'Netanya', 804),
    (3805, 'Rehovot', 804),
    (3806, 'Haifa', 805),
    (3807, 'Jerusalem', 806),
    (3808, 'Tel Aviv-Jaffa', 807),
    (3809, 'Holon', 807),
    (3810, 'Bat Yam', 807),
    (3811, 'Bene Beraq', 807),
    (3812, 'Ramat Gan', 807),
    (3813, 'Pescara', 808),
    (3814, 'Bari', 809),
    (3815, 'Taranto', 809),
    (3816, 'Foggia', 809),
    (3817, 'Lecce', 809),
    (3818, 'Andria', 809),
    (3819, 'Brindisi', 809),
    (3820, 'Barletta', 809),
    (3821, 'Reggio di Calabria', 810),
    (3822, 'Catanzaro', 810),
    (3823, 'Napoli', 811),
    (3824, 'Salerno', 811),
    (3825, 'Torre del Greco', 811),
    (3826, 'Giugliano in Campania', 811),
    (3827, 'Bologna', 812),
    (3828, 'Modena', 812),
    (3829, 'Parma', 812),
    (3830, 'Reggio nellÂ´ Emilia', 812),
    (3831, 'Ravenna', 812),
    (3832, 'Ferrara', 812),
    (3833, 'Rimini', 812),
    (3834, 'ForlÃ¬', 812),
    (3835, 'Piacenza', 812),
    (3836, 'Cesena', 812),
    (3837, 'Trieste', 813),
    (3838, 'Udine', 813),
    (3839, 'Roma', 814),
    (3840, 'Latina', 814),
    (3841, 'Genova', 815),
    (3842, 'La Spezia', 815),
    (3843, 'Milano', 816),
    (3844, 'Brescia', 816),
    (3845, 'Monza', 816),
    (3846, 'Bergamo', 816),
    (3847, 'Ancona', 817),
    (3848, 'Pesaro', 817),
    (3849, 'Torino', 818),
    (3850, 'Novara', 818),
    (3851, 'Alessandria', 818),
    (3852, 'Cagliari', 819),
    (3853, 'Sassari', 819),
    (3854, 'Palermo', 820),
    (3855, 'Catania', 820),
    (3856, 'Messina', 820),
    (3857, 'Syrakusa', 820),
    (3858, 'Firenze', 821),
    (3859, 'Prato', 821),
    (3860, 'Livorno', 821),
    (3861, 'Pisa', 821),
    (3862, 'Arezzo', 821),
    (3863, 'Trento', 822),
    (3864, 'Bolzano', 822),
    (3865, 'Perugia', 823),
    (3866, 'Terni', 823),
    (3867, 'Venezia', 824),
    (3868, 'Verona', 824),
    (3869, 'Padova', 824),
    (3870, 'Vicenza', 824),
    (3871, 'Kingston', 825),
    (3872, 'Portmore', 825),
    (3873, 'Spanish Town', 826),
    (3874, 'al-Zarqa', 827),
    (3875, 'al-Rusayfa', 827),
    (3876, 'Amman', 828),
    (3877, 'Wadi al-Sir', 828),
    (3878, 'Irbid', 829),
    (3879, 'Nagoya', 830),
    (3880, 'Toyohashi', 830),
    (3881, 'Toyota', 830),
    (3882, 'Okazaki', 830),
    (3883, 'Kasugai', 830),
    (3884, 'Ichinomiya', 830),
    (3885, 'Anjo', 830),
    (3886, 'Komaki', 830),
    (3887, 'Seto', 830),
    (3888, 'Kariya', 830),
    (3889, 'Toyokawa', 830),
    (3890, 'Handa', 830),
    (3891, 'Tokai', 830),
    (3892, 'Inazawa', 830),
    (3893, 'Konan', 830),
    (3894, 'Akita', 831),
    (3895, 'Aomori', 832),
    (3896, 'Hachinohe', 832),
    (3897, 'Hirosaki', 832),
    (3898, 'Chiba', 833),
    (3899, 'Funabashi', 833),
    (3900, 'Matsudo', 833),
    (3901, 'Ichikawa', 833),
    (3902, 'Kashiwa', 833),
    (3903, 'Ichihara', 833),
    (3904, 'Sakura', 833),
    (3905, 'Yachiyo', 833),
    (3906, 'Narashino', 833),
    (3907, 'Nagareyama', 833),
    (3908, 'Urayasu', 833),
    (3909, 'Abiko', 833),
    (3910, 'Kisarazu', 833),
    (3911, 'Noda', 833),
    (3912, 'Kamagaya', 833),
    (3913, 'Nishio', 833),
    (3914, 'Kimitsu', 833),
    (3915, 'Mobara', 833),
    (3916, 'Narita', 833),
    (3917, 'Matsuyama', 834),
    (3918, 'Niihama', 834),
    (3919, 'Imabari', 834),
    (3920, 'Fukui', 835),
    (3921, 'Fukuoka', 836),
    (3922, 'Kitakyushu', 836),
    (3923, 'Kurume', 836),
    (3924, 'Omuta', 836),
    (3925, 'Kasuga', 836),
    (3926, 'Iwaki', 837),
    (3927, 'Koriyama', 837),
    (3928, 'Fukushima', 837),
    (3929, 'Aizuwakamatsu', 837),
    (3930, 'Gifu', 838),
    (3931, 'Ogaki', 838),
    (3932, 'Kakamigahara', 838),
    (3933, 'Tajimi', 838),
    (3934, 'Maebashi', 839),
    (3935, 'Takasaki', 839),
    (3936, 'Ota', 839),
    (3937, 'Isesaki', 839),
    (3938, 'Kiryu', 839),
    (3939, 'Hiroshima', 840),
    (3940, 'Fukuyama', 840),
    (3941, 'Kure', 840),
    (3942, 'Higashihiroshima', 840),
    (3943, 'Onomichi', 840),
    (3944, 'Sapporo', 841),
    (3945, 'Asahikawa', 841),
    (3946, 'Hakodate', 841),
    (3947, 'Kushiro', 841),
    (3948, 'Obihiro', 841),
    (3949, 'Tomakomai', 841),
    (3950, 'Otaru', 841),
    (3951, 'Ebetsu', 841),
    (3952, 'Kitami', 841),
    (3953, 'Muroran', 841),
    (3954, 'Kobe', 842),
    (3955, 'Amagasaki', 842),
    (3956, 'Himeji', 842),
    (3957, 'Nishinomiya', 842),
    (3958, 'Akashi', 842),
    (3959, 'Kakogawa', 842),
    (3960, 'Takarazuka', 842),
    (3961, 'Itami', 842),
    (3962, 'Kawanishi', 842),
    (3963, 'Sanda', 842),
    (3964, 'Takasago', 842),
    (3965, 'Mito', 843),
    (3966, 'Hitachi', 843),
    (3967, 'Tsukuba', 843),
    (3968, 'Tama', 843),
    (3969, 'Tsuchiura', 843),
    (3970, 'Kanazawa', 844),
    (3971, 'Komatsu', 844),
    (3972, 'Morioka', 845),
    (3973, 'Takamatsu', 846),
    (3974, 'Kagoshima', 847),
    (3975, 'Jokohama [Yokohama]', 848),
    (3976, 'Kawasaki', 848),
    (3977, 'Sagamihara', 848),
    (3978, 'Yokosuka', 848),
    (3979, 'Fujisawa', 848),
    (3980, 'Hiratsuka', 848),
    (3981, 'Chigasaki', 848),
    (3982, 'Atsugi', 848),
    (3983, 'Yamato', 848),
    (3984, 'Odawara', 848),
    (3985, 'Kamakura', 848),
    (3986, 'Hadano', 848),
    (3987, 'Zama', 848),
    (3988, 'Ebina', 848),
    (3989, 'Isehara', 848),
    (3990, 'Kochi', 849),
    (3991, 'Kumamoto', 850),
    (3992, 'Yatsushiro', 850),
    (3993, 'Kioto', 851),
    (3994, 'Uji', 851),
    (3995, 'Maizuru', 851),
    (3996, 'Kameoka', 851),
    (3997, 'Yokkaichi', 852),
    (3998, 'Suzuka', 852),
    (3999, 'Tsu', 852),
    (4000, 'Matsusaka', 852),
    (4001, 'Kuwana', 852),
    (4002, 'Ise', 852),
    (4003, 'Sendai', 853),
    (4004, 'Ishinomaki', 853),
    (4005, 'Miyazaki', 854),
    (4006, 'Miyakonojo', 854),
    (4007, 'Nobeoka', 854),
    (4008, 'Nagano', 855),
    (4009, 'Matsumoto', 855),
    (4010, 'Ueda', 855),
    (4011, 'Iida', 855),
    (4012, 'Nagasaki', 856),
    (4013, 'Sasebo', 856),
    (4014, 'Isahaya', 856),
    (4015, 'Nara', 857),
    (4016, 'Kashihara', 857),
    (4017, 'Ikoma', 857),
    (4018, 'Yamatokoriyama', 857),
    (4019, 'Niigata', 858),
    (4020, 'Nagaoka', 858),
    (4021, 'Joetsu', 858),
    (4022, 'Kashiwazaki', 858),
    (4023, 'Oita', 859),
    (4024, 'Beppu', 859),
    (4025, 'Okayama', 860),
    (4026, 'Kurashiki', 860),
    (4027, 'Tsuyama', 860),
    (4028, 'Naha', 861),
    (4029, 'Okinawa', 861),
    (4030, 'Urasoe', 861),
    (4031, 'Osaka', 862),
    (4032, 'Sakai', 862),
    (4033, 'Higashiosaka', 862),
    (4034, 'Hirakata', 862),
    (4035, 'Toyonaka', 862),
    (4036, 'Takatsuki', 862),
    (4037, 'Suita', 862),
    (4038, 'Yao', 862),
    (4039, 'Ibaraki', 862),
    (4040, 'Neyagawa', 862),
    (4041, 'Kishiwada', 862),
    (4042, 'Izumi', 862),
    (4043, 'Moriguchi', 862),
    (4044, 'Kadoma', 862),
    (4045, 'Matsubara', 862),
    (4046, 'Daito', 862),
    (4047, 'Minoo', 862),
    (4048, 'Tondabayashi', 862),
    (4049, 'Kawachinagano', 862),
    (4050, 'Habikino', 862),
    (4051, 'Ikeda', 862),
    (4052, 'Izumisano', 862),
    (4053, 'Saga', 863),
    (4054, 'Urawa', 864),
    (4055, 'Kawaguchi', 864),
    (4056, 'Omiya', 864),
    (4057, 'Kawagoe', 864),
    (4058, 'Tokorozawa', 864),
    (4059, 'Koshigaya', 864),
    (4060, 'Soka', 864),
    (4061, 'Ageo', 864),
    (4062, 'Kasukabe', 864),
    (4063, 'Sayama', 864),
    (4064, 'Kumagaya', 864),
    (4065, 'Niiza', 864),
    (4066, 'Iruma', 864),
    (4067, 'Misato', 864),
    (4068, 'Asaka', 864),
    (4069, 'Iwatsuki', 864),
    (4070, 'Toda', 864),
    (4071, 'Fukaya', 864),
    (4072, 'Sakado', 864),
    (4073, 'Fujimi', 864),
    (4074, 'Higashimatsuyama', 864),
    (4075, 'Otsu', 865),
    (4076, 'Kusatsu', 865),
    (4077, 'Hikone', 865),
    (4078, 'Matsue', 866),
    (4079, 'Hamamatsu', 867),
    (4080, 'Shizuoka', 867),
    (4081, 'Shimizu', 867),
    (4082, 'Fuji', 867),
    (4083, 'Numazu', 867),
    (4084, 'Fujieda', 867),
    (4085, 'Fujinomiya', 867),
    (4086, 'Yaizu', 867),
    (4087, 'Mishima', 867),
    (4088, 'Utsunomiya', 868),
    (4089, 'Ashikaga', 868),
    (4090, 'Oyama', 868),
    (4091, 'Kanuma', 868),
    (4092, 'Tokushima', 869),
    (4093, 'Tokyo', 870),
    (4094, 'Hachioji', 870),
    (4095, 'Machida', 870),
    (4096, 'Fuchu', 870),
    (4097, 'Chofu', 870),
    (4098, 'Kodaira', 870),
    (4099, 'Mitaka', 870),
    (4100, 'Hino', 870),
    (4101, 'Tachikawa', 870),
    (4102, 'Hitachinaka', 870),
    (4103, 'Ome', 870),
    (4104, 'Higashimurayama', 870),
    (4105, 'Musashino', 870),
    (4106, 'Higashikurume', 870),
    (4107, 'Koganei', 870),
    (4108, 'Kokubunji', 870),
    (4109, 'Akishima', 870),
    (4110, 'Hoya', 870),
    (4111, 'Tottori', 871),
    (4112, 'Yonago', 871),
    (4113, 'Toyama', 872),
    (4114, 'Takaoka', 872),
    (4115, 'Wakayama', 873),
    (4116, 'Yamagata', 874),
    (4117, 'Sakata', 874),
    (4118, 'Tsuruoka', 874),
    (4119, 'Yonezawa', 874),
    (4120, 'Shimonoseki', 875),
    (4121, 'Ube', 875),
    (4122, 'Yamaguchi', 875),
    (4123, 'Hofu', 875),
    (4124, 'Tokuyama', 875),
    (4125, 'Iwakuni', 875),
    (4126, 'Kofu', 876),
    (4127, 'Taldyqorghan', 877),
    (4128, 'Almaty', 878),
    (4129, 'AqtÃ¶be', 879),
    (4130, 'Astana', 880),
    (4131, 'Atyrau', 881),
    (4132, 'Ã–skemen', 882),
    (4133, 'Semey', 882),
    (4134, 'Aqtau', 883),
    (4135, 'Petropavl', 884),
    (4136, 'KÃ¶kshetau', 884),
    (4137, 'Pavlodar', 885),
    (4138, 'Ekibastuz', 885),
    (4139, 'Qaraghandy', 886),
    (4140, 'Temirtau', 886),
    (4141, 'Zhezqazghan', 886),
    (4142, 'Qostanay', 887),
    (4143, 'Rudnyy', 887),
    (4144, 'Qyzylorda', 888),
    (4145, 'Shymkent', 889),
    (4146, 'Taraz', 890),
    (4147, 'Oral', 891),
    (4148, 'Suva', 892),
    (4149, 'Nyeri', 892),
    (4150, 'Kathmandu', 892),
    (4151, 'Lalitapur', 892),
    (4152, 'Birgunj', 892),
    (4153, 'San Lorenzo', 892),
    (4154, 'LambarÃ©', 892),
    (4155, 'Fernando de la Mora', 892),
    (4156, 'Kabwe', 892),
    (4157, 'Kandy', 892),
    (4158, 'Kampala', 892),
    (4159, 'Mombasa', 893),
    (4160, 'Machakos', 894),
    (4161, 'Meru', 894),
    (4162, 'Biratnagar', 894),
    (4163, 'Nairobi', 895),
    (4164, 'Kisumu', 896),
    (4165, 'Nakuru', 897),
    (4166, 'Eldoret', 897),
    (4167, 'Bishkek', 898),
    (4168, 'Osh', 899),
    (4169, 'Battambang', 900),
    (4170, 'Phnom Penh', 901),
    (4171, 'Siem Reap', 902),
    (4172, 'Bikenibeu', 903),
    (4173, 'Bairiki', 903),
    (4174, 'Basseterre', 904),
    (4175, 'Cheju', 905),
    (4176, 'Chonju', 906),
    (4177, 'Iksan', 906),
    (4178, 'Kunsan', 906),
    (4179, 'Chong-up', 906),
    (4180, 'Kimje', 906),
    (4181, 'Namwon', 906),
    (4182, 'Sunchon', 907),
    (4183, 'Mokpo', 907),
    (4184, 'Yosu', 907),
    (4185, 'Kwang-yang', 907),
    (4186, 'Naju', 907),
    (4187, 'Chongju', 908),
    (4188, 'Chungju', 908),
    (4189, 'Chechon', 908),
    (4190, 'Chonan', 909),
    (4191, 'Asan', 909),
    (4192, 'Nonsan', 909),
    (4193, 'Sosan', 909),
    (4194, 'Kongju', 909),
    (4195, 'Poryong', 909),
    (4196, 'Inchon', 910),
    (4197, 'Wonju', 911),
    (4198, 'Chunchon', 911),
    (4199, 'Kangnung', 911),
    (4200, 'Tonghae', 911),
    (4201, 'Kwangju', 912),
    (4202, 'Songnam', 913),
    (4203, 'Puchon', 913),
    (4204, 'Suwon', 913),
    (4205, 'Anyang', 913),
    (4206, 'Koyang', 913),
    (4207, 'Ansan', 913),
    (4208, 'Kwangmyong', 913),
    (4209, 'Pyongtaek', 913),
    (4210, 'Uijongbu', 913),
    (4211, 'Yong-in', 913),
    (4212, 'Kunpo', 913),
    (4213, 'Namyangju', 913),
    (4214, 'Paju', 913),
    (4215, 'Ichon', 913),
    (4216, 'Kuri', 913),
    (4217, 'Shihung', 913),
    (4218, 'Hanam', 913),
    (4219, 'Uiwang', 913),
    (4220, 'Pohang', 914),
    (4221, 'Kumi', 914),
    (4222, 'Kyongju', 914),
    (4223, 'Andong', 914),
    (4224, 'Kyongsan', 914),
    (4225, 'Kimchon', 914),
    (4226, 'Yongju', 914),
    (4227, 'Sangju', 914),
    (4228, 'Yongchon', 914),
    (4229, 'Mun-gyong', 914),
    (4230, 'Ulsan', 915),
    (4231, 'Chang-won', 915),
    (4232, 'Masan', 915),
    (4233, 'Chinju', 915),
    (4234, 'Kimhae', 915),
    (4235, 'Yangsan', 915),
    (4236, 'Koje', 915),
    (4237, 'Tong-yong', 915),
    (4238, 'Chinhae', 915),
    (4239, 'Miryang', 915),
    (4240, 'Sachon', 915),
    (4241, 'Pusan', 916),
    (4242, 'Seoul', 917),
    (4243, 'Taegu', 918),
    (4244, 'Taejon', 919),
    (4245, 'Kuwait', 920),
    (4246, 'al-Salimiya', 921),
    (4247, 'Jalib al-Shuyukh', 921),
    (4248, 'Savannakhet', 922),
    (4249, 'Vientiane', 923),
    (4250, 'Tripoli', 924),
    (4251, 'Beirut', 925),
    (4252, 'Monrovia', 926),
    (4253, 'al-Zawiya', 927),
    (4254, 'Bengasi', 928),
    (4255, 'Misrata', 929),
    (4256, 'Tripoli', 930),
    (4257, 'Castries', 931),
    (4258, 'Schaan', 932),
    (4259, 'Vaduz', 933),
    (4260, 'Suva', 934),
    (4261, 'Nyeri', 934),
    (4262, 'Kathmandu', 934),
    (4263, 'Lalitapur', 934),
    (4264, 'Birgunj', 934),
    (4265, 'San Lorenzo', 934),
    (4266, 'LambarÃ©', 934),
    (4267, 'Fernando de la Mora', 934),
    (4268, 'Kabwe', 934),
    (4269, 'Kandy', 934),
    (4270, 'Kampala', 934),
    (4271, 'Tamale', 935),
    (4272, 'Jaffna', 935),
    (4273, 'Sekondi-Takoradi', 936),
    (4274, 'Pokhara', 936),
    (4275, 'Freetown', 936),
    (4276, 'Colombo', 936),
    (4277, 'Dehiwala', 936),
    (4278, 'Moratuwa', 936),
    (4279, 'Sri Jayawardenepura Kotte', 936),
    (4280, 'Negombo', 936),
    (4281, 'Maseru', 937),
    (4282, 'Kaunas', 938),
    (4283, 'Klaipeda', 939),
    (4284, 'Panevezys', 940),
    (4285, 'Vilnius', 941),
    (4286, 'Å iauliai', 942),
    (4287, 'Luxembourg [Luxemburg/LÃ«tzebu', 943),
    (4288, 'Daugavpils', 944),
    (4289, 'Liepaja', 945),
    (4290, 'Riga', 946),
    (4291, 'Macao', 947),
    (4292, 'Casablanca', 948),
    (4293, 'Mohammedia', 948),
    (4294, 'Khouribga', 949),
    (4295, 'Settat', 949),
    (4296, 'Safi', 950),
    (4297, 'El Jadida', 950),
    (4298, 'FÃ¨s', 951),
    (4299, 'KÃ©nitra', 952),
    (4300, 'Marrakech', 953),
    (4301, 'MeknÃ¨s', 954),
    (4302, 'Oujda', 955),
    (4303, 'Nador', 955),
    (4304, 'Rabat', 956),
    (4305, 'SalÃ©', 956),
    (4306, 'TÃ©mara', 956),
    (4307, 'Agadir', 957),
    (4308, 'Beni-Mellal', 958),
    (4309, 'Tanger', 959),
    (4310, 'TÃ©touan', 959),
    (4311, 'Ksar el Kebir', 959),
    (4312, 'El Araich', 959),
    (4313, 'Taza', 960),
    (4314, 'South Hill', 961),
    (4315, 'The Valley', 961),
    (4316, 'Oranjestad', 961),
    (4317, 'Douglas', 961),
    (4318, 'Gibraltar', 961),
    (4319, 'Tamuning', 961),
    (4320, 'AgaÃ±a', 961),
    (4321, 'Flying Fish Cove', 961),
    (4322, 'Monte-Carlo', 961),
    (4323, 'Monaco-Ville', 961),
    (4324, 'Yangor', 961),
    (4325, 'Yaren', 961),
    (4326, 'Alofi', 961),
    (4327, 'Kingston', 961),
    (4328, 'Adamstown', 961),
    (4329, 'Singapore', 961),
    (4330, 'NoumÃ©a', 961),
    (4331, 'CittÃ  del Vaticano', 961),
    (4332, 'Balti', 962),
    (4333, 'Bender (TÃ®ghina)', 963),
    (4334, 'Chisinau', 964),
    (4335, 'Tiraspol', 965),
    (4336, 'Antananarivo', 966),
    (4337, 'AntsirabÃ©', 966),
    (4338, 'Fianarantsoa', 967),
    (4339, 'Mahajanga', 968),
    (4340, 'Toamasina', 969),
    (4341, 'Male', 970),
    (4342, 'Aguascalientes', 971),
    (4343, 'Tijuana', 972),
    (4344, 'Mexicali', 972),
    (4345, 'Ensenada', 972),
    (4346, 'La Paz', 973),
    (4347, 'Los Cabos', 973),
    (4348, 'Campeche', 974),
    (4349, 'Carmen', 974),
    (4350, 'Tuxtla GutiÃ©rrez', 975),
    (4351, 'Tapachula', 975),
    (4352, 'Ocosingo', 975),
    (4353, 'San CristÃ³bal de las Casas', 975),
    (4354, 'ComitÃ¡n de DomÃ­nguez', 975),
    (4355, 'Las Margaritas', 975),
    (4356, 'JuÃ¡rez', 976),
    (4357, 'Chihuahua', 976),
    (4358, 'CuauhtÃ©moc', 976),
    (4359, 'Delicias', 976),
    (4360, 'Hidalgo del Parral', 976),
    (4361, 'Saltillo', 977),
    (4362, 'TorreÃ³n', 977),
    (4363, 'Monclova', 977),
    (4364, 'Piedras Negras', 977),
    (4365, 'AcuÃ±a', 977),
    (4366, 'Matamoros', 977),
    (4367, 'Colima', 978),
    (4368, 'Manzanillo', 978),
    (4369, 'TecomÃ¡n', 978),
    (4370, 'Buenos Aires', 979),
    (4371, 'BrasÃ­lia', 979),
    (4372, 'Ciudad de MÃ©xico', 979),
    (4373, 'Caracas', 979),
    (4374, 'Catia La Mar', 979),
    (4375, 'Durango', 980),
    (4376, 'GÃ³mez Palacio', 980),
    (4377, 'Lerdo', 980),
    (4378, 'LeÃ³n', 981),
    (4379, 'Irapuato', 981),
    (4380, 'Celaya', 981),
    (4381, 'Salamanca', 981),
    (4382, 'PÃ©njamo', 981),
    (4383, 'Guanajuato', 981),
    (4384, 'Allende', 981),
    (4385, 'Silao', 981),
    (4386, 'Valle de Santiago', 981),
    (4387, 'Dolores Hidalgo', 981),
    (4388, 'AcÃ¡mbaro', 981),
    (4389, 'San Francisco del RincÃ³n', 981),
    (4390, 'San Luis de la Paz', 981),
    (4391, 'San Felipe', 981),
    (4392, 'Salvatierra', 981),
    (4393, 'Acapulco de JuÃ¡rez', 982),
    (4394, 'Chilpancingo de los Bravo', 982),
    (4395, 'Iguala de la Independencia', 982),
    (4396, 'Chilapa de Alvarez', 982),
    (4397, 'Taxco de AlarcÃ³n', 982),
    (4398, 'JosÃ© Azueta', 982),
    (4399, 'Pachuca de Soto', 983),
    (4400, 'Tulancingo de Bravo', 983),
    (4401, 'Huejutla de Reyes', 983),
    (4402, 'Guadalajara', 984),
    (4403, 'Zapopan', 984),
    (4404, 'Tlaquepaque', 984),
    (4405, 'TonalÃ¡', 984),
    (4406, 'Puerto Vallarta', 984),
    (4407, 'Lagos de Moreno', 984),
    (4408, 'Tlajomulco de ZÃºÃ±iga', 984),
    (4409, 'TepatitlÃ¡n de Morelos', 984),
    (4410, 'Ecatepec de Morelos', 985),
    (4411, 'NezahualcÃ³yotl', 985),
    (4412, 'Naucalpan de JuÃ¡rez', 985),
    (4413, 'Tlalnepantla de Baz', 985),
    (4414, 'Toluca', 985),
    (4415, 'ChimalhuacÃ¡n', 985),
    (4416, 'AtizapÃ¡n de Zaragoza', 985),
    (4417, 'CuautitlÃ¡n Izcalli', 985),
    (4418, 'TultitlÃ¡n', 985),
    (4419, 'Valle de Chalco Solidaridad', 985),
    (4420, 'Ixtapaluca', 985),
    (4421, 'NicolÃ¡s Romero', 985),
    (4422, 'Coacalco de BerriozÃ¡bal', 985),
    (4423, 'Chalco', 985),
    (4424, 'La Paz', 985),
    (4425, 'Texcoco', 985),
    (4426, 'Metepec', 985),
    (4427, 'Huixquilucan', 985),
    (4428, 'San Felipe del Progreso', 985),
    (4429, 'TecÃ¡mac', 985),
    (4430, 'Zinacantepec', 985),
    (4431, 'Ixtlahuaca', 985),
    (4432, 'Almoloya de JuÃ¡rez', 985),
    (4433, 'Zumpango', 985),
    (4434, 'Lerma', 985),
    (4435, 'Tejupilco', 985),
    (4436, 'Tultepec', 985),
    (4437, 'Morelia', 986),
    (4438, 'Uruapan', 986),
    (4439, 'LÃ¡zaro CÃ¡rdenas', 986),
    (4440, 'Zamora', 986),
    (4441, 'ZitÃ¡cuaro', 986),
    (4442, 'ApatzingÃ¡n', 986),
    (4443, 'Hidalgo', 986),
    (4444, 'Cuernavaca', 987),
    (4445, 'Jiutepec', 987),
    (4446, 'Cuautla', 987),
    (4447, 'Temixco', 987),
    (4448, 'Tepic', 988),
    (4449, 'Santiago Ixcuintla', 988),
    (4450, 'Monterrey', 989),
    (4451, 'Guadalupe', 989),
    (4452, 'San NicolÃ¡s de los Garza', 989),
    (4453, 'Apodaca', 989),
    (4454, 'General Escobedo', 989),
    (4455, 'Santa Catarina', 989),
    (4456, 'San Pedro Garza GarcÃ­a', 989),
    (4457, 'Oaxaca de JuÃ¡rez', 990),
    (4458, 'San Juan Bautista Tuxtepec', 990),
    (4459, 'Puebla', 991),
    (4460, 'TehuacÃ¡n', 991),
    (4461, 'San MartÃ­n Texmelucan', 991),
    (4462, 'Atlixco', 991),
    (4463, 'San Pedro Cholula', 991),
    (4464, 'San Juan del RÃ­o', 992),
    (4465, 'QuerÃ©taro', 993),
    (4466, 'Benito JuÃ¡rez', 994),
    (4467, 'OthÃ³n P. Blanco (Chetumal)', 994),
    (4468, 'San Luis PotosÃ­', 995),
    (4469, 'Soledad de Graciano SÃ¡nchez', 995),
    (4470, 'Ciudad Valles', 995),
    (4471, 'CuliacÃ¡n', 996),
    (4472, 'MazatlÃ¡n', 996),
    (4473, 'Ahome', 996),
    (4474, 'Guasave', 996),
    (4475, 'Navolato', 996),
    (4476, 'El Fuerte', 996),
    (4477, 'Hermosillo', 997),
    (4478, 'Cajeme', 997),
    (4479, 'Nogales', 997),
    (4480, 'San Luis RÃ­o Colorado', 997),
    (4481, 'Navojoa', 997),
    (4482, 'Guaymas', 997),
    (4483, 'Centro (Villahermosa)', 998),
    (4484, 'CÃ¡rdenas', 998),
    (4485, 'Comalcalco', 998),
    (4486, 'Huimanguillo', 998),
    (4487, 'Macuspana', 998),
    (4488, 'CunduacÃ¡n', 998),
    (4489, 'Reynosa', 999),
    (4490, 'Matamoros', 999),
    (4491, 'Nuevo Laredo', 999),
    (4492, 'Tampico', 999),
    (4493, 'Victoria', 999),
    (4494, 'Ciudad Madero', 999),
    (4495, 'Altamira', 999),
    (4496, 'El Mante', 999),
    (4497, 'RÃ­o Bravo', 999),
    (4498, 'Veracruz', 1000),
    (4499, 'Xalapa', 1000),
    (4500, 'Coatzacoalcos', 1000),
    (4501, 'CÃ³rdoba', 1000),
    (4502, 'Papantla', 1000),
    (4503, 'MinatitlÃ¡n', 1000),
    (4504, 'Poza Rica de Hidalgo', 1000),
    (4505, 'San AndrÃ©s Tuxtla', 1000),
    (4506, 'TÃºxpam', 1000),
    (4507, 'MartÃ­nez de la Torre', 1000),
    (4508, 'Orizaba', 1000),
    (4509, 'Temapache', 1000),
    (4510, 'Cosoleacaque', 1000),
    (4511, 'Tantoyuca', 1000),
    (4512, 'PÃ¡nuco', 1000),
    (4513, 'Tierra Blanca', 1000),
    (4514, 'Boca del RÃ­o', 1001),
    (4515, 'MÃ©rida', 1002),
    (4516, 'Fresnillo', 1003),
    (4517, 'Zacatecas', 1003),
    (4518, 'Guadalupe', 1003),
    (4519, 'Dalap-Uliga-Darrit', 1004),
    (4520, 'Skopje', 1005),
    (4521, 'Bamako', 1006),
    (4522, 'Valletta', 1007),
    (4523, 'Birkirkara', 1008),
    (4524, 'Bassein (Pathein)', 1009),
    (4525, 'Henzada (Hinthada)', 1009),
    (4526, 'Pagakku (Pakokku)', 1010),
    (4527, 'Mandalay', 1011),
    (4528, 'Meikhtila', 1011),
    (4529, 'Myingyan', 1011),
    (4530, 'Moulmein (Mawlamyine)', 1012),
    (4531, 'Pegu (Bago)', 1013),
    (4532, 'Prome (Pyay)', 1013),
    (4533, 'Sittwe (Akyab)', 1014),
    (4534, 'Rangoon (Yangon)', 1015),
    (4535, 'Monywa', 1016),
    (4536, 'Taunggyi (Taunggye)', 1017),
    (4537, 'Lashio (Lasho)', 1017),
    (4538, 'Mergui (Myeik)', 1018),
    (4539, 'Tavoy (Dawei)', 1018),
    (4540, 'Ulan Bator', 1019),
    (4541, 'Garapan', 1020),
    (4542, 'Xai-Xai', 1021),
    (4543, 'Gaza', 1021),
    (4544, 'Maxixe', 1022),
    (4545, 'Chimoio', 1023),
    (4546, 'Maputo', 1024),
    (4547, 'Matola', 1024),
    (4548, 'Nampula', 1025),
    (4549, 'NaÃ§ala-Porto', 1025),
    (4550, 'Beira', 1026),
    (4551, 'Tete', 1027),
    (4552, 'Quelimane', 1028),
    (4553, 'Mocuba', 1028),
    (4554, 'Gurue', 1028),
    (4555, 'NouÃ¢dhibou', 1029),
    (4556, 'Nouakchott', 1030),
    (4557, 'Plymouth', 1031),
    (4558, 'Fort-de-France', 1032),
    (4559, 'Beau Bassin-Rose Hill', 1033),
    (4560, 'Vacoas-Phoenix', 1033),
    (4561, 'Port-Louis', 1034),
    (4562, 'Blantyre', 1035),
    (4563, 'Lilongwe', 1036),
    (4564, 'Johor Baharu', 1037),
    (4565, 'Alor Setar', 1038),
    (4566, 'Sungai Petani', 1038),
    (4567, 'Kota Bharu', 1039),
    (4568, 'Seremban', 1040),
    (4569, 'Kuantan', 1041),
    (4570, 'Ipoh', 1042),
    (4571, 'Taiping', 1042),
    (4572, 'Pinang', 1043),
    (4573, 'Sandakan', 1044),
    (4574, 'Kuching', 1045),
    (4575, 'Sibu', 1045),
    (4576, 'Petaling Jaya', 1046),
    (4577, 'Kelang', 1046),
    (4578, 'Selayang Baru', 1046),
    (4579, 'Shah Alam', 1046),
    (4580, 'Kuala Terengganu', 1047),
    (4581, 'Kuala Lumpur', 1048),
    (4582, 'Mamoutzou', 1049),
    (4583, 'Windhoek', 1050),
    (4584, 'South Hill', 1051),
    (4585, 'The Valley', 1051),
    (4586, 'Oranjestad', 1051),
    (4587, 'Douglas', 1051),
    (4588, 'Gibraltar', 1051),
    (4589, 'Tamuning', 1051),
    (4590, 'AgaÃ±a', 1051),
    (4591, 'Flying Fish Cove', 1051),
    (4592, 'Monte-Carlo', 1051),
    (4593, 'Monaco-Ville', 1051),
    (4594, 'Yangor', 1051),
    (4595, 'Yaren', 1051),
    (4596, 'Alofi', 1051),
    (4597, 'Kingston', 1051),
    (4598, 'Adamstown', 1051),
    (4599, 'Singapore', 1051),
    (4600, 'NoumÃ©a', 1051),
    (4601, 'CittÃ  del Vaticano', 1051),
    (4602, 'Maradi', 1052),
    (4603, 'Niamey', 1053),
    (4604, 'Zinder', 1054),
    (4605, 'South Hill', 1055),
    (4606, 'The Valley', 1055),
    (4607, 'Oranjestad', 1055),
    (4608, 'Douglas', 1055),
    (4609, 'Gibraltar', 1055),
    (4610, 'Tamuning', 1055),
    (4611, 'AgaÃ±a', 1055),
    (4612, 'Flying Fish Cove', 1055),
    (4613, 'Monte-Carlo', 1055),
    (4614, 'Monaco-Ville', 1055),
    (4615, 'Yangor', 1055),
    (4616, 'Yaren', 1055),
    (4617, 'Alofi', 1055),
    (4618, 'Kingston', 1055),
    (4619, 'Adamstown', 1055),
    (4620, 'Singapore', 1055),
    (4621, 'NoumÃ©a', 1055),
    (4622, 'CittÃ  del Vaticano', 1055),
    (4623, 'Onitsha', 1056),
    (4624, 'Enugu', 1056),
    (4625, 'Awka', 1056),
    (4626, 'Kumo', 1057),
    (4627, 'Deba Habe', 1057),
    (4628, 'Gombe', 1057),
    (4629, 'Makurdi', 1058),
    (4630, 'Maiduguri', 1059),
    (4631, 'Calabar', 1060),
    (4632, 'Ugep', 1060),
    (4633, 'Benin City', 1061),
    (4634, 'Sapele', 1061),
    (4635, 'Warri', 1061),
    (4636, 'Abuja', 1062),
    (4637, 'Aba', 1063),
    (4638, 'Zaria', 1064),
    (4639, 'Kaduna', 1064),
    (4640, 'Kano', 1065),
    (4641, 'Katsina', 1066),
    (4642, 'Ilorin', 1067),
    (4643, 'Offa', 1067),
    (4644, 'Lagos', 1068),
    (4645, 'Mushin', 1068),
    (4646, 'Ikorodu', 1068),
    (4647, 'Shomolu', 1068),
    (4648, 'Agege', 1068),
    (4649, 'Epe', 1068),
    (4650, 'Minna', 1069),
    (4651, 'Bida', 1069),
    (4652, 'Abeokuta', 1070),
    (4653, 'Ijebu-Ode', 1070),
    (4654, 'Shagamu', 1070),
    (4655, 'Ado-Ekiti', 1071),
    (4656, 'Ikerre', 1071),
    (4657, 'Ilawe-Ekiti', 1071),
    (4658, 'Owo', 1071),
    (4659, 'Ondo', 1071),
    (4660, 'Akure', 1071),
    (4661, 'Oka-Akoko', 1071),
    (4662, 'Ikare', 1071),
    (4663, 'Ise-Ekiti', 1071),
    (4664, 'Ibadan', 1072),
    (4665, 'Ogbomosho', 1072),
    (4666, 'Oshogbo', 1072),
    (4667, 'Ilesha', 1072),
    (4668, 'Iwo', 1072),
    (4669, 'Ede', 1072),
    (4670, 'Ife', 1072),
    (4671, 'Ila', 1072),
    (4672, 'Oyo', 1072),
    (4673, 'Iseyin', 1072),
    (4674, 'Ilobu', 1072),
    (4675, 'Ikirun', 1072),
    (4676, 'Shaki', 1072),
    (4677, 'Effon-Alaiye', 1072),
    (4678, 'Ikire', 1072),
    (4679, 'Inisa', 1072),
    (4680, 'Igboho', 1072),
    (4681, 'Ejigbo', 1072),
    (4682, 'Jos', 1073),
    (4683, 'Lafia', 1073),
    (4684, 'Port Harcourt', 1074),
    (4685, 'Sokoto', 1075),
    (4686, 'Gusau', 1075),
    (4687, 'Chinandega', 1076),
    (4688, 'LeÃ³n', 1077),
    (4689, 'Managua', 1078),
    (4690, 'Masaya', 1079),
    (4691, 'South Hill', 1080),
    (4692, 'The Valley', 1080),
    (4693, 'Oranjestad', 1080),
    (4694, 'Douglas', 1080),
    (4695, 'Gibraltar', 1080),
    (4696, 'Tamuning', 1080),
    (4697, 'AgaÃ±a', 1080),
    (4698, 'Flying Fish Cove', 1080),
    (4699, 'Monte-Carlo', 1080),
    (4700, 'Monaco-Ville', 1080),
    (4701, 'Yangor', 1080),
    (4702, 'Yaren', 1080),
    (4703, 'Alofi', 1080),
    (4704, 'Kingston', 1080),
    (4705, 'Adamstown', 1080),
    (4706, 'Singapore', 1080),
    (4707, 'NoumÃ©a', 1080),
    (4708, 'CittÃ  del Vaticano', 1080),
    (4709, 'Emmen', 1081),
    (4710, 'Almere', 1082),
    (4711, 'Apeldoorn', 1083),
    (4712, 'Nijmegen', 1083),
    (4713, 'Arnhem', 1083),
    (4714, 'Ede', 1083),
    (4715, 'Groningen', 1084),
    (4716, 'Maastricht', 1085),
    (4717, 'Heerlen', 1085),
    (4718, 'Eindhoven', 1086),
    (4719, 'Tilburg', 1086),
    (4720, 'Breda', 1086),
    (4721, 'Â´s-Hertogenbosch', 1086),
    (4722, 'Amsterdam', 1087),
    (4723, 'Haarlem', 1087),
    (4724, 'Zaanstad', 1087),
    (4725, 'Haarlemmermeer', 1087),
    (4726, 'Alkmaar', 1087),
    (4727, 'Enschede', 1088),
    (4728, 'Zwolle', 1088),
    (4729, 'Utrecht', 1089),
    (4730, 'Amersfoort', 1089),
    (4731, 'Rotterdam', 1090),
    (4732, 'Haag', 1090),
    (4733, 'Dordrecht', 1090),
    (4734, 'Leiden', 1090),
    (4735, 'Zoetermeer', 1090),
    (4736, 'Delft', 1090),
    (4737, 'BÃ¦rum', 1091),
    (4738, 'Bergen', 1092),
    (4739, 'Oslo', 1093),
    (4740, 'Stavanger', 1094),
    (4741, 'Trondheim', 1095),
    (4742, 'Suva', 1096),
    (4743, 'Nyeri', 1096),
    (4744, 'Kathmandu', 1096),
    (4745, 'Lalitapur', 1096),
    (4746, 'Birgunj', 1096),
    (4747, 'San Lorenzo', 1096),
    (4748, 'LambarÃ©', 1096),
    (4749, 'Fernando de la Mora', 1096),
    (4750, 'Kabwe', 1096),
    (4751, 'Kandy', 1096),
    (4752, 'Kampala', 1096),
    (4753, 'Machakos', 1097),
    (4754, 'Meru', 1097),
    (4755, 'Biratnagar', 1097),
    (4756, 'Sekondi-Takoradi', 1098),
    (4757, 'Pokhara', 1098),
    (4758, 'Freetown', 1098),
    (4759, 'Colombo', 1098),
    (4760, 'Dehiwala', 1098),
    (4761, 'Moratuwa', 1098),
    (4762, 'Sri Jayawardenepura Kotte', 1098),
    (4763, 'Negombo', 1098),
    (4764, 'South Hill', 1099),
    (4765, 'The Valley', 1099),
    (4766, 'Oranjestad', 1099),
    (4767, 'Douglas', 1099),
    (4768, 'Gibraltar', 1099),
    (4769, 'Tamuning', 1099),
    (4770, 'AgaÃ±a', 1099),
    (4771, 'Flying Fish Cove', 1099),
    (4772, 'Monte-Carlo', 1099),
    (4773, 'Monaco-Ville', 1099),
    (4774, 'Yangor', 1099),
    (4775, 'Yaren', 1099),
    (4776, 'Alofi', 1099),
    (4777, 'Kingston', 1099),
    (4778, 'Adamstown', 1099),
    (4779, 'Singapore', 1099),
    (4780, 'NoumÃ©a', 1099),
    (4781, 'CittÃ  del Vaticano', 1099),
    (4782, 'Auckland', 1100),
    (4783, 'Manukau', 1100),
    (4784, 'North Shore', 1100),
    (4785, 'Waitakere', 1100),
    (4786, 'Christchurch', 1101),
    (4787, 'Dunedin', 1102),
    (4788, 'Hamilton', 1103),
    (4789, 'Hamilton', 1103),
    (4790, 'Wellington', 1104),
    (4791, 'Lower Hutt', 1104),
    (4792, 'Suhar', 1105),
    (4793, 'al-Sib', 1106),
    (4794, 'Bawshar', 1106),
    (4795, 'Masqat', 1106),
    (4796, 'Salala', 1107),
    (4797, 'Quetta', 1108),
    (4798, 'Khuzdar', 1108),
    (4799, 'Islamabad', 1109),
    (4800, 'Peshawar', 1110),
    (4801, 'Mardan', 1110),
    (4802, 'Mingora', 1110),
    (4803, 'Kohat', 1110),
    (4804, 'Abottabad', 1110),
    (4805, 'Dera Ismail Khan', 1110),
    (4806, 'Nowshera', 1110),
    (4807, 'Ludhiana', 1111),
    (4808, 'Amritsar', 1111),
    (4809, 'Jalandhar (Jullundur)', 1111),
    (4810, 'Patiala', 1111),
    (4811, 'Bhatinda (Bathinda)', 1111),
    (4812, 'Pathankot', 1111),
    (4813, 'Hoshiarpur', 1111),
    (4814, 'Moga', 1111),
    (4815, 'Abohar', 1111),
    (4816, 'Lahore', 1111),
    (4817, 'Faisalabad', 1111),
    (4818, 'Rawalpindi', 1111),
    (4819, 'Multan', 1111),
    (4820, 'Gujranwala', 1111),
    (4821, 'Sargodha', 1111),
    (4822, 'Sialkot', 1111),
    (4823, 'Bahawalpur', 1111),
    (4824, 'Jhang', 1111),
    (4825, 'Sheikhupura', 1111),
    (4826, 'Gujrat', 1111),
    (4827, 'Kasur', 1111),
    (4828, 'Rahim Yar Khan', 1111),
    (4829, 'Sahiwal', 1111),
    (4830, 'Okara', 1111),
    (4831, 'Wah', 1111),
    (4832, 'Dera Ghazi Khan', 1111),
    (4833, 'Chiniot', 1111),
    (4834, 'Kamoke', 1111),
    (4835, 'Mandi Burewala', 1111),
    (4836, 'Jhelum', 1111),
    (4837, 'Sadiqabad', 1111),
    (4838, 'Khanewal', 1111),
    (4839, 'Hafizabad', 1111),
    (4840, 'Muzaffargarh', 1111),
    (4841, 'Khanpur', 1111),
    (4842, 'Gojra', 1111),
    (4843, 'Bahawalnagar', 1111),
    (4844, 'Muridke', 1111),
    (4845, 'Pak Pattan', 1111),
    (4846, 'Jaranwala', 1111),
    (4847, 'Chishtian Mandi', 1111),
    (4848, 'Daska', 1111),
    (4849, 'Mandi Bahauddin', 1111),
    (4850, 'Ahmadpur East', 1111),
    (4851, 'Kamalia', 1111),
    (4852, 'Vihari', 1111),
    (4853, 'Wazirabad', 1111),
    (4854, 'Mirpur Khas', 1112),
    (4855, 'Nawabshah', 1112),
    (4856, 'Jacobabad', 1112),
    (4857, 'Shikarpur', 1112),
    (4858, 'Tando Adam', 1112),
    (4859, 'Khairpur', 1112),
    (4860, 'Dadu', 1112),
    (4861, 'Karachi', 1113),
    (4862, 'Hyderabad', 1113),
    (4863, 'Sukkur', 1113),
    (4864, 'Larkana', 1113),
    (4865, 'Ciudad de PanamÃ¡', 1114),
    (4866, 'San Miguelito', 1115),
    (4867, 'South Hill', 1116),
    (4868, 'The Valley', 1116),
    (4869, 'Oranjestad', 1116),
    (4870, 'Douglas', 1116),
    (4871, 'Gibraltar', 1116),
    (4872, 'Tamuning', 1116),
    (4873, 'AgaÃ±a', 1116),
    (4874, 'Flying Fish Cove', 1116),
    (4875, 'Monte-Carlo', 1116),
    (4876, 'Monaco-Ville', 1116),
    (4877, 'Yangor', 1116),
    (4878, 'Yaren', 1116),
    (4879, 'Alofi', 1116),
    (4880, 'Kingston', 1116),
    (4881, 'Adamstown', 1116),
    (4882, 'Singapore', 1116),
    (4883, 'NoumÃ©a', 1116),
    (4884, 'CittÃ  del Vaticano', 1116),
    (4885, 'Chimbote', 1117),
    (4886, 'Arequipa', 1118),
    (4887, 'Ayacucho', 1119),
    (4888, 'Cajamarca', 1120),
    (4889, 'Callao', 1121),
    (4890, 'Ventanilla', 1121),
    (4891, 'Cusco', 1122),
    (4892, 'HuÃ¡nuco', 1123),
    (4893, 'Ica', 1124),
    (4894, 'Chincha Alta', 1124),
    (4895, 'Huancayo', 1125),
    (4896, 'Nueva San Salvador', 1126),
    (4897, 'Trujillo', 1126),
    (4898, 'Chiclayo', 1127),
    (4899, 'Lima', 1128),
    (4900, 'Iquitos', 1129),
    (4901, 'Piura', 1130),
    (4902, 'Sullana', 1130),
    (4903, 'Castilla', 1130),
    (4904, 'Juliaca', 1131),
    (4905, 'Puno', 1131),
    (4906, 'Tacna', 1132),
    (4907, 'Pucallpa', 1133),
    (4908, 'Sultan Kudarat', 1134),
    (4909, 'Legazpi', 1135),
    (4910, 'Naga', 1135),
    (4911, 'Tabaco', 1135),
    (4912, 'Daraga (Locsin)', 1135),
    (4913, 'Sorsogon', 1135),
    (4914, 'Ligao', 1135),
    (4915, 'Tuguegarao', 1136),
    (4916, 'Ilagan', 1136),
    (4917, 'Santiago', 1136),
    (4918, 'Cauayan', 1136),
    (4919, 'Baguio', 1137),
    (4920, 'Butuan', 1138),
    (4921, 'Surigao', 1138),
    (4922, 'Bislig', 1138),
    (4923, 'Bayugan', 1138),
    (4924, 'San JosÃ© del Monte', 1139),
    (4925, 'Angeles', 1139),
    (4926, 'Tarlac', 1139),
    (4927, 'Cabanatuan', 1139),
    (4928, 'San Fernando', 1139),
    (4929, 'Olongapo', 1139),
    (4930, 'Malolos', 1139),
    (4931, 'Mabalacat', 1139),
    (4932, 'Meycauayan', 1139),
    (4933, 'Santa Maria', 1139),
    (4934, 'Lubao', 1139),
    (4935, 'San Miguel', 1139),
    (4936, 'Baliuag', 1139),
    (4937, 'Concepcion', 1139),
    (4938, 'Hagonoy', 1139),
    (4939, 'Mexico', 1139),
    (4940, 'San Jose', 1139),
    (4941, 'Arayat', 1139),
    (4942, 'Marilao', 1139),
    (4943, 'Talavera', 1139),
    (4944, 'Guagua', 1139),
    (4945, 'Capas', 1139),
    (4946, 'Iligan', 1140),
    (4947, 'Cotabato', 1140),
    (4948, 'Marawi', 1140),
    (4949, 'Midsayap', 1140),
    (4950, 'Kidapawan', 1140),
    (4951, 'Cebu', 1141),
    (4952, 'Mandaue', 1141),
    (4953, 'Lapu-Lapu', 1141),
    (4954, 'Talisay', 1141),
    (4955, 'Toledo', 1141),
    (4956, 'Dumaguete', 1141),
    (4957, 'Bayawan (Tulong)', 1141),
    (4958, 'Danao', 1141),
    (4959, 'Tacloban', 1142),
    (4960, 'Ormoc', 1142),
    (4961, 'Calbayog', 1142),
    (4962, 'Baybay', 1142),
    (4963, 'San Carlos', 1143),
    (4964, 'Dagupan', 1143),
    (4965, 'Malasiqui', 1143),
    (4966, 'Urdaneta', 1143),
    (4967, 'San Fernando', 1143),
    (4968, 'Bayambang', 1143),
    (4969, 'Laoag', 1143),
    (4970, 'Quezon', 1144),
    (4971, 'Manila', 1144),
    (4972, 'Kalookan', 1144),
    (4973, 'Pasig', 1144),
    (4974, 'Valenzuela', 1144),
    (4975, 'Las PiÃ±as', 1144),
    (4976, 'Taguig', 1144),
    (4977, 'ParaÃ±aque', 1144),
    (4978, 'Makati', 1144),
    (4979, 'Marikina', 1144),
    (4980, 'Muntinlupa', 1144),
    (4981, 'Pasay', 1144),
    (4982, 'Malabon', 1144),
    (4983, 'Mandaluyong', 1144),
    (4984, 'Navotas', 1144),
    (4985, 'San Juan del Monte', 1144),
    (4986, 'Cagayan de Oro', 1145),
    (4987, 'Valencia', 1145),
    (4988, 'Malaybalay', 1145),
    (4989, 'Ozamis', 1145),
    (4990, 'Gingoog', 1145),
    (4991, 'Davao', 1146),
    (4992, 'General Santos', 1146),
    (4993, 'Tagum', 1146),
    (4994, 'Panabo', 1146),
    (4995, 'Koronadal', 1146),
    (4996, 'Digos', 1146),
    (4997, 'Polomolok', 1146),
    (4998, 'Mati', 1146),
    (4999, 'Malita', 1146),
    (5000, 'Malungon', 1146),
    (5001, 'Antipolo', 1147),
    (5002, 'DasmariÃ±as', 1147),
    (5003, 'Bacoor', 1147),
    (5004, 'Calamba', 1147),
    (5005, 'Batangas', 1147),
    (5006, 'Cainta', 1147),
    (5007, 'San Pedro', 1147),
    (5008, 'Lipa', 1147),
    (5009, 'San Pablo', 1147),
    (5010, 'BiÃ±an', 1147),
    (5011, 'Taytay', 1147),
    (5012, 'Lucena', 1147),
    (5013, 'Imus', 1147),
    (5014, 'Binangonan', 1147),
    (5015, 'Santa Rosa', 1147),
    (5016, 'Puerto Princesa', 1147),
    (5017, 'Silang', 1147),
    (5018, 'San Mateo', 1147),
    (5019, 'Tanauan', 1147),
    (5020, 'Rodriguez (Montalban)', 1147),
    (5021, 'Sariaya', 1147),
    (5022, 'General Mariano Alvarez', 1147),
    (5023, 'San Jose', 1147),
    (5024, 'Tanza', 1147),
    (5025, 'General Trias', 1147),
    (5026, 'Cabuyao', 1147),
    (5027, 'Calapan', 1147),
    (5028, 'Cavite', 1147),
    (5029, 'Nasugbu', 1147),
    (5030, 'Santa Cruz', 1147),
    (5031, 'Candelaria', 1147),
    (5032, 'Zamboanga', 1148),
    (5033, 'Pagadian', 1148),
    (5034, 'Dipolog', 1148),
    (5035, 'Bacolod', 1149),
    (5036, 'Iloilo', 1149),
    (5037, 'Kabankalan', 1149),
    (5038, 'Cadiz', 1149),
    (5039, 'Bago', 1149),
    (5040, 'Sagay', 1149),
    (5041, 'Roxas', 1149),
    (5042, 'San Carlos', 1149),
    (5043, 'Silay', 1149),
    (5044, 'Koror', 1150),
    (5045, 'Port Moresby', 1151),
    (5046, 'Wroclaw', 1152),
    (5047, 'Walbrzych', 1152),
    (5048, 'Legnica', 1152),
    (5049, 'Jelenia GÃ³ra', 1152),
    (5050, 'Bydgoszcz', 1153),
    (5051, 'Torun', 1153),
    (5052, 'Wloclawek', 1153),
    (5053, 'Grudziadz', 1153),
    (5054, 'LÃ³dz', 1154),
    (5055, 'Lublin', 1155),
    (5056, 'GorzÃ³w Wielkopolski', 1156),
    (5057, 'Zielona GÃ³ra', 1156),
    (5058, 'KrakÃ³w', 1157),
    (5059, 'TarnÃ³w', 1157),
    (5060, 'Warszawa', 1158),
    (5061, 'Radom', 1158),
    (5062, 'Plock', 1158),
    (5063, 'Opole', 1159),
    (5064, 'RzeszÃ³w', 1160),
    (5065, 'Bialystok', 1161),
    (5066, 'Gdansk', 1162),
    (5067, 'Gdynia', 1162),
    (5068, 'Slupsk', 1162),
    (5069, 'Katowice', 1163),
    (5070, 'Czestochowa', 1163),
    (5071, 'Sosnowiec', 1163),
    (5072, 'Gliwice', 1163),
    (5073, 'Bytom', 1163),
    (5074, 'Zabrze', 1163),
    (5075, 'Bielsko-Biala', 1163),
    (5076, 'Ruda Slaska', 1163),
    (5077, 'Rybnik', 1163),
    (5078, 'Tychy', 1163),
    (5079, 'Dabrowa GÃ³rnicza', 1163),
    (5080, 'ChorzÃ³w', 1163),
    (5081, 'Jastrzebie-ZdrÃ³j', 1163),
    (5082, 'Jaworzno', 1163),
    (5083, 'Kielce', 1164),
    (5084, 'Olsztyn', 1165),
    (5085, 'Elblag', 1165),
    (5086, 'Poznan', 1166),
    (5087, 'Kalisz', 1166),
    (5088, 'Szczecin', 1167),
    (5089, 'Koszalin', 1167),
    (5090, 'Arecibo', 1168),
    (5091, 'BayamÃ³n', 1169),
    (5092, 'Caguas', 1170),
    (5093, 'Carolina', 1171),
    (5094, 'Guaynabo', 1172),
    (5095, 'MayagÃ¼ez', 1173),
    (5096, 'Ponce', 1174),
    (5097, 'San Juan', 1175),
    (5098, 'San Juan', 1175),
    (5099, 'Toa Baja', 1176),
    (5100, 'Kanggye', 1177),
    (5101, 'Hamhung', 1178),
    (5102, 'Chongjin', 1179),
    (5103, 'Kimchaek', 1179),
    (5104, 'Haeju', 1180),
    (5105, 'Sariwon', 1181),
    (5106, 'Kaesong', 1182),
    (5107, 'Wonsan', 1183),
    (5108, 'Nampo', 1184),
    (5109, 'Phyongsong', 1185),
    (5110, 'Sinuiju', 1186),
    (5111, 'Pyongyang', 1187),
    (5112, 'Hyesan', 1188),
    (5113, 'Braga', 1189),
    (5114, 'CoÃ­mbra', 1190),
    (5115, 'Lisboa', 1191),
    (5116, 'Amadora', 1191),
    (5117, 'Stockholm', 1191),
    (5118, 'Porto', 1192),
    (5119, 'Ciudad del Este', 1193),
    (5120, 'AsunciÃ³n', 1194),
    (5121, 'Suva', 1195),
    (5122, 'Nyeri', 1195),
    (5123, 'Kathmandu', 1195),
    (5124, 'Lalitapur', 1195),
    (5125, 'Birgunj', 1195),
    (5126, 'San Lorenzo', 1195),
    (5127, 'LambarÃ©', 1195),
    (5128, 'Fernando de la Mora', 1195),
    (5129, 'Kabwe', 1195),
    (5130, 'Kandy', 1195),
    (5131, 'Kampala', 1195),
    (5132, 'Xai-Xai', 1196),
    (5133, 'Gaza', 1196),
    (5134, 'Hebron', 1197),
    (5135, 'Khan Yunis', 1198),
    (5136, 'Nablus', 1199),
    (5137, 'Jabaliya', 1200),
    (5138, 'Rafah', 1201),
    (5139, 'Faaa', 1202),
    (5140, 'Papeete', 1202),
    (5141, 'Doha', 1203),
    (5142, 'Saint-Denis', 1204),
    (5143, 'Arad', 1205),
    (5144, 'Pitesti', 1206),
    (5145, 'Bacau', 1207),
    (5146, 'Oradea', 1208),
    (5147, 'Botosani', 1209),
    (5148, 'Braila', 1210),
    (5149, 'Brasov', 1211),
    (5150, 'Bucuresti', 1212),
    (5151, 'Buzau', 1213),
    (5152, 'Resita', 1214),
    (5153, 'Cluj-Napoca', 1215),
    (5154, 'Constanta', 1216),
    (5155, 'TÃ¢rgoviste', 1217),
    (5156, 'Craiova', 1218),
    (5157, 'Galati', 1219),
    (5158, 'TÃ¢rgu Jiu', 1220),
    (5159, 'Iasi', 1221),
    (5160, 'Baia Mare', 1222),
    (5161, 'Drobeta-Turnu Severin', 1223),
    (5162, 'TÃ¢rgu Mures', 1224),
    (5163, 'Piatra Neamt', 1225),
    (5164, 'Ploiesti', 1226),
    (5165, 'Satu Mare', 1227),
    (5166, 'Sibiu', 1228),
    (5167, 'Suceava', 1229),
    (5168, 'Timisoara', 1230),
    (5169, 'Tulcea', 1231),
    (5170, 'RÃ¢mnicu VÃ¢lcea', 1232),
    (5171, 'Focsani', 1233),
    (5172, 'Maikop', 1234),
    (5173, 'Barnaul', 1235),
    (5174, 'Bijsk', 1235),
    (5175, 'Rubtsovsk', 1235),
    (5176, 'BlagoveÅ¡tÅ¡ensk', 1236),
    (5177, 'Arkangeli', 1237),
    (5178, 'Severodvinsk', 1237),
    (5179, 'Astrahan', 1238),
    (5180, 'Ufa', 1239),
    (5181, 'Sterlitamak', 1239),
    (5182, 'Salavat', 1239),
    (5183, 'Neftekamsk', 1239),
    (5184, 'Oktjabrski', 1239),
    (5185, 'Belgorod', 1240),
    (5186, 'Staryi Oskol', 1240),
    (5187, 'Brjansk', 1241),
    (5188, 'Ulan-Ude', 1242),
    (5189, 'MahatÅ¡kala', 1243),
    (5190, 'Derbent', 1243),
    (5191, 'Habarovsk', 1244),
    (5192, 'Komsomolsk-na-Amure', 1244),
    (5193, 'Abakan', 1245),
    (5194, 'Surgut', 1246),
    (5195, 'Niznevartovsk', 1246),
    (5196, 'Neftejugansk', 1246),
    (5197, 'Irkutsk', 1247),
    (5198, 'Bratsk', 1247),
    (5199, 'Angarsk', 1247),
    (5200, 'Ust-Ilimsk', 1247),
    (5201, 'Usolje-Sibirskoje', 1247),
    (5202, 'Ivanovo', 1248),
    (5203, 'KineÅ¡ma', 1248),
    (5204, 'Jaroslavl', 1249),
    (5205, 'Rybinsk', 1249),
    (5206, 'NaltÅ¡ik', 1250),
    (5207, 'Kaliningrad', 1251),
    (5208, 'Elista', 1252),
    (5209, 'Kaluga', 1253),
    (5210, 'Obninsk', 1253),
    (5211, 'Petropavlovsk-KamtÅ¡atski', 1254),
    (5212, 'TÅ¡erkessk', 1255),
    (5213, 'Petroskoi', 1256),
    (5214, 'Novokuznetsk', 1257),
    (5215, 'Kemerovo', 1257),
    (5216, 'Prokopjevsk', 1257),
    (5217, 'Leninsk-Kuznetski', 1257),
    (5218, 'Kiseljovsk', 1257),
    (5219, 'MezduretÅ¡ensk', 1257),
    (5220, 'Anzero-Sudzensk', 1257),
    (5221, 'Kirov', 1258),
    (5222, 'Kirovo-TÅ¡epetsk', 1258),
    (5223, 'Syktyvkar', 1259),
    (5224, 'Uhta', 1259),
    (5225, 'Vorkuta', 1259),
    (5226, 'Kostroma', 1260),
    (5227, 'Krasnodar', 1261),
    (5228, 'SotÅ¡i', 1261),
    (5229, 'Novorossijsk', 1261),
    (5230, 'Armavir', 1261),
    (5231, 'Krasnojarsk', 1262),
    (5232, 'Norilsk', 1262),
    (5233, 'AtÅ¡insk', 1262),
    (5234, 'Kansk', 1262),
    (5235, 'Zeleznogorsk', 1262),
    (5236, 'Kurgan', 1263),
    (5237, 'Kursk', 1264),
    (5238, 'Zeleznogorsk', 1264),
    (5239, 'Lipetsk', 1265),
    (5240, 'Jelets', 1265),
    (5241, 'Magadan', 1266),
    (5242, 'JoÅ¡kar-Ola', 1267),
    (5243, 'Saransk', 1268),
    (5244, 'Moscow', 1269),
    (5245, 'Zelenograd', 1269),
    (5246, 'Podolsk', 1270),
    (5247, 'Ljubertsy', 1270),
    (5248, 'MytiÅ¡tÅ¡i', 1270),
    (5249, 'Kolomna', 1270),
    (5250, 'Elektrostal', 1270),
    (5251, 'Himki', 1270),
    (5252, 'BalaÅ¡iha', 1270),
    (5253, 'Korolev', 1270),
    (5254, 'Serpuhov', 1270),
    (5255, 'Odintsovo', 1270),
    (5256, 'Orehovo-Zujevo', 1270),
    (5257, 'Noginsk', 1270),
    (5258, 'Sergijev Posad', 1270),
    (5259, 'Å tÅ¡olkovo', 1270),
    (5260, 'Zeleznodoroznyi', 1270),
    (5261, 'Zukovski', 1270),
    (5262, 'Krasnogorsk', 1270),
    (5263, 'Klin', 1270),
    (5264, 'Murmansk', 1271),
    (5265, 'Nizni Novgorod', 1272),
    (5266, 'Dzerzinsk', 1272),
    (5267, 'Arzamas', 1272),
    (5268, 'Vladikavkaz', 1273),
    (5269, 'Veliki Novgorod', 1274),
    (5270, 'Novosibirsk', 1275),
    (5271, 'Omsk', 1276),
    (5272, 'Orenburg', 1277),
    (5273, 'Orsk', 1277),
    (5274, 'Novotroitsk', 1277),
    (5275, 'Orjol', 1278),
    (5276, 'Penza', 1279),
    (5277, 'Kuznetsk', 1279),
    (5278, 'Perm', 1280),
    (5279, 'Berezniki', 1280),
    (5280, 'Solikamsk', 1280),
    (5281, 'TÅ¡aikovski', 1280),
    (5282, 'St Petersburg', 1281),
    (5283, 'Kolpino', 1281),
    (5284, 'PuÅ¡kin', 1281),
    (5285, 'Pihkova', 1282),
    (5286, 'Velikije Luki', 1282),
    (5287, 'Vladivostok', 1283),
    (5288, 'Nahodka', 1283),
    (5289, 'Ussurijsk', 1283),
    (5290, 'Rjazan', 1284),
    (5291, 'Rostov-na-Donu', 1285),
    (5292, 'Taganrog', 1285),
    (5293, 'Å ahty', 1285),
    (5294, 'NovotÅ¡erkassk', 1285),
    (5295, 'Volgodonsk', 1285),
    (5296, 'NovoÅ¡ahtinsk', 1285),
    (5297, 'Bataisk', 1285),
    (5298, 'Jakutsk', 1286),
    (5299, 'Juzno-Sahalinsk', 1287),
    (5300, 'Samara', 1288),
    (5301, 'Toljatti', 1288),
    (5302, 'Syzran', 1288),
    (5303, 'NovokuibyÅ¡evsk', 1288),
    (5304, 'Saratov', 1289),
    (5305, 'Balakovo', 1289),
    (5306, 'Engels', 1289),
    (5307, 'BalaÅ¡ov', 1289),
    (5308, 'Smolensk', 1290),
    (5309, 'Stavropol', 1291),
    (5310, 'Nevinnomyssk', 1291),
    (5311, 'Pjatigorsk', 1291),
    (5312, 'Kislovodsk', 1291),
    (5313, 'Jessentuki', 1291),
    (5314, 'Jekaterinburg', 1292),
    (5315, 'Nizni Tagil', 1292),
    (5316, 'Kamensk-Uralski', 1292),
    (5317, 'Pervouralsk', 1292),
    (5318, 'Serov', 1292),
    (5319, 'Novouralsk', 1292),
    (5320, 'Tambov', 1293),
    (5321, 'MitÅ¡urinsk', 1293),
    (5322, 'Kazan', 1294),
    (5323, 'Nabereznyje TÅ¡elny', 1294),
    (5324, 'Niznekamsk', 1294),
    (5325, 'Almetjevsk', 1294),
    (5326, 'Zelenodolsk', 1294),
    (5327, 'Bugulma', 1294),
    (5328, 'Tjumen', 1295),
    (5329, 'Tobolsk', 1295),
    (5330, 'Tomsk', 1296),
    (5331, 'Seversk', 1296),
    (5332, 'Tula', 1297),
    (5333, 'Novomoskovsk', 1297),
    (5334, 'Tver', 1298),
    (5335, 'Kyzyl', 1299),
    (5336, 'TÅ¡eljabinsk', 1300),
    (5337, 'Magnitogorsk', 1300),
    (5338, 'Zlatoust', 1300),
    (5339, 'Miass', 1300),
    (5340, 'Grozny', 1301),
    (5341, 'TÅ¡ita', 1302),
    (5342, 'TÅ¡eboksary', 1303),
    (5343, 'NovotÅ¡eboksarsk', 1303),
    (5344, 'Izevsk', 1304),
    (5345, 'Glazov', 1304),
    (5346, 'Sarapul', 1304),
    (5347, 'Votkinsk', 1304),
    (5348, 'Uljanovsk', 1305),
    (5349, 'Dimitrovgrad', 1305),
    (5350, 'Vladimir', 1306),
    (5351, 'Kovrov', 1306),
    (5352, 'Murom', 1306),
    (5353, 'Volgograd', 1307),
    (5354, 'Volzski', 1307),
    (5355, 'KamyÅ¡in', 1307),
    (5356, 'TÅ¡erepovets', 1308),
    (5357, 'Vologda', 1308),
    (5358, 'Voronez', 1309),
    (5359, 'Nojabrsk', 1310),
    (5360, 'Novyi Urengoi', 1310),
    (5361, 'Kigali', 1311),
    (5362, 'AraÂ´ar', 1312),
    (5363, 'Burayda', 1313),
    (5364, 'Zagazig', 1314),
    (5365, 'Bilbays', 1314),
    (5366, 'al-Dammam', 1314),
    (5367, 'al-Hufuf', 1314),
    (5368, 'al-Mubarraz', 1314),
    (5369, 'al-Khubar', 1314),
    (5370, 'Jubayl', 1314),
    (5371, 'Hafar al-Batin', 1314),
    (5372, 'al-Tuqba', 1314),
    (5373, 'al-Qatif', 1314),
    (5374, 'Khamis Mushayt', 1315),
    (5375, 'Abha', 1315),
    (5376, 'Hail', 1316),
    (5377, 'Medina', 1317),
    (5378, 'Yanbu', 1317),
    (5379, 'Jedda', 1318),
    (5380, 'Mekka', 1318),
    (5381, 'al-Taif', 1318),
    (5382, 'al-Hawiya', 1318),
    (5383, 'Najran', 1319),
    (5384, 'Unayza', 1320),
    (5385, 'al-Kharj', 1321),
    (5386, 'Riyadh', 1322),
    (5387, 'Tabuk', 1323),
    (5388, 'Kusti', 1324),
    (5389, 'Port Sudan', 1325),
    (5390, 'Wad Madani', 1326),
    (5391, 'al-Qadarif', 1327),
    (5392, 'Juba', 1328),
    (5393, 'Nyala', 1329),
    (5394, 'al-Fashir', 1330),
    (5395, 'Kassala', 1331),
    (5396, 'Omdurman', 1332),
    (5397, 'Khartum', 1332),
    (5398, 'Sharq al-Nil', 1332),
    (5399, 'Obeid', 1333),
    (5400, 'Pikine', 1334),
    (5401, 'Dakar', 1334),
    (5402, 'Rufisque', 1334),
    (5403, 'Diourbel', 1335),
    (5404, 'Kaolack', 1336),
    (5405, 'Saint-Louis', 1337),
    (5406, 'ThiÃ¨s', 1338),
    (5407, 'Mbour', 1338),
    (5408, 'Ziguinchor', 1339),
    (5409, 'South Hill', 1340),
    (5410, 'The Valley', 1340),
    (5411, 'Oranjestad', 1340),
    (5412, 'Douglas', 1340),
    (5413, 'Gibraltar', 1340),
    (5414, 'Tamuning', 1340),
    (5415, 'AgaÃ±a', 1340),
    (5416, 'Flying Fish Cove', 1340),
    (5417, 'Monte-Carlo', 1340),
    (5418, 'Monaco-Ville', 1340),
    (5419, 'Yangor', 1340),
    (5420, 'Yaren', 1340),
    (5421, 'Alofi', 1340),
    (5422, 'Kingston', 1340),
    (5423, 'Adamstown', 1340),
    (5424, 'Singapore', 1340),
    (5425, 'NoumÃ©a', 1340),
    (5426, 'CittÃ  del Vaticano', 1340),
    (5427, 'Jamestown', 1341),
    (5428, 'Longyearbyen', 1342),
    (5429, 'Honiara', 1343),
    (5430, 'Sekondi-Takoradi', 1344),
    (5431, 'Pokhara', 1344),
    (5432, 'Freetown', 1344),
    (5433, 'Colombo', 1344),
    (5434, 'Dehiwala', 1344),
    (5435, 'Moratuwa', 1344),
    (5436, 'Sri Jayawardenepura Kotte', 1344),
    (5437, 'Negombo', 1344),
    (5438, 'Nueva San Salvador', 1345),
    (5439, 'Trujillo', 1345),
    (5440, 'San Miguel', 1346),
    (5441, 'San Salvador', 1347),
    (5442, 'Mejicanos', 1347),
    (5443, 'Soyapango', 1347),
    (5444, 'Apopa', 1347),
    (5445, 'Santa Ana', 1348),
    (5446, 'San Marino', 1349),
    (5447, 'Serravalle', 1350),
    (5448, 'Mogadishu', 1351),
    (5449, 'Kismaayo', 1352),
    (5450, 'Hargeysa', 1353),
    (5451, 'Saint-Pierre', 1354),
    (5452, 'SÃ£o TomÃ©', 1355),
    (5453, 'Paramaribo', 1356),
    (5454, 'Bratislava', 1357),
    (5455, 'KoÅ¡ice', 1358),
    (5456, 'PreÅ¡ov', 1358),
    (5457, 'Ljubljana', 1359),
    (5458, 'Maribor', 1360),
    (5459, 'Ã–rebro', 1361),
    (5460, 'LinkÃ¶ping', 1362),
    (5461, 'NorrkÃ¶ping', 1362),
    (5462, 'GÃ¤vle', 1363),
    (5463, 'JÃ¶nkÃ¶ping', 1364),
    (5464, 'Lisboa', 1365),
    (5465, 'Amadora', 1365),
    (5466, 'Stockholm', 1365),
    (5467, 'MalmÃ¶', 1366),
    (5468, 'Helsingborg', 1366),
    (5469, 'Lund', 1366),
    (5470, 'Uppsala', 1367),
    (5471, 'UmeÃ¥', 1368),
    (5472, 'Sundsvall', 1369),
    (5473, 'VÃ¤sterÃ¥s', 1370),
    (5474, 'Gothenburg [GÃ¶teborg]', 1371),
    (5475, 'BorÃ¥s', 1371),
    (5476, 'Mbabane', 1372),
    (5477, 'Victoria', 1373),
    (5478, 'al-Qamishliya', 1374),
    (5479, 'al-Raqqa', 1375),
    (5480, 'Aleppo', 1376),
    (5481, 'Damascus', 1377),
    (5482, 'Jaramana', 1378),
    (5483, 'Duma', 1378),
    (5484, 'Dayr al-Zawr', 1379),
    (5485, 'Hama', 1380),
    (5486, 'Hims', 1381),
    (5487, 'Idlib', 1382),
    (5488, 'Latakia', 1383),
    (5489, 'Cockburn Town', 1384),
    (5490, 'NÂ´DjamÃ©na', 1385),
    (5491, 'Moundou', 1386),
    (5492, 'LomÃ©', 1387),
    (5493, 'Bangkok', 1388),
    (5494, 'Chiang Mai', 1389),
    (5495, 'Khon Kaen', 1390),
    (5496, 'Nakhon Pathom', 1391),
    (5497, 'Nakhon Ratchasima', 1392),
    (5498, 'Nakhon Sawan', 1393),
    (5499, 'Nonthaburi', 1394),
    (5500, 'Pak Kret', 1394),
    (5501, 'Hat Yai', 1395),
    (5502, 'Songkhla', 1395),
    (5503, 'Ubon Ratchathani', 1396),
    (5504, 'Udon Thani', 1397),
    (5505, 'Dushanbe', 1398),
    (5506, 'Khujand', 1399),
    (5507, 'Fakaofo', 1400),
    (5508, 'Ashgabat', 1401),
    (5509, 'Dashhowuz', 1402),
    (5510, 'ChÃ¤rjew', 1403),
    (5511, 'Mary', 1404),
    (5512, 'Dili', 1405),
    (5513, 'NukuÂ´alofa', 1406),
    (5514, 'Chaguanas', 1407),
    (5515, 'Port-of-Spain', 1408),
    (5516, 'Ariana', 1409),
    (5517, 'Ettadhamen', 1409),
    (5518, 'Biserta', 1410),
    (5519, 'GabÃ¨s', 1411),
    (5520, 'Kairouan', 1412),
    (5521, 'Sfax', 1413),
    (5522, 'Sousse', 1414),
    (5523, 'Tunis', 1415),
    (5524, 'Adana', 1416),
    (5525, 'Tarsus', 1416),
    (5526, 'Ceyhan', 1416),
    (5527, 'Adiyaman', 1417),
    (5528, 'Afyon', 1418),
    (5529, 'Aksaray', 1419),
    (5530, 'Ankara', 1420),
    (5531, 'Antalya', 1421),
    (5532, 'Alanya', 1421),
    (5533, 'Aydin', 1422),
    (5534, 'Nazilli', 1422),
    (5535, 'Ã‡orum', 1423),
    (5536, 'Balikesir', 1424),
    (5537, 'Bandirma', 1424),
    (5538, 'Batman', 1425),
    (5539, 'Bursa', 1426),
    (5540, 'InegÃ¶l', 1426),
    (5541, 'Denizli', 1427),
    (5542, 'Diyarbakir', 1428),
    (5543, 'Bismil', 1428),
    (5544, 'Edirne', 1429),
    (5545, 'ElÃ¢zig', 1430),
    (5546, 'Erzincan', 1431),
    (5547, 'Erzurum', 1432),
    (5548, 'Eskisehir', 1433),
    (5549, 'Gaziantep', 1434),
    (5550, 'Iskenderun', 1435),
    (5551, 'Hatay (Antakya)', 1435),
    (5552, 'Mersin (IÃ§el)', 1436),
    (5553, 'Isparta', 1437),
    (5554, 'Istanbul', 1438),
    (5555, 'Sultanbeyli', 1438),
    (5556, 'Izmir', 1439),
    (5557, 'Kahramanmaras', 1440),
    (5558, 'KarabÃ¼k', 1441),
    (5559, 'Karaman', 1442),
    (5560, 'Kars', 1443),
    (5561, 'Kayseri', 1444),
    (5562, 'KÃ¼tahya', 1445),
    (5563, 'Kilis', 1446),
    (5564, 'Kirikkale', 1447),
    (5565, 'Gebze', 1448),
    (5566, 'Izmit (Kocaeli)', 1448),
    (5567, 'Konya', 1449),
    (5568, 'Malatya', 1450),
    (5569, 'Manisa', 1451),
    (5570, 'Kiziltepe', 1452),
    (5571, 'Ordu', 1453),
    (5572, 'Osmaniye', 1454),
    (5573, 'Sakarya (Adapazari)', 1455),
    (5574, 'Samsun', 1456),
    (5575, 'Sanliurfa', 1457),
    (5576, 'Viransehir', 1457),
    (5577, 'Siirt', 1458),
    (5578, 'Sivas', 1459),
    (5579, 'Ã‡orlu', 1460),
    (5580, 'Tekirdag', 1460),
    (5581, 'Tokat', 1461),
    (5582, 'Trabzon', 1462),
    (5583, 'Usak', 1463),
    (5584, 'Van', 1464),
    (5585, 'Zonguldak', 1465),
    (5586, 'Funafuti', 1466),
    (5591, 'Changhwa', 1468),
    (5592, 'Yuanlin', 1468),
    (5593, 'Chiayi', 1469),
    (5594, 'Hsinchu', 1470),
    (5595, 'Hualien', 1471),
    (5596, 'Ilan', 1472),
    (5597, 'Kaohsiung', 1473),
    (5598, 'Fengshan', 1473),
    (5599, 'Kangshan', 1473),
    (5600, 'Keelung (Chilung)', 1474),
    (5601, 'Miaoli', 1475),
    (5602, 'Nantou', 1476),
    (5603, 'Tsaotun', 1476),
    (5604, 'Pingtung', 1477),
    (5605, 'Taichung', 1478),
    (5606, 'Tali', 1478),
    (5607, 'Fengyuan', 1478),
    (5608, 'Tainan', 1479),
    (5609, 'Yungkang', 1479),
    (5610, 'Taipei', 1480),
    (5611, 'Panchiao', 1480),
    (5612, 'Chungho', 1480),
    (5613, 'Sanchung', 1480),
    (5614, 'Hsinchuang', 1480),
    (5615, 'Hsintien', 1480),
    (5616, 'Yungho', 1480),
    (5617, 'Tucheng', 1480),
    (5618, 'Luchou', 1480),
    (5619, 'Hsichuh', 1480),
    (5620, 'Shulin', 1480),
    (5621, 'Tanshui', 1480),
    (5622, 'Lungtan', 1480),
    (5623, 'Taitung', 1481),
    (5624, 'Chungli', 1482),
    (5625, 'Taoyuan', 1482),
    (5626, 'Pingchen', 1482),
    (5627, 'Pate', 1482),
    (5628, 'Yangmei', 1482),
    (5629, 'Touliu', 1483),
    (5630, 'Arusha', 1484),
    (5631, 'Dar es Salaam', 1485),
    (5632, 'Dodoma', 1486),
    (5633, 'Moshi', 1487),
    (5634, 'Mbeya', 1488),
    (5635, 'Morogoro', 1489),
    (5636, 'Mwanza', 1490),
    (5637, 'Tabora', 1491),
    (5638, 'Tanga', 1492),
    (5639, 'Zanzibar', 1493),
    (5640, 'Suva', 1494),
    (5641, 'Nyeri', 1494),
    (5642, 'Kathmandu', 1494),
    (5643, 'Lalitapur', 1494),
    (5644, 'Birgunj', 1494),
    (5645, 'San Lorenzo', 1494),
    (5646, 'LambarÃ©', 1494),
    (5647, 'Fernando de la Mora', 1494),
    (5648, 'Kabwe', 1494),
    (5649, 'Kandy', 1494),
    (5650, 'Kampala', 1494),
    (5651, 'Dnipropetrovsk', 1495),
    (5652, 'Kryvyi Rig', 1495),
    (5653, 'Dniprodzerzynsk', 1495),
    (5654, 'Nikopol', 1495),
    (5655, 'Pavlograd', 1495),
    (5656, 'Donetsk', 1496),
    (5657, 'Mariupol', 1496),
    (5658, 'Makijivka', 1496),
    (5659, 'Gorlivka', 1496),
    (5660, 'Kramatorsk', 1496),
    (5661, 'Slovjansk', 1496),
    (5662, 'Jenakijeve', 1496),
    (5663, 'Kostjantynivka', 1496),
    (5664, 'Harkova [Harkiv]', 1497),
    (5665, 'Herson', 1498),
    (5666, 'Hmelnytskyi', 1499),
    (5667, 'Kamjanets-Podilskyi', 1499),
    (5668, 'Ivano-Frankivsk', 1500),
    (5669, 'Kyiv', 1501),
    (5670, 'Bila Tserkva', 1501),
    (5671, 'Brovary', 1501),
    (5672, 'Kirovograd', 1502),
    (5673, 'Oleksandrija', 1502),
    (5674, 'Sevastopol', 1503),
    (5675, 'Simferopol', 1503),
    (5676, 'KertÅ¡', 1503),
    (5677, 'Jevpatorija', 1503),
    (5678, 'Lugansk', 1504),
    (5679, 'Sjeverodonetsk', 1504),
    (5680, 'AltÅ¡evsk', 1504),
    (5681, 'LysytÅ¡ansk', 1504),
    (5682, 'Krasnyi LutÅ¡', 1504),
    (5683, 'Stahanov', 1504),
    (5684, 'Lviv', 1505),
    (5685, 'Mykolajiv', 1506),
    (5686, 'Odesa', 1507),
    (5687, 'Izmajil', 1507),
    (5688, 'Pultava [Poltava]', 1508),
    (5689, 'KrementÅ¡uk', 1508),
    (5690, 'Rivne', 1509),
    (5691, 'Sumy', 1510),
    (5692, 'Konotop', 1510),
    (5693, 'Å ostka', 1510),
    (5694, 'Uzgorod', 1511),
    (5695, 'MukatÅ¡eve', 1511),
    (5696, 'Ternopil', 1512),
    (5697, 'TÅ¡erkasy', 1513),
    (5698, 'Uman', 1513),
    (5699, 'TÅ¡ernigiv', 1514),
    (5700, 'TÅ¡ernivtsi', 1515),
    (5701, 'Vinnytsja', 1516),
    (5702, 'Lutsk', 1517),
    (5703, 'Zaporizzja', 1518),
    (5704, 'Melitopol', 1518),
    (5705, 'Berdjansk', 1518),
    (5706, 'Zytomyr', 1519),
    (5707, 'BerdytÅ¡iv', 1519),
    (5708, 'Montevideo', 1520),
    (5709, 'Birmingham', 1521),
    (5710, 'Montgomery', 1521),
    (5711, 'Mobile', 1521),
    (5712, 'Huntsville', 1521),
    (5713, 'Anchorage', 1522),
    (5714, 'Phoenix', 1523),
    (5715, 'Tucson', 1523),
    (5716, 'Mesa', 1523),
    (5717, 'Glendale', 1523),
    (5718, 'Scottsdale', 1523),
    (5719, 'Chandler', 1523),
    (5720, 'Tempe', 1523),
    (5721, 'Gilbert', 1523),
    (5722, 'Peoria', 1523),
    (5723, 'Little Rock', 1524),
    (5724, 'Los Angeles', 1525),
    (5725, 'San Diego', 1525),
    (5726, 'San Jose', 1525),
    (5727, 'San Francisco', 1525),
    (5728, 'Long Beach', 1525),
    (5729, 'Fresno', 1525),
    (5730, 'Sacramento', 1525),
    (5731, 'Oakland', 1525),
    (5732, 'Santa Ana', 1525),
    (5733, 'Anaheim', 1525),
    (5734, 'Riverside', 1525),
    (5735, 'Bakersfield', 1525),
    (5736, 'Stockton', 1525),
    (5737, 'Fremont', 1525),
    (5738, 'Glendale', 1525),
    (5739, 'Huntington Beach', 1525),
    (5740, 'Modesto', 1525),
    (5741, 'San Bernardino', 1525),
    (5742, 'Chula Vista', 1525),
    (5743, 'Oxnard', 1525),
    (5744, 'Garden Grove', 1525),
    (5745, 'Oceanside', 1525),
    (5746, 'Ontario', 1525),
    (5747, 'Santa Clarita', 1525),
    (5748, 'Salinas', 1525),
    (5749, 'Pomona', 1525),
    (5750, 'Santa Rosa', 1525),
    (5751, 'Irvine', 1525),
    (5752, 'Moreno Valley', 1525),
    (5753, 'Pasadena', 1525),
    (5754, 'Hayward', 1525),
    (5755, 'Torrance', 1525),
    (5756, 'Escondido', 1525),
    (5757, 'Sunnyvale', 1525),
    (5758, 'Fontana', 1525),
    (5759, 'Orange', 1525),
    (5760, 'Rancho Cucamonga', 1525),
    (5761, 'East Los Angeles', 1525),
    (5762, 'Fullerton', 1525),
    (5763, 'Corona', 1525),
    (5764, 'Concord', 1525),
    (5765, 'Lancaster', 1525),
    (5766, 'Thousand Oaks', 1525),
    (5767, 'Vallejo', 1525),
    (5768, 'Palmdale', 1525),
    (5769, 'El Monte', 1525),
    (5770, 'Inglewood', 1525),
    (5771, 'Simi Valley', 1525),
    (5772, 'Costa Mesa', 1525),
    (5773, 'Downey', 1525),
    (5774, 'West Covina', 1525),
    (5775, 'Daly City', 1525),
    (5776, 'Citrus Heights', 1525),
    (5777, 'Norwalk', 1525),
    (5778, 'Berkeley', 1525),
    (5779, 'Santa Clara', 1525),
    (5780, 'San Buenaventura', 1525),
    (5781, 'Burbank', 1525),
    (5782, 'Mission Viejo', 1525),
    (5783, 'El Cajon', 1525),
    (5784, 'Richmond', 1525),
    (5785, 'Compton', 1525),
    (5786, 'Fairfield', 1525),
    (5787, 'Arden-Arcade', 1525),
    (5788, 'San Mateo', 1525),
    (5789, 'Visalia', 1525),
    (5790, 'Santa Monica', 1525),
    (5791, 'Carson', 1525),
    (5792, 'Denver', 1526),
    (5793, 'Colorado Springs', 1526),
    (5794, 'Aurora', 1526),
    (5795, 'Lakewood', 1526),
    (5796, 'Fort Collins', 1526),
    (5797, 'Arvada', 1526),
    (5798, 'Pueblo', 1526),
    (5799, 'Westminster', 1526),
    (5800, 'Boulder', 1526),
    (5801, 'Bridgeport', 1527),
    (5802, 'New Haven', 1527),
    (5803, 'Hartford', 1527),
    (5804, 'Stamford', 1527),
    (5805, 'Waterbury', 1527),
    (5806, 'Washington', 1528),
    (5807, 'Jacksonville', 1529),
    (5808, 'Miami', 1529),
    (5809, 'Tampa', 1529),
    (5810, 'Saint Petersburg', 1529),
    (5811, 'Hialeah', 1529),
    (5812, 'Orlando', 1529),
    (5813, 'Fort Lauderdale', 1529),
    (5814, 'Tallahassee', 1529),
    (5815, 'Hollywood', 1529),
    (5816, 'Pembroke Pines', 1529),
    (5817, 'Coral Springs', 1529),
    (5818, 'Cape Coral', 1529),
    (5819, 'Clearwater', 1529),
    (5820, 'Miami Beach', 1529),
    (5821, 'Gainesville', 1529),
    (5822, 'Atlanta', 1530),
    (5823, 'Augusta-Richmond County', 1530),
    (5824, 'Columbus', 1530),
    (5825, 'Savannah', 1530),
    (5826, 'Macon', 1530),
    (5827, 'Athens-Clarke County', 1530),
    (5828, 'Honolulu', 1531),
    (5829, 'Boise City', 1532),
    (5830, 'Chicago', 1533),
    (5831, 'Rockford', 1533),
    (5832, 'Aurora', 1533),
    (5833, 'Naperville', 1533),
    (5834, 'Peoria', 1533),
    (5835, 'Springfield', 1533),
    (5836, 'Joliet', 1533),
    (5837, 'Elgin', 1533),
    (5838, 'Indianapolis', 1534),
    (5839, 'Fort Wayne', 1534),
    (5840, 'Evansville', 1534),
    (5841, 'South Bend', 1534),
    (5842, 'Gary', 1534),
    (5843, 'Des Moines', 1535),
    (5844, 'Cedar Rapids', 1535),
    (5845, 'Davenport', 1535),
    (5846, 'Wichita', 1536),
    (5847, 'Overland Park', 1536),
    (5848, 'Kansas City', 1536),
    (5849, 'Topeka', 1536),
    (5850, 'Lexington-Fayette', 1537),
    (5851, 'Louisville', 1537),
    (5852, 'New Orleans', 1538),
    (5853, 'Baton Rouge', 1538),
    (5854, 'Shreveport', 1538),
    (5855, 'Metairie', 1538),
    (5856, 'Lafayette', 1538),
    (5857, 'Baltimore', 1539),
    (5858, 'Boston', 1540),
    (5859, 'Worcester', 1540),
    (5860, 'Springfield', 1540),
    (5861, 'Lowell', 1540),
    (5862, 'Cambridge', 1540),
    (5863, 'New Bedford', 1540),
    (5864, 'Brockton', 1540),
    (5865, 'Fall River', 1540),
    (5866, 'Detroit', 1541),
    (5867, 'Grand Rapids', 1541),
    (5868, 'Warren', 1541),
    (5869, 'Flint', 1541),
    (5870, 'Sterling Heights', 1541),
    (5871, 'Lansing', 1541),
    (5872, 'Ann Arbor', 1541),
    (5873, 'Livonia', 1541),
    (5874, 'Minneapolis', 1542),
    (5875, 'Saint Paul', 1542),
    (5876, 'Jackson', 1543),
    (5877, 'Kansas City', 1544),
    (5878, 'Saint Louis', 1544),
    (5879, 'Springfield', 1544),
    (5880, 'Independence', 1544),
    (5881, 'Billings', 1545),
    (5882, 'Omaha', 1546),
    (5883, 'Lincoln', 1546),
    (5884, 'Las Vegas', 1547),
    (5885, 'Reno', 1547),
    (5886, 'Henderson', 1547),
    (5887, 'Paradise', 1547),
    (5888, 'North Las Vegas', 1547),
    (5889, 'Sunrise Manor', 1547),
    (5890, 'Manchester', 1548),
    (5891, 'Newark', 1549),
    (5892, 'Jersey City', 1549),
    (5893, 'Paterson', 1549),
    (5894, 'Elizabeth', 1549),
    (5895, 'Albuquerque', 1550),
    (5896, 'New York', 1551),
    (5897, 'Buffalo', 1551),
    (5898, 'Rochester', 1551),
    (5899, 'Yonkers', 1551),
    (5900, 'Syracuse', 1551),
    (5901, 'Albany', 1551),
    (5902, 'Charlotte', 1552),
    (5903, 'Raleigh', 1552),
    (5904, 'Greensboro', 1552),
    (5905, 'Durham', 1552),
    (5906, 'Winston-Salem', 1552),
    (5907, 'Fayetteville', 1552),
    (5908, 'Cary', 1552),
    (5909, 'Columbus', 1553),
    (5910, 'Cleveland', 1553),
    (5911, 'Cincinnati', 1553),
    (5912, 'Toledo', 1553),
    (5913, 'Akron', 1553),
    (5914, 'Dayton', 1553),
    (5915, 'Oklahoma City', 1554),
    (5916, 'Tulsa', 1554),
    (5917, 'Norman', 1554),
    (5918, 'Portland', 1555),
    (5919, 'Eugene', 1555),
    (5920, 'Salem', 1555),
    (5921, 'Philadelphia', 1556),
    (5922, 'Pittsburgh', 1556),
    (5923, 'Allentown', 1556),
    (5924, 'Erie', 1556),
    (5925, 'Providence', 1557),
    (5926, 'Columbia', 1558),
    (5927, 'Charleston', 1558),
    (5928, 'Sioux Falls', 1559),
    (5929, 'Memphis', 1560),
    (5930, 'Nashville-Davidson', 1560),
    (5931, 'Knoxville', 1560),
    (5932, 'Chattanooga', 1560),
    (5933, 'Clarksville', 1560),
    (5934, 'Houston', 1561),
    (5935, 'Dallas', 1561),
    (5936, 'San Antonio', 1561),
    (5937, 'Austin', 1561),
    (5938, 'El Paso', 1561),
    (5939, 'Fort Worth', 1561),
    (5940, 'Arlington', 1561),
    (5941, 'Corpus Christi', 1561),
    (5942, 'Plano', 1561),
    (5943, 'Garland', 1561),
    (5944, 'Lubbock', 1561),
    (5945, 'Irving', 1561),
    (5946, 'Laredo', 1561),
    (5947, 'Amarillo', 1561),
    (5948, 'Brownsville', 1561),
    (5949, 'Pasadena', 1561),
    (5950, 'Grand Prairie', 1561),
    (5951, 'Mesquite', 1561),
    (5952, 'Abilene', 1561),
    (5953, 'Beaumont', 1561),
    (5954, 'Waco', 1561),
    (5955, 'Carrollton', 1561),
    (5956, 'McAllen', 1561),
    (5957, 'Wichita Falls', 1561),
    (5958, 'Midland', 1561),
    (5959, 'Odessa', 1561),
    (5960, 'Salt Lake City', 1562),
    (5961, 'West Valley City', 1562),
    (5962, 'Provo', 1562),
    (5963, 'Sandy', 1562),
    (5964, 'Virginia Beach', 1563),
    (5965, 'Norfolk', 1563),
    (5966, 'Chesapeake', 1563),
    (5967, 'Richmond', 1563),
    (5968, 'Newport News', 1563),
    (5969, 'Arlington', 1563),
    (5970, 'Hampton', 1563),
    (5971, 'Alexandria', 1563),
    (5972, 'Portsmouth', 1563),
    (5973, 'Roanoke', 1563),
    (5974, 'Seattle', 1564),
    (5975, 'Spokane', 1564),
    (5976, 'Tacoma', 1564),
    (5977, 'Vancouver', 1564),
    (5978, 'Bellevue', 1564),
    (5979, 'Milwaukee', 1565),
    (5980, 'Madison', 1565),
    (5981, 'Green Bay', 1565),
    (5982, 'Kenosha', 1565),
    (5983, 'Andijon', 1566),
    (5984, 'Buhoro', 1567),
    (5985, 'Cizah', 1568),
    (5986, 'KÃ¼kon', 1569),
    (5987, 'Fargona', 1569),
    (5988, 'Margilon', 1569),
    (5989, 'Nukus', 1570),
    (5990, 'Ãœrgenc', 1571),
    (5991, 'Namangan', 1572),
    (5992, 'Navoi', 1573),
    (5993, 'Karsi', 1574),
    (5994, 'Samarkand', 1575),
    (5995, 'Termiz', 1576),
    (5996, 'Circik', 1577),
    (5997, 'Angren', 1577),
    (5998, 'Olmalik', 1577),
    (5999, 'Toskent', 1578),
    (6000, 'South Hill', 1579),
    (6001, 'The Valley', 1579),
    (6002, 'Oranjestad', 1579),
    (6003, 'Douglas', 1579),
    (6004, 'Gibraltar', 1579),
    (6005, 'Tamuning', 1579),
    (6006, 'AgaÃ±a', 1579),
    (6007, 'Flying Fish Cove', 1579),
    (6008, 'Monte-Carlo', 1579),
    (6009, 'Monaco-Ville', 1579),
    (6010, 'Yangor', 1579),
    (6011, 'Yaren', 1579),
    (6012, 'Alofi', 1579),
    (6013, 'Kingston', 1579),
    (6014, 'Adamstown', 1579),
    (6015, 'Singapore', 1579),
    (6016, 'NoumÃ©a', 1579),
    (6017, 'CittÃ  del Vaticano', 1579),
    (6018, 'Roseau', 1580),
    (6019, 'Saint GeorgeÂ´s', 1580),
    (6020, 'Kingstown', 1580),
    (6025, 'Barcelona', 1582),
    (6026, 'Puerto La Cruz', 1582),
    (6027, 'El Tigre', 1582),
    (6028, 'Pozuelos', 1582),
    (6029, 'San Fernando de Apure', 1583),
    (6030, 'Maracay', 1584),
    (6031, 'Turmero', 1584),
    (6032, 'El LimÃ³n', 1584),
    (6033, 'Barinas', 1585),
    (6034, 'Cartagena', 1586),
    (6035, 'Ciudad Guayana', 1586),
    (6036, 'Ciudad BolÃ­var', 1586),
    (6037, 'Valencia', 1587),
    (6038, 'Puerto Cabello', 1587),
    (6039, 'Guacara', 1587),
    (6040, 'Buenos Aires', 1588),
    (6041, 'BrasÃ­lia', 1588),
    (6042, 'Ciudad de MÃ©xico', 1588),
    (6043, 'Caracas', 1588),
    (6044, 'Catia La Mar', 1588),
    (6045, 'Santa Ana de Coro', 1589),
    (6046, 'Punto Fijo', 1589),
    (6047, 'Calabozo', 1590),
    (6048, 'Valle de la Pascua', 1590),
    (6049, 'Barquisimeto', 1591),
    (6050, 'MÃ©rida', 1592),
    (6051, 'Petare', 1593),
    (6052, 'Baruta', 1593),
    (6053, 'Los Teques', 1593),
    (6054, 'Guarenas', 1593),
    (6055, 'Guatire', 1593),
    (6056, 'Ocumare del Tuy', 1593),
    (6057, 'MaturÃ­n', 1594),
    (6058, 'Acarigua', 1595),
    (6059, 'Guanare', 1595),
    (6060, 'Araure', 1595),
    (6061, 'Sincelejo', 1596),
    (6062, 'CumanÃ¡', 1596),
    (6063, 'CarÃºpano', 1596),
    (6064, 'San CristÃ³bal', 1597),
    (6065, 'Valera', 1598),
    (6066, 'San Felipe', 1599),
    (6067, 'MaracaÃ­bo', 1600),
    (6068, 'Cabimas', 1600),
    (6069, 'Ciudad Ojeda', 1600),
    (6070, 'Road Town', 1601),
    (6071, 'Charlotte Amalie', 1602),
    (6072, 'Long Xuyen', 1603),
    (6073, 'Vung Tau', 1604),
    (6074, 'Thai Nguyen', 1605),
    (6075, 'Quy Nhon', 1606),
    (6076, 'Phan ThiÃªt', 1607),
    (6077, 'Can Tho', 1608),
    (6078, 'Buon Ma Thuot', 1609),
    (6079, 'BiÃªn Hoa', 1610),
    (6080, 'Haiphong', 1611),
    (6081, 'Hanoi', 1612),
    (6082, 'Ho Chi Minh City', 1613),
    (6083, 'Nha Trang', 1614),
    (6084, 'Cam Ranh', 1614),
    (6085, 'Rach Gia', 1615),
    (6086, 'Da Lat', 1616),
    (6087, 'Nam Dinh', 1617),
    (6088, 'Vinh', 1618),
    (6089, 'Cam Pha', 1619),
    (6090, 'Da Nang', 1620),
    (6091, 'Hong Gai', 1621),
    (6092, 'Hue', 1622),
    (6093, 'My Tho', 1623),
    (6094, 'Port-Vila', 1624),
    (6095, 'Mata-Utu', 1625),
    (6096, 'Apia', 1626),
    (6097, 'Aden', 1627),
    (6098, 'al-Mukalla', 1628),
    (6099, 'Hodeida', 1629),
    (6100, 'Ibb', 1630),
    (6101, 'Sanaa', 1631),
    (6102, 'Taizz', 1632),
    (6103, 'Beograd', 1633),
    (6104, 'NiÅ¡', 1633),
    (6105, 'Kragujevac', 1633),
    (6106, 'PriÅ¡tina', 1634),
    (6107, 'Prizren', 1634),
    (6108, 'Podgorica', 1635),
    (6109, 'Novi Sad', 1636),
    (6110, 'Subotica', 1636),
    (6111, 'Port Elizabeth', 1637),
    (6112, 'East London', 1637),
    (6113, 'Uitenhage', 1637),
    (6114, 'Mdantsane', 1637),
    (6115, 'Bloemfontein', 1638),
    (6116, 'Welkom', 1638),
    (6117, 'Botshabelo', 1638),
    (6118, 'Soweto', 1639),
    (6119, 'Johannesburg', 1639),
    (6120, 'Pretoria', 1639),
    (6121, 'Vanderbijlpark', 1639),
    (6122, 'Kempton Park', 1639),
    (6123, 'Alberton', 1639),
    (6124, 'Benoni', 1639),
    (6125, 'Randburg', 1639),
    (6126, 'Vereeniging', 1639),
    (6127, 'Wonderboom', 1639),
    (6128, 'Roodepoort', 1639),
    (6129, 'Boksburg', 1639),
    (6130, 'Soshanguve', 1639),
    (6131, 'Krugersdorp', 1639),
    (6132, 'Brakpan', 1639),
    (6133, 'Oberholzer', 1639),
    (6134, 'Germiston', 1639),
    (6135, 'Springs', 1639),
    (6136, 'Westonaria', 1639),
    (6137, 'Randfontein', 1639),
    (6138, 'Nigel', 1639),
    (6139, 'Inanda', 1640),
    (6140, 'Durban', 1640),
    (6141, 'Pinetown', 1640),
    (6142, 'Pietermaritzburg', 1640),
    (6143, 'Umlazi', 1640),
    (6144, 'Newcastle', 1640),
    (6145, 'Chatsworth', 1640),
    (6146, 'Ladysmith', 1640),
    (6147, 'Witbank', 1641),
    (6148, 'Klerksdorp', 1642),
    (6149, 'Potchefstroom', 1642),
    (6150, 'Rustenburg', 1642),
    (6151, 'Kimberley', 1643),
    (6152, 'Cape Town', 1644),
    (6153, 'Paarl', 1644),(6154, 'George', 1644),(6155, 'Suva', 1645),(6156, 'Nyeri', 1645),(6157, 'Kathmandu', 1645),(6158, 'Lalitapur', 1645),(6159, 'Birgunj', 1645),(6160, 'San Lorenzo', 1645),(6161, 'LambarÃ©', 1645),(6162, 'Fernando de la Mora', 1645),(6163, 'Kabwe', 1645),(6164, 'Kandy', 1645),(6165, 'Kampala', 1645),(6166, 'Ndola', 1646),(6167, 'Kitwe', 1646),(6168, 'Chingola', 1646),(6169, 'Mufulira', 1646),(6170, 'Luanshya', 1646),(6171, 'Lusaka', 1647),(6172, 'Bulawayo', 1648),(6173, 'Harare', 1649),(6174, 'Chitungwiza', 1649),(6175, 'Mount Darwin', 1649),(6176, 'Mutare', 1650),(6177, 'Gweru', 1651)");

                $getDB->query("CREATE TABLE  houdinv_block_users  (houdinv_block_user_id  int(10) UNSIGNED NOT NULL,houdinv_block_user_email  varchar(150) DEFAULT NULL,houdinv_block_user_reason  text,houdinv_block_user_created_at  timestamp NULL DEFAULT NULL,houdinv_block_user_updated_at  timestamp NULL DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1");
    
    //  Table structure for table   houdinv_campaignemail  

                $getDB->query("CREATE TABLE   houdinv_campaignemail   (
          campaignsms_id   int(11) NOT NULL,
          campaign_name   varchar(200) NOT NULL,
          campaign_status   int(11) NOT NULL,
          campaign_text   longblob NOT NULL,
          campaign_date   varchar(200) DEFAULT NULL,
          date_time   int(200) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Table structure for table   houdinv_campaignemail_user  

                $getDB->query("CREATE TABLE   houdinv_campaignemail_user   (
          campaignsms_user_id   int(11) NOT NULL,
          campaignsms_id   int(11) NOT NULL,
          campaignsms_users_id   int(11) NOT NULL,
          campaignsms_group_id   int(11) NOT NULL DEFAULT '0'
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Table structure for table   houdinv_campaignpush  

                $getDB->query("CREATE TABLE   houdinv_campaignpush   (
          campaignsms_id   int(11) NOT NULL,
          campaign_name   varchar(200) NOT NULL,
          campaign_status   int(11) NOT NULL,
          campaign_text   longblob NOT NULL,
          campaign_date   varchar(200) DEFAULT NULL,
          date_time   int(200) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Table structure for table   houdinv_campaignpush_user  

                $getDB->query("CREATE TABLE   houdinv_campaignpush_user   (
          campaignsms_user_id   int(11) NOT NULL,
          campaignsms_id   int(11) NOT NULL,
          campaignsms_users_id   int(11) NOT NULL,
          campaignsms_group_id   int(11) NOT NULL DEFAULT '0'
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

      //  Table structure for table   houdinv_campaignsms  

                $getDB->query("CREATE TABLE   houdinv_campaignsms   (
          campaignsms_id   int(11) NOT NULL,
          campaign_name   varchar(200) NOT NULL,
          campaign_status   int(11) NOT NULL,
          campaign_text   longblob NOT NULL,
          campaign_date   varchar(200) DEFAULT NULL,
          date_time   int(200) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
        
    //  Table structure for table   houdinv_campaignsms_user  

                $getDB->query("CREATE TABLE   houdinv_campaignsms_user   (
          campaignsms_user_id   int(11) NOT NULL,
          campaignsms_id   int(11) NOT NULL,
          campaignsms_users_id   int(11) NOT NULL,
          campaignsms_group_id   int(11) NOT NULL DEFAULT '0'
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Table structure for table   houdinv_categories  

                $getDB->query("CREATE TABLE   houdinv_categories   (
          houdinv_category_id   int(11) NOT NULL,
          houdinv_category_name   varchar(100) NOT NULL,
          houdinv_category_thumb   varchar(255) NOT NULL,
          houdinv_category_description   longtext NOT NULL,
          houdinv_category_subcategory   longtext NOT NULL,
          houdinv_category_status   enum('active','deactive','block','','') NOT NULL,
          houdinv_category_created_date   varchar(100) NOT NULL,
          houdinv_category_updated_date   varchar(100) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Table structure for table   houdinv_categories  

                $getDB->query("INSERT INTO   houdinv_categories   (  houdinv_category_id  ,   houdinv_category_name  ,   houdinv_category_thumb  ,   houdinv_category_description  ,   houdinv_category_subcategory  ,   houdinv_category_status  ,   houdinv_category_created_date  ,   houdinv_category_updated_date  ) VALUES
    (1, 'Women', '6312713.jpg', 'Wome ', '', 'active', '1529302152', '1534912601'),
    (2, 'Shoes', '4227072.jpg', '4.0 out of 5 stars Love the packaging took time for it to get delivered ...\r\nGot it in the cheapest price 36500.... Love the packaging took time for it to get delivered but worth the wait.. Read the full review\r\nPublished 10 months ago by Rahul R', '', 'active', '1529302298', '1532496638'),
    (4, 'Watches', '4479094.jpg', 'Watch collection', '', 'active', '1529312190', '1532496663'),
    (5, 'Men', '6841651.jpg', 'Men', '', 'active', '1530535301', '1532496887'),
    (8, 'Electronics', '943940logo_header_banner.jpg', 'fgfh', '', 'active', '1533015384', '1533021557'),
    (6, 'gf', '3616131_g9-lYSlHSDi-WJJTWQg5Ww.jpg', 'fgfdg', '', 'active', '1531385425', '1531385425'),
    (7, 'k', '404920default.jpg', 'jkj', '', 'deactive', '1531393122', '1531358298')");

    //  Table structure for table   houdinv_checkout_address_data  

                $getDB->query("CREATE TABLE   houdinv_checkout_address_data   (
          houdinv_data_id   int(11) NOT NULL,
          houdinv_data_first_name   varchar(100) NOT NULL,
          houdinv_data_last_name   varchar(100) NOT NULL,
          houdinv_data_company_name   varchar(100) NOT NULL,
          houdinv_data_email   varchar(100) NOT NULL,
          houdinv_data_phone_no   varchar(100) NOT NULL,
          houdinv_data_country   varchar(100) NOT NULL,
          houdinv_data_address   text NOT NULL,
          houdinv_data_zip   varchar(12) NOT NULL,
          houdinv_data_city   varchar(100) NOT NULL,
          houdinv_data_user_id   int(11) NOT NULL,
          houdinv_data_order_note   text NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Dumping data for table   houdinv_checkout_address_data   

                $getDB->query("INSERT INTO   houdinv_checkout_address_data   (  houdinv_data_id  ,   houdinv_data_first_name  ,   houdinv_data_last_name  ,   houdinv_data_company_name  ,   houdinv_data_email  ,   houdinv_data_phone_no  ,   houdinv_data_country  ,   houdinv_data_address  ,   houdinv_data_zip  ,   houdinv_data_city  ,   houdinv_data_user_id  ,   houdinv_data_order_note  ) VALUES
    (1, 'payal', '', '', 'hawkscodeteam@gmail.com', '9785940038', 'canada', 'hfghg', '2345', 'h', 2, 'tryt'),
    (2, 'ghhjghj', 'hjgjhgh', 'vbnvhjgvjhg', 'hs.94.1996@gmail.com', '8947086785', 'canada', 'gfg', '302033', 'jaipur', 2, 'gg'),
    (3, 'ghhjghj', 'hjgjhgh', 'vbnvhjgvjhg', 'hs.94.1996@gmail.com', '121231', 'canada', 'hjgjhgh', '302033', 'jaipur', 2, 'wer'),
    (4, 'shivam', 'sengar', 'Shivam', 'shivam199420@gmail.com', '+917790870946', 'canada', 'sdfsd', '12412', 'jaipur', 42, ''),
    (5, 'shivam', 'sengar', 'asda', 'shivam199420@gmail.com', '+917790870946', 'american', 'asdad', '2131', 'asda', 42, 'asda'),
    (6, 'first', 'customer', '', 'first@gmail.com', '987654321', 'american', 'asdf asdfsd adsa', '1543354', 'georgia', 45, ''),
    (7, 'payal', 'goyal', '', 'goyalpayal1995@gmail.com', '+919785940038', 'canada', '119/567 mansarover', '302020', 'jaipur', 44, ''),
    (8, 'bhupendra', 'kumar', '', 'bhupendra@hawkscode.com', '00000000000000000000', 'canada', 'fhfh', '3523453', '5345345', 67, ''),
    (9, ' xf', 'f ', '', 'f', 'f', 'canada', 'f', 'f', 'f', 0, 'cc'),
    (10, 'x', 'x', 'x', 'vikashjoshi151@gmail.com', '123456789', 'canada', 'dgdfdg', 'dfgfd', 'dfgfd', 0, 'v'),
    (11, 'payal', '', '', 'Chris@gmail.com', '07777777777', 'canada', 'Address', '9854', 'City', 2, '')");

    //  Table structure for table  houdinv_countries   

                $getDB->query("CREATE TABLE   houdinv_countries   (
          country_id   int(11) NOT NULL,
          country_name   varchar(30) CHARACTER SET utf8 NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

    //  Dumping data for table   houdinv_countries     

                $getDB->query("INSERT INTO   houdinv_countries   (  country_id  ,   country_name  ) VALUES
    (1, 'Aruba'),
    (2, 'Afghanistan'),
    (3, 'Angola'),
    (4, 'Anguilla'),
    (5, 'Albania'),
    (6, 'Andorra'),
    (7, 'Netherlands Antilles'),
    (8, 'United Arab Emirates'),
    (9, 'Argentina'),
    (10, 'Armenia'),
    (11, 'American Samoa'),
    (12, 'Antarctica'),
    (13, 'French Southern territories'),
    (14, 'Antigua and Barbuda'),
    (15, 'Australia'),
    (16, 'Austria'),
    (17, 'Azerbaijan'),
    (18, 'Burundi'),
    (19, 'Belgium'),
    (20, 'Benin'),
    (21, 'Burkina Faso'),
    (22, 'Bangladesh'),
    (23, 'Bulgaria'),
    (24, 'Bahrain'),
    (25, 'Bahamas'),
    (26, 'Bosnia and Herzegovina'),
    (27, 'Belarus'),
    (28, 'Belize'),
    (29, 'Bermuda'),
    (30, 'Bolivia'),
    (31, 'Brazil'),
    (32, 'Barbados'),
    (33, 'Brunei'),
    (34, 'Bhutan'),
    (35, 'Bouvet Island'),
    (36, 'Botswana'),
    (37, 'Central African Republic'),
    (38, 'Canada'),
    (39, 'Cocos (Keeling) Islands'),
    (40, 'Switzerland'),
    (41, 'Chile'),
    (42, 'China'),
    (43, 'CÃ´te dâ€™Ivoire'),
    (44, 'Cameroon'),
    (45, 'Congo, The Democratic Republic'),
    (46, 'Congo'),
    (47, 'Cook Islands'),
    (48, 'Colombia'),
    (49, 'Comoros'),
    (50, 'Cape Verde'),
    (51, 'Costa Rica'),
    (52, 'Cuba'),
    (53, 'Christmas Island'),
    (54, 'Cayman Islands'),
    (55, 'Cyprus'),
    (56, 'Czech Republic'),
    (57, 'Germany'),
    (58, 'Djibouti'),
    (59, 'Dominica'),
    (60, 'Denmark'),
    (61, 'Dominican Republic'),
    (62, 'Algeria'),
    (63, 'Ecuador'),
    (64, 'Egypt'),
    (65, 'Eritrea'),
    (66, 'Western Sahara'),
    (67, 'Spain'),
    (68, 'Estonia'),
    (69, 'Ethiopia'),
    (70, 'Finland'),
    (71, 'Fiji Islands'),
    (72, 'Falkland Islands'),
    (73, 'France'),
    (74, 'Faroe Islands'),
    (75, 'Micronesia, Federated States o'),
    (76, 'Gabon'),
    (77, 'United Kingdom'),
    (78, 'Georgia'),
    (79, 'Ghana'),
    (80, 'Gibraltar'),
    (81, 'Guinea'),
    (82, 'Guadeloupe'),
    (83, 'Gambia'),
    (84, 'Guinea-Bissau'),
    (85, 'Equatorial Guinea'),
    (86, 'Greece'),
    (87, 'Grenada'),
    (88, 'Greenland'),
    (89, 'Guatemala'),
    (90, 'French Guiana'),
    (91, 'Guam'),
    (92, 'Guyana'),
    (93, 'Hong Kong'),
    (94, 'Heard Island and McDonald Isla'),
    (95, 'Honduras'),
    (96, 'Croatia'),
    (97, 'Haiti'),
    (98, 'Hungary'),
    (99, 'Indonesia'),
    (100, 'India'),
    (101, 'British Indian Ocean Territory'),
    (102, 'Ireland'),
    (103, 'Iran'),
    (104, 'Iraq'),
    (105, 'Iceland'),
    (106, 'Israel'),
    (107, 'Italy'),
    (108, 'Jamaica'),
    (109, 'Jordan'),
    (110, 'Japan'),
    (111, 'Kazakstan'),
    (112, 'Kenya'),
    (113, 'Kyrgyzstan'),
    (114, 'Cambodia'),
    (115, 'Kiribati'),
    (116, 'Saint Kitts and Nevis'),
    (117, 'South Korea'),
    (118, 'Kuwait'),
    (119, 'Laos'),
    (120, 'Lebanon'),
    (121, 'Liberia'),
    (122, 'Libyan Arab Jamahiriya'),
    (123, 'Saint Lucia'),
    (124, 'Liechtenstein'),
    (125, 'Sri Lanka'),
    (126, 'Lesotho'),
    (127, 'Lithuania'),
    (128, 'Luxembourg'),
    (129, 'Latvia'),
    (130, 'Macao'),
    (131, 'Morocco'),
    (132, 'Monaco'),
    (133, 'Moldova'),
    (134, 'Madagascar'),
    (135, 'Maldives'),
    (136, 'Mexico'),
    (137, 'Marshall Islands'),
    (138, 'Macedonia'),
    (139, 'Mali'),
    (140, 'Malta'),
    (141, 'Myanmar'),
    (142, 'Mongolia'),
    (143, 'Northern Mariana Islands'),
    (144, 'Mozambique'),
    (145, 'Mauritania'),
    (146, 'Montserrat'),
    (147, 'Martinique'),
    (148, 'Mauritius'),
    (149, 'Malawi'),
    (150, 'Malaysia'),
    (151, 'Mayotte'),
    (152, 'Namibia'),
    (153, 'New Caledonia'),
    (154, 'Niger'),
    (155, 'Norfolk Island'),
    (156, 'Nigeria'),
    (157, 'Nicaragua'),
    (158, 'Niue'),
    (159, 'Netherlands'),
    (160, 'Norway'),
    (161, 'Nepal'),
    (162, 'Nauru'),
    (163, 'New Zealand'),
    (164, 'Oman'),
    (165, 'Pakistan'),
    (166, 'Panama'),
    (167, 'Pitcairn'),
    (168, 'Peru'),
    (169, 'Philippines'),
    (170, 'Palau'),
    (171, 'Papua New Guinea'),
    (172, 'Poland'),
    (173, 'Puerto Rico'),
    (174, 'North Korea'),
    (175, 'Portugal'),
    (176, 'Paraguay'),
    (177, 'Palestine'),
    (178, 'French Polynesia'),
    (179, 'Qatar'),
    (180, 'RÃ©union'),
    (181, 'Romania'),
    (182, 'Russian Federation'),
    (183, 'Rwanda'),
    (184, 'Saudi Arabia'),
    (185, 'Sudan'),
    (186, 'Senegal'),
    (187, 'Singapore'),
    (188, 'South Georgia and the South Sa'),
    (189, 'Saint Helena'),
    (190, 'Svalbard and Jan Mayen'),
    (191, 'Solomon Islands'),
    (192, 'Sierra Leone'),
    (193, 'El Salvador'),
    (194, 'San Marino'),
    (195, 'Somalia'),
    (196, 'Saint Pierre and Miquelon'),
    (197, 'Sao Tome and Principe'),
    (198, 'Suriname'),
    (199, 'Slovakia'),
    (200, 'Slovenia'),
    (201, 'Sweden'),
    (202, 'Swaziland'),
    (203, 'Seychelles'),
    (204, 'Syria'),
    (205, 'Turks and Caicos Islands'),
    (206, 'Chad'),
    (207, 'Togo'),
    (208, 'Thailand'),
    (209, 'Tajikistan'),
    (210, 'Tokelau'),
    (211, 'Turkmenistan'),
    (212, 'East Timor'),
    (213, 'Tonga'),
    (214, 'Trinidad and Tobago'),
    (215, 'Tunisia'),
    (216, 'Turkey'),
    (217, 'Tuvalu'),
    (218, 'Taiwan'),
    (219, 'Tanzania'),
    (220, 'Uganda'),
    (221, 'Ukraine'),
    (222, 'United States Minor Outlying I'),
    (223, 'Uruguay'),
    (224, 'United States'),
    (225, 'Uzbekistan'),
    (226, 'Holy See (Vatican City State)'),
    (227, 'Saint Vincent and the Grenadin'),
    (228, 'Venezuela'),
    (229, 'Virgin Islands, British'),
    (230, 'Virgin Islands, U.S.'),
    (231, 'Vietnam'),
    (232, 'Vanuatu'),
    (233, 'Wallis and Futuna'),
    (234, 'Samoa'),
    (235, 'Yemen'),
    (236, 'Yugoslavia'),
    (237, 'South Africa'),
    (238, 'Zambia'),
    (239, 'Zimbabwe')");

    //  Table structure for table  houdinv_coupons   

                $getDB->query("CREATE TABLE   houdinv_coupons   (
          houdinv_coupons_id   int(11) NOT NULL,
          houdinv_coupons_name   varchar(50) NOT NULL,
          houdinv_coupons_tagline   varchar(50) NOT NULL,
          houdinv_coupons_valid_from   date NOT NULL,
          houdinv_coupons_valid_to   date NOT NULL,
          houdinv_coupons_order_amount   int(11) NOT NULL,
          houdinv_coupons_code   varchar(50) NOT NULL,
          houdinv_coupons_discount_precentage   varchar(20) NOT NULL,
          houdinv_coupons_limit   int(11) NOT NULL,
          houdinv_coupons_payment_method   enum('cod','online payment','both') NOT NULL,
          houdinv_coupons_status   enum('active','deactive') NOT NULL,
          houdinv_coupons_applicable   enum('app','both') NOT NULL,
          houdinv_coupons_created_at   varchar(30) NOT NULL,
          houdinv_coupons_modified_at   varchar(30) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Table structure for table  houdinv_coupons   

                $getDB->query("INSERT INTO   houdinv_coupons   (  houdinv_coupons_id  ,   houdinv_coupons_name  ,   houdinv_coupons_tagline  ,   houdinv_coupons_valid_from  ,   houdinv_coupons_valid_to  ,   houdinv_coupons_order_amount  ,   houdinv_coupons_code  ,   houdinv_coupons_discount_precentage  ,   houdinv_coupons_limit  ,   houdinv_coupons_payment_method  ,   houdinv_coupons_status  ,   houdinv_coupons_applicable  ,   houdinv_coupons_created_at  ,   houdinv_coupons_modified_at  ) VALUES
    (1, 'Coupon 1', 'Coupon 1', '2018-07-07', '2018-11-30', 12, 't4uxv8', '12', 400, 'both', 'active', 'both', '1530947625', ''),
    (2, 'Coupon2', 'coupon2', '2018-08-13', '2018-10-26', 120, '3J3iRE', '10', 20, 'both', 'active', 'both', '1534239713', '')");

    //  Table structure for table  houdinv_coupons_procus   

                $getDB->query("CREATE TABLE   houdinv_coupons_procus   (
          houdinv_coupons_procus_id   int(11) NOT NULL,
          houdinv_coupons_procus_coupon_id   int(11) NOT NULL,
          houdinv_coupons_procus_type   enum('product','customer') NOT NULL,
          houdinv_coupons_procus_user_id   int(11) DEFAULT NULL,
          houdinv_coupons_procus_product_id   int(11) DEFAULT NULL,
          houdinv_coupons_procus_user_group   int(11) NOT NULL DEFAULT '0',
          houdinv_coupons_procus_produdct_category   int(11) NOT NULL DEFAULT '0'
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
      
      //  Dumping data for table   houdinv_coupons_procus    

                $getDB->query("INSERT INTO   houdinv_coupons_procus   (  houdinv_coupons_procus_id  ,   houdinv_coupons_procus_coupon_id  ,   houdinv_coupons_procus_type  ,   houdinv_coupons_procus_user_id  ,   houdinv_coupons_procus_product_id  ,   houdinv_coupons_procus_user_group  ,   houdinv_coupons_procus_produdct_category  ) VALUES
      (16, 1, 'product', 0, 1, 0, 1),
      (15, 1, 'customer', 6, 0, 1, 0),
      (14, 1, 'customer', 5, 0, 4, 0),
      (13, 1, 'customer', 5, 0, 1, 0),
      (12, 1, 'product', 0, 3, 0, 0),
      (11, 1, 'product', 0, 20, 0, 0),
      (10, 1, 'customer', 6, 0, 0, 0),
      (9, 1, 'customer', 5, 0, 0, 0),
      (17, 1, 'product', 0, 2, 0, 2),
      (18, 1, 'product', 0, 3, 0, 1),
      (19, 1, 'product', 0, 4, 0, 1),
      (20, 1, 'product', 0, 5, 0, 2),
      (21, 1, 'product', 0, 6, 0, 1),
      (22, 2, 'customer', 2, 0, 0, 0),
      (23, 2, 'customer', 4, 0, 0, 0),
      (24, 2, 'customer', 5, 0, 1, 0),
      (25, 2, 'customer', 6, 0, 1, 0),
      (26, 2, 'product', 0, 28, 0, 2)");

    //  Table structure for table   houdinv_customersetting      

                $getDB->query("CREATE TABLE houdinv_customersetting(id   int(11) NOT NULL,pay_panding_amount   varchar(250) NOT NULL,pay_panding_period   varchar(250) NOT NULL,          allow_pay_panding   int(11) DEFAULT '0',date_time   varchar(250) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //   Dumping data for table   houdinv_customersetting

                $getDB->query("INSERT INTO   houdinv_customersetting   (  id  ,   pay_panding_amount  ,   pay_panding_period  ,   allow_pay_panding  ,   date_time  ) VALUES
    (1, '3000', '20', 1, '1530538606')");

     //  Table structure for table   houdinv_custom_home_data      

                $getDB->query("CREATE TABLE   houdinv_custom_home_data   (
          houdinv_custom_home_data_id   int(11) NOT NULL,
          houdinv_custom_home_data_category   varchar(100) DEFAULT NULL,
          houdinv_custom_home_data_latest_product   varchar(100) DEFAULT NULL,
          houdinv_custom_home_data_featured_product   varchar(100) DEFAULT NULL,
          houdinv_custom_home_data_testimonial   varchar(100) DEFAULT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

      //  Dumping data for table   houdinv_custom_home_data       

                $getDB->query("INSERT INTO   houdinv_custom_home_data   (  houdinv_custom_home_data_id  ,   houdinv_custom_home_data_category  ,   houdinv_custom_home_data_latest_product  ,   houdinv_custom_home_data_featured_product  ,   houdinv_custom_home_data_testimonial  ) VALUES
     (11, 'Top Category', 'Latest Product', 'Featured Product', 'Testimonials')");

    //  Table structure for table   houdinv_custom_links  

                $getDB->query("CREATE TABLE   houdinv_custom_links   (
          houdinv_custom_links_id   int(11) NOT NULL,
          houdinv_custom_links_title   varchar(30) NOT NULL,
          houdinv_custom_links_url   varchar(100) NOT NULL,
          houdinv_custom_links_target   enum('current','new') NOT NULL,
          houdinv_custom_links_created_at   varchar(30) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Dumping data for table   houdinv_custom_links  

                $getDB->query("INSERT INTO   houdinv_custom_links   (  houdinv_custom_links_id  ,   houdinv_custom_links_title  ,   houdinv_custom_links_url  ,   houdinv_custom_links_target  ,   houdinv_custom_links_created_at  ) VALUES
    (5, 'Test', 'https://tech.hawkscode.in/houdin-e', 'new', '1534238259'),
    (6, 'Test', 'https://tech.hawkscode.in/houdin-e/vendor', 'new', '1535108119'),
    (7, 'Test', 'https://tech.hawkscode.in/houdin-e/Shop/home/shivamhawskcode', 'current', '1536624000')");

// Table structure for table   houdinv_custom_slider  

                $getDB->query("CREATE TABLE   houdinv_custom_slider   (
      houdinv_custom_slider_id   int(11) NOT NULL,
      houdinv_custom_slider_image   text NOT NULL,
      houdinv_custom_slider_head   varchar(30) NOT NULL,
      houdinv_custom_slider_sub_heading   varchar(50) NOT NULL,
      houdinv_custom_slider_created_at   varchar(30) DEFAULT NULL,
      houdinv_custom_slider_modified_at   varchar(30) DEFAULT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

  // Dumping data for table   houdinv_custom_slider   

                $getDB->query("INSERT INTO   houdinv_custom_slider   (  houdinv_custom_slider_id  ,   houdinv_custom_slider_image  ,   houdinv_custom_slider_head  ,   houdinv_custom_slider_sub_heading  ,   houdinv_custom_slider_created_at  ,   houdinv_custom_slider_modified_at  ) VALUES
(4, 'slider_387010715794.jpg', 'Free Authority', 'Joker collection 2018', '1532598205', '1532598237'),
(3, 'slider_225833set.jpg', 'Joker Collection', 'joker 2018', '1532590496', NULL)");

// Table structure for table   houdinv_deliveries  

                $getDB->query("CREATE TABLE   houdinv_deliveries   (
      houdinv_delivery_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_order_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_by   enum('Delivery Boy','Delivery Service') NOT NULL,
      houdinv_delivery_boy_id   int(11) NOT NULL DEFAULT '0',
      houdinv_delivery_courier_id   tinytext,
      houdinv_deliveries_courier_name   varchar(100) DEFAULT NULL,
      houdinv_deliveries_tracking_id   int(11) NOT NULL DEFAULT '0',
      houdinv_deliveries_invoice_url   tinytext NOT NULL,
      houdinv_delivery_created_at   varchar(30) DEFAULT NULL,
      houdinv_delivery_updated_at   varchar(30) DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

  // Dumping data for table   houdinv_deliveries  

                $getDB->query("INSERT INTO   houdinv_deliveries   (  houdinv_delivery_id  ,   houdinv_delivery_order_id  ,   houdinv_delivery_by  ,   houdinv_delivery_boy_id  ,   houdinv_delivery_courier_id  ,   houdinv_deliveries_courier_name  ,   houdinv_deliveries_tracking_id  ,   houdinv_deliveries_invoice_url  ,   houdinv_delivery_created_at  ,   houdinv_delivery_updated_at  ) VALUES
(1, 20, 'Delivery Boy', 2, '0', NULL, 0, '', '1534464000', NULL),
(2, 15, 'Delivery Boy', 0, '3', NULL, 0, '', '1534464000', NULL),
(3, 11, 'Delivery Service', 0, '3', NULL, 0, '', '1534550400', NULL),
(8, 229, 'Delivery Boy', 7, '0', NULL, 0, '', '1536278400', NULL),
(9, 235, 'Delivery Service', 0, '07b55d06-48af-4b02-a6b0-1e311e22b1e6', NULL, 0, '', '1536537600', NULL),
(10, 235, 'Delivery Service', 0, 'c795f54d-fd64-421f-be96-6cb31b694d9c', 'aramex_priority_express', 2147483647, 'https://sandbox-download.postmen.com/label/2018-09-10/c795f54d-fd64-421f-be96-6cb31b694d9c-1536558103945549.pdf', '1536537600', NULL)");


// Table structure for table   houdinv_delivery_tracks   

                $getDB->query("CREATE TABLE   houdinv_delivery_tracks   (
      houdinv_delivery_track_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_track_order_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_track_delivery_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_track_updated_by   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_track_date   datetime NOT NULL,
      houdinv_delivery_track_status   tinyint(4) NOT NULL,
      houdinv_delivery_track_comment   text NOT NULL,
      houdinv_delivery_track_created_at   timestamp NULL DEFAULT NULL,
      houdinv_delivery_track_updated_at   timestamp NULL DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

  // Table structure for table   houdinv_delivery_users   

                $getDB->query("CREATE TABLE   houdinv_delivery_users   (
      houdinv_delivery_user_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_user_name   varchar(150) DEFAULT NULL,
      houdinv_delivery_user_email   varchar(150) DEFAULT NULL,
      houdinv_delivery_user_contact   varchar(20) DEFAULT NULL,
      houdinv_delivery_user_attributes   text,
      houdinv_delivery_user_address   varchar(350) DEFAULT NULL,
      houdinv_delivery_user_active_status   tinyint(4) DEFAULT '0',
      houdinv_delivery_user_created_at   timestamp NULL DEFAULT NULL,
      houdinv_delivery_user_updated_at   timestamp NULL DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

  // Table structure for table   houdinv_emailsms_payment   

                $getDB->query("CREATE TABLE   houdinv_emailsms_payment   (
      houdinv_emailsms_payment_id   int(11) NOT NULL,
      houdinv_emailsms_payment_type   enum('sms','email') NOT NULL,
      houdinv_emailsms_payment_total_purchase   int(11) NOT NULL,
      houdinv_emailsms_payment_amount   float NOT NULL,
      houdinv_emailsms_payment_status   enum('active','deactive') NOT NULL,
      houdinv_emailsms_payment_created_at   varchar(30) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

  // Table structure for table   houdinv_emailsms_stats   

                $getDB->query("CREATE TABLE   houdinv_emailsms_stats   (
      houdinv_emailsms_stats_id   int(11) NOT NULL,
      houdinv_emailsms_stats_type   enum('sms','email') NOT NULL,
      houdinv_emailsms_stats_total_credit   int(11) NOT NULL,
      houdinv_emailsms_stats_remaining_credits   int(11) NOT NULL,
      date_time   varchar(250) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

  // Dumping data for table   houdinv_emailsms_stats    

                $getDB->query("INSERT INTO   houdinv_emailsms_stats   (  houdinv_emailsms_stats_id  ,   houdinv_emailsms_stats_type  ,   houdinv_emailsms_stats_total_credit  ,   houdinv_emailsms_stats_remaining_credits  ,   date_time  ) VALUES
  (1, 'sms', 960, 1099, '1531981064'),
  (3, 'email', 1200, 1353, '1530863963')");

    // Table structure for table   houdinv_email_log   

                $getDB->query("CREATE TABLE   houdinv_email_log   (
          houdinv_email_log_id   int(11) NOT NULL,
          houdinv_email_log_credit_used   int(11) NOT NULL,
          houdinv_email_log_status   int(1) NOT NULL,
          houdinv_email_log_created_at   varchar(30) DEFAULT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    // Dumping data for table   houdinv_email_log  

                $getDB->query("INSERT INTO   houdinv_email_log   (  houdinv_email_log_id  ,   houdinv_email_log_credit_used  ,   houdinv_email_log_status  ,   houdinv_email_log_created_at  ) VALUES
    (1, 1, 1, '1532338723'),
    (2, 1, 1, '1532339050'),
    (3, 1, 1, '1533100541'),
    (4, 1, 1, '1533100901'),
    (5, 1, 1, '1533100937'),
    (6, 1, 1, '1533102961'),
    (7, 1, 1, '1533103466'),
    (8, 1, 1, '1533103730'),
    (9, 1, 1, '1533104226'),
    (10, 1, 1, '1533106581'),
    (11, 1, 1, '1533106695'),
    (12, 1, 1, '1533106701'),
    (13, 1, 1, '1533106929'),
    (14, 1, 1, '1533106961'),
    (15, 1, 1, '1533107408'),
    (16, 1, 1, '1533107558'),
    (17, 1, 1, '1533107655'),
    (18, 1, 1, '1533107778'),
    (19, 1, 1, '1533108996'),
    (20, 1, 1, '1533110178'),
    (21, 1, 1, '1533116270'),
    (22, 1, 1, '1533116442'),
    (23, 1, 1, '1533116784'),
    (24, 1, 1, '1533128663'),
    (25, 1, 1, '1533128837'),
    (26, 1, 1, '1533128856'),
    (27, 1, 1, '1533128982'),
    (28, 1, 1, '1533129255'),
    (29, 1, 1, '1533129296'),
    (30, 1, 1, '1533129387'),
    (31, 1, 1, '1533129404'),
    (32, 1, 1, '1533129472'),
    (33, 1, 1, '1533129487'),
    (34, 1, 1, '1533129577'),
    (35, 1, 1, '1533129586'),
    (36, 1, 1, '1533129662'),
    (37, 1, 1, '1533129666'),
    (38, 1, 1, '1533130007'),
    (39, 1, 1, '1533130016'),
    (40, 1, 1, '1533130188'),
    (41, 1, 1, '1533130261'),
    (42, 1, 1, '1533130395'),
    (43, 1, 1, '1533130439'),
    (44, 1, 1, '1533130450'),
    (45, 1, 1, '1533130512'),
    (46, 1, 1, '1533130682'),
    (47, 1, 1, '1533130688'),
    (48, 1, 1, '1533131108'),
    (49, 1, 1, '1533131400'),
    (50, 1, 1, '1533131427'),
    (51, 1, 1, '1533131492'),
    (52, 1, 1, '1533131589'),
    (53, 1, 1, '1533131650'),
    (54, 1, 1, '1533131665'),
    (55, 1, 1, '1533131710'),
    (56, 1, 1, '1533131798'),
    (57, 1, 1, '1533132027'),
    (58, 1, 1, '1533132206'),
    (59, 1, 1, '1533186040'),
    (60, 1, 1, '1533186055'),
    (61, 1, 1, '1533186475'),
    (62, 1, 1, '1533186484'),
    (63, 1, 1, '1533186812'),
    (64, 1, 1, '1533187405'),
    (65, 1, 1, '1533187417'),
    (66, 1, 1, '1533187569'),
    (67, 1, 1, '1533187579'),
    (68, 1, 1, '1533187713'),
    (69, 1, 1, '1533187720'),
    (70, 1, 1, '1533188015'),
    (71, 1, 1, '1533188024'),
    (72, 1, 1, '1533188342'),
    (73, 1, 1, '1533188351'),
    (74, 1, 1, '1533188418'),
    (75, 1, 1, '1533188424'),
    (76, 1, 1, '1533707316'),
    (77, 1, 1, '1533707329'),
    (78, 1, 1, '1534730263'),
    (79, 1, 1, '1534730302'),
    (80, 1, 1, '1534730742'),
    (81, 1, 1, '1534838285'),
    (82, 1, 1, '1534982400'),
    (83, 1, 1, '1534982400'),
    (84, 1, 1, '1534982400'),
    (85, 0, 0, '1534982400'),
    (86, 1, 1, '1535024411'),
    (87, 1, 1, '1535068800'),
    (88, 1, 1, '1535500800'),
    (89, 1, 1, '1535500800'),
    (90, 1, 1, '1535500800'),
    (91, 0, 0, '1535500800'),
    (92, 0, 0, '1535500800'),
    (93, 1, 1, '1535500800'),
    (94, 1, 1, '1535500800'),
    (95, 0, 0, '1535587200'),
    (96, 1, 1, '1535587200'),
    (97, 1, 1, '1535587200'),
    (98, 1, 1, '1535673600'),
    (99, 1, 1, '1535673600'),
    (100, 0, 0, '1535673600'),
    (101, 1, 1, '1535673600'),
    (102, 1, 1, '1535673600'),
    (103, 1, 1, '1535673600'),
    (104, 1, 1, '1535673600'),
    (105, 1, 1, '1535673600'),
    (106, 1, 1, '1535673600'),
    (107, 1, 1, '1535673600'),
    (108, 1, 1, '1535760000'),
    (109, 1, 1, '1535760000'),
    (110, 1, 1, '1535760000'),
    (111, 1, 1, '1535760000'),
    (112, 1, 1, '1535760000'),
    (113, 1, 1, '1535760000'),
    (114, 1, 1, '1535760000'),
    (115, 1, 1, '1535760000'),
    (116, 1, 1, '1535760000'),
    (117, 1, 1, '1535760000'),
    (118, 1, 1, '1535760000'),
    (119, 1, 1, '1535760000'),
    (120, 1, 1, '1535760000'),
    (121, 1, 1, '1535760000'),
    (122, 1, 1, '1535760000'),
    (123, 1, 1, '1535932800'),
    (124, 1, 1, '1536105600'),
    (125, 1, 1, '1536105600'),
    (126, 1, 1, '1536105600'),
    (127, 1, 1, '1536105600'),
    (128, 1, 1, '1536105600'),
    (129, 1, 1, '1536192000'),
    (130, 1, 1, '1536278400'),
    (131, 1, 1, '1536537600'),
    (132, 1, 1, '1536587077'),
    (133, 1, 1, '1536624000'),
    (134, 1, 1, '1536624000'),
    (135, 1, 1, '1536624000'),
    (136, 1, 1, '1536624000'),
    (137, 1, 1, '1536624000'),
    (138, 1, 1, '1536624000'),
    (139, 1, 1, '1536624000'),
    (140, 1, 1, '1536624000'),
    (141, 1, 1, '1536624000'),
    (142, 1, 1, '1536710400'),
    (143, 1, 1, '1536710400'),
    (144, 1, 1, '1536710400'),
    (145, 1, 1, '1536710400'),
    (146, 1, 1, '1536745559'),
    (147, 1, 1, '1536745811'),
    (148, 1, 1, '1536796800'),
    (149, 1, 1, '1536832069'),
    (150, 1, 1, '1536921199'),
    (151, 1, 1, '1536883200'),
    (152, 1, 1, '1536969600'),
    (153, 1, 1, '1536969600'),
    (154, 1, 1, '1536969600'),
    (155, 1, 1, '1536969600'),
    (156, 1, 1, '1536969600'),
    (157, 1, 1, '1536969600'),
    (158, 1, 1, '1537142400'),
    (159, 1, 1, '1537142400'),
    (160, 1, 1, '1537142400'),
    (161, 1, 1, '1537228800'),
    (162, 1, 1, '1537249027'),
    (163, 1, 1, '1537249045'),
    (164, 1, 1, '1537228800'),
    (165, 1, 1, '1537249687'),
    (166, 1, 1, '1537228800'),
    (167, 1, 1, '1537250314'),
    (168, 1, 1, '1537228800'),
    (169, 1, 1, '1537260721'),
    (170, 1, 1, '1537228800'),
    (171, 1, 1, '1537228800'),
    (172, 0, 0, '1537228800'),
    (173, 0, 0, '1537228800'),
    (174, 0, 0, '1537228800'),
    (175, 1, 1, '1537315200'),
    (176, 1, 1, '1537315200'),
    (177, 1, 1, '1537315200'),
    (178, 0, 0, '1537315200'),
    (179, 1, 1, '1537315200'),
    (180, 1, 1, '1537315200'),
    (181, 0, 0, '1537401600'),
    (182, 1, 1, '1537401600'),
    (183, 0, 0, '1537488000'),
    (184, 1, 1, '1538092800'),
    (185, 1, 1, '1538092800'),
    (186, 1, 1, '1538092800'),
    (187, 1, 1, '1538092800'),
    (188, 1, 1, '1538092800'),
    (189, 1, 1, '1538138351'),
    (190, 1, 1, '1538138450'),
    (191, 1, 1, '1538138546'),
    (192, 1, 1, '1538179200'),
    (193, 1, 1, '1538179200'),
    (194, 1, 1, '1538179200'),
    (195, 1, 1, '1538179200'),
    (196, 1, 1, '1538179200'),
    (197, 1, 1, '1538352000'),
    (198, 1, 1, '1538352000'),
    (199, 1, 1, '1538352000'),
    (200, 1, 1, '1538438400'),
    (201, 1, 1, '1538438400')");

        // Table structure for table   houdinv_email_Template   

                $getDB->query("CREATE TABLE   houdinv_email_Template   (
              houdinv_email_template_id   int(11) NOT NULL,
              houdinv_email_template_type   varchar(50) NOT NULL,
              houdinv_email_template_subject   varchar(255) NOT NULL,
              houdinv_email_template_message   text NOT NULL,
              houdinv_email_template_created_date   varchar(100) NOT NULL,
              houdinv_email_template_updated_date   varchar(100) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table   houdinv_email_Template  

                $getDB->query("INSERT INTO   houdinv_email_Template   (  houdinv_email_template_id  ,   houdinv_email_template_type  ,   houdinv_email_template_subject  ,   houdinv_email_template_message  ,   houdinv_email_template_created_date  ,   houdinv_email_template_updated_date  ) VALUES
        (1, 'register', 'Welcome on Houdin-e', '<p>Hello&nbsp;<span style=\"color: rgb(121, 121, 121);\">{user_name},</span></p><p><span style=\"color: rgb(121, 121, 121);\">Welcome to houdin-e</span></p>', '1532653504', '1535019681'),
        (4, 'On_order', 'Congrats on order', '<p>Hello&nbsp;<span style=\"color: rgb(121, 121, 121);\">{user_name},</span></p><p><span style=\"color: rgb(121, 121, 121);\">Thanks for order we will confirm your as soon as possible.</span></p>', '1535020804', '1535020804'),
        (5, 'On_order_complete', 'Your Order is completed', '<p>Hello&nbsp;<span style=\"color: rgb(121, 121, 121);\">{user_name},</span></p><p><span style=\"color: rgb(121, 121, 121);\">your order is successfully delivered to you&nbsp;&nbsp;</span><span style=\"color: rgb(121, 121, 121);\">{order_id}</span></p>', '1535022931', '1535022931')");

        // Table structure for table   houdinv_extra_expence   

                $getDB->query("CREATE TABLE   houdinv_extra_expence   (
              houdinv_extra_expence_id   int(11) NOT NULL,
              houdinv_extra_expence_payee   int(11) NOT NULL,
              houdinv_extra_expence_payee_type   enum('customer','staff','supplier') NOT NULL,
              houdinv_extra_expence_payment_method   enum('cash','card','cheque') NOT NULL,
              houdinv_extra_expence_subtotal   float NOT NULL,
              houdinv_extra_expence_tax   float NOT NULL,
              houdinv_extra_expence_total_amount   float NOT NULL,
              houdinv_extra_expence_payment_status   int(1) NOT NULL,
              houdinv_extra_expence_transaction_type   enum('credit','debit') NOT NULL,
              houdinv_extra_expence_refrence_number   varchar(50) NOT NULL,
              houdinv_extra_expence_payment_date   date NOT NULL,
              houdinv_extra_expence_created_at   varchar(30) DEFAULT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

          // Dumping data for table   houdinv_extra_expence  

                $getDB->query("INSERT INTO   houdinv_extra_expence   (  houdinv_extra_expence_id  ,   houdinv_extra_expence_payee  ,   houdinv_extra_expence_payee_type  ,   houdinv_extra_expence_payment_method  ,   houdinv_extra_expence_subtotal  ,   houdinv_extra_expence_tax  ,   houdinv_extra_expence_total_amount  ,   houdinv_extra_expence_payment_status  ,   houdinv_extra_expence_transaction_type  ,   houdinv_extra_expence_refrence_number  ,   houdinv_extra_expence_payment_date  ,   houdinv_extra_expence_created_at  ) VALUES
        (1, 3, 'customer', 'card', 5600, 424, 6024, 1, 'credit', '45345', '2018-09-20', '1537142400'),
        (2, 55, 'customer', 'card', 1200, 0, 1200, 1, 'credit', '123', '2018-09-20', '1537142400'),
        (3, 3, 'customer', 'cash', 2800, 0, 2800, 0, 'credit', '123123', '2018-09-20', '1537142400')");

        // Table structure for table   houdinv_extra_expence_item   

                $getDB->query("CREATE TABLE   houdinv_extra_expence_item   (
              houdinv_extra_expence_item_id   int(11) NOT NULL,
              houdinv_extra_expence_item_item_id   int(11) NOT NULL,
              houdinv_extra_expence_item_type   enum('inv','ninv') NOT NULL,
              houdinv_extra_expence_item_quantity   float NOT NULL,
              houdinv_extra_expence_item_rate   float NOT NULL,
              houdinv_extra_expence_item_tax   float NOT NULL,
              houdinv_extra_expence_item_total_amount   float NOT NULL,
              houdinv_extra_expence_id   int(11) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

           // Dumping data for table   houdinv_extra_expence_item  

                $getDB->query("INSERT INTO   houdinv_extra_expence_item   (  houdinv_extra_expence_item_id  ,   houdinv_extra_expence_item_item_id  ,   houdinv_extra_expence_item_type  ,   houdinv_extra_expence_item_quantity  ,   houdinv_extra_expence_item_rate  ,   houdinv_extra_expence_item_tax  ,   houdinv_extra_expence_item_total_amount  ,   houdinv_extra_expence_id  ) VALUES
        (1, 32, 'inv', 3, 1200, 9, 3600, 1),
        (2, 32, 'inv', 1, 1200, 0, 1200, 2),
        (5, 32, 'inv', 2, 1200, 0, 2400, 3),
        (6, 33, 'inv', 1, 400, 0, 400, 3)");

        // Table structure for table   houdinv_forgot_password   

                $getDB->query("CREATE TABLE   houdinv_forgot_password   (
              houdinv_forgot_password_email   varchar(150) NOT NULL,
              houdinv_forgot_password_token   varchar(50) NOT NULL,
              houdinv_forgot_password_created_at   timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_google_analytics   

                $getDB->query("CREATE TABLE   houdinv_google_analytics   (
              id   int(11) NOT NULL,
              analytics_number   varchar(250) DEFAULT NULL,
              date_time   varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");


        // Dumping data for table   houdinv_google_analytics  

                $getDB->query("INSERT INTO   houdinv_google_analytics   (  id  ,   analytics_number  ,   date_time  ) VALUES
        (1, '12456fgdf', '1530706940')");

        // Table structure for table   houdinv_inventories   

                $getDB->query("CREATE TABLE   houdinv_inventories   (
              houdinv_inventory_id   int(10) UNSIGNED NOT NULL,
              houdinv_inventory_supplier_id   int(10) UNSIGNED NOT NULL,
              houdinv_inventory_supplier_product_id   int(10) UNSIGNED NOT NULL,
              houdinv_inventory_quantity   decimal(10,0) DEFAULT NULL,
              houdinv_inventory_quantity_left   decimal(10,0) NOT NULL,
              houdinv_inventory_created_at   timestamp NULL DEFAULT NULL,
              houdinv_inventory_updated_at   timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

          // Table structure for table   houdinv_inventorysetting   

                $getDB->query("CREATE TABLE   houdinv_inventorysetting   (
              id   int(11) NOT NULL,
              inventorysetting_number   int(200) NOT NULL,
              Universal_rl   int(20) DEFAULT NULL,
              date_time   varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table   houdinv_inventorysetting  

                $getDB->query("INSERT INTO   houdinv_inventorysetting   (  id  ,   inventorysetting_number  ,   Universal_rl  ,   date_time  ) VALUES
        (1, 20, 1, '1531465563')");

            // Table structure for table   houdinv_inventory_purchase   

                $getDB->query("CREATE TABLE   houdinv_inventory_purchase   (
                  houdinv_inventory_purchase_id   int(11) NOT NULL,
                  houdinv_inventory_purchase_credit   enum('one day','one week','one month') NOT NULL,
                  houdinv_inventory_purchase_supplier_id   varchar(200) NOT NULL,
                  houdinv_inventory_purchase_delivery   varchar(30) NOT NULL,
                  houdinv_inventory_purchase_status   enum('in process','accpeted','recieve','return') NOT NULL,
                  houdinv_inventory_purchase_recieved_by   int(11) DEFAULT '0',
                  houdinv_inventory_purchase_created_at   varchar(30) DEFAULT NULL,
                  houdinv_inventory_purchase_modified_at   varchar(30) DEFAULT NULL
              ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table   houdinv_inventory_purchase  

                $getDB->query("INSERT INTO   houdinv_inventory_purchase   (  houdinv_inventory_purchase_id  ,   houdinv_inventory_purchase_credit  ,   houdinv_inventory_purchase_supplier_id  ,   houdinv_inventory_purchase_delivery  ,   houdinv_inventory_purchase_status  ,   houdinv_inventory_purchase_recieved_by  ,   houdinv_inventory_purchase_created_at  ,   houdinv_inventory_purchase_modified_at  ) VALUES
        (1, 'one week', '4', '1535673600', 'recieve', 1, '1535500800', NULL),
        (2, 'one month', '4', '1535587200', 'recieve', 1, '1535500800', NULL),
        (3, 'one month', '4', '1539475200', 'in process', 0, '1538092800', NULL)");
              
        // Table structure for table   houdinv_inventory_tracks   

                $getDB->query("CREATE TABLE   houdinv_inventory_tracks   (
                  houdinv_inventory_track_id   int(10) UNSIGNED NOT NULL,
                  houdinv_inventory_track_product_id   int(10) UNSIGNED NOT NULL,
                  houdinv_inventory_track_supplier_id   int(10) UNSIGNED NOT NULL,
                  houdinv_inventory_track_product_qty   decimal(10,0) NOT NULL,
                  houdinv_inventory_track_product_cost   float NOT NULL,
                  houdinv_inventory_track_created_at   timestamp NULL DEFAULT NULL,
                  houdinv_inventory_track_updated_at   timestamp NULL DEFAULT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

            // Table structure for table   houdinv_invoicesetting   

                $getDB->query("CREATE TABLE   houdinv_invoicesetting   (
                  id   int(250) NOT NULL,
                  show_task_breakup   int(11) DEFAULT NULL,
                  total_saving_invoice   int(11) DEFAULT NULL,
                  invoice_number_receipt   int(11) DEFAULT NULL,
                  order_number_receipt   int(11) DEFAULT NULL,
                  product_wise   int(11) DEFAULT NULL,
                  invoice_num_start   varchar(11) DEFAULT NULL,
                  date_time   varchar(200) NOT NULL
              ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

// Dumping data for table   houdinv_invoicesetting  

                $getDB->query("INSERT INTO   houdinv_invoicesetting   (  id  ,   show_task_breakup  ,   total_saving_invoice  ,   invoice_number_receipt  ,   order_number_receipt  ,   product_wise  ,   invoice_num_start  ,   date_time  ) VALUES
(1, 1, 1, 1, NULL, 1, 'Hawks-', '1530714221')");

            // Table structure for table   houdinv_navigation_store_pages   

                $getDB->query("INSERT INTO   houdinv_navigation_store_pages   (  houdinv_navigation_store_pages_id  ,   houdinv_navigation_store_pages_name  ,   houdinv_navigation_store_pages_category_id  ,   houdinv_navigation_store_pages_category_name  ,   houdinv_navigation_store_pages_custom_link_id  ,   houdinv_navigation_store_pages_custom_link_name  ) VALUES
            (65, 'privacy', 0, NULL, 0, NULL),
            (60, '', 4, 'Watches', 0, NULL),
            (59, '', 2, 'Shoes', 0, NULL),
            (64, 'contact', 0, NULL, 0, NULL),
            (58, '', 1, 'Women', 0, NULL),
            (63, 'about', 0, NULL, 0, NULL),
            (62, 'home', 0, NULL, 0, NULL),
            (61, '', 8, 'Electronics', 0, NULL),
            (66, 'disclaimer', 0, NULL, 0, NULL),
            (67, 'terms', 0, NULL, 0, NULL),
            (68, 'shipping', 0, NULL, 0, NULL),
            (69, 'refund', 0, NULL, 0, NULL),
            (70, '', 0, NULL, 7, 'Test')");


            // Table structure for table   houdinv_noninventory_products   

                $getDB->query("CREATE TABLE   houdinv_noninventory_products   (
                  houdinv_noninventory_products_id   int(11) NOT NULL,
                  houdinv_noninventory_products_name   varchar(100) NOT NULL,
                  houdinv_noninventory_products_desc   text NOT NULL,
                  houdinv_noninventory_products_price   float NOT NULL,
                  houdinv_noninventory_products_created_at   varchar(30) DEFAULT NULL
              ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

            // Table structure for table   houdinv_noninventory_products   

                $getDB->query("INSERT INTO   houdinv_noninventory_products   (  houdinv_noninventory_products_id  ,   houdinv_noninventory_products_name  ,   houdinv_noninventory_products_desc  ,   houdinv_noninventory_products_price  ,   houdinv_noninventory_products_created_at  ) VALUES
            (1, 'product1', 'product1', 123, '1536969600'),
            (2, 'Product2', 'Product2', 123, '1536969600'),
            (3, 'Product3', 'Product3', 123, '1536969600'),
            (4, 'Product4', 'Product4', 123, '1536969600'),
            (5, 'Product5', 'Product5', 234, '1536969600'),
            (6, 'Product8', 'Product8', 1200, '1536969600')");

            // Table structure for table   houdinv_offline_payment_settings   

                $getDB->query("CREATE TABLE   houdinv_offline_payment_settings   (
                  houdinv_offline_payment_setting_id   int(11) UNSIGNED NOT NULL,
                  houdinv_offline_payment_setting_user_id   int(10) UNSIGNED NOT NULL,
                  houdinv_offline_payment_setting_holder_name   varchar(150) DEFAULT NULL,
                  houdinv_offline_payment_setting_holder_number   varchar(150) DEFAULT NULL,
                  houdinv_offline_payment_setting_country   varchar(150) DEFAULT NULL,
                  houdinv_offline_payment_setting_bank_name   varchar(200) DEFAULT NULL,
                  houdinv_offline_payment_setting_iban   varchar(50) DEFAULT NULL,
                  houdinv_offline_payment_setting_shift_code   varchar(50) DEFAULT NULL,
                  houdinv_offline_payment_setting_additional_information   text,
                  houdinv_offline_payment_setting_created_at   timestamp NULL DEFAULT NULL,
                  houdinv_offline_payment_setting_updated_at   timestamp NULL DEFAULT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

            // Table structure for table   houdinv_online_payment_settings   

                $getDB->query("CREATE TABLE   houdinv_online_payment_settings   (
                  houdinv_online_payment_setting_id   int(11) NOT NULL,
                  houdinv_online_payment_setting_gateway_name   varchar(150) NOT NULL,
                  houdinv_online_payment_setting_client_id   varchar(150) NOT NULL,
                  houdinv_online_payment_setting_secret_id   varchar(150) NOT NULL,
                  houdinv_online_payment_setting_mode   varchar(20) NOT NULL,
                  houdinv_online_payment_setting_active_status   tinyint(4) NOT NULL DEFAULT '0',
                  houdinv_online_payment_setting_created_at   timestamp NULL DEFAULT NULL,
                  houdinv_online_payment_setting_updated_at   timestamp NULL DEFAULT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1");
                
            // Table structure for table   houdinv_orders   

                $getDB->query("CREATE TABLE   houdinv_orders   (
                  houdinv_order_id   int(10) UNSIGNED NOT NULL,
                  houdinv_order_user_id   int(10) UNSIGNED NOT NULL DEFAULT '0',
                  houdinv_order_product_detail   text NOT NULL,
                  houdinv_confirm_order_product_detail   text,
                  houdinv_order_confirm_outlet   int(11) NOT NULL DEFAULT '0',
                  houdinv_orders_confirm_by   int(11) NOT NULL DEFAULT '0',
                  houdinv_orders_confirm_by_role   int(1) NOT NULL DEFAULT '0',
                  houdinv_order_type   enum('App','Website','Store','Work Order') DEFAULT NULL,
                  houdinv_order_delivery_type   varchar(100) NOT NULL,
                  houdinv_order_payment_method   varchar(100) NOT NULL,
                  houdinv_payment_status   int(11) NOT NULL DEFAULT '0',
                  houdinv_orders_refund_status   int(1) NOT NULL DEFAULT '0',
                  houdinv_orders_discount   float NOT NULL,
                  houdinv_orders_discount_detail_id   int(11) NOT NULL,
                  houdinv_orders_total_Amount   float NOT NULL,
                  houdinv_orders_total_paid   float NOT NULL DEFAULT '0',
                  houdinv_orders_total_remaining   float NOT NULL DEFAULT '0',
                  houdinv_order_comments   tinytext,
                  houdinv_delivery_charge   int(11) DEFAULT '0',
                  houdinv_order_confirmation_status   enum('unbilled','Delivered','Not Delivered','cancel','return','return request','cancel request','order pickup','billed','assigned') NOT NULL,
                  houdinv_orders_delivery_status   int(11) NOT NULL,
                  houdinv_orders_deliverydate   date DEFAULT NULL,
                  houdinv_order_delivery_address_id   int(10) UNSIGNED NOT NULL,
                  houdinv_order_billing_address_id   int(11) NOT NULL,
                  houdinv_order_created_at   varchar(100) DEFAULT NULL,
                  houdinv_order_updated_at   varchar(100) DEFAULT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

                // Dumping data for table   houdinv_orders    

                $getDB->query("INSERT INTO   houdinv_orders   (  houdinv_order_id  ,   houdinv_order_user_id  ,   houdinv_order_product_detail  ,   houdinv_confirm_order_product_detail  ,   houdinv_order_confirm_outlet  ,   houdinv_orders_confirm_by  ,   houdinv_orders_confirm_by_role  ,   houdinv_order_type  ,   houdinv_order_delivery_type  ,   houdinv_order_payment_method  ,   houdinv_payment_status  ,   houdinv_orders_refund_status  ,   houdinv_orders_discount  ,   houdinv_orders_discount_detail_id  ,   houdinv_orders_total_Amount  ,   houdinv_orders_total_paid  ,   houdinv_orders_total_remaining  ,   houdinv_order_comments  ,   houdinv_delivery_charge  ,   houdinv_order_confirmation_status  ,   houdinv_orders_delivery_status  ,   houdinv_orders_deliverydate  ,   houdinv_order_delivery_address_id  ,   houdinv_order_billing_address_id  ,   houdinv_order_created_at  ,   houdinv_order_updated_at  ) VALUES
            (11, 42, '[{\"product_id\":\"20\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', '[{\"product_id\":\"20\",\"variant_id\":\"0\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":\"120\"}]', 2, 0, 0, 'Website', 'deliver', 'Bank_transfer', 1, 0, 0, 0, 576, 0, 0, NULL, 456, 'Delivered', 0, '2018-10-31', 27, 4, '1534982400', '1534982400'),
            (15, 42, '[{\"product_id\":\"19\",\"product_Count\":\"1\",\"product_actual_price\":\"300\",\"total_product_paid_Amount\":300,\"variant_id\":0}]', '[{\"product_id\":\"19\",\"variant_id\":\"0\",\"product_Count\":\"1\",\"product_actual_price\":\"300\",\"total_product_paid_Amount\":\"300\"}]', 2, 0, 0, 'Website', 'deliver', 'Bank_transfer', 1, 0, 0, 0, 756, 0, 0, NULL, 456, 'cancel request', 0, '2018-09-05', 27, 4, '1534982400', '1534982400'),
            (20, 2, '[{\"product_id\":\"19\",\"product_Count\":\"1\",\"product_actual_price\":\"300\",\"total_product_paid_Amount\":300,\"variant_id\":0},{\"product_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":12,\"variant_id\":\"6\"}]', '[{\"product_id\":\"19\",\"variant_id\":\"0\",\"product_Count\":\"1\",\"product_actual_price\":\"300\",\"total_product_paid_Amount\":\"300\"},{\"product_id\":\"0\",\"variant_id\":\"6\",\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":\"12\"}]', 4, 0, 0, 'Website', 'deliver', 'paypal', 1, 0, 0, 0, 768, 0, 0, NULL, 456, 'cancel request', 0, '2018-08-30', 27, 1, '1534982400', '1534982400'),
            (55, 42, '[{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'paypal', 1, 0, 0, 0, 576, 0, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1534982400', '1534982400'),
            (60, 42, '[{\"product_id\":\"14\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"16\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', '[{\"product_id\":\"14\",\"variant_id\":\"0\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":\"120\"},{\"product_id\":\"16\",\"variant_id\":\"0\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":\"120\"}]', 4, 0, 0, 'Website', 'deliver', 'paypal', 1, 0, 0, 0, 696, 0, 0, NULL, 456, 'cancel request', 0, '2018-08-29', 27, 4, '1534982400', '1534982400'),
            (61, 42, '[{\"product_id\":\"5\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"2\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 1, 0, 0, 0, 696, 0, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1534982400', '1534982400'),
            (63, 42, '[{\"product_id\":\"19\",\"product_Count\":\"1\",\"product_actual_price\":\"300\",\"total_product_paid_Amount\":300,\"variant_id\":0},{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 0, 0, 0, 0, 876, 0, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1534982400', '1534982400'),
            (64, 42, '[{\"product_id\":\"16\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 0, 0, 0, 0, 696, 0, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1534982400', '1534982400'),
            (65, 42, '[{\"product_id\":\"15\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"14\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 0, 0, 0, 0, 696, 0, 0, NULL, 456, 'cancel request', 0, NULL, 27, 4, '1534982400', '1534982400'),
            (66, 42, '[{\"product_id\":\"16\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 0, 0, 0, 0, 696, 0, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1534982400', '1534982400'),
            (67, 42, '[{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"19\",\"product_Count\":\"1\",\"product_actual_price\":\"300\",\"total_product_paid_Amount\":300,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 0, 0, 0, 0, 876, 0, 0, NULL, 456, 'unbilled', 0, NULL, 27, 5, '1534982400', '1534982400'),
            (68, 42, '[{\"product_id\":\"14\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 0, 0, 0, 0, 696, 0, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1534982400', '1534982400'),
            (69, 42, '[{\"product_id\":\"16\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"14\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 0, 0, 0, 0, 816, 0, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1534982400', '1534982400'),
            (70, 42, '[{\"product_id\":\"16\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 0, 0, 0, 0, 696, 0, 0, NULL, 456, 'cancel request', 0, NULL, 27, 5, '1534982400', '1534982400'),
            (71, 42, '[{\"product_id\":\"11\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"15\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"14\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"10\",\"product_Count\":\"1\",\"product_actual_price\":\"112\",\"total_product_paid_Amount\":112,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"2\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"1\",\"product_Count\":\"1\",\"product_actual_price\":\"169\",\"total_product_paid_Amount\":169,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 1, 0, 0, 0, 1337, 1337, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1534982400', '1535500800'),
            (75, 42, '[{\"product_id\":\"19\",\"product_Count\":\"1\",\"product_actual_price\":\"300\",\"total_product_paid_Amount\":300,\"variant_id\":0},{\"product_id\":\"20\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 0, 0, 0, 0, 876, 0, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1535068800', '1535068800'),
            (76, 42, '[{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"120\",\"total_product_paid_Amount\":120,\"variant_id\":0},{\"product_id\":\"19\",\"product_Count\":\"1\",\"product_actual_price\":\"300\",\"total_product_paid_Amount\":300,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 0, 0, 0, 0, 876, 0, 0, NULL, 456, 'unbilled', 0, NULL, 27, 5, '1535068800', '1535068800'),
            (96, 2, '[{\"product_id\":\"1\",\"variant_id\":\"0\",\"product_Count\":\"02\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":\"4\",\"discount\":\"0\"},{\"product_id\":\"0\",\"variant_id\":\"11\",\"product_Count\":\"2\",\"product_actual_price\":\"5500\",\"total_product_paid_Amount\":\"11000\",\"discount\":\"0\"}]', '[{\"product_id\":\"1\",\"variant_id\":\"0\",\"product_Count\":\"02\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":\"4\",\"discount\":\"0\"},{\"product_id\":\"0\",\"variant_id\":\"11\",\"product_Count\":\"2\",\"product_actual_price\":\"5500\",\"total_product_paid_Amount\":\"11000\",\"discount\":\"0\"}]', 0, 1, 0, 'Store', 'deliver', 'cash', 1, 0, 0, 0, 11004, 12, 0, '', 0, 'return request', 0, '2018-08-28', 27, 43, '1535452238', '1535673600'),
            (98, 2, '[{\"product_id\":\"0\",\"variant_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"5500\",\"total_product_paid_Amount\":\"5500\",\"discount\":\"0\"},{\"product_id\":\"0\",\"variant_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"4500\",\"total_product_paid_Amount\":\"4500\",\"discount\":\"0\"}]', '[{\"product_id\":\"0\",\"variant_id\":\"6\",\"product_Count\":\"1\",\"product_actual_price\":\"5500\",\"total_product_paid_Amount\":\"5500\",\"discount\":\"0\"},{\"product_id\":\"0\",\"variant_id\":\"7\",\"product_Count\":\"1\",\"product_actual_price\":\"4500\",\"total_product_paid_Amount\":\"4500\",\"discount\":\"0\"}]', 0, 1, 0, 'Store', 'deliver', 'cash', 1, 1, 0, 0, 10000, 9988, 12, 'sdfsdfs', 0, 'cancel', 0, '2018-08-28', 27, 32, '1535453217', NULL),
            (101, 44, '[{\"product_id\":0,\"variant_id\":\"7\",\"product_Count\":\"1\",\"product_actual_price\":\"90\",\"total_product_paid_Amount\":80},{\"product_id\":\"29\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":null,\"total_product_paid_Amount\":0},{\"product_id\":\"31\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":null,\"total_product_paid_Amount\":0}]', NULL, 0, 0, 0, 'App', 'pick_up', 'cod', 0, 0, 0, 0, 80, 0, 0, 'sdfsdf', 0, 'unbilled', 35, NULL, 27, 35, '1535611549', NULL),
            (102, 44, '[{\"product_id\":0,\"variant_id\":\"7\",\"product_Count\":\"1\",\"product_actual_price\":\"90\",\"total_product_paid_Amount\":80},{\"product_id\":\"29\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":null,\"total_product_paid_Amount\":0},{\"product_id\":\"31\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":null,\"total_product_paid_Amount\":0}]', NULL, 0, 0, 0, 'App', 'pick_up', 'cod', 0, 0, 0, 0, 80, 0, 0, 'sdfsdf', 0, 'unbilled', 35, NULL, 27, 35, '1535611765', NULL),
            (103, 44, '[{\"product_id\":0,\"variant_id\":\"7\",\"product_Count\":\"1\",\"product_actual_price\":\"90\",\"total_product_paid_Amount\":80},{\"product_id\":\"29\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":null,\"total_product_paid_Amount\":0},{\"product_id\":\"31\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":null,\"total_product_paid_Amount\":0}]', NULL, 0, 0, 0, 'App', 'pick_up', 'cod', 0, 0, 0, 0, 80, 0, 0, 'sdfsdf', 0, 'unbilled', 35, NULL, 27, 35, '1535611811', NULL),
            (104, 44, '[{\"product_id\":0,\"variant_id\":\"7\",\"product_Count\":\"1\",\"product_actual_price\":\"90\",\"total_product_paid_Amount\":80},{\"product_id\":\"29\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":null,\"total_product_paid_Amount\":0},{\"product_id\":\"31\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":null,\"total_product_paid_Amount\":0}]', NULL, 0, 0, 0, 'App', 'pick_up', 'cod', 0, 0, 0, 0, 80, 0, 0, 'sdfsdf', 0, 'unbilled', 35, NULL, 27, 35, '1535611821', NULL),
            (105, 44, '[{\"product_id\":\"16\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"19\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"5\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":10}]', NULL, 0, 0, 0, 'App', 'pick_up', 'cod', 0, 0, 0, 0, 92, 0, 0, 'sdfsdf', 78, 'unbilled', 35, NULL, 27, 35, '1535621968', NULL),
            (107, 48, '[{\"product_id\":\"19\",\"variant_id\":0,\"product_Count\":\"5\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":10},{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"5\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":10}]', NULL, 0, 0, 0, 'App', 'deliver', 'cod', 0, 0, 0, 0, 98, 0, 0, '', 78, 'unbilled', 42, NULL, 27, 42, '1535622561', NULL),
            (108, 44, '[{\"product_id\":\"20\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"19\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 0, 0, 0, 0, 462, 0, 0, NULL, 456, 'unbilled', 0, NULL, 27, 7, '1535587200', '1535587200'),
            (109, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"13\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":26}]', NULL, 0, 0, 0, 'App', 'deliver', 'cod', 0, 0, 0, 0, 104, 0, 0, '', 78, 'unbilled', 42, NULL, 27, 42, '1535622857', NULL),
            (110, 44, '[{\"product_id\":\"17\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"19\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"7\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":14}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 96, 0, 0, 'cgvgv', 78, 'unbilled', 36, NULL, 27, 36, '1535623454', NULL),
            (116, 48, '[{\"product_id\":\"16\",\"variant_id\":0,\"product_Count\":\"14\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":28},{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"2\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":4}]', NULL, 0, 0, 0, 'App', 'deliver', 'cod', 0, 0, 0, 0, 110, 0, 0, '', 78, 'unbilled', 42, NULL, 27, 42, '1535630672', NULL),
            (118, 42, '[{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"19\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"20\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"16\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"15\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"13\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"14\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 470, 0, 470, NULL, 456, 'Delivered', 0, NULL, 27, 4, '1535587200', '1535587200'),
            (119, 42, '[{\"product_id\":\"14\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"15\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"16\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"19\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"20\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 0, 0, 0, 0, 468, 0, 468, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1535587200', '1535587200'),
            (120, 42, '[{\"product_id\":\"15\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"16\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"19\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"20\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 1, 0, 0, 0, 466, 466, 0, NULL, 456, 'cancel', 0, NULL, 27, 4, '1535587200', '1535587200'),
            (121, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"13\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":26}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 104, 0, 104, '', 78, 'unbilled', 35, NULL, 27, 35, '1535592327', NULL),
            (124, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"12\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":24}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 102, 0, 102, '', 78, 'unbilled', 36, NULL, 27, 36, '1535593754', NULL),
            (126, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"8\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":16}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 94, 0, 94, 'hvyvyv', 78, 'unbilled', 36, NULL, 27, 36, '1535691154', NULL),
            (132, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"15\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":30}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 108, 0, 108, 'vtvyv', 78, 'unbilled', 36, NULL, 27, 36, '1535691489', NULL),
            (133, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"14\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":28}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 106, 0, 106, '', 78, 'unbilled', 36, NULL, 27, 36, '1535691631', NULL),
            (134, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 27, 36, '1535691699', NULL),
            (135, 44, '[{\"product_id\":\"19\",\"variant_id\":0,\"product_Count\":\"14\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":28}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 106, 0, 106, 'fff', 78, 'unbilled', 36, NULL, 27, 36, '1535691858', NULL),
            (136, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"9\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":18}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 96, 0, 96, '', 78, 'unbilled', 36, NULL, 27, 36, '1535692095', NULL),
            (137, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"8\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":16}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 94, 0, 94, 'ccc', 78, 'unbilled', 36, NULL, 27, 36, '1535695878', NULL),
            (138, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'ccc', 78, 'unbilled', 36, NULL, 27, 36, '1535695890', NULL),
            (139, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'ccc', 78, 'unbilled', 36, NULL, 27, 36, '1535695910', NULL),
            (140, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'ccc', 78, 'unbilled', 36, NULL, 27, 36, '1535695918', NULL),
            (141, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'ccc', 78, 'unbilled', 36, NULL, 27, 36, '1535696199', NULL),
            (142, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'ccc', 78, 'unbilled', 36, NULL, 27, 36, '1535696818', NULL),
            (143, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'ccc', 78, 'unbilled', 36, NULL, 27, 36, '1535696898', NULL),
            (144, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"9\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":18}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 96, 0, 96, 'cgc', 78, 'unbilled', 36, NULL, 27, 36, '1535698314', NULL),
            (145, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"10\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":20}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 98, 0, 98, '', 78, 'unbilled', 36, NULL, 27, 36, '1535698853', NULL),
            (146, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"10\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":20}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 98, 0, 98, 'njkk', 78, 'unbilled', 36, NULL, 27, 36, '1535708521', NULL),
            (147, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'njkk', 78, 'unbilled', 36, NULL, 27, 36, '1535708527', NULL),
            (148, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'njkk', 78, 'unbilled', 36, NULL, 27, 36, '1535708536', NULL),
            (149, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"19\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":38}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 116, 0, 116, 'ctctc', 78, 'unbilled', 36, NULL, 27, 36, '1535709104', NULL),
            (150, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"28\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":56}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 56, 0, 56, 'huii', 0, 'unbilled', 36, NULL, 27, 36, '1536056768', NULL),
            (151, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'huii', 78, 'unbilled', 36, NULL, 27, 36, '1536056923', NULL),
            (152, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"19\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":38}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 116, 0, 116, 'g8g8g', 78, 'unbilled', 35, NULL, 27, 35, '1536057083', NULL),
            (153, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'g8g8g', 78, 'unbilled', 35, NULL, 27, 35, '1536057084', NULL),
            (154, 44, '[{\"product_id\":\"19\",\"variant_id\":0,\"product_Count\":\"22\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":44}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 44, 0, 44, 'jkrk', 0, 'unbilled', 36, NULL, 27, 36, '1536057351', NULL),
            (155, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"14\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":28}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 106, 0, 106, 'c5f5f', 78, 'unbilled', 36, NULL, 27, 36, '1536057770', NULL),
            (156, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'c5f5f', 78, 'unbilled', 36, NULL, 27, 36, '1536058074', NULL),
            (157, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"13\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":26}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 104, 0, 104, 'jkkr', 78, 'unbilled', 36, NULL, 27, 36, '1536058412', NULL),
            (158, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"10\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":20}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 98, 0, 98, 'f5f5c5', 78, 'unbilled', 36, NULL, 27, 36, '1536060081', NULL),
            (159, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"10\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":20}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 98, 0, 98, 'ftcy', 78, 'unbilled', 36, NULL, 27, 36, '1536061720', NULL),
            (160, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"15\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":30}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 108, 0, 108, 'hjj', 78, 'unbilled', 36, NULL, 27, 36, '1536062480', NULL),
            (161, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'hjj', 78, 'unbilled', 36, NULL, 27, 36, '1536062611', NULL),
            (162, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"19\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":38}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 116, 0, 116, 'hiii', 78, 'unbilled', 36, NULL, 27, 36, '1536062806', NULL),
            (163, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"13\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":26}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 104, 0, 104, '', 78, 'unbilled', 36, NULL, 27, 36, '1536063608', NULL),
            (164, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"15\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":30}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 108, 0, 108, 'jikk', 78, 'unbilled', 36, NULL, 27, 36, '1536023451', NULL),
            (165, 2, '[{\"product_id\":\"16\",\"product_Count\":\"3\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":6,\"variant_id\":0},{\"product_id\":\"17\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"19\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"20\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 468, 0, 468, NULL, 456, 'cancel request', 0, NULL, 27, 3, '1536105600', '1536105600'),
            (166, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"9\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":18},{\"product_id\":\"19\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"1\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 1, 0, 0, 0, 100, 96, 0, NULL, 78, 'unbilled', 1, NULL, 27, 1, '1536138165', NULL),
            (167, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"9\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":18}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 96, 0, 96, '', 78, 'unbilled', 36, NULL, 27, 36, '1536139404', NULL),
            (168, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"9\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":18}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 96, 0, 96, 'vvt', 78, 'unbilled', 36, NULL, 27, 36, '1536139598', NULL),
            (169, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"10\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":20}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 98, 0, 98, 'rff', 78, 'unbilled', 36, NULL, 27, 36, '1536140390', NULL),
            (170, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"10\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":20}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 98, 0, 98, 'jjjk', 78, 'unbilled', 36, NULL, 27, 36, '1536140568', NULL),
            (171, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'jjjk', 78, 'unbilled', 36, NULL, 27, 36, '1536141180', NULL),
            (172, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"8\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":16}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 94, 0, 94, '', 78, 'unbilled', 42, NULL, 27, 42, '1536145039', NULL),
            (173, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"8\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":16}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 94, 0, 94, '', 78, 'unbilled', 36, NULL, 27, 36, '1536147947', NULL),
            (174, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 27, 36, '1536148134', NULL),
            (175, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"7\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":14}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 92, 0, 92, '', 78, 'unbilled', 42, NULL, 27, 42, '1536148404', NULL),
            (176, 48, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 42, NULL, 27, 42, '1536148621', NULL),
            (177, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"7\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":14}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 1, 0, 0, 0, 92, 102, 0, '', 78, 'unbilled', 42, NULL, 27, 42, '1536148742', NULL),
            (178, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"8\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":16}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 94, 0, 94, '', 78, 'unbilled', 42, NULL, 27, 42, '1536149301', NULL),
            (179, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"9\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":18}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 96, 0, 96, '', 78, 'unbilled', 42, NULL, 27, 42, '1536149514', NULL),
            (180, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"7\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":14}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 92, 0, 92, '', 78, 'unbilled', 42, NULL, 27, 42, '1536149628', NULL),
            (181, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"8\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":16}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 94, 0, 94, '', 78, 'unbilled', 42, NULL, 27, 42, '1536150408', NULL),
            (182, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"12\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":24}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 102, 0, 102, '', 78, 'unbilled', 42, NULL, 27, 42, '1536150532', NULL),
            (183, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"7\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":14}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 92, 0, 92, '', 78, 'unbilled', 42, NULL, 27, 42, '1536150654', NULL),
            (184, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"7\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":14}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 92, 0, 92, '', 78, 'unbilled', 42, NULL, 27, 42, '1536150798', NULL),
            (185, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"8\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":16}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 1, 0, 0, 0, 94, 123, 0, '', 78, 'unbilled', 42, NULL, 27, 42, '1536150914', NULL),
            (186, 48, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"9\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":18}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 1, 0, 0, 0, 96, 123, 0, '', 78, 'unbilled', 42, NULL, 27, 42, '1536111774', NULL),
            (187, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"12\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":24}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 102, 0, 102, 'hi vh h h ', 78, 'unbilled', 36, NULL, 27, 36, '1536210268', NULL),
            (188, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"10\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":20}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 98, 0, 98, 'hiikk', 78, 'unbilled', 36, NULL, 27, 36, '1536211334', NULL),
            (189, 44, '[{\"product_id\":\"20\",\"variant_id\":0,\"product_Count\":\"14\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":28}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 106, 0, 106, 'jii', 78, 'unbilled', 35, NULL, 27, 35, '1536212581', NULL),
            (190, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'jii', 78, 'unbilled', 35, NULL, 27, 35, '1536212868', NULL),
            (191, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"13\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":15600}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 15600, 0, 15600, 'uii', 0, 'unbilled', 35, NULL, 27, 35, '1536215380', NULL),
            (192, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'uii', 78, 'unbilled', 35, NULL, 27, 35, '1536215521', NULL),
            (193, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"13\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":15600}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 15600, 0, 15600, 'nkz', 0, 'unbilled', 36, NULL, 27, 36, '1536218247', NULL),
            (194, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 1, 0, 0, 0, 78, 51678, 0, 'nkz', 78, 'unbilled', 36, NULL, 27, 36, '1536218367', NULL),
            (195, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"9\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":10800}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 10800, 0, 10800, 'hvyvtvyvyvyv', 0, 'unbilled', 36, NULL, 27, 36, '1536219236', NULL),
            (196, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"10\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":12000}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 12000, 0, 12000, 'vyvyv', 0, 'unbilled', 36, NULL, 27, 36, '1536219440', NULL),
            (197, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"10\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":12000}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 12000, 0, 12000, '', 0, 'unbilled', 36, NULL, 27, 36, '1536219903', NULL),
            (198, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"10\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":12000}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 12000, 0, 12000, 'vhcycy', 0, 'unbilled', 36, NULL, 27, 36, '1536220421', NULL),
            (199, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"13\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":15600}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 15600, 0, 15600, 'gvtctctc', 0, 'unbilled', 36, NULL, 27, 36, '1536220945', NULL),
            (200, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"16\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":19200}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 19200, 0, 19200, 'rccrc', 0, 'unbilled', 36, NULL, 27, 36, '1536223118', NULL),
            (201, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"8\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":9600}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 9600, 0, 9600, 'bbhh', 0, 'unbilled', 36, NULL, 27, 36, '1536224719', NULL),
            (202, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"7\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":8400}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 8400, 0, 8400, '', 0, 'unbilled', 36, NULL, 27, 36, '1536225123', NULL),
            (203, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"13\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":15600}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 15600, 0, 15600, 'vtv', 0, 'unbilled', 36, NULL, 27, 36, '1536225309', NULL),
            (204, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'vtv', 78, 'unbilled', 36, NULL, 27, 36, '1536225505', NULL),
            (205, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"7\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":8400}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 8400, 0, 8400, '', 0, 'unbilled', 36, NULL, 27, 36, '1536225894', NULL),
            (206, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 27, 36, '1536225914', NULL),
            (207, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"10\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":12000}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 12000, 0, 12000, 'vtvyvy', 0, 'unbilled', 36, NULL, 27, 36, '1536226019', NULL),
            (208, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"14\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":16800}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 16800, 0, 16800, 'ghjjj', 0, 'unbilled', 36, NULL, 27, 36, '1536226374', NULL),
            (209, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 1200, 0, 1200, 'vhcu', 0, 'unbilled', 36, NULL, 27, 36, '1536228538', NULL),
            (210, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 1200, 0, 1200, 'ctcycycy', 0, 'unbilled', 36, NULL, 27, 36, '1536233637', NULL),
            (211, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 1200, 0, 1200, 'vyv', 0, 'unbilled', 36, NULL, 27, 36, '1536233857', NULL),
            (212, 42, '[{\"product_id\":\"2\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"3\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"4\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":null,\"product_Count\":\"1\",\"product_actual_price\":null,\"total_product_paid_Amount\":0,\"variant_id\":0},{\"product_id\":\"5\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'auth', 0, 0, 0, 0, 464, 0, 464, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1536192000', '1536192000'),
            (213, 42, '[{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"32\",\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1200,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'auth', 0, 0, 0, 0, 1660, 0, 1660, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1536192000', '1536192000'),
            (214, 42, '[{\"product_id\":\"2\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"3\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"4\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"5\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"1\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'auth', 0, 0, 0, 0, 470, 0, 470, NULL, 456, 'cancel request', 0, NULL, 27, 4, '1536278400', '1536278400'),
            (215, 42, '[{\"product_id\":\"3\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"1\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"5\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"4\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'auth', 0, 0, 0, 0, 468, 0, 468, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1536278400', '1536278400'),
            (216, 42, '[{\"product_id\":\"5\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"4\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'auth', 0, 0, 0, 0, 464, 0, 464, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1536278400', '1536278400'),
            (217, 42, '[{\"product_id\":\"1\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"3\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"5\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"4\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'auth', 0, 0, 0, 0, 468, 468, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1536278400', '1536278400'),
            (218, 42, '[{\"product_id\":\"5\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"4\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'auth', 0, 0, 0, 0, 464, 0, 464, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1536278400', '1536278400'),
            (219, 42, '[{\"product_id\":\"3\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"5\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"4\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'auth', 0, 0, 0, 0, 466, 0, 466, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1536278400', '1536278400'),
            (220, 42, '[{\"product_id\":\"32\",\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1200,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'auth', 0, 0, 0, 0, 1656, 1656, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1536278400', '1536278400'),
            (221, 42, '[{\"product_id\":\"32\",\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1200,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'auth', 0, 0, 0, 0, 1656, 1656, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1536278400', '1536278400'),
            (222, 44, '[{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"8\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"7\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 1, 0, 1061.28, 0, 1061.28, 'hc.', 0, 'unbilled', 36, NULL, 27, 36, '1536320246', NULL),
            (223, 44, '[{\"product_id\":\"8\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200},{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 1059.52, 0, 1059.52, '', 0, 'unbilled', 36, NULL, 27, 36, '1536321777', NULL),
            (224, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 1056, 0, 1056, 'vuv', 0, 'unbilled', 36, NULL, 27, 36, '1536322037', NULL),
            (225, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200},{\"product_id\":\"7\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 1057.76, 0, 1057.76, 'dff', 0, 'unbilled', 36, NULL, 27, 36, '1536323735', NULL),
            (226, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 1056, 0, 1056, 'ddf', 0, 'unbilled', 36, NULL, 27, 36, '1536323806', NULL),
            (227, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 1056, 0, 1056, 'nn', 0, 'unbilled', 36, NULL, 27, 36, '1536323889', NULL),
            (228, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 78, 0, 78, 'nn', 78, 'unbilled', 36, NULL, 27, 36, '1536323959', NULL),
            (229, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 78, 0, 78, 'nn', 78, 'assigned', 36, NULL, 27, 36, '1536324022', '1536278400'),
            (230, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 1, 0, 1056, 0, 1056, 'vyvy', 0, 'unbilled', 36, NULL, 27, 36, '1536324294', NULL),
            (231, 42, '[{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"4\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"3\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"5\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'auth', 0, 0, 0, 0, 466, 466, 0, NULL, 456, 'unbilled', 0, NULL, 27, 4, '1536278400', '1536278400');
            INSERT INTO   houdinv_orders   (  houdinv_order_id  ,   houdinv_order_user_id  ,   houdinv_order_product_detail  ,   houdinv_confirm_order_product_detail  ,   houdinv_order_confirm_outlet  ,   houdinv_orders_confirm_by  ,   houdinv_orders_confirm_by_role  ,   houdinv_order_type  ,   houdinv_order_delivery_type  ,   houdinv_order_payment_method  ,   houdinv_payment_status  ,   houdinv_orders_refund_status  ,   houdinv_orders_discount  ,   houdinv_orders_discount_detail_id  ,   houdinv_orders_total_Amount  ,   houdinv_orders_total_paid  ,   houdinv_orders_total_remaining  ,   houdinv_order_comments  ,   houdinv_delivery_charge  ,   houdinv_order_confirmation_status  ,   houdinv_orders_delivery_status  ,   houdinv_orders_deliverydate  ,   houdinv_order_delivery_address_id  ,   houdinv_order_billing_address_id  ,   houdinv_order_created_at  ,   houdinv_order_updated_at  ) VALUES
            (232, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 1, 0, 1056, 0, 1056, ' ', 0, 'unbilled', 46, NULL, 27, 46, '1536550334', NULL),
            (233, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200},{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 1056, 0, 1056, ' gv', 0, 'unbilled', 46, NULL, 27, 46, '1536553975', NULL),
            (234, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 1056, 0, 1056, ' jvjvjv', 0, 'unbilled', 46, NULL, 27, 46, '1536554406', NULL),
            (235, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 1056, 0, 1056, 'cycyc', 0, 'assigned', 27, NULL, 27, 27, '1536554926', '1536537600'),
            (236, 44, '[{\"product_id\":\"32\",\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1200,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'auth', 0, 0, 0, 0, 1656, 0, 1656, NULL, 456, 'unbilled', 0, NULL, 7, 7, '1536537600', '1536537600'),
            (237, 44, '[{\"product_id\":\"32\",\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1200,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'auth', 0, 0, 0, 0, 1656, 0, 1656, NULL, 456, 'unbilled', 0, NULL, 7, 7, '1536537600', '1536537600'),
            (238, 44, 'null', NULL, 0, 0, 0, 'Website', 'pick_up', 'auth', 0, 0, 0, 0, 456, 0, 456, NULL, 456, 'unbilled', 0, NULL, 7, 7, '1536537600', '1536537600'),
            (239, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 1056, 0, 1056, 'guby', 0, 'unbilled', 46, NULL, 0, 46, '1536560294', NULL),
            (240, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 78, 0, 78, 'guby', 78, 'unbilled', 46, NULL, 0, 46, '1536560314', NULL),
            (241, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 78, 0, 78, 'guby', 78, 'unbilled', 46, NULL, 0, 46, '1536560628', NULL),
            (242, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 78, 0, 78, 'guby', 78, 'unbilled', 46, NULL, 0, 46, '1536560729', NULL),
            (243, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 78, 0, 78, 'guby', 78, 'unbilled', 46, NULL, 0, 46, '1536560939', NULL),
            (244, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 78, 0, 78, 'guby', 78, 'unbilled', 46, NULL, 0, 46, '1536562382', NULL),
            (245, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 78, 0, 78, 'guby', 78, 'unbilled', 46, NULL, 0, 46, '1536562410', NULL),
            (246, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 1056, 0, 1056, 'hnj', 0, 'unbilled', 46, NULL, 0, 46, '1536564061', NULL),
            (247, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 78, 0, 78, 'hnj', 78, 'unbilled', 46, NULL, 0, 46, '1536564148', NULL),
            (248, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'pick_up', 'auth', 0, 0, 1, 0, 1056, 0, 1056, 'g g ', 0, 'unbilled', 46, NULL, 0, 46, '1536564767', NULL),
            (249, 51, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'auth', 0, 0, 1, 0, 78, 0, 78, 'g g ', 78, 'unbilled', 46, NULL, 0, 46, '1536564811', NULL),
            (250, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 1, 0, 1056, 1278, 0, 'vg', 0, 'unbilled', 27, NULL, 0, 27, '1536565529', NULL),
            (251, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 1, 0, 1056, 1278, 0, 'v gr', 0, 'unbilled', 46, NULL, 0, 46, '1536566139', NULL),
            (252, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 1, 0, 1056, 1278, 0, 'vy ', 0, 'unbilled', 46, NULL, 0, 46, '1536566762', NULL),
            (253, 51, '[{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"8\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 1059.52, 0, 1059.52, 'jvjvi', 0, 'unbilled', 27, NULL, 0, 27, '1536573234', NULL),
            (254, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 78, 0, 78, 'jvjvi', 78, 'unbilled', 27, NULL, 0, 27, '1536573250', NULL),
            (255, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 78, 0, 78, 'h h ', 78, 'unbilled', 27, NULL, 0, 27, '1536573273', NULL),
            (256, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 1, 0, 1056, 1278, 0, 'hhj', 0, 'unbilled', 46, NULL, 0, 46, '1536573357', NULL),
            (257, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 1056, 0, 1056, 'bh', 0, 'unbilled', 46, NULL, 0, 46, '1536573466', NULL),
            (258, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 1, 0, 78, 0, 78, 'bhj', 78, 'unbilled', 46, NULL, 0, 46, '1536573560', NULL),
            (259, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 1056, 0, 1056, 'jjj', 0, 'unbilled', 46, NULL, 0, 46, '1536573596', NULL),
            (260, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 1056, 0, 1056, 'bjjj', 0, 'unbilled', 46, NULL, 0, 46, '1536574510', NULL),
            (261, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 78, 0, 78, 'bjjj', 78, 'unbilled', 46, NULL, 0, 46, '1536574687', NULL),
            (262, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 1056, 0, 1056, 'chcy', 0, 'unbilled', 46, NULL, 0, 46, '1536575031', NULL),
            (263, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 1056, 0, 1056, 'g fcfc', 0, 'unbilled', 46, NULL, 0, 46, '1536580124', NULL),
            (264, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 1200, 0, 1200, '', 0, 'unbilled', 45, NULL, 0, 45, '1536581726', NULL),
            (265, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 1200, 0, 1200, '', 0, 'unbilled', 45, NULL, 0, 45, '1536582206', NULL),
            (266, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 1200, 0, 1200, '', 0, 'unbilled', 45, NULL, 0, 45, '1536582439', NULL),
            (267, 48, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 45, NULL, 0, 45, '1536582474', NULL),
            (268, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 1200, 0, 1200, '', 0, 'unbilled', 45, NULL, 0, 45, '1536582555', NULL),
            (269, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 1200, 0, 1200, '', 0, 'unbilled', 45, NULL, 0, 45, '1536583116', NULL),
            (270, 51, '[{\"product_id\":\"8\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 1, 0, 1059.52, 1282, 0, 'vhv', 0, 'unbilled', 46, NULL, 0, 46, '1536583235', NULL),
            (271, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 1200, 0, 1200, '', 0, 'unbilled', 45, NULL, 0, 45, '1536584220', NULL),
            (272, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 1200, 0, 1200, '', 0, 'unbilled', 45, NULL, 0, 45, '1536541217', NULL),
            (273, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 0, 0, 1200, 1278, 0, '', 0, 'unbilled', 45, NULL, 0, 45, '1536541616', NULL),
            (274, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 1, 0, 1056, 0, 1056, '.', 0, 'unbilled', 46, NULL, 0, 46, '1536541648', NULL),
            (275, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 1, 0, 1056, 0, 1056, '', 0, 'unbilled', 46, NULL, 0, 46, '1536541809', NULL),
            (276, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 1, 0, 78, 0, 78, '', 78, 'unbilled', 46, NULL, 0, 46, '1536541823', NULL),
            (277, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 1, 0, 1056, 0, 1056, 'bh', 0, 'unbilled', 46, NULL, 0, 46, '1536541905', NULL),
            (278, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 1, 0, 1056, 0, 1056, 'vt ', 0, 'unbilled', 46, NULL, 0, 46, '1536541996', NULL),
            (279, 51, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 1, 0, 1056, 0, 1056, 'hvyv', 0, 'unbilled', 46, NULL, 0, 46, '1536542416', NULL),
            (280, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 0, 0, 1200, 1278, 0, '', 0, 'unbilled', 45, NULL, 0, 45, '1536542436', NULL),
            (281, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 0, 0, 1200, 1278, 0, '', 0, 'unbilled', 45, NULL, 0, 45, '1536542982', NULL),
            (282, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 0, 0, 1200, 1278, 0, '', 0, 'unbilled', 45, NULL, 0, 45, '1536543142', NULL),
            (283, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 1200, 0, 1200, '', 0, 'unbilled', 45, NULL, 0, 45, '1536545332', NULL),
            (284, 48, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 0, 0, 1200, 1278, 0, '', 0, 'unbilled', 45, NULL, 0, 45, '1536553016', NULL),
            (285, 48, '[{\"product_id\":\"8\",\"variant_id\":0,\"product_Count\":\"15\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":30},{\"product_id\":\"1\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 110, 0, 110, '', 78, 'unbilled', 45, NULL, 0, 45, '1536648153', NULL),
            (286, 48, '[{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"9\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":18}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 96, 0, 96, '', 78, 'unbilled', 45, NULL, 0, 45, '1536648356', NULL),
            (287, 48, '[{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"15\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":30}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 108, 0, 108, '', 78, 'unbilled', 45, NULL, 0, 45, '1536648440', NULL),
            (288, 48, '[{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"13\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":26}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 104, 0, 104, '', 78, 'unbilled', 45, NULL, 0, 45, '1536648626', NULL),
            (289, 48, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 45, NULL, 0, 45, '1536648659', NULL),
            (290, 48, '[{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"17\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":34}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 112, 0, 112, '', 78, 'unbilled', 45, NULL, 0, 45, '1536648742', NULL),
            (291, 48, '[{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"8\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":16}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 94, 0, 94, '', 78, 'unbilled', 45, NULL, 0, 45, '1536649631', NULL),
            (292, 51, '[{\"product_id\":\"33\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":400}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 1, 0, 352, 0, 352, '', 0, 'unbilled', 46, NULL, 0, 46, '1536669589', NULL),
            (293, 51, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 1, 0, 78, 0, 78, '', 78, 'unbilled', 46, NULL, 0, 46, '1536669654', NULL),
            (294, 42, '[{\"product_id\":\"33\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":400}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 400, 0, 400, 'vgfddfg', 0, 'unbilled', 42, NULL, 0, 42, '1536628903', NULL),
            (295, 42, '[{\"product_id\":\"33\",\"product_Count\":\"3\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":1200,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 0, 0, 0, 0, 1656, 0, 1656, NULL, 456, 'unbilled', 0, NULL, 4, 4, '1536710400', '1536710400'),
            (296, 42, '[{\"product_id\":\"4\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"33\",\"product_Count\":\"1\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":400,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 1, 0, 0, 0, 860, 860, 0, NULL, 456, 'unbilled', 0, NULL, 5, 5, '1536710400', '1536710400'),
            (297, 42, '[{\"product_id\":\"35\",\"product_Count\":\"4\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":2600,\"variant_id\":0},{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0},{\"product_id\":\"33\",\"product_Count\":\"2\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":800,\"variant_id\":0},{\"product_id\":\"36\",\"product_Count\":\"1\",\"product_actual_price\":\"601\",\"total_product_paid_Amount\":601,\"variant_id\":0},{\"product_id\":\"32\",\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1200,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 1, 0, 0, 0, 6212, 6212, 0, NULL, 456, 'unbilled', 0, NULL, 4, 4, '1536796800', '1536796800'),
            (298, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536839618', NULL),
            (299, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536839797', NULL),
            (300, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536839944', NULL),
            (301, 48, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 45, NULL, 0, 45, '1536839981', NULL),
            (302, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536841565', NULL),
            (303, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536841990', NULL),
            (304, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536842508', NULL),
            (305, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536842640', NULL),
            (306, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536842682', NULL),
            (307, 48, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 45, NULL, 0, 45, '1536842758', NULL),
            (308, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536843022', NULL),
            (309, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536843106', NULL),
            (310, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"2\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1202}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 1202, 0, 1202, '', 0, 'unbilled', 35, NULL, 0, 35, '1536843407', NULL),
            (311, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 35, NULL, 0, 35, '1536843429', NULL),
            (312, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 601, 0, 601, 'bjj', 0, 'unbilled', 36, NULL, 0, 36, '1536843481', NULL),
            (313, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"2\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1202}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 1202, 0, 1202, '', 0, 'unbilled', 35, NULL, 0, 35, '1536800922', NULL),
            (314, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 35, NULL, 0, 35, '1536800938', NULL),
            (315, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'vbj', 78, 'unbilled', 35, NULL, 0, 35, '1536800965', NULL),
            (316, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 601, 0, 601, 'njj', 0, 'unbilled', 36, NULL, 0, 36, '1536800999', NULL),
            (317, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 601, 0, 601, 'vy', 0, 'unbilled', 36, NULL, 0, 36, '1536801080', NULL),
            (318, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, 'Hhu', 0, 'unbilled', 45, NULL, 0, 45, '1536801292', NULL),
            (319, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 36, NULL, 0, 36, '1536801384', NULL),
            (320, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536801510', NULL),
            (321, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536801597', NULL),
            (322, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536801653', NULL),
            (323, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536801803', NULL),
            (324, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536801937', NULL),
            (325, 62, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 601, 0, 601, 'fcr', 0, 'unbilled', 54, NULL, 0, 54, '1536801976', NULL),
            (326, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536802072', NULL),
            (327, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536802359', NULL),
            (328, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536802571', NULL),
            (329, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536802651', NULL),
            (330, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536802742', NULL),
            (331, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536802922', NULL),
            (332, 48, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 45, NULL, 0, 45, '1536802988', NULL),
            (333, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536803225', NULL),
            (334, 48, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 45, NULL, 0, 45, '1536803248', NULL),
            (335, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536803483', NULL),
            (336, 62, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 601, 0, 601, 'hj', 0, 'unbilled', 54, NULL, 0, 54, '1536898765', NULL),
            (337, 42, '[{\"product_id\":\"33\",\"product_Count\":\"2\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":800,\"variant_id\":0},{\"product_id\":\"36\",\"product_Count\":\"1\",\"product_actual_price\":\"601\",\"total_product_paid_Amount\":601,\"variant_id\":0},{\"product_id\":\"35\",\"product_Count\":\"2\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":1300,\"variant_id\":0},{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 1, 0, 0, 0, 3708, 3708, 0, NULL, 456, 'unbilled', 0, NULL, 4, 4, '1536883200', '1536883200'),
            (338, 48, '[{\"product_id\":\"8\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"1\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 656, 0, 656, '', 0, 'unbilled', 45, NULL, 0, 45, '1536922599', NULL),
            (339, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1536922686', NULL),
            (340, 48, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 45, NULL, 0, 45, '1536922699', NULL),
            (341, 48, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 45, NULL, 0, 45, '1536922706', NULL),
            (342, 42, '[{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"33\",\"product_Count\":\"1\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":400,\"variant_id\":0},{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0},{\"product_id\":\"35\",\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650,\"variant_id\":0}]', '[{\"product_id\":\"8\",\"variant_id\":\"0\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":\"2\"},{\"product_id\":\"33\",\"variant_id\":\"0\",\"product_Count\":\"1\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":\"400\"},{\"product_id\":\"34\",\"variant_id\":\"0\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":\"551\"},{\"product_id\":\"35\",\"variant_id\":\"0\",\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":\"650\"}]', 1, 0, 0, 'Website', 'deliver', 'payumoney', 1, 0, 0, 0, 2059, 2059, 0, NULL, 456, 'cancel request', 0, '2018-09-20', 4, 4, '1536883200', '1536883200'),
            (343, 44, '[{\"product_id\":\"1\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601},{\"product_id\":\"4\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 605, 0, 605, 'cfcr', 0, 'unbilled', 36, NULL, 0, 36, '1536989683', NULL),
            (344, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'cfcr', 78, 'unbilled', 36, NULL, 0, 36, '1536989703', NULL),
            (345, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 601, 0, 601, 'gtv', 0, 'unbilled', 36, NULL, 0, 36, '1536989746', NULL),
            (346, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'gtv', 78, 'unbilled', 36, NULL, 0, 36, '1536989837', NULL),
            (347, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 601, 0, 601, 'fcfc', 0, 'unbilled', 36, NULL, 0, 36, '1536990278', NULL),
            (348, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 78, 0, 78, 'fcfc', 78, 'unbilled', 36, NULL, 0, 36, '1536990303', NULL),
            (349, 64, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 601, 0, 601, 'fyvy', 0, 'unbilled', 55, NULL, 0, 55, '1536991190', NULL),
            (350, 2, '[{\"product_id\":\"35\",\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650,\"variant_id\":0},{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0},{\"product_id\":\"33\",\"product_Count\":\"2\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":800,\"variant_id\":0},{\"product_id\":\"3\",\"product_Count\":\"116\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":232,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 2689, 0, 2689, NULL, 456, 'cancel request', 0, NULL, 1, 1, '1536969600', '1536969600'),
            (351, 67, '[{\"product_id\":0,\"product_Count\":\"5\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":60,\"variant_id\":\"6\"},{\"product_id\":\"9\",\"product_Count\":\"5\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":10,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"21\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":42,\"variant_id\":0},{\"product_id\":\"36\",\"product_Count\":\"2\",\"product_actual_price\":\"601\",\"total_product_paid_Amount\":1202,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 0, 0, 0, 0, 1770, 0, 1770, NULL, 456, 'cancel request', 0, NULL, 8, 8, '1536969600', '1536969600'),
            (352, 42, '[{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"35\",\"product_Count\":\"2\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":1300,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 1, 0, 0, 0, 1758, 1758, 0, NULL, 456, 'unbilled', 0, NULL, 4, 4, '1537142400', '1537142400'),
            (353, 48, '[{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":551}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 551, 0, 551, 'Nice', 0, 'unbilled', 45, NULL, 0, 45, '1537170309', NULL),
            (354, 48, '[{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650},{\"product_id\":\"1\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 652, 0, 652, '', 0, 'unbilled', 45, NULL, 0, 45, '1537179979', NULL),
            (355, 48, '[{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":551},{\"product_id\":\"4\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 553, 0, 553, '', 0, 'unbilled', 59, NULL, 0, 59, '1537182992', NULL),
            (357, 48, '[{\"product_id\":\"33\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":400},{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 1050, 0, 1050, 'Rr', 0, 'unbilled', 59, NULL, 0, 59, '1537183172', NULL),
            (358, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 45, NULL, 0, 45, '1537183308', NULL),
            (359, 48, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 45, NULL, 0, 45, '1537183330', NULL),
            (360, 48, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 45, NULL, 0, 45, '1537183342', NULL),
            (361, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 59, NULL, 0, 59, '1537187242', NULL),
            (362, 48, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 59, NULL, 0, 59, '1537187278', NULL),
            (363, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 59, NULL, 0, 59, '1537187713', NULL),
            (364, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 59, NULL, 0, 59, '1537188190', NULL),
            (365, 48, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 59, NULL, 0, 59, '1537188225', NULL),
            (366, 48, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 59, NULL, 0, 59, '1537188270', NULL),
            (367, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 59, NULL, 0, 59, '1537188344', NULL),
            (368, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 59, NULL, 0, 59, '1537188377', NULL),
            (369, 48, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 59, NULL, 0, 59, '1537188388', NULL),
            (370, 48, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 59, NULL, 0, 59, '1537188499', NULL),
            (371, 2, '[{\"product_id\":\"34\",\"product_Count\":\"111\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":61161,\"variant_id\":0},{\"product_id\":\"5\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"6\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"33\",\"product_Count\":\"1\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":400,\"variant_id\":0},{\"product_id\":\"7\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 61569, 0, 61569, NULL, 0, 'unbilled', 0, NULL, 2, 2, '1537142400', '1537142400'),
            (372, 2, '[{\"product_id\":\"3\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"4\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"5\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"6\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"2540\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":5080,\"variant_id\":0},{\"product_id\":\"7\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"34\",\"product_Count\":\"2\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":1102,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'auth', 0, 0, 0, 0, 6648, 0, 6648, NULL, 456, 'cancel request', 0, NULL, 1, 1, '1537228800', '1537228800'),
            (373, 42, '[{\"product_id\":\"33\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":400}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 0, 0, 400, 478, 0, 'Hsbsjjs\n', 0, 'unbilled', 52, NULL, 0, 52, '1537248172', NULL),
            (374, 42, '[{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"2\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1102}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 1102, 0, 1102, 'Hdbsjd\n', 0, 'unbilled', 52, NULL, 0, 52, '1537248506', NULL),
            (375, 42, '[{\"product_id\":\"1\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":0,\"variant_id\":\"7\",\"product_Count\":\"1\",\"product_actual_price\":\"90\",\"total_product_paid_Amount\":80}]', NULL, 0, 0, 0, 'App', 'pick_up', 'COD', 0, 0, 0, 0, 82, 0, 82, 'Hshshjs', 0, 'unbilled', 52, NULL, 0, 52, '1537248564', NULL),
            (376, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'pick_up', 'auth', 0, 0, 0, 0, 601, 0, 601, 'hchf', 0, 'unbilled', 48, NULL, 0, 48, '1537255324', NULL),
            (377, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'pick_up', 'auth', 0, 0, 0, 0, 601, 0, 601, 'nn', 0, 'unbilled', 36, NULL, 0, 36, '1537255985', NULL),
            (378, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, ' g', 0, 'unbilled', 36, NULL, 0, 36, '1537257198', NULL),
            (379, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 78, 0, 78, ' g', 78, 'unbilled', 36, NULL, 0, 36, '1537257220', NULL),
            (380, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 78, 0, 78, ' g', 78, 'unbilled', 36, NULL, 0, 36, '1537257239', NULL),
            (381, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, 'yvycgyvvyvyvycyh', 0, 'unbilled', 36, NULL, 0, 36, '1537260413', NULL),
            (382, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, 'g g', 0, 'unbilled', 36, NULL, 0, 36, '1537260695', NULL),
            (383, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 78, 0, 78, 'g g', 78, 'unbilled', 36, NULL, 0, 36, '1537260807', NULL),
            (384, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, 'xfd', 0, 'unbilled', 48, NULL, 0, 48, '1537260994', NULL),
            (385, 42, '[{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 0, 0, 0, 0, 1007, 0, 1007, NULL, 456, 'unbilled', 0, NULL, 5, 5, '1537228800', '1537228800'),
            (386, 0, '[{\"product_id\":\"36\",\"product_Count\":\"12\",\"product_actual_price\":\"601\",\"total_product_paid_Amount\":7212,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 7670, 0, 7670, NULL, 456, 'unbilled', 0, NULL, 9, 9, '1537228800', '1537228800'),
            (387, 0, '[{\"product_id\":\"35\",\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 1106, 0, 1106, NULL, 456, 'unbilled', 0, NULL, 10, 9, '1537228800', '1537228800'),
            (388, 0, '[{\"product_id\":\"33\",\"product_Count\":\"1\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":400,\"variant_id\":0},{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 1407, 0, 1407, NULL, 456, 'unbilled', 0, NULL, 9, 9, '1537228800', '1537228800'),
            (389, 0, '[{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"35\",\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 0, 0, 0, 0, 1659, 0, 1659, NULL, 456, 'unbilled', 0, NULL, 10, 10, '1537228800', '1537228800'),
            (390, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, 'hkoo', 0, 'unbilled', 36, NULL, 0, 36, '1537244232', NULL),
            (391, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, 'djjd', 0, 'unbilled', 36, NULL, 0, 36, '1537334453', NULL),
            (392, 42, '[{\"product_id\":\"6\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"2\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650},{\"product_id\":\"33\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":400},{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"24\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":14424}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 0, 0, 15478, 15556, 0, '', 0, 'unbilled', 70, NULL, 0, 70, '1537348858', NULL),
            (393, 42, '[{\"product_id\":\"7\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"33\",\"product_Count\":\"2\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":800,\"variant_id\":0},{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 1809, 0, 1809, NULL, 456, 'unbilled', 0, NULL, 4, 4, '1537315200', '1537315200'),
            (394, 44, '[{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650},{\"product_id\":\"1\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"4\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"6\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 656, 0, 656, '', 0, 'unbilled', 36, NULL, 0, 36, '1537359222', NULL),
            (395, 70, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 72, NULL, 0, 72, '1537359366', NULL),
            (396, 70, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 72, NULL, 0, 72, '1537359589', NULL),
            (397, 70, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 72, NULL, 0, 72, '1537359615', NULL),
            (398, 70, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 72, NULL, 0, 72, '1537359754', NULL),
            (399, 70, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 72, NULL, 0, 72, '1537359759', NULL);
            INSERT INTO   houdinv_orders   (  houdinv_order_id  ,   houdinv_order_user_id  ,   houdinv_order_product_detail  ,   houdinv_confirm_order_product_detail  ,   houdinv_order_confirm_outlet  ,   houdinv_orders_confirm_by  ,   houdinv_orders_confirm_by_role  ,   houdinv_order_type  ,   houdinv_order_delivery_type  ,   houdinv_order_payment_method  ,   houdinv_payment_status  ,   houdinv_orders_refund_status  ,   houdinv_orders_discount  ,   houdinv_orders_discount_detail_id  ,   houdinv_orders_total_Amount  ,   houdinv_orders_total_paid  ,   houdinv_orders_total_remaining  ,   houdinv_order_comments  ,   houdinv_delivery_charge  ,   houdinv_order_confirmation_status  ,   houdinv_orders_delivery_status  ,   houdinv_orders_deliverydate  ,   houdinv_order_delivery_address_id  ,   houdinv_order_billing_address_id  ,   houdinv_order_created_at  ,   houdinv_order_updated_at  ) VALUES
            (400, 42, '[{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0},{\"product_id\":\"32\",\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1200,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 2207, 0, 2207, NULL, 456, 'unbilled', 0, NULL, 4, 4, '1537401600', '1537401600'),
            (401, 42, 'null', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 456, 0, 456, NULL, 456, 'cancel request', 0, NULL, 4, 4, '1537401600', '1537401600'),
            (402, 42, 'null', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 456, 0, 456, NULL, 456, 'unbilled', 0, NULL, 4, 4, '1537401600', '1537401600'),
            (403, 44, '[{\"product_id\":\"1\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"4\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601},{\"product_id\":\"6\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":551},{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 2360, 0, 2360, '', 0, 'unbilled', 36, NULL, 0, 36, '1537422983', NULL),
            (404, 44, '[{\"product_id\":\"8\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"1\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 605, 0, 605, '', 0, 'unbilled', 36, NULL, 0, 36, '1537423438', NULL),
            (405, 44, '[{\"product_id\":\"4\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601},{\"product_id\":\"1\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 605, 0, 605, 'vhch', 0, 'unbilled', 36, NULL, 0, 36, '1537434903', NULL),
            (406, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 0, 0, 78, 683, 0, 'vhj', 78, 'unbilled', 36, NULL, 0, 36, '1537435057', NULL),
            (407, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 36, NULL, 0, 36, '1537437613', NULL),
            (408, 75, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 601, 0, 601, 'jj', 0, 'unbilled', 73, NULL, 0, 73, '1537438804', NULL),
            (409, 44, '[{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":551}]', NULL, 0, 0, 0, 'App', 'deliver', 'PayUMoney', 0, 0, 0, 0, 551, 0, 551, '', 0, 'unbilled', 36, NULL, 0, 36, '1537440992', NULL),
            (410, 44, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200},{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"3\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1653}]', NULL, 0, 0, 0, 'App', 'pick_up', 'COD', 0, 0, 0, 0, 2853, 0, 2853, 'test', 0, 'unbilled', 36, NULL, 0, 36, '1537441370', NULL),
            (411, 44, '[{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 1, 0, 0, 0, 650, 728, 0, 'gcct', 0, 'unbilled', 36, '2018-09-28', 36, 36, '1537441475', NULL),
            (412, 42, '[{\"product_id\":\"2\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 1202, 0, 1202, 'Hbjn', 0, 'cancel request', 75, NULL, 0, 75, '1537442658', NULL),
            (413, 76, '[{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":551},{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 1152, 0, 1152, 'huuuj', 0, 'unbilled', 74, NULL, 0, 74, '1537445155', NULL),
            (414, 76, '[{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650},{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 1251, 0, 1251, 'cf', 0, 'unbilled', 74, NULL, 0, 74, '1537445720', NULL),
            (415, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 74, NULL, 0, 74, '1537445751', NULL),
            (416, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 74, NULL, 0, 74, '1537445799', NULL),
            (417, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 601, 0, 601, 'ggg', 0, 'unbilled', 74, NULL, 0, 74, '1537446107', NULL),
            (418, 78, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200}]', NULL, 0, 0, 0, 'App', 'pick_up', 'PayUMoney', 0, 0, 0, 0, 1200, 0, 1200, 'rtfgth', 0, 'unbilled', 77, NULL, 0, 77, '1537506314', NULL),
            (419, 78, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'COD', 0, 0, 0, 0, 78, 0, 78, 'rtfgth', 78, 'unbilled', 77, NULL, 0, 77, '1537506367', NULL),
            (420, 78, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 77, NULL, 0, 77, '1537507070', NULL),
            (421, 42, '[{\"product_id\":\"1\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"33\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":400}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 402, 0, 402, '', 0, 'unbilled', 76, NULL, 0, 76, '1537507137', NULL),
            (422, 42, '[{\"product_id\":\"33\",\"product_Count\":\"1\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":400,\"variant_id\":0},{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0},{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"35\",\"product_Count\":\"2\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":1300,\"variant_id\":0},{\"product_id\":\"36\",\"product_Count\":\"1\",\"product_actual_price\":\"601\",\"total_product_paid_Amount\":601,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 3312, 0, 3312, NULL, 456, 'cancel request', 0, NULL, 4, 4, '1537488000', '1537488000'),
            (423, 76, '[{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 650, 0, 650, '', 0, 'unbilled', 74, NULL, 0, 74, '1537526704', NULL),
            (424, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 601, 0, 601, 'vtv', 0, 'unbilled', 74, NULL, 0, 74, '1537527466', NULL),
            (425, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 74, NULL, 0, 74, '1537528396', NULL),
            (426, 42, '[{\"product_id\":\"33\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":400},{\"product_id\":\"9\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"33\",\"variant_id\":0,\"product_Count\":\"2\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":800},{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"2\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1102},{\"product_id\":\"3\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2},{\"product_id\":\"4\",\"variant_id\":0,\"product_Count\":\"2\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":4}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 2310, 0, 2310, '', 0, 'unbilled', 79, NULL, 0, 79, '1537582247', NULL),
            (427, 76, '[{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":551},{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650},{\"product_id\":\"3\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 1203, 0, 1203, 'hjvfjhsd', 0, 'unbilled', 74, NULL, 0, 74, '1537783115', NULL),
            (428, 76, '[{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 650, 0, 650, 'xydy', 0, 'unbilled', 74, NULL, 0, 74, '1537784714', NULL),
            (429, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, 'chuvuv', 0, 'unbilled', 74, NULL, 0, 74, '1537784779', NULL),
            (430, 76, '[{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":551}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 551, 0, 551, 'nvjvj', 0, 'unbilled', 74, NULL, 0, 74, '1537784881', NULL),
            (431, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, 'vjvj', 0, 'unbilled', 74, NULL, 0, 74, '1537785344', NULL),
            (432, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, 'ftct.', 0, 'unbilled', 74, NULL, 0, 74, '1537785491', NULL),
            (433, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, 'jjk', 0, 'unbilled', 74, NULL, 0, 74, '1537785636', NULL),
            (434, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, 'hxhc.', 0, 'unbilled', 74, NULL, 0, 74, '1537786326', NULL),
            (435, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, 'jdkf', 0, 'unbilled', 74, NULL, 0, 74, '1537792291', NULL),
            (436, 76, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 78, 0, 78, 'jdkf', 78, 'unbilled', 74, NULL, 0, 74, '1537792307', NULL),
            (437, 76, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'jdkf', 78, 'unbilled', 74, NULL, 0, 74, '1537792328', NULL),
            (438, 76, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, 'gvtvv', 0, 'unbilled', 74, NULL, 0, 74, '1537792352', NULL),
            (439, 42, '[{\"product_id\":\"32\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"2999\",\"total_product_paid_Amount\":1200},{\"product_id\":\"7\",\"variant_id\":0,\"product_Count\":\"2\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":4},{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650},{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":551},{\"product_id\":\"33\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":400},{\"product_id\":\"8\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"12\",\"total_product_paid_Amount\":2}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 2807, 0, 2807, '', 0, 'unbilled', 80, NULL, 0, 80, '1537855060', NULL),
            (440, 42, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 81, NULL, 0, 81, '1537855241', NULL),
            (441, 42, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 75, NULL, 0, 75, '1537873749', NULL),
            (442, 44, '[{\"product_id\":\"33\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":400},{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650},{\"product_id\":\"34\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":551}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 1601, 0, 1601, 'buv', 0, 'unbilled', 36, NULL, 0, 36, '1537873871', NULL),
            (443, 44, '[{\"product_id\":\"33\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":400}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 400, 0, 400, 'gvhv', 0, 'unbilled', 36, NULL, 0, 36, '1537873973', NULL),
            (444, 42, '[{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0},{\"product_id\":\"35\",\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650,\"variant_id\":0},{\"product_id\":\"33\",\"product_Count\":\"1\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":400,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 0, 0, 0, 0, 2057, 0, 2057, NULL, 456, 'unbilled', 0, NULL, 5, 5, '1537833600', '1537833600'),
            (445, 42, 'null', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 0, 0, 0, 0, 456, 0, 456, NULL, 456, 'cancel request', 0, NULL, 5, 5, '1537833600', '1537833600'),
            (446, 2, '[{\"product_id\":\"8\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'payumoney', 0, 0, 0, 0, 458, 0, 458, NULL, 456, 'cancel request', 0, NULL, 1, 1, '1538092800', '1538092800'),
            (447, 2, '[{\"product_id\":\"34\",\"product_Count\":\"5\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":2755,\"variant_id\":0},{\"product_id\":\"35\",\"product_Count\":\"9\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":5850,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'cod', 0, 0, 0, 0, 9061, 0, 9061, NULL, 456, 'cancel request', 0, NULL, 2, 11, '1538092800', '1538092800'),
            (448, 2, '[{\"product_id\":\"9\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"20\",\"product_Count\":\"1\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":400,\"variant_id\":0},{\"product_id\":\"32\",\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1200,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'cod', 0, 0, 0, 0, 2058, 0, 2058, NULL, 456, 'unbilled', 0, NULL, 3, 3, '1538092800', '1538092800'),
            (449, 2, '[{\"product_id\":\"37\",\"product_Count\":\"10\",\"product_actual_price\":\"3299\",\"total_product_paid_Amount\":32990,\"variant_id\":0},{\"product_id\":\"35\",\"product_Count\":\"4\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":2600,\"variant_id\":0},{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0},{\"product_id\":\"36\",\"product_Count\":\"7\",\"product_actual_price\":\"601\",\"total_product_paid_Amount\":4207,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'auth', 0, 0, 0, 0, 40348, 0, 40348, NULL, 0, 'unbilled', 0, NULL, 1, 1, '1538179200', '1538179200'),
            (450, 2, '[{\"product_id\":\"34\",\"product_Count\":\"1\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":551,\"variant_id\":0},{\"product_id\":\"35\",\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650,\"variant_id\":0},{\"product_id\":\"37\",\"product_Count\":\"1\",\"product_actual_price\":\"3299\",\"total_product_paid_Amount\":3299,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'cod', 0, 0, 0, 0, 4956, 0, 4956, NULL, 456, 'unbilled', 0, NULL, 1, 1, '1538179200', '1538179200'),
            (451, 44, '[{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650},{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 1251, 0, 1251, '', 0, 'unbilled', 36, NULL, 0, 36, '1538202653', NULL),
            (452, 2, '[{\"product_id\":\"2\",\"product_Count\":\"961\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":1922,\"variant_id\":0},{\"product_id\":\"34\",\"product_Count\":\"9\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":4959,\"variant_id\":0},{\"product_id\":\"1\",\"product_Count\":\"9\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":18,\"variant_id\":0},{\"product_id\":\"3\",\"product_Count\":\"4\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":8,\"variant_id\":0},{\"product_id\":\"32\",\"product_Count\":\"4\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":4800,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'auth', 0, 0, 0, 0, 12163, 0, 12163, NULL, 456, 'unbilled', 0, NULL, 1, 1, '1538179200', '1538179200'),
            (453, 2, '[{\"product_id\":\"2\",\"product_Count\":\"1\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":2,\"variant_id\":0},{\"product_id\":\"32\",\"product_Count\":\"7\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":8400,\"variant_id\":0},{\"product_id\":\"20\",\"product_Count\":\"10\",\"product_actual_price\":\"400\",\"total_product_paid_Amount\":4000,\"variant_id\":0},{\"product_id\":\"35\",\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650,\"variant_id\":0},{\"product_id\":\"37\",\"product_Count\":\"1\",\"product_actual_price\":\"3299\",\"total_product_paid_Amount\":3299,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 16351, 0, 16351, NULL, 0, 'unbilled', 0, NULL, 1, 1, '1538352000', '1538352000'),
            (454, 2, '[{\"product_id\":\"37\",\"product_Count\":\"1\",\"product_actual_price\":\"3299\",\"total_product_paid_Amount\":3299,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'payumoney', 0, 0, 0, 0, 3755, 0, 3755, NULL, 456, 'unbilled', 0, NULL, 3, 3, '1538352000', '1538352000'),
            (455, 2, '[{\"product_id\":\"2\",\"product_Count\":\"3\",\"product_actual_price\":\"2\",\"total_product_paid_Amount\":6,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'deliver', 'cod', 0, 0, 0, 0, 462, 0, 462, NULL, 456, 'unbilled', 0, NULL, 3, 3, '1538352000', '1538352000'),
            (456, 44, '[{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650}]', NULL, 0, 0, 0, 'App', 'pick_up', 'COD', 0, 0, 0, 0, 650, 0, 650, '', 0, 'unbilled', 36, NULL, 0, 36, '1538379577', NULL),
            (457, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 3299, 0, 3299, '', 0, 'unbilled', 36, NULL, 0, 36, '1538379761', NULL),
            (458, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538379780', NULL),
            (459, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'auth', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538379811', NULL),
            (460, 44, '[{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650}]', NULL, 0, 0, 0, 'App', 'pick_up', 'auth', 0, 0, 0, 0, 650, 0, 650, '', 0, 'unbilled', 36, NULL, 0, 36, '1538379868', NULL),
            (461, 44, '[{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650},{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'auth', 0, 0, 0, 0, 3949, 0, 3949, '', 0, 'unbilled', 36, NULL, 0, 36, '1538380039', NULL),
            (462, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'auth', 0, 0, 0, 0, 3299, 0, 3299, '', 0, 'unbilled', 36, NULL, 0, 36, '1538380743', NULL),
            (463, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 3299, 0, 3299, '', 0, 'unbilled', 36, NULL, 0, 36, '1538383285', NULL),
            (464, 2, '[{\"product_id\":\"35\",\"product_Count\":\"11\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":7150,\"variant_id\":0},{\"product_id\":\"37\",\"product_Count\":\"1\",\"product_actual_price\":\"3299\",\"total_product_paid_Amount\":3299,\"variant_id\":0},{\"product_id\":\"34\",\"product_Count\":\"2\",\"product_actual_price\":\"551\",\"total_product_paid_Amount\":1102,\"variant_id\":0},{\"product_id\":\"32\",\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":1200,\"variant_id\":0}]', NULL, 0, 0, 0, 'Website', 'pick_up', 'auth', 0, 0, 0, 0, 12751, 0, 12751, NULL, 0, 'unbilled', 0, NULL, 11, 11, '1538352000', '1538352000'),
            (465, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 601, 0, 601, '', 0, 'unbilled', 36, NULL, 0, 36, '1538397894', NULL),
            (466, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538397914', NULL),
            (467, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601},{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 3900, 0, 3900, 'bybub', 0, 'unbilled', 36, NULL, 0, 36, '1538355838', NULL),
            (468, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, 'bybub', 78, 'unbilled', 36, NULL, 0, 36, '1538355845', NULL),
            (469, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, 'bybub', 78, 'unbilled', 36, NULL, 0, 36, '1538355862', NULL),
            (470, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, 'bybub', 78, 'unbilled', 36, NULL, 0, 36, '1538355869', NULL),
            (471, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, 'bybub', 78, 'unbilled', 36, NULL, 0, 36, '1538355879', NULL),
            (472, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601},{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 3900, 0, 3900, '', 0, 'unbilled', 36, NULL, 0, 36, '1538355916', NULL),
            (473, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 3299, 0, 3299, '', 0, 'unbilled', 36, NULL, 0, 36, '1538463143', NULL),
            (474, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538463153', NULL),
            (475, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538463166', NULL),
            (476, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538463170', NULL),
            (477, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538463177', NULL),
            (478, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538463182', NULL),
            (479, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538463187', NULL),
            (480, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 3299, 0, 3299, '', 0, 'unbilled', 36, NULL, 0, 36, '1538463220', NULL),
            (481, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'COD', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538463233', NULL),
            (482, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'COD', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538463239', NULL),
            (483, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 3299, 0, 3299, '', 0, 'unbilled', 36, NULL, 0, 36, '1538463269', NULL),
            (484, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538463398', NULL),
            (485, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538463401', NULL),
            (486, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 3299, 0, 3299, 'hj', 0, 'billed', 36, NULL, 0, 36, '1538463719', NULL),
            (487, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'hj', 78, 'unbilled', 36, NULL, 0, 36, '1538463821', NULL),
            (488, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 3299, 0, 3299, 'hk', 0, 'unbilled', 36, NULL, 0, 36, '1538463851', NULL),
            (489, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'hj', 78, 'unbilled', 36, NULL, 0, 36, '1538463988', NULL),
            (490, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'hj', 78, 'unbilled', 36, NULL, 0, 36, '1538464085', NULL),
            (491, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'hj', 78, 'unbilled', 36, NULL, 0, 36, '1538464152', NULL),
            (492, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 3299, 0, 3299, '', 0, 'unbilled', 36, NULL, 0, 36, '1538464212', NULL),
            (493, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, '', 78, 'unbilled', 36, NULL, 0, 36, '1538464220', NULL),
            (494, 70, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', '[{\"product_id\":\"37\",\"variant_id\":\"0\",\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":\"3299\"}]', 1, 0, 0, 'App', 'deliver', 'payumoney', 1, 0, 0, 0, 3299, 0, 3299, '', 0, 'Not Delivered', 36, '2018-10-10', 0, 36, '1538464313', NULL),
            (495, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'hj', 78, 'Not Delivered', 36, NULL, 0, 36, '1538472641', NULL),
            (496, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'hj', 78, '', 36, NULL, 0, 36, '1538476564', NULL),
            (497, 44, 'null', NULL, 0, 0, 0, 'App', 'deliver', 'COD', 0, 0, 0, 0, 78, 0, 78, 'hj', 78, 'unbilled', 36, NULL, 0, 36, '1538543119', NULL),
            (498, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 3299, 0, 3299, 'jkd', 0, 'unbilled', 36, NULL, 0, 36, '1538543372', NULL),
            (499, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, 'jkd', 78, 'unbilled', 36, NULL, 0, 36, '1538543381', NULL),
            (500, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, 'jkd', 78, 'unbilled', 36, NULL, 0, 36, '1538543455', NULL),
            (501, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, 'jkd', 78, 'unbilled', 36, NULL, 0, 36, '1538543482', NULL),
            (502, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, 'jkd', 78, 'unbilled', 36, NULL, 0, 36, '1538543496', NULL),
            (503, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, 'jkd', 78, 'unbilled', 36, NULL, 0, 36, '1538543502', NULL),
            (504, 44, 'null', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 78, 0, 78, 'jkd', 78, 'unbilled', 36, NULL, 0, 36, '1538543509', NULL),
            (505, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 3299, 0, 3299, '', 0, 'unbilled', 36, NULL, 0, 36, '1538543544', NULL),
            (506, 44, '[{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 3299, 0, 3299, '', 0, 'unbilled', 36, NULL, 0, 36, '1538543606', NULL),
            (507, 44, '[{\"product_id\":\"35\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"650\",\"total_product_paid_Amount\":650},{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'auth', 0, 0, 0, 0, 3949, 0, 3949, '', 0, 'unbilled', 36, NULL, 0, 36, '1538543861', NULL),
            (508, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601},{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 3900, 0, 3900, '', 0, 'unbilled', 36, NULL, 0, 36, '1538545403', NULL),
            (509, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601},{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'auth', 0, 0, 0, 0, 3900, 0, 3900, '', 0, 'unbilled', 36, NULL, 0, 36, '1538545403', NULL),
            (510, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601},{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'deliver', 'payumoney', 0, 0, 0, 0, 3900, 0, 3900, '', 0, 'unbilled', 36, NULL, 0, 36, '1538545636', NULL),
            (511, 44, '[{\"product_id\":\"36\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"1200\",\"total_product_paid_Amount\":601},{\"product_id\":\"37\",\"variant_id\":0,\"product_Count\":\"1\",\"product_actual_price\":\"4499\",\"total_product_paid_Amount\":3299}]', NULL, 0, 0, 0, 'App', 'pick_up', 'payumoney', 0, 0, 0, 0, 3900, 0, 3900, '', 0, 'unbilled', 36, NULL, 0, 36, '1538545852', NULL)");

                // Table structure for table   houdinv_order_users   

                $getDB->query("CREATE TABLE   houdinv_order_users   (
                      houdinv_order_users_id   int(11) NOT NULL,
                      houdinv_order_users_order_id   int(11) NOT NULL,
                      houdinv_order_users_name   varchar(50) NOT NULL,
                      houdinv_order_users_contact   varchar(20) NOT NULL,
                      houdinv_order_users_main_address   varchar(255) DEFAULT NULL,
                      houdinv_order_users_city   varchar(50) DEFAULT NULL,
                      houdinv_order_users_zip   varchar(20) DEFAULT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_order_users

                $getDB->query("INSERT INTO houdinv_order_users (houdinv_order_users_id, houdinv_order_users_order_id, houdinv_order_users_name, houdinv_order_users_contact, houdinv_order_users_main_address, houdinv_order_users_city, houdinv_order_users_zip) VALUES
                (1, 93, 'shivam', '+917790870946', '', NULL, NULL),
                (2, 94, 'shivam', '+917790870946', '', NULL, NULL)");

                // Table structure for table   houdinv_payment_gateway   

                $getDB->query("CREATE TABLE houdinv_payment_gateway (
                    houdinv_payment_gateway_id int(11) NOT NULL,
                    houdinv_payment_gateway_type enum('payu','auth') NOT NULL,
                    houdinv_payment_gateway_merchnat_key varchar(100) NOT NULL,
                    houdinv_payment_gateway_merchant_salt varchar(100) NOT NULL,
                    houdinv_payment_gateway_status enum('active','deactive') NOT NULL,
                    houdinv_payment_gateway_created_at varchar(30) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_payment_gateway

                $getDB->query("INSERT INTO houdinv_payment_gateway (houdinv_payment_gateway_id, houdinv_payment_gateway_type, houdinv_payment_gateway_merchnat_key, houdinv_payment_gateway_merchant_salt, houdinv_payment_gateway_status, houdinv_payment_gateway_created_at) VALUES
                (1, 'payu', 'MBVIetjo', 'U3YohhxRvu', 'active', '1535068800'),
                (2, 'auth', '6Qq6Bfk2P', '8B533TWkc5Y4ug46', 'active', '1536192000')");

                // Table structure for table   houdinv_possetting   

                $getDB->query("CREATE TABLE houdinv_possetting (
                    id int(111) NOT NULL,
                    minimum_order_amount varchar(250) NOT NULL,
                    delivery_charges varchar(250) NOT NULL,
                    free_delivery int(250) DEFAULT NULL,
                    free_delivery_val varchar(250) DEFAULT NULL,
                    bill_out_stock int(250) DEFAULT NULL,
                    inventory_status int(250) DEFAULT NULL,
                    net_weight_content int(250) DEFAULT NULL,
                    adjustment_during int(250) DEFAULT NULL,
                    allow_sp_desc int(250) DEFAULT NULL,
                    serial_number int(250) DEFAULT NULL,
                    billing_custom_product int(250) DEFAULT NULL,
                    mobile_verify int(250) DEFAULT NULL,
                    accept_payment int(250) DEFAULT NULL,
                    same_barcode int(250) DEFAULT NULL,
                    same_mrp int(250) DEFAULT NULL,
                    fifo int(250) DEFAULT NULL,
                    a_b int(11) DEFAULT NULL,
                    date_time varchar(250) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                  // Dumping data for table houdinv_possetting

                $getDB->query("INSERT INTO houdinv_possetting (id, minimum_order_amount, delivery_charges, free_delivery, free_delivery_val, bill_out_stock, inventory_status, net_weight_content, adjustment_during, allow_sp_desc, serial_number, billing_custom_product, mobile_verify, accept_payment, same_barcode, same_mrp, fifo, a_b, date_time) VALUES
                (1, '12', '78', 1, '40', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '1533720625')");

                // Table structure for table   houdinv_products   

                $getDB->query("CREATE TABLE houdinv_products (
                    houdin_products_id int(11) NOT NULL,
                    houdin_products_category varchar(255) NOT NULL,
                    houdin_products_sub_category_level_1 varchar(255) NOT NULL,
                    houdin_products_sub_category_level_2 varchar(255) NOT NULL,
                    houdin_products_title varchar(255) NOT NULL,
                    houdin_products_short_desc text NOT NULL,
                    houdin_products_desc longblob NOT NULL,
                    houdin_products_pag_title varchar(100) NOT NULL,
                    houdin_product_type int(11) NOT NULL,
                    houdin_product_type_attr_value varchar(255) NOT NULL,
                    houdinv_products_show_on varchar(100) NOT NULL,
                    houdin_products_barcode_image varchar(100) NOT NULL,
                    houdin_products_suppliers varchar(255) NOT NULL,
                    houdin_products_warehouse varchar(255) NOT NULL,
                    houdin_products_price varchar(150) NOT NULL,
                    houdin_products_final_price int(11) DEFAULT '0',
                    houdin_products_main_sku varchar(100) NOT NULL,
                    houdinv_products_main_stock varchar(255) NOT NULL,
                    houdinv_products_total_stocks int(11) DEFAULT '0',
                    houdinv_products_main_minimum_order int(11) NOT NULL DEFAULT '0',
                    houdinv_products_main_sort_order int(11) NOT NULL DEFAULT '0',
                    houdinv_products_main_shipping text NOT NULL,
                    houdinv_products_main_featured int(11) NOT NULL DEFAULT '0',
                    houdinv_products_main_quotation int(11) NOT NULL DEFAULT '0',
                    houdinv_products_main_payment text NOT NULL,
                    houdinv_products_main_images longtext NOT NULL,
                    houdin_products_status int(11) NOT NULL DEFAULT '0',
                    houdinv_products_main_other_variant text NOT NULL,
                    houdin_products_created_date varchar(255) NOT NULL,
                    houdin_products_updated_date varchar(255) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_possetting

                $getDB->query("INSERT INTO houdinv_products (houdin_products_id, houdin_products_category, houdin_products_sub_category_level_1, houdin_products_sub_category_level_2, houdin_products_title, houdin_products_short_desc, houdin_products_desc, houdin_products_pag_title, houdin_product_type, houdin_product_type_attr_value, houdinv_products_show_on, houdin_products_barcode_image, houdin_products_suppliers, houdin_products_warehouse, houdin_products_price, houdin_products_final_price, houdin_products_main_sku, houdinv_products_main_stock, houdinv_products_total_stocks, houdinv_products_main_minimum_order, houdinv_products_main_sort_order, houdinv_products_main_shipping, houdinv_products_main_featured, houdinv_products_main_quotation, houdinv_products_main_payment, houdinv_products_main_images, houdin_products_status, houdinv_products_main_other_variant, houdin_products_created_date, houdin_products_updated_date) VALUES
                (1, '1', '', '', 'Sanyo 107.95 cm (43 inches) XT-43S8100FS Full HD Ips Smart LED TV (Black)', 'Full HD (Resolution: 1920x1080), refresh rate: 60\nConnectivity - Input: 3 HDMI, 2 USB\nAudio: 16 W output\nFree standard wall mount included in box\nWarranty Information: 1 year warranty provided by the manufacturer from date of purchase\nInstallation: For requesting installation/wall mounting/demo of this product once delivered, please contact_us on : [ 18004195088 ] and provide product\'s model name as well as seller\'s details mentioned on the invoice\n60Hz refresh rate\nDolby digital audio with 8W x 2 speakers\nComes with 1 headphones, 1 AV and 1 RF port\nDedicated netflix and youtube button on remote\nWireless Display: Mirror android devices\nEasy Returns: This product is eligible for full refund within 10 days of delivery in case of any product defects, damage or features not matching the description provided', 0x3c703e4d7920646174613c2f703e0d0a3c756c20636c6173733d22612d756e6f7264657265642d6c69737420612d766572746963616c20612d73706163696e672d6e6f6e6522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e3a20307078203070782030707820313870783b20636f6c6f723a20233934393439343b2070616464696e673a203070783b20666f6e742d66616d696c793a2027416d617a6f6e20456d626572272c20417269616c2c2073616e732d73657269663b20666f6e742d73697a653a20313370783b223e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e46756c6c20484420285265736f6c7574696f6e3a20313932307831303830292c207265667265736820726174653a20363020686572747a3c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e436f6e6e6563746976697479202d20496e7075743a20332048444d492c2032205553423c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e417564696f3a2031362057206f75747075743c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e46726565207374616e646172642077616c6c206d6f756e7420696e636c7564656420696e20626f783c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e57617272616e747920496e666f726d6174696f6e3a203120796561722077617272616e74792070726f766964656420627920746865206d616e7566616374757265722066726f6d2064617465206f662070757263686173653c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e496e7374616c6c6174696f6e3a20466f722072657175657374696e6720696e7374616c6c6174696f6e2f77616c6c206d6f756e74696e672f64656d6f206f6620746869732070726f64756374206f6e63652064656c6976657265642c20706c6561736520636f6e746163745f7573206f6e203a205b203138303034313935303838205d20616e642070726f766964652070726f647563742773206d6f64656c206e616d652061732077656c6c2061732073656c6c657227732064657461696c73206d656e74696f6e6564206f6e2074686520696e766f6963653c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e3630487a207265667265736820726174653c2f7370616e3e3c2f6c693e0d0a3c2f756c3e0d0a3c64697620636c6173733d22612d726f7720612d657870616e6465722d636f6e7461696e657220612d657870616e6465722d696e6c696e652d636f6e7461696e657222207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2077696474683a203433322e34363970783b20636f6c6f723a20233131313131313b20666f6e742d66616d696c793a2027416d617a6f6e20456d626572272c20417269616c2c2073616e732d73657269663b20666f6e742d73697a653a20313370783b2220617269612d6c6976653d22706f6c697465223e0d0a3c64697620636c6173733d22612d657870616e6465722d636f6e74656e7420612d657870616e6465722d657874656e642d636f6e74656e7420612d657870616e6465722d636f6e74656e742d657870616e64656422207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206f766572666c6f773a2068696464656e3b2220617269612d657870616e6465643d2274727565223e0d0a3c756c20636c6173733d22612d756e6f7264657265642d6c69737420612d766572746963616c20612d73706163696e672d6e6f6e6522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e3a20307078203070782030707820313870783b20636f6c6f723a20233934393439343b2070616464696e673a203070783b223e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e446f6c6279206469676974616c20617564696f20776974682038572078203220737065616b6572733c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e436f6d657320776974682031206865616470686f6e65732c203120415620616e64203120524620706f72743c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e446564696361746564206e6574666c697820616e6420796f757475626520627574746f6e206f6e2072656d6f74653c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e576972656c65737320446973706c61793a204d6972726f7220616e64726f696420646576696365733c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e456173792052657475726e733a20546869732070726f6475637420697320656c696769626c6520666f722066756c6c20726566756e642077697468696e2031302064617973206f662064656c697665727920696e2063617365206f6620616e792070726f6475637420646566656374732c2064616d616765206f72206665617475726573206e6f74206d61746368696e6720746865206465736372697074696f6e2070726f76696465643c2f7370616e3e3c2f6c693e0d0a3c2f756c3e0d0a3c2f6469763e0d0a3c2f6469763e, '', 0, '', 'Both', '', '', '', '{\"price\":\"12\",\"discount\":\"10\",\"cost_price\":\"14.000\",\"sale_price\":\"13.000\"}', 2, '00001', '[{\"main\":\"4\",\"value\":22},{\"main\":\"2\",\"value\":764},{\"main\":\"3\",\"value\":704},{\"main\":\"5\",\"value\":704}]', 90, 2, 1, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 0, '[\"cash\",\"bank\"]', '[\"1664950sm3_(1).jpg\",\"354852129.jpg\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1531465364', ''),
                (2, '1', '', '', 'Sanyo 107.95 cm (43 inches) XT-43S8100FS Full HD Ips Smart LED TV (Black)', 'Full HD (Resolution: 1920x1080), refresh rate: 60 hertz\r\nConnectivity - Input: 3 HDMI, 2 USB\r\nAudio: 16 W output\r\nFree standard wall mount included in box\r\nWarranty Information: 1 year warranty provided by the manufacturer from date of purchase\r\nInstallation: For requesting installation/wall mounting/demo of this product once delivered, please contact_us on : [ 18004195088 ] and provide product\'s model name as well as seller\'s details mentioned on the invoice\r\n60Hz refresh rate\r\nDolby digital audio w', 0x3c703e4d7920646174613c2f703e0d0a3c756c20636c6173733d22612d756e6f7264657265642d6c69737420612d766572746963616c20612d73706163696e672d6e6f6e6522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e3a20307078203070782030707820313870783b20636f6c6f723a20233934393439343b2070616464696e673a203070783b20666f6e742d66616d696c793a2027416d617a6f6e20456d626572272c20417269616c2c2073616e732d73657269663b20666f6e742d73697a653a20313370783b223e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e46756c6c20484420285265736f6c7574696f6e3a20313932307831303830292c207265667265736820726174653a20363020686572747a3c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e436f6e6e6563746976697479202d20496e7075743a20332048444d492c2032205553423c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e417564696f3a2031362057206f75747075743c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e46726565207374616e646172642077616c6c206d6f756e7420696e636c7564656420696e20626f783c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e57617272616e747920496e666f726d6174696f6e3a203120796561722077617272616e74792070726f766964656420627920746865206d616e7566616374757265722066726f6d2064617465206f662070757263686173653c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e496e7374616c6c6174696f6e3a20466f722072657175657374696e6720696e7374616c6c6174696f6e2f77616c6c206d6f756e74696e672f64656d6f206f6620746869732070726f64756374206f6e63652064656c6976657265642c20706c6561736520636f6e746163745f7573206f6e203a205b203138303034313935303838205d20616e642070726f766964652070726f647563742773206d6f64656c206e616d652061732077656c6c2061732073656c6c657227732064657461696c73206d656e74696f6e6564206f6e2074686520696e766f6963653c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e3630487a207265667265736820726174653c2f7370616e3e3c2f6c693e0d0a3c2f756c3e0d0a3c64697620636c6173733d22612d726f7720612d657870616e6465722d636f6e7461696e657220612d657870616e6465722d696e6c696e652d636f6e7461696e657222207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2077696474683a203433322e34363970783b20636f6c6f723a20233131313131313b20666f6e742d66616d696c793a2027416d617a6f6e20456d626572272c20417269616c2c2073616e732d73657269663b20666f6e742d73697a653a20313370783b2220617269612d6c6976653d22706f6c697465223e0d0a3c64697620636c6173733d22612d657870616e6465722d636f6e74656e7420612d657870616e6465722d657874656e642d636f6e74656e7420612d657870616e6465722d636f6e74656e742d657870616e64656422207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206f766572666c6f773a2068696464656e3b2220617269612d657870616e6465643d2274727565223e0d0a3c756c20636c6173733d22612d756e6f7264657265642d6c69737420612d766572746963616c20612d73706163696e672d6e6f6e6522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e3a20307078203070782030707820313870783b20636f6c6f723a20233934393439343b2070616464696e673a203070783b223e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e446f6c6279206469676974616c20617564696f20773c2f7370616e3e3c2f6c693e0d0a3c2f756c3e0d0a3c2f6469763e0d0a3c2f6469763e, '', 0, '', 'Both', '', '', '', '{\"price\":\"12\",\"discount\":\"10\",\"cost_price\":\"14.000\",\"sale_price\":\"13.000\"}', 2, '00002', '[{\"main\":\"4\",\"value\":2},{\"main\":\"2\",\"value\":704},{\"main\":\"3\",\"value\":704},\r\n{\"main\":\"5\",\"value\":704}]', 120, 2, 1, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 0, '[\"cash\",\"bank\"]', '[\"1664950sm3_(1).jpg\",\"354852129.jpg\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1531473603', '1531473603'),
                (3, '1', '', '', 'Sanyo 107.95 cm (43 inches) XT-43S8100FS Full HD Ips Smart LED TV (Black)', 'Full HD (Resolution: 1920x1080), refresh rate: 60 hertz\r\nConnectivity - Input: 3 HDMI, 2 USB\r\nAudio: 16 W output\r\nFree standard wall mount included in box\r\nWarranty Information: 1 year warranty provided by the manufacturer from date of purchase\r\nInstallation: For requesting installation/wall mounting/demo of this product once delivered, please contact_us on : [ 18004195088 ] and provide product\'s model name as well as seller\'s details mentioned on the invoice\r\n60Hz refresh rate\r\nDolby digital audio w', 0x3c703e4d7920646174613c2f703e0d0a3c756c20636c6173733d22612d756e6f7264657265642d6c69737420612d766572746963616c20612d73706163696e672d6e6f6e6522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e3a20307078203070782030707820313870783b20636f6c6f723a20233934393439343b2070616464696e673a203070783b20666f6e742d66616d696c793a2027416d617a6f6e20456d626572272c20417269616c2c2073616e732d73657269663b20666f6e742d73697a653a20313370783b223e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e46756c6c20484420285265736f6c7574696f6e3a20313932307831303830292c207265667265736820726174653a20363020686572747a3c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e436f6e6e6563746976697479202d20496e7075743a20332048444d492c2032205553423c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e417564696f3a2031362057206f75747075743c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e46726565207374616e646172642077616c6c206d6f756e7420696e636c7564656420696e20626f783c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e57617272616e747920496e666f726d6174696f6e3a203120796561722077617272616e74792070726f766964656420627920746865206d616e7566616374757265722066726f6d2064617465206f662070757263686173653c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e496e7374616c6c6174696f6e3a20466f722072657175657374696e6720696e7374616c6c6174696f6e2f77616c6c206d6f756e74696e672f64656d6f206f6620746869732070726f64756374206f6e63652064656c6976657265642c20706c6561736520636f6e746163745f7573206f6e203a205b203138303034313935303838205d20616e642070726f766964652070726f647563742773206d6f64656c206e616d652061732077656c6c2061732073656c6c657227732064657461696c73206d656e74696f6e6564206f6e2074686520696e766f6963653c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e3630487a207265667265736820726174653c2f7370616e3e3c2f6c693e0d0a3c2f756c3e0d0a3c64697620636c6173733d22612d726f7720612d657870616e6465722d636f6e7461696e657220612d657870616e6465722d696e6c696e652d636f6e7461696e657222207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2077696474683a203433322e34363970783b20636f6c6f723a20233131313131313b20666f6e742d66616d696c793a2027416d617a6f6e20456d626572272c20417269616c2c2073616e732d73657269663b20666f6e742d73697a653a20313370783b2220617269612d6c6976653d22706f6c697465223e0d0a3c64697620636c6173733d22612d657870616e6465722d636f6e74656e7420612d657870616e6465722d657874656e642d636f6e74656e7420612d657870616e6465722d636f6e74656e742d657870616e64656422207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206f766572666c6f773a2068696464656e3b2220617269612d657870616e6465643d2274727565223e0d0a3c756c20636c6173733d22612d756e6f7264657265642d6c69737420612d766572746963616c20612d73706163696e672d6e6f6e6522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e3a20307078203070782030707820313870783b20636f6c6f723a20233934393439343b2070616464696e673a203070783b223e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e446f6c6279206469676974616c20617564696f20773c2f7370616e3e3c2f6c693e0d0a3c2f756c3e0d0a3c2f6469763e0d0a3c2f6469763e, '', 0, '', 'Both', '', '', '', '{\"price\":\"12\",\"discount\":\"10\",\"cost_price\":\"14.000\",\"sale_price\":\"13.000\"}', 2, '00002', '[{\"main\":\"4\",\"value\":2},{\"main\":\"2\",\"value\":704},{\"main\":\"3\",\"value\":704},\r\n{\"main\":\"5\",\"value\":704}]', 120, 2, 1, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 0, '[\"cash\",\"bank\"]', '[\"1664950sm3_(1).jpg\",\"354852129.jpg\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1531482167', '1531482167'),
                (4, '1', '', '', 'LG 80 cm (32 inches) 32LJ573D HD Ready LED Smart TV (Mineral Silver)', 'HD Ready (Resolution: 1366 x768)\r\nConnectivity - Input: 2*HDMI, 1*USB,\r\nAudio: 20 W output\r\nFree standard wall mount provided at the time of installation\r\nInstallation: For requesting installation/wall mounting/demo of this product once delivered, please directly call LG support on 18003159999 or 18001809999 and provide product\'s model name as well as seller\'s details mentioned on the invoice\r\nLife-like color - IPS offers a color impression that is most identical to that of the original image and the colors are truly closest to nature and most comfortable for the eyes\r\nWide Viewing Angle: Among LCD panels, it is visibly clear that IPS shows the most consistent co', 0x3c756c20636c6173733d22612d756e6f7264657265642d6c69737420612d766572746963616c20612d73706163696e672d6e6f6e6522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e3a20307078203070782030707820313870783b20636f6c6f723a20233934393439343b2070616464696e673a203070783b20666f6e742d66616d696c793a2027416d617a6f6e20456d626572272c20417269616c2c2073616e732d73657269663b20666f6e742d73697a653a20313370783b223e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e484420526561647920285265736f6c7574696f6e3a20313336362078373638293c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e436f6e6e6563746976697479202d20496e7075743a20322a48444d492c20312a5553422c3c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e417564696f3a2032302057206f75747075743c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e46726565207374616e646172642077616c6c206d6f756e742070726f7669646564206174207468652074696d65206f6620696e7374616c6c6174696f6e3c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e496e7374616c6c6174696f6e3a20466f722072657175657374696e6720696e7374616c6c6174696f6e2f77616c6c206d6f756e74696e672f64656d6f206f6620746869732070726f64756374206f6e63652064656c6976657265642c20706c65617365206469726563746c792063616c6c204c4720737570706f7274206f6e203138303033313539393939206f7220313830303138303939393920616e642070726f766964652070726f647563742773206d6f64656c206e616d652061732077656c6c2061732073656c6c657227732064657461696c73206d656e74696f6e6564206f6e2074686520696e766f6963653c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e4c6966652d6c696b6520636f6c6f72202d20495053206f6666657273206120636f6c6f7220696d7072657373696f6e2074686174206973206d6f7374206964656e746963616c20746f2074686174206f6620746865206f726967696e616c20696d61676520616e642074686520636f6c6f727320617265207472756c7920636c6f7365737420746f206e617475726520616e64206d6f737420636f6d666f727461626c6520666f722074686520657965733c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e576964652056696577696e6720416e676c653a20416d6f6e67204c43442070616e656c732c2069742069732076697369626c7920636c6561722074686174204950532073686f777320746865206d6f737420636f6e73697374656e7420636f3c2f7370616e3e3c2f6c693e0d0a3c2f756c3e, '', 0, '', 'Both', '', '', '', '{\"price\":\"12\",\"discount\":\"10\",\"cost_price\":\"14.000\",\"sale_price\":\"13.000\"}', 2, '00003', '[{\"main\":\"4\",\"value\":2},{\"main\":\"2\",\"value\":704},{\"main\":\"3\",\"value\":704},\r\n{\"main\":\"5\",\"value\":704}]', 120, 2, 1, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 0, '[\"cash\",\"bank\"]', '[\"977640066.jpg\",\"8913001h.jpg\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1531465937', '1531465937'),
                (5, '1', '', '', 'Surya Full HD LED TV 32 inch with Samsung A+ Display Panel and Bass Tube Speakers for Extra Party Sound', 'Full HD Ready (Resolution: 1366x1080), Refresh Rate: 60 hertz With True Colour Support by Samsung A+ panel and Xifo Engine\r\n•Best in Sound , first time in India with In built home Theatre Speakers of bass Tube Design\r\n•With No Colour change at view angle of 179 degree\r\n•No Blackness in Colours if you the LED Tv From Top or Bottom and No colour Change\r\n•Installation to be done by customer, Seller will not provide installation.\r\n› See more product details\r\nShips in Original Packaging: \r\nPackaging may reveal contents and cannot be hidden.\r\nEasy Return policy\r\nEasy Returns: This product is eligible for full refund within 10 days of delivery in case of any product defects, damage or features not matching the description provided.', 0x3c6469762069643d2263656e746572436f6c2220636c6173733d2263656e746572436f6c416c69676e22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e2d72696768743a2032373070783b206d617267696e2d6c6566743a203631302e35333170783b20636f6c6f723a20233131313131313b20666f6e742d66616d696c793a2027416d617a6f6e20456d626572272c20417269616c2c2073616e732d73657269663b20666f6e742d73697a653a20313370783b223e0d0a3c6469762069643d226665617475726562756c6c6574735f666561747572655f6469762220636c6173733d226665617475726522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2220646174612d666561747572652d6e616d653d226665617475726562756c6c657473223e0d0a3c6469762069643d22666561747572652d62756c6c6574732220636c6173733d22612d73656374696f6e20612d73706163696e672d6d656469756d20612d73706163696e672d746f702d736d616c6c22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e2d746f703a20313070782021696d706f7274616e743b206d617267696e2d626f74746f6d3a203070783b223e0d0a3c756c20636c6173733d22612d756e6f7264657265642d6c69737420612d766572746963616c20612d73706163696e672d6e6f6e6522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e3a20307078203070782030707820313870783b20636f6c6f723a20233934393439343b2070616464696e673a203070783b223e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e46756c6c20484420526561647920285265736f6c7574696f6e3a20313336367831303830292c205265667265736820526174653a20363020686572747a2057697468205472756520436f6c6f757220537570706f72742062792053616d73756e6720412b2070616e656c20616e64205869666f20456e67696e653c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e2662756c6c3b4265737420696e20536f756e64202c2066697273742074696d6520696e20496e646961207769746820496e206275696c7420686f6d65205468656174726520537065616b657273206f66206261737320547562652044657369676e3c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e2662756c6c3b57697468204e6f20436f6c6f7572206368616e6765206174207669657720616e676c65206f6620313739206465677265653c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e2662756c6c3b4e6f20426c61636b6e65737320696e20436f6c6f75727320696620796f7520746865204c45442054762046726f6d20546f70206f7220426f74746f6d20616e64204e6f20636f6c6f7572204368616e67653c2f7370616e3e3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c6973742d7374796c653a20646973633b20776f72642d777261703a20627265616b2d776f72643b206d617267696e3a203070783b223e3c7370616e20636c6173733d22612d6c6973742d6974656d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20233131313131313b223e2662756c6c3b496e7374616c6c6174696f6e20746f20626520646f6e6520627920637573746f6d65722c2053656c6c65722077696c6c206e6f742070726f7669646520696e7374616c6c6174696f6e2e3c2f7370616e3e3c2f6c693e0d0a3c2f756c3e0d0a3c7370616e20636c6173733d2263617265746e65787422207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20636f6c6f723a20236363363630303b20666f6e742d73697a653a20312e32656d3b20666f6e742d7765696768743a20626f6c643b206d617267696e2d6c6566743a203470783b223e2672736171756f3b3c2f7370616e3e266e6273703b3c612069643d227365654d6f726544657461696c734c696e6b2220636c6173733d22612d6c696e6b2d6e6f726d616c22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233030363663303b2220687265663d2268747470733a2f2f7777772e616d617a6f6e2e696e2f64702f423037384e584c394d4d2f7265663d737370615f646b5f64657461696c5f333f7073633d3126616d703b70645f72645f693d423037384e584c394d4d26616d703b70665f72645f6d3d41315642414c39544c355743424626616d703b70665f72645f703d3437313234323231383831383733353432373026616d703b70665f72645f723d5148484a345854544d33303839545a455234415026616d703b70645f72645f77673d646a38526226616d703b70665f72645f733d6465736b746f702d64702d73696d7326616d703b70665f72645f743d343037303126616d703b70645f72645f773d344d556b6726616d703b70665f72645f693d6465736b746f702d64702d73696d7326616d703b70645f72645f723d34376639386661332d383636302d313165382d616639362d6462313532646363656434622370726f6475637444657461696c73223e536565206d6f72652070726f647563742064657461696c733c2f613e3c2f6469763e0d0a3c2f6469763e0d0a3c6469762069643d22616e646f6e436f72645f666561747572655f6469762220636c6173733d226665617475726522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2220646174612d666561747572652d6e616d653d22616e646f6e436f7264223e266e6273703b3c2f6469763e0d0a3c6469762069643d22484c4358436f6d70617269736f6e4a756d706c696e6b5f666561747572655f64697622207374796c653d22626f782d73697a696e673a20626f726465722d626f783b223e266e6273703b3c2f6469763e0d0a3c6469762069643d22656470496e67726573735f666561747572655f6469762220636c6173733d226665617475726522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2220646174612d666561747572652d6e616d653d22656470496e6772657373223e266e6273703b3c2f6469763e0d0a3c6469762069643d226f726967696e616c5061636b6167696e674d6573736167655f666561747572655f6469762220636c6173733d226665617475726522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2220646174612d666561747572652d6e616d653d226f726967696e616c5061636b6167696e674d657373616765223e0d0a3c6469762069643d226f726967696e616c5061636b6167696e674d6573736167652220636c6173733d22612d73656374696f6e20612d73706163696e672d736d616c6c22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e2d626f74746f6d3a203070783b223e3c7370616e20636c6173733d22612d73697a652d6261736520612d746578742d626f6c6422207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c696e652d6865696768743a20313970782021696d706f7274616e743b20666f6e742d7765696768743a203730302021696d706f7274616e743b223e536869707320696e204f726967696e616c205061636b6167696e673a266e6273703b3c2f7370616e3e3c6272207374796c653d22626f782d73697a696e673a20626f726465722d626f783b22202f3e3c7370616e20636c6173733d22612d73697a652d6261736522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206c696e652d6865696768743a20313970782021696d706f7274616e743b223e3c7370616e207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20666f6e742d7765696768743a20626f6c643b223e5061636b6167696e67206d61792072657665616c20636f6e74656e74733c2f7370616e3e266e6273703b616e642063616e6e6f742062652068696464656e2e3c2f7370616e3e3c2f6469763e0d0a3c2f6469763e0d0a3c6469762069643d226e6577657256657273696f6e5f666561747572655f6469762220636c6173733d226665617475726522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2220646174612d666561747572652d6e616d653d226e6577657256657273696f6e223e266e6273703b3c2f6469763e0d0a3c6469762069643d2270726f64756374416c6572745f666561747572655f6469762220636c6173733d226665617475726522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b20646973706c61793a207461626c653b2220646174612d666561747572652d6e616d653d2270726f64756374416c657274223e266e6273703b3c2f6469763e0d0a3c6469762069643d227768697465476c6f76654d6573736167655f666561747572655f6469762220636c6173733d226665617475726522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2220646174612d666561747572652d6e616d653d227768697465476c6f76654d657373616765223e266e6273703b3c2f6469763e0d0a3c6469762069643d2276656e646f72506f7765726564436f75706f6e5f666561747572655f6469762220636c6173733d226665617475726522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2220646174612d666561747572652d6e616d653d2276656e646f72506f7765726564436f75706f6e223e266e6273703b3c2f6469763e0d0a3c2f6469763e0d0a3c6469762069643d22687170577261707065722220636c6173733d2263656e746572436f6c416c69676e22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e2d72696768743a2032373070783b206d617267696e2d6c6566743a203631302e35333170783b20636f6c6f723a20233131313131313b20666f6e742d66616d696c793a2027416d617a6f6e20456d626572272c20417269616c2c2073616e732d73657269663b20666f6e742d73697a653a20313370783b223e0d0a3c6469762069643d226865726f517569636b50726f6d6f5f666561747572655f6469762220636c6173733d226665617475726522207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2220646174612d666561747572652d6e616d653d226865726f517569636b50726f6d6f223e0d0a3c6469762069643d226865726f2d717569636b2d70726f6d6f2220636c6173733d22612d726f7720612d73706163696e672d6d656469756d22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2077696474683a203433322e34363970783b206d617267696e2d626f74746f6d3a203070782021696d706f7274616e743b223e3c687220636c6173733d22612d73706163696e672d6d656469756d20612d646976696465722d6e6f726d616c22207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b206865696768743a203170783b20626f726465722d72696768742d77696474683a203070783b20626f726465722d626f74746f6d2d77696474683a203070783b20626f726465722d6c6566742d77696474683a203070783b20626f726465722d746f702d7374796c653a20736f6c69643b20626f726465722d746f702d636f6c6f723a20236537653765373b206c696e652d6865696768743a20313970783b206d617267696e2d746f703a203070783b206d617267696e2d626f74746f6d3a20313870782021696d706f7274616e743b22202f3e0d0a3c64697620636c6173733d22756e69666965645f7769646765742072636d426f647922207374796c653d22626f782d73697a696e673a20626f726465722d626f783b206d617267696e2d626f74746f6d3a20323670783b20666f6e742d73697a653a20313270783b20666f6e742d66616d696c793a20617269616c2c2068656c7665746963612c2073616e732d73657269663b223e0d0a3c6832207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2070616464696e673a203070783b206d617267696e3a203070782030707820302e3335656d3b20746578742d72656e646572696e673a206f7074696d697a654c65676962696c6974793b20666f6e742d73697a653a2031362e3270783b206c696e652d6865696768743a20323270783b20636f6c6f723a20236534373931313b223e456173792052657475726e20706f6c6963793c2f68323e0d0a3c70207374796c653d22626f782d73697a696e673a20626f726465722d626f783b2070616464696e673a203070783b206d617267696e3a203070782030707820302e35656d3b206c696e652d6865696768743a20312e34656d3b223e456173792052657475726e733a20546869732070726f6475637420697320656c696769626c6520666f722066756c6c20726566756e642077697468696e2031302064617973206f662064656c697665727920696e2063617365206f6620616e792070726f6475637420646566656374732c2064616d616765206f72206665617475726573206e6f74206d61746368696e6720746865206465736372697074696f6e2070726f76696465642e3c2f703e0d0a3c2f6469763e0d0a3c2f6469763e0d0a3c2f6469763e0d0a3c2f6469763e, '', 0, '', 'Both', '', '', '', '{\"price\":\"12\",\"discount\":\"10\",\"cost_price\":\"14.000\",\"sale_price\":\"13.000\"}', 2, '0004', '[{\"main\":\"4\",\"value\":2},{\"main\":\"2\",\"value\":704},{\"main\":\"3\",\"value\":704},\r\n{\"main\":\"5\",\"value\":704}]', 120, 2, 1, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 0, '[\"cash\",\"bank\"]', '[\"712436042.jpg\",\"646400146.jpg\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1531466076', '1531466076'),
                (6, '1', '', '', 'Surya Full HD LED TV 32 inch with Samsung A+ Display Panel and Bass Tube Speakers for Extra Party Sound', 'Full HD Ready (Resolution: 1366x1080), Refresh Rate: 60 hertz With True Colour Support by Samsung A+ panel and Xifo Engine\r\n•Best in Sound , first time in India with In built home Theatre Speakers of bass Tube Design\r\n•With No Colour change at view angle of 179 degree\r\n•No Blackness in Colours if you the LED Tv From Top or Bottom and No colour Change\r\n•Installation to be done by customer, Seller will not provide installation.\r\n› See more product details\r\nShips in Original Packaging: \r\nPackaging may reveal contents and cannot be hidden.\r\nEasy Return policy\r\nEasy Returns: This product is eligible for full refund within 10 days of delivery in case of any product defects, damage or features not matching the description provided.', 0x3c703e6f72792c2061204242432050616e6f72616d6120696e7665737469676174696f6e2068617320666f756e642e3c2f703e0d0a3c703e5468652062616e6b20616e6e6f756e63656420696e20323030382074686174204d616e636865737465722043697479206f776e657220536865696b68204d616e736f7572206861642061677265656420746f20696e76657374206d6f7265207468616e2026706f756e643b33626e2e3c2f703e0d0a3c703e427574207468652042424320666f756e64207468617420746865206d6f3c2f703e, '', 0, '', 'Both', '', '', '', '{\"price\":\"12\",\"discount\":\"10\",\"cost_price\":\"14.000\",\"sale_price\":\"13.000\"}', 2, '0004', '[{\"main\":\"4\",\"value\":2},{\"main\":\"2\",\"value\":704},{\"main\":\"3\",\"value\":704},\r\n{\"main\":\"5\",\"value\":704}]', 0, 2, 1, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 0, '[\"cash\",\"bank\"]', '[\"1747550sm1.jpg\",\"2312181m.jpg\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1531466244', '1531466244'),
                (7, '1', '', '', 'Fitbit Blaze Smart Fitness Watch, Small (Black/Silver)', 'ment bailout late in 2008 after it was rescued by £7bn worth of new investment, most of which came from the gulf states of Qatar and Abu Dhabi.\r\nHalf of the cash was supposed to be coming from Sheikh Mansour.\r\nBut Barclays has admitted it was told the investor might chan', 0x3c703e6d656e74206261696c6f7574206c61746520696e20323030382061667465722069742077617320726573637565642062792026706f756e643b37626e20776f727468206f66206e657720696e766573746d656e742c206d6f7374206f662077686963682063616d652066726f6d207468652067756c6620737461746573206f6620516174617220616e64204162752044686162692e3c2f703e0d0a3c703e48616c66206f662074686520636173682077617320737570706f73656420746f20626520636f6d696e672066726f6d20536865696b68204d616e736f75722e3c2f703e0d0a3c703e42757420426172636c617973206861732061646d69747465642069742077617320746f6c642074686520696e766573746f72206d69676874206368616e3c2f703e, '', 0, '', 'Both', '', '', '', '{\"price\":\"12\",\"discount\":\"10\",\"cost_price\":\"14.000\",\"sale_price\":\"13.000\"}', 2, '00005', '[{\"main\":\"4\",\"value\":2},{\"main\":\"2\",\"value\":704},{\"main\":\"3\",\"value\":704},\r\n{\"main\":\"5\",\"value\":704}]', 0, 2, 1, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 0, '[\"cash\",\"bank\"]', '[\"3094260g.jpg\",\"4746041h.jpg\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1531466455', '1531466455'),
                (8, '1', '', '', 'Apple Watch Series 3 GPS 42mm Smart Watch (Silver Aluminum Case, Fog Sport Band)', 'prospectuses that were issued the following day.\r\nBut the disclosure was buried deep in the small print and said that Sheikh Mansour \"has arranged for his investment…to be funded by an Abu Dhabi governmental investment vehicle, which will become the indirect shareholder\".\r\nBarclays still used the phrase \"his investment\", even though it knew Sheikh Mansour was not actually investing in the bank at the time.\r\nThe bank continued to mislead shareholders in its annual reports of 2008 and 2009, both of which identified Sheikh Mansour as t', 0x3c703e70726f73706563747573657320746861742077657265206973737565642074686520666f6c6c6f77696e67206461792e3c2f703e0d0a3c703e4275742074686520646973636c6f737572652077617320627572696564206465657020696e2074686520736d616c6c207072696e7420616e642073616964207468617420536865696b68204d616e736f7572202268617320617272616e67656420666f722068697320696e766573746d656e742668656c6c69703b746f2062652066756e64656420627920616e2041627520446861626920676f7665726e6d656e74616c20696e766573746d656e742076656869636c652c2077686963682077696c6c206265636f6d652074686520696e646972656374207368617265686f6c646572222e3c2f703e0d0a3c703e426172636c617973207374696c6c20757365642074686520706872617365202268697320696e766573746d656e74222c206576656e2074686f756768206974206b6e657720536865696b68204d616e736f757220776173206e6f742061637475616c6c7920696e76657374696e6720696e207468652062616e6b206174207468652074696d652e3c2f703e0d0a3c703e5468652062616e6b20636f6e74696e75656420746f206d69736c656164207368617265686f6c6465727320696e2069747320616e6e75616c207265706f727473206f66203230303820616e6420323030392c20626f7468206f66207768696368206964656e74696669656420536865696b68204d616e736f757220617320743c2f703e, '', 0, '', 'Both', '', '', '', '{\"price\":\"12\",\"discount\":\"10\",\"cost_price\":\"14.000\",\"sale_price\":\"13.000\"}', 2, '00006', '[{\"main\":\"4\",\"value\":2},{\"main\":\"2\",\"value\":704},{\"main\":\"3\",\"value\":704},{\"main\":\"5\",\"value\":704}]', 119, 2, 1, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 0, '[\"cash\",\"bank\"]', '[\"3930000h2-2.jpg\",\"914681161.jpg\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1531466584', '1531466584'),
                (9, '1', '', '', 'Noise Loop Lite Smartwatch (Black)', 'The chairman of IPIC is Sheikh Mansour. Although the Sheikh did not invest any of his own money in Barclays at the time, a company he controlled was initially issued warrants to buy 758 million shares in the bank.\r\nThe warrants gave the own', 0x3c703e5468652063686169726d616e206f66204950494320697320536865696b68204d616e736f75722e20416c74686f7567682074686520536865696b6820646964206e6f7420696e7665737420616e79206f6620686973206f776e206d6f6e657920696e20426172636c617973206174207468652074696d652c206120636f6d70616e7920686520636f6e74726f6c6c65642077617320696e697469616c6c79206973737565642077617272616e747320746f2062757920373538206d696c6c696f6e2073686172657320696e207468652062616e6b2e3c2f703e0d0a3c703e5468652077617272616e7473206761766520746865206f776e3c2f703e, '', 0, '', 'Both', '', '', '', '{\"price\":\"12\",\"discount\":\"10\",\"cost_price\":\"14.000\",\"sale_price\":\"13.000\"}', 2, '0006', '[{\"main\":\"4\",\"value\":2},{\"main\":\"2\",\"value\":704},{\"main\":\"3\",\"value\":704},\r\n{\"main\":\"5\",\"value\":704}]', 120, 2, 1, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 1, '[\"cash\",\"bank\"]', '[\"933253078.jpg\",\"3978581b.jpg\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1531466729', '1531466729');
                INSERT INTO houdinv_products (houdin_products_id, houdin_products_category, houdin_products_sub_category_level_1, houdin_products_sub_category_level_2, houdin_products_title, houdin_products_short_desc, houdin_products_desc, houdin_products_pag_title, houdin_product_type, houdin_product_type_attr_value, houdinv_products_show_on, houdin_products_barcode_image, houdin_products_suppliers, houdin_products_warehouse, houdin_products_price, houdin_products_final_price, houdin_products_main_sku, houdinv_products_main_stock, houdinv_products_total_stocks, houdinv_products_main_minimum_order, houdinv_products_main_sort_order, houdinv_products_main_shipping, houdinv_products_main_featured, houdinv_products_main_quotation, houdinv_products_main_payment, houdinv_products_main_images, houdin_products_status, houdinv_products_main_other_variant, houdin_products_created_date, houdin_products_updated_date) VALUES
                (20, '1', '', '', 'Hancock Men White Slim Fit Solid Formal Shirt', 'White solid formal shirt, has a spread collar, button placket, long sleeves, a patch pocket', 0x3c64697620636c6173733d227064702d70726f6475637444657363726970746f727322207374796c653d22626f782d73697a696e673a20696e68657269743b20626f726465723a203070783b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a206d656469756d3b223e0d0a3c64697620636c6173733d227064702d70726f6475637444657363726970746f7273436f6e7461696e657222207374796c653d22626f782d73697a696e673a20696e68657269743b2070616464696e673a2030707820323070783b223e0d0a3c646976207374796c653d22626f782d73697a696e673a20696e68657269743b223e0d0a3c683420636c6173733d227064702d70726f647563742d6465736372697074696f6e2d7469746c6522207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366237393b20666f6e742d73697a653a20313570783b206d617267696e2d746f703a20313570783b206d617267696e2d626f74746f6d3a203070783b20666f6e742d7765696768743a203530303b223e4d6174657269616c2026616d703b20436172653c2f68343e0d0a3c7020636c6173733d227064702d70726f647563742d6465736372697074696f6e2d636f6e74656e7422207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b206c696e652d6865696768743a20312e343b20666f6e742d73697a653a20313570783b206d617267696e2d746f703a20313570783b223e436f74746f6e266e6273703b3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b22202f3e4d616368696e652d776173683c2f703e0d0a3c2f6469763e0d0a3c2f6469763e0d0a3c2f6469763e0d0a3c64697620636c6173733d2270696e636f64652d736572766963656162696c69747922207374796c653d22626f782d73697a696e673a20696e68657269743b2070616464696e673a2030707820323070783b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a206d656469756d3b223e3c627574746f6e20636c6173733d2270696e636f64652d627574746f6e22207374796c653d22626f782d73697a696e673a20696e68657269743b20666f6e742d7374796c653a20696e68657269743b20666f6e742d76617269616e743a20696e68657269743b20666f6e742d737472657463683a20696e68657269743b20666f6e742d73697a653a20313370783b206c696e652d6865696768743a20696e68657269743b20666f6e742d66616d696c793a20696e68657269743b206f766572666c6f773a2076697369626c653b206d617267696e3a203130707820307078203070783b20746578742d7472616e73666f726d3a206361706974616c697a653b20637572736f723a20706f696e7465723b206f75746c696e653a203070783b2070616464696e673a2031307078203070783b206261636b67726f756e642d636f6c6f723a20236666666666663b20636f6c6f723a20233532366364303b20626f726465723a2030707820696e697469616c20696e697469616c3b223e436865636b2044656c6976657279204f7074696f6e733c2f627574746f6e3e3c2f6469763e0d0a3c64697620636c6173733d226d6574612d636f6e7461696e657222207374796c653d22626f782d73697a696e673a20696e68657269743b206d617267696e3a2030707820323070783b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a206d656469756d3b223e0d0a3c7020636c6173733d226d6574612d746178496e666f206d6574612d696e666f22207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233934393839663b20666f6e742d73697a653a20313370783b223e5461783a204170706c696361626c6520746178206f6e20746865206261736973206f66206578616374206c6f636174696f6e2026616d703b20646973636f756e742077696c6c2062652063686172676564206174207468652074696d65206f6620636865636b6f75743c2f703e0d0a3c7020636c6173733d226d6574612d696e666f22207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233934393839663b20666f6e742d73697a653a20313370783b223e31303025204f726967696e616c2050726f64756374733c2f703e0d0a3c7020636c6173733d226d6574612d696e666f22207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233934393839663b20666f6e742d73697a653a20313370783b223e467265652044656c6976657279206f6e206f726465722061626f76652052732e266e6273703b313139393c2f703e0d0a3c7020636c6173733d226d6574612d696e666f22207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233934393839663b20666f6e742d73697a653a20313370783b223e3c7370616e20636c6173733d226d6574612d6465736322207374796c653d22626f782d73697a696e673a20696e68657269743b206d617267696e3a20357078203070783b223e43617368206f6e2064656c6976657279206d6967687420626520617661696c61626c653c2f7370616e3e3c2f703e0d0a3c7020636c6173733d226d6574612d696e666f22207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233934393839663b20666f6e742d73697a653a20313370783b223e3c7370616e20636c6173733d226d6574612d6465736322207374796c653d22626f782d73697a696e673a20696e68657269743b206d617267696e3a20357078203070783b223e4561737920333020646179732072657475726e732026616d703b20333020646179732065786368616e6765733c2f7370616e3e3c2f703e0d0a3c7020636c6173733d226d6574612d696e666f22207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233934393839663b20666f6e742d73697a653a20313370783b223e3c7370616e20636c6173733d226d6574612d6465736322207374796c653d22626f782d73697a696e673a20696e68657269743b206d617267696e3a20357078203070783b223e5472792026616d703b20427579206d6967687420626520617661696c61626c653c2f7370616e3e3c2f703e0d0a3c2f6469763e, 'sdas', 1, '[{\"main\":\"type\",\"value\":\"234\"},{\"main\":\"warranty\",\"value\":\"2342\"}]', 'Both', '153666758144725.png', '', '1', '{\"price\":\"1200\",\"discount\":\"800\",\"cost_price\":\"1200\",\"sale_price\":\"1200\"}', 400, 'Hawks-_123', '[{\"main\":\"1\",\"value\":114}]', 114, 1, 1, '{\"length\":\"12\",\"height\":\"12\",\"breath\":\"12\",\"weight\":\"12\"}', 1, 1, '[\"cash\",\"bank\"]', '[\"732428011490857788324-Hancock-Men-Shirts-3661490857788137-1.jpg\",\"803182111490857788302-Hancock-Men-Shirts-3661490857788137-2.jpg\",\"914986211490857788266-Hancock-Men-Shirts-3661490857788137-4.jpg\",\"\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1536624000', '1536624000'),
                (34, '1', '', '', 'BOMBAY DYEING Pink Floral Flat 104 TC Cotton 1 Double Bedsheet with 2 Pillow Covers', '100% cotton\r\nMachine-wash', 0x3c703e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e53657420636f6e74656e743a266e6273703b3c2f7370616e3e3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f313f7372633d7064223e313c2f613e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e266e6273703b646f75626c65206b696e67266e6273703b3c2f7370616e3e3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f62656473686565743f7372633d7064223e62656473686565743c2f613e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e266e6273703b776974682032266e6273703b3c2f7370616e3e3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f70696c6c6f772d636f766572733f7372633d7064223e70696c6c6f7720636f766572733c2f613e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b22202f3e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e5175616c6974793a20436f617273653c2f7370616e3e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b22202f3e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e54687265616420636f756e743a203130343c2f7370616e3e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b22202f3e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e436f6c6f75723a266e6273703b3c2f7370616e3e3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f70696e6b3f7372633d7064223e50696e6b3c2f613e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b22202f3e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e5061747465726e3a20466c6f72616c20666c61743c2f7370616e3e3c2f703e, 'sada', 3, '[{\"main\":\"material\",\"value\":\"312\"},{\"main\":\"fabric\",\"value\":\"12321\"}]', 'Both', '153675502033296.png', '', '1', '{\"price\":\"1200\",\"discount\":\"649\",\"cost_price\":\"1200\",\"sale_price\":\"649\"}', 551, 'Hawks-_232423', '[{\"main\":\"1\",\"value\":119}]', 119, 1, 1, '{\"length\":\"12\",\"height\":\"12\",\"breath\":\"12\",\"weight\":\"1\"}', 1, 1, '[\"cash\",\"bank\"]', '[\"512569072e70fdc-9477-4f7e-8405-4447b5b7a41c1535015667363-Bombay-Dyeing-Axia-100-Cotton-Double-Bed-Sheet-with-2-Pillow-1.jpg\",\"51745216d63a8e8-3cb1-4758-bde4-7f5d86ff5df61535015667346-Bombay-Dyeing-Axia-100-Cotton-Double-Bed-Sheet-with-2-Pillow-2.jpg\",\"84520525b465cd3-8936-44c8-8589-4554230ff6b11535015667327-Bombay-Dyeing-Axia-100-Cotton-Double-Bed-Sheet-with-2-Pillow-3.jpg\",\"\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1536710400', '1536710400'),
                (35, '1', '', '', 'Teakwood Leathers Men Black Accessory Gift Set', 'This accessory gift set consists of a wallet and a belt', 0x3c646976207374796c653d22626f782d73697a696e673a20696e68657269743b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a206d656469756d3b223e0d0a3c7020636c6173733d227064702d70726f647563742d6465736372697074696f6e2d636f6e74656e7422207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b206c696e652d6865696768743a20312e343b20666f6e742d73697a653a20313570783b206d617267696e2d746f703a20313570783b223e3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f62726f776e3f7372633d7064223e42726f776e3c2f613e266e6273703b3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f6c6561746865723f7372633d7064223e6c6561746865723c2f613e266e6273703b74776f666f6c642077616c6c65743c6272207374796c653d22626f782d73697a696e673a20696e68657269743b22202f3e4f6e65206d61696e20636f6d706172746d656e742077697468206120736570617261746f7220736c656576653c6272207374796c653d22626f782d73697a696e673a20696e68657269743b22202f3e46697665266e6273703b3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f636172642d686f6c646572733f7372633d7064223e6361726420686f6c646572733c2f613e2c2074776f20736c697020706f636b6574732c206f6e65204944266e6273703b3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f636172642d686f6c6465723f7372633d7064223e6361726420686f6c6465723c2f613e2c206f6e6520636f696e20706f636b65742077697468206120666c61703c6272207374796c653d22626f782d73697a696e673a20696e68657269743b22202f3e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b22202f3e42656c743c6272207374796c653d22626f782d73697a696e673a20696e68657269743b22202f3e426c61636b2062656c74266e6273703b3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b22202f3e536563757265642077697468206120736c69646572206275636b6c653c6272207374796c653d22626f782d73697a696e673a20696e68657269743b22202f3e446973636c61696d65723a266e6273703b546869732070726f64756374206973206d6164652066726f6d2067656e75696e65206c656174686572207768696368206769766573206974206120756e6971756520746578747572652e205468697320697320776879207468652070726f6475637420796f752072656365697665206d6179207661727920736c696768746c792066726f6d2074686520696d6167652e3c2f703e0d0a3c2f6469763e0d0a3c646976207374796c653d22626f782d73697a696e673a20696e68657269743b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a206d656469756d3b223e0d0a3c683420636c6173733d227064702d70726f647563742d6465736372697074696f6e2d7469746c6522207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366237393b20666f6e742d73697a653a20313570783b206d617267696e2d746f703a20313570783b206d617267696e2d626f74746f6d3a203070783b20666f6e742d7765696768743a203530303b223e4d6174657269616c2026616d703b20436172653c2f68343e0d0a3c7020636c6173733d227064702d70726f647563742d6465736372697074696f6e2d636f6e74656e7422207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b206c696e652d6865696768743a20312e343b20666f6e742d73697a653a20313570783b206d617267696e2d746f703a20313570783b223e4c6561746865723c6272207374796c653d22626f782d73697a696e673a20696e68657269743b22202f3e576970652077697468206120636c65616e2c2064727920636c6f7468207768656e206e65656465643c2f703e0d0a3c2f6469763e, 'asda', 3, '[{\"main\":\"material\",\"value\":\"23\"},{\"main\":\"fabric\",\"value\":\"234\"}]', 'Both', '153675516795758.png', '', '1', '{\"price\":\"650\",\"discount\":0,\"cost_price\":\"650\",\"sale_price\":\"650\"}', 650, 'Hawks-_234df', '[{\"main\":\"1\",\"value\":119}]', 119, 1, 1, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 1, '[\"cash\",\"bank\"]', '[\"696793011495789466126-Teakwood-Leathers-Men-Black-Accessory-Gift-Set-1961495789465940-1.jpg\",\"285600111495789466112-Teakwood-Leathers-Men-Black-Accessory-Gift-Set-1961495789465940-2.jpg\",\"870910211495789466100-Teakwood-Leathers-Men-Black-Accessory-Gift-Set-1961495789465940-3.jpg\",\"713751311495789466088-Teakwood-Leathers-Men-Black-Accessory-Gift-Set-1961495789465940-4.jpg\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1536710400', '1536710400'),
                (32, '1,2,4', '5', '11', 'HRX by Hrithik Roshan Men Blue Running Shoes', 'Textile\r\nWipe with a clean, dry cloth to remove dust', 0x3c703e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e412070616972206f6620626c75652026616d703b266e6273703b3c2f7370616e3e3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f677265793f7372633d7064223e677265793c2f613e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e266e6273703b3c2f7370616e3e3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f72756e6e696e672d73706f7274732d73686f65733f7372633d7064223e72756e6e696e672073706f7274732073686f65733c2f613e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e2c2068617320726567756c6172207374796c696e672c206c6163652d75702064657461696c3c2f7370616e3e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b22202f3e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e54657874696c652075707065723c2f7370616e3e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b22202f3e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e43757368696f6e656420666f6f746265643c2f7370616e3e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b22202f3e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e546578747572656420616e64207061747465726e6564206f7574736f6c653c2f7370616e3e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b22202f3e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e57617272616e74793a2033206d6f6e7468733c2f7370616e3e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b22202f3e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e57617272616e74792070726f7669646564206279206272616e642f6d616e7566616374757265723c2f7370616e3e3c2f703e, 'HRX by Hrithik Roshan Men Blue Running Shoes', 1, '[{\"main\":\"type\",\"value\":\"12\"},{\"main\":\"warranty\",\"value\":\"12\"}]', 'Both', '153621516719843.png', '', '4,2,3,5', '{\"price\":\"2999\",\"discount\":\"1799\",\"cost_price\":\"1799\",\"sale_price\":\"1799\"}', 1200, 'Hawks-_1233', '[{\"main\":\"4\",\"value\":\"50\"},{\"main\":\"2\",\"value\":\"500\"},{\"main\":\"3\",\"value\":\"500\"},{\"main\":\"5\",\"value\":\"500\"}]', 0, 1, 10, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 1, '[\"cash\",\"bank\"]', '[\"4677850b5784a85-da50-48fb-b150-df04b22895881533531086537-HRX-by-Hrithik-Roshan-Men-Navy-Blue-Running-Shoes-5201533531-5.jpg\",\"8367871b97d3fb2-8efc-485a-9454-a674122ae4931533531086670-HRX-by-Hrithik-Roshan-Men-Navy-Blue-Running-Shoes-5201533531-1.jpg\",\"1846382727c7fc6-3ec4-495f-9bfd-185b869e763f1533531086610-HRX-by-Hrithik-Roshan-Men-Navy-Blue-Running-Shoes-5201533531-3.jpg\",\"49534638e4f12d6-b015-40a9-8c53-509397c357921533531086639-HRX-by-Hrithik-Roshan-Men-Navy-Blue-Running-Shoes-5201533531-2.jpg\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1536192000', '1536192000'),
                (36, '1', '', '', 'Voylla Silver-Toned Contemporary Jhumkas', 'Silver-toned contemporary jhumkas\r\nNo warranty\r\nWarranty provided by brand/manufacturer', 0x3c703e3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f73696c7665723f7372633d7064223e53696c7665723c2f613e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e2d746f6e656420636f6e74656d706f72617279266e6273703b3c2f7370616e3e3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f6a68756d6b61733f7372633d7064223e6a68756d6b61733c2f613e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b22202f3e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e4e6f2077617272616e74793c2f7370616e3e3c6272207374796c653d22626f782d73697a696e673a20696e68657269743b20636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b22202f3e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e57617272616e74792070726f7669646564206279266e6273703b3c2f7370616e3e3c6120636c6173733d2273656f6c696e6b22207374796c653d22626f782d73697a696e673a20696e68657269743b206261636b67726f756e642d636f6c6f723a207472616e73706172656e743b20746578742d6465636f726174696f6e2d6c696e653a206e6f6e653b20636f6c6f723a20233238326333663b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b2220687265663d2268747470733a2f2f7777772e6d796e7472612e636f6d2f6272616e643f7372633d7064223e6272616e643c2f613e3c7370616e207374796c653d22636f6c6f723a20233639366537393b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a20313570783b223e2f6d616e7566616374757265723c2f7370616e3e3c2f703e, 'sdfs', 1, '[{\"main\":\"type\",\"value\":\"sdf\"},{\"main\":\"warranty\",\"value\":\"sdf\"}]', 'Both', '153675526929701.png', '', '1', '{\"price\":\"1200\",\"discount\":\"599\",\"cost_price\":\"1200\",\"sale_price\":\"1200\"}', 601, 'Hawks-_dgfdg', '[{\"main\":\"1\",\"value\":\"0\"}]', 0, 1, 1, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 1, '[\"cash\",\"bank\"]', '[\"604352011522385772895-Voylla-Silver-Toned-Contemporary-Jhumkas-5841522385772752-1.jpg\",\"668756111522385772878-Voylla-Silver-Toned-Contemporary-Jhumkas-5841522385772752-2.jpg\",\"370325211522385772859-Voylla-Silver-Toned-Contemporary-Jhumkas-5841522385772752-3.jpg\",\"991091311522385772831-Voylla-Silver-Toned-Contemporary-Jhumkas-5841522385772752-4.jpg\",\"\",\"\",\"\",\"\",\"\"]', 1, '', '1536710400', '1536710400'),
                (37, '1', '1', '', 'Black Audio Curve Neckband Wireless Bluetooth Magnetic Earphone with Mic', 'Best Price: Rs 1260\r\nApplicable on: Orders above Rs. 1499 (only on first purchase)\r\nCoupon code: MYNTRANEWW500\r\nCoupon Discount: Rs. 500 off, excluding tax (check cart for final savings)', 0x3c703e3c7370616e207374796c653d22636f6c6f723a20233238326333663b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a206d656469756d3b223e426573742050726963653a3c2f7370616e3e3c7370616e207374796c653d22636f6c6f723a20233238326333663b20666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a206d656469756d3b223e266e6273703b3c2f7370616e3e3c7370616e20636c6173733d227064702d6f66666572732d707269636522207374796c653d22666f6e742d66616d696c793a20576869746e65793b20666f6e742d73697a653a206d656469756d3b20626f782d73697a696e673a20696e68657269743b20636f6c6f723a20236666393035613b223e5273266e6273703b313236303c2f7370616e3e3c2f703e0d0a3c756c20636c6173733d227064702d6f66666572732d6f666665724465736322207374796c653d22626f782d73697a696e673a20696e68657269743b206c6973742d7374796c653a206e6f6e653b20666f6e742d73697a653a206d656469756d3b20636f6c6f723a20233238326333663b2070616464696e673a203070783b206d617267696e3a2038707820307078203070783b2077696474683a203631322e31373270783b20666f6e742d66616d696c793a20576869746e65793b223e0d0a3c6c69207374796c653d22626f782d73697a696e673a20696e68657269743b206d617267696e2d626f74746f6d3a203670783b223e0d0a3c64697620636c6173733d227064702d6f66666572732d62756c6c657422207374796c653d22626f782d73697a696e673a20696e68657269743b2077696474683a203470783b206865696768743a203470783b20626f726465722d7261646975733a203470783b206261636b67726f756e642d636f6c6f723a20233238326333663b206d617267696e2d72696768743a20313070783b206d617267696e2d626f74746f6d3a203270783b20646973706c61793a20696e6c696e652d626c6f636b3b20766572746963616c2d616c69676e3a206d6964646c653b223e266e6273703b3c2f6469763e0d0a3c64697620636c6173733d227064702d6f66666572732d6c6162656c4d61726b757022207374796c653d22626f782d73697a696e673a20696e68657269743b20646973706c61793a20696e6c696e652d626c6f636b3b20766572746963616c2d616c69676e3a20746f703b2077696474683a203535302e39353370783b223e4170706c696361626c65206f6e3a266e6273703b3c7370616e20636c6173733d2222207374796c653d22626f782d73697a696e673a20696e68657269743b223e4f72646572732061626f76652052732e203134393920286f6e6c79206f6e206669727374207075726368617365293c2f7370616e3e3c2f6469763e0d0a3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20696e68657269743b206d617267696e2d626f74746f6d3a203670783b223e0d0a3c64697620636c6173733d227064702d6f66666572732d62756c6c657422207374796c653d22626f782d73697a696e673a20696e68657269743b2077696474683a203470783b206865696768743a203470783b20626f726465722d7261646975733a203470783b206261636b67726f756e642d636f6c6f723a20233238326333663b206d617267696e2d72696768743a20313070783b206d617267696e2d626f74746f6d3a203270783b20646973706c61793a20696e6c696e652d626c6f636b3b20766572746963616c2d616c69676e3a206d6964646c653b223e266e6273703b3c2f6469763e0d0a3c64697620636c6173733d227064702d6f66666572732d6c6162656c4d61726b757022207374796c653d22626f782d73697a696e673a20696e68657269743b20646973706c61793a20696e6c696e652d626c6f636b3b20766572746963616c2d616c69676e3a20746f703b2077696474683a203535302e39353370783b223e436f75706f6e20636f64653a266e6273703b3c7370616e20636c6173733d227064702d6f66666572732d626f6c645465787422207374796c653d22626f782d73697a696e673a20696e68657269743b223e4d594e5452414e4557573530303c2f7370616e3e3c2f6469763e0d0a3c2f6c693e0d0a3c6c69207374796c653d22626f782d73697a696e673a20696e68657269743b206d617267696e2d626f74746f6d3a203670783b223e0d0a3c64697620636c6173733d227064702d6f66666572732d62756c6c657422207374796c653d22626f782d73697a696e673a20696e68657269743b2077696474683a203470783b206865696768743a203470783b20626f726465722d7261646975733a203470783b206261636b67726f756e642d636f6c6f723a20233238326333663b206d617267696e2d72696768743a20313070783b206d617267696e2d626f74746f6d3a203270783b20646973706c61793a20696e6c696e652d626c6f636b3b20766572746963616c2d616c69676e3a206d6964646c653b223e266e6273703b3c2f6469763e0d0a3c64697620636c6173733d227064702d6f66666572732d6c6162656c4d61726b757022207374796c653d22626f782d73697a696e673a20696e68657269743b20646973706c61793a20696e6c696e652d626c6f636b3b20766572746963616c2d616c69676e3a20746f703b2077696474683a203535302e39353370783b223e436f75706f6e20446973636f756e743a266e6273703b3c7370616e20636c6173733d2222207374796c653d22626f782d73697a696e673a20696e68657269743b223e52732e20353030206f66662c206578636c7564696e67207461782028636865636b206361727420666f722066696e616c20736176696e6773293c2f7370616e3e3c2f6469763e0d0a3c2f6c693e0d0a3c2f756c3e, 'tets', 3, '[{\"main\":\"material\",\"value\":\"1\"},{\"main\":\"fabric\",\"value\":\"1\"}]', 'Both', '153819560545735.png', '', '1', '{\"price\":\"4499\",\"discount\":\"1200\",\"cost_price\":\"1000\",\"tax_price\":0,\"sale_price\":\"1200\"}', 3299, 'Hawks-_123', '[{\"main\":\"1\",\"value\":1198}]', 1198, 1, 10, '{\"length\":\"\",\"height\":\"\",\"breath\":\"\",\"weight\":\"\"}', 1, 1, '[\"cash\",\"bank\"]', '[\"4255190d38820e9-1e03-4eb6-a7d4-ea74a27330ee1531227179937-Boult-Black-Audio-Curve-Neckband-Wireless-Bluetooth-Magnetic-Earphone-with-Mic-5861531227179813-5.jpg\",\"5604351cd9c59a5-af76-4c06-83b7-b5e1378a40f31537439266709-Boult-Black-Audio-Curve-Neckband-Wireless-Bluetooth-Magnetic-1.jpg\",\"739229278294ff2-7ee2-4afb-90cd-0e76cd3c72ba1531227180013-Boult-Black-Audio-Curve-Neckband-Wireless-Bluetooth-Magnetic-Earphone-with-Mic-5861531227179813-2.jpg\",\"7920033791c631a-5e81-4698-9296-33eea39cc8291531227179988-Boult-Black-Audio-Curve-Neckband-Wireless-Bluetooth-Magnetic-Earphone-with-Mic-5861531227179813-3.jpg\",\"73629046baa7207-ec8a-4ff8-a920-094c22e032271531227179963-Boult-Black-Audio-Curve-Neckband-Wireless-Bluetooth-Magnetic-Earphone-with-Mic-5861531227179813-4.jpg\",\"\",\"\",\"\",\"\"]', 1, '', '1538179200', '1538179200')");

                // Table structure for table   houdinv_possetting   

                $getDB->query("CREATE TABLE houdinv_products_variants (
                    houdin_products_variants_id int(11) NOT NULL,
                    houdin_products_variants_new varchar(255) NOT NULL,
                    houdin_products_variants_title varchar(255) NOT NULL,
                    houdin_products_variants_prices varchar(255) NOT NULL,
                    houdinv_products_variants_final_price float DEFAULT '0',
                    houdin_products_variants_sku varchar(100) NOT NULL,
                    houdin_products_variants_stock text NOT NULL,
                    houdinv_products_variants_total_stocks int(11) NOT NULL DEFAULT '0',
                    houdin_products_variants_image varchar(100) DEFAULT NULL,
                    houdin_products_variants_default int(1) NOT NULL,
                    houdin_products_variants_product_id int(11) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                  // Dumping data for table houdinv_products_variants

                $getDB->query("INSERT INTO houdinv_products_variants (houdin_products_variants_id, houdin_products_variants_new, houdin_products_variants_title, houdin_products_variants_prices, houdinv_products_variants_final_price, houdin_products_variants_sku, houdin_products_variants_stock, houdinv_products_variants_total_stocks, houdin_products_variants_image, houdin_products_variants_default, houdin_products_variants_product_id) VALUES
                (6, '[{\"variant_name\":\"fg\",\"variant_value\":\"asf\"}]', 'Sanyo 100.95 cm (43 inches) XT-43S8100FS Full HD Ips Smart LED TV (Blue)', '80', 12, '000021', '[{\"main\":\"4\",\"value\":1},{\"main\":\"2\",\"value\":728},{\"main\":\"3\",\"value\":704},{\"main\":\"5\",\"value\":704}]', 143, '604352011522385772895-Voylla-Silver-Toned-Contemporary-Jhumkas-5841522385772752-1.jpg', 0, 1),
                (7, '[{\"variant_name\":\"fg\",\"variant_value\":\"red\"}]', 'Sanyo 107.95 cm (43 inches) XT-43S8100FS Full HD Ips Smart LED TV (Black)', '90', 12, '00002', '[{\"main\":\"4\",\"value\":2},{\"main\":\"2\",\"value\":704},{\"main\":\"3\",\"value\":704}, {\"main\":\"5\",\"value\":704}]', 100, '604352011522385772895-Voylla-Silver-Toned-Contemporary-Jhumkas-5841522385772752-1.jpg', 0, 2),
                (8, '[{\"variant_name\":\"fg\",\"variant_value\":\"black\"}]', 'Sanyo 107.95 cm (43 inches) XT-43S8100FS Full HD Ips Smart LED TV (Black)', '90', 12, '00002', '[{\"main\":\"4\",\"value\":2},{\"main\":\"2\",\"value\":704},{\"main\":\"3\",\"value\":704}, {\"main\":\"5\",\"value\":704}]', 100, '604352011522385772895-Voylla-Silver-Toned-Contemporary-Jhumkas-5841522385772752-1.jpg', 0, 2),
                (9, '[{\"variant_name\":\"fg\",\"variant_value\":\"blue\"}]', 'Sanyo 107.95 cm (43 inches) XT-43S8100FS Full HD Ips Smart LED TV (Black)', '90', 12, '00002', '[{\"main\":\"4\",\"value\":2},{\"main\":\"2\",\"value\":704},{\"main\":\"3\",\"value\":704}, {\"main\":\"5\",\"value\":704}]', 100, '604352011522385772895-Voylla-Silver-Toned-Contemporary-Jhumkas-5841522385772752-1.jpg', 0, 3)");

                // Table structure for table   houdinv_productType   

                $getDB->query("CREATE TABLE houdinv_productType (
                    houdinv_productType_id int(11) NOT NULL,
                    houdinv_productType_image varchar(255) NOT NULL,
                    houdinv_productType_name varchar(150) NOT NULL,
                    houdinv_productType_description longtext NOT NULL,
                    houdinv_productType_subattr longblob NOT NULL,
                    houdinv_productType_status enum('active','deactive','block','') NOT NULL,
                    houdinv_productType_created_date varchar(255) NOT NULL,
                    houdinv_productType_update_date varchar(255) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_productType

                $getDB->query("INSERT INTO houdinv_productType (houdinv_productType_id, houdinv_productType_image, houdinv_productType_name, houdinv_productType_description, houdinv_productType_subattr, houdinv_productType_status, houdinv_productType_created_date, houdinv_productType_update_date) VALUES
                (1, '114756C_Users_admin_AppData_Local_Packages_Microsoft_SkypeApp_kzf8qxf38zg5c_LocalState_b5ccdc7e-6723-4be6-83c2-3ad2ed75e292.png', 'test', 'newwwwwwwwwwwwwwwwwww', 0x5b2274797065222c2277617272616e7479225d, 'active', '1529316365', '1529326405'),
                (3, '794475aurora-2069242__480-1.jpg', 'second', 'new', 0x5b226d6174657269616c222c22666162726963225d, 'active', '1529326469', '1529326469')");

                // Table structure for table   houdinv_product_reviews   

                $getDB->query("CREATE TABLE houdinv_product_reviews (
                    houdinv_product_review_id int(10) UNSIGNED NOT NULL,
                    houdinv_product_review_user_id int(10) DEFAULT '0',
                    houdinv_product_review_user_email varchar(255) NOT NULL,
                    houdinv_product_review_user_name varchar(255) NOT NULL,
                    houdinv_product_review_product_id int(10) DEFAULT '0',
                    houdinv_product_reviews_variant_id int(11) DEFAULT '0',
                    houdinv_product_review_message text NOT NULL,
                    houdinv_product_review_rating int(11) DEFAULT NULL,
                    houdinv_product_review_created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_product_reviews

                $getDB->query("INSERT INTO houdinv_product_reviews (houdinv_product_review_id, houdinv_product_review_user_id, houdinv_product_review_user_email, houdinv_product_review_user_name, houdinv_product_review_product_id, houdinv_product_reviews_variant_id, houdinv_product_review_message, houdinv_product_review_rating, houdinv_product_review_created_at) VALUES
                (1, 2, 'ha@gmail.com', 'shivam', 1, 0, 'trgyhujkl', 5, '2018-08-22 10:55:23'),
                (2, NULL, 'sfs@gmail.com', 'fdsaf', 0, 6, 'fghjk', 5, '2018-08-22 12:38:54'),
                (3, 2, 'sdfs@gmail.com', 'sdfs', 0, 6, 'ead', 3, '2018-08-22 12:51:55'),
                (4, 42, 'shivam199420@gmail.com', 'shivam', 22, 0, 'Best quality', 5, '2018-08-23 10:44:43'),
                (5, 0, 'shivam199420@gmail.com', 'shivam', 9, 0, 'ksdnfks', 5, '2018-09-11 10:33:08'),
                (7, 42, 'shivam199420@gmail.com', 'shivam', 33, 0, 'guest review', 5, '2018-09-12 06:08:11'),
                (8, 42, 'sdfs@gmail.com', 'shivam', 33, 0, 'new review\r\n', 4, '2018-09-12 06:08:33'),
                (9, 42, 'shivam199420@gmail.com', 'shiva,m', 0, 7, 'sdkjfsd', 3, '2018-09-12 06:25:56'),
                (10, 42, 'shivam199420@gmail.com', 'shivam', 34, 0, 'skdnfjksdnf', 5, '2018-09-13 12:54:54'),
                (11, 42, 'shivam199420@gmail.com', 'shvam', 0, 0, 'sdnfkjsd', 5, '2018-09-13 12:59:31'),
                (12, 42, 'Test@gmail.com', 'test', 0, 0, 'jksdfs', 5, '2018-09-13 13:07:14'),
                (13, 42, 'var@gmail.cpom', 'variant', 0, 7, 'varaint', 5, '2018-09-13 13:12:14'),
                (14, 42, 'hello@gmail.com', 'hello', 35, 0, 'kjsdbnjkfnsdjk', 5, '2018-09-13 13:15:41'),
                (15, 42, 'shivam199420@gmail.com', 'shivam', 34, 0, 'skdnfkjs', 0, '2018-09-14 06:40:14'),
                (16, 42, 'sdf@gmail.com', 'dfd', 2, 0, 'jsdbfs', 5, '2018-09-14 07:13:12'),
                (17, 0, 'shivam199420@gmail.com', 'shivam', 35, 0, 'jksndfjknsdjkf', 5, '2018-09-15 11:44:30'),
                (18, 2, 'test@gmail.com', 'test', 36, 0, 'lksdnfkls', 5, '2018-09-15 11:49:29'),
                (19, 2, 'shivam199420@gmail.com', 'shiva,', 36, 0, 'lksdnfkjfs', 3, '2018-09-15 11:49:51'),
                (20, 0, 'shivam199420@gmail.com', 'fdv', 0, 0, 'sdsa', 0, '2018-09-15 12:32:54'),
                (21, 2, 'bh@gmail.com', 'khj', 165, 0, 'hl', 5, '2018-09-17 10:13:28'),
                (22, 2, 'bh@gmail.com', 'h', 4, 0, 'h', 5, '2018-09-17 11:24:39'),
                (23, 2, 'bh@gmail.com', 'khj', 4, 0, 'b', 5, '2018-09-17 11:24:57'),
                (24, 2, 'bh@gmail.com', 'ntt', 4, 0, 'hg', 5, '2018-09-17 11:25:20'),
                (25, 2, 'bh@gmail.com', 'h', 8, 0, 'trht', 4, '2018-09-17 13:56:37'),
                (26, 2, 'bh@gmail.com', 'hk', 8, 0, ',khjk', 5, '2018-09-17 13:57:02'),
                (27, 42, 'jbdj@gmail.com', 'xvxcx', 35, 0, 'xcv', 5, '2018-09-19 10:50:55'),
                (28, 42, 'Test@gmail.com', 'test', 35, 0, 'sfsadf', 3, '2018-09-19 10:58:50'),
                (29, 42, 'sdfs@gmail.com', 'sdfsa', 0, 8, 'asdsadas', 5, '2018-09-19 11:06:55'),
                (30, 42, 'Test@gmail.com', 'test', 33, 0, 'skldnfks', 5, '2018-09-19 13:20:18'),
                (31, 42, 'sdfs@gmail.com', 'sdfs', 0, 8, 'sdfsf', 5, '2018-09-19 13:24:04'),
                (32, 25, 'aliquet.vel.vulputate@tellus.org', 'suraj', 1, NULL, 'best product', 4, '2018-09-26 09:52:14'),
                (33, 25, 'aliquet.vel.vulputate@tellus.org', 'suraj', 1, 0, 'best product', 4, '2018-09-26 09:52:32'),
                (34, 76, 'reema@gmail.com', 'iijj hjjj', 1, 0, 'gfdgh', 0, '2018-09-26 10:05:44'),
                (35, 25, 'aliquet.vel.vulputate@tellus.org', 'suraj', 1, 0, 'best product', 4, '2018-09-26 10:40:22'),
                (36, 25, 'aliquet.vel.vulputate@tellus.org', 'suraj', 1, 0, 'best product', 4, '2018-09-26 10:40:31'),
                (37, 76, 'reema@gmail.com', 'iijj hjjj', 1, 0, 'hgh', 1, '2018-09-26 10:44:22'),
                (38, 44, 'goyalpayal1995@gmail.com', 'kbcvjjj', 36, 0, 'dgrhrh', 3, '2018-09-26 10:59:42'),
                (39, 44, 'goyalpayal1995@gmail.com', 'kbcvjjj', 36, 0, 'dgrhrh', 3, '2018-09-26 10:59:48'),
                (40, 44, 'goyalpayal1995@gmail.com', 'kbcvjjj', 36, 0, 'dgrhrh', 3, '2018-09-26 10:59:57'),
                (41, 44, 'goyalpayal1995@gmail.com', 'kbcvjjj', 36, 0, 'dgrhrh', 3, '2018-09-26 11:01:25'),
                (42, 44, 'goyalpayal1995@gmail.com', 'kbcvjjj', 32, 0, 'fyfy', 2, '2018-09-26 11:04:41'),
                (43, 44, 'goyalpayal1995@gmail.com', 'kbcvjjj', 36, 0, 'bhsj', 3, '2018-09-26 11:07:26'),
                (44, 44, 'goyalpayal1995@gmail.com', 'kbcvjjj', 36, 0, 'hjj', 2, '2018-09-26 11:08:38'),
                (45, 44, 'goyalpayal1995@gmail.com', 'kbcvjjj', 32, 0, 'gdhd', 2, '2018-09-26 11:16:24'),
                (47, 42, 'shivam199420@gmail.com', 'shivam sengar', 36, 0, '<UITextView: 0x12a89e800; frame = (8 186.5; 359 118); text = \'Good product at low cost ...\'; clipsToBounds = YES; autoresize = RM+BM; gestureRecognizers = <NSArray: 0x1c44468a0>; layer = <CALayer: 0x1c422df40>; contentOffset: {0, 0}; contentSize: {359, 50}; adjustedContentInset: {0, 0, 0, 0}>', 0, '2018-09-28 12:31:02'),
                (48, 42, 'shivam199420@gmail.com', 'shivam sengar', 36, 0, 'Good product at low cost', 0, '2018-09-28 12:42:31'),
                (49, 42, 'shivam199420@gmail.com', 'shivam sengar', 36, 0, 'Amazing product ', 3, '2018-09-28 14:15:15'),
                (50, 2, 'Chris@gmail.com', 'Test', 36, 0, 'Test', 2, '2018-09-28 09:36:34'),
                (51, 2, 'Chris@gmail.com', 'wr', 36, 0, 'wras', 2, '2018-09-28 09:36:54'),
                (52, 79, 'Chris@gmail.com', 'Arpit', 36, 0, 'Testing', 2, '2018-09-28 13:55:03'),
                (53, 2, 'arpit-tiwari@hotmail.com', 'show', 36, NULL, 'Site is ok', 0, '2018-09-29 07:38:39'),
                (54, 2, 'Chris@gmail.com', 'Arpit', 2, 0, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 3, '2018-09-29 10:46:27'),
                (55, 2, 'arpit-tiwari@hotmail.com', 'Arpit', 37, 0, 'Testing Review', 5, '2018-09-29 12:07:41'),
                (56, 2, 'asjdh@gami.com', 'Shoiw', 37, 0, 'sdijf', 2, '2018-09-29 12:27:22'),
                (57, 2, 'surbhisethi04@gmail.com', 'Surbhis', 37, 0, 'Tets', 1, '2018-09-29 12:30:31'),
                (58, 2, 'surbhisethi04@gmail.com', 'Arpit Tiwario', 36, 0, 'Hello Tester', 1, '2018-09-29 13:28:04'),
                (59, 2, 'asjdh@gami.com', 'Hsaihsod', 32, 0, 'sdh\r\n', 3, '2018-09-29 13:44:34'),
                (60, 2, 'Chris@gmail.com', 'Test ', 2, 0, 'Testing completed', 2, '2018-10-01 06:42:51'),
                (61, 2, 'Chris@gmail.com', 'asd', 2, 0, '1 Star Given', 1, '2018-10-01 06:43:21'),
                (62, 42, 'surbhisethi@gmail.com', 'surbhi', 2, 0, 'fdvkdmvkfmkbmflb,  bgbkmfb', 0, '2018-10-01 09:47:11'),
                (63, 2, 'deepsharma906@gmail.com', 'asd', 35, 0, 'aw', 2, '2018-10-01 10:50:49'),
                (64, 2, 'Chris@gmail.com', 'shivam', 37, 0, 'Test', 2, '2018-10-01 13:03:15'),
                (65, 2, 'ew@gmail.co', 'sdff', 2, 0, 'sdjf', 3, '2018-10-01 13:43:30')");

                // Table structure for table   houdinv_product_types   

                $getDB->query("CREATE TABLE houdinv_product_types (
                    houdinv_product_type_id int(10) UNSIGNED NOT NULL,
                    houdinv_product_type_name varchar(150) NOT NULL,
                    houdinv_product_type_description tinytext,
                    houdinv_product_type_created_at timestamp NULL DEFAULT NULL,
                    houdinv_product_type_updated_at timestamp NULL DEFAULT NULL
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

                  // Table structure for table   houdinv_purchase_products   

                $getDB->query("CREATE TABLE houdinv_purchase_products (
                    houdinv_purchase_products_id int(11) NOT NULL,
                    houdinv_purchase_products_purchase_id int(11) NOT NULL,
                    houdinv_purchase_products_product_id int(11) NOT NULL,
                    houdinv_purchase_products_product_variant_id int(11) NOT NULL,
                    houdinv_purchase_products_quantity int(11) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                  // Dumping data for table houdinv_purchase_products

                $getDB->query("INSERT INTO houdinv_purchase_products (houdinv_purchase_products_id, houdinv_purchase_products_purchase_id, houdinv_purchase_products_product_id, houdinv_purchase_products_product_variant_id, houdinv_purchase_products_quantity) VALUES
                (1, 1, 23, 0, 10),
                (2, 1, 0, 10, 10),
                (3, 1, 0, 11, 10),
                (4, 2, 1, 0, 10),
                (5, 2, 0, 6, 10),
                (6, 2, 21, 0, 10),
                (7, 3, 1, 0, 2)");

                // Table structure for table   houdinv_purchase_products_inward   

                $getDB->query("CREATE TABLE houdinv_purchase_products_inward (
                    houdinv_purchase_products_inward_id int(11) NOT NULL,
                    houdinv_purchase_products_inward_purchase_product_id int(11) NOT NULL,
                    houdinv_purchase_products_inward_variant_id int(11) NOT NULL DEFAULT '0',
                    houdinv_purchase_products_inward_purchase_id int(11) NOT NULL DEFAULT '0',
                    houdinv_purchase_products_inward_quantity_recieved int(11) NOT NULL,
                    houdinv_purchase_products_inward_quantity_return int(11) DEFAULT '0',
                    houdinv_purchase_products_inward_sale_price int(11) NOT NULL,
                    houdinv_purchase_products_inward_cost_price int(11) NOT NULL,
                    houdinv_purchase_products_inward_retail_price int(11) NOT NULL,
                    houdinv_purchase_products_inward_total_amount int(11) NOT NULL,
                    houdinv_purchase_products_inward_payment_status enum('paid','not paid') NOT NULL,
                    houdinv_purchase_products_inward_amount_paid int(11) NOT NULL,
                    houdinv_purchase_products_inward_outlet_id int(11) NOT NULL,
                    houdinv_purchase_products_inward_craeted_at varchar(30) NOT NULL,
                    houdinv_purchase_products_inward_modified_at varchar(30) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_purchase_products

                $getDB->query("INSERT INTO houdinv_purchase_products_inward (houdinv_purchase_products_inward_id, houdinv_purchase_products_inward_purchase_product_id, houdinv_purchase_products_inward_variant_id, houdinv_purchase_products_inward_purchase_id, houdinv_purchase_products_inward_quantity_recieved, houdinv_purchase_products_inward_quantity_return, houdinv_purchase_products_inward_sale_price, houdinv_purchase_products_inward_cost_price, houdinv_purchase_products_inward_retail_price, houdinv_purchase_products_inward_total_amount, houdinv_purchase_products_inward_payment_status, houdinv_purchase_products_inward_amount_paid, houdinv_purchase_products_inward_outlet_id, houdinv_purchase_products_inward_craeted_at, houdinv_purchase_products_inward_modified_at) VALUES
                (1, 23, 0, 1, 10, 0, 120, 10, 10, 100, 'paid', 100, 4, '1535500800', '1535500800'),
                (2, 0, 10, 1, 10, 0, 10, 10, 10, 100, 'paid', 100, 4, '1535500800', '1535500800'),
                (3, 0, 11, 1, 10, 0, 10, 10, 10, 100, 'paid', 100, 4, '1535500800', '1535500800'),
                (4, 1, 0, 2, 10, 0, 10, 10, 10, 100, 'paid', 30, 2, '1535500800', '1535500800'),
                (5, 0, 6, 2, 8, 2, 80, 80, 80, 640, 'paid', 140, 2, '1535500800', '1535500800'),
                (6, 21, 0, 2, 10, 0, 10, 10, 10, 100, 'paid', 100, 3, '1535500800', '1535500800')");

                // Table structure for table   houdinv_push_Template   

                $getDB->query("CREATE TABLE houdinv_push_Template (
                    houdinv_push_template_id int(11) NOT NULL,
                    houdinv_push_template_type varchar(50) NOT NULL,
                    houdinv_push_template_subject varchar(255) NOT NULL,
                    houdinv_push_template_message text NOT NULL,
                    houdinv_push_template_created_date varchar(100) NOT NULL,
                    houdinv_push_template_updated_date varchar(100) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                  // Dumping data for table houdinv_push_Template

                $getDB->query("INSERT INTO houdinv_push_Template (houdinv_push_template_id, houdinv_push_template_type, houdinv_push_template_subject, houdinv_push_template_message, houdinv_push_template_created_date, houdinv_push_template_updated_date) VALUES
                (1, 'register', '1', '11', '2018-07-27, 12:59:05', '2018-07-27, 12:59:05'),
                (2, 'forget_password', '2', '22', '1532653430', '1532653430'),
                (3, 'On_order', '3', '33', '1532653442', '1532653442'),
                (4, 'On_gift', '7', '7', '153265355', '1532653455')");

                // Table structure for table   houdinv_shipping_charges   

                $getDB->query("CREATE TABLE houdinv_shipping_charges (
                    houdinv_shipping_charge_id int(11) NOT NULL,
                    houdinv_shipping_charge_country varchar(150) DEFAULT NULL,
                    houdinv_shipping_charge_cost float DEFAULT NULL,
                    houdinv_shipping_charge_created_at timestamp NULL DEFAULT NULL,
                    houdinv_shipping_charge_updated_at timestamp NULL DEFAULT NULL
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

                  // Table structure for table   houdinv_shipping_credentials   

                $getDB->query("CREATE TABLE houdinv_shipping_credentials (
                    houdinv_shipping_credentials_id int(11) NOT NULL,
                    houdinv_shipping_credentials_key tinytext NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1 ");

                // Dumping data for table houdinv_shipping_credentials

                $getDB->query("INSERT INTO houdinv_shipping_credentials (houdinv_shipping_credentials_id, houdinv_shipping_credentials_key) VALUES
                (1, '26593907-0a55-4b55-a719-12bf280e6062')");

                // Table structure for table   houdinv_shipping_data   

                $getDB->query("CREATE TABLE houdinv_shipping_data (
                    houdinv_shipping_data_id int(11) NOT NULL,
                    houdinv_shipping_data_first_name varchar(100) NOT NULL,
                    houdinv_shipping_data_last_name varchar(100) NOT NULL,
                    houdinv_shipping_data_company_name varchar(100) NOT NULL,
                    houdinv_shipping_data_email varchar(100) NOT NULL,
                    houdinv_shipping_data_phone_no varchar(100) NOT NULL,
                    houdinv_shipping_data_country varchar(100) NOT NULL,
                    houdinv_shipping_data_address text NOT NULL,
                    houdinv_shipping_data_zip varchar(12) NOT NULL,
                    houdinv_shipping_data_city varchar(100) NOT NULL,
                    houdinv_shipping_data_user_id int(11) NOT NULL,
                    houdinv_shipping_data_order_note text NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_shipping_data

                $getDB->query("INSERT INTO houdinv_shipping_data (houdinv_shipping_data_id, houdinv_shipping_data_first_name, houdinv_shipping_data_last_name, houdinv_shipping_data_company_name, houdinv_shipping_data_email, houdinv_shipping_data_phone_no, houdinv_shipping_data_country, houdinv_shipping_data_address, houdinv_shipping_data_zip, houdinv_shipping_data_city, houdinv_shipping_data_user_id, houdinv_shipping_data_order_note) VALUES
                (1, 'ghhjghj', 'hjgjhgh', 'vbnvhjgvjhg', 'hs.94.1996@gmail.com', '8947086785', 'canada', 'jhj', '302033', 'jaipur', 2, 'hjh'),
                (2, 'payal', '', '', 'hawkscodeteam@gmail.com', '9785940038', 'canada', 'gfgdg', '302001', 'Bharatpur', 2, ''),
                (3, 'ghhjghj', 'hjgjhgh', 'vbnvhjgvjhg', 'hs.94.1996@gmail.com', '121231', 'canada', 'hjgjhgh', '302033', 'jaipur', 2, 'wer')");

                // Table structure for table   houdinv_shipping_ruels   

                $getDB->query("CREATE TABLE houdinv_shipping_ruels (
                    id int(250) NOT NULL,
                    shipping_min_value varchar(250) NOT NULL,
                    shipping_charge varchar(250) NOT NULL,
                    shiping_active int(250) DEFAULT '0',
                    date_time varchar(250) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_shipping_ruels

                $getDB->query("INSERT INTO houdinv_shipping_ruels (id, shipping_min_value, shipping_charge, shiping_active, date_time) VALUES
                (1, '12456', '456', 1, '1531213669')");

                // Table structure for table   houdinv_shipping_warehouse   

                $getDB->query("CREATE TABLE houdinv_shipping_warehouse (
                    id int(250) NOT NULL,
                    w_name varchar(250) NOT NULL,
                    w_pickup_name varchar(250) NOT NULL,
                    w_pickup_phone varchar(250) NOT NULL,
                    w_warehouse int(11) NOT NULL,
                    w_pickup_country varchar(250) DEFAULT NULL,
                    w_pickup_state varchar(250) DEFAULT NULL,
                    w_pickup_city varchar(250) DEFAULT NULL,
                    w_pickup_address varchar(250) DEFAULT NULL,
                    w_pickup_zip varchar(250) DEFAULT NULL,
                    w_date_time varchar(250) DEFAULT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_shipping_warehouse

                $getDB->query("INSERT INTO houdinv_shipping_warehouse (id, w_name, w_pickup_name, w_pickup_phone, w_warehouse, w_pickup_country, w_pickup_state, w_pickup_city, w_pickup_address, w_pickup_zip, w_date_time) VALUES
                (1, 'Houdin-e central', 'houdin-e', '+918561049836', 0, '100', '752', 'jaipur', '602 kailash tower', '302021', '1536192000')");

                // Table structure for table   houdinv_shop_applogo   

                $getDB->query("CREATE TABLE houdinv_shop_applogo (
                    applogo_id int(11) NOT NULL,
                    applogo varchar(250) NOT NULL,
                    splashscreen varchar(250) NOT NULL,
                    applogo_clear int(11) NOT NULL DEFAULT '0',
                    splashscreen_clear int(11) NOT NULL DEFAULT '0',
                    date_time varchar(200) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
                 
                // Dumping data for table houdinv_shop_applogo

                $getDB->query("INSERT INTO houdinv_shop_applogo (applogo_id, applogo, splashscreen, applogo_clear, splashscreen_clear, date_time) VALUES
                (1, 'applogo-1530972160.jpeg', 'splashscreen-1530973121.png', 1, 0, '1533110349')");


                // Table structure for table   houdinv_shop_ask_quotation   

                $getDB->query("CREATE TABLE houdinv_shop_ask_quotation (
                    houdinv_shop_ask_quotation_id int(11) NOT NULL,
                    houdinv_shop_ask_quotation_name varchar(100) NOT NULL,
                    houdinv_shop_ask_quotation_email varchar(100) NOT NULL,
                    houdinv_shop_ask_quotation_comment text NOT NULL,
                    houdinv_shop_ask_quotation_phone varchar(50) NOT NULL,
                    houdinv_shop_ask_quotation_product_id int(11) NOT NULL,
                    houdinv_shop_ask_quotation_product_count int(11) NOT NULL,
                    houdinv_shop_ask_quotation_created_date varchar(100) NOT NULL,
                    houdinv_shop_ask_quotation_update_date varchar(100) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                  // Dumping data for table houdinv_shop_ask_quotation

                $getDB->query("INSERT INTO houdinv_shop_ask_quotation (houdinv_shop_ask_quotation_id, houdinv_shop_ask_quotation_name, houdinv_shop_ask_quotation_email, houdinv_shop_ask_quotation_comment, houdinv_shop_ask_quotation_phone, houdinv_shop_ask_quotation_product_id, houdinv_shop_ask_quotation_product_count, houdinv_shop_ask_quotation_created_date, houdinv_shop_ask_quotation_update_date) VALUES
                (1, 'ankita', 'hawkscodeteam@gmail.com', 'gfhfgh', '8947086785', 31, 12, '1535419331', '1535419331'),
                (2, 'cv', 'hs.94.1996@gmail.com', 'dsf', '8947086785', 29, 12, '1535419412', '1535419412'),
                (3, 'av', 'sakshi@gmail.com', 'dfdf', '8947086785', 31, 12, '1535419920', '1535419920'),
                (5, 'shivam', 'shivam199420@gmail.com', 'hello', '917790870946', 31, 1200, '1535546759', '1535546759'),
                (6, 'shivam', 'shivam199420@gmail.com', 'hello', '917790870946', 31, 1200, '1535546877', '1535546877'),
                (7, 'shivam', 'shivam199420@gmail.com', 'hello', '917790870946', 31, 1200, '1535547033', '1535547033'),
                (8, 'shivam', 'shivam199420@gmail.com', 'hello', '917790870946', 31, 1200, '1535547084', '1535547084'),
                (9, 'shivam', 'shivam199420@gmail.com', 'hello', '917790870946', 31, 1200, '1535547100', '1535547100'),
                (10, 'shivam', 'shivam199420@gmail.com', 'hello', '917790870946', 31, 1200, '1535547118', '1535547118'),
                (11, 'shivam', 'shivam199420@gmail.com', 'hello', '917790870946', 31, 1200, '1535547398', '1535547398'),
                (12, 'shivam', 'shivam199420@gmail.com', 'hello', '917790870946', 31, 1200, '1535547417', '1535547417'),
                (13, 'shivam sengar', 'hawkscode@gmail.com', 'test', '+917790870946', 20, 10, '1535587200', '1535587200'),
                (14, 'shivam sengar', 'hawkscode@gmail.com', 'test', '+917790870946', 20, 10, '1535587200', '1535587200'),
                (15, 'payal', 'goyalpayal1995@gmail.com', 'cfsdf', '+919785940038', 8, 2, '1535760000', '1535760000'),
                (16, 'payal', 'goyalpayal1995@gmail.com', 'ycyyvyyc', 'null9785940038', 20, 0, '1535760000', '1535760000'),
                (17, 'payal', 'goyalpayal1995@gmail.com', 'ycyyvyyc', 'null9785940038', 20, 0, '1535760000', '1535760000'),
                (18, 'payal', 'goyalpayal1995@gmail.com', 'ycyyvyyc', 'null9785940038', 20, 0, '1535760000', '1535760000'),
                (19, 'payal', 'goyalpayal1995@gmail.com', 'ycyyvyyc', 'null9785940038', 20, 0, '1535760000', '1535760000'),
                (20, 'payal', 'goyalpayal1995@gmail.com', 'ycyyvyyc', 'null9785940038', 20, 0, '1535760000', '1535760000'),
                (21, 'payal', 'goyalpayal1995@gmail.com', 'ycyyvyyc', 'null9785940038', 20, 0, '1535760000', '1535760000'),
                (22, 'payal', 'goyalpayal1995@gmail.com', 'ycyyvyyc', 'null9785940038', 20, 0, '1535760000', '1535760000'),
                (23, 'payal', 'goyalpayal1995@gmail.com', 'ycyyvyyc', 'null9785940038', 20, 0, '1535760000', '1535760000'),
                (24, 'payal', 'goyalpayal1995@gmail.com', 'cfsdf', '9785940038', 8, 2, '1535760000', '1535760000'),
                (25, 'payal', 'goyalpayal1995@gmail.com', 'cfsdf', 'null9785940038', 8, 2, '1535760000', '1535760000'),
                (26, 'payal', 'geetika565@gmail.com', 'hskkd', '+919785940038', 19, 4, '1535760000', '1535760000'),
                (27, 'vhfh', 'chk@gmail.com', 'Descriptio', '+919929904141', 20, 258, '1535760000', '1535760000'),
                (28, 'payal', 'goyalpayal1995@gmail.com', 'huskdk', '+919785940038', 20, 4, '1535932800', '1535932800'),
                (29, 'geetika', 'mgeetika11@gmail.com', 'nggbtbtht', '+919799584524', 32, 5, '1536278400', '1536278400'),
                (30, 'shivam', 'sengar1994shivam@hotmail.com', 'shiav', '7790870946', 9, 13, '1536647716', '1536647716'),
                (31, 'test', 'hdbdn@gmail.com', 'bfbdjdndjd', '+917790870964', 0, 0, '1536624000', '1536624000'),
                (32, 'test', 'hdbdn@gmail.com', 'bfbdjdndjd', '+917790870964', 0, 0, '1536624000', '1536624000'),
                (33, 'test', 'hdbdn@gmail.com', 'bfbdjdndjd', '+917790870964', 0, 0, '1536624000', '1536624000'),
                (34, 'test', 'hdbdn@gmail.com', 'bfbdjdndjd', '+917790870964', 0, 0, '1536624000', '1536624000'),
                (35, 'payL', 'goyalpayal1995@gmail.com', 'vbb', '+919785940038', 33, 5, '1536624000', '1536624000'),
                (36, 'test', 'hdbdn@gmail.com', 'bfbdjdndjd', '+917790870964', 0, 0, '1536624000', '1536624000'),
                (37, 'shivam senharc', 'shia@gmail.cm', 'hxndjdnd', '+917790870946', 1, 0, '1536624000', '1536624000'),
                (38, 'shivam senharc', 'shia@gmail.cm', 'hxndjdnd', '+917790870946', 1, 126194, '1536624000', '1536624000'),
                (39, 'guyjyjgh', 'bh@gmail.com', 'drgfdf', '0000000000000000', 36, 0, '1536996177', '1536996177'),
                (40, 'guyjyjgh', 'bh@gmail.com', 'drgfdf', '0000000000000000', 36, 0, '1536996339', '1536996339'),
                (41, '5745415435              ', 'b@yan.com', '55', '-3255555', 9, -4355555, '1537013273', '1537013273'),
                (42, 'gh', 'a@gmail.com', 'h', '-15', 35, -13, '1537169410', '1537169410'),
                (43, 'bgh', 'nehat0746@gmail.com', 'i', '-21', 36, 11, '1537187578', '1537187578'),
                (44, 'shivam ', 'shivqm@gmail.com', 'Hdnsusb', '7790870946', 36, 8, '1537228800', '1537228800'),
                (45, 'vipin', 'mj@gmail.com', 'Aha', '123456789', 36, 8, '1537228800', '1537228800'),
                (46, 'hxbsjshs', 'bsnsj@gmail.com', 'hdhdbd', '+917790870946', 33, 58757, '1537315200', '1537315200'),
                (47, 'gzbs', 'hdndj@gmail.col', 'Hdhsjsn', '7790870496', 36, 54545, '1537315200', '1537315200'),
                (48, 'fghgfhfg', 'fghfg@gmail.com', 'dfsdf', '234567890', 9, 2342, '1537359063', '1537359063'),
                (49, 'sdfdsfds', 'knjjk@gmail.com', 'kjndfjkndjs', '9343284', 36, 89347, '1537359756', '1537359756'),
                (50, 'a', 'Chris@gmail.com', 'a', '7777777777234234234234', 9, 1654, '1538126251', '1538126251')");

                // Table structure for table   houdinv_shop_configurations   

                $getDB->query("CREATE TABLE houdinv_shop_configurations (
                    houdinv_shop_configuration_google_analytics varchar(150) DEFAULT NULL,
                    houdinv_shop_configuration_bing_analytics varchar(150) DEFAULT NULL,
                    houdinv_shop_configuration_product_review tinyint(4) DEFAULT '0',
                    houdinv_shop_configuration_category_direction varchar(10) DEFAULT NULL,
                    houdinv_shop_configuration_shop_logo varchar(150) DEFAULT NULL,
                    houdinv_shop_configuration_product_limit tinyint(4) DEFAULT '4',
                    houdinv_shop_configuration_tag_line varchar(200) DEFAULT NULL,
                    houdinv_shop_configuration_ad_active tinyint(4) DEFAULT '0',
                    houdinv_shop_configuration_currency int(11) DEFAULT NULL,
                    houdinv_shop_configuration_language int(11) DEFAULT NULL,
                    houdinv_shop_configuration_created_at timestamp NULL DEFAULT NULL,
                    houdinv_shop_configuration_updated_at timestamp NULL DEFAULT NULL
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

                // Table structure for table   houdinv_shop_detail   

                $getDB->query("CREATE TABLE houdinv_shop_detail (
                    houdinv_shop_id int(11) NOT NULL,
                    houdinv_shop_title varchar(255) NOT NULL,
                    houdinv_shop_business_name varchar(255) NOT NULL,
                    houdinv_shop_address varchar(255) NOT NULL,
                    houdinv_shop_contact_info varchar(255) NOT NULL,
                    houdinv_shop_communication_email varchar(100) NOT NULL,
                    houdinv_shop_order_email varchar(100) NOT NULL,
                    houdinv_shop_customer_care_email varchar(100) NOT NULL,
                    houdinv_shop_pan varchar(150) NOT NULL,
                    houdinv_shop_tin varchar(100) NOT NULL,
                    houdinv_shop_cst varchar(100) NOT NULL,
                    houdinv_shop_vat varchar(100) NOT NULL,
                    houdinv_shop_custom_domain varchar(255) NOT NULL,
                    houdinv_shop_ssl varchar(255) NOT NULL,
                    houdinv_shop_cretaed_date varchar(100) NOT NULL,
                    houdinv_shop_updated_date varchar(100) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_shop_detail  

                $getDB->query("INSERT INTO houdinv_shop_detail (houdinv_shop_id, houdinv_shop_title, houdinv_shop_business_name, houdinv_shop_address, houdinv_shop_contact_info, houdinv_shop_communication_email, houdinv_shop_order_email, houdinv_shop_customer_care_email, houdinv_shop_pan, houdinv_shop_tin, houdinv_shop_cst, houdinv_shop_vat, houdinv_shop_custom_domain, houdinv_shop_ssl, houdinv_shop_cretaed_date, houdinv_shop_updated_date) VALUES
                (1, 'Houdin-e electronics', 'Houdin-e', '602, kailash tower lal kothi', '+917790870946', 'hawkscodeteam@gmail.com', 'shivam199420@gmail.com', 'he_support@gmail.con', 'FBJQP1236678', '123456789df12345678', '12345gfr56h', '23424fdfsdfd', '', '', '1529142348', '1535500800')");  

                // Table structure for table   houdinv_shop_logo   

                $getDB->query("CREATE TABLE houdinv_shop_logo (
                    id int(200) NOT NULL,
                    shop_logo varchar(200) NOT NULL,
                    shop_favicon varchar(200) NOT NULL,
                    update_date varchar(250) NOT NULL,
                    clear int(250) NOT NULL,
                    clear_favicon int(20) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_shop_logo  

                $getDB->query("INSERT INTO houdinv_shop_logo (id, shop_logo, shop_favicon, update_date, clear, clear_favicon) VALUES
                (1, 'logo-1537142400.png', 'favicon-1530269226.ico', '1537142400', 0, 0)");  

                // Table structure for table   houdinv_shop_order_configuration   

                $getDB->query("CREATE TABLE houdinv_shop_order_configuration (
                    houdinv_shop_order_configuration_id int(11) NOT NULL,
                    houdinv_shop_order_configuration_cancellation varchar(100) DEFAULT NULL,
                    houdinv_shop_order_configuration_return varchar(100) DEFAULT NULL,
                    houdinv_shop_order_configuration_replace varchar(100) DEFAULT NULL,
                    houdinv_shop_order_configuration_created_date varchar(30) DEFAULT NULL,
                    houdinv_shop_order_configuration_modified_date varchar(30) DEFAULT NULL,
                    houdinv_shop_order_configuration_sms_email int(11) NOT NULL,
                    houdinv_shop_order_configuration_printing int(11) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                    // Dumping data for table houdinv_shop_order_configuration  

                $getDB->query("INSERT INTO houdinv_shop_order_configuration (houdinv_shop_order_configuration_id, houdinv_shop_order_configuration_cancellation, houdinv_shop_order_configuration_return, houdinv_shop_order_configuration_replace, houdinv_shop_order_configuration_created_date, houdinv_shop_order_configuration_modified_date, houdinv_shop_order_configuration_sms_email, houdinv_shop_order_configuration_printing) VALUES
                (1, '20', '15', '23', '1534584381', '1538092800', 0, 0)");  

                // Table structure for table   houdinv_shop_query   

                $getDB->query("CREATE TABLE houdinv_shop_query (
                    houdinv_shop_query_id int(11) NOT NULL,
                    houdinv_shop_query_name varchar(30) NOT NULL,
                    houdinv_shop_query_email varchar(30) NOT NULL,
                    houdinv_shop_query_subject varchar(100) NOT NULL,
                    houdinv_shop_query_message text NOT NULL,
                    houdinv_shop_query_created_at timestamp NULL DEFAULT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                    // Dumping data for table houdinv_shop_query  

                $getDB->query("INSERT INTO houdinv_shop_query (houdinv_shop_query_id, houdinv_shop_query_name, houdinv_shop_query_email, houdinv_shop_query_subject, houdinv_shop_query_message, houdinv_shop_query_created_at) VALUES
                    (1, 'shivam', 'shivam.hawkscode@gmail.com', 'Query regarding branding', 'Hello\r\nHow are you?', NULL),
                    (2, 'shivam', 'shivam.hawkscode@gmail.com', 'Query regarding branding', 'Hello\r\nHow are you?', NULL),
                    (3, 'shivam', 'shivam.hawkscode@gmail.com', 'Query regarding branding', 'Hello\r\nHow are you\r\n', NULL),
                    (4, 'shivam', 'hawkscodeteam@gmail.com', 'sdfs', 'sdfs', NULL),
                    (5, 'shiva', 'shivam199420@gmail.com', 'hello', 'jkbsd', NULL),
                    (6, 'hawkscode', 'hawkscodeteam@gmail.com', 'hawkscode', 'hawkscode', NULL),
                    (7, 'shivam', 'shivam199420@gmail.com', 'shivam', 'shiv', NULL),
                    (8, 'shiva,', 'shivam.hawkscode@gmail.com', 'jksdbj', 'lknsdkf', NULL),
                    (9, 'shiva,', 'shivam199420@gmail.com', 'shiva,', 'sdklnfks', NULL),
                    (10, 'dasd', 'asd@gmail.com', 'sandjk', 'kjbdj', NULL),
                    (11, 'dasd', 'asd@gmail.com', 'sandjk', 'kjbdj', NULL),
                    (12, 'dasd', 'asd@gmail.com', 'sandjk', 'kjbdj', NULL),
                    (13, 'Atul', 'Chris@gmail.com', 'Subject To Chriss', 'Send mail to chris', NULL),
                    (14, 'thtgfnj', 'surbhisethi04@gmail.com', 'thtgfnj', 'thtgfnj', NULL),
                    (15, 'Atul', 'atulshelke617@gmail.com', 'Test', 'Testingdsdf', NULL)");

                // Table structure for table   houdinv_skusetting   

                $getDB->query("CREATE TABLE houdinv_skusetting (
                    id int(11) NOT NULL,
                    skusetting_number varchar(250) NOT NULL,
                    date_time varchar(250) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_skusetting  

                $getDB->query("INSERT INTO houdinv_skusetting (id, skusetting_number, date_time) VALUES
                (1, 'Hawks-_', '1531407249')"); 

                 // Table structure for table   houdinv_houdinv_sms_logskusetting   

                $getDB->query("CREATE TABLE houdinv_sms_log (
                    houdinv_sms_log_id int(11) NOT NULL,
                    houdinv_sms_log_credit_used int(11) NOT NULL,
                    houdinv_sms_log_status int(1) NOT NULL,
                    houdinv_sms_log_created_at varchar(30) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_sms_log  

                $getDB->query("INSERT INTO houdinv_sms_log (houdinv_sms_log_id, houdinv_sms_log_credit_used, houdinv_sms_log_status, houdinv_sms_log_created_at) VALUES
                (1, 1, 1, '1534470973'),
                (2, 1, 1, '1534572498'),
                (3, 1, 1, '1534572599'),
                (4, 1, 1, '1534750972'),
                (5, 1, 1, '1534751307'),
                (6, 1, 1, '1534759956'),
                (7, 1, 1, '1534760519'),
                (8, 1, 1, '1534760603'),
                (9, 1, 1, '1534761099'),
                (10, 1, 1, '1534764434'),
                (11, 1, 1, '1534764446'),
                (12, 1, 1, '1534764448'),
                (13, 1, 1, '1534764460'),
                (14, 1, 1, '1534764460'),
                (15, 1, 1, '1534764461'),
                (16, 1, 1, '1534764524'),
                (17, 1, 1, '1534769888'),
                (18, 1, 1, '1534846741'),
                (19, 1, 1, '1534846795'),
                (20, 1, 1, '1534915695'),
                (21, 1, 1, '1534932705'),
                (22, 1, 1, '1534932721'),
                (23, 1, 1, '1535013974'),
                (24, 1, 1, '1535021564'),
                (25, 1, 1, '1535024874'),
                (26, 1, 1, '1535085348'),
                (27, 1, 1, '1535361100'),
                (28, 1, 1, '1535361137'),
                (29, 1, 1, '1535361173'),
                (30, 1, 1, '1535370869'),
                (31, 1, 1, '1535370932'),
                (32, 1, 1, '1535371061'),
                (33, 1, 1, '1535371253'),
                (34, 1, 1, '1535371669'),
                (35, 1, 1, '1535372877'),
                (36, 1, 1, '1535373114'),
                (37, 1, 1, '1535373793'),
                (38, 1, 1, '1535374280'),
                (39, 1, 1, '1535374462'),
                (40, 1, 1, '1535421226'),
                (41, 1, 1, '1535611550'),
                (42, 1, 1, '1535611765'),
                (43, 1, 1, '1535611821'),
                (44, 1, 1, '1535621968'),
                (45, 1, 1, '1535622004'),
                (46, 1, 1, '1535622561'),
                (47, 1, 1, '1535622858'),
                (48, 1, 1, '1535623455'),
                (49, 1, 1, '1535623509'),
                (50, 1, 1, '1535628209'),
                (51, 1, 1, '1535630673'),
                (52, 1, 1, '1535592327'),
                (53, 1, 1, '1535592380'),
                (54, 1, 1, '1535592434'),
                (55, 1, 1, '1535587200'),
                (56, 1, 1, '1535593755'),
                (57, 1, 1, '1535593788'),
                (58, 1, 1, '1535691155'),
                (59, 1, 1, '1535691167'),
                (60, 1, 1, '1535691225'),
                (61, 1, 1, '1535691232'),
                (62, 1, 1, '1535691336'),
                (63, 1, 1, '1535691342'),
                (64, 1, 1, '1535691490'),
                (65, 1, 1, '1535691631'),
                (66, 1, 1, '1535691699'),
                (67, 1, 1, '1535691858'),
                (68, 1, 1, '1535692096'),
                (69, 1, 1, '1535695878'),
                (70, 1, 1, '1535695890'),
                (71, 1, 1, '1535695911'),
                (72, 1, 1, '1535695919'),
                (73, 1, 1, '1535696200'),
                (74, 1, 1, '1535696818'),
                (75, 1, 1, '1535696899'),
                (76, 1, 1, '1535673600'),
                (77, 1, 1, '1535698315'),
                (78, 1, 1, '1535698853'),
                (79, 1, 1, '1535708521'),
                (80, 1, 1, '1535708527'),
                (81, 1, 1, '1535708536'),
                (82, 1, 1, '1536056923'),
                (83, 1, 1, '1536057084'),
                (84, 1, 1, '1536057084'),
                (85, 1, 1, '1536057352'),
                (86, 1, 1, '1536058074'),
                (87, 1, 1, '1536058413'),
                (88, 1, 1, '1536060081'),
                (89, 1, 1, '1536061721'),
                (90, 1, 1, '1536062480'),
                (91, 1, 1, '1536062611'),
                (92, 1, 1, '1536062806'),
                (93, 1, 1, '1536063609'),
                (94, 1, 1, '1536023451'),
                (95, 1, 1, '1536138165'),
                (96, 1, 1, '1536139404'),
                (97, 1, 1, '1536139598'),
                (98, 1, 1, '1536140390'),
                (99, 1, 1, '1536140568'),
                (100, 1, 1, '1536141181'),
                (101, 1, 1, '1536145039'),
                (102, 1, 1, '1536147947'),
                (103, 1, 1, '1536148134'),
                (104, 1, 1, '1536148404'),
                (105, 1, 1, '1536148622'),
                (106, 1, 1, '1536148743'),
                (107, 1, 1, '1536149301'),
                (108, 1, 1, '1536149515'),
                (109, 1, 1, '1536149628'),
                (110, 1, 1, '1536150409'),
                (111, 1, 1, '1536150533'),
                (112, 1, 1, '1536150654'),
                (113, 1, 1, '1536150798'),
                (114, 1, 1, '1536150914'),
                (115, 1, 1, '1536111775'),
                (116, 1, 1, '1536210269'),
                (117, 1, 1, '1536211335'),
                (118, 1, 1, '1536212581'),
                (119, 1, 1, '1536212868'),
                (120, 1, 1, '1536215381'),
                (121, 1, 1, '1536215521'),
                (122, 1, 1, '1536218248'),
                (123, 1, 1, '1536218367'),
                (124, 1, 1, '1536219236'),
                (125, 1, 1, '1536219440'),
                (126, 1, 1, '1536219904'),
                (127, 1, 1, '1536220421'),
                (128, 1, 1, '1536220946'),
                (129, 1, 1, '1536223119'),
                (130, 1, 1, '1536224719'),
                (131, 1, 1, '1536225123'),
                (132, 1, 1, '1536225309'),
                (133, 1, 1, '1536225505'),
                (134, 1, 1, '1536226019'),
                (135, 1, 1, '1536226375'),
                (136, 1, 1, '1536228539'),
                (137, 1, 1, '1536233637'),
                (138, 1, 1, '1536233857'),
                (139, 1, 1, '1536320246'),
                (140, 1, 1, '1536321777'),
                (141, 1, 1, '1536322037'),
                (142, 1, 1, '1536323735'),
                (143, 1, 1, '1536323807'),
                (144, 1, 1, '1536323889'),
                (145, 1, 1, '1536324022'),
                (146, 1, 1, '1536324294'),
                (147, 1, 1, '1536550335'),
                (148, 1, 1, '1536553976'),
                (149, 1, 1, '1536554406'),
                (150, 1, 1, '1536554926'),
                (151, 1, 1, '1536560628'),
                (152, 1, 1, '1536560729'),
                (153, 1, 1, '1536560939'),
                (154, 1, 1, '1536562383'),
                (155, 1, 1, '1536562410'),
                (156, 1, 1, '1536564062'),
                (157, 1, 1, '1536564148'),
                (158, 1, 1, '1536564812'),
                (159, 1, 1, '1536565530'),
                (160, 1, 1, '1536566139'),
                (161, 1, 1, '1536566762'),
                (162, 1, 1, '1536573357'),
                (163, 1, 1, '1536573466'),
                (164, 1, 1, '1536573560'),
                (165, 1, 1, '1536573597'),
                (166, 1, 1, '1536574511'),
                (167, 1, 1, '1536574687'),
                (168, 1, 1, '1536575031'),
                (169, 1, 1, '1536580125'),
                (170, 1, 1, '1536582206'),
                (171, 1, 1, '1536582556'),
                (172, 1, 1, '1536583117'),
                (173, 1, 1, '1536583235'),
                (174, 1, 1, '1536584220'),
                (175, 1, 1, '1536541217'),
                (176, 1, 1, '1536541616'),
                (177, 1, 1, '1536541648'),
                (178, 1, 1, '1536541824'),
                (179, 1, 1, '1536541905'),
                (180, 1, 1, '1536541996'),
                (181, 1, 1, '1536542416'),
                (182, 1, 1, '1536542436'),
                (183, 1, 1, '1536542983'),
                (184, 1, 1, '1536543143'),
                (185, 1, 1, '1536545332'),
                (186, 1, 1, '1536553016'),
                (187, 1, 1, '1536648154'),
                (188, 1, 1, '1536648357'),
                (189, 1, 1, '1536648440'),
                (190, 1, 1, '1536648742'),
                (191, 1, 1, '1536649632'),
                (192, 1, 1, '1536669589'),
                (193, 1, 1, '1536669654'),
                (194, 1, 1, '1536628903'),
                (195, 1, 1, '1536839618'),
                (196, 1, 1, '1536839798'),
                (197, 1, 1, '1536841565'),
                (198, 1, 1, '1536841990'),
                (199, 1, 1, '1536842508'),
                (200, 1, 1, '1536842640'),
                (201, 1, 1, '1536842682'),
                (202, 1, 1, '1536843022'),
                (203, 1, 1, '1536843106'),
                (204, 1, 1, '1536843408'),
                (205, 1, 1, '1536843429'),
                (206, 1, 1, '1536843481'),
                (207, 1, 1, '1536800999'),
                (208, 1, 1, '1536801081'),
                (209, 1, 1, '1536801292'),
                (210, 1, 1, '1536801384'),
                (211, 1, 1, '1536801510'),
                (212, 1, 1, '1536801597'),
                (213, 1, 1, '1536801653'),
                (214, 1, 1, '1536801804'),
                (215, 1, 1, '1536801940'),
                (216, 1, 1, '1536801977'),
                (217, 1, 1, '1536802073'),
                (218, 1, 1, '1536802359'),
                (219, 1, 1, '1536802571'),
                (220, 1, 1, '1536802653'),
                (221, 1, 1, '1536802742'),
                (222, 1, 1, '1536802923'),
                (223, 1, 1, '1536802988'),
                (224, 1, 1, '1536803225'),
                (225, 1, 1, '1536803248'),
                (226, 1, 1, '1536803483'),
                (227, 1, 1, '1536898766'),
                (228, 1, 1, '1536922599'),
                (229, 1, 1, '1536922687'),
                (230, 1, 1, '1536922699'),
                (231, 1, 1, '1536922707'),
                (232, 1, 1, '1536883200'),
                (233, 1, 1, '1536989683'),
                (234, 1, 1, '1536989703'),
                (235, 1, 1, '1536989746'),
                (236, 1, 1, '1536989837'),
                (237, 1, 1, '1536990278'),
                (238, 1, 1, '1536990304'),
                (239, 1, 1, '1536991190'),
                (240, 1, 1, '1537170309'),
                (241, 1, 1, '1537179979'),
                (242, 1, 1, '1537182992'),
                (243, 1, 1, '1537183003'),
                (244, 1, 1, '1537183173'),
                (245, 1, 1, '1537187242'),
                (246, 1, 1, '1537187278'),
                (247, 1, 1, '1537187713'),
                (248, 1, 1, '1537188270'),
                (249, 1, 1, '1537188344'),
                (250, 1, 1, '1537188377'),
                (251, 1, 1, '1537188389'),
                (252, 1, 1, '1537188499'),
                (253, 1, 1, '1537248172'),
                (254, 1, 1, '1537248506'),
                (255, 1, 1, '1537248564'),
                (256, 1, 1, '1537255324'),
                (257, 1, 1, '1537255985'),
                (258, 1, 1, '1537257199'),
                (259, 1, 1, '1537257220'),
                (260, 1, 1, '1537257240'),
                (261, 1, 1, '1537260414'),
                (262, 1, 1, '1537260696'),
                (263, 1, 1, '1537260808'),
                (264, 1, 1, '1537260995'),
                (265, 1, 1, '1537244233'),
                (266, 1, 1, '1537334453'),
                (267, 1, 1, '1537348858'),
                (268, 1, 1, '1537359223'),
                (269, 1, 1, '1537359366'),
                (270, 1, 1, '1537359590'),
                (271, 1, 1, '1537359615'),
                (272, 1, 1, '1537359754'),
                (273, 1, 1, '1537359759'),
                (274, 1, 1, '1537422984'),
                (275, 1, 1, '1537423438'),
                (276, 1, 1, '1537434904'),
                (277, 1, 1, '1537435057'),
                (278, 1, 1, '1537437614'),
                (279, 1, 1, '1537438804'),
                (280, 1, 1, '1537440993'),
                (281, 1, 1, '1537441370'),
                (282, 1, 1, '1537441475'),
                (283, 1, 1, '1537442658'),
                (284, 1, 1, '1537445155'),
                (285, 1, 1, '1537445721'),
                (286, 1, 1, '1537445751'),
                (287, 1, 1, '1537445799'),
                (288, 1, 1, '1537446108'),
                (289, 1, 1, '1537506315'),
                (290, 1, 1, '1537506368'),
                (291, 1, 1, '1537507071'),
                (292, 1, 1, '1537507137'),
                (293, 1, 1, '1537526704'),
                (294, 1, 1, '1537527467'),
                (295, 1, 1, '1537528396'),
                (296, 1, 1, '1537582248'),
                (297, 1, 1, '1537783115'),
                (298, 1, 1, '1537784715'),
                (299, 1, 1, '1537784779'),
                (300, 1, 1, '1537784881'),
                (301, 1, 1, '1537785345'),
                (302, 1, 1, '1537785492'),
                (303, 1, 1, '1537785636'),
                (304, 1, 1, '1537786327'),
                (305, 1, 1, '1537792328'),
                (306, 1, 1, '1537792352'),
                (307, 1, 1, '1537855061'),
                (308, 1, 1, '1537855242'),
                (309, 1, 1, '1537873750'),
                (310, 1, 1, '1537873871'),
                (311, 1, 1, '1537873973'),
                (312, 1, 1, '1538202653'),
                (313, 1, 1, '1538379578'),
                (314, 1, 1, '1538379811'),
                (315, 1, 1, '1538379868'),
                (316, 1, 1, '1538380039'),
                (317, 1, 1, '1538380743'),
                (318, 1, 1, '1538383286'),
                (319, 1, 1, '1538397894'),
                (320, 1, 1, '1538397915'),
                (321, 1, 1, '1538355838'),
                (322, 1, 1, '1538355846'),
                (323, 1, 1, '1538355863'),
                (324, 1, 1, '1538355869'),
                (325, 1, 1, '1538355880'),
                (326, 1, 1, '1538355916'),
                (327, 1, 1, '1538463144'),
                (328, 1, 1, '1538463153'),
                (329, 1, 1, '1538463166'),
                (330, 1, 1, '1538463171'),
                (331, 1, 1, '1538463177'),
                (332, 1, 1, '1538463182'),
                (333, 1, 1, '1538463187'),
                (334, 1, 1, '1538463220'),
                (335, 1, 1, '1538463233'),
                (336, 1, 1, '1538463239'),
                (337, 1, 1, '1538463269'),
                (338, 1, 1, '1538463398'),
                (339, 1, 1, '1538463402'),
                (340, 1, 1, '1538463719'),
                (341, 1, 1, '1538463821'),
                (342, 1, 1, '1538463852'),
                (343, 1, 1, '1538463989'),
                (344, 1, 1, '1538464085'),
                (345, 1, 1, '1538464152'),
                (346, 1, 1, '1538464213'),
                (347, 1, 1, '1538464220'),
                (348, 1, 1, '1538464313'),
                (349, 1, 1, '1538438400'),
                (350, 1, 1, '1538438400'),
                (351, 1, 1, '1538472641'),
                (352, 1, 1, '1538476565'),
                (353, 1, 1, '1538543119'),
                (354, 1, 1, '1538543372'),
                (355, 1, 1, '1538543382'),
                (356, 1, 1, '1538543455'),
                (357, 1, 1, '1538543482'),
                (358, 1, 1, '1538543496'),
                (359, 1, 1, '1538543503'),
                (360, 1, 1, '1538543509'),
                (361, 1, 1, '1538543545'),
                (362, 1, 1, '1538543607'),
                (363, 1, 1, '1538543862'),
                (364, 1, 1, '1538545403'),
                (365, 1, 1, '1538545403'),
                (366, 1, 1, '1538545636'),
                (367, 1, 1, '1538545853')"); 

        // Table structure for table   houdinv_sms_template   

                $getDB->query("CREATE TABLE houdinv_sms_template (
            houdinv_sms_template_id int(11) NOT NULL,
            houdinv_sms_template_type varchar(50) NOT NULL,
            houdinv_sms_template_subject varchar(255) NOT NULL,
            houdinv_sms_template_message text NOT NULL,
            houdinv_sms_template_created_date varchar(100) NOT NULL,
            houdinv_sms_template_updated_date varchar(100) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_sms_template 

                $getDB->query("INSERT INTO houdinv_sms_template (houdinv_sms_template_id, houdinv_sms_template_type, houdinv_sms_template_subject, houdinv_sms_template_message, houdinv_sms_template_created_date, houdinv_sms_template_updated_date) VALUES
        (1, 'register', 'Re: earings', '{registration_date} - date of registration\r\n', '2018-07-27, 10:38:01', '1533607303'),
        (2, 'forget_password', 'dfd', '{date} - date \r\n{user_name} - Name of user\r\n{user_mail} - Email of user', '2018-07-27, 11:50:31', '2018-07-27, 11:50:31'),
        (3, 'On_order', 'new order placed', '{date} - date \r\n{product_name} - Name of product\r\n{amount} - Total amount', '2018-07-27, 11:58:23', '2018-07-27, 11:58:23'),
        (4, 'On_ship', 'your order shipped', '{date} - date \r\n{product_name} - Name of product\r\n{amount} - Total amount', '2018-07-27, 12:01:19', '2018-07-27, 12:01:19')");
          
          // Table structure for table   houdinv_social_links   

                $getDB->query("CREATE TABLE houdinv_social_links (
            social_id int(200) NOT NULL,
            twitter_url varchar(250) NOT NULL,
            facebook_url varchar(250) NOT NULL,
            google_url varchar(250) NOT NULL,
            youtube_url varchar(250) NOT NULL,
            instagram_url varchar(250) NOT NULL,
            pinterest_url varchar(250) NOT NULL,
            linkedin_url varchar(250) NOT NULL,
            date_time varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

          // Dumping data for table houdinv_social_links 

                $getDB->query("INSERT INTO houdinv_social_links (social_id, twitter_url, facebook_url, google_url, youtube_url, instagram_url, pinterest_url, linkedin_url, date_time) VALUES
        (1, '', 'www.facebook.com', '', 'www.youtube.com', 'www.instagram.com', 'www.pinterest.com', 'www.linkedin.com', '1531216038')");

        // Table structure for table   houdinv_staff_management   

                $getDB->query("CREATE TABLE houdinv_staff_management (
            staff_id int(11) NOT NULL,
            staff_name varchar(250) NOT NULL,
            staff_contact_number varchar(250) NOT NULL,
            staff_department varchar(250) NOT NULL,
            staff_status int(11) NOT NULL,
            staff_warehouse int(11) NOT NULL,
            staff_email varchar(250) NOT NULL,
            staff_password varchar(250) DEFAULT NULL,
            staff_password_salt varchar(150) DEFAULT NULL,
            staff_alternat_contact varchar(250) DEFAULT NULL,
            staff_address longtext NOT NULL,
            password_send varchar(250) DEFAULT NULL,
            houdinv_staff_auth_token varchar(255) NOT NULL,
            houdinv_staff_auth_url_token varchar(255) NOT NULL,
            create_date varchar(200) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_staff_management 

                $getDB->query("INSERT INTO houdinv_staff_management (staff_id, staff_name, staff_contact_number, staff_department, staff_status, staff_warehouse, staff_email, staff_password, staff_password_salt, staff_alternat_contact, staff_address, password_send, houdinv_staff_auth_token, houdinv_staff_auth_url_token, create_date) VALUES
        (2, 'Deepak', '7891592009', 'DeliveryBoy', 0, 4, 'deepsharma906@gmail.com', '$2y$10$NDqdJ1Dnyp6fzEwP8GcI0OqIJpxy4t/a/2InnHlkD/DHt3vd7CVDW', '$2y$10$NDqdJ1Dnyp6fzEwP8GcI0On4xKoP8hUTqRPO7hfVQ6hoEMz1IiGSK', '7891592009', '58 B Ganesh Colony Bas Badanpura', 'email', '', '', '1535010872'),
        (9, 'shivam', '7790870946', 'DeliveryBoy', 1, 4, 'shivam199420@gmail.com', '$2y$10$6gf.Mkq1sq2YZIH6s5flI.LacU3Yv/cNR9vHV9AUExMF4UM333F4S', '$2y$10$6gf.Mkq1sq2YZIH6s5flI.Rst2GC6sc3p5R/UY.w.IEL3W6M3yZcK', '7790870946', 'Yeskdf', 'email', '', '', '1536192000')");

        // Table structure for table   houdinv_staff_management_sub   

                $getDB->query("CREATE TABLE houdinv_staff_management_sub (
            id int(11) NOT NULL,
            staff_id int(11) NOT NULL,
            dashboard int(11) DEFAULT NULL,
            order int(11) DEFAULT NULL,
            cataogry int(11) DEFAULT NULL,
            product int(11) DEFAULT NULL,
            setting int(11) DEFAULT NULL,
            staff_members int(11) DEFAULT NULL,
            inventory int(11) DEFAULT NULL,
            delivery int(11) DEFAULT NULL,
            discount int(11) DEFAULT NULL,
            supplier_management int(11) DEFAULT NULL,
            templates int(11) DEFAULT NULL,
            purchase int(11) DEFAULT NULL,
            coupons int(11) DEFAULT NULL,
            gift_voucher int(11) DEFAULT NULL,
            calendar int(11) DEFAULT NULL,
            POS int(11) DEFAULT NULL,
            store_setting int(11) DEFAULT NULL,
            tax int(11) DEFAULT NULL,
            shipping int(11) DEFAULT NULL,
            campaign int(11) DEFAULT NULL,
            customer_management int(11) DEFAULT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");


        // Dumping data for table houdinv_staff_management_sub 

                $getDB->query("INSERT INTO houdinv_staff_management_sub (id, staff_id, dashboard, order, cataogry, product, setting, staff_members, inventory, delivery, discount, supplier_management, templates, purchase, coupons, gift_voucher, calendar, POS, store_setting, tax, shipping, campaign, customer_management) VALUES
        (2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
        (8, 9, 1, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL)");

        // Table structure for table   houdinv_states   

                $getDB->query("CREATE TABLE houdinv_states (
            state_id int(11) NOT NULL,
            state_name varchar(30) COLLATE utf8_unicode_ci NOT NULL,
            country_id int(11) NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

        // Dumping data for table houdinv_states

                $getDB->query("INSERT INTO houdinv_states (state_id, state_name, country_id) VALUES
        (240, 'â€“', 1),
        (241, 'Balkh', 2),
        (242, 'Herat', 2),
        (243, 'Kabol', 2),
        (244, 'Qandahar', 2),
        (245, 'Benguela', 3),
        (246, 'Huambo', 3),
        (247, 'Luanda', 3),
        (248, 'Namibe', 3),
        (249, 'â€“', 4),
        (250, 'Tirana', 5),
        (251, 'Andorra la Vella', 6),
        (252, 'CuraÃ§ao', 7),
        (253, 'Abu Dhabi', 8),
        (254, 'Ajman', 8),
        (255, 'Dubai', 8),
        (256, 'Sharja', 8),
        (257, 'Buenos Aires', 9),
        (258, 'Catamarca', 9),
        (259, 'CÃ³rdoba', 9),
        (260, 'Chaco', 9),
        (261, 'Chubut', 9),
        (262, 'Corrientes', 9),
        (263, 'Distrito Federal', 9),
        (264, 'Entre Rios', 9),
        (265, 'Formosa', 9),
        (266, 'Jujuy', 9),
        (267, 'La Rioja', 9),
        (268, 'Mendoza', 9),
        (269, 'Misiones', 9),
        (270, 'NeuquÃ©n', 9),
        (271, 'Salta', 9),
        (272, 'San Juan', 9),
        (273, 'San Luis', 9),
        (274, 'Santa FÃ©', 9),
        (275, 'Santiago del Estero', 9),
        (276, 'TucumÃ¡n', 9),
        (277, 'Lori', 10),
        (278, 'Yerevan', 10),
        (279, 'Å irak', 10),
        (280, 'Tutuila', 11),
        (281, 'St John', 14),
        (282, 'Capital Region', 15),
        (283, 'New South Wales', 15),
        (284, 'Queensland', 15),
        (285, 'South Australia', 15),
        (286, 'Tasmania', 15),
        (287, 'Victoria', 15),
        (288, 'West Australia', 15),
        (289, 'KÃ¤rnten', 16),
        (290, 'North Austria', 16),
        (291, 'Salzburg', 16),
        (292, 'Steiermark', 16),
        (293, 'Tiroli', 16),
        (294, 'Wien', 16),
        (295, 'Baki', 17),
        (296, 'GÃ¤ncÃ¤', 17),
        (297, 'MingÃ¤Ã§evir', 17),
        (298, 'Sumqayit', 17),
        (299, 'Bujumbura', 18),
        (300, 'Antwerpen', 19),
        (301, 'Bryssel', 19),
        (302, 'East Flanderi', 19),
        (303, 'Hainaut', 19),
        (304, 'LiÃ¨ge', 19),
        (305, 'Namur', 19),
        (306, 'West Flanderi', 19),
        (307, 'Atacora', 20),
        (308, 'Atlantique', 20),
        (309, 'Borgou', 20),
        (310, 'OuÃ©mÃ©', 20),
        (311, 'BoulkiemdÃ©', 21),
        (312, 'Houet', 21),
        (313, 'Kadiogo', 21),
        (314, 'Barisal', 22),
        (315, 'Chittagong', 22),
        (316, 'Dhaka', 22),
        (317, 'Khulna', 22),
        (318, 'Rajshahi', 22),
        (319, 'Sylhet', 22),
        (320, 'Burgas', 23),
        (321, 'Grad Sofija', 23),
        (322, 'Haskovo', 23),
        (323, 'Lovec', 23),
        (324, 'Plovdiv', 23),
        (325, 'Ruse', 23),
        (326, 'Varna', 23),
        (327, 'al-Manama', 24),
        (328, 'New Providence', 25),
        (329, 'Federaatio', 26),
        (330, 'Republika Srpska', 26),
        (331, 'Brest', 27),
        (332, 'Gomel', 27),
        (333, 'Grodno', 27),
        (334, 'Horad Minsk', 27),
        (335, 'Minsk', 27),
        (336, 'Mogiljov', 27),
        (337, 'Vitebsk', 27),
        (338, 'Belize City', 28),
        (339, 'Cayo', 28),
        (340, 'Hamilton', 29),
        (341, 'Saint GeorgeÂ´s', 29),
        (342, 'Chuquisaca', 30),
        (343, 'Cochabamba', 30),
        (344, 'La Paz', 30),
        (345, 'Oruro', 30),
        (346, 'PotosÃ­', 30),
        (347, 'Santa Cruz', 30),
        (348, 'Tarija', 30),
        (349, 'Acre', 31),
        (350, 'Alagoas', 31),
        (351, 'AmapÃ¡', 31),
        (352, 'Amazonas', 31),
        (353, 'Bahia', 31),
        (354, 'CearÃ¡', 31),
        (355, 'Distrito Federal', 31),
        (356, 'EspÃ­rito Santo', 31),
        (357, 'GoiÃ¡s', 31),
        (358, 'MaranhÃ£o', 31),
        (359, 'Mato Grosso', 31),
        (360, 'Mato Grosso do Sul', 31),
        (361, 'Minas Gerais', 31),
        (362, 'ParaÃ­ba', 31),
        (363, 'ParanÃ¡', 31),
        (364, 'ParÃ¡', 31),
        (365, 'Pernambuco', 31),
        (366, 'PiauÃ­', 31),
        (367, 'Rio de Janeiro', 31),
        (368, 'Rio Grande do Norte', 31),
        (369, 'Rio Grande do Sul', 31),
        (370, 'RondÃ´nia', 31),
        (371, 'Roraima', 31),
        (372, 'Santa Catarina', 31),
        (373, 'SÃ£o Paulo', 31),
        (374, 'Sergipe', 31),
        (375, 'Tocantins', 31),
        (376, 'St Michael', 32),
        (377, 'Brunei and Muara', 33),
        (378, 'Thimphu', 34),
        (379, 'Francistown', 36),
        (380, 'Gaborone', 36),
        (381, 'Bangui', 37),
        (382, 'Alberta', 38),
        (383, 'British Colombia', 38),
        (384, 'Manitoba', 38),
        (385, 'Newfoundland', 38),
        (386, 'Nova Scotia', 38),
        (387, 'Ontario', 38),
        (388, 'QuÃ©bec', 38),
        (389, 'Saskatchewan', 38),
        (390, 'Home Island', 39),
        (391, 'West Island', 39),
        (392, 'Basel-Stadt', 40),
        (393, 'Bern', 40),
        (394, 'Geneve', 40),
        (395, 'Vaud', 40),
        (396, 'ZÃ¼rich', 40),
        (397, 'Antofagasta', 41),
        (398, 'Atacama', 41),
        (399, 'BÃ­obÃ­o', 41),
        (400, 'Coquimbo', 41),
        (401, 'La AraucanÃ­a', 41),
        (402, 'Los Lagos', 41),
        (403, 'Magallanes', 41),
        (404, 'Maule', 41),
        (405, 'OÂ´Higgins', 41),
        (406, 'Santiago', 41),
        (407, 'TarapacÃ¡', 41),
        (408, 'ValparaÃ­so', 41),
        (409, 'Anhui', 42),
        (410, 'Chongqing', 42),
        (411, 'Fujian', 42),
        (412, 'Gansu', 42),
        (413, 'Guangdong', 42),
        (414, 'Guangxi', 42),
        (415, 'Guizhou', 42),
        (416, 'Hainan', 42),
        (417, 'Hebei', 42),
        (418, 'Heilongjiang', 42),
        (419, 'Henan', 42),
        (420, 'Hubei', 42),
        (421, 'Hunan', 42),
        (422, 'Inner Mongolia', 42),
        (423, 'Jiangsu', 42),
        (424, 'Jiangxi', 42),
        (425, 'Jilin', 42),
        (426, 'Liaoning', 42),
        (427, 'Ningxia', 42),
        (428, 'Peking', 42),
        (429, 'Qinghai', 42),
        (430, 'Shaanxi', 42),
        (431, 'Shandong', 42),
        (432, 'Shanghai', 42),
        (433, 'Shanxi', 42),
        (434, 'Sichuan', 42),
        (435, 'Tianjin', 42),
        (436, 'Tibet', 42),
        (437, 'Xinxiang', 42),
        (438, 'Yunnan', 42),
        (439, 'Zhejiang', 42),
        (440, 'Abidjan', 43),
        (441, 'BouakÃ©', 43),
        (442, 'Daloa', 43),
        (443, 'Korhogo', 43),
        (444, 'Yamoussoukro', 43),
        (445, 'Centre', 44),
        (446, 'ExtrÃªme-Nord', 44),
        (447, 'Littoral', 44),
        (448, 'Nord', 44),
        (449, 'Nord-Ouest', 44),
        (450, 'Ouest', 44),
        (451, 'Bandundu', 0),
        (452, 'Bas-ZaÃ¯re', 0),
        (453, 'East Kasai', 0),
        (454, 'Equateur', 0),
        (455, 'Haute-ZaÃ¯re', 0),
        (456, 'Kinshasa', 0),
        (457, 'North Kivu', 0),
        (458, 'Shaba', 0),
        (459, 'South Kivu', 0),
        (460, 'West Kasai', 0),
        (461, 'Brazzaville', 46),
        (462, 'Kouilou', 46),
        (463, 'Rarotonga', 47),
        (464, 'Antioquia', 48),
        (465, 'AtlÃ¡ntico', 48),
        (466, 'BolÃ­var', 48),
        (467, 'BoyacÃ¡', 48),
        (468, 'Caldas', 48),
        (469, 'CaquetÃ¡', 48),
        (470, 'Cauca', 48),
        (471, 'CÃ³rdoba', 48),
        (472, 'Cesar', 48),
        (473, 'Cundinamarca', 48),
        (474, 'Huila', 48),
        (475, 'La Guajira', 48),
        (476, 'Magdalena', 48),
        (477, 'Meta', 48),
        (478, 'NariÃ±o', 48),
        (479, 'Norte de Santander', 48),
        (480, 'QuindÃ­o', 48),
        (481, 'Risaralda', 48),
        (482, 'SantafÃ© de BogotÃ¡', 48),
        (483, 'Santander', 48),
        (484, 'Sucre', 48),
        (485, 'Tolima', 48),
        (486, 'Valle', 48),
        (487, 'Njazidja', 49),
        (488, 'SÃ£o Tiago', 50),
        (489, 'San JosÃ©', 51),
        (490, 'CamagÃ¼ey', 52),
        (491, 'Ciego de Ãvila', 52),
        (492, 'Cienfuegos', 52),
        (493, 'Granma', 52),
        (494, 'GuantÃ¡namo', 52),
        (495, 'HolguÃ­n', 52),
        (496, 'La Habana', 52),
        (497, 'Las Tunas', 52),
        (498, 'Matanzas', 52),
        (499, 'Pinar del RÃ­o', 52),
        (500, 'Sancti-SpÃ­ritus', 52),
        (501, 'Santiago de Cuba', 52),
        (502, 'Villa Clara', 52),
        (503, 'â€“', 53),
        (504, 'Grand Cayman', 54),
        (505, 'Limassol', 55),
        (506, 'Nicosia', 55),
        (507, 'HlavnÃ­ mesto Praha', 56),
        (508, 'JiznÃ­ Cechy', 56),
        (509, 'JiznÃ­ Morava', 56),
        (510, 'SevernÃ­ Cechy', 56),
        (511, 'SevernÃ­ Morava', 56),
        (512, 'VÃ½chodnÃ­ Cechy', 56),
        (513, 'ZapadnÃ­ Cechy', 56),
        (514, 'Anhalt Sachsen', 57),
        (515, 'Baden-WÃ¼rttemberg', 57),
        (516, 'Baijeri', 57),
        (517, 'Berliini', 57),
        (518, 'Brandenburg', 57),
        (519, 'Bremen', 57),
        (520, 'Hamburg', 57),
        (521, 'Hessen', 57),
        (522, 'Mecklenburg-Vorpomme', 57),
        (523, 'Niedersachsen', 57),
        (524, 'Nordrhein-Westfalen', 57),
        (525, 'Rheinland-Pfalz', 57),
        (526, 'Saarland', 57),
        (527, 'Saksi', 57),
        (528, 'Schleswig-Holstein', 57),
        (529, 'ThÃ¼ringen', 57),
        (530, 'Djibouti', 58),
        (531, 'St George', 59),
        (532, 'Ã…rhus', 60),
        (533, 'Frederiksberg', 60),
        (534, 'Fyn', 60),
        (535, 'KÃ¸benhavn', 60),
        (536, 'Nordjylland', 60),
        (537, 'Distrito Nacional', 61),
        (538, 'Duarte', 61),
        (539, 'La Romana', 61),
        (540, 'Puerto Plata', 61),
        (541, 'San Pedro de MacorÃ­', 61),
        (542, 'Santiago', 61),
        (543, 'Alger', 62),
        (544, 'Annaba', 62),
        (545, 'Batna', 62),
        (546, 'BÃ©char', 62),
        (547, 'BÃ©jaÃ¯a', 62),
        (548, 'Biskra', 62),
        (549, 'Blida', 62),
        (550, 'Chlef', 62),
        (551, 'Constantine', 62),
        (552, 'GhardaÃ¯a', 62),
        (553, 'Mostaganem', 62),
        (554, 'Oran', 62),
        (555, 'SÃ©tif', 62),
        (556, 'Sidi Bel AbbÃ¨s', 62),
        (557, 'Skikda', 62),
        (558, 'TÃ©bessa', 62),
        (559, 'Tiaret', 62),
        (560, 'Tlemcen', 62),
        (561, 'Azuay', 63),
        (562, 'Chimborazo', 63),
        (563, 'El Oro', 63),
        (564, 'Esmeraldas', 63),
        (565, 'Guayas', 63),
        (566, 'Imbabura', 63),
        (567, 'Loja', 63),
        (568, 'Los RÃ­os', 63),
        (569, 'ManabÃ­', 63),
        (570, 'Pichincha', 63),
        (571, 'Tungurahua', 63),
        (572, 'al-Buhayra', 64),
        (573, 'al-Daqahliya', 64),
        (574, 'al-Faiyum', 64),
        (575, 'al-Gharbiya', 64),
        (576, 'al-Minufiya', 64),
        (577, 'al-Minya', 64),
        (578, 'al-Qalyubiya', 64),
        (579, 'al-Sharqiya', 64),
        (580, 'Aleksandria', 64),
        (581, 'Assuan', 64),
        (582, 'Asyut', 64),
        (583, 'Bani Suwayf', 64),
        (584, 'Giza', 64),
        (585, 'Ismailia', 64),
        (586, 'Kafr al-Shaykh', 64),
        (587, 'Kairo', 64),
        (588, 'Luxor', 64),
        (589, 'Port Said', 64),
        (590, 'Qina', 64),
        (591, 'Sawhaj', 64),
        (592, 'Shamal Sina', 64),
        (593, 'Suez', 64),
        (594, 'Maekel', 65),
        (595, 'El-AaiÃºn', 66),
        (596, 'Andalusia', 67),
        (597, 'Aragonia', 67),
        (598, 'Asturia', 67),
        (599, 'Balears', 67),
        (600, 'Baskimaa', 67),
        (601, 'Canary Islands', 67),
        (602, 'Cantabria', 67),
        (603, 'Castilla and LeÃ³n', 67),
        (604, 'Extremadura', 67),
        (605, 'Galicia', 67),
        (606, 'Kastilia-La Mancha', 67),
        (607, 'Katalonia', 67),
        (608, 'La Rioja', 67),
        (609, 'Madrid', 67),
        (610, 'Murcia', 67),
        (611, 'Navarra', 67),
        (612, 'Valencia', 67),
        (613, 'Harjumaa', 68),
        (614, 'Tartumaa', 68),
        (615, 'Addis Abeba', 69),
        (616, 'Amhara', 69),
        (617, 'Dire Dawa', 69),
        (618, 'Oromia', 69),
        (619, 'Tigray', 69),
        (620, 'Newmaa', 70),
        (621, 'PÃ¤ijÃ¤t-HÃ¤me', 70),
        (622, 'Pirkanmaa', 70),
        (623, 'Pohjois-Pohjanmaa', 70),
        (624, 'Varsinais-Suomi', 70),
        (625, 'Central', 71),
        (626, 'East Falkland', 72),
        (627, 'Alsace', 73),
        (628, 'Aquitaine', 73),
        (629, 'Auvergne', 73),
        (630, 'ÃŽle-de-France', 73),
        (631, 'Basse-Normandie', 73),
        (632, 'Bourgogne', 73),
        (633, 'Bretagne', 73),
        (634, 'Centre', 73),
        (635, 'Champagne-Ardenne', 73),
        (636, 'Franche-ComtÃ©', 73),
        (637, 'Haute-Normandie', 73),
        (638, 'Languedoc-Roussillon', 73),
        (639, 'Limousin', 73),
        (640, 'Lorraine', 73),
        (641, 'Midi-PyrÃ©nÃ©es', 73),
        (642, 'Nord-Pas-de-Calais', 73),
        (643, 'Pays de la Loire', 73),
        (644, 'Picardie', 73),
        (645, 'Provence-Alpes-CÃ´te', 73),
        (646, 'RhÃ´ne-Alpes', 73),
        (647, 'Streymoyar', 74),
        (648, 'Chuuk', 0),
        (649, 'Pohnpei', 0),
        (650, 'Estuaire', 76),
        (651, 'â€“', 77),
        (652, 'England', 77),
        (653, 'Jersey', 77),
        (654, 'North Ireland', 77),
        (655, 'Scotland', 77),
        (656, 'Wales', 77),
        (657, 'Abhasia [Aphazeti]', 78),
        (658, 'Adzaria [AtÅ¡ara]', 78),
        (659, 'Imereti', 78),
        (660, 'Kvemo Kartli', 78),
        (661, 'Tbilisi', 78),
        (662, 'Ashanti', 79),
        (663, 'Greater Accra', 79),
        (664, 'Northern', 79),
        (665, 'Western', 79),
        (666, 'â€“', 80),
        (667, 'Conakry', 81),
        (668, 'Basse-Terre', 82),
        (669, 'Grande-Terre', 82),
        (670, 'Banjul', 83),
        (671, 'Kombo St Mary', 83),
        (672, 'Bissau', 84),
        (673, 'Bioko', 85),
        (674, 'Attika', 86),
        (675, 'Central Macedonia', 86),
        (676, 'Crete', 86),
        (677, 'Thessalia', 86),
        (678, 'West Greece', 86),
        (679, 'St George', 87),
        (680, 'Kitaa', 88),
        (681, 'Guatemala', 89),
        (682, 'Quetzaltenango', 89),
        (683, 'Cayenne', 90),
        (684, 'â€“', 91),
        (685, 'Georgetown', 92),
        (686, 'Hongkong', 93),
        (687, 'Kowloon and New Kowl', 93),
        (688, 'AtlÃ¡ntida', 95),
        (689, 'CortÃ©s', 95),
        (690, 'Distrito Central', 95),
        (691, 'Grad Zagreb', 96),
        (692, 'Osijek-Baranja', 96),
        (693, 'Primorje-Gorski Kota', 96),
        (694, 'Split-Dalmatia', 96),
        (695, 'Nord', 97),
        (696, 'Ouest', 97),
        (697, 'Baranya', 98),
        (698, 'BÃ¡cs-Kiskun', 98),
        (699, 'Borsod-AbaÃºj-ZemplÃ', 98),
        (700, 'Budapest', 98),
        (701, 'CsongrÃ¡d', 98),
        (702, 'FejÃ©r', 98),
        (703, 'GyÃ¶r-Moson-Sopron', 98),
        (704, 'HajdÃº-Bihar', 98),
        (705, 'Szabolcs-SzatmÃ¡r-Be', 98),
        (706, 'Aceh', 99),
        (707, 'Bali', 99),
        (708, 'Bengkulu', 99),
        (709, 'Central Java', 99),
        (710, 'East Java', 99),
        (711, 'Jakarta Raya', 99),
        (712, 'Jambi', 99),
        (713, 'Kalimantan Barat', 99),
        (714, 'Kalimantan Selatan', 99),
        (715, 'Kalimantan Tengah', 99),
        (716, 'Kalimantan Timur', 99),
        (717, 'Lampung', 99),
        (718, 'Molukit', 99),
        (719, 'Nusa Tenggara Barat', 99),
        (720, 'Nusa Tenggara Timur', 99),
        (721, 'Riau', 99),
        (722, 'Sulawesi Selatan', 99),
        (723, 'Sulawesi Tengah', 99),
        (724, 'Sulawesi Tenggara', 99),
        (725, 'Sulawesi Utara', 99),
        (726, 'Sumatera Barat', 99),
        (727, 'Sumatera Selatan', 99),
        (728, 'Sumatera Utara', 99),
        (729, 'West Irian', 99),
        (730, 'West Java', 99),
        (731, 'Yogyakarta', 99),
        (732, 'Andhra Pradesh', 100),
        (733, 'Assam', 100),
        (734, 'Bihar', 100),
        (735, 'Chandigarh', 100),
        (736, 'Chhatisgarh', 100),
        (737, 'Delhi', 100),
        (738, 'Gujarat', 100),
        (739, 'Haryana', 100),
        (740, 'Jammu and Kashmir', 100),
        (741, 'Jharkhand', 100),
        (742, 'Karnataka', 100),
        (743, 'Kerala', 100),
        (744, 'Madhya Pradesh', 100),
        (745, 'Maharashtra', 100),
        (746, 'Manipur', 100),
        (747, 'Meghalaya', 100),
        (748, 'Mizoram', 100),
        (749, 'Orissa', 100),
        (750, 'Pondicherry', 100),
        (751, 'Punjab', 100),
        (752, 'Rajasthan', 100),
        (753, 'Tamil Nadu', 100),
        (754, 'Tripura', 100),
        (755, 'Uttar Pradesh', 100),
        (756, 'Uttaranchal', 100),
        (757, 'West Bengal', 100),
        (758, 'Leinster', 102),
        (759, 'Munster', 102),
        (760, 'Ardebil', 103),
        (761, 'Bushehr', 103),
        (762, 'Chaharmahal va Bakht', 103),
        (763, 'East Azerbaidzan', 103),
        (764, 'Esfahan', 103),
        (765, 'Fars', 103),
        (766, 'Gilan', 103),
        (767, 'Golestan', 103),
        (768, 'Hamadan', 103),
        (769, 'Hormozgan', 103),
        (770, 'Ilam', 103),
        (771, 'Kerman', 103),
        (772, 'Kermanshah', 103),
        (773, 'Khorasan', 103),
        (774, 'Khuzestan', 103),
        (775, 'Kordestan', 103),
        (776, 'Lorestan', 103),
        (777, 'Markazi', 103),
        (778, 'Mazandaran', 103),
        (779, 'Qazvin', 103),
        (780, 'Qom', 103),
        (781, 'Semnan', 103),
        (782, 'Sistan va Baluchesta', 103),
        (783, 'Teheran', 103),
        (784, 'West Azerbaidzan', 103),
        (785, 'Yazd', 103),
        (786, 'Zanjan', 103),
        (787, 'al-Anbar', 104),
        (788, 'al-Najaf', 104),
        (789, 'al-Qadisiya', 104),
        (790, 'al-Sulaymaniya', 104),
        (791, 'al-Tamim', 104),
        (792, 'Babil', 104),
        (793, 'Baghdad', 104),
        (794, 'Basra', 104),
        (795, 'DhiQar', 104),
        (796, 'Diyala', 104),
        (797, 'Irbil', 104),
        (798, 'Karbala', 104),
        (799, 'Maysan', 104),
        (800, 'Ninawa', 104),
        (801, 'Wasit', 104),
        (802, 'HÃ¶fuÃ°borgarsvÃ¦Ã°i', 105),
        (803, 'Ha Darom', 106),
        (804, 'Ha Merkaz', 106),
        (805, 'Haifa', 106),
        (806, 'Jerusalem', 106),
        (807, 'Tel Aviv', 106),
        (808, 'Abruzzit', 107),
        (809, 'Apulia', 107),
        (810, 'Calabria', 107),
        (811, 'Campania', 107),
        (812, 'Emilia-Romagna', 107),
        (813, 'Friuli-Venezia Giuli', 107),
        (814, 'Latium', 107),
        (815, 'Liguria', 107),
        (816, 'Lombardia', 107),
        (817, 'Marche', 107),
        (818, 'Piemonte', 107),
        (819, 'Sardinia', 107),
        (820, 'Sisilia', 107),
        (821, 'Toscana', 107),
        (822, 'Trentino-Alto Adige', 107),
        (823, 'Umbria', 107),
        (824, 'Veneto', 107),
        (825, 'St. Andrew', 108),
        (826, 'St. Catherine', 108),
        (827, 'al-Zarqa', 109),
        (828, 'Amman', 109),
        (829, 'Irbid', 109),
        (830, 'Aichi', 110),
        (831, 'Akita', 110),
        (832, 'Aomori', 110),
        (833, 'Chiba', 110),
        (834, 'Ehime', 110),
        (835, 'Fukui', 110),
        (836, 'Fukuoka', 110),
        (837, 'Fukushima', 110),
        (838, 'Gifu', 110),
        (839, 'Gumma', 110),
        (840, 'Hiroshima', 110),
        (841, 'Hokkaido', 110),
        (842, 'Hyogo', 110),
        (843, 'Ibaragi', 110),
        (844, 'Ishikawa', 110),
        (845, 'Iwate', 110),
        (846, 'Kagawa', 110),
        (847, 'Kagoshima', 110),
        (848, 'Kanagawa', 110),
        (849, 'Kochi', 110),
        (850, 'Kumamoto', 110),
        (851, 'Kyoto', 110),
        (852, 'Mie', 110),
        (853, 'Miyagi', 110),
        (854, 'Miyazaki', 110),
        (855, 'Nagano', 110),
        (856, 'Nagasaki', 110),
        (857, 'Nara', 110),
        (858, 'Niigata', 110),
        (859, 'Oita', 110),
        (860, 'Okayama', 110),
        (861, 'Okinawa', 110),
        (862, 'Osaka', 110),
        (863, 'Saga', 110),
        (864, 'Saitama', 110),
        (865, 'Shiga', 110),
        (866, 'Shimane', 110),
        (867, 'Shizuoka', 110),
        (868, 'Tochigi', 110),
        (869, 'Tokushima', 110),
        (870, 'Tokyo-to', 110),
        (871, 'Tottori', 110),
        (872, 'Toyama', 110),
        (873, 'Wakayama', 110),
        (874, 'Yamagata', 110),
        (875, 'Yamaguchi', 110),
        (876, 'Yamanashi', 110),
        (877, 'Almaty', 111),
        (878, 'Almaty Qalasy', 111),
        (879, 'AqtÃ¶be', 111),
        (880, 'Astana', 111),
        (881, 'Atyrau', 111),
        (882, 'East Kazakstan', 111),
        (883, 'Mangghystau', 111),
        (884, 'North Kazakstan', 111),
        (885, 'Pavlodar', 111),
        (886, 'Qaraghandy', 111),
        (887, 'Qostanay', 111),
        (888, 'Qyzylorda', 111),
        (889, 'South Kazakstan', 111),
        (890, 'Taraz', 111),
        (891, 'West Kazakstan', 111),
        (892, 'Central', 112),
        (893, 'Coast', 112),
        (894, 'Eastern', 112),
        (895, 'Nairobi', 112),
        (896, 'Nyanza', 112),
        (897, 'Rift Valley', 112),
        (898, 'Bishkek shaary', 113),
        (899, 'Osh', 113),
        (900, 'Battambang', 114),
        (901, 'Phnom Penh', 114),
        (902, 'Siem Reap', 114),
        (903, 'South Tarawa', 115),
        (904, 'St George Basseterre', 116),
        (905, 'Cheju', 117),
        (906, 'Chollabuk', 117),
        (907, 'Chollanam', 117),
        (908, 'Chungchongbuk', 117),
        (909, 'Chungchongnam', 117),
        (910, 'Inchon', 117),
        (911, 'Kang-won', 117),
        (912, 'Kwangju', 117),
        (913, 'Kyonggi', 117),
        (914, 'Kyongsangbuk', 117),
        (915, 'Kyongsangnam', 117),
        (916, 'Pusan', 117),
        (917, 'Seoul', 117),
        (918, 'Taegu', 117),
        (919, 'Taejon', 117),
        (920, 'al-Asima', 118),
        (921, 'Hawalli', 118),
        (922, 'Savannakhet', 119),
        (923, 'Viangchan', 119),
        (924, 'al-Shamal', 120),
        (925, 'Beirut', 120),
        (926, 'Montserrado', 121),
        (927, 'al-Zawiya', 122),
        (928, 'Bengasi', 122),
        (929, 'Misrata', 122),
        (930, 'Tripoli', 122),
        (931, 'Castries', 123),
        (932, 'Schaan', 124),
        (933, 'Vaduz', 124),
        (934, 'Central', 125),
        (935, 'Northern', 125),
        (936, 'Western', 125),
        (937, 'Maseru', 126),
        (938, 'Kaunas', 127),
        (939, 'Klaipeda', 127),
        (940, 'Panevezys', 127),
        (941, 'Vilna', 127),
        (942, 'Å iauliai', 127),
        (943, 'Luxembourg', 128),
        (944, 'Daugavpils', 129),
        (945, 'Liepaja', 129),
        (946, 'Riika', 129),
        (947, 'Macau', 130),
        (948, 'Casablanca', 131),
        (949, 'Chaouia-Ouardigha', 131),
        (950, 'Doukkala-Abda', 131),
        (951, 'FÃ¨s-Boulemane', 131),
        (952, 'Gharb-Chrarda-BÃ©ni', 131),
        (953, 'Marrakech-Tensift-Al', 131),
        (954, 'MeknÃ¨s-Tafilalet', 131),
        (955, 'Oriental', 131),
        (956, 'Rabat-SalÃ©-Zammour-', 131),
        (957, 'Souss Massa-DraÃ¢', 131),
        (958, 'Tadla-Azilal', 131),
        (959, 'Tanger-TÃ©touan', 131),
        (960, 'Taza-Al Hoceima-Taou', 131),
        (961, 'â€“', 132),
        (962, 'Balti', 133),
        (963, 'Bender (TÃ®ghina)', 133),
        (964, 'Chisinau', 133),
        (965, 'Dnjestria', 133),
        (966, 'Antananarivo', 134),
        (967, 'Fianarantsoa', 134),
        (968, 'Mahajanga', 134),
        (969, 'Toamasina', 134),
        (970, 'Maale', 135),
        (971, 'Aguascalientes', 136),
        (972, 'Baja California', 136),
        (973, 'Baja California Sur', 136),
        (974, 'Campeche', 136),
        (975, 'Chiapas', 136),
        (976, 'Chihuahua', 136),
        (977, 'Coahuila de Zaragoza', 136),
        (978, 'Colima', 136),
        (979, 'Distrito Federal', 136),
        (980, 'Durango', 136),
        (981, 'Guanajuato', 136),
        (982, 'Guerrero', 136),
        (983, 'Hidalgo', 136),
        (984, 'Jalisco', 136),
        (985, 'MÃ©xico', 136),
        (986, 'MichoacÃ¡n de Ocampo', 136),
        (987, 'Morelos', 136),
        (988, 'Nayarit', 136),
        (989, 'Nuevo LeÃ³n', 136),
        (990, 'Oaxaca', 136),
        (991, 'Puebla', 136),
        (992, 'QuerÃ©taro', 136),
        (993, 'QuerÃ©taro de Arteag', 136),
        (994, 'Quintana Roo', 136),
        (995, 'San Luis PotosÃ­', 136),
        (996, 'Sinaloa', 136),
        (997, 'Sonora', 136),
        (998, 'Tabasco', 136),
        (999, 'Tamaulipas', 136),
        (1000, 'Veracruz', 136),
        (1001, 'Veracruz-Llave', 136),
        (1002, 'YucatÃ¡n', 136),
        (1003, 'Zacatecas', 136),
        (1004, 'Majuro', 137),
        (1005, 'Skopje', 138),
        (1006, 'Bamako', 139),
        (1007, 'Inner Harbour', 140),
        (1008, 'Outer Harbour', 140),
        (1009, 'Irrawaddy [Ayeyarwad', 141),
        (1010, 'Magwe [Magway]', 141),
        (1011, 'Mandalay', 141),
        (1012, 'Mon', 141),
        (1013, 'Pegu [Bago]', 141),
        (1014, 'Rakhine', 141),
        (1015, 'Rangoon [Yangon]', 141),
        (1016, 'Sagaing', 141),
        (1017, 'Shan', 141),
        (1018, 'Tenasserim [Tanintha', 141),
        (1019, 'Ulaanbaatar', 142),
        (1020, 'Saipan', 143),
        (1021, 'Gaza', 144),
        (1022, 'Inhambane', 144),
        (1023, 'Manica', 144),
        (1024, 'Maputo', 144),
        (1025, 'Nampula', 144),
        (1026, 'Sofala', 144),
        (1027, 'Tete', 144),
        (1028, 'ZambÃ©zia', 144),
        (1029, 'Dakhlet NouÃ¢dhibou', 145),
        (1030, 'Nouakchott', 145),
        (1031, 'Plymouth', 146),
        (1032, 'Fort-de-France', 147),
        (1033, 'Plaines Wilhelms', 148),
        (1034, 'Port-Louis', 148),
        (1035, 'Blantyre', 149),
        (1036, 'Lilongwe', 149),
        (1037, 'Johor', 150),
        (1038, 'Kedah', 150),
        (1039, 'Kelantan', 150),
        (1040, 'Negeri Sembilan', 150),
        (1041, 'Pahang', 150),
        (1042, 'Perak', 150),
        (1043, 'Pulau Pinang', 150),
        (1044, 'Sabah', 150),
        (1045, 'Sarawak', 150),
        (1046, 'Selangor', 150),
        (1047, 'Terengganu', 150),
        (1048, 'Wilayah Persekutuan', 150),
        (1049, 'Mamoutzou', 151),
        (1050, 'Khomas', 152),
        (1051, 'â€“', 153),
        (1052, 'Maradi', 154),
        (1053, 'Niamey', 154),
        (1054, 'Zinder', 154),
        (1055, 'â€“', 155),
        (1056, 'Anambra & Enugu & Eb', 156),
        (1057, 'Bauchi & Gombe', 156),
        (1058, 'Benue', 156),
        (1059, 'Borno & Yobe', 156),
        (1060, 'Cross River', 156),
        (1061, 'Edo & Delta', 156),
        (1062, 'Federal Capital Dist', 156),
        (1063, 'Imo & Abia', 156),
        (1064, 'Kaduna', 156),
        (1065, 'Kano & Jigawa', 156),
        (1066, 'Katsina', 156),
        (1067, 'Kwara & Kogi', 156),
        (1068, 'Lagos', 156),
        (1069, 'Niger', 156),
        (1070, 'Ogun', 156),
        (1071, 'Ondo & Ekiti', 156),
        (1072, 'Oyo & Osun', 156),
        (1073, 'Plateau & Nassarawa', 156),
        (1074, 'Rivers & Bayelsa', 156),
        (1075, 'Sokoto & Kebbi & Zam', 156),
        (1076, 'Chinandega', 157),
        (1077, 'LeÃ³n', 157),
        (1078, 'Managua', 157),
        (1079, 'Masaya', 157),
        (1080, 'â€“', 158),
        (1081, 'Drenthe', 159),
        (1082, 'Flevoland', 159),
        (1083, 'Gelderland', 159),
        (1084, 'Groningen', 159),
        (1085, 'Limburg', 159),
        (1086, 'Noord-Brabant', 159),
        (1087, 'Noord-Holland', 159),
        (1088, 'Overijssel', 159),
        (1089, 'Utrecht', 159),
        (1090, 'Zuid-Holland', 159),
        (1091, 'Akershus', 160),
        (1092, 'Hordaland', 160),
        (1093, 'Oslo', 160),
        (1094, 'Rogaland', 160),
        (1095, 'SÃ¸r-TrÃ¸ndelag', 160),
        (1096, 'Central', 161),
        (1097, 'Eastern', 161),
        (1098, 'Western', 161),
        (1099, 'â€“', 162),
        (1100, 'Auckland', 163),
        (1101, 'Canterbury', 163),
        (1102, 'Dunedin', 163),
        (1103, 'Hamilton', 163),
        (1104, 'Wellington', 163),
        (1105, 'al-Batina', 164),
        (1106, 'Masqat', 164),
        (1107, 'Zufar', 164),
        (1108, 'Baluchistan', 165),
        (1109, 'Islamabad', 165),
        (1110, 'Nothwest Border Prov', 165),
        (1111, 'Punjab', 165),
        (1112, 'Sind', 165),
        (1113, 'Sindh', 165),
        (1114, 'PanamÃ¡', 166),
        (1115, 'San Miguelito', 166),
        (1116, 'â€“', 167),
        (1117, 'Ancash', 168),
        (1118, 'Arequipa', 168),
        (1119, 'Ayacucho', 168),
        (1120, 'Cajamarca', 168),
        (1121, 'Callao', 168),
        (1122, 'Cusco', 168),
        (1123, 'Huanuco', 168),
        (1124, 'Ica', 168),
        (1125, 'JunÃ­n', 168),
        (1126, 'La Libertad', 168),
        (1127, 'Lambayeque', 168),
        (1128, 'Lima', 168),
        (1129, 'Loreto', 168),
        (1130, 'Piura', 168),
        (1131, 'Puno', 168),
        (1132, 'Tacna', 168),
        (1133, 'Ucayali', 168),
        (1134, 'ARMM', 169),
        (1135, 'Bicol', 169),
        (1136, 'Cagayan Valley', 169),
        (1137, 'CAR', 169),
        (1138, 'Caraga', 169),
        (1139, 'Central Luzon', 169),
        (1140, 'Central Mindanao', 169),
        (1141, 'Central Visayas', 169),
        (1142, 'Eastern Visayas', 169),
        (1143, 'Ilocos', 169),
        (1144, 'National Capital Reg', 169),
        (1145, 'Northern Mindanao', 169),
        (1146, 'Southern Mindanao', 169),
        (1147, 'Southern Tagalog', 169),
        (1148, 'Western Mindanao', 169),
        (1149, 'Western Visayas', 169),
        (1150, 'Koror', 170),
        (1151, 'National Capital Dis', 171),
        (1152, 'Dolnoslaskie', 172),
        (1153, 'Kujawsko-Pomorskie', 172),
        (1154, 'Lodzkie', 172),
        (1155, 'Lubelskie', 172),
        (1156, 'Lubuskie', 172),
        (1157, 'Malopolskie', 172),
        (1158, 'Mazowieckie', 172),
        (1159, 'Opolskie', 172),
        (1160, 'Podkarpackie', 172),
        (1161, 'Podlaskie', 172),
        (1162, 'Pomorskie', 172),
        (1163, 'Slaskie', 172),
        (1164, 'Swietokrzyskie', 172),
        (1165, 'Warminsko-Mazurskie', 172),
        (1166, 'Wielkopolskie', 172),
        (1167, 'Zachodnio-Pomorskie', 172),
        (1168, 'Arecibo', 173),
        (1169, 'BayamÃ³n', 173),
        (1170, 'Caguas', 173),
        (1171, 'Carolina', 173),
        (1172, 'Guaynabo', 173),
        (1173, 'MayagÃ¼ez', 173),
        (1174, 'Ponce', 173),
        (1175, 'San Juan', 173),
        (1176, 'Toa Baja', 173),
        (1177, 'Chagang', 174),
        (1178, 'Hamgyong N', 174),
        (1179, 'Hamgyong P', 174),
        (1180, 'Hwanghae N', 174),
        (1181, 'Hwanghae P', 174),
        (1182, 'Kaesong-si', 174),
        (1183, 'Kangwon', 174),
        (1184, 'Nampo-si', 174),
        (1185, 'Pyongan N', 174),
        (1186, 'Pyongan P', 174),
        (1187, 'Pyongyang-si', 174),
        (1188, 'Yanggang', 174),
        (1189, 'Braga', 175),
        (1190, 'CoÃ­mbra', 175),
        (1191, 'Lisboa', 175),
        (1192, 'Porto', 175),
        (1193, 'Alto ParanÃ¡', 176),
        (1194, 'AsunciÃ³n', 176),
        (1195, 'Central', 176),
        (1196, 'Gaza', 177),
        (1197, 'Hebron', 177),
        (1198, 'Khan Yunis', 177),
        (1199, 'Nablus', 177),
        (1200, 'North Gaza', 177),
        (1201, 'Rafah', 177),
        (1202, 'Tahiti', 178),
        (1203, 'Doha', 179),
        (1204, 'Saint-Denis', 180),
        (1205, 'Arad', 181),
        (1206, 'Arges', 181),
        (1207, 'Bacau', 181),
        (1208, 'Bihor', 181),
        (1209, 'Botosani', 181),
        (1210, 'Braila', 181),
        (1211, 'Brasov', 181),
        (1212, 'Bukarest', 181),
        (1213, 'Buzau', 181),
        (1214, 'Caras-Severin', 181),
        (1215, 'Cluj', 181),
        (1216, 'Constanta', 181),
        (1217, 'DÃ¢mbovita', 181),
        (1218, 'Dolj', 181),
        (1219, 'Galati', 181),
        (1220, 'Gorj', 181),
        (1221, 'Iasi', 181),
        (1222, 'Maramures', 181),
        (1223, 'Mehedinti', 181),
        (1224, 'Mures', 181),
        (1225, 'Neamt', 181),
        (1226, 'Prahova', 181),
        (1227, 'Satu Mare', 181),
        (1228, 'Sibiu', 181),
        (1229, 'Suceava', 181),
        (1230, 'Timis', 181),
        (1231, 'Tulcea', 181),
        (1232, 'VÃ¢lcea', 181),
        (1233, 'Vrancea', 181),
        (1234, 'Adygea', 182),
        (1235, 'Altai', 182),
        (1236, 'Amur', 182),
        (1237, 'Arkangeli', 182),
        (1238, 'Astrahan', 182),
        (1239, 'BaÅ¡kortostan', 182),
        (1240, 'Belgorod', 182),
        (1241, 'Brjansk', 182),
        (1242, 'Burjatia', 182),
        (1243, 'Dagestan', 182),
        (1244, 'Habarovsk', 182),
        (1245, 'Hakassia', 182),
        (1246, 'Hanti-Mansia', 182),
        (1247, 'Irkutsk', 182),
        (1248, 'Ivanovo', 182),
        (1249, 'Jaroslavl', 182),
        (1250, 'Kabardi-Balkaria', 182),
        (1251, 'Kaliningrad', 182),
        (1252, 'Kalmykia', 182),
        (1253, 'Kaluga', 182),
        (1254, 'KamtÅ¡atka', 182),
        (1255, 'KaratÅ¡ai-TÅ¡erkessi', 182),
        (1256, 'Karjala', 182),
        (1257, 'Kemerovo', 182),
        (1258, 'Kirov', 182),
        (1259, 'Komi', 182),
        (1260, 'Kostroma', 182),
        (1261, 'Krasnodar', 182),
        (1262, 'Krasnojarsk', 182),
        (1263, 'Kurgan', 182),
        (1264, 'Kursk', 182),
        (1265, 'Lipetsk', 182),
        (1266, 'Magadan', 182),
        (1267, 'Marinmaa', 182),
        (1268, 'Mordva', 182),
        (1269, 'Moscow (City)', 182),
        (1270, 'Moskova', 182),
        (1271, 'Murmansk', 182),
        (1272, 'Nizni Novgorod', 182),
        (1273, 'North Ossetia-Alania', 182),
        (1274, 'Novgorod', 182),
        (1275, 'Novosibirsk', 182),
        (1276, 'Omsk', 182),
        (1277, 'Orenburg', 182),
        (1278, 'Orjol', 182),
        (1279, 'Penza', 182),
        (1280, 'Perm', 182),
        (1281, 'Pietari', 182),
        (1282, 'Pihkova', 182),
        (1283, 'Primorje', 182),
        (1284, 'Rjazan', 182),
        (1285, 'Rostov-na-Donu', 182),
        (1286, 'Saha (Jakutia)', 182),
        (1287, 'Sahalin', 182),
        (1288, 'Samara', 182),
        (1289, 'Saratov', 182),
        (1290, 'Smolensk', 182),
        (1291, 'Stavropol', 182),
        (1292, 'Sverdlovsk', 182),
        (1293, 'Tambov', 182),
        (1294, 'Tatarstan', 182),
        (1295, 'Tjumen', 182),
        (1296, 'Tomsk', 182),
        (1297, 'Tula', 182),
        (1298, 'Tver', 182),
        (1299, 'Tyva', 182),
        (1300, 'TÅ¡eljabinsk', 182),
        (1301, 'TÅ¡etÅ¡enia', 182),
        (1302, 'TÅ¡ita', 182),
        (1303, 'TÅ¡uvassia', 182),
        (1304, 'Udmurtia', 182),
        (1305, 'Uljanovsk', 182),
        (1306, 'Vladimir', 182),
        (1307, 'Volgograd', 182),
        (1308, 'Vologda', 182),
        (1309, 'Voronez', 182),
        (1310, 'Yamalin Nenetsia', 182),
        (1311, 'Kigali', 183),
        (1312, 'al-Khudud al-Samaliy', 184),
        (1313, 'al-Qasim', 184),
        (1314, 'al-Sharqiya', 184),
        (1315, 'Asir', 184),
        (1316, 'Hail', 184),
        (1317, 'Medina', 184),
        (1318, 'Mekka', 184),
        (1319, 'Najran', 184),
        (1320, 'Qasim', 184),
        (1321, 'Riad', 184),
        (1322, 'Riyadh', 184),
        (1323, 'Tabuk', 184),
        (1324, 'al-Bahr al-Abyad', 185),
        (1325, 'al-Bahr al-Ahmar', 185),
        (1326, 'al-Jazira', 185),
        (1327, 'al-Qadarif', 185),
        (1328, 'Bahr al-Jabal', 185),
        (1329, 'Darfur al-Janubiya', 185),
        (1330, 'Darfur al-Shamaliya', 185),
        (1331, 'Kassala', 185),
        (1332, 'Khartum', 185),
        (1333, 'Kurdufan al-Shamaliy', 185),
        (1334, 'Cap-Vert', 186),
        (1335, 'Diourbel', 186),
        (1336, 'Kaolack', 186),
        (1337, 'Saint-Louis', 186),
        (1338, 'ThiÃ¨s', 186),
        (1339, 'Ziguinchor', 186),
        (1340, 'â€“', 187),
        (1341, 'Saint Helena', 189),
        (1342, 'LÃ¤nsimaa', 190),
        (1343, 'Honiara', 191),
        (1344, 'Western', 192),
        (1345, 'La Libertad', 193),
        (1346, 'San Miguel', 193),
        (1347, 'San Salvador', 193),
        (1348, 'Santa Ana', 193),
        (1349, 'San Marino', 194),
        (1350, 'Serravalle/Dogano', 194),
        (1351, 'Banaadir', 195),
        (1352, 'Jubbada Hoose', 195),
        (1353, 'Woqooyi Galbeed', 195),
        (1354, 'Saint-Pierre', 196),
        (1355, 'Aqua Grande', 197),
        (1356, 'Paramaribo', 198),
        (1357, 'Bratislava', 199),
        (1358, 'VÃ½chodnÃ© Slovensko', 199),
        (1359, 'Osrednjeslovenska', 200),
        (1360, 'Podravska', 200),
        (1361, 'Ã–rebros lÃ¤n', 201),
        (1362, 'East GÃ¶tanmaan lÃ¤n', 201),
        (1363, 'GÃ¤vleborgs lÃ¤n', 201),
        (1364, 'JÃ¶nkÃ¶pings lÃ¤n', 201),
        (1365, 'Lisboa', 201),
        (1366, 'SkÃ¥ne lÃ¤n', 201),
        (1367, 'Uppsala lÃ¤n', 201),
        (1368, 'VÃ¤sterbottens lÃ¤n', 201),
        (1369, 'VÃ¤sternorrlands lÃ¤', 201),
        (1370, 'VÃ¤stmanlands lÃ¤n', 201),
        (1371, 'West GÃ¶tanmaan lÃ¤n', 201),
        (1372, 'Hhohho', 202),
        (1373, 'MahÃ©', 203),
        (1374, 'al-Hasaka', 204),
        (1375, 'al-Raqqa', 204),
        (1376, 'Aleppo', 204),
        (1377, 'Damascus', 204),
        (1378, 'Damaskos', 204),
        (1379, 'Dayr al-Zawr', 204),
        (1380, 'Hama', 204),
        (1381, 'Hims', 204),
        (1382, 'Idlib', 204),
        (1383, 'Latakia', 204),
        (1384, 'Grand Turk', 205),
        (1385, 'Chari-Baguirmi', 206),
        (1386, 'Logone Occidental', 206),
        (1387, 'Maritime', 207),
        (1388, 'Bangkok', 208),
        (1389, 'Chiang Mai', 208),
        (1390, 'Khon Kaen', 208),
        (1391, 'Nakhon Pathom', 208),
        (1392, 'Nakhon Ratchasima', 208),
        (1393, 'Nakhon Sawan', 208),
        (1394, 'Nonthaburi', 208),
        (1395, 'Songkhla', 208),
        (1396, 'Ubon Ratchathani', 208),
        (1397, 'Udon Thani', 208),
        (1398, 'Karotegin', 209),
        (1399, 'Khujand', 209),
        (1400, 'Fakaofo', 210),
        (1401, 'Ahal', 211),
        (1402, 'Dashhowuz', 211),
        (1403, 'Lebap', 211),
        (1404, 'Mary', 211),
        (1405, 'Dili', 212),
        (1406, 'Tongatapu', 213),
        (1407, 'Caroni', 214),
        (1408, 'Port-of-Spain', 214),
        (1409, 'Ariana', 215),
        (1410, 'Biserta', 215),
        (1411, 'GabÃ¨s', 215),
        (1412, 'Kairouan', 215),
        (1413, 'Sfax', 215),
        (1414, 'Sousse', 215),
        (1415, 'Tunis', 215),
        (1416, 'Adana', 216),
        (1417, 'Adiyaman', 216),
        (1418, 'Afyon', 216),
        (1419, 'Aksaray', 216),
        (1420, 'Ankara', 216),
        (1421, 'Antalya', 216),
        (1422, 'Aydin', 216),
        (1423, 'Ã‡orum', 216),
        (1424, 'Balikesir', 216),
        (1425, 'Batman', 216),
        (1426, 'Bursa', 216),
        (1427, 'Denizli', 216),
        (1428, 'Diyarbakir', 216),
        (1429, 'Edirne', 216),
        (1430, 'ElÃ¢zig', 216),
        (1431, 'Erzincan', 216),
        (1432, 'Erzurum', 216),
        (1433, 'Eskisehir', 216),
        (1434, 'Gaziantep', 216),
        (1435, 'Hatay', 216),
        (1436, 'IÃ§el', 216),
        (1437, 'Isparta', 216),
        (1438, 'Istanbul', 216),
        (1439, 'Izmir', 216),
        (1440, 'Kahramanmaras', 216),
        (1441, 'KarabÃ¼k', 216),
        (1442, 'Karaman', 216),
        (1443, 'Kars', 216),
        (1444, 'Kayseri', 216),
        (1445, 'KÃ¼tahya', 216),
        (1446, 'Kilis', 216),
        (1447, 'Kirikkale', 216),
        (1448, 'Kocaeli', 216),
        (1449, 'Konya', 216),
        (1450, 'Malatya', 216),
        (1451, 'Manisa', 216),
        (1452, 'Mardin', 216),
        (1453, 'Ordu', 216),
        (1454, 'Osmaniye', 216),
        (1455, 'Sakarya', 216),
        (1456, 'Samsun', 216),
        (1457, 'Sanliurfa', 216),
        (1458, 'Siirt', 216),
        (1459, 'Sivas', 216),
        (1460, 'Tekirdag', 216),
        (1461, 'Tokat', 216),
        (1462, 'Trabzon', 216),
        (1463, 'Usak', 216),
        (1464, 'Van', 216),
        (1465, 'Zonguldak', 216),
        (1466, 'Funafuti', 217),
        (1468, 'Changhwa', 218),
        (1469, 'Chiayi', 218),
        (1470, 'Hsinchu', 218),
        (1471, 'Hualien', 218),
        (1472, 'Ilan', 218),
        (1473, 'Kaohsiung', 218),
        (1474, 'Keelung', 218),
        (1475, 'Miaoli', 218),
        (1476, 'Nantou', 218),
        (1477, 'Pingtung', 218),
        (1478, 'Taichung', 218),
        (1479, 'Tainan', 218),
        (1480, 'Taipei', 218),
        (1481, 'Taitung', 218),
        (1482, 'Taoyuan', 218),
        (1483, 'YÃ¼nlin', 218),
        (1484, 'Arusha', 219),
        (1485, 'Dar es Salaam', 219),
        (1486, 'Dodoma', 219),
        (1487, 'Kilimanjaro', 219),
        (1488, 'Mbeya', 219),
        (1489, 'Morogoro', 219),
        (1490, 'Mwanza', 219),
        (1491, 'Tabora', 219),
        (1492, 'Tanga', 219),
        (1493, 'Zanzibar West', 219),
        (1494, 'Central', 220),
        (1495, 'Dnipropetrovsk', 221),
        (1496, 'Donetsk', 221),
        (1497, 'Harkova', 221),
        (1498, 'Herson', 221),
        (1499, 'Hmelnytskyi', 221),
        (1500, 'Ivano-Frankivsk', 221),
        (1501, 'Kiova', 221),
        (1502, 'Kirovograd', 221),
        (1503, 'Krim', 221),
        (1504, 'Lugansk', 221),
        (1505, 'Lviv', 221),
        (1506, 'Mykolajiv', 221),
        (1507, 'Odesa', 221),
        (1508, 'Pultava', 221),
        (1509, 'Rivne', 221),
        (1510, 'Sumy', 221),
        (1511, 'Taka-Karpatia', 221),
        (1512, 'Ternopil', 221),
        (1513, 'TÅ¡erkasy', 221),
        (1514, 'TÅ¡ernigiv', 221),
        (1515, 'TÅ¡ernivtsi', 221),
        (1516, 'Vinnytsja', 221),
        (1517, 'Volynia', 221),
        (1518, 'Zaporizzja', 221),
        (1519, 'Zytomyr', 221),
        (1520, 'Montevideo', 223),
        (1521, 'Alabama', 224),
        (1522, 'Alaska', 224),
        (1523, 'Arizona', 224),
        (1524, 'Arkansas', 224),
        (1525, 'California', 224),
        (1526, 'Colorado', 224),
        (1527, 'Connecticut', 224),
        (1528, 'District of Columbia', 224),
        (1529, 'Florida', 224),
        (1530, 'Georgia', 224),
        (1531, 'Hawaii', 224),
        (1532, 'Idaho', 224),
        (1533, 'Illinois', 224),
        (1534, 'Indiana', 224),
        (1535, 'Iowa', 224),
        (1536, 'Kansas', 224),
        (1537, 'Kentucky', 224),
        (1538, 'Louisiana', 224),
        (1539, 'Maryland', 224),
        (1540, 'Massachusetts', 224),
        (1541, 'Michigan', 224),
        (1542, 'Minnesota', 224),
        (1543, 'Mississippi', 224),
        (1544, 'Missouri', 224),
        (1545, 'Montana', 224),
        (1546, 'Nebraska', 224),
        (1547, 'Nevada', 224),
        (1548, 'New Hampshire', 224),
        (1549, 'New Jersey', 224),
        (1550, 'New Mexico', 224),
        (1551, 'New York', 224),
        (1552, 'North Carolina', 224),
        (1553, 'Ohio', 224),
        (1554, 'Oklahoma', 224),
        (1555, 'Oregon', 224),
        (1556, 'Pennsylvania', 224),
        (1557, 'Rhode Island', 224),
        (1558, 'South Carolina', 224),
        (1559, 'South Dakota', 224),
        (1560, 'Tennessee', 224),
        (1561, 'Texas', 224),
        (1562, 'Utah', 224),
        (1563, 'Virginia', 224),
        (1564, 'Washington', 224),
        (1565, 'Wisconsin', 224),
        (1566, 'Andijon', 225),
        (1567, 'Buhoro', 225),
        (1568, 'Cizah', 225),
        (1569, 'Fargona', 225),
        (1570, 'Karakalpakistan', 225),
        (1571, 'Khorazm', 225),
        (1572, 'Namangan', 225),
        (1573, 'Navoi', 225),
        (1574, 'Qashqadaryo', 225),
        (1575, 'Samarkand', 225),
        (1576, 'Surkhondaryo', 225),
        (1577, 'Toskent', 225),
        (1578, 'Toskent Shahri', 225),
        (1579, 'â€“', 226),
        (1580, 'St George', 0),
        (1582, 'AnzoÃ¡tegui', 228),
        (1583, 'Apure', 228),
        (1584, 'Aragua', 228),
        (1585, 'Barinas', 228),
        (1586, 'BolÃ­var', 228),
        (1587, 'Carabobo', 228),
        (1588, 'Distrito Federal', 228),
        (1589, 'FalcÃ³n', 228),
        (1590, 'GuÃ¡rico', 228),
        (1591, 'Lara', 228),
        (1592, 'MÃ©rida', 228),
        (1593, 'Miranda', 228),
        (1594, 'Monagas', 228),
        (1595, 'Portuguesa', 228),
        (1596, 'Sucre', 228),
        (1597, 'TÃ¡chira', 228),
        (1598, 'Trujillo', 228),
        (1599, 'Yaracuy', 228),
        (1600, 'Zulia', 228),
        (1601, 'Tortola', 229),
        (1602, 'St Thomas', 230),
        (1603, 'An Giang', 231),
        (1604, 'Ba Ria-Vung Tau', 231),
        (1605, 'Bac Thai', 231),
        (1606, 'Binh Dinh', 231),
        (1607, 'Binh Thuan', 231),
        (1608, 'Can Tho', 231),
        (1609, 'Dac Lac', 231),
        (1610, 'Dong Nai', 231),
        (1611, 'Haiphong', 231),
        (1612, 'Hanoi', 231),
        (1613, 'Ho Chi Minh City', 231),
        (1614, 'Khanh Hoa', 231),
        (1615, 'Kien Giang', 231),
        (1616, 'Lam Dong', 231),
        (1617, 'Nam Ha', 231),
        (1618, 'Nghe An', 231),
        (1619, 'Quang Binh', 231),
        (1620, 'Quang Nam-Da Nang', 231),
        (1621, 'Quang Ninh', 231),
        (1622, 'Thua Thien-Hue', 231),
        (1623, 'Tien Giang', 231),
        (1624, 'Shefa', 232),
        (1625, 'Wallis', 233),
        (1626, 'Upolu', 234),
        (1627, 'Aden', 235),
        (1628, 'Hadramawt', 235),
        (1629, 'Hodeida', 235),
        (1630, 'Ibb', 235),
        (1631, 'Sanaa', 235),
        (1632, 'Taizz', 235),
        (1633, 'Central Serbia', 236),
        (1634, 'Kosovo and Metohija', 236),
        (1635, 'Montenegro', 236),
        (1636, 'Vojvodina', 236),
        (1637, 'Eastern Cape', 237),
        (1638, 'Free State', 237),
        (1639, 'Gauteng', 237),
        (1640, 'KwaZulu-Natal', 237),
        (1641, 'Mpumalanga', 237),
        (1642, 'North West', 237),
        (1643, 'Northern Cape', 237),
        (1644, 'Western Cape', 237),
        (1645, 'Central', 238),
        (1646, 'Copperbelt', 238),
        (1647, 'Lusaka', 238),
        (1648, 'Bulawayo', 239),
        (1649, 'Harare', 239),
        (1650, 'Manicaland', 239),
        (1651, 'Midlands', 239)");

        // Table structure for table   houdinv_stock_transfer_log   

                $getDB->query("CREATE TABLE houdinv_stock_transfer_log (
            houdinv_stock_transfer_log_id int(11) NOT NULL,
            houdinv_stock_transfer_log_warehouse_from int(11) NOT NULL,
            houdinv_stock_transfer_log_warehouse_to int(11) NOT NULL,
            houdinv_stock_transfer_log_productdetails text NOT NULL,
            houdinv_stock_transfer_log_created_at varchar(30) NOT NULL
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_stock_transfer_log  

                $getDB->query("INSERT INTO houdinv_stock_transfer_log (houdinv_stock_transfer_log_id, houdinv_stock_transfer_log_warehouse_from, houdinv_stock_transfer_log_warehouse_to, houdinv_stock_transfer_log_productdetails, houdinv_stock_transfer_log_created_at) VALUES
        (1, 4, 2, '[{\"productId\":\"2\",\"variantId\":\"0\",\"quantity\":\"100\"},{\"productId\":\"0\",\"variantId\":\"7\",\"quantity\":\"100\"},{\"productId\":\"0\",\"variantId\":\"8\",\"quantity\":\"100\"}]', '1533691312'),
        (2, 4, 2, '[{\"productId\":\"2\",\"variantId\":\"0\",\"quantity\":\"100\"},{\"productId\":\"0\",\"variantId\":\"7\",\"quantity\":\"2\"},{\"productId\":\"\",\"variantId\":\"\",\"quantity\":\"\"},{\"productId\":\"\",\"variantId\":\"\",\"quantity\":\"\"}]', '1533693075')");

        // Table structure for table   houdinv_storediscount   

                $getDB->query("CREATE TABLE houdinv_storediscount (
            discount_id int(11) NOT NULL,
            product_cat_id varchar(200) NOT NULL,
            product_id varchar(250) NOT NULL,
            customer_id varchar(250) NOT NULL,
            discount_name varchar(200) NOT NULL,
            discount_type int(11) NOT NULL,
            discount_amount varchar(250) NOT NULL,
            discount_start varchar(250) NOT NULL,
            discount_end varchar(250) NOT NULL,
            discount_status int(11) NOT NULL DEFAULT '0',
            createdate varchar(250) NOT NULL,
            updatedate varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_storediscount  

                $getDB->query("INSERT INTO houdinv_storediscount (discount_id, product_cat_id, product_id, customer_id, discount_name, discount_type, discount_amount, discount_start, discount_end, discount_status, createdate, updatedate) VALUES
        (8, '[\"1\"]', '[\"1\"]', '0', 'Second  Discount change', 0, '10', '1531353600', '1531353600', 0, '1531206408', ''),
        (7, '[\"1\",\"4\"]', '[\"1\",\"3\",\"4\",\"6\"]', '0', 'Second  Discount change1', 0, '230', '1531180800', '1531958400', 1, '1531204661', ''),
        (6, '[\"2\"]', '[\"2\"]', '0', 'Second  Discount', 0, '10', '1530835200', '1532736000', 0, '1531138799', ''),
        (5, '[\"1\"]', '[\"1\",\"3\",\"4\"]', '0', 'First Discount', 0, '5', '1531094400', '1532563200', 1, '1531138768', ''),
        (9, '[\"1\"]', '[\"6\"]', '0', '120', 0, '23', '1531180800', '1531180800', 0, '1531206896', ''),
        (10, '[\"2\",\"4\"]', '[\"2\",\"5\"]', '0', 'Deep', 0, '1', '1531180800', '1532044800', 1, '1531207660', ''),
        (11, '[\"1\",\"4\"]', '[\"4\"]', '0', 'Second  Discount change', 0, '23', '1531180800', '1530144000', 1, '1531207983', ''),
        (12, '[\"1\",\"2\"]', '[\"1\",\"4\"]', 'null', 'Second  Discount change dee', 0, '43', '1531180800', '1531180800', 0, '1531208916', '')");

        // Table structure for table   houdinv_storepolicies   

                $getDB->query("CREATE TABLE houdinv_storepolicies (
            id int(250) NOT NULL,
            about longblob NOT NULL,
            faq longblob NOT NULL,
            Privacy_Policy longblob NOT NULL,
            Terms_Conditions longblob NOT NULL,
            Cancellation_Refund_Policy longblob NOT NULL,
            Shipping_Delivery_Policy longblob NOT NULL,
            Disclaimer_Policy longblob NOT NULL,
            date_time varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_storepolicies  

                $getDB->query("INSERT INTO houdinv_storepolicies (id, about, faq, Privacy_Policy, Terms_Conditions, Cancellation_Refund_Policy, Shipping_Delivery_Policy, Disclaimer_Policy, date_time) VALUES
(1, 0x4c6f72656d20497073756d2069732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e0d0a0d0a57687920646f207765207573652069743f0d0a49742069732061206c6f6e672065737461626c6973686564206661637420746861742061207265616465722077696c6c206265206469737472616374656420627920746865207265616461626c6520636f6e74656e74206f6620612070616765207768656e206c6f6f6b696e6720617420697473206c61796f75742e2054686520706f696e74206f66207573696e67204c6f72656d20497073756d2069732074686174206974206861732061206d6f72652d6f722d6c657373206e6f726d616c20646973747269627574696f6e206f66206c6574746572732c206173206f70706f73656420746f207573696e672027436f6e74656e7420686572652c20636f6e74656e742068657265272c206d616b696e67206974206c6f6f6b206c696b65207265616461626c6520456e676c6973682e204d616e79206465736b746f70207075626c697368696e67207061636b6167657320616e6420776562207061676520656469746f7273206e6f7720757365204c6f72656d20497073756d2061732074686569722064656661756c74206d6f64656c20746578742c20616e6420612073656172636820666f7220276c6f72656d20697073756d272077696c6c20756e636f766572206d616e7920776562207369746573207374696c6c20696e20746865697220696e66616e63792e20566172696f75732076657273696f6e7320686176652065766f6c766564206f766572207468652079656172732c20736f6d6574696d6573206279206163636964656e742c20736f6d6574696d6573206f6e20707572706f73652028696e6a65637465642068756d6f757220616e6420746865206c696b65292e, 0x4c6f72656d20497073756d2069732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e, 0x3c666f6e7420636f6c6f723d2223303033313633223e0d0a20202020202020202020202020202020202020202020202020202020202020204c6f72656d20497073756d2069732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e57687920646f207765207573652069743f49742069732061206c6f6e672065737461626c6973686564206661637420746861742061207265616465722077696c6c206265206469737472616374656420627920746865207265616461626c6520636f6e74656e74206f6620612070616765207768656e206c6f6f6b696e6720617420697473206c61796f75742e2054686520706f696e74206f66207573696e67204c6f72656d20497073756d2069732074686174206974206861732061206d6f72652d6f722d6c657373206e6f726d616c20646973747269627574696f6e206f66206c6574746572732c206173206f70706f73656420746f207573696e672027436f6e74656e7420686572652c20636f6e74656e742068657265272c206d616b696e67206974206c6f6f6b206c696b65207265616461626c6520456e676c6973682e204d616e79206465736b746f70207075626c697368696e67207061636b6167657320616e6420776562207061676520656469746f7273206e6f7720757365204c6f72656d20497073756d2061732074686569722064656661756c74206d6f64656c20746578742c20616e6420612073656172636820666f7220276c6f72656d20697073756d272077696c6c20756e636f766572206d616e7920776562207369746573207374696c6c20696e20746865697220696e66616e63792e20566172696f75732076657273696f6e7320686176652065766f6c766564206f766572207468652079656172732c20736f6d6574696d6573206279206163636964656e742c20736f6d6574696d6573206f6e20707572706f73652028696e6a65637465642068756d6f757220616e6420746865206c696b65292e200d0a202020202020202020202020202020202020202020202020202020202020203c2f666f6e743e, 0x20202020202020202020202020202020202020202020202020202020202020203c703e4c6f72656d20497073756d2069732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e57687920646f207765207573652069743f49742069732061206c6f6e672065737461626c6973686564206661637420746861742061207265616465722077696c6c206265206469737472616374656420627920746865207265616461626c6520636f6e74656e74206f6620612070616765207768656e206c6f6f6b696e6720617420697473206c61796f75742e2054686520706f696e74206f66207573696e67204c6f72656d20497073756d2069732074686174206974206861732061206d6f72652d6f722d6c657373206e6f726d616c20646973747269627574696f6e206f66206c6574746572732c206173206f70706f73656420746f207573696e672027436f6e74656e7420686572652c20636f6e74656e742068657265272c206d616b696e67206974206c6f6f6b206c696b65207265616461626c6520456e676c6973682e204d616e79206465736b746f70207075626c697368696e67207061636b6167657320616e6420776562207061676520656469746f7273206e6f7720757365204c6f72656d20497073756d2061732074686569722064656661756c74206d6f64656c20746578742c20616e6420612073656172636820666f7220276c6f72656d20697073756d272077696c6c20756e636f766572206d616e7920776562207369746573207374696c6c20696e20746865697220696e66616e63792e20566172696f75732076657273696f6e7320686176652065766f6c766564206f766572207468652079656172732c20736f6d6574696d6573206279206163636964656e742c20736f6d6574696d6573206f6e20707572706f73652028696e6a65637465642068756d6f757220616e6420746865206c696b65292e3c2f703e0d0a202020202020202020202020202020202020202020202020202020202020202020, 0x20202020202020202020202020202020202020202020202020202020202020203c703e4c6f72656d20497073756d2069732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e57687920646f207765207573652069743f49742069732061206c6f6e672065737461626c6973686564206661637420746861742061207265616465722077696c6c206265206469737472616374656420627920746865207265616461626c6520636f6e74656e74206f6620612070616765207768656e206c6f6f6b696e6720617420697473206c61796f75742e2054686520706f696e74206f66207573696e67204c6f72656d20497073756d2069732074686174206974206861732061206d6f72652d6f722d6c657373206e6f726d616c20646973747269627574696f6e206f66206c6574746572732c206173206f70706f73656420746f207573696e672027436f6e74656e7420686572652c20636f6e74656e742068657265272c206d616b696e67206974206c6f6f6b206c696b65207265616461626c6520456e676c6973682e204d616e79206465736b746f70207075626c697368696e67207061636b6167657320616e6420776562207061676520656469746f7273206e6f7720757365204c6f72656d20497073756d2061732074686569722064656661756c74206d6f64656c20746578742c20616e6420612073656172636820666f7220276c6f72656d20697073756d272077696c6c20756e636f766572206d616e7920776562207369746573207374696c6c20696e20746865697220696e66616e63792e20566172696f75732076657273696f6e7320686176652065766f6c766564206f766572207468652079656172732c20736f6d6574696d6573206279206163636964656e742c20736f6d6574696d6573206f6e20707572706f73652028696e6a65637465642068756d6f757220616e6420746865206c696b65292e3c2f703e0d0a2020202020202020202020202020202020202020202020202020202020, 0x20202020202020202020202020202020202020202020202020202020202020203c703e4c6f72656d20497073756d2069732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e57687920646f207765207573652069743f49742069732061206c6f6e672065737461626c6973686564206661637420746861742061207265616465722077696c6c206265206469737472616374656420627920746865207265616461626c6520636f6e74656e74206f6620612070616765207768656e206c6f6f6b696e6720617420697473206c61796f75742e2054686520706f696e74206f66207573696e67204c6f72656d20497073756d2069732074686174206974206861732061206d6f72652d6f722d6c657373206e6f726d616c20646973747269627574696f6e206f66206c6574746572732c206173206f70706f73656420746f207573696e672027436f6e74656e7420686572652c20636f6e74656e742068657265272c206d616b696e67206974206c6f6f6b206c696b65207265616461626c6520456e676c6973682e204d616e79206465736b746f70207075626c697368696e67207061636b6167657320616e6420776562207061676520656469746f7273206e6f7720757365204c6f72656d20497073756d2061732074686569722064656661756c74206d6f64656c20746578742c20616e6420612073656172636820666f7220276c6f72656d20697073756d272077696c6c20756e636f766572206d616e7920776562207369746573207374696c6c20696e20746865697220696e66616e63792e20566172696f75732076657273696f6e7320686176652065766f6c766564206f766572207468652079656172732c20736f6d6574696d6573206279206163636964656e742c20736f6d6574696d6573206f6e20707572706f73652028696e6a65637465642068756d6f757220616e6420746865206c696b65292e3c2f703e0d0a202020202020202020202020202020202020202020202020202020202020202020, 0x20202020202020202020202020202020202020202020202020202020202020203c703e4c6f72656d20497073756d2069732073696d706c792064756d6d792074657874206f6620746865207072696e74696e6720616e64207479706573657474696e6720696e6475737472792e204c6f72656d20497073756d20686173206265656e2074686520696e6475737472792773207374616e646172642064756d6d79207465787420657665722073696e6365207468652031353030732c207768656e20616e20756e6b6e6f776e207072696e74657220746f6f6b20612067616c6c6579206f66207479706520616e6420736372616d626c656420697420746f206d616b65206120747970652073706563696d656e20626f6f6b2e20497420686173207375727669766564206e6f74206f6e6c7920666976652063656e7475726965732c2062757420616c736f20746865206c65617020696e746f20656c656374726f6e6963207479706573657474696e672c2072656d61696e696e6720657373656e7469616c6c7920756e6368616e6765642e2049742077617320706f70756c61726973656420696e207468652031393630732077697468207468652072656c65617365206f66204c657472617365742073686565747320636f6e7461696e696e67204c6f72656d20497073756d2070617373616765732c20616e64206d6f726520726563656e746c792077697468206465736b746f70207075626c697368696e6720736f667477617265206c696b6520416c64757320506167654d616b657220696e636c7564696e672076657273696f6e73206f66204c6f72656d20497073756d2e57687920646f207765207573652069743f49742069732061206c6f6e672065737461626c6973686564206661637420746861742061207265616465722077696c6c206265206469737472616374656420627920746865207265616461626c6520636f6e74656e74206f6620612070616765207768656e206c6f6f6b696e6720617420697473206c61796f75742e2054686520706f696e74206f66207573696e67204c6f72656d20497073756d2069732074686174206974206861732061206d6f72652d6f722d6c657373206e6f726d616c20646973747269627574696f6e206f66206c6574746572732c206173206f70706f73656420746f207573696e672027436f6e74656e7420686572652c20636f6e74656e742068657265272c206d616b696e67206974206c6f6f6b206c696b65207265616461626c6520456e676c6973682e204d616e79206465736b746f70207075626c697368696e67207061636b6167657320616e6420776562207061676520656469746f7273206e6f7720757365204c6f72656d20497073756d2061732074686569722064656661756c74206d6f64656c20746578742c20616e6420612073656172636820666f7220276c6f72656d20693c2f703e0d0a20202020202020202020202020202020202020202020202020202020202020, '1530275303')");

        // Table structure for table   houdinv_storesetting   

                $getDB->query("CREATE TABLE houdinv_storesetting (
            id int(220) NOT NULL,
            display_out_stock int(250) DEFAULT NULL,
            receieve_order_out int(250) DEFAULT NULL,
            custom_html_footer int(250) DEFAULT NULL,
            add_to_cart_button int(11) DEFAULT NULL,
            email_mandatory int(11) DEFAULT NULL,
            verify_mobile_numer int(11) DEFAULT NULL,
            otp_ int(11) DEFAULT NULL,
            otp_setting varchar(200) DEFAULT NULL,
            date_time varchar(200) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

          // Dumping data for table   houdinv_storesetting   

                $getDB->query("INSERT INTO houdinv_storesetting (id, display_out_stock, receieve_order_out, custom_html_footer, add_to_cart_button, email_mandatory, verify_mobile_numer, otp_, otp_setting, date_time) VALUES
        (1, 1, 1, 1, 1, 1, 1, 1, 'always_ask', '1530783363')");

        // Table structure for table   houdinv_sub_categories_one   

                $getDB->query("CREATE TABLE houdinv_sub_categories_one (
            houdinv_sub_category_one_id int(11) NOT NULL,
            houdinv_sub_category_one_name varchar(100) NOT NULL,
            houdinv_sub_category_one_main_id int(11) NOT NULL,
            houdinv_sub_category_one_created_date varchar(100) NOT NULL,
            houdinv_sub_category_one_updated_date varchar(100) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

          // Dumping data for table houdinv_sub_categories_one  

                $getDB->query("INSERT INTO houdinv_sub_categories_one (houdinv_sub_category_one_id, houdinv_sub_category_one_name, houdinv_sub_category_one_main_id, houdinv_sub_category_one_created_date, houdinv_sub_category_one_updated_date) VALUES
          (1, 'TopWear', 1, '1533015384', '1533021557'),
          (2, 'BottomWear', 2, '1533015384', '1533021557'),
          (3, 'FootWear', 3, '1533021512', '1533021557'),
          (4, 'Extra Size', 4, '1533021512', ''),
          (5, 'Sports', 5, '1534912601', '1534912601')");

        // Table structure for table   houdinv_sub_categories_two   

                $getDB->query("CREATE TABLE houdinv_sub_categories_two (
            houdinv_sub_category_two_id int(11) NOT NULL,
            houdinv_sub_category_two_name varchar(100) NOT NULL,
            houdinv_sub_category_two_sub1_id int(11) NOT NULL,
            houdinv_sub_category_two_main_id int(11) NOT NULL,
            houdinv_sub_category_two_created_date varchar(100) NOT NULL,
            houdinv_sub_category_two_updated_date varchar(100) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

          // Dumping data for table houdinv_sub_categories_two  

                $getDB->query("INSERT INTO houdinv_sub_categories_two (houdinv_sub_category_two_id, houdinv_sub_category_two_name, houdinv_sub_category_two_sub1_id, houdinv_sub_category_two_main_id, houdinv_sub_category_two_created_date, houdinv_sub_category_two_updated_date) VALUES
          (1, 'Casula shirts', 1, 8, '1533015384', '1533021557'),
          (2, 'Shirts', 1, 8, '1533015384', '1533021557'),
          (3, 'Adidas Neo', 2, 8, '1533015384', '1533021557'),
          (4, 'Floaters', 2, 8, '1533015384', '1533021557'),
          (5, 'Kurta', 6, 8, '1533021468', '1533021557'),
          (6, 'Jeans', 4, 4, '1533021512', '1533021512'),
          (7, 'Casual Shoes', 3, 8, '1533021512', '1533021557'),
          (8, 'Shorts', 3, 8, '1533021512', '1533021557'),
          (9, 'Sports Shoes', 7, 8, '1533021557', '1533021557'),
          (10, 'Formals', 8, 8, '1533021557', '1533021557'),
          (11, 'Shirts', 5, 1, '1534912601', '1534912601')");

        // Table structure for table   houdinv_suppliers   

                $getDB->query("CREATE TABLE houdinv_suppliers (
            houdinv_supplier_id int(10) UNSIGNED NOT NULL,
            houdinv_supplier_comapany_name varchar(150) DEFAULT NULL,
            houdinv_supplier_contact_person_name varchar(150) NOT NULL,
            houdinv_supplier_email varchar(150) NOT NULL,
            houdinv_supplier_contact varchar(20) NOT NULL,
            houdinv_supplier_landline varchar(20) DEFAULT NULL,
            houdinv_supplier_address varchar(500) NOT NULL,
            houdinv_supplier_active_status enum('active','deactive') NOT NULL,
            houdinv_supplier_created_at varchar(30) NOT NULL,
            houdinv_supplier_updated_at varchar(30) NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

          // Dumping data for table houdinv_suppliers  

                $getDB->query("INSERT INTO houdinv_suppliers (houdinv_supplier_id, houdinv_supplier_comapany_name, houdinv_supplier_contact_person_name, houdinv_supplier_email, houdinv_supplier_contact, houdinv_supplier_landline, houdinv_supplier_address, houdinv_supplier_active_status, houdinv_supplier_created_at, houdinv_supplier_updated_at) VALUES
          (4, 'hawkscode data', 'shivam', 'shivam199420@gmail.com', '+9177908 70946', '1234567890', 'tonk\r\nroad', 'active', '1530336572', '1530341836'),
          (5, 'Shivam', 'Hawkscode', 'hawkscodeteam@gmail.com', '077908 70946', '123456789', 'Kailash tower', 'active', '1530967267', '1530967316'),
          (6, 'Hawkscode', 'Hawkscode', 'hawksocde@gmail.com', '+91123456789', '123456789', 'hghgh', 'active', '1534239469', ''),
          (7, 'test', 'test', 'testghju@gmail.com', '+912345676567', NULL, ',lmsdfkldjk', 'active', '1536969600', '')");

        // Table structure for table   houdinv_supplier_products   

                $getDB->query("CREATE TABLE houdinv_supplier_products (
            houdinv_supplier_products_id int(11) NOT NULL,
            houdinv_supplier_products_supplier_id int(11) NOT NULL,
            houdinv_supplier_products_product_id int(11) NOT NULL,
            houdinv_supplier_products_created_at varchar(30) NOT NULL,
            houdinv_supplier_products_updated_at varchar(30) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_supplier_products  

                $getDB->query("INSERT INTO houdinv_supplier_products (houdinv_supplier_products_id, houdinv_supplier_products_supplier_id, houdinv_supplier_products_product_id, houdinv_supplier_products_created_at, houdinv_supplier_products_updated_at) VALUES
        (4, 5, 1, '1530967316', ''),
        (3, 4, 1, '1530341836', ''),
        (5, 5, 2, '1530967316', ''),
        (6, 4, 21, '1531897956', '1531897956'),
        (8, 5, 21, '1531899000', '1531899000'),
        (9, 4, 22, '1531899261', '1531899261'),
        (10, 4, 23, '1531899584', '1531899584'),
        (11, 5, 23, '1531899584', '1531899584'),
        (12, 4, 24, '1532496497', '1532496497'),
        (13, 4, 25, '1532518311', '1532518311'),
        (14, 4, 26, '1533032458', '1533032458'),
        (15, 4, 27, '1533037699', '1533037699'),
        (16, 4, 28, '1533191191', '1533191191'),
        (17, 4, 29, '1534239081', '1534239081'),
        (18, 5, 29, '1534239081', '1534239081'),
        (19, 6, 1, '1534239469', ''),
        (20, 6, 2, '1534239469', ''),
        (21, 4, 31, '1534848376', '1534848376'),
        (22, 5, 31, '1534848376', '1534848376'),
        (23, 6, 31, '1534848376', '1534848376'),
        (24, 4, 32, '1536192000', '1536192000'),
        (25, 5, 32, '1536192000', '1536192000'),
        (26, 6, 32, '1536192000', '1536192000'),
        (27, 5, 33, '1536624000', '1536624000'),
        (28, 6, 33, '1536624000', '1536624000'),
        (29, 4, 34, '1536710400', '1536710400'),
        (30, 5, 34, '1536710400', '1536710400'),
        (31, 6, 34, '1536710400', '1536710400'),
        (32, 5, 35, '1536710400', '1536710400'),
        (33, 4, 36, '1536710400', '1536710400'),
        (34, 4, 37, '1538179200', '1538179200'),
        (35, 5, 37, '1538179200', '1538179200'),
        (36, 6, 37, '1538179200', '1538179200'),
        (37, 7, 37, '1538179200', '1538179200')");

        // Table structure for table   houdinv_taxes   

                $getDB->query("CREATE TABLE houdinv_taxes (
            houdinv_tax_id int(11) NOT NULL,
            houdinv_tax_percentage float NOT NULL,
            houdinv_tax_country varchar(150) NOT NULL,
            houdinv_tax_description text NOT NULL,
            houdinv_tax_created_at timestamp NULL DEFAULT NULL,
            houdinv_tax_updated_at timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_taxsetting   

                $getDB->query("CREATE TABLE houdinv_taxsetting (
            id int(11) NOT NULL,
            taxsetting_number varchar(250) NOT NULL,
            date_time varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_taxsetting  

                $getDB->query("INSERT INTO houdinv_taxsetting (id, taxsetting_number, date_time) VALUES
        (1, 'GTGRUSBJ15469263DEEp', '1530540319')");

        // Table structure for table   houdinv_templates   

                $getDB->query("CREATE TABLE houdinv_templates (
            houdinv_template_id int(11) NOT NULL,
            houdinv_template_header_image text,
            houdinv_template_header_text tinytext,
            houdinv_template_header_sub_text text,
            houdinv_template_footer tinytext,
            houdinv_template_copyright varchar(200) DEFAULT NULL,
            houdinv_template_active_status tinyint(4) DEFAULT '0',
            houdinv_template_meta text,
            houdinv_template_file varchar(300) DEFAULT NULL COMMENT 'Location of template folder',
            houdinv_template_created_at timestamp NULL DEFAULT NULL,
            houdinv_template_updated_at timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_template_pages   

                $getDB->query("CREATE TABLE houdinv_template_pages (
            houdinv_template_page_id int(11) NOT NULL,
            houdinv_template_page_name varchar(200) NOT NULL,
            houdinv_template_page_slug varchar(250) NOT NULL,
            houdinv_template_page_body text NOT NULL,
            houdinv_template_page_meta text,
            houdinv_template_page_created_at timestamp NULL DEFAULT NULL,
            houdinv_template_page_updated_at timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_testimonials   

                $getDB->query("CREATE TABLE houdinv_testimonials (
            id int(250) NOT NULL,
            testimonials_name varchar(250) DEFAULT NULL,
            testimonials_image varchar(250) DEFAULT NULL,
            testimonials_feedback longblob,
            testimonials_publish int(10) DEFAULT '0',
            date_time varchar(250) DEFAULT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_testimonials  

                $getDB->query("INSERT INTO houdinv_testimonials (id, testimonials_name, testimonials_image, testimonials_feedback, testimonials_publish, date_time) VALUES
        (1, 'Test1', 'testimonials-1536710400.png', 0x48656c6c6f20476f6f642057656273697465, 1, '1536710400'),
        (2, 'Test Hawkscode', 'testimonials-1536710400.png', 0x6861776b73636f64207465616d207465737420666f722066656564206261636b, 1, '1536710400'),
        (3, 'Test 3 Update test', 'testimonials-1536710400.png', 0x6861776b73636f6465205468617264207465737420616c6c20636f6e74656e6374, 1, '1536710400')");

        // Table structure for table   houdinv_transaction   

                $getDB->query("CREATE TABLE houdinv_transaction (
            houdinv_transaction_id int(11) NOT NULL,
            houdinv_transaction_transaction_id varchar(155) DEFAULT NULL,
            houdinv_transaction_type enum('credit','debit') NOT NULL,
            houdinv_transaction_method enum('online','cash') NOT NULL,
            houdinv_transaction_from enum('payumoney','cash','pos','authorize','checque','card') NOT NULL,
            houdinv_transaction_for enum('order','sms package','email package','inventory purchase','Work Order','order refund','expense') NOT NULL,
            houdinv_transaction_for_id int(11) DEFAULT '0',
            houdinv_transaction_amount int(11) DEFAULT '0',
            houdinv_transaction_date date DEFAULT NULL,
            houdinv_transaction_status enum('success','fail') NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

             // Dumping data for table houdinv_transaction  
                $getDB->query("INSERT INTO houdinv_transaction (houdinv_transaction_id, houdinv_transaction_transaction_id, houdinv_transaction_type, houdinv_transaction_method, houdinv_transaction_from, houdinv_transaction_for, houdinv_transaction_for_id, houdinv_transaction_amount, houdinv_transaction_date, houdinv_transaction_status) VALUES
             (1, 'TXN123HGAy657', 'credit', 'online', 'payumoney', 'order', 70, 696, '2018-08-23', 'success'),
             (2, 'TXN123HGAy657', 'credit', 'online', 'payumoney', 'order', 70, 696, '2018-08-23', 'success'),
             (3, 'TXN123HGAy657', 'credit', 'online', 'payumoney', 'order', 71, 1337, '2018-08-23', 'success'),
             (4, 'TXN123HGAy657', 'debit', 'online', 'payumoney', 'sms package', 0, 240, '2018-08-24', 'success'),
             (5, 'TXN123HGAy657', 'debit', 'online', 'payumoney', 'sms package', 0, 240, '2018-08-24', 'success'),
             (6, 'TXN123HGAy657', 'debit', 'online', 'payumoney', 'email package', 0, 240, '2018-08-24', 'success'),
             (7, 'TXN123HGAy657', 'debit', 'online', 'payumoney', 'email package', 0, 240, '2018-08-24', 'success'),
             (8, 'TXN123HGAy657', 'debit', 'online', 'payumoney', 'email package', 0, 240, '2018-08-24', 'success'),
             (9, 'TXN76444491', 'credit', 'cash', 'pos', 'order', 98, 9988, '2018-08-28', 'success'),
             (10, 'TXN72065261', 'credit', 'cash', 'pos', 'order', 99, 10988, '2018-08-28', 'success'),
             (11, 'TXN13146464', 'credit', 'cash', 'cash', 'order', 100, 80, '2018-08-29', 'success'),
             (12, 'TXN49773141', 'credit', 'cash', 'cash', 'order', 100, 80, '2018-08-29', 'success'),
             (13, 'TXN30641999', 'credit', 'cash', 'cash', 'order', 100, 10, '2018-08-29', 'success'),
             (14, 'TXN54299853', 'credit', 'cash', 'cash', 'order', 99, 10, '2018-08-29', 'success'),
             (15, 'TXN49853995', 'credit', 'cash', 'cash', 'order', 36, 78, '2018-08-29', 'success'),
             (16, 'TXN97864327', 'credit', 'cash', 'cash', 'order', 71, 1337, '2018-08-29', 'success'),
             (17, 'TXN66823567', 'credit', 'cash', 'cash', 'order', 37, 78, '2018-08-29', 'success'),
             (22, 'TXN18679061', 'debit', 'cash', 'cash', 'inventory purchase', 2, 570, '2018-08-29', 'success'),
             (19, 'TXN63197300', 'debit', 'cash', 'cash', 'inventory purchase', 1, 260, '2018-08-29', 'success'),
             (21, 'TXN86782057', 'debit', 'cash', 'cash', 'inventory purchase', 1, 40, '2018-08-29', 'success'),
             (23, 'TXN86621144', 'debit', 'cash', 'cash', 'inventory purchase', 2, 100, '2018-08-29', 'success'),
             (29, 'Txn44685020', 'credit', 'online', 'payumoney', 'order', 119, 468, '2018-08-30', 'success'),
             (80, 'TXN71460792', 'credit', 'cash', 'card', 'expense', 2, 1200, '2018-09-17', 'success'),
             (30, 'Txn87724424', 'credit', 'online', 'payumoney', 'order', 120, 466, '2018-08-30', 'success'),
             (31, 'Txn87724424', 'credit', 'online', 'payumoney', 'order', 120, 466, '2018-08-30', 'success'),
             (32, 'TXN47309859', 'debit', 'cash', 'payumoney', 'order refund', 98, 9988, '2018-08-31', 'success'),
             (33, 'TXN46941473', 'credit', 'cash', 'cash', 'order', 96, 12, '2018-08-31', 'success'),
             (34, 'Txn28530568', 'debit', 'online', 'payumoney', 'email package', 0, 240, '2018-09-01', 'success'),
             (35, 'TXN12876598767', 'credit', 'online', 'payumoney', 'order', 166, 96, '2018-09-05', 'success'),
             (36, 'TXN12876598767', 'credit', 'online', 'payumoney', 'order', 166, 96, '2018-09-05', 'success'),
             (37, '2', 'credit', 'online', 'payumoney', 'order', 177, 102, '2018-09-05', 'success'),
             (38, '8382', 'credit', 'online', 'payumoney', 'order', 185, 123, '2018-09-05', 'success'),
             (39, '3358', 'credit', 'online', 'payumoney', 'order', 186, 123, '2018-09-05', 'success'),
             (46, '1536218367920', 'credit', 'online', 'payumoney', 'order', 194, 51678, '2018-09-06', 'fail'),
             (45, '1536218367920', 'credit', 'online', 'payumoney', 'order', 194, 51678, '2018-09-06', 'success'),
             (44, '1536218367920', 'credit', 'online', 'payumoney', 'order', 194, 51678, '2018-09-06', 'fail'),
             (47, '1536226019245', 'credit', 'online', 'payumoney', 'order', 207, 12078, '2018-09-06', 'fail'),
             (48, '1536226019245', 'credit', 'online', 'payumoney', 'order', 207, 12078, '2018-09-06', 'fail'),
             (49, '1536226019245', 'credit', 'online', 'payumoney', 'order', 207, 12078, '2018-09-06', 'fail'),
             (50, '1536226375071', 'credit', 'online', 'payumoney', 'order', 208, 16878, '2018-09-06', 'fail'),
             (51, '1536228539340', 'credit', 'online', 'payumoney', 'order', 209, 1278, '2018-09-06', 'fail'),
             (52, '1536233637166', 'credit', 'online', 'payumoney', 'order', 210, 1278, '2018-09-06', 'fail'),
             (53, '1536233857273', 'credit', 'online', 'payumoney', 'order', 211, 1278, '2018-09-06', 'fail'),
             (54, '40018185670', 'credit', 'online', 'authorize', 'order', 217, 468, '2018-09-07', 'success'),
             (55, '2333199301', 'credit', 'online', 'authorize', 'order', 218, 464, '2018-09-07', 'fail'),
             (56, '40018185994', 'credit', 'online', 'authorize', 'order', 220, 1656, '2018-09-07', 'success'),
             (57, '40018186028', 'credit', 'online', 'authorize', 'order', 221, 1656, '2018-09-07', 'success'),
             (58, '1536322036529', 'credit', 'online', 'payumoney', 'order', 224, 1125, '2018-09-07', 'fail'),
             (59, '1536324294883', 'credit', 'online', 'payumoney', 'order', 230, 1278, '2018-09-07', 'fail'),
             (60, '60108067754', 'credit', 'online', 'authorize', 'order', 231, 466, '2018-09-07', 'success'),
             (61, '1536554926386', 'credit', 'online', 'payumoney', 'order', 235, 1278, '2018-09-10', 'fail'),
             (62, '40018276999', 'credit', 'online', 'payumoney', 'order', 250, 1278, '2018-09-10', 'success'),
             (63, '40018277261', 'credit', 'online', 'payumoney', 'order', 251, 1278, '2018-09-10', 'success'),
             (64, '40018277722', 'credit', 'online', 'payumoney', 'order', 252, 1278, '2018-09-10', 'success'),
             (65, '40018301503', 'credit', 'online', 'payumoney', 'order', 256, 1278, '2018-09-10', 'success'),
             (66, '3738389950', 'credit', 'online', 'payumoney', 'order', 260, 1278, '2018-09-10', 'fail'),
             (67, '60108186264', 'credit', 'online', 'payumoney', 'order', 270, 1282, '2018-09-10', 'success'),
             (68, '60108186939', 'credit', 'online', 'payumoney', 'order', 273, 1278, '2018-09-10', 'success'),
             (69, '60108187339', 'credit', 'online', 'payumoney', 'order', 280, 1278, '2018-09-10', 'success'),
             (70, '60108187587', 'credit', 'online', 'payumoney', 'order', 281, 1278, '2018-09-10', 'success'),
             (71, '60108187663', 'credit', 'online', 'payumoney', 'order', 282, 1278, '2018-09-10', 'success'),
             (72, '4250722893', 'credit', 'online', 'payumoney', 'order', 284, 1278, '2018-09-10', 'success'),
             (73, 'Txn16068381', 'credit', 'online', 'payumoney', 'order', 296, 860, '2018-09-12', 'success'),
             (74, 'Txn57729368', 'credit', 'online', 'payumoney', 'order', 297, 6212, '2018-09-13', 'success'),
             (75, 'Txn99751393', 'credit', 'online', 'payumoney', 'order', 337, 3708, '2018-09-14', 'success'),
             (76, 'Txn31114288', 'credit', 'online', 'payumoney', 'order', 342, 2059, '2018-09-14', 'success'),
             (79, 'TXN41763155', 'debit', 'cash', 'card', 'expense', 1, 6024, '2018-09-20', 'success'),
             (81, '2257624043', 'credit', 'online', 'authorize', 'order', 372, 6648, '2018-09-18', 'fail'),
             (82, '4241744089', 'credit', 'online', 'payumoney', 'order', 373, 478, '2018-09-18', 'success'),
             (83, '8064223330', 'credit', 'online', 'payumoney', 'order', 376, 679, '2018-09-18', 'fail'),
             (84, '6639494155', 'credit', 'online', 'payumoney', 'order', 379, 679, '2018-09-18', 'fail'),
             (85, '40018651064', 'credit', 'online', 'payumoney', 'order', 392, 15556, '2018-09-19', 'success'),
             (86, '60108689447', 'credit', 'online', 'payumoney', 'order', 406, 683, '2018-09-20', 'success'),
             (87, '60108695961', 'credit', 'online', 'payumoney', 'order', 411, 728, '2018-09-20', 'success'),
             (88, '1538397914857', 'credit', 'online', 'payumoney', 'order', 466, 679, '2018-10-01', 'fail'),
             (89, '1538399114965', 'credit', 'online', 'payumoney', 'order', 472, 3978, '2018-10-01', 'fail')");

        // Table structure for table   houdinv_users   

                $getDB->query("CREATE TABLE houdinv_users (
            houdinv_user_id int(10) UNSIGNED NOT NULL,
            houdinv_user_name varchar(150) NOT NULL,
            houdinv_user_email varchar(150) NOT NULL,
            houdinv_user_password varchar(150) NOT NULL,
            houdin_user_password_salt varchar(100) NOT NULL,
            houdinv_user_contact varchar(150) NOT NULL,
            houdinv_users_profile varchar(100) NOT NULL,
            houdinv_users_gender enum('no','male','female') NOT NULL,
            houdinv_users_dob date DEFAULT NULL,
            houdinv_users_device_id text,
            houdinv_users_pending_amount int(11) NOT NULL DEFAULT '0',
            houdinv_users_privilage float NOT NULL DEFAULT '0',
            houdinv_user_is_verified tinyint(4) NOT NULL DEFAULT '0',
            houdinv_user_is_active enum('deactive','active') NOT NULL,
            houdinv_user_created_at varchar(30) DEFAULT NULL,
            houdinv_user_updated_at varchar(30) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_users  

                $getDB->query("INSERT INTO houdinv_users (houdinv_user_id, houdinv_user_name, houdinv_user_email, houdinv_user_password, houdin_user_password_salt, houdinv_user_contact, houdinv_users_profile, houdinv_users_gender, houdinv_users_dob, houdinv_users_device_id, houdinv_users_pending_amount, houdinv_users_privilage, houdinv_user_is_verified, houdinv_user_is_active, houdinv_user_created_at, houdinv_user_updated_at) VALUES
        (2, 'payal', 'hawkscodeteam@gmail.com', '$2y$10$V.iP6xNuxa9u6U/s1Zu./OlPO/zjVVJMJG27ORG0yHOdbkvrvnAdm', '$2y$10$V.iP6xNuxa9u6U/s1Zu./OssmlTIFHTj6rpctzwMq6IIyAEr3eqdW', '9785940038', '', 'female', '1995-11-10', NULL, 48, 12, 1, 'active', '1532941882', '1533864954'),
        (3, 'Shivam Sengar', 'shivam199520@gmail.com', '$2y$10$b8BCuyGVb1yNrqKYl79GbebDx6V6/qUEYpdCqb8ACS8Brvz/WwBZO', '$2y$10$b8BCuyGVb1yNrqKYl79Gbe7.kQMWTstVtTi1yA6coE/kJWik0iQvy', '+917790870946', '', 'male', '0000-00-00', NULL, 0, 123, 1, 'active', '1533012305', ''),
        (4, 'Shivam Sengar', 'shivam1995220@gmail.com', '$2y$10$WsSwJwvDNVYvAkVBksDxR.eeGt6cAf5hK366pPm/L37VvYU9yfFFu', '$2y$10$WsSwJwvDNVYvAkVBksDxR.pevempCuu1v6MCA235Obss0vwWYeGvW', '+917790870946', '', 'male', '0000-00-00', NULL, 0, 0, 1, 'active', '1533012351', ''),
        (5, 'goyal', 'shivam.hawkscode@gmail.com', '$2y$10$RX/8pd.rPWY5Nqkd9PXU0ezPsNZwWZ1Gy5IEXYv3zT1x.L/Y336HO', '$2y$10$RX/8pd.rPWY5Nqkd9PXU0eN/7C9Ta6AfQzABBs7A.jycCR5PwEGN6', '+919785940038', '380689image001.jpg', 'female', '2018-08-16', NULL, 0, 0, 1, 'active', '1533014308', '1534144214'),
        (6, 'vipin', 'abc@gmail.com', '$2y$10$Iwrp85K.J.T56PdqDw43JOjjXTeW4LN8I/a/lt1xgrHn2QjsBaTKe', '$2y$10$Iwrp85K.J.T56PdqDw43JOi8Q/SMBY82PkxpnldhuYCZ8PjYHHrDq', '1234567890', '784709image.png', 'male', '2018-02-03', NULL, 0, 0, 1, 'active', '1533035285', '1533281102'),
        (7, 'abc abc', 'abcd@gmail.com', '$2y$10$vebwpoHydSbV6RM2qwZdrenYyedzUjt8TuxCb9DOpRp27fDrFOMN2', '$2y$10$vebwpoHydSbV6RM2qwZdren1cPLJk427294GXEIwudFhrQkZR4PCe', '9929904141', '', 'male', '0000-00-00', NULL, 0, 0, 1, 'active', '1533035859', ''),
        (8, 'tst sdfsdfsd', 'test@gmail.com', '$2y$10$4dBfNqhj2YwSOdyr0wjgv.7yc4s5TbqpIneQd2rR80Cxm3wOxKO7q', '$2y$10$4dBfNqhj2YwSOdyr0wjgv.S9IocoCVawM.eKmME/m0YgzaTt/1a1q', '+911234567890', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1533296452', ''),
        (9, 'a s', 'acf@gmail.com', '$2y$10$5Gm61GxDhzGp2mag98BPB.Udukv8BThKdXr6gPxTkpiMUwU3MCEw6', '$2y$10$5Gm61GxDhzGp2mag98BPB.XnkmTOdCZ58BhrzrA39ROk91yrsWE6G', '1234567890', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1533735566', ''),
        (10, 'a b', 'DK@gmail.com', '$2y$10$aIvbBO48gVBCjMSbTcUqR.H9tEx6ADQrocoL9p7lZn80KSibYOrS6', '$2y$10$aIvbBO48gVBCjMSbTcUqR.fBYm9JLCrAjshv3GCxWmbV8nyiyYBeW', '1234567890', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1533789942', ''),
        (11, '2a b', 'demo@gmail.com', '$2y$10$bOCbRYaOMKmqB7mEuvcelujKF7liYLGHdCtG7855mtsMvmCG3e6/6', '$2y$10$bOCbRYaOMKmqB7mEuvcelu4mBCNuUNM4nxijftgSl1bu.oytGJf6m', '16', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1533790052', ''),
        (12, '2a b', 'dem@gmail.com', '$2y$10$Na7.09/CdvLhB8r.Yi/vu.C/DCwrFs8UfyyVEIeHboI0Yjx6suMhm', '$2y$10$Na7.09/CdvLhB8r.Yi/vu.fJioapx4VouGZaG1VxdQyT.YEyEk9fi', '16', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1533793553', ''),
        (13, 'abc abc', 'th@gmail.com', '$2y$10$JdqcJMuF0iOAHmX0uC9p2uWkp5Mm3VKPolI/GS53N8jpPC1zz5mUy', '$2y$10$JdqcJMuF0iOAHmX0uC9p2uF7ddHZplxzebw.ZdTxvmwbtCWPELcRe', '9876643456', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1533794354', ''),
        (14, 'ashok saint', 'ashok@gmail.com', '$2y$10$HRR2DcZQhs9heFhNcGBz0ufB5/klcKUaUnMsj8DJpL8bzG5wok.li', '$2y$10$HRR2DcZQhs9heFhNcGBz0uPLUcaMwa1kSmA/FsZz.3MCbvvk1tV7e', '12345', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1533794470', ''),
        (15, 'Ashok', 'asc@gmail.com', '$2y$10$tGZquZWy6ugeiVh6FHHVmOmTOn..PI/65jbB8TUooU/be5URF44qC', '$2y$10$tGZquZWy6ugeiVh6FHHVmOge3OAROtLFNwemJD8/NCD4u3TH2rEmS', '0000000000000', '', 'male', '1995-02-03', NULL, 0, 0, 1, 'active', '1533795395', '1534730461'),
        (16, 'Ashok saini', 'qwe@gmail.com', '$2y$10$Ugm.MQnVfjFZadTA6cqbpeAH9jw7Ak9y/NAB3csqUoowEYSQmHcBi', '$2y$10$Ugm.MQnVfjFZadTA6cqbpe5n1sOOcevcRFP32TPjHFrJ8m0T0GLEu', '12345', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1533795753', ''),
        (17, 'ohh for', 'hg@gmail.com', '$2y$10$m9h7bEF7/3zsJPd2H684RuIy28o4UDv5cIBTY4Kwd5lhV64k/654K', '$2y$10$m9h7bEF7/3zsJPd2H684RueP74DUe6hf7NCnsL9LvHVWLkB3IZKO6', '1234567890', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1533796257', ''),
        (18, 'bhupi', 'bhupi@gmal.com', '', '', '97855539620', '', 'male', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (19, 'dat', 'd@gmail.com', '', '', '9755225555', '', 'male', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (20, 'suraj', 's@gmail.com', '', '', '45282556545', '', 'male', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (22, 'bhupi', 'placerat@risus.net', '', '', '97855539620', '', 'male', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (23, 'dat', 'porttitor.scelerisque@Crasdictum.net', '', '', '9755225555', '', 'male', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (24, 'suraj', 'Phasellus.dapibus.quam@metus.net', '', '', '45282556545', '', 'male', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (25, 'suraj', 'aliquet.vel.vulputate@tellus.org', '', '', '45282556545', '', 'male', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (26, 'suraj', 'odio.Etiam@fringillaDonec.ca', '', '', '45282556545', '', 'male', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (27, 'ttu', 'vitae.orci@anteMaecenas.net', '', '', '', '', '', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (28, 'tyu', 'habitant@Duisdignissim.net', '', '', '', '', '', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (29, 'fjf', 'enim.condimentum.eget@sit.net', '', '', '', '', '', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (30, 'fhjfj', 'diam.eu@auctor.ca', '', '', '', '', '', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (31, 'bhupi', 'hawkscode12@gmail.com', '', '', '97855539620', '', 'male', '0000-00-00', NULL, 0, 0, 0, 'deactive', '', ''),
        (32, 'vipin pareek', 'vipin@gmail.com', '$2y$10$LfTYlWBOJ.zSFnl2Qyhps.9wfySMm6et3r0a2.c.B.cd0cb//rufm', '$2y$10$LfTYlWBOJ.zSFnl2Qyhps.2wFui.rgmU21SR8zbO6IP8fUpb6NkIK', '9929904141', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1534842004', ''),
        (42, 'shivam sengar', 'shivam199420@gmail.com', '$2y$10$Rbsq8tmZXZAu0xY/oEqX9uccQeQiAq/TsN9b8IS0npg4NsQJSy1KK', '$2y$10$Rbsq8tmZXZAu0xY/oEqX9uoQDPoKony0ow6F37p5sdhBB8VMLgXIK', '7790870946', '215579image.png', 'male', '1996-05-18', NULL, 0, 0, 1, 'active', '1535020703', '1537442243'),
        (43, 'payal goyal', 'goyalpayal@gmail.com', '$2y$10$5UOGoxP.vdKCpQzJRIDyWug7PaNkmKGqrETabENHfOJov7u/BV7RS', '$2y$10$5UOGoxP.vdKCpQzJRIDyWuOSJxnn3N3jsvToJz1cbhnykPMzBkYAG', '+919785940038', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1535030136', ''),
        (44, 'kbcvjjj', 'goyalpayal1995@gmail.com', '$2y$10$iQD9D2c98bCSMFULaSWFbuM4aMpbePbEOZ108udNypHckOE/AS5ae', '$2y$10$iQD9D2c98bCSMFULaSWFbuGoIj/1tgAhasLwZdafHYIHEljuT1vCC', '+91978594003888', '2858011538397595596.jpg', 'female', '1995-02-04', 'e43ABxTfM_o:APA91bGEUQimFiBvWFY5IeC742o0Me-19nv3hY7M2FLZjnbS-YOnTajpFGSdmD0hL5vT3Gcjg4VoKZPrVg26fYl8kIrrs0D967ml1qOQTfLsbSZEE8RWTFNdcocM7dJV7rmhVB5V5FiV', 0, 0, 1, 'active', '1535030368', '1538397605'),
        (45, 'first  customer', 'first@gmail.com', '$2y$10$IT.XtP8Wybj/8irKlQlx5OwACHsExMch1VXTQ/VRagI.k8Mi561IK', '$2y$10$IT.XtP8Wybj/8irKlQlx5ObbGqfrOX2W9T2y5mjtXA/bhNEgggaVK', '987654321', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1535044159', ''),
        (46, 'pa go', 'goyal@gmail.com', '$2y$10$hS6m6ne/qWQUrObWGs0YSOXxrPgKWm06oxs4DqH.cx6rdmddXVsM2', '$2y$10$hS6m6ne/qWQUrObWGs0YSO3PAl.anvgCo5/1scR1WORdt9EXegsC2', '8058049473', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1535087671', ''),
        (47, 'Deepak sharma', 'deepsharma906@gmail.com', '$2y$10$bntPeWkFnFRYnu6pB.sBdeRRtN.CmIjEDthPVHBbSrt.42A.HIsDa', '$2y$10$bntPeWkFnFRYnu6pB.sBdei888f/kl44ek9JHUvZNey6KBmw.FvHe', 'deepsharma906@gmail.com', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1535093266', ''),
        (48, 'Geetikaaa mathru', 'mj@gmail.com', '$2y$10$aifRQJrufRteKMbBLC85quMHFHk5DxMSIpmXl4/qoxwdf53XJ1W1.', '$2y$10$aifRQJrufRteKMbBLC85quzu1TPt3Nr5pZ1FK/xV/AnTNPJIqyyc2', '9799584525', '391990image.png', 'female', '1998-03-07', NULL, 0, 0, 1, 'active', '1535348938', '1537180419'),
        (49, 'as as', 'as@gmail.com', '$2y$10$s.x5cKRC3Lp5OgD8wWsiKeIhwpC7KMdh.3zbQm0yruFdlPBhcJp9q', '$2y$10$s.x5cKRC3Lp5OgD8wWsiKe7SrcN3PoOO8wxyxueAwUu3Z5jiylDIS', '1234567890', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1535521487', ''),
        (50, 'Geetika Mathur', 'mgeetika11@gmail.com', '$2y$10$doJfOBGVm.9OrSxeoJXhmOi7jqVzm5TEeyL7TvmuhBKLiP5gY2iNi', '$2y$10$doJfOBGVm.9OrSxeoJXhmOu3Cz5w3tPF2/4tXHILVg64vq5jPqnau', '+919799584524', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1536295266', ''),
        (51, 'Geetika Mathur', 'geetika565@gmail.com', '$2y$10$HHvja3YxBCLarL9Kc4AXNOIy0O8i38ItTKjDCe5RIT13xotnmJauK', '$2y$10$HHvja3YxBCLarL9Kc4AXNO3n.Wz4S4qPANojOlM/9bio3cGAsGTU2', '+919799584524', '446255canvasLog1.jpg', 'female', '2018-01-10', NULL, 0, 0, 1, 'active', '1536295532', '1536299967'),
        (52, 'simran', 'payal123@gmail.com', '$2y$10$6ICfAyLZz.b.u5uKRn7tOu9nquVvCxW7KJGWOet7DRH6AZ69unify', '$2y$10$6ICfAyLZz.b.u5uKRn7tOu8m5YQ0OEIjeDixujjrB4xecKwIeXEXC', '+918058049473', '230675images.jpeg', 'female', '2018-09-13', NULL, 0, 0, 1, 'active', '1536296136', '1536296231'),
        (53, 'departmental data', 'department@gmail.com', '$2y$10$Lo3VupPYO41jLQOneRWD9uFGYPp/159eLYWN2/5vpAET3K3oNYsp.', '$2y$10$Lo3VupPYO41jLQOneRWD9umrfxz/bEL7c0raT.9wUPtRV1rhJm/Gq', '+917790870946', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1536581810', ''),
        (54, 'Geetika Mathur', 'anshul.mathur08@gmail.com', '$2y$10$cK9DhgXTU/XjioFi0BEkQeWMBeEbXDmy0HBGTxZEWAAajHaKyeIJy', '$2y$10$cK9DhgXTU/XjioFi0BEkQeSq0bTZrZnLF6R0gMXammdVD3SEUqEl.', '+919799584524', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1536638710', ''),
        (55, 'shivam sengar', 'sengar1994shivam@hotmail.com', '$2y$10$ozMEGaUcp0bOumjOY/dpNOZo3F1YT3du8l7DcIJoGi.EaFB7PcoMG', '$2y$10$ozMEGaUcp0bOumjOY/dpNOGbPCg.7yz./TLf1DY/Fw331ihT7kXAC', '+917790870946', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1536671160', ''),
        (61, 'general steor', 'general@gmail.com', '$2y$10$y1y4TpRy8ZvL4a4pRfZz2.3L384a9RHScpnd4RvxjYSG3bfnCPAlG', '$2y$10$y1y4TpRy8ZvL4a4pRfZz2.JI8OjaN6GwKRZcYTwDJzALIVluPYvs.', '+917790870946', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1536831297', ''),
        (62, 'payal goyal', 'payal@gmail.com', '$2y$10$FGONhvmDArZMrwlrsn6A3eAydhJlA5VKT.5yUDglDGu133VnhF5la', '$2y$10$FGONhvmDArZMrwlrsn6A3eQJ5hog3WYhFFOtrQeuIrBr9juP9WLTq', '+919785940038', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1536844683', ''),
        (63, 'shivam', 'testsdfe@gmail.com', '$2y$10$0ozW2I4N1ErS.73IFxSGbuDBRGyvCoDZNZ5IokqFK52UoZgsCxjPi', '$2y$10$0ozW2I4N1ErS.73IFxSGbuDBRGyvCoDZNZ5IokqFK52UoZgsCxjPi', '+918543216787', '', 'no', '0000-00-00', NULL, 0, 0, 1, '', '1536969600', ''),
        (64, 'cttv vgvg', 'golu@gmail.com', '$2y$10$Oq7OaRdr5YiR/YXvyFOXhOg3aMMmXehuIb1ZJMAaNd6rGf9D1s4h.', '$2y$10$Oq7OaRdr5YiR/YXvyFOXhOGZRyLd2n5F1YuYaXFoJdOkc7LXd2bii', '+919785940038', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1536991015', ''),
        (65, 'testh', 'jdfnjkd@gmail.com', '$2y$10$mtRDcX.gqY8VpQtcjletaeTz4mHhxxhjMxOlRX0iu6cO.mKi8dH8u', '$2y$10$mtRDcX.gqY8VpQtcjletaeTz4mHhxxhjMxOlRX0iu6cO.mKi8dH8u', '+9134454654645634', '', 'no', '0000-00-00', NULL, 0, 0, 1, 'active', '1536969600', ''),
        (66, 'hello', 'hellotehs@gmail.com', '$2y$10$uZWDDqkCcFciVNLE9.nhKepI/i/TNQgxTABUaFRnT4voIl.q0RYvK', '$2y$10$uZWDDqkCcFciVNLE9.nhKepI/i/TNQgxTABUaFRnT4voIl.q0RYvK', '+91764367877', '', 'no', NULL, NULL, 0, 0, 1, 'active', '1536969600', NULL),
        (67, 'bhupendra kumar', 'bhupendra@hawkscode.com', '$2y$10$aHpRmn3s7sSbKAefw0dr9urKn0jv2dumgSS22cep1WLDV415FOoVK', '$2y$10$aHpRmn3s7sSbKAefw0dr9uJC02SLcatBf/mct7vhOLaQ2BeS4Ququ', '00000000000000000000', '', 'no', NULL, NULL, 0, 0, 1, 'active', '1537005286', NULL),
        (68, 'shivam senge', 'shivam@gmail.com', '$2y$10$uAjreXN4lPFIZVIk9k/xIOfIK/5uy3ndjKKN0rrkSa4/jE38T/6ha', '$2y$10$uAjreXN4lPFIZVIk9k/xIO2QygZbAdAIYP0sX/kCIgGjjOdcd.rj.', '7790870946', '', 'no', NULL, NULL, 0, 0, 1, 'active', '1537247447', NULL),
        (69, 'Atul shelke', 'atulshelke617@gmail.com', '$2y$10$6KFR5OS/jfLfHUk0dN9M6O/cKSC.7acEhfEfgtAC0iiltywl9scg6', '$2y$10$6KFR5OS/jfLfHUk0dN9M6O5VDejTWQCM2ca2jTbmehkgsG42nh/Ty', '9782627342', '', 'no', NULL, NULL, 0, 0, 1, 'active', '1537250294', '1537250406'),
        (70, 'vipin pareek', 'qwert@gmail.com', '$2y$10$bxsJMBZWyq4TxAE7.VK8Z.XhIySJfMQotQrG0GKL4H3ZAeNU9bMw.', '$2y$10$bxsJMBZWyq4TxAE7.VK8Z.wkskwo9uYeOEnxI1P45kV23CGcIORx6', '123456789', '754414image.png', 'no', '2019-09-19', 'euNepCJ8INs:APA91bH35TbesBt5e-_ZBVTRsOk2jh1DWKEvBRdqpAUajt5-h_ieiNrHbcXMnFflBKohhc5ywHYi951imyFHz9gjBELQQ1DpHgOntzxPfzSJi5ElYKTK3mOwoaYLzkXKmn3WPyx05jCb', 0, 0, 1, 'active', '1537262098', '1537355234'),
        (71, 'shivam sengar', 'shivam1994201@gmail.com', '$2y$10$wZD6CeTFOL0CSihrq2v2sO9MU1r8TjEDBwmPqGnYhvR2zl/1hZadO', '$2y$10$wZD6CeTFOL0CSihrq2v2sOuslhaFGZKsJhvfgLGdA2.Y4veDZPmmO', '+917790870946', '', 'no', NULL, NULL, 0, 0, 1, 'active', '1537347558', NULL),
        (72, 'paa goaa', 'goyal1@gmail.com', '$2y$10$LrvcqazAm1oX/qzPUxDW..PEzN1KbftylpQjzGIYaWn4If2ER7rW6', '$2y$10$LrvcqazAm1oX/qzPUxDW..oRbdbHVAl7AhN1xrKt9CS3/mwzBYddu', '+919785940068', '', 'no', NULL, NULL, 0, 0, 1, 'active', '1537435187', NULL),
        (73, 'hjjj jjj', 'gunjan@gmail.com', '$2y$10$N6OVVAT4IUbQSYXaHA4AqeE2OwAxmYb2CrrLIn75faLHu.aFrS.IC', '$2y$10$N6OVVAT4IUbQSYXaHA4AqeASaVHFnwBUZxgRNAaWrH7vRmGx/oMeO', '+918058049473', '', 'no', NULL, NULL, 0, 0, 1, 'active', '1537435434', NULL),
        (74, 'jkk hjj', 'baby@gmail.com', '$2y$10$BIc0bLsNKxDvWimYe7eak.GNRPSsKa5TvJEaFPBIBWkMcLfrRKro6', '$2y$10$BIc0bLsNKxDvWimYe7eak.rTb.pyWusRA4B2d7A/nES1sljFJGxu6', '+91864997966', '', 'no', NULL, NULL, 0, 0, 1, 'active', '1537435702', NULL),
        (75, 'hjj hjjcgcycyc', 'geetikamaaaa@gmail.com', '$2y$10$E9SmeWgLruyock1YJSuTJumqnuylWLu7Qv/TXCV0yeNTHu05oVVOe', '$2y$10$E9SmeWgLruyock1YJSuTJuYYgaAeDfqiHYdIAoNHeqmMW6JkQoOQ6', '+918349679607686868', '164512canvasLog3.jpg', 'female', '0000-00-00', NULL, 0, 0, 1, 'active', '1537436129', '1537437460'),
        (76, 'iijj hjjj', 'reema@gmail.com', '$2y$10$51pDb.hY.uT3aFBdYix4LeuVkfgjDPUYRNrs0n2SNqDQE/3adizRu', '$2y$10$51pDb.hY.uT3aFBdYix4Leiaey2oqKj39R3PQDO7d4q5GX/7Df6xS', '+919865998', '613148splash.jpg', 'male', '2018-09-07', NULL, 0, 0, 1, 'active', '1537439343', '1537690576'),
        (77, 'hdkdk djjd', 'gola@gmail.com', '$2y$10$87PyHSNadLqeYqCQw9wJ.OaG.IYOLwH14pRm4u22iWAV5b2e5jtKC', '$2y$10$87PyHSNadLqeYqCQw9wJ.OopHD4R/kCXKVXvp1cFqVp/i7B4KgBUm', '+91466464', '193012Screenshot_20180920_065840.png', 'female', '2018-09-12', NULL, 0, 0, 1, 'active', '1537440582', '1537440751'),
        (78, 'gfhfyc fufuf', 'sir@gmail.com', '$2y$10$x205eOQ73mQjJxnrGdJp0elPcb9zqCcIzggclJccjm5D/BavZpMkW', '$2y$10$x205eOQ73mQjJxnrGdJp0ezxpmwG7Y/GLmm/XmOPzwFMftUBakvMC', '+919785940068', '9309971537442735330.jpg', 'female', '2018-09-29', NULL, 0, 0, 1, 'active', '1537442633', '1537442795'),
        (79, 'Arpit Tiwari', 'a9571630507@gmail.com', '$2y$10$9jtZsdaWngTOQxMOV4r.6e7pYe38h620M3.o.hEUDh01jiZcOQrvy', '$2y$10$9jtZsdaWngTOQxMOV4r.6eDBwSNPCc01c2MwQbJeIt5YmNVRHIlXy', '7980150955', '', 'no', NULL, NULL, 0, 0, 1, 'active', '1538138094', '1538138909'),
        (80, 'test test', 'tetststssttdtdt@gmail.com', '$2y$10$XVZlXFMlR3D2GFJSJT8meeZQejLzND1LQ6WqvD9Ebb95HKxbiVcFS', '$2y$10$XVZlXFMlR3D2GFJSJT8mee1f9v8TkU8DYNDVr3zn1hmVB6n54GHq2', '1234567890', '', 'no', NULL, NULL, 0, 0, 1, 'active', '1538476221', NULL),
        (81, 'XYZ', '859658@xyz.com', '$2y$10$PpAQnfNxG4gNaoXMXrxs1ey19iS6Mn5N3/CbQlJeS0LTtBnpguUjy', '$2y$10$PpAQnfNxG4gNaoXMXrxs1ey19iS6Mn5N3/CbQlJeS0LTtBnpguUjy', '85858585858', '', 'no', NULL, NULL, 0, 0, 1, 'active', '1538438400', NULL)");

        // Table structure for table   houdinv_users_cart   

                $getDB->query("CREATE TABLE houdinv_users_cart (
            houdinv_users_cart_id int(11) NOT NULL,
            houdinv_users_cart_user_id int(11) NOT NULL,
            houdinv_users_cart_item_id int(11) DEFAULT '0',
            houdinv_users_cart_item_variant_id int(11) DEFAULT '0',
            houdinv_users_cart_item_count int(11) NOT NULL,
            houdinv_users_cart_add_date varchar(100) NOT NULL,
            houdinv_users_cart_update_date varchar(100) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_users_cart

                $getDB->query("INSERT INTO houdinv_users_cart (houdinv_users_cart_id, houdinv_users_cart_user_id, houdinv_users_cart_item_id, houdinv_users_cart_item_variant_id, houdinv_users_cart_item_count, houdinv_users_cart_add_date, houdinv_users_cart_update_date) VALUES
        (220, 79, 8, 0, 7, '1538097696', '1538100352'),
        (316, 2, 36, 0, 1, '1538551534', '1538551534'),
        (291, 2, 8, 0, 12, '1538356918', '1538550512'),
        (219, 79, 7, 0, 4, '1538097695', '1538097696'),
        (290, 2, 20, 0, 1, '1538356494', '1538550512'),
        (215, 79, 32, 0, 6, '1538097686', '1538097687'),
        (278, 42, 9, 0, 5, '1538392279', '1538550923'),
        (315, 2, 37, 0, 1, '1538548809', '1538550512'),
        (301, 2, 2, 0, 1, '1538465417', '1538550512'),
        (194, 76, 1, 0, 1, '1537855659', '1537856166'),
        (237, 42, 34, 0, 4, '1538207404', '1538550923'),
        (221, 79, 9, 0, 1, '1538097698', '1538100352'),
        (222, 79, 20, 0, 4, '1538097698', '1538100352'),
        (243, 42, 8, 0, 3, '1538221790', '1538550923'),
        (292, 42, 7, 0, 1, '1538357311', '1538357311'),
        (280, 42, 32, 0, 1, '1538392283', '1538392283')");

        // Table structure for table   houdinv_users_forgot   

                $getDB->query("CREATE TABLE houdinv_users_forgot (
            houdinv_users_forgot_id int(11) NOT NULL,
            houdinv_users_forgot_user_id int(11) NOT NULL,
            houdinv_users_forgot_token int(4) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_users_forgot   

                $getDB->query("INSERT INTO houdinv_users_forgot (houdinv_users_forgot_id, houdinv_users_forgot_user_id, houdinv_users_forgot_token) VALUES
        (4, 5, 9936),
        (3, 5, 7043),
        (5, 5, 4447),
        (6, 5, 7817),
        (8, 6, 1455),
        (9, 6, 4092),
        (10, 6, 2416),
        (11, 5, 4587),
        (18, 5, 4683),
        (19, 5, 6855),
        (20, 5, 5894),
        (21, 6, 1932),
        (22, 5, 7676),
        (23, 5, 4052),
        (24, 5, 5067),
        (25, 5, 8146),
        (26, 5, 6379),
        (27, 5, 5133),
        (28, 5, 9007),
        (29, 5, 1462),
        (30, 5, 8956),
        (31, 5, 8737),
        (32, 5, 2499),
        (33, 5, 9379),
        (34, 5, 6846),
        (35, 5, 8226),
        (36, 5, 5227),
        (37, 5, 5456),
        (38, 5, 4900),
        (39, 5, 5937),
        (40, 5, 2762),
        (41, 5, 4144),
        (42, 5, 2668),
        (43, 5, 3329),
        (44, 5, 2476),
        (45, 5, 7516),
        (46, 5, 9018),
        (47, 5, 3713),
        (48, 5, 8777),
        (49, 5, 3992),
        (50, 5, 1145),
        (51, 5, 6075),
        (52, 5, 3262),
        (53, 5, 9536),
        (54, 5, 9052),
        (55, 5, 9655),
        (56, 5, 9308),
        (57, 5, 3933),
        (58, 5, 2796),
        (60, 5, 2664),
        (61, 5, 4526),
        (62, 5, 5199),
        (64, 5, 6609),
        (66, 5, 2820),
        (68, 5, 6248),
        (70, 46, 7821),
        (71, 46, 2557),
        (72, 46, 9395),
        (76, 42, 2215),
        (77, 42, 8581),
        (79, 42, 2393),
        (81, 42, 9921),
        (82, 42, 9554),
        (83, 2, 2103),
        (84, 2, 2890),
        (85, 2, 8593),
        (87, 2, 5885),
        (88, 48, 1969),
        (89, 42, 1029),
        (90, 79, 4719),
        (91, 79, 9286)");

        
        // Table structure for table houdinv_users_group  

                $getDB->query("CREATE TABLE houdinv_users_group (
            houdinv_users_group_id int(11) NOT NULL,
            houdinv_users_group_name varchar(50) NOT NULL,
            houdinv_users_group_status enum('active','deactive') NOT NULL,
            houdinv_users_group_created_at varchar(30) NOT NULL,
            houdinv_users_group_modified_at varchar(30) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_users_group

                $getDB->query("INSERT INTO houdinv_users_group (houdinv_users_group_id, houdinv_users_group_name, houdinv_users_group_status, houdinv_users_group_created_at, houdinv_users_group_modified_at) VALUES
        (1, 'Electricity', 'active', '1530514305', '1530525582'),
        (3, 'Clothes', 'active', '1530535197', '')");

        // Table structure for table houdinv_users_group_users_list  

                $getDB->query("CREATE TABLE houdinv_users_group_users_list (
            houdinv_users_group_users_list_id int(11) NOT NULL,
            houdinv_users_group__user_group_id int(11) NOT NULL,
            houdinv_users_group_users_id int(11) NOT NULL,
            houdinv_users_group_users_list_created_at varchar(30) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_users_group_users_list

                $getDB->query("INSERT INTO houdinv_users_group_users_list (houdinv_users_group_users_list_id, houdinv_users_group__user_group_id, houdinv_users_group_users_id, houdinv_users_group_users_list_created_at) VALUES
        (8, 1, 5, '1530776999'),
        (6, 3, 2, '1530535451'),
        (9, 1, 6, '1530776999')");

       // Table structure for table houdinv_users_whishlist  

                $getDB->query("CREATE TABLE houdinv_users_whishlist (
        houdinv_users_whishlist_id int(11) NOT NULL,
        houdinv_users_whishlist_user_id int(11) NOT NULL,
        houdinv_users_whishlist_item_id int(11) DEFAULT '0',
        houdinv_users_whishlist_item_variant_id int(11) DEFAULT '0',
        houdinv_users_whishlist_final_price varchar(100) NOT NULL,
        houdinv_users_whishlist_add_date varchar(100) NOT NULL,
        houdinv_users_whishlist_update_date varchar(100) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Dumping data for table houdinv_users_whishlist

                $getDB->query("INSERT INTO houdinv_users_whishlist (houdinv_users_whishlist_id, houdinv_users_whishlist_user_id, houdinv_users_whishlist_item_id, houdinv_users_whishlist_item_variant_id, houdinv_users_whishlist_final_price, houdinv_users_whishlist_add_date, houdinv_users_whishlist_update_date) VALUES
        (361, 2, 34, 0, '0', '1538456648', '1538456648'),
        (137, 5, 0, 6, '12', '1535021100', '1535609128'),
        (348, 2, NULL, NULL, '0', '1538384308', '1538384308'),
        (239, 67, 9, 0, '3', '1537006525', '1537006525'),
        (322, 78, 32, 0, '1200', '1537505032', '1537505074'),
        (363, 2, 1, 0, '3', '1538465935', '1538465935'),
        (364, 2, 0, 7, '0', '1538466104', '1538466104'),
        (359, 2, 35, 0, '650', '1538395569', '1538395569'),
        (292, 0, 9, 0, '3', '1537255746', '1537255746'),
        (360, 2, 20, 0, '400', '1538395734', '1538395734'),
        (245, 67, 0, 6, '0', '1537014754', '1537014754'),
        (340, 42, 8, 0, '2', '1538211298', ''),
        (357, 44, 37, 0, '3299', '1538395250', '1538397829'),
        (358, 2, 36, 0, '601', '1538395567', '1538395567'),
        (338, 42, 35, 0, '650', '1538200137', '1538200137'),
        (362, 2, 37, 0, '0', '1538463005', '1538463005'),
        (344, 2, 32, 0, '0', '1538373027', '1538373027')");

    // Table structure for table houdinv_user_address  

                $getDB->query("CREATE TABLE houdinv_user_address (
        houdinv_user_address_id int(11) NOT NULL,
        houdinv_user_address_user_id int(11) NOT NULL,
        houdinv_user_address_name varchar(30) NOT NULL,
        houdinv_user_address_phone varchar(20) NOT NULL,
        houdinv_user_address_user_address varchar(100) NOT NULL,
        houdinv_user_address_city varchar(30) NOT NULL,
        houdinv_user_address_zip varchar(10) NOT NULL,
        houdinv_user_address_country int(11) NOT NULL,
        houdinv_user_address_created_at varchar(30) DEFAULT NULL,
        houdinv_user_address_modified_at varchar(30) DEFAULT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
      
      // Dumping data for table houdinv_user_address  

                $getDB->query("INSERT INTO houdinv_user_address (houdinv_user_address_id, houdinv_user_address_user_id, houdinv_user_address_name, houdinv_user_address_phone, houdinv_user_address_user_address, houdinv_user_address_city, houdinv_user_address_zip, houdinv_user_address_country, houdinv_user_address_created_at, houdinv_user_address_modified_at) VALUES
    (27, 51, 'gunjan', '+919785940038', 'jsjkd', 'jaiks', '302021', 6, '1534842304', NULL),
    (3, 1, 'shivam', '+917790870946', '602 kailash tower lal kothi', 'jaipur', '302021', 6, '1532510134', NULL),
    (84, 79, 'Arpit', '07891592009', '58, Ganesh Clolony\r\nBas Badanpura', 'jaipuer', '302002', 6, '1538100030', NULL),
    (5, 1, 'payal', '+919785940038', 'goyal@gmail.com', 'jaipur', '302020', 0, '1533107250', NULL),
    (24, 5, 'vyvy', '+919785940038', 'ctvy', 'vybu', '305796896', 6, '1534759654', NULL),
    (20, 6, 'vipin', '9929904141', 'dkjhdjkhdsjksd', 'jaipur ', '302020', 0, '1533173082', NULL),
    (25, 5, 'gfdvg', '+919785940038', 'jbjvub', 'bibjb', '20387480', 6, '1534759825', NULL),
    (26, 5, 'goyal', '+919785940038', 'mansarover', 'jaipur', '302020', 6, '1534838598', NULL),
    (21, 6, 'ghh', '34677899008877', 'dkjhdjkhdsjksd', 'dg', '2456', 0, '1533174037', NULL),
    (18, 6, 'abc', '1234567890', 'dkjhdjkhdsjk', 'jaipur', '302020', 0, '1533210986', NULL),
    (28, 5, 'gunjanlll', '+91785940038', 'jdkm', 'ka', '302020', 6, '1534842560', NULL),
    (29, 5, 'bkdldldlld', '+916769899879', 'just hxn', 'bdn', 'idkmdkd', 6, '1534842598', NULL),
    (30, 5, 'paaa', '+91978594068', 'jvuvu', 'vyvyb', '302020', 6, '1534843241', NULL),
    (59, 48, 'geetika ', '9799181084', 'Tonk road', 'jaipur', '45678', 6, '1537179914', NULL),
    (69, 0, 'mkuim,', 'ki kn', 'jk,, ', 'ijkm jhn', 'ky8k', 6, '1537266767', NULL),
    (36, 44, 'goyal', '+919785940068', 'manstove', 'jaipur', '5789999', 6, '1535100107', NULL),
    (85, 2, 'shivam', '+917790870946', '602 kailash tower', 'jaipur', '302015', 6, '1538357137', NULL),
    (77, 78, 'sunil', '+919672304111', '602  kaila', 'jaipur cit', '303510', 6, '1537505915', '1537505959'),
    (45, 48, 'hello', '23456788', 'dkjhdjkhdsjksd', 'hhj', '13466', 6, '1536052682', NULL),
    (46, 51, 'payal', '+918058049473', 'jvjvjvkviv', 'vhvj', '302020', 6, '1536325104', NULL),
    (81, 42, 'vipin pareek', '9929904141', 'Kalish tower  near lalkothi bus stand  gandhi nagar  ton pathak  jaipur ', 'jaipur ', '302020', 6, '1537532640', '1537784169'),
    (72, 70, 'fhh', '5555', 'Ghhj', 'ghjjj', '5555', 6, '1537358628', NULL),
    (74, 76, 'ghj', '+91666', 'vhh', 'guu', '966', 6, '1537441310', NULL),
    (54, 62, 'paya', '+919785940038', 'bjk', 'huj', '302020', 6, '1536801928', NULL),
    (55, 64, 'vuvhvh', '+919785940838', 'yvygu', 'hvyvhchv', '385868', 6, '1536991109', NULL),
    (56, 64, 'payal', '+919785940038', 'hdkk', 'jdkk', '302028', 6, '1537009025', NULL),
    (57, 64, 'hvyc', '+9105858505', ' fcfc', ' fct', '86850', 6, '1537009285', NULL),
    (58, 67, 'sdscs', 'ghhgfhfghfh', 'hffhfghg', 'ghgngn', 'gngn', 6, '1537012648', '1537012721'),
    (80, 42, 'jejej', '91991', 'Kjrje', 'kek', '6161', 6, '1537532640', NULL),
    (73, 75, 'gjj', '+919785945', 'vhj', 'hhu', '966', 6, '1537438450', NULL),
    (75, 42, 'gbjj,', '5562896985858', 'Hhjn', 'hhnjhbj', '88285588', 6, '1537442347', '1537442391')");

    // Table structure for table houdinv_user_addresses  

                $getDB->query("CREATE TABLE houdinv_user_addresses (
        houdinv_user_address_id int(10) UNSIGNED NOT NULL,
        houdinv_user_address_user_id int(10) UNSIGNED NOT NULL,
        houdinv_user_address_contact_no varchar(15) DEFAULT NULL,
        houdinv_user_address_street_line1 varchar(200) NOT NULL,
        houdinv_user_address_landmark varchar(200) DEFAULT NULL,
        houdinv_user_address_pincode decimal(10,0) NOT NULL,
        houdinv_user_address_city varchar(150) NOT NULL,
        houdinv_user_address_state varchar(150) NOT NULL,
        houdinv_user_address_country varchar(150) NOT NULL,
        houdinv_user_address_created_at varchar(30) DEFAULT NULL,
        houdinv_user_address_updated_at varchar(30) DEFAULT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

    // Dumping data for table houdinv_user_addresses

                $getDB->query("INSERT INTO houdinv_user_addresses (houdinv_user_address_id, houdinv_user_address_user_id, houdinv_user_address_contact_no, houdinv_user_address_street_line1, houdinv_user_address_landmark, houdinv_user_address_pincode, houdinv_user_address_city, houdinv_user_address_state, houdinv_user_address_country, houdinv_user_address_created_at, houdinv_user_address_updated_at) VALUES
    (1, 1, '+918561049836', 'tonk\r\nroad', 'Kailash Tower', '332001', 'jaipur', 'Rajasthan', 'India', '1529370263', NULL),
    (2, 2, '+91123456789', 'sdnfjkn', 'dfgbh', '0', 'ghjn', 'ghn', 'United States', '1529479415', NULL),
    (3, 3, '+9198765431234', 'defgvhn', 'cfvghn', '456', 'fvgh', 'frtvghb', 'Albania', '1529489574', NULL),
    (4, 4, '', 'tonk\r\nroad', 'Rajasthan', '0', 'dasd', 'sdsa', 'India', '1529492542', NULL),
    (5, 5, '', 'tonk\r\nroad', 'Kailash tower', '302021', 'jaipur', 'Rajasthan', 'India', '1529492728', NULL),
    (6, 6, '198734567890', 'fvghjnk,', 'rfgh', '0', 'rftgyh', 'rtghbj', 'United States', '1529495407', NULL),
    (7, 63, '', '', '', '0', '', '', '', '1536969600', NULL)");

    // Table structure for table houdinv_user_bank_details  

                $getDB->query("CREATE TABLE houdinv_user_bank_details (
        houdinv_user_bank_detail_id int(10) UNSIGNED NOT NULL,
        houdinv_user_bank_detail_user_id int(10) UNSIGNED NOT NULL,
        houdinv_user_bank_detail_holder_name varchar(150) NOT NULL,
        houdinv_user_bank_detail_holder_number varchar(150) NOT NULL,
        houdinv_user_bank_detail_bank_name varchar(200) NOT NULL,
        houdinv_user_bank_detail_swift_no varchar(150) DEFAULT NULL,
        houdinv_user_bank_detail_iban_no varchar(150) DEFAULT NULL,
        houdinv_user_bank_detail_country varchar(150) NOT NULL,
        houdinv_user_bank_detail_created_at timestamp NULL DEFAULT NULL,
        houdinv_user_bank_detail_updated_at timestamp NULL DEFAULT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Bank details of user for refund'");

    // Table structure for table houdinv_user_payments

                $getDB->query("CREATE TABLE houdinv_user_payments (
        houdinv_user_payment_id int(10) UNSIGNED NOT NULL,
        houdinv_user_payment_user_id int(10) UNSIGNED NOT NULL,
        houdinv_user_payment_product_id int(10) UNSIGNED NOT NULL,
        houdinv_user_payment_coupon_id int(10) UNSIGNED NOT NULL,
        houdinv_user_payment_cost float NOT NULL,
        houdinv_user_payment_currency varchar(50) NOT NULL,
        houdinv_user_payment_transcaction_id varchar(150) NOT NULL,
        houdinv_user_payment_status tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 For Unsuccess, 1 For Success, 2 For cancelled, 2 For refund',
        houdinv_user_payment_mode enum('COD','Online','Store') DEFAULT NULL,
        houdinv_user_payment_time datetime NOT NULL,
        houdinv_user_payment_created_at timestamp NULL DEFAULT NULL,
        houdinv_user_payment_updated_at timestamp NULL DEFAULT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

    // Table structure for table houdinv_user_tag

                $getDB->query("CREATE TABLE houdinv_user_tag (
        houdinv_user_tag_id int(11) NOT NULL,
        houdinv_user_tag_user_id int(11) NOT NULL,
        houdinv_user_tag_name varchar(100) NOT NULL,
        houdinv_user_tag_created_date varchar(30) NOT NULL,
        houdinv_user_tag_modified_at varchar(30) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    // Dumping data for table houdinv_user_tag

                $getDB->query("INSERT INTO houdinv_user_tag (houdinv_user_tag_id, houdinv_user_tag_user_id, houdinv_user_tag_name, houdinv_user_tag_created_date, houdinv_user_tag_modified_at) VALUES
    (1, 1, 'shivam', '1529481624', ''),
    (2, 1, 'test', '1529484441', ''),
    (3, 1, 'xcvcx', '1529492237', ''),
    (4, 5, 'xcvxvx', '1529492305', ''),
    (5, 5, 'xcvxvx', '1529492305', '')");

    // Table structure for table houdinv_user_use_Coupon

                $getDB->query("CREATE TABLE houdinv_user_use_Coupon (
        houdinv_user_use_Coupon_id int(11) NOT NULL,
        houdinv_user_use_Coupon_user_id int(11) NOT NULL,
        houdinv_user_use_Coupon_used_Count int(11) NOT NULL,
        houdinv_user_use_Coupon_coupon_id int(11) NOT NULL,
        houdinv_user_use_Coupon_discount int(11) NOT NULL,
        houdinv_user_use_Coupon_add_date varchar(100) DEFAULT NULL,
        houdinv_user_use_Coupon_update_date varchar(100) DEFAULT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    // Dumping data for table houdinv_user_use_Coupon

                $getDB->query("INSERT INTO houdinv_user_use_Coupon (houdinv_user_use_Coupon_id, houdinv_user_use_Coupon_user_id, houdinv_user_use_Coupon_used_Count, houdinv_user_use_Coupon_coupon_id, houdinv_user_use_Coupon_discount, houdinv_user_use_Coupon_add_date, houdinv_user_use_Coupon_update_date) VALUES
    (1, 2, 4, 1, 5, '1533772800', NULL),
    (2, 5, 1, 1, 2, '1534157082', NULL),
    (3, 44, 1, 1, 12, '1536320246', '1536324294'),
    (4, 51, 1, 1, 12, '1536550334', '1536669654')");

    // Table structure for table houdinv_user_verifications

                $getDB->query("CREATE TABLE houdinv_user_verifications (
        houdinv_user_verification_email varchar(150) NOT NULL,
        houdinv_user_verification_token varchar(150) NOT NULL,
        houdinv_user_verification_created_at timestamp NULL DEFAULT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

    // Table structure for table houdinv_viewmembership

                $getDB->query("CREATE TABLE houdinv_viewmembership (
        id int(11) NOT NULL,
        customer_ids varchar(250) NOT NULL,
        code varchar(250) NOT NULL,
        DiscountPercentage varchar(250) NOT NULL,
        status int(1) DEFAULT '0',
        ExpiryDate varchar(250) NOT NULL,
        createdate varchar(250) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

          // Dumping data for table houdinv_viewmembership

                $getDB->query("INSERT INTO houdinv_viewmembership (id, customer_ids, code, DiscountPercentage, status, ExpiryDate, createdate) VALUES
    (1, '6', 'XZA456', '12', 1, '1531958400', '1531226913'),
    (2, '5', 'DEa145', '10', 0, '1532044800', '1531227017')");

    // Table structure for table houdinv_visitors

                $getDB->query("CREATE TABLE houdinv_visitors (
        visitors_id int(11) NOT NULL,
        counts int(200) NOT NULL,
        visitors_date varchar(250) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

          // Dumping data for table houdinv_visitors

                $getDB->query("INSERT INTO houdinv_visitors (visitors_id, counts, visitors_date) VALUES
          (1, 23, '1534982400'),
          (2, 130, '1535068800'),
          (3, 1, '1535328000'),
          (4, 50, '1535414400'),
          (5, 16, '1535500800'),
          (6, 58, '1535587200'),
          (7, 81, '1535673600'),
          (8, 6, '1535760000'),
          (9, 1, '1535932800'),
          (10, 1, '1536019200'),
          (11, 258, '1536105600'),
          (12, 278, '1536192000'),
          (13, 46, '1536278400'),
          (14, 49, '1536537600'),
          (15, 2, '1536624000'),
          (16, 22, '1536710400'),
          (17, 398, '1536796800'),
          (18, 528, '1536883200'),
          (19, 1036, '1536969600'),
          (20, 1217, '1537142400'),
          (21, 1319, '1537228800'),
          (22, 676, '1537315200'),
          (23, 334, '1537401600'),
          (24, 313, '1537488000'),
          (25, 52, '1537747200'),
          (26, 113, '1537833600'),
          (27, 3, '1537920000'),
          (28, 199, '1538092800'),
          (29, 516, '1538179200'),
          (30, 452, '1538352000'),
          (31, 78, '1538438400'),
          (32, 95, '1538524800')");

    // Table structure for table houdinv_vouchers

                $getDB->query("CREATE TABLE houdinv_vouchers (
        houdinv_vouchers_id int(11) NOT NULL,
        houdinv_vouchers_name varchar(50) NOT NULL,
        houdinv_vouchers_valid_from date NOT NULL,
        houdinv_vouchers_valid_to date NOT NULL,
        houdinv_vouchers_code varchar(6) NOT NULL,
        houdinv_vouchers_discount int(11) NOT NULL,
        houdinv_vouchers_status enum('active','deactive') NOT NULL,
        houdinv_vouchers_created_at varchar(30) DEFAULT NULL,
        houdinv_vouchers_modified_at varchar(30) DEFAULT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    // Dumping data for table houdinv_vouchers

                $getDB->query("INSERT INTO houdinv_vouchers (houdinv_vouchers_id, houdinv_vouchers_name, houdinv_vouchers_valid_from, houdinv_vouchers_valid_to, houdinv_vouchers_code, houdinv_vouchers_discount, houdinv_vouchers_status, houdinv_vouchers_created_at, houdinv_vouchers_modified_at) VALUES
    (3, 'Voucher 2', '2018-07-07', '2018-07-07', '4zxoNr', 10, 'active', '1530958438', '1530959347'),
    (4, 'Voucher 1', '2018-07-07', '2018-07-07', 'uEXbl6', 23, 'active', '1530962795', NULL)");

    // Table structure for table houdinv_workorder

                $getDB->query("CREATE TABLE houdinv_workorder (
        houdinv_workorder_id int(11) NOT NULL,
        houdinv_workorder_order_id int(11) NOT NULL DEFAULT '0',
        houdinv_workorder_customer_name varchar(150) NOT NULL,
        houdinv_workorder_customer_phone varchar(100) NOT NULL,
        houdinv_workorder_product int(11) NOT NULL DEFAULT '0',
        houdinv_workorder_variant int(11) NOT NULL DEFAULT '0',
        houdinv_workorder_quantity int(11) NOT NULL,
        houdinv_workorder_coupon_code varchar(20) NOT NULL,
        houdinv_workorder_coupon_discount int(11) NOT NULL,
        houdinv_workorder_pickup varchar(50) NOT NULL,
        houdinv_workorder_address text NOT NULL,
        houdinv_workorder_note text NOT NULL,
        houdinv_workorder_payment varchar(50) NOT NULL,
        houdinv_workorder_work_detail text NOT NULL,
        houdinv_workorder_paid_amount varchar(50) NOT NULL,
        houdinv_workorder_net_pay varchar(50) NOT NULL,
        houdinv_workorder_deliver_time varchar(100) NOT NULL,
        houdinv_workorder_deliver_date varchar(100) NOT NULL,
        houdinv_workorder_payment_status int(1) NOT NULL DEFAULT '0',
        date varchar(255) NOT NULL,
        update_date varchar(100) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    // Dumping data for table houdinv_workorder

                $getDB->query("INSERT INTO houdinv_workorder (houdinv_workorder_id, houdinv_workorder_order_id, houdinv_workorder_customer_name, houdinv_workorder_customer_phone, houdinv_workorder_product, houdinv_workorder_variant, houdinv_workorder_quantity, houdinv_workorder_coupon_code, houdinv_workorder_coupon_discount, houdinv_workorder_pickup, houdinv_workorder_address, houdinv_workorder_note, houdinv_workorder_payment, houdinv_workorder_work_detail, houdinv_workorder_paid_amount, houdinv_workorder_net_pay, houdinv_workorder_deliver_time, houdinv_workorder_deliver_date, houdinv_workorder_payment_status, date, update_date) VALUES
    (2, 117, '2', '9785940038', 1, 0, 12, '', 0, 'deliver', '31', 'sdfsd', 'cash', 'sdfs', '24', '24', '10:15 PM', '31-08-2018', 1, '1535587200', '1535587200')");

// Indexes for dumped tables

    // Indexes for table cities

                $getDB->query("ALTER TABLE cities ADD PRIMARY KEY (city_id)");

    // Indexes for table houdinv_block_users

                $getDB->query("ALTER TABLE houdinv_block_users
    ADD PRIMARY KEY (houdinv_block_user_id)");

    // Indexes for table houdinv_campaignemail

                $getDB->query("ALTER TABLE houdinv_campaignemail
    ADD PRIMARY KEY (campaignsms_id)");

                $getDB->query("ALTER TABLE houdinv_campaignemail_user
ADD PRIMARY KEY (campaignsms_user_id)");

//
// Indexes for table houdinv_campaignpush
//
                $getDB->query("ALTER TABLE houdinv_campaignpush
ADD PRIMARY KEY (campaignsms_id)");

//
// Indexes for table houdinv_campaignpush_user
//
                $getDB->query("ALTER TABLE houdinv_campaignpush_user
ADD PRIMARY KEY (campaignsms_user_id)");

//
// Indexes for table houdinv_campaignsms
//
                $getDB->query("ALTER TABLE houdinv_campaignsms
ADD PRIMARY KEY (campaignsms_id)");

//
// Indexes for table houdinv_campaignsms_user
//
                $getDB->query("ALTER TABLE houdinv_campaignsms_user
ADD PRIMARY KEY (campaignsms_user_id)");

//
// Indexes for table houdinv_categories
//
                $getDB->query("ALTER TABLE houdinv_categories
ADD PRIMARY KEY (houdinv_category_id)");

//
// Indexes for table houdinv_checkout_address_data
//
                $getDB->query("ALTER TABLE houdinv_checkout_address_data
ADD PRIMARY KEY (houdinv_data_id)");

//
// Indexes for table houdinv_countries
//
                $getDB->query("ALTER TABLE houdinv_countries
ADD PRIMARY KEY (country_id)");

//
// Indexes for table houdinv_coupons
//
                $getDB->query("ALTER TABLE houdinv_coupons
ADD PRIMARY KEY (houdinv_coupons_id)");

//
// Indexes for table houdinv_coupons_procus
//
                $getDB->query("ALTER TABLE houdinv_coupons_procus
ADD PRIMARY KEY (houdinv_coupons_procus_id)");

//
// Indexes for table houdinv_customersetting
//
                $getDB->query("ALTER TABLE houdinv_customersetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_custom_home_data
//
                $getDB->query("ALTER TABLE houdinv_custom_home_data
ADD PRIMARY KEY (houdinv_custom_home_data_id)");

//
// Indexes for table houdinv_custom_links
//
                $getDB->query("ALTER TABLE houdinv_custom_links
ADD PRIMARY KEY (houdinv_custom_links_id)");

//
// Indexes for table houdinv_custom_slider
//
                $getDB->query("ALTER TABLE houdinv_custom_slider
ADD PRIMARY KEY (houdinv_custom_slider_id)");

//
// Indexes for table houdinv_deliveries
//
                $getDB->query("ALTER TABLE houdinv_deliveries
ADD PRIMARY KEY (houdinv_delivery_id)");

//
// Indexes for table houdinv_delivery_tracks
//
                $getDB->query("ALTER TABLE houdinv_delivery_tracks
ADD PRIMARY KEY (houdinv_delivery_track_id)");

//
// Indexes for table houdinv_delivery_users
//
                $getDB->query("ALTER TABLE houdinv_delivery_users
ADD PRIMARY KEY (houdinv_delivery_user_id)");

//
// Indexes for table houdinv_emailsms_payment
//
                $getDB->query("ALTER TABLE houdinv_emailsms_payment
ADD PRIMARY KEY (houdinv_emailsms_payment_id)");

//
// Indexes for table houdinv_emailsms_stats
//
                $getDB->query("ALTER TABLE houdinv_emailsms_stats
ADD PRIMARY KEY (houdinv_emailsms_stats_id)");

//
// Indexes for table houdinv_email_log
//
                $getDB->query("ALTER TABLE houdinv_email_log
ADD PRIMARY KEY (houdinv_email_log_id)");

//
// Indexes for table houdinv_email_Template
//
                $getDB->query("ALTER TABLE houdinv_email_Template
ADD PRIMARY KEY (houdinv_email_template_id)");

//
// Indexes for table houdinv_extra_expence
//
                $getDB->query("ALTER TABLE houdinv_extra_expence
ADD PRIMARY KEY (houdinv_extra_expence_id)");

//
// Indexes for table houdinv_extra_expence_item
//
                $getDB->query("ALTER TABLE houdinv_extra_expence_item
ADD PRIMARY KEY (houdinv_extra_expence_item_id)");

//
// Indexes for table houdinv_google_analytics
//
                $getDB->query("ALTER TABLE houdinv_google_analytics
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_inventories
//
                $getDB->query("ALTER TABLE houdinv_inventories
ADD PRIMARY KEY (houdinv_inventory_id)");

//
// Indexes for table houdinv_inventorysetting
//
                $getDB->query("ALTER TABLE houdinv_inventorysetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_inventory_purchase
//
                $getDB->query("ALTER TABLE houdinv_inventory_purchase
ADD PRIMARY KEY (houdinv_inventory_purchase_id)");

//
// Indexes for table houdinv_inventory_tracks
//
                $getDB->query("ALTER TABLE houdinv_inventory_tracks
ADD PRIMARY KEY (houdinv_inventory_track_id)");

//
// Indexes for table houdinv_invoicesetting
//
                $getDB->query("ALTER TABLE houdinv_invoicesetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_navigation_store_pages
//
                $getDB->query("ALTER TABLE houdinv_navigation_store_pages
ADD PRIMARY KEY (houdinv_navigation_store_pages_id)");

//
// Indexes for table houdinv_noninventory_products
//
                $getDB->query("ALTER TABLE houdinv_noninventory_products
ADD PRIMARY KEY (houdinv_noninventory_products_id)");

//
// Indexes for table houdinv_offline_payment_settings
//
                $getDB->query("ALTER TABLE houdinv_offline_payment_settings
ADD PRIMARY KEY (houdinv_offline_payment_setting_id)");

//
// Indexes for table houdinv_online_payment_settings
//
                $getDB->query("ALTER TABLE houdinv_online_payment_settings
ADD PRIMARY KEY (houdinv_online_payment_setting_id)");

//
// Indexes for table houdinv_orders
//
                $getDB->query("ALTER TABLE houdinv_orders
ADD PRIMARY KEY (houdinv_order_id)");

//
// Indexes for table houdinv_order_users
//
                $getDB->query("ALTER TABLE houdinv_order_users
ADD PRIMARY KEY (houdinv_order_users_id)");

//
// Indexes for table houdinv_payment_gateway
//
                $getDB->query("ALTER TABLE houdinv_payment_gateway
ADD PRIMARY KEY (houdinv_payment_gateway_id)");

//
// Indexes for table houdinv_possetting
//
                $getDB->query("ALTER TABLE houdinv_possetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_products
//
                $getDB->query("ALTER TABLE houdinv_products
ADD PRIMARY KEY (houdin_products_id)");

//
// Indexes for table houdinv_products_variants
//
                $getDB->query("ALTER TABLE houdinv_products_variants
ADD PRIMARY KEY (houdin_products_variants_id)");

//
// Indexes for table houdinv_productType
//
                $getDB->query("ALTER TABLE houdinv_productType
ADD PRIMARY KEY (houdinv_productType_id)");

//
// Indexes for table houdinv_product_reviews
//
                $getDB->query("ALTER TABLE houdinv_product_reviews
ADD PRIMARY KEY (houdinv_product_review_id)");

//
// Indexes for table houdinv_product_types
//
                $getDB->query("ALTER TABLE houdinv_product_types
ADD PRIMARY KEY (houdinv_product_type_id)");

//
// Indexes for table houdinv_purchase_products
//
                $getDB->query("ALTER TABLE houdinv_purchase_products
ADD PRIMARY KEY (houdinv_purchase_products_id)");

//
// Indexes for table houdinv_purchase_products_inward
//
                $getDB->query("ALTER TABLE houdinv_purchase_products_inward
ADD PRIMARY KEY (houdinv_purchase_products_inward_id)");

//
// Indexes for table houdinv_push_Template
//
                $getDB->query("ALTER TABLE houdinv_push_Template
ADD PRIMARY KEY (houdinv_push_template_id)");

//
// Indexes for table houdinv_shipping_charges
//
                $getDB->query("ALTER TABLE houdinv_shipping_charges
ADD PRIMARY KEY (houdinv_shipping_charge_id)");

//
// Indexes for table houdinv_shipping_credentials
//
                $getDB->query("ALTER TABLE houdinv_shipping_credentials
ADD PRIMARY KEY (houdinv_shipping_credentials_id)");

//
// Indexes for table houdinv_shipping_data
//
                $getDB->query("ALTER TABLE houdinv_shipping_data
ADD PRIMARY KEY (houdinv_shipping_data_id)");

//
// Indexes for table houdinv_shipping_ruels
//
                $getDB->query("ALTER TABLE houdinv_shipping_ruels
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_shipping_warehouse
//
                $getDB->query("ALTER TABLE houdinv_shipping_warehouse
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_shop_applogo
//
                $getDB->query("ALTER TABLE houdinv_shop_applogo
ADD PRIMARY KEY (applogo_id)");

//
// Indexes for table houdinv_shop_ask_quotation
//
                $getDB->query("ALTER TABLE houdinv_shop_ask_quotation
ADD PRIMARY KEY (houdinv_shop_ask_quotation_id)");

//
// Indexes for table houdinv_shop_detail
//
                $getDB->query("ALTER TABLE houdinv_shop_detail
ADD PRIMARY KEY (houdinv_shop_id)");

//
// Indexes for table houdinv_shop_logo
//
                $getDB->query("ALTER TABLE houdinv_shop_logo
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_shop_order_configuration
//
                $getDB->query("ALTER TABLE houdinv_shop_order_configuration
ADD PRIMARY KEY (houdinv_shop_order_configuration_id)");

//
// Indexes for table houdinv_shop_query
//
                $getDB->query("ALTER TABLE houdinv_shop_query
ADD PRIMARY KEY (houdinv_shop_query_id)");

//
// Indexes for table houdinv_skusetting
//
                $getDB->query("ALTER TABLE houdinv_skusetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_sms_log
//
                $getDB->query("ALTER TABLE houdinv_sms_log
ADD PRIMARY KEY (houdinv_sms_log_id)");

//
// Indexes for table houdinv_sms_template
//
                $getDB->query("ALTER TABLE houdinv_sms_template
ADD PRIMARY KEY (houdinv_sms_template_id)");

//
// Indexes for table houdinv_social_links
//
                $getDB->query("ALTER TABLE houdinv_social_links
ADD PRIMARY KEY (social_id)");

//
// Indexes for table houdinv_staff_management
//
                $getDB->query("ALTER TABLE houdinv_staff_management
ADD PRIMARY KEY (staff_id)");

//
// Indexes for table houdinv_staff_management_sub
//
                $getDB->query("ALTER TABLE houdinv_staff_management_sub
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_states
//
                $getDB->query("ALTER TABLE houdinv_states
ADD PRIMARY KEY (state_id)");

//
// Indexes for table houdinv_stock_transfer_log
//
                $getDB->query("ALTER TABLE houdinv_stock_transfer_log
ADD PRIMARY KEY (houdinv_stock_transfer_log_id)");

//
// Indexes for table houdinv_storediscount
//
                $getDB->query("ALTER TABLE houdinv_storediscount
ADD PRIMARY KEY (discount_id)");

//
// Indexes for table houdinv_storepolicies
//
                $getDB->query("ALTER TABLE houdinv_storepolicies
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_storesetting
//
                $getDB->query("ALTER TABLE houdinv_storesetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_sub_categories_one
//
                $getDB->query("ALTER TABLE houdinv_sub_categories_one
ADD PRIMARY KEY (houdinv_sub_category_one_id)");

//
// Indexes for table houdinv_sub_categories_two
//
                $getDB->query("ALTER TABLE houdinv_sub_categories_two
ADD PRIMARY KEY (houdinv_sub_category_two_id)");

//
// Indexes for table houdinv_suppliers
//
                $getDB->query("ALTER TABLE houdinv_suppliers
ADD PRIMARY KEY (houdinv_supplier_id)");

//
// Indexes for table houdinv_supplier_products
//
                $getDB->query("ALTER TABLE houdinv_supplier_products
ADD PRIMARY KEY (houdinv_supplier_products_id)");

//
// Indexes for table houdinv_taxes
//
                $getDB->query("ALTER TABLE houdinv_taxes
ADD PRIMARY KEY (houdinv_tax_id)");

//
// Indexes for table houdinv_taxsetting
//
                $getDB->query("ALTER TABLE houdinv_taxsetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_templates
//
                $getDB->query("ALTER TABLE houdinv_templates
ADD PRIMARY KEY (houdinv_template_id)");

//
// Indexes for table houdinv_template_pages
//
                $getDB->query("ALTER TABLE houdinv_template_pages
ADD PRIMARY KEY (houdinv_template_page_id)");

//
// Indexes for table houdinv_testimonials
//
                $getDB->query("ALTER TABLE houdinv_testimonials
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_transaction
//
                $getDB->query("ALTER TABLE houdinv_transaction
ADD PRIMARY KEY (houdinv_transaction_id)");

//
// Indexes for table houdinv_users
//
                $getDB->query("ALTER TABLE houdinv_users
ADD PRIMARY KEY (houdinv_user_id)");

//
// Indexes for table houdinv_users_cart
//
                $getDB->query("ALTER TABLE houdinv_users_cart
ADD PRIMARY KEY (houdinv_users_cart_id)");

//
// Indexes for table houdinv_users_forgot
//
                $getDB->query("ALTER TABLE houdinv_users_forgot
ADD PRIMARY KEY (houdinv_users_forgot_id)");

//
// Indexes for table houdinv_users_group
//
                $getDB->query("ALTER TABLE houdinv_users_group
ADD PRIMARY KEY (houdinv_users_group_id)");

//
// Indexes for table houdinv_users_group_users_list
//
                $getDB->query("ALTER TABLE houdinv_users_group_users_list
ADD PRIMARY KEY (houdinv_users_group_users_list_id)");

//
// Indexes for table houdinv_users_whishlist
//
                $getDB->query("ALTER TABLE houdinv_users_whishlist
ADD PRIMARY KEY (houdinv_users_whishlist_id)");

//
// Indexes for table houdinv_user_address
//
                $getDB->query("ALTER TABLE houdinv_user_address
ADD PRIMARY KEY (houdinv_user_address_id)");

//
// Indexes for table houdinv_user_addresses
//
                $getDB->query("ALTER TABLE houdinv_user_addresses
ADD PRIMARY KEY (houdinv_user_address_id)");

//
// Indexes for table houdinv_user_bank_details
//
                $getDB->query("ALTER TABLE houdinv_user_bank_details
ADD PRIMARY KEY (houdinv_user_bank_detail_id)");

//
// Indexes for table houdinv_user_payments
//
                $getDB->query("ALTER TABLE houdinv_user_payments
ADD PRIMARY KEY (houdinv_user_payment_id)");

//
// Indexes for table houdinv_user_tag
//
                $getDB->query("ALTER TABLE houdinv_user_tag
ADD PRIMARY KEY (houdinv_user_tag_id)");

//
// Indexes for table houdinv_user_use_Coupon
//
                $getDB->query("ALTER TABLE houdinv_user_use_Coupon
ADD PRIMARY KEY (houdinv_user_use_Coupon_id)");

//
// Indexes for table houdinv_viewmembership
//
                $getDB->query("ALTER TABLE houdinv_viewmembership
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_visitors
//
                $getDB->query("ALTER TABLE houdinv_visitors
ADD PRIMARY KEY (visitors_id)");

//
// Indexes for table houdinv_vouchers
//
                $getDB->query("ALTER TABLE houdinv_vouchers
ADD PRIMARY KEY (houdinv_vouchers_id)");

//
// Indexes for table houdinv_workorder
//
                $getDB->query("ALTER TABLE houdinv_workorder
ADD PRIMARY KEY (houdinv_workorder_id)");

//
// AUTO_INCREMENT for dumped tables
//

//
// AUTO_INCREMENT for table cities
//
                $getDB->query("ALTER TABLE cities
MODIFY city_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6178");
//
// AUTO_INCREMENT for table houdinv_block_users
//
                $getDB->query("ALTER TABLE houdinv_block_users
MODIFY houdinv_block_user_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignemail
//
                $getDB->query("ALTER TABLE houdinv_campaignemail
MODIFY campaignsms_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignemail_user
//
                $getDB->query("ALTER TABLE houdinv_campaignemail_user
MODIFY campaignsms_user_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignpush
//
                $getDB->query("ALTER TABLE houdinv_campaignpush
MODIFY campaignsms_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignpush_user
//
                $getDB->query("ALTER TABLE houdinv_campaignpush_user
MODIFY campaignsms_user_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignsms
//
                $getDB->query("ALTER TABLE houdinv_campaignsms
MODIFY campaignsms_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignsms_user
//
                $getDB->query("ALTER TABLE houdinv_campaignsms_user
MODIFY campaignsms_user_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_categories
//
                $getDB->query("ALTER TABLE houdinv_categories
MODIFY houdinv_category_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9");
//
// AUTO_INCREMENT for table houdinv_checkout_address_data
//
                $getDB->query("ALTER TABLE houdinv_checkout_address_data
MODIFY houdinv_data_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12");
//
// AUTO_INCREMENT for table houdinv_countries
//
                $getDB->query("ALTER TABLE houdinv_countries
MODIFY country_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240");
//
// AUTO_INCREMENT for table houdinv_coupons
//
                $getDB->query("ALTER TABLE houdinv_coupons
MODIFY houdinv_coupons_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");
//
// AUTO_INCREMENT for table houdinv_coupons_procus
//
                $getDB->query("ALTER TABLE houdinv_coupons_procus
MODIFY houdinv_coupons_procus_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27");
//
// AUTO_INCREMENT for table houdinv_customersetting
//
                $getDB->query("ALTER TABLE houdinv_customersetting
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");
//
// AUTO_INCREMENT for table houdinv_custom_home_data
//
                $getDB->query("ALTER TABLE houdinv_custom_home_data
MODIFY houdinv_custom_home_data_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12");
//
// AUTO_INCREMENT for table houdinv_custom_links
//
                $getDB->query("ALTER TABLE houdinv_custom_links
MODIFY houdinv_custom_links_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8");
//
// AUTO_INCREMENT for table houdinv_custom_slider
//
                $getDB->query("ALTER TABLE houdinv_custom_slider
MODIFY houdinv_custom_slider_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5");
//
// AUTO_INCREMENT for table houdinv_deliveries
//
                $getDB->query("ALTER TABLE houdinv_deliveries
MODIFY houdinv_delivery_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11");
//
// AUTO_INCREMENT for table houdinv_delivery_tracks
//
                $getDB->query("ALTER TABLE houdinv_delivery_tracks
MODIFY houdinv_delivery_track_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_delivery_users
//
                $getDB->query("ALTER TABLE houdinv_delivery_users
MODIFY houdinv_delivery_user_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_emailsms_payment
//
                $getDB->query("ALTER TABLE houdinv_emailsms_payment
MODIFY houdinv_emailsms_payment_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_emailsms_stats
//
                $getDB->query("ALTER TABLE houdinv_emailsms_stats
MODIFY houdinv_emailsms_stats_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");
//
// AUTO_INCREMENT for table houdinv_email_log
//
                $getDB->query("ALTER TABLE houdinv_email_log
MODIFY houdinv_email_log_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202");
//
// AUTO_INCREMENT for table houdinv_email_Template
//
                $getDB->query("ALTER TABLE houdinv_email_Template
MODIFY houdinv_email_template_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6");
//
// AUTO_INCREMENT for table houdinv_extra_expence
//
                $getDB->query("ALTER TABLE houdinv_extra_expence
MODIFY houdinv_extra_expence_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");
//
// AUTO_INCREMENT for table houdinv_extra_expence_item
//
                $getDB->query("ALTER TABLE houdinv_extra_expence_item
MODIFY houdinv_extra_expence_item_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7");
//
// AUTO_INCREMENT for table houdinv_google_analytics
//
                $getDB->query("ALTER TABLE houdinv_google_analytics
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");
//
// AUTO_INCREMENT for table houdinv_inventories
//
                $getDB->query("ALTER TABLE houdinv_inventories
MODIFY houdinv_inventory_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_inventorysetting
//
                $getDB->query("ALTER TABLE houdinv_inventorysetting
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");
//
// AUTO_INCREMENT for table houdinv_inventory_purchase
//
                $getDB->query("ALTER TABLE houdinv_inventory_purchase
MODIFY houdinv_inventory_purchase_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");
//
// AUTO_INCREMENT for table houdinv_inventory_tracks
//
                $getDB->query("ALTER TABLE houdinv_inventory_tracks
MODIFY houdinv_inventory_track_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_invoicesetting
//
                $getDB->query("ALTER TABLE houdinv_invoicesetting
MODIFY id int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");
//
// AUTO_INCREMENT for table houdinv_navigation_store_pages
//
                $getDB->query("ALTER TABLE houdinv_navigation_store_pages
MODIFY houdinv_navigation_store_pages_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71");
//
// AUTO_INCREMENT for table houdinv_noninventory_products
//
                $getDB->query("ALTER TABLE houdinv_noninventory_products
MODIFY houdinv_noninventory_products_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7");
//
// AUTO_INCREMENT for table houdinv_offline_payment_settings
//
                $getDB->query("ALTER TABLE houdinv_offline_payment_settings
MODIFY houdinv_offline_payment_setting_id int(11) UNSIGNED NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_online_payment_settings

                $getDB->query("ALTER TABLE houdinv_online_payment_settings
MODIFY houdinv_online_payment_setting_id int(11) NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_orders

                $getDB->query("ALTER TABLE houdinv_orders
MODIFY houdinv_order_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=512");

// AUTO_INCREMENT for table houdinv_order_users

                $getDB->query("ALTER TABLE houdinv_order_users
MODIFY houdinv_order_users_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_payment_gateway

                $getDB->query("ALTER TABLE houdinv_payment_gateway
MODIFY houdinv_payment_gateway_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_possetting

                $getDB->query("ALTER TABLE houdinv_possetting
MODIFY id int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_products

                $getDB->query("ALTER TABLE houdinv_products
MODIFY houdin_products_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38");

// AUTO_INCREMENT for table houdinv_products_variants

                $getDB->query("ALTER TABLE houdinv_products_variants
MODIFY houdin_products_variants_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27");

// AUTO_INCREMENT for table houdinv_productType

                $getDB->query("ALTER TABLE houdinv_productType
MODIFY houdinv_productType_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");

// AUTO_INCREMENT for table houdinv_product_reviews

                $getDB->query("ALTER TABLE houdinv_product_reviews
MODIFY houdinv_product_review_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66");

// AUTO_INCREMENT for table houdinv_product_types

                $getDB->query("ALTER TABLE houdinv_product_types
MODIFY houdinv_product_type_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_purchase_products

                $getDB->query("ALTER TABLE houdinv_purchase_products
MODIFY houdinv_purchase_products_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8");

// AUTO_INCREMENT for table houdinv_purchase_products_inward

                $getDB->query("ALTER TABLE houdinv_purchase_products_inward
MODIFY houdinv_purchase_products_inward_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7");

// AUTO_INCREMENT for table houdinv_push_Template

                $getDB->query("ALTER TABLE houdinv_push_Template
MODIFY houdinv_push_template_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5");

// AUTO_INCREMENT for table houdinv_shipping_charges

                $getDB->query("ALTER TABLE houdinv_shipping_charges
MODIFY houdinv_shipping_charge_id int(11) NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_shipping_credentials

                $getDB->query("ALTER TABLE houdinv_shipping_credentials
MODIFY houdinv_shipping_credentials_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_shipping_data

                $getDB->query("ALTER TABLE houdinv_shipping_data
MODIFY houdinv_shipping_data_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");

// AUTO_INCREMENT for table houdinv_shipping_ruels

                $getDB->query("ALTER TABLE houdinv_shipping_ruels
MODIFY id int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_shipping_warehouse

                $getDB->query("ALTER TABLE houdinv_shipping_warehouse
MODIFY id int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_shop_applogo

                $getDB->query("ALTER TABLE houdinv_shop_applogo
MODIFY applogo_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_shop_ask_quotation

                $getDB->query("ALTER TABLE houdinv_shop_ask_quotation
MODIFY houdinv_shop_ask_quotation_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51");

// AUTO_INCREMENT for table houdinv_shop_detail

                $getDB->query("ALTER TABLE houdinv_shop_detail
MODIFY houdinv_shop_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_shop_logo

                $getDB->query("ALTER TABLE houdinv_shop_logo
MODIFY id int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_shop_order_configuration

                $getDB->query("ALTER TABLE houdinv_shop_order_configuration
MODIFY houdinv_shop_order_configuration_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_shop_query

                $getDB->query("ALTER TABLE houdinv_shop_query
MODIFY houdinv_shop_query_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16");

// AUTO_INCREMENT for table houdinv_skusetting

                $getDB->query("ALTER TABLE houdinv_skusetting
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_sms_log

                $getDB->query("ALTER TABLE houdinv_sms_log
MODIFY houdinv_sms_log_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=368");

// AUTO_INCREMENT for table houdinv_sms_template

                $getDB->query("ALTER TABLE houdinv_sms_template
MODIFY houdinv_sms_template_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5");

// AUTO_INCREMENT for table houdinv_social_links

                $getDB->query("ALTER TABLE houdinv_social_links
MODIFY social_id int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_staff_management

                $getDB->query("ALTER TABLE houdinv_staff_management
MODIFY staff_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10");

// AUTO_INCREMENT for table houdinv_staff_management_sub

                $getDB->query("ALTER TABLE houdinv_staff_management_sub
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9");

// AUTO_INCREMENT for table houdinv_states

                $getDB->query("ALTER TABLE houdinv_states
MODIFY state_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1652");

// AUTO_INCREMENT for table houdinv_stock_transfer_log

                $getDB->query("ALTER TABLE houdinv_stock_transfer_log
MODIFY houdinv_stock_transfer_log_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_storediscount

                $getDB->query("ALTER TABLE houdinv_storediscount
MODIFY discount_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13");

// AUTO_INCREMENT for table houdinv_storepolicies

                $getDB->query("ALTER TABLE houdinv_storepolicies
MODIFY id int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_storesetting

                $getDB->query("ALTER TABLE houdinv_storesetting
MODIFY id int(220) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_sub_categories_one

                $getDB->query("ALTER TABLE houdinv_sub_categories_one
MODIFY houdinv_sub_category_one_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6");

// AUTO_INCREMENT for table houdinv_sub_categories_two

                $getDB->query("ALTER TABLE houdinv_sub_categories_two
MODIFY houdinv_sub_category_two_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12");

// AUTO_INCREMENT for table houdinv_suppliers

                $getDB->query("ALTER TABLE houdinv_suppliers
MODIFY houdinv_supplier_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8");

// AUTO_INCREMENT for table houdinv_supplier_products

                $getDB->query("ALTER TABLE houdinv_supplier_products
MODIFY houdinv_supplier_products_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38");

// AUTO_INCREMENT for table houdinv_taxes

                $getDB->query("ALTER TABLE houdinv_taxes
MODIFY houdinv_tax_id int(11) NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_taxsetting

                $getDB->query("ALTER TABLE houdinv_taxsetting
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_templates

                $getDB->query("ALTER TABLE houdinv_templates
MODIFY houdinv_template_id int(11) NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_template_pages

                $getDB->query("ALTER TABLE houdinv_template_pages
MODIFY houdinv_template_page_id int(11) NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_testimonials

                $getDB->query("ALTER TABLE houdinv_testimonials
MODIFY id int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6");

// AUTO_INCREMENT for table houdinv_transaction

                $getDB->query("ALTER TABLE houdinv_transaction
MODIFY houdinv_transaction_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90");

// AUTO_INCREMENT for table houdinv_users

                $getDB->query("ALTER TABLE houdinv_users
MODIFY houdinv_user_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82");

// AUTO_INCREMENT for table houdinv_users_cart

                $getDB->query("ALTER TABLE houdinv_users_cart
MODIFY houdinv_users_cart_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=317");

// AUTO_INCREMENT for table houdinv_users_forgot

                $getDB->query("ALTER TABLE houdinv_users_forgot
MODIFY houdinv_users_forgot_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93");

// AUTO_INCREMENT for table houdinv_users_group

                $getDB->query("ALTER TABLE houdinv_users_group
MODIFY houdinv_users_group_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");

// AUTO_INCREMENT for table houdinv_users_group_users_list

                $getDB->query("ALTER TABLE houdinv_users_group_users_list
MODIFY houdinv_users_group_users_list_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10");

// AUTO_INCREMENT for table houdinv_users_whishlist

                $getDB->query("ALTER TABLE houdinv_users_whishlist
MODIFY houdinv_users_whishlist_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=365");

// AUTO_INCREMENT for table houdinv_user_address

                $getDB->query("ALTER TABLE houdinv_user_address
MODIFY houdinv_user_address_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86");

// AUTO_INCREMENT for table houdinv_user_addresses

                $getDB->query("ALTER TABLE houdinv_user_addresses
MODIFY houdinv_user_address_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8");

// AUTO_INCREMENT for table houdinv_user_bank_details

                $getDB->query("ALTER TABLE houdinv_user_bank_details
MODIFY houdinv_user_bank_detail_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_user_tag

                $getDB->query("ALTER TABLE houdinv_user_tag
MODIFY houdinv_user_tag_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6");

// AUTO_INCREMENT for table houdinv_user_use_Coupon

                $getDB->query("ALTER TABLE houdinv_user_use_Coupon
MODIFY houdinv_user_use_Coupon_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5");

// AUTO_INCREMENT for table houdinv_viewmembership

                $getDB->query("ALTER TABLE houdinv_viewmembership
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");

// AUTO_INCREMENT for table houdinv_visitors

                $getDB->query("ALTER TABLE houdinv_visitors
MODIFY visitors_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33");

// AUTO_INCREMENT for table houdinv_vouchers

                $getDB->query("ALTER TABLE houdinv_vouchers
MODIFY houdinv_vouchers_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5");

// AUTO_INCREMENT for table houdinv_workorder

                $getDB->query("ALTER TABLE houdinv_workorder
MODIFY houdinv_workorder_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

            } else {
                echo 'query error';
            }
        } else {
            echo 'db error';
        }
        exit();
        $getPackageData = $this->db->select('houdin_packages.*')->from('houdin_packages')->where('houdin_packages.houdin_package_status', 'active')->get()->result();
        return array('packageData' => $getPackageData);
    }






}
?>