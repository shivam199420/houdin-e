        <?php $this->load->view('Template/header') ?>
        <?php $this->load->view('Template/sidebar') ?>
        <div class="content-page">
        <div class="content">
        <div class="container">
        <div class="row">
        <div class="col-sm-12 m-b-15">
        <h4 class="page-title">Shop View</h4>
        </div>
        </div>
        <div class="row">
        <div class="col-md-6 col-lg-4">
        <div class="widget-bg-color-icon card-box fadeInDown animated">
        <div class="bg-icon bg-icon-info pull-left">
        <i class="md md-attach-money text-info"></i>
        </div>
        <div class="text-right">
        <h3 class="text-dark"><b class="counter"><?php if($customerCount) { echo $customerCount; } else { echo 0; }  ?></b></h3>
        <p class="text-muted">Total customer</p>
        </div>
        <div class="clearfix"></div>
        </div>
        </div>

        <div class="col-md-6 col-lg-4">
        <div class="widget-bg-color-icon card-box">
        <div class="bg-icon bg-icon-pink pull-left">
        <i class="md md-add-shopping-cart text-pink"></i>
        </div>
        <div class="text-right">
        <h3 class="text-dark"><b class="counter"><?php if($productList) { echo $productList; } else { echo 0; }  ?></b></h3>
        <p class="text-muted">Total products</p>
        </div>
        <div class="clearfix"></div>
        </div>
        </div>

        <div class="col-md-6 col-lg-4"> 
        <div class="widget-bg-color-icon card-box">
        <div class="bg-icon bg-icon-purple pull-left">
        <i class="md md-equalizer text-purple"></i>
        </div>
        <div class="text-right">
        <h3 class="text-dark"><b class="counter"><?php if($variantData) { echo $variantData; } else { echo 0; }  ?></b></h3>
        <p class="text-muted">Total variants</p>
        </div>
        <div class="clearfix"></div>
        </div>
       </div>
       <div class="col-md-6 col-lg-4"> 
        <div class="widget-bg-color-icon card-box">
        <div class="bg-icon bg-icon-purple pull-left">
        <i class="md md-equalizer text-purple"></i>
        </div>
        <div class="text-right">
        <h3 class="text-dark"><b class="counter"><?php if($orderData) { echo $orderData; } else { echo 0; }  ?></b></h3>
        <p class="text-muted">Total Orders</p>
        </div>
        <div class="clearfix"></div>
        </div>
        </div>


        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="card-box row">
        <h3>Shop Information</h3><br>
        <div class="col-sm-2 col-xs-2"> 
        <div class="form-group">
        <label>User Name : </label>

        </div>
        <div class="form-group">
        <label>User Email : </label>

        </div>
        <div class="form-group">
        <label>Contact : </label>

        </div>
        <div class="form-group">
        <label>Address : </label>

        </div>
       
        <div class="form-group">
        <label>Shop Name : </label>

        </div>
        <div class="form-group">
        <label>Shop Email : </label>

        </div>
        <div class="form-group">
        <label>Shop DB : </label>

        </div>
        </div>
        <div class="col-md-10 col-sm-10 col-xs-10">
        <div class="form-group">
        <span class="custom_span_info"><?php echo $userData[0]->houdin_user_name ?></span>
        </div>
        <div class="form-group">
        <span class="custom_span_info"><?php echo $userData[0]->houdin_user_email ?></span>
        </div>
        <div class="form-group">
        <span class="custom_span_info"><?php echo $userData[0]->houdin_user_contact ?></span>
        </div>
        <div class="form-group">
        <span class="custom_span_info"><?php echo $userData[0]->houdin_user_address ?></span>
        </div>
        <div class="form-group">
        <span class="custom_span_info"><?php echo $userData[0]->houdin_user_shop_shop_name ?></span>
        </div>
        <div class="form-group">
        <span class="custom_span_info"><?php echo $userData[0]->houdin_user_shop_email ?></span>
        </div>
        <div class="form-group">
        <span class="custom_span_info"><?php echo $userData[0]->houdin_user_shop_db_name ?></span>
        </div>
        </div>
        </div>
        </div>
        </div>

        </div> <!-- container -->
        </div> <!-- content -->
        <?php $this->load->view('Template/footer') ?>