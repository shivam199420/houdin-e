      <?php $this->load->view('Template/header.php') ?>
      <?php $this->load->view('Template/storesetting') ?>
      <style type="text/css">
      @media screen and (min-width: 768px){
      .nav.nav-tabs > li.active > a {
      background-color: #ffffff!important;
      border: 1px solid #cee5ee;
      border-bottom: 0px solid #000;
      }
      .nav.nav-tabs > li > a, .nav.tabs-vertical > li > a {
      background-color: #cee5ee!important;
      border-radius: 0;
      border: none;
      color: #00628b !important;
      cursor: pointer;
      line-height: 50px;
      padding-left: 20px;
      padding-right: 20px;
      letter-spacing: 0.03em;
      font-weight: 600;
      text-transform: uppercase;
      font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
      }
      .nav.nav-tabs + .tab-content {
      background: #ffffff;
      margin-bottom: 0px!important;
      padding: 0px 0px;
      }
      .panel-default > .panel-heading {
      background-color: #cee5ee;
      border-bottom: none;
      color: #797979;
      }
      .panel-group .panel {
      margin-bottom: 0;
      border-radius: 4px;
      border: 1px solid #cee5ee;
      }
      .checkbox{
      padding: 0px!important
      }
      }
      @media (max-width:767px){
      .checkbox {
      padding: 0px!important;
      }
      }
      </style>
      <div class="content-page">
      <!-- Start content -->
      <div class="content">
      <div class="container">

      <!--START Page-Title -->
      <div class="row">
  
      <div class="col-md-8">
      <h4 class="page-title">Edit Theme</h4>
      <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
      <li><a href="<?php echo base_url(); ?>storesetting/Storesettinglanding">Store setting</a></li>
      <li class="active">Edit Theme</li>
      </ol>
      </div>

      </div>
      <!--END Page-Title --> 


      <div class="row m-t-20"> 

      <div class="col-sm-12 col-md-12">
      <div class="card-box" style="border-radius: 5px 5px 0px 0px!important;margin-bottom: 0px!important;">
      <div class="row">
      <?php 
      if($this->session->flashdata('success'))
      {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
      }
      if($this->session->flashdata('error'))
      {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
      }
      ?>
      <div class="col-lg-12"> 
      <div class="search-result-box m-t-40">
      <ul class="nav nav-tabs"> 
      <li class="active"> 
      <a href="#image" data-toggle="tab" aria-expanded="true"> 
      <span class="visible-xs"><i class="fa fa-home"></i></span> 
      <span class="hidden-xs"><b>Slider Image</b></span> 
      </a>  
      </li> 
      <!-- <li class=""> 
      <a href="#text" data-toggle="tab" aria-expanded="false"> 
      <span class="visible-xs"><i class="fa fa-user"></i></span> 
      <span class="hidden-xs"><b>Banner Text</b></span> 
      </a> 
      </li>  -->
      <li class=""> 
      <a href="#home" data-toggle="tab" aria-expanded="false"> 
      <span class="visible-xs"><i class="fa fa-user"></i></span> 
      <span class="hidden-xs"><b>Customise Home</b></span> 
      </a> 
      </li> 


      <!-- <li class=""> 
      <a href="#advanced" data-toggle="tab" aria-expanded="false"> 
      <span class="visible-xs"><i class="fa fa-user"></i></span> 
      <span class="hidden-xs"><b>Advanced</b> </span> 
      </a> 
      </li>  -->
      </ul> 
      <div class="tab-content"> 
      <div class="tab-pane active" id="image"> 
      <div class="row">
      <div class="col-md-12">
      <div class="search-item">
      <div class="card-box" style="padding: 10px 20px;">
      <div class="row">
      <div class="col-md-12">
      <h3>Slider Images</h3>
       
      </div>
      <div class="row">

      <div class="col-lg-12"> 
      <div class="panel-group" id="accordion-test-2" style="padding:10px"> 
      <div class="panel panel-default"> 
      <div class="panel-heading"> 
      <h4 class="panel-title"> 
      <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseOne-2" aria-expanded="false" class="collapsed"> Image 1 </a> 
      </h4> 
      </div> 
      <div id="collapseOne-2" class="panel-collapse collapse in"> 
      <?php echo form_open(base_url( 'Storesetting/updateSlider' ), array( 'id' => 'sliderRules', 'method'=>'post' ,'enctype' => 'multipart/form-data' )); ?>
      <div class="panel-body">
      <div class="row p-l-0">
      <?php print_r(); ?>
      <div class="col-md-12 p-l-0 form-horizontal">
      <div class="form-group">
      <label class="col-md-1 col-xs-4 col-sm-4 p-l-0 control-label">Slider Image</label>
      <div class="col-md-9 col-xs-8 col-sm-8">
      </div>
      </div> 
      <div class="form-group">
      <label class="col-md-1 col-xs-4 col-sm-4 control-label p-l-0">Currently</label>
      <div class="col-md-9 col-xs-8 col-sm-8">
      <?php 
      if($sliderlist[0]->houdinv_custom_slider_image)
      {
      ?>
      <img class="custom_logo_choose" src="<?php echo base_url(); ?>upload/Slider/<?php echo $sliderlist[0]->houdinv_custom_slider_image ?>">
      <?php }
      else
      {
      ?>
      <img class="custom_logo_choose" src="<?php echo base_url(); ?>assets/images/no_photo.png">
      <?php }
      ?>
      </div>
      <?php 
      if($sliderlist[0]->houdinv_custom_slider_image)
      { 
        ?>
        <!-- <div class="col-md-2"><a herf="javascript:;" data-id="<?php //echo $sliderlist[0]->houdinv_custom_slider_id ?>" class="btn btn-xs btn-danger pull-right deleteSliderImageBtn"><i class="fa fa-trash"></i></a></div> -->
      <?php }
      ?>
      </div>
      </div>
      <div class="form-group">
      <div class="col-md-12 m-t-10 p-l-0">
      <label class="col-md-1 control-label p-l-0">Change</label>
      <div class="col-md-9 p-l-0">
      <input type="file" class="filestyle required_validation_for_edit_theme form-control" name="name">
      </div>
      </div>
      <p><i class="fa fa-info-circle"></i> Upload image of minimum size 770 x 513 px(width x height) for best fit.</p>
      </div>
      <div class="form-group">
      <label class="control-label">Heading</label>
      <input type="hidden" name="sliderId" value="<?php echo $sliderlist[0]->houdinv_custom_slider_id ?>"/>
      <input class="form-control required_validation_for_edit_theme name_validation" maxlength="30" type="text" name="headingData" value="<?php echo $sliderlist[0]->houdinv_custom_slider_head ?>">
      </div>

      <div class="form-group">
      <label class="control-label">Sub-heading</label>
      <input class="form-control required_validation_for_edit_theme name_validation" maxlength="50" type="text" name="subheadingData" value="<?php echo $sliderlist[0]->houdinv_custom_slider_sub_heading ?>">
      </div>
      <div class="form-group">
      <input type="submit" class="btn btn-success" value="Update Image"/>      
      </div>
      </div>
      </div> 
      <?php echo form_close(); ?>
      </div> 
      </div>
      <div class="panel panel-default"> 
      <div class="panel-heading"> 
      <h4 class="panel-title"> 
      <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseTwo-2" class="collapsed" aria-expanded="false">
      Image 2
      </a> 
      </h4> 
      </div> 
      <div id="collapseTwo-2" class="panel-collapse collapse "> 
      <?php echo form_open(base_url( 'Storesetting/updateSlider' ), array( 'id' => 'sliderRules', 'method'=>'post' ,'enctype' => 'multipart/form-data' )); ?>
      <div class="panel-body">
      <div class="row p-l-0">
      <div class="col-md-12 p-l-0 form-horizontal">
      <div class="form-group">
      <label class="col-md-1 col-xs-4 col-sm-4 p-l-0 control-label">Slider Image</label>
      <div class="col-md-9 col-xs-8 col-sm-8">
      </div>
      </div> 
      <div class="form-group">
      <label class="col-md-1 col-xs-4 col-sm-4 control-label p-l-0">Currently</label>
      <div class="col-md-9 col-xs-8 col-sm-8">
      <?php 
      if($sliderlist[1]->houdinv_custom_slider_image)
      {
      ?>
      <img class="custom_logo_choose" src="<?php echo base_url(); ?>upload/Slider/<?php echo $sliderlist[1]->houdinv_custom_slider_image ?>">
      <?php }
      else
      {
      ?>
      <img class="custom_logo_choose" src="<?php echo base_url(); ?>assets/images/no_photo.png">
      <?php }
      ?>
      </div>

      <?php 
      if($sliderlist[1]->houdinv_custom_slider_image)
      { 
        ?>
        <!-- <div class="col-md-2"><a herf="javascript:;" data-id="<?php //echo $sliderlist[1]->houdinv_custom_slider_id ?>" class="btn btn-xs deleteSliderImageBtn btn-danger pull-right"><i class="fa fa-trash"></i></a></div> -->
      <?php }
      ?>
      </div>
      </div>
      <div class="form-group">
      <div class="col-md-12 m-t-10 p-l-0">
      <label class="col-md-1 control-label p-l-0">Change</label>
      <div class="col-md-9 p-l-0">
      <input type="file" class="filestyle required_validation_for_edit_theme form-control" name="name">
      </div>
      </div>
      <p><i class="fa fa-info-circle"></i> Upload image of minimum size 770 x 513 px(width x height) for best fit.</p>
      </div>
      <div class="form-group">
      <label class="control-label">Heading</label>
      <input type="hidden" name="sliderId" value="<?php echo $sliderlist[1]->houdinv_custom_slider_id ?>"/>
      <input class="form-control required_validation_for_edit_theme name_validation" maxlength="30" type="text" name="headingData" value="<?php echo $sliderlist[1]->houdinv_custom_slider_head ?>">
      </div>

      <div class="form-group">
      <label class="control-label">Sub-heading</label>
      <input class="form-control required_validation_for_edit_theme name_validation" maxlength="50" type="text" name="subheadingData" value="<?php echo $sliderlist[1]->houdinv_custom_slider_sub_heading ?>">
      </div>
      <div class="form-group">
      <input type="submit" class="btn btn-success" value="Update Image"/>      
      </div>
      </div>
      </div> 
      <?php echo form_close(); ?>

      </div> 
      </div> 
      <div class="panel panel-default"> 
      <div class="panel-heading"> 
      <h4 class="panel-title"> 
      <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseThree-2" class="collapsed" aria-expanded="false">
      Image 3
      </a> 
      </h4> 
      </div> 
      <div id="collapseThree-2" class="panel-collapse collapse"> 
      <?php echo form_open(base_url( 'Storesetting/updateSlider' ), array( 'id' => 'sliderRules', 'method'=>'post' ,'enctype' => 'multipart/form-data' )); ?>
      <div class="panel-body">
      <div class="row p-l-0">
      <div class="col-md-12 p-l-0 form-horizontal">
      <div class="form-group">
      <label class="col-md-1 col-xs-4 col-sm-4 p-l-0 control-label">Slider Image</label>
      <div class="col-md-9 col-xs-8 col-sm-8">
      </div>
      </div> 
      <div class="form-group">
      <label class="col-md-1 col-xs-4 col-sm-4 control-label p-l-0">Currently</label>
      <div class="col-md-9 col-xs-8 col-sm-8">
      <?php 
      if($sliderlist[2]->houdinv_custom_slider_image)
      {
      ?>
      <img class="custom_logo_choose" src="<?php echo base_url(); ?>upload/Slider/<?php echo $sliderlist[2]->houdinv_custom_slider_image ?>">
      <?php }
      else
      {
      ?>
      <img class="custom_logo_choose" src="<?php echo base_url(); ?>assets/images/no_photo.png">
      <?php }
      ?>
      </div>
      <?php 
      if($sliderlist[2]->houdinv_custom_slider_image)
      { 
        ?>
        <!-- <div class="col-md-2"><a herf="javascript:;" data-id="<?php //echo $sliderlist[2]->houdinv_custom_slider_id ?>" class="btn btn-xs deleteSliderImageBtn btn-danger pull-right"><i class="fa fa-trash"></i></a></div> -->
      <?php }
      ?>
      </div>
      </div>
      <div class="form-group">
      <div class="col-md-12 m-t-10 p-l-0">
      <label class="col-md-1 control-label p-l-0">Change</label>
      <div class="col-md-9 p-l-0">
      <input type="file" class="filestyle required_validation_for_edit_theme form-control" name="name">
      </div>
      </div>
      <p><i class="fa fa-info-circle"></i> Upload image of minimum size 770 x 513 px(width x height) for best fit.</p>
      </div>
      <div class="form-group">
      <label class="control-label">Heading</label>
      <input type="hidden" name="sliderId" value="<?php echo $sliderlist[2]->houdinv_custom_slider_id ?>"/>
      <input class="form-control required_validation_for_edit_theme name_validation" maxlength="30" type="text" name="headingData" value="<?php echo $sliderlist[2]->houdinv_custom_slider_head ?>">
      </div>

      <div class="form-group">
      <label class="control-label">Sub-heading</label>
      <input class="form-control required_validation_for_edit_theme name_validation" maxlength="50" type="text" name="subheadingData" value="<?php echo $sliderlist[2]->houdinv_custom_slider_sub_heading ?>">
      </div>
      <div class="form-group">
      <input type="submit" class="btn btn-success" value="Update Image"/>      
      </div>
      </div>
      </div> 
      <?php echo form_close(); ?>

      </div> 
      </div> 
      <div class="panel panel-default"> 
      <div class="panel-heading"> 
      <h4 class="panel-title"> 
      <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseFour-2" class="collapsed" aria-expanded="false">
      Image 4
      </a>  
      </h4> 
      </div> 
      <div id="collapseFour-2" class="panel-collapse collapse"> 
      <?php echo form_open(base_url( 'Storesetting/updateSlider' ), array( 'id' => 'sliderRules', 'method'=>'post' ,'enctype' => 'multipart/form-data' )); ?>
      <div class="panel-body">
      <div class="row p-l-0">
      <div class="col-md-12 p-l-0 form-horizontal">
      <div class="form-group">
      <label class="col-md-1 col-xs-4 col-sm-4 p-l-0 control-label">Slider Image</label>
      <div class="col-md-9 col-xs-8 col-sm-8">
      </div>
      </div> 
      <div class="form-group">
      <label class="col-md-1 col-xs-4 col-sm-4 control-label p-l-0">Currently</label>
      <div class="col-md-9 col-xs-8 col-sm-8">
      <?php 
      if($sliderlist[3]->houdinv_custom_slider_image)
      {
      ?>
      <img class="custom_logo_choose" src="<?php echo base_url(); ?>upload/Slider/<?php echo $sliderlist[3]->houdinv_custom_slider_image ?>">
      <?php }
      else
      {
      ?>
      <img class="custom_logo_choose" src="<?php echo base_url(); ?>assets/images/no_photo.png">
      <?php }
      ?>
      </div>
      <?php 
      if($sliderlist[3]->houdinv_custom_slider_image)
      { 
        ?>
        <!-- <div class="col-md-2"><a herf="javascript:;" data-id="<?php //echo $sliderlist[3]->houdinv_custom_slider_id ?>" class="btn btn-xs deleteSliderImageBtn btn-danger pull-right"><i class="fa fa-trash"></i></a></div> -->
      <?php }
      ?>
      </div>
      </div>
      <div class="form-group">
      <div class="col-md-12 m-t-10 p-l-0">
      <label class="col-md-1 control-label p-l-0">Change</label>
      <div class="col-md-9 p-l-0">
      <input type="file" class="filestyle required_validation_for_edit_theme form-control" name="name">
      </div>
      </div>
      <p><i class="fa fa-info-circle"></i> Upload image of minimum size 770 x 513 px(width x height) for best fit.</p>
      </div>
      <div class="form-group">
      <label class="control-label">Heading</label>
      <input type="hidden" name="sliderId" value="<?php echo $sliderlist[3]->houdinv_custom_slider_id ?>"/>
      <input class="form-control required_validation_for_edit_theme name_validation" maxlength="30" type="text" name="headingData" value="<?php echo $sliderlist[3]->houdinv_custom_slider_head ?>">
      </div>

      <div class="form-group">
      <label class="control-label">Sub-heading</label>
      <input class="form-control required_validation_for_edit_theme name_validation" maxlength="50" type="text" name="subheadingData" value="<?php echo $sliderlist[3]->houdinv_custom_slider_sub_heading ?>">
      </div>
      <div class="form-group">
      <input type="submit" class="btn btn-success" value="Update Image"/>      
      </div>
      </div>
      </div> 
      <?php echo form_close(); ?>
      </div> 
      <!-- </div>  -->
      </div>
      </div> 
      </div>
      </div>
      </form>
      </div>
      </div>
      </div>
      </div>
      </div> 
      </div> 

      <!-- end All results tab -->


      <!--START TEXT tab -->
      <div class="tab-pane" id="text"> 
      <div class="row">
      <div class="col-md-12">
      <div class="search-item">
      <div class="card-box" style="padding: 10px 20px;">
      <div class="row">
      <div class="col-md-12">
      <h3>Banner Text</h3>
      <div class="col-md-6 checkbox checkbox-custom m-b-10">
      <input id="checkbox11" type="checkbox" checked="">
      <label>Banner enabled</label>
      </div>
      </div>
      <form method="">
      <div class="row">
      <div class="col-lg-12"> 
      <div class="panel-group" id="accordion-test-2" style="padding:10px"> 
      <div class="panel panel-default"> 

      <div id="collapseOne-2" class="panel-collapse collapse in"> 
      <div class="panel-body">
      <div class="form-group">
      <label class="control-label">Banner text</label>
      <input class="form-control" type="text" name="" value="">
      <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Enabling banner will disable slider on your store</p>
      </div>

      <div class="form-group m-b-0">
      <button class="btn btn-success waves-effect waves-light" type="Save">
      Save
      </button>
      </div>
      </div> 
      </div> 
      </div>
      </div> 
      </div>
      </div>
      </form>
      </div>
      </div>
      </div>
      </div>
      </div> 
      </div> 
      <!-- END TEXT tab -->
      <!-- START HOME tab -->
      <div class="tab-pane" id="home"> 
      <div class="row">
      <div class="col-md-12">
      <div class="search-item">
      <div class="card-box" style="padding: 10px 20px;">
      <div class="row">
      <div class="col-md-12">
      <h3>Customize Home</h3>


      </div>

      <div class="row">
      <div class="col-lg-12"> 
      <div class="panel-group" id="accordion-test-2" style="padding:10px"> 
      <div class="panel panel-default"> 

      <div id="collapseOne-2" class="panel-collapse collapse in"> 
      <?php echo form_open(base_url( 'Storesetting/updateCustomHome' ), array( 'id' => 'addcustomhome', 'method'=>'post' )); ?>
      <div class="panel-body">
      <div class="alert alert-danger showErrorData" style="display:none;">Please fill all the selected fields</div>
      <p><i class="fa fa-info-circle"></i> Click and enable the icons you want be display on your store</p>
      <div class="checkbox checkbox-custom m-b-10">
      <input type="checkbox" <?php if($customList[0]->houdinv_custom_home_data_category) { ?> checked="checked" <?php  } ?> class="sethomeData" data-id="category">
      <label>Show Product Category</label>
      </div>
      <div class="checkbox checkbox-custom m-b-10">
      <input type="checkbox" <?php if($customList[0]->houdinv_custom_home_data_latest_product) { ?> checked="checked" <?php  } ?> class="sethomeData" data-id="products">
      <label>Show Latest Products</label>
      </div>
      <div class="checkbox checkbox-custom m-b-10">
      <input type="checkbox" <?php if($customList[0]->houdinv_custom_home_data_featured_product) { ?> checked="checked" <?php  } ?> class="sethomeData" data-id="featured">
      <label>Show Featured Products</label>
      </div>
      <div class="checkbox checkbox-custom m-b-10">
      <input type="checkbox" <?php if($customList[0]->houdinv_custom_home_data_testimonial) { ?> checked="checked" <?php  } ?> class="sethomeData" data-id="testimonial">
      <label>Show Testimonial</label>
      </div>
      
      <div class="form-group">
      <label class="control-label"> Categories Heading</label>
      <input class="form-control commonheadingclass" value="<?php echo $customList[0]->houdinv_custom_home_data_category ?>" <?php if($customList[0]->houdinv_custom_home_data_category == "") { ?> disabled="disabled" <?php  } ?>  data-id="category" type="text" name="categoryHeading">
      </div>

      <div class="form-group">
      <label class="control-label">Latest Products Heading</label>
      <input class="form-control commonheadingclass" value="<?php echo $customList[0]->houdinv_custom_home_data_latest_product ?>" <?php if($customList[0]->houdinv_custom_home_data_latest_product == "") { ?> disabled="disabled" <?php  } ?> data-id="products" type="text" name="productHeading">
      </div>
      <div class="form-group">
      <label class="control-label">Featured Products Heading</label>
      <input class="form-control commonheadingclass" value="<?php echo $customList[0]->houdinv_custom_home_data_featured_product ?>" <?php if($customList[0]->houdinv_custom_home_data_featured_product == "") { ?> disabled="disabled" <?php  } ?> data-id="featured" type="text" name="featuredProductHeading">
      </div>
      <div class="form-group">
      <label class="control-label">Testimonials Heading</label>
      <input class="form-control commonheadingclass" value="<?php echo $customList[0]->houdinv_custom_home_data_testimonial ?>" <?php if($customList[0]->houdinv_custom_home_data_testimonial == "") { ?> disabled="disabled" <?php  } ?> data-id="testimonial" type="text" name="testimonialHeading">
      </div>
      <div class="form-group m-b-0"><input type="submit" class="btn btn-success" value="Save"/> </div>
      </div> 
      <?php echo form_close(); ?>
      </div> 
      </div>
      </div> 
      </div>
      </div>
      
      </div>
      </div>
      </div>
      </div>
      </div> 
      </div> 
      <!-- END HOME tab -->
      <!-- START ADVANCED tab -->
      <div class="tab-pane" id="advanced"> 
      <div class="row">
      <div class="col-md-12">
      <div class="search-item">
      <div class="card-box" style="padding: 10px 20px;">
      <div class="row">
      <div class="col-md-12">
      <h3>Add Your CSS:</h3>


      </div>
      <form method="">
      <div class="row">
      <div class="col-lg-12"> 
      <div class="panel-group" id="accordion-test-2" style="padding:10px"> 
      <div class="panel panel-default"> 

      <div id="collapseFour-2" class="panel-collapse collapse in"> 
      <div class="panel-body">


      <div class="form-group">
      <label class="control-label">Custom css</label>
      <textarea class="form-control" rows="7"></textarea>
      </div>


      <div class="form-group m-b-0">
      <button class="btn btn-success waves-effect waves-light" type="Save">
      Save
      </button>
      </div> 
      </div> 
      </div> 
      </div>
      </div> 
      </div>
      </div>
      </form>
      </div>
      </div>
      </div>
      </div>
      </div> 
      </div> 
      <!-- END ADVANCED tab -->
      </div> 
      </div>
      </div>
      </div>

      </div>

      </div>

      </div>
      <!-- Delete slider images -->
      <div id="deleteSliderImagesmodal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Storesetting/deleteSliderImage' ), array( 'method'=>'post' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Slider Image</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleteSliderImageId" name="deleteSliderImageId"/>
        <h4><b>Do you really want to Delete this slider image ?</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" value="Delete">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
      <?php $this->load->view('Template/footer.php') ?>
      <!-- CLient side form validation -->
      <script>
      $(document).ready(function(){
        $(document).on('submit','#addcustomhome',function(){
          $('.showErrorData').hide();
        var setCountData = 0;
         $('.sethomeData').each(function(){
             var setCheckData = $(this).attr('data-id');
             if($(this).prop('checked') == true)
             {
                $('.commonheadingclass').each(function(){
                    if($(this).attr('data-id') == setCheckData)
                    {
                        var getValue = $(this).val();   
                        if(getValue)
                        {

                        }
                        else
                        {
                            setCountData++;
                        }
                    }
                })
             }
         });
         if(setCountData != 0)
         {
            $('.showErrorData').show();
            return false;
         }
    });
      })
      </script>
       
<!-- CLient side form validation -->
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#sliderRules',function(){
			var check_required_field='';
			$(this).find(".required_validation_for_edit_theme").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>