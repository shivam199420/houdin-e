<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style>
.button_box {
    background: #6e6f6f;
    border-radius: 3px;
    text-align: center;
    padding: 12px;
}
.button_box h4 {
    font-size: 17px;
    color: #fff;
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">
        
                <div class="col-md-8">
                   <h4 class="page-title">Tally Exports</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url() ?>">Home</a></li>
                  <li class="active"><a href="javascript:void(0);">Tally</a></li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->

            </div>
           <!--END Page-Title -->
          <div class="row m-t-20">
            <div class="col-md-12">
              <div class="col-md-3">
                <div class="button_box">
                  <h4><span class="m-r-5"><i class="fa fa-wpforms"></i></span>Export Customer Data</h4>
                </div>
              </div>
              <div class="col-md-3">
                <div class="button_box">
                  <h4><span class="m-r-5"><i class="fa fa-envelope-open"></i></span>Export Revenue</h4>
                </div>
              </div>
              <div class="col-md-3">

                <div class="button_box">
                  <h4><span class="m-r-5"><i class="fa fa-window-restore"></i></span>Export Product</h4>
                </div>

              </div>
              <div class="col-md-3">

                <div class="button_box">
                  <h4><span class="m-r-5"><i class="fa fa-sticky-note-o"></i></span>Export Orders</h4>
                </div>

              </div>
            </div>
          </div>

                      </div>
                    </div>
                  </div>






  <?php $this->load->view('Template/footer.php') ?>
