        <!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/logo1.png">
        <title>Houdin - Welcome to Houdin Admin Panel</title>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/login_custom.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
        <style>
        	.account-pages
        	 {
			    background: url(../vendor/assets/images/updated_login_banner.jpg);
			    position: absolute;
			    height: 100%;
			    width: 100%;
			    background-size: 100% 100%;
			 }
             .card-box{
                background-color: #ffffff69!important;
             }
             .text-dark { 
                color: #ffffff !important;
            }
            .alert-success {
                background-color: rgba(95, 190, 170, 0.69)!important;
                border-color: rgba(95, 190, 170, 0.52)!important;
                color: #ffffff!important;
}
        </style>

        </head>
        <body>
        <div class="account-pages"></div> 
        <div class="clearfix"></div>
        <div class="wrapper-page">
        <div class=" card-box card_box_custom">
        <div class="panel-heading">
        <?php
        $getmaster = getmasterurl();
        $getDynamicDb = $this->load->database('master',true);
        $resultAdminLogoData = $getDynamicDb->select('houdin_admin_logo_image')->from('houdin_admin_logo')->where('houdin_admin_logo_id','1')->get()->result();
        $setDynamicImageData = "".$getmaster."uploads/logo/".$resultAdminLogoData[0]->houdin_admin_logo_image;
        $setDynamicImageData2=base_url()."assets/images/favicon_W.png"
        ?>
        <h3 class="text-center"> <img src="<?php echo $setDynamicImageData ?>" class="logo_head"/></h3>
        </div>
        <div class="panel-body">
        <?php 
        if($this->session->flashdata('success'))
        {
                echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        if($this->session->flashdata('error'))
        {
                echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        ?>
       <?php echo form_open(base_url( 'Forget' ),  array( 'method' => 'post', 'class' => 'form-horizontal','id'=>"forgetPassForm" ));?>
        <div class="form-group ">
        <div class="col-xs-12">
        <input class="form-control required_validation_for_forget_pass name_validation number_validation" maxlength="5" type="text"  name="confirmationCode"  placeholder="Confirmation code" >
        <!--<p class="messagesd"></p>-->
        </div>
        </div> 
        <div class="form-group">
        <div class="col-xs-12">
        <input class="form-control required_validation_for_register required_validation_for_forget_pass name_validation password" type="password" name="password"  placeholder="Password" >
        </div>
        </div>
        <div class="form-group">
        <div class="col-xs-12">
        <input class="form-control required_validation_for_register required_validation_for_forget_pass name_validation confirm_password" type="password" name="cpassword" placeholder="Retype Password" >
        </div>
        </div>
        <div class="form-group text-center m-t-40">
        <div class="col-xs-12">
        <input type="submit" class="btn btn_theme_dark text-uppercase setForgetData" name="updateForgetPass" value="Submit"/>
        </div>
        </div>
        <div class="form-group m-t-30 m-b-0">
        <div class="col-sm-12">
        </div>
        <?php echo form_close();?>
        </div>
        </div>
        </div>
        <!--script-->
        <script>
        var resizefunc = [];
        </script>
        <!-- jQuery  -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation.js"></script>
        <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#forgetPassForm',function(){
			var check_required_field='';
			$(".required_validation_for_forget_pass").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>
        <script>
        $(document).ready(function(){
                $(document).on('keyup','.confirm_password',function(){
                        var originalPass = $('.password').val();
                        if($(this).val() == originalPass)
                        {
                                $('.setForgetData').attr('disabled',false);
                                $(this).css('border','1px solid #d3d3d3');
                        }
                        else
                        {
                                $('.setForgetData').attr('disabled',true);
                                $(this).css('border','1px solid red');
                        }
                });
                $(document).on('keyup','.password',function(){
                        var originalPass = $('.confirm_password').val();
                        if($(this).val() == originalPass)
                        {
                                $('.setForgetData').attr('disabled',false);
                        }
                        else
                        {
                                $('.setForgetData').attr('disabled',true);
                        }
                });
        });
        </script>
        </body>
        </html>
