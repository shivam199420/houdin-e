      <?php $this->load->view('Template/header.php') ?>
      <?php $this->load->view('Template/sidebar.php');
      $getCurrency = getVendorCurrency();
      if($getCurrency[0]->houdin_users_currency=="USD")
      {
        $currencysymbol= "$";
      }else if($getCurrency[0]->houdin_users_currency=="AUD"){
        $currencysymbol= "$";
      }else if($getCurrency[0]->houdin_users_currency=="Euro"){
        $currencysymbol= "£";
      }else if($getCurrency[0]->houdin_users_currency=="Pound"){
        $currencysymbol= "€";
      }else if($getCurrency[0]->houdin_users_currency=="INR"){
        $currencysymbol= "₹";
      }
      ?>
      <style type="text/css">
      .tabs li.tab {
      background-color: #ffffff;
      display: block;
      float: left;
      margin: 0;
      text-align: center;
      width: 20%;
      }
      </style>
      <div class="content-page">
      <!-- Start content -->
      <div class="content">
      <div class="container">
      <!--START Page-Title -->
      <div class="row">

      <div class="col-md-8">
      <h4 class="page-title">Order Pickup</h4>
      <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>">Home</a></li>
     <li><a href="<?php echo base_url(); ?>order/Orderlanding">Order</a></li>
      <li class="active">Order Pickup</li>
      </ol>
      </div>
      <!-- <div class="col-md-4">
      <form role="search" class="navbar-left app-search pull-left custom_search_all">
      <input type="text" placeholder="Search..." class="form-control">
      <a href=""><i class="fa fa-search"></i></a>
      </form></div> -->

      </div>
      <!--END Page-Title -->
      <!-- START TAB -->
      <!-- START TAB -->
    <div class="row m-t-20">
    <div class="col-sm-12">
    <?php
    if($this->session->flashdata('success'))
    {
        echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
    }
    if($this->session->flashdata('error'))
    {
        echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
    }
    ?>
    <div class="setErrorMessageData alert alert-danger" style="display:none"></div>
    </div>
    </div>
      <div class="row m-t-20">
      <div class="col-lg-12">
      <!-- <ul class="nav nav-tabs tabs">
      <li class="tab">
      <a href="<?php //echo base_url();?>Order/allorders">
      <span class="visible-xs"><i class="fa fa-home"></i></span>
      <span class="hidden-xs">All Orders</span>
      </a>
      </li>
      <li class="tab">
      <a href="<?php //echo base_url();?>Order/deliverypending">
      <span class="visible-xs"><i class="fa fa-user"></i></span>
      <span class="hidden-xs">Delivery Pending</span>
      </a>
      </li>
      <li class="tab">
      <a href="<?php //echo base_url();?>Order/deliverycompleted">
      <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
      <span class="hidden-xs">Order Completed</span>
      </a>
      </li>
      <li class="active tab">
      <a style="color: #adadad !important;" class="active" href="#pickup" data-toggle="tab" aria-expanded="true">
      <span class="visible-xs"><i class="fa fa-cog"></i></span>
      <span class="hidden-xs">Order Pick Up</span>
      </a>
      </li>
      <li class="tab">
      <a href="#quotation" data-toggle="tab" aria-expanded="true">
      <span class="visible-xs"><i class="fa fa-cog"></i></span>
      <span class="hidden-xs">Order Quotation</span>
      </a>
      </li>
      </ul>  -->

      <!-- START TAB ALLORDER -->
      <div class="tab-content">
      <div class="tab-pane" id="allorder">

      <div class="row">
      <div class="col-md-12">

      <div class="card-box table-responsive">
      <!--<div class="btn-group pull-right m-t-10 m-b-20">
      <button type="button" class="btn btn-default m-r-5" title="Delete" data-toggle="modal" data-target="#delete_order"><i class="fa fa-trash"></i></button>
      <button type="button" class="btn btn-default m-r-5" title="Change Status" data-toggle="modal" data-target="#Change-status"><i class="fa fa-toggle-on "></i></button>
      <a href="<?php echo base_url(); ?>order/orderlistpdf" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
      <button type="button" class="btn btn-default m-r-5" title="Export To CSV"><i class="fa fa-file"></i></button>
      <button type="button" class="btn btn-default m-r-5" title="Delivery Tag" data-toggle="modal" data-target="#delivery"><i class="fa fa-truck"></i></button>

      </div>-->

      </div>
      </div>
      </div>
      </div>
      <!-- END TAB ALLORDER -->


      <!-- START TAB DELIVERY -->
      <div class="tab-pane" id="delivery">
      <div class="row">
      <div class="col-md-12">

      <div class="card-box table-responsive">
      <!--<div class="btn-group pull-right m-t-10 m-b-20">
      <button type="button" class="btn btn-default m-r-5" title="Delete" data-toggle="modal" data-target="#delete_order"><i class="fa fa-trash"></i></button>
      <button type="button" class="btn btn-default m-r-5" title="Change Status" data-toggle="modal" data-target="#Change-status"><i class="fa fa-toggle-on "></i></button>
      <a href="<?php echo base_url(); ?>Delivery/deliverydetailPdf" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
      <button type="button" class="btn btn-default m-r-5" title="Export To CSV"><i class="fa fa-file"></i></button>
      </div>-->

      </div>
      </div>
      </div>
      </div>
      <!-- END TAB DELIVERY -->

      <!-- START TAB COMPLETED -->

      <div class="tab-pane" id="completed">
      <div class="row">
      <div class="col-md-12">
      <div class="card-box table-responsive">
      <!-- <div class="btn-group pull-right m-t-10 m-b-20">
      <button type="button" class="btn btn-default m-r-5" title="Delete" data-toggle="modal" data-target="#delete_order"><i class="fa fa-trash"></i></button>
      <button type="button" class="btn btn-default m-r-5" title="Change Status" data-toggle="modal" data-target="#Change-status"><i class="fa fa-toggle-on "></i></button>
      <a href="<?php echo base_url(); ?>Delivery/deliverydetailPdf" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
      <button type="button" class="btn btn-default m-r-5" title="Export To CSV"><i class="fa fa-file"></i></button>
      </div>-->

      </div>
      </div>
      </div>

      </div>
      <!-- END TAB COMPLETED -->

      <!-- END TAB PICKUP -->

      <div class="tab-pane active" id="pickup">
      <div class="row">
      <div class="col-md-12">

      <div class="card-box table-responsive">
      <div class="btn-group pull-right m-t-10 m-b-20">
      <a href="<?php echo base_url() ?>order/orderlistpdf/pickup" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
      </div>
      <table class="table table-striped table-bordered table_shop_custom">
      <thead>
      <tr>
      <th>Order ID</th>
      <th>Delivery Details</th>
      <th>Sales Channel</th>
      <th>Order / Payment Status</th>
      <th>Customer</th>
      <th>Action</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach($all as $allOrderData)
      {
        if($allOrderData['main']['houdinv_order_confirmation_status'] == 'unbilled' || $allOrderData['main']['houdinv_order_confirmation_status'] == 'cancel' || $allOrderData['main']['houdinv_order_confirmation_status'] == 'return' || $allOrderData['main']['houdinv_order_confirmation_status'] == 'cancel request')
                {
                    $setStatusColor = 'red !important';
                }
                else if($allOrderData['main']['houdinv_order_confirmation_status'] == 'Processing' || $allOrderData['main']['houdinv_order_confirmation_status'] == 'order pickup' || $allOrderData['houdinv_order_confirmation_status'] == 'Processed' || $allOrderData['main']['houdinv_order_confirmation_status'] == 'billed')
                {
                    $setStatusColor = 'yellow !important';
                }
                else
                {
                    $setStatusColor = 'green !important';
                }
            ?>
             <tr>
            <td><a href="<?php echo base_url() ?>order/vieworder/<?php echo $allOrderData['main']['houdinv_order_id'] ?>">#<?php echo $allOrderData['main']['houdinv_order_id'] ?></a></td>
            <td><?php
            if($allOrderData['main']['houdinv_order_delivery_type'] == 'deliver')
            {
                $setDeliveryText = "Home Delivery";
            }
            else
            {
                $setDeliveryText = "Customer Pickup";
            }
            if($allOrderData['main']['houdinv_orders_deliverydate']) { echo date('d-m-Y',strtotime($allOrderData['main']['houdinv_orders_deliverydate'])); } ?><br/><?php echo "<label style='font-weight:normal;color:".$setStatusColor."'>".$allOrderData['main']['houdinv_order_confirmation_status']."</label><br/>".$setDeliveryText ?></td>
            <td><?php echo $allOrderData['main']['houdinv_order_type'] ?></td>
            <td><?php
            if($allOrderData['main']['houdinv_payment_status'] == 0) { $setPaymentStatus = 'Not Paid'; $setColor='red !important'; } else { $setPaymentStatus = 'Paid'; $setColor='green !important'; } echo "<label style='font-weight:normal !important;color:".$setStatusColor."'>".$allOrderData['main']['houdinv_order_confirmation_status']."</label>"." / <label style='font-weight:normal !important;color:".$setColor."'>".$setPaymentStatus."</label>"; ?></td>
            <td><?php
            $setToatalAmount = $allOrderData['main']['houdinv_orders_total_Amount'];
            if($allOrderData['user'][0]->houdinv_order_users_name)
            {
                $setUserString = $allOrderData['user'][0]->houdinv_order_users_name. "(".$allOrderData['user'][0]->houdinv_order_users_contact." )<br/>";
            }
            else
            {
                $setUserString = $allOrderData['user'][0]->houdinv_user_name. "( ".$allOrderData['user'][0]->houdinv_user_email." , ".$allOrderData['user'][0]->houdinv_user_contact." )<br/>";
            }
            echo "".$setUserString."".$currencysymbol."&nbsp;<strong>".$setToatalAmount."</strong>"
            ?></td>
            <td>
            <?php
            if($allOrderData['main']['houdinv_order_confirmation_status'] == 'unbilled')
            {
            ?>
            <a href="<?php echo base_url() ?>order/billedconfirmation/<?php echo $allOrderData['main']['houdinv_order_id'] ?>" class="btn btn-primary btn-sm">Confirm Order</a>
            <?php }
            ?>
            <?php 
            if($allOrderData['main']['houdinv_orders_total_remaining'] > 0)
            {
            ?>
            <button type="button" class="btn btn-sm btn-primary updatePaymentData" data-id="<?php echo $allOrderData['main']['houdinv_order_id'] ?>">Collect Payment</button>
            <?php }
            ?>
            <button type="button" class="btn btn-sm btn-primary updateOrderStatus" data-id="<?php echo $allOrderData['main']['houdinv_order_id'] ?>" data-status="<?php echo $allOrderData['main']['houdinv_order_confirmation_status'] ?>">Update Status</button>
            <button type="button" class="btn btn-sm btn-primary DeleteOrderBtn" data-id="<?php echo $allOrderData['main']['houdinv_order_id'] ?>">Delete</button>
            <?php
            if($allOrderData['main']['houdinv_order_delivery_type'] == "deliver" && $allOrderData['main']['houdinv_order_confirmation_status'] != "assigned" && $allOrderData['main']['houdinv_order_confirmation_status'] != "cancel")
            {
            ?>
            <button type="button" class="btn btn-sm btn-primary tagDeliveryBoydata" data-id="<?php echo $allOrderData['main']['houdinv_order_id'] ?>">Assign for delivery</button>
            <?php }
            ?>
            </td>
            </tr>
      <?php
      }
      ?>
      </tbody>
      </table>
      </div>
      </div>
      </div>
      </div>
      <!-- END TAB PICKUP -->

      <!-- START TAB QUOTATION -->

      <div class="tab-pane" id="quotation">
      <div class="row">
      <div class="col-md-12">

      <div class="card-box table-responsive">
      <!-- <div class="btn-group pull-right m-t-10 m-b-20">
      <button type="button" class="btn btn-default m-r-5" title="Delete" data-toggle="modal" data-target="#delete_order"><i class="fa fa-trash"></i></button>
      <button type="button" class="btn btn-default m-r-5" title="Change Status" data-toggle="modal" data-target="#Change-status"><i class="fa fa-toggle-on "></i></button>
      <a href="<?php echo base_url(); ?>Delivery/deliverydetailPdf" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
      <button type="button" class="btn btn-default m-r-5" title="Export To CSV"><i class="fa fa-file"></i></button>

      </div>-->
      <table class="table table-striped table-bordered table_shop_custom">
      <thead>
      <tr>
      <th><input type="checkbox"></th>
      <th>Order ID</th>
      <th>Ordered On</th>
      <th>Order By</th>
      <th>Order Mode(Web,App)</th>
      <th>Amount</th>
      <th>Payment Status</th>
      <th>Order Status</th>
      </tr>
      </thead>
      <tbody>
      <tr>
      <td><input type="checkbox"></td>
      <td><a href="<?php echo base_url(); ?>order/vieworder">gtghh</a></td>
      <td>JS5861112</td>
      <td>shouy</td>
      <td>Cash</td>
      <td>50840</td>
      <td><button type="button" class="btn btn-success btn btn-xs">Active</button></td>
      <td>
      <button type="button" class="btn btn-warning btn btn-xs">Deactive</button>
      </td>
      </tr>
      </tbody>
      </table>
      </div>
      </div>
      </div>
      </div>

      <!-- END TAB QUOTATION -->

      </div>

      </div>


      </div>
      <!-- END TAB -->
      </div>
      </div>
      </div>
      <!--change Status-->
      <div id="UpdateOrderStatus" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <?php echo form_open(base_url( 'Order/deliverypickup'), array( 'id' => 'allOrderChnageStatusData', 'method'=>'post' ));?>
            <div class="modal-content">

            <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

            <h4 class="modal-title">Change Status</h4>

            </div>

            <div class="modal-body">



            <div class="row">
            <div class="col-md-12">

            <div class="form-group no-margin">
            <input type="hidden" class="updateOrderStatusId required_validation_for_all_order" name="updateOrderStatusId"/>
            <label for="field-7" class="control-label">Change Status</label>

            <select class="form-control updateOrderStatusDataValue required_validation_for_all_order" name="updateOrderStatusDataValue">
            <option value="">Choose Status</option>
            <option value="unbilled">Unbilled</option>
            <option value="billed">billed</option>
            <option value="Processed">Processed</option>
            <option value="Processing">Processing</option>
            <option value="order pickup">order pickup</option>
            <option value="Not Delivered">Not Delivered</option>
            <option value="Delivered">Delivered</option>
            <option value="cancel request">cancel request</option>
            <option value="cancel">cancel</option>
            <option value="return">return</option>
            </select>

            </div>

            </div>
            </div>

            </div>

            <div class="modal-footer">

            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

            <input type="submit" class="btn btn-info " name="updateOrderStatusBtn" value="Update Status">

            </div>

            </div>
            <?php echo form_close(); ?>
            </div>

            </div>

            <!--Delete-->

            <div id="deleteOrderModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

            <div class="modal-dialog">
            <?php echo form_open(base_url( 'Order/deliverypickup'), array( 'method'=>'post' ));?>
            <div class="modal-content">

            <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

            <h4 class="modal-title">Delete Order</h4>
            </div>
            <div class="modal-body">
            <div class="row">
            <div class="col-md-12">
            <input type="hidden" class="deleteOrderId" name="deleteOrderId"/>
            <h4><b>Do you really want to Delete this order ?</b></h4>
            </div>
            </div>
            </div>
            <div class="modal-footer">

            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

            <input type="submit" class="btn btn-info" name="deleteOrderBtnSet" value="Delete">

            </div>

            </div>
            <?php echo form_close(); ?>
            </div>

            </div>

            <!--Start Delivery-->
            <div id="deliveryTagModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

            <div class="modal-dialog">
            <?php echo form_open(base_url( 'Order/deliverypickup'), array( 'id'=>"deliveryTagData", 'method'=>'post' ));?>
            <div class="modal-content">

            <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

            <h4 class="modal-title">Delivery Tag</h4>

            </div>

            <div class="modal-body">


            <div class="row">
            <div class="col-md-12">
            <form action="#">
            <div class="form-group setErrorMessage">

            </div>
            <div class="form-group">
            <label>Choose Delivery Type</label>
            <input type="hidden" class="deliveryOrderId" name="deliveryOrderId"/>
            <input type="hidden" class="deliveryboyEmail" name="deliveryboyEmail"/>
            <select class="form-control required_validation_for_all_order setDeliveryType" name="deliveryType">
            <option value="">Choose Delivery Types</option>
            <option value="1">Courier Service</option>
            <option value="2">Delivery Boy</option>
            <option value="3">Customer Pickup</option>
            </select>
            </div>
            <div class="form-group">
            <label for="userName">Choose Delivery Service</label>
            <select class="form-control select2 setAllOrdersdata" name="deliveryBoyId">
             <option value="">Choose Delivery Boy</option>
            </select>
            </div>
            </div>
            </div>

            </div>

            <div class="modal-footer">

            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

            <input type="submit" class="btn btn-info" name="tagDeliveryBoys" value="Add tag">

            </div>

            </div>
            <?php echo form_close(); ?>
            </div>

            </div>
              <!--Payment Pending-->
     <div id="updatePaymentStatusModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

<div class="modal-dialog">
<?php echo form_open(base_url( 'Order/updatePaymentStatus'), array( 'id'=>"updatePaymentStatusForm", 'method'=>'post' ));?>
<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

<h4 class="modal-title">Update Payment</h4>

</div>

<div class="modal-body">


<div class="row">
<div class="col-md-12">
<form action="#">
<div class="form-group setErrorMessage">

</div>
<div class="form-group">
<label>Total Order Amount</label>
<input type="text" class="form-control totalOrderAmount required_validation_for_all_paymentStatus" name="totalOrderAmount" readonly="readonly"/>
</div>
<div class="form-group">
<label for="userName">Total Paid Amount</label>
<input type="text" class="form-control totalPaidAmount required_validation_for_all_paymentStatus" name="totalPaidAmount" readonly="readonly"/>
</div>
<div class="form-group">
    <input type="hidden" name="paymentOrderId" class="paymentOrderId"/>
    <input type="hidden" name="paymentOrderPage" value="pickup"/>
    <input type="hidden" name="customerId" class="customerId"/>
    <input type="hidden" name="totalPendingAmount" class="totalPendingAmount"/>
<label for="userName">Total Remaining Amount</label>
<input type="text" class="form-control totalRemainingAmount required_validation_for_all_paymentStatus" name="totalRemainingAmount" readonly="readonly"/>
</div>
<div class="form-group">
<label for="userName">Amount Privilage</label>
<input type="text" class="form-control AmountPriv required_validation_for_all_paymentStatus" name="AmountPriv" readonly="readonly"/>
</div>
<div class="form-group">
<label for="userName">Amount Paid</label>
<input type="text" class="form-control AmountPaid required_validation_for_all_paymentStatus number_validation name_validation" name="AmountPaid"/>
</div>
<div class="form-group">
<label for="userName">New Reamaining Amount</label>
<input type="text" class="form-control newRemainingAmount required_validation_for_all_paymentStatus" name="newRemainingAmount" readonly="readonly"/>
</div>
<div class="form-group">
<label for="userName">Choose Payment Status</label>
<select class="form-control required_validation_for_all_paymentStatus" name="paymentStatus">
<option value="">Choose PaymentStatus</option>
<option value="1">Paid</option>
<option value="0">Not Paid</option>
</select>
</div>
<div class="form-group">
<label for="userName">Choose Payment Mode</label>
<select class="form-control required_validation_for_all_paymentStatus" name="paymentMode">
<option value="">Choose PaymentStatus</option>
<option value="cash">Cash</option>
<option value="card">Card</option>
<option value="cheque">Cheque</option>
</select>
</div>
</div>
</div>

</div>

<div class="modal-footer">
<div class="alert alert-danger setFooterErrorMessage" style="display:none">Sndf</div>
<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

<input type="submit" class="btn btn-info" name="UpdatePaymentStatusBtn" value="Update Payment">

</div>

</div>
<?php echo form_close(); ?>
</div>

</div>
      <?php $this->load->view('Template/footer.php') ?>

  <!-- CLient side form validation -->
  <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#allOrderChnageStatusData,#deliveryTagData',function(){
			var check_required_field='';
			$(this).find(".required_validation_for_all_order").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>
