<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <div class="btn-group pull-right m-t-15">
                                    <a href="" class="btn btn-default btn-sm m-l-5" data-toggle="modal" data-target="#add_business_catagory"> <i class="fa fa-plus m-r-5"></i>Add Business</a>
                                </div>
                                <h4 class="page-title">Business Category</h4>
                            </div>
                        </div>
                                            <?php
                                    
            if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    ?>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="card-box table-responsive">
                              <table class="table table-striped table-bordered table_shop_custom">
                                <thead>
                                  <tr>
                                    <th>Business Name</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <?php
                                
                                 foreach($category as $items)
                                {?>
                                  <tr>
                                    <td><?php echo $items['houdin_business_category_name'];?></td>
                                    <td><?php echo $items['houdin_business_category_desc'];?></td>
                                    <td><button type="button" class="btn btn-warning btn btn-xs"><?php echo $items['houdin_business_categories_status'];?></button></td>
                                    <td>
                                        <a href="#" class="btn btn-warning btn btn-sm edit_popup" data-toggle="modal" data-target="#edit_business_catagory"><i class="fa fa-pencil m-r-5"></i>Edit</a>
                                        <input type="hidden" id="edit_all_data" name="<?php echo $items['houdin_business_category_name'];?>" data-des="<?php echo $items['houdin_business_category_desc'];?>" data-status="<?php echo $items['houdin_business_categories_status'];?>" data-id="<?php echo $items['houdin_business_category_id'];?>"/>
                                        <button type="button" class="btn btn-warning btn btn-sm delete_popup" data-toggle="modal" data-target="#delete_business_catagory"><i class="fa fa-trash m-r-5"></i>Delete</button>
                                    </td>
                                  </tr>
                                  <?php }
                                  ?>
                                </tbody>
                              </table>
                             <ul class="pagination pull-right"> 
                              <?php echo $this->pagination->create_links(); ?> </ul>
                            </div>
                          </div>
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->



<div id="delete_business_catagory" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >

<div class="modal-dialog">
<?php echo form_open(base_url( 'setting/businesscategory' ),  array( 'method' => 'post', 'id'=>"Delete_form" ));?>
 
<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

<h4 class="modal-title">Delete Entry</h4>

</div>

<div class="modal-body">
<div class="row">
<div class="col-md-12">

<h4><b>Do you really want to delete this entry ?</b></h4>
</div>
</div>
</div>

<div class="modal-footer">
<input type="hidden" id="delete_input" name="delete_input">
<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

<input type="submit" class="btn btn-info" name="delete" value="Delete">

</div>

</div>
<?php echo form_close();?>
</div>

</div>


<div id="edit_business_catagory" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<?php echo form_open(base_url( 'setting/businesscategory' ),  array( 'method' => 'post', 'id'=>"Edit_form" ));?>
  
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
<h4 class="modal-title">Edit Business Category</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-12">
<div class="form-group no-margin">
<label for="field-7" class="control-label">Business Name</label>
<input type="text" id="Edit_name" name="name" class="form-control required_validation_for_register" placeholder="Business Name">
<input type="hidden" id="Edit_id" name="Edit_id">
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group no-margin">
<label for="field-7" class="control-label">Business Description</label>
<input type="text" id="Edit_des" name="description" class="form-control required_validation_for_register" placeholder="Business Description">
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group no-margin">
<label for="field-7" class="control-label">Status</label>
<select class="form-control required_validation_for_register" id="Edit_status" name="status">
<option value="">Choose Status</option>
<option value="active">Active</option>
<option value="deactive">Dective</option>
<option value="block">Blocked</option>
</select>
</div>
</div>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
<input type="submit" class="btn btn-info" name="Edit" value="Sumbit">
</div>
</div>
 <?php echo form_close();?>
</div>
</div>
<div id="add_business_catagory" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<?php echo form_open(base_url( 'setting/businesscategory' ),  array( 'method' => 'post', 'id'=>"Add_form" ));?>
     
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
<h4 class="modal-title">Add Business Category</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-12">
<div class="form-group no-margin">
<label for="field-7" class="control-label">Business Name</label>
<input type="text" name="name" class="form-control required_validation_for_register" placeholder="Business Name">
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group no-margin">
<label for="field-7" class="control-label">Business Description</label>
<input type="text" name="description" class="form-control required_validation_for_register" placeholder="Business Description">
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group no-margin">
<label for="field-7" class="control-label">Status</label>
<select class="form-control required_validation_for_register" name="status">
<option value="">Choose Status</option>
<option value="active">Active</option>
<option value="deactive">Dective</option>
<option value="block">Blocked</option>
</select>
</div>
</div>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
<input type="submit" class="btn btn-info" name="Add" value="Sumbit">
</div>
</div>
 <?php echo form_close();?>
</div>
</div>



<div id="send_emial_shop" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
<form>
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                  <h4 class="modal-title">Send Email</h4>
                </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group no-margin">
                            <label for="field-7" class="control-label">Subject</label>
                            <input type="text" class="form-control" placeholder="Subject" />
                        </div>
                    </div>
                </div>
                <div class="row">
                   <div class="col-md-12">
                        <div class="form-group no-margin">
                            <label for="field-7" class="control-label">Message</label>
                            <textarea class="form-control" rows="4"></textarea>
                        </div>

                    </div>
                </div>
              </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-info" name="" value="Update Status">
          </div>

     </div>
     </form>
 
 </div>
</div>
<?php $this->load->view('Template/footer.php') ?>
        <script type="text/javascript">
    /*==============Form Validation===========================*/
        $(document).ready(function(){

        $(document).on('submit','#Edit_form,#Add_form',function(c){
            
                               var rep_image_val='';
                 $(this).find(".required_validation_for_register").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                        
                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
                
                $(this).find('.required_validation_for_register').on('keyup blur',function()
                                {
                                    
                                        $(this).css("border-color","#ccc");
                                });
                                
                            if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                else
                {
                }  
            
          });
          
          
          // category edit detail fill 
          $(document).on('click','.edit_popup',function()
          {
            console.log($(this).children('#edit_all_data'));
            
            $('#Edit_name').val($(this).siblings('#edit_all_data').attr("name"));
             $('#Edit_id').val($(this).siblings('#edit_all_data').attr("data-id"));
              $('#Edit_des').val($(this).siblings('#edit_all_data').attr("data-des"));
               $('#Edit_status').val($(this).siblings('#edit_all_data').attr("data-status"));
          });
          
                    // category delete detail fill 
          $(document).on('click','.delete_popup',function()
          {
            console.log($(this).parents('#edit_all_data'));
            $('#delete_input').val($(this).siblings('#edit_all_data').attr("data-id"));
           
          });
          
        });
            </script>
