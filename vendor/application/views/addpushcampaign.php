<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">Add Push Campaign</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>Campaign">Campaign</a></li>
                  <li class="active">Add Push Campaign</li>
                  </ol>
                  </div>
                 
            </div>
           <!--END Page-Title -->
            <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
           <!--END Page-Title -->
            <div class="row m-t-20">
        <div class="col-sm-12">
        <div class="card-box">
 <?php echo form_open(base_url('Campaign/campaign_addpush'), array( 'id' => 'campaign_addsms', 'method'=>'POST' ));?>
        <div class="row">
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 control-label"> Campaign Name</label>
        <div class="col-md-10">
         <input type="text" class="form-control required_validation_for_add_custo" placeholder="Campaign Name" name="campaign_name">
        </div>
        </div>


         <div class="form-group">
        <label class="col-md-2 control-label"> Customers Name</label>
        <div class="col-md-10">
       <select class="form-control select2 select_input_requred" name="campaignCustomer[]" multiple="">
            <option value="">Choose Customers</option>
            <?php 
            foreach($customerList as $customerListData)
            {
             ?> 
             <option value="<?php echo $customerListData->houdinv_user_id ?>"><?php echo $customerListData->houdinv_user_name ?></option>  
           <?php }
            ?>
            </select>
        </div>
        </div>

         <div class="form-group">
        <label class="col-md-2 control-label"> Customer Group</label>
        <div class="col-md-10">
        
            <select class="form-control select2 select_input_requred" name="campaignCustomerGroup[]" multiple="">
            <option value="">Choose Customer Group</option>
            <?php 
            foreach($customerGroupLits as $customerGroupLitsData)
            {
             ?> 
             <option value="<?php echo $customerGroupLitsData->houdinv_users_group_id ?>"><?php echo $customerGroupLitsData->houdinv_users_group_name ?></option>  
           <?php } ?>
            </select>
        </div> 
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Status</label>
        <div class="col-md-10">
        <select class="form-control required_validation_for_add_custo" name="campaign_status">  
        <option value="">Choose Status</option>       
        <option value="1">Active</option>
        <option value="0">Deactive</option>
        </select>
        </div>
        </div>


        <div class="form-group">
        <label class="col-md-2 control-label">Campaign Text</label>
        <div class="col-md-10">
        <textarea class="form-control required_validation_for_add_custo" name="campaign_text"></textarea>
        </div>
        </div>
        <div class="choosedate">
        
        </div>
        </div>
        <div class="col-md-12 ">
         
          <input type="submit" class="btn btn-success pull-right m-r-10" name="addcampign"  value="Add Campaign">

        <!--<input type="submit"  class="btn btn-default pull-right m-r-10 save_later" name="save__send" value="Save & Send">-->

        <button type="reset" class="btn btn-info pull-right m-r-10 save_later" id="save_later">Save & Send Later</button>
        </div>
        </div>
       <?php echo form_close(); ?>
        </div>
        </div>
        </div>
          </div>
        </div>
          </div>

<?php $this->load->view('Template/footer.php') ?>
<script>
 $(document).ready(function(){
        $(".select2").select2();

        });
        </script>
<script type="text/javascript">  
$(document).ready(function(){
    $('#save_later').click(function(){
  
$('.choosedate').html('<div class="form-group"><label class="col-md-2 control-label">Choose Date</label><div class="col-md-10"><input type="text" class="form-control date_picker required_validation_for_add_custo" placeholder="Choose Date"  name="campaign_date" /></div></div>');


$(".save_later").attr("disabled", true);



$('.date_picker').focus(function(){
            $(this).daterangepicker({                    
            singleDatePicker: true,
            showDropdowns: false,    
            locale: { 
                format: 'YYYY-MM-DD',
            }
            });      
        });   

    });

 $('#campaign_addsms').submit(function(e){ 

var check_required_field='';
      $(this).find(".required_validation_for_add_custo").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {
        e.preventDefault(); 
        return false;
      }else{      
       return true;

      }  
    });  


 }); 

</script>
