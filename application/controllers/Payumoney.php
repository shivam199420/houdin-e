<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payumoney extends CI_Controller {
	function __construct() 
    {
        parent::__construct();
        if(!$_COOKIE['tempUserPackageId'])
        {
            $this->load->helper('url');
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(''.base_url().'/vendor', 'refresh'); 
        }
    }
    public function index()
    {
        $this->load->view('payumoney');
    }
    public function payumoneyResponse()
    {
        print_r($_POST);
    }
}