    <?php $this->load->view('Template/header.php') ?>
    <?php $this->load->view('Template/sidebar.php') ?>
    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">
    <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Customer Group</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>customer/Customerlanding">Customer Management</a></li>
                  <li class="active">Customer Group</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                  
            </div>
           <!--END Page-Title --> 

    <div class="row m-t-20">
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Customer.png">
    <h2 class="m-0 text-dark counter font-600"><?php if($totalCustomer) { echo $totalCustomer; } else { echo 0; } ?></h2>
    <div class="text-muted m-t-5">Total Group</div>
    </div>
    </div>  
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Group.png">
    <h2 class="m-0 text-dark counter font-600"><?php if($activeCustomer) { echo $activeCustomer; } else { echo 0; } ?></h2>
    <div class="text-muted m-t-5">Total Active Group</div>
    </div>
    </div> 
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Group.png">
    <h2 class="m-0 text-dark counter font-600"><?php if($deactiveCustomer) { echo $deactiveCustomer; } else { echo 0; } ?></h2>
    <div class="text-muted m-t-5">Total Deactive Group</div>
    </div>
    </div> 
    </div>
    <div class="row">
    <div class="col-md-12">
    <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
    ?>
    </div>
    </div>
    <div class="row">
    <div class="col-md-12">

    <div class="card-box table-responsive">
    <div class="btn-group pull-right m-t-10 m-b-20">

    <button type="button" class="btn btn-default m-r-5" title="Create Group" data-toggle="modal" data-target="#addCustomerGroupModal"><i class="fa fa-plus"></i></button>


    </div>
    <table class="table table-striped table-bordered table_shop_custom">
    <thead>
    <tr>
    <th>Group Name</th>
    <th>Group Status</th>
    <th>Created Date</th>
    <th>Action</th>
    </tr>
    </thead>


    <tbody>
    <?php 
    foreach($groupList as $groupListData)
    {
    ?>
    <tr>
    <td><a href="<?php echo base_url(); ?>Customer/viewcustomergroup/<?php echo $groupListData->houdinv_users_group_id ?>"><?php echo $groupListData->houdinv_users_group_name ?></a></td>
    <td>
    <?php 
    if($groupListData->houdinv_users_group_status == 'active')
    {
      $setText = "Active";
      $setClass = "success";
    }
    else if($groupListData->houdinv_users_group_status == 'deactive')
    {
      $setText = "Deactive";
      $setClass = "warning";
    }
    ?>
    <button type="button" class="btn btn-<?php echo $setClass ?> btn-xs"><?php echo $setText; ?></button>
    </td>
    <td><?php echo date('d-m-Y',$groupListData->houdinv_users_group_created_at); ?></td>
    <td>
    <button class="btn btn-success waves-effect waves-light editCustomerGroupData" data-id="<?php echo $groupListData->houdinv_users_group_id ?>" data-name="<?php echo $groupListData->houdinv_users_group_name ?>" data-status="<?php echo $groupListData->houdinv_users_group_status ?>">Edit</button>
    <button class="btn btn-success waves-effect waves-light deleteCustomerGroupBtn" data-id="<?php echo $groupListData->houdinv_users_group_id ?>">Delete</button>
    <a class="btn btn-success waves-effect waves-light" target="_blank" href="<?php echo base_url(); ?>Coupons/add/customergroup/<?php echo $groupListData->houdinv_users_group_name ?>/<?php echo $groupListData->houdinv_users_group_id ?>">Add coupon</a>
    </td>
    </tr>
    <?php } ?>
    </tbody>
    </table>
    <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>

    <!--Delete-->

    <div id="deleteCustomerGroupModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">
    <?php echo form_open(base_url( 'customer/customergroup' ), array('method'=>'post' ));?>
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Delete Group</h4>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-md-12">
    <input type="hidden" class="deleteGroupId" name="deleteGroupId"/>
    <h4><b>Do you really want to Delete this group ?</b></h4>
    </div>
    </div>
    </div>
    <div class="modal-footer">

    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

    <input type="submit" class="btn btn-info" name="deleteCustomerGroup" value="Delete">

    </div>

    </div>
    <?php echo form_close(); ?>
    </div>

    </div>
    <!-- START Add -->
    <div id="addCustomerGroupModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">
    <?php echo form_open(base_url( 'customer/customergroup' ), array( 'id' => 'addCustomerGroupForm', 'method'=>'post' ));?>
    <div class="modal-content">

    <div class="modal-header">

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Create Group</h4>
    </div>

    <div class="modal-body">

    <div class="row">
    <div class="col-md-12">

    <div class="form-group">

    <label for="field-7" class="control-label">Group Name</label>

    <input type="text" name="customerGroupName" class="form-control required_validation_for_add_customer_group name_validation" placeholder="Group Name">
    </div>
    <div class="form-group">

    <label for="field-7" class="control-label">Choose Status</label>

    <select class="form-control required_validation_for_add_customer_group" name="customerGroupStatus">
    <option value="">Choose Status</option>
    <option value="active">Active</option>
    <option value="deactive">Deactive</option>
    </select>
    </div>
    </div>
    </div>
    </div>
    <div class="modal-footer">

    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

    <input type="submit" class="btn btn-info " name="addCustomerGroupBtn" value="Add Group">

    </div>

    </div>
    <?php echo form_close(); ?>
    </div>

    </div>
    <!--END Add -->    
    <!-- START edit -->
    <div id="editCustomerGroupModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">
    <?php echo form_open(base_url( 'customer/customergroup/' ), array( 'id' => 'editCustomerGroupForm', 'method'=>'post' ));?>
    <div class="modal-content">

    <div class="modal-header">

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
     <h4 class="modal-title">Edit Group</h4>
    </div>

    <div class="modal-body">

    <div class="row">
    <div class="col-md-12">

    <div class="form-group">
    <input type="hidden" class="editGroupId" name="editGroupId"/>
    <label for="field-7" class="control-label">Group Name</label>

    <input type="text" name="editCustomerGroupName" class="form-control required_validation_for_edit_customer_group name_validation editCustomerGroupName" placeholder="Group Name">
    </div>
    <div class="form-group">

    <label for="field-7" class="control-label">Choose Status</label>

    <select class="form-control required_validation_for_edit_customer_group editCustomerGroupStatus" name="editCustomerGroupStatus">
    <option value="">Choose Status</option>
    <option value="active">Active</option>
    <option value="deactive">Deactive</option>
    </select>
    </div>
    </div>
    </div>
    </div>
    <div class="modal-footer">

    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

    <input type="submit" class="btn btn-info " name="editCustomerGroupBtn" value="Update Group">

    </div>

    </div>
    <?php echo form_close(); ?>
    </div>

    </div>
    <!--END Add -->    
    <?php $this->load->view('Template/footer.php') ?>
    <!-- CLient side form validation -->
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addCustomerGroupForm',function(){
			var check_required_field='';
			$(".required_validation_for_add_customer_group").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>
  <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#editCustomerGroupForm',function(){
			var check_required_field='';
			$(".required_validation_for_edit_customer_group").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>