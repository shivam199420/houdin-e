        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <style type="text/css">
        @media screen and (min-width: 768px){}
        .dropdown.dropdown-lg .dropdown-menu {
        min-width: 390px!important;
        }
        }
        </style>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">
        <!--START Page-Title -->
        <div class="row">
     
        <div class="col-md-8">
        <h4 class="page-title">Gift Vouchers</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li class="active">Gift Vouchers</li>
        </ol>
        </div>
        <!-- <div class="col-md-4">
        <form role="search" class="navbar-left app-search pull-left custom_search_all">
        <input type="text" placeholder="Search..." class="form-control">
        <a href=""><i class="fa fa-search"></i></a>
        </form></div> -->

        </div>
        <div class="row">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        ?>
        </div>
        <!--END Page-Title -->
        <div class="row m-t-20">
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_GiftVoucher.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($total) { echo $total; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total Gift Voucher</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Giftvoucher.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($totalActive) { echo $totalActive; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total Active Gift Coupons</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Giftvoucher.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($totalDeactive) { echo $totalDeactive; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total Deactive Gift Coupons</div>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">

        <div class="card-box table-responsive"> 
        <div class="btn-group pull-right m-t-10 m-b-20">
        <a href="<?php echo base_url(); ?>giftvouchers/add" class="btn btn-default m-r-5" title="Add gift voucher"><i class="fa fa-plus"></i></a>
        <button type="button" class="btn btn-default m-r-5 setSupplierMultiBtn deleteMultipleVoucher" style="display:none" title="Delete"><i class="fa fa-trash"></i></button>
        <button type="button" class="btn btn-default m-r-5 setSupplierMultiBtn changeStatusMultippleVoucher" style="display:none" title="Change Status"><i class="fa fa-toggle-on"></i></button>
        </div>
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th><input type="checkbox" class="masterSupplierCheck"></th>
        <th>Gift Voucher Name</th>
        <th>Gift Voucher Valid From - To</th>
        <th>Gift Voucher Code</th>
        <th>Gift Voucher Discount</th>
        <th>Gift Voucher Status</th>
        <th>Action</th>

        </tr>

        </thead>
        <tbody>
        <?php 
        foreach($voucherList as $voucherListData)
        {
        ?>    
        <tr>
        <td><input type="checkbox" class="childSupplierCheck" data-id="<?php echo $voucherListData->houdinv_vouchers_id ?>"></td>
        <td> <a href="<?php echo base_url(); ?>giftvouchers/view/<?php echo $voucherListData->houdinv_vouchers_id ?>"><?php echo $voucherListData->houdinv_vouchers_name ?></a></td>
        <td><?php echo date('d-m-Y',$voucherListData->houdinv_vouchers_valid_from)." - ".date('d-m-Y',$voucherListData->houdinv_vouchers_valid_to); ?></td>
        <td><?php echo $voucherListData->houdinv_vouchers_code ?></td>
        <td><?php echo $voucherListData->houdinv_vouchers_discount ?>%</td>
        <td>
        <?php 
        if($voucherListData->houdinv_vouchers_status == 'active') { $setText="Active";$setClass="success"; }
        else if($voucherListData->houdinv_vouchers_status == 'deactive') { $setText="Deactive";$setClass="warning"; }
        ?>
        <button type="button" class="btn btn-xs btn-<?php echo $setClass; ?>"><?php echo $setText; ?></button>
        </td>
        <td> 
        <a href="<?php echo base_url(); ?>giftvouchers/edit/<?php echo $voucherListData->houdinv_vouchers_id ?>"><button class="btn btn-success waves-effect waves-light" >Edit</button></a> 
        <button data-id="<?php echo $voucherListData->houdinv_vouchers_id ?>" class="btn btn-success waves-effect waves-light deleteSingleGiftVoucher">Delete</button>
        <a href="<?php echo base_url(); ?>Giftvouchers/Giftprint/<?php echo $voucherListData->houdinv_vouchers_id ?>"><button class="btn btn-success waves-effect waves-light" >Print</button></a>
        </td>
        </tr>
        <?php }
        ?>
        </tbody>
        </table>
        <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
        </div>
        </div>
        </div>


        </div>
        </div>
        </div>

        <!-- Delete Modal -->
        <div id="deleteGiftVoucher" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Giftvouchers' ), array( 'method'=>'post' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Voucher</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleteVoucherId" name="deleteVoucherId"/>
        <h4><b>Do you really want to Delete this gift voucher ?</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="deleteGiftVoucherData" value="Delete">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>



        <!---Start Change Status -->
        <div id="updateGiftVouchermodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <?php echo form_open(base_url( 'Giftvouchers' ), array( 'method'=>'post', 'id'=>'GiftVoucherForm' ));?>
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Change Status</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group"> 
        <input type="hidden" class="updateStatusId" name="updateStatusId"/>
        <label for="field-1" class="control-label">Choose Status</label> 
        <select class="form-control required_validation_for_gift_vouchers" name="editGiftVoucherStatus">
        <option value="">Choose Status</option>
        <option value="active">Active</option>
        <option value="deactive">Deactive</option>
        </select>
        </div> 
        </div> 
        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <input type="submit" class="btn btn-info" name="updateStatusData" value="Update Status"/>
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div>
        <!-- End Status -->



        <!---Start Send -->
        <div id="send" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Send Email</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12 form-horizontal"> 
        <div class="form-group">
        <label class="col-md-3 control-label">Subject</label>
        <div class="col-md-9">
        <input type="text" value="" class="form-control" name="" placeholder="Subject">
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-3 control-label">Message</label>
        <div class="col-md-9">
        <textarea style="width:100%" type="text"  value="" class="form-control" name="" placeholder="Message"></textarea>
        </div>
        </div>  
        </div> 

        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <button type="button" class="btn btn-info waves-effect waves-light">Send</button> 
        </div> 
        </div> 
        </div>
        </div><!-- End Send -->

        <!---Start Text -->
        <div id="text" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Send Text</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12 form-horizontal"> 
        <div class="form-group">
        <label class="col-md-3 control-label">Subject</label>
        <div class="col-md-9">
        <input type="text" value="" class="form-control" name="" placeholder="Subject">
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-3 control-label">Message</label>
        <div class="col-md-9">
        <textarea style="width:100%" type="text"  value="" class="form-control" name="" placeholder="Message"></textarea>
        </div>
        </div>  
        </div> 

        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <button type="button" class="btn btn-info waves-effect waves-light">Send</button> 
        </div> 
        </div> 
        </div>
        </div><!-- End Text -->
        <?php $this->load->view('Template/footer.php') ?>
        <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#GiftVoucherForm',function(){
                var check_required_field='';
                $(this).find(".required_validation_for_gift_vouchers").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>