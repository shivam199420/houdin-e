<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends CI_Controller 
{
        	function __construct() 
    {
        parent::__construct();
        
        $this->auth_users->is_logged_in();
        $this->load->library('form_validation'); 
        $this->load->model('Categorymodel'); 
        $this->load->model('Storesettingmodel'); 
        		$this->load->helper('url');
		$this->load->library('pagination');
        $this->perPage=30;
                $this->Validconfigs = array(
        array(
                'field' => 'category_name',
                'label' => 'Category name',
                'rules' => 'required'
        ),

        array(
                'field' => 'category_desc',
                'label' => 'Category description',
                'rules' => 'required'
              ),
        array('field'=>'category_status',
              'label'=>'Category status',
              'rules'=>'required'
              )
            );
            
        
      $this->Productconfigs = array(
        array(
                'field' => 'type',
                'label' => 'product type name',
                'rules' => 'required'
        ),

        array(
                'field' => 'description',
                'label' => 'description',
                'rules' => 'required'
              ),
        array('field'=>'status',
              'label'=>'status',
              'rules'=>'required'
              )
            );
        
	}
    
      public function Categorylanding()
    {
        $this->load->view('categorymanagementlandingpage');
    }
    public function categorymanagement()
    {
        
       
        if($this->input->post('add_Form'))
        {
           
                $this->form_validation->set_rules($this->Validconfigs);
                
            //     if (empty($_FILES['category_thumb']['name']))
            //       {
            //  $this->form_validation->set_rules('category_thumb', 'Image', 'required');
            //     }
                
                if($this->input->post('sub'))
                {
                 $this->form_validation->set_rules('sub[]','Subcategory','required');  
               // $sub = json_Encode($this->input->post('sub'));  
                 $sub = $this->input->post('sub');   
                }
                else
                {
                    $sub='';
                }
                if($this->form_validation->run() != true)
                {
                       $this->session->set_flashdata('message_name',validation_errors() );
                      redirect(base_url().'Category/categorymanagement', 'refresh');    
                }
             
                                    if(!empty($_FILES['category_thumb']['name']))
                    {
                        $newFileName = rand(100000,999999)."".$_FILES['category_thumb']['name'];
                        $config['upload_path'] = "images/category/";
                        $config['allowed_types'] = 'jpg|jpeg|png';
                        $config['max_size'] = '6144';
                        $config['file_name'] = $newFileName;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('category_thumb'))
                        {
                                  //  print_R("no",$_FILES['category_thumb']['name']);
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                        }
                        else
                        {
                          //  print_R("po");
                          $picture = '';  
                        }
                    }
                    else
                    {
                        //print_R("mo");
                         $picture = ''; 
                    }
               // exit;
                $date = strtotime(date("Y-m-d"));
                $array=array("houdinv_category_description"=>$this->input->post('category_desc'),
                             "houdinv_category_name"=>$this->input->post('category_name'),
                           //  "houdinv_category_subcategory"=>$sub,
                             "houdinv_category_status"=>$this->input->post('category_status'),
                             "houdinv_category_created_date"=>$date,
                             "houdinv_category_updated_date"=>$date,
                             "houdinv_category_thumb"=>$picture);
                
                $add_data = $this->Categorymodel->Add($array,$sub);
                if ($this->input->post('product_type')) {
                    $redirect_url = base_url().'Product/addproduct';
                } else {
                    $redirect_url = base_url().'Category/categorymanagement';
                }
                if($add_data['message'] == 'yes')
                {
                  $setArray = array('id'=>$add_data['id'],'name'=>$this->input->post('category_name'));
                  $this->Storesettingmodel->addCategoeyData($setArray);
                       $this->session->set_flashdata('success',"Category added succesfully" );
                      redirect($redirect_url, 'refresh');                      
                }
                else if($add_data['message'] == 'exist')
                {
                       $this->session->set_flashdata('message_name',"Category name already exist" );
                      redirect($redirect_url, 'refresh');                      
                }
                else
                {
                       $this->session->set_flashdata('message_name',"Somthing went wrong ! try again" );
                      redirect($redirect_url, 'refresh');                      
                }
        }
        
        
                
        if($this->input->post('edit_Form'))
        {

                $this->form_validation->set_rules($this->Validconfigs);
                

                if($this->input->post('sub'))
                {
                 $this->form_validation->set_rules('sub[]','Subcategory','required');  
               // $sub = json_Encode($this->input->post('sub'));  
                  $sub = $this->input->post('sub');  
                }
                else
                {
                    $sub='';
                }
               
                if($this->form_validation->run() != true)
                {
                       $this->session->set_flashdata('message_name',validation_errors() );
                      redirect(base_url().'Category/categorymanagement', 'refresh');    
                }
             
                                    if(!empty($_FILES['category_thumb']['name']))
                    {
                        $newFileName = rand(100000,999999)."".$_FILES['category_thumb']['name'];
                        $config['upload_path'] = "images/category/";
                        $config['allowed_types'] = 'jpg|jpeg|png';
                        $config['max_size'] = '6144';
                        $config['file_name'] = $newFileName;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('category_thumb'))
                        {
                                  //  print_R("no",$_FILES['category_thumb']['name']);
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                        }
                        else
                        {
                          //  print_R("po");
                          $picture = '';  
                        }
                    }
                    else
                    {
                        //print_R("mo");
                         $picture = ''; 
                    }
               // exit;
                $date = strtotime(date("Y-m-d"));
                $array=array("houdinv_category_description"=>$this->input->post('category_desc'),
                             "houdinv_category_name"=>$this->input->post('category_name'),
                             "houdinv_category_subcategory"=>'',
                             "houdinv_category_status"=>$this->input->post('category_status'),
                             "houdinv_category_updated_date"=>$date
                             );
                             if($picture)
                             {
                               $array["houdinv_category_thumb"]=$picture;
                             }
                $rt = $this->input->post('edit_id');
                $add_data = $this->Categorymodel->Edit($array,$rt,$sub);
                if($add_data)
                {
                       $this->session->set_flashdata('success',"Category updated succesfully" );
                      redirect(base_url().'Category/categorymanagement', 'refresh');                      
                }
                else
                {
                       $this->session->set_flashdata('message_name',"Somthing went wrong ! try again" );
                      redirect(base_url().'Category/categorymanagement', 'refresh');                      
                }
        }
        
        if($this->input->post('delete'))
        {
          $id = $this->input->post('delete_input');  
           $add_data = $this->Categorymodel->Delete($id);
            if($add_data)
            {
                   $this->session->set_flashdata('success',"Category deleted succesfully" );
                  redirect(base_url().'Category/categorymanagement', 'refresh');                      
            }
            else
            {
                   $this->session->set_flashdata('message_name',"Somthing went wrong ! try again" );
                  redirect(base_url().'Category/categorymanagement', 'refresh');                      
            }
        }
                // fetch all category data
        $All= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->Categorymodel->getAll($conditions);
        //pagination config
        $config['base_url']    = base_url().'Category/categorymanagement/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset; 
        $conditions['limit'] = $this->perPage;
        $All['category'] = $this->Categorymodel->getAll($conditions);
       
        $this->load->view('productcategorymanagement',$All);
    }
    public function producttype()
    {
         if($this->input->post('Add_product'))
        {

              $this->form_validation->set_rules($this->Productconfigs);
                
            //     if (empty($_FILES['attribute']['name']))
            //       {
            //  $this->form_validation->set_rules('attribute', 'Image', 'required');
            //     }
                
                
                

                if($this->input->post('Attr_type'))
                {
                 $this->form_validation->set_rules('Attr_type[]','Sub type attribute','required');  
                $sub = json_Encode($this->input->post('Attr_type'));  
                    
                }
                else
                {
                    $sub='';
                }
                if($this->form_validation->run() != true)
                {
                       $this->session->set_flashdata('message_name',validation_errors() );
                      redirect(base_url().'Category/producttype', 'refresh');    
                }
                
               if(!empty($_FILES['attribute']['name']))
                    {
                        $newFileName = rand(100000,999999)."".$_FILES['attribute']['name'];
                        $config['upload_path'] = "images/product_type/";
                        $config['allowed_types'] = 'jpg|jpeg|png';
                        $config['max_size'] = '6144';
                        $config['file_name'] = $newFileName;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('attribute'))
                        {
                                  //  print_R("no",$_FILES['category_thumb']['name']);
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                        }
                        else
                        {
                          //  print_R("po");
                          $picture = '';  
                        }
                    }
                    else
                    {
                        //print_R("mo");
                         $picture = ''; 
                    }
               // exit;
                $date = strtotime(date("Y-m-d"));
                $array=array("houdinv_productType_description"=>$this->input->post('description'),
                             "houdinv_productType_name"=>$this->input->post('type'),
                             "houdinv_productType_subattr"=>$sub,
                             "houdinv_productType_status"=>$this->input->post('status'),
                             "houdinv_productType_created_date"=>$date,
                             "houdinv_productType_update_date"=>$date,
                             "houdinv_productType_image"=>$picture);

               
                $add_data = $this->Categorymodel->AddProduct($array);
                if($add_data['message'] == 'yes')
                {
                       $this->session->set_flashdata('success',"Product type added succesfully" );
                      redirect(base_url().'Category/producttype', 'refresh');                      
                }
                else if($add_data['message'] == 'exist')
                {
                       $this->session->set_flashdata('message_name',"Product type already exist" );
                      redirect(base_url().'Category/producttype', 'refresh');                      
                }
                else
                {
                       $this->session->set_flashdata('message_name',"Somthing went wrong ! try again" );
                      redirect(base_url().'Category/producttype', 'refresh');                      
                }

        }
        
        
        if($this->input->post('Edit_product'))
        {

              $this->form_validation->set_rules($this->Productconfigs);
                

                
                
                

                if($this->input->post('Attr_type'))
                {
                 $this->form_validation->set_rules('Attr_type[]','Sub type attribute','required');  
                $sub = json_Encode($this->input->post('Attr_type'));  
                    
                }
                else
                {
                    $sub='';
                }
                if($this->form_validation->run() != true)
                {
                       $this->session->set_flashdata('message_name',validation_errors() );
                      redirect(base_url().'Category/producttype', 'refresh');    
                }
                
                
                 if(!empty($_FILES['attribute']['name']))
                    {
                        $newFileName = rand(100000,999999)."".$_FILES['attribute']['name'];
                        $config['upload_path'] = "images/product_type/";
                        $config['allowed_types'] = 'jpg|jpeg|png';
                        $config['max_size'] = '6144';
                        $config['file_name'] = $newFileName;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('attribute'))
                        {
                                  //  print_R("no",$_FILES['category_thumb']['name']);
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                        }
                        else
                        {
                          //  print_R("po");
                          $picture = '';  
                        }
                    }
                    else
                    {
                        //print_R("mo");
                         $picture = ''; 
                    }
               // exit;
                $date = strtotime(date("Y-m-d"));
                $array=array("houdinv_productType_description"=>$this->input->post('description'),
                             "houdinv_productType_name"=>$this->input->post('type'),
                             "houdinv_productType_subattr"=>$sub,
                             "houdinv_productType_status"=>$this->input->post('status'),
                            
                             "houdinv_productType_update_date"=>$date);
                             
                                                       if($picture)
                             {
                               $array["houdinv_productType_image"]=$picture;
                             }
                 $rt = $this->input->post('edit_id');
                $add_data = $this->Categorymodel->EditProduct($array,$rt);
                if($add_data)
                {
                       $this->session->set_flashdata('success',"Product type updated succesfully" );
                      redirect(base_url().'Category/producttype', 'refresh');                      
                }
                else
                {
                       $this->session->set_flashdata('message_name',"Somthing went wrong ! try again" );
                      redirect(base_url().'Category/producttype', 'refresh');                      
                }
                


        }
        
   if($this->input->post('delete'))
        {
          $id = $this->input->post('delete_input');  
           $add_data = $this->Categorymodel->DeleteProduct($id);
            if($add_data)
            {
                   $this->session->set_flashdata('success',"Category deleted succesfully" );
                  redirect(base_url().'Category/producttype', 'refresh');                      
            }
            else
            {
                   $this->session->set_flashdata('message_name',"Somthing went wrong ! try again" );
                  redirect(base_url().'Category/producttype', 'refresh');                      
            }
        }

        
                $All= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->Categorymodel->getAllProduct($conditions);
        //pagination config
        $config['base_url']    = base_url().'Category/categorymanagement/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset; 
        $conditions['limit'] = $this->perPage;
        $All['Product'] = $this->Categorymodel->getAllProduct($conditions);
        
        $this->load->view('producttypemanagement',$All);
    }
}
