/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	  config.filebrowserImageBrowseUrl = "ckeditor/ckupload.php";

  // The location of a script that handles file uploads in the Image dialog.
	  config.filebrowserUploadUrl = "ckeditor/ckupload.php";
};
