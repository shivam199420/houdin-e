<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reset extends CI_Controller {
	function __construct() 
    {
        parent::__construct();
        $this->load->library('form_validation'); 
        $this->load->model('Shopregistermodel');
     }
	public function index()
	{
	   
       
               // check token data
        if($this->session->userdata('forgetuserTokenset') == "")
        {
         //   redirect(base_url(), 'refresh');
        }
        if($this->input->post('resetPasswordData'))
        {
            if($this->input->post('password') == $this->input->post('confirmPassword'))
            {
                $this->form_validation->set_rules('forgetPin','text','required|max_length[5]|integer');
                $this->form_validation->set_rules('password','text','required');
                $this->form_validation->set_rules('confirmPassword','text','required');
                if($this->form_validation->run() == true)
                {
                    if($this->session->userdata('forgetToken') == $this->input->post('forgetPin'))
                    {
                        $setArrayData = array('pin'=>$this->input->post('forgetPin'),'password'=>$this->input->post('password'));
                        $getUpdateStatus = $this->Foregtmodel->updateAdminPassword($setArrayData);
                        if($getUpdateStatus['message'] == 'yes')
                        {
                            $this->session->unset_userdata('forgetToken');
                            $this->session->set_flashdata('success','Password updated successfully');
                            $this->load->helper('url');
                            redirect(base_url(), 'refresh'); 
                        }
                        else if($getUpdateStatus['message'] == 'pin')
                        {
                            $this->session->set_flashdata('error','Your pin is expired. Please generate new pin');
                            $this->load->helper('url');
                            redirect(base_url()."forget", 'refresh');  
                        }
                        else
                        {
                            $this->session->set_flashdata('error','Something went wrong. Please try again');
                            $this->load->helper('url');
                            redirect(base_url()."forget/passworddata", 'refresh');  
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('error','Please enter correct pin');
                        $this->load->helper('url');
                        redirect(base_url()."forget/passworddata", 'refresh');    
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','All fields are mandatory');
                    $this->load->helper('url');
                    redirect(base_url()."forget/passworddata", 'refresh');
                }   
            }
            else
            {
                $this->session->set_flashdata('error','Password are not matched');
                $this->load->helper('url');
                redirect(base_url()."forget/passworddata", 'refresh');
            }
        }
      
       
		$this->load->view('Reset_password');
	}
    
}