<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaction extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->auth_users->is_logged_in();
        $this->load->model('transactionmodel');
        $this->load->library('Pdf');
        $this->perPage = 50;
    }
    public function index()
    {
        // Custom Pagination
        $getTotalCount = $this->transactionmodel->getTransactionTotalCount();
        $config['base_url']    = base_url().'transaction/';
        $config['uri_segment'] = 2;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(2);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getTransactionData = $this->transactionmodel->fetchTransactiondata($setArray);
        $this->load->view('vendortransaction',$getTransactionData);
    }
  
    public function transactionlist()
    {
        $getLogo = fetchvedorsitelogo();
        $setHtmlString = "";
        $getPdfData = $this->transactionmodel->fetchTransactionPdfData();
        foreach($getPdfData as $transactionDataList)
        {
            if($transactionDataList->houdinv_transaction_type == 'credit')
            {
                $setTransactionType = 'Credit';
                $setColor = "green";
            }
            else
            {
                $setTransactionType = 'Debit';
                $setColor = "red";
            }
            // transaction status
            if($transactionDataList->houdinv_transaction_status == 'success')
            {
                $setTransactionStatus = 'Success';
                $setButtonColor = "green";
            }
            else
            {
                $setTransactionStatus = 'Fail';
                $setButtonColor = "red";
            }
            if($setHtmlString)
            {
                $setHtmlString = $setHtmlString."".'
                <tr>
                <td style="font-size:12px;font-weight:700;" >'.$transactionDataList->houdinv_transaction_transaction_id.'<br/><span style="color:'.$setColor.'">'.$setTransactionType.' ('.$transactionDataList->houdinv_transaction_method.')</span></td>
                <td style="font-size:12px;font-weight:700;" >'.date('d-m-Y',strtotime($transactionDataList->houdinv_transaction_date)).' </td>
                <td style="font-size:12px;font-weight:700;" >'.$transactionDataList->houdinv_transaction_from.' </td>
                <td style="font-size:12px;font-weight:700;" >'.$transactionDataList->houdinv_transaction_for.'</td>
                <td style="font-size:12px;font-weight:700;">'.$transactionDataList->houdinv_transaction_amount.'</td>
                <td style="font-size:12px;font-weight:700;"><span style="color:'.$setButtonColor.'">'.$setTransactionStatus.'</span></td>
                </tr>
                ';
            }
            else
            {
                $setHtmlString = '
                <tr>
                <td style="font-size:12px;font-weight:700;" >'.$transactionDataList->houdinv_transaction_transaction_id.'<br/><span style="color:'.$setColor.'">'.$setTransactionType.' ('.$transactionDataList->houdinv_transaction_method.')</span></td>
                <td style="font-size:12px;font-weight:700;" >'.date('d-m-Y',strtotime($transactionDataList->houdinv_transaction_date)).' </td>
                <td style="font-size:12px;font-weight:700;" >'.$transactionDataList->houdinv_transaction_from.' </td>
                <td style="font-size:12px;font-weight:700;" >'.$transactionDataList->houdinv_transaction_for.'</td>
                <td style="font-size:12px;font-weight:700;">'.$transactionDataList->houdinv_transaction_amount.'</td>
                <td style="font-size:12px;font-weight:700;"><span style="color:'.$setButtonColor.'">'.$setTransactionStatus.'</span></td>
                </tr>
                ';
            }
        }
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Customer Detail');
    $pdf->SetSubject('Houdin-e - Customer Detail');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = <<<EOD
<table style="width:100%;">
<tbody>
<tr>

<td style="">
<img style="display:block; margin:auto; width:150px;" src="$getLogo" alt="Logo">
</td>
<td style="text-align:right">Transaction List</td>
</tr>
</tbody>
</table>

<table>
<tr><td>&nbsp;</td></tr>
</table>




 <table>
 <tr><td>&nbsp;</td></tr>
 </table>
 <table border="1" cellpadding="10">
 <tr>
 <td style="font-size:12px;font-weight:700;" >Tansaction id</td>
   <td style="font-size:12px;font-weight:700;" >Transaction Date </td>
<td style="font-size:12px;font-weight:700;" >Transation Method </td>
<td style="font-size:12px;font-weight:700;" >Transaction For </td>
       <td style="font-size:12px;font-weight:700;">Transaction Amount</td>
       <td style="font-size:12px;font-weight:700;">Transaction Status</td>

 </tr>
 $setHtmlString
 </table>
 <table>
 <tr><td>&nbsp;</td></tr>
 </table>

EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_transaction_list.pdf', 'I');
    }
}
