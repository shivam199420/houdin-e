<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view("".$getShopName."/Template/header"); ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 
  <style type="text/css">
 ol.progtrckr {
margin: 0;
padding: 0;
padding-bottom: 5%;
list-style-type none;
}

ol.progtrckr li {
display: inline-block;
text-align: center;
line-height: 3.5em;
}
ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
ol.progtrckr[data-progtrckr-steps="5"] li { width: %; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
}
ol.progtrckr li.progtrckr-todo {
    color: silver;
    border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
    content: "\00a0\00a0";
}
ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 50%;
    line-height: 1em;
}
ol.progtrckr li.progtrckr-done:before {
    content: "\2713";
    color: white;
    background-color: yellowgreen;
    height: 2.2em;
    width: 2.2em;
    line-height: 2.2em;
    border: none;
    border-radius: 2.2em;
}
ol.progtrckr li.progtrckr-todo:before {
    content: "\039F";
    color: silver;
    background-color: white;
    font-size: 2.2em;
    bottom: -1.2em;
}
@media(max-width: 767px){

  ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
    width: 100%;
}
ol.progtrckr li.progtrckr-todo {
    color: silver;
    border-bottom: 4px solid silver;
    width: 100%;
}
ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 48%;
    line-height: 1em;
}
}
</style>

<!-- ================== START MAIN SECTION ================== -->
    <div class="main" role="main">
      <?php if($message=='No data')
      {
          ?>
      <div class="login_page_area">
      <div class="container">
      <div class="row">
        <div class="col-md-2 col-sm-2"></div>
        <div class="col-md-8 col-sm-8 col-xs-12">
          <div class="create_account_area">
            <h2 class="caa_heading text-center">Order</h2>
            <div class="caa_form_area">
              <p class="text-center">You have no order. Please <a href="index.html" style="text-decoration: underline;">check here</a> to order now.
                              </p>
            </div>
          </div>
        </div>
      </div>
      </div>
      </div>

             <?php

      }
      else
      {
        ?>

<div class="page-location">
            <div class="page-location-layer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-location-title">
                                <h3>My Order</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>







        <!-- Start cart area -->
                <div class="cart-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="cart-section order-history">
                                    <!-- this table for desktop view -->
                                    <div class="desktop-view">
                                    <div class="table-responsive">
                                        <table>
                                            <tbody>

                                                <tr class="cart-header">
                                                    <th>
                                                        <div>Order Id</div>
                                                    </th>
                                                    <th>
                                                        <div>Product Description</div>
                                                    </th>
                                                    <th>
                                                        <div>Product Name</div>
                                                    </th>
                                                    <th>
                                                        <div>View</div>
                                                    </th>
                                                    <th>
                                                        <div>Action</div>
                                                    </th>
                                                </tr>
                                                <!-- Start single item -->

                                                <?php
                                                $i=1;
                                                foreach($data as $orderData)
                                                {
                                                    ?>
                                                <tr>
                                                  <td class="cart-price">
                                                      <div>
                                                        #<?php echo $orderData['orderId']; ?>
                                                      </div>
                                                  </td>
                                                    <td class="cart-img">
                                                        <div>
                                                            <img src="<?php echo $orderData['productimage'];?>" alt="" class="img-fluid" style="height: 100px;">
                                                        </div>
                                                    </td>
                                                    <td class="cart-product-title">
                                                      <div class="col-md-12 pl">

                                                              <h3><?php echo $orderData['productName'];?></h3>

                                                              <ol class="progtrckr" data-progtrckr-steps="5">
                                                            <li class="progtrckr-done">Unbilled</li>

                                                      </div>
                                                    </td>
                                                    <td>
                                                        <div>
                                                          <a href="<?php echo base_url(); ?>Orders/orderdetail/<?php echo $orderData['orderId']; ?>" class="dlt-btn add-cart-btn acc_btn" id="acc_Create" type="submit">Details</a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div>
                                                          <?php
                                                          if($orderSetting[0]->houdinv_shop_order_configuration_cancellation == 1)
                                                          {
                                                          if($orderData['deliverystatus'] != 'Delivered' && $orderData['deliverystatus'] != 'return request' && $orderData['deliverystatus'] != 'cancel' && $orderData['deliverystatus'] != 'return' && $orderData['deliverystatus'] != 'cancel request' && $orderData['deliverystatus'] != 'order pickup' && $orderData['deliverystatus'] != 'unbilled')
                                                          {
                                                          ?>
                                                          <a class="dlt-btn add-cart-btn acc_btn cancelOrderBtn" data-id="<?php echo $orderData['orderId'] ?>" type="button" id="acc_Create">Cancel</a>
                                                          <?php } }
                                                          else if($orderSetting[0]->houdinv_shop_order_configuration_return != "" && $orderSetting[0]->houdinv_shop_order_configuration_return != 0)
                                                          {
                                                            if($orderData['deliverystatus'] == 'Delivered')
                                                          {
                                                            $getDeliveryDate = $orderData['deliverydate'];
                                                            $getTodayDate = date('d-m-Y');
                                                            if(strtotime($getTodayDate) > strtotime($getDeliveryDate))
                                                            {
                                                              $datetime1 = new DateTime($getTodayDate);
                                                              $datetime2 = new DateTime($getDeliveryDate);
                                                              $interval = $datetime1->diff($datetime2);
                                                              if($interval > 0 && $interval <= $orderSetting[0]->houdinv_shop_order_configuration_return)
                                                              {
                                                          ?>
                                                          <a class="dlt-btn add-cart-btn acc_btn" type="submit" id="acc_Create">Return</a>
                                                          <?php } }
                                                            }
                                                          }
                                                          ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                                $i++;
                                                             }?>
                                                <!-- Start single item -->

                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                    <!-- This table for responsive view -->
                                    <div class="responsive-view">
                                        <table>
                                            <!-- Start single item -->
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td class="cart-img">
                                                                <div>
                                                                    <img src="images/products/man-small-1.jpg" alt="" class="img-fluid">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="cart-product-title">
                                                                <div>
                                                                    <a href="product-detail.html">Men's Casual One Button Slim Fit Blazer</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="cart-price">
                                                                <div> Total price : $120</div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <a href="javascript:void(0)" class="dlt-btn add-cart-btn">
                                                                       Return Order
                                                                    </a>
                                                                    <a href="javascript:void(0)" class="dlt-btn add-cart-btn">
                                                                        Re Order
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <a href="javascript:void(0)" class="dlt-btn">
                                                                        <i class="fa fa-trash"></i>
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!-- Start single item -->
                                        </table>
                                    </div>
                                </div>
                                <div class="cart-btn-area">
                                    <a href="#" class="porduct-place-order-btn">BACK</a>
                                </div>
                            </div>
                        </div>
                    </div>












      </div>
      <!-- Start business logo -->
      <div class="business-logo">
          <div class="container">
           </div>
      </div>
      <?php
      }?>
  </div>
  <!-- ================== END MAIN SECTION ================== -->

<!-- cancel ordwer -->
<div id="cancelOrderModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<form method="post">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" style="margin:0px; padding:0px;" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Cancel Order</h4>
  </div>
  <div class="modal-body">
<input type="hidden" class="cancelOrderId" name="cancelOrderId" />
    <h3>Do you really want to raise cancel order request.</h3>
  </div>
  <div class="modal-footer">
<input type="submit" name="cancelOrder" class="btn btn-danger" value="Cancel"/>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>
</form>
</div>
</div>

<!-- Shopping Cart End -->

<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view("".$getShopName."/Template/footer"); ?>

<script type="text/javascript">
$(document).on('click','.cancelOrderBtn',function(){
  $('.cancelOrderId').val($(this).attr('data-id'));
  $('#cancelOrderModal').modal('show');
})
</script>
