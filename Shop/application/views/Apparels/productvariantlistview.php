<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
if(count($variantData) <= 0)
{
    redirect(base_url()."home/", 'refresh');
}
    $price_all = json_decode($productData['0']->houdin_products_price,true);
    // set variant price
    if($price_all['discount'] != 0 && $price_all['discount'] != "")
    {
        $setDiscountedPrice = $variantData[0]->houdin_products_variants_prices-$price_all['discount'];
        $setOriginalPrice = $variantData[0]->houdin_products_variants_prices;
    }
    else
    {
        $setDiscountedPrice = 0;
        $setOriginalPrice = $variantData[0]->houdin_products_variants_prices;
    }
    $overall_rating = $reviews_sum->houdinv_product_review_rating;
    $total_rating = count($reviews);
    $final_rating = $overall_rating/$total_rating;
     ?>
     <?php
     $getCurrency = getShopCurrency();
     $currencysymbol=$getCurrency[0]->houdin_users_currency;
     ?>
<style>
    .checked {
    color: orange;
}

</style>
    <!-- Page item Area -->
    <div id="page_item_area">
    <div class="container">
    <div class="row">
    <div class="col-sm-6 text-left">
    <h3>Shop Details</h3>
    </div>
    </div>
    </div>
    </div>

    <!-- Product Details Area  -->
    <div class="prdct_dtls_page_area">
    <div class="container">
            <div class="row">
        <div class="container">
        <div class="col-sm-12">
        <?php
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>
        </div>
    <div class="row">
    <!-- Product Details Image -->
    <div class="col-md-6 col-xs-12">
    <div class="pd_img fix">
    <?php 
    if($variantData[0]->houdin_products_variants_image)
    {
        $setVariantIMage = $this->session->userdata('vendorURL')."upload/productImage/".$variantData[0]->houdin_products_variants_image;
    }
    else
    {
        $setVariantIMage = base_url()."Extra/noPhotoFound.png";
    }
    ?>
    <a class="venobox" href="<?php echo $setVariantIMage ?>"><img src="<?php echo $setVariantIMage;?>" alt=""/></a>
    </div>
    </div>
    <!-- Product Details Content -->
    <div class="col-md-6 col-xs-12">
    <div class="prdct_dtls_content">
    <h3 class="pd_title" style="line-height:1.2"><?php echo $variantData[0]->houdin_products_variants_title; ?> <span style="color:red;"> <?php $quantity=$product['main_product']->houdinv_products_total_stocks; if($quantity==0){echo "( Out Of Stock )";} ?></span></h3>
    <div class="pd_price_dtls fix">
    <!-- Product Price -->
    <div class="pd_price">
    <?php
    if($setDiscountedPrice != 0)
    {
    ?>
     <span class="new"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $variantData[0]->houdinv_products_variants_final_price; ?></span>
    <span class="old">(<?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $price_all['sale_price'];  ?>)</span>
    <?php }
    else
    {
    ?>
    <span class="new">(<?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $setOriginalPrice;  ?>)</span>
    <?php }
    ?>
    </div>
    <!-- Product Ratting -->
    <div class="pd_ratng">
    <div class="rtngs">

 <?php for($i=1 ; $i<6 ;$i++)
    {
    if($i <= $final_rating)
    {
        $posi = strpos($final_rating,".");
        if($posi>=0)
        {
         $exp =  explode(".",$final_rating);
         if($exp[0]==$i)
         {
            ?>
     <i class="fa fa-star"></i>
            <?php
            }
            else
            {
                echo '<i class="fa fa-star"></i>';
            }
        }
        else
        {
        ?>
        <i class="fa fa-star"></i>
        <?php
         }
     }
     else
     {
        ?>

    <i class="fa fa-star-o"></i>


    <?php } } echo "(".$total_rating.")"; ?>


    </div>








    </div>
    </div>
    <div class="pd_text">
    <h4>overview:</h4>
    <p><?php echo $productData[0]->houdin_products_short_desc; ?></p>
    </div>

    <!-- <div class="pd_img_size fix">
    <h4>size:</h4>
    <a href="#">s</a>
    <a href="#">m</a>
    <a href="#">l</a>
    <a href="#">xl</a>
    <a href="#">xxl</a>
    </div> -->
    <div class="pd_clr_qntty_dtls fix">
    <!-- <div class="pd_clr">
    <h4>color:</h4>
    <a href="#" class="active" style="background: #ffac9a;">color 1</a>
    <a href="#" style="background: #ddd;">color 2</a>
    <a href="#" style="background: #000000;">color 3</a>
    </div> -->
    <div class="pd_qntty_area">
    <h4>quantity:</h4>
    <div class="pd_qty fix">
    <input value="1" min="1" data-max="<?php echo $variantData[0]->houdinv_products_variants_total_stocks ?>" name="qttybutton" class="cart-plus-minus-box setCartQunatity" type="number">
    </div>
    </div>
    </div>
    <!-- Product Action -->
    <div class="pd_btn fix">
    <a class="btn btn-default acc_btn Add_to_cart_button" data-variant="<?php echo $variantData[0]->houdin_products_variants_id ?>"  data-cart="0">add to bag</a>
    <a class="btn btn-default acc_btn btn_icn Add_to_whishlist_button" data-variant="<?php echo $variantData[0]->houdin_products_variants_id ?>"  data-cart="0"><i class="fa fa-heart"></i></a>
    </div>
    </div>
    </div>
    </div>

    <div class="row">
    <div class="col-md-12">
    <div class="pd_tab_area fix">
    <ul class="pd_tab_btn nav nav-tabs" role="tablist">
    <li>
    <a class="active" href="#description" role="tab" data-toggle="tab">Description</a>
    </li>
    <li>
    <a href="#information" role="tab" data-toggle="tab">Add Review</a>
    </li>
    <li>
    <a href="#reviews" role="tab" data-toggle="tab">Reviews</a>
    </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade show active" id="description">
 <?php echo $productData[0]->houdin_products_desc; ?>
    </div>

     <div role="tabpanel" class="tab-pane fade" id="information">
     <div class="col-md-12 rcf_pdnglft">
    <div class="rtng_cmnt_form_area fix">
    <h3>Add your Comments</h3>
    <div class="rtng_form">
   <?php echo form_open(base_url( 'Productlistview/variant/'.$this->uri->segment('3').'' ), array( 'id' => 'rtng_form',"class"=>"checkout_form", 'method'=>'post' ));?>

    <div class="strat-rating" style="padding:25px 0px;">
      <span data-rating="1" style="font-size: 22px;" class="fa fa-star "></span>
      <span data-rating="2" style="font-size: 22px;" class="fa fa-star "></span>
      <span data-rating="3" style="font-size: 22px;" class="fa fa-star "></span>
      <span data-rating="4" style="font-size: 22px;" class="fa fa-star "></span>
      <span data-rating="5" style="font-size: 22px;" class="fa fa-star"></span>
      <input type="hidden"  name="rating" class="rating_data" value="0" />
      <input type="hidden"  name="product_id" class="product" value="0" />
        <input type="hidden"  name="variant_id" class="" value="<?php echo $this->uri->segment('3'); ?>" />
     </div>
    <div class="input-area"><input  class="required_validation_for_review name_validation" type="text" name="user_name" placeholder="Type your name" /></div>
    <div class="input-area"><input  class="required_validation_for_review name_validation email_validation" type="text" name="user_email" placeholder="Type your email address" /></div>
    <div class="input-area"><textarea  class="required_validation_for_review" name="user_message" placeholder="Write a review"></textarea></div>
    <input class="btn border-btn submit_form_review" type="submit" name="review_submit" value="Add Review" />
     <?php echo form_close(); ?>
    </div>
    </div>
    </div>
    </div>

    <div role="tabpanel" class="tab-pane fade" id="reviews">
    <div class="pda_rtng_area fix">
    <h2><?php echo round($final_rating,1); ?><span>(Overall)</span></h2>
    <span style="color: black;font-size: 13px;line-height: 3;">Based on <?php echo $total_rating; ?> Comments</span>
    </div>
    <div class="rtng_cmnt_area fix">

    <?php
    foreach($reviews as $user_Review)
    {
        ?>


    <div class="single_rtng_cmnt">
    <div class="rtngs">
    <?php for($i=1 ; $i<6 ;$i++)
    {
    if($i <= $user_Review->houdinv_product_review_rating)
    {


    ?>
    <i class="fa fa-star" style="color:orange"></i>
    <?php
     }
     else
     {
        ?>

    <i class="fa fa-star-o"></i>


    <?php } } ?>



    <span>(<?php echo  $user_Review->houdinv_product_review_rating; ?>)</span>
    </div>
    <div class="rtng_author">
    <h3><?php echo $user_Review->houdinv_product_review_user_name ?></h3>
    <span style="color:black"><?php echo $user_Review->houdinv_product_review_created_at ?></span>
    </div>
    <p style="color:black"><?php echo  $user_Review->houdinv_product_review_message; ?></p>
    </div>
    <?php
    }?>
    </div>

    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>


    <!-- Related Product Area -->
    <div class="related_prdct_area text-center">
    <div class="container">
    <!-- Section Title -->
    <div class="rp_title text-center"><h3>Related products</h3></div>

    <div class="row">
    <?php foreach($relateData as $related)
    {
        ?>


    <div class="col-lg-3 col-md-4 col-sm-6">
    <div class="single_product">
    <div class="product_image">
    <?php 
    if($related->houdin_products_variants_image)
    {
        $setVariantIMageData = $this->session->userdata('vendorURL')."upload/productImage/".$related->houdin_products_variants_image;
    }
    else
    {
        $setVariantIMageData = base_url()."Extra/noPhotoFound.png";
    }
    ?>
    <img src="<?php echo $setVariantIMageData ?>" style="width: 253px!important;
    height: 310px!important;" alt=""/>
    <div class="box-content">
    <a href="javascript:;" class="Add_to_whishlist_button" data-variant="<?php echo $related->houdin_products_variants_id ?>"  data-cart="0" ><i class="fa fa-heart-o"></i></a>
    <a class="Add_to_cart_button" data-variant="<?php echo $related->houdin_products_variants_id ?>"  data-cart="0"><i class="fa fa-cart-plus"></i></a>
    <a href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>"><i class="fa fa-search"></i></a>
    </div>
    </div>

    <div class="product_btm_text">
    <h4><a href="#"><?php echo $related->houdin_products_variants_title; ?></a></h4>
    <span class="price">RS.<?php echo $related->houdin_products_variants_prices; ?></span>
    </div>
    </div>
    </div> <!-- End Col -->


     <?php
    }?>


    </div>
    </div>
    </div>
    <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view(''.$getShopName.'/Template/footer'); ?>

    <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('keyup','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        })
        $(document).on('change','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        })
    })
    </script>
