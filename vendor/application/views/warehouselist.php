<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style type="text/css">
  .checkbox {
    padding-left: 0px;
}
</style> 
 
 <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
 
             <!--START Page-Title -->
            <div class="row">
            
                <div class="col-md-8">
                   <h4 class="page-title">Warehouse</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                   <li><a href="<?php echo base_url(); ?>shipping/Shippinglanding">Shipping</a></li>
                  <li class="active">Warehouse</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                  
            </div>
           <!--END Page-Title -->  
            <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
            <div class="row m-t-20">
              <div class="col-lg-12">
                <div class="card-box" style="float: left;width: 100%;">
                <div class="col-md-6 pull-left" style="margin-bottom: 14px;"><h3 class="text_black">Warehouses List</h3></div>
                <?php 
                $getUserPlan = getVendorplan();
                if(count($getUserPlan) > 0)
                {
                    if($getUserPlan[0]->houdin_package_stock_location > count($warehouse_list))
                    {
                     ?>   
                     <div class="col-md-6 pull-right"><h3 class="pull-right"><button type="button" class="btn btn-default m-r-5" data-toggle="modal" data-target="#add" title="Add purchase order"><i class="fa fa-plus"></i></button></h3></div>
                    <?php }
                }
                else
                {
                ?>
                <div class="col-md-6 pull-right"><h3 class="pull-right"><button type="button" class="btn btn-default m-r-5" data-toggle="modal" data-target="#add" title="Add purchase order"><i class="fa fa-plus"></i></button></h3></div>
                <?php }
                ?>

                <hr>
                <div class="clearfix"></div>
                <?php 
              //print_r($warehouse_list);
                
                if(empty($warehouse_list)){
                  echo ' <p class="text_black">No warehouse found.</p>';
                }else{
             

                foreach($warehouse_list as $key => $value) {
                  # code...
              
                ?>
              
          
               <div class="col-md-3">
                <div class="address">
                 <address class="text_black">
                   <p><strong><?=$value->w_name?></strong></p>
                   <p><?=$value->w_pickup_name?></p>                    
                    <p><?=$value->w_pickup_address?></p>
                    <p><?=$value->w_pickup_city?> , <?=$value->state_name?></p>
                   
                    <p><?=$value->country_name?> <?=$value->w_pickup_zip?></p>

                   
                    <p><b>Contact:</b> <?=$value->w_pickup_phone?></p>

                   
                    <span class="right">
                     <input type="hidden" name="update_w_id" class="update_w_id" value="<?=$value->id?>">
                    <button type="button" class="btn btn-default m-r-5 w_edit" title="Edit"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-default m-r-5 w_delete" title="Delete"><i class="fa fa-trash"></i></button>
                    </span>
                 </address>
                 </div>
               </div>

               <?php } } ?>

 
                
                </div>
              </div>
              
              
            </div>

                                      

                                       
      </div>
    </div>
  </div>

  <!--Delete-->

  <div id="delete_Warehouselist" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
       <?php echo form_open(base_url('Shipping/warehouselist_delete'), array( 'id' => 'warehouselist_delete', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete Warehouselist</h4>

          </div>

          <div class="modal-body">
          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this warehouselist ?</b></h4>
          </div>
          </div>
          </div>
             <input type="hidden" name="delete_w_id" id="delete_w_id">
          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Delete">

          </div>


          </div>
          <?php echo form_close(); ?>
          </div>

          </div>

    <!-- END Delete -->
    <!-- START Add -->
     <div id="add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Add Warehouse</h4> 
                                                </div> 

<?php echo form_open(base_url('Shipping/warehouselist_save'), array( 'id' => 'warehouse_list_save', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
                <div class="ajax_Loginresponse_result">
                                                <div class="modal-body"> 
                                                    <div class="row">
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-4 control-label">Name</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control required_validation_for_add_supplier name_validation" name="w_name" placeholder="Name">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup Name</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control required_validation_for_add_supplier name_validation" name="w_pickup_name" placeholder="Pickup Name">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup phone</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control Internationphonecode required_validation_for_add_supplier name_validation" name="w_pickup_phone" placeholder="Contact">
        </div>

        </div>
        
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup country</label>
        <div class="col-md-8">
        <select class="form-control required_validation_for_add_supplier country_addlist" name="w_pickup_country">
       <option value="">Select Country</option>
        <?php foreach ($country_list as $key => $value) {
           $country_id=$value->country_id;
                $country_name=$value->country_name;
                echo "<option value='$country_id'>$country_name</option>";
        } ?>
         
        </select>
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">State/Region/Province</label>
        <div class="col-md-8">
        <select class="form-control required_validation_for_add_supplier state_addlist" name="w_pickup_state">
        <option value="">Select State/Region/Province</option>

        </select>
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup city</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control required_validation_for_add_supplier name_validation" name="w_pickup_city" placeholder="City">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup address</label>
        <div class="col-md-8">
        <textarea class="form-control required_validation_for_add_supplier name_validation" name="w_pickup_address" rows="5" placeholder="Address"></textarea>
        </div>
        </div>
         <div class="form-group">
        <label class="col-md-4 control-label">Pickup PIN/ZIP code</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control required_validation_for_add_supplier name_validation" name="w_pickup_zip" placeholder="PIN/ZIP code">
        </div>
        </div>


        </div>

       
        </div>
                                                </div>  </div>
                                                <div class="modal-footer"> 
                                                  
                                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button> 
                                                </div> 

                                                 <?php echo form_close(); ?>
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->

    <!-- END Add -->      

     <!-- START Edit -->
     <div id="edit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Edit Warehouse</h4> 
                                                </div>
                                             

                                             <?php echo form_open(base_url('Shipping/warehouselist_edit'), array( 'id' => 'warehouselist_edit', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
                                   <input type="hidden" name="w_id" id="id_wid">

                <div class="ajax_Loginresponse_result_edit">
                                                <div class="modal-body"> 
                                                    <div class="row">
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-4 control-label">Name</label>
        <div class="col-md-8">
        <input type="text" value="" id="id_w_name" class="form-control required_validation_for_add_supplier name_validation" name="w_name" placeholder="Name">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup Name</label>
        <div class="col-md-8">
        <input type="text" value="" id="id_w_pickup_name" class="form-control required_validation_for_add_supplier name_validation" name="w_pickup_name" placeholder="Pickup Name">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup phone</label>
        <div class="col-md-8">
        <input type="text" value="" id="id_w_pickup_phone" class="form-control Internationphonecode required_validation_for_add_supplier name_validation" name="w_pickup_phone" placeholder="Contact">
        </div>

        </div>
        
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup country</label>
        <div class="col-md-8">
        <select class="form-control required_validation_for_add_supplier country_addlist" id="id_w_pickup_country" name="w_pickup_country">
       <option value="">Select Country</option>
        <?php foreach ($country_list as $key => $value) {
           $country_id=$value->country_id;
                $country_name=$value->country_name;
                echo "<option value='$country_id'>$country_name</option>";
        } ?>
         
        </select>
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">State/Region/Province</label>
        <div class="col-md-8">
        <select class="form-control required_validation_for_add_supplier state_addlist" id="id_w_pickup_state" name="w_pickup_state">
        <option value="">Select State/Region/Province</option>

        </select>
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup city</label>
        <div class="col-md-8">
        <input type="text" value="" id="id_w_pickup_city" class="form-control required_validation_for_add_supplier name_validation " name="w_pickup_city" placeholder="City">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup address</label>
        <div class="col-md-8">
        <textarea class="form-control required_validation_for_add_supplier name_validation" id="id_w_pickup_address" name="w_pickup_address" rows="5" placeholder="Address"></textarea>
        </div>
        </div>
         <div class="form-group">
        <label class="col-md-4 control-label">Pickup PIN/ZIP code</label>
        <div class="col-md-8">
        <input type="text" value="" id="id_w_pickup_zip" class="form-control required_validation_for_add_supplier name_validation" name="w_pickup_zip" placeholder="PIN/ZIP code">
        </div>
        </div>


        </div>

       
        </div>
                                                </div>  </div>
                                                <div class="modal-footer"> 
                                                  
                                                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button> 
                                                </div> 

                                                 <?php echo form_close(); ?>

                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->

    <!-- END Edit -->      

<?php $this->load->view('Template/footer.php') ?>

<script>
$(document).ready(function(){

 //$('.country_addlist').on('change',function(){
  $(document).on('change','.country_addlist',function(){

    urls='<?=base_url();?>'+'Shipping/state_addlist';
        var countryID = $(this).val();
        var dataString = $('#warehouse_list_save').serialize();
        if(countryID){
            $.ajax({
                type:'POST',
                url:urls,
                data:'country_id='+countryID+'&'+dataString,
                success:function(html){
                    $('.state_addlist').html(html);
                  //  $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('.state_addlist').html('<option value="">Select country first</option>');
            //$('#city').html('<option value="">Select state first</option>'); 
        }
    });

 $('#warehouse_list_save').submit(function(e){
    e.preventDefault(); 
 

var check_required_field='';
      $(this).find(".required_validation_for_add_supplier").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {
        return false;
      }else{
      


    urls='<?=base_url();?>'+'Shipping/warehouselist_save';
          var dataString = $(this).serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".ajax_Loginresponse_result").html(data);
        }else{
        window.location.reload();
        }
        }
        }); } 
    });  

 $(".w_delete").click(function(){     
       update_id=$(this).siblings('.update_w_id').val();       
       $('#delete_w_id').val(update_id);              
      $('#delete_Warehouselist').modal('show');
       });


 $(".w_edit").click(function(){  
 $this=$(this);
  $this.html('<i class="fa fa-spinner fa-spin" style="color: #ffffff;"></i>');   
       update_id=$(this).siblings('.update_w_id').val();
               
  urls='<?=base_url();?>'+'Shipping/warehouselist_edit_poup';
       
        var dataString = $('#warehouselist_edit').serialize();
        if(update_id){
            $.ajax({
                type:'POST',
                url:urls,
                data:'update_id='+update_id+'&'+dataString,
                success:function(html){
                 html=JSON.parse(html);
                 var all_data=html['Edit_poup_val']['0'];
                 var w_name =all_data['w_name'];
                 $('#id_w_name').val(w_name);
                 var w_pickup_name =all_data['w_pickup_name'];
                 $('#id_w_pickup_name').val(w_pickup_name);
                 var w_pickup_phone =all_data['w_pickup_phone'];
                 $('#id_w_pickup_phone').val(w_pickup_phone);
                 var country_name =all_data['country_name'];
                 var country_id =all_data['country_id'];
                  $('#id_w_pickup_country').val(country_id);
                 var state_id =all_data['state_id'];
                 var state_name =all_data['state_name'];                  
                 $('#id_w_pickup_state').html('<option value="'+state_id+'">'+state_name+'</option>');
                 var w_pickup_city =all_data['w_pickup_city'];
                 $('#id_w_pickup_city').val(w_pickup_city);
                 var w_pickup_address =all_data['w_pickup_address'];
                 $('#id_w_pickup_address').val(w_pickup_address);
                 var w_pickup_zip =all_data['w_pickup_zip'];
                 $('#id_w_pickup_zip').val(w_pickup_zip);
                 var w_ids =all_data['id'];
                 $('#id_wid').val(w_ids);
                 
                //  console.log(html);
                   $('#edit').modal('show');

                    $this.html('<i class="fa fa-edit"></i>');  
                }
            }); 
        }else{
             alert('Please Check your internat Connction is Slow ');
        }

      // $('#delete_w_id').val(update_id);              
      
       });

  $('#warehouselist_edit').submit(function(e){
    e.preventDefault(); 
 
var check_required_field='';
      $(this).find(".required_validation_for_add_supplier").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
     //  alert(check_required_field);
      if(check_required_field)
      {
        return false;
      }else{
        
    

    urls='<?=base_url();?>'+'Shipping/warehouselist_edit';
          var dataString = $(this).serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".ajax_Loginresponse_result_edit").html(data);
        }else{
        window.location.reload();
        }
        }
        });   }  
    });

    $(".select2").select2();
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
