<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function getRemainingEmail()
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $getEmaildata = $getVendorDb->select('houdinv_emailsms_stats_remaining_credits,houdinv_emailsms_stats_id')->from('houdinv_emailsms_stats')->where('houdinv_emailsms_stats_type','email')->get()->result();
    return array('remaining'=>$getEmaildata[0]->houdinv_emailsms_stats_remaining_credits,'id'=>$getEmaildata[0]->houdinv_emailsms_stats_id);
}
function updateSuceessEmailCount($remaining)
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $setUpdatedArray = array('houdinv_emailsms_stats_remaining_credits'=>$remaining);
    $getVendorDb->where('houdinv_emailsms_stats_type','email');
    $setData = strtotime(date('Y-m-d'));
    $getVendorDb->update('houdinv_emailsms_stats',$setUpdatedArray);
    $setInsertArray = array('houdinv_email_log_credit_used'=>'1','houdinv_email_log_status'=>'1','houdinv_email_log_created_at'=>$setData);
    $getVendorDb->insert('houdinv_email_log',$setInsertArray);
}
function updateErrorEmailLog()
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $setData = strtotime(date('Y-m-d'));
    $setInsertArray = array('houdinv_email_log_credit_used'=>'0','houdinv_email_log_status'=>'0','houdinv_email_log_created_at'=>$setData);
    $getVendorDb->insert('houdinv_email_log',$setInsertArray);
}
function fetchvedorsitelogo()
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $getSiteLogo = $getVendorDb->select('shop_logo')->from('houdinv_shop_logo')->get()->result();
    if(count($getSiteLogo) > 0)
    {
        $getSiteLogo = base_url()."upload/logo/".$getSiteLogo[0]->shop_logo;
    }
    else
    {
        $getDynamicDb = $CI->load->database('master',true);
        $resultAdminLogoData = $getDynamicDb->select('houdin_admin_logo_image')->from('houdin_admin_logo')->where('houdin_admin_logo_id','1')->get()->result();
        $getSiteLogo = "http://3.22.189.222/houdin-e/master/uploads/logo/".$resultAdminLogoData[0]->houdin_admin_logo_image;
    }
    return $getSiteLogo;
}
function fetchvendorinfo()
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $getVendorInfo = $getVendorDb->select('*')->from('houdinv_shop_detail')->where('houdinv_shop_id','1')->get()->result();
    if(count($getVendorInfo) > 0)
    {
        $setStore = array('name'=>$getVendorInfo[0]->houdinv_shop_business_name,'email'=>$getVendorInfo[0]->houdinv_shop_communication_email);
    }
    else
    {
        $getDynamicDb = $CI->load->database('master',true);
        $getVendorAuth = $CI->session->userdata('vendorAuth');
        $getInfo = $getDynamicDb->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id,houdin_users.houdin_user_email,houdin_user_shops.houdin_user_shop_shop_name')
        ->from('houdin_vendor_auth')
        ->where('houdin_vendor_auth.houdin_vendor_auth_token',$getVendorAuth)
        ->join('houdin_user_shops','houdin_user_shops.houdin_user_shop_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id','left outer')    
        ->join('houdin_users','houdin_users.houdin_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id','left outer')->get()->result();
        $setStore = array('name'=>$getInfo[0]->houdin_user_shop_shop_name,'email'=>$getInfo[0]->houdin_user_email);
    }
    return $setStore;
}
function fetchsocialurl()
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $getSocialUrl = $getVendorDb->select('facebook_url,twitter_url,youtube_url')->from('houdinv_social_links')->where('social_id','1')->get()->result();
    return $getSocialUrl;
}
function remainingSMS()
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $getSMSCount = $getVendorDb->select('houdinv_emailsms_stats_remaining_credits')->from('houdinv_emailsms_stats')->where('houdinv_emailsms_stats_type','sms')->get()->result();
    return array('sms'=>$getSMSCount[0]->houdinv_emailsms_stats_remaining_credits);
}
function succcessSMS($data)
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $getVendorDb->where('houdinv_emailsms_stats_type','sms');
    $getVendorDb->update('houdinv_emailsms_stats',array('houdinv_emailsms_stats_remaining_credits'=>$data));
    $setData = strtotime(date('Y-m-d h:i:s'));
    $setinsertArray = array('houdinv_sms_log_credit_used'=>'1','houdinv_sms_log_status'=>"1",'houdinv_sms_log_created_at'=>$setData);
    $getVendorDb->insert('houdinv_sms_log',$setinsertArray);
}
function failureSMS()
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $setinsertArray = array('houdinv_sms_log_credit_used'=>'0','houdinv_sms_log_status'=>"0",'houdinv_sms_log_created_at'=>$setData);
    $getVendorDb->insert('houdinv_sms_log',$setinsertArray);
}