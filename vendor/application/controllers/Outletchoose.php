<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Outletchoose extends CI_Controller
{
    function __construct() 
    {
       parent::__construct();        
        $this->auth_users->is_logged_in();       
        $this->load->model('Shippingmodel');        
        $this->load->helper('url');
        $this->load->library('user_agent');


    }

public function index(){
    
  if($this->uri->segment(3)){
 $Get_warehouse= $this->Shippingmodel->Getwarehouse($this->uri->segment(3)); 
$this->session->set_userdata("WAREHOUSE",$Get_warehouse);
}else{
    $this->session->set_userdata("WAREHOUSE",'');   
} 
 
if(!empty($_SERVER['HTTP_REFERER']))
{ 
   
    redirect($this->agent->referrer());
}else{
    redirect(base_url());
}

 }
}