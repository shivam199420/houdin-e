<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>

<div class="content-page">
    <!-- Start content --> 
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">
                <div class="col-md-8">
                    <h4 class="page-title">Product</h4>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li class="active">Product</li>
                  </ol>
                </div>
                 
            </div>
            <!--END Page-Title -->

            <!-- basic product states -->
            <div class="row m-t-20">
            <div class="col-lg-3 col-sm-6">
                <div class="widget-panel widget-style-2 bg-white">
                    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Products.png">
                    <h2 class="m-0 text-dark counter font-600"><?php echo $allStatus['AllProduct']; ?></h2>
                    <div class="text-muted m-t-5">Total product</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="widget-panel widget-style-2 bg-white">
                    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Products.png">
                    <h2 class="m-0 text-dark counter font-600"><?php echo $allStatus['AllActive']; ?></h2>
                    <div class="text-muted m-t-5">Active Product </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="widget-panel widget-style-2 bg-white">
                    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Products.png">
                    <h2 class="m-0 text-dark counter font-600"><?php echo $allStatus['AllDective']; ?></h2>
                    <div class="text-muted m-t-5">Total Deactive Product</div>
                </div>
            </div>

            </div>

            <div class="row m-t-30">
                <div class="col-md-12">
                    <!-- Product List -->
                    <div class="col-lg-3">
                        <div class="our-team-main">
                            <div class="team-front">
                <img src="<?php echo base_url(); ?>assets/images/landingpageicons/Category.png">
                <h3>Product List</h3>
                <p>Product List</p>
                </div>
                
               <a href="<?php echo base_url(); ?>product"> <div class="team-back">
                <span>
                <h2 class="font24">Product List</h2>
                </span>
                </div></a>
                
                </div>
                </div>
                <!-- Add Product -->
                <div class="col-lg-3">
                        <div class="our-team-main">
                            <div class="team-front">
                <img src="<?php echo base_url(); ?>assets/images/landingpageicons/Category.png">
                <h3>Add Product</h3>
                <p>Add Product</p>
                </div>
                
               <a href="<?php echo base_url(); ?>Product/addproduct"> <div class="team-back">
                <span>
                <h2 class="font24">Add Product</h2>
                </span>
                </div></a>
                
                </div>
                </div>

                <!-- Missing Deatil -->
                <div class="col-lg-3">
                        <div class="our-team-main">
                            <div class="team-front">
                <img src="<?php echo base_url(); ?>assets/images/landingpageicons/Category.png">
                <h3>Missing Detail</h3>
                <p>Missing Detail</p>
                </div>
                
                <div class="team-back">
                <a href="javascript:;"> <span>
                <h2 class="font17"><a href="<?php echo base_url() ?>Product/missingImages" class="text-white">Missing images</a></h2>
                </span></a>
                <a href="javascript:;"> <span>
                <h2 class="font17"><a class="text-white" href="<?php echo base_url(); ?>Product/missingcategory">Missing category/product type</a></h2>
                </span></a>
                </div>
                
                </div>
                </div>
                    <!--team-1-->
                    <div class="col-lg-3">
                        <div class="our-team-main">
                            <div class="team-front">
                <img src="<?php echo base_url(); ?>assets/images/landingpageicons/Category.png">
                <h3>Category</h3>
                <p>Category</p>
                </div>
                
               <a href="<?php echo base_url(); ?>Category/categorymanagement"> <div class="team-back">
                <span>
                <h2 class="font24">Category</h2>
                </span>
                </div></a>
                
                </div>
                </div>
                <!--team-1-->
                
                <!--team-2-->
               <div class="col-lg-3">
                <div class="our-team-main">
                
                <div class="team-front">
                <img src="<?php echo base_url(); ?>assets/images/landingpageicons/Product_Type.png">
                <h3>Product Type</h3>
                <p>Category</p>
                </div>
                
                <a href="<?php echo base_url(); ?>Category/producttype"><div class="team-back">
                <span>
                <h2 class="font24">Product Type</h2>
                </span>
                </div></a>
                
                </div>
                </div>
                <!--team-2-->
                
               

                </div>
            </div><!-- Row -->


      </div>
    </div>
  </div>
<!--Delete-->

  <div id="delete_order" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
          <form method="post">
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete Order</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this order ?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Delete">

          </div>

          </div>
          </form>
          </div>

          </div>

<!--change Status-->
<div id="Change-status" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <form method="post">
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Change Status</h4>

        </div>

        <div class="modal-body">



        <div class="row">
        <div class="col-md-12">

        <div class="form-group no-margin">

        <label for="field-7" class="control-label">Change Status</label>

        <select class="form-control " name=""><option value="">Choose Status</option><option value="1">Active</option><option value="0">Deactive</option><option value="2">Block</option></select>

        </div>

        </div>
        </div>





        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Update Status">

        </div>

        </div>
        </form>
        </div>

        </div> 

        <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Add purchase order</h4> 
                                                </div> 
                                                <div class="modal-body"> 
                                                    <div class="row"> 
                                                        <div class="col-md-6"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label">Select Product</label> 
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                </select>
                                                            </div> 
                                                        </div> 
                                                        <div class="col-md-6"> 
                                                            <div class="form-group"> 
                                                                <label for="field-2" class="control-label">Select Supplier</label> 
                                                                <select class="form-control">
                                                                    <option>Supplier1</option>
                                                                    <option>Supplier2</option>
                                                                    <option>Supplier3</option>
                                                                    <option>Supplier4</option>
                                                                    <option>Supplier5</option>
                                                                </select> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-3" class="control-label">Quantity</label> 
                                                                <input type="text" class="form-control" id="field-3" placeholder="Quantity"> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-6"> 
                                                            <div class="form-group"> 
                                                                <label for="field-4" class="control-label">Delivery date</label> 
                                                                <input type="text" class="form-control date_picker" id="field-4" placeholder="Delivery date"> 
                                                            </div> 
                                                        </div> 
                                                        <div class="col-md-6"> 
                                                            <div class="form-group"> 
                                                                <label for="field-5" class="control-label">Credit period</label> 
                                                                <input type="text" class="form-control" id="field-5" placeholder="Credit period"> 
                                                            </div> 
                                                        </div> 
                                                     
                                                    </div> 
                                                    <!--<div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group no-margin"> 
                                                                <label for="field-7" class="control-label">Personal Info</label> 
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
                                                            </div> 
                                                        </div> 
                                                    </div> -->
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                                </div> 
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->

<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
