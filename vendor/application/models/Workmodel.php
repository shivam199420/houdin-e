<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Workmodel extends CI_Model{
    function __construct() 
    {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
    }

    public function fetchPosData()
    {
        // get customer info
        $getCustomerData = $this->db->select('houdinv_user_name,houdinv_user_id,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_is_active','active')->get()->result();
        // get counrty list
        $getMasterDB = $this->load->database('master',true);
        $getCountryData = $getMasterDB->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id,houdin_users.houdin_user_country,houdin_countries.houdin_country_name')
        ->from('houdin_vendor_auth')->where('houdin_vendor_auth.houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))
        ->join('houdin_users','houdin_users.houdin_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id','left outer')
        ->join('houdin_countries','houdin_countries.houdin_country_id = houdin_users.houdin_user_country')->get()->result();
        $setCountryArray = array('id'=>$getCountryData[0]->houdin_user_country,'country'=>$getCountryData[0]->houdin_country_name);
        if($this->session->userdata('vendorRole') == 0)
        {
            // get product data
            $getProducts = $this->db->select('*')->from('houdinv_products')->where('houdin_products_status','1')->where('houdinv_products_total_stocks >','0')->get()->result();
            foreach($getProducts as $getProductsList)
            {
                $getPriceData = json_decode($getProductsList->houdin_products_price,true);
                if($getPriceData['tax_price'])
                {
                    $getTax = $this->db->select('houdinv_tax_percenatge')->from('houdinv_taxes')->where('houdinv_tax_id',$getPriceData['tax_price'])->get()->result();
                    $setTax = round($getProductsList->houdin_products_final_price*$getTax[0]->houdinv_tax_percenatge)/100;
                }
                else
                {
                    $setTax = 0;
                }
                if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                {
                    $setFinalPrice = $getProductsList->houdin_products_final_price;
                }
                else
                {
                    $setFinalPrice = $getProductsList->houdin_products_final_price;
                }
               // $setPorudtcArray[] = array('productName'=>$getProductsList->houdin_products_title,'produtcId'=>$getProductsList->houdin_products_id,'variantId'=>0,
               // 'productPrice'=>$setFinalPrice,'stock'=>$getProductsList->houdinv_products_total_stocks,'tax_price'=>$getTax[0]->houdinv_tax_percenatge,'cp'=>$getPriceData['cost_price'],'mrp'=>$getPriceData['sale_price'],'tax_pricedata'=>$setTax);
            }
            // get variant data
            $getVariantData = $this->db->select('*')->from('houdinv_products_variants')->where('houdinv_products_variants_total_stocks >','0')->get()->result();
            foreach($getVariantData as $getVariantDataList)
            {
  $mainproducnt_Id=$getVariantDataList->houdin_products_variants_product_id;
  $GetmanProducts = $this->db->select('*')->from('houdinv_products')->where('houdin_products_id',$mainproducnt_Id)->get()->row();
         
                // fetch variant parent
                $getProducts = $this->db->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getVariantDataList->houdin_products_variants_product_id)->get()->result();
                $getPriceData = json_decode($getProducts[0]->houdin_products_price,true);
                if($getPriceData['tax_price'])
                {
                    $getTax = $this->db->select('houdinv_tax_percenatge')->from('houdinv_taxes')->where('houdinv_tax_id',$getPriceData['tax_price'])->get()->result();
                    $setTax = round($getVariantDataList->houdinv_products_variants_final_price*$getTax[0]->houdinv_tax_percenatge)/100;
                }
                else
                {
                    $setTax = 0;
                }
                if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                {
                    $setFinalPrice = $getVariantDataList->houdinv_products_variants_final_price;
                }
                else
                {
                    $setFinalPrice = $getVariantDataList->houdinv_products_variants_final_price;
                }
                $getVariantPrice = json_decode($getVariantDataList->houdinv_products_variants_new_price,true);
                
                $setPorudtcArray[] = array('productName'=>$GetmanProducts->houdin_products_title.' - '.$getVariantDataList->houdin_products_variants_title,'produtcId'=>0,'variantId'=>$getVariantDataList->houdin_products_variants_id,
                'productPrice'=>$setFinalPrice,'stock'=>$getVariantDataList->houdinv_products_variants_total_stocks,'tax_price'=>$getTax[0]->houdinv_tax_percenatge,'cp'=>$getVariantPrice['cost_price'],'mrp'=>$getVariantPrice['sale_price'],'tax_pricedata'=>$setTax);
            }   
        }
        else
        {

        }

       // return $setPorudtcArray;
         return array('productData'=>$setPorudtcArray,'customerList'=>$getCustomerData,'countryData'=>$setCountryArray);
    }







    public function FetchProduct()
    {
        $getProductList = $this->db->select("*")->from("houdinv_products")->where('houdin_products_status','1')->order_by("houdin_products_id","desc")->get()->result();
        foreach($getProductList as $getProductsList)
        {
            //$setProductArray[] = array('productName'=>$getProductListData->houdin_products_title,'productPrice'=>$getProductListData->houdin_products_final_price,'productStock'=>$getProductListData->houdinv_products_total_stocks,'productId'=>$getProductListData->houdin_products_id,'variantId'=>0);
     
            $setProductArray[] = array('productName'=>$getProductsList->houdin_products_title,'produtcId'=>$getProductsList->houdin_products_id,'variantId'=>0,
            'productPrice'=>$setFinalPrice,'stock'=>$getProductsList->houdinv_products_total_stocks,'tax_price'=>$getTax[0]->houdinv_tax_percenatge,'cp'=>$getPriceData['cost_price'],'mrp'=>$getPriceData['sale_price'],'tax_pricedata'=>$setTax);
     
        }
        $getVariantList = $this->db->select('*')->from('houdinv_products_variants')->get()->result();
        foreach($getVariantList as $getVariantDataList)
        {
            $getVariantPrice = json_decode($getVariantDataList->houdinv_products_variants_new_price,true);
           // $setProductArray[] = array('productName'=>$getVariantListData->houdin_products_variants_title,'productPrice'=>$getVariantListData->houdinv_products_variants_final_price,'productStock'=>$getVariantListData->houdinv_products_variants_total_stocks,'productId'=>0,'variantId'=>$getVariantListData->houdin_products_variants_id);
            $setProductArray[] = array('productName'=>$getVariantDataList->houdin_products_variants_title,'produtcId'=>0,'variantId'=>$getVariantDataList->houdin_products_variants_id,
            'productPrice'=>$setFinalPrice,'stock'=>$getVariantDataList->houdinv_products_variants_total_stocks,'tax_price'=>$getTax[0]->houdinv_tax_percenatge,'cp'=>$getVariantPrice['cost_price'],'mrp'=>$getVariantPrice['sale_price'],'tax_pricedata'=>$setTax);
     
       
        }
        return $setProductArray;
    }
    public function matchCoupon($val)
    {
        $val = strtolower($val);
        $data =  $this->db->select("*")->from("houdinv_coupons")->where("houdinv_coupons_code",$val)->get()->row();
        return $data;
    }
    public function AddWorkOrder($data,$order,$trans)
    {
        $this->db->insert("houdinv_workorder",$data);
        $id =  $this->db->insert_id();
        if($id)
        {
            $getorderInsert = $this->db->insert('houdinv_orders',$order);
            $getOrderId = $this->db->insert_id();
            $this->db->where('houdinv_workorder_id',$id);
            $this->db->update('houdinv_workorder',array('houdinv_workorder_order_id'=>$getOrderId));
            $setTrnsactionArray = array('houdinv_transaction_transaction_id'=>$trans['houdinv_transaction_transaction_id'],'houdinv_transaction_type'=>$trans['houdinv_transaction_type'],
            'houdinv_transaction_method'=>$trans['houdinv_transaction_method'],'houdinv_transaction_from'=>$trans['houdinv_transaction_from'],'houdinv_transaction_for'=>$trans['houdinv_transaction_for'],
            'houdinv_transaction_for_id'=>$id,'houdinv_transaction_amount'=>$trans['houdinv_transaction_amount'],'houdinv_transaction_date'=>$trans['houdinv_transaction_date'],
            'houdinv_transaction_status'=>$trans['houdinv_transaction_status']);
            if($getorderInsert)
            {
                if(count($trans) > 0)
                {
                    $getTransactionInsert = $this->db->insert('houdinv_transaction',$setTrnsactionArray);
                    if($getTransactionInsert)
                    {
                        return array('message'=>'yes');
                    }
                    else
                    {
                        return array('message'=>'trans');
                    }
                }
                else
                {
                    return array('message'=>'yes');
                }
            }
            else
            {
                if(count($trans) > 0)
                {
                    $getTransactionInsert = $this->db->insert('houdinv_transaction',$setTrnsactionArray);
                    if($getTransactionInsert)
                    {
                        return array('message'=>'order');    
                    }
                    else
                    {
                        return array('message'=>'ordertrans');    
                    }
                }
                else
                {
                    return array('message'=>'order'); 
                }
            }
        }
        else
        {
            return array('message'=>'no');
        }
    }


    public function setOrderData($workorderdata,$order,$extra,$pending,$transaction)
    {         
        $this->db->insert("houdinv_workorder",$workorderdata);
        $id =  $this->db->insert_id();

        $this->db->insert('houdinv_orders',$order);
        $getOrderId = $this->db->insert_id();
        if($getOrderId)
        {
            $this->db->where('houdinv_workorder_id',$id);
            $this->db->update('houdinv_workorder',array('houdinv_workorder_order_id'=>$getOrderId));
             // update account receivable(debtor) in case of order is COD
             if($order['houdinv_order_payment_method'] != 'card' && $order['houdinv_order_payment_method'] != 'cheque')
             {
                 if($order['houdinv_payment_status'] == '1')
                 {
                    $getAccountData = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id',16)->where('houdinv_accounts_detail_type_id',170)->get()->result();
                    if(count($getAccountData) > 0)
                    {
                        $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getAccountData[0]->houdinv_accounts_id,
                        'houdinv_accounts_balance_sheet_ref_type'=>'Undeposited Fund',
                        'houdinv_accounts_balance_sheet_pay_account'=>$order['houdinv_order_user_id'],
                        'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                        'houdinv_accounts_balance_sheet_account'=>0,
                        'houdinv_accounts_balance_sheet_order_id'=>$getOrderId,
                        'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                        'houdinv_accounts_balance_sheet__increase'=>$order['houdinv_orders_total_paid'],
                        'houdinv_accounts_balance_sheet_decrease'=>0,
                        'houdinv_accounts_balance_sheet_deposit'=>0,
                        'houdinv_accounts_balance_sheet_final_balance'=>0,
                        'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                        $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                        // get amount
                        $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                        ->where('houdinv_accounts_balance_sheet_account_id',$getAccountData[0]->houdinv_accounts_id)->get()->result();
                        if(count($getTotalAmount) > 0)
                        {
                            $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                        }
                        else
                        {
                            $setFinalAmount = 0;
                        }
                        $this->db->where('houdinv_accounts_id',$getAccountData[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                    }
                 }
                 else
                 {
                    $getAccountData = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id',1)->where('houdinv_accounts_detail_type_id','1')->get()->result();
                    if(count($getAccountData) > 0)
                    {
                        $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getAccountData[0]->houdinv_accounts_id,
                        'houdinv_accounts_balance_sheet_ref_type'=>'Pending Amount',
                        'houdinv_accounts_balance_sheet_pay_account'=>$order['houdinv_order_user_id'],
                        'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                        'houdinv_accounts_balance_sheet_account'=>0,
                        'houdinv_accounts_balance_sheet_order_id'=>$getOrderId,
                        'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                        'houdinv_accounts_balance_sheet__increase'=>$order['houdinv_orders_total_remaining'],
                        'houdinv_accounts_balance_sheet_decrease'=>0,
                        'houdinv_accounts_balance_sheet_deposit'=>0,
                        'houdinv_accounts_balance_sheet_final_balance'=>0,
                        'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                        $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                        // get amount
                        $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                        ->where('houdinv_accounts_balance_sheet_account_id',$getAccountData[0]->houdinv_accounts_id)->get()->result();
                        if(count($getTotalAmount) > 0)
                        {
                            $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                        }
                        else
                        {
                            $setFinalAmount = 0;
                        }
                        $this->db->where('houdinv_accounts_id',$getAccountData[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                    }
                 }
             }
             else
             {
                // update accounts data
                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
                if(count($getUndepositedAccount) > 0)
                {
                    $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                    'houdinv_accounts_balance_sheet_ref_type'=>'Online Transaction',
                    'houdinv_accounts_balance_sheet_pay_account'=>$order['houdinv_order_user_id'],
                    'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                    'houdinv_accounts_balance_sheet_account'=>0,
                    'houdinv_accounts_balance_sheet_order_id'=>$getOrderId,
                    'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                    'houdinv_accounts_balance_sheet__increase'=>$order['houdinv_orders_total_paid'],
                    'houdinv_accounts_balance_sheet_decrease'=>0,
                    'houdinv_accounts_balance_sheet_deposit'=>0,
                    'houdinv_accounts_balance_sheet_final_balance'=>0,
                    'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                    $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                    // get amount
                    $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                    ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                    if(count($getTotalAmount) > 0)
                    {
                        $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                    }
                    else
                    {
                        $setFinalAmount = 0;
                    }
                    $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                }
                // end here
             }
             // end here
            if(count($extra) > 0)
            {
                $getAddress = $extra['address'];
                $getAddressValueData = explode('^',$getAddress);
                $setInsertArray = array('houdinv_order_users_order_id'=>$getOrderId,'houdinv_order_users_name'=>$extra['name'],'houdinv_order_users_contact'=>$extra['contact'],'houdinv_order_users_main_address'=>$getAddressValueData[0],
                'houdinv_order_users_city'=>$getAddressValueData[1],'houdinv_order_users_zip'=>$getAddressValueData[2]);
                $getInsertData = $this->db->insert('houdinv_order_users',$setInsertArray);
                if($getInsertData)
                {
                    // add undeposited fund
                            if($order['houdinv_order_payment_method'] == "cash")
                            {
                                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->get()->result();    
                                if(count($getUndepositedAccount) > 0)
                                {
                                    $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                                    'houdinv_accounts_balance_sheet_ref_type'=>'Undeposited Fund',
                                    'houdinv_accounts_balance_sheet_pay_account'=>$order['houdinv_order_user_id'],
                                    'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                                    'houdinv_accounts_balance_sheet_account'=>0,
                                    'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                                    'houdinv_accounts_balance_sheet__increase'=>$order['houdinv_orders_total_paid'],
                                    'houdinv_accounts_balance_sheet_decrease'=>0,
                                    'houdinv_accounts_balance_sheet_deposit'=>0,
                                    'houdinv_accounts_balance_sheet_final_balance'=>0,
                                    'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                                    $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                                    // get amount
                                    $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                                    ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                                    if(count($getTotalAmount) > 0)
                                    {
                                        $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                                    }
                                    else
                                    {
                                        $setFinalAmount = 0;
                                    }
                                    $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                                }
                            }
                    return array('message'=>'yes','orderId'=>$getOrderId);
                }
                else
                {
                    return array('message'=>'user','orderId'=>$getOrderId);
                }
            }
            else
            {
               
                if(count($transaction) > 0)
                {
                    $setTransactionId = "TXN".rand(99999999,10000000);
                    $setTrasanctionArray = array('houdinv_transaction_transaction_id'=>$setTransactionId,'houdinv_transaction_type'=>'credit',
                    'houdinv_transaction_method'=>$transaction['houdinv_transaction_method'],'houdinv_transaction_from'=>'pos',
                    'houdinv_transaction_for'=>'order','houdinv_transaction_for_id'=>$getOrderId,'houdinv_transaction_amount'=>$transaction['houdinv_transaction_amount'],
                    'houdinv_transaction_date'=>date('Y-m-d'),'houdinv_transaction_status'=>'success');
                    $this->db->insert('houdinv_transaction',$setTrasanctionArray);
                }
   
                if(count($pending) > 0)
                {
                    $getPendingAmount = $this->db->select('houdinv_users_pending_amount')->from('houdinv_users')->where('houdinv_user_id',$pending['userId'])->get()->result();
                    if(count($getPendingAmount) > 0)
                    {
                        $newPendingAmount = $getPendingAmount[0]->houdinv_users_pending_amount+$pending['pendingAmount'];
                    }
                    else
                    {
                        $newPendingAmount = $pending['pendingAmount'];
                    }
                    $this->db->where('houdinv_user_id',$pending['userId']);
                    $this->db->update('houdinv_users',array('houdinv_users_pending_amount'=>$newPendingAmount));
                }

                return array('message'=>'yes','orderId'=>$getOrderId);    
            }
        }
        else
        {
            return array('message'=>'no');
        }
    }








    public function UpdateWorkOrder($data,$id,$workOrder)
    {
        $getTransactionStatus = $this->db->insert('houdinv_transaction',$data);
        if($getTransactionStatus)
        {
            $this->db->where('houdinv_order_id',$id);
            $getStatus = $getOrderStatus = $this->db->update('houdinv_orders',array('houdinv_payment_status'=>'1'));
            if($getStatus)
            {
                $this->db->where('houdinv_workorder_id',$workOrder);
                $this->db->update('houdinv_workorder',array('houdinv_workorder_payment_status'=>'1'));
                return array('message'=>'yes');
            }
            else
            {
                $this->db->where('houdinv_workorder_id',$workOrder);
                $this->db->update('houdinv_workorder',array('houdinv_workorder_payment_status'=>'1'));
                return array('message'=>'order');
            }
        }
        else
        {
            return array('message'=>'no');
        }
   
    }
    public function DeleteWorkOrder($id,$orderID)
    {
        $this->db->where('houdinv_workorder_id',$id);
        $this->db->delete("houdinv_workorder");
        $this->db->where('houdinv_order_id',$orderID);
        $this->db->delete('houdinv_orders');
        $this->db->where('houdinv_transaction_for','Work Order')->where('houdinv_transaction_for_id',$id);
        $this->db->delete('houdinv_transaction');
    }
    public function AllWorkOrders()
    {
        $getWorkOrderData = $this->db->select('houdinv_workorder.*,houdinv_users.houdinv_user_name,houdinv_users.houdinv_user_contact,houdinv_user_address.houdinv_user_address_user_address,houdinv_coupons.houdinv_coupons_id')
        ->from('houdinv_workorder')
        ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_workorder.houdinv_workorder_customer_name','left outer')
        ->join('houdinv_user_address','houdinv_user_address.houdinv_user_address_id = houdinv_workorder.houdinv_workorder_address','left outer')
        ->join('houdinv_coupons','houdinv_coupons.houdinv_coupons_code = houdinv_workorder.houdinv_workorder_coupon_code','left outer')
        ->get()->result();
        foreach($getWorkOrderData as $getWorkOrderDataList)
        {
            if($getWorkOrderDataList->houdinv_workorder_product != 0 && $getWorkOrderDataList->houdinv_workorder_product != "")
            {
                $getProduct = $this->db->select('houdin_products_title')->from('houdinv_products')->where('houdin_products_id',$getWorkOrderDataList->houdinv_workorder_product)->get()->result();
                $getProductName = $getProduct[0]->houdin_products_title;
            }
            else
            {
                $getProductVariant = $this->db->select('houdin_products_variants_title')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getWorkOrderDataList->houdinv_workorder_variant)->get()->result();
                $getProductName = $getProductVariant[0]->houdin_products_variants_title;
            }
            $setArrayData[] = array('workorder'=>$getWorkOrderDataList,'pname'=>$getProductName);
         }
        return $setArrayData;

    }
    public function fetchOutletData()
    {
        $getOutletData = $this->db->select('*')->from('houdinv_shipping_warehouse')->get()->result();
        return $getOutletData;
    }
}
