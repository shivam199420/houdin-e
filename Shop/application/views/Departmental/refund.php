<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header") ?>

    <!-- Page Title -->
    <section>
        <div class="container py-85 py-tn-50">
            <div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-12 ">
                            <h2 class="title text-center">Refund Policy</h2>
                             <div class="title-border mx-auto m-b-tn-35 text-center"></div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </section>

    <!-- End Page Title -->
   

    <!-- Why Choose Us -->
    <section id="why-choose">
        <div class="container py-70 py-tn-30">
            <div class="row p-t-46 p-b-32">
                
                    <div class="port-title p-r-60 p-r-tn-0">
                        <p class="title-detail"> <?php echo $refund[0]->Cancellation_Refund_Policy ?></p>
                    </div>
                
            </div>
        </div>
    </section>
    <!-- End Why Choose Us -->
    <?php 
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer") ?>