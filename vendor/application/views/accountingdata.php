<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); ?>
<style type="text/css">
.zoom img{
    
    transition-duration: 5s;
    margin: 0 auto;
}
img {
    vertical-align: middle;
    height: 40px;
    width: auto; 
} 

.zoom { 
    transition: all 1s;
    -ms-transition: all 1s;
    -webkit-transition: all 1s;
    margin-top: 0px;
    padding-top: 0px;
    
}
.zoom:hover {
    -ms-transform: scale(2); /* IE 9 */
    -webkit-transform: scale(2); /* Safari 3-8 */
    transform: scale(2); 
    margin-left: 40px;
}
.m-b-10{
    margin-bottom: 10px;
}
.select2-container.select2-container--default
{
    width:100% !important;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" /> 
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
<!--START Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                <div class="col-md-8">
                   <h4 class="page-title">Accounting</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>Accounts">Accounts</a></li>
                 
                  <li class="active">Accounting</li>
                  </ol> 
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                  
                </div>
                <?php 
           
                ?>
            </div>
           <!--END Page-Title --> 
           
           <div class="row">
        <div class="col-sm-12">
            <?php 
            if($this->session->flashdata('error'))
            {
                echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
            }
            if($this->session->flashdata('success'))
            {
                echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
            }
            ?>
        </div>
    </div>
                                    
        <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">
              <div class="btn-group pull-right m-t-10 m-b-20">
                  
                  <a href="<?php echo base_url() ?>Accounts/addaccounting" type="button" class="btn btn-default m-r-5" title="Add account"><i class="fa fa-plus"></i> Add</a>
                    <a href="<?php echo base_url() ?>Accounts/balancetransfers" type="button" class="btn btn-default m-r-5" title="Balance ransfer account to account"><i class="fa fa-plus"></i> Balance Transfer</a>
               <a href="<?php echo base_url() ?>Accounts/journalentry_account" type="button" class="btn btn-default m-r-5" title="journal entry accounting"><i class="fa fa-plus"></i></a>

              </div>
              <table class="table table-hover table-bordered table_shop_custom">
                <thead>
                  <tr>

                    <th style="width: 15%">Name</th>
                    <th>Type</th>
                    <th>Detail type</th>
                    <th>Tax Rate</th>
                    <th>Balance</th>
                    <!-- <th>Bank Balance</th> -->
                    <th>Action</th>  
                  </tr>
                </thead>
                <tbody>
<?php foreach($all as $data)
{   
    ?>
    <tr>
        <td><?php echo $data['houdinv_accounts_name']; ?></td>
        <td><?php echo $data['account_type_name']; ?></td>
        <td><?php echo $data['houdinv_detail_type_name']; ?></td>
        <td><?php echo $data['houdinv_accounts_default_tax_code']; ?></td>
        <td><?php if($data['houdinv_accounts_final_balance']) { echo $data['houdinv_accounts_final_balance']; } else { echo  0; } ?></td>
        <!-- <td></td> -->
        <td>
        <?php 
        if($data['houdinv_detail_type_report'] == 'register')
        {
        ?>
        <!-- <a class="btn btn-default m-r-5" title="View Register"  href="<?php echo base_url();?>Accounts/accounthistory/<?php echo $data['houdinv_accounts_id'];?>"><i class="fa fa-eye"></i></a> -->
        
        <a class="btn btn-default m-r-5" title="Account Details"  href="<?php echo base_url();?>Accounts/accounthistorydetails/<?php echo $data['houdinv_accounts_id'];?>"><i class="fa fa-eye"></i></a>
        
        <?php }
        ?>
        <!-- <a class="btn btn-default m-r-5" title="Edit Account"  href="<?php //echo base_url();?>Accounts/editaccountdata/<?php //echo $data['houdinv_accounts_id'];?>"><i class="fa fa-edit"></i></a> -->
        <a class="btn btn-default m-r-5" title="View Report"  href="<?php echo base_url();?>Accounts/report/<?php echo $data['houdinv_accounts_id'];?>"><i class="fa fa-file"></i></a>
        <?php 
        if(($data['houdinv_accounts_account_type_id'] == 16 && $data['houdinv_accounts_detail_type_id'] == 170) ||
        ($data['houdinv_accounts_account_type_id'] == 3 && $data['houdinv_accounts_detail_type_id'] == 21) ||
        ($data['houdinv_accounts_account_type_id'] == 1 && $data['houdinv_accounts_detail_type_id'] == 1) ||
        ($data['houdinv_accounts_account_type_id'] == 2 && $data['houdinv_accounts_detail_type_id'] == 7) || 
        ($data['houdinv_accounts_account_type_id'] == 14 && $data['houdinv_accounts_detail_type_id'] == 152) ||
        ($data['houdinv_accounts_account_type_id'] == 11 && $data['houdinv_accounts_detail_type_id'] == 109) || 
        ($data['houdinv_accounts_account_type_id'] == 6 && $data['houdinv_accounts_detail_type_id'] == 52))
        {
        }
        else
        {
        ?>
        <button type="button" class="btn btn-default m-r-5 deleteAccountData" data-id="<?php echo $data['houdinv_accounts_id'];?>" title="Delete Account"><i class="fa fa-trash"></i></button></td>
        <?php }
    ?>
        
    </tr>
    <?php
}?>
              
                
               
                </tbody>
              </table>
              
              <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--Delete-->

  <div id="delete_Accounts" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
      <?php echo form_open(base_url().'Accounts/accounting'); ?>
 
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete Account</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this account ?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
               <input type="hidden" name="delete_id" class="delete_id" />
          <input type="submit" class="btn btn-info" name="DeleteAccountEntry" value="Delete">

          </div>

          </div>
       <?php echo form_close(); ?> 
          </div>

          </div>
<!--change Status-->
<div id="add_journal_entry" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

            <div class="modal-dialog">
            <?php echo form_open(base_url( 'Account/AddjournalEntry' ), array( 'id' => 'AddjournalEntryForm', 'method'=>'post' ));?>
            <div class="modal-content">

            <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

            <h4 class="modal-title">Add Journal Entry</h4>

            </div>

            <div class="modal-body">

            <div class="row">
            <div class="col-md-12">
            
            <div class="form-group">
            <label>Date</label>
            <input type="text" class="form-control date_picker" value="<?php echo date('Y-m-d') ?>" name="journaldate"/>
            </div>
            
            <div class="form-group">
            <label for="userName">Ref. Number</label>
            <input type="text" class="form-control name_validation" name="refNumber"/>
            </div>
            <div class="form-group">
            <label for="userName">Entry Type</label>
            <input type="text" class="form-control" name="entryType" value="journal Entry" readonly="readonly"/>
            </div>
            <div class="form-group">
            <label for="userName">Select Payee</label>
            <input type="hidden" name="payeetype" class="payeetype"/>
            <select class="form-control select1 setPayeeData" name="payeeid">
            <option value="">Select Payee</option>
            <?php 
            for($index = 0; $index < count($accountholder); $index++)
            {
            ?>
            <option data-user="<?php echo $accountholder[$index]['type']; ?>" value="<?php echo $accountholder[$index]['id']; ?>"><?php echo $accountholder[$index]['name'] ; ?></option>
            <?php }
            ?>
            </select>
            </div>
            <div class="form-group">
            <label for="userName">Select Account</label>
            <select class="form-control select1" name="accoutnid">
            <option value="">Select Account</option>
            <?php 
            foreach($parentAccount as $parentAccountList)
            {
            ?>
            <option value="<?php echo $parentAccountList->houdinv_accounts_id ?>"><?php echo $parentAccountList->houdinv_accounts_name ?></option>
            <?php }
            ?>
            </select>
            </div>
            <div class="form-group">
            <label for="userName">Memo</label>
            <input type="text" class="form-control name_validation" name="memo"/>
            </div>
            <div class="form-group">
            <label for="userName">Debit</label>
            <input type="text" class="form-control name_validation setDecrease" name="setDecrease" placeholder="Decrease"/>
            </div>
            <div class="form-group">
            <label for="userName">Credit</label>
            <input type="text" class="form-control name_validation setIncreaseData" name="setIncreaseData" placeholder="Increase"/>
            </div>
            <div class="form-group">
            <label for="userName">Choose Concile</label>
            <select class="form-control" name="concile">
            <option value="">Choose Concile Status</option><option value="C">C</option><option value="R">R</option>
            </select>
            </div>
            </div>
            </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-info" name="addjournalEntry" value="Add Journal Entry">

            </div>

            </div>
            <?php echo form_close(); ?>
            </div>

            </div>



<?php $this->load->view('Template/footer') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<script>
// $(document).on("click",".status_tab",function()
// {
    
//   $id_status = $(this).attr("data-id");  
//   $("#status_id").val($id_status);
    
// });

// $(document).on("click",".delete_tab",function()
// {
    
//   $id_delete = $(this).attr("data-id"); 
//    $("#delete_id").val($id_delete);    
// });
$(document).on('click','.deleteAccountData',function(){
    $('.delete_id').val($(this).attr('data-id'));
    $('#delete_Accounts').modal('show');
})
$(".select1").select2({
    // minimumInputLength: 2
});
$(document).on('blur','.setIncreaseData',function(){
    if($(this).val() != "")
    {
        $('.setDecrease').val('');
    }
});
$(document).on('blur','.setDecrease',function(){
    if($(this).val() != "")
    {
        $('.setIncreaseData').val('');
    }
});
$(document).on('change','.setPayeeData',function()
{
    $('.payeetype').val($(this).children('option:selected').attr('data-user'));
})

</script>
