        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <style type="text/css">
        .zoom img{

        transition-duration: 5s;
        margin: 0 auto;
        }
        img {
        vertical-align: middle;
        height: 40px;
        width: auto; 
        } 

        .zoom {
        transition: all 1s;
        -ms-transition: all 1s;
        -webkit-transition: all 1s;
        margin-top: 0px;
        padding-top: 0px;

        }
        .zoom:hover {
        -ms-transform: scale(2); /* IE 9 */
        -webkit-transform: scale(2); /* Safari 3-8 */
        transform: scale(2); 
        margin-left: 40px;
        }
        </style>
        <div class="content-page">
        <!-- Start content --> 
        <div class="content">
        <div class="container">


        <!--START Page-Title -->
        <div class="row">
      
        <div class="col-md-8">
        <h4 class="page-title">Manage Inventory</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>inventory/Inventorylanding">Inventory</a></li>
        <li class="active">Manage Inventory</li>
        </ol>
        </div>
        <!-- <div class="col-md-4">
        <form role="search" class="navbar-left app-search pull-left custom_search_all">
        <input type="text" placeholder="Search..." class="form-control">
        <a href=""><i class="fa fa-search"></i></a>
        </form></div> -->

        </div>
        <!--END Page-Title -->

        <div class="row m-t-20">
        <div class="col-md-12">

        <div class="card-box table-responsive">

        <table id="manageinventory" class="table table-striped table-bordered table_shop_custom  display">
        <thead>
        <tr>
        <th style="width: 15%">Product Image</th>
        <th>Product Title</th>
        <th>Product Stock</th>
        <th>Variants View</th>

        </tr>

        </thead>
        <tbody>
        
        </tbody>
        </table>
        </div>
        </div>
        </div>


        </div>
        </div> 
        </div>


       <!-- Modal -->
  <div class="modal fade" id="stockcheckModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="productnameset">Inventory variants</h4>
        </div>
        <div class="modal-body">
        <table id="manageinventoryvariants" style="width: 100%;" class="table table-striped table-bordered">
        <thead>
        <tr>
        <th style="width: 15%">Product Image</th>
        <th>Product Title</th>
        <th>Product Stock</th>      

        </tr>

        </thead>
        <tbody id="tbodayinvontrydata">
        
        </tbody>
        </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

        <?php $this->load->view('Template/footer.php') ?>

        <script>
        $(document).ready(function() {
        $('#manageinventory').DataTable( {
        "ajax": "<?php echo base_url(); ?>inventory/manageinventory_api"
        } );

        $(document).on('click','.stockcheckBtn',function(){
                var table = $('#manageinventoryvariants').DataTable();
                table.destroy();
             var product_id = $(this).attr('datap-id');             
             var product_name = $(this).attr('datap-name'); 
             $('#productnameset').html(product_name);  
             $('#manageinventoryvariants').DataTable( {
        "ajax": "<?php echo base_url(); ?>inventory/manageinventory_api_varints?pid="+product_id
        } );

             $('#stockcheckModal').modal('show'); 
        });
        } );

        </script>