<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');

$images_all = json_decode($product['main_product']->houdinv_products_main_images);
$price_all = json_decode($product['main_product']->houdin_products_price,true);
$final_price = $product['main_product']->houdin_products_final_price;
$overall_rating = $product['reviews_sum']->houdinv_product_review_rating;
$total_rating = count($product['reviews']);
$final_rating = $overall_rating/$total_rating;
 ?>
 <?php
 $getCurrency = getShopCurrency();
 $currencysymbol=$getCurrency[0]->houdin_users_currency;
 ?>
<style>
.checked {
color: orange;
}

</style>


   <!-- About Banner Start -->

   <section id="about">

       <div class="container">

           <div class="row">

               <div class="about-heading text-center">

                   <h2>Product Details</h2>

               </div>

           </div>

       </div>

   </section>

   <!-- About Banner End -->



   <!-- Product Details Part start -->

   <section id="details-of-product">

       <div class="container">

           <div class="row">

               <div class="col-md-5 tahsan2">

                   <div class="xzoom-container">

                      <img class="xzoom" id="xzoom-default" src="<?php echo $this->session->userdata('vendorUrl') ?>/upload/productImage/<?php echo $images_all[0]; ?>" xoriginal="<?php echo $this->session->userdata('vendorUrl') ?>/upload/productImage/<?php echo $images_all[0]; ?>" alt="" />

                        <div class="xzoom-thumbs">

                        <?php foreach($images_all as $image_al){ ?>
                        <a href="<?php echo $this->session->userdata('vendorUrl') ?>/upload/productImage/<?php echo $image_al; ?>"><img class="xzoom-gallery" width="80" src="<?php echo $this->session->userdata('vendorUrl') ?>/upload/productImage/<?php echo $image_al; ?>"  xpreview="<?php echo $this->session->userdata('vendorUrl') ?>/upload/productImage/<?php echo $image_al; ?>" alt=""></a>
                        <?php } ?>

                        <!-- <a href="images/girl1.jpg"><img class="xzoom-gallery" width="80" src="images/girl1.jpg" alt=""></a>

                        <a href="images/girl1.jpg"><img class="xzoom-gallery" width="80" src="images/girl1.jpg" alt=""></a> -->

                      </div>

                    </div>

               </div>

               <div class="details-product-item col-md-7">

                   <div class="product-details">

                       <div>

                          <h3><?php echo $product['main_product']->houdin_products_title; ?></h3>

                          <h4>

                            <?php for($i=1 ; $i<6 ;$i++)
                               {
                               if($i <= $final_rating)
                               {
                                   $posi = strpos($final_rating,".");
                                   if($posi>=0)
                                   {
                                    $exp =  explode(".",$final_rating);
                                    if($exp[0]==$i)
                                    {
                                       ?>
                                   <i class="fa fa-star-half-o"></i>
                                       <?php
                                       }
                                       else
                                       {
                                           echo '<i class="fa fa-star"></i>';
                                       }
                                   }
                                   else
                                   {
                                   ?>
                                   <i class="fa fa-star"></i>
                                   <?php
                                    }
                                }
                                else
                                {
                                   ?>

                               <i class="fa fa-star-o"></i>


                               <?php } } ?>

                              <span>| (<?php echo $total_rating; ?> Reviews)</span>

                          </h4>

                          <h5>  <span class="new"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $final_price; ?></span>
                          <strike>  <span class="old">(<?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $price_all['sale_price'];  ?>)</span></strike>
                          </h5>

                          <p><?php echo $product['main_product']->houdin_products_short_desc; ?></p>

                       </div>

                   </div>

                   <div class="color-select category">

                    <?php if(!empty($product['related'])){?> 
                    <select style="margin-bottom: 36px;width: 50%;" title="Pick a number" class="form-control variantsselect">
                    <option>Select variants </option>
                    <?php 
                    foreach($product['related'] as $related){?>
                    <option value="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>"><?php echo $related->houdinv_products_variants_name; ?></option>
                    <?php } ?>
                    </select>
                    <?php  }?>

                      <div class="checkbox chek2 checkbox-success check22">

                           <ul>

                              <li><span>Availability</span></li>

                               <li>
                                 <?php
                                    if($product['main_product']->houdinv_products_total_stocks > 0)
                                    {
                                        $setStock = "In Stock";
                                    }
                                    else
                                    {
                                        $setStock = "Out of Stock";
                                    }
                                  ?>
                                    <p><?php echo $setStock; ?></p>

                               </li>

                           </ul>

                        </div>


                   </div>

                   <div class="quantity">



                       <div class="handle-counter" id="handleCounter">

                       <div class="quan-head">

                          <h3>Quantity :</h3>

                      </div>

                        <div class="input">

                          <input value="1" min="1" data-max="<?php echo $product['main_product']->houdinv_products_total_stocks ?>" name="qttybutton" class="cart-plus-minus-box setCartQunatity" type="text">
                            <!-- <input type="text" value="1"> -->

                        </div>

                        <div class="click">

                            <button class="counter-plus btn btn-primary">+</button>

                            <button class="counter-minus btn btn-primary">-</button>

                        </div>

                        <div class="clearfix"></div>

                    </div>

                   </div>

                   <div class="add-wishlist">

                     <?php
                     if($product['setcartStatus'] == 'yes')
                     {
                       ?>
                       <a href="javascript:;" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" title="Already in Cart">
                                          <i class="fa fa-heart"></i>
                                      </a>
                     <?php }
                     else {
                       ?>
                       <a href="javascript:;" class="Add_to_cart_button" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" title="Add to Cart">
                                          <i class="fa fa-heart"></i>
                                      </a>
                     <?php }
                     ?>

                     <?php
                     if($product['setWishlistStatus'] == 'yes')
                     {
                       ?>
                       <a href="javascript:;" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" title="Already in wishlist">
                               <i class="fa fa-shopping-basket"></i>
                           </a>
                     <?php }
                     else {
                       ?>
                       <a href="javascript:;" class="Add_to_whishlist_button" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" title="Add to wishlist">
                               <i class="fa fa-shopping-basket"></i>
                           </a>
                     <?php }
                     ?>

                   </div>

                   <!-- <div class="code-cate">

                       <p>Code<a href="#">: SKY-12345678</a></p>

                       <p>Category<a href="#" class="tahsan3">: Fashion</a></p>

                       <p>Tags<a href="#" class="tahsan5">: T-shirt, Skirt</a></p>

                   </div>

                   <div class="share">

                    <h6>Share:</h6>

                      <a href="#"><i class="fa fa-facebook"></i></a>

                      <a href="#"><i class="fa fa-twitter"></i></a>

                      <a href="#"><i class="fa fa-behance"></i></a>

                      <a href="#"><i class="fa fa-linkedin"></i></a>

                      <a href="#"><i class="fa fa-pinterest-p"></i></a>

                  </div> -->

               </div>

           </div>

       </div>

   </section>

  <!-- product details end -->



<!-- Product Discription Part start -->

   <section id="discription">

       <div class="container">

           <div class="">

               <div class="">

                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Description</a></li>

                     <li><a href="#">|</a></li>

                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Reviews</a></li>

                </ul>

                <!-- Tab panes -->

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="home">

                        <p> <?php echo $product['main_product']->houdin_products_desc; ?>			</p>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="profile"><div class="row">
                                <div class="col-md-6">
                                <?php
                                if(!$final_rating > 0)
                                {
                                ?>
                                <p>There are no reviews yet.</p>
                                    <p class="text-bigger">Be the first to review</p>
                                <?php }
                                else
                                {
                                    foreach($product['reviews'] as $user_Review)
                                      {
                                ?>
                                 <div style="margin-bottom:10px">
                                          <div class=""><?php echo $user_Review->houdinv_product_review_user_name ?></div>


                                          <div class="strat-rating">
                                          <?php for($i=1 ; $i<6 ;$i++)
                                          {
                                          if($i <= $user_Review->houdinv_product_review_rating)
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star checked"></span>
                                          <?php }
                                          else
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star"></span>
                                          <?php } } ?>

                                            <span class="text-date"><?php echo $user_Review->houdinv_product_review_created_at ?></span>
                                          </div>


                                          <div class="word_break" style=""><?php echo  $user_Review->houdinv_product_review_message; ?></div>
                                        </div>

                                <?php } }
                                ?>


                                </div>
                                <div class="col-md-6">
                                    <div class="form-add table-responsive">
                                    <?php echo form_open(base_url( 'Productlistview' ), array( 'id' => 'rtng_form',"class"=>"checkout_form", 'method'=>'post' ));?>
                                        <div class="form-group">

                                        <div class="strat-rating pd_mobile_star">
                                            <span data-rating="1" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="2" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="3" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="4" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="5" style="font-size: 22px;" class="fa fa-star"></span>
                                            <input type="hidden"  name="rating" class="rating_data" value="0" />
                                            <input type="hidden"  name="product_id" class="product" value="<?php echo $product_id; ?>" />
                                            <input type="hidden"  name="variant_id" class="" value="0" />
                                        </div>
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation form-control" type="text" name="user_name" placeholder="Your Name" />
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation email_validation form-control" type="text" name="user_email" placeholder="Your Email" />
                                        </div>
                                        <div class="form-group">

                                        <textarea  class="required_validation_for_review form-control" name="user_message" rows="3" placeholder="Write a review"></textarea>
                                        </div>
                                        <input class="btn-primary form-control submit_form_review" type="submit" name="review_submit" value="Add Review" />
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                          </div>

                </div>



                <!-- Latest Part Start -->
                      <?php
                      if(count($product['related']) > 0)
                      {
                       ?>
                      <section id="latest">

                      <i class="fa fa-chevron-left prv-arrow2"></i>

                      <i class="fa fa-chevron-right nxt-arrow2"></i>

                      <div class="container">

                      <div class="row">

                      <div class="latest-main">

                      <div class="heading2 text-center">

                      <h2>Related products</h2>

                      </div>

                      <div class="latest-item">

                        <?php foreach($product['related'] as $related)
                        {
                        ?>

                      <div class="gallery_product col-md-3">

                        <div class="featured-product">
                        <?php 
                        if($related->houdin_products_variants_image)
                        {
                            $setVariantIMageData = $this->session->userdata('vendorURL')."upload/productImage/".$related->houdin_products_variants_image;
                        }
                        else
                        {
                            $setVariantIMageData = base_url()."Extra/noPhotoFound.png";
                        }
                        ?>
                        <img src="<?php echo $setVariantIMageData ?>" alt="featured-product-img" class="img-responsive">

                        <div class="overlay2 text-center">

                        <a class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $related->houdin_products_variants_id ?>"><i class="fa fa-heart"></i></a>



                        <a href="<?php echo base_url() ?>Productlistview/<?php echo $related->houdin_products_variants_id ?>"><i class="fa fa-random"></i></a>

                        <?php

                        if($latestProductsList->houdinv_products_total_stocks <= 0)
                        {
                          if($cartFunction == 1)
                          {
                            ?>
                            <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $related->houdin_products_variants_id ?>"><i class="fa fa-shopping-basket"></i></a>
                          <?php  }
                        }
                        else
                        {
                          ?>
                          <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $related->houdin_products_variants_id ?>"><i class="fa fa-shopping-basket"></i></a>
                        <?php }
                        ?>

                        </div>

                        </div>

                      <div class="feat-details">

                      <a href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>"><p><?php echo $related->houdin_products_variants_title; ?></p></a>
                      <span><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $related->houdinv_products_variants_final_price; ?></span>

                      <div class="clearfix"></div>

                      </div>


                      </div>
                    <?php }
                    ?>



                      </div>

                      </div>

                      </div>

                      </div>

                      </section>

                    <?php }
                    ?>



               </div>

           </div>

       </div>

   </section>

   <!-- product discription end -->

   <?php
   $getShopName = getfolderName($this->session->userdata('shopName'));
   $this->load->view(''.$getShopName.'/Template/footer'); ?>

   <script type="text/javascript">
   $(document).ready(function(){
       $(document).on('keyup','.setCartQunatity',function(){
           if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
           {
               $(this).val($(this).attr('data-max'));
           }
       })
       $(document).on('change','.setCartQunatity',function(){
           if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
           {
               $(this).val($(this).attr('data-max'));
           }
       })
   })
   </script>



   <script src="<?php echo base_url() ?>Extra/optical/js/slick.js"></script>

   <script src="<?php echo base_url() ?>Extra/optical/js/jquery.countdown.min.js"></script>

   <script src="<?php echo base_url() ?>Extra/optical/js/handleCounter.js"></script>

   <script src="<?php echo base_url() ?>Extra/optical/js/xzoom.min.js"></script>

   <script src="<?php echo base_url() ?>Extra/optical/js/setup.js"></script>

   <script src="<?php echo base_url() ?>Extra/optical/js/jquery.meanmenu.min.js"></script>

   <script>

    $(function ($) {

           var options = {

               minimum: 1,

               maximize: 100,

               onChange: valChanged,

               onMinimum: function(e) {

                   console.log('reached minimum: '+e)

               },

               onMaximize: function(e) {

                   console.log('reached maximize'+e)

               }

           }

           $('#handleCounter').handleCounter(options)

           $('#handleCounter2').handleCounter(options)

     $('#handleCounter3').handleCounter({maximize: 100})

       })

       function valChanged(d) {

//            console.log(d)

       }

   </script>
