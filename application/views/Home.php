<?php  $this->load->view('Template/header');?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css">
   
<!-- <link href="<?php echo base_url(); ?>assets/css/index.css" rel="stylesheet">-->
    <link href="<?php echo base_url(); ?>assets/css/mfb.css" rel="stylesheet">
 
<style type="text/css">
  .custom-left-title{
    margin-top: 4%;
    font-size: 46px;

  }
  .pb-8, .py-8 {
    padding-bottom: 2rem !important;
}
.font600{
  font-weight: 600;
}
.mb-1, .my-1 {
    margin-bottom: .25rem !important;
    line-height: 17px !important;
    font-size: 18px !important;
    color:#000;
}
.pricing-1 .plan-name {
        padding: 20px;
    text-transform: uppercase;
    font-size: 22px;
    font-weight: 600;
    border-bottom: 1px solid #f1f2f3;
    background: linear-gradient(to bottom,#9e6d83 0,#d4c5cc 101%);
    color:#ffffff;
    border-radius: 0px 25px 0px 25px;

}
.pricing-2-transform
{
    transform: scale(1.1);
}
.pricing-1 .small
{
        background: #a98093;
    padding: 9px;
    color: #fff!important;
    font-size: 15px;
}
.pricing-1 {
    background-color: #fff;
    border: 1px solid #f1f2f3;
    border-radius: 5px;
    text-align: center;
    -webkit-transition: .5s;
    transition: .5s;
    min-height: 728px;
}
.pricing-1 .price {
    font-size: 38px;
    font-weight: 600;
}
.font46{
  font-size: 38px;
    color: #5079ad;
}
.custom-min-height{
  min-height:296px;
}
.text-about {
  
  overflow:hidden;
  font-size:48px;
}
.section-header {
    text-align: center;
    max-width: 70%;
    margin: 0 auto 30px !important;
}
.text-about:first-of-type {    /* For increasing performance 
                       ID/Class should've been used. 
                       For a small demo 
                       it's okaish for now */
  animation: showup 6s infinite;
}

.text-about:last-of-type {
  width:0px;
  animation: reveal 7s infinite;
}

.text-about:last-of-type span {
  margin-left:-355px;
  animation: slidein 7s infinite;
}
.background--red {
    background-color: #5079ad;
}
.background {
    position: relative;
    padding: 30px 0;
}
.triangle--red:after {
    border-color: #5079ad transparent transparent transparent !important;
}
.bottom__triangle:after {
    border-color: #ffffff transparent transparent transparent;
    content: '';
    position: absolute;
    margin-top: 30px;
    transform: translateX(-50%);
    border-style: solid;
    border-width: 30px 75px 0 75px;
    margin-left: 50%;
    bottom: -30px;
    z-index: 100;
}
 
.custom_lead_p
{
    margin:10px 0px;
}
.btn_scroll_down {
    -webkit-animation: pulse 2s infinite;
    animation: pulse 2s infinite;
}
@-webkit-keyframes pulse {
  0% {
    -webkit-transform: translate(0, 0);
            transform: translate(0, 0); }
  50% {
    -webkit-transform: translate(0, 10px);
            transform: translate(0, 10px); }
  100% {
    -webkit-transform: translate(0, 0);
            transform: translate(0, 0); } }

@keyframes pulse {
  0% {
    -webkit-transform: translate(0, 0);
            transform: translate(0, 0); }
  50% {
    -webkit-transform: translate(0, 10px);
            transform: translate(0, 10px); }
  100% {
    -webkit-transform: translate(0, 0);
            transform: translate(0, 0); } }
}

@keyframes showup {
    0% {opacity:0;}
    20% {opacity:1;}
    80% {opacity:1;}
    100% {opacity:0;}
}

@keyframes slidein {
    0% { margin-left:-800px; }
    20% { margin-left:-800px; }
    35% { margin-left:0px; }
    100% { margin-left:0px; }
}

@keyframes reveal {
    0% {opacity:0;width:0px;}
    20% {opacity:1;width:0px;}
    30% {width:500px;}
    80% {opacity:1;}
    100% {opacity:0;width:500px;}
}
@media (max-width:767px){
  .h-fullscreen, .h-100vh {
    height: 64vh !important;
}
.font46 {
    font-size: 36px;
    line-height: 42px;
}
.custom_lead_p {
    font-size: 1.17188rem !important;
    line-height: 1.65;
}
.navbar-light .navbar-toggler, .navbar-stick-light.stick .navbar-toggler {
    color: black!important;
    z-index: 9999;
}
.navbar-expand-lg .nav-navbar:not(.nav-inline) {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    flex-direction: column;
    flex-wrap: nowrap;
    width: 100%;
    left: 40px;
}
.navbar-expand-lg .nav-navbar:not(.nav-inline)>.nav-item>.nav-link, .navbar-expand-lg .nav-navbar:not(.nav-inline)>.nav-link {
    min-height: inherit;
    /* line-height: inherit; */
    padding-top: 0.75rem;
    padding-bottom: 0.75rem;
    font-size: 0.95rem;
    text-transform: none;
    line-height: 33px;
    font-size: 19px;
}
}
.p-t-0{
    padding-top:0px!important;
}


.p-cut{
    line-height: 29px !important;
    font-size: 19px !important;
    color: #000;
    padding: 22px;
}
.text-muted p{
    line-height: 29px !important;
    font-size: 19px !important;
    color: #000;
    padding: 0px;
}
/* .btn_theme_light {
    background-color: #5479ad !important;
    color: #fff;
} */
.text-blue{
    color:#5079ad;
}
.text-purple{
    color:#6e3951;
}
/* #site-footer {
    background-color: #34495d !important;
    background-color: #8aaae5 !important; } */

.skw-pages {
  overflow: hidden;
  position: relative;
  height: 70vh;
}

.skw-page {linear-gradient(to bottom,#5079ad 0,#597bad 101%)
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
}
.skw-page__half {
  position: absolute;
  top: 0;
  width: 50%;
  height: 70vh;
  transition: -webkit-transform 1s;
  transition: transform 1s;
  transition: transform 1s, -webkit-transform 1s;
}
.skw-page__half--left {
  left: 0;
  -webkit-transform: translate3d(-32.4vh, 100%, 0);
          transform: translate3d(-32.4vh, 100%, 0);
}
.skw-page__half--right {
  left: 50%;
  -webkit-transform: translate3d(32.4vh, -100%, 0);
          transform: translate3d(32.4vh, -100%, 0);
}
.skw-page.active .skw-page__half {
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
}
.skw-page__skewed {
  overflow: hidden;
  position: absolute;
  top: 0;
  width: 140%;
  height: 100%;
  -webkit-transform: skewX(-18deg);
          transform: skewX(-18deg);
  background: #000;
}
.skw-page__half--left .skw-page__skewed {
  left: -40%;
}
.skw-page__half--right .skw-page__skewed {
  right: -40%;
}
.skw-page__content {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column wrap;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  padding: 0 30%;
  color: #fff;
  -webkit-transform: skewX(18deg);
          transform: skewX(18deg);
  transition: opacity 1s, -webkit-transform 1s;
  transition: transform 1s, opacity 1s;
  transition: transform 1s, opacity 1s, -webkit-transform 1s;
  background-size: cover;
}
.skw-page__half--left .skw-page__content {
  padding-left: 30%;
  padding-right: 30%;
  -webkit-transform-origin: 100% 0;
          transform-origin: 100% 0;
}
.skw-page__half--right .skw-page__content {
  padding-left: 30%;
  padding-right: 30%;
  -webkit-transform-origin: 0 100%;
          transform-origin: 0 100%;
}
.skw-page.inactive .skw-page__content {
  opacity: 0.5;
  -webkit-transform: skewX(18deg) scale(0.95);
          transform: skewX(18deg) scale(0.95);
}
.skw-page__heading {
  margin-bottom: 15px;
  text-transform: uppercase;
  font-size: 25px;
  text-align: center;
  color:#fff;
}
.skw-page__description {
  font-size: 18px;
  text-align: center;
}
.skw-page__link {
  color: #FFA0A0;
}
.skw-page-1 .skw-page__half--left .skw-page__content {
  background-image: url("<?php echo base_url(); ?>assets/img/slide_1.jpeg");
}
.skw-page-1 .skw-page__half--right .skw-page__content {
  background: #292929;
}
.skw-page-2 .skw-page__half--left .skw-page__content {
  background: #292929;
}
.skw-page-2 .skw-page__half--right .skw-page__content {
  background-image: url("<?php echo base_url(); ?>assets/img/slide1.jpeg");
}
.skw-page-3 .skw-page__half--left .skw-page__content {
  background-image: url("<?php echo base_url(); ?>assets/img/slide2.jpeg");
}
.skw-page-3 .skw-page__half--right .skw-page__content {
  background: #292929;
}
.skw-page-4 .skw-page__half--left .skw-page__content {
  background: #292929;
}
.skw-page-4 .skw-page__half--right .skw-page__content {
  background-image: url("<?php echo base_url(); ?>assets/img/slide3.jpeg");
}
.skw-page-5 .skw-page__half--left .skw-page__content {
  background-image: url("<?php echo base_url(); ?>assets/img/slide4.jpeg");
}
.skw-page-5 .skw-page__half--right .skw-page__content {
  background: #292929;
}
.main-content{ 
  padding-top: 0px;  
}

</style>
     
  <!-- <section class="full-landing-section bg_lightblue full-landing-section-pb60" data-aos="fade-up" data-aos-duration="3000">
    <div class="full-landing-section-header text-color1">Explore <span class="styletext"> Who We Are !</span></div>  

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12">

                  <div class="explore_who_we_are_row">
                    
                      <div class="explore_who_we_are_row_col">
                        <div class="explore_who_we_are_row_col_inner">
                            <div class="explore_who_we_are_row_col_image"><img src="<?php echo base_url() ?>assets/images/icon/Group 44@2x.png"/></div>
                            <div class="explore_who_we_are_row_col_heading">Manage Easy</div>
                            <div class="explore_who_we_are_row_col_text text-color4">Houdin-e is the only software that lets you easily and most efficiently manage your website</div>
                        </div>
                      </div>

                      <div class="explore_who_we_are_row_col">
                        <div class="explore_who_we_are_row_col_inner">
                            <div class="explore_who_we_are_row_col_image"><img src="<?php echo base_url() ?>assets/images/icon/Group 45@2x.png"/></div>
                            <div class="explore_who_we_are_row_col_heading">Grow Higher</div>
                            <div class="explore_who_we_are_row_col_text text-color4">App and retail stores all under one software, which not only saves time but increases productivity</div>
                        </div>
                      </div>


                      <div class="explore_who_we_are_row_col">
                        <div class="explore_who_we_are_row_col_inner">
                            <div class="explore_who_we_are_row_col_image"><img src="<?php echo base_url() ?>assets/images/icon/Group 46@2x.png"/></div>
                            <div class="explore_who_we_are_row_col_heading">Sell More</div>
                            <div class="explore_who_we_are_row_col_text text-color4">If you have always wanted a dynamic shift in your business growth, Houdin-e is just the right solution for you</div>
                        </div>
                      </div>

                  </div>
              </div>
        </div>
    </div>
    
  </section> -->


  <section class="full-landing-section" data-aos="fade-down" data-aos-duration="3000">

      <div class="full-landing-section-header">  </div>

      <div class="container">
      <img src="<?php echo base_url() ?>assets/images/home_banner.png" alt="Mockup">
      </div>

  </section>

  <section class="full-landing-section bg_alternative">
         
         <div class="full-landing-section-header Houdin_take_your_business_header text-color1"><h6 class="highlight">E-commerce</h6>Take your business online with  <span class="styletext"> Houdin-e</span></div>  
           
           <div class="Houdin_take_your_business text-center">
               <p class="Houdin_take_your_business_text">Give your customers the power to shop the way they like</p>                    
           </div>

           <div class="Houdin_take_your_business text-center">
               <button class="btn_theme_bluelight btn-round  px-6 m-b-5 fw-700 btn-round2" style="font-family: 'Roboto',sans-serif;
                 letter-spacing: 1px;">TRY FOR FREE</button>
           </div>

           <div class="container">
             <img src="<?php echo base_url() ?>assets/images/All Device MockUp - 2.png" alt="Mockup">
           </div>

     </section>

        <!-- <section class="full-landing-section Houdin_take_your_business bg_alternative">
         
          <div class="full-landing-section-header Houdin_take_your_business_header text-color1"><h6 class="highlight">Business Grow</h6> Take your business online with  <span class="styletext"> Houdin-e</span></div>  
            
            <div class="Houdin_take_your_business text-center">
                <p class="Houdin_take_your_business_text">Give your customers the power to shop the way they like</p>                    
            </div>

            <div class="Houdin_take_your_business text-center">
                <button class="btn_theme_bluelight btn-round  px-6 m-b-5 fw-700 btn-round2" style="font-family: 'Roboto',sans-serif;
                  letter-spacing: 1px;">TRY FOR FREE</button>
            </div>

            <div class="container">
              <img src="<?php echo base_url() ?>assets/images/home_banner.png" alt="Mockup">
            </div>

      </section> -->


<section class="full-landing-section vertsatile--point">

  <div class="versatile--point-content">

    <div class="full-landing-section-header Houdin_take_your_business_header text-color5"><h6 class="highlight-2">Point of sale</h6>A simple, elegant and versatile point of <span class="styletext">sale</span></div> 

    <div class="Houdin_take_your_business text-center">
        <p class="Houdin_take_your_business_text-2 text-color5">Now Bill Your customers with ease with seamless pos</p>                    
    </div>

    <div class="Houdin_take_your_business text-center">
      <button class="btn_theme_bluelight btn-round  px-6 m-b-5 fw-700 btn-round2" style="font-family: 'Roboto',sans-serif;
        letter-spacing: 1px;">TRY FOR FREE</button>
    </div>

    </div>

    

    <!-- <div class="text-center">
            <p class="Houdin_omni_direction_text_section ">Houdin-e provides a cloud-based Point of sale system that doesn’t require any additional hardware.
            </p>     
            <p class="Houdin_omni_direction_text_section ">It works on your laptop, tablets or even mobile phone, so you can bill your customers at your convenience.  
            </p>         
        </div> -->

<!-- 
      <div>
          <img src="<?php echo base_url() ?>assets/images/POS.jpg" alt="Mockup">
      </div> -->

</section>


    <section class="full-landing-section bg_alternative">

    <div class="full-landing-section-header Houdin_take_your_business_header text-color1"><h6 class="highlight">Accounting</h6>Manage and track your accounts with our  <span class="styletext">online accounting software</span></div> 

    <div class="Houdin_take_your_business text-center">
        <p class="Houdin_take_your_business_text text-color1">Houdin-e Accounting Software is in-sync with your retail and  E-commerce sales Now Manage Your Complete account from One place</p>                    
    </div>

    <div class="container">
      <img src="<?php echo base_url() ?>assets/images/accounting Section image.png" alt="Mockup">
    </div>

    </section>


<!-- <section class="full-landing-section  bg_gray1 full-landing-section-pb60" data-aos="fade-up" data-aos-duration="3000">
      
      <div class="full-landing-section-header text-color1">Choose your <span class="styletext"> Perfect plan</span></div>  

      <div class="choose-plan-content-row">

          <div class="container">
                <div class="row">
                  <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="plans-enquiry-box" >
                            <div class="plans-enquiry-box-heading text-color1">TAILORED</div>
                            <div class="plans-enquiry-box-text">Houdin-e tailored just for you. Our team will contact to understand your requirements and deliver you the perfect solution for your business.</div>
                             <div class="text-center">
                              <a class="btn_theme_bluelight btn-round" href="<?php echo base_url() ?>home/contact">Send Enquiry</a>
                            </div>

                              <?php 
                                if($_COOKIE['tempUserPackageId'])
                                {
                                ?>
                                <p style="font-size: 18px;color: black;">
                                Your trial period has expired. But don't you worry! Upgrade now!</p>
                              <p style="font-size: 18px;color: black;">Have some doubts about whether you should continue? Get in touch with us now at support@houdine.com and let us help you find the best plan for you.</p>
                                <?php }
                                ?>

                        </div>

                        <div class="plans-box-col-row" >
                            <?php 
                              $countcheck = 0;
                              foreach($packageData as $packageDataList)
                              {
                               
                                $getMainArray = $packageDataList;
                                   if($getMainArray->houdin_package_name == 'Tailored')
                                  {
                                  }
                                  else{
                            ?>
                            <div class="plans-box-col">
                              <div class="text-color1 plans-box-col-heading">
                                <?php echo $getMainArray->houdin_package_name ?></div>
                               <div class="text-color1 plans-box-col-price"><sup>₹</sup><?php echo  $getMainArray->houdin_package_price;  ?></div>
                              <div class="text-color2 plans-box-col-tagling text-color2">Billed Yearly</div>
                              <div class="plans-box-col-heading-name ">ENJOY ALL THE FEATURES</div>
                              <ul class="plans-box-col-list text-color2">
                                  <li><?php echo $getMainArray->houdin_package_product ?> Products</li>
                                  <li><?php echo $getMainArray->houdin_package_staff_accounts ?> Staff Accounts</li>
                                  <li><?php echo $getMainArray->houdin_package_stock_location ?> Stock Location</li>
                                  <li><?php echo $getMainArray->houdin_package_app_access ?> App Access</li>
                                  <li><?php echo $getMainArray->houdin_package_order_pm ?> Order Per Month</li>
                                  <li><?php echo $getMainArray->houdin_package_templates ?> Templates</li>
                                  <li><?php echo $getMainArray->houdin_package_sms ?> Free SMS</li>
                                  <li> <?php echo $getMainArray->houdin_package_email ?> Free Email</li>
                                  <?php if($getMainArray->houdin_package_accounting == 'yes') { ?>
                                  <li> Accounting Integration</li>
                                  <?php } ?>
                                  <?php if($getMainArray->houdin_package_pos == 'yes') {?>
                                    <li> POS Integration</li>
                                    <?php } ?>
                                 <?php if($getMainArray->houdin_package_delivery == 'yes') { ?> <li>Delivery Integration</li><?php } ?>
                                <?php if($getMainArray->houdin_package_payment_gateway == 'yes') { ?>  <li>Payment Gateway Integration</li> 
                              <?php }?>

  
                              </ul>

                              <div class="plans-box-col-action-bottom text-center" >
                              <a class="btn_theme_bluelight btn-round" data-toggle="modal" data-target="#user-register" href="javascript:;">Star Free Trial</a>
                            </div> 
                            </div>

                          <?php }
                           if($countcheck==0){
                             $countcheck=1;
                              
                          ?>
                           
                              <div class="plans-box-adsbox hidden-xs"><img src="<?php echo base_url() ?>assets/images/GrowYourBusiness.png"/></div>

                            <?php } ?>
  

                          <?php } ?>

                        </div>

                    </div>
                    <div class="col-md-2"></div>
                </div>

            </div> 



      </div> <!-- end section content -->


<!-- </section> -->







<?php /*

 <section class="section py-8" style="background:linear-gradient(to bottom,#5079ad 0,#597bad 101%)">
          <h1 style="text-align:center;color:#fff">Explore Who We Are !</h1>
        <div class="container" style="background-color:#fff;padding: 40px;">
       
           <div class="row"> 

            <div class="col-12 col-md-6 text-center " >
              <div class="text-about text_black  font600">Manage</div> 
              <div class="text-about text_black  font600 text-blue">Sell</div>
              <div class="text-about text_black  font600 text-purple">Grow</div>
<div class="text-about text_black  font600"> 
 
   <!--<span><sell style="color:#5079ad">sell</label><br/> <grow style="color:#6e3951">grow</label></span>-->
</div>
            </div>
        
         <div class="col-12 col-md-6 pb-70 align-self-center">
          
          <p class="lead-2 custom_lead_p text_black mt-20">Houdin-e is the only software that lets you easily and most efficiently manage your website, app and retail stores all under one software, which not only saves time but increases productivity and innovation.
If you have always wanted a dynamic shift in your business growth and hesitated due to technical complexities or budget factors, Houdin-e is just the right solution for you.
The world is going digital, allow us to help you to stay ahead.
 <br>
<b>Manage Easy, Sell More, Grow Higher.</b> </p>

        <!--<a class="btn btn-xl btn-round btn-second w-200 mr-3 px-6 d-none d-md-inline-block" href="" data-scrollto="section-intro">Get Started</a>-->

      


        </div>
          </div>
    

        </div>
      </section>

<section class="section text-white py-8 p-t-0">
<img src="assets/img/Pure White.jpg">
        
            </section>
           
            
 
      <div class="clearfix"></div> 
	  <section class="section py-8" style="background-image:url('<?php echo base_url(); ?>assets/img/Devil in Light Purple.jpg');background-size: contain;">
        <div class="container">
          <header class="section-header"> 
            <h2 class="font600 text_black font46">Choose your perfect plan</h2>
             
          </header> 


          <div class="row gap-y text-center">
              
            <?php 
            $i = 1;
            foreach($packageData as $packageDataList)
            {
              $getMainArray = $packageDataList;
              if($getMainArray->houdin_package_name == 'Tailored')
              {
              ?>
              <div class="col-md-<?php if(count($packageData) == 3) { echo "4"; } else if(count($packageData) == 4) { echo "3"; } ?>">
                  
              <div class="pricing-1 btn_scroll_down <?php if($i == 2){ echo "pricing-2-transform"; } ?>">
                <p class="plan-name"><?php echo $getMainArray->houdin_package_name ?></p>
                <br>
                <!-- <h2 class="price"> <span class="price-unit">&#8377;</span>
                  <span data-bind-radio="pricing-1"><?php //echo $getMainArray->houdin_package_price ?></span>
				  </h2>
               <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">Yearly</span>
                </p> -->
                <div class="custom-min-height">   
                <div class="text-muted">
                  <p class="mb-1"> <?php echo $getMainArray->houdin_package_description ?></p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                </div>
                </div>

                <br>
                <br>
                <br>
                <br>
                <br>
                 
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="<?php echo base_url() ?>home/contact">Contact Us</a></p>
              </div>
            </div>
              <?php }
              else
              {
              ?>
                <div class="col-md-<?php if(count($packageData) == 3) { echo "4"; } else if(count($packageData) == 4) { echo "3"; } ?>">
              <div class="pricing-1 btn_scroll_down <?php if($i == 2){ echo "pricing-2-transform"; } ?>">
                <p class="plan-name"><?php echo $getMainArray->houdin_package_name ?></p>
                <br>
                <h2 class="price"> <span class="price-unit">&#8377;</span>
                  <span data-bind-radio="pricing-1"><?php echo $getMainArray->houdin_package_price ?></span>
				  </h2>
               <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">Yearly</span>
                </p>
                <div class="custom-min-height">   
                <div class="text-muted">
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_product ?> Products</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_staff_accounts ?> Staff Accounts</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_stock_location ?> Stock Location</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_app_access ?> App Access</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_order_pm ?> Order Per Month</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_templates ?> Templates</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_sms ?> Free SMS</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_email ?> Free Email</p>
                  <?php if($getMainArray->houdin_package_accounting == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Accounting Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_pos == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> POS Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_delivery == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Delivery Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_payment_gateway == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Payment Gateway Integration</p><?php } ?>
                </div>
                </div>

               
                <?php 
                if($_COOKIE['tempUserPackageId'])
                {
                ?>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="<?php echo base_url() ?>Payumoney/<?php echo $getMainArray->houdin_package_id ?>/<?php echo $getMainArray->houdin_package_name ?>/<?php echo $getMainArray->houdin_package_price ?>">Upgrade Your Package</a></p>
                <?php }
                else
                {
                ?>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" data-toggle="modal" data-target="#user-register" href="javascript:;">Start Free Trial</a></p>
                <?php }
                ?>
              </div>
            </div>
              <?php }
            ?>
            <?php $i++; }  
            ?>
            


            <!-- <div class="col-md-4">
              <div class="pricing-1 popular">
                <p class="plan-name">Gold</p>
                <br>
                <h2 class="price text-second">
                  <span class="price-unit">$</span>
                  <span data-bind-radio="pricing-1">39</span>
                </h2>
                <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">monthly</span>
                </p>

                <div class="text-muted">
					
                 <p class="mb-1"><i class="ti-check text-second mr-2"></i> Enabled International Payments</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i> 50+ beautiful themes</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i> 25+ premium themes</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i>Payment enabled Facebook store</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i> Wholesale pricing. Wishlist</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i> 30-Day Onboarding support</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i>  Superfast website (CDN).</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i> CRM, Marketing tools</p>
                  
                </div>

                <br>
                <p class="text-center py-3"><a class="btn btn-second btn-round" href="">Get started</a></p>
              </div>
            </div> -->


            <!-- <div class="col-md-4">
              <div class="pricing-1">
                <p class="plan-name">PLATINUM</p>
                <br>
                <h2 class="price">
                  <span class="price-unit">$</span>
                  <span data-bind-radio="pricing-1">69</span>
                </h2>
                <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">monthly</span>
                </p>

                <div class="text-muted">
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> Gold Plan with iOS app and Android app</p>
				<p class="mb-1"><i class="ti-check text-three mr-2"></i> Personal eCommerce consultant</p>
				<p class="mb-1"><i class="ti-check text-three mr-2"></i>Store setup cost included</p>
				<p class="mb-1"><i class="ti-check text-three mr-2"></i>  Coupon code promotions</p>
				<p class="mb-1"><i class="ti-check text-three mr-2"></i>   Digital marketing</p>
				<p class="mb-1"><i class="ti-check text-three mr-2"></i>  Integrated SSL</p> 
                </div>

                <br>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="">Get started</a></p>
              </div>
            </div> -->

          </div>


        </div>
      </section>   */ ?>




     
     
      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | CTA
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      
      <section class="section text-white py-8">
        <canvas class="overlay opacity-95" data-granim="#667eea,#764ba2,#00cdac,#3cba92"></canvas>

        <div class="container">
          <header class="section-header">
            <small><strong>Own it</strong></small>
            <h2 class="display-3 fw-400">Get It <span class="mark-underline">Now</span></h2>
            <hr>
            <p class="lead-2">If you have made your decision to purchase this template, go ahead and press on the following button to get a license in less than a minute.</p>
          </header>

          <p class="text-center">
            <a class="btn btn-xl btn-round btn-light w-250" href="">Purchase for <span class="pl-2">$19</span></a><br>
            <small><a class="text-white opacity-80" href="">or purchase an Extended License</a></small>
          </p>
        </div>
      </section>!-->

<?php  $this->load->view('Template/footer');?>
<script>
    $(document).ready(function() {

      if($(window).width()>767)
      {
        var height = $(window).height()-$('nav.navbar-top').height(); 
       $('.home-main-banner').css({'height':height});
      }

  var curPage = 1;
  var numOfPages = $(".skw-page").length;
  var animTime = 1000;
  var scrolling = false;
  var pgPrefix = ".skw-page-";

  function pagination() {
    scrolling = true;

    $(pgPrefix + curPage).removeClass("inactive").addClass("active");
    $(pgPrefix + (curPage - 1)).addClass("inactive");
    $(pgPrefix + (curPage + 1)).removeClass("active");

    setTimeout(function() {
      scrolling = false;
    }, animTime);
  };

  function navigateUp() {
    if (curPage === 1) return;
    curPage--;
    pagination();
  };

  function navigateDown() {
    if (curPage === numOfPages) return;
    curPage++;
    pagination();
  };

  $(document).on("mousewheel DOMMouseScroll", function(e) {
    if (scrolling) return;
    if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
      navigateUp();
    } else { 
      navigateDown();
    }
  });

  $(document).on("keydown", function(e) {
    if (scrolling) return;
    if (e.which === 38) {
      navigateUp();
    } else if (e.which === 40) {
      navigateDown();
    }
  });

});
</script>