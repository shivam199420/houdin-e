<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');

$images_all = json_decode($product['main_product']->houdinv_products_main_images);
$price_all = json_decode($product['main_product']->houdin_products_price,true);
$final_price = $product['main_product']->houdin_products_final_price;
$overall_rating = $product['reviews_sum']->houdinv_product_review_rating;
$total_rating = count($product['reviews']);
$final_rating = $overall_rating/$total_rating;
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<style>
    .checked {
    color: orange;
}

</style>
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">View Product</h2>

            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- single product area start -->
    <div class="single-product-area ptb-100">
        <div class="container">
            <div class="row">
            <?php
            if($this->session->flashdata('error'))
            {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
            }
            if($this->session->flashdata('success'))
            {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
            }
            ?>
                <div class="col-md-5">
                    <div class="product-details-tab">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <?php
                            $i = 1;
                            for($index = 0; $index < count($images_all); $index++)
                            {
                                if($images_all [$index])
                                {
                            ?>
                            <div class="tab-pane <?php if($i == 1) { echo "active"; } ?>" id="product<?php echo $i; ?>">
                                <div class="large-img">
                                    <img src="<?php echo $this->session->userdata('vendorUrl') ?>/vendor/upload/productImage/<?php echo $images_all [$index] ?>" alt="" />
                                </div>
                            </div>
                            <?php $i++; } }
                            ?>
                        </div>
                        <!-- Nav tabs -->
                        <div class="details-tab owl-carousel">
                            <?php
                            $j = 1;
                            for($indexthumb = 0; $indexthumb < count($images_all); $indexthumb++)
                            {
                                if($images_all [$indexthumb])
                                {
                            ?>
                            <div <?php if($j == 1) {  ?>class="active" <?php } ?>><a href="#product<?php echo $j; ?>" data-toggle="tab"><img src="../../../houdin-e/vendor/upload/productImage/<?php echo $images_all [$indexthumb] ?>" alt="" /></a></div>
                            <?php $j++; } } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="single-product-content">
                        <div class="single-product-dec pb-30  for-pro-border">
                            <h2><?php echo $product['main_product']->houdin_products_title; ?></h2>
                            <span class="ratting">
                            <?php
                            for($i=1 ; $i<6 ;$i++)
                            {
                                if($i <= $final_rating)
                                {
                                    $posi = strpos($final_rating,".");
                                    if($posi>=0)
                                    {
                                        $exp =  explode(".",$final_rating);
                                        if($exp[0]==$i)
                                        {
                            ?>
                            <i class="fa fa-star-half-o"></i>
                            <?php
                            }
                            else
                            {
                                echo '<i class="fa fa-star"></i>';
                            }
                            }
                            else
                            {
                            ?>
                            <i class="fa fa-star"></i>
                            <?php
                            }
                            }
                            else
                            {
                                ?>
                                <i class="fa fa-star-o"></i>
                              <?php } } echo "( ".$total_rating." )"; ?>

                                </span>
                                <?php
                                if($final_price)
                                {
                                ?>
                                <h3><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $final_price; ?>&nbsp;&nbsp;<strike><span><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $price_all['sale_price']; ?></span></strike></h3>
                                <?php }
                                else
                                {
                                ?>
                                <h3><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $final_price; ?></h3>
                                <?php }
                                ?>

                            <p><?php echo $product['main_product']->houdin_products_short_desc; ?></p>
                        </div>
                        <div class="single-cart-color for-pro-border">
                            <?php
                            if($product['main_product']->houdinv_products_total_stocks > 0)
                            {
                                $setStock = "in stock";
                            }
                            else
                            {
                                $setStock = "out of stock";
                            }
                            ?>
                            <p>availability : <span><?php echo $setStock; ?></span></p>
                            <?php if(!empty($product['related'])){?> 
                            <select style="margin-bottom: 36px;width: 50%;" title="Pick a number" class="form-control variantsselect">
                            <option>Select variants </option>
                            <?php 
                            foreach($product['related'] as $related){?>
                            <option value="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>"><?php echo $related->houdinv_products_variants_name; ?></option>
                            <?php } ?>
                            </select>
                            <?php  }?>


                            <div class="pro-quality pd_qntty_area">
                                <p>Quantity:</p>
                                <div class="pd_qty">
                                <input value="1" min="1" data-max="<?php echo $product['main_product']->houdinv_products_total_stocks ?>" name="qttybutton" class="cart-plus-minus-box setCartQunatity" type="number">
                                </div>

                            </div>
                            <div class="single-pro-cart">
                              <?php
                              if($product['setcartStatus'] == 'yes')
                              {
                                ?>
                                <a href="javascript:;" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" title="Add to Cart">
                                        <i class="pe-7s-cart"></i>
                                        Already in cart
                                    </a>
                              <?php }
                              else {
                                ?>
                                <a href="javascript:;" class="Add_to_cart_button" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" title="Add to Cart">
                                        <i class="pe-7s-cart"></i>
                                        Add to cart
                                    </a>
                              <?php }
                              ?>
                              <?php
                              if($product['setcartStatus'] == 'yes')
                              {
                                ?>
                                <a href="javascript:;" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" title="wishlist">
                                        <i class="pe-7s-like"></i>
                                        Already in wishlist
                                    </a>
                              <?php }
                              else {
                                ?>
                                <a href="javascript:;" class="Add_to_whishlist_button" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" title="wishlist">
                                        <i class="pe-7s-like"></i>
                                        Add to wishlist
                                    </a>
                              <?php }
                              ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- single product area end -->
    <!-- single product area end -->
    <div class="single-product-dec-area">
        <div class="container">
            <div class="single-product-dec-tab">
                <ul role="tablist">
                    <li class="active">
                        <a href="#description" data-toggle="tab">
                            description
                        </a>
                    </li>
                    <li>
                        <a href="#reviews" data-toggle="tab">
                            reviews
                        </a>
                    </li>
                </ul>
            </div>
            <div class="single-product-dec pb-100">
                <div class="tab-content">
                    <div class="tab-pane active" id="description">
                    <?php echo $product['main_product']->houdin_products_desc; ?>
                    </div>
                    <div class="tab-pane" id="reviews">
                        <div class="customer-reviews-all">
                            <div class="row">
                                <div class="col-md-6">
                                <?php
                                if(!$final_rating > 0)
                                {
                                ?>
                                <p>There are no reviews yet.</p>
                                    <p class="text-bigger">Be the first to review</p>
                                <?php }
                                else
                                {
                                    foreach($product['reviews'] as $user_Review)
                                      {
                                ?>
                                 <div style="margin-bottom:10px">
                                          <div class=""><?php echo $user_Review->houdinv_product_review_user_name ?></div>


                                          <div class="strat-rating">
                                          <?php for($i=1 ; $i<6 ;$i++)
                                          {
                                          if($i <= $user_Review->houdinv_product_review_rating)
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star checked"></span>
                                          <?php }
                                          else
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star"></span>
                                          <?php } } ?>

                                            <span class="text-date"><?php echo $user_Review->houdinv_product_review_created_at ?></span>
                                          </div>


                                          <div class=""><?php echo  $user_Review->houdinv_product_review_message; ?></div>
                                        </div>

                                <?php } }
                                ?>


                                </div>
                                <div class="col-md-6">
                                    <div class="form-add table-responsive">
                                    <?php echo form_open(base_url( 'Productlistview' ), array( 'id' => 'rtng_form',"class"=>"checkout_form", 'method'=>'post' ));?>
                                        <div class="form-group">

                                        <div class="strat-rating">
                                            <span data-rating="1" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="2" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="3" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="4" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="5" style="font-size: 22px;" class="fa fa-star"></span>
                                            <input type="hidden"  name="rating" class="rating_data" value="0" />
                                            <input type="hidden"  name="product_id" class="product" value="<?php echo $product_id; ?>" />
                                            <input type="hidden"  name="variant_id" class="" value="0" />
                                        </div>
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation form-control" type="text" name="user_name" placeholder="Your Name" />
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation email_validation form-control" type="text" name="user_email" placeholder="Your Email" />
                                        </div>
                                        <div class="form-group">

                                        <textarea  class="required_validation_for_review form-control" name="user_message" rows="3" placeholder="Write a review"></textarea>
                                        </div>
                                        <input class="btn-round submit_form_review" type="submit" name="review_submit" value="Add Review" />
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if(count($product['related']) > 0)
            {
            ?>
            <div class="features-tab pb-100">
                <div class="home-2-tab">
                    <ul role="tablist">
                        <li class="active">
                            <a href="#dresses" data-toggle="tab">
                                    related products
                                </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="dresses">
                        <div class="row">
                            <div class="product-curosel product-curosel-style owl-carousel">
                            <?php
                            foreach($product['related'] as $related)
                            {

                            ?>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="single-shop">
                                        <div class="shop-img">
                                            <a href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>">
                                            <?php 
                                            if($related->houdin_products_variants_image)
                                            {
                                                $setVariantIMageData = $this->session->userdata('vendorURL')."upload/productImage/".$related->houdin_products_variants_image;
                                            }
                                            else
                                            {
                                                $setVariantIMageData = base_url()."Extra/noPhotoFound.png";
                                            }
                                            ?>
                                            <img src="<?php echo $setVariantIMageData; ?>" alt="" /></a>

                                            <div class="button-group">
                                                <a href="javascript:;" title="Add to Cart" class="Add_to_cart_button" data-variant="<?php echo $related->houdin_products_variants_id ?>"  data-cart="0">
                                                    <i class="pe-7s-cart"></i>
                                                </a>

                                                <a href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>" title="Quick View">
                                                    <i class="pe-7s-look"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="shop-text-all">
                                            <div class="title-color fix">
                                                <div class="shop-title f-left">
                                                    <h3><a href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>"><?php echo $related->houdin_products_variants_title ?></a></h3>
                                                </div>
                                                <div class="price f-right">
                                                    <span class="new"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $related->houdinv_products_variants_final_price; ?> </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }  ?>


                            </div>
            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>

    <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('keyup','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        })
        $(document).on('change','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        });

        var image = '<?php echo $setImageData; ?>';
        // console.log(image)
    !function(e)
    {
        "use strict";
        jQuery("#slide100-01").slide100({autoPlay:"false",timeAuto:3e3,deLay:400,linkIMG:[image],linkThumb:[image]})}();

    })
    </script>
