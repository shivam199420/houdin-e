<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <div class="btn-group pull-right m-t-15">
                                    <button type="button" class="btn btn-default btn-sm m-l-5" data-toggle="modal" data-target="#addTemplateModal"> <i class="fa fa-plus m-r-5"></i>Add Template</button>
                                </div>
                                <h4 class="page-title">Template Management</h4>

                            </div>
                        </div>

                        <div class="row">
                        <div class="col-sm-12">
                        <?php 
                        if($this->session->flashdata('success'))
                        {
                            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                        }
                        if($this->session->flashdata('error'))
                        {
                            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                        }
                        ?>
                        </div>
                        </div>
                        <div class="row">
                <div class="col-md-6 col-lg-4">
                  <div class="widget-panel widget-style-2 bg-light">
                      <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Template.png">
                      <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($totalTemplate) { echo $totalTemplate; } else { echo 0; } ?></b></h2>
                      <div class="text-muted m-t-5 custom_width_sm">Total Template</div>
                      <div class="clearfix"></div>
                  </div>
                </div>

                <div class="col-md-6 col-lg-4">
                  <div class="widget-panel widget-style-2 bg-light">
                      <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Template.png">
                      <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($activeTemplate) { echo $activeTemplate; } else { echo 0; } ?></b></h2>
                      <div class="text-muted m-t-5 custom_width_sm">Total Active Temaplate</div>
                      <div class="clearfix"></div>
                  </div>
                </div>

                 <div class="col-md-6 col-lg-4">
                  <div class="widget-panel widget-style-2 bg-light">
                      <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Template.png">
                      <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($deactivateTemplate) { echo $deactivateTemplate; } else { echo 0; } ?></b></h2>
                      <div class="text-muted m-t-5 custom_width_sm">Total Deactive Template</div>
                      <div class="clearfix"></div>
                  </div>
                </div>
            </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card-box table-responsive">
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th>Template Name</th>
                    <th>Directory Name</th>
                    <th>Cataogry</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                foreach($templateList as $templateListdata)
                {
                 ?>   
                 <tr>
                    <td><?php echo $templateListdata->houdin_template_name ?></td>
                    <td><?php echo $templateListdata->houdin_template_file ?></td>
                    <td><?php echo $templateListdata->houdin_business_category_name ?></td>
                    <td>
                    <?php 
                    if($templateListdata->houdin_template_status == 'active')
                    {
                        $setText = 'Active';
                        $setClass = 'success';
                    }
                    else if($templateListdata->houdin_template_status == 'deactive')
                    {
                        $setText = 'Deactive';
                        $setClass = 'warning';
                    }
                    ?>
                    <button type="button" class="btn btn-<?php echo $setClass ?> btn btn-xs"><?php echo $setText ?></button></td>
                    <td>
                        <button type="button" data-status="<?php echo $templateListdata->houdin_template_status ?>" data-description="<?php echo $templateListdata->houdin_template_description ?>" data-category="<?php echo $templateListdata->houdin_template_category ?>" data-file="<?php echo $templateListdata->houdin_template_file ?>" data-name="<?php echo $templateListdata->houdin_template_name ?>" data-id="<?php echo $templateListdata->houdin_template_id ?>"  class="btn btn-warning btn btn-sm editTemplateBtnData"><i class="fa fa-pencil m-r-5"></i>Edit</button>
                        <button type="button" data-id="<?php echo $templateListdata->houdin_template_id ?>" class="btn btn-warning btn btn-sm deleteTemplateDataBtn"><i class="fa fa-trash m-r-5"></i>Delete</button>
                    </td>
                  </tr>
                <?php }
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
                    </div> <!-- container -->

                </div> <!-- content -->

                <div id="addTemplateModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                      <?php echo form_open(base_url( 'Setting/template' ),array( 'id' => 'addTemplateForm','class'=>'m-t-30','method'=>'post','enctype'=>'multipart/form-data'));?>
                            <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                  <h4 class="modal-title">Add Template</h4>
                                </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Template Name</label>
                                            <input type="text" class="form-control required_validation_for_add_template name_validation" name="addTemplateName" placeholder="Template Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Cataogry</label>
                                            <select class="form-control required_validation_for_add_template" name="addTemplateCategory">
                                            <option value="">Choose Category</option>
                                              <?php 
                                              foreach($businessCaegory as $businessCategoryData)
                                              { 
                                              ?>    
                                              <option value="<?php echo $businessCategoryData->houdin_business_category_id ?>"><?php echo $businessCategoryData->houdin_business_category_name ?></option>
                                              <?php }
                                              ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Directory Name</label>
                                            <input type="text" class="form-control required_validation_for_add_template name_validation" name="addTemplateDirectoryUrl" placeholder="Directory Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Description</label>
                                            <textarea rows="4" class="form-control required_validation_for_add_template name_validation" name="addTemplateDescription"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Template Thumbnail</label>
                                            <input type="file" name="name" class="form-control required_validation_for_add_template"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Status</label>
                                            <select class="form-control required_validation_for_add_template" name="addTemplateStatus">
                                              <option value="">Choose Status</option>
                                              <option value="active">Active</option>
                                              <option value="deactive">Deactive</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                              </div>

                          <div class="modal-footer">
                              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                              <input type="submit" class="btn btn-info" name="addTemplateDataBtn" value="Add Template">
                          </div>

                     </div>
                  <?php echo form_close(); ?>
                 </div>
                </div>
    <!-- Edit template modal -->
                <div id="editTempateModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                      <?php echo form_open(base_url( 'Setting/template' ),array( 'id' => 'editTemplateForm','class'=>'m-t-30','method'=>'post','enctype'=>'multipart/form-data'));?>
                            <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                  <h4 class="modal-title">Edit Template</h4>
                                </div>
                            <div class="modal-body">
                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Template Name</label>
                                            <input type="hidden" class="editTemplateId" name="editTemplateId"/>
                                            <input type="text" class="form-control required_validation_for_edit_template name_validation editTemplateName" name="editTemplateName" placeholder="Template Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Cataogry</label>
                                            <select class="form-control required_validation_for_edit_template editTemplateCategory" name="editTemplateCategory">
                                            <option value="">Choose Category</option>
                                              <?php 
                                              foreach($businessCaegory as $businessCategoryData)
                                              { 
                                              ?>    
                                              <option value="<?php echo $businessCategoryData->houdin_business_category_id ?>"><?php echo $businessCategoryData->houdin_business_category_name ?></option>
                                              <?php }
                                              ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Directory Name</label>
                                            <input type="text" class="form-control required_validation_for_edit_template name_validation editTemplateDirectoryUrl" name="editTemplateDirectoryUrl" placeholder="Directory Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Description</label>
                                            <textarea rows="4" class="form-control required_validation_for_edit_template name_validation editTemplateDescription" name="editTemplateDescription"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Template Thumbnail</label>
                                            <input type="file" name="name" class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Status</label>
                                            <select class="form-control required_validation_for_edit_template editTemplateStatus" name="editTemplateStatus">
                                              <option value="">Choose Status</option>
                                              <option value="active">Active</option>
                                              <option value="deactive">Deactive</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                              </div>

                          <div class="modal-footer">
                              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                              <input type="submit" class="btn btn-info" name="updateTemplateData" value="Submit">
                          </div>

                     </div>
                  <?php echo form_close(); ?>
                 </div>
                </div>
  <div id="deleteTemplateModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog">
  <?php echo form_open(base_url( 'Setting/template' ),array('class'=>'m-t-30','method'=>'post'));?>
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
  <h4 class="modal-title">Delete Entry</h4>
  </div>
  <div class="modal-body">
  <div class="row">
  <div class="col-md-12">
  <input type="hidden" class="deleteTemplateId" name="deleteTemplateId"/>
  <h4><b>Do you really want to delete this entry ?</b></h4>
  </div>
  </div>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
  <input type="submit" class="btn btn-info" name="deleteTemplateBtn" value="Delete">
  </div>
  </div>
  <?php echo form_close(); ?>
  </div>
  </div>
  <?php $this->load->view('Template/footer.php') ?>
  <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addTemplateForm',function(){
			var check_required_field='';
			$(".required_validation_for_add_template").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>
    <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#editTemplateForm',function(){
			var check_required_field='';
			$(".required_validation_for_edit_template").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>