        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <style>
        .modal-ku {
  width: 750px;
  margin: auto;
}
        </style>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">


        <!--START Page-Title -->
        <div class="row">
  
        <div class="col-md-8">
        <h4 class="page-title">Stock Transfer</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>inventory/Inventorylanding">Inventory</a></li>
        <li class="active">Stock Transfer</li>
        </ol>
        </div>
        <!-- <div class="col-md-4">
        <form role="search" class="navbar-left app-search pull-left custom_search_all">
        <input type="text" placeholder="Search..." class="form-control">
        <a href=""><i class="fa fa-search"></i></a>
        </form></div> -->

        </div>
        <!--END Page-Title -->
        <div class="row m-t-20">
        <div class="col-md-12">
        <?php 
        if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        <div class="card-box table-responsive">
        <div class="btn-group pull-right m-t-10 m-b-20">
         
        <a href="javascript:;" class="btn btn-default m-r-5 stockTransferBtn_new" title="Add Stock Transfer"><i class="fa fa-plus"></i></a>




        </div>
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th>SNo.</th>
        <th>Invoice No</th>
        <th>Delivered Name</th>
        <th>Delivered Date</th>
        <th>Received Name</th>
        <th>Received Date</th>
         
        </tr>

        </thead>
        <tbody>
        <?php 
        $m=1;
        foreach($productList as $productListData)
        {
            $log_productdetails=  json_decode($productListData->houdinv_stock_transfer_log_productdetails,true);
            $log_productdetails=$log_productdetails[0];
            
        ?>
        <tr>
        <td><?=$m;?></td>
        <td><?=$log_productdetails['invoiceno'];?></td>
        <td><?=$log_productdetails['Delivered_name'];?></td>
        <td><?=$log_productdetails['Delivered_date'];?></td>
        <td><?=$log_productdetails['Received_name'];?></td>
        <td><?=$log_productdetails['Received_date'];?></td>
       </tr>
        <?php $m++; }
        ?>
        </tbody>
        </table>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div id="stockTransferModal" class="modal fade modal-ku" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <div class="modal-content"> 
        <?php echo form_open(base_url( 'Inventory/sendStockTransferData' ), array( 'id' => 'stockTransferForm', 'method'=>'post' ));?>
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Transfer</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group setMessageData">

        </div>
        <div class="form-group col-md-6">
        <label for="userName"> From</label>
        <select class="form-control required_validation_for_stock_transfer select2 setProductId" name="warehouseFrom">
        <option value="">Choose warehouse</option>
        <?php
        foreach($warehouselist as $warehouselistData)
        {
        ?>
        <option value="<?php echo $warehouselistData->id ?>"><?php echo $warehouselistData->w_name ?></option>
        <?php }
        ?>
        </select>
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">To</label>
        <select class="form-control  required_validation_for_stock_transfer select2 setWarehouseTo" name="warehouseTo" disabled="disabled">
        <option value="">Choose warehouse</option>
        <?php
        foreach($warehouselist as $warehouselistData)
        {
        ?>
        <option value="<?php echo $warehouselistData->id ?>"><?php echo $warehouselistData->w_name ?></option>
        <?php }
        ?>
        </select>
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Invoice no.</label>
        <input type="text" class="form-control required_validation_for_stock_transfer" name="invoiceno">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Date:</label>
        <input type="date" class="form-control required_validation_for_stock_transfer" name="date">
        </div> 
        <div class="form-group">
        <table class="table">
        <thead>
        <tr>
        <th><input type="checkbox" class="masterStockCheck"/></th>
        <th>Product Name</th>
        <th>Quantity</th>
        </tr>
        </thead>
        <tbody class="setStockData">
         
        </tbody>
        </table>
        <p class="h4 mb-4">Delivered by details:</p>

        <div class="form-group col-md-6">
        <label for="userName">Name</label>
        <input type="text" class="form-control required_validation_for_stock_transfer" name="Delivered_name">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Date:</label>
        <input type="date" class="form-control required_validation_for_stock_transfer" name="Delivered_date">
        </div> 

        <div class="form-group col-md-6">
        <label for="userName">Driver Name</label>
        <input type="text" class="form-control required_validation_for_stock_transfer" name="Delivered_drivername">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Vehicle Number</label>
        <input type="text" class="form-control required_validation_for_stock_transfer" name="Delivered_vehiclenumber">
        </div>
        <p class="h4 mb-4">Received by details:</p>

        <div class="form-group col-md-6">
        <label for="userName">Name</label>
        <input type="text" class="form-control required_validation_for_stock_transfer" name="Received_name">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Date:</label>
        <input type="date" class="form-control required_validation_for_stock_transfer" name="Received_date">
        </div> 

        </div> 
        </div> 

        </div> 

        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <button type="submit" class="btn btn-info waves-effect waves-light transferData" disabled="disabled">Transfer</button> 
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- /.modal -->


        <div id="stockTransferModal_new" class="modal fade modal-ku" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <div class="modal-content"> 
        <?php echo form_open(base_url( 'Inventory/sendStockTransfersData' ), array( 'id' => 'stockTransferForm_new', 'method'=>'post' ));?>
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Transfer</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group setMessageData">

        </div>
        <div class="form-group col-md-6">
        <label for="userName"> From</label>
        <select class="form-control required_validation_for_stock_transfer1 select2 warehouse_data" name="warehouseFrom">
        <option value="">Choose warehouse</option>
        <?php
        foreach($warehouselist as $warehouselistData)
        {
        ?>
        <option value="<?php echo $warehouselistData->id ?>"><?php echo $warehouselistData->w_name ?></option>
        <?php }
        ?>
        </select>
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">To</label>
        <select class="form-control  required_validation_for_stock_transfer1 select2 setWarehouseTo" name="warehouseTo" >
        <option value="">Choose warehouse</option>
        <?php
        foreach($warehouselist as $warehouselistData)
        {
        ?>
        <option value="<?php echo $warehouselistData->id ?>"><?php echo $warehouselistData->w_name ?></option>
        <?php }
        ?>
        </select>
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Invoice no.</label>
        <input type="text" class="form-control required_validation_for_stock_transfer1" name="invoiceno">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Date:</label>
        <input type="date" class="form-control required_validation_for_stock_transfer1" name="date">
        </div> 
        <div class="form-group">
        <table class="table">
        <thead>
        <tr>
         
        <th>Product Name</th>
        <th>Quantity</th>
        </tr>
        </thead>
        <tbody class="setRowData">
        <tr class="mainParentRow">
        <td>
            <div class="form-group" style="margin-bottom: 0px!important;">
       
            <select class="form-control setproductData select1" name="Allproduct_idandvarints[]" tabindex="14">
            <option value="">Choose Product</option>
             
        </select>

              </div>
    </td> <td><input type="text" value="" class="form-control getQuantity number_validation" name="quantity[]" placeholder=""></td>
        <tr>
         
        </tbody>
        </table>
        <div class="pull-right">
    <button type="button" class="btn btn-default addnewproductRow" style="border-radius: 14px;">Add New Product</button>
</div>

        <p class="h4 mb-4">Delivered by details:</p>

        <div class="form-group col-md-6">
        <label for="userName">Name</label>
        <input type="text" class="form-control required_validation_for_stock_transfer1" name="Delivered_name">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Date:</label>
        <input type="date" class="form-control required_validation_for_stock_transfer1" name="Delivered_date">
        </div> 

        <div class="form-group col-md-6">
        <label for="userName">Driver Name</label>
        <input type="text" class="form-control required_validation_for_stock_transfer1" name="Delivered_drivername">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Vehicle Number</label>
        <input type="text" class="form-control required_validation_for_stock_transfer1" name="Delivered_vehiclenumber">
        </div>
        <p class="h4 mb-4">Received by details:</p>

        <div class="form-group col-md-6">
        <label for="userName">Name</label>
        <input type="text" class="form-control required_validation_for_stock_transfer1" name="Received_name">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Date:</label>
        <input type="date" class="form-control required_validation_for_stock_transfer1" name="Received_date">
        </div> 

        </div> 
        </div> 

        </div> 

        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <button type="submit" class="btn btn-info waves-effect waves-light transferData">Transfer</button> 
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- /.modal -->

        <?php $this->load->view('Template/footer.php') ?>
        <script type="text/javascript">

$(document).on('change','.warehouse_data',function(e){

var warehouse_id=$(this).val();
 
 
getProductswithvarints(warehouse_id);

});


function getProductswithvarints(warehouse_id){
    if (!isNaN(warehouse_id)) {
	   var ddlState = $('.setproductData');
	   ddlState.empty();
	   ddlState.append($("<option></option>").val('').html('Please wait ...'));
	   $.ajax({
		 url: '<?=base_url()?>inventory/fetchStockTransferProducts',
		 type: 'GET',
		 dataType: 'json',
		 data: { warehouse_id: warehouse_id },
		 success: function (d) {
 
		   ddlState.empty(); // Clear the please wait
		   ddlState.append($("<option></option>").val('').html('Select Product'));
		   $.each(d, function (i, product) {
			 ddlState.append($("<option></option>").val(product.produtcId+'||'+product.variantId).html(product.productName));
		   });
		 },
		 error: function () {
		   alert('Error!');
		 }
	   });
	 }

}

//ExtratrAppand
 $(document).on('click','.addnewproductRow',function(){
 

 var rowClone = $('.setRowData .mainParentRow:first-child').clone();
 $('.setRowData').append(rowClone);
 //console.log(rowClone);
 
});
 

function sethtmldata(setText,dataset)
{
    if(setText == 'add')
    {
        var countRows = ($('.mainParentRow').length)+1;
        $('.setRowData').append('<tr ddd class="mainParentRow"><input type="hidden" class="inputProdcutId" name="inputProdcutId[]"/><input type="hidden" class="inputDiscountedPrice" name="inputDiscountedPrice[]"/><input type="hidden" class="inputVariantId" name="inputVariantId[]"/><input type="hidden" class="inputQuantity" name="inputQuantity[]"/><input type="hidden" class="inputActualPrice" name="inputActualPrice[]"/><input type="hidden" class="inputtotaltax" name="inputtotaltax[]"/><input type="hidden" class="inputTotalPrice" name="inputTotalPrice[]"/><td><div class="form-group" style="margin-bottom: 0px!important;"><select class="form-control setproductData select_auto'+countRows+'"><option value="">Choose Product</option><?php foreach($productData as $productDataList){?><option data-tax="<?php echo $productDataList['tax_price'] ?>"data-cp="<?php echo $productDataList['cp'] ?>" data-mrp="<?php echo $productDataList['mrp'] ?>" data-price="<?php echo $productDataList['productPrice'] ?>" data-stock="<?php echo $productDataList['stock'] ?>" data-variant="<?php echo $productDataList['variantId'] ?>" data-product="<?php echo $productDataList['produtcId'] ?>"><?php echo $productDataList['productName'] ?></option><?php } ?></select></div></td><td class="mrp"></td><td class="cp"></td><td class="setMainPrice"></td><td><input type="text" value="" class="form-control specialPrice number_validation" name="" placeholder=""></td><td class="tax"></td><td><input type="text" value="" class="form-control getQuantity number_validation" name="" placeholder=""></td><td class="setFinalPrice">0</td></tr>');
        $(".select_auto"+countRows+"").select2({
            minimumInputLength: 2
        });
    }
}

$(document).on('click','.stockTransferBtn_new',function(e){
 
$('#stockTransferModal_new').modal('show');
});

$(document).on('submit','#stockTransferForm_new',function(e){
            
                var check_required_field='';
                $(".required_validation_for_stock_transfer1").each(function(){
                    var val22 = $(this).val();
                     
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            
        });


        $(document).on('submit','#stockTransferForm',function(e){
            var setValidateCounter = 0;
            $('.childStockCheck').each(function(){
                if($(this).prop('checked') == true)
                {
                    var getQuantity = $(this).parents('.mainParentRow').find('.setQunatityData').val();
                    if(getQuantity == "")
                    {
                        setValidateCounter++;
                    }
                }
            });
            if(setValidateCounter == 0)
            {
                var check_required_field='';
                $(".required_validation_for_stock_transfer").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            }
            else
            {
                $('.setMessageData').html('');
                $('.setMessageData').append('<div class="alert alert-danger">Please fill all the selected product quantity</div>');
                return false;
            }
        });
        </script>

