<?php
class AdminLog extends CI_Model
{
  function __construct()

    {

        parent::__construct();
        $this->logTbale="houdin_user_logs";
        $this->userTbale="houdin_admin_users";
    }
    

    public function getAllLogs($params=false)
    {
        $this->db->select("*")
        ->from($this->logTbale);
        //->join($this->logTbale,"$this->userTbale.houdin_admin_user_id =$this->logTbale.houdin_user_log_userId","right outer");
                        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit'],$params['start']);
            }
            elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit']);
            }

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
            {
                $result = $this->db->count_all_results();
            }else
            {
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
            
           
            return $result;
                        

        
    }
    
}