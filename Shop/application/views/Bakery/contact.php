<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header') ?>


	 <div id="back2top"><i class="fa fa-angle-up"></i></div>

<div class="loader"></div>

<div class="page-wrap">

  <!--section-->

  <div class="ps-section--hero"><img src="<?=base_url();?>Extra/<?=strtolower($Template);?>/images/hero/01.jpg" alt="">

	<div class="ps-section__content text-center">

	  <h3 class="ps-section__title">OUR Contact</h3>

	</div>

  </div>

  <div class="ps-section pt-80 pb-80">
  <?php
		if($this->session->flashdata('error'))
		{
			echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
		}
		else if($this->session->flashdata('success'))
		{
			echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
		}
		?>
	<div class="container">

	  <div class="ps-contact">

		<div class="row">

			  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

				<div class="ps-contact__info">

				  <div class="mb-60" id="contact-map" data-address="New York, NY" data-title="BAKERY LOCATION!" data-zoom="17"></div>

				  <div class="ps-contact__block">
            <?php //print_r($shopInfo[0]);?>
					<!-- <h4>BASEMENT COMPANY, NEW YORK</h4> -->
					<p><?php echo $shopInfo[0]->houdinv_shop_address ?> </p>
					<p><i class="fa fa-envelope-o"></i><a href="mailto:<?php echo $shopInfo[0]->houdinv_shop_customer_care_email ?>">  <?php echo $shopInfo[0]->houdinv_shop_customer_care_email ?> </a> <br />
		</p>
		<p><i class="fa fa-envelope-o"></i><a href="mailto:<?php echo $shopInfo[0]->houdinv_shop_communication_email ?>">  <?php echo $shopInfo[0]->houdinv_shop_communication_email ?> </a>
		</p>

					<p><i class="fa fa-phone"></i><?php echo $shopInfo[0]->houdinv_shop_contact_info ?></p>

					<!-- <h4>Follow Us</h4>

						<ul class="ps-contact__social">

						  <li><a href="#"><i class="fa fa-facebook"></i></a></li>

						  <li><a href="#"><i class="fa fa-twitter"></i></a></li>

						  <li><a href="#"><i class="fa fa-rss"></i></a></li>

						</ul> -->

				  </div>

				</div>

			  </div>
			  <?php echo form_open(base_url('Contact/addcontact' ), array( 'id' => 'shopContact', 'method'=>'post' ));?>

			  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

				<div class="ps-contact__form">

				  <div class="form-group">

				<input type="text" name="userName" class="form-control required_validation_for_shop_contact name_validation" placeholder="Name*" />

				  </div>

				  <div class="form-group">

					<input type="text" name="userEmail" class="form-control required_validation_for_shop_contact email_validation name_validation" placeholder="Email*" />

				  </div>

				  <div class="form-group">

				<input type="text" name="userEmailSub" class="form-control required_validation_for_shop_contact name_validation" placeholder="Subject" />

				  </div>

				  <div class="form-group">

				<textarea name="userEmailBody" rows="5" class="form-control required_validation_for_shop_contact name_validation" placeholder="Text your message here..."></textarea>


				  </div>



				  <div class="form-group mt-30">

					<button type="submit"  class="ps-btn ps-btn--sm ps-contact__submit">Submit</button>

				  </div>

				</div>

			  </div>
			  <?php echo form_close(); ?>

		</div>

	  </div>

	</div>

  </div>



<?php $this->load->view($Template.'/Template/footer'); ?>
		<!-- Google Map APi
		============================================ -->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwIQh7LGryQdDDi-A603lR8NqiF3R_ycA"></script>


		<script>

		function initialize() {
		var mapOptions = {
		zoom: 15,

		scrollwheel: false,
		center: new google.maps.LatLng(43.538265, -80.305982),

		};
		var map = new google.maps.Map(document.getElementById('contact_map_area'),
		mapOptions);
		var marker = new google.maps.Marker({
		position: map.getCenter(),
		icon: 'img/map_pin.png',
		map: map,

		});

		}
		google.maps.event.addDomListener(window, 'load', initialize);
		</script>
		<script type="text/javascript">
		$(document).ready(function(){
			$(document).on('submit','#shopContact',function(){
				var check_required_field='';
				$(this).find(".required_validation_for_shop_contact").each(function(){
					var val22 = $(this).val();
					if (!val22){
						check_required_field =$(this).size();
						$(this).css("border-color","#ccc");
						$(this).css("border-color","red");
					}
					$(this).on('keypress change',function(){
						$(this).css("border-color","#ccc");
					});
				});
				if(check_required_field)
				{
					return false;
				}
				else {
					return true;
				}
			});
		});
		</script>
