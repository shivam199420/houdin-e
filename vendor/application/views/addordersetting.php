    <?php $this->load->view('Template/header.php') ?>
    <?php $this->load->view('Template/settingsidebar.php') ?>

    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">

    <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Order Setting</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li><a href="javascript:void(0);">General</a></li>
                  <li class="active">Order Setting</li>
                  </ol>
                  </div>
                 
            
            </div>
           <!--END Page-Title -->  
    <div class="row">
    <div class="col-sm-12">
    <?php
    if($this->session->flashdata('error'))
    {
        echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
    }
    else if($this->session->flashdata('success'))
    {
        echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
    }
    ?>
    </div>
    </div>
    <div class="row">
    <div class="col-sm-12 m-t-20">
    <div class="card-box">
    <?php echo form_open(base_url( 'Setting/orderSetting' ), array( 'method'=>'post' ));?>
    <div class="parentClass">
    <div class="form-group">
    <label>Cancellation period</label>
    <input value="<?php echo $orderList[0]->houdinv_shop_order_configuration_cancellation ?>" class="form-control number_validation name_validation setTextData" name="cancellationPeriod" readonly="readonly" type="text">
    <p>Number of days after order placement till which cancellation is allowed. (optional)</p>
    </div>
    <input type="checkbox" <?php if($orderList[0]->houdinv_shop_order_configuration_cancellation != "") { echo "checked=checked"; } ?> class="checkData" />&nbsp;Cancellation enabled
    <p>Order cancellation is not enabled in your store unless you check this and save.</p>
    </div>
    <div class="parentClass">
    <div class="form-group">
    <label>Return period</label>
    <input value="<?php echo $orderList[0]->houdinv_shop_order_configuration_return ?>" class="form-control number_validation name_validation setTextData" name="returnPeriod" readonly="readonly" type="text">

    </div>
    <div class="form-group">
    <input type="checkbox" <?php if($orderList[0]->houdinv_shop_order_configuration_return != "") { echo "checked=checked"; } ?> class="checkData" />&nbsp;Return enabled
    <p>Returns of items in an order is not enabled unless you check this and save.</p>
    </div>
    </div>
    <div class="parentClass">
    <div class="form-group">
    <label>Replace period</label>
    <input value="<?php echo $orderList[0]->houdinv_shop_order_configuration_replace ?>" class="form-control number_validation name_validation setTextData" name="replacePeriod" readonly="readonly" type="text">
    </div>
    <div class="form-group">
    <input type="checkbox" <?php if($orderList[0]->houdinv_shop_order_configuration_replace != "") { echo "checked=checked"; } ?> class="checkData" />&nbsp;Replace enabled
    <p>Replace of items in an order is not enabled unless you check this and save.
    </p>
    </div>
    <div class="form-group">
    <input type="checkbox" value="1" name="sms_email" <?php if($orderList[0]->houdinv_shop_order_configuration_sms_email == "1") { echo "checked=checked"; } ?> class="">&nbsp;Recieve an sms or Email For dailty sales
    </div>
    <div class="form-group">
    <input type="checkbox" value="1" name="allow_printing" <?php if($orderList[0]->houdinv_shop_order_configuration_printing == "1") { echo "checked=checked"; } ?> class="">&nbsp;Allow Printing Performa
    </div>
    </div>


    </div>
    <!--pricing-->

    <!--inventory-->

    </div>
    <div class="col-md-12">
    <input type="submit" class="btn btn-primary" name="insertOrderConfiguration" value="Save"/>
    </div>
    <?php echo form_close(); ?>
    </div>

    </div>
    </div>
    </div>
    <!--Delete-->


    <?php $this->load->view('Template/footer.php') ?>
