<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header") ?>

    <section>
        <div class="container py-85 py-tn-50">
            <!--<div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8 text-center">
                            <h2 class="title">Account</h2>
                            <div class="title-border mx-auto m-b-55 m-b-tn-35"></div>
                        </div>
                       
                    </div>
                </div>
            </div>-->
            <div class="row">
        <div class="container">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>
        </div>
            <div class="port-body">
                <div class="contact-form">
                
                    

                        <div class="col-sm-6 pull-left register-box">

                        <div class="row justify-content-center">
                        <div class="col-md-8 text-center">
                            <h2 class="title">Register</h2>
                            <div class="title-border mx-auto m-b-55 m-b-tn-35"></div>
                        </div>
                       
                    </div>
                        <?php echo form_open(base_url( 'Register/addShopUsers' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
                                <div class="form-group au-form">
                                <input type="text" class="required_validation_for_user characterValidation name_validation" name="userFirstName" placeholder="Enter Firstname" />
                                </div>
                             <div class="form-group au-form">
                             <input type="text" class="required_validation_for_user characterValidation name_validation" name="userLastName" placeholder="Enter Lastname"/>
                                </div>
                                <div class="form-group au-form">
                                <input type="email" class="required_validation_for_user name_validation email_validation" name="userEmail" placeholder="Enter Emailaddress"/>
                                </div>

                                 <div class="form-group au-form">
                                 <input type="text" class="required_validation_for_user name_validation number_validation" name="userContact" placeholder="Enter Contact Number"/>
                                </div>

                                 <div class="form-group au-form">
                                 <input type="password" class="required_validation_for_user name_validation opass" name="userPass" placeholder="Enter password"/>
                                </div>


                                 <div class="form-group au-form">
                                 <input type="password" class="required_validation_for_user name_validation cpass" name="userRetypePass" placeholder="Conform password" />
                                </div>

                               
                                
                                <div class="form-group au-form">
                                <input class="custom-box-button" type="submit"  class="acc_btn setDisableData mx-auto" value="Create an account"/>
                                </div>
                    <?php echo form_close(); ?>
                    </div><!-- end column-->
                    <div class="col-sm-6 pull-left login-box">
                    <div class="row justify-content-center">
                        <div class="col-md-8 text-center">
                            <h2 class="title">Login</h2>
                            <div class="title-border mx-auto m-b-55 m-b-tn-35"></div>
                        </div>
                       
                    </div>
                    <?php echo form_open(base_url( 'Register/checkUserAuth' ), array( 'id' => 'addShopLogin', 'method'=>'post' ));?>
                            <div class="form-group au-form">
                            <input type="email"  name="userEmail" class="required_validation_for_user name_validation email_validation" placeholder="Enter Email Address"/>
                                </div>
                                <div class="form-group au-form">
                                <input type="password" name="userPass" class="required_validation_for_user name_validation" placeholder="Enter Password"/>
                                </div>
                                <div class="form-group au-form">
                                <a href="javascript:;" title="Recover your forgotten password" data-toggle="modal" data-target="#myModal">Forgot your password?</a>    
                            </div>
                                <div class="form-group au-form">
                                    <input class="custom-box-button" type="submit"  class="mx-auto" value="Login"/>
                                </div>
                    <?php echo form_close(); ?>
                    </div><!-- end column-->
                    <div class="clearfix"></div>
                  
                </div>
            </div>
        </div>
    </section>
     <!-- Modal -->
     <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <?php echo form_open(base_url( 'Register/checkUserForgot' ), array( 'id' => 'addShopUserForgot', 'method'=>'post' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Forgot your password</h4>
        <button type="button" class="close" data-dismiss="modal" style="float: right;">&times;</button>
        </div>
        <div class="modal-body">
        <input placeholder="Please enter your Email address" name="emailForgot" class="required_validation_for_user name_validation email_validation" type="text" style="width:100%;">
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-default acc_btn" style="float: left" value="Submit"/>
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        <!-- modal end -->
       <?php 
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer") ?>
    <script type="text/javascript">
        $(document).ready(function(){
                $(document).on('submit','#addShopUsers,#addShopLogin,#addShopUserForgot',function(){
                        var check_required_field=0;
                        $(this).find(".required_validation_for_user").each(function(){
                                var val22 = $(this).val();
                                if (!val22){
                                        check_required_field++;
                                        $(this).css("border-color","#ccc");
                                        $(this).css("border-color","red");
                                }
                                $(this).on('keypress change',function(){
                                        $(this).css("border-color","#ccc");
                                });
                        });
                        if(check_required_field != 0)
                        {
                                return false;
                        }
                        else {
                                return true;
                        }
                });
        });
                </script>