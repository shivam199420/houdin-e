<?php
class Csv extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->auth_users->is_logged_in();
        $this->load->model('Csvmodel');
        $this->load->library('csvimport');
    }

    function index() {
        $data['addressbook'] = $this->Csvmodel->get_addressbook();
        $this->load->view('csvindex', $data);
    }

    function importcsv() {
        $data['addressbook'] = $this->Csvmodel->get_addressbook();
        $data['error'] = '';    //initialize image upload error array to empty

        $config['upload_path'] = APPPATH . '../upload'; 
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';

        $this->load->library('upload', $config);


        // If upload failed, display error
        if (!$this->upload->do_upload()) {
             
            $this->data['error'] = $this->upload->display_errors();
            $this->session->set_flashdata('error',$this->upload->display_errors());
            redirect(base_url().'Customer');
        } else {
            $file_data = $this->upload->data();
            $file_path =  APPPATH . '../upload/'.$file_data['file_name'];
            
            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);
                foreach ($csv_array as $row) {
                   $get_email_check= $this->Csvmodel->get_email_check($row['Email']);
              if($get_email_check){
                    $insert_data = array(
                        'houdinv_user_name'=>$row['Name'],
                        'houdinv_users_gender'=>$row['Gender'],
                        'houdinv_user_contact'=>$row['Phone'],
                        'houdinv_user_email'=>$row['Email'],
                    );
                    $this->Csvmodel->insert_csv($insert_data);
                }
                    
                }
                $this->session->set_flashdata('success', 'Csv Data Imported Succesfully');
                redirect(base_url().'Customer');
                echo "<pre>"; print_r($insert_data);
            } else 
            $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
        
                redirect(base_url().'Customer');
            }
            
        } 


        function customerimportcsv() {
            $data['addressbook'] = $this->Csvmodel->get_addressbook();
            $data['error'] = '';    //initialize image upload error array to empty
    
            $config['upload_path'] = APPPATH . '../upload/csv'; 
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '1000';
    
            $this->load->library('upload', $config);
    
    
            // If upload failed, display error
            if (!$this->upload->do_upload()) {
                 
                $this->data['error'] = $this->upload->display_errors();
                $this->session->set_flashdata('error',$this->upload->display_errors());
                redirect(base_url().'product');
            } else {
                $file_data = $this->upload->data();
                $file_path =  APPPATH . '../upload/csv/'.$file_data['file_name'];
                
                if ($this->csvimport->get_array($file_path)) {
                    $csv_array = $this->csvimport->get_array($file_path);
                    foreach ($csv_array as $row) {
                      // $get_email_check= $this->Csvmodel->get_email_check($row['Email']);
                 // if($get_email_check){
                        $insert_data = array(
                            'houdin_products_title'=>$row['product_titel'],
                            'houdin_products_short_desc'=>$row['short_desc'],
                            'houdin_products_desc'=>$row['product_desc'],
                            'houdin_products_pag_title'=>$row['page_titel'],
                        );
                        $this->Csvmodel->insert_product_csv($insert_data);
                   // }
                        
                    }
                    $this->session->set_flashdata('success', 'Csv Data Imported Succesfully');
                    redirect(base_url().'product');
                    echo "<pre>"; print_r($insert_data);
                } else 
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            
                    redirect(base_url().'product');
                }
                
            } 


}
/*END OF FILE*/
