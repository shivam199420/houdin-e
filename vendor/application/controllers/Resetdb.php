<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Resetdb extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->auth_users->is_logged_in();
        $this->load->model('Resetdbmodel'); 
    }
    public function index()
    {
        $this->Resetdbmodel->getDb();
        redirect(base_url()."Dashboard");
    }
}

