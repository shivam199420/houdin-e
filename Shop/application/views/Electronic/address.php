<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
  <!-- Content -->
  <div id="content"> 
    
    <!-- Linking -->
    <div class="linking">
      <div class="container">
        <ol class="breadcrumb">
          <li class="active">Address</li>
        </ol>
      </div>
    </div>
    
    <!-- Blog -->
    <section class="login-sec  padding-bottom-100">
      <div class="container">
        <div class="row">
        <?php 
        if(count($userAddress) > 0)
        {
        ?>
        <a href="<?php echo base_url() ?>Address/add" class="btn-round pull-right" style="margin-bottom:10px">Add New Address</a><div class="clearfix"></div>
        <?php } ?>
        <?php 
        if(!count($userAddress) > 0)
        {
        ?>
          <div class="col-md-6 col-md-offset-3"> 
            <!-- Login Your Account -->
            <h5>Address</h5>
            <hr>
            <p>Add new address here.</p>
                <li >
                  <a href="<?php echo base_url() ?>Address/add"class="btn-round">Add New Address</a>
                </li>
              </ul>
            
          </div>
        <?php  } 
        else
        {
        ?>
        <div class="transportation">
        <?php 
        foreach($userAddress as $userAddressList)
        {
          ?> 
        <div class="col-sm-4">
                    <div class="charges">
                      <h6><?php echo $userAddressList->houdinv_user_address_name."(".$userAddressList->houdinv_user_address_phone.")" ?>
                      <span style="float: right;font-size: 10px;position: absolute;right: 23px;top: 7px;">
                      <a href="<?php echo base_url() ?>Address/edit/<?php echo $userAddressList->houdinv_user_address_id ?>" style="font-size:12px" class="text-success"><i class="fa fa-pencil"></i></a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a href="<?php echo base_url() ?>Address/deleteAddress/<?php echo $userAddressList->houdinv_user_address_id ?>" class="text-danger" style="font-size:12px"><i class="fa fa-trash-o"></i></a></span>
                      </h6>
                      <br>
                      <span><address><?php echo $userAddressList->houdinv_user_address_user_address ?>,<br>
                <?php echo $userAddressList->houdinv_user_address_city ?>,<br>
                <?php echo $userAddressList->houdinv_user_address_zip ?>
                </address></span> </div>
                  </div>
        <?php } ?> </div> <?php  }
        ?>
        </div>
      </div>
    </section>
  </div>
  <!-- End Content --> 
  <?php 
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer') 
  ?>