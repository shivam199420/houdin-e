<!doctype html>
<html class="no-js" lang="en">
<?php
$getDbName = getDBName();
$vendorURL = $this->session->userdata('vendorURL');
$this->load->helper('dynamicdatabaseapp');
$getDBDetail = switchDynamicDatabaseApp($this->session->userdata('shopName'));
$this->db = $this->load->database($getDBDetail,true);
$con = mysqli_connect("localhost","root","houdine123",$getDB);
$getImages = mysqli_query($con,"select shop_logo,shop_favicon from houdinv_shop_logo limit 1");
$getImageData = mysqli_fetch_array($getImages);
// get username
$getUserId = $this->session->userdata('userAuth');
$getUser = mysqli_query($con,"select houdinv_user_name from houdinv_users where houdinv_user_id='$getUserId'");
$getUserData = mysqli_fetch_array($getUser);
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<script type="text/javascript">
      var base_url = "<?php echo base_url() ?>";
  </script>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?php 
  $getShopName = getshopname();
  if($getShopName)
  {
      $setShopName = $getShopName;
  }
  else
  {
      $setShopName = 'Houdin-e';
  }
  ?>
  <title>Welcome To <?php echo $setShopName; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="<?php if($getImageData['shop_favicon']) { ?><?=$vendorURL?>upload/logo/<?php echo $getImageData['shop_favicon'];  ?> <?php } else { ?> <?php echo base_url() ?>Extra/apparels/img/hundin-e-favicon.png <?php  } ?> ">

    <!-- all css here -->
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/assets/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/lib/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/lib/css/preview.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/assets/css/meanmenu.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/assets/css/bundle.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/genearl/assets/css/intlTelInput.css">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

    <style type="text/css">
        @media (max-width: 767px){
        .title1, .title4 {
    font-size: 20px;
    margin-bottom: 0;
    margin-left: 29px;
}
.preview-2 .nivoSlider:hover .nivo-directionNav a.nivo-nextNav {
    right: 83px;
}
.preview-2 .nivo-directionNav a.nivo-nextNav::before {
    font: 400 15px/30px FontAwesome;
    height: 30px;
    top: 45%;
    width: 30px;
    right: -64px;
}
        }
    </style>
    <style type="text/css">
        @media (max-width: 767px){
            .mobile-menu-area .mean-nav > ul {
    height: 85px;
    overflow-y: auto;
}
        }
    </style>
    <style type="text/css">
.desktop-showContent {
  visibility: visible !important;
  opacity: 1 !important;
}
.desktop-query {
  width: 300px;
  float: right;
  line-height: 0;
  margin: 20px 10px 20px 0;
}
.desktop-query > input.desktop-searchBar {
  font-family: Whitney;
  display: inline-block;
  float: left;
  font-size: 16px;
  height: 20px;
  line-height: 24px;
  width: 235px;
  color: #696E79;
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  padding: 8px 10px 10px 10px;
  margin: 0;
  outline: 0;
  border: 1px solid #D5D6D9;
  -webkit-border-radius: 4px 0 0 4px;
  -moz-border-radius: 4px 0 0 4px;
  border-radius: 4px 0 0 4px;
  border-right: 0;
  background: #F5F5F6;
}
input.desktop-searchBar:focus {
  background-color: #FFFFFF;
  border-color: #14CDA8;
}
input.desktop-searchBar:focus + a.desktop-submit {
  background-color: #FFFFFF;
  border-color: #14CDA8;
}
a.desktop-submit {
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  display: inline-block;
  height: 28px;
  width: 40px;
  text-align: center;
  padding: 8px 0 2px 0;
  background: #F5F5F6;
  border: 1px solid #D4D5D9;
  border-left: none;
  -webkit-border-radius: 0 4px 4px 0;
  -moz-border-radius: 0 4px 4px 0;
  border-radius: 0 4px 4px 0;
}
.desktop-autoSuggest {
  z-index: 4;
  position: relative;
  display: block;
  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0);
  -moz-box-shadow: 0 1px 6px rgba(0, 0, 0, 0);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0);
  /* rgba(0,0,0,0.3) */
  font-size: 11px;
  color: #696E79;
  background-color: #fff;
  -webkit-font-smoothing: antialiased;
  visibility: hidden;
  opacity: 0;
  -webkit-box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.3);
  -moz-box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.3);
  box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.3);
  width:100%;
}
.desktop-autoSuggest .desktop-group {
  margin: 0 0 10px 0;
  padding: 0;
  max-height: 300px;
  overflow-y: auto;
}

.desktop-group > li {
  list-style: none;
  padding-left: 12px;
  text-align: left;
  display: flex;
    border-bottom: 1px solid #ddd;
}
.desktop-group > li >a{}
  .desktop-group > li >a.linkcat_search{
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    padding:8px;
  }
  .desktop-group > li >a.linkcat_search_cart{
    width: 35px;
    text-align: center;
    background-color:#eee;
    border-left:1px solid #ddd;
    padding: 8px;

  }
.desktop-suggestion {
  background-color: #FFFFFF;
}
.desktop-suggestion:hover {
  background-color: #D4D5D9;
  cursor: pointer;
}
.desktop-suggestionTitle {
  font-weight: bold;
 /* background-color: #F5F5F6;*/
  cursor: pointer;
  padding: 8px;
  background-color: #333;
  color: white;

}
.search-box {
    line-height: 38px;
}
</style>
<style>
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url('<?php echo base_url() ?>Extra/genearl/assets/img/Preloader_6.gif') center no-repeat #fff;
}


.mega-dropdown {
  position: static !important;
}
.mega-dropdown-menu {
    padding: 20px 0px;
    width: 100%;
    box-shadow: none;
    -webkit-box-shadow: none;
}
.mega-dropdown-menu > li > ul {
  padding: 0;
  margin: 0;
}
.mega-dropdown-menu > li > ul > li {
  list-style: none;
}
.mega-dropdown-menu > li > ul > li > a {
  display: block;
  color: #222;
  padding: 3px 5px;
}
.mega-dropdown-menu > li ul > li > a:hover, .mega-dropdown-menu > li ul > li > a:focus {
    text-decoration: none;
    margin-left: 5px!important;
    border-left: 4px solid gray;
}
.mega-dropdown-menu .dropdown-header {
  font-size: 18px;
  color: #0c0c0c;
  padding: 5px 60px 5px 5px;
  line-height: 30px;
  border-bottom: 1px solid gray;
    margin-bottom: 5px;
}
.dropdown {
    background: transparent!important;
    box-shadow: none!important;
    text-align: left!important;
    top: 100%!important;
    transition: none!important;
     width: auto!important;
    z-index: 999!important;
    opacity: 1!important;
    visibility: visible!important;
    left: 0!important;
    transform: none!important;
}
.dropdown-menu {
    /*position: absolute;
    top: 100%;
    left: 0;
    z-index: 1000;
    display: none;
    float: left;
    min-width: 160px;
    padding: 5px 0;
    margin: 2px 0 0;
    list-style: none;
    font-size: 14px;
    text-align: left;
    background-color: #fff;
    border: 1px solid #ccc;
    border: 1px solid rgba(0,0,0,0.15);
    border-radius: 4px;
    -webkit-box-shadow: 0 6px 12px rgba(0,0,0,0.175);
    box-shadow: 0 6px 12px rgba(0,0,0,0.175);
    -webkit-background-clip: padding-box;
    background-clip: padding-box;*/
}
.float-right{
  float: right;
}

.left_spacing{
      padding-left: 10px!important;
}
@media(max-width: 767px)
{
  .float_left_mobile
  {
    float: left !important;
  }
}
</style>
</head>

<body>
  <!-- Paste this code after body tag -->
  <div class="se-pre-con"></div>
  <!-- Ends -->
    <!-- header start -->
    <header class="header-area home-style-2">
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-3 col-xs-6">
                        <div class="logo">
                            <a href="<?php echo base_url() ?>home/">
                                <img src="<?php if($getImageData['shop_logo']) { ?><?=$vendorURL;?>upload/logo/<?php echo $getImageData['shop_logo']  ?> <?php } else { ?> <?php echo base_url() ?>Extra/genearl/assets/img/logo/logo-1532414830.png <?php  } ?>" alt="LOGO" style="height: 47px!important;">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-9 col-xs-6">
                        <div class="cart-menu">
                            <div class="search-style-2 float-right float_left_mobile">
                                <a class="icon-search-2" href="#">
                                    <i class="pe-7s-search"></i>
                                </a>
                                <div class="search2-content">

                                        <div class="search-input-button2" id="searchboxsuggestionproduct">
                                        <input type="hidden" class="form-control" value="category1" name="shopName" placeholder="enter keyword"/>
                                            <input type="text" class="form-control" name="typeahead" id="searchfiltertext"  autocomplete="off" spellcheck="false" placeholder="enter keyword">
                                            <button name="submit" class="search-button2" type="submit">
                                                <i class="pe-7s-search"></i>
                                            </button><br>
                                            <div id="searchfilterdataheader" class=" desktop-autoSuggest desktop-showContent" ></div>
                                        </div>

                                </div>
                            </div>
                             <!-- cart -->
                             <?php
        $AllMiniCart =array();
        $totalCartData =0;
        if($this->session->userdata('userAuth'))
        {
            $data_All = $this->db->select("*")->from("houdinv_users_cart") ->where("houdinv_users_cart.houdinv_users_cart_user_id",$this->session->userdata('userAuth'))->get()->result();
            $setCartArray = array();
            foreach($data_All as $data_AllData)
            {
                if($data_AllData->houdinv_users_cart_item_id != 0 && $data_AllData->houdinv_users_cart_item_id != "")
                {
                    $getProductData = $this->db->select('*')->from('houdinv_products')->where('houdin_products_id',$data_AllData->houdinv_users_cart_item_id)->get()->result();
                    // setImageData
                    $getImage = json_decode($getProductData[0]->houdinv_products_main_images,true);
                    // set price
                    $getPrice = json_decode($getProductData[0]->houdin_products_price,true);
                    if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
                    {
                        $setPrice = $getProductData[0]->houdin_products_final_price;
                    }
                    else
                    {
                        $setPrice = $getProductData[0]->houdin_products_final_price;
                    }
                    $setCartArray[] = array('count'=>$data_AllData->houdinv_users_cart_item_count,'productName'=>$getProductData[0]->houdin_products_title,'productprice'=>$setPrice,'productImage'=>$getImage[0],'cartId'=>$data_AllData->houdinv_users_cart_id);
                }
                else
                {
                    $getProductData = $this->db->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data_AllData->houdinv_users_cart_item_variant_id)->get()->result();
                    // get product
                    $getExtraProduct = $this->db->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getProductData[0]->houdin_products_variants_product_id)->get()->result();
                    // set price
                    $getPrice = json_decode($getExtraProduct[0]->houdin_products_price,true);
                    if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
                    {
                        $setPrice = $getProductData[0]->houdin_products_variants_prices-$getPrice['discount'];
                    }
                    else
                    {
                        $setPrice = $getProductData[0]->houdin_products_variants_prices;
                    }

                    $setCartArray[] = array('count'=>$data_AllData->houdinv_users_cart_item_count,'productName'=>$getProductData[0]->houdin_products_variants_title,'productprice'=>$setPrice,'productImage'=>$getProductData[0]->houdin_products_variants_image,'cartId'=>$data_AllData->houdinv_users_cart_id);
                }
            }
            $totalCartData=count($setCartArray);

            //  $AllMiniCart =$this->Cartmodel->ShowCartData1($this->session->userdata('userAuth'));
            }
            ?>
                            <div class="shopping-cart float-right float_left_mobile cart_menu_area">
                                <a class="top-cart" ><i class="pe-7s-cart"></i></a>
                                <span class="cart_number"><?php  echo $totalCartData; ?></span>
                                <ul class="mc-pro-list">
                                <?php
                                if($totalCartData>0)
                                {

                                $i=1;
                                $final_price =0;
                                foreach($setCartArray as $setCartArrayList)
                                {
                                    $image = $setCartArrayList['productImage'];
                                    $count = $setCartArrayList['count'];
                                    $main_price = $setCartArrayList['productprice'];
                                    $total_price = $main_price*$count;
                                    $final_price =$final_price+$total_price;
                                    ?>
                                    <li>
                                        <div class="cart-img-price">
                                            <div class="cart-img">
                                                <a href="javascript:;"><img src="<?php echo $this->session->userdata('vendorUrl'); ?>/upload/productImage/<?php echo $image; ?>" style="width:70px" alt="" /></a>
                                            </div>
                                            <div class="cart-content">
                                                <h3><a href="javascript:;"><?php echo substr($setCartArrayList['productName'],0,12); ?></a> </h3>
                                                <span class="cart-price"><?php echo $count ?> x <?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $main_price; ?></span>
                                            </div>
                                            <div data-id="<?php echo $setCartArrayList['cartId']; ?>" data-replace="<?php echo $total_price; ?>" class="cart-del cp_cart_remove">
                                                <i class="pe-7s-close-circle"></i>
                                            </div>
                                        </div>
                                    </li>
                                <?php }
                            ?>
                             <li>
                                        <p class="total">
                                            Subtotal:
                                            <span><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $final_price; ?></span>
                                        </p>
                                    </li>
                                    <li>
                                        <p class="buttons">
                                            <a class="javascript:;" href="<?php echo base_url() ?>cart">View Cart</a>
                                            <a class="checkout" href="<?php echo base_url() ?>checkout">Checkout</a>
                                        </p>
                                    </li>
                            <?php }
                                else
                                {
                                ?>
                                <li>
                                    <p class="total"> No item found </p>
                                    </li>
                                <?php }
                                ?>


                                </ul>
                            </div>
                            <!-- cart end -->
                            <div class="user user-style-3 float-right float_left_mobile">

                                    <a href="javascript:;">
                                    <i class="pe-7s-add-user"></i>
                               </a>


                                <div class="currence-user-page">
                                    <div class="user-page">
                                        <ul>
                                        <?php
                                        if($this->session->userdata('userAuth') != "")
                                        {
                                            ?>
                                             <li><a href="<?php echo base_url(); ?>Orders">My order</a></li>
                                            <li><a href="<?php echo base_url() ?>Address">My address</a></li>
                                            <li><a href="<?php echo base_url() ?>Changepassword">Change password</a></li>
                                            <li><a href="<?php echo base_url() ?>wishlist">Wishlist</a></li>
                                            <li><a href="<?php echo base_url() ?>Logout">Logout</a></li>
                                        <?php }
                                        else
                                        {
                                        ?>
                                        <li><a href="<?php echo base_url() ?>Register"><i class="pe-7s-next-2"></i> Sign In</a></li>
                                        <li><a href="<?php echo base_url() ?>Register"><i class="pe-7s-add-user"></i> Create an account</a></li>
                                        <?php }
                                        ?>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="main-menu f-right">
                                <nav>
                                    <ul>
                                    <?php
                                    //get header navigation
                                    $getHeadernav = mysqli_query($con,"select * from houdinv_navigation_store_pages where houdinv_navigation_store_pages_name='home' || houdinv_navigation_store_pages_category_id !=0 || houdinv_navigation_store_pages_custom_link_id !=0");
                                    while($getHeaderNavData = mysqli_fetch_array($getHeadernav))
                                    {
                                        if($getHeaderNavData['houdinv_navigation_store_pages_name'] == 'home')
                                        {
                                        ?>
                                        <li class="hashsub"><a href="<?php echo base_url() ?>home/<?php echo $this->session->userdata('shopName') ?>">Home</a></li>
                                        <?php }
                                        else if($getHeaderNavData['houdinv_navigation_store_pages_custom_link_id'] != 0)
                                        {
                                                $setCustomLinkId = $getHeaderNavData['houdinv_navigation_store_pages_custom_link_id'];
                                                $getCustomLinkData = mysqli_query($con,"select * from houdinv_custom_links where houdinv_custom_links_id = $setCustomLinkId");
                                                $fetchCustomLinkData = mysqli_fetch_array($getCustomLinkData);
                                        ?>
                                        <li class="hashsub"><a href="<?php echo $fetchCustomLinkData['houdinv_custom_links_url'] ?>" <?php if($fetchCustomLinkData['houdinv_custom_links_target'] == 'new') { ?> target="_blank" <?php  } ?>><?php echo $fetchCustomLinkData['houdinv_custom_links_title'] ?></a></li>
                                        <?php }
                                        else
                                        {
                                                $getMainCategoryId = $getHeaderNavData['houdinv_navigation_store_pages_category_id'];
                                                $getSubCategoryData = mysqli_query($con,"select houdinv_sub_category_one_name,houdinv_sub_category_one_id from houdinv_sub_categories_one where houdinv_sub_category_one_main_id = $getMainCategoryId");
                                                $totalCategoryData=mysqli_num_rows($getSubCategoryData);
                                        ?>
                                         <li class="dropdown mega-dropdown"><a  href="<?php echo base_url() ?>Productlist/<?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_name'] ?><?php if($totalCategoryData > 0) { ?> <i class="fa fa-angle-down"></i> <?php  } ?></a>
                                           <?php
                                           if($totalCategoryData > 0)
                                            {
                                           ?>
                                          <ul class="dropdown-menu mega-dropdown-menu">
                                            <?php
                                            while($fetchSubCategory = mysqli_fetch_assoc($getSubCategoryData))
                                            {
                                                    $getSubCategoryId = $fetchSubCategory['houdinv_sub_category_one_id'];
                                             ?>
                                            <li class="col-sm-4">
                                                <ul>
                                                <li><a href="<?php echo base_url() ?>Productlist/<?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>/<?php echo $getSubCategoryId ?>"><?php echo $fetchSubCategory['houdinv_sub_category_one_name'] ?></a>

                                                <ul class="left_spacing">
                                                  <?php
                                                  $getSubSubCategory = mysqli_query($con,"select houdinv_sub_category_two_name,houdinv_sub_category_two_id from houdinv_sub_categories_two where houdinv_sub_category_two_sub1_id = $getSubCategoryId");
                                                  while($fetchSubSubCategory = mysqli_fetch_assoc($getSubSubCategory))
                                                  {
                                              $setSubSubCategoryId = $fetchSubSubCategory['houdinv_sub_category_two_id'];
                                                  ?>
                                                  <li><a href="<?php echo base_url() ?>Productlist/<?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>/<?php echo $getSubCategoryId ?>/<?php echo $setSubSubCategoryId ?>"><?php echo $fetchSubSubCategory['houdinv_sub_category_two_name']; ?></a></li>
                                                <?php } ?>
                                                </ul>
                                                </li>
                                              <!--  <li><a href="#">Carousel Control</a></li>
                                                <li><a href="#">Left & Right Navigation</a></li>
                                                <li><a href="#">Four Columns Grid</a></li> -->
                                              </ul>
                                            </li>
                                          </ul>
                                        <?php } } ?>
                                        <?php
                                        // if($totalCategoryData > 0)
                                        // {
                                        ?>
                                        <!--<ul class="sub-menu">
                                            <div class="container submenu-inner">
                                        <?php
                                        // while($fetchSubCategory = mysqli_fetch_assoc($getSubCategoryData))
                                        // {
                                        //         $getSubCategoryId = $fetchSubCategory['houdinv_sub_category_one_id'];
                                            ?>

                                            <li><a href="<?php //echo base_url() ?>Productlist/<?php //echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>/<?php //echo $getSubCategoryId ?>"><?php //echo $fetchSubCategory['houdinv_sub_category_one_name'] ?></a>
                                        <?php
                                        //$getSubSubCategory = mysqli_query($con,"select houdinv_sub_category_two_name,houdinv_sub_category_two_id from houdinv_sub_categories_two where houdinv_sub_category_two_sub1_id = $getSubCategoryId");
                                        ?>
                                        <ul class="sub-subdropdown">
                                        <?php
                                        // while($fetchSubSubCategory = mysqli_fetch_assoc($getSubSubCategory))
                                        // {
                                        //         $setSubSubCategoryId = $fetchSubSubCategory['houdinv_sub_category_two_id'];
                                        ?>
                                        <li><a href="<?php //echo base_url() ?>Productlist/<?php //echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>/<?php //echo $getSubCategoryId ?>/<?php //echo $setSubSubCategoryId ?>"><?php //echo $fetchSubSubCategory['houdinv_sub_category_two_name']; ?></a>
                                        <?php //}
                                        ?>
                                        </ul>
                                        </li>
                                        <?php //}
                                        ?>
                                        </div>
                                            </ul>
                                        <?php //}
                                        ?>
                                         -->
                                        </li>
                                        <?php }
                                }
                                ?>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header end -->
    <!-- mobile-menu-area start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul>
                                <li><a class="active1" href="index.html">HOME</a></li>
                                <li><a href="about-us.html">ABOUT</a></li>
                                <li><a href="shop-page.html">SHOP</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- mobile-menu-area end -->
