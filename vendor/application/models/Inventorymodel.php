
<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventorymodel extends CI_Model{
	function __construct() {
		parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
	$this->productTbl = 'houdinv_products';
	$this->productVariantTbl = 'houdinv_products_variants';
	}
	public function fetchaddinventory_api(){
		$getInventoryData = $this->db->select('houdinv_inventory_purchase.*,houdinv_suppliers.houdinv_supplier_comapany_name,houdinv_suppliers.houdinv_supplier_email,houdinv_suppliers.houdinv_supplier_contact')
		->from('houdinv_inventory_purchase')->where('houdinv_inventory_purchase.houdinv_inventory_purchase_status !=','recieve')
		->join('houdinv_suppliers','houdinv_suppliers.houdinv_supplier_id = houdinv_inventory_purchase.houdinv_inventory_purchase_supplier_id')
		->get()->result();
		 return $getInventoryData;
	}

	public function deletePurchaseData($data)
	{
	$explodData = explode(',',$data);
	$this->db->where_in('houdinv_purchase_products_id',$explodData);
	$getDeleteStatus = $this->db->delete('houdinv_purchase_products');
	if($getDeleteStatus)
	{
	return true;

	}   
	else
	{
	return  false;
	}
	}

	public function lowinventorys_api()
	{
		$this->db->select('*')->from('houdinv_inventorysetting')->where('Universal_rl','1'); 
		$getinventory =$this->db->get()->row();
		if($getinventory){
			$getinventory_value=$getinventory->inventorysetting_number;
		}else{
			$getinventory_value='1';
		}
		$this->db->select('*')->from('houdinv_products')->where('houdinv_products_total_stocks<=',$getinventory_value); 
		$getproduct =$this->db->get()->result();
		
		$this->db->select('*')->from('houdinv_products_variants as A');
		//$this->db->join('houdinv_products as B', 'B.houdin_products_id = A.houdin_products_variants_product_id');
		$this->db->where('A.houdinv_products_variants_total_stocks<=',$getinventory_value); 
		$lowvariants =$this->db->get()->result();
		$variants=array_merge($getproduct, $lowvariants);

		$setMainProductArray = array();
  foreach($variants as $variantss){
	  $childArray = array();
	  $products_id=$variantss->houdin_products_id;
	  $products_varints_id=$variantss->houdin_products_variants_product_id;

	  $this->db->select('houdinv_supplier_products.houdinv_supplier_products_supplier_id,hs.houdinv_supplier_comapany_name,hs.houdinv_supplier_contact_person_name,hs.houdinv_supplier_id')->from('houdinv_supplier_products');
	  $this->db->where('houdinv_supplier_products_product_id',$products_id); 
	  $this->db->or_where('houdinv_supplier_products_product_id',$products_varints_id);
	  $this->db->join('houdinv_suppliers as hs','hs.houdinv_supplier_id = houdinv_supplier_products.houdinv_supplier_products_supplier_id');
	  $getsupliers=$this->db->get()->result();
	 // $this->db->select('houdinv_supplier_id,houdinv_supplier_comapany_name,houdinv_supplier_contact_person_name')->from('houdinv_suppliers')->where('houdinv_supplier_id', $getsupliers[0]->houdinv_supplier_products_supplier_id); 
		//$suppliers[] =$this->db->get()->result();
		$childArray['product'] = $variantss;
		$childArray['supplier'] = $getsupliers;
		array_push($setMainProductArray,$childArray);
  }
 /* echo "<pre>";
  print_r($setMainProductArray);
  echo "</pre>";
die; */


		return  $setMainProductArray;
	}

	public function lowinventory_total($value='')
	{
		$this->db->select('*')->from('houdinv_inventorysetting')->where('Universal_rl','1'); 
		$getinventory =$this->db->get()->row();
		if($getinventory){
			$getinventory_value=$getinventory->inventorysetting_number;
		}else{
			$getinventory_value='1';
		}
		$this->db->select('COUNT(*) AS totalproduct')->from($this->productTbl)->where('houdin_products_status','1'); 
		$totalproduct =$this->db->get()->row();
		$this->db->select('COUNT(*) AS outofstock')->from($this->productTbl)->where('houdin_products_status','1')->where('houdinv_products_total_stocks',0); 
		$outofstock =$this->db->get()->row();
		$this->db->select('COUNT(*) AS lowstock')->from($this->productTbl)->where('houdin_products_status','1')->where('houdinv_products_total_stocks<=',$getinventory_value); 
		$lowstock =$this->db->get()->row();
		return  $arrayName = array('totalproduct' =>$totalproduct->totalproduct,'outofstock'=>$outofstock->outofstock,'lowstock'=>$lowstock->lowstock);
	}

	public function suppliersGet()
	{
	$this->db->select('houdinv_supplier_id,houdinv_supplier_comapany_name,houdinv_supplier_contact_person_name')->from('houdinv_suppliers')->where('houdinv_supplier_active_status','active'); 
	return $suppliers =$this->db->get()->result();
	}
	public function addPurchaseOrderData($data)
	{
	$setData = strtotime($data['deliveryDate']);

 $data_product=$data['productall'];


 foreach($data_product as $data_products){
	$supplier_id=$data_products['supplier'];
  $product_ids=$data_products['product'];


  $setPurchaseMain = array('houdinv_inventory_purchase_credit'=>$data['creditPeriod'],'houdinv_inventory_purchase_supplier_id'=>$supplier_id,'houdinv_inventory_purchase_delivery'=>$setData,'houdinv_inventory_purchase_status'=>'in process','houdinv_inventory_purchase_created_at'=>strtotime(date("y-m-d")));
  $this->db->insert('houdinv_inventory_purchase',$setPurchaseMain);
  $getData = $this->db->insert_id();
  if($getData)
  {
  for($index = 0; $index < count($data_products['product']); $index++)
  {
  $setInsertArray = array('houdinv_purchase_products_purchase_id'=>$getData,'houdinv_purchase_products_product_id'=>$data_products['product'][$index],'houdinv_purchase_products_quantity'=>$data['quantity'][$index]);
  $this->db->insert('houdinv_purchase_products',$setInsertArray);
  }

   
  }
  
 }
  return true;
	
	}
	// deduct inventory data
	public function fetchDeductInventoryData()
	{
		$getPurchaseData = $this->db->select('houdinv_purchase_products_inward.houdinv_purchase_products_inward_quantity_return,houdinv_purchase_products.houdinv_purchase_products_purchase_id,houdinv_purchase_products.houdinv_purchase_products_product_id,houdinv_purchase_products.houdinv_purchase_products_product_variant_id')->from('houdinv_purchase_products_inward')->where('houdinv_purchase_products_inward_quantity_return != 0')
		->join('houdinv_purchase_products','houdinv_purchase_products.houdinv_purchase_products_id = houdinv_purchase_products_inward.houdinv_purchase_products_inward_purchase_product_id','left outer')->get()->result();
		foreach($getPurchaseData as $getPurchaseDataList)
		{
			if($getPurchaseDataList->houdinv_purchase_products_product_id != 0 && $getPurchaseDataList->houdinv_purchase_products_product_id != "")
			{
				$getProductdata = $this->db->select('houdin_products_title')->from('houdinv_products')->where('houdin_products_id',$getPurchaseDataList->houdinv_purchase_products_product_id)->get()->result();
				$setdeductInventorydata[] = array('purchaseId'=>$getPurchaseDataList->houdinv_purchase_products_purchase_id,'productTitle'=>$getProductdata[0]->houdin_products_title,'return'=>$getPurchaseDataList->houdinv_purchase_products_inward_quantity_return);
			}
			else
			{
				$getvariantdata = $this->db->select('houdin_products_variants_title')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getPurchaseDataList->houdinv_purchase_products_product_variant_id)->get()->result();
				$setdeductInventorydata[] = array('purchaseId'=>$getPurchaseDataList->houdinv_purchase_products_purchase_id,'productTitle'=>$getvariantdata[0]->houdin_products_variants_title,'return'=>$getPurchaseDataList->houdinv_purchase_products_inward_quantity_return);
			}
		}
		return array('deductInventory'=>$setdeductInventorydata);
	}
	public function getproductVariantTotalCount()
	{
		// Product Count
		$getProductCount = $this->db->select('COUNT(*) AS produtcId')->from('houdinv_stock_transfer_log')->get()->result();
		return array('totalRows'=>$getProductCount[0]->produtcId);
	}
	public function fetchProductsDataStockTransfer($data)
	{
		$this->db->select('*')->from('houdinv_stock_transfer_log');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getProductDatadata = $this->db->get();
		$resultProductdata = ($getProductDatadata->num_rows() > 0)?$getProductDatadata->result():FALSE;

		// get warehouse list
		$getWareHouseList = $this->db->select('*')->from('houdinv_shipping_warehouse')->get()->result();
		return array('productList'=>$resultProductdata,'warehouselist'=>$getWareHouseList);
	}

	public function getproductVariantTotalCount1()
	{
		// Product Count
		$getProductCount = $this->db->select('COUNT(houdin_products_id) AS produtcId')->from('houdinv_products')->where('houdin_products_status','1')->get()->result();
		return array('totalRows'=>$getProductCount[0]->produtcId);
	}

	public function fetchProductsDataStockTransfer1($data)
	{
		$this->db->select('*')->from('houdinv_products')->where('houdin_products_status','1');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getProductDatadata = $this->db->get();
		$resultProductdata = ($getProductDatadata->num_rows() > 0)?$getProductDatadata->result():FALSE;

		// get warehouse list
		$getWareHouseList = $this->db->select('*')->from('houdinv_shipping_warehouse')->get()->result();
		return array('productList'=>$resultProductdata,'warehouselist'=>$getWareHouseList);
	}
 
	public function fetchProductsListData($warehouse_Id){
		 
            // get product data
			 $this->db->select('*')->from('houdinv_products');
			$this->db->where('houdin_products_status','1');
			$this->db->where('houdinv_products_total_stocks >','0');
			$where = "FIND_IN_SET('".$warehouse_Id."', houdin_products_warehouse)"; 
               $this->db->where( $where ); 
			$getProducts =$this->db->get()->result();
            foreach($getProducts as $getProductsList)
            {
                

                $getPriceData = json_decode($getProductsList->houdin_products_price,true);
                if($getPriceData['tax_price'])
                {
                    $getTax = $this->db->select('houdinv_tax_percenatge')->from('houdinv_taxes')->where('houdinv_tax_id',$getPriceData['tax_price'])->get()->result();
                    $setTax = round($getProductsList->houdin_products_final_price*$getTax[0]->houdinv_tax_percenatge)/100;
                }
                else
                {
                    $setTax = 0;
                }
                if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                {
                    $setFinalPrice = $getProductsList->houdin_products_final_price;
                }
                else
                {
                    $setFinalPrice = $getProductsList->houdin_products_final_price;
                }
                //$setPorudtcArray[] = array('productName'=>$getProductsList->houdin_products_title,'produtcId'=>$getProductsList->houdin_products_id,'variantId'=>0,
               // 'productPrice'=>$setFinalPrice,'stock'=>$getProductsList->houdinv_products_total_stocks,'tax_price'=>$getTax[0]->houdinv_tax_percenatge,'cp'=>$getPriceData['cost_price'],'mrp'=>$getPriceData['sale_price'],'tax_pricedata'=>$setTax);
            }
            // get variant data
            $getVariantData = $this->db->select('*')->from('houdinv_products_variants')->where('houdinv_products_variants_total_stocks >','0')->get()->result();
            foreach($getVariantData as $getVariantDataList)
            { 
                $mainproducnt_Id=$getVariantDataList->houdin_products_variants_product_id;
                $GetmanProducts = $this->db->select('*')->from('houdinv_products')->where('houdin_products_id',$mainproducnt_Id)->get()->row();
                      
                // fetch variant parent
                $getProducts = $this->db->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getVariantDataList->houdin_products_variants_product_id)->get()->result();
                $getPriceData = json_decode($getProducts[0]->houdin_products_price,true);
                if($getPriceData['tax_price'])
                {
                    $getTax = $this->db->select('houdinv_tax_percenatge')->from('houdinv_taxes')->where('houdinv_tax_id',$getPriceData['tax_price'])->get()->result();
                    $setTax = round($getVariantDataList->houdinv_products_variants_final_price*$getTax[0]->houdinv_tax_percenatge)/100;
                }
                else
                {
                    $setTax = 0;
                }
                if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                {
                    $setFinalPrice = $getVariantDataList->houdinv_products_variants_final_price;
                }
                else
                {
                    $setFinalPrice = $getVariantDataList->houdinv_products_variants_final_price;
                }
                $getVariantPrice = json_decode($getVariantDataList->houdinv_products_variants_new_price,true);
                
                $setPorudtcArray[] = array('productName'=>$GetmanProducts->houdin_products_title.' - '.$getVariantDataList->houdin_products_variants_title,'produtcId'=>$mainproducnt_Id,'variantId'=>$getVariantDataList->houdin_products_variants_id,
                'productPrice'=>$setFinalPrice,'stock'=>$getVariantDataList->houdinv_products_variants_total_stocks,'tax_price'=>$getTax[0]->houdinv_tax_percenatge,'cp'=>$getVariantPrice['cost_price'],'mrp'=>$getVariantPrice['sale_price'],'tax_pricedata'=>$setTax);
            }   
         
        return $setPorudtcArray;

	}


	public function fetchProductListData($data)
	{
		$getDataValue = "";
	/*	$getProductData = $this->db->select('*')->from('houdinv_products')->where('houdin_products_id',$data['productId'])->get()->result();
		$getProductQuantity = json_decode($getProductData[0]->houdinv_products_main_stock,true);
		for($productIndex = 0; $productIndex < count($getProductQuantity); $productIndex++)
		{
			if($getProductQuantity[$productIndex]['main'] == $data['warehouseId'])
			{
				$setStoreStock = $getProductQuantity[$productIndex]['value'];
				if($getDataValue)
				{
					$getDataValue = $getDataValue."".'<tr class="mainParentRow">
					<td><input type="checkbox" class="childStockCheck" data-product="'.$getProductData[0]->houdin_products_id.'" data-variant="0"/></td>
					<input type="hidden" name="productId[]" class="setProduct"/>
					<input type="hidden" name="variantId[]" class="setVariant"/>
					<td>Sanyo</td>
					<td><input type="text" readonly="readonly" name="quantity[]" data-max="'.$setStoreStock.'" class="form-control setQunatityData number_validation"/></td>
					</tr>';
				}
				else
				{
					$getDataValue ='<tr class="mainParentRow">
					<td><input type="checkbox" class="childStockCheck" data-product="'.$getProductData[0]->houdin_products_id.'" data-variant="0"/></td>
					<input type="hidden" name="productId[]" class="setProduct"/>
					<input type="hidden" name="variantId[]" class="setVariant"/>
					<td><a href="javascript:;" data-toggle="tooltip" title="'.$getProductData[0]->houdin_products_title.'">'.substr($getProductData[0]->houdin_products_title,0,5).'</a></td>
					<td><input type="text" readonly="readonly" name="quantity[]" data-max="'.$setStoreStock.'" class="form-control setQunatityData number_validation"/></td>
					</tr>';
				}    
			}
		}      */
		// Get variant data
		$getVariantData = $this->db->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_product_id',$data['productId'])->get()->result();
		if(count($getVariantData) > 0)
		{
			foreach($getVariantData as $getVariantDataList)
			{
				$getVariantQuantity = json_decode($getVariantDataList->houdin_products_variants_stock,true);
				for($variantIndex = 0; $variantIndex < count($getVariantQuantity); $variantIndex++)
				{
					
					if($getVariantQuantity[$variantIndex]['main'] == $data['warehouseId'])
					{
						$setStoreStock = $getVariantQuantity[$variantIndex]['value'];
						if($getDataValue)
						{
							$getDataValue = $getDataValue."".'<tr class="mainParentRow">
							<td><input type="checkbox" class="childStockCheck" data-product="0" data-variant="'.$getVariantDataList->houdin_products_variants_id.'"/></td>
							<input type="hidden" name="productId[]" class="setProduct"/>
							<input type="hidden" name="variantId[]" class="setVariant"/>
							<td><a href="javascript:;" data-toggle="tooltip" title="'.$getVariantDataList->houdin_products_variants_title.'">'.substr($getVariantDataList->houdin_products_variants_title,0,5).'</a></td>
							<td><input type="text" readonly="readonly" name="quantity[]" data-max="'.$setStoreStock.'" class="form-control setQunatityData number_validation"/></td>
							</tr>';
						}
						else
						{
							$getDataValue = '<tr class="mainParentRow">
							<td><input type="checkbox" class="childStockCheck" data-product="0" data-variant="'.$getVariantDataList->houdin_products_variants_id.'"/></td>
							<input type="hidden" name="productId[]" class="setProduct"/>
							<input type="hidden" name="variantId[]" class="setVariant"/>
							<td><a href="javascript:;" data-toggle="tooltip" title="'.$getVariantDataList->houdin_products_variants_title.'">'.substr($getVariantDataList->houdin_products_variants_title,0,5).'</a></td>
							<td><input type="text" readonly="readonly" name="quantity[]" data-max="'.$setStoreStock.'" class="form-control setQunatityData number_validation"/></td>
							</tr>';
						}
					}
				}
			}
		}
		if($getDataValue)
		{
			return $getDataValue;
		}
		else
		{
			return 'no';
		}
	}

	public function sendStocksData($data)
	{
		$setProductStockArray = array();
		$setVariantStockArray = array();
		$setMainArrayData = array();
	 $deveredtype=$data['Alldata'];
		for($index = 0; $index < count($data['productId']); $index++)
		{ 
			// print_r($data['productId'][$index]);
			$productId_And_varints= explode("||",$data['productId'][$index]);
			$productId=$productId_And_varints[0];
			  $varints_id=$productId_And_varints[1];

			
				// check product variant data
		 	$getVariantData = $this->db->select('houdin_products_variants_stock')->from('houdinv_products_variants')->where('houdin_products_variants_id',$varints_id)->get()->result();
				$getVariantStock = json_decode($getVariantData[0]->houdin_products_variants_stock,true);
				for($variantStock = 0; $variantStock < count($getVariantStock); $variantStock++)
				{
					if($getVariantStock[$variantStock]['main'] == $data['warehouseFrom'])
					{
						$getCurrentWarehouseQuantity = $getVariantStock[$variantStock]['value'];
						$getRemainingWarehouseQuantity = $getVariantStock[$variantStock]['value'] - $data['quantity'][$variantStock];
						$setVariantStockArray[] = array('main'=>$getVariantStock[$variantStock]['main'],'value'=>$getRemainingWarehouseQuantity); 
					}
					else if($getVariantStock[$variantStock]['main'] == $data['warehouseTo'])
					{
						$getCurrentWarehouseQuantity = $getVariantStock[$variantStock]['value'];
						$getRemainingWarehouseQuantity = $getVariantStock[$variantStock]['value'] + $data['quantity'][$variantStock];
						$setVariantStockArray[] = array('main'=>$getVariantStock[$variantStock]['main'],'value'=>$getRemainingWarehouseQuantity);
					}
					else
					{
						$setVariantStockArray[] = array('main'=>$getVariantStock[$variantStock]['main'],'value'=>$getVariantStock[$variantStock]['value']);
					}
				}
				$setupdateVariantArray = array('houdin_products_variants_stock'=>json_encode($setVariantStockArray));
				$this->db->where('houdin_products_variants_id',$varints_id);
				$this->db->update('houdinv_products_variants',$setupdateVariantArray);
			 
			$setMainArrayData[] = array('productId'=>$productId,'variantId'=>$varints_id,'quantity'=>$data['quantity'][$index],
			'invoiceno'=>$deveredtype['invoiceno'],'date'=>$deveredtype['date'],'Delivered_name'=>$deveredtype['Delivered_name'],'Delivered_date'=>$deveredtype['Delivered_date'],
			'Delivered_drivername'=>$deveredtype['Delivered_drivername'],'Delivered_vehiclenumber'=>$deveredtype['Delivered_vehiclenumber'],
			'Received_name'=>$deveredtype['Received_name'],'Received_date'=>$deveredtype['Received_date'],);
		 
		}

	 
		$setData = strtotime(date('Y-m-d'));
		$setStockLog = array('houdinv_stock_transfer_log_warehouse_from'=>$data['warehouseFrom'],'houdinv_stock_transfer_log_warehouse_to'=>$data['warehouseTo'],'houdinv_stock_transfer_log_productdetails'=>json_encode($setMainArrayData),'houdinv_stock_transfer_log_created_at'=>$setData);
		$this->db->insert('houdinv_stock_transfer_log',$setStockLog);
		return array('message'=>'success');

	}


	public function sendStockData($data)
	{
		$setProductStockArray = array();
		$setVariantStockArray = array();
		$setMainArrayData = array();
		for($index = 0; $index < count($data['productId']); $index++)
		{
			if($data['productId'][$index] != 0)
			{
				$getProductData = $this->db->select('houdinv_products_main_stock')->from('houdinv_products')->where('houdin_products_id',$data['productId'][$index])->get()->result();
				$getProductStock = json_decode($getProductData[0]->houdinv_products_main_stock,true);
				for($productStock = 0; $productStock < count($getProductStock); $productStock++)
				{
					// check product data
					if($getProductStock[$productStock]['main'] == $data['warehouseFrom'])
					{
						$getCurrentWarehouseQuantity = $getProductStock[$productStock]['value'];
						$getRemainingWarehouseQuantity = $getProductStock[$productStock]['value'] - $data['quantity'][$productStock];
						$setProductStockArray[] = array('main'=>$getProductStock[$productStock]['main'],'value'=>$getRemainingWarehouseQuantity); 
					}
					else if($getProductStock[$productStock]['main'] == $data['warehouseTo'])
					{
						$getCurrentWarehouseQuantity = $getProductStock[$productStock]['value'];
						$getRemainingWarehouseQuantity = $getProductStock[$productStock]['value'] + $data['quantity'][$productStock];
						$setProductStockArray[] = array('main'=>$getProductStock[$productStock]['main'],'value'=>$getRemainingWarehouseQuantity);
					}
					else
					{
						$setProductStockArray[] = array('main'=>$getProductStock[$productStock]['main'],'value'=>$getProductStock[$productStock]['value']);
					}
				}
				$setupdateProductArray = array('houdinv_products_main_stock'=>json_encode($setProductStockArray));
				$this->db->where('houdin_products_id',$data['productId'][$index]);
				$this->db->update('houdinv_products',$setupdateProductArray);
			}
			else
			{
				// check product variant data
				$getVariantData = $this->db->select('houdin_products_variants_stock')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data['variantId'][$index])->get()->result();
				$getVariantStock = json_decode($getVariantData[0]->houdin_products_variants_stock,true);
				for($variantStock = 0; $variantStock < count($getVariantStock); $variantStock++)
				{
					if($getVariantStock[$variantStock]['main'] == $data['warehouseFrom'])
					{
						$getCurrentWarehouseQuantity = $getVariantStock[$variantStock]['value'];
						$getRemainingWarehouseQuantity = $getVariantStock[$variantStock]['value'] - $data['quantity'][$variantStock];
						$setVariantStockArray[] = array('main'=>$getVariantStock[$variantStock]['main'],'value'=>$getRemainingWarehouseQuantity); 
					}
					else if($getVariantStock[$variantStock]['main'] == $data['warehouseTo'])
					{
						$getCurrentWarehouseQuantity = $getVariantStock[$variantStock]['value'];
						$getRemainingWarehouseQuantity = $getVariantStock[$variantStock]['value'] + $data['quantity'][$variantStock];
						$setVariantStockArray[] = array('main'=>$getVariantStock[$variantStock]['main'],'value'=>$getRemainingWarehouseQuantity);
					}
					else
					{
						$setVariantStockArray[] = array('main'=>$getVariantStock[$variantStock]['main'],'value'=>$getVariantStock[$variantStock]['value']);
					}
				}
				$setupdateVariantArray = array('houdin_products_variants_stock'=>json_encode($setVariantStockArray));
				$this->db->where('houdin_products_variants_id',$data['variantId'][$index]);
				$this->db->update('houdinv_products_variants',$setupdateVariantArray);
			}
			$setMainArrayData[] = array('productId'=>$data['productId'][$index],'variantId'=>$data['variantId'][$index],'quantity'=>$data['quantity'][$index]);
		}
		$setData = strtotime(date('Y-m-d'));
		$setStockLog = array('houdinv_stock_transfer_log_warehouse_from'=>$data['warehouseFrom'],'houdinv_stock_transfer_log_warehouse_to'=>$data['warehouseTo'],'houdinv_stock_transfer_log_productdetails'=>json_encode($setMainArrayData),'houdinv_stock_transfer_log_created_at'=>$setData);
		$this->db->insert('houdinv_stock_transfer_log',$setStockLog);
		return array('message'=>'success');
	}
	public function fetchManageInventory()
	{
		$getProductData = $this->db->select('houdin_products_id,houdin_products_title,houdinv_products_total_stocks,houdinv_products_main_images')->from('houdinv_products')->where('houdin_products_status','1')->get()->result(); 
		// $getVariantData = $this->db->select('houdin_products_variants_title,houdinv_products_variants_total_stocks,houdin_products_variants_image')->from('houdinv_products_variants')->get()->result(); 
		
		
		//$getFinalData = array_merge($getProductData,$getVariantData);
		return $getProductData;
	}

	public function fetchVarintInventory($productId)
	{
		//$getProductData = $this->db->select('houdin_products_title,houdinv_products_total_stocks,houdinv_products_main_images')->from('houdinv_products')->where('houdin_products_status','1')->get()->result(); 
	  $getVariantData = $this->db->select('houdin_products_variants_title,houdinv_products_variants_total_stocks,houdin_products_variants_image')->from('houdinv_products_variants')->where('houdin_products_variants_product_id',$productId)->get()->result(); 
		
		
		//$getFinalData = array_merge($getProductData,$getVariantData);
		return $getVariantData;
	}

	
}  
