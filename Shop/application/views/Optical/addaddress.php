<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>
   <!-- About Banner Start -->

   <section id="about">

       <div class="container">

           <div class="row">

               <div class="about-heading text-center">

                   <h2>New Address</h2>

               </div>

           </div>

       </div>

   </section>

<!-- About Banner End -->



<!-- my account part Start -->

<section id="account">

    <div class="container">

        <div class="row">

          <?php
          if($this->session->flashdata('error'))
          {
              echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
          }
          else if($this->session->flashdata('success'))
          {
              echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
          }
          ?>

            <div class="col-md-3">

            </div>

            <div class="col-md-6">

                <div class="login-account">
                    <h3>New address</h3>
                    <p>Add your new address.</p>
                    <hr>
                    <?php echo form_open(base_url( 'Address/adduseraddress' ), array( 'id' => 'addaddress', 'method'=>'post' ));?>

                    <div class="form-group">

                    <input name="username" placeholder="Name" class="form-control required_validation_for_address name_validation" type="text">

                    </div>

                    <div class="form-group">

                      <input name="userphone" placeholder="Phone number" class="form-control required_validation_for_address name_validation" type="text">

                    </div>

                    <div class="form-group">

                      <select class="form-control required_validation_for_address" name="usercountry">
                      <option value="">Choose Country</option>
                      <option value="<?php echo $countryList[0]->houdin_user_shop_country ?>"><?php echo $countryList[0]->houdin_country_name ?></option>
                      </select>

                    </div>

                    <div class="form-group">
                        <textarea rows="3" name="usermainaddress" placeholder="Street address. Apartment, suite, unit etc. (optional)" class="form-control required_validation_for_address name_validation"></textarea>
                    </div>

                    <div class="form-group">

                    <input name="usercity" placeholder="Town/City*" class="form-control required_validation_for_address name_validation" type="text">

                    </div>

                    <div class="form-group">

                      <input name="userpincode" placeholder="Post code / Zip" class="form-control required_validation_for_address name_validation" type="text">

                    </div>


                   <div class="procd">

                    <input type="submit" class="btn btn-primary acc_btn" value="Add Address"/>

                    </div>

                    <?php echo form_close(); ?>

                </div>

            </div>



        </div>

    </div>

</section>

<!-- my account part End -->

  <!-- Footer Part Start -->
  <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer'); ?>

  <script type="text/javascript">
  $(document).ready(function(){
      $(document).on('submit','#addaddress',function(){
          var check_required_field='';
          $(this).find(".required_validation_for_address").each(function(){
              var val22 = $(this).val();
              if (!val22){
                  check_required_field =$(this).size();
                  $(this).css("border-color","#ccc");
                  $(this).css("border-color","red");
              }
              $(this).on('keypress change',function(){
                  $(this).css("border-color","#ccc");
              });
          });
          if(check_required_field)
          {
              return false;
          }
          else {
              return true;
          }
      });
  });
  </script>
