<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Forget extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Vendorloginmodel');
        $this->load->library('form_validation');
    }
    public function index()
    {
        if($this->session->userdata('tempCode') == "")
        {
            $this->session->flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // update forget pass
        if($this->input->post('updateForgetPass'))
        {
            $this->form_validation->set_rules('confirmationCode','text','required|exact_length[5]|integer');
            $this->form_validation->set_rules('password','text','required');
            $this->form_validation->set_rules('cpassword','text','required');
            if($this->form_validation->run() == true)
            {
                $getPin = $this->input->post('confirmationCode');
                $getOrginalPassword = $this->input->post('password');
                $getRePassword = $this->input->post('cpassword');
                if($getPin == $this->session->userdata('tempCode'))
                {
                    if($getOrginalPassword == $getRePassword)
                    {
                        $setArrayData = array('pin'=>$getPin,'passwordData'=>$getOrginalPassword);
                        $getUpdatePassStatus = $this->Vendorloginmodel->updateUserPass($setArrayData);
                        if($getUpdatePassStatus['message'] == 'pin')
                        {
                            $this->session->set_flashdata('error','Please enter correct pin');
                            $this->load->helper('url');
                            redirect(base_url()."forget", 'refresh');

                        }
                        else if($getUpdatePassStatus['message'] == 'yes')
                        {
                            $this->session->unset_userdata('tempCode');
                            $this->session->set_flashdata('success','Your password updated successfully');
                            $this->load->helper('url');
                            redirect(base_url(), 'refresh');
                        }
                        else if($getUpdatePassStatus['message'] == 'no')
                        {
                            $this->session->set_flashdata('error','Something went wrong. Please try again');
                            $this->load->helper('url');
                            redirect(base_url()."forget", 'refresh');
                        }
                        else
                        {
                            $this->session->set_flashdata('error','Something went wrong. Please try again');
                            $this->load->helper('url');
                            redirect(base_url()."forget", 'refresh');
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('error','Passwords are not matched');
                        $this->load->helper('url');
                        redirect(base_url()."forget", 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','Please enter correct pin');
                    $this->load->helper('url');
                    redirect(base_url()."forget", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fileds are required');
                $this->load->helper('url');
                redirect(base_url()."forget", 'refresh');
            }
        }
        $this->load->view('forget');
    }
}
