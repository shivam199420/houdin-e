<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Setting extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->load->library('form_validation'); 
        $this->load->model('Settingmodel');
        $this->load->library('Pdf');
        $this->perPage = 100;
        $this->auth_users->is_logged_in();  
         $this->load->helper('url'); 
	}
    public function compnaydetails()
    {
        if($this->input->post('save'))
        {
            $this->form_validation->set_rules('title','Shop title','required');
            $this->form_validation->set_rules('business_name','Business name','required');
            $this->form_validation->set_rules('shop_Contact','Shop contact','required');
            $this->form_validation->set_rules('shop_communication_email','Shop communication email','required');
            $this->form_validation->set_rules('shop_order_email','Shop order email','required');
            $this->form_validation->set_rules('shop_customer_email','Shop customer email','required');
            if($this->form_validation->run() != true)
            {
                $this->session->set_flashdata('message_name',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."Setting/compnaydetails", 'refresh');
            }
            if(!$this->input->post('ssl'))
            {
                $ssl='';
            }
            else
            {
                $ssl= $this->input->post('ssl');
            }
            $date= strtotime(date("Y-m-d"));
            $array = array(
            "houdinv_shop_title"=>$this->input->post('title'),
            "houdinv_shop_business_name"=>$this->input->post('business_name'),
            "houdinv_shop_address"=>$this->input->post('shop_address'),
            "houdinv_shop_contact_info"=>$this->input->post('shop_Contact'),
            "houdinv_shop_communication_email"=>$this->input->post('shop_communication_email'),
            "houdinv_shop_order_email"=>$this->input->post('shop_order_email'),
            "houdinv_shop_customer_care_email"=>$this->input->post('shop_customer_email'),
            "houdinv_shop_pan"=>$this->input->post('shop_pan'),
            "houdinv_shop_tin"=>$this->input->post('shop_tin'),
            "houdinv_shop_cst"=>"cst",
            "houdinv_shop_vat"=>$this->input->post('shop_vat'),
            "houdinv_shop_custom_domain"=>"custom domain",
            "houdinv_shop_ssl"=>$ssl,
            "houdinv_shop_updated_date"=>$date
            );
            $result =   $this->Settingmodel->AddShopInfo($array);
            if($result)
            {
                // upload vendor profile
                if(!empty($_FILES['name']['name']))
                {
                    $newFileName = rand(100000,999999)."".$_FILES['name']['name'];
                    $config['upload_path'] = "upload/vendorprofile/";
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $config['max_size'] = '6144';
                    $config['file_name'] = $newFileName;
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('name'))
                    {
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name']; 
                        $getData = $this->Settingmodel->AddvendorProfile($picture);
                        $this->session->set_flashdata($getData['response'],$getData['message']);
                        $this->load->helper('url');
                        redirect(base_url()."Setting/compnaydetails", 'refresh');
                    }
                    else
                    {
                        $this->session->set_flashdata('message_name',"Data update successfully.".$this->upload->display_errors()."");
                        $this->load->helper('url');
                        redirect(base_url()."Setting/compnaydetails", 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('success',"Data update successfully");
                    $this->load->helper('url');
                    redirect(base_url()."Setting/compnaydetails", 'refresh');
                } // End Here.      
            }
            else
            {
                $this->session->set_flashdata('message_name',"Somthing went wrong ! try again later");
                $this->load->helper('url');
                redirect(base_url()."Setting/compnaydetails", 'refresh');  
            }
        }
        $fg['all_data'] =   $this->Settingmodel->fetchShopData();
        $this->load->view('addcompnaydetails',$fg);    
    }
    public function ordersetting()
    {
       
        // update order setting
        if($this->input->post('insertOrderConfiguration'))
        {
            $this->form_validation->set_rules('cancellationPeriod','text','integer');
            $this->form_validation->set_rules('returnPeriod','text','integer');
            $this->form_validation->set_rules('replacePeriod','text','integer');
            if($this->form_validation->run() == true)
            {
                $setArrayData = array('cancel'=>$this->input->post('cancellationPeriod'),'return'=>$this->input->post('returnPeriod'),'replace'=>$this->input->post('replacePeriod'),'allow_printing'=>$this->input->post('allow_printing'),'sms_email'=>$this->input->post('sms_email'));
                $getUpdateStatus = $this->Settingmodel->updateOrderConfig($setArrayData);
                if($getUpdateStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success',"Order configuration updated successfully");
                    $this->load->helper('url');
                    redirect(base_url()."Setting/ordersetting", 'refresh');  
                }
                else
                {
                    $this->session->set_flashdata('error',"Something went wrong.Please try again");
                    $this->load->helper('url');
                    redirect(base_url()."Setting/ordersetting", 'refresh');  
                }
            }
            else
            {
                $this->session->set_flashdata('error',"Please use integer value");
                
                redirect(base_url()."Setting/ordersetting", 'refresh');  
            }
        }
        $getOrderConfiguration = $this->Settingmodel->fetchOrderConfigData();
        $this->load->view('addordersetting',$getOrderConfiguration);
    }
    public function customersetting()
    { $getcustomersetting['customersetting'] = $this->Settingmodel->fetchstoreCustomerSettingRecord();

        $this->load->view('addcustomersetting',$getcustomersetting);
    }

public function customersetting_save()
{
   //is_unique check the unique email value in users table
         $this->form_validation->set_rules('pay_panding_amount', 'Pay pending payment Amount', 'trim|required|numeric');
         $this->form_validation->set_rules('pay_panding_period', 'Pay pending payment Period', 'trim|required|numeric');
         //$this->form_validation->set_rules('testimonials_image', 'Image', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="pay_panding_customer";         
                $this->load->view('models_',$reg_pup);            
        }else{ 

             $postData_shipping = $this->input->post(); 

          
                  $pay_panding_amount= $postData_shipping['pay_panding_amount'];
                  $pay_panding_period= $postData_shipping['pay_panding_period'];
                  $allow_pay_panding= $postData_shipping['allow_pay_panding'];
                  $update_id= $postData_shipping['update_id'];
           
           
              $insert_data=array('id'=>$update_id,'pay_panding_amount' =>$pay_panding_amount,'pay_panding_period' =>$pay_panding_period,'allow_pay_panding'=>$allow_pay_panding,'date_time'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Settingmodel->save_customersetting($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Customer Setting Successfully Save');        
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
            }



            }
}

    public function inventorysetting()
    { $getinventorysetting['inventorysetting'] = $this->Settingmodel->fetchinventorysettingRecord();

        $this->load->view('addinventorysetting',$getinventorysetting);
    }

public function  inventorysetting_save()
{
   //is_unique check the unique email value in users table
         $this->form_validation->set_rules('inventorysetting_number', 'Tax Number', 'trim|required|numeric');
        // $this->form_validation->set_rules('pay_panding_period', 'Pay pending payment Period', 'trim|required|numeric');
         //$this->form_validation->set_rules('testimonials_image', 'Image', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="inventorysetting";         
                $this->load->view('models_',$reg_pup);            
        }else{ 

             $postData_shipping = $this->input->post(); 

          
                  $inventorysetting_number= $postData_shipping['inventorysetting_number'];  
                  $Universal_rl= $postData_shipping['Universal_rl'];                
                  $update_id= $postData_shipping['update_id'];
           
           
              $insert_data=array('id'=>$update_id,'inventorysetting_number' =>$inventorysetting_number,'Universal_rl'=>$Universal_rl,'date_time'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Settingmodel->save_inventorysetting($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Inventory Successfully Save');        
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
            }
            }
}


    public function taxsetting()
    {
        $gettaxsetting['taxsetting'] = $this->Settingmodel->fetchtaxsettingRecord();
        $gettaxsetting['fetchtax'] = $this->Settingmodel->fetchtaxData();

        $this->load->view('addtaxsetting',$gettaxsetting);
    }
    public function addnewtax()
    {
        $this->form_validation->set_rules('taxname','tax name','required');
        $this->form_validation->set_rules('taxpercentage','tax percentage','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('houdinv_tax_name'=>$this->input->post('taxname'),'houdinv_tax_percenatge'=>$this->input->post('taxpercentage'));
            $getTaxResponse = $this->Settingmodel->addNewTaxData($setArray,$this->input->post('taxId'));
            if($getTaxResponse)
            {
                $this->session->set_flashdata('success','Tax addedd successfully');
                redirect(base_url()."Setting/taxsetting",'refresh');    
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url()."Setting/taxsetting",'refresh');    
            }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."Setting/taxsetting",'refresh');
        }
    }
    public function deletetax()
    {
        $this->form_validation->set_rules("deleteTaxId",'text','required');
        if($this->form_validation->run() == true)
        {
            $getDeleteRespnse = $this->Settingmodel->deleteTax($this->input->post('deleteTaxId'));
            if($getDeleteRespnse)
            {
                $this->session->set_flashdata('success','Tax deleted successfully');
                redirect(base_url()."Setting/taxsetting",'refresh');    
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url()."Setting/taxsetting",'refresh');    
            }
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(base_url()."Setting/taxsetting",'refresh');
        }
    }
public function taxsetting_save()
{
   //is_unique check the unique email value in users table
         $this->form_validation->set_rules('taxsetting_number', 'Tax Number', 'trim|required');
        // $this->form_validation->set_rules('pay_panding_period', 'Pay pending payment Period', 'trim|required|numeric');
         //$this->form_validation->set_rules('testimonials_image', 'Image', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="taxsetting";         
                $this->load->view('models_',$reg_pup);            
        }else{ 

             $postData_shipping = $this->input->post(); 

          
                  $taxsetting_number= $postData_shipping['taxsetting_number'];                  
                  $update_id= $postData_shipping['update_id'];
           
           
              $insert_data=array('id'=>$update_id,'taxsetting_number' =>$taxsetting_number,'date_time'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Settingmodel->save_taxsetting($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Tax Number Successfully Save');        
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
            }



            }
}

    public function skusetting()
    {
         $getskusetting['skusetting'] = $this->Settingmodel->fetchskusettingRecord();

        $this->load->view('addskusetting',$getskusetting);
    }

public function skusetting_save()
{
   //is_unique check the unique email value in users table
         $this->form_validation->set_rules('skusetting_number', 'SKU Number', 'trim|required');
         
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="skusetting";         
                $this->load->view('models_',$reg_pup);            
        }else{ 

             $postData_shipping = $this->input->post(); 

          
                  $skusetting_number= $postData_shipping['skusetting_number'];  

                $data_function=$this->Settingmodel->fetchskusettingRecord();
                  
   
                  $update_id= $data_function[0]->id;
                  
           
              $insert_data=array('id'=>$update_id,'skusetting_number' =>$skusetting_number,'date_time'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Settingmodel->save_skusetting($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','SKU Number Successfully Save');        
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
            }



            }
}


public function possetting(){
 if($this->input->post()){
 $this->form_validation->set_rules('minimum_order_amount', 'Minimum Order Number', 'trim|required');
 $this->form_validation->set_rules('delivery_charges', 'Delivery Charges', 'trim|required');
         
if ($this->form_validation->run() == FALSE){ 

      $getpossetting['possetting'] = $this->Settingmodel->fetchpossettingRecord();

   $this->load->view('addpossetting',$getpossetting);         
        }else{ 

           $postData_shipping = $this->input->post(); 

          
                  $minimum_order_amount= $postData_shipping['minimum_order_amount'];                  
                  $delivery_charges= $postData_shipping['delivery_charges'];

                  $free_delivery= $postData_shipping['free_delivery'];
                  $free_delivery_val= $postData_shipping['free_delivery_val'];
                  $bill_out_stock= $postData_shipping['bill_out_stock'];
                  $inventory_status= $postData_shipping['inventory_status'];
                  $net_weight_content= $postData_shipping['net_weight_content'];
                  $adjustment_during= $postData_shipping['adjustment_during'];
                  $allow_sp_desc= $postData_shipping['allow_sp_desc'];
                  $serial_number= $postData_shipping['serial_number'];
                  $billing_custom_product= $postData_shipping['billing_custom_product'];
                  $mobile_verify= $postData_shipping['mobile_verify'];
                  $accept_payment= $postData_shipping['accept_payment'];
                  $same_barcode= $postData_shipping['same_barcode'];
                  $same_mrp= $postData_shipping['same_mrp'];
                  $fifo=$postData_shipping['fifo'];
                  $a_b=$postData_shipping['a_b'];
                $update_id=  $this->Settingmodel->fetchpossettingRecord();
           
              $insert_data=array('id'=>$update_id[0]->id,'minimum_order_amount' =>$minimum_order_amount,'delivery_charges'=>$delivery_charges,'free_delivery'=>$free_delivery,'free_delivery_val'=>$free_delivery_val,'bill_out_stock'=>$bill_out_stock,'inventory_status'=>$inventory_status,'net_weight_content'=>$net_weight_content,'adjustment_during'=>$adjustment_during,'allow_sp_desc'=>$allow_sp_desc,'serial_number'=>$serial_number,'billing_custom_product'=>$billing_custom_product,'mobile_verify'=>$mobile_verify,'accept_payment'=>$accept_payment,'same_barcode'=>$same_barcode,'same_mrp'=>$same_mrp,'fifo'=>$fifo,'a_b'=>$a_b,'date_time'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Settingmodel->save_addpossetting($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','PosSetting Successfully Save');        
             
            redirect(base_url()."Setting/possetting", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
            }

          }

 }else{
  $getpossetting['possetting'] = $this->Settingmodel->fetchpossettingRecord();

   $this->load->view('addpossetting',$getpossetting);
 }

       
    }


    
    public function resetaccountsetting()
    {
        $this->load->view('addresetaccountsetting');
    }
public function invoicesetting(){
      if($this->input->post()){

      $postData_analytics = $this->input->post(); 

      $show_task_breakup= $postData_analytics['show_task_breakup'];
      $total_saving_invoice= $postData_analytics['total_saving_invoice'];
      $invoice_number_receipt= $postData_analytics['invoice_number_receipt'];
      $order_number_receipt= $postData_analytics['order_number_receipt'];
      $product_wise= $postData_analytics['product_wise'];
      $invoice_num_start= $postData_analytics['invoice_num_start'];


      $update_id=  $this->Settingmodel->fetchinvoicesettingRecord();

      $insert_data=array('id'=>$update_id[0]->id,'show_task_breakup' =>$show_task_breakup,'total_saving_invoice'=>$total_saving_invoice,'invoice_number_receipt'=>$invoice_number_receipt,'order_number_receipt'=>$order_number_receipt,'product_wise'=>$product_wise,'invoice_num_start'=>$invoice_num_start,'date_time'=>strtotime(date('Y-m-d')));

      $file_id = $this->Settingmodel->save_invoicesetting($insert_data);
      if($file_id)
      {$this->session->set_flashdata('success','Invoice Setting Successfully Save');        

      redirect(base_url()."Setting/invoicesetting", 'refresh');
      }else{                
      $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
      }


      }else{
      $getinvoicesetting['invoicesetting'] = $this->Settingmodel->fetchinvoicesettingRecord();

      $this->load->view('addinvoicesetting',$getinvoicesetting);
      }

        
 }
     
        public function memberShiptype()
    {
        $this->load->view('setting_membership_type');
    }

      public function storesetting()
    {
    	$Getstoresetting['storesetting']=$this->Settingmodel->fetchstoresettingRecord();
        $this->load->view('storesetting',$Getstoresetting);
    }

    public function storesettingAdd()
    {
    	  if($this->input->post()){

      $postData_storesetting = $this->input->post(); 

      $display_out_stock= $postData_storesetting['display_out_stock'];
      $receieve_order_out= $postData_storesetting['receieve_order_out'];
      $custom_html_footer= $postData_storesetting['custom_html_footer'];
      $add_to_cart_button= $postData_storesetting['add_to_cart_button'];
      $email_mandatory= $postData_storesetting['email_mandatory'];
      $verify_mobile_numer= $postData_storesetting['verify_mobile_numer'];
      $otp_= $postData_storesetting['otp_'];
      $otp_setting= $postData_storesetting['otp_setting'];

      $update_id=  $this->Settingmodel->fetchstoresettingRecord();

      $insert_data=array('id'=>$update_id[0]->id,'display_out_stock' =>$display_out_stock,'receieve_order_out'=>$receieve_order_out,'custom_html_footer'=>$custom_html_footer,'add_to_cart_button'=>$add_to_cart_button,'email_mandatory'=>$email_mandatory,'verify_mobile_numer'=>$verify_mobile_numer,'otp_'=>$otp_,'otp_setting'=>$otp_setting,'date_time'=>strtotime(date('Y-m-d')));

      $file_id = $this->Settingmodel->save_storesetting($insert_data);
      if($file_id)
      {$this->session->set_flashdata('success','Store Setting Successfully Save');        

      redirect(base_url()."Setting/storesetting");
      }else{                
      $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
      }


      }else{
      	$this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
redirect(base_url()."Setting/storesetting",'');
      }
    }
     public function deliverysetting()
    {
        $this->load->view('adddeliverysetting');
    }
     
      public function smsemail()
    {
 
	 $Getsmsemail['sms_pakege']=$this->Settingmodel->fetchsmsemailRecord();
       $Getsmsemail['email_pakege']=$this->Settingmodel->fetchsms_emailRecord();
      $Getsmsemail['my_sms_remaing']= $this->Settingmodel->fetch_smsemail_Record('sms');
      $Getsmsemail['my_email_remaing']= $this->Settingmodel->fetch_smsemail_Record('email');
        $this->load->view('smsemail',$Getsmsemail);
    }

    public function buysmsadd()
    {

    	  if($this->input->post()){
    	  	$this->form_validation->set_rules('sms_pakage', 'Please Select Package ', 'trim|required');
         
        if ($this->form_validation->run() == FALSE){ 
                  	$this->session->set_flashdata('error',validation_errors());
redirect(base_url()."Setting/smsemail",''); 
        }else{

      $postData_storesetting = $this->input->post(); 

      $sms_pakage= $postData_storesetting['sms_pakage'];
      $pakage_data= $this->Settingmodel->fetchsmsemailRecord($sms_pakage);
      $getShopDetails = fetchVendorDetails();
    $this->session->set_userdata('smsPackage',$pakage_data);
    $this->session->set_userdata('productType','sms');
    $this->session->set_userdata('userdata',$getShopDetails); 
    redirect(base_url()."Setting/payyoumoney");

//     $pakage_datas=$pakage_data[0]->houdin_sms_package_sms_count; 
          
  
//       $update_id=  $this->Settingmodel->fetch_smsemail_Record('sms');
//    $total_credit=$update_id[0]->houdinv_emailsms_stats_total_credit;
//     $remaining_credit=$update_id[0]->houdinv_emailsms_stats_remaining_credits;
 
//  $total_credits=$total_credit+$pakage_datas;
//  $remaining_credits=$remaining_credit+$pakage_datas;


//       $insert_data=array('houdinv_emailsms_stats_id'=>$update_id[0]->houdinv_emailsms_stats_id,'houdinv_emailsms_stats_type' =>'sms','houdinv_emailsms_stats_total_credit'=>$total_credits,'houdinv_emailsms_stats_remaining_credits'=>$remaining_credits,'date_time'=>strtotime(date('YmdHis')));

//       $file_id = $this->Settingmodel->save_smsemail($insert_data);
//       if($file_id)
//       {$this->session->set_flashdata('success','SMS Add Successfully');        

//       redirect(base_url()."Setting/smsemail");
//       }else{                
//       $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
//       }
   }

      }else{
      	$this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
redirect(base_url()."Setting/smsemail",'');
      }
    }
    public function buyemailadd()
    {
    	if($this->input->post()){
    	  	$this->form_validation->set_rules('email_pakeges', 'Please Select Package ', 'trim|required');
         
        if ($this->form_validation->run() == FALSE){ 
                  	$this->session->set_flashdata('error',validation_errors());
redirect(base_url()."Setting/smsemail",''); 
        }else{

      $postData_storesetting = $this->input->post(); 

      $sms_pakage= $postData_storesetting['email_pakeges'];
      $pakage_data= $this->Settingmodel->fetchsms_emailRecord($sms_pakage);
      $getShopDetails = fetchVendorDetails();
      $this->session->set_userdata('smsPackage',$pakage_data);
      $this->session->set_userdata('productType','email');
      $this->session->set_userdata('userdata',$getShopDetails); 
      redirect(base_url()."Setting/payyoumoney");
 
   }

      }else{
      	$this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
redirect(base_url()."Setting/smsemail",'');
      }
    }



    public function smslogs()
    {
        // Custom Pagination
        $getTotalCount = $this->Settingmodel->getSMSTotalCount();
        $config['base_url']    = base_url().'Setting/smslogs/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getSMS = $this->Settingmodel->getsmslog($setArray);
        $this->load->view('smslogs',$getSMS);
    }
     
    public function emaillogs()
    {
        // Custom Pagination
        $getTotalCount = $this->Settingmodel->getemailTotalCount();
        $config['base_url']    = base_url().'Setting/emaillogs/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getEmail = $this->Settingmodel->getEmailLog($setArray);
        $this->load->view('emaillogs',$getEmail);
    }
    public function analytics()
    {

 if($this->input->post()){
 $this->form_validation->set_rules('analytics_number', 'Google Analytics Number', 'trim|required');
  
if ($this->form_validation->run() == FALSE){ 

      $getanalytics['analytics'] = $this->Settingmodel->fetchanalyticsRecord();

   $this->load->view('analytics',$getanalytics);         
        }else{ 

           $postData_analytics = $this->input->post(); 

          
                  $analytics_number= $postData_analytics['analytics_number'];                  
                  
                $update_id=  $this->Settingmodel->fetchanalyticsRecord();
           
              $insert_data=array('id'=>$update_id[0]->id,'analytics_number' =>$analytics_number,'date_time'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Settingmodel->save_analytics($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Google Analytics Successfully Save');        
             
            redirect(base_url()."Setting/analytics", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
            }

          }

 }else{
  $getanalytics['analytics'] = $this->Settingmodel->fetchanalyticsRecord();

   $this->load->view('analytics',$getanalytics);
 }


    }
    public function payyoumoney()
    {
        $this->input->post();
        if($this->input->post())
        {
            // print_r($this->input->post());
            // exit(); 
        }
        $this->load->view('payumoney');
    }
    public function payumoneyresponse()
    {
        $this->session->unset_userdata('smsPackage');
        $this->session->unset_userdata('userdata');
        $this->session->unset_userdata('productType');
        if($_POST['txnStatus'] == 'SUCCESS')
        {
            $setSMSArray = array('price'=>$this->session->userdata('price'),'package'=>$this->session->userdata('package'),'product'=>$this->session->userdata('type'));
            if($this->session->userdata('type') == 'sms')
            {
                $setOrderType = 'sms package';
            }
            else
            {
                $setOrderType = 'email package';
            }
            $setTransactionArray = array('houdinv_transaction_type'=>'debit','houdinv_transaction_for'=>$setOrderType,
            'houdinv_transaction_method'=>'online','houdinv_transaction_amount'=>$_POST['amount'],'houdinv_transaction_date'=>date('Y-m-d'),
            'houdinv_transaction_status'=>'success','houdinv_transaction_transaction_id'=>$_POST['txnid'],'houdinv_transaction_from'=>'payumoney');
            $getSuccessStatus = $this->Settingmodel->updateSMS($setSMSArray,$setTransactionArray);
            if($getSuccessStatus['message'] == 'yes')
            {
                $this->session->unset_userdata('price');
                $this->session->unset_userdata('package');
                $this->session->unset_userdata('type');
                $this->session->set_flashdata('success','Your package subscription is success');        
                redirect(base_url()."setting/smsemail", 'refresh');
            }
            else
            {
                $this->session->unset_userdata('price');
                $this->session->unset_userdata('package');
                $this->session->unset_userdata('type');
                $this->session->set_flashdata('error','Something went wrong. Please contact to support with your transaction id: '.$_POST['txnid'].'');        
                redirect(base_url()."setting/smsemail", 'refresh');
            }
        }
        else
        {
            if($this->session->userdata('type') == 'sms')
            {
                $setOrderType = 'sms package';
            }
            else
            {
                $setOrderType = 'email package';
            }
            $setTransactionArray = array('houdinv_transaction_type'=>'debit','houdinv_transaction_for'=>$setOrderType,'houdinv_transaction_method'=>'online',
            'houdinv_transaction_amount'=>$_POST['amount'],'houdinv_transaction_date'=>date('Y-m-d'),'houdinv_transaction_status'=>'fail','houdinv_transaction_transaction_id'=>$_POST['txnid'],'houdinv_transaction_from'=>'payumoney');
            $this->Settingmodel->updateFailureTransaction($setTransactionArray);
            $this->session->unset_userdata('price');
            $this->session->unset_userdata('package');
            $this->session->unset_userdata('type');
            $this->session->set_flashdata('error','Transaction failed. Please try again');        
            redirect(base_url()."setting/smsemail", 'refresh');
        }
    }
public function previewinvoice()
    {
        $getLogo = fetchvedorsitelogo();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Invoice Performa');
    $pdf->SetSubject('Houdin-e - Invoice Performa');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$invoicesetting=$this->Settingmodel->fetchinvoicesettingRecord();
$invoicesettings=$invoicesetting[0];
//print_r($invoicesettings);
 if($invoicesettings->show_task_breakup=='1'){
    $show_task_breakup='checked';
  }else{
    $show_task_breakup='';
  }
  if($invoicesettings->total_saving_invoice=='1'){
    $total_saving_invoice='checked';
  }else{
    $total_saving_invoice='';
  }
  if($invoicesettings->invoice_number_receipt=='1'){
    $invoice_number_receipt='checked';
  }else{
    $invoice_number_receipt='';
  }
  if($invoicesettings->order_number_receipt=='1'){
    $order_number_receipt='<p style="font-size:12px;text-align:right;">Order ID : #12523652</p>';
  }else{
    $order_number_receipt='';
  }
  if($invoicesettings->product_wise=='1'){
    $product_wise='checked';
  }else{
    $product_wise='';
  } 

$html = <<<EOD

<tbody><tr style="text-align:center;"><td><img src="$getLogo" width="229px"></td></tr>
</tbody></table>
<br>  
<br>
<table style="width:100%;">
<tbody><tr>
<td style="float: left;width: 60%;">
<h5>Customer Details</h5>
<p style="font-size:12px;">Surbhi Sethi </p>
<p style="font-size:12px;">Mobile : +91 12345-6789</p>
<p style="font-size:12px;">Email : info@gmail.com</p>


<p style="font-size:12px;">Address : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</p>
</td>
<td style="float: left;width: 35%;">
<h5 style="text-align:right;">Order Details</h5>
$order_number_receipt
<p style="font-size:12px;text-align:right;">Order Status : Shipped</p>
<p style="font-size:12px;text-align:right;">Sales channel : Web</p>
<p style="font-size:12px;text-align:right;">Date : 15 Aug 2016</p>
<p style="font-size:12px;text-align:right;">Time : 15:00:00</p>
</td>
</tr>
</tbody></table>
<br>
<br>
<table style="width:100%;" cellpadding="10">
<tbody><tr>
<td style="border-bottom:1px solid #000;font-size:12px;font-weight:bold;">Order ID</td>
<td style="border-bottom:1px solid #000;font-size:12px;font-weight:bold;">Order Name</td>
<td style="border-bottom:1px solid #000;font-size:12px;font-weight:bold;">QTY</td>
<td style="border-bottom:1px solid #000;font-size:12px;font-weight:bold;">Price</td>
<td style="border-bottom:1px solid #000;font-size:12px;font-weight:bold;">Total Price</td>
</tr>
<tr>
<td style="border-bottom:1px solid #000;font-size:12px;">#mfvv44</td>
<td style="border-bottom:1px solid #000;font-size:12px;">Jeans</td>
<td style="border-bottom:1px solid #000;font-size:12px;">3</td>
<td style="border-bottom:1px solid #000;font-size:12px;">8500</td>
<td style="border-bottom:1px solid #000;font-size:12px;">2202</td>
</tr>
<tr>
<td colspan="4" style="text-align:right;font-size:12px;">Delivery Charges</td>
<td style="font-size:12px;">1212</td>
</tr>
<tr>
<td colspan="4" style="text-align:right;border-bottom:1px solid #000;font-size:12px;">Extra GST</td>
<td style="border-bottom:1px solid #000;font-size:12px;">1212</td>
</tr>
<tr>
<td colspan="4" style="text-align:right;font-size:12px;">Net payable</td>
<td style="font-size:12px;">1121</td>
</tr>
</tbody></table>
<br>
<br>
<table style="width:100%;"><tbody><tr><td style="font-size:10px;"><b>Note:</b>&nbsp;incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit</td></tr></tbody></table>
</body>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_invoice_performa.pdf', 'I');
    }
     

}
