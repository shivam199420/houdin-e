<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Checkout extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        if($this->session->userdata('userAuth') == "")
        {
            redirect(base_url()."register", 'refresh');
        }
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
        $this->from = fromemail();
        $this->load->model('Cartmodel');
        $this->load->model('ShopCheckoutmodel');
        $this->load->model('Shopuserprofile');
        $this->alwaysCheck = array(
                                    array("field"=>"customRadio",
                                       "label"=>"Payment method",
                                       "rules"=>"required"),
                                       
                                       
                                    array("field"=>"order_delivery_type",
                                       "label"=>"order delivery type",
                                       "rules"=>"required")         
                                 
                                 );
        $this->billing_validation = array(
                                array("field"=>"billing_first_name",
                                      "label"=>"billing first name",
                                      "rules"=>"required"),
                                      
                                 array("field"=>"billing_email",
                                      "label"=>"billing email",
                                      "rules"=>"required"),
                                      
                                 array("field"=>"billing_phone",
                                      "label"=>"billing phone",
                                      "rules"=>"required"),
                                      
                                 array("field"=>"billing_country",
                                      "label"=>"billing country",
                                      "rules"=>"required"),
                                      
                                 array("field"=>"billing_street",
                                      "label"=>"billing Addresss",
                                      "rules"=>"required"),
                                      
                                 array("field"=>"billing_code",
                                      "label"=>"billing code",
                                      "rules"=>"required"),
                                                               
                                 array("field"=>"billing_city",
                                      "label"=>"billing city",
                                      "rules"=>"required")    
                                 
                                 );
                                 
      $this->shipping_validation = array(
                                array("field"=>"shipping_first_name",
                                      "label"=>"shipping first name",
                                      "rules"=>"required"),
                                      
                                 array("field"=>"shipping_email",
                                      "label"=>"shipping email",
                                      "rules"=>"required"),
                                      
                                 array("field"=>"shipping_phone",
                                      "label"=>"shipping phone",
                                      "rules"=>"required"),
                                      
                                 array("field"=>"shipping_country",
                                      "label"=>"shipping country",
                                      "rules"=>"required"),
                                      
                                 array("field"=>"shipping_street",
                                      "label"=>"shipping Addresss",
                                      "rules"=>"required"),
                                      
                                 array("field"=>"shipping_code",
                                      "label"=>"shipping code",
                                      "rules"=>"required"),
                                                               
                                 array("field"=>"shipping_city",
                                      "label"=>"shipping city",
                                      "rules"=>"required")
                                 
                                 );                           
    }
    public function index()
    {
        $checkoutData['AllCart'] =$this->Cartmodel->ShowCartData($this->session->userdata('userAuth')); // user cart data 
        $checkoutData['userData']= $this->Shopuserprofile->getUserAllData($this->session->userdata('userAuth')); // user information from profile
        $checkoutData['AddressData'] =$this->ShopCheckoutmodel->AddressData($this->session->userdata('userAuth')); //billing and shipping already stored
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/checkout',$checkoutData);
    }
    
    
   public function UpdateCheckout()
   {
    $date = strtotime(date("Y-m-d"));
    if(!$this->input->post('billing_radio'))
    {
        $this->form_validation->set_rules($this->billing_validation);
        if($this->form_validation->run() == false)
        {
            $this->session->set_flashdata('message_name',validation_errors() );  
            redirect(base_url().'Checkout', 'refresh'); 
        }
    }
    if($this->input->post('billing_checkbox') && !$this->input->post('shipping_radio'))
    {
        $this->form_validation->set_rules($this->shipping_validation);
        if($this->form_validation->run() == false)
        {
            $this->session->set_flashdata('message_name',validation_errors() );
            redirect(base_url().'Checkout', 'refresh');
        }
    }
    $this->form_validation->set_rules($this->alwaysCheck);
    if($this->form_validation->run() == false)
    {
        $this->session->set_flashdata('message_name',validation_errors() );  
        redirect(base_url().'Checkout', 'refresh');
    }  
    if(!$this->input->post('billing_radio'))
    {        
        $billing_detail = array("houdinv_data_first_name"=>$this->input->post('billing_first_name'),
                                "houdinv_data_last_name"=>$this->input->post('billing_last_name'),
                                "houdinv_data_company_name"=>$this->input->post('billing_company_name'),
                                "houdinv_data_email"=>$this->input->post('billing_email'),
                                "houdinv_data_phone_no"=>$this->input->post('billing_phone'),
                                "houdinv_data_country"=>$this->input->post('billing_country'),
                                "houdinv_data_address"=>$this->input->post('billing_street'),
                                "houdinv_data_zip"=>$this->input->post('billing_code'),
                                "houdinv_data_city"=>$this->input->post('billing_city'),
                                "houdinv_data_order_note"=>$this->input->post('billing_order_note'),
                                "houdinv_data_user_id"=>$this->session->userdata('userAuth'));   
          
    }
    else
    { 
        $billing_detail = array("billing_data_old"=>$this->input->post('billing_radio'));     
    }
    if($this->input->post('billing_checkbox')) 
    {                    
        if(!$this->input->post('shipping_radio'))
        {
            $shipping_detail =  array("houdinv_data_first_name"=>$this->input->post('shipping_first_name'),
                                        "houdinv_data_last_name"=>$this->input->post('shipping_last_name'),
                                        "houdinv_data_company_name"=>$this->input->post('shipping_company_name'),
                                        "houdinv_data_email"=>$this->input->post('shipping_email'),
                                        "houdinv_data_phone_no"=>$this->input->post('shipping_phone'),
                                        "houdinv_data_country"=>$this->input->post('shipping_country'),
                                        "houdinv_data_address"=>$this->input->post('shipping_street'),
                                        "houdinv_data_zip"=>$this->input->post('shipping_code'),
                                        "houdinv_data_city"=>$this->input->post('shipping_city'),
                                        "houdinv_data_order_note"=>$this->input->post('shipping_order_note'),
                                        "houdinv_data_user_id"=>$this->session->userdata('userAuth'));  
        }
        else
        { 
            $shipping_detail = array("shipping_data_old"=>$this->input->post('shipping_radio'));     
        }
    }
    else
    {
        $shipping_detail =  array("billing_data_only"=>"yes");        
    }                                
    $cart_item = $this->Cartmodel->ShowCartData($this->session->userdata('userAuth'));
    $final_price =0;  
    // user coupon data
    $user_coupon = array("data"=>"");
    if($this->input->post('dont_remove_if_Want_discount_on') && $this->input->post('dont_remove_if_Want_discount_discount') && $this->input->post('dont_remove_if_Want_discount_id'))
    {    
        $user_coupon = array("data"=>array("houdinv_user_use_Coupon_user_id"=>$this->session->userdata('userAuth'),
                            "houdinv_user_use_Coupon_coupon_id"=>$this->input->post('dont_remove_if_Want_discount_id'),
                            "houdinv_user_use_Coupon_discount"=>$this->input->post('dont_remove_if_Want_discount_discount'),
                            "houdinv_user_use_Coupon_add_date"=>$date,
                            "houdinv_user_use_Coupon_update_date"=>$date
                            ));      
    }
    //end
    foreach($cart_item as $thisItem)
    {
        $count = $thisItem['count']; 
        $main_price = $thisItem['productPrice']; 
        $total_price = $main_price*$count;
        $final_price =$final_price+$total_price;
        $order_product[] = array("product_id"=>$thisItem['productId'],
                                "product_Count"=>$count,
                                "product_actual_price"=>$main_price,
                                "total_product_paid_Amount"=>$total_price,
                                "variant_id"=>$thisItem['variantId']);  

                                if($thisItem['productId']){
                                    $GetprodcutId =$thisItem['productId'];
                                }else{
                                    $GetprodcutIds =$this->ShopCheckoutmodel->GetprodcutId_varints($thisItem['variantId']);
                               
                                    $GetprodcutId=$GetprodcutIds->p_id;
                                }

                         $variablep_id[]=$GetprodcutId;



    }
    $SetprodcutId = implode (", ", $variablep_id);
                        
       if($user_coupon['data'])
                          {
                            
         $final_price = $final_price -  ($this->input->post('dont_remove_if_Want_discount_discount'));                  
                          }   
             $shipping =$this->ShopCheckoutmodel->shippingCharge($final_price);
                         
                               $final_price = $final_price + $shipping;                    
                                         
       $order__detail = array("houdinv_order_user_id"=>$this->session->userdata('userAuth'),
                              "product_ids" =>$SetprodcutId,
                              "houdinv_order_product_detail"=>json_encode($order_product),
                              "houdinv_order_payment_method"=>$this->input->post("customRadio"),
                              "houdinv_order_type"=>"Website",
                              "houdinv_order_confirmation_status"=>'unbilled',
                              "houdinv_orders_total_Amount"=>$final_price,
                              "houdinv_orders_total_paid"=>0,
                              "houdinv_orders_total_remaining"=>$final_price,
                              "houdinv_order_created_at"=>$date,
                              "houdinv_order_updated_at"=>$date,
                              "houdinv_payment_status"=>0,
                              "houdinv_delivery_charge"=>$shipping,
                              "houdinv_order_delivery_type"=>$this->input->post("order_delivery_type"),
                              "houdinv_orders_discount"=>$this->input->post('dont_remove_if_Want_discount_discount')); 
                              
        $update = $this->ShopCheckoutmodel->checkout($billing_detail,$shipping_detail,$order__detail,$user_coupon);                                      
  
   if($update)
   {
       if($this->input->post("customRadio") == 'payumoney')
       {
           $getUserDetail = fetchUsername($this->session->userdata('userAuth'),$this->session->userdata('shopName'));
           $this->session->set_flashdata('amount',$final_price);
           $this->session->set_flashdata('name',$getUserDetail['name']);
           $this->session->set_flashdata('email',$getUserDetail['email']);
           $this->session->set_flashdata('orderId',$update);
           redirect(base_url().'Checkout/payumoney', 'refresh');  
       }
       else if($this->input->post("customRadio") == 'auth')
       {
           $this->session->set_userdata('authorizedOrderData',$update);
            redirect(base_url().'Checkout/authorized', 'refresh');  
       }
       else
       {
            // get Email Count
            $getEmailCount = fetchEmailCount($this->session->userdata('shopName'));
            if($getEmailCount[0]->houdinv_emailsms_stats_remaining_credits > 0)
            {
                $getEmailTemplate = fetchOrderTemaplTemplate($this->session->userdata('shopName'));
                if(count($getEmailTemplate) > 0)
                {
                    $setSubject = $getEmailTemplate[0]->houdinv_email_template_subject;
                    $getMessage = $getEmailTemplate[0]->houdinv_email_template_message;
                    $getFindString = strpos($getMessage,"{user_name}");
                    if($getFindString)
                    {
                        $getUserName = fetchUsername($this->session->userdata('userAuth'),$this->session->userdata('shopName'));
                        $setMessage = str_replace('{user_name}',$getUserName['name'],$getMessage);
                    }
                    else
                    {
                        $setMessage = $getFindString;
                    }
                    $getEmail = fetchEmailSMSCredentials($this->session->userdata('shopName'));
                    // send email to admin user
                    $url = 'https://api.sendgrid.com/';
                    $user = $getEmail['sendGrid'][0]->houdin_sendgrid_username;
                    $pass = $getEmail['sendGrid'][0]->houdin_sendgrid_password;
                    $json_string = array(
                    'to' => array(
                        $getUserName['name']
                    ),
                    'category' => 'test_category'
                    );
                    $htm = '
                    <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                    <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                    <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                    <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                    <img src="'.base_url().'assets/images/main-logo.png" style="max-width: 53%;width:100%"/></td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$setMessage.'</td></tr>
                    
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                    <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                    <table width="100%" ><tr >
                    <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e 2018--All right reserverd </td></tr>
                    <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
                    </tr></table>
                    </div>
                    </div></td></tr></table>';
                    $params = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'x-smtpapi' => json_encode($json_string),
                        'to'        => $getUserName['name'],
                        'fromname'  => 'Registration',
                        'subject'   => $setSubject,
                        'html'      => $htm,
                        'from'      => $this->from,
                    );
                    $request =  $url.'api/mail.send.json';
                    $session = curl_init($request);
                    curl_setopt ($session, CURLOPT_POST, true);
                    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                    curl_setopt($session, CURLOPT_HEADER, false);
                    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($session);
                    curl_close($session);
                    $getResponse = json_decode($response);
                    if($getResponse->message == 'success')
                    {
                        $getRemaining = $getEmailCount[0]->houdinv_emailsms_stats_remaining_credits-1;
                        updateSuceessEmailCount($getRemaining,$this->session->userdata('shopName'));
                        $this->session->set_flashdata('success',"Your order is successfully placed");
                    redirect(base_url().'Orders', 'refresh');    
                    }
                    else
                    {
                        updateErrorEmailLog($this->session->userdata('shopName'));
                        $this->session->set_flashdata('success',"Your order is successfully placed");
                                redirect(base_url().'Orders', 'refresh');    
                    }
                }
                else
                {
                    $this->session->set_flashdata('success',"Your order is successfull");
                    redirect(base_url().'Orders', 'refresh');       
                }
            }
            else
            {
                $this->session->set_flashdata('success',"Your order is successfull");
                redirect(base_url().'Orders', 'refresh');   
            }
       }        
   }
   else
   {
    
            $this->session->set_flashdata('message_name',"Somethong went wrong! please try again later");
                   redirect(base_url().'Checkout', 'refresh');   
    
   }
   } 
      
   public function CheckoutCouponValidationCheck()
     {
          $code = $this->input->post("coupon_code");  
        if($code)
          {
             $isexist = $this->ShopCheckoutmodel->IsCouponExist($code);  
             if($isexist)
             {
                if($isexist['code']==200)
                {
                  
                       $usergroup = $this->ShopCheckoutmodel->CouponValidForUsergroup($isexist['msg'],$this->session->userdata('userAuth'));    
                      if($usergroup['code']==200)
                      {
                        
                         
                          $result = $this->finalCouponCheck($isexist['msg'],$this->session->userdata('userAuth'),$isexist['cost'],"all","");    
                        
                      }
                      else
                      {
                            
                        $userCartData = $this->ShopCheckoutmodel->CouponValidForProduct($isexist['msg'],$this->session->userdata('userAuth'));      
                           if($userCartData['code']==200)
                           {
                               $result = $this->finalCouponCheck($isexist['msg'],$this->session->userdata('userAuth'),$isexist['cost'],"product",$userCartData['msg']);    
                           }
                           else
                           {
                            
                                $result =   $userCartData;  
                         
                            } 
                      }  
                  
                     
                }
                else
                {
                 $result =   $isexist; 
                }
             } 
             else
             {
                 $result = array("code"=>400,"msg"=>"somthing went wrong try again");
             }
    
        }
        else
        {
              $result = array("code"=>400,"msg"=>"Please fill the text field");   
            
        }
          echo json_encode($result);
     }   
     
     
     public function finalCouponCheck($coupon_id,$userId,$amount,$validFor,$Id)
     {
          $minAmount = $this->ShopCheckoutmodel->minAmount($coupon_id,$userId); 
          if($minAmount['code']==200)
          {
               $Already_use = $this->ShopCheckoutmodel->Already_use($coupon_id,$userId,$minAmount['coupons_limit']); 
               if($Already_use['code']==200)
               {
                    $result = array("code"=>200,"amount"=>$amount,"valid_for"=>$validFor,"product_id"=>$Id,"coupon_id"=>$coupon_id);  
               }
               else
               {
                    $result = $Already_use;
               }
                  
          } 
          else
          {
               $result = $minAmount;
          }  
            
            return $result;   
            
     }
    //set payu money form
    public function payumoney()
    {
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/payumoney');
    }  
    public function authorized()
    {
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/authorized');
    }
    public function setAuthorizedPaymentData()
    {
        $this->session->unset_userdata('authorizedOrderData');
        $this->form_validation->set_rules('cardNumber','text','required');
        $this->form_validation->set_rules('orderId','text','required');
        $this->form_validation->set_rules('expiryMonth','text','required');
        $this->form_validation->set_rules('expiryYear','text','required');
        $this->form_validation->set_rules('securitypin','text','required');
        if($this->form_validation->run() == true)
        {
            $setArray['main'] = array('cardNumber'=>$this->input->post('cardNumber'),'orderId'=>$this->input->post('orderId'),'expiryMonth'=>$this->input->post('expiryMonth'),
            'expiryYear'=>$this->input->post('expiryYear'),'cardHolderName'=>$this->input->post('name'),'securityPin'=>$this->input->post('securitypin'));
            $getShopName = getfolderName($this->session->userdata('shopName'));
            $this->load->view(''.$getShopName.'/authorizedTransaction',$setArray);
        }
        else
        {
            $this->session->set_flashdata('success',"Your order is successfully placed but something went wrong with transaction.");
            redirect(base_url().'Orders', 'refresh'); 
        }
        
    }
    public function payumoneyresponse()
    {
        if($_POST['txnMessage'] == 'Transaction Successful')
        {
            $getAmount = json_decode($_POST['amount_split'],true);
            if(!$this->uri->segment('3'))
            {
                $this->session->set_flashdata('success',"Your order is successfully placed but something went wrong with transaction so please contact to our support with transaction id: ".$_POST['txnid']."");
                redirect(base_url().'Orders', 'refresh'); 
            }
            else
            {
                $setTransactionArray = array('houdinv_transaction_type'=>'credit','houdinv_transaction_method'=>'online','houdinv_transaction_for'=>'order',
                'houdinv_transaction_for_id'=>$this->uri->segment('3'),'houdinv_transaction_amount'=>$getAmount['PAYU'],'houdinv_transaction_date'=>date('Y-m-d'),'houdinv_transaction_status'=>'success',
                'houdinv_transaction_transaction_id'=>$_POST['txnid'],'houdinv_transaction_from'=>'payumoney');
                $getSuccessStatus = $this->ShopCheckoutmodel->updateSuccessTransaction($setTransactionArray);
                if($getSuccessStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success',"Your order is successfully placed");
                    redirect(base_url().'Orders', 'refresh');   
                }
                else
                {
                    $this->session->set_flashdata('success',"Your order is successfully placed but something went wrong with transaction so please contact to our support with transaction id: ".$_POST['txnid']."");
                    redirect(base_url().'Orders', 'refresh'); 
                }
            }
        }
        else
        {
            $setTransactionArray = array('houdinv_transaction_type'=>'credit','houdinv_transaction_method'=>'online','houdinv_transaction_for'=>'order',
            'houdinv_transaction_for_id'=>$this->uri->segment('3'),'houdinv_transaction_amount'=>$getAmount['PAYU'],
            'houdinv_transaction_date'=>date('Y-m-d'),'houdinv_transaction_status'=>'fail','houdinv_transaction_transaction_id'=>$_POST['txnid'],'houdinv_transaction_from'=>'payumoney');
            $getStatus = $this->ShopCheckoutmodel->updateFailureTransaction($setTransactionArray);
            $this->session->set_flashdata('success',"Your order is successfully placed but something went wrong with transaction so please contact to our support with transaction id: ".$_POST['txnid']."");
            redirect(base_url().'Orders', 'refresh'); 
        }
    }
 }    