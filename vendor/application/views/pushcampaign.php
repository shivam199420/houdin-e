<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
             <!--START Page-Title -->
            <div class="row">
           
                <div class="col-md-8">
                   <h4 class="page-title">Push Campaign</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                   <li><a href="<?php echo base_url(); ?>Campaign">Campaign</a></li>
                  <li class="active">Push Campaign</li>
                  </ol>
                  </div>
                 
            </div>
           <!--END Page-Title --> 
               <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>

        <?php  //print_r($Deactive_campag); ?>
            <div class="row m-t-20">
                      <div class="col-lg-4 col-sm-6">
                          <div class="widget-panel widget-style-2 bg-white">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Campaign.png">
                              <h2 class="m-0 text-dark counter font-600"><?=$total_campags->total_campag?></h2>
                              <div class="text-muted m-t-5">Total Campaign </div>
                          </div>
                      </div>
                      <div class="col-lg-4 col-sm-6">
                          <div class="widget-panel widget-style-2 bg-white">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Campaign.png">
                              <h2 class="m-0 text-dark counter font-600"><?=$Active_campag->Active_campag;?></h2>
                              <div class="text-muted m-t-5">Total Active Campaign </div>
                          </div>
                      </div>
                      <div class="col-lg-4 col-sm-6">
                         <div class="widget-panel widget-style-2 bg-white">
                             <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Campaign.png">
                             <h2 class="m-0 text-dark counter font-600"><?=$Deactive_campag->Deactive_campag;?></h2>
                             <div class="text-muted m-t-5">Total Deactive Campaign </div>
                         </div>
                     </div>

                    </div>
                    <div class="row">
            <div class="col-md-12">

              <div class="card-box table-responsive">
                <div class="btn-group pull-right m-t-10 m-b-20">
                    <button type="button" class="btn btn-default m-r-5 delete_campaign_mlti"><i class="fa fa-trash"></i></button>
                    <a href="<?php echo base_url(); ?>Campaign/addpush" class="btn btn-default m-r-5" title="Add Campaign"><i class="fa fa-plus "></i></a>

                    <a href="<?php echo base_url(); ?>Campaign/generatecampaignpdf_push" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>

                </div>

                 <?php if($campaignsms){ ?>
            <?php echo form_open(base_url('Campaign/campaign_push_multidelete'), array( 'id' => 'campaign_multidelete', 'method'=>'POST' ));?>
                     
                <table class="table table-striped table-bordered table_shop_custom">
                  <thead>
                    <tr>
                      <th>
                       <input type="checkbox" class="all_select" id="all_select">
                       </th>
                      <th>Campaign Name</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php  foreach ($campaignsms as $key => $value) {
                      
                      if($value->campaign_status=='1'){
                        $statsu_='<p style="color: green;">Active</p>';
                      }else{
                           $statsu_='<p style="color: red;">Deactive</p>';
                      }
                    ?>
                    <tr>

                      <td><input type="checkbox" class="multiple_check" name="multiple_check[]" value="<?=$value->campaignsms_id?>"></td>
                      <td><?=$value->campaign_name?></td>
                      <td><?=$statsu_?></td>

                      <td>
                      <input type="hidden" name="campaignsms-ids" class="campaignsms-ids" value="<?=$value->campaignsms_id?>">
                           <button type="button" class="btn btn-default  m-r-5 send_campaign">Send Campaign</button>
                         <button type="button" class="btn btn-default  m-r-5 delete_campaign">Delete</button>
                            <a href="<?php echo base_url(); ?>Campaign/editpush/<?=$value->campaignsms_id?>/" class="btn btn-default m-r-5" title="Edit Campaign">Edit</a>
                      </td>
                    </tr>
                    <?php  } ?>
                  </tbody>
                </table>
                <?php echo form_close(); ?>

                <?php } ?>
              </div>
            </div>
          </div>
                      </div>
                    </div>
                  </div>

 

 <div id="delete_campaign_mlti" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                      <div class="modal-dialog">
                  
                      <div class="modal-content">

                      <div class="modal-header">

                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                      <h4 class="modal-title">Delete Order</h4>

                      </div>

                      <div class="modal-body">



                      <div class="row">
                      <div class="col-md-12">
                      <h4><b>Do you really want to Delete Select Campaign ?</b></h4>
                      </div>
                      </div>
                      </div>
                     
                      <div class="modal-footer">
                      <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                      <input type="button" id="delete_allcheckrows" class="btn btn-info" name="" value="Delete">

                      </div>

                      </div>
                    
                      </div>

                      </div>



                  <div id="delete_campaign" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                      <div class="modal-dialog">
                     <?php echo form_open(base_url('Campaign/campaign_addpush_delete'), array( 'id' => 'campaign_addsms_delete', 'method'=>'POST' ));?>
                      <div class="modal-content">

                      <div class="modal-header">

                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                      <h4 class="modal-title">Delete Order</h4>

                      </div>

                      <div class="modal-body">



                      <div class="row">
                      <div class="col-md-12">
                      <h4><b>Do you really want to Delete this order ?</b></h4>
                      </div>
                      </div>
                      </div>
                     <input type="hidden" name="hidden_campagian_id" id="hidden_campagian_id">
                      <div class="modal-footer">

                      <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                      <input type="submit" class="btn btn-info" name="" value="Delete">

                      </div>

                      </div>
                    <?php echo form_close(); ?>
                      </div>

                      </div>

                                        <div id="send_campaign" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                                            <div class="modal-dialog">
                                            <form method="post">
                                            <div class="modal-content">

                                            <div class="modal-header">

                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                            <h4 class="modal-title">Send Campaign</h4>

                                            </div>

                                            <div class="modal-body">



                                            <div class="row">
                                            <div class="col-md-12">
                                          <h4>Do You Want to Send this.</h4>
                                            </div>
                                            </div>
                                            </div>

                                            <div class="modal-footer">

                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                                            <input type="submit" class="btn btn-info" name="" value="Add Camapaign">

                                            </div>

                                            </div>
                                            </form>
                                            </div>

                                            </div>
 <?php $this->load->view('Template/footer.php');//send_campaign?>
 <script type="text/javascript">  
$(document).ready(function(){
    $('.send_campaign').click(function(){ 
       campaignsmsids=$(this).siblings('.campaignsms-ids').val();
       
      $('#send_campaign').modal('toggle');   

    });

    $('.delete_campaign').click(function(){  
    campaignsmsids=$(this).siblings('.campaignsms-ids').val();
    $('#hidden_campagian_id').val(campaignsmsids);

      $('#delete_campaign').modal('toggle');   

    });

 $("#all_select").click(function () {
     $('.multiple_check').not(this).prop('checked', this.checked);
 });

   $('.delete_campaign_mlti').click(function(){  
    if($('.multiple_check:checkbox:checked').length == 0)
   alert('At least one checkbox checked then delete ');
else
  // at least one checkbox checked...
      $('#delete_campaign_mlti').modal('toggle');   

    });
 
 $('#delete_allcheckrows').click(function(){
 
  $("#campaign_multidelete").submit();
 });

     });
</script>
