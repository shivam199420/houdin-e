<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SearchResult extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopproductsearchmodel');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $getProductSearchData =array();
         $this->form_validation->set_rules('shopName','text','required');
         $this->form_validation->set_rules('keyword','text','required');
         if($this->form_validation->run() == true)
         {
            $setArraySearch = array('shopname'=>$this->input->post('shopName'),'keyword'=>$this->input->post('keyword'));
            $getProductSearchData = $this->Shopproductsearchmodel->searchProduct($setArraySearch);
          
            
             
         }
         else
         {
          
               $this->session->set_flashdata('message_name',"Add some keywords in input box");
                  //  redirect(base_url().'SearchResult', 'refresh');  
         }
         $getShopName = getfolderName($this->session->userdata('shopName'));
        
        $this->load->view(''.$getShopName.'/search',$getProductSearchData);   
    }
    
}       