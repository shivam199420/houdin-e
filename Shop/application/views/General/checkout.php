<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
  $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
  $ip = $_SERVER['REMOTE_ADDR'];
}

$url = "http://www.geoplugin.net/json.gp?ip=".$ip;
// append the header putting the secret key and hash
$request_headers = array();
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 60);
curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$data = curl_exec($ch);
if (curl_errno($ch))
{
  print "Error: " . curl_error($ch);
}
else
{
  $transaction = json_decode($data, TRUE);
  curl_close($ch);
}
   $transaction['geoplugin_countryName'];
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<?php
$name = explode(" ",$userData->houdinv_user_name);
if(!$name[1])
{
    $name[1]='';
}
     ?>
     <style>
     .set-custom-button
     {
        background: #333 none repeat scroll 0 0;
        border: medium none;
        border-radius: 0;
        box-shadow: none;
        color: #fff;
        display: block;
        float: left;
        font-size: 14px;
        font-weight: 500;
        height: 45px;
        line-height: 45px;
        text-transform: uppercase;
        transition: all 0.3s ease 0s;
        width: 100%;
        margin-top: 20px;
        text-align: center;
        text-transform: uppercase;
        cursor:pointer;
     }
     </style>
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">checkout</h2>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- checkout area start -->
    <div class="checkout-area ptb-100">
        <div class="container">
        <?php
        if($this->session->flashdata('message_name'))
        {
            echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
        }
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        <?php
         if(count($AllCart)<1)
        {
            ?>
            <div class="row"><div class="col-sm-12"><h3>Your cart is empty</h3></div></div>
        <?php }
        else
        {
          ?>

          <!-- start top section -->
          <div class="row">
                <!-- left div start -->
                <div class="col-md-7">
                    <div class="returning-customer">
                        <h3><i class="fa fa-user"></i> User Details</h3>
                    </div>
                </div>
                <!-- and left -->
                <!-- right -->
                <div class="col-md-5">
                    <div class="customer-coupon">
                        <h3><i class="fa fa-square-o"></i> Have a coupon? <span id="coupon">Click here to enter your code</span></h3>
                        <div id="have-coupon" class="coupon-checkout-content">
                            <div class="coupon-info">

                                    <p class="checkout-coupon cuppon-wrap">
                                        <label>
                                            Coupon code
                                            <span class="required">*</span>
                                        </label>
                                        <input type="text" class="coupon_text"/>
                                        <input class="coupon-submit btn border-btn checkout_coupon_apply add_cart_custom" type="button" value="Apply Coupon" />
                                        <p class="coupon_msg_text" style="color: red;margin:15px"></p>
                                        <p class="coupon_msg_text1" style="color: green;margin:15px"></p>

                                    </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_open(base_url( 'Checkout/UpdateCheckout' ), array( 'id' => 'UpdateCheckout',"class"=>"checkout_form", 'method'=>'post' ));?>
            <!--end  right -->
            <!-- end top section -->
            <!-- statr main section -->
            <div class="row">
                <!-- main left -->
                <div class="col-md-7">
                    <div class="billing-details-area">
                        <h2>Billing details</h2>
                        <?php
                        if(count($AddressData) > 0)
                        {
                            foreach($AddressData as $address)
                            {
                                ?>
                                <div class="row returning-customer">

                                <div class="form-group col-md-12">
                                <p style="padding:5px;"><input name="billing_radio" type="radio" value="<?php echo $address->houdinv_user_address_id ;?>" class="billing_already_radio1"/>&nbsp;&nbsp;
                                  <?php echo $address->houdinv_user_address_name ;?> <br/>
                                  <?php echo $address->houdinv_user_address_phone ;?><br/>
                                  <?php echo $address->houdinv_user_address_user_address ;?><br/>
                                  <?php echo $address->houdinv_user_address_zip ;?>,<?php echo $address->houdinv_user_address_city ;?></p>
                                </div>
                                </div>
                                <?php }
                            } ?>
                            <div class="order-button-payment">
                            <a class="billing_Change set-custom-button" tabindex="0" style="margin-bottom: 10px;">Change address</a>
                                    </div>
                                    <!-- another billing address -->
                            <div class="anotherForm billing_form"  style="<?php if($AddressData) { echo 'display:none'; } ?>">
                            <div class="qc-button" style="text-align:center">
                            <div class="form-group col-md-12">
                            <a class="btn border-btn billing_Change1 set-custom-button" tabindex="0">Use Default</a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group au-form require">
                                        <label>First Name</label>
                                        <input name="billing_first_name" placeholder="First name" value="<?php echo $name[0]; ?>"  class="form-control required_validation_for_checkout" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group au-form require">
                                        <label>Last Name</label>
                                        <input name="billing_last_name" placeholder="Last name" value="<?php echo $name[1]; ?>" class="form-control" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group au-form">
                                        <label>Company Name</label>
                                        <input name="billing_company_name" placeholder="Company name" value="<?php  ?>" class="form-control" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group au-form">
                                        <label>Email Address</label>
                                        <input name="billing_email" placeholder="Email address" value="<?php echo $userData->houdinv_user_email; ?>" class="form-control required_validation_for_checkout" type="email">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group au-form">
                                        <label>Contact Number</label>
                                        <input name="billing_phone" placeholder="Phone number"  value="<?php echo $userData->houdinv_user_contact; ?>" class="form-control required_validation_for_checkout" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group au-form require">
                                        <label>Country</label>
                                        <select id="country" name="billing_country" class="custom-select required_validation_for_checkout" >
										<option value="">Choose Country</option>
                        <option value="Afganistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Angola">Angola</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bonaire">Bonaire</option>
                        <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Brazil">Brazil</option>
                        <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                        <option value="Brunei">Brunei</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Canary Islands">Canary Islands</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Channel Islands">Channel Islands</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos Island">Cocos Island</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Congo">Congo</option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Cote DIvoire">Cote D'Ivoire</option>
                        <option value="Croatia">Croatia</option>
                        <option value="Cuba">Cuba</option>
                        <option value="Curaco">Curacao</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Republic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="East Timor">East Timor</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands">Falkland Islands</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Ter">French Southern Ter</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Great Britain">Great Britain</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Hawaii">Hawaii</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iran">Iran</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Isle of Man">Isle of Man</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="Korea North">Korea North</option>
                        <option value="Korea Sout">Korea South</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Laos">Laos</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libya">Libya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Macau">Macau</option>
                        <option value="Macedonia">Macedonia</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique">Martinique</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mayotte">Mayotte</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Midway Islands">Midway Islands</option>
                        <option value="Moldova">Moldova</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Montserrat">Montserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Nambia">Nambia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherland Antilles">Netherland Antilles</option>
                        <option value="Netherlands">Netherlands (Holland, Europe)</option>
                        <option value="Nevis">Nevis</option>
                        <option value="New Caledonia">New Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue">Niue</option>
                        <option value="Norfolk Island">Norfolk Island</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau Island">Palau Island</option>
                        <option value="Palestine">Palestine</option>
                        <option value="Panama">Panama</option>
                        <option value="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Phillipines">Philippines</option>
                        <option value="Pitcairn Island">Pitcairn Island</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Republic of Montenegro">Republic of Montenegro</option>
                        <option value="Republic of Serbia">Republic of Serbia</option>
                        <option value="Reunion">Reunion</option>
                        <option value="Romania">Romania</option>
                        <option value="Russia">Russia</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="St Barthelemy">St Barthelemy</option>
                        <option value="St Eustatius">St Eustatius</option>
                        <option value="St Helena">St Helena</option>
                        <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                        <option value="St Lucia">St Lucia</option>
                        <option value="St Maarten">St Maarten</option>
                        <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                        <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                        <option value="Saipan">Saipan</option>
                        <option value="Samoa">Samoa</option>
                        <option value="Samoa American">Samoa American</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal">Senegal</option>
                        <option value="Serbia">Serbia</option>
                        <option value="Seychelles">Seychelles</option>
                        <option value="Sierra Leone">Sierra Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Slovakia">Slovakia</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South Africa</option>
                        <option value="Spain">Spain</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="Sudan">Sudan</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Syria">Syria</option>
                        <option value="Tahiti">Tahiti</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania">Tanzania</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Togo">Togo</option>
                        <option value="Tokelau">Tokelau</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                        <option value="Tuvalu">Tuvalu</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Erimates">United Arab Emirates</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="United States of America">United States of America</option>
                        <option value="Uraguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Vatican City State">Vatican City State</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Vietnam">Vietnam</option>
                        <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                        <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                        <option value="Wake Island">Wake Island</option>
                        <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                        <option value="Yemen">Yemen</option>
                        <option value="Zaire">Zaire</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
									</select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group au-form require">
                                        <label>Address</label>
                                        <textarea rows="3" name="billing_street" id="address" placeholder="Street address. Apartment, suite, unit etc. (optional)" class="form-control required_validation_for_checkout"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group au-form">
                                    <label>Postcode / Zip</label>
                                    <input name="billing_code" placeholder="Post code / Zip" class="form-control required_validation_for_checkout" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group au-form require">
                                        <label>Town/City</label>
                                        <input name="billing_city" placeholder="Town / City" class="form-control required_validation_for_checkout" type="text">
                                    </div>
                                </div>


                            </div>


                        </div>

                    </div>
                    <div class="clearfix"></div>
                                    <!-- another billing address end here -->
                    <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="billing_checkbox" style="height:15px; width:3%" class="custom-control-input" id="customCheck1">&nbsp;&nbsp;&nbsp;SHIP TO A DIFFERENT ADDRESS?

                    </div>
                    <!-- shipping form -->
                    <div class="shipping_form" style="display: none;">
                    <h2>Shipping details</h2>
                    <?php
                        if(count($AddressData) > 0)
                        {
                            foreach($AddressData as $address)
                            {
                                ?>
                                <div class="row returning-customer">
                                <div class="form-group col-md-1">
                                <input name="billing_radio" type="radio" value="<?php echo $address->houdinv_user_address_id ;?>" class="billing_already_radio1"/>
                                </div>

                                <div class="form-group col-md-11">
                                <p style="padding:5px;">  <?php echo $address->houdinv_user_address_name ;?> <br/>
                                  <?php echo $address->houdinv_user_address_phone ;?><br/>
                                  <?php echo $address->houdinv_user_address_user_address ;?><br/>
                                  <?php echo $address->houdinv_user_address_zip ;?>,<?php echo $address->houdinv_user_address_city ;?></p>
                                </div>
                                </div>
                                <?php }
                        }
                        ?>
                         <div class="qc-button">
                            <a class="shipping_Change set-custom-button" style="padding:10px" tabindex="0">Change address</a>
                        </div>
                        <div class="fill_Shipping" style="<?php if($AddressData) { echo 'display:none'; } ?>">
                        <div class="form-row qc-button" style="text-align:center">
                        <div class="form-group col-md-6">
                            <a class="set-custom-button shipping_Change1" tabindex="0">Use Default</a>
                        </div>
                        <div class="col-md-6"></div>
                        </div>
                        <div class="col-sm-12">
                                    <div class="form-group au-form">
                                    <label>First Name</label>
                                    <input name="shipping_first_name" placeholder="First name" value="<?php echo $name[0]; ?>"  class="form-control required_validation_for_checkout1" type="text">
                                    </div>
                                </div>

                                 <div class="col-sm-12">
                                    <div class="form-group au-form">
                                    <label>Last Name</label>
                                    <input name="shipping_last_name" placeholder="Last name" value="<?php echo $name[1]; ?>" class="form-control" type="text">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group au-form">
                                    <label>Company Name</label>
                                    <input name="shipping_company_name" placeholder="Company name" value="<?php  ?>" class="form-control" type="text">
                                    </div>
                                </div>

                                 <div class="col-sm-12">
                                    <div class="form-group au-form">
                                    <label>Email</label>
                                    <input name="shipping_email" placeholder="Email address" value="<?php echo $userData->houdinv_user_email; ?>" class="form-control required_validation_for_checkout1" type="email">
                                    </div>
                                </div>

                                         <div class="col-sm-12">
                                    <div class="form-group au-form">
                                    <label>Contact</label>
                                    <input name="shipping_phone" placeholder="Phone number"  value="<?php echo $userData->houdinv_user_contact; ?>" class="form-control required_validation_for_checkout1" type="text">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group au-form">
                                    <label>Country</label>
                                    <select id="country" name="shipping_country" class="custom-select required_validation_for_checkout1" >
										<option value="">Choose Country</option>
                        <option value="Afganistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Angola">Angola</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bonaire">Bonaire</option>
                        <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Brazil">Brazil</option>
                        <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                        <option value="Brunei">Brunei</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Canary Islands">Canary Islands</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Channel Islands">Channel Islands</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos Island">Cocos Island</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Congo">Congo</option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Cote DIvoire">Cote D'Ivoire</option>
                        <option value="Croatia">Croatia</option>
                        <option value="Cuba">Cuba</option>
                        <option value="Curaco">Curacao</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Republic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="East Timor">East Timor</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands">Falkland Islands</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Ter">French Southern Ter</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Great Britain">Great Britain</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Hawaii">Hawaii</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iran">Iran</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Isle of Man">Isle of Man</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="Korea North">Korea North</option>
                        <option value="Korea Sout">Korea South</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Laos">Laos</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libya">Libya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Macau">Macau</option>
                        <option value="Macedonia">Macedonia</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique">Martinique</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mayotte">Mayotte</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Midway Islands">Midway Islands</option>
                        <option value="Moldova">Moldova</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Montserrat">Montserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Nambia">Nambia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherland Antilles">Netherland Antilles</option>
                        <option value="Netherlands">Netherlands (Holland, Europe)</option>
                        <option value="Nevis">Nevis</option>
                        <option value="New Caledonia">New Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue">Niue</option>
                        <option value="Norfolk Island">Norfolk Island</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau Island">Palau Island</option>
                        <option value="Palestine">Palestine</option>
                        <option value="Panama">Panama</option>
                        <option value="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Phillipines">Philippines</option>
                        <option value="Pitcairn Island">Pitcairn Island</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Republic of Montenegro">Republic of Montenegro</option>
                        <option value="Republic of Serbia">Republic of Serbia</option>
                        <option value="Reunion">Reunion</option>
                        <option value="Romania">Romania</option>
                        <option value="Russia">Russia</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="St Barthelemy">St Barthelemy</option>
                        <option value="St Eustatius">St Eustatius</option>
                        <option value="St Helena">St Helena</option>
                        <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                        <option value="St Lucia">St Lucia</option>
                        <option value="St Maarten">St Maarten</option>
                        <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                        <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                        <option value="Saipan">Saipan</option>
                        <option value="Samoa">Samoa</option>
                        <option value="Samoa American">Samoa American</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal">Senegal</option>
                        <option value="Serbia">Serbia</option>
                        <option value="Seychelles">Seychelles</option>
                        <option value="Sierra Leone">Sierra Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Slovakia">Slovakia</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South Africa</option>
                        <option value="Spain">Spain</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="Sudan">Sudan</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Syria">Syria</option>
                        <option value="Tahiti">Tahiti</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania">Tanzania</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Togo">Togo</option>
                        <option value="Tokelau">Tokelau</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                        <option value="Tuvalu">Tuvalu</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Erimates">United Arab Emirates</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="United States of America">United States of America</option>
                        <option value="Uraguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Vatican City State">Vatican City State</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Vietnam">Vietnam</option>
                        <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                        <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                        <option value="Wake Island">Wake Island</option>
                        <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                        <option value="Yemen">Yemen</option>
                        <option value="Zaire">Zaire</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
									</select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group au-form">
                                    <label>Address</label>
                                    <textarea rows="3" name="shipping_street" id="address" placeholder="Street address. Apartment, suite, unit etc. (optional)" class="form-control required_validation_for_checkout1"></textarea>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group au-form">
                                    <label>Post / Zip Code</label>
                                    <input name="shipping_code"  placeholder="Post code / Zip" class="form-control required_validation_for_checkout1 number_validation" type="text">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group au-form">
                                    <label>Town / City</label>
                                    <input name="shipping_city" placeholder="Town / City" class="form-control required_validation_for_checkout1" type="text">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group au-form">
                                    <label>Order Note</label>
                                    <textarea rows="3" name="shipping_order_note" placeholder="Order note" class="form-control"></textarea>
                                    </div>
                                </div>


                            </div>

                    </div>

                    </div>

                </div>

                <!-- end main left -->
                <!-- main right -->
                <div class="col-md-5">
                    <div class="your-order-payment">
                        <div class="your-order">
                            <h2>Your Order</h2>
                            <ul>
                            <?php
                                $final_price =0;
                                foreach($AllCart as $thisItem)
                                {
                                    $count = $thisItem['count'];
                                    $main_price = $thisItem['productPrice'];
                                    $total_price = $main_price*$count;
                                    $final_price =$final_price+$total_price;
                                ?>
                                <li><?php echo $thisItem['productName']; ?><span class="product_total_<?php echo $thisItem->houdin_products_id; ?>"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $total_price; ?></span></li>
                                <?php } ?>
                                <!-- extra data -->
                                <li>Subtotal<input type="hidden" name="dont_remove_if_Want_discount_on" class="dont_remove_if_Want_discount_on" value=""/> <span class="subtotal_Span"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $final_price; ?></span></li>
                                <li>Discount <input type="hidden" name="dont_remove_if_Want_discount_discount"  class="dont_remove_if_Want_discount_discount" value=""/> <span class="Discount_Span"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>0</span></li>
                                <?php
                                $shipping =$this->ShopCheckoutmodel->shippingCharge($final_price);
                                $final_price = $final_price + $shipping;
                                ?>
                                <li class="order-total">Shipping  <span class="shipping_Span"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $shipping ; ?></span></li>
                                <li class="order-total">Total <input type="hidden" name="dont_remove_if_Want_discount_id" class="dont_remove_if_Want_discount_id" value=""/> <span class="amount_span"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $final_price; ?></span> </li>
                                <!-- end here -->
                            </ul>
                        </div>
                        <div class="your-payment">
                            <h2>payment method</h2>
                            <div class="payment-method">
                                <div class="payment-accordion">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Delivery
                                                        </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="panel-body">
                                                <ul style="list-style:none">
                                                <li>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" style="width: 3%;height: 15px;" id="customRadio3" value="deliver" name="order_delivery_type" class="custom-control-input customRadio_payment1">
                                                        <label class="custom-control-label" for="customRadio3">Deliver</label>

                                                    </div>

                                                </li>

                                                <li>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" style="width: 3%;height: 15px;" value="pick_up" id="customRadio4" name="order_delivery_type" class="custom-control-input customRadio_payment1">
                                                        <label class="custom-control-label" for="customRadio4">Pick up</label>
                                                    </div>
                                                </li>
                                            </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                                        aria-controls="collapseTwo">
                                                       Payment Option
                                                        </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                <div class="panel-body">
                                                <ul>
								<li>
									<div class="custom-control custom-radio">
										<input type="radio" id="customRadio1"  style="width: 3%;height: 15px;" value="cod" name="customRadio" class="custom-control-input customRadio_payment"/>
										<label class="custom-control-label" for="customRadio1">COD</label>

									</div>

								</li>
                <?php
                $country=$transaction['geoplugin_countryName'];
                if($country == 'India')
                {
                                $getPayU = getPayuCredentials($this->session->userdata('shopName'));
                                if(count($getPayU) > 0)
                                {
                                ?>
                                <li>
									<div class="custom-control custom-radio">
										<input type="radio" value="payumoney"  style="width: 3%;height: 15px;" id="customRadio2" name="customRadio" class="custom-control-input customRadio_payment"/>
										<label class="custom-control-label" for="customRadio2">Pay with credit / debit card</label>
									</div>
								</li>
                                <?php
                              } } else {
                                ?>
                                <?php
                                $getAuth = getAuthCredentials();
                                if(count($getAuth) > 0)
                                {
                                 ?>
                                 <li>
									<div class="custom-control custom-radio">
										<input type="radio" value="auth"  style="width: 3%;height: 15px;" id="customRadio5" name="customRadio" class="custom-control-input customRadio_payment"/>
										<label class="custom-control-label" for="customRadio5">Pay with credit / debit card</label>
									</div>
								</li>
              <?php } }  ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="order-button-payment">
                                    <a  class="set-custom-button m-t-50 float-right submit_form_checkout"><input type="hidden" name="sub" value="Place Order"/>Place Order</a>

                                    </div>
                                </div>
                            </div>
                            <!-- end main right -->
                        </div>
                    </div>
                </div>
            </div>

        <?php
    echo form_close();
    }
        ?>

        </div>
    </div>
    <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>

  <script>

  $( document ).ready(function() {
    var $radios = $('input:radio[name=order_delivery_type]');
    if($radios.is(':checked') === false) {
      $radios.filter('[value=deliver]').prop('checked', true);
    }

    var shipss=$('.shipping_Span').text();
    var amountss=$('.amount_span').text();
    var new_amount=amountss-shipss;

    $('input[type=radio][name=order_delivery_type]').change(function() {
      if (this.value == 'deliver') {
        $('.shipping_Span').text(shipss);
        $('.amount_span').text(amountss);
      }
      else if (this.value == 'pick_up') {
        $('.shipping_Span').text('0');
        $('.amount_span').text(new_amount);
      }
    });
  });

  </script>
