<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Terms extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopuserextrapages');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $getTerms = $this->Shopuserextrapages->fetchTermsData();
        $this->load->view(''.$getShopName.'/terms',$getTerms);
    }
     
}
