<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboardmodel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
        $this->userTbl = '';
    }



public function fetchCustomerRecord($data = false){
        // fetch customer data
        $this->db->select('*')->from('houdinv_users'); 
        $this->db->order_by("houdinv_user_id", "DESC"); 
        $this->db->limit(10);         
        $getCustomerdata = $this->db->get();
        $resultCustomerdata = ($getCustomerdata->num_rows() > 0)?$getCustomerdata->result():FALSE;
        
// fetch customer data

$this->db->select("houdinv_orders.*,houdinv_users.houdinv_user_id,houdinv_users.houdinv_user_name")
->from("houdinv_orders")
->join("houdinv_users","houdinv_users.houdinv_user_id=houdinv_orders.houdinv_order_user_id")
->order_by("houdinv_orders.houdinv_order_id","desc");
$this->db->limit(10);         
$getOrderdata = $this->db->get();
$resultlatest_order = ($getOrderdata->num_rows() > 0)?$getOrderdata->result():FALSE;


         

        // fetch privilage data
        $getCustomerSetting = $this->db->select('*')->from('houdinv_customersetting')->get()->result();

// fetch visitors data

$get_last_days = $this->getLastNDays(12);

for($i=0; $i<count($get_last_days); $i++){
$this->db->select('counts')->from('houdinv_visitors'); 
$this->db->where("visitors_date", strtotime($get_last_days[$i])); 
 
$getvisitorsdata = $this->db->get();    
 if($getvisitorsdata->num_rows() > 0){
    $getvisitorsdatas =$getvisitorsdata->result();


     $count_val=$getvisitorsdatas[0]->counts;
 }else{
    $count_val=0;
 }
$newcounts_arr[]=array('count'=> $count_val,'dates'=>$get_last_days[$i]);

}


for($s=0; $s<count($get_last_days); $s++){
    $this->db->select('count("houdinv_order_id") as counts ,houdinv_order_created_at')->from('houdinv_orders'); 
    $this->db->where("houdinv_order_created_at", strtotime($get_last_days[$s]));      
    $getOrdercountdata = $this->db->get(); 
   
    
     if($getOrdercountdata->num_rows() > 0){
        $getOrdercountdatas =$getOrdercountdata->result();

//print_r($getOrdercountdatas);

        //echo date('Y-m-d',$getOrdercountdatas[0]->houdinv_order_created_at);
    
         $countorder_val=$getOrdercountdatas[0]->counts;
     }else{
        $count_val=0;
     }
    $newcountsorder_arr[]=array('count'=> $countorder_val,'dates'=>$get_last_days[$s]);
    
    }
 
 
  $pri_setting_business=$this->setting_business_fun();
  $pri_Store_setting=$this->setting_store_fun();
  $pri_Shipping_setting=$this->Shipping_fun();
  $pri_Supplier_setting=$this->Supplier_fun();
  $pri_categories_setting=$this->categories_fun();
  $pri_Product_setting=$this->Product_fun();
  $pri_Staff_setting=$this->Staff_fun();
//   get basic stats
    $getTotalOrdeAmount = $this->db->select('SUM(houdinv_orders_total_Amount) AS totalOrderAmount')->from('houdinv_orders')->get()->result();
    $getTotalAmountPaid = $this->db->select('SUM(houdinv_orders_total_paid) AS totalOrderAmountPaid')->from('houdinv_orders')->get()->result();
    $getTotalRemainingAmount = $this->db->select('SUM(houdinv_orders_total_remaining) AS totalOrderAmountRemain')->from('houdinv_orders')->get()->result();
    $getTotalTrnsaction = $this->db->select('SUM(houdinv_transaction_amount) AS totalTransaction')->from('houdinv_transaction')->get()->result();
    $getTotalTransactionCredit = $this->db->select('SUM(houdinv_transaction_amount) AS totalTransactionCredit')->from('houdinv_transaction')->where('houdinv_transaction_type','credit')->get()->result();
    $getTotalTransactionDebit = $this->db->select('SUM(houdinv_transaction_amount) AS totalTransactionDebit')->from('houdinv_transaction')->where('houdinv_transaction_type','debit')->get()->result();
    

        return array('Staff'=>$pri_Staff_setting,'Product'=>$pri_Product_setting,'Categories'=>$pri_categories_setting,'Supplier'=>$pri_Supplier_setting,
        'Shipping'=>$pri_Shipping_setting,'setting_store'=>$pri_Store_setting,'setting_business'=>$pri_setting_business,
        'orders_chart'=>$newcountsorder_arr,'visitors'=>$newcounts_arr,'latest_order'=>$resultlatest_order,
         'customerList'=>$resultCustomerdata,
         'totalOrderAmount'=>$getTotalOrdeAmount[0]->totalOrderAmount,'totalPaidAmount'=>$getTotalAmountPaid[0]->totalOrderAmountPaid,
         'totalRemainigAmount'=>$getTotalRemainingAmount[0]->totalOrderAmountRemain,'totalTransaction'=>$getTotalTrnsaction[0]->totalTransaction,
            'totalTransactionCredit'=>$getTotalTransactionCredit[0]->totalTransactionCredit,'totalTransactionDebit'=>$getTotalTransactionDebit[0]->totalTransactionDebit,);
    }

    public function getLastNDays($days, $format = 'Y-m-d'){
		$m = date("m"); $de= date("d"); $y= date("Y");
		$dateArray = array();
		for($i=0; $i<=$days-1; $i++){
			$dateArray[] =  date($format, mktime(0,0,0,$m,($de-$i),$y)); 
		}
		return array_reverse($dateArray);
    }
    
public function setting_business_fun(){

// Setting primary
$activeclass='';
$total_setting_business=9;
$b_pval='0';
$b_href="#";
$activeclass='active';
$this->db->select('*')->from('houdinv_shop_detail');  
$shop_detail = $this->db->get();    
 if($shop_detail->num_rows()=='' || $shop_detail->num_rows()=='0'){
$b_href="Setting/compnaydetails";
$b_pval='1';
$activeclass='';
 }
 

   $this->db->select('*')->from('houdinv_shop_order_configuration');  
   $order_configuration = $this->db->get(); 
   if($order_configuration->num_rows()=='' || $order_configuration->num_rows()=='0'){

    if($b_href=='#'){
        $b_href="Setting/ordersetting";
    }
       
       $b_pval='2';
       $activeclass='';
   }
       $this->db->select('*')->from('houdinv_possetting');  
       $possetting = $this->db->get();
       if($possetting->num_rows()=='' || $possetting->num_rows()=='0'){
           
        if($b_href=='#'){
            $b_href="Setting/possetting";
        }
        
           $b_pval='3';
           $activeclass='';
       }
           $this->db->select('*')->from('houdinv_customersetting');  
           $customersetting = $this->db->get();
           if($customersetting->num_rows()=='' || $customersetting->num_rows()=='0'){
            if($b_href=='#'){
                $b_href="Setting/customersetting"; 
            }  
          
               $b_pval='4';
               $activeclass='';
           }
               $this->db->select('*')->from('houdinv_inventorysetting');  
               $inventorysetting= $this->db->get();
               if($inventorysetting->num_rows()=='' || $inventorysetting->num_rows()=='0'){
                if($b_href=='#'){
                    $b_href="Setting/inventorysetting";
                }    
                
                   $b_pval='5'; 
                   $activeclass='';
               }
                   $this->db->select('*')->from('houdinv_taxsetting');  
                   $taxsetting= $this->db->get();
                   if($taxsetting->num_rows()=='' || $taxsetting->num_rows()=='0'){
                    if($b_href=='#'){
                        $b_href="Setting/taxsetting"; 
                    }   
                   
                       $b_pval='6';
                       $activeclass='';
                   }
                       $this->db->select('*')->from('houdinv_skusetting');  
                       $skusetting= $this->db->get();
                       if($skusetting->num_rows()=='' || $skusetting->num_rows()=='0'){
                        if($b_href=='#'){
                            $b_href="Setting/skusetting";
                        }   
                         
                           $b_pval='7';
                           $activeclass='';
                       }
                           $this->db->select('*')->from('houdinv_invoicesetting');  
                       $invoicesetting= $this->db->get();
                       if($invoicesetting->num_rows()=='' || $invoicesetting->num_rows()=='0'){
                        if($b_href=='#'){
                            $b_href="Setting/invoicesetting"; 
                        }   
                        
                           $b_pval='8';
                           $activeclass='';
                       }
                           $this->db->select('*')->from('houdinv_storesetting');  
                           $storesetting= $this->db->get();
                           if($storesetting->num_rows()=='' || $storesetting->num_rows()=='0'){
                            if($b_href=='#'){
                                $b_href="Setting/storesetting";
                            }   
                            
                               $b_pval='9'; 
                               $activeclass='';
                           }
                               
return $pri_setting_business=array('href'=>$b_href,'val'=>$total_setting_business-$b_pval,'class'=>$activeclass);

    }

public function setting_store_fun(){

    // Setting primary
     
$total_setting_business='9';
$b_pval='0';
$b_href="#";
$activeclass='active';
    $this->db->select('*')->from('houdinv_shop_logo');  
    $shop_logo = $this->db->get();    
     if($shop_logo->num_rows()=='' || $shop_logo->num_rows()=='0'){
        if($b_href=='#'){
            $b_href="Storesetting/logo";
        }
        $activeclass='';  
       
    $b_pval='1';
    
     } 
       $this->db->select('*')->from('houdinv_navigation_store_pages');  
       $navigation = $this->db->get(); 
       if($navigation->num_rows()=='' || $navigation->num_rows()=='0'){
        if($b_href=='#'){
            $b_href="storesetting/Storesettinglanding";
        }
        $activeclass='';   
        
           $b_pval='2';
    
       } 
           $this->db->select('*')->from('houdinv_storepolicies');  
           $storepolicies = $this->db->get();
           if($storepolicies->num_rows()=='' || $storepolicies->num_rows()=='0'){
            if($b_href=='#'){
                $b_href="Storesetting/storepolicies"; 
            }
            $activeclass='';   
            
               $b_pval='3';
           } 
               $this->db->select('*')->from('houdinv_social_links');  
               $social_links = $this->db->get();
               if($social_links->num_rows()=='' || $social_links->num_rows()=='0'){
                if($b_href=='#'){
                    $b_href="Storesetting/sociallinks";
                }
                $activeclass='';    
                 
                   $b_pval='4';
               } 
                   $this->db->select('*')->from('houdinv_testimonials');  
                   $testimonials= $this->db->get();
                   if($testimonials->num_rows()=='' || $testimonials->num_rows()=='0'){
                    if($b_href=='#'){
                        $b_href="Storesetting/storetestimonials";
                    }
                    $activeclass='';    
                    
                       $b_pval='5'; 
                   } 
                       $this->db->select('*')->from('houdinv_taxsetting');  
                       $taxsetting= $this->db->get();
                       if($taxsetting->num_rows()=='' || $taxsetting->num_rows()=='0'){
                        if($b_href=='#'){
                            $b_href="Setting/taxsetting"; 
                        }
                        $activeclass='';    
                       
                           $b_pval='6';
                       } 
                           $this->db->select('*')->from('houdinv_skusetting');  
                           $skusetting= $this->db->get();
                           if($skusetting->num_rows()=='' || $skusetting->num_rows()=='0'){
                            if($b_href=='#'){
                                $b_href="Setting/skusetting"; 
                            }
                            $activeclass='';   
                            
                               $b_pval='7';
                           } 
                               $this->db->select('*')->from('houdinv_invoicesetting');  
                           $invoicesetting= $this->db->get();
                           if($invoicesetting->num_rows()=='' || $invoicesetting->num_rows()=='0'){
                            if($b_href=='#'){
                                $b_href="Setting/invoicesetting"; 
                            }
                            $activeclass='';   
                           
                               $b_pval='8';
                           } 
                               $this->db->select('*')->from('houdinv_storesetting');  
                               $storesetting= $this->db->get();
                               if($storesetting->num_rows()=='' || $storesetting->num_rows()=='0'){
                                if($b_href=='#'){
                                    $b_href="Setting/storesetting";
                                }
                                $activeclass='';    
                                
                                   $b_pval='9'; 
                               } 
                                    
                                
    
                           
      
    
                          
    
                       
                   
                
    
    
            
    
           
    
      
    return $pri_setting_business=array('href'=>$b_href,'val'=>$total_setting_business-$b_pval,'class'=>$activeclass);
    
        }
public function Shipping_fun(){
     // Setting primary
        $b_href="#";
        $b_pval='0';
        $activeclass='active'; 
        $total_setting_business='2';
     $this->db->select('*')->from('houdinv_shipping_ruels');  
     $shipping_ruels = $this->db->get();    
      if($shipping_ruels->num_rows()=='' || $shipping_ruels->num_rows()=='0'){
     
     if($b_href=='#'){
        $b_href="Shipping/shippingrule";
    }
    $activeclass='';
     $b_pval='1';
     
      } 
        $this->db->select('*')->from('houdinv_shipping_warehouse');  
        $shipping_warehouse = $this->db->get(); 
        if($shipping_warehouse->num_rows()=='' || $shipping_warehouse->num_rows()=='0'){
            
            if($b_href=='#'){
                $b_href="Shipping/warehouselist";
            }
            $activeclass='';
            $b_pval='2';
     
        } 
     
       
     return $pri_setting_business=array('href'=>$b_href,'val'=>$total_setting_business-$b_pval,'class'=>$activeclass);
     
}      

public function Supplier_fun(){
// Supplier primary
$activeclass='';
$this->db->select('*')->from('houdinv_suppliers');  
$suppliers = $this->db->get();    
 if($suppliers->num_rows()=='' || $suppliers->num_rows()=='0'){
$b_href="Supplier";
$b_pval='0';

 }else{
           $b_href="#";
           $b_pval='1';
           $activeclass='active';

 }

    return $pri_setting_business=array('href'=>$b_href,'val'=>$b_pval,'class'=>$activeclass);
}

public function categories_fun(){
    // Supplier primary
    $b_href="#";
    $b_pval='0';
    $activeclass='active'; 
    $total_setting_business='2';
    $this->db->select('*')->from('houdinv_categories');  
    $categories = $this->db->get();    
     if($categories->num_rows()=='' || $categories->num_rows()=='0'){
    
    if($b_href=='#'){
        $b_href="Category/categorymanagement";
    }
    $activeclass='';
    $b_pval='1';
    
     } 
        $this->db->select('*')->from('houdinv_productType');  
        $productType = $this->db->get(); 
        if($productType->num_rows()=='' || $productType->num_rows()=='0'){
            
            if($b_href=='#'){
                $b_href="Category/producttype";
            }
            $activeclass='';
            $b_pval='2';
     
        } 
     
    
      
    
        return $pri_setting_business=array('href'=>$b_href,'val'=>$total_setting_business-$b_pval,'class'=>$activeclass);
    }


    public function Product_fun(){
        // Supplier primary
        $activeclass='';
        $this->db->select('*')->from('houdinv_products');  
        $products = $this->db->get();    
         if($products->num_rows()=='' || $products->num_rows()=='0'){
        $b_href="product";
        $b_pval='0';
        
         }else{
            $b_href="#";
            $b_pval='1';
            $activeclass='active'; 
         
        
         }
        
            return $pri_setting_business=array('href'=>$b_href,'val'=>$b_pval,'class'=>$activeclass);
        }

        public function Staff_fun(){
            // Supplier primary
            $activeclass='';
            $this->db->select('*')->from('houdinv_staff_management');  
            $Staff = $this->db->get();    
             if($Staff->num_rows()=='' || $Staff->num_rows()=='0'){
            $b_href="staff";
            $b_pval='0';
            
             }else{
                $b_href="#";
                $b_pval='1';
                $activeclass='active'; 
             
            
             }
            
                return $pri_setting_business=array('href'=>$b_href,'val'=>$b_pval,'class'=>$activeclass);
            }


}