<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); ?>
<style type="text/css">
.zoom img{
    
    transition-duration: 5s;
    margin: 0 auto;
}
img {
    vertical-align: middle;
    height: 40px;
    width: auto; 
} 

.zoom { 
    transition: all 1s;
    -ms-transition: all 1s;
    -webkit-transition: all 1s;
    margin-top: 0px;
    padding-top: 0px;
    
}
.zoom:hover {
    -ms-transform: scale(2); /* IE 9 */
    -webkit-transform: scale(2); /* Safari 3-8 */
    transform: scale(2); 
    margin-left: 40px;
}
.m-b-10{
    margin-bottom: 10px;
}
.min-height{
  min-height: 194px;
    overflow-y: scroll;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" /> 
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
<!--START Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                <div class="col-md-8">
                   <h4 class="page-title">Add Account</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                 
                  <li class="active">Add Account</li>
                  </ol>
                  </div>
              <!--     <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                  
                </div>
                <?php 
           
                ?>
            </div>
           <!--END Page-Title --> 
           

       <?php echo form_open(base_url().'Accounts/addaccounting',array("id"=>"Addaccount","enctype"=>"multipart/form-data")); ?>
                              
        <div class="row">
            <?php  
    if($this->session->flashdata('error'))
    {
      echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
    }
    if($this->session->flashdata('success'))
    {
      echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    ?>
          <div class="col-md-12">
         

            <div class="card-box table-responsive">
                         
              <div class="col-md-6 m-b-10">

                <div class="form-group no-margin">

                <label for="field-7" class="control-label">Account Type</label>

                <select class="form-control account_type required_validation_for_account" name="account_type">
                <?php foreach($type as $name)
                {
                  if($name->account_type_id!="16"){
                    ?>
                      <option value="<?php echo $name->account_type_id; ?>"><?php echo $name->account_type_name; ?></option>
                    <?php
                } }?>
              
            
                
                <input type="hidden" name="status_id" id="status_id" />

                </div>

              </div>
              <div class="col-md-6 m-b-10">

                <div class="form-group no-margin">

                <label for="field-7" class="control-label">*Detail Type</label>

                <select class="form-control detail_Type required_validation_for_account" name="detail_Type">
                
                
                </select>
                
                <input type="hidden" name="status_id" id="status_id" />

                </div>

              </div>
              <div class="col-md-6 m-b-10">

                <div class="form-group no-margin">

                <label for="field-7" class="control-label">*Name</label>

       <input class="form-control input_name required_validation_for_account" type="text" name="Name"  />

                </div>

              </div>
               <div class="col-md-6 m-b-10">

                <div class="form-group no-margin">

                <label for="field-7" class="control-label">Description</label>

                <input class="form-control input_text" type="text" name="Description"  />

                </div>

              </div>
              
              <div class="col-md-6 m-b-10">

                <div class="form-group no-margin">

                <label for="field-7" class="control-label"></label>

                <div class="form-control min-height detail_text" disabled="" aria-disabled="true"></div>

                </div>

              </div>
            
              <div class="col-md-6 m-b-10">

                <div class="form-group no-margin">

              

               <input type="checkbox" class="masterSupplierCheck">
                 <label for="field-7" class="control-label">Is sub-account</label>

                </div>

              </div>
               <div class="col-md-6 m-b-10">

                <div class="form-group no-margin">
                <select class="form-control masterSupplierCheck_input" name="parents" disabled="">
                <option value="">Choose Parent Account</option>
                <?php 
foreach($parent as $val)
{
 
    ?>
 
  <option value="<?php echo $val['houdinv_accounts_name'] ;?>"><?php echo $val['houdinv_accounts_name'] ;?></option>
     <?php
}?>
                </select>
<!-- <input   aria-disabled="true" class="form-control masterSupplierCheck_input" placeholder="Enter parent account" list="parents"  />
<datalist id="parents">


  
</datalist> -->
                </div>
<p style="color: red;" class="message_text"></p>
              </div>

              <div class="col-md-6 m-b-10">

                <div class="form-group no-margin">
  <label for="field-7" class="control-label">Default Tax Code</label>
  <select class="form-control" name="my_tax" > 
  <option value="">Choose Tax</option>
  <option value="0% IGST">0% IGST</option>
  <option value="Out of Scope"> Out of Scope    </option>
  <option value="0% GST">0% GST</option>
  <option value="14.5% ST">14.5% ST</option>
  <option value="14.0% VAT">14.0% VAT</option>
  <option value="28.0% IGST">28.0% IGST</option>
  <option value="15.0% ST">15.0% ST</option>
  <option value="28.0% GST">28.0% GST</option>
  <option value="12.0% GST">12.0% GST</option>
  <option value="18.0% GST">18.0% GST</option>
  <option value="3.0% GST">3.0% GST</option>
  <option value="0.25% IGST">0.25% IGST</option>
  <option value="5.0% GST">5.0% GST</option>
  <option value="12.0% IGST">12.0% IGST</option>
  <option value="2.0% CST">2.0% CST</option>
  <option value="0.25% GST">0.25% GST</option>
  
  
    <option value="Exempt IGST">Exempt IGST</option>
  <option value="3.0% IGST">3.0% IGST</option>
  <option value="4.0% VAT">4.0% VAT</option>
  <option value="5.0% IGST">5.0% IGST</option>
  <option value="12.36% ST">12.36% ST</option>
  <option value="5.0% VAT">5.0% VAT</option>
  <option value="Exempt GST">Exempt GST</option>
  <option value="18.0% IGST">18.0% IGST</option>
  <option value="14.00% ST">14.00% ST</option>
  </select>
<!-- <input class="form-control" placeholder="Enter tax" list="taxlist" /> -->
<!-- <datalist id="taxlist">
  
 
</datalist> -->
                </div>

              </div>
              
            <div class="col-md-6 m-b-10 check_Type1">

                <div class="form-group no-margin">

              

               <input type="checkbox"  class="testcheckbox">
                 <label for="field-7" class="control-label">Track depreciation of this asset</label>

                </div>
<span class="explanation" style="display: none;">QuickBooks Online Plus creates two subaccounts for this asset: an account to track the cost, and an account to track the depreciation.</span>
              </div>    
              
              
              <div class="check_Type">
              <div class="col-md-3 m-b-10">

                <div class="form-group no-margin">

                <label for="field-7" class="control-label">Balance</label>

                <input class="form-control" onkeyup="checkDec(this);"   type="text" name="balance"  />

                </div>

              </div>
 
              <div class="col-md-3 m-b-10">

                <div class="form-group no-margin">

                <label for="field-7" class="control-label">as of</label>

                <input class="form-control date_picker" value="<?php echo date('Y-m-d'); ?>" type="text" name="as_of_one"  />

                </div>

              </div>
            
                  </div>
                  
                                <div class="check_Type2" style="display: none;">
              <div class="col-md-3 m-b-10">

                <div class="form-group no-margin">

                <label for="field-7" class="control-label">Depreciation</label>

                <input class="form-control" onkeyup="checkDec(this);"   type="text" name="Depreciation"  />

                </div>

              </div>

              <div class="col-md-3 m-b-10">

                <div class="form-group no-margin">

                <label for="field-7" class="control-label">as of</label>

                <input class="form-control" type="date" name="as_of_one1"  />

                </div>

              </div>
            
                  </div>
            
              
            </div>
          </div>
        </div>
        <input type="submit" name="save" class="btn btn-default" id="button_submit" value="Save"/>
        <?php echo form_close(); ?>
        
      </div>
    </div>
  </div>




<?php $this->load->view('Template/footer.php') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<script>
$(document).on("click",".status_tab",function()
{
    
  $id_status = $(this).attr("data-id");  
  $("#status_id").val($id_status);
    
});

$(document).on("click",".delete_tab",function()
{
    
  $id_delete = $(this).attr("data-id"); 
   $("#delete_id").val($id_delete);  
    
});

</script>
<script>
$(document).on("change",".account_type",function()
 {
  one();
 });
 
 function one()
 {
        var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
    var type = $('.account_type').val();

      jQuery.ajax({
          type: "Post",
          url: "<?php echo base_url(); ?>Accounts/accounttypedetailfetch",
          data:{'csrf_test_name':csrfHash,"id":"hello","type":type},
        
          success: function(data) {
            //detail_Type
            var name_option = '';
            if(data)
            {
                $.each($.parseJSON(data),function(key,value)
                {
                   console.log(value);
                   console.log(key); 
                   name_option+='<option value="'+value.houdinv_detail_type_id+'">'+value.houdinv_detail_type_name+'</option>';
                });
            }
            $('.detail_Type').html(name_option);
          //  console.log(data);
              two();
            }
            });
 }
 
 $(document).on("change",".detail_Type",function()
 {
two();
 });
 
 
 function two()
 {
        var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
    var type = $('.detail_Type').val();

      jQuery.ajax({
          type: "Post",
          url: "<?php echo base_url(); ?>Accounts/accounttypedetailfullfetch",
          data:{'csrf_test_name':csrfHash,"id":"hello","type":type},
        
          success: function(data) {
            //detail_Type
            var name_option = '';
            if(data)
            {
               var output =  $.parseJSON(data)
               $('.input_name').val(output.houdinv_detail_type_name);
               $('.detail_text').html(output.houdinv_detail_type_description);
               if(output.houdinv_detail_type_balance_box==0)
               {
                $('.check_Type').css("display","none");
               }
               else
               {
                $('.check_Type').css("display","block");
               }
               
                 if(output.houdinv_detail_type_depreciation==0)
               {
                $('.check_Type1').css("display","none");
               }
               else
               {
                $('.check_Type1').css("display","block");
               }
              
            }
          
          //  console.log(data);

            }
            });
 }
 
   one();
   
   
   
   $(document).on('change','.testcheckbox',function()
   {

    if($(this).is(":checked"))
    {
      $('.explanation').css('display','block');  
       $('.check_Type2').css('display','block');  
    }
    else
    {
      $('.explanation').css('display','none');  
       $('.check_Type2').css('display','none');   
    }
    
   });
   
   
      $(document).on('change','.masterSupplierCheck',function()
   {

    if(!$(this).is(":checked"))
    {
      $('.masterSupplierCheck_input').prop('disabled','disabled');  
    
    }
    else
    {
      $('.masterSupplierCheck_input').prop('disabled',false);  
    }
    
   });
   
   
   //form validation 
   var count_check =true;
           $(document).on('submit','#Addaccount',function(c){
           
            
                               var rep_image_val='';
                 $(this).find(".required_validation_for_account").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                        
                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
                

                
         if($('.masterSupplierCheck').is(":checked"))
         {
          
             if(!$('.masterSupplierCheck_input').val())
             {
               $('.message_text').text('fill this field');
                rep_image_val = 'error form';
                                $(this).css("border-color","red"); 
             }  
             
         }
                
                
                 
      
                
                
                

                $('.required_validation_for_account,.masterSupplierCheck_input').on('keyup blur change',function()
                                {
                                    
                                        $(this).css("border-color","#ccc");
                                        $('.message_text').text('');
                                });
                                
                               
                        
                
   
                  
                                
                                
   if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
              else
             {
                        
         if($('.masterSupplierCheck').is(":checked"))
         {   
               
                if(count_check) 
                {  
                    c.preventDefault();
                three($('.masterSupplierCheck_input').val(),$('.account_type').val());
                }
             }
             
             }   
            
          });
          
                          function checkDec(el){
 var ex = /^[0-9]+\.?[0-9]*$/;
 if(ex.test(el.value)==false){
   el.value = el.value.substring(0,el.value.length - 1);
  }
 }
 
 
 function checkINt(el){
 var ex = /^[0-9]*$/;
 if(ex.test(el.value)==false){
   el.value = el.value.substring(0,el.value.length - 1);
  }
 }
 
 
  function three(vals,type)
 {
        var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
  //  var type = $('.detail_Type').val();

      jQuery.ajax({
          type: "Post",
          url: "<?php echo base_url(); ?>Accounts/accountTypeMatch",
          data:{'csrf_test_name':csrfHash,"id":vals,"type":type},
        
          success: function(data) {
            //detail_Type
            var name_option = '';
            if(!data)
            {
          
           
                $('.message_text').text('parent account must be from current account type');
                 
            }
            else
            {
                count_check=false;
             $('#button_submit').trigger('click');   
            }
          
          //  console.log(data);

            }
            });
 }
</script>