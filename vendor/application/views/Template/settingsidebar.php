<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
<?php
$getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
  if($this->session->userdata('vendorRole')==1)
    {
$sidebar_permission =  $this->db->select("*")->from("houdinv_staff_management_sub")
    ->join("houdinv_staff_management","houdinv_staff_management.staff_id=houdinv_staff_management_sub.staff_id","right outer")
    ->where("houdinv_staff_management.houdinv_staff_auth_token",$this->session->userdata('vendorAuth'))
    ->get()->row();
    }
    else
    {
        $sidebar_permission = '';
    }
    
  //  print_R($sidebar_permission);
?>
        <div id="sidebar-menu">
            <ul>

            <?php if(!$sidebar_permission || ($sidebar_permission->setting) !=''){ 
        ?>  


                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect <?php if($this->router->fetch_class()=="Setting"){echo 'active';}?>" ><i class="fa fa-cogs"></i> <span> Setting </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="javascript:void(0);"> <span> General</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                              <li><a href="<?php echo base_url(); ?>Setting/compnaydetails">Company Details</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/ordersetting">Orders Setting</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/possetting">POS Setting</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/customersetting">Customer Setting</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/inventorysetting">Inventory Setting</a></li>
                                <li><a href="<?php echo base_url(); ?>Setting/taxsetting">Tax Setting</a></li>
                                  <!-- <li><a href="<?php //echo base_url(); ?>Setting/skusetting">SKU Setting</a></li> -->
                              <li><a href="<?php echo base_url(); ?>Setting/invoicesetting">Invoice Setting</a></li>
                              <!--<li><a href="#">Supplier Setting</a></li>-->
                              <li><a href="<?php echo base_url(); ?>Setting/storesetting">Store Setting</a></li>
                              <!--<li><a href="#">Online Store Setting</a></li>-->
                            </ul>
                        </li> 
                          <li><a href="<?php echo base_url(); ?>setting/resetaccountsetting"> <span> Reset Account</span></a></li>
                        <li><a href="<?php echo base_url(); ?>setting/smsemail">SMS & Email</a></li>
                        <!--<li><a href="#">Tax</a></li>-->
                          <!-- <li><a href="<?php //echo base_url(); ?>setting/analytics">Analytics Key</a></li> -->
                       <!-- <li><a href="<?php echo base_url(); ?>setting/memberShiptype">Membership Type</a></li>-->
                      <!--  <li><a href="#">Outlet</a></li>
                        <li><a href="#">Delivery</a></li>
                        <li><a href="#">Domain Configuration</a></li>
                        <li><a href="#">Account Settings</a></li>-->

                    </ul>
                </li>
               
               

         <?php  
         }
          ?>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
