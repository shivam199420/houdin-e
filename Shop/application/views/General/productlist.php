<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<style>

    .quotation
    {
        background: #333 none repeat scroll 0 0;
    color: #fff;
    font-size: 13px;
    font-weight: 500;
    /*right: -24px;*/
    margin: 0;
    padding: 1px 30px;
    position: absolute;
    text-align: left;
    text-transform: uppercase;
    top: 48px;
    /*transform: rotate(45deg);*/
    z-index: 99;
    }

</style>
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">Product List</h2>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- login area end -->
    <div class="shop-page-area ptb-100">
        <div class="container">
            <div class="row">
            <?php
            if($this->session->flashdata('error'))
            {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
            }
            if($this->session->flashdata('success'))
            {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
            }
            ?>
                <div class="col-md-12">
                  <?php
                  $count_product=count($productList);
                  if($count_product==0)
                  {?>
                    <div class="container"><div class="col-sm-12 text-center"><h3>Products are not available</h3></div></div>
                  <?php } else { ?>
                    <div class="blog-wrapper shop-page-mrg">
                        <div class="tab-menu-product">
                            <div class="tab-menu-sort">
                                <div class="tab-menu">

                                </div>
                                <div class="tab-sort">
                                    <label>Sort By : </label>
                                    <?php echo form_open(base_url( 'Productlist/'.$this->uri->segment('2').'/'.$this->uri->segment('3').'/'.$this->uri->segment('4')), array( 'id' => 'Productlist', 'method'=>'get' ));?>

                                    <select class="sort-select" name="sort">
                                    <option value="">select</option>
                                    <option value="name" <?php if($_REQUEST['sort'] =="name") { echo "selected"; }?> >Name Descending</option>
                                    <option value="date" <?php if($_REQUEST['sort'] =="date") { echo "selected"; }?>>Date Descending</option>
                                    <option value="price" <?php if($_REQUEST['sort'] =="pricehl") { echo "selected"; }?>>Price High to Low</option>
                                    <option value="price" <?php if($_REQUEST['sort'] =="pricelh") { echo "selected"; }?>>Price Low to High</option>
                                    </select>

                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                            <div class="tab-product">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="grid">
                                        <div class="row appendProductList">
                                        <?php
                                        foreach($productList as $productListData)
                                        {
                                            $getProductImage = json_decode($productListData->houdinv_products_main_images,true);
                                            $getProductPrice = json_decode($productListData->houdin_products_price,true);
                                        ?>
                                            <div class="col-md-3 col-lg-3 col-sm-6">
                                                <div class="single-shop mb-40">
                                                    <div class="shop-img">
                                                        <a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>">
                                                        <?php 
                                                        if($getProductImage[0])
                                                        {
                                                            $setMainImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                                                        }
                                                        else
                                                        {
                                                            $setMainImage = base_url()."Extra/noPhotoFound.png";
                                                        }
                                                        ?>
                                                        <img src="<?php $setMainImage; ?>" alt="" style="width: 270px; height: 360px;" /></a>
                                                        <?php if($productListData->houdinv_products_total_stocks <= 0) { ?>
                                                        <div class="price-up-down">
                                                            <span class="sale-new">Out of Stock</span>
                                                        </div>
                                                        <?php } ?>
                                                        <?php
                                                        if($productListData->houdinv_products_main_quotation == 1)
                                                        {
                                                        ?>
                                                        <span class="model_qute_show quotation_span quotation" data-id="<?php echo $productListData->houdin_products_id ?>" data-toggle="modal">Ask Quotation</span>

                                                        <?php
                                                        }
                                                        ?>
                                                        <div class="button-group">
                                                        <?php
                                                        if($productListData->houdinv_products_total_stocks <= 0)
                                                        {
                                                        if($storeSetting[0]->receieve_order_out == 1)
                                                        {
                                                            ?>
                                                            <a href="javascript:;" class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>" title="Add to Cart">
                                                                <i class="pe-7s-cart"></i>
                                                            </a>
                                                        <?php  }
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <a href="javascript:;" class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>" title="Add to Cart">
                                                                <i class="pe-7s-cart"></i>
                                                            </a>
                                                        <?php }
                                                        ?>
                                                        <a class="wishlist Add_to_whishlist_button"  href="javascript:;" title="Wishlist" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>">
                                                                <i class="pe-7s-like"></i>
                                                            </a>
                                                            <a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>" title="Quick View">
                                                                <i class="pe-7s-look"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="shop-text-all">
                                                        <div class="title-color fix">
                                                            <div class="shop-title">
                                                                <h3><a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>"><?php echo substr($productListData->houdin_products_title,0, 25).".." ; ?></a></h3>
                                                            </div>
                                                            <div class="price">
                                                                <span class="new"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $productListData->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $productListData->houdin_products_final_price; ?> <?php  } ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                            
                                        </div>
                                    </div>
                                    <div class="page-pagination text-center">
                                      <?php
                                      if($count > 0)
                                      {
                                          if($count >= 12)
                                          {
                                              $remain = $count-12;
                                          }
                                          else
                                          {
                                              $remain = 0;
                                          }
                                      ?>
                                      <div class="row">
                                      <div class="col-sm-2"><button class="btn btn-sm btn-success" style="width:100%;margin-bottom: 10px;">Total Product:<?php echo $count; ?></button></div>
                                      <?php
                                      if($remain > 0)
                                      {
                                      ?>
                                      <div class="col-sm-10"><button type="button" data-remain="<?php echo $remain ?>" data-last="12" class="btn btn-sm btn-default acc_btn setLoadMoredata" style="width:100%;margin-top: 0px;padding: 4px;border-radius: 4px;margin-bottom: 10px;">Load More&nbsp;<i class="fa fa-spinner fa-spin setSpinnerData" style="display:none;"></i></button></div>
                                      <?php }
                                      ?>
                                      </div>
                                      <?php }
                                      ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- login area end -->
    <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>

  <script type="text/javascript">
  $(document).ready(function(){
      $(document).on('click','.setLoadMoredata',function(){
          $(this).prop('disabled',true);
          $('.setSpinnerData').show();
          var getCategory = '<?php echo $this->uri->segment('2') ?>';
          var getSubCategory = '<?php echo  $this->uri->segment('3') ?>';
          var getSubSubCategory = '<?php echo $this->uri->segment('4') ?>';
          var sortBy = '<?php echo $_REQUEST['sort'] ?>';
          var dataLast = $(this).attr('data-last');
          var dataRemain = $(this).attr('data-remain');
          $.ajax({
          type: "POST",
          url: "<?php echo base_url() ?>Ajaxcontroller/fetchProductPaginationData",
          data: {"dataLast": dataLast,"dataRemain":dataRemain,"getCategory":getCategory,"getSubCategory":getSubCategory,"getSubSubCategory":getSubSubCategory,"sortBy":sortBy},
          success: function(datas) {
              $('.setSpinnerData').hide();
              $('.setLoadMoredata').prop('disabled',false);
              var setData = jQuery.parseJSON(datas);

                 var grid = "";
              for(var i=0; i < setData.main.length ; i++)
              {
                  var $productListData = setData.main[i];
                  var textsplit = $productListData.houdin_products_title;
                  var $setCartDesign = "";
                  var $setquotationBadge = "";
                    var $setStockBadge = "";
                    var $settingData = setData.settingData;
                      if($productListData.houdinv_products_total_stocks <= 0)
                          {
                              if($settingData[0].receieve_order_out == 1)
                              {
                                  $setCartDesign = '<a href="javascript:;" class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'" title="Add to Cart"><i class="pe-7s-cart"></i></a>';
                              }
                          }
                          else
                          {
                              $setCartDesign = '<a href="javascript:;" class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'" title="Add to Cart"><i class="pe-7s-cart"></i></a>';
                          }
                          if($productListData.houdinv_products_total_stocks <= 0)
                          {
                            $setStockBadge = '<div class="price-up-down"><span class="sale-new">Out of Stock</span></div>';
                          }
                          if($productListData.houdinv_products_main_quotation == 1)
                          {
                            $setquotationBadge = '<span class="model_qute_show quotation_span quotation" data-id="'+$productListData.houdin_products_id+'" data-toggle="modal">Ask Quotation</span>';
                          }

                    var   $getProductImage = jQuery.parseJSON($productListData.houdinv_products_main_images);
                     var     $getProductPrice = jQuery.parseJSON($productListData.houdin_products_price);
                        console.log($getProductPrice);
                          if($getProductPrice.discount != 0 && $getProductPrice.discount  != "")
                          {
                             var $DiscountedPrice = $getProductPrice.price-$getProductPrice.discount;
                             var $originalPrice = $getProductPrice.price;
                          }
                          else
                          {
                            var  $DiscountedPrice = 0;
                            var  $originalPrice = $getProductPrice.price;
                          }
                          if($DiscountedPrice == 0)
                          {
                            var  $setPrice = $originalPrice;
                          }
                          else
                          {
                             var $setPrice = "<strike>"+$originalPrice+"</strike>&nbsp;"+$DiscountedPrice+"";
                          }
                        if($getProductImage[0])
                        {
                            var setMainImage = '<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/'+$getProductImage[0]+'';
                        }
                        else
                        {
                            var setMainImage = '<?php echo base_url() ?>Extra/noPhotoFound.png';
                        }
                                                        
                 grid+='<div class="col-md-3 col-lg-3 col-sm-6">'
                      +'<div class="single-shop mb-40">'
                      +'<div class="shop-img">'
                        +'<a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'"><img src="'+setMainImage+'" alt="" style="width: 270px; height: 360px;" /></a>'
                        +$setStockBadge
                      +''+$setquotationBadge+'<div class="button-group">'
                      +$setCartDesign
                      +'<a class="wishlist Add_to_whishlist_button"  href="javascript:;" title="Wishlist" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><i class="pe-7s-like"></i></a><a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'" title="Quick View"><i class="pe-7s-look"></i></a></div></div>'
                       +'<div class="shop-text-all"> <div class="title-color fix"><div class="shop-title"> <h3><a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'">'+textsplit.substring(0,25)+'</a></h3> </div> <div class="price">'
                      +'<span class="new"><?php if($currencysymbol=="USD")  {  echo "$";  }else if($currencysymbol=="AUD"){  echo "$";  }else if($currencysymbol=="Euro"){  echo "£";  }else if($currencysymbol=="Pound"){  echo "€";  }else if($currencysymbol=="INR"){  echo "₹";  }  ?>&nbsp;'+$setPrice+'</span>'
                      +'</div>'
                +'</div></div></div></div>';

              }
              $('.appendProductList').append(grid);


              $('.setLoadMoredata').attr('data-last',setData.last);
          }
          });
      });
  });
  </script>
