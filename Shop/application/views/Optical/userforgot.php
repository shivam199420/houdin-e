<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>
   <!-- About Banner Start -->

   <section id="about">

       <div class="container">

           <div class="row">

               <div class="about-heading text-center">

                   <h2>Change Password</h2>

               </div>

           </div>

       </div>

   </section>

<!-- About Banner End -->



<!-- my account part Start -->

<section id="account">

    <div class="container">

        <div class="row">

          <?php
              if($this->session->flashdata('error'))
              {
                  echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
              }
              if($this->session->flashdata('success'))
              {
                  echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
              }
              ?>
              <?php echo form_open(base_url( 'Forgot/updatePass' ), array( 'id' => 'forgotPass', 'method'=>'post' ));?>

            <div class="col-md-3">

            </div>

            <div class="col-md-6">

                <div class="login-account">
                    <h3>Change password</h3>
                    <p>Fillbellow form change password.</p>
                    <hr>
                  
                    <div class="form-group">

                      <input type="text" name="forgotPin" class=" form-control required_validation_for_user_forgot name_validation number_validation" maxlength="4" />

                    </div>

                    <div class="form-group">

                      <input type="password" name="forgotPass" class="form-control required_validation_for_user_forgot name_validation opass" />

                    </div>

                    <div class="form-group">

                      <input type="password" name="forgotConPass" class="form-control required_validation_for_user_forgot name_validation cpass"  />

                    </div>



                   <div class="procd">

                      <input type="submit" class="btn btn-primary acc_btn text-center" value="Submit"/>

                    </div>

                  <?php echo form_close(); ?>

                </div>

            </div>



        </div>

    </div>

</section>

<!-- my account part End -->
<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/footer'); ?>
 <!-- CLient side form validation -->
<script type="text/javascript">
$(document).ready(function(){
    $(document).on('submit','#forgotPass',function(){
        var check_required_field='';
        $(this).find(".required_validation_for_user_forgot").each(function(){
            var val22 = $(this).val();
            if (!val22){
                check_required_field =$(this).size();
                $(this).css("border-color","#ccc");
                $(this).css("border-color","red");
            }
            $(this).on('keypress change',function(){
                $(this).css("border-color","#ccc");
            });
        });
        if(check_required_field)
        {
            return false;
        }
        else {
            return true;
        }
    });
});
</script>
