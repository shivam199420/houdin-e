<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header") ?>
    <section>
        <div class="container py-85 py-tn-50">
            <div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8 text-center">
                            <h2 class="title">contact us</h2>
                            <div class="title-border mx-auto m-b-55 m-b-tn-35"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
    <div class="container">
    <?php 
		if($this->session->flashdata('error'))
		{
			echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
		}
		else if($this->session->flashdata('success'))
		{
			echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
		}
		?>
    </div>
</div>
            <div class="port-body">
                <div class="contact-form">
                    <div class="messages" id="status"></div>
                    <?php echo form_open(base_url( 'Contact/addcontact' ), array( 'id' => 'shopContact', 'method'=>'post' ));?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                    <input type="text" name="userName" class="required_validation_for_shop_contact name_validation" id="name" name="name" placeholder="Your Full Name *"  >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                    <input type="email" name="userEmail" class="required_validation_for_shop_contact name_validation email_validation" id="email" name="email" placeholder="Your Mail *">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group au-form">
                                    <input type="text" name="userEmailSub" class="required_validation_for_shop_contact name_validation" id="subject" name="subject" placeholder="Your Subject">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group au-form">
                                    <textarea rows="9" name="userEmailBody" class="required_validation_for_shop_contact name_validation" placeholder="Your Message" id="msg" name="msg"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group au-form">
                                <input type="submit" id="contactBtn" style="cursor:pointer" class="mx-auto" value="Send us now"/>
                                </div>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>

      <?php 
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer") ?>

    <script type="text/javascript">
		$(document).ready(function(){
			$(document).on('submit','#shopContact',function(){
				var check_required_field=0;
				$(this).find(".required_validation_for_shop_contact").each(function(){
					var val22 = $(this).val();
					if (!val22){
						check_required_field++;
						$(this).css("border-color","#ccc");
						$(this).css("border-color","red");
					}
					$(this).on('keypress change',function(){
						$(this).css("border-color","#ccc");
					});
				});
				if(check_required_field != 0)
				{
					return false;
				}
				else {
					return true;
				}
			});
		});
		</script>		