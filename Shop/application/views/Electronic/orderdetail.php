<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<style type="text/css">
.heading hr:before {
    height: 2px;
    background: #0088cc;
    content: "";
    position: absolute;
    top: -1px;
    width: 90px;
    left: 0px;
}
.heading {
    margin-top: 20px;
}
.btn {
	margin-bottom: 8px;
    background: none;
    display: inline-block;
    font-size: 14px;
    padding: 9px 36px;
    font-weight: 500;
    border-radius: 30px;
    color: #08c;
    border: 2px solid #08c;
    position: relative;
    overflow: hidden;
    -webkit-transition: all 0.4s ease-in-out;
    -moz-transition: all 0.4s ease-in-out;
    -o-transition: all 0.4s ease-in-out;
    -ms-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
}
.btn:hover {
    color: #fff;
    border: 2px solid #337ab7;
    background: #0088cc;
}
</style>
		<div class="content">
		<div class="container">
			<div class="heading">
		    	<h2>Order Detail</h2>
		    	<hr>
		    </div>
			<div class="row">
           
				<div class="col-xs-12 col-md-12 margin-left63">
                	 <div class="pull-right">
					 <?php 
						$array_return = array("Delivered");
						if( in_array($orders->houdinv_order_confirmation_status,$array_return))
						{
							?>
		                	 	<form id="return_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
		                     <input type="text" name="returnOrderId" value=""/>
		                   <input type="text" value="returnOrder" name="returnOrder"/>
		                    </form>
							<button class="btn btn-default return_button">Return Order</button>
							<?php 
						}
						$array_cancel = array("unbilled","Not Delivered","billed","assigned");
						if( in_array($orders->houdinv_order_confirmation_status,$array_cancel))
						{
							?>
		               
		                    <form id="cancel_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
		                     <input type="text" name="cancelOrderId" value=""/>
		                   <input type="text" value="cancelOrder" name="cancelOrder"/>
		                    </form>
							<button class="btn btn-default cancel_button">Cancel Order</button>
						<?php } ?>
                	 </div>
            	</div>
              
			</div>
			<div class="row">
				<div class="col-sm-12">
				</div>
			</div>
			<div class="row m-t-20">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="custom-margin-order">
								<div class="pull-left">
									<p>Order ID : <span class="text_lightblue">#<?php echo $orders->houdinv_order_id ;?></span></p><br>
									<p>Order Status : <span class="text_lightblue text-uppercase"><?php echo $orders->houdinv_order_confirmation_status ;?></span></p>
								</div>
								<div class="pull-right margin-right40">
									<p>Date : <span class="text_lightblue"><?php echo date("Y-m-d ,h:i:s",$orders->houdinv_order_created_at) ;?></span></p>
								</div>
							</div>
							<hr>
								<div class="table-responsive">
									
									<table class="table m-t-30 ">
										<thead>
											<tr>
												<th>Product Name</th>
												<th>Quantity</th>
												<th>Rate</th>
												<th>Tax</th>
												<th>Total Price</th>
											</tr>
										</thead>
                                        <tbody>
										<?php 
                                        $total_product_gross = 0;
                                        foreach($orders->product_all as $detail)
                                        { ?>
                                        <tr>
                                        <td> <?php echo $detail->title ; ?></td>
                                        <td><?php echo $detail->allDetail->product_Count ; ?></td>
                                        <td><?php echo $detail->allDetail->product_actual_price; ?></td>
                                        <td>0</td>
                                        <td><?php echo $detail->allDetail->total_product_paid_Amount; ?></td>
                                       </tr>
                                        <?php 
                                        $total_product_gross = $total_product_gross + $detail->allDetail->total_product_paid_Amount;
                                        } ?>
                                        </tbody>
									</table>
									<hr style="margin-top: 0px!important">
								</div>
						
								<div class="row" style="border-radius: 0px;">
									<div class="col-md-9"></div>
									<div class="col-md-3">
										<p class=""><span class="custom-width-set">Gross Amount :</span> <span class="text-right"> &#8377; <?php echo $total_product_gross;?></span></p>
										<p class=""><span class="custom-width-set">Additional discount :</span><span class="text-right">&#8377; <?php echo  $orders->houdinv_orders_discount; ?></span></p>
										
										<p class=""><span class="custom-width-set">Delivery charges :</span><span class="text-right">&#8377; <?php echo $orders->houdinv_delivery_charge; ?></span></p>
										<p class=""><span class="custom-width-set">Round off :</span> <span class="text-right">&#8377; <?php echo $orders->houdinv_orders_total_Amount; ?> </span></p>
										<p class=""><span class="custom-width-set"><b>Net Amount :</b></span><b> <span class="text-right">&#8377; <?php echo $orders->houdinv_orders_total_Amount; ?></span></b></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div> <!-- container -->
				 <!-- content -->
			</div>

<?php 
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer') 
  ?>
  <script>
            $(document).on("click",".cancel_button",function()
            {
               $("#cancel_form").submit(); 
                
            });
            
              $(document).on("click",".return_button",function()
            {
               $("#return_form").submit(); 
                
            });
            
            </script>