<?php $this->load->view('Template/header') ?>
    <?php $this->load->view('Template/possidebar') ?>
     <style type="text/css">
     .select2 {
         width:100% !important;
     }
         .m-t-0-btn
         {
            margin: 0px !important;
         }
          .radio.radio-inline {
        margin-top: 0;
        margin-left: 0px;
        }
        label.btn-primary.active{
            background-color: #964460 !important;  
            border: 1px solid #964460 !important;  
        }
        .btn-default.active
        {
            background: #fff !important;
            color: #000 !important;
        }
        </style>

          <style type="text/css">
                  .section_order_pos_content{
                    display: flex;
                    border-top: 1px solid #5079ad !important;
                }
                
                .section_order_pos_content .leftContent_pos_ordercontent{
                    border-right: 1px solid #5079ad !important;
                    padding: 0px;
                }
                .section_order_pos_content .leftContent_pos_ordercontent_table{

                }
                .section_order_pos_content .leftContent_pos_ordercontent_table thead{
                    background-color: #5079ad !important;
                    font-size: 14px;
                }
                .section_order_pos_content .leftContent_pos_ordercontent_table thead th{
                    font-size: 12px;
                    color: #f4f8fb;
                    height: 70px;
                    vertical-align: middle;
                    text-align: center;
                    border: 1px solid #fff;
                }
            .section_order_pos_content .leftContent_pos_ordercontent_table tbody{}
            .section_order_pos_content .leftContent_pos_ordercontent_table tbody td{}
            .section_order_pos_content .leftContent_pos_ordercontent_table td,
            .section_order_pos_content .leftContent_pos_ordercontent_table th{}
            .section_order_pos_content .sidebar_pos_ordercontent{
                padding:0px;
                margin: 0px;
                padding-bottom: 150px;
                position: relative;
            }
            .section_order_pos_content .sidebar_pos_ordercontent .sidebar_pos_ordercontent_footer{
                position: absolute;
                left: 0px;
                right: 0px;
                bottom: 0px;
                background-color: #f5f9fb;
                padding: 10px;
            }
                .section_order_pos_payment_sidebar_table{
                        background-color: #fff;
                        margin-bottom: 0px;
                }
                .section_order_pos_payment_sidebar_table_label{
                    text-align: right;
                    width: 50%;
                }
                .section_order_pos_payment_sidebar_table_value{
                    text-align: right;
                }
                .section_order_pos_payment_sidebar_table td,
                .section_order_pos_payment_sidebar_table th{
                    border: 0px;
                    color: #000;
                    border-top:0px !important;
                    border-bottom: 1px solid #fff !important;
                    padding: 8px 15px!important;
                    font-size: 13px;
                }
                 .section_order_pos_payment_system_table{
                            width: 100%;
                                position: absolute;
    bottom: 0;
    box-shadow: 0px 0px 11px -3px;
                    }
                    .section_order_pos_payment_system_table th,
                    .section_order_pos_payment_system_table td{
                        padding: 10px;
                        color: #000;
                        font-size: 13px;
                    } 
                     .section_order_pos_payment_system_table th{}
                     .section_order_pos_payment_system_table td{}
                     .order-checkout-payment-method{
                            width: 100%;
                            float: right;
                            margin-bottom: 20px;
                           
                        }
                        .order-checkout-payment-method .order-checkout-payment-box-row-item{
                          float: left;
                          width: 100%; 
                          border-bottom: 1px solid #efefef;
                          margin: 0px; 
                        }
                        
                        .order-checkout-payment-method .order-checkout-payment-box-row-item .order-checkout-payment-box-row-content{
                          float: left;
                          width: 100%;
                          padding: 15px;  
                          font-size: 14px;
                          display:none;
                        }
                        .order-checkout-payment-method .order-checkout-payment-box-row{
                            float: left;
                            width: 50%;
                            padding: 15px 0px;
                            border-bottom: 1px solid #5079ad !important;
                            position: relative;
                            margin: 0px;
                            cursor: pointer;
                                border: 1px solid #ddd;
                        }
                        
                        .order-checkout-payment-method .order-checkout-payment-box-row input[type="radio"]{
                         display: none;
                        } 
                       
                      
                        .order-checkout-payment-method .order-checkout-payment-box-row input[type="radio"]:checked {
                          border: none;
                          -webkit-box-shadow: 0 0 0 10px #244667 inset;
                          box-shadow: 0 0 0 10px #244667 inset;
                        }
                        .order-checkout-payment-method .order-checkout-payment-box-row input[type="radio"]:checked + .order-checkout-payment-box-row-title{
                          color: #244667    ;
                          font-weight: bold;    
                        }
                        
                          .order-checkout-payment-method .order-checkout-payment-box-row input[type="radio"]:checked + .order-checkout-payment-box-row-title:before{
                            background: #e3f0fd;
                          }
                        .order-checkout-payment-method .order-checkout-payment-box-row-title{
                          font-weight: normal;
                          font-size: 14px;
                          margin-left: 10px;
                        }
                        .order-checkout-payment-method .order-checkout-payment-box-row-icon{
                             z-index: 999999;
    width: 24px;margin-right: 10px;
                        }
                          .order-checkout-payment-method-heading{
                            white-space: nowrap;
                            vertical-align: top;
                            padding: 10px;
                            color: #f4f8fb;
                            font-weight: bold;
                            background: #5079ad !important;
                          }
                            .order-checkout-payment-method .order-checkout-payment-box-row-title:before{
                                content: '';
                                background: #fff;
                                left: 0px;
                                right: 0px;
                                top: 0px;
                                bottom: 0px;
                                z-index: 0;
                          }
                            .order-checkout-payment-method .order-checkout-payment-box-row-title span{
                                position: relative;
                                z-index: 1;
                                font-size: 13px;
                           }
                          .mainParentRow{
                                background-color: rgb(245, 249, 251);
                          }

                          .pending_amount_display{
                                padding: 10px;
                                float: left;
                                width: 100%;
                                font-weight: bold;
                                color: #000000;
                                font-size: 16px;
                                margin-bottom: 10px;
                          }
                          .pending_amount_display label{
                             font-size: 13px;
                          }
                          .bill-subhead{    font-size: 21px;
    padding: 10px;}
    .btn-group-custom>.btn:first-child:not(:last-child):not(.dropdown-toggle) {
    border-top-left-radius: 14px;
    border-bottom-left-radius: 14px;
}
.btn-group-custom>.btn:not(:first-child):not(:last-child):not(.dropdown-toggle) {
    border-radius: 0;
    border-top-right-radius: 14px;
    border-bottom-right-radius: 14px;
}
.btn-group-custom>.btn:last-child:not(:first-child), .btn-group>.dropdown-toggle:not(:first-child) {
    border-top-left-radius: 14px;
    border-bottom-left-radius: 14px;
    border-top-right-radius: 14px;
    border-bottom-right-radius: 14px;
}
.registerdCustomerData{float: left;width: 100%;}
.tab-content{padding: 30px 8px;}
.custom-cust< span{width: 100% !important;}

 
     </style>
        <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container"> 
        <!--START Page-Title -->
        <div class="row">
        <div class="col-md-8">
        <h4 class="page-title">POS</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li class="active">POS</li>
        </ol>
        </div>
        </div>

        <?php echo form_open(base_url( 'POS/addOrderData' ), array( 'id' => 'submitMainForm', 'method'=>'post' ));?>
        <?php
        if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        ?>
        <input type="hidden" name="customer_id" class="customer_id" value=""/>
        <div class="alert alert-danger setErrorMessage" style="display:none;"></div>
        <div class="alert alert-success setSucccessMessage" style="display:none;"></div>
        <div class="row m-b-20 m-t-20">
            <span class="bill-subhead">Billing</span>
            <div class="btn-group btn-group-custom" data-toggle="btns">
  <a class="btn btn-default pos_type_btn active m-t-0-btn deliveryTypeData" href="#first" data-toggle="tab" data-value="0">Cash N Carry</a>
  <a class="btn btn-default pos_type_btn m-t-0-btn deliveryTypeData" href="#first" data-toggle="tab" data-value="1">Home Delivery</a>
   <!-- <a class="btn btn-default pos_type_btn" id="addCustomerbtntext" href="#third" data-toggle="model" data-value="2" style="margin-top: 0px !important;">Add Customer</a> -->
   <a class="btn btn-default add_customer_btn" data-toggle="modal" data-target="#check_customer_add_customer" style="margin-top: 0px !important;">Add Customer</a>   
   <a class="btn btn-default add_product_btn" data-toggle="modal" data-target="#add_product_modal" style="margin-top: 0px !important;">Add Product</a>   
   
</div>

<!-- Tab panes -->

        </div>



        <div class="section_order_pos_content"> 
     
        <div class="col-md-9 leftContent_pos_ordercontent">
        
       <div class="tab-content">
  <div class="tab-pane active" id="first">
      

      
        
  
      <div class="table-responsive">
        <table class="table leftContent_pos_ordercontent_table able-bordered table_shop_custom">
        <thead>
        <tr> 
         <th style="width:30%">Product Detail</th>
         <th style="width:8%">MRP</th>
         <th style="width:8%">CP</th>
        <th style="width:8%">SP</th>
        <th style="width:8%">Dis.P.</th>
        <th style="width:8%">Tax</th>
        <th style="width:8%">Qty</th>
        <th style="width:8%">Total</th>
        </tr>
        </thead>
        <tbody class="setRowData">
        <tr class="mainParentRow">
        <input type="hidden" class="inputProdcutId" name="inputProdcutId[]">
        <input type="hidden" class="inputVariantId" name="inputVariantId[]">
        <input type="hidden" class="inputQuantity" name="inputQuantity[]">
        <input type="hidden" class="inputActualPrice" name="inputActualPrice[]">
        <input type="hidden" class="inputDiscountedPrice" name="inputDiscountedPrice[]">
        <input type="hidden" class="inputTotalPrice" name="inputTotalPrice[]">
        <input type="hidden" class="inputtotaltax" name="inputtotaltax[]">
        <td>
            <div class="form-group" style="margin-bottom: 0px!important;">
            <select class="form-control setproductData select1" tabindex="14">
            <option value="">Choose Product</option>
            <?php 
                foreach($productData as $productDataList)
                {
            ?>
                <option data-price="<?php echo $productDataList['productPrice'] ?>" data-stock="<?php echo $productDataList['stock'] ?>" 
                data-variant="<?php echo $productDataList['variantId'] ?>" 
                data-product="<?php echo $productDataList['produtcId'] ?>" 
                data-tax="<?php echo $productDataList['tax_price'] ?>" data-cp="<?php echo $productDataList['cp'] ?>" data-taxprice="<?php echo $productDataList['tax_pricedata']; ?>" data-mrp="<?php echo $productDataList['mrp'] ?>"><?php echo $productDataList['productName'] ?></option>
            <?php 
            }
            ?>
        </select>

              </div>
    </td>
    <td class="mrp"></td>
    <td class="cp"></td>
        <td class="setMainPrice"></td>
        <td><input type="text" value="" class="form-control specialPrice number_validation" name="" placeholder=""></td>
        <td class="tax"></td>
        <td><input type="text" value="" class="form-control getQuantity number_validation" name="" placeholder=""></td>
        <td class="setFinalPrice">0</td>
        </tr>
        </tbody>
        </table>
        <div class="pull-right">
    <button type="button" class="btn btn-default addnewproductRow" style="border-radius: 14px;">Add New Product</button>
</div>
        </div>
  </div>
  <div class="tab-pane" id="second">
      
  </div>
  <div class="tab-pane" id="third">
    <div class="row form-horizontal">
      <div class="form-group">
        <label class="col-md-3 control-label">Customer Type</label>
        <div class="col-md-4">
        <input type="radio" tabindex="5" id="inlineRadio3" class="checkCustomerTypeData" value="RC" name="customerType" checked="">&nbsp;&nbsp;Registered
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4">
        <input type="radio" id="inlineRadio4" class="checkCustomerTypeData"  tabindex="6" value="NRC" name="customerType">&nbsp;&nbsp;Not Registered
        </div>
         
        </div>
        <!-- Registered customer -->
        <div class="registerdCustomerData">
        <div class="form-group">
        <label class="col-md-3 control-label">Customer Name</label>
        <div class="col-md-9">
        <select class="form-control select1 custom-cust selectCustomerData requiredValidationForCustomerName" tabindex="7" name="registeredCustomerId"> 
        <option value="">Choose Customer</option>
        <?php 
        foreach($customerList as $customerListData)
        {
        ?>
        <option value="<?php echo $customerListData->houdinv_user_id ?>"  data-name="<?=$customerListData->houdinv_user_name;?>" data-contact="<?php echo $customerListData->houdinv_user_contact ?>"><?php echo $customerListData->houdinv_user_name."(".$customerListData->houdinv_user_contact.")"  ?></option>
        <?php }
        ?>
        </select>
        </div>


        </div>

        <div class="form-group">
        <label class="col-md-3 control-label">Contact No.</label>
        <div class="col-md-9">
        <input type="text" value="" tabindex="8" readonly="readonly" class="form-control setCustomerContact" name="" placeholder="">
        </div>
        </div>
        

        </div>
        <!-- Non registred customer -->
        <div class="nonRegisterdCustomerData" style="display:none;"> 
        <div class="form-group">
        <label class="col-md-3 control-label">Customer Name</label>
        <div class="col-md-9">
        <input type="text" tabindex="10" class="form-control requiredValidationForNonRegisteredUser" name="setNonRegisteredCustomerName" placeholder="Customer Name">
        </div>


        </div>

        <div class="form-group">
        <input type="hidden" class="setNonRegisteredCustomerAddress" name="setNonRegisteredCustomerAddress"/>
        <label class="col-md-3 control-label">Contact No.</label>
        <div class="col-md-9">
        <input type="text" tabindex="11" class="form-control requiredValidationForNonRegisteredUser" name="setNonRegisteredCustomerContact" placeholder="Customer Contact">
        </div>
        </div>
          <div class="form-group setAddressData" >
        <label class="col-md-3 control-label">Choose Address</label>
        <div class="col-md-9">
        <select class="form-control setRegisteredCustomerAddress requiredValidationForRegisteredAddress" tabindex="9" name="setRegisteredCustomerAddress">
        
        </select>
        </div>
        </div>
        <div class="form-group setAddressData">
        <div class="col-sm-12">
            <label class="pull-right addRegistredUserAddress" style="cursor:pointer">Add Another Address</label></div>
        </div>
       
        </div>
</div>
</div>
       
    

        
        </div>

 
        <div class="form-group" style="padding: 10px;"> 
        <label  for="field-1" class="control-label">Coupon Disc</label>
         
         <input type="hidden" class="couponCodeId" name="couponCodeId"/>
         <input type="text" value="" tabindex="3" class="form-control couponCodeData" name="couponCodeData" placeholder="Coupon Code">
          
 
         <label for="field-1" class="control-label">Note</label>
         <textarea class="form-control" rows="4" name="customerNote"></textarea> 
 
         </div>
        
    </div>
        <div class="col-md-3 sidebar_pos_ordercontent">
        


       
        <table class="table able-striped section_order_pos_payment_sidebar_table">
            <tbody><tr>
                <td class="section_order_pos_payment_sidebar_table_label">Total sales</td>
                <td class="section_order_pos_payment_sidebar_table_value"><input type="hidden" class="finalAmountData" name="finalAmountData"><div class="setFinalTotal">0</div></td>
            </tr>
           

             <tr>
                <td class="section_order_pos_payment_sidebar_table_label">Discount</td>
                <td class="section_order_pos_payment_sidebar_table_value"><input type="hidden" class="finalDiscountData" name="finalDiscountData"><div class="setCouponDiscount">0</div></td>
            </tr>
              <tr>
                <td class="section_order_pos_payment_sidebar_table_label">Tax</td>
                <td class="section_order_pos_payment_sidebar_table_value"><input type="hidden" class="finalDiscountData" name="finalDiscountData"><div class="setCouponDiscount">0</div></td>
            </tr>
             <tr>
                <td class="section_order_pos_payment_sidebar_table_label">Add. Adjustment</td>
                <td class="section_order_pos_payment_sidebar_table_value"><input type="text" value="" class="form-control setPrivMaxValue" name="pendingAmountData1" placeholder="0.00"></td>
            </tr>
              
             <tr>
                <td class="section_order_pos_payment_sidebar_table_label">Rounding Off</td>
                <td class="section_order_pos_payment_sidebar_table_value"><input type="hidden" class="finalDiscountData" name="finalDiscountData"><div class="setCouponDiscount">0</div></td>
            </tr>
            <tr>
                <th class="section_order_pos_payment_sidebar_table_label">Net Payable:</th>
                <th class="section_order_pos_payment_sidebar_table_value"><input type="hidden" class="netPayableAmount" name="netPayableAmount"><div class="setNetPayable">0</div></th>
            </tr>

        </tbody></table> 
 
        

                
 
                
         
               

                <div class="paymnet_option_pos">
                     <div class="order-checkout-payment-method-heading">
                        Invoice
                    </div>
                      <div class="order-checkout-payment-method">
                    
                           <label class="order-checkout-payment-box-row">
                                <input type="radio" id="inlineRadio1" value="cash" name="paymentOption" checked="" class="order-checkout-payment-method-type">
                                 
                              <span class="order-checkout-payment-box-row-title"> <img src="<?php echo base_url();?>assets/images/wallet.png" class="order-checkout-payment-box-row-icon"><span>Cash</span></span>
                             
                              </label> 
    <label class="order-checkout-payment-box-row">                            
                            <input type="radio" id="inlineRadio2" value="credit card" name="paymentOption" class="order-checkout-payment-method-type"> 
                              <span class="order-checkout-payment-box-row-title"> <img src="<?php echo base_url();?>assets/images/credit-card.png" class="order-checkout-payment-box-row-icon"><span>Credit Card </span></span>
                                               
                            </label> 
<label class="order-checkout-payment-box-row">
                                <input type="radio" id="inlineRadio2" value="debit card" name="paymentOption" class="order-checkout-payment-method-type">  
                              <span class="order-checkout-payment-box-row-title"><img src="<?php echo base_url();?>assets/images/payment.png" class="order-checkout-payment-box-row-icon"><span>Debit Card</span></span> 
                               
                            </label>
                            <label class="order-checkout-payment-box-row">
                                <input type="radio" id="inlineRadio2" value="cheque" name="paymentOption" class="order-checkout-payment-method-type">  
                              <span class="order-checkout-payment-box-row-title"> <img src="<?php echo base_url();?>assets/images/cheque.png" class="order-checkout-payment-box-row-icon"><span>Cheque</span></span> 
                              
                            </label>

                            <label class="order-checkout-payment-box-row">
                                <input type="radio" id="inlineRadio2" value="Gift Card" name="paymentOption" class="order-checkout-payment-method-type">  
                              <span class="order-checkout-payment-box-row-title"><img src="<?php echo base_url();?>assets/images/add-user.png" class="order-checkout-payment-box-row-icon"><span>Gift Card</span></span> 
                               
                            </label> 

                           <label class="order-checkout-payment-box-row">
                                <input type="radio" id="inlineRadio2" value="customer Credit" name="paymentOption" class="order-checkout-payment-method-type">  
                              <span class="order-checkout-payment-box-row-title"><img src="<?php echo base_url();?>assets/images/ticket.png" class="order-checkout-payment-box-row-icon"><span>Customer Credit</span></span> 
                               
                            </label>
                            
                           
                    </div>  
                </div>
      
                <div class="form-group">
       
            <table class="section_order_pos_payment_system_table">
               
                <tbody><tr style="">
                    <th>Pay Pending Amount</th>
                    <td>   
                    <input type="text" value="" class="form-control setPrivMaxValue" name="pendingAmountData" placeholder="0.00">
                    <select style="display:none" class="form-control checkPaymentStatus" name="paymentStatus">
                            <option value="">Choose Payment Status</option>
                            <option value="1" selected>Paid</option>
                            <option value="0">Not Paid</option>
                            </select>
                   </td>
                </tr> 
  
                <tr><td colspan="2"> <div class="text-center hidden-print">
        <!-- <a href="POS/printinvoice" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i> Print</a> -->
        <input type="button" name="setPosDataBtn" class="btn btn-primary btn-round btn-block submitMainFormData generateOrderBtn" disabled="disabled" value="Complete  Order">
        </div></td></tr>

            </tbody></table>

            <div class="alert alert-danger setErrorMessage" style="display:none;"></div>
        <div class="alert alert-success setSucccessMessage" style="display:none;"></div>
                
           
        </div>
      
        </div>


            </div>
        </div>
    </div>


    <?php echo form_close(); ?>
        <!-- add registred user address modal -->
        <div id="addRegistredUserAddressModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
            <?php echo form_open(base_url( 'POS/addUserAddress' ), array( 'id' => 'addPosForm', 'method'=>'post' ));?>
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title custom_title"><b>Add Address</b></h4> 
                    </div> 
                    <div class="modal-body"> 
                            <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                <input type="hidden" class="addAddressCustomerId" name="addAddressCustomerId"/>
                                    <label for="field-3" class="control-label">Name</label> 
                                    <input type="text" name="userName" class=" form-control required_validation_for_pos name_validation" placeholder="User Name"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Contact</label> 
                                    <input type="text" name="userPhone" class=" form-control required_validation_for_pos name_validation" placeholder="Contact"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Address</label> 
                                    <textarea class="form-control required_validation_for_pos name_validation" rows="5" name="userMainAddress"> </textarea>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">City</label> 
                                    <input type="text" name="userCity" class=" form-control required_validation_for_pos name_validation" placeholder="City"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Zip</label> 
                                    <input type="text" name="userZip" class=" form-control required_validation_for_pos name_validation" placeholder="Zip"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Country</label> 
                                    <select class="form-control required_validation_for_pos" name="userCountry">
                                    <option value="">Choose Country</option>
                                    <option value="<?php echo $countryData['id'] ?>"><?php echo $countryData['country'] ?></option>
                                    </select>
                                </div> 
                            </div> 
                        </div> 
                         
                    </div> 
                    <div class="modal-footer"> 
                    <input type="submit" name="payumoneyAdd" class="btn btn-info addRegisteredUserBtnData" value="Add"/>
                    </div> 
                </div> 
                <?php echo form_close(); ?>
            </div>
        </div><!-- /.modal -->

 
 <!-- Add  customer data -->

  <!-- add registred user address modal -->
  <div id="check_customer_add_customer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title custom_title"><b>Add Customer</b></h4> 
                    </div> 
                    <div class="modal-body"> 
                            <div class="row"> 
                            <div class="col-md-12 add_customer_html"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label"></label> 
                                    <input type="text" name="mobile_number" class="form-control mobile_number" placeholder="Search"> 
                                    <i class="fa fa-search search-box"></i>
                                </div> 
                                <div class="form-group"> 
                                    <ul class="customer_list">
                                    
                                    </ul>
                                </div> 
                                <!-- <div class="form-group pull-right customer-not-found">
                                <label>Not found? <a href="javascript:;" class="add_new_customer">Add new customer</a></label>
                                </div> -->
                            </div> 
                        </div> 
                         
                    </div> 
                    <!-- <div class="modal-footer"> 
                    <a href='javascript:;' class="btn btn-info">Add Customer</a>
                    </div>  -->
                </div> 
            </div>
        </div>

        <!-- End add customer  -->

        <!-- add customer modal -->
        <div id="add_customer_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title custom_title"><b>Add Customer</b></h4> 
                    </div> 
                    <div class="modal-body"> 
                            <div class="row"> 
                            <div class="alert alert-danger add_customer_error d-none"></div>
                            <div class="col-md-12"> 

                             <div class="form-group"> 
                                    <label for="field-3" class="control-label">Name</label> 
                                    <input type="text" name="name" class="form-control name" placeholder="Customer Name"> 
                                </div> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Mobile Number</label> 
                                    <input type="text" name="mobile_number" class="form-control number_validation mobile_number_user" maxlength=10 placeholder="Mobile Number"> 
                                </div> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Email</label> 
                                    <input type="email" name="email" class="form-control email" placeholder="Customer Email"> 
                                </div> 

                            </div> 
                        </div> 
                         
                    </div> 
                    <div class="modal-footer"> 
                    <a href='javascript:;' id="add_customer" class="btn btn-info">Add<i class="fa fa-spinner fa-spin spiner d-none"></i></a>
                    </div> 
                </div> 
            </div>
        </div>

        <!-- end add customer modal -->

<!-- add product modal -->
<div id="add_product_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
            <?php echo form_open(base_url().'Product/addproduct',array("id"=>"Addproductn","enctype"=>"multipart/form-data"));
           ?>  
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title custom_title"><b>Add Product</b></h4> 
                    </div> 
                    <div class="modal-body"> 
                            <div class="row"> 
                            <div class="alert alert-danger add_customer_error d-none"></div>
                            <div class="col-md-12"> 

                                <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control required_validation_for_product" name="title" placeholder="Please enter product title" />
                                </div>
                                 <!-- product visibility-->
                                <div class="form-group d-none advance_modal">
                                <label>Show product on</label>
                                <div > <input class="form-control" type="radio" name="show_on"  style="float:left;" value="App"/><span style="margin-left:2%">App</span></div>
                                <div > <input class="form-control" type="radio" name="show_on" style="float:left;" value="Website"/><span style="margin-left:2%">Website</span></div>
                                <div ><input class="form-control" type="radio" checked name="show_on" style="float:left;" value="Both"/><span style="margin-left:2%">Both</span></div>
                            </div>
                                <div class="form-group">
                                <label>WareHouse</label>
                                
                                <select class="form-control ware_house select2 select_input_requred " multiple="" name="product_warehouse[]">
                                <option value="">Choose Warehouse</option>
                                <?php foreach($WareHouse as $house)
                                {
                                $warehous_list = "";
                                $warehous_list .= "<option value='".$house->id."' data-name='".$house->w_name."'>". $house->w_name."</option>";
                                ?>
                                <option value="<?php echo $house->id; ?>" data-name="<?php echo $house->w_name; ?>"><?php echo $house->w_name; ?></option>
                                
                                <?php } ?>
                                </select>
                                </div>
                                <!-- show store quantity when user choose warehouse -->
                            <div class="form-group Add_cost_quantity" ></div>   

                            </div> 
                        </div> 
                        <!--START ADD Inventory SECTION -->      
        <div class="row">
             <div class="col-md-12">
              <h4 class="page-title">Inventory</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">
                <div class="form-group">
                        <label>Low Stock Alert</label>
                        <input type="text" onkeyup="checkINt(this);" class="form-control validated_integer " name="order_sort" placeholder="1" />
                      </div>
                <div class="form-group">
                        <label>Batch/Lot no </label>
                        <input type="text" class="form-control" name="batchNumber" placeholder="Enter Batch/Lot no" />
                      </div>

                      <div class="form-group">
                        <label>Product Expiry </label>
                        <input type="text" class="form-control date_picker" name="expiry" placeholder="Product Expiry Date" />
        </div>     
                    </div>
              </div> 
        </div>
       <!--END ADD Inventory SECTION -->


         <!--START ADD Pricing SECTION -->      
         <div class="row">
             <div class="col-md-12">
              <h4 class="page-title">Pricing</h4> <br>
                
                    <div class="form-group">
                        <div class="col-md-6">
                          <div class="form-group">
                        <label>MRP</label>
                        <input type="text"  onkeyup="checkDec(this);"  class="form-control required_validation_for_product validated_numeric" name="Main_price" placeholder="Enter product price here, only numbers" />
                      </div>
                     
                        </div>
                        <div class="col-md-6" style="margin-top: 25px;">
                         <div class="input-group">
                        <span class="input-group-addon">
                        <input type="checkbox" class="discount_Click">
                      </span>
                      <input type="text" class="form-control discount_price"  onkeyup="checkDec(this);"  name="discount_price" placeholder="Enter discounted price here, only numbers">
                      </div>
                     </div>
                     <input type="hidden" name="pos_id" value="1"/>
                     
                                            <div class="col-md-12">


                                            <div class="col-md-6">
                      <div class="form-group">
                        <label>Sale price</label>
                        <input type="text"  onkeyup="checkDec(this);"  class="form-control required_validation_for_product" name="sale_price" placeholder="Enter product sale price" />
                      </div>
                       </div>

                       <div class="col-md-6">
                      <div class="form-group">
                        <label>Sales Tax&nbsp;&nbsp;<input type="checkbox" name="sales_tax_check"/></label>
                        <select class="form-control" name="sales_tax">
                        <option value="">Choose Sale Tax</option>
                        <?php 
                        foreach($taxData as $taxDataList)
                        {
                        ?>
                        <option value="<?php echo $taxDataList->houdinv_tax_id ?>"><?php echo $taxDataList->houdinv_tax_name ?></option>
                        <?php }
                        ?>
                        </select>
                      </div>
                       </div>
                       </div>
                        <div class="col-md-12">
                        <div class="col-md-6">
                          <div class="form-group">
                        <label>Cost Price</label>
                        <input type="text" onkeyup="checkDec(this);" id="costP"  class="required_validation_for_product form-control" name="cost_price" placeholder="Enter product cost price" />
                      </div>
                       </div>
                       <div class="col-sm-6">
                      <div class="form-group">
                        <label>Purchase Tax&nbsp;&nbsp;<input type="checkbox" name="purchase_tax_check"/></label>
                        <select class="form-control" name="purchase_tax">
                        <option value="">Choose Purchase Tax</option>
                        <?php 
                        foreach($taxData as $taxDataList)
                        {
                        ?>
                        <option value="<?php echo $taxDataList->houdinv_tax_id ?>"><?php echo $taxDataList->houdinv_tax_name ?></option>
                        <?php }
                        ?>
                        </select>
                      </div>
                      </div>
                       </div>                                                                              
                 
                 
                </div>
              </div> 
        </div>
                    </div> 
                    <div class="modal-footer"> 
                    <input type="submit" name="formSubmit"  value="submit" class="btn btn-info">
                    </div> 
                </div> 
                <?php echo form_close(); ?>
            </div>
        </div>
<!-- end product modal -->

<?php $this->load->view('Template/footer') ?>

<input type="hidden" id="today-date" value="<?php echo date('Y-m-d') ?>" />
<input type="hidden" id="base-url" value="<?php echo base_url(); ?>" />
<input type="hidden" id="warehouse-list" value="<?php echo $warehous_list; ?>" />
<?php 
$set_options = '';
foreach($productData as $productDataList)
{
$set_options .= "<option data-tax='".$productDataList['tax_price']."'data-cp='".$productDataList['cp']."' data-mrp='".$productDataList['mrp']."' data-price='".$productDataList['productPrice']."' data-stock='".$productDataList['stock']."' data-variant='".$productDataList['variantId']."' data-product='".$productDataList['produtcId']."'>".$productDataList['productName']."</option>";
}
?>
<script>
var product_list = "<?php echo $set_options ?>";
var base_url = "<?php echo base_url() ?>";
</script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom/page-js/pos/pos.js"></script>
    
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom/validation-js/add-product/add-product-validation.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom/page-js/add-product/add-product.js"></script>
<script type="text/javascript">
function checkDec(el){
  var ex = /^[0-9]+\.?[0-9]*$/;
  if(ex.test(el.value)==false) {
    el.value = el.value.substring(0,el.value.length - 1);
  }
}
function checkINt(el){
  var ex = /^[0-9]*$/;
  if(ex.test(el.value)==false) {
   el.value = el.value.substring(0,el.value.length - 1);
  }
}
</script>