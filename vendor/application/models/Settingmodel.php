<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settingmodel extends CI_Model{
  function __construct() 
  {
    parent::__construct();
    $this->load->helper('database');
    $getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
    $this->userTbl = '';
    $this->masterDB = $this->load->database('master', TRUE);
  }
  public function   AddShopInfo($data)
  {
    $already_data = $this->db->select("*")->from("houdinv_shop_detail")->get()->result();
    if(count($already_data))
    {
      $this->db->where("houdinv_shop_id",$already_data[0]->houdinv_shop_id);
      $re =  $this->db->update("houdinv_shop_detail",$data);
      if($re)
      {
        return true;
      }    
    }
    else
    {
      $data['houdinv_shop_cretaed_date'] = $data['houdinv_shop_updated_date'];
      $re =  $this->db->insert("houdinv_shop_detail",$data);
      if($re)
      {
        return true;
      }  
    }
  }
  public function AddvendorProfile($data)
  {
    $already_data = $this->db->select("*")->from("houdinv_shop_detail")->get()->result();
    if(count($already_data) > 0)
    {
      $getUpdateStatus = $this->db->where('houdinv_shop_id',$already_data[0]->houdinv_shop_id)->update('houdinv_shop_detail',array('profile_image'=>$data));
      if($getUpdateStatus)
      {
        return array('response','success','message'=>'Data updated successfully');  
      }
      else
      {
        return array('response','message_name','message'=>'Something went wrong. Please try again');  
      }
    }
    else
    {
      return array('response','message_name','message'=>'Something went wrong. Please try again');
    }
  }
  public function fetchShopData()
  {
    $all_data = $this->db->select("*")->from("houdinv_shop_detail")->get()->result(); 
    return $all_data;
  } 
  public function updateOrderConfig($data)
  {
    $this->db->select('houdinv_shop_order_configuration_id')->from('houdinv_shop_order_configuration');
    $getConfigData = $this->db->get()->result();
    $setDate = strtotime(date('Y-m-d'));
    if(count($getConfigData) > 0)
    {
      $setUpdateArayData = array('houdinv_shop_order_configuration_cancellation'=>$data['cancel'],'houdinv_shop_order_configuration_return'=>$data['return'],'houdinv_shop_order_configuration_replace'=>$data['replace'],'houdinv_shop_order_configuration_modified_date'=>$setDate,'houdinv_shop_order_configuration_sms_email'=>$data['sms_email'],'houdinv_shop_order_configuration_printing'=>$data['allow_printing']);
      $this->db->where('houdinv_shop_order_configuration_id',$getConfigData[0]->houdinv_shop_order_configuration_id);
      $getUpdateStatus = $this->db->update('houdinv_shop_order_configuration',$setUpdateArayData);
      if($getUpdateStatus == 1)
      {
        return array('message'=>'yes');
      }
      else
      {
        return array('message'=>'no');
      }
    }
    else
    {
      $setInsertArray = array('houdinv_shop_order_configuration_cancellation'=>$data['cancel'],'houdinv_shop_order_configuration_return'=>$data['return'],'houdinv_shop_order_configuration_replace'=>$data['replace'],'houdinv_shop_order_configuration_created_date'=>$setDate);
      $getInsertStatus = $this->db->insert('houdinv_shop_order_configuration',$setInsertArray);
      if($getInsertStatus == 1)
      {
        return array('message'=>'yes');
      }
      else
      {
        return array('message'=>'no');
      }
    }
  }
  public function fetchOrderConfigData()
  {
    $this->db->select('*')->from('houdinv_shop_order_configuration');
    $getData = $this->db->get()->result();
    return array('orderList'=>$getData);
  }

  public function save_customersetting($data)
  {
     if($data['id']){
          $this->db->where('id', $data['id']);
      
$getInsertData = $this->db->update('houdinv_customersetting',$data);  
         }else{
          $getInsertData = $this->db->insert('houdinv_customersetting',$data);
         }
            if($getInsertData == 1)
            {
                 return ture;
            }
            else
            {
                return false;
            }


  }
  public function fetchstoreCustomerSettingRecord(){
  $this->db->select('*')->from('houdinv_customersetting'); 
 $getcustomersetting = $this->db->get()->result();
        return $getcustomersetting;
}


public function save_taxsetting($data)
  {
     if($data['id']){
          $this->db->where('id', $data['id']);
      
$getInsertData = $this->db->update('houdinv_taxsetting',$data);  
         }else{
          $getInsertData = $this->db->insert('houdinv_taxsetting',$data);
         }
            if($getInsertData == 1)
            {
                 return ture;
            }
            else
            {
                return false;
            }


  }
    public function fetchtaxsettingRecord(){
  $this->db->select('*')->from('houdinv_taxsetting'); 
 $getcustomersetting = $this->db->get()->result();
        return $getcustomersetting;
}


public function save_skusetting($data)
  {
     if($data['id']){
          $this->db->where('id', $data['id']);
      
$getInsertData = $this->db->update('houdinv_skusetting',$data);  
         }else{
          $getInsertData = $this->db->insert('houdinv_skusetting',$data);
         }
            if($getInsertData == 1)
            {
                 return ture;
            }
            else
            {
                return false;
            }


  }
    public function fetchskusettingRecord(){

  $this->db->select('*')->from('houdinv_skusetting'); 
 $getcustomersetting =$this->db->get()->result();
        return $getcustomersetting;
}
  public function fetchinventorysettingRecord(){
  $this->db->select('*')->from('houdinv_inventorysetting'); 
 $getcustomersetting = $this->db->get()->result();
        return $getcustomersetting;
}
public function save_inventorysetting($data)
  {
     if($data['id']){
          $this->db->where('id', $data['id']);
      
$getInsertData = $this->db->update('houdinv_inventorysetting',$data);  
         }else{
          $getInsertData = $this->db->insert('houdinv_inventorysetting',$data);
         }
            if($getInsertData == 1)
            {
                 return ture;
            }
            else
            {
                return false;
            }


  }
  public function save_addpossetting($data)
  {
      if($data['id']){
          $this->db->where('id', $data['id']);
      
$getInsertData = $this->db->update('houdinv_possetting',$data);  
         }else{
          $getInsertData = $this->db->insert('houdinv_possetting',$data);
         }
            if($getInsertData == 1)
            {
                 return ture;
            }
            else
            {
                return false;
            }
  }
  public function fetchpossettingRecord()
  {
   $this->db->select('*')->from('houdinv_possetting'); 
 $getcustomersetting = $this->db->get()->result();
        return $getcustomersetting;
  }

 public function save_analytics($data)
  {
      if($data['id']){
          $this->db->where('id', $data['id']);
      
$getInsertData = $this->db->update('houdinv_google_analytics',$data);  
         }else{
          $getInsertData = $this->db->insert('houdinv_google_analytics',$data);
         }
            if($getInsertData == 1)
            {
                 return ture;
            }
            else
            {
                return false;
            }
  }
  public function fetchanalyticsRecord()
  {
   $this->db->select('*')->from('houdinv_google_analytics'); 
 $getcustomersetting = $this->db->get()->result();
        return $getcustomersetting;
  }

  public function save_invoicesetting($data)
  {
      if($data['id']){
          $this->db->where('id', $data['id']);
      
$getInsertData = $this->db->update('houdinv_invoicesetting',$data);  
         }else{
          $getInsertData = $this->db->insert('houdinv_invoicesetting',$data);
         }
            if($getInsertData == 1)
            {
                 return ture;
            }
            else
            {
                return false;
            }
  }
  public function fetchinvoicesettingRecord()
  {
   $this->db->select('*')->from('houdinv_invoicesetting'); 
 $getcustomersetting = $this->db->get()->result();
        return $getcustomersetting;
  }
public function fetchstoresettingRecord(){
$this->db->select('*')->from('houdinv_storesetting'); 
 $getSearchsocial = $this->db->get()->result();
         return $getSearchsocial;

 }

 public function save_storesetting($data){
 
 if($data['id']){
          $this->db->where('id', $data['id']);
      
$getInsertData = $this->db->update('houdinv_storesetting',$data);  
         }else{
          $getInsertData = $this->db->insert('houdinv_storesetting',$data);
         }
 if($getInsertData == 1){
                 return ture;
            }
            else
            {
                return false;
            }
          }
public function fetchsmsemailRecord($value=false)
{
 $this->masterDB->select('*')->from('houdin_sms_package');
  $this->masterDB->where('houdin_sms_package_status','active');
  if($value){$this->masterDB->where('houdin_sms_package_id',$value);} 
 $getSearchsocial = $this->masterDB->get()->result();
         return $getSearchsocial;
}
public function fetchsms_emailRecord($value=false)
{
 $this->masterDB->select('*')->from('houdin_email_package'); 
 $this->masterDB->where('houdin_email_package_status','active'); 
 if($value){$this->masterDB->where('houdin_email_package_id',$value);} 
 $getSearchsocial = $this->masterDB->get()->result();
         return $getSearchsocial;
}
public function fetch_smsemail_Record($value)
{
  $this->db->select('*')->from('houdinv_emailsms_stats'); 
  $this->db->where('houdinv_emailsms_stats_type',$value); 
 $getSearchsocial = $this->db->get()->result();
         return $getSearchsocial;
}

public function save_smsemail($data)
{
  if($data['houdinv_emailsms_stats_id']){
          $this->db->where('houdinv_emailsms_stats_id', $data['houdinv_emailsms_stats_id']);
      
$getInsertData = $this->db->update('houdinv_emailsms_stats',$data);  
         }else{
          $getInsertData = $this->db->insert('houdinv_emailsms_stats',$data);
         }
 if($getInsertData == 1){
                 return ture;
            }
            else
            {
                return false;
            }
}
public function updateSMS($sms,$transaction)
{
  $setData = strtotime(date('Y-m-d'));
  if($sms['product'] == 'sms')
  {
    $getSmsProduct = $this->db->select('houdinv_emailsms_stats_total_credit,houdinv_emailsms_stats_remaining_credits')->from('houdinv_emailsms_stats')->where('houdinv_emailsms_stats_type','sms')->get()->result();
    if(count($getSmsProduct) > 0)
    {
       // update accounts data
       $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
       if(count($getUndepositedAccount) > 0)
       {
          $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
          'houdinv_accounts_balance_sheet_ref_type'=>'Online Transaction',
          'houdinv_accounts_balance_sheet_pay_account'=>0,
          'houdinv_accounts_balance_sheet_payee_type'=>'Owner',
          'houdinv_accounts_balance_sheet_account'=>0,
          'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
          'houdinv_accounts_balance_sheet__increase'=>0,
          'houdinv_accounts_balance_sheet_decrease'=>$transaction['houdinv_transaction_amount'],
          'houdinv_accounts_balance_sheet_deposit'=>0,
          'houdinv_accounts_balance_sheet_final_balance'=>0,
          'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
          $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
          // get amount
          $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
          ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
          if(count($getTotalAmount) > 0)
          {
              $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
          }
          else
          {
              $setFinalAmount = 0;
          }
          $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
       }
       // end here
      $finalCredit = $getSmsProduct[0]->houdinv_emailsms_stats_total_credit+$sms['package'];
      $remainCredit = $getSmsProduct[0]->houdinv_emailsms_stats_remaining_credits+$sms['package'];
      $this->db->where('houdinv_emailsms_stats_type','sms');
      $getUpdate = $this->db->update('houdinv_emailsms_stats',array('houdinv_emailsms_stats_total_credit'=>$finalCredit,'houdinv_emailsms_stats_remaining_credits'=>$remainCredit));
      if($getUpdate)
      {
        $getTransaction = $this->db->insert('houdinv_transaction',$transaction);
        return array('message'=>'yes');
      }
      else
      {
        return array('message'=>'no');
      }

    }
    else
    {
      $setSMSInsertArray = array('houdinv_emailsms_stats_type'=>'sms','houdinv_emailsms_stats_total_credit'=>$sms['package'],'houdinv_emailsms_stats_remaining_credits'=>$sms['package'],'date_time'=>$setData);
      $getInsertData = $this->db->insert('houdinv_emailsms_stats',$setSMSInsertArray);
      if($getInsertData)
      {
        $getTransaction = $this->db->insert('houdinv_transaction',$transaction);
      }
      else
      {
        return array('message'=>'no');
      }
    }
  }
  else
  {
    $getSmsProduct = $this->db->select('houdinv_emailsms_stats_total_credit,houdinv_emailsms_stats_remaining_credits')->from('houdinv_emailsms_stats')->where('houdinv_emailsms_stats_type','email')->get()->result();
    if(count($getSmsProduct) > 0)
    {
      // update accounts data
      $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
      if(count($getUndepositedAccount) > 0)
      {
          $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
          'houdinv_accounts_balance_sheet_ref_type'=>'Online Transaction',
          'houdinv_accounts_balance_sheet_pay_account'=>0,
          'houdinv_accounts_balance_sheet_payee_type'=>'Owner',
          'houdinv_accounts_balance_sheet_account'=>0,
          'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
          'houdinv_accounts_balance_sheet__increase'=>0,
          'houdinv_accounts_balance_sheet_decrease'=>$transaction['houdinv_transaction_amount'],
          'houdinv_accounts_balance_sheet_deposit'=>0,
          'houdinv_accounts_balance_sheet_final_balance'=>0,
          'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
          $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
          // get amount
          $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
          ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
          if(count($getTotalAmount) > 0)
          {
              $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
          }
          else
          {
              $setFinalAmount = 0;
          }
          $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
      }
      // end here
      $finalCredit = $getSmsProduct[0]->houdinv_emailsms_stats_total_credit+$sms['package'];
      $remainCredit = $getSmsProduct[0]->houdinv_emailsms_stats_remaining_credits+$sms['package'];
      $this->db->where('houdinv_emailsms_stats_type','email');
      $getUpdate = $this->db->update('houdinv_emailsms_stats',array('houdinv_emailsms_stats_total_credit'=>$finalCredit,'houdinv_emailsms_stats_remaining_credits'=>$remainCredit));
      if($getUpdate)
      {
        $getTransaction = $this->db->insert('houdinv_transaction',$transaction);
        return array('message'=>'yes');
      }
      else
      {
        return array('message'=>'no');
      }

    }
    else
    {
      $setSMSInsertArray = array('houdinv_emailsms_stats_type'=>'email','houdinv_emailsms_stats_total_credit'=>$sms['package'],'houdinv_emailsms_stats_remaining_credits'=>$sms['package'],'date_time'=>$setData);
      $getInsertData = $this->db->insert('houdinv_emailsms_stats',$setSMSInsertArray);
      if($getInsertData)
      {
        $getTransaction = $this->db->insert('houdinv_transaction',$transaction);
      }
      else
      {
        return array('message'=>'no');
      }
    }
  }
}
public function updateFailureTransaction($data)
{
  $getData = $this->db->insert('houdinv_transaction',$data);
}
public function getSMSTotalCount()
{
  $gteCount = $this->db->select('COUNT(houdinv_sms_log_id) AS SMSCOunt')->from('houdinv_sms_log')->get()->result();
  return array('totalRows'=>$$gteCount[0]->SMSCOunt);
}
public function getemailTotalCount()
{
  $gteCount = $this->db->select('COUNT(houdinv_email_log_id) AS emailCOunt')->from('houdinv_email_log')->get()->result();
  return array('totalRows'=>$$gteCount[0]->emailCOunt);
}
public function getsmslog($data)
{
    $this->db->select('*')->from('houdinv_sms_log');
    if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getSMSLogdata = $this->db->get();
        $resultsmsdata = ($getSMSLogdata->num_rows() > 0)?$getSMSLogdata->result():FALSE;
        return array('smslog'=>$resultsmsdata);
}
public function getEmailLog($data)
{
  $this->db->select('*')->from('houdinv_email_log');
    if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getEmailLogdata = $this->db->get();
        $resultEmaildata = ($getEmailLogdata->num_rows() > 0)?$getEmailLogdata->result():FALSE;
        return array('emaillog'=>$resultEmaildata);
}
public function addNewTaxData($data,$id = false)
{
  if($id)
  {
    return $this->db->where('houdinv_tax_id',$id)->update('houdinv_taxes',$data);
  }
  else
  {
    return $this->db->insert('houdinv_taxes',$data);
  }
}
public function fetchtaxData()
{
  return $this->db->select('*')->from('houdinv_taxes')->get()->result();
}
public function deleteTax($data)
{
  return $this->db->where('houdinv_tax_id',$data)->delete('houdinv_taxes');
}
}