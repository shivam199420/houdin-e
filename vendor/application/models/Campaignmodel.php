<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Campaignmodel extends CI_Model{
  function __construct() 
  {
    parent::__construct();
    $this->load->helper('database');
    $getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
    $this->masterDB = $this->load->database('master', TRUE);
  }
 public function fetch_customerRecord()
 {
 	 // fetch customer list
        $this->db->select('houdinv_user_id,houdinv_user_name')->from('houdinv_users')->where('houdinv_user_is_active','active');
        $getCustomerList = $this->db->get()->result();
        // fethch Customer Group
        $this->db->select('houdinv_users_group_id,houdinv_users_group_name')->from('houdinv_users_group')->where('houdinv_users_group_status','active');
        $getCustomerGroupList = $this->db->get()->result();
        return array('customerList'=>$getCustomerList,'customerGroupLits'=>$getCustomerGroupList);
 	 
 }
 public function fetch_groupTocustomer($data='')
 {
 	//$campaignCustomers_group= explode(",", $data);
$conctect_data='';

 	$this->db->select('houdinv_users_group_users_id')->from('houdinv_users_group_users_list')->where_in('houdinv_users_group__user_group_id',$data);
         $getCustomerList = $this->db->get()->result();
         foreach ($getCustomerList as $key => $value) {
         $conctect_data.=$value->houdinv_users_group_users_id.',';
         }

        return rtrim($conctect_data,','); 
 }
 	public function insertSMSCampaign($data)
 	{
 		$setData = strtotime(date('Y-m-d'));
 		$setNextdate = strtotime($data['date']);
 		$setCampaignInsertArray = array('campaign_name'=>$data['name'],'campaign_status'=>$data['status'],'campaign_text'=>$data['text'],'campaign_date'=>$setNextdate,'date_time'=>$setData);
 		$this->db->insert('houdinv_campaignsms',$setCampaignInsertArray);
 		$getCampignId = $this->db->insert_id();
 		if($getCampignId)
 		{
 			for($index = 0; $index < count($data['customer']); $index++)
 			{
 				$setInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$data['customer'][$index]);
 				$this->db->insert('houdinv_campaignsms_user',$setInsertArray);
 			}
 			$this->db->select('houdinv_users_group__user_group_id,houdinv_users_group_users_id')->from('houdinv_users_group_users_list')->where_in('houdinv_users_group__user_group_id',$data['customerGroup']);
 			$getCustomerGoupList = $this->db->get()->result();
 			foreach($getCustomerGoupList as $getCustomerGoupListData)
 			{
 				$setCampaignInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$getCustomerGoupListData->houdinv_users_group_users_id,'campaignsms_group_id'=>$getCustomerGoupListData->houdinv_users_group__user_group_id);
 				$this->db->insert('houdinv_campaignsms_user',$setCampaignInsertArray);
 			}
 			return array('message'=>'yes');
 		}
 		else
 		{
 			return array('message'=>'no');
 		}
 	}


 	public function fetch_current_campaign()
 	{
 		 // fethch Customer Group
        $this->db->select('*')->from('houdinv_campaignsms');
        $getcampaignsmsList = $this->db->get()->result();

        $this->db->select('COUNT(*) as total_campag')->from('houdinv_campaignsms');
        $total_campags = $this->db->get()->result();

         $this->db->select('COUNT(*) as Active_campag')->from('houdinv_campaignsms');
         $this->db->where('campaign_status',1);

        $Active_campagList = $this->db->get()->result();

       $this->db->select('COUNT(*) as Deactive_campag')->from('houdinv_campaignsms');
       $this->db->where('campaign_status !=',1);
        $Deactive_campagList = $this->db->get()->result(); 

        return array('campaignsms'=>$getcampaignsmsList,'total_campags'=>$total_campags[0],'Active_campag'=>$Active_campagList[0],'Deactive_campag'=>$Deactive_campagList[0]);
 	}

 	public function deleteSMSCampaign($delete)
 	{
 		$delete_id= $this->db->delete('houdinv_campaignsms', array('campaignsms_id' => $delete));
 		           $this->db->delete('houdinv_campaignsms_user', array('campaignsms_id' => $delete));
 		if($delete_id){
 			return true;
 		}else{
 			return false;
 		}
 	}
 	public function fetch_CampaignEditRecord($value)
 	{
 $this->db->select('houdinv_user_id,houdinv_user_name')->from('houdinv_users')->where('houdinv_user_is_active','active');
        $getCustomerList = $this->db->get()->result();
        // fethch Customer Group
        $this->db->select('houdinv_users_group_id,houdinv_users_group_name')->from('houdinv_users_group')->where('houdinv_users_group_status','active');
        $getCustomerGroupList = $this->db->get()->result();


 		  $this->db->select('*')->from('houdinv_campaignsms');
 		    $this->db->where('campaignsms_id',$value);
        $getcampaignsmsList = $this->db->get()->result();


    $this->db->select('campaignsms_users_id')->from('houdinv_campaignsms_user');
 		    $this->db->where('campaignsms_id',$value);
 		    $this->db->where('campaignsms_group_id',0);
        $campaignsms_users = $this->db->get()->result();
          $this->db->select('campaignsms_group_id')->from('houdinv_campaignsms_user');
 		    $this->db->where('campaignsms_id',$value);
 		    $this->db->where('campaignsms_group_id !=',0);
        $campaignsms_group = $this->db->get()->result();    


        return array('customerList'=>$getCustomerList,'customerGroupLits'=>$getCustomerGroupList,'getcampaignsmsList'=>$getcampaignsmsList,'campaignsms_users'=>$campaignsms_users,'campaignsms_group'=>$campaignsms_group);
 		 
 	}

 	public function updateSMSCampaign($data)
 	{
 	 $setData = strtotime(date('Y-m-d'));
 		$setNextdate = strtotime($data['date']);
 		$setCampaignInsertArray = array('campaign_name'=>$data['name'],'campaign_status'=>$data['status'],'campaign_text'=>$data['text'],'campaign_date'=>$setNextdate,'date_time'=>$setData);
 		 $this->db->where('campaignsms_id',$data['campaignsms_id']);
 		$this->db->update('houdinv_campaignsms',$setCampaignInsertArray);
 		
 		$getCampignId = $data['campaignsms_id'];
 		if($getCampignId)
 		{
 			 $this->db->delete('houdinv_campaignsms_user', array('campaignsms_id' => $getCampignId));

 			for($index = 0; $index < count($data['customer']); $index++)
 			{
 				$setInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$data['customer'][$index]);
 				$this->db->insert('houdinv_campaignsms_user',$setInsertArray);
 			}
 			$this->db->select('houdinv_users_group__user_group_id,houdinv_users_group_users_id')->from('houdinv_users_group_users_list')->where_in('houdinv_users_group__user_group_id',$data['customerGroup']);
 			$getCustomerGoupList = $this->db->get()->result();
 			foreach($getCustomerGoupList as $getCustomerGoupListData)
 			{
 				$setCampaignInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$getCustomerGoupListData->houdinv_users_group_users_id,'campaignsms_group_id'=>$getCustomerGoupListData->houdinv_users_group__user_group_id);
 				$this->db->insert('houdinv_campaignsms_user',$setCampaignInsertArray);
 			}
 			return array('message'=>'yes');
 		}
 		else
 		{
 			return array('message'=>'no');
 		}
 	}
  public function Delete_multiSMSCampaign($delete)
  {
    if($delete){
      $this->db->where_in('campaignsms_id',$delete);
      $delete_id= $this->db->delete('houdinv_campaignsms');
      $this->db->where_in('campaignsms_id',$delete);
               $this->db->delete('houdinv_campaignsms_user');
           
           return true;

             } 
             return false;

          
  }

  public function fetch_shop_logo_campaign($type=false)
  {
  $this->db->select('*')->from('houdinv_shop_logo');
if($type=='favicon'){$this->db->or_where('clear_favicon',0);}else{ $this->db->or_where('clear',0);} 
 $getSearchData = $this->db->get()->result();
        return $getSearchData;
  }

  // Email Code To Deepak Kumar

  public function fetch_current_campaignemail()
  {
     // fethch Customer Group
        $this->db->select('*')->from('houdinv_campaignemail');
        $getcampaignsmsList = $this->db->get()->result();

        $this->db->select('COUNT(*) as total_campag')->from('houdinv_campaignemail');
        $total_campags = $this->db->get()->result();

         $this->db->select('COUNT(*) as Active_campag')->from('houdinv_campaignemail');
         $this->db->where('campaign_status',1);

        $Active_campagList = $this->db->get()->result();

       $this->db->select('COUNT(*) as Deactive_campag')->from('houdinv_campaignemail');
       $this->db->where('campaign_status !=',1);
        $Deactive_campagList = $this->db->get()->result(); 

        return array('campaignsms'=>$getcampaignsmsList,'total_campags'=>$total_campags[0],'Active_campag'=>$Active_campagList[0],'Deactive_campag'=>$Deactive_campagList[0]);
  }

  public function Delete_multiEMAILCampaign($delete)
  {
    if($delete){
      $this->db->where_in('campaignsms_id',$delete);
      $delete_id= $this->db->delete('houdinv_campaignemail');
      $this->db->where_in('campaignsms_id',$delete);
               $this->db->delete('houdinv_campaignemail_user');
           
           return true;

             } 
             return false;

          
  }
    public function deleteEMAILCampaign($delete)
  {
    $delete_id= $this->db->delete('houdinv_campaignemail', array('campaignsms_id' => $delete));
               $this->db->delete('houdinv_campaignemail_user', array('campaignsms_id' => $delete));
    if($delete_id){
      return true;
    }else{
      return false;
    }
  }

  public function insertEMAILCampaign($data)
  {
    $setData = strtotime(date('Y-m-d'));
    $setNextdate = strtotime($data['date']);
    $setCampaignInsertArray = array('campaign_name'=>$data['name'],'campaign_status'=>$data['status'],'campaign_text'=>$data['text'],'campaign_date'=>$setNextdate,'date_time'=>$setData);
    $this->db->insert('houdinv_campaignemail',$setCampaignInsertArray);
    $getCampignId = $this->db->insert_id();
    if($getCampignId)
    {
      for($index = 0; $index < count($data['customer']); $index++)
      {
        $setInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$data['customer'][$index]);
        $this->db->insert('houdinv_campaignemail_user',$setInsertArray);
      }
      $this->db->select('houdinv_users_group__user_group_id,houdinv_users_group_users_id')->from('houdinv_users_group_users_list')->where_in('houdinv_users_group__user_group_id',$data['customerGroup']);
      $getCustomerGoupList = $this->db->get()->result();
      foreach($getCustomerGoupList as $getCustomerGoupListData)
      {
        $setCampaignInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$getCustomerGoupListData->houdinv_users_group_users_id,'campaignsms_group_id'=>$getCustomerGoupListData->houdinv_users_group__user_group_id);
        $this->db->insert('houdinv_campaignemail_user',$setCampaignInsertArray);
      }
      return array('message'=>'yes');
    }
    else
    {
      return array('message'=>'no');
    }
  }


  public function fetch_CampaignEdit_emailRecord($value)
  {
 $this->db->select('houdinv_user_id,houdinv_user_name')->from('houdinv_users')->where('houdinv_user_is_active','active');
        $getCustomerList = $this->db->get()->result();
        // fethch Customer Group
        $this->db->select('houdinv_users_group_id,houdinv_users_group_name')->from('houdinv_users_group')->where('houdinv_users_group_status','active');
        $getCustomerGroupList = $this->db->get()->result();


      $this->db->select('*')->from('houdinv_campaignemail');
        $this->db->where('campaignsms_id',$value);
        $getcampaignsmsList = $this->db->get()->result();


    $this->db->select('campaignsms_users_id')->from('houdinv_campaignemail_user');
        $this->db->where('campaignsms_id',$value);
        $this->db->where('campaignsms_group_id',0);
        $campaignsms_users = $this->db->get()->result();
          $this->db->select('campaignsms_group_id')->from('houdinv_campaignemail_user');
        $this->db->where('campaignsms_id',$value);
        $this->db->where('campaignsms_group_id !=',0);
        $campaignsms_group = $this->db->get()->result();    


        return array('customerList'=>$getCustomerList,'customerGroupLits'=>$getCustomerGroupList,'getcampaignsmsList'=>$getcampaignsmsList,'campaignsms_users'=>$campaignsms_users,'campaignsms_group'=>$campaignsms_group);
     
  }

public function updateEMAILCampaign($data)
  {
   $setData = strtotime(date('Y-m-d'));
    $setNextdate = strtotime($data['date']);
    $setCampaignInsertArray = array('campaign_name'=>$data['name'],'campaign_status'=>$data['status'],'campaign_text'=>$data['text'],'campaign_date'=>$setNextdate,'date_time'=>$setData);
     $this->db->where('campaignsms_id',$data['campaignsms_id']);
    $this->db->update('houdinv_campaignemail',$setCampaignInsertArray);
    
    $getCampignId = $data['campaignsms_id'];
    if($getCampignId)
    {
       $this->db->delete('houdinv_campaignemail_user', array('campaignsms_id' => $getCampignId));

      for($index = 0; $index < count($data['customer']); $index++)
      {
        $setInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$data['customer'][$index]);
        $this->db->insert('houdinv_campaignemail_user',$setInsertArray);
      }
      $this->db->select('houdinv_users_group__user_group_id,houdinv_users_group_users_id')->from('houdinv_users_group_users_list')->where_in('houdinv_users_group__user_group_id',$data['customerGroup']);
      $getCustomerGoupList = $this->db->get()->result();
      foreach($getCustomerGoupList as $getCustomerGoupListData)
      {
        $setCampaignInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$getCustomerGoupListData->houdinv_users_group_users_id,'campaignsms_group_id'=>$getCustomerGoupListData->houdinv_users_group__user_group_id);
        $this->db->insert('houdinv_campaignemail_user',$setCampaignInsertArray);
      }
      return array('message'=>'yes');
    }
    else
    {
      return array('message'=>'no');
    }
  }
  // Push Notification Code start 



  public function fetch_current_campaign_push()
  {
     // fethch Customer Group
        $this->db->select('*')->from('houdinv_campaignpush');
        $getcampaignsmsList = $this->db->get()->result();

        $this->db->select('COUNT(*) as total_campag')->from('houdinv_campaignpush');
        $total_campags = $this->db->get()->result();

         $this->db->select('COUNT(*) as Active_campag')->from('houdinv_campaignpush');
         $this->db->where('campaign_status',1);

        $Active_campagList = $this->db->get()->result();

       $this->db->select('COUNT(*) as Deactive_campag')->from('houdinv_campaignpush');
       $this->db->where('campaign_status !=',1);
        $Deactive_campagList = $this->db->get()->result(); 

        return array('campaignsms'=>$getcampaignsmsList,'total_campags'=>$total_campags[0],'Active_campag'=>$Active_campagList[0],'Deactive_campag'=>$Deactive_campagList[0]);
  }

public function Delete_multiPUSHCampaign($delete)
  {
    if($delete){
      $this->db->where_in('campaignsms_id',$delete);
      $delete_id= $this->db->delete('houdinv_campaignpush');
      $this->db->where_in('campaignsms_id',$delete);
               $this->db->delete('houdinv_campaignpush_user');
           
           return true;

             } 
             return false;

          
  }
  public function deletePUSHCampaign($delete)
  {
    $delete_id= $this->db->delete('houdinv_campaignpush', array('campaignsms_id' => $delete));
               $this->db->delete('houdinv_campaignpush_user', array('campaignsms_id' => $delete));
    if($delete_id){
      return true;
    }else{
      return false;
    }
  }

  public function fetch_CampaignEdit_pushRecord($value)
  {
 $this->db->select('houdinv_user_id,houdinv_user_name')->from('houdinv_users')->where('houdinv_user_is_active','active');
        $getCustomerList = $this->db->get()->result();
        // fethch Customer Group
        $this->db->select('houdinv_users_group_id,houdinv_users_group_name')->from('houdinv_users_group')->where('houdinv_users_group_status','active');
        $getCustomerGroupList = $this->db->get()->result();


      $this->db->select('*')->from('houdinv_campaignpush');
        $this->db->where('campaignsms_id',$value);
        $getcampaignsmsList = $this->db->get()->result();


    $this->db->select('campaignsms_users_id')->from('houdinv_campaignpush_user');
        $this->db->where('campaignsms_id',$value);
        $this->db->where('campaignsms_group_id',0);
        $campaignsms_users = $this->db->get()->result();
          $this->db->select('campaignsms_group_id')->from('houdinv_campaignpush_user');
        $this->db->where('campaignsms_id',$value);
        $this->db->where('campaignsms_group_id !=',0);
        $campaignsms_group = $this->db->get()->result();    


        return array('customerList'=>$getCustomerList,'customerGroupLits'=>$getCustomerGroupList,'getcampaignsmsList'=>$getcampaignsmsList,'campaignsms_users'=>$campaignsms_users,'campaignsms_group'=>$campaignsms_group);
     
  }
public function insertPUSHCampaign($data)
  {
    $setData = strtotime(date('Y-m-d'));
    $setNextdate = strtotime($data['date']);
    $setCampaignInsertArray = array('campaign_name'=>$data['name'],'campaign_status'=>$data['status'],'campaign_text'=>$data['text'],'campaign_date'=>$setNextdate,'date_time'=>$setData);
    $this->db->insert('houdinv_campaignpush',$setCampaignInsertArray);
    $getCampignId = $this->db->insert_id();
    if($getCampignId)
    {
      for($index = 0; $index < count($data['customer']); $index++)
      {
        $setInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$data['customer'][$index]);
        $this->db->insert('houdinv_campaignpush_user',$setInsertArray);
      }
      $this->db->select('houdinv_users_group__user_group_id,houdinv_users_group_users_id')->from('houdinv_users_group_users_list')->where_in('houdinv_users_group__user_group_id',$data['customerGroup']);
      $getCustomerGoupList = $this->db->get()->result();
      foreach($getCustomerGoupList as $getCustomerGoupListData)
      {
        $setCampaignInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$getCustomerGoupListData->houdinv_users_group_users_id,'campaignsms_group_id'=>$getCustomerGoupListData->houdinv_users_group__user_group_id);
        $this->db->insert('houdinv_campaignpush_user',$setCampaignInsertArray);
      }
      return array('message'=>'yes');
    }
    else
    {
      return array('message'=>'no');
    }
  }
public function updatePUSHCampaign($data)
  {
   $setData = strtotime(date('Y-m-d'));
    $setNextdate = strtotime($data['date']);
    $setCampaignInsertArray = array('campaign_name'=>$data['name'],'campaign_status'=>$data['status'],'campaign_text'=>$data['text'],'campaign_date'=>$setNextdate,'date_time'=>$setData);
     $this->db->where('campaignsms_id',$data['campaignsms_id']);
    $this->db->update('houdinv_campaignpush',$setCampaignInsertArray);
    
    $getCampignId = $data['campaignsms_id'];
    if($getCampignId)
    {
       $this->db->delete('houdinv_campaignpush_user', array('campaignsms_id' => $getCampignId));

      for($index = 0; $index < count($data['customer']); $index++)
      {
        $setInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$data['customer'][$index]);
        $this->db->insert('houdinv_campaignpush_user',$setInsertArray);
      }
      $this->db->select('houdinv_users_group__user_group_id,houdinv_users_group_users_id')->from('houdinv_users_group_users_list')->where_in('houdinv_users_group__user_group_id',$data['customerGroup']);
      $getCustomerGoupList = $this->db->get()->result();
      foreach($getCustomerGoupList as $getCustomerGoupListData)
      {
        $setCampaignInsertArray = array('campaignsms_id'=>$getCampignId,'campaignsms_users_id'=>$getCustomerGoupListData->houdinv_users_group_users_id,'campaignsms_group_id'=>$getCustomerGoupListData->houdinv_users_group__user_group_id);
        $this->db->insert('houdinv_campaignpush_user',$setCampaignInsertArray);
      }
      return array('message'=>'yes');
    }
    else
    {
      return array('message'=>'no');
    }
  }



}

  ?>