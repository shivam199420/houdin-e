<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_product_variants_table extends CI_Migration {
    
    /**
     * construct function
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }
    
    public function up()
    {
        $fields = array(
            'houdin_products_variant_warehouse' => array(
              'type' => 'VARCHAR',
              'constraint' => 255,
              'after' => 'houdin_products_variants_default'
            )
          ); 
          $this->dbforge->add_column('houdinv_products_variants', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('houdinv_products_variants', 'houdin_products_variant_warehouse');
    }
}