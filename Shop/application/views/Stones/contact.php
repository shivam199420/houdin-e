<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header') ?>

    <!-- ================== START MAIN SECTION ================== -->
    <div class="main" role="main">
        <!-- Start page location -->
        <div class="page-location">
            <div class="page-location-layer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-location-title">
                                <h3>Contact Us</h3>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start contact us area -->
        <div class="contact-us">
            <div class="contactus-info">
                <div class="container">
                <?php
                if($this->session->flashdata('error'))
                {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                }
                else if($this->session->flashdata('success'))
                {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                }
                ?>
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <div class="contactus-single-info">
                                <div class="info-icon">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <h6 class="contact-address">address</h6>
                                <span>
                                <?php echo $shopInfo[0]->houdinv_shop_address ?>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="contactus-single-info">
                                <div class="info-icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <h6 class="contact-phone">phone number</h6>
                                <span><?php echo $shopInfo[0]->houdinv_shop_contact_info ?></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="contactus-single-info">
                                <div class="info-icon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <h6 class="contact-email">email address</h6>
                                <span> <?php echo $shopInfo[0]->houdinv_shop_customer_care_email ?> </a></span>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="contactus-form">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                        <?php echo form_open(base_url( 'Contact/addcontact' ), array( 'id' => 'shopContact', 'method'=>'post' ));?>
                                <div class="row">
                                    <div class="col-md-12">
                                    <input type="text" name="userName" class="form-control required_validation_for_shop_contact name_validation" placeholder="Name*" />
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-12">
                                    <input type="text" name="userEmail" class="form-control required_validation_for_shop_contact email_validation name_validation" placeholder="Email*" />
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-12">
                                <input type="text" name="userEmailSub" class="form-control required_validation_for_shop_contact name_validation" placeholder="Subject" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    <textarea name="userEmailBody" rows="5" class="form-control required_validation_for_shop_contact name_validation" placeholder="Message"></textarea>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">send message</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ================== END MAIN SECTION ================== -->
    <?php 
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view(''.$getShopName.'/Template/footer') ?>

    <script type="text/javascript">
		$(document).ready(function(){
			$(document).on('submit','#shopContact',function(){
				var check_required_field=0;
				$(this).find(".required_validation_for_shop_contact").each(function(){
					var val22 = $(this).val();
					if (!val22){
						check_required_field++;
						$(this).css("border-color","#ccc");
						$(this).css("border-color","red");
					}
					$(this).on('keypress change',function(){
						$(this).css("border-color","#ccc");
					});
				});
				if(check_required_field != 0)
				{
					return false;
				}
				else {
					return true;
				}
			});
		});
		</script>