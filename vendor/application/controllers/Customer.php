<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Customer extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
        $this->load->helper('form');
        $this->load->library('Pdf');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Customermodel');
        $this->perPage = 100;
        $this->auth_users->is_logged_in();
	}
    
    
       public function Customerlanding()
    {
        $this->load->view('customermanagementlandingpage');
    } 
    
    public function index()
    {
        if($this->session->userdata('vendorAuth') == "")
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // Custom Pagination
        $getTotalCount = $this->Customermodel->getCustomerTotalCount();
        $config['base_url']    = base_url().'expence/';
        $config['uri_segment'] = 1;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(1);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getCustomerData = $this->Customermodel->fetchCustomerRecord($setArray);
        $this->load->view('customer',$getCustomerData);
    }
    // search customer Data
    public function advanceCustomerSearch()
    {
        $setSearchArray = array('name'=>$this->input->post('customerName'),'contact'=>$this->input->post('customerContact'),'email'=>$this->input->post('customerEmail'),'status'=>$this->input->post('customerStatus'));
        $getSearch = $this->Customermodel->searchCustomerData($setSearchArray);
        $this->session->set_flashdata('searchData',$getSearch);
        
        $this->session->set_flashdata('customerName',$this->input->post('customerName'));
        $this->session->set_flashdata('customerContact',$this->input->post('customerContact'));
        $this->session->set_flashdata('customerEmail',$this->input->post('customerEmail'));
        $this->session->set_flashdata('customerStatus',$this->input->post('customerStatus'));
        
        $this->load->helper('url');
        redirect(base_url()."Customer", 'refresh');
    }
    public function add()
    {
        if($this->session->userdata('vendorAuth') == "")
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // add customer
        if($this->input->post('customerAddDriver'))
        {
            $this->form_validation->set_rules('customerName','text','required');
            $this->form_validation->set_rules('customerMobile','text','required');
            $this->form_validation->set_rules('customerEmail','text','required|valid_email');
            if($this->form_validation->run() == true)
            {
                $setInsertArray = array('name'=>$this->input->post('customerName'),'mobile'=>$this->input->post('customerMobile'),'customerAlternate'=>$this->input->post('customerAlternate'),'email'=>$this->input->post('customerEmail'),
                'address'=>$this->input->post('customerAddress'),'landmark'=>$this->input->post('customerLandmark'),'country'=>$this->input->post('customerCountry'),'state'=>$this->input->post('customerState'),
                'city'=>$this->input->post('customerCity'),'pin'=>$this->input->post('customerPin'),'status'=>$this->input->post('customerStatus'));
                $getUserStatus = $this->Customermodel->addCustomerData($setInsertArray);
                if($getUserStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Customer addedd successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/add", 'refresh');
                }
                else if($getUserStatus['message'] == 'email')
                {
                    $this->session->set_flashdata('error','Email address is already exist');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/add", 'refresh');
                }
                else if($getUserStatus['message'] == 'phone')
                {
                    $this->session->set_flashdata('error','Contact number is already exist');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/add", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/add", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fileds with * are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer/add", 'refresh');
            }
        }
        $this->load->view('addcustomer');
    }
    public function edit()
    {
        $this->load->view('editcustomer');
    }
    public function view()
    {
        if($this->session->userdata('vendorAuth') == "")
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        $getCustomerId = $this->uri->segment('3');
        if($getCustomerId == "")
        {
            $this->load->helper('url');
            redirect(base_url()."customer", 'refresh');
        }
        $getCustomerDetails = $this->Customermodel->fetchCustomerDetails($getCustomerId);
        $this->load->view('viewcustomer',$getCustomerDetails);
    }
    public function privilegedcustomer()
    {
        
         // Custom Pagination
        $getTotalCount = $this->Customermodel->getPrivilageCustomerTotalCount();
        $config['base_url']    = base_url().'Customer/privilegedcustomer';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getCustomerData = $this->Customermodel->fetchPrivilageCustomerRecord($setArray);
       
      
       
        $this->load->view('privilegedcustomer',$getCustomerData);
    }
    public function paymentPending()
    {
        
                 // Custom Pagination
        $getTotalCount = $this->Customermodel->getCustomerPendingTotalCount();
        $config['base_url']    = base_url().'Customer/paymentPending';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getCustomerData = $this->Customermodel->fetchPendingCustomerRecord($setArray);
       
      
        
        $this->load->view('customeropaymentpending',$getCustomerData);
    }
    // delete customer 
    public function deleteCustomer()
    {
        if($this->input->post('deleteCustomerData'))
        {
            $this->form_validation->set_rules('deleteCustomerId','text','required');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->Customermodel->deleteCustomerData($this->input->post('deleteCustomerId'));
                if($getDeleteStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Customer deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Customer", 'refresh');
            }
        }
    }
    // add single customer tag
    public function addTag()
    {
        if($this->input->post('addSingleTag'))
        {
            $this->form_validation->set_rules('customerTagId','text','required');
            $this->form_validation->set_rules('addCustomerTagData','text','required');
            if($this->form_validation->run() == true)
            {
                $setTagArray = array('id'=>$this->input->post('customerTagId'),'tag'=>$this->input->post('addCustomerTagData'));
               $getTagStatus =  $this->Customermodel->addSingleCustomerTag($setTagArray);
               if($getTagStatus['message'] == 'yes')
               {
                    $this->session->set_flashdata('success','Tag addedd successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
               }
               else
               {
                    $this->session->set_flashdata('error','Some tag are not added. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
               }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer", 'refresh');
            }
        }
    }
    // update customer status
    public function changeStatus()
    {
        if($this->input->post('updateCustomerStatus'))
        {
            $this->form_validation->set_rules('changeCustomerStatusId','text','required|integer');
            $this->form_validation->set_rules('chooseStatus','text','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setStatusArray = array('id'=>$this->input->post('changeCustomerStatusId'),'status'=>$this->input->post('chooseStatus'));
                $getChangeStatus = $this->Customermodel->updateCustomerStatus($setStatusArray);
                if($getChangeStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Status updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer", 'refresh');
            }
        }
    }
    
    // update customer privilage
       public function changePrivilage()
    {
        if($this->input->post('updateCustomerPrivilage'))
        {
            $this->form_validation->set_rules('changeCustomerPrivilageId','text','required|integer');
            $this->form_validation->set_rules('choosePrivilage','Privilage','required|numeric');
            if($this->form_validation->run() == true)
            {
                $setPrivilageArray = array('id'=>$this->input->post('changeCustomerPrivilageId'),'Privilage'=>$this->input->post('choosePrivilage'));
                $getChangePrivilage = $this->Customermodel->updateCustomerPrivilage($setPrivilageArray);
                if($getChangePrivilage['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Privilage updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer", 'refresh');
            }
        }
    } 
  
  
      // update customer privilage from privilage page
       public function changePrivilagePage()
    {
        if($this->input->post('updateCustomerPrivilage'))
        {
            $this->form_validation->set_rules('changeCustomerPrivilageId','text','required|integer');
            $this->form_validation->set_rules('choosePrivilage','Privilage','required|numeric');
            if($this->form_validation->run() == true)
            {
                $setPrivilageArray = array('id'=>$this->input->post('changeCustomerPrivilageId'),'Privilage'=>$this->input->post('choosePrivilage'));
                $getChangePrivilage = $this->Customermodel->updateCustomerPrivilage($setPrivilageArray);
                if($getChangePrivilage['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Privilage updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/privilegedcustomer", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/privilegedcustomer", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer/privilegedcustomer", 'refresh');
            }
        }
    } 
    
    
        // update customer privilage
       public function changePendingAmount()
    {
        if($this->input->post('updateCustomerPendingAmount'))
        {
            $this->form_validation->set_rules('changeCustomerPendingId','text','required|integer');
            $this->form_validation->set_rules('choosePendingAmount','Amount','required|numeric');
            if($this->form_validation->run() == true)
            {
                $setPendingArray = array('id'=>$this->input->post('changeCustomerPendingId'),'pending'=>$this->input->post('choosePendingAmount'));
                $getChangePending = $this->Customermodel->updateCustomerPendingAmount($setPendingArray);
                if($getChangePending['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Pending amount updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer", 'refresh');
            }
        }
    } 
    
       
        // update customer privilage page 
       public function changePendingAmountPage()
    {
        if($this->input->post('updateCustomerPendingAmount'))
        {
            $this->form_validation->set_rules('changeCustomerPendingId','text','required|integer');
            $this->form_validation->set_rules('choosePendingAmount','Amount','required|numeric');
            if($this->form_validation->run() == true)
            {
                $setPendingArray = array('id'=>$this->input->post('changeCustomerPendingId'),'pending'=>$this->input->post('choosePendingAmount'));
                $getChangePending = $this->Customermodel->updateCustomerPendingAmount($setPendingArray);
                if($getChangePending['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Pending amount updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/paymentPending", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/paymentPending", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer/paymentPending", 'refresh');
            }
        }
    }  
    // send payment remonder
    public function sendPaymentReminderEmail()
    {
        $getSmsEmail = smsemailcredentials();
        $getLogo = fetchvedorsitelogo();
        $getInfo = fetchvendorinfo();
        $getSocial = fetchsocialurl(); 
        if(count($getSocial) > 0)
        {
            $setSocialString = "";
            if($getSocial[0]->facebook_url)
            {
                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
            }
            if($getSocial[0]->twitter_url)
            {
                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
            }
            if($getSocial[0]->youtube_url)
            {
                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
            }
        }
        $this->form_validation->set_rules('paymentReminderEmail','text','required|valid_email');
        $this->form_validation->set_rules('paymentReminderAmount','text','required');
        if($this->form_validation->run() == true)
        {
            $getEmailPackage = $this->Customermodel->checkVendorEmailPackage();
            if($getEmailPackage > 0)
            {
                // send email to admin user
                $url = 'https://api.sendgrid.com/';
                $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                $json_string = array(
                'to' => array(
                    $this->input->post('paymentReminderEmail')
                ),
                'category' => 'test_category'
                );
                $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                  <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                  <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                    <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                      <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                          <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                            <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                          <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                            <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                              <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                  <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Your payment <strong>'.$this->input->post('paymentReminderAmount').'</strong> is pending against houdin-e</td></tr>
                                  
                                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                      <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">
                                      Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
                <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                <table width="100%" ><tr >
                <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
                <tr><td style="">
                '.$setSocialString.'</td>
                </tr></table>
                </div>
                </div></td></tr></table>';
                $params = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'x-smtpapi' => json_encode($json_string),
                    'to'        => $this->input->post('paymentReminderEmail'),
                    'fromname'  => $getInfo['name'],
                    'subject'   => 'Payment Reminder',
                    'html'      => $htm,
                    'from'      => $getInfo['email'],
                );
                $request =  $url.'api/mail.send.json';
                $session = curl_init($request);
                curl_setopt ($session, CURLOPT_POST, true);
                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                curl_setopt($session, CURLOPT_HEADER, false);
                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($session);
                curl_close($session);
                $getResponse = json_decode($response);
                if($getResponse->message == 'success')
                {
                    $this->Customermodel->updateVendorEmailPackage($getEmailPackage);
                    $this->session->set_flashdata('success','Payment reminder sent successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
                else
                {
                    $this->Customermodel->updateVendorEmaillog();
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
            }
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            redirect(base_url()."Customer", 'refresh');
        }
    }
    // Customer Group
    public function customergroup()
    {
        if($this->session->userdata('vendorAuth') == "")
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        if($this->input->post('addCustomerGroupBtn'))
        {
            $this->form_validation->set_rules('customerGroupName','text','trim|required');
            $this->form_validation->set_rules('customerGroupStatus','select option','trim|required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setInsertArray = array('name'=>$this->input->post('customerGroupName'),'status'=>$this->input->post('customerGroupStatus'));
                $getGroupInsertStatus = $this->Customermodel->addCustomerGroup($setInsertArray);
                if($getGroupInsertStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Customer group addedd successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/customergroup", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/customergroup", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer/customergroup", 'refresh');
            }
        }
        // delete customer group
        if($this->input->post('deleteCustomerGroup'))
        {
            $this->form_validation->set_rules('deleteGroupId','text','required|trim|integer');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->Customermodel->deleteCustomerGroup($this->input->post('deleteGroupId'));
                if($getDeleteStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Customer group deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/customergroup", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/customergroup", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Customer/customergroup", 'refresh');
            }
        }
        // update customer group
        if($this->input->post('editCustomerGroupBtn'))
        {
            $this->form_validation->set_rules('editCustomerGroupName','text','trim|required');
            $this->form_validation->set_rules('editCustomerGroupStatus','select option','trim|required|in_list[active,deactive]');
            $this->form_validation->set_rules('editGroupId','text','trim|required');
            if($this->form_validation->run() == true)
            {
                $setUpdateArray = array('name'=>$this->input->post('editCustomerGroupName'),'status'=>$this->input->post('editCustomerGroupStatus'),'id'=>$this->input->post('editGroupId'));
                $getCustomerGroupUpdate = $this->Customermodel->updateCustomerGroupData($setUpdateArray);
                if($getCustomerGroupUpdate['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Customer group updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/customergroup", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/customergroup", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer/customergroup", 'refresh');
            }
        }
        // Custom Pagination
        $getTotalCount = $this->Customermodel->getCustomerGroupTotalCount();
        $config['base_url']    = base_url().'Customer/customergroup';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getCustomerGroups = $this->Customermodel->fetchCustomerGroupsData($setArray);
        $this->load->view('customergroup',$getCustomerGroups);
    }
    public function viewcustomergroup()
    {
        if($this->session->userdata('vendorAuth') == "")
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        $getGroupId = $this->uri->segment('3');
        if($getGroupId == "")
        {
            $this->load->helper('url');
            redirect(base_url()."Customer/customergroup", 'refresh');
        }
        // add customer to group
        if($this->input->post('addCustomerGroup'))
        {
            $getCosuntArray = $this->input->post('customerIddata');
            if(count($getCosuntArray) > 0)
            {
                $getInsertStatus = $this->Customermodel->addCustomerGroupData($getCosuntArray);
                if($getInsertStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Customer addedd successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/viewcustomergroup/".$this->uri->segment('3')."", 'refresh');

                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/viewcustomergroup/".$this->uri->segment('3')."", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer/viewcustomergroup/".$this->uri->segment('3')."", 'refresh');
            }
        }
        // delete customer data
        if($this->input->post('deleteGroupCustomerData'))
        {
            $this->form_validation->set_rules('deleteCustomerGroupId','text','required');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->Customermodel->deleteGroupCustomerData($this->input->post('deleteCustomerGroupId'));
                if($getDeleteStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Customer deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/viewcustomergroup/".$this->uri->segment('3')."", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/viewcustomergroup/".$this->uri->segment('3')."", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Customer/viewcustomergroup/".$this->uri->segment('3')."", 'refresh');
            }
        }
        // Custom Pagination
        $getTotalCount = $this->Customermodel->getCustomerGroupUserTotalCount();
        $config['base_url']    = base_url().'Customer/viewcustomergroup';
        $config['uri_segment'] = 4;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(4);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getcustomergroupDataValue = $this->Customermodel->fetchCustomerGroupData($setArray);
        $this->load->view('viewcustomergroup',$getcustomergroupDataValue);
    }
    // send email 
    public function sendEmail()
    {
        $getSmsEmail = smsemailcredentials();
        $getInfo = fetchvendorinfo();
        
        if($this->input->post('sendCustomerEmail'))
        {
            $this->form_validation->set_rules('customerEmail','text','required');
            $this->form_validation->set_rules('emailSubject','text','required');
            $this->form_validation->set_rules('emailBody','text','required');
            if($this->form_validation->run() == true)
            {
                $getExpldoeData = explode(',',$this->input->post('customerEmail'));
                $totalMail = count($getExpldoeData);
                $remaining_mail = $this->Customermodel->checkEmailRemaining();
                
                if(($remaining_mail->houdinv_emailsms_stats_remaining_credits) >= $totalMail)
                {
                $setSuccessArray = array();
                for($index = 0; $index< count($getExpldoeData); $index++)
                {
                    // send email to admin user
                    $url = 'https://api.sendgrid.com/';
                    $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                    $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                    $json_string = array(
                    'to' => array(
                        $getExpldoeData[$index]
                    ),
                    'category' => 'test_category'
                    );
                    $htm = $this->input->post('emailBody');
                    $params = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'x-smtpapi' => json_encode($json_string),
                        'to'        => $getExpldoeData[$index],
                        'fromname'  => $getInfo['name'],
                        'subject'   => $this->input->post('emailSubject'),
                        'html'      => $htm,
                        'from'      => $getInfo['email'],
                    );
                    $request =  $url.'api/mail.send.json';
                    $session = curl_init($request);
                    curl_setopt ($session, CURLOPT_POST, true);
                    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                    curl_setopt($session, CURLOPT_HEADER, false);
                    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($session);
                    curl_close($session);
                    $getResponse = json_decode($response);
                    if($getResponse->message != 'success')
                    {
                        array_push($setSuccessArray,'yes');
                        
                        
                    }
                    else
                    {
                        $this->Customermodel->emailDeduct();
                    }
                }
                if(!count($setSuccessArray) > 0)
                {
                    
                    $this->session->set_flashdata('success','Email send successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Some emails are not send.');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh');
                }
                }
                else
                {
                     $this->session->set_flashdata('error','You do not have enough remaining email creadit.');
                    $this->load->helper('url');
                    redirect(base_url()."Customer", 'refresh'); 
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer", 'refresh');
            }
        }
    }
    public function customerDetailPdf()
    {
        // $get customer details data
        $getLogo = fetchvedorsitelogo();
        $url = base_url();
        $getCustomerData = $this->Customermodel->getCustomerPdfData();
        $getTotalCustomer = $getCustomerData['totalCustomer'];
        $getTotalActiveCustomer = $getCustomerData['totalActiveCustomer'];
        $getTotalDeactiveCustomer = $getCustomerData['totalDeactiveCustomer'];
        // set customer list
        $setHtmlList = "";
        foreach($getCustomerData['custoemrList'] as $custoemrListData)
        {
            if($setHtmlList)
            {
                $setHtmlList = $setHtmlList.'<tr>
                <td style="font-size:12px;">'.$custoemrListData->houdinv_user_name.'</td>
                <td style="font-size:12px;">'.$custoemrListData->houdinv_user_email.'</td>
                <td style="font-size:12px;">'.$custoemrListData->houdinv_user_contact.'</td>
                <td style="font-size:12px;">'.$custoemrListData->houdinv_users_gender.'</td>
                <td style="font-size:12px;">'.$custoemrListData->houdinv_user_is_active.'</td>
                </tr>';
            }
            else
            {
                $setHtmlList = '<tr>
                <td style="font-size:12px;">'.$custoemrListData->houdinv_user_name.'</td>
                <td style="font-size:12px;">'.$custoemrListData->houdinv_user_email.'</td>
                <td style="font-size:12px;">'.$custoemrListData->houdinv_user_contact.'</td>
                <td style="font-size:12px;">'.$custoemrListData->houdinv_users_gender.'</td>
                <td style="font-size:12px;">'.$custoemrListData->houdinv_user_is_active.'</td>
                </tr>';
            }
        }
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Customer List');
    $pdf->SetSubject('Houdin-e - Customer List');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = <<<EOD



<table style="width:100%;">
<tbody>
<tr>

<td>
<img style="display:block; margin:auto; width:150px; " src="$getLogo" alt="Logo"></td>



<td style="font-size:15px;text-align:right"><h3>Customer List</h3></td>

</tr>
</tbody>
</table>

<table>
<tr><td>&nbsp;</td></tr>
</table>

<table style="width:100%;" cellpadding="8" border="1">
<tr>
  <td style="text-align:center;">
  <img src=$url"assets/images/Total_Products.png" width="35px;"><br/>
  <span style="font-size:12px;font-weight:bold;">$getTotalCustomer</span><br/>
  <span style="font-size:12px;font-weight:bold; color:#008cb6">Total Customer</span>

   </td>
   <td style="text-align:center;">
   <img src=$url"assets/images/Total_Products.png" width="35px;"><br/>
   <span style="font-size:12px;font-weight:bold;">$getTotalActiveCustomer</span><br/>
   <span style="font-size:12px; font-weight:bold; color:#008cb6">Active Customer</span>

    </td>
    <td style="text-align:center;">
    <img src=$url"assets/images/Total_Products.png" width="35px;"><br/>
    <span style="font-size:12px;font-weight:bold;">$getTotalDeactiveCustomer</span><br/>
    <span style="font-size:12px; font-weight:bold; color:#008cb6">Deactive Customer</span>

     </td>
     </tr>

   </table>


   <table>
   <tr><td>&nbsp;</td></tr>
   </table>
  <table style="width:100%;" cellpadding="10" border="1">
<tr>
<td style="font-size:12px;font-weight:bold;">Name</td>
<td style="font-size:12px;font-weight:bold;">Email</td>
<td style="font-size:12px;font-weight:bold;">Contact</td>
<td style="font-size:12px;font-weight:bold;">Gender</td>
<td style="font-size:12px;font-weight:bold;">Status</td>
</tr>
$setHtmlList
</table>
   <table>
   <tr><td>&nbsp;</td></tr>
   </table>

EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_customer_list.pdf', 'I');
    }
    public function customerpaymentPending()
    {
        
        
         $getCustomerData = $this->Customermodel->fetchPendingCustomerRecord();
         $getLogodata = fetchvedorsitelogo();
        $url = base_url();

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Payment Pending Detail');
    $pdf->SetSubject('Houdin-e - Payment Pending Detail');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = '<table style="width:100%;">
<tr style="text-align:center;margin-bottom:20px"><td><img style="display:block; margin:auto; width:150px; " src="'.$getLogodata.'" alt="Logo"></td></td>
<td style="float:right;text-align:right"><p>Payment pending</p></td></tr>
</table>
<br/><br/>

 
<table style="width:100%;" cellpadding="10" cellspacing="0" border="">
<tr>
  <td width="49%" style="background-color:#FFE4E1">
   <table width="100%">
    <tr>
      <td style="width:20%" rowspan="3"><img src="'.$url.'assets/images/Total_Products.png" width="50px;"></td>
       <td style="padding-top:20px!important;margin-top:20px!important">'.$getCustomerData['totalCustomer'].'</td>
     </tr>
     <tr>
       
       <td style="width:80%;font-size:14px;color:gray">Total Payment</td>
     </tr>
    
   </table>
  </td>
  <td width="2%"></td>
  <td width="49%" style="background-color:#FFE4E1">
   <table width="100%">
      <tr>
       <td style="width:20%" rowspan="2"><img src="'.$url.'assets/images/Out_of_Stock.png" width="50px";margin:auto;></td>
       <td style="margin-top:20px;">'.$getCustomerData['totalPendingCustomer'].'</td>
     </tr>
     <tr> 
      <td style="width:80%;font-size:14px;color:gray;">Payment pending</td>
     </tr>
    
   </table>
  </td>
  
  </tr>

</table>  
<br>
<hr>


<table style="width:100%;" cellpadding="10" border="1">
<tr>
<th>Customer</th>
                    <th>Customer contact</th>
                    <th>Pending amount</th>
</tr>';
foreach($getCustomerData['customerList'] as $customerListData)
                                {
$html.='<tr>
<td>'.$customerListData->houdinv_user_name.'</td>
               <td>'.$customerListData->houdinv_user_contact.'</td>
                  <td>'.$customerListData->houdinv_users_pending_amount.'</td>
</tr>';
}
$html.='</table>
<br/>
<br/>';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_customer_payment_pending_list.pdf', 'I');
    }
    
    
    
    public function customerprivileged()
    {

        $getLogo = fetchvedorsitelogo();
        $url = base_url();


         $getCustomerData = $this->Customermodel->fetchPrivilageCustomerRecord();
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Customer Detail');
    $pdf->SetSubject('Houdin-e - Customer Detail');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = '<table style="width:100%;">
<tr style="text-align:center;margin-bottom:20px"><td><img src="'.$getLogo.'" width="150px"></td>
<td style="float:right;text-align:right"><p>Privileged customer</p></td>
</tr>

</table>
<br/>
<br/>

 
<table style="width:100%;" cellpadding="10" cellspacing="0" border="1">
<tr>
  <td width="50%">
   <table width="100%">
    <tr>
      <td style="width:20%" rowspan="3"><img src="'.$url.'assets/images/Total_Products.png" width="50px;"></td>
       <td style="padding-top:20px!important;margin-top:20px!important">'.$getCustomerData['totalCustomer'].'</td>
     </tr>
     <tr>
       
       <td style="width:80%;font-size:14px;color:gray">Total customer</td>
     </tr>
    
   </table>
  </td>
 
  <td width="50%">
   <table width="100%">
      <tr>
       <td style="width:20%" rowspan="2"><img src="'.$url.'assets/images/Out_of_Stock.png" width="50px";margin:auto;></td>
       <td style="margin-top:20px;">'.$getCustomerData['totalPrivilageCustomer'].'</td>
     </tr>
     <tr> 
      <td style="width:80%;font-size:14px;color:gray;">Total privileged customer</td>
     </tr>
    
   </table>
  </td>
  
  </tr>

</table>  
<br>
<hr>


<table style="width:100%;" cellpadding="10" border="1">
<tr>
<td style="font-size:12px;font-weight:bold;">Name</td>
<td style="font-size:12px;font-weight:bold;">Contact</td>
<td style="font-size:12px;font-weight:bold;">email</td>
<td style="font-size:12px;font-weight:bold;">Privilage Amount</td>
</tr>';
foreach($getCustomerData['customerList'] as $customerListData)
                                {
$html.='<tr>

<td style="font-size:12px;">'.$customerListData->houdinv_user_name.'</td>
<td style="font-size:12px;">'.$customerListData->houdinv_user_contact.'</td>
<td style="font-size:12px;">'.$customerListData->houdinv_user_email.'</td>
<td style="font-size:12px;">'.$customerListData->houdinv_users_privilage.'</td>


</tr>';
}
$html.='</table>
<br/>
<br/>';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_customer_list.pdf', 'I');
    }
    
    
    
    public function pendingAmountNotify()
    {
        
             if($this->input->post('pendingAmountNotifyname'))
        {
            $this->form_validation->set_rules('hidden_email','text','required');
            $this->form_validation->set_rules('notify','notify text','required');
            if($this->form_validation->run() == true)
            {
                $getEmailPackage = $this->Customermodel->checkVendorEmailPackage();
                if($getEmailPackage > 0)
                {
                    $getSmsEmail = smsemailcredentials();
                    $getLogo = fetchvedorsitelogo();
                    $getInfo = fetchvendorinfo();
                    $getSocial = fetchsocialurl(); 
                    if(count($getSocial) > 0)
                    {
                        $setSocialString = "";
                        if($getSocial[0]->facebook_url)
                        {
                            $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                        }
                        if($getSocial[0]->twitter_url)
                        {
                            $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                        }
                        if($getSocial[0]->youtube_url)
                        {
                            $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                        }
                    }
                    $email = $this->input->post('hidden_email');
                    $url = 'https://api.sendgrid.com/';
                        $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                        $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                        $json_string = array(
                        'to' => array(
                            $email
                        ),
                        'category' => 'test_category'
                        );
                        
                        
                $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                    <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                    <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                    <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                    <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Hello '.$this->input->post('name').'</strong></td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->input->post('notify').'</td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                    <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                    <table width="100%" ><tr >
                    <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e 2018--All right reserverd </td></tr>
                    <tr><td style="">
                    '.$setSocialString.'</td>
                    </tr></table>
                    </div>
                    </div></td></tr></table>';
                    
                        $params = array(
                            'api_user'  => $user,
                            'api_key'   => $pass,
                            'x-smtpapi' => json_encode($json_string),
                            'to'        => $email,
                            'fromname'  => $getInfo['name'],
                            'subject'   => 'Your amount is pending',
                            'html'      => $htm,
                            'from'      => $getInfo['email'],
                        );
                        $request =  $url.'api/mail.send.json';
                        $session = curl_init($request);
                        curl_setopt ($session, CURLOPT_POST, true);
                        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                        curl_setopt($session, CURLOPT_HEADER, false);
                        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($session);
                        curl_close($session);
                        $getResponse = json_decode($response);
                    if($getResponse->message == 'success')
                    {
                        updateSuceessEmailCount($getEmailPackage-1);
                        $this->session->set_flashdata('success','Mail sent to customer');
                        $this->load->helper('url');
                        redirect(base_url()."Customer/paymentPending", 'refresh');
                    }
                    else
                    {
                        updateErrorEmailLog();
                        $this->session->set_flashdata('error','Something went wrong. Please try again');
                        $this->load->helper('url');
                        redirect(base_url()."Customer/paymentPending", 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','Please upgrade your email package');
                    $this->load->helper('url');
                    redirect(base_url()."Customer/paymentPending", 'refresh');
                }
                
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Customer/paymentPending", 'refresh');
            }
        }   
        
    }
    
    
  

    
}
