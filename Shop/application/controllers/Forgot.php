<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Forgot extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopuserforgot');
        if($this->session->userdata('userAuth') != "")
        {
            redirect(base_url()."home/", 'refresh');
        }
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        if($this->session->userdata('tempToken') == "")
        {
            $this->session->set_flashdata('error','Your token is expired');
            redirect(base_url()."Register", 'refresh');
        }
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/userforgot');
    }
    public function updatePass()
    {
        $this->form_validation->set_rules('forgotPin','Pin','required|exact_length[4]');
        $this->form_validation->set_rules('forgotPass','Password','required');
        $this->form_validation->set_rules('forgotConPass','Confirm Password','required');
        if($this->form_validation->run() == true)
        {
            $getPass = $this->input->post('forgotPass');
            $getConfirmPass = $this->input->post('forgotConPass');
            if($getPass == $getConfirmPass)
            {
                $getUserPin = $this->input->post('forgotPin');
                if($this->session->userdata('tempToken') == $getUserPin)
                {
                    $setPassArray = array('pin'=>$getUserPin,'password'=>$getPass);
                    $getUpdateStatus = $this->Shopuserforgot->updateUserPassword($setPassArray);
                    if($getUpdateStatus['message'] == 'pin')
                    {
                        $this->session->set_flashdata('error','Please enter correct pin');
                        redirect(base_url()."Forgot", 'refresh');    
                    }
                    else if($getUpdateStatus['message'] == 'yes')
                    {
                        $this->session->set_flashdata('success','Password updated successfully');
                        redirect(base_url()."Register", 'refresh');    
                    }
                    else if($getUpdateStatus['message'] == 'no')
                    {
                        $this->session->set_flashdata('error','Something went wrong. Please try again');
                        redirect(base_url()."Forgot", 'refresh');    
                    }
                    else
                    {
                        $this->session->set_flashdata('error','Something went wrong. Please try again');
                        redirect(base_url()."Forgot", 'refresh');    
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','Please enter correct pin');
                    redirect(base_url()."Forgot", 'refresh');    
                }
            }   
            else
            {
                $this->session->set_flashdata('error','Passwords are not matched');
                redirect(base_url()."Forgot", 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."Forgot", 'refresh');
        }
    }
}
