<?php  $this->load->view('Template/header');?> 

<header class="header bg-gray header-page-top">
  <div class="header-page-top-inner">
	  <div class="container">
		<h1 class="display-4">Blog</h1> 
  </div>
	</div>
</header>

   <section class="section bg-gray p-0">
        <div class="container">
          <div class="row">
            <div class="col-md-10 col-xl-9 mx-auto">


              <div class="card hover-shadow-7 my-8">
                <div class="row">
                  <div class="col-md-4">
                    <a href="blog-single.html"><img class="fit-cover position-absolute h-100" src="<?php echo base_url(); ?>assets/img/blog/1.jpg" alt="..."></a>
                  </div>

                  <div class="col-md-8">
                    <div class="p-7">
                      <h4>We relocated our office to a new designed garage</h4>
                      <p>Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title.</p>
                      <a class="small ls-1" href="<?php echo base_url(); ?>home/blog_detail">Read More <span class="pl-1">&xrarr;</span></a>
                    </div>
                  </div>
                </div>
              </div>


              <div class="card hover-shadow-7 my-8">
                <div class="row">
                  <div class="col-md-4">
                    <a href="blog-single.html"><img class="fit-cover position-absolute h-100" src="<?php echo base_url(); ?>assets/img/blog/2.jpg" alt="..."></a>
                  </div>

                  <div class="col-md-8">
                    <div class="p-7">
                      <h4>Top 5 brilliant content marketing strategies</h4>
                      <p>Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title.</p>
                      <a class="small ls-1" href="<?php echo base_url(); ?>home/blog_detail">Read More <span class="pl-1">&xrarr;</span></a>
                    </div>
                  </div>
                </div>
              </div>


              <div class="card hover-shadow-7 my-8">
                <div class="row">
                  <div class="col-md-4">
                    <a href="blog-single.html"><img class="fit-cover position-absolute h-100" src="<?php echo base_url(); ?>assets/img/blog/3.jpg" alt="..."></a>
                  </div>

                  <div class="col-md-8">
                    <div class="p-7">
                      <h4>Best practices for minimalist design with example</h4>
                      <p>Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title.</p>
                      <a class="small ls-1" href="<?php echo base_url(); ?>home/blog_detail">Read More <span class="pl-1">&xrarr;</span></a>
                    </div>
                  </div>
                </div>
              </div>


              <div class="card hover-shadow-7 my-8">
                <div class="row">
                  <div class="col-md-4">
                    <a href=""><img class="fit-cover position-absolute h-100" src="<?php echo base_url(); ?>assets/img/blog/4.jpg" alt="..."></a>
                  </div>

                  <div class="col-md-8">
                    <div class="p-7">
                      <h4>Congratulate and thank to Maryam for joining our team</h4>
                      <p>Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title.</p>
                      <a class="small ls-1" href="<?php echo base_url(); ?>home/blog_detail">Read More <span class="pl-1">&xrarr;</span></a>
                    </div>
                  </div>
                </div>
              </div>


              <div class="card hover-shadow-7 my-8">
                <div class="row">
                  <div class="col-md-4">
                    <a href="blog-single.html"><img class="fit-cover position-absolute h-100" src="<?php echo base_url(); ?>assets/img/blog/5.jpg" alt="..."></a>
                  </div>

                  <div class="col-md-8">
                    <div class="p-7">
                      <h4>New published books to read by a product designer</h4>
                      <p>Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title.</p>
                      <a class="small ls-1" href="<?php echo base_url(); ?>home/blog_detail">Read More <span class="pl-1">&xrarr;</span></a>
                    </div>
                  </div>
                </div>
              </div>


              <div class="card hover-shadow-7 my-8">
                <div class="row">
                  <div class="col-md-4">
                    <a href="blog-single.html"><img class="fit-cover position-absolute h-100" src="<?php echo base_url(); ?>assets/img/blog/6.jpg" alt="..."></a>
                  </div>

                  <div class="col-md-8">
                    <div class="p-7">
                      <h4>This is why it's time to ditch dress codes at work</h4>
                      <p>Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title.</p>
                      <a class="small ls-1" href="<?php echo base_url(); ?>home/blog_detail">Read More <span class="pl-1">&xrarr;</span></a>
                    </div>
                  </div>
                </div>
              </div>



             
            </div>
          </div>
        </div>
      </section>

  	

<?php  $this->load->view('Template/footer');?>