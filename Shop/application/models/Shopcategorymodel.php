<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopcategorymodel extends CI_Model{
    function __construct() 
    {
        
    }
    public function fetchCategoryData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);
        // get product category data
        $getProductCategory = $getDynamicDB->select('*')->from('houdinv_categories')->where('houdinv_category_status','active')->get()->result();
        if(count($getProductCategory) > 0)
        {
            // set main array data
            // $mainArraydata = array();
            $main_array =array();
            $sub_array =array();
            foreach($getProductCategory as $getProductCategoryList)
            {
                $getProductCategoryId = $getProductCategoryList->houdinv_category_id;
                // set product category array
                if($getProductCategoryList->houdinv_category_thumb)
                {
                    $setCategoryImage = $this->session->userdata('vendorURL')."images/category/".$getProductCategoryList->houdinv_category_thumb;
                }
                else
                {
                    $getShop = shopurl();
                    $setCategoryImage = "".$getShop."Extra/noimage.jpg";
                }
                $setProductCategoryArray = array('categoryId'=>$getProductCategoryList->houdinv_category_id,'categoyrname'=>$getProductCategoryList->houdinv_category_name,'categoryImage'=>$setCategoryImage);
                // get product categort at level 0
                $getSubCategoryData = $getDynamicDB->select('*')->from('houdinv_sub_categories_one')->where('houdinv_sub_category_one_main_id',$getProductCategoryId)->get()->result();
                if(count($getSubCategoryData) > 0)
                {
                    $setSubCategoryData = array();
                    $sub_array =array();
                    foreach($getSubCategoryData as $getSubCategoryDataList)
                    {
                        $setSubCategoryArray = array('subcategoryId'=>$getSubCategoryDataList->houdinv_sub_category_one_id,'subcategoyrName'=>$getSubCategoryDataList->houdinv_sub_category_one_name);
                        array_push($setSubCategoryData,$setSubCategoryArray);
                        //  get product category ar level 1
                        $getSubSubCategoryData = $getDynamicDB->select('*')->from('houdinv_sub_categories_two')->where('houdinv_sub_category_two_sub1_id',$getSubCategoryData[0]->houdinv_sub_category_one_id)->get()->result();
                        // if(count($getSubSubCategoryData) > 0)
                        // {
                            $setSubSubCategoryData = array();
                            foreach($getSubSubCategoryData as $getSubSubCategoryDataList)
                            {
                                $setSubSubCategoryArray = array('subSubcategoryId'=>$getSubSubCategoryDataList->houdinv_sub_category_two_id,'subSubcategoryName'=>$getSubSubCategoryDataList->houdinv_sub_category_two_name);
                                array_push($setSubSubCategoryData,$setSubSubCategoryArray);    
                            }
                        // }
                        // else
                        // {
                        //     $sub_array =array("main"=>$setSubCategoryArray,"sub"=>);
                        // }

                        $sub_array[] =array("main"=>$setSubCategoryArray,"sub"=>$setSubSubCategoryData);
                    }

                    
                        // $categoryData['category'][] = $setProductCategoryArray;
                        // $categoryData['category']['subcategory'][] = $setSubCategoryData;    
                        // $categoryData['category']['subcategory']['subSubcategory'][] = $setSubSubCategoryData;
                }
                else
                {
                    $categoryData['category'][] = $setProductCategoryArray;
                }
                $main_array[] = array("main"=>$setProductCategoryArray,"subcat"=>$sub_array);
            }
            // array_push($mainArraydata,$main_array);
            return array('message'=>'Success','data'=>$main_array);
        }
        else
        {
            return array('message'=>'No data');
        }
    }
} 

?>