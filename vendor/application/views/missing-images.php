<?php 
$this->load->view('Template/header.php');
$this->load->view('Template/sidebar.php');
?>
<div class="content-page">
<!-- Start content -->
<div class="content">
    <div class="container">
<!--START Page-Title -->
        <div class="row">
            <div class="col-md-8">
                <h4 class="page-title">Product</h4>
                <ol class="breadcrumb">
                <!-- <li><a href="<?php echo base_url(); ?>">Product/productLandingpage</a></li> -->

                <li class="active">Missing Images</li>
                </ol>
                </div>
        </div>
         
        <!--END Page-Title -->
        <div class="row">
          <div class="col-md-12">
          <?php 
           if ($this->session->flashdata('message_name')) {
                echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            }
          if ($this->session->flashdata('success')) {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
          ?>
            <div class="card-box table-responsive">
            <?php echo form_open(base_url().'Product/addMissingImages',array("id"=>"add-category-form","enctype"=>"multipart/form-data")); ?>
            <div class="pull-right">
            <input type="submit" class="btn btn-info " name="add_image" value="Add Images">
            </div>
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th style="width: 15%">Item</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                foreach ($all as $product) {
                    $image_data = $product['houdinv_products_main_images'];
                    $image_data = json_decode($image_data);
                    if (!$image_data[0])  {
                    ?>

                    <tr>
                    <td>
                    <?php 
                    if ($image_data[0])  {
                    ?>
                    <img class="missing-category-image" src="<?php echo base_url(); ?>upload/productImage/<?php echo $image[0];  ?>"><br/><?php echo $product['houdin_products_title'] ?>
                    <?php } else {
                      ?>
                      <img class="missing-category-image" src="<?php echo base_url(); ?>images/no.png"/><br/><?php echo $product['houdin_products_title'] ?>
                     <?php }
                     ?>
                     </td>
                     <td class="add_category_row">
                     <div class="images_preview_row">
                        <div class="col-md-8">
                        <div class="form-group">
                        <!-- first_image -->
                        <input type="hidden" name="product_id[]" value="<?php echo $product['houdin_products_id']?>"/>
                        <input type="file" class="form-control images_class first_image"  name="images[]" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />
                        </div>
                    </div>
                        <div class="col-md-4" >
                        <img  class="thunbnail_image images_preview" style="height:128px; width:128px" src="<?php echo base_url() ?>assets/images/users/avatar-1.jpg"/>
                        </div>
                        </div>
                     </td>
                     </tr>
                <?php } }
                ?>
                </tbody>
                <tbody>
                </tbody>
              </table>

              <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom/validation-js/add-product/add-product-validation.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom/page-js/add-product/add-product.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom/page-js/product-category/add-category.js"></script>
