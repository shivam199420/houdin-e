        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/storesetting') ?>

        <style type="text/css">
        @media screen and (min-width: 768px){}
        .dropdown.dropdown-lg .dropdown-menu {
        min-width: 390px!important;
        }
        .checkbox{
        padding-left: 0px;
        }
        .checkbox, .radio {
        position: relative;
        display: block;
        margin-top: 0px;
        margin-bottom: 10px;
        }
        }
        </style>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">

        <!--START Page-Title -->
        <div class="row">
     
        <div class="col-md-8">
        <h4 class="page-title">Store Policies</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>storesetting/Storesettinglanding">Store setting</a></li>
        <li class="active">Store Policies</li>
        </ol>
        </div>

        </div>
        <!--END Page-Title --> 
        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>

        <div class="row m-t-20">
        <div class="col-sm-12 col-md-12">
        <div class="card-box" style="padding: 10px 20px;">

        <div class="row">


        <?php echo form_open(base_url('Storesetting/storepolicies_frm'), array( 'id' => 'storepolicies_frm', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
        <div class="row">
        <div class="row">  
        <div class="col-lg-12"> 
        <div class="panel-group" id="accordion-test-2" style="padding:10px">
        <h5>Specify details about your business that makes you unique.</h5>
        <p>Click below to edit content live</p> 
        <!-- About us section -->
        <div class="panel panel-default"> 
        <div class="panel-heading"> 
        <h4 class="panel-title"> 
        <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseOne-2" aria-expanded="false" class="collapsed">
        About Your Shop
        </a> 
        </h4> 
        </div> 
        <?php  

        $Policy_datas=$Policy_data[0];
        ?>
        <div id="collapseOne-2" class="panel-collapse collapse in"> 
        <div class="panel-body">
        <textarea  class="summernote" name="about"><?=$Policy_datas->about?></textarea>

        </div> 
        </div> 
        </div>
        <!-- Faq Data -->
        <div class="panel panel-default"> 
        <div class="panel-heading"> 
        <h4 class="panel-title"> 
        <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseOne-faq" aria-expanded="false" class="collapsed">
        FAQ
        </a> 
        </h4> 
        </div> 
        <?php  

        
        ?>
        <div id="collapseOne-faq" class="panel-collapse collapse"> 
        <div class="panel-body">
        <textarea  class="summernote" name="FAQ"><?=$Policy_datas->faq?></textarea>

        </div> 
        </div> 
        </div>

        <div class="panel panel-default"> 
        <div class="panel-heading"> 
        <h4 class="panel-title"> 
        <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseOne-Privacy_Policy" aria-expanded="false" class="collapsed">
        Privacy Policy
        </a> 
        </h4> 
        </div> 
        <?php  

        
        ?>
        <div id="collapseOne-Privacy_Policy" class="panel-collapse collapse"> 
        <div class="panel-body">
        <textarea  class="summernote" name="Privacy_Policy"><?=$Policy_datas->Privacy_Policy?></textarea>

        </div> 
        </div> 
        </div>
        <div class="panel panel-default"> 
        <div class="panel-heading"> 
        <h4 class="panel-title"> 
        <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseTwo-2" class="collapsed" aria-expanded="false">
        Terms and Conditions
        </a> 
        </h4> 
        </div> 
        <div id="collapseTwo-2" class="panel-collapse collapse "> 
        <div class="panel-body">
        <textarea  class="summernote" name="Terms_Conditions"><?=$Policy_datas->Terms_Conditions?></textarea>

        </div> 
        </div> 
        </div> 
        <div class="panel panel-default"> 
        <div class="panel-heading"> 
        <h4 class="panel-title"> 
        <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseThree-2" class="collapsed" aria-expanded="false">
        Cancellation and Refund Policy
        </a> 
        </h4> 
        </div> 
        <div id="collapseThree-2" class="panel-collapse collapse"> 
        <div class="panel-body">
        <textarea  class="summernote" name="Cancellation_Refund_Policy"><?=$Policy_datas->Cancellation_Refund_Policy?></textarea>
        </div> 
        </div> 
        </div> 
        <div class="panel panel-default"> 
        <div class="panel-heading"> 
        <h4 class="panel-title"> 
        <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseFour-2" class="collapsed" aria-expanded="false">
        Shipping and Delivery Policy
        </a>  
        </h4> 
        </div> 
        <div id="collapseFour-2" class="panel-collapse collapse"> 
        <div class="panel-body">
        <textarea  class="summernote" name="Shipping_Delivery_Policy"><?=$Policy_datas->Shipping_Delivery_Policy?></textarea>
        </div> 
        </div> 
        </div>
        <div class="panel panel-default"> 
        <div class="panel-heading"> 
        <h4 class="panel-title"> 
        <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseFive-2" class="collapsed" aria-expanded="false">
        Disclaimer Policy
        </a>  
        </h4> 
        </div> 
        <div id="collapseFive-2" class="panel-collapse collapse"> 
        <div class="panel-body">
        <textarea  class="summernote" name="Disclaimer_Policy"><?=$Policy_datas->Disclaimer_Policy?></textarea>
        </div> 
        </div> 
        </div>
        </div>
        <div class="m-l-10">
        <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
        </div> 
        </div>
        </div> <!-- end row -->
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        </div>
        </div>
        <?php $this->load->view('Template/footer.php') ?>

