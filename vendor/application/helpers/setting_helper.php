<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function getcompleteordertemapte()
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $getOrderTemplate = $getVendorDb->select('*')->from('houdinv_email_Template')->where('houdinv_email_template_type','On_order_complete')->get()->result();
    return $getOrderTemplate;
}
function fetchUsername($data)
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $getUserName = $getVendorDb->select('houdinv_user_name,houdinv_user_email')->from('houdinv_users')->where('houdinv_user_id',$data)->get()->result();
    return array('name'=>$getUserName[0]->houdinv_user_name,'email'=>$getUserName[0]->houdinv_user_email);
}
function fetchVendorDetails()
{
    $CI =& get_instance();
    $getMasterDb = $CI->load->database('master',true);
    $getVendorDetails = $getMasterDb->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id,houdin_users.*')->from('houdin_vendor_auth')->where('houdin_vendor_auth.houdin_vendor_auth_token',$CI->session->userdata('vendorAuth'))
                ->join('houdin_users','houdin_users.houdin_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id')->get()->result();
    return $getVendorDetails;
}
function fetchoutletDetailsData()
{
    $CI =& get_instance();
    if($CI->session->userdata('vendorOutlet') == 0)
    {
        $getMasterDb = $CI->load->database('master',true);
        $getVendorAuthData = $getMasterDb->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id')->from('houdin_vendor_auth')
        ->where('houdin_vendor_auth.houdin_vendor_auth_token',$CI->session->userdata('vendorAuth'))->get()->result();
        $getUserId = $getVendorAuthData[0]->houdin_vendor_auth_vendor_id;
    }
    else
    {
        $getDatabaseData = switchDynamicDatabase();
        $getVendorDb = $CI->load->database($getDatabaseData,true);
        
        $getStaffId = $getVendorDb->select('staff_id')->from('houdinv_staff_management')->where('houdinv_staff_auth_token',$CI->session->userdata('vendorAuth'))->get()->result();
        $getUserId = $getStaffId[0]->staff_id;
    }
    return array('userId'=>$getUserId);
}
function fetchPosSetting()
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    $getPosData = $getVendorDb->select('*')->from('houdinv_possetting')->where('id','1')->get()->result();
    return $getPosData;
}
function invoiceSetting()
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    
    $getInvoiceSetting = $getVendorDb->select('*')->from('houdinv_invoicesetting')->where('id','1')->get()->result();
    return $getInvoiceSetting;
}
function smsemailcredentials()
{
    $CI =& get_instance();
    $master = $CI->load->database('master',true);
    $getSendGrid = $master->select('*')->from('houdin_sendgrid')->where('houdin_sendgrid_id','1')->get()->result();
    $getTwilio = $master->select('*')->from('houdin_twilio')->where('houdin_twilio_id','1')->get()->result();
    return array('sendgrid'=>$getSendGrid,'twilio'=>$getTwilio);
}
function masterPyumoney()
{
    $CI =& get_instance();
    $master = $CI->load->database('master',true);
    $getMasterPayumoney = $master->select('*')->from('houdin_payumoney')->get()->result();
    return $getMasterPayumoney;
}
function getpostemanKey()
{
    $CI =& get_instance();
    
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    
    $getKey = $getVendorDb->select('houdinv_shipping_credentials_key')->from('houdinv_shipping_credentials')->where('houdinv_shipping_credentials_id','1')->get()->result();
    return $getKey;
}
function getOrderInfo($orderId)
{
    $CI =& get_instance();
    
    $getDatabaseData = switchDynamicDatabase();
    $getVendorDb = $CI->load->database($getDatabaseData,true);
    
    $getOrderData = $getVendorDb->select('*')->from('houdinv_orders')->where('houdinv_order_id',$orderId)->get()->result();
    // get user info
    $getUserData = $getVendorDb->select('*')->from('houdinv_users')->where('houdinv_user_id',$getOrderData[0]->houdinv_order_user_id)->get()->result();
    // get address infon
    $getAddressData = $getVendorDb->select('*')->from('houdinv_user_address')->where('houdinv_user_address_id',$getOrderData[0]->houdinv_order_delivery_address_id)->get()->result();
    // get product and variant info
    if($getOrderData[0]->houdinv_confirm_order_product_detail)
    {
        $getProductData = json_decode($getOrderData[0]->houdinv_confirm_order_product_detail,true);
    }
    else 
    {
        $getProductData = json_decode($getOrderData[0]->houdinv_order_product_detail,true);
    }
    $setPorductArray = array();
    foreach($getProductData as $getProductDataList)
    {
        if($getProductDataList['product_id'] != 0 && $getProductDataList['product_id'] != "")
        {
            $setPrice = $getProductDataList['product_Count']*$getProductDataList['total_product_paid_Amount'];
            $getProductData = $getVendorDb->select('*')->from('houdinv_products')->where('houdin_products_id',$getProductDataList['product_id'])->get()->result();
            $setWeight = json_decode($getProductData[0]->houdinv_products_main_shipping,true);
            $setPorductArray[] = array('count'=>$getProductDataList['product_Count'],'price'=>$setPrice,'name'=>$getProductData[0]->houdin_products_title,'SKU'=>$getProductData[0]->houdin_products_main_sku,'weight'=>$setWeight['weight']);
        }
        else
        {
            $setPrice = $getProductDataList['product_Count']*$getProductDataList['total_product_paid_Amount'];
            $getProductData = $getVendorDb->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getProductDataList['variant_id'])->get()->result();
            // get procut weight 
            $getProductWeight = $getVendorDb->select('houdinv_products_main_shipping')->from('houdinv_products')->where('houdin_products_id',$getProductData[0]->houdin_products_variants_product_id)->get()->result();
            $setWeight = json_decode($getProductWeight[0]->houdinv_products_main_shipping,true);
            $setPorductArray[] = array('count'=>$getProductDataList['product_Count'],'price'=>$setPrice,'name'=>$getProductData[0]->houdin_products_variants_title,'SKU'=>$getProductData[0]->houdin_products_variants_sku,'weight'=>$setWeight['weight']);
        }
    }
    return array('order'=>$getOrderData,'user'=>$getUserData,'address'=>$getAddressData,'productData'=>$setPorductArray);
}
function getVendorCurrency()
{
    $CI =& get_instance();
    $master = $CI->load->database('master',true);
    $getCurrency = $master->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id,houdin_users.houdin_users_currency')->from('houdin_vendor_auth')->where('houdin_vendor_auth.houdin_vendor_auth_token',$CI->session->userdata('vendorAuth'))
    ->join('houdin_users','houdin_users.houdin_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id')->get()->result();
    return $getCurrency;
}
function getVendorLanguage()
{
    $CI =& get_instance();
    $master = $CI->load->database('master',true);
    $getLanguage = $master->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id,houdin_user_shops.houdin_user_shops_language,houdin_languages.houdin_language_name')
    ->from('houdin_vendor_auth')
    ->where('houdin_vendor_auth.houdin_vendor_auth_token',$CI->session->userdata('vendorAuth'))
    ->join('houdin_user_shops','houdin_user_shops.houdin_user_shop_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id')
    ->join('houdin_languages','houdin_languages.houdin_language_id = houdin_user_shops.houdin_user_shops_language')->get()->result();
    return $getLanguage;
}
function updategetUserDevice($userid)
{   
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getDatatbase = $CI->load->database($getDatabaseData,true);
    // get user id
    $getUserData = $getDatatbase->select('houdinv_order_user_id')->from('houdinv_orders')->where('houdinv_order_id',$userid)->get()->result();
    $getDeviceId = $getDatatbase->select('houdinv_users_device_id')->from('houdinv_users')->where('houdinv_user_id',$getUserData[0]->houdinv_order_user_id)->get()->result();
    return $getDeviceId;
}
function SetDynamicUrl()
{
    $setUrl = "//3.22.189.222/houdin-e";
    return $setUrl;
}
function shopNameData()
{
    $CI =& get_instance();
    $masterDB = $CI->load->database('master',true);
    $getShop = $masterDB->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id,houdin_user_shops.domain_url')->from('houdin_vendor_auth')->where('houdin_vendor_auth.houdin_vendor_auth_token',$CI->session->userdata('vendorAuth'))
            ->join('houdin_user_shops','houdin_user_shops.houdin_user_shop_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id')->get()->result();
    return $getShop[0]->domain_url;
}
function getMainAccount($id)
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getDatatbase = $CI->load->database($getDatabaseData,true);
    $getId = $getDatatbase->select('houdinv_accounts_account_type_id')->from('houdinv_accounts')->where('houdinv_accounts_id',$id)->get()->result();
    return $getId[0]->houdinv_accounts_account_type_id;
}
function getloworderamount()
{
    $CI =& get_instance(); 
    $getDatabaseData = switchDynamicDatabase();
    $getDatatbase = $CI->load->database($getDatabaseData,true);
    $getReorderQuantity = $getDatatbase->select('inventorysetting_number')->from('houdinv_inventorysetting')->where('Universal_rl','1')->get()->result();
    if(count($getReorderQuantity) > 0)
    {
        return array('message'=>'yes','quantity'=>$getReorderQuantity[0]->inventorysetting_number);
    }
    else
    {
        return array('message'=>'yes','quantity'=>'1');
       // return array('message'=>'no');
    }
}
function fromemail()
{
    return "noreply@houdine.com";
}
function toemail()
{
    return "nimit@houdine.com";
}
function getvendorurl()
{
    return "http://3.22.189.222/houdin-e/vendor/";
}
function getmasterurl()
{
    return "http://3.22.189.222/houdin-e/master/";
}
function getTaxamount($data)
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getDatatbase = $CI->load->database($getDatabaseData,true);
    
    $getTaxId = $getDatatbase->select('houdinv_tax_percenatge')->from('houdinv_taxes')->where('houdinv_tax_id',$data)->get()->result();
    return $getTaxId[0]->houdinv_tax_percenatge;
}
/**
 * getVariantTotalstock()
 * @purpose: to fetch variant total stock 
 * @created by: shivam singh sengar
 * @created at: 30 Nov 2019 20:32
 * @param PRODUCT_ID $product_id int
 * @return int
 */
function getVariantTotalstock($id)
{
    $CI =& get_instance();
    $getDatabaseData = switchDynamicDatabase();
    $getDatatbase = $CI->load->database($getDatabaseData,true);
    $total_variant_stock = $getDatatbase->query('SELECT SUM(houdinv_products_variants_total_stocks) as stock FROM `houdinv_products_variants` WHERE houdin_products_variants_product_id ='.$id.'')->result();
    return $total_variant_stock[0]->stock;
}
