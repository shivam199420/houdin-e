<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header') ?>

<!-- Page item Area -->
<div class="ps-section--hero"><img src="images/hero/03.jpg" alt="">

  <div class="ps-section__content text-center">

    <h3 class="ps-section__title">Shipping & Delivery Policy</h3>

  </div>

</div>
<!-- About Page -->
<div class="about_page_area fix">
<div class="container">
<div class="row about_inner">
<?php echo $shipping[0]->Shipping_Delivery_Policy ?>

</div>
</div>
<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/footer') ?>
