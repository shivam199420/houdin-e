<!--  FOOTER START  -->
<?php 
$getDB = getDBName();
$con = mysqli_connect("localhost","root","houdine123",$getDB);
$getShopData = mysqli_query($con,"select houdinv_shop_business_name,houdinv_shop_address,houdinv_shop_communication_email,houdinv_shop_contact_info from houdinv_shop_detail where houdinv_shop_id=1");
$getShopValue = mysqli_fetch_array($getShopData);
// get footer social links
$getShopSocialLink = mysqli_query($con,'select * from houdinv_social_links');
$getSocialLinks = mysqli_fetch_array($getShopSocialLink);
// get footer url
$getUrl = mysqli_query($con,"select * from houdinv_navigation_store_pages");
$setFooterLinks = array();
while($getFooterUrl = mysqli_fetch_array($getUrl))
{
  array_push($setFooterLinks,$getFooterUrl['houdinv_navigation_store_pages_name']);
}
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;

?>
<div class="modal fade" id="add_Quotation_data" role="dialog" aria-hidden="true" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ask Quotation</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>

      </div>
      <form class="modal-body" id="add_Quotation_dataform" method="post" action="<?php echo base_url(); ?>Orders/quatationAdd">

        <div class="row">
          <div class="col-md-12">

            <div class="card-box" style="float:left;width: 100%;">
              <div class="form-group">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text"  class="form-control require_quote characterValidation required_validation_for_quotation name_validation" name="name" placeholder="name" />
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email"  class="form-control require_quote email_validation required_validation_for_quotation name_validation" name="email" placeholder="email" />
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label>Phone</label>
                    <input type="text" pattern="^[-+]?\d+$"  class="phone_telephone form-control require_quote required_validation_for_quotation name_validation" style="width:100%" name="phone" placeholder="phone" />
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label>For product count</label>
                    <input type="text"  class="form-control require_quote number_validation" required_validation_for_quotation name_validation name="count" placeholder="product count" />
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control require_quote required_validation_for_quotation name_validation" name="comment"></textarea>
                  </div>
                </div>


                <div class="col-md-12">
                  <div class="form-group">
                    <input type="hidden" value="" id="product_quete_id" name="product_id"/>
                    <input type="submit" style="background: #33d286;color: #fff;z-index: 99999999;top: 15px;left: 8px;padding: 3px 12px;border-radius: 2px;" class="form-control"  value="submit" name="quote_submit"  />
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>

      </form>




    </div>
  </div>
</div>
<footer class="footer_area">
  <div class="container">
    <div class="row">


      <div class="col-md-4 col-sm-6 text-center">
        <div class="single_ftr">
          <h4 class="sf_title">Information</h4>
          <ul>
            <?php
            if(in_array('about',$setFooterLinks))
            {
              echo '<li><a href="'.base_url().'About">About Us</a></li>';
            }
            if(in_array('terms',$setFooterLinks))
            {
              echo '<li><a href="'.base_url().'Terms">Terms & Conditions</a></li>';
            }
            if(in_array('privacy',$setFooterLinks))
            {
              echo '<li><a href="'.base_url().'Privacy">Privacy Policy</a></li>';
            }
            if(in_array('faq',$setFooterLinks))
            {
              echo '<li><a href="'.base_url().'Faq">FAQs</a></li>';
            }
            if(in_array('contact',$setFooterLinks))
            {
              echo '<li><a href="'.base_url().'Contact">Contact Us</a></li>';
            }
            ?>
          </ul>
        </div>
      </div> <!--  End Col -->

      <div class="col-md-4 col-sm-6 text-center">
        <div class="single_ftr">
          <h4 class="sf_title">Services</h4>
          <ul>
            <?php
            if(in_array('disclaimer',$setFooterLinks))
            {
              echo '<li><a href="'.base_url().'Disclaimer">Disclaimer</a></li>';
            }
            if(in_array('shipping',$setFooterLinks))
            {
              echo '<li><a href="'.base_url().'Shippingpolicy">Shipping & Delivery</a></li>';
            }
            if(in_array('refund',$setFooterLinks))
            {
              echo '<li><a href="'.base_url().'Refund">Cancellation & Refund</a></li>';
            }
            ?>
            <?php
            if($this->session->userdata('userAuth') == "")
            {
              echo '<li><a href="'.base_url().'register">Register </a></li>';
            }
            ?>

          </ul>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 text-center">
        <div class="single_ftr">
          <h4 class="sf_title">Contacts</h4>
          <ul>
            <li><?php if($getShopValue['houdinv_shop_address']) { echo $getShopValue['houdinv_shop_address']; } else { echo "--"; } ?></li>
            <li><?php if($getShopValue['houdinv_shop_contact_info']) { echo $getShopValue['houdinv_shop_contact_info']; } else { echo "--"; } ?></li>
            <li><?php if($getShopValue['houdinv_shop_communication_email']) { echo $getShopValue['houdinv_shop_communication_email']; } else { echo "--"; } ?></li>
          </ul>
        </div>
      </div> <!--  End Col -->
      <!--  End Col -->
      <!--  End Col -->

    </div>
  </div>


  <div class="ftr_btm_area">
    <div class="container">
      <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
          <p class="copyright_text text-center">&copy; <?php echo date('Y') ?> All Rights Reserved <?php if($getShopValue['houdinv_shop_business_name']) { echo $getShopValue['houdinv_shop_business_name']; } else  { echo "Houdin-e"; } ?> </p>
        </div>
        <div class="col-sm-4">
          <div class="text-center ">
            <?php
            if($getSocialLinks['facebook_url'] && $getSocialLinks['facebook_url'] != "")
            {
              ?>
              <a href="https://<?php echo $getSocialLinks['facebook_url'] ?>" target="_blank"><i class="fa fa-facebook-square my_font_icon fa_f_ML"></i></a>
            <?php }
            ?>
            <?php
            if($getSocialLinks['instagram_url'] && $getSocialLinks['instagram_url'] != "")
            {
              ?>
              <a href="https://<?php echo $getSocialLinks['instagram_url'] ?>" target="_blank"><i class="fa fa-instagram my_font_icon fa_f_ML"></i></a>
            <?php }
            ?>
            <?php
            if($getSocialLinks['google_url'] && $getSocialLinks['google_url'] != "")
            {
              ?>
              <a href="https://<?php echo $getSocialLinks['google_url'] ?>" target="_blank"><i class="fa fa-google-plus-square my_font_icon fa_f_ML"></i></a>
            <?php }
            ?>
            <?php
            if($getSocialLinks['youtube_url'] && $getSocialLinks['youtube_url'] != "")
            {
              ?>
              <a href="https://<?php echo $getSocialLinks['youtube_url'] ?>" target="_blank"><i class="fa fa-youtube-square my_font_icon fa_f_ML"></i></a>
            <?php }
            ?>
            <?php
            if($getSocialLinks['pinterest_url'] && $getSocialLinks['pinterest_url'] != "")
            {
              ?>
              <a href="https://<?php echo $getSocialLinks['pinterest_url'] ?>" target="_blank"><i class="fa fa-pinterest-square my_font_icon fa_f_ML"></i></a>
            <?php }
            ?>
            <?php
            if($getSocialLinks['linkedin_url'] && $getSocialLinks['linkedin_url'] != "")
            {
              ?>
              <a href="https://<?php echo $getSocialLinks['linkedin_url'] ?>" target="_blank"><i class="fa fa-linkedin my_font_icon fa_f_ML"></i></a>
            <?php }
            ?>
            <?php
            if($getSocialLinks['twitter_url'] && $getSocialLinks['twitter_url'] != "")
            {
              ?>
              <a href="https://<?php echo $getSocialLinks['twitter_url'] ?>" target="_blank"><i class="fa fa-twitter-square my_font_icon fa_f_ML"></i></a>
            <?php }
            ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!--  FOOTER END  -->
<script type="text/javascript">
var My_variable='<?php echo  $this->session->userdata('userAuth'); ?>';
var base_url='<?php echo base_url(); ?>';
var setCurrency = '<?php echo $currencysymbol; ?>';
</script>
<script src="<?php echo base_url() ?>Extra/apparels/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/jquery.meanmenu.min.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/jquery.mixitup.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/waypoints.min.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/wow.min.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/venobox.min.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/simplePlayer.js"></script>

<script src="<?php echo base_url() ?>Extra/apparels/js/validation.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/main.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/intlTelInput.js"></script>


<!-- <script src="<?php echo base_url() ?>Extra/searchkeyword.js"></script> -->
<script type="text/javascript">


$('#searchboxsuggestionproduct #searchfiltertext').on('keyup',function(){


  var val = $(this).val();
  val = val.replace(/^\s|\s+$/, "");
  if (val) {
    $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-spinner fa-spin"></i>');
    $.ajax({
      type: "Get",
      url: '<?=base_url();?>search?key='+val,
      dataType: "json",
      success: function(data) {
        if(data){
          var entery = data['entries'];
          var cartstatus = data['cart'];
          $('#searchfilterdataheader').html('').show();
          $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
          if(entery){
            var allcat='';
            var cat='';

            for (var i=0;i<entery.length;i++) {
              if(cartstatus=='yes'){
                var cartbtn='<a   data-variant="'+entery[i]['clientLevelId']+'" data-cart="'+entery[i]['id']+'" title="Add cart" class="linkcat_search_cart Add_to_cart_button"><i class="fa fa-shopping-bag" aria-hidden="true"></i></a>';
              }

              if(entery[i]['category']=="Category"){
                cat+=' <li class="desktop-suggestion null"><a  href="'+entery[i]['action']+'" class="linkcat_search" title="'+entery[i]['name']+'">'+entery[i]['name']+'</a></li>'
              }
              else{
                allcat+=' <li class="desktop-suggestion null"><a href="'+entery[i]['action']+'" class="linkcat_search" title="'+entery[i]['name']+'">'+entery[i]['name']+'</a>'+cartbtn+'</li>'
              }

            }
            // console.log(cat);
            if(cat || allcat){

              if(allcat){
                // <ul class="desktop-group">
                allcat = ' <li class="desktop-suggestionTitle">All Others</li>'+allcat;

              }
              if(cat){
                cat = ' <li class="desktop-suggestionTitle">Categories</li>'+cat;
              }

              $('#searchfilterdataheader').html('<ul class="desktop-group">'+allcat+cat+'</ul>');

            }
            else{
              $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
            }

          }
          else{
            $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
          }
          //  console.log(data['entries']);

        }
      },
      error: function(){
        $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
        $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
      }
    });



  } else {
    $('#searchfilterdataheader').html('').hide();
    $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
  }
});


function searchForData(value, isLoadMoreMode) {
  // create the ajax object
  ajax = new XMLHttpRequest();
  // the function to execute on ready state is changed
  ajax.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      try {
        var json = JSON.parse(this.responseText)
      } catch (e) {
        noUsers();
        return;
      }

      if (json.length === 0) {
        if (isLoadMoreMode) {
          alert('No more to load');
        } else {
          noUsers();
        }
      } else {
        showUsers(json);
      }


    }
  }
  // open the connection
  ajax.open('GET', '<?=base_url();?>search?key=' + value + '&startFrom=' + loadedUsers , true);
  // send
  ajax.send();
}

function showUsers(data) {
  // the function to create a row
  //console.log(data);


  // loop through the data

}



</script>

<!-- update customer visit -->
<script type="text/javascript">
$(document).ready(function(){
  $.ajax({
    type: "POST",
    url: base_url+"Logout/visitorCount",
    success: function(data) {

    }
  });
})
</script>
<!-- CLient side form validation -->
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('submit','#add_Quotation_dataform',function(){
    var check_required_field='';
    $(".required_validation_for_quotation").each(function(){
      var val22 = $(this).val();
      if (!val22){
        check_required_field =$(this).size();
        $(this).css("border-color","#ccc");
        $(this).css("border-color","red");
      }
      $(this).on('keypress change',function(){
        $(this).css("border-color","#ccc");
      });
    });
    if(check_required_field)
    {
      return false;
    }
    else {
      return true;
    }
  });
});
</script>
<script>
    $(".phone_telephone").intlTelInput({
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: "body",
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "auto",
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: "<?php echo base_url() ?>Extra/apparels/js/utils.js"
    });
  </script>
</body>
</html>
