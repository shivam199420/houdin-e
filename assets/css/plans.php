<?php  $this->load->view('Template/header');?> 

 


 

 <section class="full-landing-section  bg_gray1 full-landing-section-pb60">
      
      <div class="full-landing-section-header text-color1" data-aos="fade-up" data-aos-duration="3000">Choose your <span class="styletext"> Perfect plan</span></div>  

      <div class="choose-plan-content-row">

          <div class="container">
                <div class="row">
                  <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="plans-enquiry-box" data-aos="fade-up" data-aos-duration="3000">
                            <div class="plans-enquiry-box-heading text-color1">TAILORED</div>
                            <div class="plans-enquiry-box-text">Houdin-e tailored just for you. Our team will contact to understand your requirements and deliver you the perfect solution for your business.</div>
                             <div class="text-center">
                              <a class="btn_theme_bluelight btn-round" href="<?php echo base_url() ?>home/contact">Send Enquiry</a>
                            </div>

                              <?php 
                                if($_COOKIE['tempUserPackageId'])
                                {
                                ?>
                                <p style="font-size: 18px;color: black;">
                                Your trial period has expired. But don't you worry! Upgrade now!</p>
                              <p style="font-size: 18px;color: black;">Have some doubts about whether you should continue? Get in touch with us now at support@houdine.com and let us help you find the best plan for you.</p>
                                <?php }
                                ?>

                        </div>

                        <div class="plans-box-col-row" >
                            <?php 
                              $countcheck = 0;
                              foreach($packageData as $packageDataList)
                              {
                               
                                $getMainArray = $packageDataList;
                                   if($getMainArray->houdin_package_name == 'Tailored')
                                  {
                                  }
                                  else{
                            ?>
                            <div class="plans-box-col" data-aos="fade-up" data-aos-duration="3000">
                              <div class="text-color1 plans-box-col-heading">
                                <?php echo $getMainArray->houdin_package_name ?></div>
                               <div class="text-color1 plans-box-col-price"><sup>₹</sup>
                                <?php echo trim($getMainArray->houdin_package_price); ?></div>
                              <div class="text-color2 plans-box-col-tagling text-color2">Billed Yearly</div>
                              <div class="plans-box-col-heading-name ">ENJOY ALL THE FEATURES</div>
                              <ul class="plans-box-col-list text-color2">
                                  <li><?php echo $getMainArray->houdin_package_product ?> Products</li>
                                  <li><?php echo $getMainArray->houdin_package_staff_accounts ?> Staff Accounts</li>
                                  <li><?php echo $getMainArray->houdin_package_stock_location ?> Stock Location</li>
                                  <li><?php echo $getMainArray->houdin_package_app_access ?> App Access</li>
                                  <li><?php echo $getMainArray->houdin_package_order_pm ?> Order Per Month</li>
                                  <li><?php echo $getMainArray->houdin_package_templates ?> Templates</li>
                                  <li><?php echo $getMainArray->houdin_package_sms ?> Free SMS</li>
                                  <li> <?php echo $getMainArray->houdin_package_email ?> Free Email</li>
                                  <?php if($getMainArray->houdin_package_accounting == 'yes') { ?>
                                  <li> Accounting Integration</li>
                                  <?php } ?>
                                  <?php if($getMainArray->houdin_package_pos == 'yes') {?>
                                    <li> POS Integration</li>
                                    <?php } ?>
                                 <?php if($getMainArray->houdin_package_delivery == 'yes') { ?> <li>Delivery Integration</li><?php } ?>
                                <?php if($getMainArray->houdin_package_payment_gateway == 'yes') { ?>  <li>Payment Gateway Integration</li> 
                              <?php }?>

  
                              </ul>

                              <div class="plans-box-col-action-bottom text-center" >
                              <a class="btn_theme_bluelight btn-round" data-toggle="modal" data-target="#user-register" href="javascript:;">Star Free Trial</a>
                            </div> 
                            </div>

                          <?php }
                           if($countcheck==0){
                             $countcheck=1;
                              
                          ?>
                           
                              <div class="plans-box-adsbox hidden-xs" data-aos="fade-up" data-aos-duration="3000"><img src="<?php echo base_url() ?>assets/images/Grow Your Business.svg"/></div>

                            <?php } ?>
  

                          <?php } ?>

                        </div>

                    </div>
                    <div class="col-md-2"></div>
                </div>

            </div> 



      </div> <!-- end section content -->


</section>
<?php /*

  <header class="header bg-gray header-page-top">
  <div class="header-page-top-inner">
    <div class="container">
    <h1 class="display-4">Plans</h1> 
  </div>
  </div>
</header>
  <div class="clearfix"></div>
	 <section class="section py-8">
        <div class="container">
          <header class="section-header"> 
            <h2>Choose your perfect plan</h2>
             
          </header> 


          <div class="row gap-y text-center">
          <?php 
          if($_COOKIE['tempUserPackageId'])
          {
          ?>
          <p style="font-size: 18px;color: black;">
          Your trial period has expired. But don't you worry! Upgrade now!</p>
        <p style="font-size: 18px;color: black;">Have some doubts about whether you should continue? Get in touch with us now at support@houdine.com and let us help you find the best plan for you.</p>
          <?php }
          ?>
           <?php 
            foreach($packageData as $packageDataList)
            {
              $getMainArray = $packageDataList;
              if($getMainArray->houdin_package_name == 'Tailored')
              {
              ?>
              <div class="col-md-<?php if(count($packageData) == 3) { echo "4"; } else if(count($packageData) == 4) { echo "3"; } ?>">
              <div class="pricing-1">
                <p class="plan-name"><?php echo $getMainArray->houdin_package_name ?></p>
                <br>
              
                <div class="custom-min-height">   
                <div class="text-muted">
                  <p class="mb-1"> <?php echo $getMainArray->houdin_package_description ?></p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                </div>
                </div>

                <br>
                <br>
                <br>
                <br>
                <br>
                 
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="<?php echo base_url() ?>home/contact">Contact Us</a></p>
              </div>
            </div>
              <?php }
              else
              {
              ?>
                <div class="col-md-<?php if(count($packageData) == 3) { echo "4"; } else if(count($packageData) == 4) { echo "3"; } ?>">
              <div class="pricing-1">
                <p class="plan-name"><?php echo $getMainArray->houdin_package_name ?></p>
                <br>
                <h2 class="price"> <span class="price-unit">&#8377;</span>
                  <span data-bind-radio="pricing-1"><?php echo $getMainArray->houdin_package_price ?></span>
				  </h2>
               <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">Yearly</span>
                </p>
                <div class="custom-min-height">   
                <div class="text-muted">
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_product ?> Products</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_staff_accounts ?> Staff Accounts</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_stock_location ?> Stock Location</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_app_access ?> App Access</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_order_pm ?> Order Per Month</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_templates ?> Templates</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_sms ?> Free SMS</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_email ?> Free Email</p>
                  <?php if($getMainArray->houdin_package_accounting == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Accounting Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_pos == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> POS Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_delivery == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Delivery Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_payment_gateway == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Payment Gateway Integration</p><?php } ?>
                </div>
                </div>

                <br>
                <?php 
                if($_COOKIE['tempUserPackageId'])
                {
                ?>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="<?php echo base_url() ?>Payumoney/<?php echo $getMainArray->houdin_package_id ?>/<?php echo $getMainArray->houdin_package_name ?>/<?php echo $getMainArray->houdin_package_price ?>">Upgrade Your Package</a></p>
                <?php }
                else
                {
                ?>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" data-toggle="modal" data-target="#user-register" href="javascript:;">Start Free Trial</a></p>
                <?php }
                ?>
              </div>
            </div>
              <?php }
            ?>
            <?php }  
            ?>

 

          </div>


        </div>
      </section>
*/?>
  	

<?php  $this->load->view('Template/footer');?>