<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
 <style type="text/css">
   .coupon {
    border: 3px dashed #bcbcbc;
    border-radius: 10px;
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", 
    "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; 
    font-weight: 300;
}

.coupon #head {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    min-height: 56px;
}

.coupon #footer {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
}

#title .visible-xs {
    font-size: 12px;
}

.coupon #title img {
    font-size: 30px;
    height: 30px;
    margin-top: 5px;
}

@media screen and (max-width: 500px) {
    .coupon #title img {
        height: 15px;
    }
}

.coupon #title span {
    float: right;
    margin-top: 5px;
    font-weight: 700;
    text-transform: uppercase;
}

.coupon-img {
    width: 100%;
    margin-bottom: 15px;
    padding: 0;
}

.items {
    margin: 15px 0;
}

.usd, .cents {
    font-size: 20px;
}

.number {
    font-size: 40px;
    font-weight: 700;
}

sup {
    top: -15px;
}

#business-info ul {
    margin: 0;
    padding: 0;
    list-style-type: none;
    text-align: center;
}

#business-info ul li { 
    display: inline;
    text-align: center;
}

#business-info ul li span {
    text-decoration: none;
    padding: .2em 1em;
}

#business-info ul li span i {
    padding-right: 5px;
}

.disclosure {
    padding-top: 15px;
    font-size: 11px;
    color: #bcbcbc;
    text-align: center;
}

.coupon-code {
    color: #333333;
    font-size: 11px;
}

.exp {
    color: #f34235;
}

.print {
    font-size: 14px;
    float: right;
}



/*------------------dont copy these lines----------------------*/
body {
    font-family: "HelveticaNeue-Light", "Helvetica Neue Light", 
    "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; 
    font-weight: 300;
}
.row {
    margin: 30px 0;
}

#quicknav ul {
    margin: 0;
    padding: 0;
    list-style-type: none;
    text-align: center;
}

#quicknav ul li { 
    display: inline; 
}

#quicknav ul li a {
    text-decoration: none;
    padding: .2em 1em;
}

.btn-danger, 
.btn-success, 
.btn-info, 
.btn-warning, 
.btn-primary {
    width: 105px;
}

.btn-default {
    margin-bottom: 40px;
}
.color_pink{
  color:#b6257c;
}
.color_blue{
  font-size: 20px;
  color: #258bb6;
  font-weight: 600;
}
.color_blue1{

  color: #258bb6;

}
 </style>
 <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
          
                <div class="pull-left">
                 <h4 class="page-title">Print Gift voucher</h4>
                 </div>
                 <div class="hidden-print">
                                            <div class="pull-right">
                                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                                                <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                                            </div>
                 </div>
           
            </div>
     <div class="row m-t-20">
         <div class="col-lg-12">
             <div class="card-box">
                 <div class="row">
                     <div class="col-md-12">
                        <div class="panel panel-default coupon">
                          <div class="panel-heading" id="head">
                              <?php 
                              $getMaster = getmasterurl();
                              ?>
                          <div class="text-center"><img src="<?php echo $getMaster ?>assets/images/main-logo.png" alt="velonic"></div>
                         </div>
                        <div class="panel-body">
                         <!-- <img src="http://streamafrica.com/wp-content/uploads/2016/01/hotels-4.jpg" class="coupon-img img-rounded">-->
                          <div class="col-md-12 well well-sm">
                              <div id="business-info">
                                  <ul>
                                      <li class="color_blue">Gift Voucher Name</li>
                                      
                                  </ul>
                              </div>
                          </div>
                          <div class="col-md-9">
                          <!--<p><b class="color_blue1">Discount</b> 50%</p>-->
                             <p><b class="color_blue1">Gift Voucher Code</b> #JS21425658</p>
                           
                      
                          </div>

                          <div class="col-md-3">
                              <div class="offer color_pink">
                                  <span class="INR"><sub>Discount(%) </sub></span>
                                  <span class="number">50%</span> 
                                  <span class="cents"><sup>upto</sup></span>
                              </div>
                          </div>
                       
                        </div>
                        <div class="panel-footer">
                          <div class="exp"><span class="text-left">Gift Voucher Valid From: Sep 30, 2017</span>
                          
                          <span class="text-right" style="float: right;">Gift Voucher Valid To: Oct 1, 2017</span></div>
                        </div>
                      </div>
               </div>
          </div>
        </div>
    </div>       
 </div>

<?php $this->load->view('Template/footer.php') ?>

<script>
$(document).ready(function(){

    $(".select2").select2();
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
