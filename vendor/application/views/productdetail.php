        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <style type="text/css">
            .panel .panel-body p + p {
    margin-top: 5px !important;
}
        </style>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">
        <!--START Page-Title -->
        <div class="row">
         
        <div class="col-md-8">
        <h4 class="page-title">Product detail</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
       <li><a href="<?php echo base_url(); ?>Product">Product</a></li>
        

        <li class="active">Product Detail</li>
        </ol>  
        </div>
        
        </div>
        <!--END Page-Title -->

        <div class="row m-t-20">
        <div class="col-md-12">
        <div class="panel panel-default">
        <!-- <div class="panel-heading">
        <h4>Invoice</h4>
        </div> -->
        
        <div class="panel-body" style="padding:0px!important">
        <div class="" style="border: 1px solid #eee;float: left;width: 100%;">

        <div class="multiple-section-h4" style="background-color: #fff!important">
        
        
        <h4 class="">Product Name : <span class="text_lightblue" style="color:#797979">Lorem Ipsum</span></h4>
        <h4 class="">Description : <span class="text_lightblue" style="color:#797979">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</span></h4>
        <h4 class="">Product image : </h4>
        <img height="50" src="<?php echo base_url() ?>upload/productImage/72857301024.jpg">
       
       
     
        </div>
      
        </div>
        <hr>
            <div class="clearfix"></div>
 
        <div class="multiple-section" style="margin-top: 20px">
        <h4 class="multiple-section-h4">Batch 1</h4>
 
        <div class="col-md-4"> <p class="">
            <span class="custom-width-set">Cost price :</span>$1256</p>
            <p class=""><span class="custom-width-set">Sale price :</span>$1350 </p>
            <p class=""><span class="custom-width-set">Sale tax :</span>12% </p>
        </div>
       <div class="col-md-4"> <p class="">
           <span class="custom-width-set">Purchase Tax :</span>10%</p>
            <p class=""><span class="custom-width-set">Discount :</span>2% </p>
            <p class=""><span class="custom-width-set">MRP :</span>$1150 </p>
       </div>
       <div class="col-md-4"> <p class="">
           <span class="custom-width-set">Expiry date :</span>DD/MM/YYYY</p>
            <p class=""><span class="custom-width-set">Stock date :</span>DD/MM/YYYY </p>
       </div>
       
        <div class="clearfix"></div>
        </div>
        <div class="multiple-section">
        <h4 class="multiple-section-h4">Batch 1</h4>
 
        <div class="col-md-4"> <p class="">
            <span class="custom-width-set">Cost price :</span>$1256</p>
            <p class=""><span class="custom-width-set">Sale price :</span>$1350 </p>
            <p class=""><span class="custom-width-set">Sale tax :</span>12% </p>
        </div>
       <div class="col-md-4"> <p class="">
           <span class="custom-width-set">Purchase Tax :</span>10%</p>
            <p class=""><span class="custom-width-set">Discount :</span>2% </p>
            <p class=""><span class="custom-width-set">MRP :</span>$1150 </p>
       </div>
       <div class="col-md-4"> <p class="">
           <span class="custom-width-set">Expiry date :</span>DD/MM/YYYY</p>
            <p class=""><span class="custom-width-set">Stock date :</span>DD/MM/YYYY </p>
       </div>
       
        <div class="clearfix"></div>
        </div>
        <div class="multiple-section">
        <h4 class="multiple-section-h4">Batch 1</h4>
 
        <div class="col-md-4"> <p class="">
            <span class="custom-width-set">Cost price :</span>$1256</p>
            <p class=""><span class="custom-width-set">Sale price :</span>$1350 </p>
            <p class=""><span class="custom-width-set">Sale tax :</span>12% </p>
        </div>
       <div class="col-md-4"> <p class="">
           <span class="custom-width-set">Purchase Tax :</span>10%</p>
            <p class=""><span class="custom-width-set">Discount :</span>2% </p>
            <p class=""><span class="custom-width-set">MRP :</span>$1150 </p>
       </div>
       <div class="col-md-4"> <p class="">
           <span class="custom-width-set">Expiry date :</span>DD/MM/YYYY</p>
            <p class=""><span class="custom-width-set">Stock date :</span>DD/MM/YYYY </p>
       </div>
       
        <div class="clearfix"></div>
        </div>

       
        </div>
        </div>
          <div class="clearfix"></div>

         
        </div>
    

        </div>

        </div> <!-- container -->

        </div> <!-- content -->
        </div>

        <!-- Update customer comment -->
        <div class="modal fade" id="updateCustomerCommentModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Update Customer Comment</strong></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="form-group">
        <label>Customer Comment</label>
        <div class="form-group">
        <textarea class="form-control setUserComment" name="setUserComment" row="4"></textarea>
        </div>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="updateUserOrderComment" value="Update"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div> 

        <!-- Update delivery Date -->
        <div class="modal fade" id="updateDeliveryDateModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Update Delivery Date</strong></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="form-group">
        <label>Delivery Date</label>
        <div class="form-group">
        <input type="text" class="form-control date_picker updateDeliveryDateData" placeholder="Delivery Date" name="updateDeliveryDateData"/>
        </div>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="updateDeliveryDate" value="Update"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>

         <!--Send Email to user  -->
         <div class="modal fade" id="sendInvoiceEmailModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Send Invoice</strong></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="form-group">
        <input type="hidden" name="customerEmailData" class="customerEmailData"/>
        <input type="hidden" name="customerOrderData" name="customerOrderData"/>
        <label>Do you really want to send invoice on customer registered email address?</label>
        
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="sendCustomerInvoice" value="Send"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div> 
        <!-- retrun order -->
        <div class="modal fade" id="returnOrderModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Return order</strong></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="form-group">
        <input type="hidden" name="retrunOrderId" class="retrunOrderId"/>
        <label>Do you really want to update the order status?</label>
        
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="returnOrderData" value="Update"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div> 
        <!-- Cancel order -->
        <div class="modal fade" id="cancelOrderModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Cancel order</strong></h4>
        </div>
        <div class="modal-body">



        <div class="row">
        <div class="form-group">
        <input type="hidden" name="cancelOrderId" class="cancelOrderId"/>
        <label>Do you really want to update the order status?</label>
        
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="cancelOrderData" value="Update"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div> 
        <div class="modal fade" id="refundOrderModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Refund order</strong></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="form-group">
        <input type="hidden" name="refundOrderId" class="refundOrderId"/>
        <input type="hidden" name="refundOrderAmount" class="refundOrderAmount"/>
        <label>Do you really want to initiate the refund?</label>
        
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="refundOrderData" value="Update"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div> 
        <?php $this->load->view('Template/footer.php') ?>
        <script type="text/javascript">
        $(document).ready(function(){
                $(document).on('click','.userCommentMore',function(){
                        $(this).hide();
                        $('.setCommentText').html('');
                        $('.setCommentText').html($(this).attr('data-text'));
                        $('.userCommentLess').show();
                })
                $(document).on('click','.userCommentLess',function(){
                        $(this).hide();
                        $('.setCommentText').html('');
                        $('.setCommentText').html($(this).attr('data-text'));
                        $('.userCommentMore').show();
                });
                $(document).on('click','.editUserComment',function(){
                        $('.setUserComment').text($(this).attr('data-text'));
                        $('#updateCustomerCommentModal').modal('show');
                });
                $(document).on('click','.editDeliveryDate',function(){
                        $('.updateDeliveryDateData').val($(this).attr('data-date'));
                        $('#updateDeliveryDateModal').modal('show');
                });
                $(document).on('click','.sendCustomerInvoiceEmail',function(){
                        $('.customerEmailData').val($(this).attr('data-email'));
                        $('.customerOrderData').val($(this).attr('data-id'));
                        $('#sendInvoiceEmailModal').modal('show');
                });
                $(document).on('click','.returnorderBtn',function(){
                        $('.retrunOrderId').val($(this).attr('data-id'));
                        $('#returnOrderModal').modal('show');
                });
                $(document).on('click','.cancelOrderBtn',function(){
                        $('.cancelOrderId').val($(this).attr('data-id'));
                        $('#cancelOrderModal').modal('show');
                });
                $(document).on('click','.refundAmount',function(){
                        $('.refundOrderId').val($(this).attr('data-id'));
                        $('.refundOrderAmount').val($(this).attr('data-amount'));
                        $('#refundOrderModal').modal('show');
                })
        })
        </script>