<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 
  <!-- Content -->
  <div id="content">

     <!-- Linking -->
    <div class="linking">
      <div class="container">
        <ol class="breadcrumb">
          <li class="active">Wishlist</li>
        </ol>
      </div>
    </div>

    <!-- Ship Process -->
    <div class="ship-process padding-top-30 padding-bottom-30">
      <div class="container">
        <ul class="row">

        </ul>
      </div>
    </div>
    <!-- Shopping Cart -->
    <section class="shopping-cart padding-bottom-60">
      <div class="container">
      <div class="table-responsive">
        <table class="table">
          <thead>

        <tr>
            <th class="cpt_no">#</th>
            <th class="cpt_img">Product</th>
            <th class="stock">stock status</th>
            <th class="cpt_p">price</th>
            <th class="add-cart">add to cart</th>
            <th class="cpt_r">remove</th>
        </tr>
    </thead>
          </thead>
          <tbody>
            <?php
            $i=1;
            foreach($wishproducts as $thisItem)
            {
                $image = json_Decode($thisItem->houdinv_products_main_images,true);
                $stock = $thisItem->houdinv_products_total_stocks;
                $price = json_DEcode($thisItem->houdin_products_price,true);
                if($stock>0)
                {
                    $status = "In stock";
                }
                else
                {
                    $status = "Out of stock";
                }
            ?>

            <tr>
            <td class="text-center padding-top-60"><?php echo $i; ?></td>
              <td><div class="media">
                  <div class="media-left">   <img class="" style="height: 100px;" src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $image[0]; ?>" alt="" >   </div>
                  <div class="media-body">
                    <p><?php echo $thisItem->houdin_products_title; ?></p>
                  </div>
                </div></td>
              <td class="text-center padding-top-60"><?php echo $status; ?></td>
               <td class="text-center padding-top-60"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $price['sale_price']-$price['discount']; ?></td>
               <td class="text-center padding-top-60"><a   data-variant="0" data-cart="<?php echo $thisItem->houdin_products_id; ?>"  class="btn-round Add_to_cart_button cp_Wish_remove" style="background: #0088cc;" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>">add to cart</a></td>
              <td class="text-center padding-top-60"><a class="cp_Wish_remove" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>"><i class="fa fa-trash" style="color: #0088cc;font-size: 28px;"></i></a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        </div>
      </div>
    </section>
  </div>
  <!-- End Content -->
    <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>
