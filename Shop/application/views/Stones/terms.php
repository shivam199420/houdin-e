<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header') ?>

    <!-- ================== START MAIN SECTION ================== -->
    <div class="main" role="main">
        <!-- Start page location -->
        <div class="page-location">
            <div class="page-location-layer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-location-title">
                                <h3>terms & Condition</h3>
                            </div>
                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start cart area -->
        <div class="cart-area ">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cart-section">
                            <!-- this table for desktop view -->
                            <div class="desktop-view">
                                
                                <div class="row">
                                    <div class="container ">
                                        <div class="about_page_area fix">
                                            <div class="container">
                                                <div class="row about_inner">
                                                <?php echo $terms[0]->Terms_Conditions ?>
                                                    
                                                     
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="business-logo">
        </div>
    <!-- ================== END MAIN SECTION ================== -->
    <?php 
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view(''.$getShopName.'/Template/footer') ?>