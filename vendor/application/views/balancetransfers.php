<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); ?>
<?php 
$getCurrency = getVendorCurrency();
if($getCurrency[0]->houdin_users_currency=="USD")
{
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="AUD"){
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="Euro"){
	$currencysymbol= "£";
}else if($getCurrency[0]->houdin_users_currency=="Pound"){
	$currencysymbol= "€";
}else if($getCurrency[0]->houdin_users_currency=="INR"){
	$currencysymbol= "₹";
}
?>
<style type="text/css">
.zoom img{
    
    transition-duration: 5s;
    margin: 0 auto;
}
img {
    vertical-align: middle;
    height: 40px;
    width: auto; 
} 

.zoom { 
    transition: all 1s;
    -ms-transition: all 1s;
    -webkit-transition: all 1s;
    margin-top: 0px;
    padding-top: 0px;
    
}
.zoom:hover {
    -ms-transform: scale(2); /* IE 9 */
    -webkit-transform: scale(2); /* Safari 3-8 */
    transform: scale(2); 
    margin-left: 40px;
}
.m-b-10{
    margin-bottom: 10px;
}
.table-bordered > tbody > tr > td {
    vertical-align: text-top !important;
}
.remove-tr
{
    display:none;
}
.select2.select2-container
{
    margin-bottom:4px;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" /> 
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
<!--START Page-Title -->
            <div class="row">
            <?php 
            
            $lastEntry = getMainAccount($this->uri->segment('3'));
            ?>
                <div class="col-sm-12">
                <div class="col-md-8">
                   <h4 class="page-title">Account / <?php echo $accountname; ?></h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>accounts/accounting">Accounting </a></li>
                  <li class="active">Balance Transfer</li>
                  </ol>
                  </div>
                  <div class="col-md-4">
                  <select class="form-control navbar-left app-search pull-left custom_search_all setNewEntry">
                  <option value="">Add Entry</option>
                  <!-- <option value="Deposit">Deposit</option>
                  <option value="Receive Payment">Receive Payment</option>
                  <option value="Refund">Refund</option> -->
                  <option value="Transfer">Transfer</option>
                  <option value="journal Entry">journal Entry</option>
                  </select>
                  <!-- <form role="search" class="">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form> -->
                      </div>
                  
                </div>
                <?php 
           
                ?>
            </div>
           <!--END Page-Title --> 
           

                                    
        <div class="row">
          <div class="col-md-12">
          <?php 
          if($this->session->flashdata('success'))
          {
              echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
          }
          if($this->session->flashdata('error'))
          {
              echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
          }
          ?>
             
          <div class="clearfix"></div>
            <div class="card-box table-responsive">
              
            <?php echo form_open(base_url('Accounts/balancetransferssave'), array( 'id' => 'balancetransfersave', 'method'=>'POST' ));?>

<div class="row m-t-20">
<div class="col-sm-12">
<div class="card-box">


<div class="row">
<div class="col-md-12 form-horizontal">

<div class="form-group">
<label class="col-md-2 control-label">Transfer From</label>
<div class="col-md-4">
<select class="form-control select1 required_validation_for_vandor transferfrom" name="transferfrom">
 
                    <option value="">Select Account</option>
                    <?php 
                    foreach($parentAccount as $parentAccountList)
                    {
                    ?>
                  <option  value="<?php echo $parentAccountList->houdinv_accounts_id ?>"><?php echo $parentAccountList->houdinv_accounts_name; ?></option>
                    <?php
                    }?>
                    
</select>
</div>
<div class="blanceshowcurrentaccount" ><label class="col-md-2 control-label">Total Balance </label> <div class="col-md-4 blanceshowcurrentaccount_show">00.00</div> </div>

</div>


 
 

<div class="form-group">

<div><label class="col-md-2 control-label">Transfer To</label></div>
<div class="col-md-4">
<select class="form-control select1 required_validation_for_vandor transferto" name="transferto">
 
<option value="">Select Account</option>
                    <?php 
                    foreach($parentAccount as $parentAccountList)
                    {
                    ?>
                  <option  value="<?php echo $parentAccountList->houdinv_accounts_id ?>"><?php echo $parentAccountList->houdinv_accounts_name; ?></option>
                    <?php
                    }?>
</select>
</div>
 <label class="col-md-2 control-label"> Ref No. </label> <div class="col-md-4"><input type="text" value="<?=set_value('refnumber');?>" class="form-control required_validation_for_vandor" name="refnumber" placeholder="Ref No."> </div>  

</div>

<div class="form-group">
<label class="col-md-2 control-label">Transfer Amount</label>
<div class="col-md-4">
<input type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="<?=set_value('transferamount');?>" class="form-control required_validation_for_vandor" name="transferamount" placeholder="Transfer Amount">
<div class="errorMessage"><?php echo form_error('transferamount'); ?></div>
</div>

<label class="col-md-2 control-label">Transfer Date</label>
<div class="col-md-4">
<input type="text" value="<?=set_value('transferdate');?>" class="form-control date_picker required_validation_for_vandor" name="transferdate" placeholder="Transfer Date">
<div class="errorMessage"><?php echo form_error('transferdate'); ?></div>

</div>
</div>
 
 
<div class="form-group">
<label class="col-md-2 control-label">Memo</label>
<div class="col-md-10">
<textarea class="form-control required_validation_for_vandor" placeholder="Memo type here..." name="transfermemo"> <?=set_value('transfermemo');?></textarea>
<div class="errorMessage"><?php echo form_error('transfermemo'); ?></div>
</div>
</div>
 

 
<div class="form-group">
<div class="col-md-12 ">
<input type="submit" class="btn btn-default pull-right m-r-10" id="roommate_but" name="transferdata" value="Submit">
<button type="reset" class="btn btn-info pull-right m-r-10">Reset</button>
</div></div>

</div>
</div>
</div>
</div>
</div>
<?php echo form_close(); ?>
              
           
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--Delete-->

  <div id="delete_product" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
      <?php echo form_open(base_url().'Product/',array("id"=>"DeleteProduct","enctype"=>"multipart/form-data")); ?>
 
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete product</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this product ?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
               <input type="hidden" name="delete_id" id="delete_id" />
          <input type="submit" class="btn btn-info" name="DeleteEntry" value="Delete">

          </div>

          </div>
       <?php echo form_close(); ?> 
          </div>

          </div>
<!--change Status-->
<div id="deleteBalanceSheetModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <?php echo form_open(base_url( 'Accounts/deletebalancesheet/'.$this->uri->segment('3').'' ), array('method'=>'post'));?>
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Delete Purchase</h4>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-md-12">
    <input type="hidden" class="deletebalancesheetId" name="deletebalancesheetId"/>
    <h4><b>Do you really want to Delete this purchase ?</b></h4>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-info" name="deletePurchase" value="Delete">
    </div>
    </div>
    <?php echo form_close(); ?>
    </div>
    </div>



<?php $this->load->view('Template/footer.php') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<script>
$(document).on("click",".status_tab",function()
{
    
  $id_status = $(this).attr("data-id");  
  $("#status_id").val($id_status);
    
});

$(document).on("click",".delete_tab",function()
{
    
  $id_delete = $(this).attr("data-id"); 
   $("#delete_id").val($id_delete);  
    
});
document.querySelector('input[list]').addEventListener('input', function(e) {
    var input = e.target,
        list = input.getAttribute('list'),
        options = document.querySelectorAll('#' + list + ' option'),
        hiddenInput = document.getElementById(input.id + '-hidden'),
        inputValue = input.value;

    hiddenInput.value = inputValue;

    for(var i = 0; i < options.length; i++) {
        var option = options[i];

        if(option.innerText === inputValue) {
            hiddenInput.value = option.getAttribute('data-value');
            break;
        }
    }
});

$(document).on("click",'.table_shop_custom_tr',function()
{
    $(this).parents('.table-responsive').find('tr').removeClass("remove-tr");
    $(this).addClass("remove-tr");
    $('.form_row').show();
  var date =   $(this).find('.date').text();
 $('.date1 input').val(date);
 
 $('.one1 input').val($(this).find('.one').text());
 $('.two1 input').val($(this).find('.two').text());
 $('.account1 input').val($(this).find('.account').text());
 $('.memo1 input').val($(this).find('.memo').text());
 $('.decrease1 input').val($(this).find('.decrease').text());
 $('.increase1 input').val($(this).find('.increase').text());
 
 $('.status1 input').val($(this).find('.status').text());
 $('.edit_id').val($(this).find('.edit_fetch').val());
 
  $('.edit_id_account').val($(this).find('.edit_fetch_Account_id').val());

});

$(document).on('click','.save_datsa',function()
{
   
           var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
    

      jQuery.ajax({
          type: "Post",
          url: "<?php echo base_url(); ?>Accounts/accuntingUpdate",
          data:{'csrf_test_name':csrfHash,"id":"hello","date":$('.date1 input').val(),
                "ref_name":$('.one1 input').val(),"ref_type":$('.two1 input').val(),
                "pay_account":$('.account1 input').val(),"memo":$('.memo1 input').val(),
                "decrease":$('.decrease1 input').val(),
                "increase":$('.increase1 input').val(),"status":$('.status1 input').val(),
                "id":$('.edit_id').val(),"account_id":$('.edit_id_account').val()},
        
          success: function(data) {
               window.location.reload();
            }
            });
    
});



$(document).on('click','.delete_datsa',function()
{
   
           var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
    

      jQuery.ajax({
          type: "Post",
          url: "<?php echo base_url(); ?>Accounts/accuntingDElete",
          data:{'csrf_test_name':csrfHash,
                "id":$('.edit_id').val(),"account_id":$('.edit_id_account').val()},
        
          success: function(data) {
               window.location.reload();
            }
            });
    
});

$(document).on("click",'.cancle_datsa',function()
{
    $(this).parents('.table-responsive').find('tr').removeClass("remove-tr");
    
    $('.form_row').hide();


});

</script>

<script type="text/javascript">

$(document).ready(function() {
    $(".transferfrom").change(function () {    
        var transferfrom=this.value;               
        if(transferfrom){
            $("select.transferto option[value='"+transferfrom+"']").prop('disabled',true);
        }
          $.ajax({
         type: "POST",
         url:   "<?=base_url()?>Accounts/GetblanceShowtheblancetrafer", 
         data: {textbox:transferfrom},
         success: 
              function(data){
                 
                $(".blanceshowcurrentaccount_show").html('<p style="background-color: #5392c9; width: 79px;height: 23px;padding: 2px;color: #ffff;">'+data+'</p>');
              }
          });// you have missed this bracket
        
        
        
    });








    $('#balancetransfersave').submit(function(e){ 

var check_required_field='';
      $(this).find(".required_validation_for_vandor").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {
        e.preventDefault(); 
        return false;
      }else{
        $('#roommate_but').prop('disabled', true);      
       return true;

      }  
    });  




});















$(document).ready(function(){
    $(document).on('change','.setNewEntry',function(){
        if($(this).val())
        {
            var setHtmldata = "";
            setHtmldata += '<tr class="appendRowData">'
                    +'<td class="date1"><input class="form-control date_picker getDate" value="<?php echo date('Y-m-d'); ?>" type="text"  /></td>'
                    +'<td ><span class="one1"><input class="form-control getRefNumber" type="text" placeholder="Ref No." name="ref_name"  /></span><br/>'
                    +'<span class="two1"><input class="form-control setValueForEntryType" type="text" readonly="" name="ref_type"  /></span></td>'
                    +'<td  class="account1"><input type="hidden" name="payeetype" class="payeetype"/><select class="form-control select1 setPayeeData" style="margin-bottom:4px" name="account">'
                    +'<option value="">Select Payee</option> <?php foreach($accountholder as $holder) { ?>'
                    +'<option data-user="<?php echo $holder['type']; ?>" value="<?php echo $holder['id']; ?>"><?php echo $holder['name'] ; ?></option>'
                    +'<?php } ?></select><br/>'
                    +'<select class="form-control select1 selectAccount" name="parentaccount"><option value="">Select Account</option>'
                    +'<?php  foreach($parentAccount as $parentAccountList) { ?>'
                    +'<option  value="<?php echo $parentAccountList->houdinv_accounts_id ?>"><?php echo $parentAccountList->houdinv_accounts_name; ?></option>'
                    +'<?php } ?></select></td><td class="memo1"><input class="form-control getMemoData" type="text" placeholder="Memo" name="Memo"  /></td>'
                    +'<td  class="decrease1"><input class="form-control setDecrease" type="text" placeholder="decrease" name="decrease"  /></td>'
                    +'<td  class="increase1"><input class="form-control setIncreaseData" type="text" placeholder="increase" name="increase"  /></td>'
                    +'<td class="status1"><select class="form-control concileStatus" name="concileStatus"><option value="">Choose Concile Status</option><option value="C">C</option><option value="R">R</option></select></td>'
                    +'<td><input type="hidden" class="edit_id" name="edit_id"/><input type="hidden" class="edit_id_account" name="edit_id_account"/></td>'
                    +'<td> <button type="button" class="btn btn-primary btn-xs primary saveNewData" title="Save"><i class="fa fa-check "></i></button>'
                    +'<button type="button" class="btn btn-warning btn-xs secondary cancelAppendData" aria-label="Cancel" data-dojo-attach-point="_cancelBtn"><i class="fa fa-times"></i></button>'
                    +'</td></tr>';
                    $('.appendRowData').remove();
                    $('.appendBodyData').prepend(setHtmldata);
                    if($(this).val() == 'Deposit')
                    {
                        $('.setRefNumber').prop('disabled',true);
                    }
                    else if($(this).val() == 'Receive Payment')
                    {
                        $('.setDecrease').prop('disabled',true);
                    }
                    else if($(this).val() == 'Refund')
                    {
                        $('.setRefNumber').prop('disabled',true);
                        $('.setIncreaseData').prop('disabled',true);
                    }
                    else if($(this).val() == 'Transfer')
                    {
                        $('.setRefNumber').prop('disabled',true);
                        $('.setPayeeData').prop('disabled',true);
                    }
                    
            $('.setValueForEntryType').val($(this).val());
            $(".select1").select2({
            // minimumInputLength: 2
            });
            $('.date_picker').focus(function(){
            $(this).daterangepicker({                    
            singleDatePicker: true,
            showDropdowns: false,    
            locale: { 
                format: 'YYYY-MM-DD',
            }
            });      
        });  
            // $('.form_row').show();
        }
        else
        {
            $('.setValueForEntryType').val('');
            $('.form_row').hide();
        }
    });
    $(document).on('click','.cancelAppendData',function(){
        $('.appendRowData').remove();
    })
    $(".select1").select2({
    // minimumInputLength: 2
});
$(document).on('blur','.setIncreaseData',function(){
    if($(this).val() != "")
    {
        $('.setDecrease').val('');
    }
});
$(document).on('blur','.setDecrease',function(){
    if($(this).val() != "")
    {
        $('.setIncreaseData').val('');
    }
});
$(document).on('change','.setPayeeData',function()
{
    $('.payeetype').val($(this).children('option:selected').attr('data-user'));
})
 $(document).on('click','.saveNewData',function(){
    var setBaseData = '<?php echo base_url() ?>';
    var setDatavalue = $(this).parents('.appendRowData');
    var getAccountId = '<?php echo $this->uri->segment('3'); ?>';
    var getdate = setDatavalue.find('.getDate').val();
    var getRefNumber = setDatavalue.find('.getRefNumber').val();
    var entrytype = setDatavalue.find('.setValueForEntryType').val();
    var setPayeeData = setDatavalue.find('.setPayeeData').val();
    var selectAccount = setDatavalue.find('.selectAccount').val();
    var getMemoData = setDatavalue.find('.getMemoData').val();
    var setDecrease = setDatavalue.find('.setDecrease').val();
    var setIncreaseData = setDatavalue.find('.setIncreaseData').val();
    var concileStatus = setDatavalue.find('.concileStatus').val();
    var payeetype = setDatavalue.find('.payeetype').val();
    if(entrytype == 'Deposit')
    {
        if(getdate != "" && selectAccount != "" && setIncreaseData != "")
        {
            addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,setDecrease,setIncreaseData,concileStatus,payeetype);
        }
        else
        {
            if(getdate == "") { setDatavalue.find('.getDate').attr('style','border:1px solid red !important'); }
            if(selectAccount == "") { setDatavalue.find('.selectAccount').next('.select2-container').attr('style','border:1px solid red !important'); }
            if(setIncreaseData == "") { setDatavalue.find('.setIncreaseData').attr('style','border:1px solid red !important'); }
            return false;
        }   

    }
    else if(entrytype == 'Receive Payment')
    {
        if(getdate != "" && setIncreaseData != "")
        {
            addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,setDecrease,setIncreaseData,concileStatus,payeetype);
        }
        else
        {
            if(getdate == "") { setDatavalue.find('.getDate').attr('style','border:1px solid red !important'); }
            if(setPayeeData == "") { setDatavalue.find('.setPayeeData').next('.select2-container').attr('style','border:1px solid red !important'); }
            if(setIncreaseData == "") { setDatavalue.find('.setIncreaseData').attr('style','border:1px solid red !important'); }
            return false;
        }
    }
    else if(entrytype == 'Refund')
    {
        if(getdate != "" && setDecrease != "")
        {
            addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,setDecrease,setIncreaseData,concileStatus,payeetype);
        }
        else
        {
            if(getdate == "") { setDatavalue.find('.getDate').attr('style','border:1px solid red !important'); }
            if(setDecrease == "") { setDatavalue.find('.setDecrease').attr('style','border:1px solid red !important'); }
            return false;
        }
    }
    else if(entrytype == 'Transfer')
    {
        if(getdate != "" && selectAccount != "")
        {
            addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,setDecrease,setIncreaseData,concileStatus,payeetype);
        }
        else
        {
            if(getdate == "") { setDatavalue.find('.getDate').attr('style','border:1px solid red !important'); }
            if(selectAccount == "") { setDatavalue.find('.selectAccount').next('.select2-container').attr('style','border:1px solid red !important'); }
            return false;
        }
    }
    else if(entrytype == 'journal Entry')
    {
        var setLastData = '<?php echo $lastEntry ?>';
        if(setLastData == 2 || setLastData == 4 || setLastData == 5)
        {
            var assetincrease = setDecrease;
            var assetDecrese = setIncreaseData;
        }
        else
        {
            var assetincrease = setIncreaseData;
            var assetDecrese = setDecrease;
        }
        if(setLastData == 16 || setLastData == 3)
        {
            if(getdate != "")
            {
                addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,assetDecrese,assetincrease,concileStatus,payeetype);
            }
            else
            {
                if(getdate == "") { setDatavalue.find('.getDate').css('border','1px solid red !important'); }
                if(selectAccount == "") { setDatavalue.find('.selectAccount').next('.select2-container').css('border','1px solid red !important'); }
                return false;
            }
        }
        else
        {
            if(getdate != "" && selectAccount != "")
            {
                addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,assetDecrese,assetincrease,concileStatus,payeetype);
            }
            else
            {
                if(getdate == "") { setDatavalue.find('.getDate').css('border','1px solid red !important'); }
                if(selectAccount == "") { setDatavalue.find('.selectAccount').next('.select2-container').css('border','1px solid red !important'); }
                return false;
            }
        }
        
    }
 });
 function addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,setDecrease,setIncreaseData,concileStatus,payeetype)
 {
     $('.saveNewData').prop('disabled',true);
    jQuery.ajax({
        type: "POST",
        url: setBaseData+"Accounts/addNewAccountJournal",
        data: {"getAccountId": getAccountId,"getdate":getdate,"getRefNumber":getRefNumber,"entrytype":entrytype,"setPayeeData":setPayeeData,"selectAccount":selectAccount,"getMemoData":getMemoData,"setDecrease":setDecrease,"setIncreaseData":setIncreaseData,'concileStatus':concileStatus,"payeetype":payeetype
        },
        success: function(data) {
        var getData = $.parseJSON(data)
        if(getData.message == 'yes')
        {
            alert('Entry addedd successfully');
            location.reload();
        }
        else
        {
            alert('Something went wrong. Please try again');
            $('.saveNewData').prop('disabled',false);
        }
        }
        }); 
 }
 $(document).on('click','.deleteAccountingBalance',function(){
     $('.deletebalancesheetId').val($(this).attr('data-id'));
     $("#deleteBalanceSheetModal").modal('show');
 })
})
</script>