        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/storesetting') ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/magnific-popup/css/magnific-popup.css"/>
        <style type="text/css">
        @media screen and (min-width: 768px){}
        .dropdown.dropdown-lg .dropdown-menu {
        min-width: 390px!important;
        }
        .checkbox{
        padding-left: 0px;
        }
        .checkbox, .radio {
        position: relative;
        display: block;
        margin-top: 0px;
        margin-bottom: 10px;
        }
        .thumb {
        background-color: #ebf0f2;
        border-radius: 3px;
        box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);
        margin-top: 20px;
        padding-bottom: 10px;
        padding-left: 10px;
        padding-right: 10px;
        padding-top: 10px;
        width: 100%;
        }
        .portfolioFilter a {
        -moz-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
        -moz-transition: all 0.3s ease-out;
        -ms-transition: all 0.3s ease-out;
        -o-transition: all 0.3s ease-out;
        transition: all 0.3s ease-out;
        -webkit-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
        -webkit-transition: all 0.3s ease-out;
        box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
        color: #333333;
        border-radius: 3px;
        padding: 10px 10px;
        display: inline-block;
        margin-bottom: 5px;
        width: 100%;
        }
        }
        </style>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">

        <!--START Page-Title -->
        <div class="row">
   
        <div class="col-md-8">
        <h4 class="page-title">Store Theme</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>storesetting/Storesettinglanding">Store setting</a></li>
        <li class="active">Store Theme</li>
        </ol>
        </div>

        </div>
        <!--END Page-Title --> 


        <div class="row m-t-20">

        <div class="col-sm-12 col-md-12">
        <div class="card-box" style="border-radius: 5px 5px 0px 0px!important;margin-bottom: 0px!important;">
        <!-- SECTION FILTER 
        ================================================== -->  
        <div class="align-center">
        <h3>Your Business: <?php echo $templateInfo[0]->houdin_business_category_name ?></h3>
        <!-- <h4>Select a look for your store.</h4>
        <p>Choose any theme irrespective of category for your brand and get going</p> -->
        </div>

        <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-12 ">
        <div class="portfolioFilter">
        <ul class="custom_ul_filter">
        <h3 class="custom_h3_filter" style="color: #ff7089!important;text-align: center;">Category</h3>
        <li class="custom_li_filter"><a class="current" href="#" data-filter=".departmental"><?php echo $templateInfo[0]->houdin_business_category_name ?></a></li>
        <!-- <li class="custom_li_filter"><a href="#" data-filter=".apparels">Apparels</a></li>
        <li class="custom_li_filter"><a href="#" data-filter=".stonetiles">Stone and tiles</a></li>
        <li class="custom_li_filter"><a href="#" data-filter=".electronics">Electronics and electrical appliances</a></li>
        <li class="custom_li_filter"><a href="#" data-filter=".optical">Optical</a></li>
        <li class="custom_li_filter"><a href="" data-filter=".orderstores">Work order stores </a></li>
        <li class="custom_li_filter"><a href="" data-filter=".generalstore">Other general stores</a></li> -->

        </ul>


        </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-12 ">
        <div class="portfolioContainer">
        <div class="col-sm-12 col-lg-6 col-md-6 departmental ">
        <div class="gal-detail thumb">
        <!-- <a href="https://tech.hawkscode.in/houdin-e/master/uploads/template/<?php echo $templateInfo[0]->houdin_template_thumbnail ?>" class="image-popup" title="Screenshot-1"> -->
        <?php 
        $getMaster = getmasterurl();
        ?>
        <img src="<?php echo base_url() ?>uploads/template/<?php echo $templateInfo[0]->houdin_template_thumbnail ?>" class="thumb-img" alt="work-thumbnail">
        </a>
        <!-- <div class="align-center"><button type="button" class="m-t-10 btn btn-danger waves-effect waves-light">SELECT</button></div> -->
        </div>
        </div>



        <!-- <div class="col-sm-12 col-lg-6 col-md-6 apparels" style="display: none;">
        <div class="gal-detail thumb">
        <h4 class="m-t-0 align-center">Crop Top</h4>
        <a href="<?php //echo base_url();?>assets/images/gallery/5.jpg" class="image-popup" title="Screenshot-5">
        <img src="<?php //echo base_url();?>assets/images/gallery/5.jpg" class="thumb-img" alt="work-thumbnail">
        </a>
        <div class="align-center"><button type="button" class="m-t-10 btn btn-danger waves-effect waves-light">SELECT</button></div>
        </div>
        </div>

        <div class="col-sm-12 col-lg-6 col-md-6 stonetiles" style="display: none;">
        <div class="gal-detail thumb">
        <h4 class="m-t-0 align-center">Kotastone</h4>
        <a href="<?php //echo base_url();?>assets/images/gallery/6.jpg" class="image-popup" title="Screenshot-6">
        <img src="<?php //echo base_url();?>assets/images/gallery/6.jpg" class="thumb-img" alt="work-thumbnail">
        </a>
        <div class="align-center"><button type="button" class="m-t-10 btn btn-success waves-effect waves-light">SELECTED</button></div>
        </div>
        </div>



        <div class="col-sm-12 col-lg-6 col-md-6 electronics" style="display: none;">
        <div class="gal-detail thumb">
        <h4 class="m-t-0 align-center">Mobile</h4>
        <a href="<?php //echo base_url();?>assets/images/gallery/9.jpg" class="image-popup" title="Screenshot-9">
        <img src="<?php //echo base_url();?>assets/images/gallery/9.jpg" class="thumb-img" alt="work-thumbnail">
        </a>

        <div class="align-center"><button type="button" class="m-t-10 btn btn-danger waves-effect waves-light">SELECT</button></div>
        </div>
        </div>

        <div class="col-sm-12 col-lg-6 col-md-6 optical" style="display: none;">
        <div class="gal-detail thumb">
        <h4 class="m-t-0 align-center">Spaces</h4>
        <a href="<?php //echo base_url();?>assets/images/gallery/10.jpg" class="image-popup" title="Screenshot-10">
        <img src="<?php //echo base_url();?>assets/images/gallery/10.jpg" class="thumb-img" alt="work-thumbnail">
        </a>
        <div class="align-center"><button type="button" class="m-t-10 btn btn-danger waves-effect waves-light">SELECT</button></div>
        </div>
        </div>

        <div class="col-sm-12 col-lg-6 col-md-6 orderstores" style="display: none;">
        <div class="gal-detail thumb">
        <h4 class="m-t-0 align-center">Dress</h4>
        <a href="<?php //echo base_url();?>assets/images/gallery/10.jpg" class="image-popup" title="Screenshot-10">
        <img src="<?php //echo base_url();?>assets/images/gallery/10.jpg" class="thumb-img" alt="work-thumbnail">
        </a>
        <div class="align-center"><button type="button" class="m-t-10 btn btn-danger waves-effect waves-light">SELECT</button></div>
        </div>
        </div>

        <div class="col-sm-12 col-lg-6 col-md-6 generalstore" style="display: none;">
        <div class="gal-detail thumb">
        <h4 class="m-t-0 align-center">Generalstore</h4>
        <a href="<?php //echo base_url();?>assets/images/gallery/10.jpg" class="image-popup" title="Screenshot-10">
        <img src="<?php //echo base_url();?>assets/images/gallery/10.jpg" class="thumb-img" alt="work-thumbnail">
        </a>
        <div class="align-center"><button type="button" class="m-t-10 btn btn-danger waves-effect waves-light">SELECT</button></div>
        </div>
        </div> -->
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <?php $this->load->view('Template/footer.php') ?>
        <!-- <script type="text/javascript" src="<?php //echo base_url(); ?>assets/plugins/isotope/js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php //echo base_url(); ?>assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script> -->

        <script type="text/javascript">
        // $(window).load(function(){
        // var $container = $('.portfolioContainer');
        // $container.isotope({
        // filter: '.departmental',
        // animationOptions: {
        // duration: 750,
        // easing: 'linear',
        // queue: false
        // }
        // });

        // $('.portfolioFilter a').click(function(){
        // $('.portfolioFilter .current').removeClass('current');
        // $(this).addClass('current');

        // var selector = $(this).attr('data-filter');
        // $container.isotope({
        // filter: selector,
        // animationOptions: {
        // duration: 750,
        // easing: 'linear',
        // queue: false
        // }
        // });
        // return false;
        // }); 
        // });
        // $(document).ready(function() {
        // $('.image-popup').magnificPopup({
        // type: 'image',
        // closeOnContentClick: true,
        // mainClass: 'mfp-fade',
        // gallery: {
        // enabled: true,
        // navigateByImgClick: true,
        // preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        // }
        // });
        // });
        </script>
