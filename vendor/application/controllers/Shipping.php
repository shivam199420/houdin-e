<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Shipping extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->load->library('form_validation');
         $this->load->helper('url');
          $this->load->model('Shippingmodel');
          $this->auth_users->is_logged_in();

    }
    public function partner()
    {
        // update postmen api key
        if($this->input->post('updateApiKey'))
        {
            $this->form_validation->set_rules('postmenapikey','api key','required');
            if($this->form_validation->run() == true)
            {
                $setArrayData = array('houdinv_shipping_credentials_key'=>$this->input->post('postmenapikey'));
                $getPostemanKey = $this->Shippingmodel->updatepostmenKey($setArrayData,$this->input->post('postmenapikeyid'));
                if($getPostemanKey)
                {
                    $this->session->set_flashdata('success','Key updated successfully');             
                    redirect("Shipping/partner", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');             
                    redirect("Shipping/partner", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());             
                redirect("Shipping/partner", 'refresh');
            }
        }
        $getShippingDetail = $this->Shippingmodel->getPostemanKey();
        $this->load->view('courierpartner',$getShippingDetail);
    }
    
    public function Shippinglanding()
    {
        $this->load->view('shippinglandingpage');
    }
    
    public function partnersetting()
    {
        $this->load->view('partnersetting');
    }
    public function shippingrule()
    {
         $getshippingrule['shippingrule'] = $this->Shippingmodel->fetchshippingruleRecord();

        $this->load->view('shippingrule',$getshippingrule);
    }

public function shippingrule_save()
{
   //is_unique check the unique email value in users table
         $this->form_validation->set_rules('shipping_min_value', 'Minimum value', 'trim|required|numeric');
         $this->form_validation->set_rules('shipping_charge', 'Shipping Charge', 'trim|required|numeric');
         //$this->form_validation->set_rules('testimonials_image', 'Image', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="shipingruls";         
                $this->load->view('models_',$reg_pup);            
        }else{ 

             $postData_shipping = $this->input->post(); 

          
                  $shipping_min_value= $postData_shipping['shipping_min_value'];
                  $shipping_charge= $postData_shipping['shipping_charge'];
                  $shiping_active= $postData_shipping['shiping_active'];
                  $shiping_update_id= $postData_shipping['shiping_update_id'];
           
           
              $insert_data=array('id'=>$shiping_update_id,'shipping_min_value' =>$shipping_min_value,'shipping_charge' =>$shipping_charge,'shiping_active'=>$shiping_active,'date_time'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Shippingmodel->save_shipingruls($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Shipping Successfully Save');        
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            }



            }
}

    public function warehouselist()
    {
         $warehouselist['warehouse_list'] = $this->Shippingmodel->fetchwarehouseRecord();
         $warehouselist['country_list'] = $this->Shippingmodel->country_addlist();
        $this->load->view('warehouselist',$warehouselist);
    }

    public function warehouselist_delete()
    {
             
           if($this->input->post('delete_w_id')){

      $file_id = $this->Shippingmodel->delete_warehouse($this->input->post('delete_w_id'));
            if($file_id)
            {$this->session->set_flashdata('success','Warehouse Successfully Delete');             
             
           redirect("Shipping/warehouselist", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            }

    }


    }


    public function warehouselist_save()
{
   //is_unique check the unique email value in users table
         $this->form_validation->set_rules('w_name', 'Warehouse Name', 'trim|required');
         $this->form_validation->set_rules('w_pickup_name', 'Warehouse Pickup Name', 'trim|required');
          $this->form_validation->set_rules('w_pickup_phone', 'Warehouse Pickup Phone', 'trim|required');
           $this->form_validation->set_rules('w_pickup_country', 'Warehouse Pickup Country', 'trim|required');
            $this->form_validation->set_rules('w_pickup_state', 'Warehouse Pickup State', 'trim|required');
             $this->form_validation->set_rules('w_pickup_city', 'Warehouse Pickup City', 'trim|required');
              $this->form_validation->set_rules('w_pickup_address', 'Warehouse Pickup Address', 'trim|required');
               $this->form_validation->set_rules('w_pickup_zip', 'Warehouse Pickup Zip', 'trim|required');
          
       
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="warehouselist";  
        $reg_pup['country_list'] = $this->Shippingmodel->country_addlist();        
                $this->load->view('models_',$reg_pup);            
        }else{ 

             $postData_shipping = $this->input->post(); 

          
                  $w_name= $postData_shipping['w_name'];
                  $w_pickup_name= $postData_shipping['w_pickup_name'];
                  $w_pickup_phone= $postData_shipping['w_pickup_phone'];
                  $w_pickup_country= $postData_shipping['w_pickup_country'];
                  $w_pickup_state= $postData_shipping['w_pickup_state'];
                  $w_pickup_city= $postData_shipping['w_pickup_city'];
                  $w_pickup_address= $postData_shipping['w_pickup_address'];
                  $w_pickup_zip= $postData_shipping['w_pickup_zip'];
                    $w_id= $postData_shipping['w_id'];

           
           
              $insert_data=array('id'=>$w_id,'w_name' =>$w_name,'w_pickup_name' =>$w_pickup_name,'w_pickup_phone'=>$w_pickup_phone,'w_pickup_country' =>$w_pickup_country,'w_pickup_state' =>$w_pickup_state,'w_pickup_city'=>$w_pickup_city,'w_pickup_address' =>$w_pickup_address,'w_pickup_zip'=>$w_pickup_zip,'w_date_time'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Shippingmodel->save_Warehouses_list($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Warehouses Successfully Save');        
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            } 
            }
       }
       
       public function state_addlist()
       {
         $country_id = $this->input->post('country_id'); 

          $variable = $this->Shippingmodel->state_addlist($country_id);
             echo '<option value="">Select state</option>';
          foreach ($variable as $key => $value) {
              $state_id=$value->state_id;
        $state_name=$value->state_name;
         echo  "<option value='$state_id'>$state_name</option>";
        
          }
           
       }

       public function warehouselist_edit_poup($value='')
       {
               if($this->input->post('update_id')){

      $edit_result = $this->Shippingmodel->fetchwarehouseRecord_edit($this->input->post('update_id'));
           
      
    }
    $arrayName = array('Edit_poup_val' =>$edit_result);
    echo json_encode($arrayName);
       }

       public function warehouselist_edit()
       {
           //is_unique check the unique email value in users table
         $this->form_validation->set_rules('w_name', 'Warehouse Name', 'trim|required');
         $this->form_validation->set_rules('w_pickup_name', 'Warehouse Pickup Name', 'trim|required');
          $this->form_validation->set_rules('w_pickup_phone', 'Warehouse Pickup Phone', 'trim|required');
           $this->form_validation->set_rules('w_pickup_country', 'Warehouse Pickup Country', 'trim|required');
            $this->form_validation->set_rules('w_pickup_state', 'Warehouse Pickup State', 'trim|required');
             $this->form_validation->set_rules('w_pickup_city', 'Warehouse Pickup City', 'trim|required');
              $this->form_validation->set_rules('w_pickup_address', 'Warehouse Pickup Address', 'trim|required');
               $this->form_validation->set_rules('w_pickup_zip', 'Warehouse Pickup Zip', 'trim|required');
          
       
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="warehouselist_edit";  
        $reg_pup['country_list'] = $this->Shippingmodel->country_addlist();        
                $this->load->view('models_',$reg_pup);            
        }else{ 

             $postData_shipping = $this->input->post(); 

          
                  $w_name= $postData_shipping['w_name'];
                  $w_pickup_name= $postData_shipping['w_pickup_name'];
                  $w_pickup_phone= $postData_shipping['w_pickup_phone'];
                  $w_pickup_country= $postData_shipping['w_pickup_country'];
                  $w_pickup_state= $postData_shipping['w_pickup_state'];
                  $w_pickup_city= $postData_shipping['w_pickup_city'];
                  $w_pickup_address= $postData_shipping['w_pickup_address'];
                  $w_pickup_zip= $postData_shipping['w_pickup_zip'];
                    $w_id= $postData_shipping['w_id'];

           
           
              $insert_data=array('id'=>$w_id,'w_name' =>$w_name,'w_pickup_name' =>$w_pickup_name,'w_pickup_phone'=>$w_pickup_phone,'w_pickup_country' =>$w_pickup_country,'w_pickup_state' =>$w_pickup_state,'w_pickup_city'=>$w_pickup_city,'w_pickup_address' =>$w_pickup_address,'w_pickup_zip'=>$w_pickup_zip,'w_date_time'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Shippingmodel->save_Warehouses_list($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Warehouses Successfully Update');        
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            } 
            }
       }




}
