<?php $this->load->view('Bakery/Template/header'); ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<style>
#quote-carousel {
    padding: 0 10px 30px 10px;
    margin-top: 60px;
}
#quote-carousel .carousel-control {
    background: none;
    color: #CACACA;
    font-size: 2.3em;
    text-shadow: none;
    margin-top: 30px;
}
#quote-carousel .carousel-indicators {
    position: relative;
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-top: 20px;
    margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
    width: 50px;
    height: 50px;
    cursor: pointer;
    border: 1px solid #ccc;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    border-radius: 50%;
    opacity: 0.4;
    overflow: hidden;
    transition: all .4s ease-in;
    vertical-align: middle;
}
#quote-carousel .carousel-indicators .active {
    width: 128px;
    height: 128px;
    opacity: 1;
    transition: all .2s;
}
.item blockquote {
    border-left: none;
    margin: 0;
}
.item blockquote p:before {
    content: "\f10d";
    font-family: 'Fontawesome';
    float: left;
    margin-right: 10px;
}
</style>
    <div id="back2top"><i class="fa fa-angle-up"></i></div>

    <div class="loader"></div>

    <div class="page-wrap">
    <?php
        if(count($sliderListData) > 0)
        {
        ?>
      <div class="ps-banner--home-1">

        <div class="rev_slider_wrapper fullscreen-container" id="revolution-slider-1" data-alias="concept121" data-source="gallery" style="background-color:#000000;padding:0px;">

          <div class="rev_slider fullscreenbanner" id="rev_slider_1059_1" style="display:none;" data-version="5.4.1">

            <ul class="ps-banner">
            <?php
        foreach($sliderListData as $keys=>$sliderListDataList)
        {
          ?>


              <li data-index="rs-297<?=$keys;?>" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?php echo $this->session->userdata('vendorURL') ?>upload/Slider/<?php echo $sliderListDataList->houdinv_custom_slider_image ?>" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""><img class="rev-slidebg" src="<?php echo $this->session->userdata('vendorURL') ?>upload/Slider/<?php echo $sliderListDataList->houdinv_custom_slider_image ?>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" data-no-retina>

                <div class="tp-caption ps-banner__caption" id="layer0<?=$keys;?>" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-100','-80','-120','-120']" data-width="['none','none','none','400']" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames="[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1700,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]" style="z-index: 7; white-space: nowrap;text-transform:left;"><?php echo $sliderListDataList->houdinv_custom_slider_head ?></div>

                <div class="tp-caption ps-banner__description" id="layer0<?=$keys+1;?>" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-type="text" data-responsive_offset="on" data-textAlign="['center','center','center','center']" data-frames="[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1500,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]"><?php echo $sliderListDataList->houdinv_custom_slider_sub_heading ?></div>

              </li>
              <?php }
        ?>

              <!-- <li data-index="rs-2973" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?php echo base_url() ?>Extra/bakery/assets/images/concept4-100x100.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""><img class="rev-slidebg" src="<?php echo base_url() ?>Extra/bakery/images/banner/img-slider-2.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" data-no-retina>

                <div class="tp-caption ps-banner__caption" id="layer04" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-100','-80','-120','-120']" data-width="['none','none','none','400']" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames="[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1700,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]" style="z-index: 7; white-space: nowrap;text-transform:left;">A little bliss in every bite</div>

                <div class="tp-caption ps-banner__description" id="layer05" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-type="text" data-responsive_offset="on" data-textAlign="['center','center','center','center']" data-frames="[{&quot;from&quot;:&quot;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;&quot;,&quot;speed&quot;:1500,&quot;to&quot;:&quot;o:1;&quot;,&quot;delay&quot;:1500,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1000,&quot;to&quot;:&quot;x:left(R);&quot;,&quot;ease&quot;:&quot;Power3.easeIn&quot;}]">Macaroon lollipop gummi bears liquorice sweet. Icing dessert sweet apple pie chocolate cake. Jujubes halvah cupcake cheesecake.</div>

              </li> -->

            </ul>

          </div>

        </div>

      </div>
<?php } ?>

<?php
        if(count($categoryData) > 0 && $customHome[0]->houdinv_custom_home_data_category)
        {
        ?>

      <section class="ps-section ps-section--best-seller pt-40 pb-100">

        <div class="container">

          <div class="ps-section__header text-center mb-50">

            <!-- <h4 class="ps-section__top">Sweet Cupcakes</h4> -->

            <h3 class="ps-section__title ps-section__title--full"><?php echo $customHome[0]->houdinv_custom_home_data_category; ?></h3>

          </div>

          <div class="ps-section__content">

            <div class="owl-slider owl-slider--best-seller" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="30" data-owl-nav="true" data-owl-dots="false" data-owl-animate-in="" data-owl-animate-out="" data-owl-item="4" data-owl-item-xs="1" data-owl-item-sm="2" data-owl-item-md="3" data-owl-item-lg="4" data-owl-nav-left="&lt;i class=&quot;ps-icon--back&quot;&gt;&lt;/i&gt;" data-owl-nav-right="&lt;i class=&quot;ps-icon--next&quot;&gt;&lt;/i&gt;">

        <?php
        foreach($categoryData as $categoryDataList)
        {
          if($categoryDataList->houdinv_category_thumb)
          {
            $setImageData = $this->session->userdata('vendorURL')."images/category/".$categoryDataList->houdinv_category_thumb;
          }
          else
          {
            $setImageData = base_url()."Extra/noPhotoFound.png";
          }
        ?>

              <div class="ps-product">

                <div class="ps-product__thumbnail">

                  <!-- <div class="ps-badge"><span>-50%</span></div> -->
                  <a class="ps-product__overlay" href="<?php echo base_url() ?>Productlist/<?php echo $categoryDataList->houdinv_category_id ?>"></a><img src="<?php echo $setImageData; ?>" alt="" width="261" height="361">



                </div>

                <div class="ps-product__content"><a class="ps-product__title" href="<?php echo base_url() ?>Productlist/<?php echo $categoryDataList->houdinv_category_id ?>"><?=$categoryDataList->houdinv_category_name?></a>

                  <div class="ps-product__category"><a class="ps-product__category" href="<?php echo base_url() ?>Productlist/<?php echo $categoryDataList->houdinv_category_id ?>"><?php echo date("Y"); ?> Collection</a>

                  </div>


                </div>

              </div>
        <?php } ?>

          </div>

        </div>

      </section>
        <?php } ?>

 <?php
        if($customHome[0]->houdinv_custom_home_data_latest_product && count($latestProducts) > 0)
        {
        ?>
 <section class="ps-section ps-section--offer  pb-40">

<div class="container">

  <div class="ps-section__header text-center mb-100">

    <h4 class="ps-section__top">Making People Happy</h4>

    <h3 class="ps-section__title ps-section__title--full"><?php echo $customHome[0]->houdinv_custom_home_data_latest_product; ?></h3>

  </div>

    <div class="row">
    <?php
        // print_r($latestProducts);
        foreach($latestProducts as $latestProductsList)
        {
          $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
          $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
        ?>
         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 ">


          <div class="ps-section__content">
          <div class="ps-product">

<div class="ps-product__thumbnail">

  <a class="ps-product__overlay" href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"></a>
  <?php 
    if($getProductImage[0])
    {
        $setlatestIMageData = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
    }
    else
    {
        $setlatestIMageData = base_url()."Extra/noPhotoFound.png";
    }
    ?>
  <img src="<?php echo $setlatestIMageData ?>" alt="" width="263" height="337">
  <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?><div class="ps-badge ps-badge--new"><span>Out of Stock</span></div><?php }
  if($latestProductsList->houdinv_products_main_quotation == 1)
  {
  ?>
  <div class="ps-badge ps-badge--new model_qute_show" data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal">
  <span style="font-size: 11px;text-align: center;">Ask Quotation</span></div>
  <?php }
  ?>
  <ul class="ps-product__action">

    <li><a class="" href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>" data-effect="mfp-zoom-out" data-tooltip="View"><i class="ps-icon--search"></i></a></li>

    <li><a  class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" data-tooltip="Add to wishlist"><i class="ps-icon--heart"></i></a></li>

    <!-- <li><a href="#" data-tooltip="Compare"><i class="ps-icon--reload"></i></a></li> -->
    <?php

        if($latestProductsList->houdinv_products_total_stocks <= 0)
        {
          if($cartFunction == 1)
          {
            ?>
    <li><a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" data-tooltip="Add to cart"><i class="ps-icon--shopping-cart"></i></a></li>
    <?php  }
        }
        else
        {
          ?>
          <li>    <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-cart-plus"></i></a>
      </li>
          <?php }
        ?>
  </ul>

</div>

<div class="ps-product__content"><a class="ps-product__title" href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><?php echo substr($latestProductsList->houdin_products_title,0, 25).".." ; ?></a>

   <!-- <div class="ps-product__category"><a class="ps-product__category" href="product-listing.html">cupcake</a><a class="ps-product__category" href="product-listing.html">sweet</a><a class="ps-product__category" href="product-listing.html">bio</a>

  </div> -->



  <p class="ps-product__price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?> <?php  } ?></p>

</div>

</div>

</div>

          </div>
        <?php  }?>
         </div>
    </div>

</div>
</section>
        <?php } ?>
        <?php
        if($customHome[0]->houdinv_custom_home_data_featured_product)
        {
         ?>
      <section class="ps-section ps-section--offer pt-80 pb-40">

        <div class="container">

          <div class="ps-section__header text-center mb-100">

            <h4 class="ps-section__top">Making People Happy</h4>

            <h3 class="ps-section__title ps-section__title--full"><?php echo $customHome[0]->houdinv_custom_home_data_featured_product; ?></h3>

          </div>

          <div class="row">

                <?php
        foreach($featuredProduct as $latestProductsList)
        {
           //print_r($latestProductsList);
          $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
          $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
        ?>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">


                  <div class="ps-section__content">

                    <div class="ps-product--list">

                      <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"></a>
                      <?php 
                      if($getProductImage[0])
                      {
                          $setlatestIMageDataValue = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                      }
                      else
                      {
                          $setlatestIMageDataValue = base_url()."Extra/noPhotoFound.png";
                      }
                      ?>
                      <img src="<?php echo $setlatestIMageDataValue ?>" alt="" width="150" height="192">

                      <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?><div class="new_badge">Out of Stock</div><?php } ?>
                      </div>
                      <div class="ps-product__content">

                        <h4 class="ps-product__title"><a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><?php echo substr($latestProductsList->houdin_products_title,0, 25).".." ; ?></a></h4>

                        <p><?=$latestProductsList->houdin_products_short_desc?></p>

                        <p class="ps-product__price">

                         <?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?> <?php  } ?>

                        </p><a class="ps-btn ps-btn--xs" href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>">Order now<i class="fa fa-angle-right"></i></a>

                      </div>

                    </div>

                  </div>

                </div>
        <?php } ?>
          </div>

        </div>

      </section>
        <?php } ?>

  <?php if(count($storeTestimonial) > 0 && $customHome[0]->houdinv_custom_home_data_testimonial)
        {
        ?>

<section class="ps-section ps-section--offer  pb-40">

        <div class="container">

          <div class="ps-section__header text-center ">

            <h4 class="ps-section__top">Happy People</h4>

            <h3 class="ps-section__title ps-section__title--full"><?php echo $customHome[0]->houdinv_custom_home_data_testimonial; ?></h3>

          </div>

          <div class="row">

<div class="container" >
        <div class="row">
            <div class="col-md-12">
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner text-center">
                        <!-- Quote 1 -->
                        <?php
        foreach($storeTestimonial as $ke=>$storeTestimonialData)
        {
          $active='';
          if($ke==0){
            $active='active';
          }
        ?>
                        <div class="item <?=$active?>">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <p> <?php echo $storeTestimonialData->testimonials_feedback ?></p>
                                        <small><?php echo $storeTestimonialData->testimonials_name ?></small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
        <?php }

        ?>

                    </div>
                    <!-- Bottom Carousel Indicators -->
                    <ol class="carousel-indicators">
                        <?php

                        foreach($storeTestimonial as $kes=>$storeTestimonialData)
                        {
                          $active='';
                          if($kes==0){
                            $active='active';
                          }
                        ?>
                        <li data-target="#quote-carousel" data-slide-to="<?=$kes;?>" class="<?=$active;?>"><img class="img-responsive " src="<?php if($storeTestimonialData->testimonials_image) { ?> <?php echo $this->session->userdata('vendorURL')."upload/testimonials/".$storeTestimonialData->testimonials_image; ?> <?php  } else { ?> <?php echo base_url() ?>Extra/apparels/img/testimonial/1.jpg <?php } ?>" alt=""
                          width="250" height="250">
                        </li>
                        <?php } ?>

                    </ol>

                    <!-- Carousel Buttons Next/Prev -->
                    <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
        </div>
         </div>



           </div>

</div>

</section>
        <?php } ?>
      <?php $this->load->view('Bakery/Template/footer') ?>
