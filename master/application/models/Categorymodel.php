<?php
class Categorymodel extends CI_Model
{
  function __construct()

    {

        parent::__construct();
        $this->categoryTbale="houdin_business_categories";
        $this->appCategoryTable = "app_business_category";
    }
    

    public function getAllCategory($params=false)
    {
        $this->db->select("*")
        ->from($this->categoryTbale);
                        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit'],$params['start']);
            }
            elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit']);
            }

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
            {
                $result = $this->db->count_all_results();
            }else
            {
                $query = $this->db->order_by('houdin_business_category_id','desc')->get(); 
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
            
           
            return $result;
                        

        
    }
    
    public function CategoryAdd($data=false)
    {
        $this->db->insert($this->categoryTbale,$data);
        return true;
    }
    
    public function CategoryEdit($data=false,$id)
    {
        $this->db->where("houdin_business_category_id",$id);
        $this->db->update($this->categoryTbale,$data);
        return true;
    }
    public function CategoryDelete($data=false)
    {
        $this->db->delete($this->categoryTbale,array("houdin_business_category_id"=>$data));
        return true;
    }
/**
 * getAllAppCategory()
 * @purpose: to fetch app category
 * @created by: Shivam Singh Sengar
 * @created at: 08 Dec 2019
 * @param PARAMS $params
 * @return Array
 */
public function getAllAppCategory($params=false)
{
    $this->db->select("*")
    ->from($this->appCategoryTable);
                    
    if(array_key_exists("start",$params) && array_key_exists("limit",$params))
        {
            $this->db->limit($params['limit'],$params['start']);
        }
        elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
        {
            $this->db->limit($params['limit']);
        }

        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
        {
            $result = $this->db->count_all_results();
        }else
        {
            $query = $this->db->order_by('updated_at','desc')->get(); 
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        return $result;      
}

/**
 * appCategoryAdd()
 * @purpose: to add app category
 * @created by: shivam singh sengar
 * @created at: 08 Dec 2019
 * @param DATA $data
 * @return BOOLEAN (0 | 1);
 */
public function appCategoryAdd($data=false)
{
    $this->db->insert($this->appCategoryTable,$data);
    return true;
}

/**
 * appCategoryEdit()
 * @purpose: To edii app category
 * @created by: Shivam singh sengar
 * @created at: 08 dec 2019
 * @param DATA $data
 * @param ID $id
 * @return BOOLEAN (0 | 1)
 */
public function appCategoryEdit($data=false,$id)
{
    $this->db->where("id",$id);
    $this->db->update($this->appCategoryTable,$data);
    return true;
}

/**
 * appCategoryDelete()
 * @purpose: to delete app category
 * @created by: shivam singh sengar
 * @created at: 08 dec 2019
 * @param DATA $data
 * @return BOOLEAN (0 | 1)
 */
public function appCategoryDelete($data=false)
{
    $this->db->delete($this->appCategoryTable,array("id"=>$data));
    return true;
}
}