<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Categorymodel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
        $this->categoryTbl = 'houdinv_categories';
        $this->productTbl = 'houdinv_productType';
    }
    
       public function getAll($params=false)
    {
        $this->db->select("*")
        ->from($this->categoryTbl);
        //->join("houdinv_sub_categories_one","houdinv_categories.houdinv_category_id=houdinv_sub_categories_one.houdinv_sub_category_one_main_id");
                        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit'],$params['start']);
            }
            elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit']);
            }
                    elseif(array_key_exists("active",$params))
            {
                $this->db->where('houdin_country_status','active');
            }

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
            {
                $result = $this->db->count_all_results();
            }else
            {
                $query = $this->db->get();
                $result1 = ($query->num_rows() > 0)?$query->result_array():FALSE;
                $i=0;
                $sub_cat1 = array();
                $sub_cat2 =array();
                $category_part =array();
              
                foreach($result1 as $single)
                {
                    $main = $single;
                  $sub_cat1  =  $this->db->select("*")
                  ->from("houdinv_sub_categories_one")
                  ->where("houdinv_sub_category_one_main_id",$single['houdinv_category_id'])
                  ->get()->result();
               $sub_array =array();
                   foreach($sub_cat1 as $single2)
                   {
                   $sub_cat2  =  $this->db->select("*")
                      ->from("houdinv_sub_categories_two")
                       ->where("houdinv_sub_category_two_sub1_id",$single2->houdinv_sub_category_one_id)
                      ->get()->result(); 
                      
                    $sub_array[] = array("main"=>$single2,"sub"=>$sub_cat2);
                       
                   
                   } 
               
                $category_part[] = array("main"=>$single,"sub"=>$sub_array);      
                  }   
                  
                $result = $category_part;  
               
            }
            
           
            return $result;
                        

        
    }
    
    public function Add($data=false,$sub)
    {
        $getCategoryNameData = $this->db->select('houdinv_category_id')->from('houdinv_categories')->where('houdinv_category_name',$data['houdinv_category_name'])->get()->result();
        if(!count($getCategoryNameData) > 0)
        {
            $this->db->insert($this->categoryTbl,$data);
            $id_insert = $this->db->insert_id();
            if($id_insert)
            {
             foreach($sub as $print)
                {
                        $one_cat=array("houdinv_sub_category_one_name"=>$print['main'],
                                 "houdinv_sub_category_one_main_id"=>$id_insert,
                                 "houdinv_sub_category_one_created_date"=>$data['houdinv_category_created_date'],
                                 "houdinv_sub_category_one_updated_date"=>$data['houdinv_category_updated_date']);
                       
                      $this->db->insert('houdinv_sub_categories_one',$one_cat);
                      $id_insert_sub = $this->db->insert_id();
                       if($print['sub'] && $id_insert_sub)
                       {
                           foreach($print['sub'] as $second_level)
                            {
                                
                             $two_cat=array("houdinv_sub_category_two_sub1_id"=>$id_insert_sub,
                                 "houdinv_sub_category_two_name"=>$second_level['main'],
                                 "houdinv_sub_category_two_main_id"=>$id_insert,
                                 "houdinv_sub_category_two_created_date"=>$data['houdinv_category_created_date'],
                                 "houdinv_sub_category_two_updated_date"=>$data['houdinv_category_updated_date']);       
                       
                              $this->db->insert('houdinv_sub_categories_two',$two_cat);
                            }
                       }
                       
                }
                return array('message'=>"yes",'id'=>$id_insert);
             }
             else
             {
                return array('message'=>"no");
             }   
             
        }
        else
        {
            return array('message'=>"exist");
        }
       
        
    }
    
    public function Edit($data=false,$id,$sub)
    {
        $this->db->where("houdinv_category_id",$id);
     $id_insert =  $this->db->update($this->categoryTbl,$data);
        
    if($id_insert)
        {
         foreach($sub as $print)
            {
                    $one_cat=array("houdinv_sub_category_one_name"=>$print['main'],
                             "houdinv_sub_category_one_main_id"=>$id,
                             "houdinv_sub_category_one_updated_date"=>$data['houdinv_category_updated_date']);
                 
                  if(!$print['main_id']) 
                  {
                        $one_cat['houdinv_sub_category_one_created_date']=$one_cat['houdinv_sub_category_one_updated_date'];
                      $this->db->insert('houdinv_sub_categories_one',$one_cat);
                      $id_insert_sub = $this->db->insert_id();
                  }
                  else
                  {
                      $this->db->where("houdinv_sub_category_one_id",$print['main_id']);
                        $this->db->update('houdinv_sub_categories_one',$one_cat); 
                        $id_insert_sub = $print['main_id'];
                      
                  }
                  
                   if($print['sub'] && $id_insert_sub)
                   {
                       foreach($print['sub'] as $second_level)
                        {
                            
                         $two_cat=array("houdinv_sub_category_two_sub1_id"=>$id_insert_sub,
                             "houdinv_sub_category_two_name"=>$second_level['main'],
                             "houdinv_sub_category_two_main_id"=>$id,
                             "houdinv_sub_category_two_updated_date"=>$data['houdinv_category_updated_date']);       
                   
                   
                      if(!$second_level['main_id']) 
                         {
                           $two_cat['houdinv_sub_category_two_created_date']=$two_cat['houdinv_sub_category_two_updated_date'];  
                          $this->db->insert('houdinv_sub_categories_two',$two_cat);
                          
                          }
                          else
                          {
                            $this->db->where("houdinv_sub_category_two_id",$second_level['main_id']);
                           $this->db->update('houdinv_sub_categories_two',$two_cat); 
                          }
                          
                          
                          
                        }
                   }
                   
            }
         }  
        
        
        
        return true;
    }
    public function Delete($data=false)
    {
        $this->db->delete($this->categoryTbl,array("houdinv_category_id"=>$data));
        return true;
    } 
    
    
    
    
      public function getAllProduct($params=false)
    {
        $this->db->select("*")
        ->from($this->productTbl);
                        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit'],$params['start']);
            }
            elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit']);
            }
                    elseif(array_key_exists("active",$params))
            {
                $this->db->where('houdin_country_status','active');
            }

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
            {
                $result = $this->db->count_all_results();
            }else
            {
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
            
           
            return $result;
                        

        
    }
    
    public function AddProduct($data=false)
    {
       
        $getTypeData = $this->db->select('houdinv_productType_name')->from('houdinv_productType')->where('houdinv_productType_name',$data['houdinv_productType_name'])->get()->result();
        if(!count($getTypeData) > 0)
        {
            $getData = $this->db->insert($this->productTbl,$data);
            if($getData)
            {
                return array('message'=>'yes');
            }
        }
        else
        {
            return array('message'=>'exist');
        }
    }
    
    public function EditProduct($data=false,$id)
    {
        $this->db->where("houdinv_productType_id",$id);
        $this->db->update($this->productTbl,$data);
        return true;
    }
    public function DeleteProduct($data=false)
    {
        $this->db->delete($this->productTbl,array("houdinv_productType_id"=>$data));
        return true;
    } 
    
    
    public function getCategoryTypeData($need)
    {
        $return =array();
        if($need=='both' || $need=='category')
        {
       $result1 = $this->db->select("*")->from("houdinv_categories")->where("houdinv_category_status","active")
        ->get()->result();
        
        
        $sub_cat1 = array();
              $sub_cat2 =array();  
                $category =array();
                
                
                foreach($result1 as $single)
                {
                    $main = $single;
                  $sub_cat1  =  $this->db->select("*")
                  ->from("houdinv_sub_categories_one")
                  ->where("houdinv_sub_category_one_main_id",$single->houdinv_category_id)
                  ->get()->result();
                 $sub_array =array();
                   foreach($sub_cat1 as $single2)
                   {
                   $sub_cat2  =  $this->db->select("*")
                      ->from("houdinv_sub_categories_two")
                       ->where("houdinv_sub_category_two_sub1_id",$single2->houdinv_sub_category_one_id)
                      ->get()->result(); 
                      
                    $sub_array[] = array("main"=>$single2,"sub"=>$sub_cat2);
                       
                   
                   } 
               
                $category[] = array("main"=>$single,"sub"=>$sub_array);      
                  }   
       
               
        
        
        
        
        
        $return['category']=$category;
       }
        if($need=='both' || $need=='type')
        { 
       $type = $this->db->select("*")->from("houdinv_productType")->where("houdinv_productType_status","active")
        ->get()->result();
         $return['type']=$type;
        }
        return $return;
    }
    
  }  