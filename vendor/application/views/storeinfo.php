<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style type="text/css">
  @media screen and (min-width: 768px){}
.dropdown.dropdown-lg .dropdown-menu {
    min-width: 390px!important;
}
.checkbox{
  padding-left: 0px;
}
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 10px;
}
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
 
            <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">Store Info</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Store Setting</a></li>
                  <li class="active">Store Info</li>
                  </ol>
                  </div>
                
            </div>
           <!--END Page-Title --> 
            

         <div class="row m-t-20">
        
       <div class="col-sm-12 col-md-12">
         <div class="card-box" style="border-radius: 5px 5px 0px 0px!important;margin-bottom: 0px!important;">
           <div class="row">
             <div class="pull-left">
              <h5>Specify details about your business that makes you unique.</h5>
              <p>Click below to edit content live</p>
              <h3 class="custom_h3_info">Home Page</h3> 
                <div class="button-list">
                    <button type="button" class="btn btn-success waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-edit"></i>
                       </span>Edit Live</button>

                    <button type="button" class="btn btn-danger waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-undo"></i>
                       </span>Reset Content 1</button>

                    <button type="button" class="btn btn-danger waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-undo"></i>
                       </span>Reset Content 2</button>

                       <button type="button" class="btn btn-danger waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-undo"></i>
                       </span>Reset Content 3</button>
                </div>
              <h3 class="custom_h3_info">About Us Page</h3> 
                <div class="button-list">
                    <button type="button" class="btn btn-success waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-edit"></i>
                       </span>Edit Live</button>

                    <button type="button" class="btn btn-danger waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-undo"></i>
                       </span>Reset Content 1</button>

                    <button type="button" class="btn btn-danger waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-undo"></i>
                       </span>Reset Content 2</button>

                </div> 
                <h3 class="custom_h3_info">Contact Us Page</h3> 
                <div class="button-list">
                    <button type="button" class="btn btn-success waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-edit"></i>
                       </span>Edit Live</button>

                    <button type="button" class="btn btn-danger waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-undo"></i>
                       </span>Reset Content</button>

                </div> 
                 <h3 class="custom_h3_info">FAQ Page</h3> 
                <div class="button-list">
                    <button type="button" class="btn btn-success waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-edit"></i>
                       </span>Edit Live</button>

                    <button type="button" class="btn btn-danger waves-effect waves-light">
                       <span class="btn-label"><i class="fa fa-undo"></i>
                       </span>Reset Content 1</button>


                </div>  
             </div>
           </div>
         </div>
        
       </div>

        </div>

     

<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
