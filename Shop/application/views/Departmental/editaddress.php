<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header") ?>

    <section>
        <div class="container py-85 py-tn-50">
            <div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8 text-center">
                            <h2 class="title">Edit Address</h2>
                            <div class="title-border mx-auto "></div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                <?php
                if($this->session->flashdata('error'))
                {
                    echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
                }
                else if($this->session->flashdata('success'))
                {
                    echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
                }
                ?>
                </div>
            </div>
            <div class="port-body">
                <div class="contact-form">
                <?php echo form_open(base_url( 'Address/edit/'.$this->uri->segment('3').'' ), array( 'id' => 'addaddress', 'method'=>'post' ));?>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <input name="username" value="<?php echo $address[0]->houdinv_user_address_name ?>" placeholder="Name" class="form-control characterValidation required_validation_for_address name_validation" type="text">
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <input name="userphone" value="<?php echo $address[0]->houdinv_user_address_phone ?>" placeholder="Phone number" pattern="^[-+]?\d+$"  class="phone_telephone form-control required_validation_for_address name_validation" type="text">
                                </div>
                            </div>
            </div>
                            <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <textarea rows="3" name="usermainaddress" placeholder="Street address. Apartment, suite, unit etc. (optional)" class="form-control required_validation_for_address name_validation"><?php echo $address[0]->houdinv_user_address_user_address ?></textarea>
                                </div>
                            </div>
            </div>

             <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <input name="usercity" value="<?php echo $address[0]->houdinv_user_address_city ?>" placeholder="Town/City*" class="form-control required_validation_for_address name_validation" type="text">
                                </div>
                            </div>
            </div>

             <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <input name="userpincode" value="<?php echo $address[0]->houdinv_user_address_zip ?>" placeholder="Post code / Zip" class="form-control required_validation_for_address name_validation" type="text">
                                </div>
                            </div>
            </div>


             <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <select class="form-control required_validation_for_address" name="usercountry">
                                <option value="">Choose Country</option>
                                <option <?php if($address[0]->houdinv_user_address_country == $countryList[0]->houdin_user_shop_country) { ?> selected="selected" <?php  } ?> value="<?php echo $countryList[0]->houdin_user_shop_country ?>"><?php echo $countryList[0]->houdin_country_name ?></option>
                                </select>
                                </div>
                            </div>
            </div>

            <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <input type="submit" name="editAddress" class="mx-auto acc_btn text-center acc_btn" value="Update Address"/>
                                </div>
                            </div>
            </div>

                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>


        </div>


    </section>
    <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer") ?>

    <script type="text/javascript">
$(document).ready(function(){
    $(document).on('submit','#addaddress',function(){
        var check_required_field='';
        $(this).find(".required_validation_for_address").each(function(){
            var val22 = $(this).val();
            if (!val22){
                check_required_field =$(this).size();
                $(this).css("border-color","#ccc");
                $(this).css("border-color","red");
            }
            $(this).on('keypress change',function(){
                $(this).css("border-color","#ccc");
            });
        });
        if(check_required_field)
        {
            return false;
        }
        else {
            return true;
        }
    });
});
</script>
