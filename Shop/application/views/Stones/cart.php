<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
    <!-- ================== START MAIN SECTION ================== -->
    <div class="main" role="main">
        <!-- Start page location -->
        <div class="page-location">
            <div class="page-location-layer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-location-title">
                                <h3>Shopping Cart</h3>
                            </div>
                            <!-- <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Cart</li>
                                </ol>
                            </nav> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start cart area -->
        <div class="cart-area">
            <div class="container">
              <?php
              if(count($AllCart)<1)
              {
                ?>
                <div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h3 class="text-center">Your cart is empty</h3></div></div>
                <?php
              }
              else
              {
                ?>
                <div class="row">
                  <?php
                  if($this->session->flashdata('error'))
                  {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                  }

                  if($this->session->flashdata('message_name'))
                  {
                    echo '<div class="alert alert-danger">'.implode("</br>",json_Decode($this->session->flashdata('message_name'))).'</div>';
                  }
                  if($this->session->flashdata('success'))
                  {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                  }
                  ?>
                    <div class="col-lg-12">
                        <div class="cart-section">
                            <!-- this table for desktop view -->
                            <!-- desktop-view -->
                            <div class=" cart_table_area table-responsive">
                                <table class="cart_prdct_table">
                                  <?php echo form_open(base_url( 'Cart/UpdateCart' ), array( 'id' => 'UpdateCart', 'method'=>'post' ));?>
                                    <tbody>
                                        <tr class="cart-header">
                                            <th>
                                                <div>Image</div>
                                            </th>
                                            <th>
                                                <div>PRODUCT TITLE</div>
                                            </th>
                                            <th>
                                                <div>Unit Price</div>
                                            </th>
                                            <th>
                                                <div>Quantity</div>
                                            </th>
                                            <th>
                                                <div>Total Price</div>
                                            </th>
                                            <th>
                                                <div>ACTION</div>
                                            </th>
                                        </tr>
                                        <!-- Start single item -->

                                        <?php
                                        $i=1;
                                        $final_price =0;
                                        foreach($AllCart as $thisItem)
                                        {
                                          if($thisItem['stock'] <= 0)
                													{
                														$setStockData = '(Out of stock)';
                													}
                                          $count = $thisItem['count'];
                                          $main_price = $thisItem['productPrice'];
                                          $total_price = $main_price*$count;
                                          $final_price =$final_price+$total_price;

                                          ?>

                                        <tr class="tr_row">
                                            <td class="cart-img">
                                                <div>
                                                  <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $thisItem['productImage']; ?>" alt="cart1" class="img-fluid">
                                                </div>
                                            </td>
                                            <td class="cart-product-title">
                                                <div>
                                                      <a href="<?php if($thisItem['productId'] != 0) { echo base_url()."Productlistview/".$thisItem['productId'];  } else { echo base_url()."Productlistview/variant/".$thisItem['productId']; }  ?>"><h5><?php echo $thisItem['productName']; ?></h5><span style="color:red"><?php echo $setStockData; ?></span></a>
                                                </div>
                                            </td>
                                            <td class="cart-product-title product-price">
                                                <div>
                                                    <p class="cp_price"><?php echo $main_price; ?></p>
                                                </div>
                                            </td>
                                            <td class="cart-qty">
                                                <div class=" cp_quntty">
                                                    <input name="product[<?php echo  $i-1; ?>][product_id]" value="<?php echo $thisItem['cartId']; ?>" size="2" type="hidden">
                                                    <input name="product[<?php echo  $i-1; ?>][quantity]" value="<?php echo $thisItem['count']; ?>" size="2" type="number">
                                                </div>
                                            </td>
                                            <td class="cart-price">
                                                <div><p class="cpp_total"><?php echo $total_price; ?></p></div>
                                            </td>
                                            <td>
                                                <div>
                                                    <a href="javascript:;" class="cp_cart_remove" data-replace="<?php echo $total_price; ?>" data-id="<?php echo $thisItem['cartId']; ?>" >
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $i++; } ?>
                                        <!-- Start single item -->

                                    </tbody>
                                </table>
                            </div>
                            <?php form_close();?>
                        </div>
                        <div class="order-detail">
                            <h4>your order</h4>
                            <ul class="clearfix">
                                <!-- <li><span class="order-prodcut-title">Product Name</span> <span class="pull-right">1 X $120</span></li>
                                <li><span class="order-prodcut-title">Shipping Rate</span> <span class="pull-right">$10</span></li> -->
                                <li><span class="order-prodcut-title">Cart Subtotal</span> <span id="mcart_subtotal_class" class="pull-right mcart_subtotal_class"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span></li>
                                <li>
                                    <h5><strong>Total <span class="mcart_subtotal_class pull-right"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span></strong></h5>
                                </li>
                            </ul>
                        </div>
                        <div class="cart-btn-area">
                            <a href="javascript:;" class="update_cart porduct-place-order-btn">Update Cart</a>
                            <a href="<?php echo base_url() ?>Checkout" class="porduct-place-order-btn">CHECKOUT</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <!-- Start business logo -->
        <div class="business-logo">
            <div class="container">

            </div>
        </div>
    </div>
    <!-- ================== END MAIN SECTION ================== -->
    <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view(''.$getShopName.'/Template/footer')
    ?>
