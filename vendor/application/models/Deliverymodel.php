<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Deliverymodel extends CI_Model{
  function __construct() 
  {
    parent::__construct();
    $this->load->helper('database');
    $getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
  }
  public function getDeliveryTotalCount()
  {
      $getTotalData = $this->db->select('COUNT(houdinv_delivery_id) as totalDelivery')->from('houdinv_deliveries')->order_by('houdinv_delivery_id','DESC')->get()->result();
      return array('totalRows'=>$getTotalData[0]->totalDelivery);
  }
  public function getDeliverDetails($data)
  {
        $this->db->select('*')->from('houdinv_deliveries');
        if($data['start'] && $data['limit'])
        {
          $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getDeliverydata = $this->db->get();
        $resultDeliverydata = ($getDeliverydata->num_rows() > 0)?$getDeliverydata->result():FALSE;
        $setMainArray = array();
        foreach($resultDeliverydata as $resultDeliverydataList)
        {
            $setChildArray = array();
            // get order details
            $getOrderDetails = $this->db->select('*')->from('houdinv_orders')->where('houdinv_order_id',$resultDeliverydataList->houdinv_delivery_order_id)->get()->result();
            if($resultDeliverydataList->houdinv_delivery_boy_id != 0 && $resultDeliverydataList->houdinv_delivery_boy_id != "")
            {
                $getStaffdata = $this->db->select('*')->from('houdinv_staff_management')->where('staff_id',$resultDeliverydataList->houdinv_delivery_boy_id)->get()->result();
            }
            else
            {
                $getStaffdata = array();
            }
            $setChildArray['delivery'] = $resultDeliverydataList;
            $setChildArray['order'] = $getOrderDetails;
            $setChildArray['deliveryboy'] = $getStaffdata;
            array_push($setMainArray,$setChildArray);
        }
        return array('deliveryData'=>$setMainArray);
  }
}

  ?>