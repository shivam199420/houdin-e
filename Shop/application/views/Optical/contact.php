<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>
 
   <!-- About Banner Start -->

   <section id="about">

       <div class="container">

           <div class="row">

               <div class="about-heading text-center">

                   <h2>Contact</h2>

               </div>

           </div>

       </div>

   </section>

<!-- About Banner End -->

  

<!-- Contact part Start -->

<section id="account">

    <div class="container">

        <div class="row">

            <div class="col-md-8">

                <div class="contact">
                <?php
                if($this->session->flashdata('error'))
                {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                }
                else if($this->session->flashdata('success'))
                {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                }
                ?>
                   <?php echo form_open(base_url( 'Contact/addcontact' ), array( 'id' => 'shopContact', 'method'=>'post' ));?>

                    <h3>Send us a Message</h3>

                    <div class="col-md-6 pl0 col-sm-6">

                        <div class="form-group">

                           <input type="text" name="userName" class="form-control required_validation_for_shop_contact name_validation" placeholder="Name*" />

                        </div>

                    </div>

                    <div class="col-md-6 col-sm-6 email">

                        <div class="form-group">

                           <input type="text" name="userEmail" class="form-control required_validation_for_shop_contact email_validation name_validation" placeholder="Email*" />

                        </div>

                    </div>

                    <div class="col-md-12 pl0 col-sm-6">

                        <div class="form-group">

                            <input type="text" name="userEmailSub" class="form-control required_validation_for_shop_contact name_validation" placeholder="Subject" />

                        </div>

                    </div>

                    <div class="col-md-12 pl0 location">

                        <div class="form-group mb-0">

                             <textarea name="userEmailBody" rows="5" class="form-control required_validation_for_shop_contact name_validation" placeholder="Message"></textarea>

                            <div class="sub">

                                <button type="submit" class="btn"><i class="fa fa-paper-plane"></i> </button>

                            </div>

                        </div>

                    </div>


                    
                    <?php echo form_close(); ?>

                </div>

            </div>

            <div class="col-md-4">

                <div class="contact-us">

                    <h3>Contact with us</h3>

                    <p><i class="fa fa-map-marker"></i> <a href="javascript:;"><?php echo $shopInfo[0]->houdinv_shop_address ?></a></p>

                    <p><i class="fa fa-phone"></i><?php echo $shopInfo[0]->houdinv_shop_contact_info ?></p>

                    <p><i class="fa fa-envelope"></i><a href="mailto:<?php echo $shopInfo[0]->houdinv_shop_customer_care_email ?>">Customer Care: <?php echo $shopInfo[0]->houdinv_shop_customer_care_email ?> </a></p>

                </div>

            </div>

        </div>

    </div>

</section>

<!-- contact part End -->  
  
<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/footer') ?>
 