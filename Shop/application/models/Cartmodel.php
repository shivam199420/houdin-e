<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cartmodel extends CI_Model{
    function __construct() 
    {
    $this->getAppDB = switchDynamicDatabase($data['shop']);
    $this->getDynamicDB = $this->load->database($this->getAppDB,true);
    }

    public function ShowWhishlistData($id)
    {
    $data = $this->getDynamicDB->select("*")->from("houdinv_users_whishlist") 
    ->join("houdinv_products","houdinv_users_whishlist.houdinv_users_whishlist_item_id=houdinv_products.houdin_products_id")
    ->where("houdinv_users_whishlist.houdinv_users_whishlist_user_id",$id)->get()->result();

    return $data;
    }


    public function AddToWishlistDB($data)
    {
        
    $this->getDynamicDB->select("*")->from("houdinv_users_whishlist") 
    ->where("houdinv_users_whishlist_user_id",$data['houdinv_users_whishlist_user_id']);
    
    if($data['houdinv_users_whishlist_item_id'] != 0)
    {
    $this->getDynamicDB->where("houdinv_users_whishlist_item_id",$data['houdinv_users_whishlist_item_id']); 
       
    }
    else
    {
    $this->getDynamicDB->where("houdinv_users_whishlist_item_variant_id",$data['houdinv_users_whishlist_item_variant_id']); 
    }
   
   $already =  $this->getDynamicDB->get()->row();
 
    if(!$already)
    {
    $this->getDynamicDB->insert("houdinv_users_whishlist",$data);
    }      
    return $already;
    }
    
    public function RemoveFromWhishlistDB($id)
    {

    $this->getDynamicDB->delete("houdinv_users_whishlist",array('houdinv_users_whishlist_id'=>$id));  

    }

    public function ShowCartData($id)
    {
    $data = $this->getDynamicDB->select("*")->from("houdinv_users_cart") ->where("houdinv_users_cart.houdinv_users_cart_user_id",$id)->get()->result();
    $setShowCartData = array();
    foreach($data as $dataList)
    {
        if($dataList->houdinv_users_cart_item_id != 0 && $dataList->houdinv_users_cart_item_id != "")
        {
            $getProductList = $this->getDynamicDB->select('*')->from('houdinv_products')->where('houdin_products_id',$dataList->houdinv_users_cart_item_id)->get()->result();
            // get image data
            $getImageData = json_decode($getProductList[0]->houdinv_products_main_images,true);
            $setShowCartData[] = array('count'=>$dataList->houdinv_users_cart_item_count,'productName'=>$getProductList[0]->houdin_products_title,'productImage'=>$getImageData[0],'productPrice'=>$getProductList[0]->houdin_products_final_price,'cartId'=>$dataList->houdinv_users_cart_id,'productId'=>$getProductList[0]->houdin_products_id,'variantId'=>0,'stock'=>$getProductList[0]->houdinv_products_total_stocks);
        }
        else
        {
            $getVariantList = $this->getDynamicDB->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$dataList->houdinv_users_cart_item_variant_id)->get()->result();
            $setShowCartData[] = array('count'=>$dataList->houdinv_users_cart_item_count,'productName'=>$getVariantList[0]->houdin_products_variants_title,'productImage'=>$getVariantList[0]->houdin_products_variants_image,'productPrice'=>$getVariantList[0]->houdinv_products_variants_final_price,'cartId'=>$dataList->houdinv_users_cart_id,'productId'=>0,'variantId'=>$getVariantList[0]->houdin_products_variants_id,'stock'=>$getVariantList[0]->houdinv_products_variants_total_stocks);
        }
    }
    return $setShowCartData;
    }

    public function isInStock($product,$quantity)
    {
     $return =   $this->getDynamicDB->select("*")->from("houdinv_products")
       ->where("houdin_products_id",$product)->get()->row(); 
      
    }
  
  
  
    public function checkProductQuantity($data)
    {
        if($data['productId'] != 0)
        {
            $getProductList = $this->getDynamicDB->select('houdinv_products_total_stocks,houdin_products_title')->from('houdinv_products')->where('houdin_products_id',$data['productId'])->get()->result();
        
            if(count($getProductList) > 0)
            {
                return array('message'=>'Success','quantity'=>$getProductList[0]->houdinv_products_total_stocks,'title'=>$getProductList[0]->houdin_products_title);
            } 
            else
            {
                return array('message'=>'Something went wrong. Please try again');
            }
        }
        else
        {
            $getVariantList = $this->getDynamicDB->select('houdinv_products_variants_total_stocks,houdin_products_variants_title')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data['variantid'])->get()->result();
            if(count($getVariantList) > 0)
            {
                return array('message'=>'Success','quantity'=>$getVariantList[0]->houdinv_products_variants_total_stocks,'title'=>$getVariantList[0]->houdin_products_variants_title);
            } 
            else
            {
                return array('message'=>'Something went wrong. Please try again');
            }
        }
    }  
    
    public function AddToCartDB($data,$quantity)
    {
    $already = $this->getDynamicDB->select("*")->from("houdinv_users_cart") 
    ->where("houdinv_users_cart_user_id",$data['houdinv_users_cart_user_id']);
    if($data['houdinv_users_cart_item_id'] != 0) { $this->getDynamicDB->where("houdinv_users_cart_item_id",$data['houdinv_users_cart_item_id']); }  
    if($data['houdinv_users_cart_item_variant_id'] != 0) { $this->getDynamicDB->where("houdinv_users_cart_item_variant_id",$data['houdinv_users_cart_item_variant_id']); }  
    $already = $this->getDynamicDB->get()->row();
    if(!$already)
    {
    $this->getDynamicDB->insert("houdinv_users_cart",$data);
    } 
    else
    {
    $cart_Count = $already->houdinv_users_cart_item_count+$quantity;
    $this->getDynamicDB->where("houdinv_users_cart_id",$already->houdinv_users_cart_id);
    $this->getDynamicDB->update("houdinv_users_cart",array("houdinv_users_cart_item_count"=>$cart_Count,
    "houdinv_users_cart_update_date"=>$data['houdinv_users_cart_update_date']));   
    }     

    $data_All = $this->getDynamicDB->select("*")->from("houdinv_users_cart") ->where("houdinv_users_cart.houdinv_users_cart_user_id",$data['houdinv_users_cart_user_id'])->get()->result();
    $setCartArray = array();
    foreach($data_All as $data_AllData)
    {
        if($data_AllData->houdinv_users_cart_item_id != 0 && $data_AllData->houdinv_users_cart_item_id != "")
        {
            $getProductData = $this->getDynamicDB->select('*')->from('houdinv_products')->where('houdin_products_id',$data_AllData->houdinv_users_cart_item_id)->get()->result();
            // setImageData
            $getImage = json_decode($getProductData[0]->houdinv_products_main_images,true);
            // set price
            $getPrice = json_decode($getProductData[0]->houdin_products_price,true);
            if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
            {
                $setPrice = $getProductData[0]->houdin_products_final_price;
            }
            else
            {
                $setPrice = $getProductData[0]->houdin_products_final_price;
            }
            $setCartArray[] = array('count'=>$data_AllData->houdinv_users_cart_item_count,'productName'=>$getProductData[0]->houdin_products_title,'productprice'=>$setPrice,'productImage'=>$getImage[0],'houdinv_users_cart_id'=>$data_AllData->houdinv_users_cart_id);
        }
        else
        {
            $getProductData = $this->getDynamicDB->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data_AllData->houdinv_users_cart_item_variant_id)->get()->result();
            // get product
            $getExtraProduct = $this->getDynamicDB->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getProductData[0]->houdin_products_variants_product_id)->get()->result();
             // set price
             $getPrice = json_decode($getExtraProduct[0]->houdin_products_price,true);
             if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
             {
                 $setPrice = $getProductData[0]->houdin_products_variants_prices-$getPrice['discount'];
             }
             else
             {
                 $setPrice = $getProductData[0]->houdin_products_variants_prices;
             }

            $setCartArray[] = array('count'=>$data_AllData->houdinv_users_cart_item_count,'productName'=>$getProductData[0]->houdin_products_variants_title,'productprice'=>$setPrice,'productImage'=>$getProductData[0]->houdin_products_variants_image,'houdinv_users_cart_id'=>$data_AllData->houdinv_users_cart_id);
        }
    }
    return $setCartArray;
    }


    public function UpdateCartDB($data)
    {
        $not_update = array();
        if($data)
        {
            foreach($data as $val)
            {
                $already = $this->getDynamicDB->select("*")->from("houdinv_users_cart") ->where("houdinv_users_cart_id",$val['product_id'])->get()->row();
                if($already)
                {
                    $setQuantityArray = array('shopName'=>'category1','productId'=>$already->houdinv_users_cart_item_id,'variantid'=>$already->houdinv_users_cart_item_variant_id);
                    $getQuantityData = $this->checkProductQuantity($setQuantityArray);
                    if($getQuantityData['message'] != 'Success')
                    {
                        $not_update[] = array("product_id"=>$already->houdinv_users_cart_item_id,"title"=>$getQuantityData['title'],"variantid"=>$already->houdinv_users_cart_item_variant_id,
                        "stock"=>0); 
                    } 
                    else if($getQuantityData['quantity']<$val['quantity'])
                    {
                        $not_update[] = array("product_id"=>$already->houdinv_users_cart_item_id,
                        "title"=>$getQuantityData['title'],
                        "variantid"=>$already->houdinv_users_cart_item_variant_id,"stock"=>$getQuantityData['quantity']); 
                    }  
                    else
                    {
                        if($val['quantity']>0)
                        {
                            $date = strtotime(date("Y-m-d, h:i:s"));
                            $this->getDynamicDB->where("houdinv_users_cart_id",$already->houdinv_users_cart_id);
                            $this->getDynamicDB->update("houdinv_users_cart",array("houdinv_users_cart_item_count"=>$val['quantity'],
                            "houdinv_users_cart_update_date"=>$date));
                        }
                        else
                        {
                            $this->getDynamicDB->delete("houdinv_users_cart",array('houdinv_users_cart_id'=>$already->houdinv_users_cart_id));  
                        }
                    }
                }
            }
        }
       return $not_update; 
    }


    public function RemoveFromCartDB($id)
    {
    $this->getDynamicDB->delete("houdinv_users_cart",array('houdinv_users_cart_id'=>$id));      
    $data_All = $this->getDynamicDB->select("*")->from("houdinv_users_cart") ->where("houdinv_users_cart.houdinv_users_cart_user_id",$this->session->userdata('userAuth'))->get()->result();
    $setCartArray = array();
    foreach($data_All as $data_AllData)
    {
        if($data_AllData->houdinv_users_cart_item_id != 0 && $data_AllData->houdinv_users_cart_item_id != "")
        {
            $getProductData = $this->getDynamicDB->select('*')->from('houdinv_products')->where('houdin_products_id',$data_AllData->houdinv_users_cart_item_id)->get()->result();
            // setImageData
            $getImage = json_decode($getProductData[0]->houdinv_products_main_images,true);
            // set price
            $getPrice = json_decode($getProductData[0]->houdin_products_price,true);
            if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
            {
                $setPrice = $getProductData[0]->houdin_products_final_price;
            }
            else
            {
                $setPrice = $getProductData[0]->houdin_products_final_price;
            }
            $setCartArray[] = array('count'=>$data_AllData->houdinv_users_cart_item_count,'productName'=>$getProductData[0]->houdin_products_title,'productprice'=>$setPrice,'productImage'=>$getImage[0],'houdinv_users_cart_id'=>$data_AllData->houdinv_users_cart_id);
        }
        else
        {
            $getProductData = $this->getDynamicDB->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data_AllData->houdinv_users_cart_item_variant_id)->get()->result();
            // get product
            $getExtraProduct = $this->getDynamicDB->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getProductData[0]->houdin_products_variants_product_id)->get()->result();
             // set price
             $getPrice = json_decode($getExtraProduct[0]->houdin_products_price,true);
             if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
             {
                 $setPrice = $getProductData[0]->houdin_products_variants_prices-$getPrice['discount'];
             }
             else
             {
                 $setPrice = $getProductData[0]->houdin_products_variants_prices;
             }

            $setCartArray[] = array('count'=>$data_AllData->houdinv_users_cart_item_count,'productName'=>$getProductData[0]->houdin_products_variants_title,'productprice'=>$setPrice,'productImage'=>$getProductData[0]->houdin_products_variants_image,'houdinv_users_cart_id'=>$data_AllData->houdinv_users_cart_id);
        }
    }
    return $setCartArray;

    }
    public function addProductCartApp($data,$shop,$wishlistId)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shop['shopname']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getCartData = $getDynamicDB->select('houdinv_users_cart_id')->from('houdinv_users_cart')->where('houdinv_users_cart_item_id',$data['houdinv_users_cart_item_id'])->where('houdinv_users_cart_item_variant_id',$data['houdinv_users_cart_item_variant_id'])->get()->result();
        if(count($getCartData) > 0)
        {
            $setUpdateArray = array('houdinv_users_cart_user_id'=>$data['houdinv_users_cart_user_id'],'houdinv_users_cart_item_id'=>$data['houdinv_users_cart_item_id'],
            'houdinv_users_cart_item_variant_id'=>$data['houdinv_users_cart_item_variant_id'],'houdinv_users_cart_item_count'=>$data['houdinv_users_cart_item_count'],
            'houdinv_users_cart_update_date'=>$shop['dataValue']
            );
            $getDynamicDB->where('houdinv_users_cart_id',$getCartData[0]->houdinv_users_cart_id);
            $getUpdateStatus = $getDynamicDB->update('houdinv_users_cart',$setUpdateArray);
            if($getUpdateStatus == 1)
            {
                // Get user cart count
                $getCartCountData = $getDynamicDB->select('COUNT(houdinv_users_cart_id) AS cartCount')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$data['houdinv_users_cart_user_id'])->get()->result();
                if($getCartCountData[0]->cartCount) 
                {
                    $setCartCount = $getCartCountData[0]->cartCount;
                }
                else
                {
                    $setCartCount = 0;
                }
                // move wishlist from cart
                if($wishlistId != 0 && $wishlistId != "")
                {
                    $getDynamicDB->where('houdinv_users_whishlist_id',$wishlistId);
                    $getDynamicDB->delete('houdinv_users_whishlist');
                }
                return array('message'=>'Product updated to cart','count'=>$setCartCount);
            }
            else
            {
                return array('message'=>'Something went wrong. Please try again');
            }
        }   
        else
        {
            $getInsertStatus = $getDynamicDB->insert('houdinv_users_cart',$data);
            if($getInsertStatus == 1)
            {
                // Get user cart count
                $getCartCountData = $getDynamicDB->select('COUNT(houdinv_users_cart_id) AS cartCount')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$data['houdinv_users_cart_user_id'])->get()->result();
                if($getCartCountData[0]->cartCount) 
                {
                    $setCartCount = $getCartCountData[0]->cartCount;
                }
                else
                {
                    $setCartCount = 0;
                }
                // move wishlist from cart
                if($wishlistId != 0 && $wishlistId != "")
                {
                    $getDynamicDB->where('houdinv_users_whishlist_id',$wishlistId);
                    $getDynamicDB->delete('houdinv_users_whishlist');
                }
                return array('message'=>'Product addedd to cart','count'=>$setCartCount);
            }
            else
            {
                return array('message'=>'Something went wrong. Please try again');
            }
        }
    }
    
    

    
}