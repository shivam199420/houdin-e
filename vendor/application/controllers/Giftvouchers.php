<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Giftvouchers extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->perPage = 100;
        $this->load->model('Giftvouchermodel');
        $this->auth_users->is_logged_in();
    }
    public function index()
    {
        // delete Gift Voucher
        if($this->input->post('deleteGiftVoucherData'))
        {
            $this->form_validation->set_rules('deleteVoucherId','text','required');
            if($this->form_validation->run() == true)
            {
                $getStatus = $this->Giftvouchermodel->deleteGiftVocher($this->input->post('deleteVoucherId'));
                if($getStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Gift voucher deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Giftvouchers", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Giftvouchers", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Giftvouchers", 'refresh');
            }
        }
        // update status
        if($this->input->post('updateStatusData'))
        {
            $this->form_validation->set_rules('updateStatusId','text','required');
            $this->form_validation->set_rules('editGiftVoucherStatus','select option','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setArray = array('id'=>$this->input->post('updateStatusId'),'editGiftVoucherStatus'=>$this->input->post('editGiftVoucherStatus'));
                $getUpdateStatus = $this->Giftvouchermodel->updateGiftVoucherStatus($setArray);
                if($getUpdateStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Gift Voucher status updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Giftvouchers", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Giftvouchers", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Giftvouchers", 'refresh');
            }
        }
        // Custom Pagination
        $getTotalCount = $this->Giftvouchermodel->getGiftVoucherTotalCount();
        $config['base_url']    = base_url().'Giftvouchers/';
        $config['uri_segment'] = 2;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(2);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getGiftVoucherDeatils = $this->Giftvouchermodel->fetchVoucherDetail($setArray);
        $this->load->view('giftvouchers',$getGiftVoucherDeatils);
    }
    public function add()
    {
        if($this->input->post('addGiftVoucher'))
        {
            $this->form_validation->set_rules('voucherName','Voucher Name','required|trim');
            $this->form_validation->set_rules('voucherValidFrom','Voucher Valid From','required');
            $this->form_validation->set_rules('voucherValidTo','Voucher Valid To','required');
            $this->form_validation->set_rules('voucherCode','Voucher Code','required|trim');
            $this->form_validation->set_rules('voucherDiscount','Voucher Discount','required|trim');
            $this->form_validation->set_rules('voucherStatus','Voucher Status','required|trim|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setData = strtotime(date('Y-m-d'));
                $setInsertArray = array('houdinv_vouchers_name'=>$this->input->post('voucherName'),'houdinv_vouchers_valid_from'=>$this->input->post('voucherValidFrom'),
                'houdinv_vouchers_valid_to'=>$this->input->post('voucherValidTo'),'houdinv_vouchers_code'=>$this->input->post('voucherCode'),
                'houdinv_vouchers_discount'=>$this->input->post('voucherDiscount'),'houdinv_vouchers_status'=>$this->input->post('voucherStatus'),
                'houdinv_vouchers_created_at'=>$setData);
                $getStatus = $this->Giftvouchermodel->addGiftVouchersData($setInsertArray);
                if($getStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Gift Voucher addedd sucessfully');
                    $this->load->helper('url');
                    redirect(base_url()."Giftvouchers", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Giftvouchers/add", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."Giftvouchers/add", 'refresh');
            }
        }
        $this->load->view('addgiftvouchers');
    }
    public function edit()
    {
        $getEditId = $this->uri->segment('3');
        if($getEditId == "")
        {
            $this->load->helper('url');
            redirect(base_url()."Giftvouchers", 'refresh');
        }
        // update value
        if($this->input->post('editGiftVoucher'))
        {
            $this->form_validation->set_rules('voucherName','Voucher Name','required|trim');
            $this->form_validation->set_rules('voucherValidFrom','Voucher Valid From','required');
            $this->form_validation->set_rules('voucherValidTo','Voucher Valid To','required');
            $this->form_validation->set_rules('voucherCode','Voucher Code','required|trim');
            $this->form_validation->set_rules('voucherDiscount','Voucher Discount','required|trim');
            $this->form_validation->set_rules('voucherStatus','Voucher Status','required|trim|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setData = strtotime(date('Y-m-d'));
                $setUpdateArray = array('houdinv_vouchers_name'=>$this->input->post('voucherName'),'houdinv_vouchers_valid_from'=>$this->input->post('voucherValidFrom'),
                'houdinv_vouchers_valid_to'=>$this->input->post('voucherValidTo'),'houdinv_vouchers_code'=>$this->input->post('voucherCode'),
                'houdinv_vouchers_discount'=>$this->input->post('voucherDiscount'),'houdinv_vouchers_status'=>$this->input->post('voucherStatus'),
                'houdinv_vouchers_modified_at'=>$setData);
                $getUpdateStatus = $this->Giftvouchermodel->updateGiftVoucher($setUpdateArray,$getEditId);
                if($getUpdateStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Gift voucher updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Giftvouchers/edit/".$getEditId."", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Giftvouchers/edit/".$getEditId."", 'refresh');}
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."Giftvouchers/edit/".$getEditId."", 'refresh');
            }
        }
        $getGiftVocuher = $this->Giftvouchermodel->fetchGiftVoucherData($getEditId);
        $this->load->view('editgiftvouchers',$getGiftVocuher);
    }
    public function view()
    {
        $this->load->view('viewgiftvouchers');
    }
    public function Giftprint()
    {
        $this->load->view('printgiftvouchers');
    }
}
