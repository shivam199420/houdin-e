<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Staffmodel extends CI_Model{
  function __construct() 
  {
    parent::__construct();
    $this->load->helper('database');
    $getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
    $this->masterDB = $this->load->database('master', TRUE);
  }

public function insert_staff($value)
{
   $getDBName = $this->session->userdata('shop');
$main_data=$value['main_staff_data'];
$sub_data=$value['staff_subdata'];
	     $this->db->insert('houdinv_staff_management',$main_data);
       $getCampignId = $this->db->insert_id();
       
 		if($getCampignId)
 		{
         $this->masterDB->select('*')->from('houdin_vendor_staff')->where('vendor_staff_email',$main_data['staff_email']);
         $houdin_vendor_staff = $this->masterDB->get()->row();
   if(empty($houdin_vendor_staff)){
      $datahoudin_vendor_staff=array('vendor_staff_email'=>$main_data['staff_email'],'vendor_staff_shop_name'=>$getDBName,'vendor_staff_db_name'=>$getDBName);
      $this->masterDB->insert('houdin_vendor_staff',$datahoudin_vendor_staff);
   }

 			$staff_id=array('staff_id'=>$getCampignId);
 			 
 		$this->db->insert('houdinv_staff_management_sub',array_merge($staff_id,$sub_data));

 		return true;
 		}else{
 			return false;
 		}
}
public function fetch_staffRecord()
{
   $this->db->select('*')->from('houdinv_staff_management');
        $resultstaffdata = $this->db->get()->result();
        // get total coupons
        $this->db->select('COUNT(*) AS totalstaff')->from('houdinv_staff_management');
        $getTotalstaff = $this->db->get()->result();
        // get total Active coupons
        $this->db->select('COUNT(*) AS totalActivestaff')->from('houdinv_staff_management')->where('staff_status',1);
        $getTotalActivestaff = $this->db->get()->result();
        // get total coupons
        $this->db->select('COUNT(*) AS totalDeactivestaff')->from('houdinv_staff_management')->where('staff_status!=',1);
        $getTotalDeactiveCoupons = $this->db->get()->result();
        return array('staffLsit'=>$resultstaffdata,'Totalstaff'=>$getTotalstaff[0]->totalstaff,'totalActivestaff'=>$getTotalActivestaff[0]->totalActivestaff,'TotalDeactiveCoupons'=>$getTotalDeactiveCoupons[0]->totalDeactivestaff);
    
}

public function get_array($value='')
{
   $this->db->select('*')->from('houdinv_staff_management');
      return  $resultstaffdata = $this->db->get()->result();
}

public function staff_delete($value='')
{
	                  $this->db->where($value);
     $getInsertData = $this->db->delete('houdinv_staff_management');

                      $this->db->where($value);
     $getInsertData_sub = $this->db->delete('houdinv_staff_management_sub');

 if($getInsertData == 1)
            { return ture;}else{return false;} 
}

public function Delete_multiSMSstaff($delete)
{

	 if($delete){
      $this->db->where_in('staff_id',$delete);
      $delete_id= $this->db->delete('houdinv_staff_management');
      $this->db->where_in('staff_id',$delete);
               $this->db->delete('houdinv_staff_management_sub');
           
          if($delete_id){
          	return true;

          }else{
          	return false;
}

           
             } 
             return false;
}


public function fetch_onestaffRecord($value)
{
	$this->db->select('*')->from('houdinv_staff_management')->where('staff_id',$value);
        $resultstaffdata = $this->db->get()->result();

        	$this->db->select('*')->from('houdinv_staff_management_sub')->where('staff_id',$value);
        $staffsubLsit = $this->db->get()->result();

 
         return array('staffLsit'=>$resultstaffdata[0],'staffsubLsit'=>$staffsubLsit[0]);  
}

public function getWarhouse()
{
   $data =  $this->db->select("*")->from("houdinv_shipping_warehouse")->get()->result();
   return $data;
}

public function update_staff($value,$updateid){

	 $main_data=$value['main_staff_data'];
   $sub_data=$value['staff_subdata'];
   $staff_id=array('staff_id'=>$updateid);
   
          $this->db->where('staff_id',$updateid);
	     $this->db->update('houdinv_staff_management',$main_data);
      
       
       $this->db->select('*')->from('houdinv_staff_management_sub')->where('staff_id',$updateid);
       $staffsubLsit = $this->db->get()->result();

 		if($staffsubLsit)
 		{
      
       
 		$this->db->where('staff_id',$updateid);
 		$this->db->update('houdinv_staff_management_sub',$sub_data);

 		return true;
 		}else{
      $this->db->set(array_merge($staff_id,$sub_data));
      $this->db->insert('houdinv_staff_management_sub');
      return true;
 		}
}


  }?>