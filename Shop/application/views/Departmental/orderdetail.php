<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header") ?>

<!------ Include the above in your HEAD tag ---------->

<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<style type="text/css">
	.invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}
</style>
    <section>
        <div class="container py-85 py-tn-50">
           <div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8 text-center">
                            <h2 class="title">Order Detail</h2>
                            <div class="title-border mx-auto m-b-55 m-b-tn-35"></div>
                        </div>
                       
                    </div>
                </div>
            </div>
           
            <div class="port-body">
            <div class="container">
            <div class="order-detail-section">
    <div class="row">
        <div class="col-xs-12">
        	<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>Order Id:</strong>#<?php echo $orders->houdinv_order_id ;?><br>
    				<strong>Order Status:</strong><?php echo $orders->houdinv_order_confirmation_status ;?><br>
    				<strong>Order Date:</strong><?php echo date("Y-m-d ,h:i:s",$orders->houdinv_order_created_at) ;?><br><br>
    				     
    				        	
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
					 
					<?php 
					$array_return = array("Delivered");
					if( in_array($orders->houdinv_order_confirmation_status,$array_return))
					{
					?>
					<span>
					 <form id="return_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
                     <input type="text" name="returnOrderId" value="<?php echo $this->uri->segment("3"); ?>"/>
                   <input type="text" value="returnOrder" name="returnOrder"/>
                    </form>
					<button class="btn btn-default return_button">Return Order</button><br>
					<?php } 
					$array_cancel = array("unbilled","Not Delivered","billed","assigned");
					if( in_array($orders->houdinv_order_confirmation_status,$array_cancel))
					{
					?>
					<span><form id="cancel_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
                     <input type="text" name="cancelOrderId" value="<?php echo $this->uri->segment("3"); ?>"/>
                   <input type="text" value="cancelOrder" name="cancelOrder"/>
                    </form>
					<button class="btn btn-default cancel_button">Cancel Order</button>
						</span>
					<?php } ?>
    				</address>
    			</div>
    		</div>
    	
    
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Confirmed Products</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
								<th>Product Name</th>
								<th>Quantity</th>
								<th>Rate</th>
								<th>Tax</th>
								<th>Total Price</th>
                                </tr>
    						</thead>
    						<tbody>
							<?php 
							$total_product_gross = 0;
							foreach($orders->product_all as $detail)
							{ ?>
    							<tr>
								<td> <?php echo $detail->title ; ?></td>
								<td><?php echo $detail->allDetail->product_Count ; ?></td>
								<td><?php echo $detail->allDetail->product_actual_price; ?></td>
								<td>0</td>
								<td><?php echo $detail->allDetail->total_product_paid_Amount; ?></td>
								</tr>
								<?php 
                                        $total_product_gross = $total_product_gross + $detail->allDetail->total_product_paid_Amount;
                                        } ?>
                                 
    							
    							<tr> 
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line text-right"><strong>Gross Amount</strong></td>
    								<td class="thick-line text-right"> &#8377; <?php echo $total_product_gross;?></td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-right"><strong>Additional Discount</strong></td>
    								<td class="no-line text-right">&#8377; <?php echo  $orders->houdinv_orders_discount; ?></td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-right"><strong>Delivery Charges</strong></td>
    								<td class="no-line text-right">&#8377; <?php echo $orders->houdinv_delivery_charge; ?></td>
    							</tr>
    							<hr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-right"><strong>Round Off</strong></td>
    								<td class="no-line text-right">&#8377; <?php echo $orders->houdinv_orders_total_Amount; ?></td>
    							</tr>
    							<tr> 
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-right"><strong>Net Payable</strong></td>
    								<td class="no-line text-right">&#8377; <?php echo $orders->houdinv_orders_total_Amount; ?></td>
    							</tr>
    						 
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    </div>
</div>
            </div>
        </div>
    </section>
   
 <?php 
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer") ?>

	 <script>
            $(document).on("click",".cancel_button",function()
            {
               $("#cancel_form").submit(); 
                
            });
            
              $(document).on("click",".return_button",function()
            {
               $("#return_form").submit(); 
                
            });
            
            </script>