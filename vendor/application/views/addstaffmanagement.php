<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Add Staff</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>staff">Staff Management</a></li>
                 
                  <li class="active">Add Staff</li>
                  </ol>
                  </div>
                 
            </div>
           <!--END Page-Title --> 
           <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
           <!--END Page-Title -->
           <?php echo form_open(base_url('staff/add'), array( 'id' => 'staff_add', 'method'=>'POST' ));?>

            <div class="row m-t-20">
        <div class="col-sm-12">
        <div class="card-box">
 
         
        <div class="row">
        <div class="col-md-12 form-horizontal">

        <div class="form-group">
        <label class="col-md-2 control-label">Outlet WareHouse</label>
        <div class="col-md-10">
        <select class="form-control required_validation_for_vandor" name="staff_warehouse">
        <option value="">Choose One</option>
        <?php foreach($WareHouse as $house)
        {
          ?>
        <option value="<?php echo $house->id; ?>"><?php echo $house->w_name; ?></option>
        
        <?php } ?>
        </select>
        </div>
        </div>


        <div class="form-group">
        <label class="col-md-2 control-label">Name</label>
        <div class="col-md-10">
        <input type="text" value="<?=set_value('staff_name');?>" class="form-control required_validation_for_vandor" name="staff_name" placeholder="Staff Name">
         <div class="errorMessage"><?php echo form_error('staff_name'); ?></div>
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Contact Number</label>
        <div class="col-md-10">
        <input type="text" value="<?=set_value('staff_contact_number');?>" class="form-control required_validation_for_vandor" name="staff_contact_number" placeholder="Staff Number">
         <div class="errorMessage"><?php echo form_error('staff_contact_number'); ?></div>
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Department</label>
        <div class="col-md-10">
        <select class="form-control required_validation_for_vandor" name="staff_department">
        <option value="">Choose Department</option>
        <option value="Account">Account</option>
        <option value="DeliveryBoy">Delivery Boy</option>
        <option value="DataEntry">Data Entry</option>
        <option value="Other">Other</option>
        </select>
        </div>
        </div>
        <!-- <div class="form-group">
        <label class="col-md-2 control-label">Status</label>
        <div class="col-md-10">
        <select class="form-control required_validation_for_vandor" name="staff_status">
        <option value="">Choose Status</option>
        <option value="1">Active</option>
        <option value="0">Deactive</option>
        </select>
        </div>
        </div> -->

         


         <div class="form-group">
        <label class="col-md-2 control-label">Email</label>
        <div class="col-md-10">
        <input type="text" value="<?=set_value('staff_email');?>" class="form-control required_validation_for_vandor" name="staff_email" placeholder="Staff Email">
         <div class="errorMessage"><?php echo form_error('staff_email'); ?></div>
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Alternate Number</label>
        <div class="col-md-10">
        <input type="text" value="<?=set_value('staff_alternat_contact');?>" class="form-control" name="staff_alternat_contact" placeholder="Alternate Number">
         <div class="errorMessage"><?php echo form_error('staff_alternat_contact'); ?></div>
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Address</label>
        <div class="col-md-10">
        <textarea class="form-control required_validation_for_vandor" placeholder="Address type here..." name="staff_address"><?=set_value('staff_address');?></textarea>
         <div class="errorMessage"><?php echo form_error('staff_address'); ?></div>
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Password sent on</label>
        <div class="col-md-10">
        <input type="radio" value="email" class="" checked="" name="password_send">&nbsp;Through email<br/>
          <input type="radio" value="sms" class="" name="password_send">&nbsp;Through sms
        </div>
        </div>

        </div>
       
        </div>
         
        </div>
        </div>
        </div>
        <div class="row m-t-20">
          <div class="col-md-12">
            <div class="card-box row">
            <h4 class="page-title m-b-15">Access</h4>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1" name="dashboard" />&nbsp;<span style="color:#000;font-weight:bold;">Dashboard</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1"  name="order" />&nbsp;<span style="color:#000;font-weight:bold;">Order</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1"  name="cataogry" />&nbsp;<span style="color:#000;font-weight:bold;">Cataogry</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox"  value="1" name="product"/>&nbsp;<span style="color:#000;font-weight:bold;">Product</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1"  name="setting" />&nbsp;<span style="color:#000;font-weight:bold;">Setting</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox"  value="1" name="staff_members" />&nbsp;<span style="color:#000;font-weight:bold;">Staff Members</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox"  value="1" name="inventory" />&nbsp;<span style="color:#000;font-weight:bold;">Inventory</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1" name="customer_management" />&nbsp;<span style="color:#000;font-weight:bold;">Customer Management</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1"  name="delivery" />&nbsp;<span style="color:#000;font-weight:bold;">Delivery</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1"  name="discount" />&nbsp;<span style="color:#000;font-weight:bold;">Discount</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1"  name="supplier_management" />&nbsp;<span style="color:#000;font-weight:bold;">Supplier Management</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1"  name="templates" />&nbsp;<span style="color:#000;font-weight:bold;">Templates</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1" name="purchase" />&nbsp;<span style="color:#000;font-weight:bold;">Purchase</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox"  value="1" name="coupons" />&nbsp;<span style="color:#000;font-weight:bold;">Coupons</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1"  name="gift_voucher" />&nbsp;<span style="color:#000;font-weight:bold;">Gift Voucher</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox"  value="1" name="calendar" />&nbsp;<span style="color:#000;font-weight:bold;">Calendar</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox"  value="1" name="POS" />&nbsp;<span style="color:#000;font-weight:bold;">POS</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox"  value="1" name="store_setting" />&nbsp;<span style="color:#000;font-weight:bold;">Store Setting</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox"  value="1" name="tax" />&nbsp;<span style="color:#000;font-weight:bold;">Tax</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox"  value="1" name="shipping" />&nbsp;<span style="color:#000;font-weight:bold;">Shipping</span>
            </div>
            <div class="col-md-3 m-b-10">
              <input type="checkbox" value="1"  name="campaign" />&nbsp;<span style="color:#000;font-weight:bold;">Campaign</span>
            </div>

             <div class="col-md-12 ">
        <input type="submit" class="btn btn-default pull-right m-r-10" name="customSearchDriver" value="Submit">
        <button type="reset" class="btn btn-info pull-right m-r-10">Reset</button>
        </div>
            </div>
          </div>
        </div>

           <?php echo form_close(); ?>
 
</div>
</div>
</div>
<?php $this->load->view('Template/footer.php') ?>
<script>
 $(document).ready(function(){
        $(".select2").select2();

        });
        </script>
<script type="text/javascript">  
$(document).ready(function(){
     

 $('#staff_add').submit(function(e){ 

var check_required_field='';
      $(this).find(".required_validation_for_vandor").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {
        e.preventDefault(); 
        return false;
      }else{      
       return true;

      }  
    });  


 }); 

</script>

