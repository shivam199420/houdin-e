<?php
class Packagemodel extends CI_Model
{
    function __construct()
    {  
        parent::__construct();
    } 
    // insert plugin 
    public function insertPluginData($data)
    {
        $getDateValue = strtotime(date('Y-m-d h:i:s'));
        $insertArrayData = array('houdin_plugin_name'=>$data['pluginName'],'houdin_plugin_active_status'=>'deactive','houdin_plugin_created_at'=>$getDateValue,'houdin_plugin_description'=>$data['description']);
        $insertData = $this->db->insert('houdin_plugins',$insertArrayData);
        $getLastInsertDataId = $this->db->insert_id();
        if($getLastInsertDataId)
        {
            return array('message'=>'success','id'=>$getLastInsertDataId);
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // update plugin image data
    public function updatePluginImage($data)
    {
        $updateArrayData = array('houdin_plugin_image'=>$data['imageData']);
        $this->db->where('houdin_plugin_id',$data['pluginId']);
        $getUpdateStatusData = $this->db->update('houdin_plugins',$updateArrayData);
        if($getUpdateStatusData)
        {
            return array('message'=>'success');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // count plugin data
    public function getTotalCount()
    {//->where('houdin_plugin_active_status','active')
        $this->db->select('count(houdin_plugin_id) AS countData')->from('houdin_plugins');
        $getPluginCount = $this->db->get()->result();  
        return array('totalRows'=>$getPluginCount[0]->countData);
    }
    // get plugin data value
    public function getPluginDataValue($data)
    {
        $this->db->select('*')->from('houdin_plugins');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getPlugindata = $this->db->get();
        $resultPluginData = ($getPlugindata->num_rows() > 0)?$getPlugindata->result():FALSE;
        return array('pluginList'=>$resultPluginData);
    }
    // delete plugin data
    public function deletePlugindataValue($data)
    {
        $this->db->where('houdin_plugin_id',$data);
        $getDeleteStatus = $this->db->delete('houdin_plugins');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'success');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    
    
    public function setPluginPriceValue($data,$id)
    {
      $this->db->where('houdin_plugin_id',$id);
        $getUpdateValue = $this->db->update('houdin_plugins',$data);   
              if($getUpdateValue)
        {
            return array('message'=>'success');
        }
        else
        {
            return array('message'=>'no');
        }  
    }
    
    

    
    // update plugin data
    public function updatePluginData($data)
    {
        $setDateData = strtotime(date('Y-m-d h:i:s'));
        $setArrayData = array('houdin_plugin_name'=>$data['pluginName'],'houdin_plugin_description'=>$data['description'],'houdin_plugin_active_status'=>$data['status'],'houdin_plugin_updated_at'=>$setDateData);
        $this->db->where('houdin_plugin_id',$data['pluginId']);
        $getUpdateValue = $this->db->update('houdin_plugins',$setArrayData);
        if($getUpdateValue)
        {
            return array('message'=>'success');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // fetch package page data
    public function fetchPackageData()
    {
        // fetch plugin list
        $this->db->select('*')->from('houdin_plugins')->where('houdin_plugin_active_status','active');
        $getPluginData = $this->db->get()->result();
        // get package list
        return array('pluginList'=>$getPluginData);
    }
    // insert package data
    public function setPackageData($data)
    {       
        // insert package data
        $getInsertStatus = $this->db->insert('houdin_packages',$data);
        $getPackageId = $this->db->insert_id();
        if($getInsertStatus == 1)
        {
            // // insert package plugin
            // $insertPluginArray = array('houdin_package_plugin_plu_id'=>$data['pluginId'],'houdin_package_plugin_pkg_id'=>$getPackageId,'houdin_package_plugin_created_at'=>$setDateData);
            // $getInsertData = $this->db->insert('houdin_package_plugins',$insertPluginArray);
            return array('message'=>'success','id'=>$getPackageId);
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // count total package
    public function getPackageTotalCount()
    {
        $this->db->select('COUNT(houdin_package_id) AS packageIdData')->from('houdin_packages')->where('houdin_package_status','active');
        $getPackageData = $this->db->get()->result();
        return array('totalRows'=>$getPackageData[0]->packageIdData);
    }
    // fetch package page data
    public function getPackageDataValue($data)
    {
        $this->db->select('houdin_packages.*')->from('houdin_packages')->where('houdin_packages.houdin_package_status','active');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getPackagedata = $this->db->get();
        $resultPackageData = ($getPackagedata->num_rows() > 0)?$getPackagedata->result():FALSE;
        return array('packageLsit'=>$resultPackageData);
    }
    // delete package 
    public function deletePackageData($data)
    {
        $this->db->where('houdin_package_id',$data);
        $getPackageDeleteStatus = $this->db->delete('houdin_packages');
        if($getPackageDeleteStatus == 1)
        {
            $this->db->where('houdin_package_plugin_pkg_id',$data);
            $getPackagePluginDeleteStatus = $this->db->delete('houdin_package_plugins');
            if($getPackagePluginDeleteStatus == 1)
            {
                return array('message'=>'success');
            }
            else
            {
                return array('message'=>'no');    
            }
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // edit package data
    public function getEditPackgeList($data)
    {
        $this->db->select('*')->from('houdin_plugins')->where('houdin_plugin_active_status','active');
        $getPluginData = $this->db->get()->result();
        // get package info 
        $this->db->select('houdin_packages.*,houdin_package_plugins.houdin_package_plugin_plu_id')->from('houdin_packages')->where('houdin_packages.houdin_package_id',$data)->join('houdin_package_plugins','houdin_package_plugins.houdin_package_plugin_pkg_id = houdin_packages.houdin_package_id','left outer');
        $getPackageInfo = $this->db->get()->result();
        return array('pluginListData'=>$getPluginData,'packageDataInfo'=>$getPackageInfo);
    }
    
    
    
    public function setPackagePriceValue($data,$id)
    {
       // print_R($data);
      $this->db->where('houdin_package_id',$id);
        $getUpdateValue = $this->db->update('houdin_packages',$data);   
              if($getUpdateValue)
        {
            return array('message'=>'success');
        }
        else
        {
            return array('message'=>'no');
        }  
    }
    
    public function updatePackageDetailsData($data,$id)
    {
        $this->db->where('houdin_package_id',$id);
        $getUpdateData = $this->db->update('houdin_packages',$data);
        if($getUpdateData == 1)
        {
            return array('message'=>'success','id'=>$id);
            // $setUpdatePackagePluginData = array('houdin_package_plugin_plu_id'=>$data['pluginid'],'houdin_package_plugin_updated_at'=>$getdateData);
            // $this->db->where('houdin_package_plugin_pkg_id',$data['packageIdData']);
            // $getUpdateValue = $this->db->update('houdin_package_plugins',$setUpdatePackagePluginData);
            // if($getUpdateValue == 1)
            // {
                
            // }
            // else
            // {
            //     return array('message'=>'no');
            // }
        }
        else
        {
            return array('message'=>'no');
        }
    }
}
