<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Transactionmodel extends CI_Model{
    function __construct() 
    {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
    }
    public function getTransactionTotalCount()
    {
        $getTransactionData = $this->db->select('COUNT(houdinv_transaction_id) as totalTransaction')->from('houdinv_transaction')->get()->result();
        return array('totalRows'=>$getTransactionData[0]->totalTransaction);
    }
    public function fetchTransactiondata($data)
    {
        $setDateData = "";
        $getTotalTransaction = "";
        $getTotalCredit = "";
        $getTotalDebit = "";
        for($index = 0; $index < 11; $index++)
        {
            $getDateData = date('Y-m-d', strtotime('-'.$index.' days'));
            $getDateDataValue = date('d-m-Y',strtotime($getDateData));
            // get total transaction
            $getTotalAmount = $this->db->select('SUM(houdinv_transaction_amount) as totalTransaction')->from('houdinv_transaction')
            ->where('houdinv_transaction_status','success')->where('houdinv_transaction_date',$getDateData)->get()->result();
            if($getTotalAmount[0]->totalTransaction)
            {
                $setTotalAmount = $getTotalAmount[0]->totalTransaction;
            }
            else
            {
                $setTotalAmount = 0;
            }
            // get total credit transaction
            $getTotalCreditAmount = $this->db->select('SUM(houdinv_transaction_amount) as totalCreditTransaction')->from('houdinv_transaction')
            ->where('houdinv_transaction_status','success')->where('houdinv_transaction_type','credit')
            ->where('houdinv_transaction_date',$getDateData)->get()->result();
            if($getTotalCreditAmount[0]->totalCreditTransaction)
            {
                $setTotalCredit = $getTotalCreditAmount[0]->totalCreditTransaction;
            }
            else
            {
                $setTotalCredit = 0;
            }
            // get total debit transaction
            $getTotalDebitAmount = $this->db->select('SUM(houdinv_transaction_amount) as totalDebitTransaction')->from('houdinv_transaction')
            ->where('houdinv_transaction_status','success')->where('houdinv_transaction_type','debit')
            ->where('houdinv_transaction_date',$getDateData)->get()->result();
            if($getTotalDebitAmount[0]->totalDebitTransaction)
            {
                $setTotalDebit = $getTotalDebitAmount[0]->totalDebitTransaction;
            }
            else
            {
                $setTotalDebit = 0;
            }

            if($setDateData)
            {
                $setDateData = $setDateData.","."'".$getDateDataValue."'";
            }
            else
            {
                $setDateData = "'".$getDateDataValue."'";
            }
            // get total transaction
            if($getTotalTransaction)
            {
                $getTotalTransaction = $getTotalTransaction.",".$setTotalAmount;
            }
            else
            {
                $getTotalTransaction = $setTotalAmount;
            }
            // get total credit
            if($getTotalCredit)
            {
                $getTotalCredit = $getTotalCredit.",".$setTotalCredit;
            }
            else
            {
                $getTotalCredit = $setTotalCredit;
            }
            // get total debit
            if($getTotalDebit)
            {
                $getTotalDebit = $getTotalDebit.",".$setTotalDebit;
            }
            else
            {
                $getTotalDebit = $setTotalDebit;
            }
        }
        // get transaction data
        $this->db->select('*')->from('houdinv_transaction');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getTransactiondata = $this->db->get();
        $resultTransactionData = ($getTransactiondata->num_rows() > 0)?$getTransactiondata->result():FALSE;
        return array('transactionDate'=>$setDateData,'totalAmount'=>$getTotalTransaction,'totalCreditAmount'=>$getTotalCredit,'totalDebitAmount'=>$getTotalDebit,'transactionData'=>$resultTransactionData);
    }
    public function fetchTransactionPdfData()
    {
        $getTransactiondata = $this->db->select('*')->from('houdinv_transaction')->get()->result();
        return $getTransactiondata;
    }
}