        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">
        <!-- Page-Title -->
        <div class="row">
        <div class="col-sm-12 m-b-15">
        <div class="btn-group pull-right m-t-15">
        <button type="button" class="btn btn-default btn-sm m-l-5" data-toggle="modal" data-target="#addSMSPackageData"> <i class="fa fa-plus m-r-5"></i>Add SMS Package</button>
        </div>
        <h4 class="page-title">SMS Package</h4>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        else if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        ?>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="card-box table-responsive">
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th>Package Name </th>
        <th>Package Price</th>
        <th>Total SMS</th>
        <th>Status</th>
        <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        foreach($packageList as $packageListData)
        {
        ?>    
        <tr>
        <td><?php echo $packageListData->houdin_sms_package_name ?></td>
        <td><?php echo $packageListData->houdin_sms_package_price ?></td>
        <td><?php echo $packageListData->houdin_sms_package_sms_count ?></td>
        <td><?php if($packageListData->houdin_sms_package_status == 'active') { $setText = 'Active'; $setClass="success"; } 
        else if($packageListData->houdin_sms_package_status == 'deactive') { $setText = 'Deactive'; $setClass="warning"; }
        ?>
        <button type="button" class="btn btn-xs btn-<?php echo $setClass; ?>"><?php echo $setText; ?></button>
        </td>
        <td>
        <button type="button" data-status="<?php echo $packageListData->houdin_sms_package_status ?>" data-count="<?php echo $packageListData->houdin_sms_package_sms_count ?>" data-price="<?php echo $packageListData->houdin_sms_package_price ?>" data-name="<?php echo $packageListData->houdin_sms_package_name ?>" data-id="<?php echo $packageListData->houdin_sms_package_id ?>" class="btn btn-warning btn btn-sm editSMSPackageBtn"><i class="fa fa-pencil m-r-5"></i>Edit</button>
        <button data-id="<?php echo $packageListData->houdin_sms_package_id ?>" type="button" class="btn btn-warning btn btn-sm deleteSMSPackageBtn"><i class="fa fa-trash m-r-5"></i>Delete</button>
        </td>
        </tr>
        <?php }
        ?>
        </tbody>
        </table>
        </div>
        </div>
        </div>


        </div> <!-- container -->

        </div> <!-- content -->

        <div id="addSMSPackageData" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Package/smspackage' ), array( 'id' => 'addSMSPackageForm', 'class' => 'form-horizontal' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title">Add SMS Package</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Package Name</label>
        <input type="text" name="smsPackageName" class="form-control required_validation_for_add_sms name_validation" maxlength="50" placeholder="Package Name" />
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Package price</label>
        <input type="text" name="smsPackagePrice" class="form-control required_validation_for_add_sms name_validation number_validation" placeholder="Package price" />
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Total sms contain</label>
        <input type="text" name="smsPackagecount" class="form-control required_validation_for_add_sms name_validation number_validation" placeholder="Total sms contain" />
        </div>
        </div>
        </div>

        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Status</label>
        <select class="form-control required_validation_for_add_sms" name="smsPackageStatus">
        <option value="">Choose Status</option>
        <option value="active">Active</option>
        <option value="deactive">Deactive</option>
        </select>
        </div>
        </div>
        </div>
        </div>

        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="addSmsPackage" value="Submit">
        </div>

        </div>
        <?php echo form_close(); ?>
        </div>
        </div>

        <div id="editSMSPackageModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Package/smspackage' ), array( 'id' => 'editSMSPackageForm'));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title">Edit SMS Package</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <input type="hidden" class="editSMSPackageId" name="editSMSPackageId"/>
        <label for="field-7" class="control-label">Package Name</label>
        <input type="text" name="editSMSPackageName" class="form-control required_validation_for_add_sms name_validation editSMSPackageName" placeholder="Package Name" />
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Package price</label>
        <input type="text" name="editSMSPackagePrice" class="form-control required_validation_for_add_sms name_validation number_validation editSMSPackagePrice" placeholder="Package price" />
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Total sms contain</label>
        <input type="text" name="editSMSPackageCount" class="form-control required_validation_for_add_sms name_validation number_validation editSMSPackageCount" placeholder="Total sms contain" />
        </div>
        </div>
        </div>

        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Status</label>
        <select class="form-control required_validation_for_add_sms editSMSPackageStatus" name="editSMSPackageStatus">
        <option value="">Choose Status</option>
        <option value="active">Active</option>
        <option value="deactive">Deactive</option>
        </select>
        </div>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="editSMSPackage" value="Submit">
        </div>

        </div>
        </form>
        </div>
        </div>


        <div id="deleteSMSPackageModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Package/smspackage' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title">Delete Entry</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleSMSPackageId" name="deleSMSPackageId"/>
        <h4><b>Do you really want to delete this entry ?</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="deleteSMSpackage" value="Delete">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        <?php $this->load->view('Template/footer.php') ?>
        <!-- CLient side form validation -->
        <script type="text/javascript">
            $(document).ready(function(){
                $(document).on('submit','#addSMSPackageForm,#editSMSPackageForm',function(){
                    var check_required_field='';
                    $(this).find(".required_validation_for_add_sms").each(function(){
                        var val22 = $(this).val();
                        if (!val22){
                            check_required_field =$(this).size();
                            $(this).css("border-color","#ccc");
                            $(this).css("border-color","red");
                        }
                        $(this).on('keypress change',function(){
                            $(this).css("border-color","#ccc");
                        });
                    });
                    if(check_required_field)
                    {
                        return false;
                    }
                    else {
                        return true;
                    }
                });
            });
            </script>