<?php  $this->load->view('Template/header');?> 

<header class="header bg-gray header-page-top">
  <div class="header-page-top-inner">
	  <div class="container">
		<h1 class="display-4">Your Account Setup</h1> 
  </div>
	</div>
</header>

		 
	   <section class="section  bg-img-bottom" >
        <div class="container text-center">
		<div class="col-md-8 mx-auto">	
			<ul class="register_active_step_box">
				<li><a class="register_active_step_box_btn"><span>1</span>OTP Verification</a></li>
				<li><a class="register_active_step_box_btn current_step_active"><span>2</span> Basic Information</a></li>
				 
			</ul>
			</div>
			<div class="clearfix"></div>
           <?php 
           if($this->session->flashdata('error'))
           {
             echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
           }
           if($this->session->flashdata('success'))
           {
             echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
           }
           ?>
      <?php echo form_open(base_url( 'Register/complete' ), array( 'id' => 'finalStep', 'method'=>'post'));?>
			  <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
				  <div class="input-group">
                	<input type="text" name="storename" class="form-control required_validation_for_final_step name_validation" placeholder="Your Store Name">
					  <div class="input-group-prepend"><span class="input-group-text" style="background-color: #eee;color:  #000;padding:  0px 15px;">.houdine.com</span></div>					  
				  </div>
              </div> 
            </div>
            <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
                <input type="email" name="email" class="form-control required_validation_for_final_step name_validation email_validation" placeholder="Email">
              </div> 
            </div>
            <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
              <textarea class="form-control" name="address" rows="4" class="form-control required_validation_for_final_step name_validation" placeholder="Your Address"></textarea>
              </div> 
            </div>
            <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
                <input type="text" name="city" class="form-control required_validation_for_final_step name_validation" placeholder="City">
              </div> 
            </div>
            <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
              <select name="country" class="form-control required_validation_for_final_step" >
					  <option value="" selected>Choose Country</option>
            <?php 
            foreach($countryList as $countryListData)
            {
            ?>  
            <option value="<?php echo $countryListData->houdin_country_id ?>"><?php echo $countryListData->houdin_country_name ?></option>
            <?php }
            ?>
				  </select>
              </div> 
            </div>
			  
			  
			  <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
                <input type="password" name="password" class="form-control required_validation_for_final_step name_validation opass" placeholder="Password">
              </div> 
            </div>
            <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
                <input type="password" name="confirmPassword" class="form-control required_validation_for_final_step name_validation cpass" placeholder="Retype Password">
              </div> 
            </div>
			  
			  <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
              <select name="sell" class="form-control required_validation_for_final_step" >
					  <option value="" selected>Are you already selling?</option>
					  <option value="yes">Yes</option>
					  <option value="no">No</option>				  
				  </select>
              </div> 
            </div>
			  <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
				  <select name="businessCategory" class="form-control required_validation_for_final_step" >
					  <option value="" selected>Choose Website Theme</option>
            <?php 
            foreach($businessCategory as $businessCategoryData)
            {
            ?>  
            <option value="<?php echo $businessCategoryData->houdin_business_category_id ?>"><?php echo $businessCategoryData->houdin_business_category_name ?></option>
            <?php }
            ?>
				  </select>
              </div> 
            </div>
            <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
				  <select name="appbusinessCategory" class="form-control required_validation_for_final_step" >
					  <option value="" selected>Choose App Business Category</option>
            <?php 
            foreach($app_business_categories as $app_business_category)
            {
            ?>  
            <option value="<?php echo $app_business_category->id ?>"><?php echo $app_business_category->name ?></option>
            <?php }
            ?>
				  </select>
              </div> 
            </div>
            <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
				  <select name="businessLanguage" class="form-control required_validation_for_final_step" >
					  <option value="" selected>Choose Business Language</option>
            <?php 
            foreach($langugae as $langugaedata)
            {
            ?>  
            <option value="<?php echo $langugaedata->houdin_language_id ?>"><?php echo $langugaedata->houdin_language_name_value ?></option>
            <?php }
            ?>
				  </select>
              </div> 
            </div>
            <div class="form-row">
              <div class="col-md-6 mx-auto mb-6">
				  <select name="currency" class="form-control required_validation_for_final_step" >
					  <option value="" selected>Choose Business Currency</option>
            <option value="INR">INR</option>
            <option value="USD">USD</option>
            <option value="AUD">AUD</option>
            <option value="EURO">EURO</option>
            <option value="POUND">POUND</option>
				  </select>
              </div> 
            </div>
          <div class="col-md-6 mx-auto text-center">
          <input type="submit" class="btn btn-lg btn-primary btn-block btn-round setStartBtn" name="startYourShop" value="Now Start"/>         
          </div>
          <?php echo form_close(); ?>

           <br/>


        </div>
      </section>
<?php  $this->load->view('Template/footer');?>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#finalStep',function(){
			var check_required_field='';
			$(".required_validation_for_final_step").each(function(){
				var val22 = $(this).val();
				if (!val22){
          // check_required_field =$(this).size();
          check_required_field =$(this).length;
					$(this).css("border-color","#ccc");
					$(this).css("border","1px solid red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
         return true;
			}
		});
	});
	</script>