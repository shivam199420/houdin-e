<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
             <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Membership Type</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li class="active">Membership Type</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div>
                   -->
            </div>
           <!--END Page-Title --> 

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                  <div class="btn-group pull-right m-t-10 m-b-20">
                     <button type="button" class="btn btn-default m-r-5" data-toggle="modal" data-target="#add"><i class="fa fa-plus"></i>&nbsp;Add</button>
                   </div>

                </div>
            </div>
<?php
                                    
            if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
   // print_r($all_data);
    ?>
        <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">
               <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th><input type="checkbox"></th>
                    <th>Membership Type</th>
                   
                   <th>Status</th>
                     <th>Action</th>
                    
                  </tr>
         
                </thead>
                <tbody>
                  <tr>
                    <td><input type="checkbox"></td>
                    <td>Preminum</td>
                     
                    <td>Remaining</td>
                    <td>
                    <button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#edit">Edit</button>
           
                    <button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#delete_order">Delete</button>
                   
                    
                   
                    </td>
            </tr>
                 
                </tbody>
              </table>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!--change Status-->

        <div id="add_catagory" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                <div class="modal-dialog">
                
                <?php echo form_open(base_url().'Category/categorymanagement',array("id"=>'categoryForm',"enctype"=>"multipart/form-data"))?>
                <div class="modal-content">

                <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h4 class="modal-title">Add Catagory</h4>

                </div>

                <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                <div class="form-group no-margin">
                <label for="field-7" class="control-label">Catagory Name</label>
                <input type="text" name="category_name" class="form-control required_validation_for_category" placeholder="Catagory Name" />
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                <div class="form-group no-margin">
                <label for="field-7" class="control-label">Catagory Thunmnail</label>
                <input type="file" name="category_thumb" class="form-control required_validation_for_category" />
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                <div class="form-group no-margin">
                <label for="field-7" class="control-label">Catagory Description</label>
              <textarea class="form-control required_validation_for_category" name="category_desc" rows="4"></textarea>
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                <div class="form-group no-margin">
                <label for="field-7" class="control-label">Catagory Status</label>
                <select class="form-control required_validation_for_category" name="category_status">
                  <option value="">Choose Status</option>
                  <option value="active">Active</option>
                  <option value="deactive">Dective</option>
                  <option value="block">Block</option>
                </select>
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                <div class="form-group no-margin">
                <button type="button" class="btn btn-info" id="add-sub" ><i class="fa fa-plus"></i>&nbsp;Create Subcatagory</button>

                </div>
                </div>
                </div>

                
                <div class="add-new"></div>
                </div>

                <div class="modal-footer">

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
 
                <input type="submit" class="btn btn-info " name="add_Form" value="Submit">

                </div>

                </div>
               <?php echo form_close(); ?>
                </div>

                </div>


<!--Delete-->

  <div id="delete_order" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
          <form method="post">
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title"></h4>

          </div>

          <div class="modal-body">

          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this membership ?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Delete">

          </div>

          </div>
          </form>
          </div>

          </div>

 <!-- End Delete -->
 
 <!--Start Edit-->

  <div id="edit" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
          <form method="post">
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title"></h4>

          </div>

          <div class="modal-body">

           <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label">Membership Type</label> 
                                                                <select class="form-control">
                                                                    <option>Preminum</option>
                                                                    <option>Silver</option>
                                                                    <option>Gold</option>
                                                                   
                                                                </select> 
                                                            </div> 
                                                        </div> 
                                                         
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label">Status</label> 
                                                                <select class="form-control">
                                                                    <option>Pending</option>
                                                                    <option>paid</option>
                                                                    
                                                                   
                                                                </select> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Submit">

          </div>

          </div>
          </form>
          </div>

          </div>

  <!--End Edit -->                 

    <!--Start Add-->

  <div id="add" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
          <form method="post">
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title"></h4>

          </div>

          <div class="modal-body">

           <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label">Membership Type</label> 
                                                                <select class="form-control">
                                                                    <option>Preminum</option>
                                                                    <option>Silver</option>
                                                                    <option>Gold</option>
                                                                   
                                                                </select> 
                                                            </div> 
                                                        </div> 
                                                         
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label">Status</label> 
                                                                <select class="form-control">
                                                                    <option>Pending</option>
                                                                    <option>paid</option>
                                                                    
                                                                   
                                                                </select> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Delete">

          </div>

          </div>
          </form>
          </div>

          </div>

  <!--End Add -->                 




<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">
$(document).ready(function(){

jQuery("#add-sub").click(function () { 
   // alert('Add Experince');
   var htm ='<div class="row removeRow">'
                htm+='<div class="col-md-12">';
                htm+='<div class="form-group no-margin">';
                htm+='<label for="field-7" class="control-label">Sub Catagory Name</label>';
                htm+='<input type="text" name="sub[]" class="form-control required_validation_for_category" placeholder="Sub Catagory Name" />';
                htm+='<span><i class="fa fa-remove removebutton" style="font-size:25px;color:red;float:right"></i></span></div>';
                htm+='</div>';
                htm+='</div>';
  jQuery(".add-new").
  append(htm);
});


jQuery("#edit-sub").click(function () { 
   // alert('Add Experince');
   var htm ='<div class="row removeRow">'
                htm+='<div class="col-md-12">';
                htm+='<div class="form-group no-margin">';
                htm+='<label for="field-7" class="control-label">Sub Catagory Name</label>';
                htm+='<input type="text" name="sub[]" class="form-control required_validation_for_category" placeholder="Sub Catagory Name" />';
                htm+='<span><i class="fa fa-remove removebutton" style="font-size:25px;color:red;float:right"></i></span></div>';
                htm+='</div>';
                htm+='</div>';
  jQuery(".edit-new").
  append(htm);
});


 });
 
 
 $(document).on("click",'.removebutton',function()
 {
    //alert("ffd");
    $(this).parents('.removeRow').remove();
    
 });
</script>

        <script type="text/javascript">
    /*==============Form Validation===========================*/
        $(document).ready(function(){

        $(document).on('submit','#Edit_form,#categoryForm',function(c){
            
                               var rep_image_val='';
                 $(this).find(".required_validation_for_category").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                        
                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
                
                $(this).find('.required_validation_for_category').on('keyup blur',function()
                                {
                                    
                                        $(this).css("border-color","#ccc");
                                });
                                
                            if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                else
                {
                }  
            
          });
          
          
          // category edit detail fill 
          $(document).on('click','.edit_popup',function()
          {
            $('.required_validation_for_category').css("border-color","#ccc");
                        $('#Edit_form')[0].reset();
                        jQuery(".edit-new").
  empty(''); 
            console.log($(this).children('#edit_all_data'));
            
            $('#edit_name').val($(this).siblings('#edit_all_data').attr("name"));
             $('#edit_id').val($(this).siblings('#edit_all_data').attr("data-id"));
            
               $('#edit_status').val($(this).siblings('#edit_all_data').attr("data-status"));
                $('#edit_desc').val($(this).siblings('#edit_all_data').attr("data-des"));
                
                
                            var ids = $(this).siblings('#edit_all_data').attr('data-sub');
                          //  console.log(ids);

            // $('.checkboxClass').find('.input_price').prop("disabled",true);
            var htm='';
            if(ids)
            {
          $.each($.parseJSON(ids), function(idx,obj) {
//console.log(obj);
                htm+='<div class="row removeRow">'
                htm+='<div class="col-md-12">';
                htm+='<div class="form-group no-margin">';
                htm+='<label for="field-7" class="control-label">Sub Catagory Name</label>';
                htm+='<input type="text" value="'+obj+'" name="sub[]" class="form-control required_validation_for_category" placeholder="Sub Catagory Name" />';
                htm+='<span><i class="fa fa-remove removebutton" style="font-size:25px;color:red;float:right"></i></span></div>';
                htm+='</div>';
                htm+='</div>';
     }); 

        }   
                
            jQuery(".edit-new").
  append(htm);     
 
          });
          
                    // category delete detail fill 
          $(document).on('click','.delete_popup',function()
          {
            console.log($(this).parents('#edit_all_data'));
            $('#delete_input').val($(this).siblings('#edit_all_data').attr("data-id"));
           
          });
          
        });
            </script>