<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Storesettingmodel extends CI_Model{
function __construct() {
    parent::__construct();
    $this->load->helper('database');
    $getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
}

    public function insert_logo($data){
        $getLogoData = $this->db->select('id')->from('houdinv_shop_logo')->get()->result();
        if(!count($getLogoData) > 0)
        {
            $getInsertData = $this->db->insert('houdinv_shop_logo',$data);   
            if($getInsertData == 1)
            {
            return ture;
            }
            else
            {
            return false;
            }
        }
        else
        {
            $this->db->where('id',$getLogoData[0]->id);
            $getInsertData = $this->db->update('houdinv_shop_logo',$data);   
            if($getInsertData == 1)
            {
            return ture;
            }
            else
            {
            return false;
            }
        }
    


    }

    public function fetchstorelogoRecord($type){
    $this->db->select('*')->from('houdinv_shop_logo');
    if($type=='favicon'){$this->db->or_where('clear_favicon',0);}else{ $this->db->or_where('clear',0);} 
    $getSearchData = $this->db->get()->result();
    return $getSearchData;

    }

    public function save_policy($data){
    
        $getStorePolicies = $this->db->select('*')->from('houdinv_storepolicies')->get()->result();
        if(count($getStorePolicies) > 0)
        {
            $this->db->where('id',$getStorePolicies[0]->id);
            $getInsertData = $this->db->update('houdinv_storepolicies',$data);   
            if($getInsertData == 1)
            {
            return ture;
            }
            else
            {
            return false;
            }
        }
        else
        {
            $getInsertData = $this->db->insert('houdinv_storepolicies',$data);   
            if($getInsertData == 1)
            {
            return ture;
            }
            else
            {
            return false;
            }
        }
    }

    public function fetchstorePolicyRecord(){
    $this->db->select('*')->from('houdinv_storepolicies'); 
    $getstorepolicies = $this->db->get()->result();
    return $getstorepolicies;
    }

    public function save_sociallink($data){


        $this->db->select('*')->from('houdinv_social_links'); 
        $getSearchsocial = $this->db->get()->result();
  if($getSearchsocial){

    $getInsertData = $this->db->update('houdinv_social_links',$data);   
    if($getInsertData == 1)
    {
    return true;
    }
    else
    {
    return false;
    }

  }else{
    $getInsertData =$this->db->insert('houdinv_social_links',$data);      
    if($getInsertData == 1)
    {
    return true;
    }
    else
    {
    return false;
    }
}
    }


    public function fetchstoreSocialRecord(){
    $this->db->select('*')->from('houdinv_social_links'); 
    $getSearchsocial = $this->db->get()->result();
    return $getSearchsocial;

    }

    public function save_testimonials($data){

    $getInsertData = $this->db->insert('houdinv_testimonials',$data);   
    if($getInsertData == 1)
    {
    return ture;
    }
    else
    {
    return false;
    }}

    public function fetchstoreTestimonialsRecord(){
    $this->db->select('*')->from('houdinv_testimonials'); 
    $gettestimonials = $this->db->get()->result();
    return $gettestimonials;

    }

    public function delete_testimonials($id)
    {
    $this->db->where('id', $id);
    $delete_data= $this->db->delete('houdinv_testimonials');
    if($delete_data == 1)
    {
    return ture;
    }
    else
    {
    return false;
    }
    }

    public function edit_testimonials($data){
    $this->db->where('id', $data['id']);
    $getInsertData = $this->db->update('houdinv_testimonials',$data);   
    if($getInsertData == 1)
    {
    return ture;
    }
    else
    {
    return false;
    }}
    public function fetchNavigationData()
    {
        $getCategorydata = $this->db->select('houdinv_category_name,houdinv_category_id')->from('houdinv_categories')->where('houdinv_category_status','active')->get()->result();
        // get custom navigation data
        $getNavigationdata = $this->db->select('*')->from('houdinv_navigation_store_pages')->get()->result();
        // get only custom data
        $getCustomNavigationdata = $this->db->select('*')->from('houdinv_navigation_store_pages')->where('houdinv_navigation_store_pages_category_id',0)->get()->result();
        // get dynamic category
        $getCategoryNavigationdata = $this->db->select('*')->from('houdinv_navigation_store_pages')->where('houdinv_navigation_store_pages_category_id != 0')->get()->result();
        return array('categoryData'=>$getCategorydata,'navigationdata'=>$getNavigationdata,'customNav'=>$getCustomNavigationdata,'categoryNav'=>$getCategoryNavigationdata);
    }
    public function addStoreNavigationPage($data)
    {
        $this->db->where('houdinv_navigation_store_pages_category_id',0);
        $this->db->delete('houdinv_navigation_store_pages');
        $setExplaode = explode(',',$data);
        for($index = 0; $index < count($setExplaode); $index++)
        {
            $setInsertArray = array('houdinv_navigation_store_pages_name'=>$setExplaode[$index]);
            $this->db->insert('houdinv_navigation_store_pages',$setInsertArray);
        }
        return array('message'=>'yes');
    }
    public function fetchMenuData()
    {
        $getHomeNavigationData = $this->db->select('*')->from('houdinv_navigation_store_pages')->get()->result();
        $setNavData = "";
        foreach($getHomeNavigationData as $getHomeNavigationDataList)
        {
            if($getHomeNavigationDataList->houdinv_navigation_store_pages_name == 'home')
            {
                $setNavigationName = 'Home';
            }
            else if($getHomeNavigationDataList->houdinv_navigation_store_pages_name == 'about')
            {
                $setNavigationName = 'About Us';
            }
            else if($getHomeNavigationDataList->houdinv_navigation_store_pages_name == 'contact')
            {
                $setNavigationName = 'Contact Us';
            }
            else if($getHomeNavigationDataList->houdinv_navigation_store_pages_name == 'privacy')
            {
                $setNavigationName = 'Privacy';
            }
            else if($getHomeNavigationDataList->houdinv_navigation_store_pages_name == 'disclaimer')
            {
                $setNavigationName = 'Disclaimer';
            }
            else if($getHomeNavigationDataList->houdinv_navigation_store_pages_name == 'terms')
            {
                $setNavigationName = 'Terms & Conditions';
            }
            else if($getHomeNavigationDataList->houdinv_navigation_store_pages_name == 'shipping')
            {
                $setNavigationName = 'Shipping and Delivery Policy';
            }
            else if($getHomeNavigationDataList->houdinv_navigation_store_pages_name == 'refund')
            {
                $setNavigationName = 'Cancellation and refund policy';
            }
            else if($getHomeNavigationDataList->houdinv_navigation_store_pages_category_id != 0)
            {
                $setNavigationName = $getHomeNavigationDataList->houdinv_navigation_store_pages_category_name;
            }
            else if($getHomeNavigationDataList->houdinv_navigation_store_pages_custom_link_id != 0)
            {
                $setNavigationName = $getHomeNavigationDataList->houdinv_navigation_store_pages_custom_link_name;
            }
            if($getHomeNavigationDataList->houdinv_navigation_store_pages_custom_link_id == 0)
            {
                if($setNavData)
                {
                    $setNavData = $setNavData."".'<li class="dd-item">
                    <div class="dd-handle">
                    '.$setNavigationName.' <span class="pull-right"><a href="javascript:;" data-id="'.$getHomeNavigationDataList->houdinv_navigation_store_pages_id.'" class="removeMenu btn btn-xs btn-danger"><i class="fa fa-remove"></i></a></span>
                    </div>
                    </li>';
                }
                else
                {
                    $setNavData = '<li class="dd-item">
                    <div class="dd-handle">
                    '.$setNavigationName.' <span class="pull-right"><a href="javascript:;" data-id="'.$getHomeNavigationDataList->houdinv_navigation_store_pages_id.'" class="removeMenu btn btn-xs btn-danger"><i class="fa fa-remove"></i></a></span>
                    </div>
                    </li>';
                }
            }
            else
            {
                if($setNavData)
                {
                    $setNavData = $setNavData."".'<li class="dd-item">
                    <div class="dd-handle">
                    '.$setNavigationName.' <span class="pull-right"><a href="javascript:;" data-id="'.$getHomeNavigationDataList->houdinv_navigation_store_pages_id.'" class="removeCustomLinkMenu btn btn-xs btn-danger"><i class="fa fa-remove"></i></a></span>
                    </div>
                    </li>';
                }
                else
                {
                    $setNavData = '<li class="dd-item">
                    <div class="dd-handle">
                    '.$setNavigationName.' <span class="pull-right"><a href="javascript:;" data-id="'.$getHomeNavigationDataList->houdinv_navigation_store_pages_id.'" class="removeCustomLinkMenu btn btn-xs btn-danger"><i class="fa fa-remove"></i></a></span>
                    </div>
                    </li>';
                }
            }
        }
        return $setNavData;
    }
    public function updateCustomHomeData($data)
    {
        $this->db->empty_table('houdinv_custom_home_data');
        $getInsertStatus = $this->db->insert('houdinv_custom_home_data',$data);
        if($getInsertStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function fetchCustomHomeData()
    {
        $getCustomHomeData = $this->db->select('*')->from('houdinv_custom_home_data')->get()->result();
        // get slider data
        $getSliderData = $this->db->select('*')->from('houdinv_custom_slider')->get()->result();
        return array('customList'=>$getCustomHomeData,'sliderlist'=>$getSliderData);
    }
    public function updateSliderImage($data)
    {
        $setData = strtotime(date('Y-m-d'));
        if($data['id'])
        {
            $setupdateArray = array('houdinv_custom_slider_image'=>$data['imageData'],'houdinv_custom_slider_head'=>$data['heading'],'houdinv_custom_slider_sub_heading'=>$data['subheading'],
            'houdinv_custom_slider_modified_at'=>$setData
            );
            $this->db->where('houdinv_custom_slider_id',$data['id']);
            $getUpdateData = $this->db->update('houdinv_custom_slider',$setupdateArray);
            if($getUpdateData == 1)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }
        }
        else
        {
            $setInsertArray = array('houdinv_custom_slider_image'=>$data['imageData'],'houdinv_custom_slider_head'=>$data['heading'],'houdinv_custom_slider_sub_heading'=>$data['subheading'],
            'houdinv_custom_slider_created_at'=>$setData
            );
            $getInsertStatus = $this->db->insert('houdinv_custom_slider',$setInsertArray);
            if($getInsertStatus == 1)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }
        }
    }
    public function deleteSliderImage($data)
    {
        $this->db->where('houdinv_custom_slider_id',$data);
        $getDeleteStatus = $this->db->delete('houdinv_custom_slider');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function deleteSliderNav($data)
    {
        $this->db->where('houdinv_navigation_store_pages_id',$data);
        $getDeleteStatus = $this->db->delete('houdinv_navigation_store_pages');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function addCategoryNav($data)
    {
        $this->db->where('houdinv_navigation_store_pages_category_id != 0');
        $this->db->delete('houdinv_navigation_store_pages');
        $setExplodeID = explode(',',$data['id']);
        $setExplodeName = explode(',',$data['name']);
        for($index = 0; $index < count($setExplodeID); $index++)
        {
            $setInsertArray = array("houdinv_navigation_store_pages_category_id"=>$setExplodeID[$index],'houdinv_navigation_store_pages_category_name'=>$setExplodeName[$index]);
            $this->db->insert('houdinv_navigation_store_pages',$setInsertArray);
        }
        return array('message'=>'yes');
    }
    public function addCategoeyData($data)
    {
        $setInsertArray = array("houdinv_navigation_store_pages_category_id"=>$data['id'],'houdinv_navigation_store_pages_category_name'=>$data['name']);
        $this->db->insert('houdinv_navigation_store_pages',$setInsertArray);
    }
    public function fetchThemeData()
    {
        $masterDB = $this->load->database('master',true);
        $getVedorId = $masterDB->select('houdin_vendor_auth_vendor_id')->from('houdin_vendor_auth')->get()->result();
        if(count($getVedorId) > 0)
        {
            $getTemplateInfo = $masterDB->select('houdin_user_shops.houdin_user_shops_category,houdin_business_categories.houdin_business_category_name,houdin_templates.houdin_template_thumbnail')->from('houdin_user_shops')->where('houdin_user_shop_user_id',$getVedorId[0]->houdin_vendor_auth_vendor_id)
            ->join('houdin_business_categories','houdin_business_categories.houdin_business_category_id = houdin_user_shops.houdin_user_shops_category','left outer')
            ->join('houdin_templates','houdin_templates.houdin_template_category = houdin_user_shops.houdin_user_shops_category','left outer')->get()->result();
            return array('templateInfo'=>$getTemplateInfo);
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function addcustomLinksData($data)
    {
        $this->db->insert('houdinv_custom_links',$data);
        $getLastInsertId = $this->db->insert_id();
        if($getLastInsertId != "")
        {
            $setinsertArray = array('houdinv_navigation_store_pages_custom_link_id'=>$getLastInsertId,'houdinv_navigation_store_pages_custom_link_name'=>$data['houdinv_custom_links_title']);
            $getInsertStatus = $this->db->insert('houdinv_navigation_store_pages',$setinsertArray);
            if($getInsertStatus == 1)
            {
                return array('messgae'=>'yes');    
            }
            else
            {
                return array('messgae'=>'no');    
            }
        }
        else
        {
            return array('messgae'=>'no');
        }
    }
    public function deleteCustomMenuNavigation($data)
    {
        $getCustomLinkData = $this->db->select('houdinv_navigation_store_pages_custom_link_id')->from('houdinv_navigation_store_pages')->where('houdinv_navigation_store_pages_id',$data)->get()->result();
        if(count($getCustomLinkData) > 0)
        {
            $this->db->where('houdinv_custom_links_id',$getCustomLinkData[0]->houdinv_navigation_store_pages_custom_link_id);
            $this->db->delete('houdinv_custom_links');
            $this->db->where('houdinv_navigation_store_pages_id',$data);
            $getDeleteStatus = $this->db->delete('houdinv_navigation_store_pages');
            if($getDeleteStatus == 1)
            {
                return array('messgae'=>'yes');
            }
            else
            {
                return array('messgae'=>'no');    
            }
        }
        else
        {
            return array('messgae'=>'no');
        }
    }
}
