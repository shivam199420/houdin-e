<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ajaxcontroller extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopproductlistmodel');   
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }     
    }
    public function fetchProductPaginationData()
    {
        $settingData = stroeSetting($this->session->userdata('shopName'));
        $getLast = $this->input->post('dataLast');
        $getRemain = $this->input->post('dataRemain');
        $getCategoryId = $this->input->post('getCategory');
        $getSubCategoyrId = $this->input->post('getSubCategory');
        $getSubSubCategoyrId = $this->input->post('getSubSubCategory');
        $sortBy = $this->input->post('sortBy');
        $setArrayData = array('categoryId'=>$getCategoryId,'subcategory'=>$getSubCategoyrId,'subsubcategory'=>$getSubSubCategoyrId,'lastid'=>$getLast,'getRemain'=>$getSubSubCategoyrId,'sortBy'=>$sortBy);
        $getProductList = $this->Shopproductlistmodel->fetchAjaxProduct($setArrayData);
        // get cart setting
        $getTotalFetchedProduct = count($getProductList);
        $setLastData = $getTotalFetchedProduct+$getLast;
        $getSetting = $this->Shopproductlistmodel->fethcCartSetting();
        $setHtmlData = "";
        foreach($getProductList as $productListData)
        {
            $setCartDesign = "";
            $setStockBadge = "";
            if($productListData->houdinv_products_total_stocks <= 0)
            { 
                if($settingData[0]->receieve_order_out == 1)
                {
                    $setCartDesign = '<a class="Add_to_cart_button" data-variant="0" data-cart="'.$productListData->houdin_products_id.'"><i class="fa fa-cart-plus"></i></a>';
                }
            }
            else
            {
                $setCartDesign = '<a class="Add_to_cart_button" data-variant="0" data-cart="'.$productListData->houdin_products_id.'"><i class="fa fa-cart-plus"></i></a>';
            }
            if($productListData->houdinv_products_total_stocks <= 0)
            {
                $setStockBadge = '<div class="new_badge">Out of Stock</div>';
            }
            $getProductImage = json_decode($productListData->houdinv_products_main_images,true);
            $getProductPrice = json_decode($productListData->houdin_products_price,true);
            if($getProductPrice['discount'] != 0 && $getProductPrice['discount']  != "")
            {                
                $DiscountedPrice = $getProductPrice['price']-$getProductPrice['discount'];
                $originalPrice = $getProductPrice['price'];
            }
            else
            {
                $DiscountedPrice = 0;
                $originalPrice = $getProductPrice['price'];
            }
            if($DiscountedPrice == 0)
            {
                $setPrice = $originalPrice;
            }
            else
            {
                $setPrice = "<strike>".$originalPrice."</strike>&nbsp;".$DiscountedPrice."";
            }
            if($setHtmlData)
            {
                $setHtmlData = $setHtmlData."".'
                <div class="col-md-3 col-sm-6">
            <div class="single_product">
            <div class="product_image">
            <img src="'.$this->session->userdata('vendorURL').'upload/productImage/'.$getProductImage[0].'" alt="" style="width: 250px!important; height: 300px!important;"/>
            '.$setStockBadge.'
            <div class="box-content">
            <a href="javascript:;" class="Add_to_whishlist_button" data-cart="'.$productListData->houdin_products_id.'" ><i class="fa fa-heart-o"></i></a>
             
            '.$setCartDesign.'
             
                 
            
            <a href="'.base_url().'Productlistview/'.$productListData->houdin_products_id.'"><i class="fa fa-search"></i></a>
            </div>										
            </div>

            <div class="product_btm_text">
            <h4><a href="'.base_url().'Productlistview/'.$productListData->houdin_products_id.'">'.substr($productListData->houdin_products_title,0, 25)."..".'</a></h4>
            <span class="price">&#8377;&nbsp;'.$setPrice.'</span>
            </div>
            </div>								
            </div>
                ';
            }
            else
            {
                $setHtmlData = '
                <div class="col-md-3 col-sm-6">
            <div class="single_product">
            <div class="product_image">
            <img src="'.$this->session->userdata('vendorURL').'upload/productImage/'.$getProductImage[0].'" alt="" style="width: 250px!important; height: 300px!important;"/>
            '.$setStockBadge.'
            <div class="box-content">
            <a href="javascript:;" class="Add_to_whishlist_button" data-cart="'.$productListData->houdin_products_id.'" ><i class="fa fa-heart-o"></i></a>
             
                '.$setCartDesign.'
            
            <a href="'.base_url().'Productlistview/'.$productListData->houdin_products_id.'"><i class="fa fa-search"></i></a>
            </div>										
            </div>

            <div class="product_btm_text">
            <h4><a href="'.base_url().'Productlistview/'.$productListData->houdin_products_id.'">'.substr($productListData->houdin_products_title,0, 25)."..".'</a></h4>
            <span class="price">&#8377;&nbsp;'.$setPrice.'</span>
            </div>
            </div>								
            </div>
                ';
            }
    }
    $setArray = array('main'=>$getProductList,'last'=>$setLastData,"settingData"=>$settingData);
    echo json_encode($setArray);
}
}
