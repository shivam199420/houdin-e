<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

           
              <!--START Page-Title -->
            <div class="row">
        
                <div class="col-md-8">
                   <h4 class="page-title">Discount Detail</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>discount/Discountlanding">Discount</a></li>
                  <li class="active">Discount Detail</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div>
                   -->
            </div>
           <!--END Page-Title -->
           
                                
        <div class="row m-t-20">
          <div class="col-md-12">

            <div class="card-box table-responsive">
            
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th><input type="checkbox"></th>
                    <th>Discount applied by</th>
                    <th>Discount applied on</th>
                    <th>Date</th>
                

                  </tr>
                </thead>
                
                <tbody>
                  <tr>
                    <td><input type="checkbox"></td>
                    <td>DD/MM/YYYY</td>
                    <td>DD/MM/YYYY</td>
                    <td>DD/MM/YYYY</td>
                 
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>


<!--Add Discount-->
<div id="adddiscount" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <form method="post">
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

        <h4 class="modal-title">Add Discount</h4>

        </div>

        <div class="modal-body">



        <div class="row form-horizontal">
        <div class="col-md-12">
 
       <div class="form-group">
        <label class="col-md-4 control-label">Product type</label>
        <div class="col-md-8">
        <select class="form-control">
            <option>Single product</option>
            <option>Product category</option>
            <option>Product type</option>
            <option>Customer group</option>
        </select>
        </div>
        </div>
         
           
        <div class="form-group">
                      <label class="col-md-4 control-label" for="userName">Choose product</label>
                     <div class="col-md-8">
                      <select class="form-control select2">

                                            <option>Select</option>
                                            <optgroup label="Alaskan/Hawaiian Time Zone">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                            </optgroup>
                                            <optgroup label="Pacific Time Zone">
                                                <option value="CA">California</option>
                                                <option value="NV">Nevada</option>
                                                <option value="OR">Oregon</option>
                                                <option value="WA">Washington</option>
                                            </optgroup>
                                            <optgroup label="Mountain Time Zone">
                                                <option value="AZ">Arizona</option>
                                                <option value="CO">Colorado</option>
                                                <option value="ID">Idaho</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="UT">Utah</option>
                                                <option value="WY">Wyoming</option>
                                            </optgroup>
                                            <optgroup label="Central Time Zone">
                                                <option value="AL">Alabama</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TX">Texas</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="WI">Wisconsin</option>
                                            </optgroup>
                                            <optgroup label="Eastern Time Zone">
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="IN">Indiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="OH">Ohio</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WV">West Virginia</option>
                                            </optgroup>
                                        </select>
                                        </div>
                    </div>
                    <div class="form-group">
        <label class="col-md-4 control-label">Discount name</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control" name="" placeholder="Discount name">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Discount percentage</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control" name="" placeholder="Discount percentage">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Discount started at</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control" name="" placeholder="Discount started at">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Discount end at</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control" name="" placeholder="Discount end at">
        </div>
        </div>
         <div class="form-group">
        <label class="col-md-4 control-label">Choose status</label>
        <div class="col-md-8">
        <select class="form-control">
            <option>Active</option>
            <option>Deactive</option>
            
        </select>
        </div>
        </div>

        </div>
        </div>





        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Submit">

        </div>

        </div>
        </form>
        </div>

        </div>

<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".select2").select2();
  $(".click_show").click(function(){ 
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
