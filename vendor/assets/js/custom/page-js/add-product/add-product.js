// select 2 js
$(".select2").select2();
let TODAY_DATE = $('#today-date').val();
let BASE_URL = $('#base-url').val();
let WAREOUSE_LIST = $('#warehouse-list').val();
// when user choose product type the this action perform
 $(document).on('change','#type_change',function(c){
    jQuery(".Add_attribut").empty(''); 
    var ids = $(this).val();
    var option_array =  $(this).find('option[value="'+ids+'"]').attr("data-sub");
    var htm='';
    var attr_is=0;
    $.each($.parseJSON(option_array), function(idx,obj) {
        htm+='<label class="col-md-3">'+obj+'</label>';
        htm+='<input type="hidden" class="form-control" value="'+obj+'" name="Attr['+attr_is+'][main]" placeholder="Enter value"/>';
        htm+='<input type="text" class="form-control " name="Attr['+attr_is+'][value]" placeholder="Enter value"/>';
        attr_is++;
    }); 
    $('.Add_attribut').append(htm);
});
// end product type

// add  warehouse quantity
$(document).on('change','.ware_house',function(c){
    jQuery(".Add_cost_quantity").empty(''); 
    var $this = $(this);         
    var idsss = $(this).val(); 
    var htm='';
    var attr_new=0;         
    $.each(idsss, function(idx,obj) {
        var values =  $this.find('option[value="'+obj+'"]').attr("data-name");
        htm+='<label class="col-md-8">'+values+' outlet quantity </label>';
        htm+='<input type="hidden" class="form-control" value="'+obj+'" name="quanti['+attr_new+'][main]" />';
        htm+='<label class="col-sm-9"><input type="text" onkeyup="checkINt(this);" class="form-control required_validation_for_product without_variant_data" name="quanti['+attr_new+'][value]" placeholder="Enter value"/></label><label class="col-sm-3"><input type="text" value="'+TODAY_DATE+'" class="form-control required_validation_for_product date_picker without_variant_data" name="quanti['+attr_new+'][date]" placeholder="As of date"/></label>';
        attr_new++;
$(document).on('focus','.date_picker',function(){
    $(this).daterangepicker({                    
        singleDatePicker: true,
        showDropdowns: false,    
        locale: { 
          format: 'YYYY-MM-DD',
        }
    });      
});  
  }); 

//   append html
$('.Add_cost_quantity').append(htm);
}); 

// Barcode generator
$(document).on('click','.barcode_generate',function()
{
    var $that = $(this);
    var dataString = "flag=fetchmediaaudio";
    // ajax to generate barcode
    jQuery.ajax({
        type: "GET",
        url: BASE_URL+"Product/set_barcode",
        processData:false,
        cacheData:false,
        success: function(data) {
            $that.siblings('input').val(data);
            $that.siblings('.barcode_images').attr("src",""+BASE_URL+"images/barcode/"+data);
            $that.siblings('.message_text').text('');
        }
    });
});
// end bar code generator

// show image thumbnail 
$('.images_class').on('change',function()
{
    var file1 = $(this)[0];
    var file = file1.files[0];
    var reader  = new FileReader();
    reader.onloadend = function () {
        $(file1).parents('.images_preview_row').find('.images_preview').attr('src',reader.result);
    }
    if (file) {
        reader.readAsDataURL(file);
    } else {
        $(this).parents('.images_preview_row').find('.images_preview').attr('src','');
            
    }
});
// end image thumbnail
function previewFile(div) {
    var x = document.getElementById(div);
    var preview = x.querySelector('img');
    var file    = document.querySelector('input[type=file]').files[0];
    var reader  = new FileReader();
    reader.onloadend = function () {
    preview.src = reader.result;
}

if (file) {
    reader.readAsDataURL(file);
} else {
    preview.src = "";
}
}
$(".click_show").click(function(){
    $(".hide_div").show();
    $(".click_show").hide();
});

$(".click_hide").click(function(){
    $(".hide_div").hide();
    $(".click_show").show();
});
var op  = 1;       
$(document).on("click",".add_option",function()
{
    var htm ="";
    var getLength = $('.houdin_addvariants_box_row').length;
    htm +='<tr class="table_tr addvariants_row houdin_addvariants_box_row" data-num="'+getLength+'">' 
    +'<td><div class="input-group">'
    +'<input type="text" class="form-control save_option_name_text" placeholder="option name like color, size, size" max-length="100" required="">'
    +'<span class="input-group-btn">'
    +'<button style="margin:0px!important;" class="btn btn-success save_option_name" type="submit"><i class="fa fa-check"></i></button>'
    +'</span>'
    +'</div></td>'
    +'<td class="delete_option_append"><div class="input-group">'
    +'<input type="text" class="form-control option_val_text" placeholder="option values" max-length="100" required="">'
    +'<span class="input-group-btn">'
    +'<button style="margin:0px!important;" class="btn btn-success save_option_val" type="submit"><i class="fa fa-check"></i></button>'
    +'</span>'
    +'</div></td>' 
    +'<td class="remove_option" style="padding-left: 10px"><span class="input-group-btn">'
    +'<button style="margin:0px!important;" class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>'
    +'</span></td>'
    +'</tr>';
    $(".append_table").append(htm);
    op++;
    // if(op==3) {
    //     // $(".add_option").css("display","none"); 
    // }
});

// remove option data
$(document).on("click",".remove_option",function()
{
    $(this).parents(".table_tr").remove();
    op--;
    if(op!=3) {
        $(".add_option").css("display","block"); 
    }
    houdin_newaddvariants_box();
    setcombination();
});
var cusm = 0;
var indexing = "<?php echo $indexing; ?>";
var hudinVariantdata;
function houdin_newaddvariants_box(){
    hudinVariantdata=[];
    $('.houdin_addvariants_box .houdin_addvariants_box_row').each(function(){
        var $optiontitle = $(this).find('.save_option_name_text').attr('data-name');
        if($optiontitle){
            var added_option_value=[];
            $(this).find('.added_option_btn').each(function(){
                added_option_value.push($(this).attr('data-name'));
            });  
            // hudinVariantdata[$optiontitle]=added_option_value;                 
            var variantsOptionRow=[];
            variantsOptionRow[$optiontitle]=added_option_value;
            hudinVariantdata.push(variantsOptionRow);
        } else {
            alert('Option now available');
        }
    });
}

houdin_newaddvariants_box();
// save option value
$(document).on("click",".save_option_val",function()
{   
    var optionRowTitle = $(this).parents('.houdin_addvariants_box_row').find('.save_option_name_text').attr('data-name');
    var optionValue = $(this).parents('.houdin_addvariants_box_row').find('.option_val_text').val();
    var optionNum = $(this).parents('.houdin_addvariants_box_row').attr('data-num');
    if(optionRowTitle) {
        if(optionValue) { 
        var checkalreadymatchoptionvalue=true;
        if(hudinVariantdata.length>0){ 
            $.each(hudinVariantdata,function(count,optionrow){
                var currentRowData=optionrow[optionRowTitle]; 
                if(currentRowData){ 
                    $.each(currentRowData,function(index,obj){
                        if(obj==optionValue) {
                            checkalreadymatchoptionvalue=false;  
                        }
                    });
                } 
            });
        }
        if(checkalreadymatchoptionvalue) {                     
            alert('new option value insert');
            var options = '<div class="btn-group div_group added_option_btn" data-num="'+optionNum+'" data-name="'+optionValue+'">'
            +'<button type="button" class="btn btn-primary btn-sm"><span class="default-text">'+optionValue+'</span>&nbsp;&nbsp;<span class="remove_option_value"><i class="fa fa-trash"></i></span></button>';
            +'</div>';
            $(this).parents('.houdin_addvariants_box_row').find('.delete_option_append').append(options);
            // hide warehouse and barcode of main product when user add variants
            $('.variant_hide').hide();
        } else {
            alert('Already have same '+optionRowTitle+ ' option value');    
        }
        } else{
            alert("Please write option value ");
        }
    } else{
        alert("Please write option name  first ");
    }
    houdin_newaddvariants_box()
    calculatevariantData();

    // calculate variant
    function calculatevariantData(){                  
        $(".tr_class_var").each(function(){ 
            var checkval1 = $('.ware_house').val();
            var $that = $(this);
            var stock_var =[]; 
            var array_temp =[];  
            var already_Array  =[];
            var productwarehouse_data=[];
            $(".qty_val").each(function(index)
            {
                var label = $(this).find('label').text();
                var id_var =  $(this).find('input[type="hidden"]').val();
                var values_var =  $(this).find('input[type="number"]').val();  
                productwarehouse_data.push({'id':id_var,'val':values_var,'name':label}); 
            });
            if (productwarehouse_data.length>0) {
                if ($that.find('.stock_var_Class').val()) {
                    var productwarehouse_dataAlready = $that.find('.stock_var_Class').val();
                    productwarehouse_dataAlready = JSON.parse(productwarehouse_dataAlready);
                        if(productwarehouse_dataAlready.length>0){
                            $.each(productwarehouse_dataAlready, function(idx,obj) {
                                var alreadyid=obj.id;
                                var alreadyid_val=obj.val;
                                for (var i = 0; i < productwarehouse_data.length; i++) {                   
                                    if(productwarehouse_data[i].id==alreadyid){ 
                                        productwarehouse_data[i].val = alreadyid_val;
                                    }
                                }    
                            });
                            $that.find('.stock_var_Class').val(JSON.stringify(productwarehouse_data));
                        } else{
                            $that.find('.stock_var_Class').val(JSON.stringify(productwarehouse_data));
                        }
                } else{
                    $that.find('.stock_var_Class').val(JSON.stringify(productwarehouse_data));
                }
            } else{
                $that.find('.stock_var_Class').val('');
            }
        });
    } 
// end calculate variant 
$('#data').val(hudinVariantdata);
if(hudinVariantdata.length>0)
{
var vaiantsOptiontitle='';
var indexing=0;
var ind=0;
$.each(hudinVariantdata,function(index,data){
var titlecOption = Object.getOwnPropertyNames(data)[1];

vaiantsOptiontitle+='<th data-col="'+titlecOption+'">'+titlecOption+'</th>';
if(data.length)
{

}
});
$('.option_val_text').each(function(){

})
setcombination();
       
}
else{ }
});
// remove variants
$(document).on("click",".remove_option_value",function()
{
$(this).parents(".div_group").remove();
houdin_newaddvariants_box();
setcombination();
});
    
// variant option
$(document).on('change','.variant_option', function() {
$(".tr_class_var").find("input").prop( "readonly", false );
$(this).parents(".tr_class_var").find(".title_new").val($("#titles").val());
$(this).parents(".tr_class_var").find("input[type='text'],input[type='number']").prop( "readonly", true );
});
// save variant option name
$(document).on("click",".save_option_name",function()
{
var name1 = $(this).parents(".table_tr").find(".save_option_name_text").val();
if(!name1)
{
alert("please fill the name first");
}
else
{
var checkalreadymatchoptionvalue=true;
if(hudinVariantdata.length>0){ 
$.each(hudinVariantdata,function(count,optionrow){
  if(optionrow[name1]){
    checkalreadymatchoptionvalue=false;
  }
});
}
if(!checkalreadymatchoptionvalue){
alert('Already ' + name1 + ' option available');
}
else{               
$(this).parents(".table_tr").find(".save_option_name_text").attr('data-name',name1);
alert(name1 +" option name saved");
houdin_newaddvariants_box();
}
}
});
// delete variant
$(document).on("click",".delete-variant",function()
{
$(this).parents(".tr_class_var").remove();
$("#delet_id").val($(this).attr("data-id"));
$("#AddId").submit();
setcombination();
}); 
// create variant combination
function setcombination()
{
let warehouse_list = WAREOUSE_LIST;
var setParentArray = [];
var getArrayLength = $('.houdin_addvariants_box_row ').length
for(var index = 0; index < getArrayLength; index++)
{
var setChildArray = [];
$('.added_option_btn').each(function(){
if($(this).attr('data-num') == index)
{
  if($(this).attr('data-name'))
  {
    setChildArray.push($(this).attr('data-name'));
  }
}
});
setParentArray.push(setChildArray);
}
var getResultData = allPossibleCases(setParentArray);
var option_add = '<div class="card-box has_cusm" style="width: 100%;>' 
+'<h4 class="clearfix"><b class="pull-left">Other Variants</b>'
+'</h4>';
/* +'<table id="variants" class="table table-stripped">'
+'<thead class="variant_table_is">'
+'<tr class="tr_head">'
+'<th>Variants</th>'
+'<th>Title</th>'
+'<th>Qty.</th>'
+'<th>MRP</th>'
+'<th>CP</th>'
+'<th>Margin</th>'
+'<th>SP</th>'
+'<th>Barcode</th>'
+'<th>Image</th>'                           
+'<th>Warehouse</th>'
+'</tr>'
+'</thead>'
+'<tbody class="variant_tbody">'; */
for(var data = 0; data < getResultData.length; data++)
{
// new updated design according to client requirement
option_add += '<div class="card-box"><div class="form-group"><h4>'+getResultData[data]+'</h4><input type="hidden" name="variantData[]" value="'+getResultData[data]+'"/></div>'
+'<div class="col-sm-3"><div class="form-group"><label style="color: #666666 !important;font-weight: bold;">Title<input type="text" name="new_title[]" class="required_validation_for_product form-control"></label></div></div>'
+'<div class="col-sm-3"><div class="form-group"><label style="color: #666666 !important;font-weight: bold;">Qty.<input type="text" name="new_qty[]" class=" form-control"></label></div></div>'
+'<div class="col-sm-3"><div class="form-group"><label style="color: #666666 !important;font-weight: bold;">MRP<input type="text" name="new_mrp[]" class="required_validation_for_product form-control"></label></div></div>'
+'<div class="col-sm-3"><div class="form-group"><label style="color: #666666 !important;font-weight: bold;">CP<input type="text" name="new_cp[]" class="required_validation_for_product form-control"></label></div></div>'
+'<div class="col-sm-3"><div class="form-group"><label style="color: #666666 !important;font-weight: bold;">Margin<input type="text" name="new_margin[]" class=" form-control"></label></div></div>'
+'<div class="col-sm-3"><div class="form-group"><label style="color: #666666 !important;font-weight: bold;">SP<input type="text" name="new_sp[]" class="required_validation_for_product form-control"></label></div></div>'
+'<div class="col-sm-3"><div class="form-group"><label style="color: #666666 !important;font-weight: bold;">Barcode<input type="text" name="barcode_data[]" class=" form-control"><a  class="btn btn-default btn-xs m-r-5 barcode_generate"><i class="fa fa-barcode"></i>Generate Barcode</a><img class="barcode_images" src="" /><p style="color:red" class="message_text"></p></label></div></div>'
+'<div class="col-sm-3"><div class="form-group"><label style="color: #666666 !important;font-weight: bold;">Image<input type="file" name="new_image[]"/></label></div></div>'
+'<div class="clearfix"></div><div class="col-sm-3"><div class="form-group"><label style="color: #666666 !important;font-weight: bold;">Warehouse<select class="form-control required_validation_for_product ware_house  select2 select_input_requred" multiple="" name="product_variant_warehouse[]"><option value="">Choose Warehouse</option>'+warehouse_list+'</select></label></div></div>'
+'</div><div class="clearfix"></div>';
/* option_add +='<tr class="tr_class_var">'
+'<td>'+getResultData[data]+'<input type="hidden" name="variantData[]" value="'+getResultData[data]+'"/></td>'
+'<td><input  type="text" name="new_title[]" class="form-control " ></td>'
+'<td><input  type="text"  name="new_qty[]" class="form-control " ></td>'
+'<td><input  type="text"  name="new_mrp[]" class="form-control " ></td>'
+'<td><input  type="text"  name="new_cp[]" class="form-control " ></td>'
+'<td><input  type="text"  name="new_margin[]" class="form-control " ></td>'
+'<td><input  type="text"  name="new_sp[]" class="form-control " ></td>'
+'<td><input type="hidden"  name="barcode_data[]"  class="form-control barcode_input" placeholder="Enter Bar Code"/><a  class="btn btn-default btn-xs m-r-5 barcode_generate"><i class="fa fa-barcode"></i>Generate Barcode</a><img class="barcode_images" src="" /><p style="color:red" class="message_text"></p></td>'
+'<td><input type="file" name="new_image[]"/></td>'
+'<td><select class="form-control ware_house  select2 select_input_requred" multiple="" name="product_warehouse[]"><option value="">Choose Warehouse</option>'+warehouse_list+'</select></td>'
+'</tr>'; */
}
// option_add += '</div></tbody></table>';
option_add += '</div>';
$(".combinations").html('').html(option_add);
$(".select2").select2();  
if ($('.card-box.has_cusm .card-box').length <= 0) {
$(".combinations").html('');
$('.variant_hide').show();
}
}
function allPossibleCases(arr) {
if (arr.length == 1) {
return arr[0];
} else {
var result = [];
var allCasesOfRest = allPossibleCases(arr.slice(1));  // recur with the rest of array
for (var i = 0; i < allCasesOfRest.length; i++) {
for (var j = 0; j < arr[0].length; j++) {
  result.push(arr[0][j] + allCasesOfRest[i]);
}
}
return result;
}

}

// js for description box
$(document).on('click','.show_description',function (){
    $('.descption_box').removeClass('d-none');
    $(this).addClass('d-none');
    $('.hide_description').removeClass('d-none');
    $('.descption_box').addClass('d-none');
    CKEDITOR.replace( 'editor1' );
});
$(document).on('click','.hide_description',function (){
    $('.descption_box').removeClass('d-none');
    $(this).addClass('d-none');
    $('.show_description').removeClass('d-none');
    CKEDITOR.instances['editor1'].destroy();
    $('.descption_box').addClass('d-none');
});

// function to add image 
$(document).on('click','.add-more-image',function() {
    let image_length = $('.image_section').find('.images_preview_row').length;
    let image_name = image_length + 1;
    let set_html = '<div class="images_preview_row"><div class="col-md-8"><div class="form-group">'
    +'<lable>Image '+image_name+'</lable><input type="file" class="form-control images_class"   name="images['+image_length+']" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />'
    +'</div></div><div class="col-md-4" >'
    +'<img src="'+BASE_URL+'assets/images/users/avatar-1.jpg" class="thunbnail_image images_preview"/>'
    +'<span class="text-danger delete-product-image"><i class="fa fa-trash-o remove_product_image"></i></span>'
    +'</div></div>';
    $('.image_section').append(set_html);
})

// remove product image
$(document).on('click','.remove_product_image',function(){
    $(this).parents('.images_preview_row').remove();
});

// product modal
$(document).on('click','.product_modal',function(){
    if ($(this).is(':checked')){
        let checked_value = parseInt($(this).val());
        if (checked_value == 1) {
            $('.advance_modal').addClass('d-none');
        } else {
            $('.advance_modal').removeClass('d-none');
        }
    }
})
// ckeditor
// CKEDITOR.replace( 'editor1' );
CKEDITOR.replace( 'editor2' );