<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Faq extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopuserextrapages');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $getFaq = $this->Shopuserextrapages->fetchFaqData();
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/faq',$getFaq);
    }
     
}
