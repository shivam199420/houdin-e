<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>


<!-- About Banner Start -->
<section id="about">

  <div class="container">

    <div class="row">

      <div class="about-heading text-center">

        <h2>Shopping Cart</h2>

      </div>

    </div>

  </div>

</section>

<!-- About Banner End -->



<!-- Shopping Cart Start -->

<section id="card">

  <div class="container">
    <?php
    if(count($AllCart)<1)
    {
      ?>
      <div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h3 class="text-center">Your cart is empty</h3></div></div>
      <?php
    }
    else
    {
      ?>
      <div class="row">
        <?php
        if($this->session->flashdata('error'))
        {
          echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }

        if($this->session->flashdata('message_name'))
        {
          echo '<div class="alert alert-danger">'.implode("</br>",json_Decode($this->session->flashdata('message_name'))).'</div>';
        }
        if($this->session->flashdata('success'))
        {
          echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>

        <div class="col-md-9">

          <div class="cart-info">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered table-shop cart_prdct_table">
              <?php echo form_open(base_url( 'Cart/UpdateCart' ), array( 'id' => 'UpdateCart', 'method'=>'post' ));?>
              <tbody>

                <tr>

                  <th class="tl">Product & Color</th>

                  <th>Unit Price</th>

                  <th>Quantity</th>

                  <th>Total Price</th>

                  <th>Remove Item</th>

                </tr>

                <?php
                $i=1;
                $final_price =0;
                foreach($AllCart as $thisItem)
                {
                  if($thisItem['stock'] <= 0)
                  {
                    $setStockData = '(Out of stock)';
                  }
                  $count = $thisItem['count'];
                  $main_price = $thisItem['productPrice'];
                  $total_price = $main_price*$count;
                  $final_price =$final_price+$total_price;

                  ?>
                  <tr class="tr_row">

                    <td>

                      <div class="col-md-12 pl">

                        <div class="col-md-3 pl">

                          <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $thisItem['productImage']; ?>" alt="cart1" class="img-responsive" style="height: 80px;">

                        </div>

                        <div class="col-md-9 pro-info pl text-left">

                          <a href="<?php if($thisItem['productId'] != 0) { echo base_url()."Productlistview/".$thisItem['productId'];  } else { echo base_url()."Productlistview/variant/".$thisItem['productId']; }  ?>"><h3><?php echo $thisItem['productName']; ?></h3><span style="color:red"><?php echo $setStockData; ?></span></a>

                        </div>

                      </div>

                    </td>

                    <td class="product-price"><p class="cp_price"><?php echo $main_price; ?></p></td>

                    <td>

                      <div class="quantity">

                        <div class="handle-counter" id="handleCounter">

                          <div class=" cp_quntty">

                            <input name="product[<?php echo  $i-1; ?>][product_id]" value="<?php echo $thisItem['cartId']; ?>" size="2" type="hidden">
                            <input name="product[<?php echo  $i-1; ?>][quantity]" value="<?php echo $thisItem['count']; ?>" size="2" type="number">

                          </div>



                          <div class="clearfix"></div>

                        </div>

                      </div>

                    </td>

                    <td class="product-subtotal"><p class="cpp_total"><?php echo $total_price; ?></p></td>

                    <td class="product-remove"><a href="javascript:;" class="cp_cart_remove" data-replace="<?php echo $total_price; ?>" data-id="<?php echo $thisItem['cartId']; ?>" ><i class="fa fa-trash-o"></i></a></td>

                  </tr>
                  <?php $i++; } ?>
                </tbody>

              </table>
              </div>

            </div>

          </div>

          <div class="col-md-3">

            <div class="cart-total total-amount">

              <ul>

                <li><h3>Cart Total</h3></li>

                <li><span>Cart Subtotal</span><a href="#"><span id="mcart_subtotal_class" class="mcart_subtotal_class"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span></a></li>

                <li><span>Grand Total</span><a href="#"><span class="mcart_subtotal_class"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span></a></li>

              </ul>

            </div>

            <div class="proceed">

              <a href="javascript:;" class="update_cart">update cart</a> <a class="updat" href="<?php echo base_url() ?>Checkout">proceed</a>

            </div>

          </div>
          <?php echo form_close(); ?>
        </div>
      <?php } ?>

    </div>

  </section>

  <!-- Shopping Cart End -->

  <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>
