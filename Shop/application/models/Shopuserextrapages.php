<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopuserextrapages extends CI_Model{
    function __construct() 
    {
    }
    public function fetchAboutData()
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getAboutContent = $getDynamicWebDb->select('*')->from('houdinv_storepolicies')->get()->result();
        
        return array('about'=>$getAboutContent);
    }
    public function fetchTermsData()
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getTermsContent = $getDynamicWebDb->select('Terms_Conditions')->from('houdinv_storepolicies')->get()->result();
        return array('terms'=>$getTermsContent);
    }
    public function fetchprivacyData()
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getPrivacyContent = $getDynamicWebDb->select('Privacy_Policy')->from('houdinv_storepolicies')->get()->result();
        return array('privacy'=>$getPrivacyContent);
    }
    public function fetchFaqData()
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);
        
        $getFaqContent = $getDynamicWebDb->select('faq')->from('houdinv_storepolicies')->get()->result();
        return array('Faq'=>$getFaqContent);
    }
    public function fetchDisclaimerData()
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);
        
        $getFaqContent = $getDynamicWebDb->select('Disclaimer_Policy')->from('houdinv_storepolicies')->get()->result();
        return array('disclaimer'=>$getFaqContent);
    }
    public function fetchShippingData()
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);
        
        $getShippingContent = $getDynamicWebDb->select('Shipping_Delivery_Policy')->from('houdinv_storepolicies')->get()->result();
        return array('shipping'=>$getShippingContent);
    }
    public function fetchRefundData()
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);
        
        $getRefundContent = $getDynamicWebDb->select('Cancellation_Refund_Policy')->from('houdinv_storepolicies')->get()->result();
        return array('refund'=>$getRefundContent);
    }
} 
?>