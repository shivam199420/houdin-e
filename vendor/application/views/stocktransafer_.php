        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <style>
        .modal-ku {
  width: 750px;
  margin: auto;
}
        </style>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">


        <!--START Page-Title -->
        <div class="row">
  
        <div class="col-md-8">
        <h4 class="page-title">Stock Transfer</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>inventory/Inventorylanding">Inventory</a></li>
        <li class="active">Stock Transfer</li>
        </ol>
        </div>
        <!-- <div class="col-md-4">
        <form role="search" class="navbar-left app-search pull-left custom_search_all">
        <input type="text" placeholder="Search..." class="form-control">
        <a href=""><i class="fa fa-search"></i></a>
        </form></div> -->

        </div>
        <!--END Page-Title -->
        <div class="row m-t-20">
        <div class="col-md-12">
        <?php 
        if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        <div class="card-box table-responsive">

        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th>Product Id</th>
        <th>Product Name</th>
        <th>Action</th>
        </tr>

        </thead>
        <tbody>
        <?php 
        foreach($productList as $productListData)
        {
        ?>
        <tr>
        <td><?php echo $productListData->houdin_products_id; ?></td>
        <td><a href="<?php echo base_url(); ?>Product/editproduct/<?php echo $productListData->houdin_products_id; ?>" target="_blank"><?php echo $productListData->houdin_products_title; ?></a></td>
        <td><button class="btn btn-success waves-effect waves-light stockTransferBtn" data-id="<?php echo $productListData->houdin_products_id; ?>">Transfer</button></td>
        </tr>
        <?php }
        ?>
        </tbody>
        </table>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div id="stockTransferModal" class="modal fade modal-ku" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <div class="modal-content"> 
        <?php echo form_open(base_url( 'Inventory/sendStockTransferData' ), array( 'id' => 'stockTransferForm', 'method'=>'post' ));?>
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Transfer</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group setMessageData">

        </div>
        <div class="form-group col-md-6">
        <label for="userName"> From</label>
        <select class="form-control required_validation_for_stock_transfer select2 setProductId" name="warehouseFrom">
        <option value="">Choose warehouse</option>
        <?php
        foreach($warehouselist as $warehouselistData)
        {
        ?>
        <option value="<?php echo $warehouselistData->id ?>"><?php echo $warehouselistData->w_name ?></option>
        <?php }
        ?>
        </select>
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">To</label>
        <select class="form-control  required_validation_for_stock_transfer select2 setWarehouseTo" name="warehouseTo" disabled="disabled">
        <option value="">Choose warehouse</option>
        <?php
        foreach($warehouselist as $warehouselistData)
        {
        ?>
        <option value="<?php echo $warehouselistData->id ?>"><?php echo $warehouselistData->w_name ?></option>
        <?php }
        ?>
        </select>
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Invoice no.</label>
        <input type="text" class="form-control required_validation_for_stock_transfer" name="invoiceno">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Date:</label>
        <input type="date" class="form-control required_validation_for_stock_transfer" name="date">
        </div> 
        <div class="form-group">
        <table class="table">
        <thead>
        <tr>
        <th><input type="checkbox" class="masterStockCheck"/></th>
        <th>Product Name</th>
        <th>Quantity</th>
        </tr>
        </thead>
        <tbody class="setStockData">
         
        </tbody>
        </table>
        <p class="h4 mb-4">Delivered by details:</p>

        <div class="form-group col-md-6">
        <label for="userName">Name</label>
        <input type="text" class="form-control required_validation_for_stock_transfer" name="Delivered_name">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Date:</label>
        <input type="date" class="form-control required_validation_for_stock_transfer" name="Delivered_date">
        </div> 

        <div class="form-group col-md-6">
        <label for="userName">Driver Name</label>
        <input type="text" class="form-control required_validation_for_stock_transfer" name="Delivered_drivername">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Vehicle Number</label>
        <input type="text" class="form-control required_validation_for_stock_transfer" name="Delivered_vehiclenumber">
        </div>
        <p class="h4 mb-4">Received by details:</p>

        <div class="form-group col-md-6">
        <label for="userName">Name</label>
        <input type="text" class="form-control required_validation_for_stock_transfer" name="Received_name">
        </div> 
        <div class="form-group col-md-6">
        <label for="userName">Date:</label>
        <input type="date" class="form-control required_validation_for_stock_transfer" name="Received_date">
        </div> 

        </div> 
        </div> 

        </div> 

        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <button type="submit" class="btn btn-info waves-effect waves-light transferData" disabled="disabled">Transfer</button> 
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- /.modal -->
        <?php $this->load->view('Template/footer.php') ?>
        <script type="text/javascript">
        $(document).on('submit','#stockTransferForm',function(e){
            var setValidateCounter = 0;
            $('.childStockCheck').each(function(){
                if($(this).prop('checked') == true)
                {
                    var getQuantity = $(this).parents('.mainParentRow').find('.setQunatityData').val();
                    if(getQuantity == "")
                    {
                        setValidateCounter++;
                    }
                }
            });
            if(setValidateCounter == 0)
            {
                var check_required_field='';
                $(".required_validation_for_stock_transfer").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            }
            else
            {
                $('.setMessageData').html('');
                $('.setMessageData').append('<div class="alert alert-danger">Please fill all the selected product quantity</div>');
                return false;
            }
        });
        </script>

