<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Userwishlistmodel extends CI_Model{
    function __construct() 
    {
    }
    public function updateWishlistData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);
        // get cart count
        
        $setdata = strtotime(date('Y-m-d h:i:s'));
        if($data['product'] != 0 && $data['product'] != "")
        {
            $getProductPrice = $getDynamicDB->select('houdin_products_final_price')->from('houdinv_products')->where('houdin_products_id',$data['product'])->get()->result();
            $setFinalPrice = $getProductPrice[0]->houdin_products_final_price;
        }
        else
        {
            $getVariantPrice = $getDynamicDB->select('houdinv_products_variants_final_price,houdin_products_variants_product_id')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data['variant'])->get()->result();
            $setFinalPrice = $getVariantPrice[0]->houdinv_products_variants_final_price;
            
        }
        // check product is already exist or not
        $getWishlistData = $getDynamicDB->select('houdinv_users_whishlist_id')->from('houdinv_users_whishlist')->where('houdinv_users_whishlist_item_id',$data['product'])->where('houdinv_users_whishlist_item_variant_id',$data['variant'])->get()->result();
        if(count($getWishlistData) > 0)
        {
            $setInsertArray = array('houdinv_users_whishlist_user_id'=>$data['userId'],'houdinv_users_whishlist_item_id'=>$data['product'],'houdinv_users_whishlist_item_variant_id'=>$data['variant'],'houdinv_users_whishlist_final_price'=>$setFinalPrice,'houdinv_users_whishlist_update_date'=>$setdata);
            $getDynamicDB->where('houdinv_users_whishlist_id',$getWishlistData[0]->houdinv_users_whishlist_id);
            $getUpdateData = $getDynamicDB->update('houdinv_users_whishlist',$setInsertArray);
            if($getUpdateData == 1)
            {
                // if product move from cart to wishlist
                if($data['cartId'] != 0 && $data['cartId'] != "")
                {
                    $getDynamicDB->where('houdinv_users_cart_id',$data['cartId']);
                    $getDynamicDB->delete('houdinv_users_cart');
                    $getCartCount = $getDynamicDB->select('count(houdinv_users_cart_id) as cartCount')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$data['userId'])->get()->result();
                }
                if($getCartCount[0]->cartCount)
                {
                    return array('message'=>'Success','cartCount'=>$getCartCount[0]->cartCount);
                }
                else
                {
                    return array('message'=>'Success');
                }
            }
            else
            {
                return array('message'=>'Something went wrong. Please try again');
            }
        }
        else
        {
            $setInsertArray = array('houdinv_users_whishlist_user_id'=>$data['userId'],'houdinv_users_whishlist_item_id'=>$data['product'],'houdinv_users_whishlist_item_variant_id'=>$data['variant'],'houdinv_users_whishlist_final_price'=>$setFinalPrice,'houdinv_users_whishlist_add_date'=>$setdata);
            $getInsertData = $getDynamicDB->insert('houdinv_users_whishlist',$setInsertArray);
            if($getInsertData == 1)
            {
                // if product move from cart to wishlist
                if($data['cartId'] != 0 && $data['cartId'] != "")
                {
                    $getDynamicDB->where('houdinv_users_cart_id',$data['cartId']);
                    $getDynamicDB->delete('houdinv_users_cart');
                    $getCartCount = $getDynamicDB->select('count(houdinv_users_cart_id) as cartCount')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$data['userId'])->get()->result();
                }
                if($getCartCount[0]->cartCount)
                {
                    return array('message'=>'Success','cartCount'=>$getCartCount[0]->cartCount);
                }
                else
                {
                    return array('message'=>'Success');
                }
            }
            else
            {
                
                return array('message'=>'Something went wrong. Please try again');
            }
        }
    }
} 

?>