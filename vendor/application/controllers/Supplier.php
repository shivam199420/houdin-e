<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Supplier extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->auth_users->is_logged_in();
        $this->load->library('Pdf');
        $this->load->library('form_validation');
        $this->load->model('Suppliermodel');
        $this->load->library('pagination');
        $this->perPage = 100;
	}
    public function index()
    {
        $getSmsEmail = smsemailcredentials();
        if($this->session->userdata('vendorAuth') == "")
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // delete supplier data
        if($this->input->post('deleteSuppplier'))
        {
            $this->form_validation->set_rules('deleteSupplierId','text','trim|required|integer');
            if($this->form_validation->run() == true)
            {
                $getSupplierDeleteStatus = $this->Suppliermodel->deleteSupplierData($this->input->post('deleteSupplierId'));
                if($getSupplierDeleteStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Supplier deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Supplier", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Supplier", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Supplier", 'refresh');
            }
        }
        // update single supplier status
        if($this->input->post('updateSingleSupplierStatus'))
        {
            $this->form_validation->set_rules('changeStatusSupplierId','text','required|trim|integer');
            $this->form_validation->set_rules('supplierStatus','text','required|trim|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setStatusArray = array('id'=>$this->input->post('changeStatusSupplierId'),'status'=>$this->input->post('supplierStatus'));
                $getUpdateStatus = $this->Suppliermodel->updateSupplierStatus($setStatusArray);
                if($getUpdateStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Status updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Supplier", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Supplier", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Supplier", 'refresh');
            }
        }
        // send email data
        if($this->input->post('sendSupplierEmail'))
        {
            $this->form_validation->set_rules('supplierEmailData','text','required|trim|valid_email');
            $this->form_validation->set_rules('emailSubject','text','required|trim');
            $this->form_validation->set_rules('emailMessage','text','required|trim');
            if($this->form_validation->run() == true)
            {
                // put validation
                $getRemainingEmail = getRemainingEmail();
                if($getRemainingEmail['remaining'] > 0)
                {
                    $getInfo = fetchvendorinfo();
                    // send email to admin user
                    $url = 'https://api.sendgrid.com/';
                    $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                    $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                    $json_string = array(
                    'to' => array(
                    $this->input->post('supplierEmailData')
                    ),
                    'category' => 'test_category'
                    );
                    $htm = $this->input->post('emailMessage');
                    $params = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'x-smtpapi' => json_encode($json_string),
                        'to'        => $this->input->post('supplierEmailData'),
                        'fromname'  => $getInfo['name'],
                        'subject'   => $this->input->post('emailSubject'),
                        'html'      => $htm,
                        'from'      => $getInfo['email'],
                    );
                    $request =  $url.'api/mail.send.json';
                    $session = curl_init($request);
                    curl_setopt ($session, CURLOPT_POST, true);
                    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                    curl_setopt($session, CURLOPT_HEADER, false);
                    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($session);
                    curl_close($session);
                    $getResponse = json_decode($response);
                    if($getResponse->message == 'success')
                    {
                        updateSuceessEmailCount($getRemainingEmail['remaining']-1);
                        $this->session->set_flashdata('success','Email send successsfully');
                        $this->load->helper('url');
                        redirect(base_url()."Supplier", 'refresh');
                    }
                    else
                    {
                        updateErrorEmailLog();
                        $this->session->set_flashdata('error','Something went wrong. Please try again');
                        $this->load->helper('url');
                        redirect(base_url()."Supplier", 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','Please upgrade your email package');
                    $this->load->helper('url');
                    redirect(base_url()."Supplier", 'refresh');
                }
                
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Supplier", 'refresh');
            }
        }
        //update multiple status data
        if($this->input->post('updateMultipleStatusData'))
        {
            $this->form_validation->set_rules('changeStatusMultipleSupplierId','text','required');
            $this->form_validation->set_rules('multipleSupplierStatus','text','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setUpdateArray = array('id'=>$this->input->post('changeStatusMultipleSupplierId'),'status'=>$this->input->post('multipleSupplierStatus'));
                $updateMultipleStatus = $this->Suppliermodel->setMultipleStatus($setUpdateArray);
                if($updateMultipleStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Status updated successsfully');
                    $this->load->helper('url');
                    redirect(base_url()."Supplier", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Supplier", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Supplier", 'refresh');
            }
        }
        // delete multiple supplier
        if($this->input->post('deleteMultipleSuppplier'))
        {
            $this->form_validation->set_rules('deleteMultipleSupplierId','text','required');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->Suppliermodel->deleteMultipleSupplier($this->input->post('deleteMultipleSupplierId'));
                if($getDeleteStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Suppliers deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Supplier", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Supplier", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Supplier", 'refresh');
            }
        }
        // Custom Pagination
        $getTotalCount = $this->Suppliermodel->getSupplierTotalCount();
        $config['base_url']    = base_url().'supplier/';
        $config['uri_segment'] = 2;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(2);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getSupplierDetails = $this->Suppliermodel->getSupplierDetailsData($setArray);
        $this->load->view('supplier',$getSupplierDetails);
    }
    public function addsupplier()
    {
        if($this->session->userdata('vendorAuth') == "")
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        if($this->input->post('addSupplierBtn'))
        {
            $this->form_validation->set_rules('supplierCompanyName','text','trim|required');
            $this->form_validation->set_rules('supplierContactPerson','text','trim|required');
            $this->form_validation->set_rules('supplierEmail','text','trim|required');
            $this->form_validation->set_rules('supplierMobile','text','trim|required');
            $this->form_validation->set_rules('supplierAddress','text','trim|required');
            if($this->form_validation->run() == true)
            {
                $setSupplierArray = array('company'=>$this->input->post('supplierCompanyName'),'contactPerson'=>$this->input->post('supplierContactPerson'),
                'email'=>$this->input->post('supplierEmail'),'mobile'=>$this->input->post('supplierMobile'),'landline'=>$this->input->post('supplierlandline'),'address'=>$this->input->post('supplierAddress'),
                'products'=>$this->input->post('supplierProduct'),'status'=>'active');
                $getSupplierInsertStatus = $this->Suppliermodel->addSupplierData($setSupplierArray);
                if($getSupplierInsertStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Supplier addedd successfully');
                    $this->load->helper('url');
                    redirect(base_url()."supplier/addsupplier", 'refresh');
                }
                else if($getSupplierInsertStatus['message'] == 'supplier')
                {
                    $this->session->set_flashdata('error','Contact Number or email address is already assigned with '.$getSupplierInsertStatus['companyName'].'');
                    $this->load->helper('url');
                    redirect(base_url()."supplier/addsupplier", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."supplier/addsupplier", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fileds are mandatory or email address is already exist');
                $this->load->helper('url');
                redirect(base_url()."supplier/addsupplier", 'refresh');
            }
        }
        $this->load->view('addsupplierdata');
    }
    public function editsupplier()
    {
        if($this->session->userdata('vendorAuth') == "")
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        $getSupplierId = $this->uri->segment('3');
        if($getSupplierId == "")
        {
            $this->load->helper('url');
            redirect(base_url()."supplier", 'refresh');
        }
        // edit supplier data
        if($this->input->post('editSupplierBtn'))
        {
            $this->form_validation->set_rules('supplierCompanyName','text','trim|required');
            $this->form_validation->set_rules('supplierContactPerson','text','trim|required');
            $this->form_validation->set_rules('supplierEmail','text','trim|required|valid_email');
            $this->form_validation->set_rules('supplierMobile','text','trim|required');
            $this->form_validation->set_rules('supplierAddress','text','trim|required');
            if($this->form_validation->run() == true)
            {
                $setSupplierArray = array('company'=>$this->input->post('supplierCompanyName'),'contactPerson'=>$this->input->post('supplierContactPerson'),
                'email'=>$this->input->post('supplierEmail'),'mobile'=>$this->input->post('supplierMobile'),'landline'=>$this->input->post('supplierlandline'),'address'=>$this->input->post('supplierAddress'),
                'products'=>$this->input->post('supplierProduct'),'status'=>'active','supplierId'=>$getSupplierId);
                $getSupplierUpdateStatus = $this->Suppliermodel->updateSupplierData($setSupplierArray);
                if($getSupplierUpdateStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Supplier addedd successfully');
                    $this->load->helper('url');
                    redirect(base_url()."supplier/editsupplier/".$getSupplierId."", 'refresh');
                }
                else if($getSupplierUpdateStatus['message'] == 'supplier')
                {
                    $this->session->set_flashdata('error','Contact Number or email address is already assigned with '.$getSupplierUpdateStatus['companyName'].'');
                    $this->load->helper('url');
                    redirect(base_url()."supplier/editsupplier/".$getSupplierId."", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."supplier/editsupplier/".$getSupplierId."", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fileds are mandatory or email address is already exist');
                $this->load->helper('url');
                redirect(base_url()."supplier/editsupplier/".$getSupplierId."", 'refresh');
            }
        }
        // End here
        $getsupplierDataValue = $this->Suppliermodel->fetchEditSupplierDataValue($getSupplierId);
        $this->load->view('editsupplierdata',$getsupplierDataValue);
    }
    public function viewsupplier()
    {
        if($this->session->userdata('vendorAuth') == "")
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        $getSupplierId = $this->uri->segment('3');
        if($getSupplierId == "")
        {
            $this->load->helper('url');
            redirect(base_url()."supplier", 'refresh');
        }
        $getSupplierDetails = $this->Suppliermodel->fetchSupplierDetails($getSupplierId);
        $this->load->view('viewsupplierdata',$getSupplierDetails);
    }
    public function tagsupplier()
    {
        $this->load->view('tagsupplierdata');
    }
    public function searchProductName()
    {
        $getKeyword = $this->input->post('keyword');
        if($getKeyword != "")
        {
            $getSearchStatus = $this->Suppliermodel->getSearchedProduct($getKeyword);
            if($getSearchStatus['message'] == 'success')
            {
                echo $getSearchStatus['result'];
            }
            else
            {
                echo 'no';
            }
        }
        else
        {
            echo "no";
        }
    }
    public function searchSupplierData()
    {
        if($this->input->post('supplierSearchSubmitData'))
        {
            $setSearchArray = array('company'=>$this->input->post('searchCompanyName'),'person'=>$this->input->post('searchContactPerson'),'phone'=>$this->input->post('searchSupplierPhone'),'email'=>$this->input->post('searchSupplierEmail'),'status'=>$this->input->post('searchSupplierStatus'));
        }
    }
     
}
