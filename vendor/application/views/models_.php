<style type="text/css">
.errorMessage {
    color: red;
    
}
</style>
<?php 

if($model_type=='add_new'){
?>

                  <div class="modal-body">                
                   <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label" name="testimonials_names">Name</label> 
                                    <input type="text" class="form-control" value="<?php echo set_value('testimonials_name'); ?>" id="field-3" name="testimonials_name" placeholder=""> 
                                </div>
                                 <div class="errorMessage"><?php echo form_error('testimonials_name'); ?></div>

                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Image</label> 
                                     <input type="file" class="filestyle" data-buttonname="btn-white"  name="testimonials_image">
                                </div> 
                            </div> 
                            
                        </div> 
                            <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Feedback</label> 
                                    <textarea type="text" class="form-control" name="testimonials_feedback" rows="5" placeholder=""><?php echo set_value('testimonials_feedback'); ?></textarea> 
                                </div> 
                            </div> 
                            <div class="errorMessage"><?php echo form_error('testimonials_feedback'); ?></div>
                        </div> 
                        <div class="row">
                              <div class="col-md-6 checkbox checkbox-custom">
                                <input id="Publish_check" type="checkbox" name="testimonials_checkbox">
                                <label>Publish</label>
                              </div>
                             </div>
                      
                    </div>
                    <?php 

 } 

if($model_type=='edit'){  ?>

<div class="modal-body">

          <div class="row">
          <div class="col-md-12">

          <div class="card-box" style="padding: 0px 20px!important">
             
                                    <div class="row p-l-0">
                                        <div class="col-md-12 p-l-0 form-horizontal">
                                          <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input class="form-control" id="testimonial_edit_name" value="<?php echo set_value('testimonials_name'); ?>" type="text" name="testimonials_name" >
                                            </div>
                                            <div class="errorMessage"><?php echo form_error('testimonials_name'); ?></div>
                                            <div class="form-group">
                                            <label class="control-label">Image</label>
                                             <input type="file" class="filestyle" name="testimonials_image" data-buttonname="btn-white">
                                             <br/>
                                             <div id="imageset"></div>
                                            </div>
                                            <div class="form-group">
                                            <label class="control-label">Feedback</label>
                                            <textarea class="form-control" name="testimonials_feedback" id="testimonial_edit_feedback"   rows="2"><?php echo set_value('testimonials_feedback'); ?></textarea>
                                            </div>
                                             <div class="errorMessage"><?php echo form_error('testimonials_feedback'); ?></div>
                                         <div class="col-md-6 checkbox checkbox-custom m-b-10">
                                           <input id="tastimoneal_publish_check" type="checkbox" name="testimonials_checkbox" value="1">
                                           <label>Publish</label>
                                        </div>
                                       
                                        </div>                             
                           
                                    </div>
                                            
                                  
                                  </div>
          </div>
          </div>
          </div>


                    <?php }  if($model_type=='shipingruls'){?>

                      <div class="form-group">
                      <label for="shipping_min_value">Minimum order</label>
                      <input type="text" name="shipping_min_value" parsley-trigger="order" value="<?php echo set_value('shipping_min_value'); ?>" placeholder="" class="form-control" id="shipping_min_value">
                    
                    </div>
                     <div class="errorMessage"><?php echo form_error('shipping_min_value'); ?></div>
                    <div class="form-group">
                      <label for="shipping_charge">Shipping charge</label>
                      <input type="text" name="shipping_charge" parsley-trigger="change" value="<?php echo set_value('shipping_charge'); ?>" placeholder="" class="form-control" id="shipping_charge">
                    
                    </div>
                     <div class="errorMessage"><?php echo form_error('shipping_charge'); ?></div>

                     <?php }   

 if($model_type=='warehouselist'){  ?>

       <div class="modal-body"> 
                                                    <div class="row">
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-4 control-label">Name</label>
        <div class="col-md-8">
        <input type="text" value="<?php echo set_value('w_name'); ?>" class="form-control required_validation_for_add_supplier name_validation" name="w_name" placeholder="Name">
         <div class="errorMessage"><?php echo form_error('w_name'); ?></div>
        </div>
        
        </div>
       
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup Name</label>
        <div class="col-md-8">
        <input type="text" value="<?php echo set_value('w_pickup_name'); ?>" class="form-control required_validation_for_add_supplier name_validation" name="w_pickup_name" placeholder="Point of contact">
        <div class="errorMessage"><?php echo form_error('w_pickup_name'); ?></div>
        </div>
       
        </div> 
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup phone</label>
        <div class="col-md-8">
        <input type="text" value="<?php echo set_value('w_pickup_phone'); ?>" class="form-control Internationphonecode required_validation_for_add_supplier name_validation" name="w_pickup_phone" placeholder="Contact">
        <div class="errorMessage"><?php echo form_error('w_pickup_phone'); ?></div>
        </div>

        </div>
        
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup country</label>
        <div class="col-md-8">
        <select class="form-control required_validation_for_add_supplier country_addlist" name="w_pickup_country">
       <option value="">Select Country</option>
        <?php foreach ($country_list as $key => $value) {
           $country_id=$value->country_id;
                  $w_pickup_phone_val=  set_value('w_pickup_country');
                 if($w_pickup_phone_val==$country_id){
                 $select_val='selected';
                 }else{ $select_val='';}
                $country_name=$value->country_name;
                echo "<option  value='$country_id' $select_val >$country_name</option>";
        } ?>
         
        </select>
        <div class="errorMessage"><?php echo form_error('w_pickup_country'); ?></div>
        </div>
        
        </div> 
        <div class="form-group">
        <label class="col-md-4 control-label">State/Region/Province</label>
        <div class="col-md-8">
        <select class="form-control required_validation_for_add_supplier state_addlist" name="w_pickup_state">
        <option value=""  >Select State/Region/Province</option>

        </select>
        <div class="errorMessage"><?php echo form_error('w_pickup_state'); ?></div>
        </div>
       
        </div> 
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup city</label>
        <div class="col-md-8">
        <input type="text" value="<?php echo set_value('w_pickup_city'); ?>" class="form-control required_validation_for_add_supplier name_validation email_validation" name="w_pickup_city" placeholder="City">
        <div class="errorMessage"><?php echo form_error('w_pickup_city'); ?></div>
        </div>
        
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup address</label>
        <div class="col-md-8">
        <textarea class="form-control required_validation_for_add_supplier name_validation" name="w_pickup_address" rows="5" placeholder="Address"><?php echo set_value('w_pickup_address'); ?></textarea>
          <div class="errorMessage"><?php echo form_error('w_pickup_address'); ?></div>
        </div>
      
        </div>
         <div class="form-group">
        <label class="col-md-4 control-label">Pickup PIN/ZIP code</label>
        <div class="col-md-8">
        <input type="text" value="<?php echo set_value('w_pickup_zip'); ?>" class="form-control name_validation" name="w_pickup_zip" placeholder="PIN/ZIP code">
        <div class="errorMessage"><?php echo form_error('w_pickup_zip'); ?></div>
        </div>
        
        </div>


        </div>

       
        </div>
                                                </div> 
  <?php } if($model_type=='warehouselist_edit'){  ?>

 <div class="modal-body"> 
                                                    <div class="row">
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-4 control-label">Name</label>
        <div class="col-md-8">
        <input type="text" value="<?php echo set_value('w_name'); ?>" id="id_w_name" class="form-control required_validation_for_add_supplier name_validation" name="w_name" placeholder="Name">
         <div class="errorMessage"><?php echo form_error('w_name'); ?></div>
        </div>
        
        </div>
       
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup Name</label>
        <div class="col-md-8">
        <input type="text" value="<?php echo set_value('w_pickup_name'); ?>" id="id_w_pickup_name" class="form-control required_validation_for_add_supplier name_validation" name="w_pickup_name" placeholder="Point of contact">
        <div class="errorMessage"><?php echo form_error('w_pickup_name'); ?></div>
        </div>
       
        </div> 
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup phone</label>
        <div class="col-md-8">
        <input type="text" value="<?php echo set_value('w_pickup_phone'); ?>" id="id_w_pickup_phone" class="form-control Internationphonecode required_validation_for_add_supplier name_validation" name="w_pickup_phone" placeholder="Contact">
        <div class="errorMessage"><?php echo form_error('w_pickup_phone'); ?></div>
        </div>

        </div>
        
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup country</label>
        <div class="col-md-8">
        <select class="form-control required_validation_for_add_supplier country_addlist" id="id_w_pickup_country" name="w_pickup_country">
       <option value="">Select Country</option>
        <?php foreach ($country_list as $key => $value) {
           $country_id=$value->country_id;
                  $w_pickup_phone_val=  set_value('w_pickup_country');
                 if($w_pickup_phone_val==$country_id){
                 $select_val='selected';
                 }else{ $select_val='';}
                $country_name=$value->country_name;
                echo "<option  value='$country_id' $select_val >$country_name</option>";
        } ?>
         
        </select>
        <div class="errorMessage"><?php echo form_error('w_pickup_country'); ?></div>
        </div>
        
        </div> 
        <div class="form-group">
        <label class="col-md-4 control-label">State/Region/Province</label>
        <div class="col-md-8">
        <select class="form-control required_validation_for_add_supplier state_addlist" id="id_w_pickup_state" name="w_pickup_state">
        <option value=""  >Select State/Region/Province</option>

        </select>
        <div class="errorMessage"><?php echo form_error('w_pickup_state'); ?></div>
        </div>
       
        </div> 
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup city</label>
        <div class="col-md-8">
        <input type="text" value="<?php echo set_value('w_pickup_city'); ?>" id="id_w_pickup_city" class="form-control required_validation_for_add_supplier name_validation email_validation" name="w_pickup_city" placeholder="City">
        <div class="errorMessage"><?php echo form_error('w_pickup_city'); ?></div>
        </div>
        
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Pickup address</label>
        <div class="col-md-8">
        <textarea class="form-control required_validation_for_add_supplier name_validation" id="id_w_pickup_address" name="w_pickup_address" rows="5" placeholder="Address"><?php echo set_value('w_pickup_address'); ?></textarea>
          <div class="errorMessage"><?php echo form_error('w_pickup_address'); ?></div>
        </div>
      
        </div>
         <div class="form-group">
        <label class="col-md-4 control-label">Pickup PIN/ZIP code</label>
        <div class="col-md-8">
        <input type="text" value="<?php echo set_value('w_pickup_zip'); ?>" id="id_w_pickup_zip" class="form-control name_validation" name="w_pickup_zip" placeholder="PIN/ZIP code">
        <div class="errorMessage"><?php echo form_error('w_pickup_zip'); ?></div>
        </div>
        
        </div>


        </div>

       
        </div>
   



  <?php } if($model_type=='pay_panding_customer'){  ?>


   <div class="col-md-6 form-horizontal">
        <div class="form-group">
        <label class="col-md-3 control-label">Amount</label>
        <div class="col-md-9">
        <input type="text" value="<?php echo set_value('pay_panding_amount'); ?>"   id="enable_amount" class="form-control required_validation_for_add_custo" name="pay_panding_amount" placeholder="Amount">
        <div class="errorMessage"><?php echo form_error('pay_panding_amount'); ?></div>
        </div>
        </div>
      </div>
      <div class="col-md-6 form-horizontal">
    <div class="form-group">
    <label class="col-md-3 control-label">Period</label>
    <div class="col-md-9">
    <input type="text" value="<?php echo set_value('pay_panding_period'); ?>"  id="enable_period" class="form-control required_validation_for_add_custo"  name="pay_panding_period" placeholder="Period">
    <div class="errorMessage"><?php echo form_error('pay_panding_period'); ?></div>
    </div>
    </div>
  </div>

  
    <?php } if($model_type=='taxsetting'){  ?>

    <div class="form-group">
        <label>Tax Registration Id</label>
        <input class="form-control required_validation_for_add_custo" value="<?php echo set_value('taxsetting_number'); ?>" name="taxsetting_number"  type="text" placeholder="Tax Registration Id">
        <div class="errorMessage"><?php echo form_error('taxsetting_number'); ?></div>
        </div>

    
    <?php } if($model_type=='skusetting'){  ?>

    <div class="form-group">
         
       <label>Maintainance Sku</label>         
         <input class="form-control required_validation_for_add_custo" value="<?=set_value('skusetting_number');?>" name="skusetting_number"  type="text" placeholder="Maintainance Saku">
        <div class="errorMessage"><?php echo form_error('skusetting_number'); ?></div>
        </div>

  
<?php } if($model_type=='inventorysetting'){  ?>

    <div class="form-group">
        <label>Universal Reorder Quantity</label>
        <input class="form-control required_validation_for_add_custo" value="<?=set_value('inventorysetting_number');?>" name="inventorysetting_number"  type="text" placeholder="Universal Reorder Quantity"><div class="errorMessage"><?php echo form_error('inventorysetting_number'); ?></div>
        </div>
         
          <?php } if($model_type=='discount'){  ?>

        <div class="form-group">
        <label class="col-md-4 control-label">Discount name</label>
        <div class="col-md-8">
        <input type="text" value="<?=set_value('discountname');?>"  class="form-control required_validation_for_add_custo" name="discountname" placeholder="Discount name">
        <div class="errorMessage"><?php echo form_error('discountname'); ?></div>
        </div>
        </div>       

         <div class="form-group">
        <label class="col-md-4 control-label">Discount Value</label>
        <div class="col-md-8">
        <input type="text" value="<?=set_value('discountvalue');?>"  class="form-control required_validation_for_add_custo" name="discountvalue" placeholder="Discount Value">
        <div class="errorMessage"><?php echo form_error('discountvalue'); ?></div>
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label">Discount started at</label>
        <div class="col-md-8">
        <input type="text" value="<?=set_value('discountstarted');?>"  class="form-control date_picker required_validation_for_add_custo" name="discountstarted" placeholder="Discount started at">
        <div class="errorMessage"><?php echo form_error('discountstarted'); ?></div>
        </div>
        </div>
           <?php }  ?>



            <?php  if($model_type=='discountedit'){  ?>

        <div class="form-group">
        <label class="col-md-4 control-label">Discount name</label>
        <div class="col-md-8">
        <input type="text" value="<?=set_value('discountname');?>"  id="discount_name" class="form-control required_validation_for_add_custo" name="discountname" placeholder="Discount name">
        <div class="errorMessage"><?php echo form_error('discountname'); ?></div>
        </div>
        </div>       

         <div class="form-group">
        <label class="col-md-4 control-label">Discount Value</label>
        <div class="col-md-8">
        <input type="text" value="<?=set_value('discountvalue');?>" id="discount_amount" class="form-control required_validation_for_add_custo" name="discountvalue" placeholder="Discount Value">
        <div class="errorMessage"><?php echo form_error('discountvalue'); ?></div>
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label">Discount started at</label>
        <div class="col-md-8">
        <input type="text" value="<?=set_value('discountstarted');?>" id="discount_start" class="form-control date_picker required_validation_for_add_custo" name="discountstarted" placeholder="Discount started at">
        <div class="errorMessage"><?php echo form_error('discountstarted'); ?></div>
        </div>
        </div>

        <?php } if($model_type=='Membership'){  ?>

        <div class="form-group">
           <label for="field-7" class="control-label">Code</label>
           <input class="form-control required_validation_for_vandor" type="text" name="Code" value="<?=set_value('Code');?>">
           started at">
        <div class="errorMessage"><?php echo form_error('Code'); ?></div>
        </div>
         <div class="form-group">
           <label for="field-7" class="control-label">Discount Percentage</label>
           <input class="form-control required_validation_for_vandor" type="text" name="DiscountPercentage" value="<?=set_value('DiscountPercentage');?>">
           started at">
        <div class="errorMessage"><?php echo form_error('DiscountPercentage'); ?></div>
        </div>
         <div class="form-group">
           <label for="field-7" class="control-label">Expiry Date</label>
           <input class="form-control date_picker required_validation_for_vandor" type="text" name="ExpiryDate" value="<?=set_value('ExpiryDate');?>">
           started at">
        <div class="errorMessage"><?php echo form_error('ExpiryDate'); ?></div>
        </div>
      </div>
           <?php }  ?>