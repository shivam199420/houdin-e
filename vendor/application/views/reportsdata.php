<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                         <!--START Page-Title -->
            <div class="row">
           
                <div class="col-md-8">
                   <h4 class="page-title">Reports</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li class="active">Reports</li>
                  </ol> 
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form>
                  </div> -->
                 
            </div>
           <!--END Page-Title --> 
                        <div class="row"> 
        <div class="col-md-12 m-t-30">
        <div class="col-md-3">
        <div class="form-group">
        <button type="button"  class="btn btn-default  btn-block">Today</button>
        </div>
        </div>
        <div class="col-md-3">
        <div class="form-group">
        <button type="button"  class="btn btn-default  btn-block ">This Week</button>
        </div>
        </div>
        <div class="col-md-3">
        <div class="form-group">
        <button type="button"  class="btn btn-default btn-block ">This Month</button>
        </div>
        </div>
       <!-- <div class="col-md-3">
        <div class="form-group">
        <button type="button" data-set="year" class="btn btn-default btn-block ">This Year</button>
        </div>
        </div>-->
        </div>
       
        </div>
                        <div class="row ">
                        <div class="card-box" style="float: left;">
                          <div class="col-md-12">
                            <div class="col-md-6">
                            <p class="graph_title" style="text-align: center;">Customer graph</p>
                          
                              <img width="100%" src="<?php echo base_url(); ?>assets/images/graph.jpg" />
                          
                            </div>
                             <div class="col-md-6">
                             <p class="graph_title" style="text-align: center;">Order graph</p>
                              <img width="100%" src="<?php echo base_url(); ?>assets/images/graph.jpg" />
                            </div>
                     
                          </div>
                          </div>
                        </div>
                       
                        <div class="row m-t-20">
       

        <div class="card-box table-responsive">
        <form action="#">
          <div class="form-group">
                      <label for="userName">Choose Product</label>
                     <select class="form-control">
                      <option>Product-1</option>
                      <option>Product-2</option>
                    </select>
                   
                    </div>

        </form>

        <div class="col-md-12">
                            <p class="graph_title" style="text-align: center;">Product selling graph</p>
                          
                              <img width="100%" src="<?php echo base_url(); ?>assets/images/productsellinggraph.png" />
                          
                            </div>
        </div>
        
        </div>        </div>


                                  </div> <!-- container -->

                              </div> <!-- content -->
<?php $this->load->view('Template/footer.php') ?>
  <script>
  $(document).ready(function(){
  $('#click_advance').click(function() {
  $('#display_advance').toggle('5000');
  $("i", this).toggleClass("fa fa-chevron-up fa fa-chevron-down");
  });
  });
  </script>
