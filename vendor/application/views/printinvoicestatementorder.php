<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>
<?php 
$getInvoiceSetting = invoiceSetting(); 
$getLogo = fetchvedorsitelogo();
?>
 

 <style>
/* reset */

*
{
	border: 0;
	box-sizing: content-box;
	color: inherit;
	font-family: inherit;
	font-size: inherit;
	font-style: inherit;
	font-weight: inherit;
	line-height: inherit;
	list-style: none;
	margin: 0;
	padding: 0;
	text-decoration: none;
	vertical-align: top;
}

/* content editable */

*[  ] { border-radius: 0.25em; min-width: 1em; outline: 0; }

*[  ] { cursor: pointer; }

*[  ]:hover, *[  ]:focus, td:hover *[  ], td:focus *[  ], img.hover { background: #DEF; box-shadow: 0 0 1em 0.5em #DEF; }

span[  ] { display: inline-block; }

/* heading */

h1 { font: bold 100% sans-serif; letter-spacing: 0.5em; text-align: center; text-transform: uppercase; }

/* table */

table { font-size: 75%; table-layout: fixed; width: 100%; }
table { border-collapse: separate; border-spacing: 2px; }
th, td { border-width: 1px; padding: 0.5em; position: relative; text-align: left; }
th, td { border-radius: 0.25em; border-style: solid; }
th {     background: #047aa7;
    border-color: #2a7aa7;
    color: #fff;}
td { border-color: #DDD; }

/* page */

html { font: 16px/1 'Open Sans', sans-serif; overflow: auto; padding: 0.5in; }
html { background: #999; cursor: default; }

body { box-sizing: border-box; height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }
body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }

/* header */

header { margin: 0 0 3em; }
header:after { clear: both; content: ""; display: table; }

header h1 { background: #2a7aa7; border-radius: 0.25em; color: #FFF; margin: 0 0 1em; padding: 0.5em 0; }
header address { float: left; font-size: 75%; font-style: normal; line-height: 1.25; margin: 0 1em 1em 0; }
header address p { margin: 0 0 0.25em; }
header span, header img { display: block; float: right; }
header span { margin: 0 0 1em 1em; max-height: 25%; max-width: 35%; position: relative; }
header img { max-height: 100%; max-width: 100%; }
header input { cursor: pointer; -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; height: 100%; left: 0; opacity: 0; position: absolute; top: 0; width: 100%; }

/* article */

article, article address, table.meta, table.inventory { margin: 0 0 2em; }
article:after { clear: both; content: ""; display: table; }
article h1 { clip: rect(0 0 0 0); position: absolute; }

article address { float: left; font-size: 125%; font-weight: bold; }

/* table meta & balance */

table.meta, table.balance { float: right; width: 36%; }
table.meta:after, table.balance:after { clear: both; content: ""; display: table; }

/* table meta */

table.meta th { width: 40%; }
table.meta td { width: 60%; }

/* table items */

table.inventory { clear: both; width: 100%; }
table.inventory th { font-weight: bold; text-align: center; }

table.inventory td:nth-child(1) { width: 26%; }
table.inventory td:nth-child(2) { width: 38%; }
table.inventory td:nth-child(3) { text-align: right; width: 12%; }
table.inventory td:nth-child(4) { text-align: right; width: 12%; }
table.inventory td:nth-child(5) { text-align: right; width: 12%; }

/* table balance */

table.balance th, table.balance td { width: 50%; }
table.balance td { text-align: right; }

/* aside */

aside h1 { border: none; border-width: 0 0 1px; margin: 0 0 1em; }
aside h1 { border-color: #999; border-bottom-style: solid; }

/* javascript */

.add, .cut
{
	border-width: 1px;
	display: block;
	font-size: .8rem;
	padding: 0.25em 0.5em;	
	float: left;
	text-align: center;
	width: 0.6em;
}

.add, .cut
{
	background: #9AF;
	box-shadow: 0 1px 2px rgba(0,0,0,0.2);
	background-image: -moz-linear-gradient(#00ADEE 5%, #0078A5 100%);
	background-image: -webkit-linear-gradient(#00ADEE 5%, #0078A5 100%);
	border-radius: 0.5em;
	border-color: #0076A3;
	color: #FFF;
	cursor: pointer;
	font-weight: bold;
	text-shadow: 0 -1px 2px rgba(0,0,0,0.333);
}

.add { margin: -2.5em 0 0; }

.add:hover { background: #00ADEE; }

.cut { opacity: 0; position: absolute; top: 0; left: -1.5em; }
.cut { -webkit-transition: opacity 100ms ease-in; }

tr:hover .cut { opacity: 1; }
.customer-table{}
.customer-table td{border-color: #fff;padding: 3px;}
.customer-table td:last-child{float: right;}
.bd-none-table td{border-color: #fff;}
.additional-notes p{color: #515151;
    line-height: 24px;
    font-size: 14px;}
    .bd-border-td{border-bottom: 2px solid;padding-top: 17px;}
@media print {
	* { -webkit-print-color-adjust: exact; }
	html { background: none; padding: 0; }
	body { box-shadow: none; margin: 0; }
	span:empty { display: none; }
	.add, .cut { display: none; }
}

@page { margin: 0; }
</style>
<html>
	<head>
		<meta charset="utf-8">
		<title>Invoice</title>
	 
	</head>
	<body>
		<header>
			<h1>Invoice</h1>
			<address>
			<img alt="Store Logo"  width="229px" src="<?php print_r($getLogo); ?>">
				<p><?php if($shop_detail->houdinv_shop_title) { echo $shop_detail->houdinv_shop_title; } ?></p>
				<?php if($shop_detail->houdinv_shop_address) { ?> 	<p> <?php echo $shop_detail->houdinv_shop_address ?></p><?php } ?>
				<p> <?php if($shop_detail->houdinv_shop_contact_info) { echo $shop_detail->houdinv_shop_contact_info; } ?></p>
                <p> <?php if($shop_detail->houdinv_shop_order_email) { echo $shop_detail->houdinv_shop_order_email; } ?></p>
            </address>
			<span class="customer-detail">
			<h1>Work Order</h1>
			<table class="customer-table">
				<tr>
				<td>W.o.#</td>
				<td><?php echo $maindata[0]['main']->houdinv_order_id ?></td>
				</tr>
					<tr>
				<td>W.o.Date</td>
				<td><?php if($maindata[0]['main']->houdinv_orders_deliverydate) { ?> <p style="font-size:12px;text-align:right;">  <?php echo  date('d-m-Y',strtotime($maindata[0]['main']->houdinv_orders_deliverydate)); ?></p> <?php  } ?>
				</td>
				</tr>
					<tr>
				<td>Order Status :</td>
				<td><?php echo $maindata[0]['main']->houdinv_order_confirmation_status ?></td>
				</tr>
					 
					<tr>
				<td>Sales channel</td>
				<td> <?php echo $maindata[0]['main']->houdinv_order_type ?></td>
				</tr>
			</table>
			</span>
		</header>
		<table class="bd-none-table">
				<thead>
					<tr>
						<th><span>JOB</span></th>
						<th><span>BILL TO</span></th>
						<th><span>SHIP TO(if different)</span></th>
						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><span> </span></td>
						<td>
                            
                        <p><?php if($maindata[0]['user'][0]->houdinv_order_users_name) { echo $maindata[0]['user'][0]->houdinv_order_users_name; } else { echo $maindata[0]['user'][0]->houdinv_user_name; } ?></p>
				
                            <?php 
							if(count($extraAddress) > 0)
							{
							?>  <span> <?php echo $extraAddress[0]->houdinv_order_users_main_address ?>, <?php echo $extraAddress[0]->houdinv_order_users_city ?>, <?php echo $extraAddress[0]->houdinv_order_users_zip ?></span>
							<?php }else{?>
								<span> <?php echo $billingAddress[0]->houdinv_user_address_user_address ?>, <?php echo $billingAddress[0]->houdinv_user_address_city ?>, <?php echo $billingAddress[0]->houdinv_user_address_zip ?></span>
                                <?php } ?>

                                <?php if($maindata[0]['user'][0]->houdinv_user_email) { ?> 	<p> <?php echo $maindata[0]['user'][0]->houdinv_user_email ?></p><?php } ?>
				<p> <?php if($maindata[0]['user'][0]->houdinv_order_users_contact) { echo $maindata[0]['user'][0]->houdinv_order_users_contact; } else { echo $maindata[0]['user'][0]->houdinv_user_contact; } ?></p>
		  
						</td>
						<td>
                        <p><?php if($maindata[0]['user'][0]->houdinv_order_users_name) { echo $maindata[0]['user'][0]->houdinv_order_users_name; } else { echo $maindata[0]['user'][0]->houdinv_user_name; } ?></p>
				
                            <?php 
							if(count($extraAddress) > 0)
							{
							?>  <span> <?php echo $extraAddress[0]->houdinv_order_users_main_address ?>, <?php echo $extraAddress[0]->houdinv_order_users_city ?>, <?php echo $extraAddress[0]->houdinv_order_users_zip ?></span>
							<?php }else{?>
								<span>  <?php echo $deliveryAddress[0]->houdinv_user_address_user_address ?>, <?php echo $deliveryAddress[0]->houdinv_user_address_city ?>, <?php echo $deliveryAddress[0]->houdinv_user_address_zip ?></span>
                            <?php } ?>
                            <?php if($maindata[0]['user'][0]->houdinv_user_email) { ?> 	<p> <?php echo $maindata[0]['user'][0]->houdinv_user_email ?></p><?php } ?>
				<p> <?php if($maindata[0]['user'][0]->houdinv_order_users_contact) { echo $maindata[0]['user'][0]->houdinv_order_users_contact; } else { echo $maindata[0]['user'][0]->houdinv_user_contact; } ?></p>
		  

						</td>
						
					</tr>
					 
				</tbody>
			</table>
		<article>
	
			<table class="inventory">
				<thead>
					<tr>
						<th><span   >Item</span></th>
						<th><span   >Rate</span></th>
						<th><span   >Tax</span></th>
						<th><span   >Quantity</span></th>
						<th><span   >Price</span></th>
					</tr>
				</thead>
				<tbody>

						<?php 
						$setSubTotaldata = 0;
						$setTaxToatal = 0;
						$setFinalTotal = 0;
						foreach($productDetail as $productDetaildata)
						{
							$setSubTotaldata = $setSubTotaldata+($productDetaildata['saleprice']*$productDetaildata['quantity']);
							$setTaxToatal = $setTaxToatal+$productDetaildata['totaltax'];
							$setFinalTotal = $setFinalTotal+$productDetaildata['total'];
						?>
					<tr>
						<td><span><?php echo $productDetaildata['productName'] ?></span></td>
						<td><span ><?php echo $productDetaildata['saleprice'] ?></span></td>
						<td>  <span><?php echo $productDetaildata['totaltax'] ?></span></td>
						<td><span><?php echo $productDetaildata['quantity'] ?></span></td>
						<td> <span><?php echo $productDetaildata['total'] ?></span></td>
                    </tr>
                        <?php } ?>

				</tbody>
			</table>
			<table class="balance">
				<tr>
					<th><span   >Sub Total</span></th>
					<td><span data-prefix></span><span><?php echo $setSubTotaldata; ?></span></td>
				</tr>
				<tr>
					<th><span   >Taxable</span></th>
					<td><span data-prefix></span><span   ><?php echo $setTaxToatal; ?></span></td>
				</tr>
				<tr>
					<th><span>Delivery Charges</span></th>
					<td><span data-prefix></span><span><?php echo $maindata[0]['main']->houdinv_delivery_charge ?></span></td>
				</tr>
				<tr>
					<th><span>Discount</span></th>
					<td><span data-prefix></span><span><?php echo $maindata[0]['main']->houdinv_orders_discount ?></span></td>
				</tr>
				 
				<tr>
					<th><span   >Total</span></th>
					<td><span data-prefix></span><span><?php echo $maindata[0]['main']->houdinv_orders_total_Amount ?></span></td>
				</tr>
			</table>
		</article>
		<aside>
			<h1><span   >Additional Notes</span></h1>
			<div  class="additional-notes" >
				<p><?php echo $maindata[0]['main']->houdinv_order_comments ?></p>
			 
			</div>
		</aside>
		<table class="bd-none-table" style="margin-top: 20px;">
			<tr>
			<td width="8%">signature</td>
			<td><div class="bd-border-td"></td>
			<td width="8%">Date</td>
			<td><div class="bd-border-td"></td>
			</tr>
		</table>
	</body>
</html>


<br/>
<br/>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script>
$(document).ready(function(){
    window.setTimeout(function(){
      window.print();
         window.close();  
    },2000)
})
</script>
<!-- <table style="width:100%;"><tr><td style="font-size:10px;"><b>Note:</b>&nbsp;incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit</td></tr></table> -->
