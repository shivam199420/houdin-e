<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/calendarsidebar.php') ?><div class="content-page">
<link href="<?php echo base_url(); ?>assets/plugins/fullcalendar/css/fullcalendar.min.css" rel="stylesheet" />     
<link href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
	<style tyle="text/css">.fc-event{cursor: pointer;}</style>
	<div class="content">
                    <div class="container">

                         <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Calendar</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li class="active">Calendar</li>
                  </ol>
                  </div>
                 
            </div>
                   <?php     
                     if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
   // print_r($all_data);
    ?>  
           <!--END Page-Title -->  
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
									
                                <div class="pull-right m-b-10">
                            <!-- <button type="button" data-toggle="modal" data-target="#addorder-modal"  class="btn btn-default waves-effect waves-light">Add Order <span class="m-l-5"><i class="fa fa-plus-circle"></i></span></button>                                    
                              -->
                              
                              <a href="<?php echo base_url();?>Workorder"><button type="button"   class="btn btn-default waves-effect waves-light">Add Order <span class="m-l-5"><i class="fa fa-plus-circle"></i></span></button></a> 
                                </div>

                              
                            </div>
                        </div>

                        
						
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="row">
                                 
                                    <div class="col-md-12">
                                        <div class="card-box">
                                            <div id="calendar"></div>
                                        </div>
                                    </div> <!-- end col -->
                                </div>  <!-- end row --> 
								
                            </div>
                            <!-- end col-12 -->
                        </div> <!-- end row --> 
                    </div> <!-- container -->
                               
                </div> <!-- content -->
	
	
	 <!-- Add order MODAL -->
		<!-- <div class="modal fade" id="addorder-modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><strong>Add New Order</strong></h4>
					</div>
					<div class="modal-body">
						<div class="row">
						<form class="form-horizontal" id="addorderform" method="post">
						
							<div class="form-group">
								<label class="col-md-4 control-label">Customer name</label>
								<div class="col-md-8">
			 <select class="form-control select_name  select1 required_validation_for_workorder" name="customer_name">
<option value="">Choose one</option>
<?php 
// foreach($getCustomerData['customerList'] as $value)
// {
   ?>
 
      
         <option value="<?php //echo $value->houdinv_user_id; ?>" data-phone="<?php //echo $value->houdinv_user_contact; ?>"><?php //echo $value->houdinv_user_name; ?></option>
                                                <?php 
//}
?>
                                           
                      </select>
                      	</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Customer contact</label>
								<div class="col-md-8">
								<input type="text" value="" class="form-control required_field" name="order_customercontact" placeholder="">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Customer Product</label>
								<div class="col-md-8">
								<input type="text" value=""  class="form-control required_field" name="order_customerproduct" placeholder="">
								</div>
							</div>
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Choose quantity</label>
								<div class="col-md-8">
								<input type="number" value="" min="0" class="form-control required_field" name="order_customerquantity" placeholder="">
								</div>
							</div>
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Coupon</label>
								<div class="col-md-8">
								<input type="text" value="" class="form-control" name="order_coupon" placeholder="">
								</div>
							</div>
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Pick up type</label>
								<div class="col-md-8">
									<select class="form-control required_field" name="order_pickuptype">
									    <option value="">Choose Pickup Type</option>
										<option value="Home Delivery">Home Delivery</option>
										<option value="Customer pickup">Customer pickup</option>
								  </select>
								</div>
							</div>
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Delivery Date Time</label>
								<div class="col-md-8">
									<div class="input-group">

												<input type="text" class="form-control datepicker required_field" id="order_delivery_date_add" name="order_delivery_date" placeholder="Date"/>
												<span class="input-group-addon">&</span>
												<input type="text" class="form-control timepicker required_field" id="order_delivery_time_add" name="order_delivery_time" placeholder="Time"/>
									</div>									
									  
								</div>
							</div>
							
							
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Delivery address</label>
								<div class="col-md-8">
									  <input type="text" value="" class="form-control required_field" name="order_delivery_address" placeholder="">
								</div>
							</div>
							
							
								<div class="form-group">
								<label class="col-md-4 control-label">Payment By</label>
								<div class="col-md-8">
									<select class="form-control required_field" name="order_paymentmethod">
									    <option value="">Choose Payment Method</option>
										<option value="1">Cash</option>
										<option value="2">Card</option>
										<option value="3">Checque</option>
										<option value="4">Wallet</option> 
								  </select>
								</div>
							</div>
							
							
							
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Note</label>
								<div class="col-md-8">
									  <input type="text" value="" class="form-control" name="order_note" placeholder="">
								</div>
							</div>
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Total Amount Paid</label>
								<div class="col-md-8">									
									<div class="input-group"><span class="input-group-addon">$</span><input type="text" value="100.00" class="form-control required_field" name="order_totalamount" placeholder=""></div>
								</div>
							</div>

							
							<div class="form-group">
								<label class="col-md-4 control-label">Discount applied</label>
								<div class="col-md-8">
									<div class="input-group"><span class="input-group-addon">$</span><input type="text" value="0.00" class="form-control required_field" name="order_disamount" placeholder=""></div>									 
								</div>
							</div>
							
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Net payable amount</label>
								<div class="col-md-8">
									<div class="input-group"><span class="input-group-addon">$</span><input type="text" value="100.00" class="form-control required_field" name="order_netpayable" placeholder=""></div>
									 
								</div>
							</div>
							
							<div class="form-group">
							 
								<div class="col-md-8 col-sm-offset-4">
									   <button type="submit" class="btn btn-default	 save-event waves-effect waves-light">Create order</button>
								</div>
							</div>
							
						 
						</form>
					</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
					</div>
				</div>
			</div>
		</div>  -->
	
	
	<!-- Edit order MODAL -->
		<div class="modal fade none-border" id="editorder-modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><strong>Edit Order</strong></h4>
					</div>
					<div class="modal-body">
							
			  <?php echo form_open(base_url().'Calendar/WorkorderUpdate',array("id"=>"editorderform","class"=>"form-horizontal","enctype"=>"multipart/form-data")); ?>
 
     			
							<div class="form-group">
								<label class="col-md-4 control-label">Customer name</label>
								<div class="col-md-8">
								<input type="hidden" name="edit_id" id="edit_id" />
								<select class="form-control select_name  required_field" disabled="disabled" name="customer_name">
								<option value="">Choose one</option>
								<?php 
								foreach($getCustomerData['customerList'] as $value)
								{
								   ?>
								   <option value="<?php echo $value->houdinv_user_id; ?>" data-phone="<?php echo $value->houdinv_user_contact; ?>"><?php echo $value->houdinv_user_name; ?></option>
                            <?php 
								}
								?>
                                           
                      </select>
                      	</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Customer contact</label>
								<div class="col-md-8">
								<input type="text" class="form-control required_field"  disabled="disabled" id="order_customercontact" name="customer_phone" placeholder="">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Customer Product</label>
								<div class="col-md-8">
                                <input type="text" class="form-control setCustomerProduct"  disabled="disabled"/>
                                </div>
							</div>
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Choose quantity</label>
								<div class="col-md-8">
								<input type="number" class="form-control required_field"  disabled="disabled" id="order_customerquantity" min="1" name="customer_quantity" placeholder="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Coupon</label>
								<div class="col-md-8">
								<input type="text" class="form-control coupon_code"  disabled="disabled" id="order_coupon" name="customer_coupon" placeholder="">
							 	</div>
								</div>
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Pick up type</label>
								<div class="col-md-8">
									<select class="form-control required_field"  disabled="disabled" name="Customer_pickup_type" id="order_pickuptype">
									    <option value="">Choose Pickup Type</option>
										<option value="deliver">Home delivery</option>
                                          <option value="pick_up">Customer pickup</option>
								  </select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Delivery Date Time</label>
								<div class="col-md-8">
									<div class="input-group">
										<input type="text" class="form-control datepicker required_field"  disabled="disabled" id="order_delivery_date" name="order_delivery_date" placeholder="Date"/>
										<span class="input-group-addon">&</span>
										<input type="text" class="form-control timepicker required_field"  disabled="disabled" id="order_delivery_time" name="order_delivery_time" placeholder="Time"/>
									</div>									
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Delivery address</label>
								<div class="col-md-8">
									  <input type="text" class="form-control required_field"  disabled="disabled" id="order_delivery_address" name="customer_address" placeholder="">
								</div>
							</div>
							
							
								<div class="form-group">
								<label class="col-md-4 control-label">Payment By</label>
								<div class="col-md-8">
									<select class="form-control required_field" disabled="disabled" name="customer_payment_type" id="order_paymentmethod">
									    <option value="">Choose Payment Method</option>
									 <option value="cash">Cash</option>
                                      <option value="card">Card</option>
                                      <option value="cheque">Cheque</option>
								  </select>
								</div>
							</div>
							
							
							 
							<div class="form-group">
								<label class="col-md-4 control-label">Note</label>
								<div class="col-md-8">
									  <input type="text"  disabled="disabled"  class="form-control" id="order_note" name="customer_note" placeholder="">
								</div>
							</div>
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Total Amount Paid</label>
								<div class="col-md-8">
									<div class="input-group"><input type="text" readonly="" class="form-control required_field total_paid" id="order_totalamount" name="customer_total_amount_paid" placeholder=""></div>
								</div>
							</div>

							
							<div class="form-group">
								<label class="col-md-4 control-label">Discount applied</label>
								<div class="col-md-8">
									<div class="input-group"> <input type="text" readonly="" class="form-control  coupon_discount" id="order_disamount" name="customer_discount" placeholder=""></div>
								</div>
							</div>
							
							
							<div class="form-group">
								<label class="col-md-4 control-label">Net payable amount</label>
								<div class="col-md-8">
									<div class="input-group"><input type="text" readonly="" class="form-control required_field net_paid" id="order_netpayable" name="Customer_Net_payble_amount" placeholder=""></div>
								</div>
							</div>
							 
							<div class="form-group">
								<label class="col-md-4 control-label">Payment Status</label>
								<div class="col-md-8">
									<div class="input-group">
										<input type="hidden" class="setorderid" name="setorderid" />
									<select class="form-control setPaymentStatus" name="setPaymentStatus" id="setPaymentStatus">
										<option value="">Choose Payment Status</option>
										<option value="1">Paid</option>
										<option value="0">Not Paid</option>
									</select>		
								</div>
								</div>
							</div>
							
							<div class="form-group">
							 
								<div class="col-md-8 col-sm-offset-4">
									   <button type="submit" class="btn btn-default save-event waves-effect waves-light" name="savedata" value="savedata">Update Order</button>
								</div>
							</div>
							
						 
					<?php echo form_close(); ?>
					
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>						
						<button type="button"  id="ids_delete" data-id="" class="btn btn-danger delete-order waves-effect waves-light">Delete order</button>
					</div>
				</div>
			</div>
		</div> 
	
<?php $this->load->view('Template/footer.php') ?>
<script src="<?php echo base_url() ?>assets/plugins/moment/moment.js"></script>
<script src='<?php echo base_url() ?>assets/plugins/fullcalendar/js/fullcalendar.min.js'></script>
<script src="<?php echo base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".select1").select2({
    minimumInputLength: 2
});
})
</script>
<script type="text/javascript">
	
	  
	!function($) { 

		var CalendarApp = function() {
			this.$body = $("body")
			this.$modal = $('#addorder-modal'),
			this.$editmodal = $('#editorder-modal'), 
			this.$calendar = $('#calendar'),			
			this.$orderForm = $('#addorderform'),
			this.$calendarObj = null
		};


		 
		/* on click on event */
		CalendarApp.prototype.onEventClick =  function (calEvent, jsEvent, view) {
			console.log(calEvent); 
			var $this = this; 
				$this.$editmodal.find('input#order_customername').val(calEvent.name);
                $this.$editmodal.find('input#edit_id').val(calEvent.id);
                
                
                $this.$editmodal.find('.select_name').val(calEvent.customer_id);
				$this.$editmodal.find('input#order_customercontact').val(calEvent.contact);
				$this.$editmodal.find('input.setCustomerProduct').val(calEvent.product);
				$this.$editmodal.find('input#order_customerquantity').val(calEvent.quantity);
				$this.$editmodal.find('input#order_coupon').val(calEvent.coupon);
				$this.$editmodal.find('select#order_pickuptype').val(calEvent.order_pickuptype);
				$this.$editmodal.find('input#order_delivery_address').val(calEvent.delAddress);
				$this.$editmodal.find('input#order_delivery_date').val(calEvent.delDate);
				$this.$editmodal.find('input#order_delivery_time').val(calEvent.delTime);
				$this.$editmodal.find('select#order_paymentmethod').val(calEvent.paymentBy);
				$this.$editmodal.find('input#order_note').val(calEvent.note);
				$this.$editmodal.find('input#order_totalamount').val(calEvent.totalAmount);
				$this.$editmodal.find('input#order_disamount').val(calEvent.discountAmount);
				$this.$editmodal.find('input#order_netpayable').val(calEvent.netPayAmount);
				$this.$editmodal.find('select#setPaymentStatus').val(calEvent.payment_status);
				$this.$editmodal.find('input.setorderid').val(calEvent.order_id);
				
				if(calEvent.payment_status == '0')
				{
					$('.save-event').prop('disabled',false);
					$('select#setPaymentStatus').prop('disabled',false);
				}
				else
				{
					$('.save-event').prop('disabled',true);
					$('select#setPaymentStatus').prop('disabled',true);
				}
				$this.$editmodal.modal({
					backdrop: 'static'
				});
				$this.$editmodal.find('.delete-order').unbind('click').click(function () {
					if(confirm("Order Want to delete?")){
						$this.$calendarObj.fullCalendar('removeEvents', function (ev) {
						  
							return (ev._id == calEvent._id);
						});
                        
                        
                           $.ajax({
                              type: 'POST',
                              url: '<?php echo base_url(); ?>Calendar/WorkorderDelete',
                              data:{'edit_id':calEvent._id,'order_id':calEvent.order_id},
                              success: function(data_res) {
								
                              	$this.$editmodal.modal('hide');
								
                              }
                             }); 
                        
                        
                        
					
					}
				});
			
			
				$this.$editmodal.find('form').on('submit', function (e) {
					var formStatus = checkForm($(this)); 
					if(!formStatus){
						e.preventDefault();
						return false;
					}
				//	calEvent.title = $(this).find("input[name=order_customername]").val();
				//	$this.$calendarObj.fullCalendar('updateEvent', calEvent);
				 // $this.$editmodal.modal('hide');
					
				});
		},
		/* on select */
		CalendarApp.prototype.onSelect = function (start, end, allDay) {
			 
			var $createOrderDate = new Date(start);	
			$('#order_delivery_date_add').datepicker("update", $createOrderDate);
			$('#order_delivery_time_add').timepicker('update', $createOrderDate);
			var $this = this;
				$this.$modal.modal({
					backdrop: 'static'
				});
				 
		},
		 
		 
		CalendarApp.prototype.init = function() {
		  
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();
			var form = '';
			var today = new Date($.now());
            console.log(today);

     var defaultEvents =[];
        var defaultEvents1 = <?php echo $Events; ?>;
         console.log(defaultEvents1);
         
       $.each(defaultEvents1, function(vals)
         {
            console.log(defaultEvents1[vals].delDate);
            var gh = new Date(defaultEvents1[vals].start);
            console.log(gh);
            defaultEvents.push(
            { id: defaultEvents1[vals].id,
					title: defaultEvents1[vals].name, 
					name: defaultEvents1[vals].name,
                    customer_id:defaultEvents1[vals].customer_id,
					contact: defaultEvents1[vals].contact,
					product: defaultEvents1[vals].product,
					quantity: defaultEvents1[vals].quantity,
					coupon: defaultEvents1[vals].coupon, 
					delDate: defaultEvents1[vals].delDate,
					delTime: defaultEvents1[vals].delTime,
					delAddress: defaultEvents1[vals].delAddress, 
					order_pickuptype:defaultEvents1[vals].order_pickuptype,
					paymentBy: defaultEvents1[vals].paymentBy,
					note: defaultEvents1[vals].note,  
					totalAmount:defaultEvents1[vals].totalAmount,  
					discountAmount: defaultEvents1[vals].discountAmount,  
					netPayAmount: defaultEvents1[vals].netPayAmount,  
					start: gh,
					order_id:defaultEvents1[vals].orderId,
					payment_status: defaultEvents1[vals].paymentStatus,
					coupon_id : defaultEvents1[vals].coupon_id,
					className: 'bg-purple'
				});
        });
   
	/*		var defaultEvents2 =  [{
					id: 293,
					title: 'Hey!', 
					name: 'Hhg',
					contact: '11111!',
					product: 'abc!',
					quantity: 'welcome!',
					coupon: 'test123', 
					delDate: '',
					delTime: '11:00 AM',
					delAddress: 'HawksCode!', 
					order_pickuptype:'Home Delivery',
					paymentBy: 'Cash',
					note: 'Lorem ipsum dolor',  
					totalAmount: '80.00',  
					discountAmount: '0.00',  
					netPayAmount: '80.00',  
					start: new Date($.now() + 158000000),
					className: 'bg-purple'
				}, { 
					title: 'See John Deo',
					id: 293, 
					name: 'See John Deo!',
					contact: '11111!',
					product: 'abc!',
					quantity: 'welcome!',
					coupon: '',
					delDate: '',
					delTime: '12:00 AM',
					delAddress: 'HawksCode!',
					order_pickuptype:'Home Delivery',
					paymentBy: 'Cash',
					note: '',  
					totalAmount: '100.00',  
					discountAmount: '0.00',  
					netPayAmount: '100.00', 
					start: today,
					end: today,
					className: 'bg-danger'
				}, {
					id: 296,
					title: 'Hey!', 
					name: 'Hey!',
					contact: '11111!',
					product: 'abc!',
					quantity: 'welcome!',
					coupon: '',
					order_pickuptype:'Home Delivery',
					delDate: '',
					delTime: '10:00 AM',		
					delAddress: 'HawksCode!',
					paymentBy: 'Cash',
					note: '',  
					totalAmount: '90.00',  
					discountAmount: '0.00',  
					netPayAmount: '90.00', 
					start: new Date($.now() + 338000000),
					className: 'bg-primary'
				}];
                */

			var $this = this;
			$this.$calendarObj = $this.$calendar.fullCalendar({
				slotDuration: '00:15:00', /* If we want to split day time each 15minutes */
				minTime: '08:00:00',
				maxTime: '19:00:00',  
				defaultView: 'month',  
				handleWindowResize: true,   
				height: $(window).height() - 200,   
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				events: defaultEvents,
				editable: false,
				droppable: false, // this allows things to be dropped onto the calendar !!!
				eventLimit: false, // allow "more" link when too many events
				selectable: true, 
				select: function (start, end, allDay) { $this.onSelect(start, end, allDay); },
				eventClick: function(calEvent, jsEvent, view) { $this.onEventClick(calEvent, jsEvent, view); }

			});

			//on new event
			this.$orderForm.on('submit', function(e){ 
				var formStatus = checkForm($(this)); 
				if(!formStatus){
					e.preventDefault();
				}
			});
		},

	   //init CalendarApp
		$.CalendarApp = new CalendarApp, $.CalendarApp.Constructor = CalendarApp

	}(window.jQuery),

	//initializing CalendarApp
	function($) {
		"use strict";
		$.CalendarApp.init()
	}(window.jQuery);
	
	function checkForm($form){
	var check_required_field=''; 
	$($form).find(".required_field").each(function(){ 
		if (!$(this).val()){
			check_required_field =$(this).size(); 
			$(this).css("border-color","red");
		}
		$(this).on('keypress change',function(){
			$(this).css("border-color","");
		});
		});
		if(check_required_field)
		{ 
			return false;
		}
		else{
			return true;
		}
		
	} 

	jQuery('.timepicker').timepicker({defaultTIme : true,minuteStep : 15,autoclose: true,});
	jQuery('.datepicker').datepicker({
			autoclose: true,
			todayHighlight: true,
			format: 'dd-mm-yyyy',
	});
	console.log(new Date("October 13, 2014 11:13:00"));
	
    
$(".select_name").on("change",function()
{
    var option = $('option:selected', this).attr('data-phone');
  //  alert(option);
   $("#order_customercontact").val(option); 
});


$(".product_change").on("change",function()
{
   var coupon_val =  $(".coupon_discount").val();
   var quantity = $("#order_customerquantity").val();
var option1 = $('option:selected', this).attr('data-price');  
option1 = option1*quantity;
$(".total_paid").val(option1); 
if(!coupon_val)
{
$(".net_paid").val(option1); 
}
else
{
    
   var overall1 = ((coupon_val)/100)*option1;
   var finals1 = option1 - overall1; 
   $(".net_paid").val(finals1); 
}

});


$("#order_customerquantity").on("keyup change paste blur",function()
{
     var coupon_val =  $(".coupon_discount").val();
      var total_amount = $('option:selected', '.product_change').attr('data-price');  
     
    var quantity = $(this).val();
      var option1 = total_amount*quantity;
      if(!coupon_val)
{
    $(".total_paid").val(option1); 
$(".net_paid").val(option1); 
}
else
{
    $(".total_paid").val(option1); 
   var overall1 = ((coupon_val)/100)*option1;
   var finals1 = option1 - overall1; 
   $(".net_paid").val(finals1); 
}
      
      
});

$(".Apply_coupon").on("click",function()
{
    var coupon = $(".coupon_code").val();
 var product =  $(".product_change").val();
 
 if(product)
 {
    if(coupon)
    {
  $.ajax({
  type: 'POST',
  url: '<?php echo base_url(); ?>Workorder/checkCoupon',
  data:{code:coupon},
  success: function(data_res) {
  var datas = $.parseJSON(data_res);
  console.log(datas);
  if(datas.msg =="no")
  {
      var total_amount = $(".total_paid").val(); 
 
   $(".net_paid").val(total_amount);
     $(".coupon_discount").val("");
 $(".change_p").replaceWith('<p class="change_p p_danger">No such coupon code</p>');
  }
  else if(datas.msg =="yes")
  {
   var total_amount = $(".total_paid").val(); 
   var overall = ((datas.percent)/100)*total_amount;
   var finals = total_amount - overall;
   $(".net_paid").val(finals); 
    $(".coupon_discount").val(datas.percent);
   $(".change_p").replaceWith('<p class="change_p p_success">Discount '+datas.percent+'% Applied successfully </p>'); 
  }
  else
  {
     var total_amount = $(".total_paid").val(); 
 
   $(".net_paid").val(total_amount);
   
     $(".coupon_discount").val("");
 $(".change_p").replaceWith('<p class="change_p p_danger">Something went wrong ! Try Again</p>');
  }

  
  
  }
 });
 }
 else
 {
    alert("Please fill coupon code first");
 }
 }
 else
 {
    alert("Please select product first");
 }
 
});
</script>
    
     
</script>