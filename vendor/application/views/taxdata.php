<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); ?>
<style type="text/css">

</style>

<div class="content-page">
<div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">

                <div class="col-md-12">
                   <h4 class="page-title">Tax Report</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li class="active">Tax Report</li>
                  </ol>
                  </div>
            </div>
            <?php 
            if(count($tax) > 0)
            {
            ?>
            
            <div class="row">
        <div class="col-md-12">
    <div class="card-box table-responsive">
    <table class="table table-striped table-bordered table_shop_custom">
    <thead>
    <tr>

    <th>Tax Report Name</th>
    <th>Action</th>
    </tr>
    </thead>
    <tbody>
        <?php 
        foreach($tax as $taxList)
        {
        ?>  
        <tr>
        <td><?php echo $taxList->houdinv_tax_name." (".$taxList->houdinv_tax_percenatge."%)" ?></td>
        <td>
        <a href="<?php echo base_url(); ?>Accounts/viewreport/<?php echo $taxList->houdinv_tax_id  ?>" class="btn btn-default m-r-5"><i class="fa fa-eye"></i></a>
        </td>
        </tr>
        <?php }
        ?>
         
        </tbody>
    </table>
    </div>
    </div>
    </div>
            <?php }
            else
            {
            ?>
             <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                    <div class="inner-box-add-tax text-center">
                        <div class="col-md-12">
                            <a href="<?php echo base_url(); ?>Setting/taxsetting" class="btn btn-default"><i class="fa fa-plus"></i></a>
                            </div>
                            <div class="clearfix"></div>
                            <h3 class="m-t-20">Please add tax to view the tax report</h3>
                            </div>
                    </div>
                </div>
            </div>
            <?php }
            ?>
           
            
          
                      
                      </div>
                    </div>
                  </div>
 <?php $this->load->view('Template/footer.php') ?>

