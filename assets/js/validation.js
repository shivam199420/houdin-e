// Number validation
$(document).ready(function() {
    $(".number_validation").keydown(function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
    (e.keyCode >= 35 && e.keyCode <= 40)) {
    return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
    }
    });
    // Name validation
    $(document).on('keydown', '.name_validation', function(e) {
    if (e.which === 32 &&  e.target.selectionStart === 0) {return false;}  });
    });
    //Email Validation
    jQuery(document).ready(function(){
    function ValidateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
    };
    jQuery(".email_validation").blur(function () {
    if (!ValidateEmail(jQuery(this).val())) {
    jQuery(this).val("");
    }
    else {
    return  true;
    }
    });
    });
    // DatePicker Value
    $(document).ready(function(){
        $('.date_picker').focus(function(){
            $(this).daterangepicker({                    
            singleDatePicker: true,
            showDropdowns: false,    
            locale: { 
                format: 'YYYY-MM-DD',
            }
            });      
        });   
    });
    // check final step of registration process
    $(document).on('keyup','.cpass',function(){
        var getOriginalPass = $('.opass').val();
        var getconfirmPass = $(this).val();
        if(getOriginalPass == getconfirmPass)
        {
            $('.setStartBtn').attr('disabled',false);
            $(this).css('border','1px solid #d3d3d3');
        }
        else
        {
            $('.setStartBtn').attr('disabled',true);
            $(this).css('border','1px solid red');
        }
    });
    // check revers
    $(document).on('keyup','.opass',function(){
        var getOriginalPass = $(this).val();
        var getconfirmPass = $('.cpass').val();
        if(getOriginalPass == getconfirmPass)
        {
            $('.setStartBtn').attr('disabled',false);
        }
        else
        {
            $('.setStartBtn').attr('disabled',true);
        }
    });
    
    
    // forget pass validation
    

  
      