<!DOCTYPE html>
<html>
<head> 
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon_icon.png">
<title>Houdin-e - Forget Password</title>
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/login_custom.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
</head>
<body>

		<div class="account-pages"></div>
		<div class="clearfix"></div>
		<div class="wrapper-page">
			<div class=" card-box card_box_custom">
        <div class="panel-heading">
        <h3 class="text-center"> <img src="<?php echo base_url(); ?>assets/images/Logo_F_W.png" class="logo_head"/></h3>
        </div>
				<div class="panel-body">
				<?php 
				if($this->session->flashdata('error'))
				{
					echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
				}
				?>
				<?php echo form_open(base_url( 'Forget' ), array( 'id' => 'forgetPasswordForm', 'class' => 'text-center' ));?>
						<div class="alert alert-info alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
								×
							</button>
							Enter your <b>Email</b> and instructions will be sent to you!
						</div>
						<div class="form-group m-b-0">
							<div class="input-group">
								<input type="email" name="adminEmail" class="form-control required_validation_for_forget_password name_validation email_validation" placeholder="Enter Email">
								<span class="input-group-btn">
									<input type="submit" value="Reset" class="btn btn_theme_dark" name="adminForgetPasswordBtn"/>
								</span>
							</div>
						</div>
						<?php echo form_close();?>
				</div>
			</div>
		</div>

    <script>
    var resizefunc = [];
    </script>
    <!-- jQuery  -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/validation.js"></script>
    </body>
	</html>
	<!-- CLient side form validation -->
	<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#forgetPasswordForm',function(){
			var check_required_field='';
			$(".required_validation_for_forget_password").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>