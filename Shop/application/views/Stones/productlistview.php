<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');


$images_all = json_decode($product['main_product']->houdinv_products_main_images);
$price_all = json_decode($product['main_product']->houdin_products_price,true);
$final_price = $product['main_product']->houdin_products_final_price;
$overall_rating = $product['reviews_sum']->houdinv_product_review_rating;
$total_rating = count($product['reviews']);
$final_rating = $overall_rating/$total_rating;
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
if($currencysymbol=="USD")
{
  $setCurrencySymbol =  "$";
}else if($currencysymbol=="AUD"){
  $setCurrencySymbol =  "$";
}else if($currencysymbol=="Euro"){
  $setCurrencySymbol =  "£";
}else if($currencysymbol=="Pound"){
  $setCurrencySymbol =  "€";
}else if($currencysymbol=="INR"){
  $setCurrencySymbol =  "₹";
}
?>
<style>
    .checked {
    color: orange;
}

</style>
    <!-- ================== START MAIN SECTION ================== -->
    <div class="main" role="main">
        <!-- Start page location -->
        <div class="page-location">
            <div class="page-location-layer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-location-title">
                                <h3>Product detail</h3>
                            </div>
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Product detail</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start single product area -->
        <div class="single-product-area">
            <div class="container">
              <?php
              if($this->session->flashdata('error'))
              {
                      echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
              }
              if($this->session->flashdata('success'))
              {
                      echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
              }
              ?>
                <div class="row">

                    <div class="col-lg-6 col-md-12">
                        <div class="flexslider">
                            <ul class="slides">
                            <?php
                                    $setImageData = "";
                                    for($index = 0; $index < count($images_all); $index++)
                                    {
                                        if($images_all[$index])
                                        {
                                            $setNumberData = $index+1;
                                            if($setImageData)
                                            {
                                                $setImageData = $setImageData.",".$this->session->userdata('vendorURL')."upload/productImage/". $images_all[$index]."";
                                            }
                                            else
                                            {
                                                $setImageData = "".$this->session->userdata('vendorURL')."upload/productImage/". $images_all[$index]."";
                                            }
                                            ?>

                                <li data-thumb="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $images_all[$index]; ?>"><img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $images_all[$index]; ?>" alt="" class="img-fluid"></li>
                                <?php } }
                                            // echo "shivam".$setImageData;
                                            ?>
                                            <!--
                                <li data-thumb="images/view/thumb/02.jpg"><img src="images/view/02.jpg" alt="" class="img-fluid"></li>
                                <li data-thumb="images/view/thumb/03.jpg"><img src="images/view/03.jpg" alt="" class="img-fluid"></li>
                                <li data-thumb="images/view/thumb/04.jpg"><img src="images/view/04.jpg" alt="" class="img-fluid"></li> -->
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="single-product-detail mt-lg-0 mt-5">
                            <h4 class="title mt-0"><?php echo $product['main_product']->houdin_products_title; ?></h4>
                            <ul class="review d-flex">
                              <?php for($i=1 ; $i<6 ;$i++)
                                 {
                                 if($i <= $final_rating)
                                 {
                                     $posi = strpos($final_rating,".");
                                     if($posi>=0)
                                     {
                                      $exp =  explode(".",$final_rating);
                                      if($exp[0]==$i)
                                      {
                                         ?>
                                     <li><i class="fa fa-star"></i></li>
                                         <?php
                                         }
                                         else
                                         {
                                             echo '<li><i class="fa fa-star"></i></li>';
                                         }
                                     }
                                     else
                                     {
                                     ?>
                                     <li><i class="fa fa-star"></i></li>
                                     <?php
                                      }
                                  }
                                  else
                                  {
                                     ?>

                                 <li><i class="fa fa-star-o"></i></li>


                               <?php } } ?>


                                <li class="review-number">(<?php echo $total_rating; ?> Reviews)</li>
                            </ul>
                            <h5><?php echo $setCurrencySymbol."".number_format((float)$final_price, 1, '.', ''); ?> <small><del><?php echo $setCurrencySymbol."".number_format((float)$price_all['sale_price'], 1, '.', '');  ?></del></small></h5>
                            <div class="product-info">
                                <p><?php echo $product['main_product']->houdin_products_short_desc; ?></p>
                            </div>

                        <?php if(!empty($product['related'])){?> 
                        <select style="margin-bottom: 36px;width: 50%;" title="Pick a number" class="form-control variantsselect">
                        <option>Select variants </option>
                        <?php 
                        foreach($product['related'] as $related){?>
                        <option value="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>"><?php echo $related->houdinv_products_variants_name; ?></option>
                        <?php } ?>
                        </select>
                        <?php  }?>



                            <h5 class="available">AVAILABILITY : <span> <?php
                                    if($product['main_product']->houdinv_products_total_stocks > 0)
                                    {
                                        echo '<a href="javascript:;">In stock</a>';
                                    }
                                    else
                                    {
                                        echo '<a href="javascript:;" class="text-danger">Out of stock</a>';
                                    }
                                    ?></span></h5>


                            <div class="product-qty">
                                <form>
                                    <label>
                                        <span class="title mt-0">quantity
                                        <input value="1" min="1" data-max="<?php echo $product['main_product']->houdinv_products_total_stocks ?>" name="qttybutton" class="cart-plus-minus-box qty" type="number">

                                        </span>
                                    </label>
                                </form>
                            </div>
                            <div class="product-add">
                                <ul class="d-flex">
                                  <?php
                                  if($product['setcartStatus'] == 'yes')
                                  {
                                    ?>
                                    <li><a class=" btn-default acc_btn cart" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>"><i class="fa fa-shopping-basket" style="color:red;"></i></a></li>
                                  <?php }
                                  else {
                                    ?>
                                    <li><a class=" btn-default acc_btn Add_to_cart_button cart" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>"><i class="fa fa-shopping-basket"></i></a></li>
                                  <?php }
                                  ?>

                                  <?php
                                  if($product['setWishlistStatus'] == 'yes')
                                  {
                                    ?>
                                      <li><a class="btn-default acc_btn btn_icn wishlist" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" class="wishlist"><i class="fa fa-heart" style="color:red;"></i></a></li>
                                  <?php }
                                  else {
                                    ?>
                                    <li><a class="btn-default acc_btn btn_icn Add_to_whishlist_button wishlist" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" class="wishlist"><i class="fa fa-heart"></i></a></li>
                                  <?php }
                                  ?>

                                    <!--<li><a class="btn-default acc_btn btn_icn exchange" ><i class="fa fa-exchange"></i></a></li>-->
                                </ul>
                            </div>
                          <!--  <div class="social-shere">
                                <ul class="d-flex">
                                    <li class="title">Social shere :</li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start comment area -->
        <div class="comment-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Specifications</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">reviews</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <?php echo $product['main_product']->houdin_products_desc; ?>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                              <ul class="Specifications-table">
                                    <li>
                                        <span class="sp-title pull-left">Length</span>
                                        <?php if($product['main_product']->length)
                                        { ?>
                                        <span class="sp-name"><?php echo $product['main_product']->length; ?></span>
                                        <?php } else { ?>
                                        <span class="sp-name">NA</span>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <span class="sp-title pull-left">Height</span>
                                        <?php if($product['main_product']->height)
                                        { ?>
                                        <span class="sp-name"><?php echo $product['main_product']->height; ?></span>
                                        <?php } else { ?>
                                        <span class="sp-name">NA</span>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <span class="sp-title pull-left">Breath</span>
                                        <?php if($product['main_product']->breath)
                                        { ?>
                                        <span class="sp-name"><?php echo $product['main_product']->breath; ?></span>
                                        <?php } else { ?>
                                        <span class="sp-name">NA</span>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <span class="sp-title pull-left">Weight</span>
                                        <?php if($product['main_product']->weight)
                                        { ?>
                                        <span class="sp-name"><?php echo $product['main_product']->weight; ?></span>
                                        <?php } else { ?>
                                        <span class="sp-name">NA</span>
                                        <?php } ?>
                                    </li>

                                </ul>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="row">
                                  <!--
                                    <div class="col-lg-6 show-reviews">
                                        <div class="row ">
                                            <div class="average col-lg-6 text-center">
                                                <h6>Average Rating</h6>
                                                <div class="total-star">
                                                    <i class="fa fa-star"></i>
                                                    <span>(0)</span>
                                                </div>
                                            </div>
                                            <div class="review-list col-lg-6">
                                                <ul>
                                                    <li>
                                                        <span class="starts">5 <i class="fa fa-star"></i> (0)</span> <span class="bar"></span>
                                                    </li>
                                                    <li>
                                                        <span class="starts">4 <i class="fa fa-star"></i> (0)</span> <span class="bar"></span>
                                                    </li>
                                                    <li>
                                                        <span class="starts">3 <i class="fa fa-star"></i> (0)</span> <span class="bar"></span>
                                                    </li>
                                                    <li>
                                                        <span class="starts">2 <i class="fa fa-star"></i> (0)</span> <span class="bar"></span>
                                                    </li>
                                                    <li>
                                                        <span class="starts">1 <i class="fa fa-star"></i> (0)</span> <span class="bar"></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 submit-reviews text-center">
                                        <h4>Have you used this product before?</h4>
                                        <ul class="d-flex justify-content-center">
                                            <li><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                        </ul>
                                        <p>25/04/2018</p>
                                        <p class="rate">Rate it!</p>
                                    </div>

   end -->

                                    <?php
                                    if(!$final_rating > 0)
                                    {
                                        ?>
                                        <div class="col-sm-6 pull-left">
                                         <p>There are no reviews yet.</p>
                                    <p class="text-bigger">Be the first to review “Cloud Wall Clock”</p>
                                    </div>
                                    <?php }
                                    else
                                    {
                                    ?>
                                    <div class="review-section-single col-sm-6 pull-left" style="overflow-y: scroll;height: 330px; overflow-x: hidden;">
                                        <?php
                                        foreach($product['reviews'] as $user_Review)
                                        {
                                        ?>
                                        <div class="review-tab m-b-10">
                                            <div class="review-name-title font600"><?php echo $user_Review->houdinv_product_review_user_name ?></div>
                                            <div class="star">
                                                <div class="form-group m-b-0">
                                                    <div class="strat-rating">
                                                      <?php for($i=1 ; $i<6 ;$i++)
                                                      {
                                                      if($i <= $user_Review->houdinv_product_review_rating)
                                                      {
                                                      ?>
                                                      <i class="fa fa-star"></i>
                                                      <?php
                                                       }
                                                       else
                                                       {
                                                          ?>

                                                      <i class="fa fa-star-o"></i>


                                                      <?php } } ?>

                                                    <span class="text-date"><?php echo $user_Review->houdinv_product_review_created_at ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-description"><?php echo  $user_Review->houdinv_product_review_message; ?></div>
                                        </div>
                                        <?php }
                                        ?>
                                    </div>
                                    <?php }
                                    ?>
                                    <div class="comment-rating col-sm-6" style="    box-shadow: 1px 1px 12px 1px #ddd;padding: 21px;">
                                        <div class="">
                                          <?php echo form_open(base_url( 'Productlistview' ), array( 'id' => 'rtng_form',"class"=>"checkout_form", 'method'=>'post' ));?>
                                              <div class="form-group">

                                              <div class="strat-rating">
                                                  <span data-rating="1" style="font-size: 22px;" class="fa fa-star "></span>
                                                  <span data-rating="2" style="font-size: 22px;" class="fa fa-star "></span>
                                                  <span data-rating="3" style="font-size: 22px;" class="fa fa-star "></span>
                                                  <span data-rating="4" style="font-size: 22px;" class="fa fa-star "></span>
                                                  <span data-rating="5" style="font-size: 22px;" class="fa fa-star"></span>
                                                  <input type="hidden"  name="rating" class="rating_data" value="0" />
                                                  <input type="hidden"  name="product_id" class="product" value="<?php echo $product_id; ?>" />
                                                  <input type="hidden"  name="variant_id" class="" value="0" />
                                              </div>
                                              </div>
                                              <div class="form-group">

                                              <input  class="required_validation_for_review name_validation form-control" type="text" name="user_name" placeholder="Your Name" />
                                              </div>
                                              <div class="form-group">

                                              <input  class="required_validation_for_review name_validation email_validation form-control" type="text" name="user_email" placeholder="Your Email" />
                                              </div>
                                              <div class="form-group">

                                              <textarea  class="required_validation_for_review form-control" name="user_message" rows="3" placeholder="Write a review"></textarea>
                                              </div>
                                              <input class="btn-round submit_form_review" type="submit" name="review_submit" value="Add Review" />
                                              <?php echo form_close(); ?>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- Related Product Area -->
    <?php
    if(count($product['related']) > 0)
    {
    ?>
    <div class="related_prdct_area text-center">
    <div class="container">
    <!-- Section Title -->
    <div class="rp_title text-center"><h3>Related products</h3></div>

    <div class="row">
    <?php foreach($product['related'] as $related)
    {
        ?>


    <div class="col-lg-3 col-md-4 col-sm-6">
    <div class="single_product">
    <div class="product_image">
    <?php 
    if($related->houdin_products_variants_image)
    {
        $setVariantIMageData = $this->session->userdata('vendorURL')."upload/productImage/".$related->houdin_products_variants_image;
    }
    else
    {
        $setVariantIMageData = base_url()."Extra/noPhotoFound.png";
    }
    ?>
    <img src="<?php echo $setVariantIMageData; ?>" style="width: 253px!important;
    height: 310px!important;" alt=""/>
    <div class="box-content">
    <a href="javascript:;" class="Add_to_whishlist_button related_button" data-variant="<?php echo $related->houdin_products_variants_id ?>"  data-cart="0" ><i class="fa fa-heart-o"></i></a>
    <a class="Add_to_cart_button related_button" data-variant="<?php echo $related->houdin_products_variants_id ?>"  data-cart="0"><i class="fa fa-cart-plus"></i></a>
    <a href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>" class="related_button"><i class="fa fa-search"></i></a>
    </div>
    </div>

    <div class="product_btm_text">
    <h4><a href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>"><?php echo $related->houdin_products_variants_title; ?></a></h4>
    <span class="price"> <?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $related->houdinv_products_variants_final_price; ?></span>
    </div>
    </div>
    </div> <!-- End Col -->


     <?php
    }?>


    </div>
    </div>
    </div>
<?php } ?>
    <!-- ================== END MAIN SECTION ================== -->
    <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view(''.$getShopName.'/Template/footer') ?>
    <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('keyup','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        })
        $(document).on('change','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        });
    })
    </script>
