<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ordermodel extends CI_Model{
  function __construct() 
  {
    parent::__construct();
    $this->load->helper('database');
    $getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
  }
  
  
public function FetchOrderData($params=false)
{
    $this->db->select("houdinv_orders.*")->from("houdinv_orders")->order_by("houdinv_orders.houdinv_order_id","desc");
    if(array_key_exists("start",$params) && array_key_exists("limit",$params))
    {
        $this->db->limit($params['limit'],$params['start']);
    }
    elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
    {
        $this->db->limit($params['limit']);
    }
    if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
    {
        $result = $this->db->count_all_results();
    }else
    {
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    $setMainArrayData = array();
    foreach($result as $resultList)
    {
        $setChildArray = array();
        if($resultList['houdinv_order_user_id'] == 0)
        {
            $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
            ->where('houdinv_order_users_order_id',$resultList['houdinv_order_id'])->get()->result();
        }
        else
        {
            $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_id',$resultList['houdinv_order_user_id'])->get()->result();
        }
        $setChildArray['main'] = $resultList;
        $setChildArray['user'] = $getUserData;
        array_push($setMainArrayData,$setChildArray);
    }
    return $setMainArrayData;
}
    
    
    public function allOrderLanding()
    {
        $allCount =   $this->db->select("*")->from("houdinv_orders")->count_all_results();

        $allComplete =   $this->db->select("*")->from("houdinv_orders")->where("houdinv_order_confirmation_status",'Delivered')->count_all_results();
        
        $allCancel =   $this->db->select("*")->from("houdinv_orders")->where("houdinv_order_confirmation_status","cancel")->count_all_results(); 

        $array = array("allCount"=>$allCount,"allComplete"=>$allComplete,"allCancel"=>$allCancel);   
      return $array;  
    }
  
  
   public function FetchOrderDeliverPendingData($params=false)
   {
       $this->db->select("houdinv_orders.*")
       ->from("houdinv_orders")
       ->where("houdinv_orders.houdinv_orders_delivery_status",0)
       ->where("houdinv_orders.houdinv_order_delivery_type",'deliver');
          if( $this->session->userdata('vendorOutlet')>0)
               {
                $this->db->where("houdinv_orders.houdinv_order_confirm_outlet",$this->session->userdata('vendorOutlet')); 
               } 
       $this->db->group_start()
       ->where("houdinv_orders.houdinv_order_confirmation_status","order pickup")
       ->or_where("houdinv_orders.houdinv_order_confirmation_status","billed")
       ->or_where("houdinv_orders.houdinv_order_confirmation_status","assigned")
       ->group_end()
       ->order_by("houdinv_orders.houdinv_order_id","desc");         
        if(array_key_exists("start",$params) && array_key_exists("limit",$params))
        {
            $this->db->limit($params['limit'],$params['start']);
        }
        elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
        {
            $this->db->limit($params['limit']);
        }
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
        {
            $result = $this->db->count_all_results();
        }else
        {
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        $setMainArrayData = array();
        foreach($result as $resultList)
        {
            $setChildArray = array();
            // get transaction date
            $getTransactiondate = $this->db->select('houdinv_transaction_date')->from('houdinv_transaction')->where('houdinv_transaction_for_id',$resultList['houdinv_order_id'])->get()->result();
            if(count($getTransactiondate) > 0)
            {
                $setTransactionData = date('d-m-Y',strtotime($getTransactiondate[0]->houdinv_transaction_date));
            }
            else
            {
                $setTransactionData = "";
            }
            // get delivery service data
            $getorderdelivery = $this->db->select('*')->from('houdinv_deliveries')->where('houdinv_delivery_order_id',$resultList['houdinv_order_id'])->get()->result();
            if(count($getorderdelivery) > 0)
            {
                if($getorderdelivery[0]->houdinv_delivery_by == 'Delivery Boy')
                {
                    $getDetailsdata = $this->db->select('staff_name')->from('houdinv_staff_management')->where('staff_id',$getorderdelivery[0]->houdinv_delivery_boy_id)->get()->result();
                    $setName = $getDetailsdata[0]->staff_name;
                }
                else if($getorderdelivery[0]->houdinv_delivery_by == 'Delivery Service')
                {
                    $setName = 'Courier Service';
                }
                else
                {
                    $setName = 'Customer Pickup';
                }
            }
            else
            {
                $setName = 'Customer Pickup';
            }
            if($resultList['houdinv_order_user_id'] == 0)
            {
                $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
                ->where('houdinv_order_users_order_id',$resultList['houdinv_order_id'])->get()->result();

            }
            else
            {
                $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_id',$resultList['houdinv_order_user_id'])->get()->result();
            }
            $setChildArray['main'] = $resultList;
            $setChildArray['user'] = $getUserData;
            $setChildArray['service'] = $setName;
            $setChildArray['transaction'] = $setTransactionData;
            array_push($setMainArrayData,$setChildArray);
        }
        return $setMainArrayData;
   }
  
     public function FetchOrderCompleteData($params=false)
    {
        $this->db->select("houdinv_orders.*")
        ->from("houdinv_orders")
        ->where('houdinv_orders.houdinv_order_confirmation_status','Delivered')
        ->order_by("houdinv_orders.houdinv_order_id","desc");
        
           if( $this->session->userdata('vendorOutlet')>0)
               {
                $this->db->where("houdinv_orders.houdinv_order_confirm_outlet",$this->session->userdata('vendorOutlet')); 
               } 
      
        if(array_key_exists("start",$params) && array_key_exists("limit",$params))
        {
            $this->db->limit($params['limit'],$params['start']);
        }
        elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
        {
            $this->db->limit($params['limit']);
        }
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
        {
            $result = $this->db->count_all_results();
        }
        else
        {
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        $setMainArrayData = array();
        foreach($result as $resultList)
        {
            $setChildArray = array();
            if($resultList['houdinv_order_user_id'] == 0)
            {
                $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
                ->where('houdinv_order_users_order_id',$resultList['houdinv_order_id'])->get()->result();
            }
            else
            {
                $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_id',$resultList['houdinv_order_user_id'])->get()->result();
            }
            $setChildArray['main'] = $resultList;
            $setChildArray['user'] = $getUserData;
            array_push($setMainArrayData,$setChildArray);
        }
            return $setMainArrayData;
   }
  
  public function FetchOrderPickupData($params=false)
   {
       $this->db->select("houdinv_orders.*")
       ->from("houdinv_orders")->order_by("houdinv_orders.houdinv_order_id","desc")
       ->where("houdinv_orders.houdinv_order_delivery_type",'pick_up');  
          if( $this->session->userdata('vendorOutlet')>0)
               {
                $this->db->where("houdinv_orders.houdinv_order_confirm_outlet",$this->session->userdata('vendorOutlet')); 
               } 
                
        if(array_key_exists("start",$params) && array_key_exists("limit",$params))
        {
            $this->db->limit($params['limit'],$params['start']);
        }
        elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
        {
            $this->db->limit($params['limit']);
        }
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
        {
            $result = $this->db->count_all_results();
        }else
        {
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        $setMainArrayData = array();
        foreach($result as $resultList)
        {
            $setChildArray = array();
            if($resultList['houdinv_order_user_id'] == 0)
            {
                $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
                ->where('houdinv_order_users_order_id',$resultList['houdinv_order_id'])->get()->result();
            }
            else
            {
                $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_id',$resultList['houdinv_order_user_id'])->get()->result();
            }
            $setChildArray['main'] = $resultList;
            $setChildArray['user'] = $getUserData;
            array_push($setMainArrayData,$setChildArray);
        }
        return $setMainArrayData;
   }
   
   public function orderDetail($data)
   {
       $this->db->select("houdinv_orders.*,houdinv_users.houdinv_user_id,houdinv_users.houdinv_user_name,houdinv_users.houdinv_user_email,
       houdinv_users.houdinv_user_contact,houdinv_shipping_warehouse.w_name,houdinv_shipping_warehouse.w_pickup_address,houdinv_shipping_warehouse.w_pickup_city,
       houdinv_deliveries.houdinv_delivery_by,houdinv_deliveries.houdinv_delivery_boy_id,houdinv_deliveries.houdinv_delivery_courier_id")
       ->from("houdinv_orders")
       ->join("houdinv_users","houdinv_users.houdinv_user_id=houdinv_orders.houdinv_order_user_id",'left outer')
       ->join('houdinv_shipping_warehouse','houdinv_shipping_warehouse.id = houdinv_orders.houdinv_order_confirm_outlet','left outer')
       ->join('houdinv_deliveries','houdinv_deliveries.houdinv_delivery_order_id = houdinv_orders.houdinv_order_id','left outer')
       ->where("houdinv_orders.houdinv_order_id",$data);
       $query = $this->db->get()->row();
        
        // fetch order confirm by
        if($query->houdinv_orders_confirm_by_role == 0)
        {
            $masterDB = $this->load->database('master', TRUE);
            $getConfirmByInfo = $masterDB->select('houdin_user_name,houdin_user_email,houdin_user_contact')->from('houdin_users')->where('houdin_user_id',$query->houdinv_orders_confirm_by)->get()->result();            
            $setConfirmByArray = array('name'=>$getConfirmByInfo[0]->houdin_user_name,'email'=>$getConfirmByInfo[0]->houdin_user_email,'phone'=>$getConfirmByInfo[0]->houdin_user_contact);
        }
        else
        {
            $getConfirmByInfo = $this->db->select('staff_name,staff_contact_number,staff_email')->from('houdinv_staff_management')->where('staff_id',$query->houdinv_orders_confirm_by)->get()->result();
            $setConfirmByArray = array('name'=>$getConfirmByInfo[0]->staff_name,'email'=>$getConfirmByInfo[0]->staff_email,'phone'=>$getConfirmByInfo[0]->staff_contact_number);
        }
        // fetch billing and shipping info
        $getBillingAddress = $this->db->select('*')->from('houdinv_user_address')->where('houdinv_user_address_id',$query->houdinv_order_billing_address_id)->get()->result();
        $getShippingAddress = $this->db->select('*')->from('houdinv_user_address')->where('houdinv_user_address_id',$query->houdinv_order_delivery_address_id)->get()->result();

        //  get delivery boy detail
        if($query->houdinv_delivery_by == 'Delivery Boy')
        {
            $getDeliveryBoyDetails = $this->db->select('staff_name,staff_contact_number,staff_email')->from('houdinv_staff_management')->where('staff_id',$query->houdinv_delivery_boy_id)->get()->result();
            $setDeliveryArray = array('name'=>$getDeliveryBoyDetails[0]->staff_name,'number'=>$getDeliveryBoyDetails[0]->staff_contact_number,'email'=>$getDeliveryBoyDetails[0]->staff_email);
        }
        else
        {
            $setDeliveryArray = array();
        }
        // get main product list data
        $setProductMainArray = array();
        if($query->houdinv_confirm_order_product_detail)
        {
            $setProductDeatails = json_decode($query->houdinv_confirm_order_product_detail,true);
            for($index = 0; $index < count($setProductDeatails); $index++)
            {
                if($setProductDeatails[$index]['product_id'] != 0 && $setProductDeatails[$index]['product_id'] != "")
                {
                    $getProuductTitle = $this->db->select('houdin_products_title')->from('houdinv_products')->where('houdin_products_id',$setProductDeatails[$index]['product_id'])->get()->result();
                    $setProductTitle = $getProuductTitle[0]->houdin_products_title;
                }
                else
                {
                    $getProuductTitle = $this->db->select('houdin_products_variants_title')->from('houdinv_products_variants')->where('houdin_products_variants_id',$setProductDeatails[$index]['variant_id'])->get()->result();
                    $setProductTitle = $getProuductTitle[0]->houdin_products_variants_title;
                }
                $setProductMainArray[] = array('productName'=>$setProductTitle,'quantity'=>$setProductDeatails[$index]['product_Count'],'price'=>$setProductDeatails[$index]['product_actual_price'],'total'=>$setProductDeatails[$index]['total_product_paid_Amount'],'saleprice'=>$setProductDeatails[$index]['saleprice'],'totaltax'=>$setProductDeatails[$index]['totaltax']);
            }
        }
        else
        {
            $setProductDeatails = json_decode($query->houdinv_order_product_detail,true);
            for($index = 0; $index < count($setProductDeatails); $index++)
            {
                if($setProductDeatails[$index]['product_id'] != 0 && $setProductDeatails[$index]['product_id'] != "")
                {
                    $getProuductTitle = $this->db->select('houdin_products_title')->from('houdinv_products')->where('houdin_products_id',$setProductDeatails[$index]['product_id'])->get()->result();
                    $setProductTitle = $getProuductTitle[0]->houdin_products_title;
                }
                else
                {
                    $getProuductTitle = $this->db->select('houdin_products_variants_title')->from('houdinv_products_variants')->where('houdin_products_variants_id',$setProductDeatails[$index]['variant_id'])->get()->result();
                    $setProductTitle = $getProuductTitle[0]->houdin_products_variants_title;
                }
                $setProductMainArray[] = array('productName'=>$setProductTitle,'quantity'=>$setProductDeatails[$index]['product_Count'],'price'=>$setProductDeatails[$index]['product_actual_price'],'total'=>$setProductDeatails[$index]['total_product_paid_Amount']);
            }
        }

        // print_r($getBillingAddress);
        // die;
        return array('mainData'=>$query,'confirmBy'=>$setConfirmByArray,'billingAddress'=>$getBillingAddress,'shippingAddress'=>$getShippingAddress,'deliveryData'=>$setDeliveryArray,'mainProductData'=>$setProductMainArray);
   }
   
   public function productById($id)
   {
    
   $data =  $this->db->select("houdin_products_category,houdin_products_sub_category_level_1,houdin_products_sub_category_level_2,houdin_products_id,houdin_products_title,houdinv_products_main_images")
    ->from("houdinv_products")
    ->where("houdin_products_id",$id)->get()->row();
    
    $cat1 = explode(",",$data->houdin_products_category);
    
    
   $main_category = $this->db->select("houdinv_category_name,houdinv_category_id")->from("houdinv_categories")->where_in("houdinv_category_id",$cat1)->get()->result(); 
   
   $sub_category1 = $this->db->select("houdinv_sub_category_one_id,houdinv_sub_category_one_main_id,houdinv_sub_category_one_name")->from("houdinv_sub_categories_one")->where_in("houdinv_sub_category_one_id",explode(",",$data->houdin_products_sub_category_level_1))->get()->result(); 
   
   $sub_category2 = $this->db->select("houdinv_sub_category_two_id,houdinv_sub_category_two_sub1_id,houdinv_sub_category_two_main_id,houdinv_sub_category_two_name")->from("houdinv_sub_categories_two")->where_in("houdinv_sub_category_two_id",explode(",",$data->houdin_products_sub_category_level_2))->get()->result();
   
   $product = array("product_main"=>$data,"main_cat"=>$main_category,"sub_cat1"=>$sub_category1,"sub_cat2"=>$sub_category2); 
   
   return $product;
    
   }
   public function getUnbilledOrderCount()
   {
       $getCount = $this->db->select('COUNT(houdinv_order_id) AS totalUnbilledOrder')->from('houdinv_orders')->where('houdinv_order_confirmation_status','unbilled')->get()->result();
       return array('totalRows'=>$getCount[0]->totalUnbilledOrder);
   }
   public function getUnbilledOrderDetails($data)
   {
    $this->db->select('houdinv_orders.*,houdinv_users.houdinv_user_name,houdinv_users.houdinv_user_email,houdinv_users.houdinv_user_contact')->from('houdinv_orders')->where('houdinv_orders.houdinv_order_confirmation_status','unbilled')
    ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_orders.houdinv_order_user_id','left outer');
    if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getUnbilleddata = $this->db->get();
        $resultUnbilleddata = ($getUnbilleddata->num_rows() > 0)?$getUnbilleddata->result():FALSE;
        return array('unbilledData'=>$resultUnbilleddata);
   }
   public function fetchUnbilledData($data)
   {
       $setData = "";
       $getUnbilledDataValue = $this->db->select('houdinv_orders.*,houdinv_user_address.*')
       ->from('houdinv_orders')->where('houdinv_orders.houdinv_order_id',$data)
       ->join('houdinv_user_address','houdinv_user_address.houdinv_user_address_id = houdinv_orders.houdinv_order_delivery_address_id','left outer')
       ->get()->result();
    //    $setArray = array('paymentStatus'=>$getUnbilledDataValue[0]->houdinv_payment_status,'customerRequest'=>$getUnbilledDataValue[0]->houdinv_order_product_detail);
       $getProductDetails = json_decode($getUnbilledDataValue[0]->houdinv_order_product_detail,true);
       for($index = 0; $index < count($getProductDetails); $index++)
       {
           if($getProductDetails[$index]['variant_id'] == 0)
           {
                $getProductName = $this->db->select('houdin_products_title,houdinv_products_total_stocks,houdin_products_final_price,houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getProductDetails[$index]['product_id'])->get()->result();
                $setProductName = $getProductName[0]->houdin_products_title;
                $setProductStock = $getProductName[0]->houdinv_products_total_stocks;
                $setProductFinalPrice = $getProductName[0]->houdin_products_final_price;
                $getPriceData = json_decode($getProductName[0]->houdin_products_price,true);
                
                if($getPriceData['tax_price'])
                {
                    $getTax = getTaxamount($getPriceData['tax_price']);
                    $getTaxAmount = ($setProductFinalPrice*$getTax)/100;
                    $singleTax = round($getTaxAmount);
                    $getTaxAmount = round($getTaxAmount)*$getProductDetails[$index]['product_Count'];
                }
                else
                {
                    $getTaxAmount = 0;
                    $singleTax = 0;
                }
                $setProductFinalPrice = ($setProductFinalPrice-$singleTax)*$getProductDetails[$index]['product_Count'];
                $getSalePrice = $getProductDetails[$index]['product_actual_price']-$singleTax;
           }
           else
           {
                $getProductName = $this->db->select('houdin_products_variants_title')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getProductDetails[$index]['variant_id'])->get()->result();
                $setProductName = $getProductName[0]->houdin_products_variants_title;
                $setProductStock = $getProductName[0]->houdinv_products_variants_total_stocks;
                $setProductFinalPrice = $getProductName[0]->houdinv_products_variants_final_price;
                $getTaxAmount = 0;
                $setProductFinalPrice = $setProductFinalPrice-$getTaxAmount;
                $getSalePrice = $getProductDetails[$index]['product_actual_price']-$getTaxAmount;
           }
           if($setData)
            {
                $setData = $setData.'<tr class="mainUnbilledRow">
                <!--<td><input type="checkbox" class="childUnbilleCheck"></td>-->
                <input type="hidden" name="productId[]" value="'.$getProductDetails[$index]['product_id'].'"/>
                <input type="hidden" name="variantId[]" value="'.$getProductDetails[$index]['variant_id'].'"/>
                <input type="hidden" name="actualPrice[]" value="'.$getProductDetails[$index]['product_actual_price'].'"/>
                <input type="hidden" name="saleprice[]" value="'.$getSalePrice.'"/>
                <input type="hidden" name="totaltax[]" value="'.$getTaxAmount.'"/>
                <input type="hidden" name="totalPaid[]" value="'.$getProductDetails[$index]['total_product_paid_Amount'].'"/>
                <td>'.$setProductName.'</td>
                <td>'.$setProductStock.'</td>
                <td>'.$getProductDetails[$index]['product_Count'].'</th>
                <td><input type="text" data-max="'.$getProductDetails[$index]['product_Count'].'" name="productCount[]" class="form-control number_validation required_validation_for_billed getEnteredCount quantityData"/></th>
                <td>'.$setProductFinalPrice.'/'.$getTaxAmount.'</td>
                </tr>';
            }
            else
            {
                $setData = '<tr class="mainUnbilledRow">
                <!--<td><input type="checkbox" class="childUnbilleCheck"></td>-->
                <input type="hidden" name="productId[]" value="'.$getProductDetails[$index]['product_id'].'"/>
                <input type="hidden" name="variantId[]" value="'.$getProductDetails[$index]['variant_id'].'"/>
                <input type="hidden" name="actualPrice[]" value="'.$getProductDetails[$index]['product_actual_price'].'"/>
                <input type="hidden" name="totaltax[]" value="'.$getTaxAmount.'"/>
                <input type="hidden" name="saleprice[]" value="'.$getSalePrice.'"/>
                <input type="hidden" name="totalPaid[]" value="'.$getProductDetails[$index]['total_product_paid_Amount'].'"/>
                <td>'.$setProductName.'</td>
                <td>'.$setProductStock.'</td>
                <td>'.$getProductDetails[$index]['product_Count'].'</th>
                <td><input type="text" data-max="'.$getProductDetails[$index]['product_Count'].'" name="productCount[]" class="form-control number_validation required_validation_for_billed getEnteredCount quantityData"/></th>
                <td>'.$setProductFinalPrice.'/'.$getTaxAmount.'</td>
                </tr>';
            }
       }
        //    get ware house data
        $getWarehouseList = $this->db->select('id,w_name')->from('houdinv_shipping_warehouse')->get()->result();
        return array('unbilledData'=>$setData,'mainArray'=>$getUnbilledDataValue,'warehouseData'=>$getWarehouseList);
   }
   public function updateConfirmOrder($data)
   {    
       $setArrayData = array();
       $setTotalSalesAmount = 0;
       for($index = 0; $index < count($data['totalPaid']); $index++)
       {
           $setTotalSalesAmount = $setTotalSalesAmount+($data['actualPrice'][$index]*$data['confirmCount'][$index]);
            $setStockArray = array();
            $setArrayData[] = array('product_id'=>$data['productId'][$index],'variant_id'=>$data['variantId'][$index],'product_Count'=>$data['confirmCount'][$index],
            'product_actual_price'=>$data['actualPrice'][$index],'total_product_paid_Amount'=>$data['totalPaid'][$index],'saleprice'=>$data['saleprice'][$index],'totaltax'=>$data['totalTax'][$index]);
            // deduct inventory data
            if($data['productId'][$index] != 0 && $data['productId'][$index] != "")
            {
                $getProductData = $this->db->select('houdinv_products_main_stock,houdinv_products_total_stocks')->from('houdinv_products')->where('houdin_products_id',$data['productId'][$index])->get()->result();
                $getMainStock = json_decode($getProductData[0]->houdinv_products_main_stock,true);
                $getFinalTotalStock = $getProductData[0]->houdinv_products_total_stocks-$data['confirmCount'][$index];
                for($stock = 0; $stock < count($getMainStock); $stock++)
                {
                    if($getMainStock[$stock]['main'] == $data['outletId'])
                    {
                        $getValue = $getMainStock[$stock]['value']-$data['confirmCount'][$index];
                        $setStockArray[] = array('main'=>$getMainStock[$stock]['main'],'value'=>$getValue);
                    }
                    else
                    {
                        $setStockArray[] = array('main'=>$getMainStock[$stock]['main'],'value'=>$getMainStock[$stock]['value']);
                    }
                }
                $setProductUpdateArray = array('houdinv_products_main_stock'=>json_encode($setStockArray),'houdinv_products_total_stocks'=>$getFinalTotalStock);
                $this->db->where('houdin_products_id',$data['productId'][$index]);
                $this->db->update('houdinv_products',$setProductUpdateArray);
            } // end product set 
            else
            {
                $getVariantId = $this->db->select('houdin_products_variants_stock,houdinv_products_variants_total_stocks')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data['variantId'][$index])->get()->result();
                $getMainStock = json_decode($getVariantId[0]->houdin_products_variants_stock,true);
                $finalTotalStock = $getVariantId[0]->houdinv_products_variants_total_stocks-$data['confirmCount'][$index];
                for($stock = 0; $stock < count($getMainStock); $stock++)
                {
                    if($getMainStock[$stock]['main'] == $data['outletId'])
                    {
                        $getValue = $getMainStock[$stock]['value']-$data['confirmCount'][$index];
                        $setStockArray[] = array('main'=>$getMainStock[$stock]['main'],'value'=>$getValue);
                    }
                    else
                    {
                        $setStockArray[] = array('main'=>$getMainStock[$stock]['main'],'value'=>$getMainStock[$stock]['value']);
                    }
                }
                $setVariantUpdate = array('houdin_products_variants_stock'=>json_encode($setStockArray),'houdinv_products_variants_total_stocks'=>$finalTotalStock);
                $this->db->where('houdin_products_variants_id',$data['variantId'][$index]);
                $this->db->update('houdinv_products_variants',$setVariantUpdate);
            } // end product variant set
       }
        // get user id data
        if($this->session->userdata('vendorRole') == 0)
        {
            $masterDB = $this->load->database('master', TRUE);
            // get user id
            $getLoginUserId = $masterDB->select('houdin_user_auth_user_id')->from('houdin_user_auth')->where('houdin_user_auth_url_token',$this->session->userdata('vendorAuth'))->get()->result();
            $setUserId = $getLoginUserId[0]->houdin_user_auth_user_id;
            $setUserRole = $this->session->userdata('vendorRole');
        }
        else
        {
            $getUserId = $this->db->select('staff_id')->from('houdinv_staff_management')->where('houdinv_staff_auth_token',$this->session->userdata('vendorAuth'))->get()->result();
            $setUserId = $getUserId[0]->staff_id;
            $setUserRole = $this->session->userdata('vendorRole');
        }
       $getConfirmData = json_encode($setArrayData);
       $setUpdateArrayData = array('houdinv_confirm_order_product_detail'=>$getConfirmData,'houdinv_payment_status'=>$data['paymentStatus'],'houdinv_orders_deliverydate'=>$data['deliveryDate'],
       'houdinv_order_confirmation_status'=>'billed','houdinv_order_confirm_outlet'=>$data['outletId'],'houdinv_orders_confirm_by'=>$setUserId,'houdinv_orders_confirm_by_role'=>$setUserRole);
       $this->db->where('houdinv_order_id',$this->uri->segment('3'));
       $getUpdateStatus = $this->db->update('houdinv_orders',$setUpdateArrayData);
       if($getUpdateStatus)
       {
           // Add sales data
           $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','11')->where('houdinv_accounts_detail_type_id','109')->get()->result();    
           if(count($getUndepositedAccount) > 0)
           {
               $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
               'houdinv_accounts_balance_sheet_ref_type'=>'Inventory Sales',
               'houdinv_accounts_balance_sheet_pay_account'=>0,
               'houdinv_accounts_balance_sheet_payee_type'=>'customer',
               'houdinv_accounts_balance_sheet_account'=>0,
               'houdinv_accounts_balance_sheet_order_id'=>$this->uri->segment('3'),
               'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
               'houdinv_accounts_balance_sheet__increase'=>$setTotalSalesAmount,
               'houdinv_accounts_balance_sheet_decrease'=>0,
               'houdinv_accounts_balance_sheet_deposit'=>0,
               'houdinv_accounts_balance_sheet_final_balance'=>0,
               'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
               $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
               // get amount
               $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
               ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
               if(count($getTotalAmount) > 0)
               {
                   $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
               }
               else
               {
                   $setFinalAmount = 0;
               }
               $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
           }
        //    deduct inventory account
        $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','2')->where('houdinv_accounts_detail_type_id','7')->get()->result();    
           if(count($getUndepositedAccount) > 0)
           {
               $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
               'houdinv_accounts_balance_sheet_ref_type'=>'Inventory Sales',
               'houdinv_accounts_balance_sheet_pay_account'=>0,
               'houdinv_accounts_balance_sheet_payee_type'=>'customer',
               'houdinv_accounts_balance_sheet_account'=>0,
               'houdinv_accounts_balance_sheet_order_id'=>$this->uri->segment('3'),
               'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
               'houdinv_accounts_balance_sheet__increase'=>0,
               'houdinv_accounts_balance_sheet_decrease'=>$setTotalSalesAmount,
               'houdinv_accounts_balance_sheet_deposit'=>0,
               'houdinv_accounts_balance_sheet_final_balance'=>0,
               'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
               $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
               // get amount
               $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
               ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
               if(count($getTotalAmount) > 0)
               {
                   $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
               }
               else
               {
                   $setFinalAmount = 0;
               }
               $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
           }
        //   add undeposited fund
        $getOrderInformation = $this->db->select('*')->from('houdinv_orders')->where('houdinv_order_id',$this->uri->segment('3'))->get()->result();
         if($getOrderInformation[0]->houdinv_order_payment_method == 'cod')
         {
            $getAccountData = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id',1)->where('houdinv_accounts_detail_type_id','1')->get()->result();
            if(count($getAccountData) > 0)
            {
                $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getAccountData[0]->houdinv_accounts_id,
                'houdinv_accounts_balance_sheet_ref_type'=>$this->uri->segment('3'),
                'houdinv_accounts_balance_sheet_pay_account'=>$getOrderInformation[0]->houdinv_order_user_id,
                'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                'houdinv_accounts_balance_sheet_account'=>0,
                'houdinv_accounts_balance_sheet_order_id'=>$this->uri->segment('3'),
                'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                'houdinv_accounts_balance_sheet__increase'=>0,
                'houdinv_accounts_balance_sheet_memo'=>'Pending Amount',
                'houdinv_accounts_balance_sheet_decrease'=>$getOrderInformation[0]->houdinv_orders_total_remaining,
                'houdinv_accounts_balance_sheet_deposit'=>0,
                'houdinv_accounts_balance_sheet_final_balance'=>0,
                'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                // get amount
                $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                ->where('houdinv_accounts_balance_sheet_account_id',$getAccountData[0]->houdinv_accounts_id)->get()->result();
                if(count($getTotalAmount) > 0)
                {
                    $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                }
                else
                {
                    $setFinalAmount = 0;
                }
                $this->db->where('houdinv_accounts_id',$getAccountData[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
            }
         }
           // get user phone
            $getUserData = $this->db->select('houdinv_orders.houdinv_order_id,houdinv_users.houdinv_user_contact')->from('houdinv_orders')->where('houdinv_orders.houdinv_order_id',$this->uri->segment('3'))
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_orders.houdinv_order_user_id')->get()->result();
           if(count($getUserData) > 0)
           {
                $getSmsData = $this->db->select('houdinv_emailsms_stats_remaining_credits,houdinv_emailsms_stats_id')->from('houdinv_emailsms_stats')->where('houdinv_emailsms_stats_type','sms')->get()->result();
                if(count($getSmsData) > 0)
                {
                    if($getSmsData[0]->houdinv_emailsms_stats_remaining_credits > 0)
                    {
                        $getSmsEmail = smsemailcredentials();
                        $setData = strtotime(date('Y-m-d'));
                        $id = $getSmsEmail['twilio'][0]->houdin_twilio_sid;
                        $token = $getSmsEmail['twilio'][0]->houdin_twilio_token;
                        $url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
                        $from = $getSmsEmail['twilio'][0]->houdin_twilio_number;
                        $to = $getUserData[0]->houdinv_user_contact; // twilio trial verified number
                        $body = "Your order number ".$this->uri->segment('3')." has been confirmed";
                        $data = array (
                            'From' => $from,
                            'To' => $getUserData[0]->houdinv_user_contact,
                            'Body' => $body,
                        );
                        $post = http_build_query($data); 
                        $x = curl_init($url );
                        curl_setopt($x, CURLOPT_POST, true);
                        curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
                        curl_setopt($x, CURLOPT_POSTFIELDS, $post);
                        $y = curl_exec($x);
                        $getResponse = json_decode($y);
                        if($getResponse->error)
                        {
                            $setData = strtotime(date('Y-m-d'));
                            $setInsertArray = array('houdinv_sms_log_credit_used'=>'0','houdinv_sms_log_status'=>'0','houdinv_sms_log_created_at'=>$setData);
                            $this->db->insert('houdinv_sms_log',$setInsertArray);
                        }
                        else
                        {
                            $setData = strtotime(date('Y-m-d'));
                            $getRemainingSMS = $getSmsData[0]->houdinv_emailsms_stats_remaining_credits-1;
                            $setUpdatedArray = array('houdinv_emailsms_stats_remaining_credits'=>$getRemainingSMS);
                            $this->db->where('houdinv_emailsms_stats_id',$getSmsData[0]->houdinv_emailsms_stats_id);
                            $this->db->update('houdinv_emailsms_stats',$setUpdatedArray);
                            $setInsertArray = array('houdinv_sms_log_credit_used'=>'1','houdinv_sms_log_status'=>'1','houdinv_sms_log_created_at'=>$setData);
                            $this->db->insert('houdinv_sms_log',$setInsertArray);
                        }
                    }
                }
           }
           return array('message'=>'yes');
       }
       else
       {
        return array('message'=>'no');
       }
   }
   public function updateOrderStatusData($data)
   {
       $this->db->where('houdinv_order_id',$data['id']);
       $getUpdateData = $this->db->update('houdinv_orders',array('houdinv_order_confirmation_status'=>$data['status']));
       if($getUpdateData == 1)
       {
            return array('message'=>'yes');
       }
       else
       {
            return array('message'=>'no');
       }
   }
   public function deleteOrderData($data)
   {
       $this->db->where('houdinv_order_id',$data);
       $getDeleteStatus = $this->db->delete('houdinv_orders');
       if($getDeleteStatus == 1)
       {
            return array('message'=>'yes');
       }
       else
       {
            return array('message'=>'no');
       }
   }
   public function getAllOrdersPdf()
   {
        $getAllOrdes = $this->db->select("houdinv_orders.*")->from("houdinv_orders")->get()->result();
        $setMainArrayData = array();
        foreach($getAllOrdes as $resultList)
        {
            $setChildArray = array();
            if($resultList->houdinv_order_user_id == 0)
            {
                $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
                ->where('houdinv_order_users_order_id',$resultList->houdinv_order_id)->get()->result();
            }
            else
            {
                $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')
                ->where('houdinv_user_id',$resultList->houdinv_order_user_id)->get()->result();
            }
            $setChildArray['main'] = $resultList;
            $setChildArray['user'] = $getUserData;
            array_push($setMainArrayData,$setChildArray);
        }
        return $setMainArrayData;
   }
   public function getDeliveryBoysData()
   {
       $setStaffMember = "";
       $getStaffMember = $this->db->select('*')->from('houdinv_staff_management')->where('staff_department','DeliveryBoy')->get()->result();
       if(count($getStaffMember) > 0)
       {    
            foreach($getStaffMember as $getStaffMemberList)
            {
                    $setStaffData = $getStaffMemberList->staff_name." (".$getStaffMemberList->staff_email.", ".$getStaffMemberList->staff_contact_number.")";
                    if($setStaffMember)
                    {
                        
                        $setStaffMember = $setStaffMember.'<option data-email="'.$getStaffMemberList->staff_email.'" value="'.$getStaffMemberList->staff_id.'">'.$setStaffData.'</option>';
                    }
                    else
                    {
                        $setStaffMember = '<option value="">Choose Delivery Boy</option><option data-email="'.$getStaffMemberList->staff_email.'" value="'.$getStaffMemberList->staff_id.'">'.$setStaffData.'</option>';
                    }
            }
            return array('message'=>'yes','setData'=>$setStaffMember);
       }
       else
       {
        return array('message'=>'no');
       }
   }
   public function tagDeliveryBoyData($data)
   {
       $setData = strtotime(date('d-m-Y'));
       if($data['type'] == 2)
       {
            $setDeliveryInsertArray = array('houdinv_delivery_order_id'=>$data['orderId'],'houdinv_delivery_by'=>'Delivery Boy','houdinv_delivery_boy_id'=>$data['id'],'houdinv_delivery_courier_id'=>0,'houdinv_delivery_created_at'=>$setData);
            $getInsertStatus = $this->db->insert('houdinv_deliveries',$setDeliveryInsertArray);
       }
       else if($data['type'] == 3)
       {
            $getInsertStatus = $this->db->where('houdinv_order_id',$data['orderId'])->update('houdinv_orders',array('houdinv_order_delivery_type'=>"pick_up"));
            $setDeliveryInsertArray = array('houdinv_delivery_order_id'=>$data['orderId'],'houdinv_delivery_by'=>'Customer Pickup','houdinv_delivery_boy_id'=>0,'houdinv_delivery_courier_id'=>0,'houdinv_delivery_created_at'=>$setData);
            $getInsertStatus = $this->db->insert('houdinv_deliveries',$setDeliveryInsertArray);
       }
       else
       {
           $getKey = getpostemanKey();
           $getUserDetail = getOrderInfo($data['orderId']);
            //    create delivery package for courier service
            // get courier service info
            $url = 'https://sandbox-api.postmen.com/v3/shipper-accounts/'.$data['id'].'';
	        $method = 'GET';
	        $headers = array(
	        "content-type: application/json",
	        "postmen-api-key: ".$getKey[0]->houdinv_shipping_credentials_key.""
	        );
	        $curl = curl_init();
	        curl_setopt_array($curl, array(
	        CURLOPT_RETURNTRANSFER => true,
	        CURLOPT_URL => $url,
	        CURLOPT_CUSTOMREQUEST => $method,
	        CURLOPT_HTTPHEADER => $headers,
	        ));
	        $response = curl_exec($curl);
	        $err = curl_error($curl);
	        curl_close($curl);
	        if ($err) {
                return array('message'=>'shipper');
	            } else {
                    // get courier service info
                   $getCourierServiceInfo = json_decode($response,true);
                //    get order info
                    $getOrderUserinfo = getOrderInfo($data['orderId']);
                    $itemsData = "";
                    for($index = 0; $index < count($getOrderUserinfo['productData']); $index++)
                    {
                        $setMainData = $getOrderUserinfo['productData'][$index];
                        if($setMainData['weight'])
                        {
                            $setWeight = $setMainData['weight'];
                        }
                        else
                        {
                            $setWeight = 0.6;
                        }
                        if($itemsData)
                        {
                            $itemsData = $itemsData.",".'{
                                "description": "Order Data",
                                "origin_country": "IND",
                                "quantity": '.$setMainData['count'].',
                                "price": {
                                    "amount": '.$setMainData['price'].',
                                    "currency": "INR"
                                },
                                "weight": {
                                    "value": '.$setWeight.',
                                    "unit": "kg"},"sku": "'.$setMainData['SKU'].'"}';
                        }
                        else
                        {
                            $itemsData = '{
                                "description": "Order Data",
                                "origin_country": "IND",
                                "quantity": '.$setMainData['count'].',
                                "price": {
                                    "amount": '.$setMainData['price'].',
                                    "currency": "INR"
                                },
                                "weight": {
                                    "value": '.$setWeight.',
                                    "unit": "kg"},"sku": "'.$setMainData['SKU'].'"}';
                        }

                    }
                     
                    if($getOrderUserinfo['order'][0]->houdinv_order_payment_method == 'cod')
                    {
                        $setCODData = '"references": ["Handle with care"],
                    	"service_options": [{
                    		"type": "signature",
                    		"cod_value": {
                    			"currency": "INR",
                    			"amount": '.$getOrderUserinfo['order'][0]->houdinv_orders_total_Amount.'
                    		}
                    	}],';
                    }
                    else
                    {
                        $setCODData = "";
                    }
                    if($getCourierServiceInfo['data']['address']['fax'])
                    {
                        $setFaxData = $getCourierServiceInfo['data']['address']['fax'];
                    }
                    else
                    {
                        $setFaxData = '11';
                    }
                    if($getCourierServiceInfo['data']['address']['street2'])
                    {
                        $setStreet2 = $getCourierServiceInfo['data']['address']['street2'];
                    }
                    else
                    {
                        $setStreet2 = 'street2';
                    }
                    if($getCourierServiceInfo['data']['address']['street3'])
                    {
                        $setStreet3 = $getCourierServiceInfo['data']['address']['street3'];
                    }
                    else
                    {
                        $setStreet3 = 'street3';
                    }
                    if($getCourierServiceInfo['data']['address']['tax_id'])
                    {
                        $setTaxId = $getCourierServiceInfo['data']['address']['tax_id'];
                    }
                    else
                    {
                        $setTaxId = "no";
                    }
                   if($getCourierServiceInfo['meta']['message'] == 'OK')
                   {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://sandbox-api.postmen.com/v3/labels");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, '{
                    	"async": false,
                    	"billing": {
                    		"paid_by": "shipper"
                    	},
                    	"customs": {
                    		"billing": {
                    			"paid_by": "recipient"
                    		},
                    		"purpose": "merchandise"
                    	},
                    	"return_shipment": false,
                    	"is_document": false,
                    	"service_type": "aramex_priority_express",
                    	"paper_size": "default",
                    	"shipper_account": {
                    		"id": "'.$data['id'].'"
                    	},
                    	'.$setCODData.'
                    	"invoice": null,
                    	"shipment": {
                    		"ship_from": {
                    			"contact_name": "'.$getCourierServiceInfo['data']['address']['contact_name'].'",
                    			"company_name": "'.$getCourierServiceInfo['data']['address']['company_name'].'",
                    			"email": "'.$getCourierServiceInfo['data']['address']['email'].'",
                    			"street1": "'.$getCourierServiceInfo['data']['address']['street1'].'",
                    			"phone": "'.$getCourierServiceInfo['data']['address']['phone'].'",
                    			"city": "'.$getCourierServiceInfo['data']['address']['city'].'",
                    			"state": "'.$getCourierServiceInfo['data']['address']['state'].'",
                    			"postal_code": "'.$getCourierServiceInfo['data']['address']['postal_code'].'",
                    			"country": "'.$getCourierServiceInfo['data']['address']['country'].'",
                                "type": "'.$getCourierServiceInfo['data']['address']['type'].'",
                    			"street2": "'.$setStreet2.'",
                    			"tax_id": "'.$setTaxId.'",
                    			"street3": "'.$setStreet3.'",
                    			"fax": "'.$setFaxData.'"
                    		},
                    		"ship_to": {
                    			"contact_name": "'.$getOrderUserinfo['address'][0]->houdinv_user_address_name.'",
                    			"phone": "'.$getOrderUserinfo['address'][0]->houdinv_user_address_phone.'",
                    			"street1": "'.$getOrderUserinfo['address'][0]->houdinv_user_address_user_address.'",
                    			"postal_code": "'.$getOrderUserinfo['address'][0]->houdinv_user_address_zip.'",
                                "city": "'.$getOrderUserinfo['address'][0]->houdinv_user_address_city.'",
                                "email":"'.$getOrderUserinfo['user'][0]->houdinv_user_email.'",
                    			"country": "IND",
                    			"type": "residential"
                    		},
                    		"parcels": [
                                {
                    			"description": "Food Xs",
                    			"box_type": "custom",
                    			"weight": {
                    				"value": 2,
                    				"unit": "kg"
                    			},
                    			"dimension": {
                    				"width": 20,
                    				"height": 40,
                    				"depth": 40,
                    				"unit": "cm"
                    			},
                    			"items": [
                                    '.$itemsData.'
                                        ]
                                    }
                                    ]}}');
                    curl_setopt($ch, CURLOPT_POST, 1);
                    
                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = "Postmen-Api-Key: ".$getKey[0]->houdinv_shipping_credentials_key."";
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    
                    $result = curl_exec($ch);
                    if (curl_errno($ch)) {
                        echo 'Error:' . curl_error($ch);
                    }
                    curl_close ($ch);
                    $getCourierMessage = json_decode($result,true);
                    if($getCourierMessage['meta']['message'] == 'OK')
                    {
                        $setDeliveryInsertArray = array('houdinv_delivery_order_id'=>$data['orderId'],
                            'houdinv_delivery_by'=>'Delivery Service','houdinv_delivery_boy_id'=>0,
                            'houdinv_delivery_courier_id'=>$getCourierMessage['data']['id'],
                            'houdinv_deliveries_courier_name'=>$getCourierMessage['data']['rate']['service_type'],
                            'houdinv_deliveries_tracking_id'=>$getCourierMessage['data']['tracking_numbers'][0],
                            'houdinv_deliveries_invoice_url'=>$getCourierMessage['data']['files']['label']['url'],
                            'houdinv_delivery_created_at'=>$setData);
                        $getInsertStatus = $this->db->insert('houdinv_deliveries',$setDeliveryInsertArray);
                    }
                    else
                    {
                        return array('message'=>$getCourierMessage['meta']['message']);
                    }
                   }
                   else
                   {
                        return array('message'=>'shipper');
                   }
	            }
       }
       if($getInsertStatus == 1)
       {
            $setUpdateArray = array('houdinv_order_confirmation_status'=>'assigned','houdinv_order_updated_at'=>$setData);
            $this->db->where('houdinv_order_id',$data['orderId']);
            $getUpdateStatus = $this->db->update('houdinv_orders',$setUpdateArray);
            if($getUpdateStatus == 1)
            {
                return array('message'=>'yes');     
            }
            else
            {
                return array('message'=>'no');     
            }
       }
       else
       {
           return array('message'=>'no');
       }
   }
   public function getEmailStatus()
   {
       $getEmailData = $this->db->select('houdinv_emailsms_stats_id,houdinv_emailsms_stats_remaining_credits')->from('houdinv_emailsms_stats')->where('houdinv_emailsms_stats_type','email')->get()->result();
       if($getEmailData[0]->houdinv_emailsms_stats_remaining_credits > 0)
       {
           return array('message'=>'success','remaining'=>$getEmailData[0]->houdinv_emailsms_stats_remaining_credits,'id'=>$getEmailData[0]->houdinv_emailsms_stats_id);
       }
   }
   public function updateCreditsData($data,$id)
   {
        $this->db->where('houdinv_emailsms_stats_id',$id);
        $this->db->update('houdinv_emailsms_stats',$data);
        $setData = strtotime(date('Y-m-d'));
        $insertArray = array('houdinv_sms_log_credit_used'=>'1','houdinv_sms_log_status'=>'1','houdinv_sms_log_created_at'=>$setData);
        $this->db->insert('houdinv_sms_log',$insertArray);
   }
   public function getComplatedOrdersPdf()
   {
        $getAllOrdes = $this->db->select("houdinv_orders.*")
        ->from("houdinv_orders")
        ->where('houdinv_orders.houdinv_order_confirmation_status','Delivered')->get()->result();
        $setMainArrayData = array();
        foreach($getAllOrdes as $resultList)
        {
            $setChildArray = array();
            if($resultList->houdinv_order_user_id == 0)
            {
                $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
                ->where('houdinv_order_users_order_id',$resultList->houdinv_order_id)->get()->result();
            }
            else
            {
                $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_id',$resultList->houdinv_order_user_id)->get()->result();
            }
            $setChildArray['main'] = $resultList;
            $setChildArray['user'] = $getUserData;
            array_push($setMainArrayData,$setChildArray);
        }
        return $setMainArrayData;
   }
   public function getPendingOrdersPdf()
   {
        $getDeliveryPending = $this->db->select("houdinv_orders.*")
        ->from("houdinv_orders")
        ->where("houdinv_orders.houdinv_orders_delivery_status",0)
        ->where("houdinv_orders.houdinv_order_delivery_type",'deliver')
        ->group_start()
        ->where("houdinv_orders.houdinv_order_confirmation_status","order pickup")
        ->or_where("houdinv_orders.houdinv_order_confirmation_status","billed")
        ->or_where("houdinv_orders.houdinv_order_confirmation_status","assigned")
        ->group_end()
        ->order_by("houdinv_orders.houdinv_order_id","desc")       
        ->get()->result();
        $setMainArrayData = array();
        foreach($getDeliveryPending as $resultList)
        {
            $setChildArray = array();
            if($resultList->houdinv_order_user_id == 0)
            {
                $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
                ->where('houdinv_order_users_order_id',$resultList->houdinv_order_id)->get()->result();
            }
            else
            {
                $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_id',$resultList->houdinv_order_user_id)->get()->result();
            }
            $setChildArray['main'] = $resultList;
            $setChildArray['user'] = $getUserData;
            array_push($setMainArrayData,$setChildArray);
        }
        return $setMainArrayData;
   }
   public function getPickupOrdersPdf()
   {
        $getPickUpOrder = $this->db->select("houdinv_orders.*")
        ->from("houdinv_orders")
        ->order_by("houdinv_orders.houdinv_order_id","desc")
        ->where("houdinv_orders.houdinv_order_delivery_type",'pick_up')
        ->get()->result();
        $setMainArrayData = array();
        foreach($getPickUpOrder as $resultList)
        {
            $setChildArray = array();
            if($resultList->houdinv_order_user_id == 0)
            {
                $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
                ->where('houdinv_order_users_order_id',$resultList->houdinv_order_id)->get()->result();
            }
            else
            {
                $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_id',$resultList->houdinv_order_user_id)->get()->result();
            }
            $setChildArray['main'] = $resultList;
            $setChildArray['user'] = $getUserData;
            array_push($setMainArrayData,$setChildArray);
        }
        return $setMainArrayData;
   }
   public function getPaymentPendingOrderCount()
   {
       $getTotalCount = $this->db->select('count(houdinv_order_id) AS totalPaymentPending')->from('houdinv_orders')->where('houdinv_payment_status',0)->get()->result();
       return array('totalRows'=>$getTotalCount[0]->totalPaymentPending);
   }
   public function getPaymentPendingOrderDetails($data)
   {
        $this->db->select('houdinv_orders.*')->from('houdinv_orders')
        ->where('houdinv_orders.houdinv_payment_status',0);
            if( $this->session->userdata('vendorOutlet')>0)
               {
                $this->db->where("houdinv_orders.houdinv_order_confirm_outlet",$this->session->userdata('vendorOutlet')); 
               } 
        if($data['start'] && $data['limit'])
            {
                $this->db->limit($data['limit'],$data['start']);
            }
            elseif(!$data['start'] && $data['limit'])
            {
                $this->db->limit($data['limit']);
            }  
            $getPaymentPendingdata = $this->db->get();
            $resultPaymentPendingdata = ($getPaymentPendingdata->num_rows() > 0)?$getPaymentPendingdata->result_array():FALSE;
            $setMainArrayData = array();
        foreach($resultPaymentPendingdata as $resultList)
        {
            // get transaction data
            
            $setChildArray = array();
            if($resultList['houdinv_order_user_id'] == 0)
            {
                $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
                ->where('houdinv_order_users_order_id',$resultList['houdinv_order_id'])->get()->result();
            }
            else
            {
                $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_id',$resultList['houdinv_order_user_id'])->get()->result();
            }
            $setChildArray['main'] = $resultList;
            $setChildArray['user'] = $getUserData;
            array_push($setMainArrayData,$setChildArray);
        }
            return $setMainArrayData;
   }
   public function getPaymentPendingOrdersPdf()
   {
        $getPaymentPending = $this->db->select('houdinv_orders.*')->from('houdinv_orders')->where('houdinv_orders.houdinv_payment_status',0)->get()->result();
        $setMainArrayData = array();
        foreach($getPaymentPending as $resultList)
        {
            $setChildArray = array();
            if($resultList->houdinv_order_user_id == 0)
            {
                $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
                ->where('houdinv_order_users_order_id',$resultList->houdinv_order_id)->get()->result();
            }
            else
            {
                $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')
                ->where('houdinv_user_id',$resultList->houdinv_order_user_id)->get()->result();
            }
            $setChildArray['main'] = $resultList;
            $setChildArray['user'] = $getUserData;
            array_push($setMainArrayData,$setChildArray);
        }
        return $setMainArrayData;
   }
   public function getUnbilledOrdersPdf()
   {
        $getUnbilledOrder = $this->db->select('houdinv_orders.*')->from('houdinv_orders')->where('houdinv_orders.houdinv_order_confirmation_status','unbilled')->get()->result();
        $setMainArrayData = array();
       foreach($getUnbilledOrder as $resultList)
       {
            
           $setChildArray = array();
           if($resultList->houdinv_order_user_id == 0)
           {
               $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
               ->where('houdinv_order_users_order_id',$resultList->houdinv_order_id)->get()->result();
           }
           else
           {
               $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_id',$resultList->houdinv_order_user_id)->get()->result();
           }
           $setChildArray['main'] = $resultList;
           $setChildArray['user'] = $getUserData;
           array_push($setMainArrayData,$setChildArray);
       }
        return $setMainArrayData;
   }
   public function fetchInvoiceDetailsData($data)
   {
        //    get main detail
       $getMainDetails = $this->db->select('houdinv_orders.*')
       ->from('houdinv_orders')->where('houdinv_orders.houdinv_order_id',$data)->get()->result();
       $setMainArrayData = array();
       foreach($getMainDetails as $resultList)
       {
            
           $setChildArray = array();
           if($resultList->houdinv_order_user_id == 0)
           {
               $getUserData = $this->db->select('houdinv_order_users_name,houdinv_order_users_contact')->from('houdinv_order_users')
               ->where('houdinv_order_users_order_id',$resultList->houdinv_order_id)->get()->result();
           }
           else
           {
               $getUserData = $this->db->select('houdinv_user_name,houdinv_user_email,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_id',$resultList->houdinv_order_user_id)->get()->result();
           }
           $setChildArray['main'] = $resultList;
           $setChildArray['user'] = $getUserData;
           array_push($setMainArrayData,$setChildArray);
       }
       //    get main product details
        if($getMainDetails[0]->houdinv_confirm_order_product_detail)
        {
                $getProductDetails = json_decode($getMainDetails[0]->houdinv_confirm_order_product_detail,true);
        }
        else
        {
                $getProductDetails = json_decode($getMainDetails[0]->houdinv_order_product_detail,true);
        }
        $setProductDetailArray = array();
        for($index = 0; $index < count($getProductDetails); $index++)
        {
            if($getProductDetails[$index]['product_id'] != 0 && $getProductDetails[$index]['product_id'] != "")
            {
                $getProductName = $this->db->select('*')->from('houdinv_products')->where('houdin_products_id',$getProductDetails[$index]['product_id'])->get()->result();
                $setProductName = $getProductName[0]->houdin_products_title;
            }
            else
            {
                $getProductName = $this->db->select('houdin_products_variants_title')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getProductDetails[$index]['variant_id'])->get()->result();
                $setProductName = $getProductName[0]->houdin_products_variants_title;
            }
            $setProductDetailArray[] = array('productName'=>$setProductName,'quantity'=>$getProductDetails[$index]['product_Count'],'actualPrice'=>$getProductDetails[$index]['product_actual_price'],'total'=>$getProductDetails[$index]['total_product_paid_Amount'],'saleprice'=>$getProductDetails[$index]['saleprice'],'totaltax'=>$getProductDetails[$index]['totaltax']);
        }
        //    get delivery address
        if($getMainDetails[0]->houdinv_order_user_id == 0)
        {
            $getExtraAddress = $this->db->select('*')->from('houdinv_order_users')->where('houdinv_order_users_order_id',$getMainDetails[0]->houdinv_order_id)->get()->result();
        }
        $getDeliveryAddress = $this->db->select('*')->from('houdinv_user_address')->where('houdinv_user_address_id',$getMainDetails[0]->houdinv_order_delivery_address_id)->get()->result();

        //    get billing address
        $getBillingAddress = $this->db->select('*')->from('houdinv_user_address')->where('houdinv_user_address_id',$getMainDetails[0]->houdinv_order_billing_address_id)->get()->result();
        $shop_detail = $this->db->select('*')->from('houdinv_shop_detail')->get()->row();
     
        return array('shop_detail'=>$shop_detail,'maindata'=>$setMainArrayData,'deliveryAddress'=>$getDeliveryAddress,'billingAddress'=>$getBillingAddress,'productDetail'=>$setProductDetailArray,'extraAddress'=>$getExtraAddress);
   }
   public function updateUserOrderComment($data)
   {
       $this->db->where('houdinv_order_id',$data['orderId']);
       $getUpdate = $this->db->update('houdinv_orders',array('houdinv_order_comments'=>$data['comment']));
       if($getUpdate == 1)
       {
            return array('message'=>'yes');
       }
       else
       {
           return array('message'=>'no');
       }
   }
   public function updatedDeliveryDate($data)
   {
        $this->db->where('houdinv_order_id',$data['orderId']);
        $getUpdate = $this->db->update('houdinv_orders',array('houdinv_orders_deliverydate'=>$data['deliveryDate']));
        if($getUpdate == 1)
       {
            return array('message'=>'yes');
       }
       else
       {
           return array('message'=>'no');
       }
   }
   public function fetchUserInfoData($data)
   {
       $getUserInfo = $this->db->select('houdinv_orders.houdinv_order_user_id,houdinv_users.houdinv_user_name,houdinv_users.houdinv_user_email')->from('houdinv_orders')->where('houdinv_orders.houdinv_order_id',$data)
       ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_orders.houdinv_order_user_id')
       ->get()->result();
        return $getUserInfo;
   }
   public function fetchRemainingPayment($data)
   {
       $getData = $this->db->select('houdinv_orders.houdinv_orders_total_Amount,houdinv_orders.houdinv_orders_total_paid,houdinv_orders.houdinv_orders_total_remaining,houdinv_users.houdinv_users_privilage,houdinv_users.houdinv_users_pending_amount,houdinv_users.houdinv_user_id')
       ->from('houdinv_orders')->where('houdinv_orders.houdinv_order_id',$data)
       ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_orders.houdinv_order_user_id')->get()->result();
       if(count($getData) > 0)
       {
            return array('message'=>'Success','data'=>$getData,'orderId'=>$data);
       }
       else
       {
            return array('message'=>'Something went wrong. Please try again');
       }
   }
   public function updatePaymentStatus($order,$transaction,$customer,$orderId,$customerId,$paymentmode)
   {
    //    update order
        $this->db->where('houdinv_order_id',$orderId);
        $getOrderUpdateStatus = $this->db->update('houdinv_orders',$order);
        if($getOrderUpdateStatus == 1)
        {
            // add accounting section
            if($paymentmode == 'cash')
            {
                // add undeposited fund
                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->get()->result();    
                if(count($getUndepositedAccount) > 0)
                {
                    $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                    'houdinv_accounts_balance_sheet_ref_type'=>'Undeposited Fund',
                    'houdinv_accounts_balance_sheet_pay_account'=>$customerId,
                    'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                    'houdinv_accounts_balance_sheet_account'=>0,
                    'houdinv_accounts_balance_sheet_order_id'=>$orderId,
                    'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                    'houdinv_accounts_balance_sheet__increase'=>$order['houdinv_orders_total_paid'],
                    'houdinv_accounts_balance_sheet_decrease'=>0,
                    'houdinv_accounts_balance_sheet_deposit'=>0,
                    'houdinv_accounts_balance_sheet_final_balance'=>0,
                    'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                    $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                    // get amount
                    $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                    ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                    if(count($getTotalAmount) > 0)
                    {
                        $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                    }
                    else
                    {
                        $setFinalAmount = 0;
                    }
                    $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                }
            }
            else
            {
                // add current fund
                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
                if(count($getUndepositedAccount) > 0)
                {
                    $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                    'houdinv_accounts_balance_sheet_ref_type'=>'Current',
                    'houdinv_accounts_balance_sheet_pay_account'=>$customerId,
                    'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                    'houdinv_accounts_balance_sheet_account'=>0,
                    'houdinv_accounts_balance_sheet_order_id'=>$orderId,
                    'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                    'houdinv_accounts_balance_sheet__increase'=>$order['houdinv_orders_total_paid'],
                    'houdinv_accounts_balance_sheet_decrease'=>0,
                    'houdinv_accounts_balance_sheet_deposit'=>0,
                    'houdinv_accounts_balance_sheet_final_balance'=>0,
                    'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                    $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                    // get amount
                    $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                    ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                    if(count($getTotalAmount) > 0)
                    {
                        $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                    }
                    else
                    {
                        $setFinalAmount = 0;
                    }
                    $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                }
            }
            $getAccountData = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id',1)->where('houdinv_accounts_detail_type_id','1')->get()->result();
            if(count($getAccountData) > 0)
            {
                $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getAccountData[0]->houdinv_accounts_id,
                'houdinv_accounts_balance_sheet_ref_type'=>'Pending Amount',
                'houdinv_accounts_balance_sheet_pay_account'=>$customerId,
                'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                'houdinv_accounts_balance_sheet_account'=>0,
                'houdinv_accounts_balance_sheet_order_id'=>$orderId,
                'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                'houdinv_accounts_balance_sheet__increase'=>0,
                'houdinv_accounts_balance_sheet_decrease'=>$order['houdinv_orders_total_paid'],
                'houdinv_accounts_balance_sheet_deposit'=>0,
                'houdinv_accounts_balance_sheet_final_balance'=>0,
                'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                // get amount
                $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                ->where('houdinv_accounts_balance_sheet_account_id',$getAccountData[0]->houdinv_accounts_id)->get()->result();
                if(count($getTotalAmount) > 0)
                {
                    $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                }
                else
                {
                    $setFinalAmount = 0;
                }
                $this->db->where('houdinv_accounts_id',$getAccountData[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
            }
            //  end here

            // update transaction
            $getInserStatus = $this->db->insert('houdinv_transaction',$transaction);
            if($getInserStatus == 1)
            {
                if(count($customer) > 0)
                {
                    $this->db->where('houdinv_user_id',$customerId);
                    $getCustomerUpdate = $this->db->update('houdinv_users',$customer);
                    if($getCustomerUpdate == 1)
                    {
                        return array('message'=>'yes');
                    }
                    else
                    {
                        return array('message'=>'cus');
                    }
                }
                else
                {
                    return array('message'=>'yes');
                }
            }
            else
            {
                if(count($customer) > 0)
                {
                    $this->db->where('houdinv_user_id',$customerId);
                    $getCustomerUpdate = $this->db->update('houdinv_users',$customer);
                    if($getCustomerUpdate == 1)
                    {
                        return array('message'=>'transaction');
                    }
                    else
                    {
                        return array('message'=>'custransaction');
                    }
                }
                else
                {
                    return array('message'=>'transaction');
                }
            }
        }
        else
        {
            return array('message'=>'no');
        }
   }
   public function getQuotationOrderCount()
   {
       $getRows = $this->db->select('COUNT(houdinv_shop_ask_quotation_id) as TotalCount')->from('houdinv_shop_ask_quotation')->get()->result();
       return array('totalRows'=>$getRows[0]->TotalCount);
   }
   public function fetchDeliveryQuotationData($data)
   {
       $this->db->select('houdinv_shop_ask_quotation.*,houdinv_products.houdin_products_title')->from('houdinv_shop_ask_quotation')
       ->join('houdinv_products','houdinv_products.houdin_products_id = houdinv_shop_ask_quotation.houdinv_shop_ask_quotation_product_id');
       if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getQuotationdata = $this->db->get();
        $resultQuotationddata = ($getQuotationdata->num_rows() > 0)?$getQuotationdata->result():FALSE;
        return array('quotaion'=>$resultQuotationddata);
   }
   public function deleteOrderQuotation($data)
   {
       $this->db->where('houdinv_shop_ask_quotation_id',$data);
       $getDeleteStatus = $this->db->delete('houdinv_shop_ask_quotation');
       if($getDeleteStatus)
       {
            return array('message'=>'yes');
       }
       else
       {
            return array('message'=>'no');
       }
   }
   public function updateReturnOrderStatus($data)
   {
       $this->db->where('houdinv_order_id',$data);
       $getData = $this->db->update('houdinv_orders',array('houdinv_order_confirmation_status'=>'return'));
       return $getData;
   }
   public function fetchCustomerDetail($data)
   {
       $getCustomerDetails = $this->db->select('houdinv_orders.houdinv_order_user_id,houdinv_users.houdinv_user_name,houdinv_users.houdinv_user_email,')->from('houdinv_orders')->where('houdinv_orders.houdinv_order_id',$data)
        ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_orders.houdinv_order_user_id','left outer')->get()->result();
        return $getCustomerDetails;
   }
   public function updateCancelOrderStatus($data)
   {
        $this->db->where('houdinv_order_id',$data);
        $getData = $this->db->update('houdinv_orders',array('houdinv_order_confirmation_status'=>'cancel'));
        return $getData;
   }
   public function updateRefundAmountdata($data)
   {
        $this->db->where('houdinv_order_id',$data['id']);
        $getData = $this->db->update('houdinv_orders',array('houdinv_orders_refund_status'=>'1'));
        if($getData)
        {
            $set = "TXN".rand(99999999,10000000);
            $setTransactionArray = array('houdinv_transaction_transaction_id'=>$set,'houdinv_transaction_type'=>'debit','houdinv_transaction_method'=>'cash','houdinv_transaction_from'=>'cash',
            'houdinv_transaction_for'=>'order refund','houdinv_transaction_for_id'=>$data['id'],'houdinv_transaction_amount'=>$data['amount'],'houdinv_transaction_date'=>date('Y-m-d'),'houdinv_transaction_status'=>'success');
            $getInsertStatsu = $this->db->insert('houdinv_transaction',$setTransactionArray);
            if($getInsertStatsu)
            {
                $getOrderInfo = $this->db->select('*')->from('houdinv_orders')->where('houdinv_order_id',$data['id'])->get()->result();
                // update accounts data
                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
                if(count($getUndepositedAccount) > 0)
                {
                    $getCustomerId = $this->db->select('houdinv_order_user_id')->from('houdinv_orders')->where('houdinv_order_id',$data['houdinv_transaction_for_id'])->get()->result();
                    if(count($getCustomerId) > 0)
                    {
                        $getCustomerId = $getCustomerId;
                    }
                    else
                    {
                        $getCustomerId = "";
                    }
                    $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                    'houdinv_accounts_balance_sheet_ref_type'=>$data['id'],
                    'houdinv_accounts_balance_sheet_pay_account'=>$getOrderInfo[0]->houdinv_order_user_id,
                    'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                    'houdinv_accounts_balance_sheet_account'=>0,
                    'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                    'houdinv_accounts_balance_sheet__increase'=>0,
                    'houdinv_accounts_balance_sheet_order_id'=>$data['id'],
                    'houdinv_accounts_balance_sheet_decrease'=>$data['amount'],
                    'houdinv_accounts_balance_sheet_memo'=>'Refund',
                    'houdinv_accounts_balance_sheet_deposit'=>0,
                    'houdinv_accounts_balance_sheet_final_balance'=>0,
                    'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                    $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                    // get amount
                    $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                    ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                    if(count($getTotalAmount) > 0)
                    {
                        $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                    }
                    else
                    {
                        $setFinalAmount = 0;
                    }
                    $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                }
                // end here
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'trans');
            }
        }
        else
        {
            return array('message'=>'no');
        }
   }
  }