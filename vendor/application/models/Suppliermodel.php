<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Suppliermodel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
    }
    public function getSearchedProduct($data)
    {
        $this->db->select('houdin_products_title,houdin_products_id')->from('houdinv_products')->where('houdin_products_status','1')->where("houdin_products_title LIKE '%$data%'");
        $getSeacrhedData = $this->db->get()->result();
        $setDataList = "";
        if(count($getSeacrhedData) > 0)
        {
            foreach($getSeacrhedData as $getSeacrhedDataList)
            {
                if($setDataList)
                {
                    $setDataList = $setDataList.""."<li class='searh-suggestionboxlistbox_row'><a class='selectProductData' data-id='".$getSeacrhedDataList->houdin_products_id."' href='javascript:;'>".$getSeacrhedDataList->houdin_products_title."</a></li>";
                }
                else
                {
                    $setDataList = "<li class='searh-suggestionboxlistbox_row'><a class='selectProductData' data-id='".$getSeacrhedDataList->houdin_products_id."' href='javascript:;'>".$getSeacrhedDataList->houdin_products_title."</a></li>";
                }
            }
            return array('message'=>'success','result'=>$setDataList);
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function addSupplierData($data)
    {
        $setdate = strtotime(date('Y-m-d'));
        // check supplier data
        $getSupplierList = $this->db->select('houdinv_supplier_comapany_name')->from('houdinv_suppliers')->where('houdinv_supplier_contact',$data['mobile'])->or_where('houdinv_supplier_email',$data['email'])->get()->result();
        if(count($getSupplierList) > 0)
        {
            return array('message'=>'supplier','companyName'=>$getSupplierList[0]->houdinv_supplier_comapany_name);
        }
        else
        {
            $setInsertArray = array('houdinv_supplier_comapany_name'=>$data['company'],'houdinv_supplier_contact_person_name'=>$data['contactPerson'],'houdinv_supplier_email'=>$data['email'],'houdinv_supplier_contact'=>$data['mobile'],'houdinv_supplier_landline'=>$data['landline'],'houdinv_supplier_address'=>$data['address'],'houdinv_supplier_active_status'=>$data['status'],'houdinv_supplier_created_at'=>$setdate);
            $getInsertStatus = $this->db->insert('houdinv_suppliers',$setInsertArray); 
            $getLastId = $this->db->insert_id();
            if($getLastId != "")
            {
                if(count($data['products']) > 0)
                {
                    for($index = 0; $index < count($data['products']); $index++)
                    {
                        $setInsertProductData = array('houdinv_supplier_products_supplier_id'=>$getLastId,'houdinv_supplier_products_product_id'=>$data['products'][$index],'houdinv_supplier_products_created_at'=>$setdate);
                        $this->db->insert('houdinv_supplier_products',$setInsertProductData);
                    }
                    return array('message'=>'yes');
                }
                else
                {
                    return array('message'=>'yes');
                }
            }
            else
            {
                return array('message'=>'no');
            }
        }
    }
    public function getSupplierTotalCount()
    {
        $this->db->select('count(houdinv_supplier_id) AS supplierCount')->from('houdinv_suppliers');
        $getTotalCount = $this->db->get()->result();
        return array('totalRows'=>$getTotalCount); 
    }
    public function getSupplierDetailsData($data)
    {
        $this->db->select('*')->from('houdinv_suppliers');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getSupplierdata = $this->db->get();
        $resultSupplierdata = ($getSupplierdata->num_rows() > 0)?$getSupplierdata->result():FALSE;
        // get total supplier
        $this->db->select('COUNT(houdinv_supplier_id) AS totalSuppliers')->from('houdinv_suppliers');
        $getTotalSuppliers = $this->db->get()->result();
        // get total active supplier
        $this->db->select('COUNT(houdinv_supplier_id) AS totalActiveSuppliers')->from('houdinv_suppliers')->where('houdinv_supplier_active_status','active');
        $getTotalActiveSuppliers = $this->db->get()->result();
        // get total supplier
        $this->db->select('COUNT(houdinv_supplier_id) AS totalDeactiveSuppliers')->from('houdinv_suppliers')->where('houdinv_supplier_active_status','deactive');
        $getTotalDeactiveSuppliers = $this->db->get()->result();
        return array('supplierList'=>$resultSupplierdata,'totalSuppliers'=>$getTotalSuppliers[0]->totalSuppliers,'activeSuppliers'=>$getTotalActiveSuppliers[0]->totalActiveSuppliers,'deactiveSuppliers'=>$getTotalDeactiveSuppliers[0]->totalDeactiveSuppliers);
    }
    public function deleteSupplierData($data)
    {
        $this->db->where('houdinv_supplier_id',$data);
        $getSupplierDeleteStatus = $this->db->delete('houdinv_suppliers');
        if($getSupplierDeleteStatus == 1)
        {
            $this->db->where('houdinv_supplier_products_supplier_id',$data);
            $getDeleteSupplierProdcut = $this->db->delete('houdinv_supplier_products');
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function updateSupplierStatus($data)
    {
        $setUpdateArray = array('houdinv_supplier_active_status'=>$data['status']);
        $this->db->where('houdinv_supplier_id',$data['id']);
        $getUpdateStatus = $this->db->update('houdinv_suppliers',$setUpdateArray);
        if($getUpdateStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function setMultipleStatus($data)
    {
        $expldoeId = explode(",",$data['id']);
        $setUpdateArray = array('houdinv_supplier_active_status'=>$data['status']);
        for($index = 0; $index < count($expldoeId); $index++)
        {
            $this->db->where('houdinv_supplier_id',$expldoeId[$index]);
            $this->db->update('houdinv_suppliers',$setUpdateArray);
        }
        return array('message'=>'yes');
    }
    public function deleteMultipleSupplier($data)
    {
        $explodeIdData = explode(',',$data);
        for($index = 0; $index < count($explodeIdData); $index++)
        {
            $this->db->where('houdinv_supplier_id',$explodeIdData[$index]);
            $this->db->delete('houdinv_suppliers');
            $this->db->where('houdinv_supplier_products_supplier_id',$explodeIdData[$index]);
            $this->db->delete('houdinv_supplier_products');
        }
        return array('message'=>'yes');
    }
    public function fetchSupplierDetails($data)
    {
        // get personal info
        $this->db->select('*')->from('houdinv_suppliers')->where('houdinv_supplier_id',$data);
        $getSuuplierInfo = $this->db->get()->result();
        return array('supplierInfo'=>$getSuuplierInfo);
    }
    public function fetchEditSupplierDataValue($data)
    {
        $this->db->select('*')->from('houdinv_suppliers')->where('houdinv_suppliers.houdinv_supplier_id',$data);
        $getSupplierDetails = $this->db->get()->result();
        // fetch supplier products
        $this->db->select('houdinv_supplier_products.houdinv_supplier_products_product_id,houdinv_products.houdin_products_title')->from('houdinv_supplier_products')->where('houdinv_supplier_products.houdinv_supplier_products_supplier_id',$data)
        ->join('houdinv_products','houdinv_products.houdin_products_id = houdinv_supplier_products.houdinv_supplier_products_product_id');
        $getSupplierProducts = $this->db->get()->result();
        return array('fetchEditSupplierList'=>$getSupplierDetails,'supplierProducts'=>$getSupplierProducts);
    }
    public function updateSupplierData($data)
    {
        $setDateData = strtotime(date('Y-m-d'));
        // check for supplier
        $getEmailContactCheck = $this->db->select('houdinv_supplier_comapany_name')->from('houdinv_suppliers')->where('houdinv_supplier_id != '.$data['supplierId'].'')
        ->group_start()
        ->where('houdinv_supplier_email',$data['email'])->or_where('houdinv_supplier_contact',$data['mobile'])
        ->group_end()->get()->result();
        if(count($getEmailContactCheck) > 0)
        {
            return array('message'=>'supplier','companyName'=>$getEmailContactCheck[0]->houdinv_supplier_comapany_name);
        }
        else
        {
            $setUpdateArray = array('houdinv_supplier_comapany_name'=>$data['company'],'houdinv_supplier_contact_person_name'=>$data['contactPerson'],'houdinv_supplier_email'=>$data['email'],
            'houdinv_supplier_contact'=>$data['mobile'],'houdinv_supplier_landline'=>$data['landline'],'houdinv_supplier_active_status'=>$data['status'],'houdinv_supplier_updated_at'=>$setDateData);
            $this->db->where('houdinv_supplier_id',$data['supplierId']);
            $getUpdateData = $this->db->update('houdinv_suppliers',$setUpdateArray);
            if($getUpdateData == 1)
            {
                $this->db->where('houdinv_supplier_products_supplier_id',$data['supplierId']);
                $this->db->delete('houdinv_supplier_products');
                if(count($data['products']) > 0)
                {
                    for($index = 0; $index < count($data['products']); $index++)
                    {
                        $setInsertProductData = array('houdinv_supplier_products_supplier_id'=>$data['supplierId'],'houdinv_supplier_products_product_id'=>$data['products'][$index],'houdinv_supplier_products_created_at'=>$setDateData);
                        $this->db->insert('houdinv_supplier_products',$setInsertProductData);
                    }
                    return array('message'=>'yes');
                }
                else
                {
                    return array('message'=>'yes');
                }
                
            }
            else
            {
                return array('message'=>'no');
            }
        }
    }
 }