<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <h4 class="page-title">Admin Logs</h4>
                            </div>
                        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card-box table-responsive">
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th>IP</th>
                    <th> Location</th>
                    <th>Browser</th>
                    <th>Date</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
               
                foreach($log as $data) 
                {
                    
                    ?>
                  <tr>
                    <td><?php echo $data['houdin_user_log_ip_address']; ?></td>
                    <td><?php echo $data['houdin_user_log_geo_location']; ?></td>
                    <td><?php echo $data['houdin_user_log_browser']; ?></td>
                    <td><?php echo date("Y-m-d, h:i:s",$data['houdin_user_log_created_at']); ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
 <ul class="pagination pull-right"> 
                              <?php echo $this->pagination->create_links(); ?> </ul>

                    </div> <!-- container -->

                </div> <!-- content -->




  <?php $this->load->view('Template/footer.php') ?>
