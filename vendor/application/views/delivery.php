<?php $this->load->view('Template/header') ?>
<?php $this->load->view('Template/sidebar.php');
$getCurrency = getVendorCurrency();
if($getCurrency[0]->houdin_users_currency=="USD")
{
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="AUD"){
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="Euro"){
	$currencysymbol= "£";
}else if($getCurrency[0]->houdin_users_currency=="Pound"){
	$currencysymbol= "€";
}else if($getCurrency[0]->houdin_users_currency=="INR"){
	$currencysymbol= "₹";
}
?>

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

             <!--START Page-Title -->
            <div class="row">

                <div class="col-md-8">
                   <h4 class="page-title">Delivery</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li class="active">Delivery</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->

            </div>
           <!--END Page-Title -->



        <div class="row">
          <div class="col-md-12">
            <div class="card-box table-responsive">
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th>Order Details</th>
                    <th>Delivery Service Details</th>
                    <th>Order Status</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                foreach($deliveryData as $deliveryDataList)
                {

                ?>
                <tr>
                    <td>
                    <a href="<?php echo base_url(); ?>Order/vieworder/<?php echo $deliveryDataList['delivery']->houdinv_delivery_order_id ?>">#<?php echo $deliveryDataList['delivery']->houdinv_delivery_order_id ?></a>(Total Amount: <?php echo $currencysymbol ?><?php echo $deliveryDataList['order'][0]->houdinv_orders_total_Amount ?>)<br/>
                    <label style="color:green">Total Paid: (<?php echo $currencysymbol ?><?php echo $deliveryDataList['order'][0]->houdinv_orders_total_paid ?>)</label>
                    <label style="color:red">Total Remaining: (<?php echo $currencysymbol ?><?php echo $deliveryDataList['order'][0]->houdinv_orders_total_remaining ?>)</label>
                    </td>
                    <td><?php
                    if(count($deliveryDataList['deliveryboy']) > 0)
                    {
                    ?>
                    <?php echo $deliveryDataList['deliveryboy'][0]->staff_name."(".$deliveryDataList['deliveryboy'][0]->staff_contact_number.", ".$deliveryDataList['deliveryboy'][0]->staff_email.")" ?>
                    <?php }
                    else
                    {
                    ?>
                    <?php echo $deliveryDataList['delivery']->houdinv_delivery_courier_id."<br/>Service:".$deliveryDataList['delivery']->houdinv_deliveries_courier_name.""."Trackig Number: ".$deliveryDataList['delivery']->houdinv_deliveries_tracking_id."<br/>
                    <a href='".$deliveryDataList['delivery']->houdinv_deliveries_invoice_url."'>Download</a>"
                    ?>
                    <?php }
                    ?></td>
                    <td><button type="button" class="btn btn-xs btn-warning"><?php echo $deliveryDataList['order'][0]->houdinv_order_confirmation_status ?></button></td>
                  </tr>
                <?php }
                ?>
                </tbody>
              </table>
              <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>

<?php $this->load->view('Template/footer') ?>
