<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
         <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">SMS Logs</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Templates</a></li>
                  <li class="active">SMS Logs</li>
                  </ol>
                  </div>
                
            </div>
           <!--END Page-Title -->  
          <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>

                    <th>SMS Used</th>
                    <th>Date</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach($smslog as $smslogData)
                  {                
                    ?>
                  <tr>
                    <td><?php echo $smslogData->houdinv_sms_log_credit_used ?></td>
                    <td><?php echo date('d-m-Y',$smslogData->houdinv_sms_log_created_at) ?></td>
                    <td>
                      <?php 
                      if($smslogData->houdinv_sms_log_status == 1)
                      {
                        $setclass = 'success';
                        $settext = 'Success';
                      }
                      else
                      {
                        $setclass = 'danger';
                        $settext = 'Failure';
                      }
                      ?>
                      <button class="btn btn-xs btn-<?php echo $setclass ?>"><?php echo $settext ?></button>
                    </td>
                    
                  </tr>
                  <?php }
                  ?>
                </tbody>
              </table>
              <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
                      </div>
                    </div>
                  </div>
                <?php $this->load->view('Template/footer.php') ?>
