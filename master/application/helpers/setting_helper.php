<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function logo()
{
    $CI =& get_instance();
    $getLogodata = $CI->db->select('houdin_admin_logo_image')->from('houdin_admin_logo')->where('houdin_admin_logo_id','1')->get()->result();
    $setImage = base_url()."uploads/logo/".$getLogodata[0]->houdin_admin_logo_image;
    return $setImage;
}
function twillio()
{
    $CI =& get_instance();
    $getTwillio = $CI->db->select('*')->from('houdin_twilio')->where('houdin_twilio_id','1')->get()->result();
    return $getTwillio;
}
function sendgrid()
{
    $CI =& get_instance();
    $getSendgrid = $CI->db->select('*')->from('houdin_sendgrid')->where('houdin_sendgrid_id','1')->get()->result();
    return $getSendgrid;
}
function fromemail()
{
    return "noreply@houdine.com";
}
function toemail()
{
    return "nimit@warrdel.com";
}
function getvendorurl()
{
    return "https://houdine.com/vendor/";
}

function getUserId($token)
{
    $CI =& get_instance();
    $getLogodata = $CI->db->select('houdin_user_auth_user_id')
    ->from('houdin_user_auth')
    ->where('houdin_user_auth_url_token',$token)->get()->result();
    return $getLogodata;
}