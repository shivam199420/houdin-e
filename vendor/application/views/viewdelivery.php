<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style type="text/css">
    .widget-style-2 i {
    background: rgba(244, 248, 251, 0.6) !important;
    font-size: 48px;
    padding: 30px 8px;
}
.custom_lineheight{
    line-height: 2.6;
    margin-bottom: 0px;
}
@media(max-width:767px){
    .custom_lineheight{
    line-height:1.6!important;
    margin-bottom: 0px;
}
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
   <!--START Page-Title -->
            <div class="row">
         
                <div class="col-md-8">
                   <h4 class="page-title">View Delivery</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Delivery</a></li>
                  <li class="active">View Delivery</li>
                  </ol>
                  </div>
                  <div class="col-md-4">
                  <!-- <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form>
                       <div class="btn-group pull-right m-t-10 m-b-20"> -->

        
        <button type="button" class="btn btn-default m-r-5" title="Add customer " data-toggle="modal" data-target="#Order-status"><i class="fa fa-toggle-on"></i></button>

   

        </div>
        </div>
              
            </div>
           <!--END Page-Title --> 

             
        <div class="row m-t-20">
        <div class="col-sm-3">
        

        <div class="card-box no-pad no-margin"> 
       
        <form method="">
        <form method=""> 
        <div class="row">
        <div class="col-md-12 form-horizontal pad-10">
        <h3 class="custom_h3">Personal Info</h3>
        <div class="form-group m-b-0">
        <label class="col-md-4 col-sm-4 col-xs-4 control-label pad-left-20">Name : </label>
        <div class="col-md-8 col-sm-8 col-xs-8">
        <p class="custom_lineheight">Xyz detail</p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-sm-4 col-xs-4 control-label pad-left-20">Last Name:</label>
        <div class="col-md-8 col-sm-8 col-xs-8">
        <p class="custom_lineheight">detail</p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-sm-4 col-xs-4 control-label pad-left-20">Gender : </label>
        <div class="col-md-8 col-sm-8 col-xs-8">
        <p class="custom_lineheight">Xyz detail</p>
        </div>
        </div>
        
        <div class="form-group m-b-0">
        <label class="col-md-4 col-sm-4 col-xs-4 control-label pad-left-20">DOB : </label>
        <div class="col-md-8 col-sm-8 col-xs-8">
       <p class="custom_lineheight">DD/MM/YYYY</p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-sm-4 col-xs-4 control-label pad-left-20">Email : </label>
        <div class="col-md-8 col-sm-8 col-xs-8">
        <p class="custom_lineheight">xyz@gmail.com</p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-sm-4 col-xs-4 control-label pad-left-20">Address : </label>
        <div class="col-md-8 col-sm-8 col-xs-8">
        <p class="custom_lineheight">Xyz, Colony, USA</p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-sm-4 col-xs-4 control-label pad-left-20">City : </label>
        <div class="col-md-8 col-sm-8 col-xs-8">
        <p class="custom_lineheight">Xyz</p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-sm-4 col-xs-4 control-label pad-left-20">Pincode : </label>
        <div class="col-md-8 col-sm-8 col-xs-8">
        <p class="custom_lineheight">302019</p>
        </div>
        </div>
        
           
          
        </div>
      
        </div>
        </form>
        </form>
        </div>

        </div>

        <div class="col-md-9">
            <div class="card-box no-pad no-margin">
                 <div class="row"> 
                        
                            <div class="col-lg-12"> 
                                <ul class="nav nav-tabs navtab-bg nav-justified"> 
                                    <li class="active"> 
                                        <a href="#home1" data-toggle="tab" aria-expanded="false"> 
                                            <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                            <span class="hidden-xs">Delivery boy detail</span> 
                                        </a> 
                                    </li> 
                                    <li class=""> 
                                        <a href="#profile1" data-toggle="tab" aria-expanded="true"> 
                                            <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                            <span class="hidden-xs">Order track</span> 
                                        </a> 
                                    </li> 
                                    <!--<li class=""> 
                                        <a href="#messages1" data-toggle="tab" aria-expanded="false"> 
                                            <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> 
                                            <span class="hidden-xs">Order status</span> 
                                        </a> 
                                    </li> -->
                                   
                                </ul> 
                                <div class="tab-content"> 
                                    <div class="tab-pane active" id="home1"> 
                                      
                                        <div class="row">
                                                 <div class="container">
                                                  <table class="table table-striped table-bordered table_shop_custom">
                                                    <thead>
                                                      <tr>
                                                      
                                                        <th><input type="checkbox"></th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Contact</th>
                                                        <th>Address</th>
                                                      
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td><input type="checkbox"></td>
                                                        <td>John Player</td>
                                                        <td>info@gmail.com</td> 
                                                        <td>xxxxxxxxxx</td>
                                                        <td>Xyz, USA</td>
                                                        
                                                      
                                                      </tr>
                                                    </tbody>
                              </table>
                              </div>
                              </div>
                                    </div> 
                                    <div class="tab-pane" id="profile1"> 
                                        
                                        <div class="row">
                                                 <div class="container">
                                                  <table class="table table-striped table-bordered table_shop_custom">
                                                    <thead>
                                                      <tr>
                                                      
                                                        <th><input type="checkbox"></th>
                                                        <th>Order ID</th>
                                                        <th>Order Date </th>
                                                        <th>Track Order</th>
                                                        <th>Order Status</th>
                                                        
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td><input type="checkbox"></td>
                                                        <td>H124568</td>
                                                        
                                                        <td>DD/MM/YYYY</td>
                                                        <td>Shipped</td> 
                                                        <td>Pending</td>
                                                        
                                                      
                                                      </tr>
                                                    </tbody>
                              </table>
                              </div> 
                              </div>
                                    </div> 
                                    <!--<div class="tab-pane" id="messages1"> 
                                       <div class="row m-t-20">
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="widget-panel widget-style-2 bg-white">
                                                <i class="md md-attach-money text-primary"></i>
                                                <h2 class="m-0 text-dark counter font-600">50568</h2>
                                                <div class="text-muted m-t-5">Total paid for order </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="widget-panel widget-style-2 bg-white">
                                                <i class="md md-add-shopping-cart text-pink"></i>
                                                <h2 class="m-0 text-dark counter font-600">1256</h2>
                                                <div class="text-muted m-t-5">Total unpaid amount </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="widget-panel widget-style-2 bg-white">
                                                <i class="md md-store-mall-directory text-info"></i>
                                                <h2 class="m-0 text-dark counter font-600">18</h2>
                                                <div class="text-muted m-t-5">Total cancelled order</div>
                                            </div>
                                        </div>



                                      </div>
                                        <div class="row">
                                                 <div class="container">
                                                 <img src="../assets/images/revenue1.jpg">
                                                  </div>
                                         </div>
                                    </div> -->
                                    
                                </div> 

                                               
                            </div> 
                        </div>
                         <!-- end row --> 
            </div>
        </div>
        </div>

                                       <div id="con-close-modal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Add Inventory</h4> 
                                                </div> 
                                                <div class="modal-body"> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label">Choose Status</label> 
                                                                <select class="form-control">
                                                                    <option>Receive</option>
                                                                    <option>Return</option>
                                                                    
                                                                </select>
                                                            </div> 
                                                        </div> 
                                                     
                                                    </div> 
                                                    <!--<div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-3" class="control-label">Note</label> 
                                                                <input type="text" class="form-control" id="field-3" placeholder="Quantity"> 
                                                            </div> 
                                                        </div> 
                                                    </div> -->
                                                  
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group no-margin"> 
                                                                <label for="field-7" class="control-label">Note</label> 
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                                </div> 
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->
                              
      </div>
    </div>
  </div>
<!--Delete-->
  <!--change Status-->
        <div id="Order-status" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <form method="post">
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Order Status</h4>

        </div>

        <div class="modal-body">



        <div class="row">
        <div class="col-md-12">

        <div class="form-group no-margin">

        <label for="field-7" class="control-label">Order Status</label>

        <select class="form-control " name=""><option value="">Choose Status</option><option value="1">Active</option><option value="0">Deactive</option><option value="2">Block</option></select>

        </div>
        <div class="form-group no-margin">

        <label for="field-7" class="control-label">Change Status</label>

        <select class="form-control " name=""><option value="">Choose Option</option><option value="1">Active</option><option value="0">Deactive</option><option value="2">Block</option></select>

        </div>

        </div>
        </div>





        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Update Status">

        </div>

        </div>
        </form>
        </div>

        </div> 

  

<!--change Status-->


        

<?php $this->load->view('Template/footer.php') ?>
  <script>
            var resizefunc = [];
        </script>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
