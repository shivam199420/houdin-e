<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header") ?>

    <section>
        <div class="container py-85 py-tn-50">
            <div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8 text-center">
                            <h2 class="title">Change Password</h2>
                            <p class="title-detail">Please enter your details here.</p>
                            <div class="title-border mx-auto m-b-55 m-b-tn-35"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="port-body">
            <?php 
            if($this->session->flashdata('error'))
            {
                echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
            }
            else if($this->session->flashdata('success'))
            {
                echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
            }
            ?>
                <div class="contact-form">
                    <div class="messages" id="status"></div>
                    <?php echo form_open(base_url( 'Changepassword/updatepass' ), array( 'id' => 'changepass', 'method'=>'post' ));?>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                    <div class="help-block with-errors"></div>
                                    <input type="password" placeholder="Your Current Passwrod" name="currentPass" class="required_validation_for_change_pass name_validation form-control" />
                                </div>
                                <div class="form-group au-form">
                                    <div class="help-block with-errors"></div>
                                    <input type="password" placeholder="Your New Passwrod" name="newpass" class="required_validation_for_change_pass name_validation opass form-control" />
                                </div>
                                <div class="form-group au-form">
                                    <div class="help-block with-errors"></div>
                                    <input type="password" placeholder="Re-enter New Passwrod" name="newconfirmpass" class="required_validation_for_change_pass name_validation cpass form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">      
                        <div class="col-md-3"></div>                      
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <input type="submit" id="contactBtn" class="mx-auto setDisableData" value="update Password"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
 <?php 
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer") ?>
     <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#changepass',function(){
                var check_required_field=0;
                $(this).find(".required_validation_for_change_pass").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field++
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field != 0)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>