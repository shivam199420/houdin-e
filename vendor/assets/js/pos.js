$(".select1").select2({
    minimumInputLength: 2
});
// Select serach function 
// set product data
$(document).on('change','.setproductData',function(){
    $(this).parents('.mainParentRow').find('.setMainPrice').text('').text($(this).children('option:selected').attr('data-price'));
    $(this).parents('.mainParentRow').find('.specialPrice').val('').val($(this).children('option:selected').attr('data-price'));
    $(this).parents('.mainParentRow').find('.getQuantity').attr('data-max',$(this).children('option:selected').attr('data-stock'));
    // Call final price function
    var quantity = $(this).parents('.mainParentRow').find('.getQuantity').val();
    var getSpecialPriceData = $(this).parents('.mainParentRow').find('.specialPrice').val();
    var dataSet = $(this);
    if(quantity && getSpecialPriceData)
    {
        finalPrice(quantity,getSpecialPriceData,dataSet); 
    }
    setProductArrayData(dataSet);
});
// get quantity
$(document).on('keyup','.getQuantity',function(e){
    var getSpecialPriceData = $(this).parents('.mainParentRow').find('.specialPrice').val();
    if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
    {
        var quantity = $(this).attr('data-max');
        $(this).val($(this).attr('data-max'));
    }
    else
    {
        var quantity = $(this).val();
    }
    var dataSet = $(this);
    finalPrice(quantity,getSpecialPriceData,dataSet); // Call final price function
    setProductArrayData(dataSet);
    // check enter is pressed or not on quantity
    if(e.which == 13){
        var getEmptyValue = emptyValueCheck()  // call empty valur check function
        if(getEmptyValue == 0)
        {
            var setText = 'add';
            var dataset = $(this);
            sethtmldata(setText)  // call html append function
        }
    }
});
// get coupon code ajax
$(document).on('click','.applyCouponCodeData',function(){
    var getCouponCode = $('.couponCodeData').val();
    var getNetPayable = $('.setNetPayable').text();
    couponCodeAjax(getCouponCode,1,getNetPayable);  // check coupon code ajax
})
// get special price
$(document).on('keyup','.specialPrice',function(){
    var getSpecialPriceData = $(this).val();
    var quantity = $(this).parents('.mainParentRow').find('.getQuantity').val();
    var dataSet = $(this);
    finalPrice(quantity,getSpecialPriceData,dataSet);// Call final price function
    setProductArrayData(dataSet);
});
// set customer type
$(document).on('click','.checkCustomerTypeData',function(){
    // call to check customer type
    checkCustomerTypeData();
    // call to check delivery type
    setDeliveryType();
});

// get customer contact
$(document).on('change','.selectCustomerData',function(){
    $('.setCustomerContact').val($(this).children('option:selected').attr('data-contact'));
    fetchCustomerAddress($(this).val());
    checkCustomerPriv($(this).val())
});
// set delivery type
$(document).on('click','.deliveryTypeData',function(){
    // call delivery type function
    setDeliveryType(); 
    // call to check customer type
    checkCustomerTypeData();
});
// add registred user address
$(document).on('click','.addRegistredUserAddress',function(){
    if($('.selectCustomerData').val())
    {
        $('.addAddressCustomerId').val($('.selectCustomerData').val());
        $('#addRegistredUserAddressModal').modal('show');
        $('.setErrorMessage').hide();
    }
    else
    {
        $('.setErrorMessage').show();
        $('.setErrorMessage').text('').text('Please choose customer');
    }   
});
// client side validaion
$(document).on('submit','#addPosForm',function(e){
    $('.setErrorMessage').hide();
    $('.setSucccessMessage').hide();
var check_required_field='';
$(this).find(".required_validation_for_pos").each(function(){
    var val22 = $(this).val();
    if (!val22){
        check_required_field =$(this).size();
        $(this).css("border-color","#ccc");
        $(this).css("border-color","red");
    }
    $(this).on('keypress change',function(){
        $(this).css("border-color","#ccc");
    });
});
if(check_required_field)
{
    return false;
}
else {
    e.preventDefault();
    $('.addRegisteredUserBtnData').prop('disabled',true);
    var formValue = $('#addPosForm').serializeArray();
    jQuery.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>POS/addRegisteredUserAddress",
    data: formValue,
    success: function(data) {
        var getJsonData = jQuery.parseJSON(data);
        if(getJsonData.messgae == 'Address is addedd successfully')
        {
            $('#addRegistredUserAddressModal').modal('hide');
            $('.setSucccessMessage').show()
            $('.setSucccessMessage').text('').text(getJsonData.messgae);
            // call custoemr address function
            fetchCustomerAddress($('.selectCustomerData').val());
        }
        else
        {
            $('#addRegistredUserAddressModal').modal('hide');
            $('.setErrorMessage').show();
            $('.setErrorMessage').text('').text(getJsonData.messgae);
        }
    }
    }); 
    
}
});
// add address for non registered users
$(document).on('click','.addNonRegistredUserAddress',function(){
$('#addNonRegistredUserAddressModal').modal('show');
});
// edit current address of non registered user
$(document).on('click','.setAddressDataValue',function(){
$('.nonUserMainAddress').text($(this).attr('data-address'));
$('.nonUserCity').val($(this).attr('data-city'));
$('.nonUserZip').val($(this).attr('data-zip'));
$('#addNonRegistredUserAddressModal').modal('show');
})
// set validation for non registred user
$(document).on('submit','#addNonRegisteredForm',function(e){
    $('.setErrorMessage').hide();
    $('.setSucccessMessage').hide();
    $('.addNonRegisteredUserBtnData').prop('disabled',true);
var check_required_field='';
$(this).find(".required_validation_for_pos_non").each(function(){
    var val22 = $(this).val();
    if (!val22){
        check_required_field =$(this).size();
        $(this).css("border-color","#ccc");
        $(this).css("border-color","red");
    }
    $(this).on('keypress change',function(){
        $(this).css("border-color","#ccc");
    });
});
if(check_required_field)
{
    $('.addNonRegisteredUserBtnData').prop('disabled',false);
    return false;
}
else 
{
    e.preventDefault();
    $('.addNonRegisteredUserBtnData').prop('disabled',false);
    var formValue = $('#addNonRegisteredForm').serializeArray();
    var getMainAddress = formValue[1].value;
    var getCity = formValue[2].value;
    var getZip = formValue[3].value;
    $('.setAddressDataValue').val('').val(getMainAddress+", "+getCity+", "+getZip);
    $('.setAddressDataValue').attr('data-address',getMainAddress);
    $('.setAddressDataValue').attr('data-city',getCity);
    $('.setAddressDataValue').attr('data-zip',getZip);
    $('#addNonRegistredUserAddressModal').modal('hide');
    var setAddressString = getMainAddress+"^"+getCity+"^"+getZip;
    $('.setNonRegisteredCustomerAddress').val(setAddressString);
}
});
// set additionla adjustment
$(document).on('click','.additionalAdjustmentCheckboxData',function(){
$('.setPrivMaxValue').val('');
if($(this).prop('checked') == true)
{
    $('.setPrivMaxValue').val($('.setPrivMaxValue').attr('data-max'));
    $('.additionalAdjustmentText').show();
}    
else
{
    $('.setPrivMaxValue').val('');
    $('.additionalAdjustmentText').hide();
}
setFinalPriceValue();
});
// set adjustment on price
$(document).on('keyup','.setPrivMaxValue',function(){
var getAdjustMentValue = $(this).val();
setPendingAmountData(getAdjustMentValue);
})
// final price function 
function finalPrice(quantity,specialprice,dataSet)
{
    if(specialprice)
    {
        if(quantity)
        {
            var setQuantity = quantity;
        }
        else
        {
            setQuantity = 0;
            dataSet.val(setQuantity);
        }
        var setFinalPrice = (parseFloat(specialprice)*parseFloat(setQuantity))
        dataSet.parents('.mainParentRow').find('.setFinalPrice').text('').text(setFinalPrice);
        dataSet.parents('.mainParentRow').find('.specialPrice').css('border','1px solid #d3d3d3');
    }
    else
    {
        dataSet.val(0);
        dataSet.parents('.mainParentRow').find('.specialPrice').css('border','1px solid red');
        dataSet.parents('.mainParentRow').find('.setFinalPrice').text('0');
    }
    setFinalPriceValue(); // set final value
}
// add html or remove html
function sethtmldata(setText,dataset)
{
    if(setText == 'add')
    {
        var countRows = ($('.mainParentRow').length)+1;
        $('.setRowData').append('<tr class="mainParentRow"><input type="hidden" class="inputProdcutId" name="inputProdcutId[]"/><input type="hidden" class="inputDiscountedPrice" name="inputDiscountedPrice[]"/><input type="hidden" class="inputVariantId" name="inputVariantId[]"/><input type="hidden" class="inputQuantity" name="inputQuantity[]"/><input type="hidden" class="inputActualPrice" name="inputActualPrice[]"/><input type="hidden" class="inputTotalPrice" name="inputTotalPrice[]"/><td><div class="form-group" style="margin-bottom: 0px!important;"><select class="form-control setproductData select_auto'+countRows+'"><option value="">Choose Product</option><?php foreach($productData as $productDataList){?><option data-price="<?php echo $productDataList['productPrice'] ?>" data-stock="<?php echo $productDataList['stock'] ?>" data-variant="<?php echo $productDataList['variantId'] ?>" data-product="<?php echo $productDataList['produtcId'] ?>"><?php echo $productDataList['productName'] ?></option><?php } ?></select></div></td><td class="setMainPrice"></td><td><input type="text" value="" class="form-control specialPrice number_validation" name="" placeholder=""></td><td><input type="text" value="" class="form-control getQuantity number_validation" name="" placeholder=""></td><td class="setFinalPrice">0</td></tr>');
        $(".select_auto"+countRows+"").select2({
            minimumInputLength: 2
        });
    }
}
// calculate final price value
function setFinalPriceValue()
{
    // each function to calculate total
    var setTotalData = 0;
    $('.setFinalPrice').each(function(){
        setTotalData = setTotalData+parseFloat($(this).text());
    });
    // set subtotal
    $('.setFinalTotal').text('').text(setTotalData);
    var getDiscount = $('.setCouponDiscount').text();
    var getFinalPayable = setTotalData-parseFloat(getDiscount);
    $('.setNetPayable').text(getFinalPayable);
    // update coupon code data
    var getCouponCode = $('.couponCodeData').val();
    var getCustomer = $('.selectCustomerData').val();
    var getNetPayable = $('.setFinalTotal').text();
    if(getCouponCode && getCustomer && getNetPayable)
    {
        couponCodeAjax(getCouponCode,getCustomer,getNetPayable);
    }
    var getAdjustMentValue = $('.setPrivMaxValue').val();
    if(getAdjustMentValue && getAdjustMentValue > 0)
    {
        setPendingAmountData(getAdjustMentValue)
    }
    // set amount
    $('.finalAmountData').val($('.setFinalTotal').text());
    $('.finalDiscountData').val($('.setCouponDiscount').text());
    $('.netPayableAmount').val($('.setNetPayable').text());
}
// check all empty value
function emptyValueCheck()
{
    // product select 
    var checkData = 0;
    $('.setproductData').each(function(){
        if($(this).val() == '')
        {
            $(this).css('border','1px solid red');
            checkData++;
        }
        else
        {
            $(this).css('border','1px solid #d3d3d3');
        }
    });
    // check special price value
    $('.specialPrice').each(function(){
        if($(this).val() == 0 || $(this).val() == "")
        {
            $(this).css('border','1px solid red');
            checkData++;
        }
        else
        {
            $(this).css('border','1px solid #d3d3d3');
        }
    });
    // check quantity
    $('.getQuantity').each(function(){
        if($(this).val() == 0 || $(this).val() == "")
        {
            $(this).css('border','1px solid red');
            checkData++;
        }
        else
        {
            $(this).css('border','1px solid #d3d3d3');
        }
    });
     return checkData;
}
// check coupon ajax data
function couponCodeAjax(coupon,customerId,getNetPayable)
{
    $('.setErrorMessage').hide();
    $('.setSucccessMessage').hide();
    $('.applyCouponCodeData').prop('disabled',true);
    jQuery.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>POS/getCouponValidity",
    data: {"coupon": coupon,'customerId':customerId,'getNetPayable':getNetPayable},
    success: function(data) {
       var getJsonData = jQuery.parseJSON(data);
       if(getJsonData.message == 'Success')
       {
           $('.setSucccessMessage').show().text('Coupon code applied Succefully')
           var getSubTotal = $('.setFinalTotal').text();
           var couponDiscount = getJsonData.discount;
           var appliedCouponId = getJsonData.couponId;
           $('.couponCodeId').val(appliedCouponId);
           if(couponDiscount > 0)
           {
               var setNetDiscount = (parseFloat(getSubTotal)*parseFloat(couponDiscount))/100;
               var setNetPayable = parseFloat(getSubTotal)-setNetDiscount;
           }
           else
           {
               var setNetDiscount = 0;
               var setNetPayable = getSubTotal;
           }
           $('.setCouponDiscount').text(setNetDiscount);
           $('.setNetPayable').text(setNetPayable);
           $('.applyCouponCodeData').prop('disabled',false);
           var getAdjustMentValue = $('.setPrivMaxValue').val();
           if(getAdjustMentValue && getAdjustMentValue > 0)
           {
               setPendingAmountData(getAdjustMentValue)
            }
            // set amount
            $('.finalAmountData').val($('.setFinalTotal').text());
            $('.finalDiscountData').val($('.setCouponDiscount').text());
            $('.netPayableAmount').val($('.setNetPayable').text());
       }
       else
       {
            $('.applyCouponCodeData').prop('disabled',false);
            $('.setErrorMessage').show().text(getJsonData.message);
       }
    }
    }); 
}
// fetch customer registred address
function fetchCustomerAddress(customerId)
{
    var getDeliveryType = setDeliveryType();
    if(getDeliveryType == 'HD')
    {
        $('.setErrorMessage').hide();
        $('.setSucccessMessage').hide();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>POS/getCustomerAddress",
        data: {'customerId':customerId },
        success: function(data) {
            var getJsonData = jQuery.parseJSON(data);
            if(getJsonData.message == 'Success')
            {
                var setCustomerAddress = '<option value="">Choose Address</option>';
                $.each(getJsonData.data,function(){
                    setCustomerAddress += '<option value="'+this.addressId+'">'+this.mainAddress+', '+this.city+', '+this.zip+'</option>'
                });
                $('.setRegisteredCustomerAddress').html(setCustomerAddress);
            }
            else
            {
                $('.setErrorMessage').show().text(getJsonData.message);
            }
        }
        }); 
    }
}
// check deliveryType Data
function setDeliveryType()
{
    var setPickupValue= "";
    $('.deliveryTypeData').each(function(){
        if($(this).prop('checked') == true)
        {
            if($(this).val() == 'HD')
            {
                $('.setAddressData').show();
                setPickupValue = 'HD';
            }
            else
            {
                $('.setAddressData').hide();
                setPickupValue = 'PC';
            }
        }
    });
    return setPickupValue;
}
// check customer type function
function checkCustomerTypeData()
{
    $('.additionalAdjustmentCheck').hide();
    $('.selectCustomerData').val('');
    $('.setCustomerContact').val('');
    $('.checkCustomerTypeData').each(function(){
    if($(this).prop('checked') == true)
    {
        var getValue = $(this).val();
        if(getValue == 'RC')
        {
            $('.registerdCustomerData').show();
            $('.nonRegisterdCustomerData').hide();
        }
        else
        {
            $('.registerdCustomerData').hide();
            $('.nonRegisterdCustomerData').show();
        }
    }
})
}
function checkCustomerPriv(customerId)
{
    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>POS/getCustomerPrivilage",
        data: {'customerId':customerId },
        success: function(data) {
            var getData = jQuery.parseJSON(data);
            if(getData.message == 'Success')
            {
                if(getData.dataValuePriv > 0)
                {
                    $('.additionalAdjustmentCheck').show();
                    $('.setPrivMaxValue').attr('data-max',getData.dataValuePriv)
                }
                else
                {
                    $('.additionalAdjustmentCheck').hide();
                    $('.setPrivMaxValue').attr('data-max','')
                }
            }
        }
        }); 
}
function setPendingAmountData(priceData)
{
    $('.setErrorMessage').hide();
    if(parseFloat($('.setPrivMaxValue').attr('data-max')) < parseFloat(priceData))
    {
        $('.setPrivMaxValue').val($('.setPrivMaxValue').attr('data-max'));
    }
    var getTotalAmount = $('.setFinalTotal').text();
    var getDiscount = $('.setCouponDiscount').text();
    var getFianlNetPayable = (parseFloat(getTotalAmount)-parseFloat(getDiscount));
    var additionalAdjustment = $('.setPrivMaxValue').val();
    if(getFianlNetPayable && getFianlNetPayable > 0 && additionalAdjustment && additionalAdjustment > 0)
    {
        if(getFianlNetPayable > additionalAdjustment)
        {
            var setNewNetPayable = getFianlNetPayable - additionalAdjustment;
        }
        else
        {
            var setNewNetPayable = additionalAdjustment - getFianlNetPayable;
        }
        $('.setNetPayable').text('').text(setNewNetPayable);
    }
    else
    {
        $('.setPrivMaxValue').val('1');
    }
}
// set product value
function setProductArrayData(dataset)
{
    var getProductId = dataset.parents('.mainParentRow').find('.setproductData').children('option:selected').attr('data-product');
    var getVariantId = dataset.parents('.mainParentRow').find('.setproductData').children('option:selected').attr('data-variant');
    var actualPrice = dataset.parents('.mainParentRow').find('.specialPrice').val();
    var quantityData = dataset.parents('.mainParentRow').find('.getQuantity').val();
    var finalPriceData = dataset.parents('.mainParentRow').find('.setFinalPrice').text();
    var productInitialPrice = dataset.parents('.mainParentRow').find('.setMainPrice').text();
    var findDiscountData = parseFloat(productInitialPrice)-parseFloat(actualPrice);
    dataset.parents('.mainParentRow').find('.inputProdcutId').val(getProductId);
    dataset.parents('.mainParentRow').find('.inputVariantId').val(getVariantId);
    dataset.parents('.mainParentRow').find('.inputQuantity').val(quantityData);
    dataset.parents('.mainParentRow').find('.inputActualPrice').val(actualPrice);
    dataset.parents('.mainParentRow').find('.inputTotalPrice').val(finalPriceData);
    dataset.parents('.mainParentRow').find('.inputDiscountedPrice').val(findDiscountData);
}
$(document).on('click','.submitMainFormData',function(){
    var setData = 0;

    $('.checkCustomerTypeData').each(function(){
        if($(this).prop('checked') == true)
        {
            if($(this).val() == 'RC')
            {
                $('.requiredValidationForCustomerName').each(function(){
                    if($(this).val() == "")
                    {
                        setData++;
                        $(this).css('border','1px solid red');
                    }
                    else
                    {
                        $(this).css('border','1px solid #d3d3d3');
                    }
                });
                $('.deliveryTypeData').each(function(){
                if($(this).prop('checked') == true)
                {
                    if($(this).val() == 'HD')
                    {
                        if($('.requiredValidationForRegisteredAddress').val() == "")
                        {
                            setData++;
                            $('.requiredValidationForRegisteredAddress').css('border','1px solid red');
                        }
                        else
                        {
                            $('.requiredValidationForRegisteredAddress').css('border','1px solid #d3d3d3');
                        }
                    }
                    else
                    {
                        if($('.requiredValidationForNonRegisteredAddress').val() == "")
                        {
                            setData++;
                            $('.requiredValidationForNonRegisteredAddress').css('border','1px solid red');
                        }
                        else
                        {
                            $('.requiredValidationForNonRegisteredAddress').css('border','1px solid #d3d3d3');
                        }
                    }
                }
            })
            }
            else
            {
                $('.requiredValidationForNonRegisteredUser').each(function(){
                    if($(this).val() == "")
                    {
                        setData++;
                        $(this).css('border','1px solid red');
                    }
                    else
                    {
                        $(this).css('border','1px solid #d3d3d3');
                    }
                })
            }
        }
    });
    var getValueData = emptyValueCheck();
    setData = getValueData;
    if($('.checkPaymentStatus').val() == "")
    {
        setData++;
        $('.checkPaymentStatus').css('border','1px solid red');
    }
    else
    {
        $('.checkPaymentStatus').css('border','1px solid #d3d3d3');
    }
    console.log(setData);
    if(setData == 0)
    {
        generateOrderData();
    }
    else
    {
        return false;
    }
});
function generateOrderData()
{
    $('#submitMainForm').submit();
}