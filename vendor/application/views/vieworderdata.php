        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">
        <!--START Page-Title -->
        <div class="row">
         
        <div class="col-md-8">
        <h4 class="page-title">View Order</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
       <li><a href="<?php echo base_url(); ?>order/Orderlanding">Order</a></li>
        <li><a href="<?php echo base_url(); ?>order/allorders">All Order</a></li>

        <li class="active">View Order</li>
        </ol>  
        </div>
        
        </div>
        <!--END Page-Title -->

        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
        <div class="row m-t-20">
        <div class="col-md-9">
        <div class="panel panel-default">
        <!-- <div class="panel-heading">
        <h4>Invoice</h4>
        </div> -->
        <?php 
        // print_r($mainData);
        ?>
        <div class="panel-body" style="border: 1px solid #eee;">
        <div class="custom-margin-order">
        <div class="pull-left">
        <b>Order ID : <span class="text_lightblue">#<?php echo $mainData->houdinv_order_id ?></span></b><br/>
        <b>Order Status : <span class="text_lightblue text-uppercase"><?php echo $mainData->houdinv_order_confirmation_status ?></span></b><br>
        <?php 
        if($mainData->houdinv_order_confirmation_status != 'return' && $mainData->houdinv_order_confirmation_status != 'cancel')
        {
        ?>
        <button style="margin-top:10px" data-id="<?php echo $mainData->houdinv_order_id ?>" class="custom-btn-multiple-section returnorderBtn">Return Order</button>
        <button data-id="<?php echo $mainData->houdinv_order_id ?>" class="custom-btn-multiple-section cancelOrderBtn">Cancel Order</button>
        <?php }
        if($mainData->houdinv_payment_status == 1 && $mainData->houdinv_orders_refund_status == 0)
        {
        ?>
        <button data-amount="<?php echo $mainData->houdinv_orders_total_paid; ?>" data-id="<?php echo $mainData->houdinv_order_id ?>" class="custom-btn-multiple-section refundAmount">Initiate Refund</button>
        <?php }
        ?>
        </div>
        <div class="pull-right">
        <b>Date : <span class="text_lightblue"><?php echo date('d-m-Y h:i',$mainData->houdinv_order_created_at) ?></span></b><br><br>
        <a style="margin-top:10px" class="link-a-blue custom-btn-multiple-section sendCustomerInvoiceEmail" data-id="<?php echo $mainData->houdinv_order_id ?>" data-email="<?php echo  $mainData->houdinv_user_email ?>" href="javascript:;">Sms / Email</a>
        <a class="link-a-blue custom-btn-multiple-section" target="_blank" href="<?php echo base_url() ?>order/printinvoicestatement/<?php echo $mainData->houdinv_order_id ?>">Print / View</a>
        <a class="link-a-blue custom-btn-multiple-section" target="_blank" href="<?php echo base_url() ?>order/orderinvoicepdf/<?php echo $mainData->houdinv_order_id ?>">Download</a>
        </div>
        
        </div>
        <hr>
            <div class="clearfix"></div>
 
        <div class="multiple-section">
        <h4 class="multiple-section-h4">Customer Detail</h4>
 
        <div class="col-md-4"> <p><i class="fa fa-user"></i> <?php echo $mainData->houdinv_user_name ?></p></div>
        
        
        <div class="col-md-4"><p><i class="fa fa-phone"></i> <?php echo $mainData->houdinv_user_contact ?></p></div>
        
      
        <div class="col-md-4"><p><i class="fa fa-envelope"></i> <?php echo $mainData->houdinv_user_email ?></p></div>
        <div class="clearfix"></div>
        </div>

        <div class="row">
        <div class="col-md-12">
        <div class="table-responsive">
        <div class="multiple-section-h4">
        <div class="pull-left">
        <b><i class="fa fa-minus-circle"></i> Confirmed Products</b>

        </div>
        </div>

        <table class="table m-t-30">

        <thead>
        <tr>
        <th>Product Name</th>
        <th>Quantity</th>
        <th>Rate</th>
        <th>Tax</th>
        <th>Total Price</th>
        </tr></thead>
        <tbody>
        <?php 
        $setSubTotal = 0;
        $setTax = 0;
        foreach($mainProductData as $mainProductDataList)
        {
            $setSubTotal = $setSubTotal+($mainProductDataList['saleprice']*$mainProductDataList['quantity']);
            $setTax = $setTax+$mainProductDataList['totaltax'];
        ?>
        <tr>
        <td><?php echo $mainProductDataList['productName'] ?></td>
        <td><?php echo $mainProductDataList['quantity'] ?></td>
        <td><?php echo $mainProductDataList['saleprice'] ?></td>
        <td><?php echo $mainProductDataList['totaltax'] ?></td>
        <td><?php echo $mainProductDataList['total'] ?></td>
        </tr>
        <?php }
        ?>

        </tbody>
        </table>
        <hr style="margin-top: 0px!important">
        </div>
        </div>
        </div>
        <div class="row" style="border-radius: 0px;">
        <div class="col-md-4 col-md-offset-8">
        <p class="text-right"><span class="custom-width-set">Gross Amount :</span> <?php echo $setSubTotal ?></p>
        <p class="text-right"><span class="custom-width-set">Tax :</span> <?php echo $setTax ?></p>
        <p class="text-right"><span class="custom-width-set">Additional discount :</span> <?php if($mainData->houdinv_orders_discount) { echo $mainData->houdinv_orders_discount; } else { echo 0; } ?></p>
       
        <p class="text-right"><span class="custom-width-set">Delivery charges :</span> <?php if($mainData->houdinv_delivery_charge) { echo $mainData->houdinv_delivery_charge; } else { echo 0; } ?></p>
        <p class="text-right"><span class="custom-width-set">Round off :</span> <?php echo ceil($mainData->houdinv_orders_total_Amount) ?> </p>
        <p class="text-right"><span class="custom-width-set"><b>Net Amount :</span> <?php echo ceil($mainData->houdinv_orders_total_Amount) ?></b></p>

        </div>
        </div>
        <hr> 
        <div class="row" style="border-radius: 0px;">
        <div class="col-md-4 col-md-offset-8">
        <p class="text-right"><span class="custom-width-set">Total Paid Amount :</span> <?php echo $mainData->houdinv_orders_total_paid; ?> </p>
        <p class="text-right"><span class="custom-width-set">Total Remaining Amount :</span> <?php echo $mainData->houdinv_orders_total_remaining; ?> </p>

        </div>
        </div>
        <!--<div class="hidden-print">
        <div class="pull-right">
        <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
        <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
        </div>
        </div>-->
        </div>
        </div>
          <div class="clearfix"></div>

         <div class="multiple-section">
        <h4 class="multiple-section-h4">Order Note <span class="custom-read-link editUserComment" style="cursor:pointer;" data-text="<?php echo $mainData->houdinv_order_comments ?>"> <i class="fa fa-pencil-square-o"></i>Edit</span></h4>

        <div class="col-md-1"><i class="fa fa-sticky-note-o"></i></div>
        <div class="col-md-10"><p class="setCommentText"><?php echo substr($mainData->houdinv_order_comments,0,120); ?></p>
        <?php 
        if(strlen($mainData->houdinv_order_comments) > 120)
        {
        ?> 
        <span class="custom-read-link userCommentMore" style="cursor:pointer;" data-text="<?php echo $mainData->houdinv_order_comments ?>">Read More</span>
        <span class="custom-read-link userCommentLess" data-text="<?php echo substr($mainData->houdinv_order_comments,0,120); ?>" style="display:none;cursor:pointer;">Read less</span>
        <?php }
        ?>
        </div>
        <div class="clearfix"></div>

        </div>
        </div>
        <div class="col-md-3">
        <div class="panel panel-default">
        <!-- <div class="panel-heading">
        <h4>Invoice</h4>
        </div> -->
        <div class="panel-body" style="padding: 0px !important;">
       <!--  <div class="clearfix"></div>
        <div class="multiple-section">
        <h4 class="multiple-section-h4" style="margin-top: 0px!important">Contact</h4>
        

        </div>
        <div class="clearfix"></div>
        <div class="multiple-section">
        <h4 class="multiple-section-h4">Invoice / Proforma</h4>
        
        </div> -->
        <div class="clearfix"></div>
        <div class="multiple-section">
        <h4 class="multiple-section-h4">Order Status</h4>
        <button class="custom-btn-multiple-section color-brown text-uppercase"><?php if($mainData->houdinv_payment_status == 1) { echo "PAID"; } else { echo "NOT PAID"; } ?></button>
        <button class="custom-btn-multiple-section color-brown text-uppercase"><?php echo $mainData->houdinv_order_confirmation_status ?></button>

        </div>
        <div class="clearfix"></div>

        <div class="multiple-section">
        <h4 class="multiple-section-h4">Sales Channel</h4>
        <div class="col-md-1"><a class="link-a-blue" href="javascript:;"><?php if($mainData->houdinv_order_type == 'Website') { ?><i class="fa fa-desktop"></i> <?php } else if($mainData->houdinv_order_type == 'App') { ?> <i class="fa fa-mobile"></i> <?php  } else { ?> <i class="fa fa-shopping-cart"></i> <?php  } ?></a></div>
        <div class="col-md-10"><p><?php echo $mainData->houdinv_order_type ?></p></div>
        <div class="clearfix"></div>
        <hr>
        <h4 class="multiple-section-h4">delivery Detail</h4>
        <?php
        if($mainData->houdinv_orders_deliverydate)
        {
        ?>
        <div class="col-md-1"><i class="fa fa-calendar"></i></div>
        <div class="col-md-10"><p><?php echo date('d-m-Y',strtotime($mainData->houdinv_orders_deliverydate)); ?>&nbsp;<span class="custom-read-link editDeliveryDate" style="cursor:pointer" data-date="<?php echo $mainData->houdinv_orders_deliverydate ?>">Edit</span></p></div>
        <div class="clearfix"></div>
       <?php  }    
       if(count($deliveryData) > 0)
       {
        ?>
        <div class="col-md-1"><i class="fa fa-truck"></i></div>
        <div class="col-md-10"><p><?php echo $mainData->houdinv_delivery_by ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-1"><i class="fa fa-user"></i></div>
        <div class="col-md-10"><p><?php echo $deliveryData['name'] ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-1"><i class="fa fa-phone"></i></div>
        <div class="col-md-10"><p><?php echo $deliveryData['number'] ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-1"><i class="fa fa-envelope"></i></div>
        <div class="col-md-10"><p><?php echo $deliveryData['email'] ?></p></div>
        <div class="clearfix"></div>
       <?php }
        ?>
        </div>
       
        <div class="clearfix"></div>

        <div class="multiple-section">
        <h4 class="multiple-section-h4">Shipping Address</h4>
        <div class="col-md-1"><i class="fa fa-user"></i></div>
        <div class="col-md-10"><p><?php echo $shippingAddress[0]->houdinv_user_address_name ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-1"><i class="fa fa-phone"></i></div>
        <div class="col-md-10"><p><?php echo $shippingAddress[0]->houdinv_user_address_phone ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-1"><i class="fa fa-map-marker"></i></div>
        <div class="col-md-10"><p><?php echo $shippingAddress[0]->houdinv_user_address_user_address.", ".$shippingAddress[0]->houdinv_user_address_city." ".$shippingAddress[0]->houdinv_user_address_zip ?></p></div>
        <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
        <div class="multiple-section">
        <h4 class="multiple-section-h4">Billing Address</h4>
        <div class="col-md-1"><i class="fa fa-user"></i></div>
        <div class="col-md-10"><p><?php echo $billingAddress[0]->houdinv_user_address_name ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-1"><i class="fa fa-phone"></i></div>
        <div class="col-md-10"><p><?php echo $billingAddress[0]->houdinv_user_address_phone ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-1"><i class="fa fa-map-marker"></i></div>
        <div class="col-md-10"><p><?php echo $billingAddress[0]->houdinv_user_address_user_address.", ".$billingAddress[0]->houdinv_user_address_city." ".$billingAddress[0]->houdinv_user_address_zip ?></p></div>
        <div class="clearfix"></div>
        </div>
        
        <div class="clearfix"></div>
      
       

        <?php 
        if($mainData->w_name || $mainData->w_pickup_address)
        {
        ?>
        <div class="multiple-section">
        <h4 class="multiple-section-h4">Outlet</h4>
        <div class="col-md-1"><i class="fa fa-user"></i></div>
        <div class="col-md-10"><p><?php echo $mainData->w_name ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-1"><i class="fa fa-map-marker"></i></div>
        <div class="col-md-10"><p><?php echo $mainData->w_pickup_address.", ".$mainData->w_pickup_city ?></p></div>
        </div>
        <?php }
        ?>
        <div class="clearfix"></div>
        <?php 
        if(count($confirmBy) > 0)
        {
        ?>
         <div class="multiple-section">
        <h4 class="multiple-section-h4">Billed by</h4>
        <div class="col-md-1"><i class="fa fa-user"></i></div>
        <div class="col-md-10"><p><?php echo $confirmBy['name'] ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-1"><i class="fa fa-phone"></i></div>
        <div class="col-md-10"><p><?php echo $confirmBy['phone'] ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-1"><i class="fa fa-envelope"></i></div>
        <div class="col-md-10"><p><?php echo $confirmBy['email'] ?></p></div>
        <div class="clearfix"></div>
        </div>
        <?php }
        ?>
       
        <div class="clearfix"></div>
        </div>
        </div>          
        </div>

        </div>

        </div> <!-- container -->

        </div> <!-- content -->
        </div>

        <!-- Update customer comment -->
        <div class="modal fade" id="updateCustomerCommentModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Update Customer Comment</strong></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="form-group">
        <label>Customer Comment</label>
        <div class="form-group">
        <textarea class="form-control setUserComment" name="setUserComment" row="4"></textarea>
        </div>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="updateUserOrderComment" value="Update"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div> 

        <!-- Update delivery Date -->
        <div class="modal fade" id="updateDeliveryDateModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Update Delivery Date</strong></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="form-group">
        <label>Delivery Date</label>
        <div class="form-group">
        <input type="text" class="form-control date_picker updateDeliveryDateData" placeholder="Delivery Date" name="updateDeliveryDateData"/>
        </div>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="updateDeliveryDate" value="Update"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>

         <!--Send Email to user  -->
         <div class="modal fade" id="sendInvoiceEmailModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Send Invoice</strong></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="form-group">
        <input type="hidden" name="customerEmailData" class="customerEmailData"/>
        <input type="hidden" name="customerOrderData" name="customerOrderData"/>
        <label>Do you really want to send invoice on customer registered email address?</label>
        
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="sendCustomerInvoice" value="Send"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div> 
        <!-- retrun order -->
        <div class="modal fade" id="returnOrderModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Return order</strong></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="form-group">
        <input type="hidden" name="retrunOrderId" class="retrunOrderId"/>
        <label>Do you really want to update the order status?</label>
        
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="returnOrderData" value="Update"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div> 
        <!-- Cancel order -->
        <div class="modal fade" id="cancelOrderModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Cancel order</strong></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="form-group">
        <input type="hidden" name="cancelOrderId" class="cancelOrderId"/>
        <label>Do you really want to update the order status?</label>
        
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="cancelOrderData" value="Update"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div> 
        <div class="modal fade" id="refundOrderModal">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'order/vieworder/'.$this->uri->segment('3').'' ), array('method'=>'post')); ?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><strong>Refund order</strong></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="form-group">
        <input type="hidden" name="refundOrderId" class="refundOrderId"/>
        <input type="hidden" name="refundOrderAmount" class="refundOrderAmount"/>
        <label>Do you really want to initiate the refund?</label>
        
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" name="refundOrderData" value="Update"/>
        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Cancel</button>												
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div> 
        <?php $this->load->view('Template/footer.php') ?>
        <script type="text/javascript">
        $(document).ready(function(){
                $(document).on('click','.userCommentMore',function(){
                        $(this).hide();
                        $('.setCommentText').html('');
                        $('.setCommentText').html($(this).attr('data-text'));
                        $('.userCommentLess').show();
                })
                $(document).on('click','.userCommentLess',function(){
                        $(this).hide();
                        $('.setCommentText').html('');
                        $('.setCommentText').html($(this).attr('data-text'));
                        $('.userCommentMore').show();
                });
                $(document).on('click','.editUserComment',function(){
                        $('.setUserComment').text($(this).attr('data-text'));
                        $('#updateCustomerCommentModal').modal('show');
                });
                $(document).on('click','.editDeliveryDate',function(){
                        $('.updateDeliveryDateData').val($(this).attr('data-date'));
                        $('#updateDeliveryDateModal').modal('show');
                });
                $(document).on('click','.sendCustomerInvoiceEmail',function(){
                        $('.customerEmailData').val($(this).attr('data-email'));
                        $('.customerOrderData').val($(this).attr('data-id'));
                        $('#sendInvoiceEmailModal').modal('show');
                });
                $(document).on('click','.returnorderBtn',function(){
                        $('.retrunOrderId').val($(this).attr('data-id'));
                        $('#returnOrderModal').modal('show');
                });
                $(document).on('click','.cancelOrderBtn',function(){
                        $('.cancelOrderId').val($(this).attr('data-id'));
                        $('#cancelOrderModal').modal('show');
                });
                $(document).on('click','.refundAmount',function(){
                        $('.refundOrderId').val($(this).attr('data-id'));
                        $('.refundOrderAmount').val($(this).attr('data-amount'));
                        $('#refundOrderModal').modal('show');
                })
        })
        </script>