<footer class="footer text-right">
    © <?php echo date('Y') ?>. All rights reserved.
</footer>
</div>
<script>
    var resizefunc = [];
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<?php 
$getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
$getVendorLanguage = getVendorLanguage();
if($getVendorLanguage[0]->houdin_language_name)
{
    $setLanguage = $getVendorLanguage[0]->houdin_language_name;
}
else
{
    $setLanguage = 'en';
}
?>
<iframe id="tranalateframe" src="<?php echo base_url() ?>translate.php#googtrans(<?php echo $setLanguage; ?>)" style="display:none"></iframe> 
<script type="text/javascript">function googleTranslateElementInit() {new google.translate.TranslateElement({pageLanguage: 'es', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');}</script>
<script type="text/javascript">
	jQuery(document).ready(function(){ 
		  jQuery('#tranalateframe').on('load',function(){ 
            //   alert('load');
			//   jQuery(this).remove();
			var createTranalate = document.createElement("script");
			createTranalate.type = "text/javascript";
			createTranalate.src = "//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit";
			jQuery("head").append(createTranalate); 
		  });
	});
</script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/plugins/peity/jquery.peity.min.js"></script>

<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/counterup/jquery.counterup.min.js"></script>



<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>



<script src="<?php echo base_url(); ?>assets/plugins/raphael/raphael-min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/jquery-knob/jquery.knob.js"></script>


<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/validation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/intlTelInput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=tss8slf34egt7xabxhdnie3a2o4k4b9jq51pqx7qa20zdrct"></script> -->
<script src="<?php echo base_url(); ?>assets/plugins/summernote/summernote.min.js"></script>

<!-- HIGH CHART -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<?php 
$masterDB = $this->load->database('master',true);
$getLanguage = $masterDB->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id,houdin_user_shops.houdin_user_shops_language,houdin_languages.houdin_language_name')
->from('houdin_vendor_auth')->where('houdin_vendor_auth.houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))
->join('houdin_user_shops','houdin_user_shops.houdin_user_shop_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id','left outer')
->join('houdin_languages','houdin_languages.houdin_language_id = houdin_user_shops.houdin_user_shops_language','left outer')->get()->result();


?>
<script>

jQuery(document).ready(function(){

$('.summernote').summernote({
height: 350,                 // set editor height
minHeight: null,             // set minimum height of editor
maxHeight: null,             // set maximum height of editor
focus: false                 // set focus to editable area after initializing summernote
});

$('.inline-editor').summernote({
airMode: true            
});

});
</script>
  <script>//tinymce.init({ selector:'.setEditor' });</script>
<script>
	$(document).ready(function(){
    $(".Internationphonecode").intlTelInput({
        hiddenInput: "full_phone",
          initialCountry: "auto",
          geoIpLookup: function(callback) {
            $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              callback(countryCode);
            });
          },
    // allowDropdown: false,
    // autoHideDialCode: false,
    // autoPlaceholder: "off",
    // dropdownContainer: "body",
    // excludeCountries: ["us"],
    // formatOnDisplay: false,
    // geoIpLookup: function(callback) {
    //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    //     var countryCode = (resp && resp.country) ? resp.country : "";
    //     callback(countryCode);
    //   });
    // },
    // hiddenInput: "full_number",
    // initialCountry: "auto",
    // nationalMode: false,
    // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    // placeholderNumberType: "MOBILE",
    preferredCountries: ['in', 'us'],
    // separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/js/utils.js"
    });
});	
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });

        $(".knob").knob();

    });
</script>
 
</body>
</html>
