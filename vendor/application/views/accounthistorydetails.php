<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); ?>
<?php 
$getCurrency = getVendorCurrency();
if($getCurrency[0]->houdin_users_currency=="USD")
{
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="AUD"){
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="Euro"){
	$currencysymbol= "£";
}else if($getCurrency[0]->houdin_users_currency=="Pound"){
	$currencysymbol= "€";
}else if($getCurrency[0]->houdin_users_currency=="INR"){
	$currencysymbol= "₹";
}
?>
<style type="text/css">
.zoom img{
    
    transition-duration: 5s;
    margin: 0 auto;
}
img {
    vertical-align: middle;
    height: 40px;
    width: auto; 
} 

.zoom { 
    transition: all 1s;
    -ms-transition: all 1s;
    -webkit-transition: all 1s;
    margin-top: 0px;
    padding-top: 0px;
    
}
.zoom:hover {
    -ms-transform: scale(2); /* IE 9 */
    -webkit-transform: scale(2); /* Safari 3-8 */
    transform: scale(2); 
    margin-left: 40px;
}
.m-b-10{
    margin-bottom: 10px;
}
.table-bordered > tbody > tr > td {
    vertical-align: text-top !important;
}
.remove-tr
{
    display:none;
}
.select2.select2-container
{
    margin-bottom:4px;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" /> 
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
<!--START Page-Title -->
            <div class="row">
            <?php 
            
            $lastEntry = getMainAccount($this->uri->segment('3'));
            ?>
                <div class="col-sm-12">
                <div class="col-md-8">
                   <h4 class="page-title">Account / <?php echo $accountname; ?></h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>accounts/accounting">Accounting </a></li>
                  <li class="active">Account History</li>
                  </ol>
                  </div>
                <!-- <div class="col-md-4">
                  <select class="form-control navbar-left app-search pull-left custom_search_all setNewEntry">
                  <option value="">Add Entry</option>                  
                  <option value="Transfer">Transfer</option>
                  <option value="journal Entry">journal Entry</option>
                  </select>              
                </div> -->
                  
                </div>
                <?php 
           
                ?>
            </div>
           <!--END Page-Title --> 
           

                                    
        <div class="row">
          <div class="col-md-12">
          <?php 
          if($this->session->flashdata('success'))
          {
              echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
          }
          if($this->session->flashdata('error'))
          {
              echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
          }
          ?>
             <!-- <div class="col-md-6 m-b-10">
             <select class="form-control " name="status_value">
                <option value="">Choose Name</option>
                <option value="1">Bank</option>
                <option value="0">Current assets</option>
                <option value="2">Fixed assets</option></select>
          </div> -->
          <div class="clearfix"></div>
            <div class="card-box table-responsive">
              
              <table id="AccountHistorytbl"  class="table table-hover table-bordered table_shop_custom">
                <thead>
                  <tr>
                  <th style="display:none">No.</th>
                    <th style="">Date</th>
                    <th>Ref No. type</th>
                    <th>Payee account</th>
                    <th>Memo</th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th><i class="fa fa-check"></i></th> 
                    <th>Tax</th> 
                    <th>Balance</th>
                  </tr>
                </thead>
                <tbody class="appendBodyData">
                
         <?php //print_r($AccountHistory); 
         $totalblan=0;
         foreach($AccountHistory as $keys=>$Accountdata){
            $DebitAccountBal=$Accountdata->houdinv_accounts_balance_sheet_decrease;
            $CreditAccountBal=$Accountdata->houdinv_accounts_balance_sheet__increase;
         ?>

            <tr class="table_shop_custom_tr">
             <td style="display:none" ><?=$keys;?></td>
            <td class="date"><?php echo date("d-m-Y",strtotime($Accountdata->houdinv_accounts_balance_sheet_date)); ?><br/>
                    <a href="javascript:;" data-id="<?php echo $Accountdata->houdinv_accounts_balance_sheet_id ?>" class="btn btn-danger btn-xs deleteAccountingBalance"><i class="fa fa-trash-o"></i></a>
                    </td>

     
                    <td>
                    <?php 
                    if($Accountdata->houdinv_accounts_balance_sheet_order_id)
                    {
                    ?>
                    <a href="<?php echo base_url() ?>order/orderinvoicepdf/<?php echo $Accountdata->houdinv_accounts_balance_sheet_order_id ?>">
                    <?php }
                    else if($Accountdata->houdinv_accounts_balance_sheet_purchase_id)
                    {
                    ?>
                    <a href="<?php echo base_url() ?>purchase/generateinvoice/<?php echo $Accountdata->houdinv_accounts_balance_sheet_purchase_id ?>">
                    <?php }
                    ?>
                    <span class="one"><?php echo $Accountdata->houdinv_accounts_balance_sheet_reference;?></span>
                    <br />
                    <span class="two"><?php echo $Accountdata->houdinv_accounts_balance_sheet_ref_type;?></span></a></td>

             
             <td class="account"><?php if($Accountdata->houdinv_accounts_balance_sheet_account=='0') { echo "--"; } else { echo "--"; }  ?></td>
             <td class="memo"><?php echo $Accountdata->houdinv_accounts_balance_sheet_memo; ?></td>
            
            <?php 
            
            if($Accountdata->houdinv_accounts_account_type_id == 2 || $Accountdata->houdinv_accounts_account_type_id == 4 || $Accountdata->houdinv_accounts_account_type_id == 5
            || $Accountdata->houdinv_accounts_account_type_id == 1 || $Accountdata->houdinv_accounts_account_type_id == 3 || $Accountdata->houdinv_accounts_account_type_id == 13
            || $Accountdata->houdinv_accounts_account_type_id == 14 || $Accountdata->houdinv_accounts_account_type_id == 15)
            { 
                
                if($CreditAccountBal==0){

                    $totalblan=$totalblan+$DebitAccountBal;
                    }else{
                      $totalblan=$totalblan-$CreditAccountBal;
                    } 
                
                ?>
                
                <td class="Debit"><?php echo $DebitAccountBal; ?></td>
            <td class="Credit"><?php echo $CreditAccountBal; ?></td>

           <?php  }else{
                // Liabilities type Account
            ?>
<td class="Debit"><?php echo $DebitAccountBal; ?></td>
            <td class="Credit"><?php echo $CreditAccountBal; ?></td>
           
            <?php
        
        if($CreditAccountBal==0){

            $totalblan=$totalblan-$DebitAccountBal;
            }else{
              $totalblan=$totalblan+$CreditAccountBal;
            }
        
        } ?>
            <td class="status"><?php echo $Accountdata->houdinv_accounts_balance_sheet_concile;?></td>
                    <td><?php echo $Accountdata->houdinv_accounts_balance_sheet_tax;?></td>

            
                    <td>
                    <input type="hidden" class="edit_fetch" value="<?php echo $Accountdata->houdinv_accounts_balance_sheet_id;?>"/>
                       <input type="hidden" class="edit_fetch_Account_id" value="<?php echo $Accountdata->houdinv_accounts_balance_sheet_account_id;?>"/>
                      
                      <?php 
                      
                    
                      
                      ?>
                       <?php echo $totalblan; ?></td>

            
            </tr>

         <?php } ?>
                </tbody>
              </table>
              
              <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
              <div class="col-sm-12"><div class="pull-right"><h3 class="text-center">Ending Balance<br><?php echo $currencysymbol ?>&nbsp;<?php echo $totalblan; ?></h5></div></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--Delete-->

  <div id="delete_product" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
      <?php echo form_open(base_url().'Product/',array("id"=>"DeleteProduct","enctype"=>"multipart/form-data")); ?>
 
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete product</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this product ?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
               <input type="hidden" name="delete_id" id="delete_id" />
          <input type="submit" class="btn btn-info" name="DeleteEntry" value="Delete">

          </div>

          </div>
       <?php echo form_close(); ?> 
          </div>

          </div>
<!--change Status-->
<div id="deleteBalanceSheetModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <?php echo form_open(base_url( 'Accounts/deletebalancesheet/'.$this->uri->segment('3').'' ), array('method'=>'post'));?>
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Delete Purchase</h4>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-md-12">
    <input type="hidden" class="deletebalancesheetId" name="deletebalancesheetId"/>
    <h4><b>Do you really want to Delete this purchase ?</b></h4>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-info" name="deletePurchase" value="Delete">
    </div>
    </div>
    <?php echo form_close(); ?>
    </div>
    </div>



<?php $this->load->view('Template/footer.php') ?>
<script>
  $(document).ready(function() {
    $('#AccountHistorytbl').DataTable( {
        "order": [[ 0, "desc" ]],
        "pageLength": 50
    } );
} );
  
  </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<script>
$(document).on("click",".status_tab",function()
{
    
  $id_status = $(this).attr("data-id");  
  $("#status_id").val($id_status);
    
});

$(document).on("click",".delete_tab",function()
{
    
  $id_delete = $(this).attr("data-id"); 
   $("#delete_id").val($id_delete);  
    
});
document.querySelector('input[list]').addEventListener('input', function(e) {
    var input = e.target,
        list = input.getAttribute('list'),
        options = document.querySelectorAll('#' + list + ' option'),
        hiddenInput = document.getElementById(input.id + '-hidden'),
        inputValue = input.value;

    hiddenInput.value = inputValue;

    for(var i = 0; i < options.length; i++) {
        var option = options[i];

        if(option.innerText === inputValue) {
            hiddenInput.value = option.getAttribute('data-value');
            break;
        }
    }
});

$(document).on("click",'.table_shop_custom_tr',function()
{
    $(this).parents('.table-responsive').find('tr').removeClass("remove-tr");
    $(this).addClass("remove-tr");
    $('.form_row').show();
  var date =   $(this).find('.date').text();
 $('.date1 input').val(date);
 
 $('.one1 input').val($(this).find('.one').text());
 $('.two1 input').val($(this).find('.two').text());
 $('.account1 input').val($(this).find('.account').text());
 $('.memo1 input').val($(this).find('.memo').text());
 $('.decrease1 input').val($(this).find('.decrease').text());
 $('.increase1 input').val($(this).find('.increase').text());
 
 $('.status1 input').val($(this).find('.status').text());
 $('.edit_id').val($(this).find('.edit_fetch').val());
 
  $('.edit_id_account').val($(this).find('.edit_fetch_Account_id').val());

});

$(document).on('click','.save_datsa',function()
{
   
           var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
    

      jQuery.ajax({
          type: "Post",
          url: "<?php echo base_url(); ?>Accounts/accuntingUpdate",
          data:{'csrf_test_name':csrfHash,"id":"hello","date":$('.date1 input').val(),
                "ref_name":$('.one1 input').val(),"ref_type":$('.two1 input').val(),
                "pay_account":$('.account1 input').val(),"memo":$('.memo1 input').val(),
                "decrease":$('.decrease1 input').val(),
                "increase":$('.increase1 input').val(),"status":$('.status1 input').val(),
                "id":$('.edit_id').val(),"account_id":$('.edit_id_account').val()},
        
          success: function(data) {
               window.location.reload();
            }
            });
    
});



$(document).on('click','.delete_datsa',function()
{
   
           var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
    

      jQuery.ajax({
          type: "Post",
          url: "<?php echo base_url(); ?>Accounts/accuntingDElete",
          data:{'csrf_test_name':csrfHash,
                "id":$('.edit_id').val(),"account_id":$('.edit_id_account').val()},
        
          success: function(data) {
               window.location.reload();
            }
            });
    
});

$(document).on("click",'.cancle_datsa',function()
{
    $(this).parents('.table-responsive').find('tr').removeClass("remove-tr");
    
    $('.form_row').hide();


});

</script>
<script type="text/javascript">
$(document).ready(function(){
    );
    $(document).on('click','.cancelAppendData',function(){
        $('.appendRowData').remove();
    })
    $(".select1").select2({
    // minimumInputLength: 2
});
$(document).on('blur','.setIncreaseData',function(){
    if($(this).val() != "")
    {
        $('.setDecrease').val('');
    }
});
$(document).on('blur','.setDecrease',function(){
    if($(this).val() != "")
    {
        $('.setIncreaseData').val('');
    }
});
$(document).on('change','.setPayeeData',function()
{
    $('.payeetype').val($(this).children('option:selected').attr('data-user'));
})
 $(document).on('click','.saveNewData',function(){
    var setBaseData = '<?php echo base_url() ?>';
    var setDatavalue = $(this).parents('.appendRowData');
    var getAccountId = '<?php echo $this->uri->segment('3'); ?>';
    var getdate = setDatavalue.find('.getDate').val();
    var getRefNumber = setDatavalue.find('.getRefNumber').val();
    var entrytype = setDatavalue.find('.setValueForEntryType').val();
    var setPayeeData = setDatavalue.find('.setPayeeData').val();
    var selectAccount = setDatavalue.find('.selectAccount').val();
    var getMemoData = setDatavalue.find('.getMemoData').val();
    var setDecrease = setDatavalue.find('.setDecrease').val();
    var setIncreaseData = setDatavalue.find('.setIncreaseData').val();
    var concileStatus = setDatavalue.find('.concileStatus').val();
    var payeetype = setDatavalue.find('.payeetype').val();
    if(entrytype == 'Deposit')
    {
        if(getdate != "" && selectAccount != "" && setIncreaseData != "")
        {
            addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,setDecrease,setIncreaseData,concileStatus,payeetype);
        }
        else
        {
            if(getdate == "") { setDatavalue.find('.getDate').attr('style','border:1px solid red !important'); }
            if(selectAccount == "") { setDatavalue.find('.selectAccount').next('.select2-container').attr('style','border:1px solid red !important'); }
            if(setIncreaseData == "") { setDatavalue.find('.setIncreaseData').attr('style','border:1px solid red !important'); }
            return false;
        }   

    }
    else if(entrytype == 'Receive Payment')
    {
        if(getdate != "" && setIncreaseData != "")
        {
            addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,setDecrease,setIncreaseData,concileStatus,payeetype);
        }
        else
        {
            if(getdate == "") { setDatavalue.find('.getDate').attr('style','border:1px solid red !important'); }
            if(setPayeeData == "") { setDatavalue.find('.setPayeeData').next('.select2-container').attr('style','border:1px solid red !important'); }
            if(setIncreaseData == "") { setDatavalue.find('.setIncreaseData').attr('style','border:1px solid red !important'); }
            return false;
        }
    }
    else if(entrytype == 'Refund')
    {
        if(getdate != "" && setDecrease != "")
        {
            addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,setDecrease,setIncreaseData,concileStatus,payeetype);
        }
        else
        {
            if(getdate == "") { setDatavalue.find('.getDate').attr('style','border:1px solid red !important'); }
            if(setDecrease == "") { setDatavalue.find('.setDecrease').attr('style','border:1px solid red !important'); }
            return false;
        }
    }
    else if(entrytype == 'Transfer')
    {
        if(getdate != "" && selectAccount != "")
        {
            addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,setDecrease,setIncreaseData,concileStatus,payeetype);
        }
        else
        {
            if(getdate == "") { setDatavalue.find('.getDate').attr('style','border:1px solid red !important'); }
            if(selectAccount == "") { setDatavalue.find('.selectAccount').next('.select2-container').attr('style','border:1px solid red !important'); }
            return false;
        }
    }
    else if(entrytype == 'journal Entry')
    {
        var setLastData = '<?php echo $lastEntry ?>';
        if(setLastData == 2 || setLastData == 4 || setLastData == 5)
        {
            var assetincrease = setDecrease;
            var assetDecrese = setIncreaseData;
        }
        else
        {
            var assetincrease = setIncreaseData;
            var assetDecrese = setDecrease;
        }
        if(setLastData == 16 || setLastData == 3)
        {
            if(getdate != "")
            {
                addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,assetDecrese,assetincrease,concileStatus,payeetype);
            }
            else
            {
                if(getdate == "") { setDatavalue.find('.getDate').css('border','1px solid red !important'); }
                if(selectAccount == "") { setDatavalue.find('.selectAccount').next('.select2-container').css('border','1px solid red !important'); }
                return false;
            }
        }
        else
        {
            if(getdate != "" && selectAccount != "")
            {
                addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,assetDecrese,assetincrease,concileStatus,payeetype);
            }
            else
            {
                if(getdate == "") { setDatavalue.find('.getDate').css('border','1px solid red !important'); }
                if(selectAccount == "") { setDatavalue.find('.selectAccount').next('.select2-container').css('border','1px solid red !important'); }
                return false;
            }
        }
        
    }
 });
 function addnewEntry(setBaseData,getAccountId,getdate,getRefNumber,entrytype,setPayeeData,selectAccount,getMemoData,setDecrease,setIncreaseData,concileStatus,payeetype)
 {
     $('.saveNewData').prop('disabled',true);
    jQuery.ajax({
        type: "POST",
        url: setBaseData+"Accounts/addNewAccountJournal",
        data: {"getAccountId": getAccountId,"getdate":getdate,"getRefNumber":getRefNumber,"entrytype":entrytype,"setPayeeData":setPayeeData,"selectAccount":selectAccount,"getMemoData":getMemoData,"setDecrease":setDecrease,"setIncreaseData":setIncreaseData,'concileStatus':concileStatus,"payeetype":payeetype
        },
        success: function(data) {
        var getData = $.parseJSON(data)
        if(getData.message == 'yes')
        {
            alert('Entry addedd successfully');
            location.reload();
        }
        else
        {
            alert('Something went wrong. Please try again');
            $('.saveNewData').prop('disabled',false);
        }
        }
        }); 
 }
 $(document).on('click','.deleteAccountingBalance',function(){
     $('.deletebalancesheetId').val($(this).attr('data-id'));
     $("#deleteBalanceSheetModal").modal('show');
 })
})
</script>