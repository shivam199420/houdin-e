<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Setting extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        if($this->session->userdata('role') != 0)
        {
            $this->load->helper('access');
            $getData = access($this->session->userdata('department'));
            if(!in_array('setting',$getData))
            {
                $this->load->helper('url');
                redirect(base_url()."dashboard", 'refresh');
            }
        }
        $this->load->model('Categorymodel');
        $this->load->model('Countrymodel');
        $this->load->model('SettingModel');
		$this->load->helper('url');
		$this->load->library('pagination');
        $this->perPage = 100;
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->Validconfigs = array(
        array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
        ),
        array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'required'
        ),
        array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
              )
            );
            
        $this->countryConfig = array(
        array(
                'field' => 'houdin_country_name',
                'label' => 'Country Name',
                'rules' => 'required'
        ),
        array(
                'field' => 'houdin_country_status',
                'label' => 'Status',
                'rules' => 'required'
        )
            );
	}           
	public function language()
	{
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // add langugae data
        if($this->input->post('addLanguageBtn'))
        {
            $this->form_validation->set_rules('selectMasterLanguage','select option','required');
            $this->form_validation->set_rules('slectMasterLanguageStatus','select option','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setLanguageInsertData = array('language'=>$this->input->post('selectMasterLanguage'),'status'=>$this->input->post('slectMasterLanguageStatus'),'languageName'=>$this->input->post('languageNameData'));
                $getLanguageStatus = $this->SettingModel->insertLanguageData($setLanguageInsertData);
                if($getLanguageStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Language added successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/language", 'refresh');
                }
                else if($getLanguageStatus['message'] == 'exist')
                {
                    $this->session->set_flashdata('success','Langugae is already present');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/language", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/language", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Setting/language", 'refresh');
            }
        }
        // delete language btn
        if($this->input->post('deleteLngaugeBtn'))
        {
            $this->form_validation->set_rules('deleteLangugaeId','text','required');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->SettingModel->deleteLanguageData($this->input->post('deleteLangugaeId'));
                if($getDeleteStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('error','Langugae deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/language", 'refresh');
                }
                else
                { 
                    $this->session->set_flashdata('error','Something went wrong.Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/language", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong.Please try again');
                $this->load->helper('url');
                redirect(base_url()."Setting/language", 'refresh');
            }
        }
        // edit langauge data
        if($this->input->post('editLangaugeBtn'))
        {
            $this->form_validation->set_rules('editLanguageName','select option','required');
            $this->form_validation->set_rules('editLangugaeStatus','select option','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setUpdateArray = array('houdin_language_name'=>$this->input->post('editLanguageName'),'houdin_language_name_value'=>$this->input->post('languageNameData'),'houdin_language_status'=>$this->input->post('editLangugaeStatus'),'editLanguageId'=>$this->input->post('editLanguageId'));
                $getLanguageUpdateStatus = $this->SettingModel->updateLanguageDataValue($setUpdateArray);
                if($getLanguageUpdateStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Langugae updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/language", 'refresh');
                }
                else if($getLanguageUpdateStatus['message'] == 'exist')
                {
                    $this->session->set_flashdata('error','Langugae is already present');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/language", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong.Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/language", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Setting/language", 'refresh');
            }
        }
        // Custom Pagination
        $getTotalCount = $this->SettingModel->countLanguageListdata();
        $config['base_url']    = base_url().'Setting/language/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getLanguageData = $this->SettingModel->getLanguageData($setArray);
		$this->load->view('languageupdate',$getLanguageData);
    }
    public function country()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        if($this->input->post('Add'))
            {
                $this->form_validation->set_rules($this->countryConfig);
                if($this->form_validation->run() != true)
                {
                       $this->session->set_flashdata('message_name',validation_errors() );
                      redirect(base_url().'setting/country', 'refresh');    
                }
                $array = $this->input->post();
                $date = strtotime(date("Y-m-d, h:i:s"));
                $array['houdin_country_created_at']=$date;
                $array['houdin_country_updated_at']=$date;
                $indexSpam = array_search('Submit', $array);
                unset($array[$indexSpam]);
                $data = $this->Countrymodel->CountryAdd($array);
                if($data)
                {
                        $this->session->set_flashdata('success',"Country added successfully");
                        redirect(base_url().'setting/country', 'refresh');                 
                }    
            }
            // edit
      if($this->input->post('Edit'))
        {
            $this->form_validation->set_rules($this->countryConfig);            
            if($this->form_validation->run() != true)
            {
                   $this->session->set_flashdata('message_name',validation_errors() );
                  redirect(base_url().'setting/country?', 'refresh');    
            }
            $array = array("houdin_country_name"=>$this->input->post('houdin_country_name'),
            "houdin_country_status"=>$this->input->post('houdin_country_status'));
            $date = strtotime(date("Y-m-d, h:i:s"));
            $array['houdin_country_updated_at']=$date;
            $data = $this->Countrymodel->CountryEdit($array,$this->input->post('Edit_id'));
            if($data)
            {
                $this->session->set_flashdata('success',"Country updated successfully");
                redirect(base_url().'setting/country', 'refresh');                 
            }
        }   
        if($this->input->post('delete'))
        {
            if($this->input->post('delete_input'))
            {
                $data = $this->Countrymodel->CountryDelete($this->input->post('delete_input'));
                if($data)
                {
                    $this->session->set_flashdata('success',"Country deleted successfully");
                    redirect(base_url().'setting/country', 'refresh');                 
                }  
            }
        }
        // fetch all category data
        $Allcountry= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->Countrymodel->getAllCountry($conditions);
        //pagination config
        $config['base_url']    = base_url().'setting/country/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset; 
        $conditions['limit'] = $this->perPage;
        $Allcountry['country'] = $this->Countrymodel->getAllCountry($conditions);
        $this->load->view('countryupdate',$Allcountry);
    }
    public function template()
    {         
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }  
        // Add template data
        if($this->input->post('addTemplateDataBtn'))
        {
            // upload admin image
            if(!empty($_FILES['name']['name']))
            {
                $newFileName = rand(100000,999999)."".$_FILES['name']['name'];
                $config['upload_path'] = "uploads/template/";
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['max_size'] = '10000';
                $config['file_name'] = $newFileName;
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('name'))
                {
                $uploadData = $this->upload->data();
                $picture = $uploadData['file_name'];
                $this->form_validation->set_rules('addTemplateName','text','required');
                $this->form_validation->set_rules('addTemplateCategory','select option','required');
                $this->form_validation->set_rules('addTemplateDirectoryUrl','text','required|valid_url');
                $this->form_validation->set_rules('addTemplateStatus','text','required|in_list[active,deactive]'); 
                $this->form_validation->set_rules('addTemplateDescription','text','required');
                if($this->form_validation->run() == true)
                {
                    $setInsertArrayData = array('templateName'=>$this->input->post('addTemplateName'),'templateCategory'=>$this->input->post('addTemplateCategory'),'directory'=>$this->input->post('addTemplateDirectoryUrl'),'status'=>$this->input->post('addTemplateStatus'),'templateDescription'=>$this->input->post('addTemplateDescription'),'templateThumbnail'=>$picture);
                    $getInsertStatus = $this->SettingModel->insertTemplateData($setInsertArrayData);
                    if($getInsertStatus['message'] == 'yes')
                    {
                        $this->session->set_flashdata('success','Template addedd successfully');
                        $this->load->helper('url');
                        redirect(base_url()."Setting/template", 'refresh');
                    }
                    else
                    {
                        $this->session->set_flashdata('error','Something went wrong. Please try again');
                        $this->load->helper('url');
                        redirect(base_url()."Setting/template", 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','All fields are mandatory');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/template", 'refresh');
                }
                }
                else
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('error',$error['error']);
                    $this->load->helper('url');
                    redirect(base_url()."Setting/template", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Please uplaod image');
                $this->load->helper('url');
                redirect(base_url()."Setting/template", 'refresh');
            } // End Here.
        }
        // edit template data
        if($this->input->post('updateTemplateData'))
        {
            $this->form_validation->set_rules('editTemplateName','text','required');
            $this->form_validation->set_rules('editTemplateCategory','select option','required');
            $this->form_validation->set_rules('editTemplateDirectoryUrl','text','required|valid_url');
            $this->form_validation->set_rules('editTemplateStatus','text','required|in_list[active,deactive]'); 
            $this->form_validation->set_rules('editTemplateDescription','text','required');
            if($this->form_validation->run() == true)
            {
                $updatedArrayData = array('name'=>$this->input->post('editTemplateName'),'category'=>$this->input->post('editTemplateCategory'),'directory'=>$this->input->post('editTemplateDirectoryUrl'),'status'=>$this->input->post('editTemplateStatus'),'description'=>$this->input->post('editTemplateDescription'),'id'=>$this->input->post('editTemplateId'));
                $getStatus = $this->SettingModel->updateTemplatedata($updatedArrayData);
                if($getStatus['message'] == 'yes')
                {
                    // upload admin image
                    if(!empty($_FILES['name']['name']))
                    {
                        $newFileName = rand(100000,999999)."".$_FILES['name']['name'];
                        $config['upload_path'] = "uploads/template/";
                        $config['allowed_types'] = 'jpg|jpeg|png';
                        $config['max_size'] = '6144';
                        $config['file_name'] = $newFileName;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('name'))
                        {
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                        if($this->form_validation->run() == true)
                        {
                            $imageUpatedArray = array('imageData'=>$picture,'id'=>$getStatus['id']);
                            $getImageUpdateStatus = $this->SettingModel->updateTemplaetImage($imageUpatedArray);
                            if($getImageUpdateStatus['message'] == 'yes')
                            {
                                $this->session->set_flashdata('success','Template updated  successfully');
                                $this->load->helper('url');
                                redirect(base_url()."Setting/template", 'refresh');
                            }
                            else
                            {
                                $this->session->set_flashdata('error','Something went wrong during image upload. Please try again');
                                $this->load->helper('url');
                                redirect(base_url()."Setting/template", 'refresh');
                            }
                        }
                        else
                        {
                            $this->session->set_flashdata('error','All fields are mandatory');
                            $this->load->helper('url');
                            redirect(base_url()."Setting/template", 'refresh');
                        }
                        }
                        else
                        {
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('error',$error);
                            $this->load->helper('url');
                            redirect(base_url()."Setting/template", 'refresh');
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Template updated successfully');
                        $this->load->helper('url');
                        redirect(base_url()."Setting/template", 'refresh');
                    } // End Here.
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/template", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."Setting/template", 'refresh');
            }
        }
        // delete template data
        if($this->input->post('deleteTemplateBtn'))
        {
            $this->form_validation->set_rules('deleteTemplateId','text','required');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->SettingModel->deletetemplateData($this->input->post('deleteTemplateId'));
                if($getDeleteStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Template deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/template", 'refresh');

                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong.Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/template", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong.Please try again');
                $this->load->helper('url');
                redirect(base_url()."Setting/template", 'refresh');
            }
        }
        // Custom Pagination
        $getTotalCount = $this->SettingModel->countTemplateListdata();
        $config['base_url']    = base_url().'Setting/template/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getTemplatedataValue = $this->SettingModel->fetchTemplateData($setArray);
        $this->load->view('templateupdate',$getTemplatedataValue);
    }
   public function businesscategory()
   {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        if($this->input->post('Add'))
        {
            $this->form_validation->set_rules($this->Validconfigs);
            if($this->form_validation->run() != true)
            {
                $this->session->set_flashdata('message_name',validation_errors() );
                redirect(base_url().'setting/businesscategory?', 'refresh');    
            }
            $date = strtotime(date("Y-m-d, h:i:s"));
            $array = array("houdin_business_category_name"=>$this->input->post('name'),"houdin_business_category_desc"=>$this->input->post('description'),
            "houdin_business_categories_status"=>$this->input->post('status'),"houdin_business_category_created_at"=>$date,
            "houdin_business_category_updated_at"=>$date);
            $data = $this->Categorymodel->CategoryAdd($array);
            if($data)
            {
                $this->session->set_flashdata('success',"Category added successfully");
                redirect(base_url().'setting/businesscategory?', 'refresh');                 
            }
        }     
        if($this->input->post('Edit'))
        {
            $this->form_validation->set_rules($this->Validconfigs);
            if($this->form_validation->run() != true)
            {
                $this->session->set_flashdata('message_name',validation_errors() );
                redirect(base_url().'setting/businesscategory?', 'refresh');    
            }
            $date = strtotime(date("Y-m-d, h:i:s"));
            $array = array("houdin_business_category_name"=>$this->input->post('name'),"houdin_business_category_desc"=>$this->input->post('description'),
            "houdin_business_categories_status"=>$this->input->post('status'),
            "houdin_business_category_updated_at"=>$date);
            $data = $this->Categorymodel->CategoryEdit($array,$this->input->post('Edit_id'));
            if($data)
            {
                $this->session->set_flashdata('success',"Category updated successfully");
                redirect(base_url().'setting/businesscategory?', 'refresh');                 
            }
        }   
        if($this->input->post('delete'))
        {
            if($this->input->post('delete_input'))
            {
                $data = $this->Categorymodel->CategoryDelete($this->input->post('delete_input'));
                if($data)
                {
                    $this->session->set_flashdata('success',"Category deleted successfully");
                    redirect(base_url().'setting/businesscategory?', 'refresh');                 
                }  
            }
        }
        // fetch all category data
        $Allcategory= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->Categorymodel->getAllCategory($conditions);
        //pagination config
        $config['base_url']    = base_url().'setting/businesscategory/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        $Allcategory['category'] = $this->Categorymodel->getAllCategory($conditions);
        $this->load->view('businesscategoryupdate',$Allcategory);
    }
    public function logomanagement()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // add admin logo data
        if($this->input->post('addMasterAdminLogoBtn'))
        {
            // upload admin image
            if(!empty($_FILES['name']['name']))
            {
                $newFileName = rand(100000,999999)."".$_FILES['name']['name'];
                $config['upload_path'] = "uploads/logo/";
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['max_size'] = '6144';
                $config['file_name'] = $newFileName;
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('name'))
                {
                $uploadData = $this->upload->data();
                $picture = $uploadData['file_name'];
                $setInsertArray = array('imageData'=>$picture);
                $getImageStatus = $this->SettingModel->addAdminImageData($setInsertArray);
                if($getImageStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Image added successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/logomanagement", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/logomanagement", 'refresh');
                }
                }
                else
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('error',$error);
                    $this->load->helper('url');
                    redirect(base_url()."Setting/logomanagement", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Please uplaod image');
                $this->load->helper('url');
                redirect(base_url()."Setting/logomanagement", 'refresh');
            } // End Here.
        }
        // Update admin logo data
        if($this->input->post('updateAdminLogoBtn'))
        {
            // upload admin image
            if(!empty($_FILES['name']['name']))
            {
                $newFileName = rand(100000,999999)."".$_FILES['name']['name'];
                $config['upload_path'] = "uploads/logo/";
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['max_size'] = '6144';
                $config['file_name'] = $newFileName;
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('name'))
                {
                $uploadData = $this->upload->data();
                $picture = $uploadData['file_name'];
                $setInsertArray = array('imageData'=>$picture);
                $getImageStatus = $this->SettingModel->updateAdminImageData($setInsertArray);
                if($getImageStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Image updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/logomanagement", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/logomanagement", 'refresh');
                }
                }
                else
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('error',$error);
                    $this->load->helper('url');
                    redirect(base_url()."Setting/logomanagement", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Please uplaod image');
                $this->load->helper('url');
                redirect(base_url()."Setting/logomanagement", 'refresh');
            } // End Here.
        }
        $getLogodata = $this->SettingModel->fetchMasterLogo();
        $this->load->view('logomanagement',$getLogodata);
    }
    public function smscredentials()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // add or update SMS Credentials
        if($this->input->post('addSMSCredentials'))
        {
            $this->form_validation->set_rules('twilioSid','Twilio SID','required');
            $this->form_validation->set_rules('twilioNumber','Twilio Number','required');
            $this->form_validation->set_rules('twilioToken','Twilio Token','required');
            if($this->form_validation->run() == true)
            {
                $setTwilioInsertArray = array('houdin_twilio_sid'=>$this->input->post('twilioSid'),'houdin_twilio_token'=>$this->input->post('twilioToken'),'houdin_twilio_number'=>$this->input->post('twilioNumber'),'id'=>$this->input->post('smsCredentialsId'));
                $getStatus = $this->SettingModel->setTwilioCredentials($setTwilioInsertArray);
                if($getStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','SMS credentials are added successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/smscredentials", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/smscredentials", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."Setting/smscredentials", 'refresh');
            }
        }
        $getSMSCredentials = $this->SettingModel->fetchTwilio();
        $this->load->view('smscredentials',$getSMSCredentials);
    }
    public function emailcredentials()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        if($this->input->post('updateEmailCredentials'))
        {
            $this->form_validation->set_rules('username','user name','required');
            $this->form_validation->set_rules('password','password','required');
            if($this->form_validation->run() == true)
            {
                $setArray = array('houdin_sendgrid_username'=>$this->input->post('username'),'houdin_sendgrid_password'=>$this->input->post('password'));
                $getUpdateStatus = $this->SettingModel->updateEmailCredentials($setArray,$this->input->post('emailID'));
                if($getUpdateStatus)
                {
                    $this->session->set_flashdata('success','Email credentials are added successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/emailcredentials", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/emailcredentials", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."Setting/emailcredentials", 'refresh');
            }
        }
        $getEmailCredentials = $this->SettingModel->getEmailCredentials();
        $this->load->view('emailcredentials',$getEmailCredentials);
    }
    public function payumoney()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        if($this->input->post('updatepayumoneyCredentials'))
        {
            $this->form_validation->set_rules('merchantKey','merchant key','required');
            $this->form_validation->set_rules('merchantSalt','merchant salt','required');
            if($this->form_validation->run() == true)
            {
                $setArray = array('houdin_payumoney_key'=>$this->input->post('merchantKey'),'houdin_payumoney_salt'=>$this->input->post('merchantSalt'));
                $getUpdateStatus = $this->SettingModel->updatePayumoney($setArray,$this->input->post('payumoneyId'));
                if($getUpdateStatus)
                {
                    $this->session->set_flashdata('success','Payumoney credentials are added successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/payumoney", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Setting/payumoney", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."Setting/payumoney", 'refresh');
            }
        }
        $getPayumoney = $this->SettingModel->fetchPayumoney();
        $this->load->view('payumoney',$getPayumoney);
    }
}
