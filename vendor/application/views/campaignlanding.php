<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>

<div class="content-page">
    <!-- Start content --> 
    <div class="content">
        <div class="container">

            
            <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">Campaign</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                 
                  <li class="active">Campaign</li>
                  </ol>
                  </div>
                  
            </div>
           <!--END Page-Title -->
         <div class="row m-t-30">
                <div class="col-md-12">
                      <!--team-1-->
                <div class="col-lg-3">
                <div class="our-team-main">
                
                <div class="team-front">
                 <img src="<?php echo base_url(); ?>assets/images/landingpageicons/SMS_Campaign.png">
                <h3>Sms Campaign</h3>
                <p>Campaign</p>
                </div>
                
               <a href="<?php echo base_url();?>Campaign/sms"> <div class="team-back">
                <span>
                <h2 class="font24">Sms Campaign</h2>
                </span>
                </div></a>
                
                </div>
                </div>
                <!--team-1-->
                
                <!--team-2-->
               <div class="col-lg-3">
                <div class="our-team-main">
                
                <div class="team-front">
                 <img src="<?php echo base_url(); ?>assets/images/landingpageicons/Email_Campaign.png">
                <h3>Email Campaign</h3>
                <p>Campaign</p>
                </div>
                
                <a href="<?php echo base_url();?>campaign/email"><div class="team-back">
                <span>
                <h2 class="font24">Email Campaign</h2>
                </span>
                </div></a>
                
                </div>
                </div>
                <!--team-2-->
                
                <!--team-3-->
               <div class="col-lg-3">
                <div class="our-team-main">
                
                <div class="team-front">
                <img src="<?php echo base_url(); ?>assets/images/landingpageicons/Push_Campaign.png">
                <h3>Push Campaign</h3>
                <p>Campaign</p> 
                </div>
                
                <a href="<?php echo base_url();?>campaign/push"><div class="team-back">
                <span>
                <h2 class="font24">Push Campaign</h2>
                </span>
                </div></a>
                
                </div>
                </div>
                <!--team-3-->


                </div>
            </div><!-- Row -->


      </div>
    </div>
  </div>
 

<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
