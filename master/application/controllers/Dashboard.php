<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        $this->load->model('Dashboardmodel');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->perPage = 100;
	} 
    public function index()
    {
        $getDashboardData = $this->Dashboardmodel->fetchDahsboardData();
        $this->load->view('dashboard',$getDashboardData);
    }
}
