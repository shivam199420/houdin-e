<?php  $this->load->view('Template/header');?> 

<header class="header bg-gray header-page-top">
  <div class="header-page-top-inner">
	  <div class="container"> 
		<h1 class="display-4">FAQ</h1> 
  </div>
	</div>
</header>



 <section class="section  py-8">
        <div class="container">
             <header class="section-header">
                <h3 style="margin: 0px;">Genral</h3> 
              </header>
          <div class="row"> 

            <div class="col-12 col-md-12"> 
            <div class="accordion mb-15" id="accordion-general">
				
            <div class="card"> 
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-general" href="#collapse-general-1" class="" aria-expanded="true">What is Houdin-e?</a>
              </h5>

              <div id="collapse-general-1" class="in collapse show" aria-expanded="false" style="border: 1px solid rgba(181, 185, 191, 0.31);">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-general" href="#collapse-general-2" class="collapsed" aria-expanded="false">Where can I learn more about how to use Houdin-e?</a>
              </h5>

              <div id="collapse-general-2" class="collapse" aria-expanded="false" style="border: 1px solid rgba(181, 185, 191, 0.31);">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-general" href="#collapse-general-3" class="collapsed" aria-expanded="false">Where can I get more details about your security practices?</a>
              </h5>

              <div id="collapse-general-3" class="collapse" aria-expanded="false" style="border: 1px solid rgba(181, 185, 191, 0.31);">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-general" href="#collapse-general-4" class="collapsed" aria-expanded="false">Where can I get more details about your security practices?</a>
              </h5>

              <div id="collapse-general-4" class="collapse" aria-expanded="false" style="border: 1px solid rgba(181, 185, 191, 0.31);">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-general" href="#collapse-general-5" class="collapsed" aria-expanded="false">What is Houdin-e approach to data privacy?</a>
              </h5> 
              <div id="collapse-general-5" class="collapse" aria-expanded="false" style="border: 1px solid rgba(181, 185, 191, 0.31);">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>
				
          </div>

				
          </div>
          </div>
        </div>
      </section>
      
      
       <section class="section  py-8 bg-gray">
        <div class="container">
             <header class="section-header">
                <h3 style="margin: 0px;">License</h3> 
              </header>
          <div class="row"> 

            <div class="col-12 col-md-12"> 
             <div class="accordion" id="accordion-license">
            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-license" href="#collapse-license-1">What is reqular license?</a>
              </h5>

              <div id="collapse-license-1" class="collapse in">
                <div class="card-body">
                  Regular license can be used for end products that do not charge users for access or service(access is free and there will be no monthly subscription fee). Single regular license can be used for single end product and end product can be used by you or your client. If you want to sell end product to multiple clients then you will need to purchase separate license for each client. The same rule applies if you want to use the same end product on multiple domains(unique setup). 
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-license" href="#collapse-license-2">What is extended license?</a>
              </h5>

              <div id="collapse-license-2" class="collapse">
                <div class="card-body">
                  Extended license can be used for end products(web service or SAAS) that charges users for access or service(e.g: monthly subscription fee). Single extended license can be used for single end product and end product can be used by you or your client. If you want to sell end product to multiple clients then you will need to purchase separate extended license for each client. The same rule applies if you want to use the same end product on multiple domains(unique setup). 
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-license" href="#collapse-license-3">Where can I find your Terms of Service (TOS)?</a>
              </h5>

              <div id="collapse-license-3" class="collapse">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>
          </div>


          </div>
        </div>
			</div>
      </section>

      
      <section class="section py-8">
        <div class="container">
          <header class="section-header"> 
            <h3 style="margin: 0px;">Payment</h3> 
          </header>


          <div class="accordion" id="accordion-payment">
            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-payment" href="#collapse-payment-1" class="collapsed" aria-expanded="false">Can I use Houdin-e for free?</a>
              </h5>

              <div id="collapse-payment-1" class="in collapse" aria-expanded="false" style="">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-payment" href="#collapse-payment-2" class="collapsed" aria-expanded="false">What payment services do you support?</a>
              </h5>

              <div id="collapse-payment-2" class="collapse" aria-expanded="false">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-payment" href="#collapse-payment-3" class="collapsed" aria-expanded="false">Can I cancel my subscription?</a>
              </h5>

              <div id="collapse-payment-3" class="collapse" aria-expanded="false">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-payment" href="#collapse-payment-4" class="collapsed" aria-expanded="false">Is this a secure site for purchases?</a>
              </h5>

              <div id="collapse-payment-4" class="collapse" aria-expanded="false">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-payment" href="#collapse-payment-5" class="collapsed" aria-expanded="false">Can I update my card details?</a>
              </h5>

              <div id="collapse-payment-5" class="collapse" aria-expanded="false">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-payment" href="#collapse-payment-6" class="collapsed" aria-expanded="false">What if I want to change plans?</a>
              </h5>

              <div id="collapse-payment-6" class="collapse" aria-expanded="false">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>


            <div class="card">
              <h5 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion-payment" href="#collapse-payment-7" class="collapsed" aria-expanded="false">How long are your contracts?</a>
              </h5>

              <div id="collapse-payment-7" class="collapse" aria-expanded="false">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                </div>
              </div>
            </div>
          </div> 

        </div>
      </section>
  
 



<?php  $this->load->view('Template/footer');?>