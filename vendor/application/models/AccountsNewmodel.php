<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AccountsNewmodel extends CI_Model{
  function __construct() 
  {
    parent::__construct();
    $this->load->helper('database');
    $getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
  }


  public function getAccountHistory($id)
 {
    //   
  return  $getData = $this->db->select("houdinv_accounts_balance_sheet.*,houdinv_accounts.houdinv_accounts_final_balance as balance,houdinv_accounts.houdinv_accounts_account_type_id")->from("houdinv_accounts_balance_sheet")
   ->join("houdinv_accounts","houdinv_accounts.houdinv_accounts_id=houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account_id",'left outer')
   ->where("houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account_id",$id)/*->or_where('houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account',$id)*/
   ->order_by("houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_id","Asc")
  ->get()->result(); 
   

 }

 public function balancetransfers($fromdata,$todata=false){


    $getData = $this->db->insert('houdinv_accounts_balance_sheet',$fromdata);
    $getLastId = $this->db->insert_id();
  $fromdataid=$fromdata['houdinv_accounts_balance_sheet_account_id'];  
 $getaccountdata=$this->getAccountHistory($fromdataid);
 $fromFinelBls= $this->Updatefinelblance($getaccountdata);
 $this->db->where('houdinv_accounts_id',$fromdataid)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$fromFinelBls));

    if($todata){
    $getData = $this->db->insert('houdinv_accounts_balance_sheet',$todata);
    $getLastId = $this->db->insert_id();
    $Todataid=$todata['houdinv_accounts_balance_sheet_account_id']; 
    $getaccountdatato=$this->getAccountHistory($Todataid);
    $ToFinelBls=$this->Updatefinelblance($getaccountdatato);
    $this->db->where('houdinv_accounts_id',$Todataid)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$ToFinelBls));


}

    return true;
 }


private function Updatefinelblance($getaccountdata){

    $totalblan=0;
    foreach($getaccountdata as $keys=>$Accountdata){
       $DebitAccountBal=$Accountdata->houdinv_accounts_balance_sheet_decrease;
       $CreditAccountBal=$Accountdata->houdinv_accounts_balance_sheet__increase;
   
   
       if($Accountdata->houdinv_accounts_account_type_id == 2 || $Accountdata->houdinv_accounts_account_type_id == 4 || $Accountdata->houdinv_accounts_account_type_id == 5
               || $Accountdata->houdinv_accounts_account_type_id == 1 || $Accountdata->houdinv_accounts_account_type_id == 3 || $Accountdata->houdinv_accounts_account_type_id == 13
               || $Accountdata->houdinv_accounts_account_type_id == 14 || $Accountdata->houdinv_accounts_account_type_id == 15)
               { 
                   
                   if($CreditAccountBal==0){
   
                       $totalblan=$totalblan+$DebitAccountBal;
                       }else{
                         $totalblan=$totalblan-$CreditAccountBal;
                       }
   
                   }else{
                       if($CreditAccountBal==0){
   
                           $totalblan=$totalblan-$DebitAccountBal;
                           }else{
                             $totalblan=$totalblan+$CreditAccountBal;
                           }
                   }
   
    }
    return  $totalblan;

}


}