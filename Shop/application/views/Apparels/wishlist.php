 <?php
 $getShopName = getfolderName($this->session->userdata('shopName'));
 $this->load->view(''.$getShopName.'/Template/header'); ?>
 <?php
 $getCurrency = getShopCurrency();
 $currencysymbol=$getCurrency[0]->houdin_users_currency;
 ?> 
		<!-- Page item Area -->
		<div id="page_item_area">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 text-left">
						<h3>Wishlist</h3>
					</div>


				</div>
			</div>
		</div>

		<!-- Wishlist Page -->
		<div class="wishlist-page">
			<div class="container">
				<div class="table-responsive">
					<table class="table cart-table cart_prdct_table text-center">
						<thead>
							<tr>
								<th class="cpt_no">#</th>
								<th class="cpt_img">image</th>
								<th class="cpt_pn">product name</th>
								<th class="stock">stock status</th>
								<th class="cpt_p">price</th>
								<th class="add-cart">add to cart</th>
								<th class="cpt_r">remove</th>
							</tr>
						</thead>
						<tbody>
						<?php

						// print_r($wishproducts);

                        $i=1;

                        foreach($wishproducts as $thisItem)
                        {
                          $image = json_Decode($thisItem->houdinv_products_main_images,true);
                          $stock = $thisItem->houdinv_products_total_stocks;
                          $price = json_DEcode($thisItem->houdin_products_price,true);
                          if($stock>0)
                          {
                            $status = "In stock";
                          }
                          else
                          {
                            $status = "Out of stock";
                          }
                        ?>
							<tr class="tr_row">
								<td><span class="cart-number"><?php echo $i; ?></span></td>
								<td><img style="max-width: 40%;" src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $image[0]; ?>" alt="" /></td>
								<td><a href="javascript:;" class="cart-pro-title"><?php echo $thisItem->houdin_products_title; ?></a></td>
								<td><p class="stock in-stock"><?php echo $status; ?></p></td>
								<td><p class="cart-pro-price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $thisItem->houdin_products_final_price; ?></p></td>
								<td><a   data-variant="0" data-cart="<?php echo $thisItem->houdin_products_id; ?>"  class="btn border-btn Add_to_cart_button cp_Wish_remove" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>">add to cart</a></td>
								<td><a class="cp_Wish_remove" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>"><i class="fa fa-trash"></i></a></td>
							</tr>
						<?php
                        $i++;
						}

						foreach($wishvarint as $wishvarints)
                        {
                          $image = $wishvarints->houdin_products_variants_image;
                          $stock = $wishvarints->houdinv_products_variants_total_stocks;
                          $price = $wishvarints->houdin_products_variants_prices;
                          if($stock>0)
                          {
                            $status = "In stock";
                          }
                          else
                          {
                            $status = "Out of stock";
                          }
                        ?>
							<tr class="tr_row">
								<td><span class="cart-number"><?php echo $i; ?></span></td>
								<td><a href="#" class="cp_img"><img style="max-width: 40%;" src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $image; ?>" alt="" /></a></td>
								<td><a href="#" class="cart-pro-title"><?php echo $wishvarints->houdin_products_variants_title; ?></a></td>
								<td><p class="stock in-stock"><?php echo $status; ?></p></td>
								<td><p class="cart-pro-price"><?php echo $wishvarints->houdinv_products_variants_final_price; ?></p></td>
								<td><a  data-variant="<?php echo $wishvarints->houdinv_users_whishlist_item_variant_id; ?>" data-cart="0" class="btn border-btn Add_to_cart_button cp_Wish_remove" data-id="<?php echo $wishvarints->houdinv_users_whishlist_id; ?>">add to cart</a></td>
								<td><a class="cp_Wish_remove" data-id="<?php echo $wishvarints->houdinv_users_whishlist_id; ?>"><i class="fa fa-trash"></i></a></td>
							</tr>
						<?php
                        $i++;
                        } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
				<?php
				$getShopName = getfolderName($this->session->userdata('shopName'));
				$this->load->view(''.$getShopName.'/Template/footer'); ?>
