<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Refund extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopuserextrapages');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $getRefund = $this->Shopuserextrapages->fetchRefundData();
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/refund',$getRefund);
    }
     
}
