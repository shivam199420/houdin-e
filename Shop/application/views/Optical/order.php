<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view("".$getShopName."/Template/header"); ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 
  <style type="text/css">
      ol.progtrckr {
  margin: 0;
  padding: 0;
      padding-bottom: 5%;
  list-style-type none;
}

ol.progtrckr li {
  display: inline-block;
  text-align: center;
  line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
ol.progtrckr[data-progtrckr-steps="5"] li { width: %; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

ol.progtrckr li.progtrckr-done {
  color: black;
  border-bottom: 4px solid yellowgreen;
}
ol.progtrckr li.progtrckr-todo {
  color: silver;
  border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
  content: "\00a0\00a0";
}
ol.progtrckr li:before {
  position: relative;
  bottom: -2.5em;
  float: left;
  left: 50%;
  line-height: 1em;
}
ol.progtrckr li.progtrckr-done:before {
  content: "\2713";
  color: white;
  background-color: yellowgreen;
  height: 2.2em;
  width: 2.2em;
  line-height: 2.2em;
  border: none;
  border-radius: 2.2em;
}
ol.progtrckr li.progtrckr-todo:before {
  content: "\039F";
  color: silver;
  background-color: white;
  font-size: 2.2em;
  bottom: -1.2em;
}
@media(max-width: 767px){

ol.progtrckr li.progtrckr-done {
  color: black;
  border-bottom: 4px solid yellowgreen;
  width: 100%;
}
ol.progtrckr li.progtrckr-todo {
  color: silver;
  border-bottom: 4px solid silver;
  width: 100%;
}
ol.progtrckr li:before {
  position: relative;
  bottom: -2.5em;
  float: left;
  left: 48%;
  line-height: 1em;
}
}
</style>

   <!-- About Banner Start -->

   <section id="about">

       <div class="container">

           <div class="row">

               <div class="about-heading text-center">

                   <h2>My Order</h2>

               </div>

           </div>

       </div>

   </section>

<!-- About Banner End -->


<?php if($message=='No data')
{
    ?>
<div class="login_page_area">
<div class="container">
<div class="row">
  <div class="col-md-2 col-sm-2"></div>
  <div class="col-md-8 col-sm-8 col-xs-12">
    <div class="create_account_area">
      <h2 class="caa_heading text-center">Order</h2>
      <div class="caa_form_area">
        <p class="text-center">You have no order. Please <a href="<?php echo base_url() ?>" style="text-decoration: underline;">check here</a> to order now.
                        </p>
      </div>
    </div>
  </div>
</div>
</div>
</div>

       <?php

}
else
{
  ?>

<!-- Shopping Cart Start -->

<section id="card">

    <div class="container">

        <div class="row table-responsive">

            <div class="col-md-12">

                <div class="cart-info">

                    <table class="table table-striped table-hover table-bordered text-center">

                        <tbody>

                            <tr>

                                <th  class="cpt_no">Order Id</th>

                                <th class="cpt_img">Product Description</th>

                                <th class="cpt_pn">Product Name</th>

                                <th class="cpt_r">View</th>

                                <th class="cpt_r">Action</th>


                            </tr>
                            <?php
                            $i=1;
                            foreach($data as $orderData)
                            {
                                ?>

                            <tr>

                                <td>

                                #<?php echo $orderData['orderId']; ?>

                                </td>

                                <td><div class="col-md-12 pl">
                                        <h3><?php echo $orderData['productName'];?></h3>
                                        <ol class="progtrckr" data-progtrckr-steps="5">
                                      <li class="progtrckr-done">Unbilled</li>
                                                                                  <li class="<?php if($orderData->deliverystatus != 'unbilled') { echo "progtrckr-done"; } else { echo "progtrckr-todo"; } ?>">Billed</li>
                                                                                  <li class="<?php if($orderData->deliverystatus != 'unbilled' && $orderData->deliverystatus != 'billed' && $orderData->deliverystatus == 'assigned') { echo "progtrckr-done"; } else { echo "progtrckr-todo"; } ?>progtrckr-done">Assigned</li>
                                                                                  <li class="<?php if($orderData->deliverystatus == 'Delivered') { echo  "progtrckr-done"; } else { echo "progtrckr-todo"; } ?>progtrckr-todo">Delivered</li>
                                                                                  </ol>

                                    </div>
                                  </td>
                                <td>

                                    <div class="quantity">

<?php echo $orderData['productName'];?>

                                   </div>

                                </td>

                                <td><a href="<?php echo base_url(); ?>Orders/orderdetail/<?php echo $orderData['orderId']; ?>"><button class="btn btn-primary acc_btn" type="submit" id="acc_Create">Details</button></a></td>

                                <td>
                                  <?php
                                  if($orderSetting[0]->houdinv_shop_order_configuration_cancellation == 1)
                                  {
                                  if($orderData['deliverystatus'] != 'Delivered' && $orderData['deliverystatus'] != 'return request' && $orderData['deliverystatus'] != 'cancel' && $orderData['deliverystatus'] != 'return' && $orderData['deliverystatus'] != 'cancel request' && $orderData['deliverystatus'] != 'order pickup' && $orderData['deliverystatus'] != 'unbilled')
                                  {
                                  ?>
                                  <button class="btn btn-danger acc_btn cancelOrderBtn" data-id="<?php echo $orderData['orderId'] ?>" type="button" id="acc_Create">Cancel</button>
                                  <?php } }
                                  else if($orderSetting[0]->houdinv_shop_order_configuration_return != "" && $orderSetting[0]->houdinv_shop_order_configuration_return != 0)
                                  {
                                    if($orderData['deliverystatus'] == 'Delivered')
                                  {
                                    $getDeliveryDate = $orderData['deliverydate'];
                                    $getTodayDate = date('d-m-Y');
                                    if(strtotime($getTodayDate) > strtotime($getDeliveryDate))
                                    {
                                      $datetime1 = new DateTime($getTodayDate);
                                      $datetime2 = new DateTime($getDeliveryDate);
                                      $interval = $datetime1->diff($datetime2);
                                      if($interval > 0 && $interval <= $orderSetting[0]->houdinv_shop_order_configuration_return)
                                      {
                                  ?>
                                  <button class="btn btn-danger acc_btn" type="submit" id="acc_Create">Return</button>
                                  <?php } }
                                    }
                                  }
                                  ?>

                                  </td>

                            </tr>
                            <?php
                                            $i++;
                                         }?>

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</section>

<?php
}?>

<!-- cancel ordwer -->
<div id="cancelOrderModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<form method="post">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" style="margin:0px; padding:0px;" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Cancel Order</h4>
  </div>
  <div class="modal-body">
<input type="hidden" class="cancelOrderId" name="cancelOrderId" />
    <h3>Do you really want to raise cancel order request.</h3>
  </div>
  <div class="modal-footer">
<input type="submit" name="cancelOrder" class="btn btn-danger" value="Cancel"/>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>
</form>
</div>
</div>

<!-- Shopping Cart End -->

<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view("".$getShopName."/Template/footer"); ?>

<script type="text/javascript">
$(document).on('click','.cancelOrderBtn',function(){
  $('.cancelOrderId').val($(this).attr('data-id'));
  $('#cancelOrderModal').modal('show');
})
</script>
