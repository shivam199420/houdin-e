<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view("".$getShopName."/Template/header"); ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 

<style type="text/css">
.content {
margin-bottom: 60px;
margin-top: 30px;
padding: 20px 5px 15px 5px;
}
.page-title {
font-size: 22px;
font-weight: 600;
margin-bottom: 0px;
margin-top: 10px;
color: #244667;
text-transform: uppercase;
}
.breadcrumb {
background-color: transparent;
margin-bottom: 0px;
padding-top: 10px;
padding-left: 0px;
}
.breadcrumb>.active {
color: #777;
}
.m-t-20 {
margin-top: 10px !important;
}
.panel {
border: none;
margin-bottom: 20px;
background: #ebf0f2;
}
.panel .panel-body {
padding: 20px;
}
.multiple-section {
margin-bottom: 20px;
border: 1px solid #eeeeee;
padding: 10px;
}
.multiple-section-h4 {
font-weight: 600;
background-color: #f4f8fb;
padding: 10px;
margin: 0px 0px 10px 0px;
float: left;
width: 100%;
}
.link-a-blue {
color: #6ba5dc;
}
.custom-btn-multiple-section {
background-color: #fff;
border: 1px solid #ececec;
line-height: 28px;
}
.multiple-section {
margin-bottom: 20px;
border: 1px solid #eeeeee;
padding: 10px;
}
.ui-group-buttons .or {
    position: relative;
    float: left;
    /* width: .3em; */
    height: 1.5em;
    z-index: 3;
    font-size: 13px;
}
.ui-group-buttons .or:before{position:absolute;top:50%;left:50%;content:'or';background-color:#5a5a5a;margin-top:-.1em;margin-left:-.9em;width:1.8em;height:1.8em;line-height:1.55;color:#fff;font-style:normal;font-weight:400;text-align:center;border-radius:500px;-webkit-box-shadow:0 0 0 1px rgba(0,0,0,0.1);box-shadow:0 0 0 1px rgba(0,0,0,0.1);-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box}
.ui-group-buttons .or:after{position:absolute;top:0;left:0;content:' ';width:.3em;height:2.84em;background-color:rgba(0,0,0,0);border-top:.6em solid #5a5a5a;border-bottom:.6em solid #5a5a5a}
.ui-group-buttons{display:inline-block;vertical-align:middle}
.ui-group-buttons:after{content:".";display:block;height:0;clear:both;visibility:hidden}
.ui-group-buttons .btn{float:left;border-radius:0}
.ui-group-buttons .btn:first-child{margin-left:0;border-top-left-radius:.25em;border-bottom-left-radius:.25em;padding-right:15px}
.ui-group-buttons .btn:last-child{border-top-right-radius:.25em;border-bottom-right-radius:.25em;padding-left:15px}
.margin-left63{
	    margin-left: 64.2%;
}
@media(max-width: 767px){
	.btn {
    margin-bottom: 3%!important;
    width: 100%!important;
}
.margin-left63{
	    margin-left: 0%!important;
}
.margin-right40{
	    float: left !important;
}

}
</style>
<?php
//print_R($orders);
?>
<div id="page_item_area">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 text-left">
				<h3>Order Detail</h3>
			</div>
		</div>
	</div>
</div>

	<!-- Start content -->
	<div class="content">
		<div class="container">
			<div class="row">

				<div class="col-xs-12 col-md-12 margin-left63">
                	 <?php
            $array_return = array("Delivered");
            if( in_array($orders->houdinv_order_confirmation_status,$array_return))
            {
                ?>
                  <form id="return_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
                     <input type="text" name="returnOrderId" value="<?php echo $this->uri->segment("3"); ?>"/>
                   <input type="text" value="returnOrder" name="returnOrder"/>
                    </form>
					<button class="btn btn-default return_button">Return Order</button>
				 <?php
                 }

            $array_cancel = array("unbilled","Not Delivered","billed","assigned");
            if( in_array($orders->houdinv_order_confirmation_status,$array_cancel))
            {
                ?>

                    <form id="cancel_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
                     <input type="text" name="cancelOrderId" value="<?php echo $this->uri->segment("3"); ?>"/>
                   <input type="text" value="cancelOrder" name="cancelOrder"/>
                    </form>
					<button class="btn btn-default cancel_button">Cancel Order</button>
			   <?php
            } ?>
            	</div>

			</div>
			<div class="row">
				<div class="col-sm-12">
				</div>
			</div>
			<div class="row m-t-20">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="custom-margin-order">
								<div class="pull-left">
									<b>Order ID : <span class="text_lightblue">#<?php echo $orders->houdinv_order_id ;?></span></b><br>
									<b>Order Status : <span class="text_lightblue text-uppercase"><?php echo $orders->houdinv_order_confirmation_status ;?></span></b>
								</div>
								<div class="pull-right margin-right40">
									<b>Date : <span class="text_lightblue"><?php echo date("Y-m-d ,h:i:s",$orders->houdinv_order_created_at) ;?></span></b>
								</div>
							</div>
							<hr>
								<div class="">
									<div class="multiple-section-h4">
										<div class="pull-left">
											<b><i class="fa fa-minus-circle"></i> Confirmed Products</b>
										</div>
									</div>
                  <div class="table-responsive">
									<table class="table m-t-30 table-responsive">
										<thead>
											<tr>
												<th>Product Name</th>
												<th>Quantity</th>
												<th>Rate</th>
												<th>Tax</th>
												<th>Total Price</th>
											</tr>
										</thead>
                                        <tbody>
                                        <?php
                                        $total_product_gross = 0;
                                        foreach($orders->product_all as $detail)
                                        { ?>
                                        <tr>
                                        <td> <?php echo $detail->title ; ?></td>
                                        <td><?php echo $detail->allDetail->product_Count ; ?></td>
                                        <td><?php echo $detail->allDetail->product_actual_price; ?></td>
                                        <td>0</td>
                                        <td><?php echo $detail->allDetail->total_product_paid_Amount; ?></td>
                                       </tr>
                                        <?php
                                        $total_product_gross = $total_product_gross + $detail->allDetail->total_product_paid_Amount;
                                        } ?>
                                        </tbody>
									</table>
                  </div>
									<hr style="margin-top: 0px!important">
								</div>

								<div class="row" style="border-radius: 0px;">
									<div class="col-md-9"></div>
									<div class="col-md-3">
										<p class=""><span class="custom-width-set">Gross Amount :</span> <span class="text-right"> <?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $total_product_gross;?></span></p>
										<p class=""><span class="custom-width-set">Additional discount :</span><span class="text-right"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo  $orders->houdinv_orders_discount; ?></span></p>

										<p class=""><span class="custom-width-set">Delivery charges :</span><span class="text-right"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $orders->houdinv_delivery_charge; ?></span></p>
										<p class=""><span class="custom-width-set">Round off :</span> <span class="text-right"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $orders->houdinv_orders_total_Amount; ?> </span></p>
										<p class=""><span class="custom-width-set"><b>Net Amount :</b></span><b> <span class="text-right"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $orders->houdinv_orders_total_Amount; ?></span></b></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div> <!-- container -->
				 <!-- content -->
			</div>
			<?php
			$getShopName = getfolderName($this->session->userdata('shopName'));
			$this->load->view("".$getShopName."/Template/footer"); ?>

            <script>
            $(document).on("click",".cancel_button",function()
            {
               $("#cancel_form").submit();

            });

              $(document).on("click",".return_button",function()
            {
               $("#return_form").submit();

            });

            </script>
