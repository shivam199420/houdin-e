<?php
class Forgetmodel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }  
    public function sendForgetTokenData($data)
    {
                // check user email and set forget data
        $this->db->select('houdin_user_email')->from('houdin_users')->where('houdin_user_email',$data);
        $getEmailData = $this->db->get()->result();
        if(count($getEmailData) > 0)
        {
            // insert in to forget table
            $getDateTime = strtotime(date('Y-m-d h:i:s'));
            $getTokenData = rand(99999,10000);
            $insertArray = array('houdin_user_reset_token'=>$getTokenData);
            $getinsertStatus = $this->db->update('houdin_users',$insertArray);
            if($getinsertStatus)
            {
                return array('message'=>'success','emaildata'=>$getEmailData[0]->houdin_user_email,'tokendata'=>$getTokenData);
            }
            else
            {
                return array('message'=>'Something went wrong. Please try again');
            }
        }
        else
        {
            return array('message'=>'no');
        }
   }
   
   
   
       // update admin password data
    public function updateUserPassword($data)
    {
        $this->db->select('houdin_user_email')->from('houdin_users')->where('houdin_user_reset_token',$data['pin']);
        $getForgetData = $this->db->get()->result();
        if(count($getForgetData) > 0)
        {
            // call helper function
            $passworddata = $data['password'];
            
            
              $letters1='abcdefghijklmnopqrstuvwxyz'; 
            $string1=''; 
            for($x=0; $x<3; ++$x)
            {  
                $string1.=$letters1[rand(0,25)].rand(0,9); 
            }
            $saltdata = password_hash($string1,PASSWORD_DEFAULT);
            $pass = crypt($passworddata,$saltdata);
            
            $getDateData = strtotime(date('Y-m-d h:i:s'));
            $updateArrayData = array('houdin_user_password'=>$pass,'houdin_user_password_salt'=>$saltdata,'houdin_user_reset_token'=>'','houdin_user_updated_at'=>$getDateData);
            $this->db->where('houdin_user_email',$getForgetData[0]->houdin_user_email);
            $getUpdateData = $this->db->update('houdin_users',$updateArrayData);

            if($getUpdateData == 1)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }
        }
        else
        {
            return array('message'=>'pin');
        }
    }
        
}