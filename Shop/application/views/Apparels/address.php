        <?php
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/Template/header'); ?>

        <!-- Page item Area -->
        <div id="page_item_area">
        <div class="container">
        <div class="row">
        <div class="col-sm-6 text-left">
        <h3>Address</h3>
        </div>
        </div>
        </div>
        </div>

        <div class="row">
        <div class="container">
        <?php
        if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger' style='width:100%;'>".$this->session->flashdata('error')."</div>";
        }
        else if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success' style='width:100%;'>".$this->session->flashdata('success')."</div>";
        }
        ?>
        <div class="col-sm-12">
        <?php
        if(count($userAddress) > 0)
        {
        ?>
        <a href="<?php echo base_url() ?>Address/add" class="btn btn-default acc_btn text-center pull-right">Add New Address</a>
        <?php } ?>
        </div>
        </div>
        </div>
        <!-- Login Page -->
        <div class="login_page_area">
        <div class="container">
            <div class="row">
            <?php
            foreach($userAddress as $userAddressList)
            {
             ?>
             <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="create_account_area">
                <div class="row">
                   <div class="col-md-8">
                <h4 class="caa_heading" style="font-size:15px"><?php echo $userAddressList->houdinv_user_address_name."(".$userAddressList->houdinv_user_address_phone.")" ?>
                </h4>
                </div>
                <div class="col-md-4">
                  <span style="float: right;font-size: 10px;">
                    <a href="<?php echo base_url() ?>Address/edit/<?php echo $userAddressList->houdinv_user_address_id ?>" style="font-size:10px" class="btn btn-xs btn-success">
                      <i class="fa fa-pencil"></i>
                    </a>&nbsp;&nbsp;
                    <a href="<?php echo base_url() ?>Address/deleteAddress/<?php echo $userAddressList->houdinv_user_address_id ?>" class="btn btn-xs btn-danger" style="font-size:10px">
                      <i class="fa fa-trash-o"></i>
                    </a>
                  </span>
                </div>
                </div>
                <div class="caa_form_area">
                <address><?php echo $userAddressList->houdinv_user_address_user_address ?>,<br>
                <?php echo $userAddressList->houdinv_user_address_city ?>,<br>
                <?php echo $userAddressList->houdinv_user_address_zip ?>
                </address>
                </div>
                </div>
                </div>
            <?php }
            ?>

            </div>
        <div class="row">

        <?php
        if(!count($userAddress) > 0)
        {
        ?>
         <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="create_account_area">
        <h2 class="caa_heading">Address</h2>
        <div class="caa_form_area">
        <p>Add new address here.</p>
        <div class="caa_form_group">
        <div class="col-md-12 text-center">
        <a href="<?php echo base_url() ?>Address/add" class="btn btn-default acc_btn text-center">Add New Address</a>
        </div>
        </div>
        </div>
        </div>
        </div>
        <?php }
        ?>

        </div>
        </div>
        </div>
        <?php
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/Template/footer'); ?>
