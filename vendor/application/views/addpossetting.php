<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/settingsidebar.php') ?>
 
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">POS Setting</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li><a href="javascript:void(0);">General</a></li>
                  <li class="active">POS Setting</li>
                  </ol>
                  </div>
            
            </div>
           <!--END Page-Title -->  
             <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>

        <?php 
 
         $possettings=$possetting[0];

  if($possettings->free_delivery=='1'){
    $free_delivery='checked';
   $free_delivery_val= $possettings->free_delivery_val;
   $input_filed_free='<input type="text" value="'.$free_delivery_val.'" placeholder="Order Amount Crossed" name="free_delivery_val" class="form-control required_validation_for_add_custo">';

  }else{
    $free_delivery='';
  }
  if($possettings->bill_out_stock=='1'){
    $bill_out_stock='checked';
  }else{
    $bill_out_stock='';
  }
  if($possettings->inventory_status=='1'){
    $inventory_status='checked';
  }else{
    $inventory_status='';
  }
  if($possettings->net_weight_content=='1'){
    $net_weight_content='checked';
  }else{
    $net_weight_content='';
  }
  if($possettings->adjustment_during=='1'){
    $adjustment_during='checked';
  }else{
    $adjustment_during='';
  }
  if($possettings->allow_sp_desc=='1'){
    $allow_sp_desc='checked';
  }else{
    $allow_sp_desc='';
  }
  if($possettings->serial_number=='1'){
    $serial_number='checked';
  }else{
    $serial_number='';
  }
  if($possettings->billing_custom_product=='1'){
    $billing_custom_product='checked';
  }else{
    $billing_custom_product='';
  }
  if($possettings->mobile_verify=='1'){
    $mobile_verify='checked';
  }else{
    $mobile_verify='';
  }
  if($possettings->accept_payment=='1'){
    $accept_payment='checked';
  }else{
    $accept_payment='';
  }
  if($possettings->same_barcode=='1'){
    $same_barcode='checked';
  }else{
    $same_barcode='';
  }
  if($possettings->same_mrp=='1'){
    $same_mrp='checked';
  }else{
    $same_mrp='';
  }
  if($possettings->fifo=='1'){
    $fifo='checked';
  }else{
    $fifo='';
  }
  if($possettings->a_b=='1'){
    $a_b='checked';
  }else{
    $a_b='';
  }
    
        

        ?>
           <?php echo form_open(base_url('Setting/possetting'), array( 'id' => 'possetting_save', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
            <div class="row m-t-20">
              <div class="col-md-12">
                <div class="card-box row">
                  <div class="col-md-12">
            <div class="form-group">
          <label>Minimum Order Amount</label>
            <input type="text" value="<?=$possettings->minimum_order_amount?>" name="minimum_order_amount" class="form-control required_validation_for_add_custo">
            <div class="errorMessage"><?php echo form_error('minimum_order_amount'); ?></div>
          </div>
          </div>
          <div class="col-md-12">
    <div class="form-group">
  <label>Delivery Charges</label>
  <input type="text" value="<?=$possettings->delivery_charges?>" name="delivery_charges" class="form-control required_validation_for_add_custo">
  <div class="errorMessage"><?php echo form_error('delivery_charges'); ?></div>
  </div>
  </div>
  <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" id="free_delivery" <?=$free_delivery;?> name="free_delivery">&nbsp;Apply Free Delivery When Order Amount Crossed

<div class="free_delivery_html"> <?=$input_filed_free;?> </div>

</div>
</div>
<div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1"  <?=$bill_out_stock;?> name="bill_out_stock">&nbsp;Bill Products That are Out Of Stock
</div>
</div>
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1"  <?=$inventory_status;?> name="inventory_status">&nbsp;Show Inventory Status On Product Page On the E-commerence Websites
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1"  <?=$net_weight_content;?> name="net_weight_content">&nbsp;Show Net Weight/Content On the Same
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1"  <?=$adjustment_during;?> name="adjustment_during">&nbsp;Allow Additional Adjustment During Billing
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?=$allow_sp_desc;?>  name="allow_sp_desc">&nbsp;Allow SP or Disc Notification
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1"  <?=$serial_number;?> name="serial_number">&nbsp;Enable Capturing IMEI/Serial Number
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?=$billing_custom_product;?>  name="billing_custom_product">&nbsp;Billing Custom Products
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?=$mobile_verify;?>  name="mobile_verify">&nbsp;Enable Mobile no Verification When Receiveing a Payment On Customer Credit
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1"  <?=$accept_payment;?> name="accept_payment">&nbsp;Accept Payments Without Customer Registration
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<p>Inventory Deduction Rules</p>
<div class="row">

                            			<div class="col-sm-12">

 
												<div class="form-group m-r-10">
                          <input type="checkbox" <?=$same_barcode;?>  value="1" name="same_barcode" >
													<label for="exampleInputName2">Same Barcode</label>

												</div>
												<div class="form-group m-r-10">
                            <input type="checkbox" <?=$same_mrp;?>  value="1"  name="same_mrp">
													<label for="exampleInputEmail2">Same MRP</label>

												</div>
                        <div class="form-group m-r-10">
                            <input type="checkbox" <?=$fifo;?>  value="1"  name="fifo">
													<label for="exampleInputEmail2">FIFO</label>

												</div>
                        <div class="form-group m-r-10">
                            <input type="checkbox" <?=$a_b;?>  value="1"  name="a_b">
													<label for="exampleInputEmail2">A & B</label>

												</div>

										 
                            			</div>

                            		</div>
</div>
</div> -->
<div class="col-md-12">
  <div class="form-group">
    <button type="submit"  class="btn btn-default">Submit</button>
  </div>

</div>
               </div>
              </div>
            </div>
            <?php echo form_close(); ?>


 

          </div>
        </div>
      </div>
    <?php $this->load->view('Template/footer.php') ?>


<script type="text/javascript">
  
$(document).ready(function(){

 
 $('#free_delivery').change(function() {
        if(this.checked) {
         
         
 $('.free_delivery_html').html('<input type="text" value="" placeholder="Order Amount Crossed" name="free_delivery_val" class="form-control required_validation_for_add_custo">');
        }else{
          
              $('.free_delivery_html').html('');
        }
                
    });


 $('#possetting_save').submit(function(e){
    
  

var check_required_field='';
      $(this).find(".required_validation_for_add_custo").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {
        e.preventDefault(); 
        return false;
      }else{      
       return true;

      }  
    });  


 }); 

</script>