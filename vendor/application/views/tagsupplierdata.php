<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

             <!--START Page-Title -->
            <div class="row">
             
                <div class="col-md-8">
                   <h4 class="page-title">Tag Supplier</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Supplier</a></li>
                  <li class="active">Tag Supplier</li>
                  </ol>
                  </div>
                  
            </div>
           <!--END Page-Title -->
            

                                      <div class="row m-t-20">
        <div class="col-sm-12">
        <div class="card-box">
        
        <form method="">
        <div class="row">
       <div class="col-md-12 form-horizontal">
        
       
        <div class="form-group">
        <label class="col-md-3 control-label p-t-0"><h4 class="m-t-0 header-title" style="font-size: 22px;margin-bottom: 29px;    color: #f9a02d!important;"><b>  Product</b></h4>
        Axe</label>
       <div class="col-md-9">
       <h4 class="m-t-0 header-title" style="font-size: 22px;margin-bottom: 29px;    color: #f9a02d!important;"><b>Supplier
</b></h4>
          <select class="form-control">
              <option>Choose Options</option>
          <option>Type 1</option>
          <option>Type 2</option>
        </select>
          </div>
        </div>
        </div>
        <div class="col-md-12 form-horizontal">
         
          <div class="form-group">
          <label class="col-md-3 control-label p-t-0">Rock</label>
          <div class="col-md-9">
          <select class="form-control">
              <option>Choose Options</option>
          <option>Type 1</option>
          <option>Type 2</option>
        </select>
          </div>
          </div>
          
        </div>
        <div class="col-md-12 ">
        <input type="submit" class="btn btn-default pull-right m-r-10" name="customSearchDriver" value="Submit">
        <button type="reset" class="btn btn-info pull-right m-r-10">Reset</button>
        </div>
        </div>
        </form>
        </div>
        </div>
        </div>

                                       <div id="con-close-modal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Transfer</h4> 
                                                </div> 
                                                <div class="modal-body"> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label">Choose Location</label> 
                                                                <select class="form-control">
                                                                    <option>Jaipur</option>
                                                                    <option>Ajmer</option>
                                                                </select>
                                                            </div> 
                                                        </div> 
                                                     
                                                    </div> 
                                                    
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                                </div> 
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->
                                      
                                    
        


      </div>
    </div>
  </div>
<!--Delete-->

<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
