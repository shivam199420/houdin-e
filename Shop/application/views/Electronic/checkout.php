<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
  $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
  $ip = $_SERVER['REMOTE_ADDR'];
}

$url = "http://www.geoplugin.net/json.gp?ip=".$ip;
// append the header putting the secret key and hash
$request_headers = array();
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 60);
curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$data = curl_exec($ch);
if (curl_errno($ch))
{
  print "Error: " . curl_error($ch);
}
else
{
  $transaction = json_decode($data, TRUE);
  curl_close($ch);
}
   $transaction['geoplugin_countryName'];
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<?php
$name = explode(" ",$userData->houdinv_user_name);
if(!$name[1])
{
    $name[1]='';
}
?>
  <!-- Content -->
  <div id="content">
    <!-- Payout Method -->
    <section class="padding-bottom-60">

      <div class="container">



        <!-- Payout Method -->
        <?php
        if($this->session->flashdata('message_name'))
        {
            echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
        }
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        <?php echo form_open(base_url( 'Checkout/UpdateCheckout' ), array( 'id' => 'UpdateCheckout',"class"=>"checkout_form", 'method'=>'post' ));?>
        <div class="check-out margin-top-50">
            <!-- section 1  start -->
        <div class="col-sm-6">
        <div class="heading">
            <h2>Billing Details</h2>
            <hr>
        </div>
        <?php

        if($AddressData)
        {
        ?>
        <div class="billing_already">
        <?php
        foreach($AddressData as $address)
        {

        ?>
        <div class="payment-method">

        <div class="form-group col-md-12" style="">
        <p style="padding:5px;">
        <input name="billing_radio" type="radio" value="<?php echo $address->houdinv_user_address_id ;?>" class="billing_already_radio1"/>
        <?php echo $address->houdinv_user_address_name ;?> <br/>
        <?php echo $address->houdinv_user_address_phone ;?><br/>
        <?php echo $address->houdinv_user_address_user_address ;?><br/>
        <?php echo $address->houdinv_user_address_zip ;?>,<?php echo $address->houdinv_user_address_city ;?></p>
        </div>
        </div>
        <?php } ?>
        <div class="qc-button" style="text-align:center">
        <a class="btn-round billing_Change" style="cursor:pointer;margin-bottom: 10px;" tabindex="0">Change address</a>
        </div>
        </div>
       <?php } ?>
       <div class="billing_form" style="<?php if($AddressData) { echo 'display:none'; } ?>">
       <div class="form-row qc-button" style="text-align:center">
           <div class="form-group col-md-6">
               <a class="btn-round billing_Change1" style="cursor:pointer" tabindex="0">Use Default</a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">

        <div class="col-sm-6"><label> First name
        <input name="billing_first_name" placeholder="First name" value="<?php echo $name[0]; ?>"  class="form-control required_validation_for_checkout" type="text">
        </label>
        </div>
        <div class="col-sm-6"><label> Last name
        <input name="billing_last_name" placeholder="Last name" value="<?php echo $name[1]; ?>" class="form-control" type="text">
        </label>
        </div>

        <div class="col-sm-6"><label> Company name
        <input name="billing_company_name" placeholder="Company name" value="<?php  ?>" class="form-control" type="text">
        </label>
        </div>
        <div class="col-sm-6"><label> Email
        <input name="billing_email" placeholder="Email address" value="<?php echo $userData->houdinv_user_email; ?>" class="form-control required_validation_for_checkout" type="email">
        </label>
        </div>

        <div class="col-sm-6"><label> Contact
        <input name="billing_phone" placeholder="Phone number"  value="<?php echo $userData->houdinv_user_contact; ?>" class="form-control required_validation_for_checkout" type="text">
        </label>
        </div>
        <div class="col-sm-6"><label> Country
        <select id="country" name="billing_country" class="custom-select form-control required_validation_for_checkout" >
            <option value="">Choose Country</option>
                <option value="">Choose Country</option>
                        <option value="Afganistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Angola">Angola</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bonaire">Bonaire</option>
                        <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Brazil">Brazil</option>
                        <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                        <option value="Brunei">Brunei</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Canary Islands">Canary Islands</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Channel Islands">Channel Islands</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos Island">Cocos Island</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Congo">Congo</option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Cote DIvoire">Cote D'Ivoire</option>
                        <option value="Croatia">Croatia</option>
                        <option value="Cuba">Cuba</option>
                        <option value="Curaco">Curacao</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Republic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="East Timor">East Timor</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands">Falkland Islands</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Ter">French Southern Ter</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Great Britain">Great Britain</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Hawaii">Hawaii</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iran">Iran</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Isle of Man">Isle of Man</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="Korea North">Korea North</option>
                        <option value="Korea Sout">Korea South</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Laos">Laos</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libya">Libya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Macau">Macau</option>
                        <option value="Macedonia">Macedonia</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique">Martinique</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mayotte">Mayotte</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Midway Islands">Midway Islands</option>
                        <option value="Moldova">Moldova</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Montserrat">Montserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Nambia">Nambia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherland Antilles">Netherland Antilles</option>
                        <option value="Netherlands">Netherlands (Holland, Europe)</option>
                        <option value="Nevis">Nevis</option>
                        <option value="New Caledonia">New Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue">Niue</option>
                        <option value="Norfolk Island">Norfolk Island</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau Island">Palau Island</option>
                        <option value="Palestine">Palestine</option>
                        <option value="Panama">Panama</option>
                        <option value="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Phillipines">Philippines</option>
                        <option value="Pitcairn Island">Pitcairn Island</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Republic of Montenegro">Republic of Montenegro</option>
                        <option value="Republic of Serbia">Republic of Serbia</option>
                        <option value="Reunion">Reunion</option>
                        <option value="Romania">Romania</option>
                        <option value="Russia">Russia</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="St Barthelemy">St Barthelemy</option>
                        <option value="St Eustatius">St Eustatius</option>
                        <option value="St Helena">St Helena</option>
                        <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                        <option value="St Lucia">St Lucia</option>
                        <option value="St Maarten">St Maarten</option>
                        <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                        <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                        <option value="Saipan">Saipan</option>
                        <option value="Samoa">Samoa</option>
                        <option value="Samoa American">Samoa American</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal">Senegal</option>
                        <option value="Serbia">Serbia</option>
                        <option value="Seychelles">Seychelles</option>
                        <option value="Sierra Leone">Sierra Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Slovakia">Slovakia</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South Africa</option>
                        <option value="Spain">Spain</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="Sudan">Sudan</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Syria">Syria</option>
                        <option value="Tahiti">Tahiti</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania">Tanzania</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Togo">Togo</option>
                        <option value="Tokelau">Tokelau</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                        <option value="Tuvalu">Tuvalu</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Erimates">United Arab Emirates</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="United States of America">United States of America</option>
                        <option value="Uraguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Vatican City State">Vatican City State</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Vietnam">Vietnam</option>
                        <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                        <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                        <option value="Wake Island">Wake Island</option>
                        <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                        <option value="Yemen">Yemen</option>
                        <option value="Zaire">Zaire</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
            </select>
        </label>
        </div>

        <div class="col-sm-12"><label> Address
        <textarea rows="3" name="billing_street" id="address" placeholder="Street address. Apartment, suite, unit etc. (optional)" class="form-control required_validation_for_checkout"></textarea>
        </label>
        </div>

        <div class="col-sm-6"><label> Post code/ Zip
        <input name="billing_code" placeholder="Post code / Zip" class="form-control required_validation_for_checkout" type="text">
        </label>
        </div>
        <div class="col-sm-6"><label> Town / City
        <input name="billing_city" placeholder="Town / City" class="form-control required_validation_for_checkout" type="text">
        </label>
        </div>

        <div class="col-sm-12"><label> Order Note
        <textarea rows="3" name="billing_order_note" placeholder="Order note" class="form-control"></textarea>
        </label>
        </div>
         <!-- End form -->


 </div>
        </div>
        <div class="custom-control custom-checkbox">
            <input type="checkbox" name="billing_checkbox" class="custom-control-input" id="customCheck1">&nbsp;&nbsp;&nbsp;SHIP TO A DIFFERENT ADDRESS?
        </div>
        <div class="shipping_form" style="display: none;">
        <div class="heading">
            <h2>Shipping Detail</h2>
            <hr>
            <?php
            if($AddressData)
            {
            ?>
            <div class="shipping_already ">
            <?php
            foreach($AddressData as $address)
            {
          ?>
           <div class="payment-method">
        <div class="form-group col-md-12" style="">
        <p style="padding:5px;">
        <input name="shipping_radio" type="radio" value="<?php echo $address->houdinv_user_address_id ;?>" class="shipping_already_radio"/>
        <?php echo $address->houdinv_user_address_name ;?> <br/>
        <?php echo $address->houdinv_user_address_phone ;?><br/>
        <?php echo $address->houdinv_user_address_user_address ;?><br/>
        <?php echo $address->houdinv_user_address_zip ;?>,<?php echo $address->houdinv_user_address_city ;?></p>
        </div>
        </div>
          <?php } ?>
          <div class="qc-button" style="text-align:center">
              <a class="btn-round shipping_Change" tabindex="0">Change address</a>
                </div>
            </div>

            <?php } ?>
          </div>
          <div class="fill_Shipping" style="<?php if($AddressData) { echo 'display:none'; } ?>">
          <div class="form-row qc-button" style="text-align:center">
                        <div class="form-group col-md-6">
                            <a class="btn-round shipping_Change1" tabindex="0">Use Default</a>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">

                        <div class="col-sm-6">
                        <label> First name
                        <input name="shipping_first_name" placeholder="First name" value="<?php echo $name[0]; ?>"  class="form-control required_validation_for_checkout1" type="text">
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <label> Last name
                        <input name="shipping_last_name" placeholder="Last name" value="<?php echo $name[1]; ?>" class="form-control" type="text">
                        </label>
                    </div>

                    <div class="col-sm-6">
                        <label> Company name
                        <input name="shipping_company_name" placeholder="Company name" value="<?php  ?>" class="form-control" type="text">
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <label> Email
                        <input name="shipping_email" placeholder="Email address" value="<?php echo $userData->houdinv_user_email; ?>" class="form-control required_validation_for_checkout1" type="email">
                        </label>
                    </div>

                    <div class="col-sm-6">
                        <label> Contact
                        <input name="shipping_phone" placeholder="Phone number"  value="<?php echo $userData->houdinv_user_contact; ?>" class="form-control required_validation_for_checkout1" type="text">
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <label> Country
                        <select id="country" name="shipping_country" class="custom-select form-control required_validation_for_checkout1" >
                        <option value="">Choose Country</option>
                        <option value="Afganistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Angola">Angola</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bonaire">Bonaire</option>
                        <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Brazil">Brazil</option>
                        <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                        <option value="Brunei">Brunei</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Canary Islands">Canary Islands</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Channel Islands">Channel Islands</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos Island">Cocos Island</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Congo">Congo</option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Cote DIvoire">Cote D'Ivoire</option>
                        <option value="Croatia">Croatia</option>
                        <option value="Cuba">Cuba</option>
                        <option value="Curaco">Curacao</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Republic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="East Timor">East Timor</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands">Falkland Islands</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Ter">French Southern Ter</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Great Britain">Great Britain</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Hawaii">Hawaii</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iran">Iran</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Isle of Man">Isle of Man</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="Korea North">Korea North</option>
                        <option value="Korea Sout">Korea South</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Laos">Laos</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libya">Libya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Macau">Macau</option>
                        <option value="Macedonia">Macedonia</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique">Martinique</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mayotte">Mayotte</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Midway Islands">Midway Islands</option>
                        <option value="Moldova">Moldova</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Montserrat">Montserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Nambia">Nambia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherland Antilles">Netherland Antilles</option>
                        <option value="Netherlands">Netherlands (Holland, Europe)</option>
                        <option value="Nevis">Nevis</option>
                        <option value="New Caledonia">New Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue">Niue</option>
                        <option value="Norfolk Island">Norfolk Island</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau Island">Palau Island</option>
                        <option value="Palestine">Palestine</option>
                        <option value="Panama">Panama</option>
                        <option value="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Phillipines">Philippines</option>
                        <option value="Pitcairn Island">Pitcairn Island</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Republic of Montenegro">Republic of Montenegro</option>
                        <option value="Republic of Serbia">Republic of Serbia</option>
                        <option value="Reunion">Reunion</option>
                        <option value="Romania">Romania</option>
                        <option value="Russia">Russia</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="St Barthelemy">St Barthelemy</option>
                        <option value="St Eustatius">St Eustatius</option>
                        <option value="St Helena">St Helena</option>
                        <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                        <option value="St Lucia">St Lucia</option>
                        <option value="St Maarten">St Maarten</option>
                        <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                        <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                        <option value="Saipan">Saipan</option>
                        <option value="Samoa">Samoa</option>
                        <option value="Samoa American">Samoa American</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal">Senegal</option>
                        <option value="Serbia">Serbia</option>
                        <option value="Seychelles">Seychelles</option>
                        <option value="Sierra Leone">Sierra Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Slovakia">Slovakia</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South Africa</option>
                        <option value="Spain">Spain</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="Sudan">Sudan</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Syria">Syria</option>
                        <option value="Tahiti">Tahiti</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania">Tanzania</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Togo">Togo</option>
                        <option value="Tokelau">Tokelau</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                        <option value="Tuvalu">Tuvalu</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Erimates">United Arab Emirates</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="United States of America">United States of America</option>
                        <option value="Uraguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Vatican City State">Vatican City State</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Vietnam">Vietnam</option>
                        <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                        <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                        <option value="Wake Island">Wake Island</option>
                        <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                        <option value="Yemen">Yemen</option>
                        <option value="Zaire">Zaire</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
                    </select>
                        </label>
                    </div>

                    <div class="col-sm-12">
                        <label> Address
                        <textarea rows="3" name="shipping_street" id="address" placeholder="Street address. Apartment, suite, unit etc. (optional)" class="form-control required_validation_for_checkout1"></textarea>
                        </label>
                    </div>

                    <div class="col-sm-6">
                        <label> Postal Code / Zip
                        <input name="shipping_code"  placeholder="Post code / Zip" class="form-control required_validation_for_checkout1 number_validation" type="text">
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <label> Town / City
                        <input name="shipping_city" placeholder="Town / City" class="form-control required_validation_for_checkout1" type="text">
                        </label>
                    </div>

                    <div class="col-sm-12">
                        <label> Order Note
                        <textarea rows="3" name="shipping_order_note" placeholder="Order note" class="form-control"></textarea>
                        </label>
                    </div>

                </div>
        </div>

        </div>
            </div>




        </div>
        <!-- end section 1 -->
        <div class="col-sm-6">
 <!-- Shopping Cart -->
 <div class="heading">
            <h2>Shopping Cart</h2>
            <hr>
          </div>
          <ul class="row check-item" style="list-style:none">
          <?php
            $final_price =0;
            foreach($AllCart as $thisItem)
            {
                $count = $thisItem['count'];
                $main_price = $thisItem['productPrice'];
                $total_price = $main_price*$count;
                $final_price =$final_price+$total_price;

        ?>
          <!-- Check Item List -->

            <li class="col-xs-8">
              <p><?php echo $thisItem['productName']; ?></p>
            </li>
            <li class="col-xs-4 text-center">
              <p><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><span class="product_total_<?php echo $thisItem->houdin_products_id; ?>"><?php echo $total_price; ?></p>
            </li>

            <?php } ?>
</ul>
            <ul class="row check-item" style="list-style:none">
            <li class="col-xs-8">
              <p>Sub Total</p>
            </li>
            <li class="col-xs-4 text-center">
              <p><input type="hidden" name="dont_remove_if_Want_discount_on" class="dont_remove_if_Want_discount_on" value=""/><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <span class="subtotal_Span"><?php echo $final_price; ?></span></p>
            </li>
          </ul>

          <ul class="row check-item" style="list-style:none">
            <li class="col-xs-8">
              <p>Discount</p>
            </li>
            <li class="col-xs-4 text-center">
              <p><input type="hidden" name="dont_remove_if_Want_discount_discount"  class="dont_remove_if_Want_discount_discount" value=""/><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <span class="Discount_Span">0</span></p>
            </li>
          </ul>
          <?php
        $shipping =$this->ShopCheckoutmodel->shippingCharge($final_price);
        $final_price = $final_price + $shipping;
        ?>
          <ul class="row check-item" style="list-style:none">
            <li class="col-xs-8">
              <p>Shipping</p>
            </li>
            <li class="col-xs-4 text-center">
              <p><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <span class="shipping_Span"><?php echo $shipping ; ?></p>
            </li>
          </ul>

          <ul class="row check-item" style="list-style:none">
            <li class="col-xs-8">
              <p>Net Payable</p>
            </li>
            <li class="col-xs-4 text-center">
              <p><input type="hidden" name="dont_remove_if_Want_discount_id" class="dont_remove_if_Want_discount_id" value=""/><span ><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <span class="amount_span"><?php echo $final_price; ?></span></span></p>
            </li>
          </ul>

          <!-- Payment information -->
          <div class="heading margin-top-50">
            <h2>Delivery information</h2>
            <hr>
          </div>

          <!-- Check Item List -->
          <ul class="row check-item" style="list-style:none">

            <li class="col-xs-12">
              <p><input type="radio" id="customRadio3" value="deliver" name="order_delivery_type" class="custom-control-input customRadio_payment1">&nbsp;&nbsp;Deliver</p>
            </li>
          </ul>


          <ul class="row check-item" style="list-style:none">

            <li class="col-xs-12">
              <p><input type="radio" value="pick_up" id="customRadio4" name="order_delivery_type" class="custom-control-input customRadio_payment1">&nbsp;&nbsp;Pick up</p>
            </li>
          </ul>

          <div class="heading margin-top-50">
            <h2>Payment information</h2>
            <hr>
          </div>

          <ul class="row check-item" style="list-style:none">
            <li class="col-xs-12 ">
              <p><input type="radio" id="customRadio1" value="cod" name="customRadio" class="custom-control-input customRadio_payment"/>&nbsp;&nbsp;COD</p>
            </li>
          </ul>
          <?php
          $country=$transaction['geoplugin_countryName'];
          if($country == 'India')
          {
            $getPayU = getPayuCredentials($this->session->userdata('shopName'));
            if(count($getPayU) > 0)
            {
            ?>
           <ul class="row check-item" style="list-style:none">
            <li class="col-xs-12">
              <p><input type="radio" value="payumoney" id="customRadio2" name="customRadio" class="custom-control-input customRadio_payment"/>&nbsp;&nbsp;Pay with credit / debit card</p>
            </li>
          </ul>
        <?php } } else { ?>

             <?php
            $getAuth = getAuthCredentials($this->session->userdata('shopName'));
            if(count($getAuth) > 0)
            {
                ?>
                <ul class="row check-item" style="list-style:none">

            <li class="col-xs-12">
              <p><input type="radio" value="auth" id="customRadio5" name="customRadio" class="custom-control-input customRadio_payment"/>&nbsp;&nbsp;Pay with credit / debit card</p>
            </li>
          </ul>
        <?php } } ?>
        </div>
         <div class="col-sm-12">
    <div class="heading ">
    <h2>Discount</h2>
    <hr>
    </div>
    <div class="cuppon-wrap">


    <input type="text" class="coupon_text form-control"/>
    <p class="coupon_msg_text" style="color: red;margin:15px"></p>
    <p class="coupon_msg_text1" style="color: green;margin:15px"></p>
    <button class="btn-round checkout_coupon_apply" type="button" style="margin-bottom: 15px;">apply coupon</button>

    </div>
    </div>
        <div class="clearfix"></div>
        <hr/>
        <!-- Button -->
        <div class="pro-btn" style="text-align: right !important;">
        <a  class="btn-round submit_form_checkout"><input type="hidden" name="sub" value="Place Order"/>Place Order</a></div>
        </div>
    <?php echo form_close(); ?>

      </div>
    </section>
  </div>
  <!-- End Content -->
  <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>

  <script>

  $( document ).ready(function() {
    var $radios = $('input:radio[name=order_delivery_type]');
    if($radios.is(':checked') === false) {
      $radios.filter('[value=deliver]').prop('checked', true);
    }

    var shipss=$('.shipping_Span').text();
    var amountss=$('.amount_span').text();
    var new_amount=amountss-shipss;

    $('input[type=radio][name=order_delivery_type]').change(function() {
      if (this.value == 'deliver') {
        $('.shipping_Span').text(shipss);
        $('.amount_span').text(amountss);
      }
      else if (this.value == 'pick_up') {
        $('.shipping_Span').text('0');
        $('.amount_span').text(new_amount);
      }
    });
  });

  </script>
