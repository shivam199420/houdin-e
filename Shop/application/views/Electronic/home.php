<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>
<?php
$getData = stroeSetting($this->session->userdata('shopName'));
$cartFunction = $getData[0]->receieve_order_out;
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<style type="text/css">

.testimonial-section2{
  /*height: 600px;*/
  position: relative;
  padding: 50px 0;
  background-color: #0088cc;
  color: white!important;
}

.testim .wrap {
    position: relative;
    width: 100%;
    max-width: 1020px;
    padding: 40px 20px;
    margin: auto;
}

.testim .arrow {
    display: block;
    position: absolute;
    color: #eee;
    cursor: pointer;
    font-size: 2em;
    top: 50%;
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -o-transform: translateY(-50%);
    transform: translateY(-50%);
    -webkit-transition: all .3s ease-in-out;
    -ms-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;
    padding: 5px;
    z-index: 22222222;
}

.testim .arrow:before {
    cursor: pointer;
}

.testim .arrow:hover {
    color: #aaaaaa;
}


.testim .arrow.left {
    left: 10px;
}

.testim .arrow.right {
    right: 10px;
}

.testim .dots {
    text-align: center;
    position: absolute;
    width: 100%;
    bottom: 60px;
    left: 0;
    display: block;
    z-index: 3333;
    height: 12px;
}

.testim .dots .dot {
    list-style-type: none;
    display: inline-block;
    width: 12px;
    height: 12px;
    border-radius: 50%;
    border: 1px solid #eee;
    margin: 0 10px;
    cursor: pointer;
    -webkit-transition: all .5s ease-in-out;
    -ms-transition: all .5s ease-in-out;
    -moz-transition: all .5s ease-in-out;
    -o-transition: all .5s ease-in-out;
    transition: all .5s ease-in-out;
    position: relative;
}

.testim .dots .dot.active,
.testim .dots .dot:hover {
        background: #f5f5f7;
    border-color: #aaaaaa;
}

.testim .dots .dot.active {
    -webkit-animation: testim-scale .5s ease-in-out forwards;
    -moz-animation: testim-scale .5s ease-in-out forwards;
    -ms-animation: testim-scale .5s ease-in-out forwards;
    -o-animation: testim-scale .5s ease-in-out forwards;
    animation: testim-scale .5s ease-in-out forwards;
}

.testim .cont {
    position: relative;
    overflow: hidden;
}

.testim .cont > div {
    text-align: center;
    position: absolute;
    top: 0;
    left: 0;
    padding: 0 0 70px 0;
    opacity: 0;
}

.testim .cont > div.inactive {
    opacity: ;
}


.testim .cont > div.active {
    position: relative;
    opacity: 1;
}


.testim .cont div .img img {
    display: block;
    width: 100px;
    height: 100px;
    margin: auto;
    border-radius: 50%;
}

.testim .cont div .h4 {
    color: white;
    font-size: 1.2em;
    margin: 15px 0;
}

.testim .cont div p {
    font-size: 1.15em;
    color: #eee;
    width: 80%;
    margin: auto;
}

.testim .cont div.active .img img {
    -webkit-animation: testim-show .5s ease-in-out forwards;
    -moz-animation: testim-show .5s ease-in-out forwards;
    -ms-animation: testim-show .5s ease-in-out forwards;
    -o-animation: testim-show .5s ease-in-out forwards;
    animation: testim-show .5s ease-in-out forwards;
}

.testim .cont div.active .h4 {
    -webkit-animation: testim-content-in .4s ease-in-out forwards;
    -moz-animation: testim-content-in .4s ease-in-out forwards;
    -ms-animation: testim-content-in .4s ease-in-out forwards;
    -o-animation: testim-content-in .4s ease-in-out forwards;
    animation: testim-content-in .4s ease-in-out forwards;
}

.testim .cont div.active p {
    -webkit-animation: testim-content-in .5s ease-in-out forwards;
    -moz-animation: testim-content-in .5s ease-in-out forwards;
    -ms-animation: testim-content-in .5s ease-in-out forwards;
    -o-animation: testim-content-in .5s ease-in-out forwards;
    animation: testim-content-in .5s ease-in-out forwards;
}

.testim .cont div.inactive .img img {
    -webkit-animation: testim-hide .5s ease-in-out forwards;
    -moz-animation: testim-hide .5s ease-in-out forwards;
    -ms-animation: testim-hide .5s ease-in-out forwards;
    -o-animation: testim-hide .5s ease-in-out forwards;
    animation: testim-hide .5s ease-in-out forwards;
}

.testim .cont div.inactive .h4 {
    -webkit-animation: testim-content-out .4s ease-in-out forwards;
    -moz-animation: testim-content-out .4s ease-in-out forwards;
    -ms-animation: testim-content-out .4s ease-in-out forwards;
    -o-animation: testim-content-out .4s ease-in-out forwards;
    animation: testim-content-out .4s ease-in-out forwards;
}

.testim .cont div.inactive p {
    -webkit-animation: testim-content-out .5s ease-in-out forwards;
    -moz-animation: testim-content-out .5s ease-in-out forwards;
    -ms-animation: testim-content-out .5s ease-in-out forwards;
    -o-animation: testim-content-out .5s ease-in-out forwards;
    animation: testim-content-out .5s ease-in-out forwards;
}

@-webkit-keyframes testim-scale {
    0% {
        -webkit-box-shadow: 0px 0px 0px 0px #eee;
        box-shadow: 0px 0px 0px 0px #eee;
    }

    35% {
        -webkit-box-shadow: 0px 0px 10px 5px #eee;
        box-shadow: 0px 0px 10px 5px #eee;
    }

    70% {
        -webkit-box-shadow: 0px 0px 10px 5px #ea830e;
        box-shadow: 0px 0px 10px 5px #ea830e;
    }

    100% {
        -webkit-box-shadow: 0px 0px 0px 0px #ea830e;
        box-shadow: 0px 0px 0px 0px #ea830e;
    }
}

@-moz-keyframes testim-scale {
    0% {
        -moz-box-shadow: 0px 0px 0px 0px #eee;
        box-shadow: 0px 0px 0px 0px #eee;
    }

    35% {
        -moz-box-shadow: 0px 0px 10px 5px #eee;
        box-shadow: 0px 0px 10px 5px #eee;
    }

    70% {
        -moz-box-shadow: 0px 0px 10px 5px #ea830e;
        box-shadow: 0px 0px 10px 5px #ea830e;
    }

    100% {
        -moz-box-shadow: 0px 0px 0px 0px #ea830e;
        box-shadow: 0px 0px 0px 0px #ea830e;
    }
}

@-ms-keyframes testim-scale {
    0% {
        -ms-box-shadow: 0px 0px 0px 0px #eee;
        box-shadow: 0px 0px 0px 0px #eee;
    }

    35% {
        -ms-box-shadow: 0px 0px 10px 5px #eee;
        box-shadow: 0px 0px 10px 5px #eee;
    }

    70% {
        -ms-box-shadow: 0px 0px 10px 5px #ea830e;
        box-shadow: 0px 0px 10px 5px #ea830e;
    }

    100% {
        -ms-box-shadow: 0px 0px 0px 0px #ea830e;
        box-shadow: 0px 0px 0px 0px #ea830e;
    }
}

@-o-keyframes testim-scale {
    0% {
        -o-box-shadow: 0px 0px 0px 0px #eee;
        box-shadow: 0px 0px 0px 0px #eee;
    }

    35% {
        -o-box-shadow: 0px 0px 10px 5px #eee;
        box-shadow: 0px 0px 10px 5px #eee;
    }

    70% {
        -o-box-shadow: 0px 0px 10px 5px #ea830e;
        box-shadow: 0px 0px 10px 5px #ea830e;
    }

    100% {
        -o-box-shadow: 0px 0px 0px 0px #ea830e;
        box-shadow: 0px 0px 0px 0px #ea830e;
    }
}

@keyframes testim-scale {
    0% {
        box-shadow: 0px 0px 0px 0px #eee;
    }

    35% {
        box-shadow: 0px 0px 10px 5px #eee;
    }

    70% {
        box-shadow: 0px 0px 10px 5px #ea830e;
    }

    100% {
        box-shadow: 0px 0px 0px 0px #ea830e;
    }
}

@-webkit-keyframes testim-content-in {
    from {
        opacity: 0;
        -webkit-transform: translateY(100%);
        transform: translateY(100%);
    }

    to {
        opacity: 1;
        -webkit-transform: translateY(0);
        transform: translateY(0);
    }
}

@-moz-keyframes testim-content-in {
    from {
        opacity: 0;
        -moz-transform: translateY(100%);
        transform: translateY(100%);
    }

    to {
        opacity: 1;
        -moz-transform: translateY(0);
        transform: translateY(0);
    }
}

@-ms-keyframes testim-content-in {
    from {
        opacity: 0;
        -ms-transform: translateY(100%);
        transform: translateY(100%);l
    }

    to {
        opacity: 1;
        -ms-transform: translateY(0);
        transform: translateY(0);
    }
}

@-o-keyframes testim-content-in {
    from {
        opacity: 0;
        -o-transform: translateY(100%);
        transform: translateY(100%);
    }

    to {
        opacity: 1;
        -o-transform: translateY(0);
        transform: translateY(0);
    }
}

@keyframes testim-content-in {
    from {
        opacity: 0;
        transform: translateY(100%);
    }

    to {
        opacity: 1;
        transform: translateY(0);
    }
}

@-webkit-keyframes testim-content-out {
    from {
        opacity: 1;
        -webkit-transform: translateY(0);
        transform: translateY(0);
    }

    to {
        opacity: 0;
        -webkit-transform: translateY(-100%);
        transform: translateY(-100%);
    }
}

@-moz-keyframes testim-content-out {
    from {
        opacity: 1;
        -moz-transform: translateY(0);
        transform: translateY(0);
    }

    to {
        opacity: 0;
        -moz-transform: translateY(-100%);
        transform: translateY(-100%);
    }
}

@-ms-keyframes testim-content-out {
    from {
        opacity: 1;
        -ms-transform: translateY(0);
        transform: translateY(0);
    }

    to {
        opacity: 0;
        -ms-transform: translateY(-100%);
        transform: translateY(-100%);
    }
}

@-o-keyframes testim-content-out {
    from {
        opacity: 1;
        -o-transform: translateY(0);
        transform: translateY(0);
    }

    to {
        opacity: 0;
        transform: translateY(-100%);
        transform: translateY(-100%);
    }
}

@keyframes testim-content-out {
    from {
        opacity: 1;
        transform: translateY(0);
    }

    to {
        opacity: 0;
        transform: translateY(-100%);
    }
}

@-webkit-keyframes testim-show {
    from {
        opacity: 0;
        -webkit-transform: scale(0);
        transform: scale(0);
    }

    to {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }
}

@-moz-keyframes testim-show {
    from {
        opacity: 0;
        -moz-transform: scale(0);
        transform: scale(0);
    }

    to {
        opacity: 1;
        -moz-transform: scale(1);
        transform: scale(1);
    }
}

@-ms-keyframes testim-show {
    from {
        opacity: 0;
        -ms-transform: scale(0);
        transform: scale(0);
    }

    to {
        opacity: 1;
        -ms-transform: scale(1);
        transform: scale(1);
    }
}

@-o-keyframes testim-show {
    from {
        opacity: 0;
        -o-transform: scale(0);
        transform: scale(0);
    }

    to {
        opacity: 1;
        -o-transform: scale(1);
        transform: scale(1);
    }
}

@keyframes testim-show {
    from {
        opacity: 0;
        transform: scale(0);
    }

    to {
        opacity: 1;
        transform: scale(1);
    }
}

@-webkit-keyframes testim-hide {
    from {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }

    to {
        opacity: 0;
        -webkit-transform: scale(0);
        transform: scale(0);
    }
}

@-moz-keyframes testim-hide {
    from {
        opacity: 1;
        -moz-transform: scale(1);
        transform: scale(1);
    }

    to {
        opacity: 0;
        -moz-transform: scale(0);
        transform: scale(0);
    }
}

@-ms-keyframes testim-hide {
    from {
        opacity: 1;
        -ms-transform: scale(1);
        transform: scale(1);
    }

    to {
        opacity: 0;
        -ms-transform: scale(0);
        transform: scale(0);
    }
}

@-o-keyframes testim-hide {
    from {
        opacity: 1;
        -o-transform: scale(1);
        transform: scale(1);
    }

    to {
        opacity: 0;
        -o-transform: scale(0);
        transform: scale(0);
    }
}

@keyframes testim-hide {
    from {
        opacity: 1;
        transform: scale(1);
    }

    to {
        opacity: 0;
        transform: scale(0);
    }
}

@media all and (max-width: 300px) {
  body {
    font-size: 14px;
  }
}

@media all and (max-width: 500px) {
  .testim .arrow {
    font-size: 1.5em;
  }

  .testim .cont div p {
    line-height: 25px;
  }

}

</style>

<!-- Start Slider Area -->
<?php
if(count($sliderListData) > 0)
{
?>
<section class="slid-sec">
<div class="container">
<div class="container-fluid">
<div class="row">

<!-- Main Slider  -->
<div class="col-md-12 no-padding">

<!-- Main Slider Start -->
<div class="tp-banner-container">
<div class="tp-banner">
<ul>
<?php
foreach($sliderListData as $sliderListDataList)
{
?>
<!-- SLIDE  -->
<li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >
<!-- MAIN IMAGE -->

<img src="<?php echo $this->session->userdata('vendorURL') ?>upload/Slider/<?php echo $sliderListDataList->houdinv_custom_slider_image ?>"  alt="slider"  data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat">

<!-- LAYER NR. 1 -->
<div class="tp-caption sfl tp-resizeme"
data-x="left" data-hoffset="60"
data-y="center" data-voffset="-110"
data-speed="800"
data-start="800"
data-easing="Power3.easeInOut"
data-splitin="chars"
data-splitout="none"
data-elementdelay="0.03"
data-endelementdelay="0.4"
data-endspeed="300"
style="z-index: 5; font-size:30px; font-weight:500; color:#888888;  max-width: auto; max-height: auto; white-space: nowrap;"><?php echo $sliderListDataList->houdinv_custom_slider_head ?> </div>

<!-- LAYER NR. 2 -->
<div class="tp-caption sfr tp-resizeme"
data-x="left" data-hoffset="60"
data-y="center" data-voffset="-60"
data-speed="800"
data-start="1000"
data-easing="Power3.easeInOut"
data-splitin="chars"
data-splitout="none"
data-elementdelay="0.03"
data-endelementdelay="0.1"
data-endspeed="300"
style="z-index: 6; font-size:50px; color:#0088cc; font-weight:800; white-space: nowrap;"><?php echo $sliderListDataList->houdinv_custom_slider_sub_heading ?></div>
</li>
<?php } ?>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<?php } ?>
<!-- Content -->
<div id="content">
<?php
if($this->session->flashdata('message_name'))
{
    echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
}
if($this->session->flashdata('success'))
{
    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
}
?>
    <!-- Product category -->
    <?php
    if(count($categoryData) > 0 && $customHome[0]->houdinv_custom_home_data_category)
    {
    ?>
    <section class=" padding-bottom-60">
        <div class="container">
    <div class="heading">
    <h2><?php echo $customHome[0]->houdinv_custom_home_data_category; ?></h2>
    <hr>
    </div>
    <ul class="row" style="list-style: none;display: inline-block;">
    <?php
    foreach($categoryData as $categoryDataList)
    {
        if($categoryDataList->houdinv_category_thumb)
        {
        $setImageData = $this->session->userdata('vendorURL')."images/category/".$categoryDataList->houdinv_category_thumb;
        }
        else
        {
        $setImageData = base_url()."Extra/noPhotoFound.png";
        }
    ?>
    
    <a href="<?php echo base_url() ?>Productlist/<?php echo $categoryDataList->houdinv_category_id ?>">
      <li class="col-md-3">  <img class="img-responsive" src="<?php echo $setImageData ?>" style="width:100%;height: 361px;" alt="">
        <div class="bottom-details"> <span><?php echo date("Y"); ?> Collection</span>

        <h3><?php echo $categoryDataList->houdinv_category_name ?></h3>
           </div>

      </li>
      </a>
    <?php } ?>
    </ul>
    </div>
  </section>
    <?php } ?>

<!-- Latest Product -->
<?php
if($customHome[0]->houdinv_custom_home_data_latest_product && count($latestProducts) > 0)
{
?>
<section class="light-gry-bg padding-top-60 padding-bottom-30">
<div class="container">

<!-- heading -->
<div class="heading">
<h2><?php echo $customHome[0]->houdinv_custom_home_data_latest_product; ?></h2>
<hr>
</div>

<!-- Items -->
<div class="item-col-4">
<?php
foreach($latestProducts as $latestProductsList)
{
    $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
    $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
?>

<!-- Product -->
<div class="product">
<article> 

<?php 
    if($getProductImage[0])
    {
        $setHomeData = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
    }
    else
    {
        $setHomeData = base_url()."Extra/noPhotoFound.png";
    }
    ?>

<img class="img-responsive" src="<?php echo $setHomeData ?>" alt="" >

<?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?><span <?php if($latestProductsList->houdinv_products_main_quotation == 1) { ?> style="top:41px" <?php  } ?> class="sale-tag">Out of Stock</span>  <?php } ?>
<?php
if($latestProductsList->houdinv_products_main_quotation == 1)
{
?>
<span class="new-tag model_qute_show"  data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal">Ask Quotation</span>
<?php
}
?>
<!-- Content -->
<a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>" class="tittle"><?php echo substr($latestProductsList->houdin_products_title,0, 35).".." ; ?></a>
<!-- Reviews -->
<br/>
<div class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?> <?php  } ?></div>
<?php

        if($latestProductsList->houdinv_products_total_stocks <= 0)
        {
          if($cartFunction == 1)
          {
            ?>
            <a href="javascript:;" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" class="cart-btn Add_to_cart_button"><i class="icon-basket-loaded"></i></a>
         <?php  }
        }
        else
        {
          ?>
            <a href="javascript:;" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" class="cart-btn Add_to_cart_button" title="Add to Cart"><i class="icon-basket-loaded"></i></a>
        <?php }
        ?>
         <a href="javascript:;"  class="cart-btn Add_to_cart_button m-r-10" title="Add To Wishlist"><i class="fa fa-heart "></i></a>
</article>
</div>
<?php } ?>
</div>
</div>
</section>
<?php } ?>
<?php
if($customHome[0]->houdinv_custom_home_data_featured_product)
{
?>
<section class="light-gry-bg padding-top-60 padding-bottom-30">
<div class="container">

<!-- heading -->
<div class="heading">
<h2><?php echo $customHome[0]->houdinv_custom_home_data_featured_product; ?></h2>
<hr>
</div>

<!-- Items -->
<div class="item-col-4">
<?php
foreach($featuredProduct as $latestProductsList)
{
    $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
    $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
?>
<!-- Product -->
<div class="product">
<article> 
<?php 
    if($getProductImage[0])
    {
        $setFeaturedData = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
    }
    else
    {
        $setFeaturedData = base_url()."Extra/noPhotoFound.png";
    }
    ?>
<img class="img-responsive" src="<?php echo $setFeaturedData; ?>" alt="" >

<?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?><span <?php if($latestProductsList->houdinv_products_main_quotation == 1) { ?> style="top:41px" <?php  } ?> class="sale-tag">Out of Stock</span>  <?php } ?>
<?php
if($latestProductsList->houdinv_products_main_quotation == 1)
{
?>
<span class="new-tag model_qute_show"  data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal">Ask Quotation</span>
<?php
}
?>
<!-- Content -->
<a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>" class="tittle"><?php echo substr($latestProductsList->houdin_products_title,0, 35).".." ; ?></a>
<!-- Reviews -->
<br/>
<div class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?> <?php  } ?></div>
<?php
if($latestProductsList->houdinv_products_total_stocks <= 0)
{
    if($cartFunction == 1)
    {
    ?>
    <a href="javascript:;" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" class="cart-btn Add_to_cart_button"><i class="icon-basket-loaded"></i></a>
    <?php  }
}
else
{
    ?>
    <a href="javascript:;" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" class="cart-btn Add_to_cart_button"><i class="icon-basket-loaded"></i></a>
<?php }
?>
<a href="javascript:;" class="cart-btn Add_to_cart_button m-r-10" title="Add To Wishlist"><i class="fa fa-heart "></i></a>
</article>
</div>
<?php } ?>
</div>
</div>
</section>

<!-- mann section -->
<?php
if(count($storeTestimonial) > 0 && $customHome[0]->houdinv_custom_home_data_testimonial)
{
?>
    <section class="testimonial-section2">
 <div class="row text-center">
           <div class="col-12">
              <div class="h2"><?php echo $customHome[0]->houdinv_custom_home_data_testimonial; ?></div>
           </div>
        </div>
       <div id="testim" class="testim">
            <div class="wrap">

                <span id="right-arrow" class="arrow right fa fa-chevron-right"></span>
                <span id="left-arrow" class="arrow left fa fa-chevron-left "></span>
                <ul id="testim-dots" class="dots">
                <?php
                $i = 1;
                foreach($storeTestimonial as $storeTestimonialData)
                {
                ?>

                    <li class="dot <?php if($i == 1){ echo "active"; } ?>"></li>
                <?php $i++; } ?>
                </ul>
                <div id="testim-content" class="cont">
                <?php
                $i = 1;
                foreach($storeTestimonial as $storeTestimonialData)
                {
                ?>
                    <div class="active">
                        <div class="img"><img src="<?php if($storeTestimonialData->testimonials_image) { ?> <?php echo $this->session->userdata('vendorURL')."upload/testimonials/".$storeTestimonialData->testimonials_image; ?> <?php  } else { ?> <?php echo base_url() ?>Extra/apparels/img/testimonial/1.jpg <?php } ?>" alt=""></div>
                        <div class="h4"><?php echo $storeTestimonialData->testimonials_name ?></div>
                        <p><?php echo $storeTestimonialData->testimonials_feedback ?></p>
                    </div>
                <?php } ?>
                    <!-- <div>
                        <div class="img"><img src="https://image.ibb.co/cNP817/pexels_photo_220453.jpg" alt=""></div>
                        <div class="h4">Jessica</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                    </div>

                    <div>
                        <div class="img"><img src="https://image.ibb.co/iN3qES/pexels_photo_324658.jpg" alt=""></div>
                         <div class="h4">Kellie</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                    </div>

                    <div>
                        <div class="img"><img src="https://image.ibb.co/kL6AES/Top_SA_Nicky_Oppenheimer.jpg" alt=""></div>
                        <div class="h4">Jessica</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                    </div>

                    <div>
                        <div class="img"><img src="https://image.ibb.co/gUPag7/image.jpg" alt=""></div>
                        <div class="h4">Kellie</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                    </div> -->

                </div>
                 </div>
            </div>

    </section>
<?php } ?>
<!-- end section -->
<?php
}
?>
<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/footer') ?>

<script type="text/javascript">

// vars
'use strict'
var testim = document.getElementById("testim"),
        testimDots = Array.prototype.slice.call(document.getElementById("testim-dots").children),
    testimContent = Array.prototype.slice.call(document.getElementById("testim-content").children),
    testimLeftArrow = document.getElementById("left-arrow"),
    testimRightArrow = document.getElementById("right-arrow"),
    testimSpeed = 4500,
    currentSlide = 0,
    currentActive = 0,
    testimTimer,
        touchStartPos,
        touchEndPos,
        touchPosDiff,
        ignoreTouch = 30;
;

window.onload = function() {

    // Testim Script
    function playSlide(slide) {
        for (var k = 0; k < testimDots.length; k++) {
            testimContent[k].classList.remove("active");
            testimContent[k].classList.remove("inactive");
            testimDots[k].classList.remove("active");
        }

        if (slide < 0) {
            slide = currentSlide = testimContent.length-1;
        }

        if (slide > testimContent.length - 1) {
            slide = currentSlide = 0;
        }

        if (currentActive != currentSlide) {
            testimContent[currentActive].classList.add("inactive");
        }
        testimContent[slide].classList.add("active");
        testimDots[slide].classList.add("active");

        currentActive = currentSlide;

        clearTimeout(testimTimer);
        testimTimer = setTimeout(function() {
            playSlide(currentSlide += 1);
        }, testimSpeed)
    }

    testimLeftArrow.addEventListener("click", function() {
        playSlide(currentSlide -= 1);
    })

    testimRightArrow.addEventListener("click", function() {
        playSlide(currentSlide += 1);
    })

    for (var l = 0; l < testimDots.length; l++) {
        testimDots[l].addEventListener("click", function() {
            playSlide(currentSlide = testimDots.indexOf(this));
        })
    }

    playSlide(currentSlide);

    // keyboard shortcuts
    document.addEventListener("keyup", function(e) {
        switch (e.keyCode) {
            case 37:
                testimLeftArrow.click();
                break;

            case 39:
                testimRightArrow.click();
                break;

            case 39:
                testimRightArrow.click();
                break;

            default:
                break;
        }
    })

        testim.addEventListener("touchstart", function(e) {
                touchStartPos = e.changedTouches[0].clientX;
        })

        testim.addEventListener("touchend", function(e) {
                touchEndPos = e.changedTouches[0].clientX;

                touchPosDiff = touchStartPos - touchEndPos;

                console.log(touchPosDiff);
                console.log(touchStartPos);
                console.log(touchEndPos);


                if (touchPosDiff > 0 + ignoreTouch) {
                        testimLeftArrow.click();
                } else if (touchPosDiff < 0 - ignoreTouch) {
                        testimRightArrow.click();
                } else {
                    return;
                }

        })
}
</script>
