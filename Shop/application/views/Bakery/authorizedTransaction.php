<?php 
 
if(!count($main) > 0)
{
    $this->session->set_flashdata('success',"Your order is successfully placed but something went wrong with transaction.");
    redirect(base_url().'Orders', 'refresh'); 
}
$this->load->helper('authorize');
$getInfoData = getOrderInfo($main['orderId']);
if(count($getInfoData['address']) > 0)
{
    $setAddress = $getInfoData['address'][0]->houdinv_user_address_user_address;
    $setCity = $getInfoData['address'][0]->houdinv_user_address_city;
    $setZip = $getInfoData['address'][0]->houdinv_user_address_zip;
}
else
{
    $setAddress = 'no';
    $setCity = 'no';
    $setZip = 'no';
}
include(APPPATH.'libraries/authorize/vendor/autoload.php');
include_once(APPPATH.'libraries/authorize/constants/SampleCodeConstants.php');
 
    use net\authorize\api\contract\v1 as AnetAPI;
    use net\authorize\api\controller as AnetController;
    $amount = $getInfoData['order'][0]->houdinv_orders_total_Amount;
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantAuthentication->setName(\SampleCodeConstants::MERCHANT_LOGIN_ID);
    $merchantAuthentication->setTransactionKey(\SampleCodeConstants::MERCHANT_TRANSACTION_KEY);
     
    // Set the transaction's refId
    $refId = 'ref' . time();
    $invoiceNumber = "INV".rand(999999,100000);
    // Create the payment data for a credit card
    $creditCard = new AnetAPI\CreditCardType();
    $creditCard->setCardNumber($main['cardNumber']);
    $creditCard->setExpirationDate($main['expiryYear']."-".$main['expiryMonth']);
    $creditCard->setCardCode($main['securityPin']);
    
    // Add the payment data to a paymentType object
    $paymentOne = new AnetAPI\PaymentType();
    $paymentOne->setCreditCard($creditCard);
    
    // Create order information
    $order = new AnetAPI\OrderType();
    $order->setInvoiceNumber($invoiceNumber);
    $order->setDescription("Orders");
    
    // Set the customer's Bill To address
    $customerAddress = new AnetAPI\CustomerAddressType();
    $customerAddress->setFirstName($getInfoData['user'][0]->houdinv_user_name);
    $customerAddress->setLastName($getInfoData['user'][0]->houdinv_user_name);
    $customerAddress->setCompany($getInfoData['user'][0]->houdinv_user_name);
    $customerAddress->setAddress($setAddress);
    $customerAddress->setCity($setCity);
    // $customerAddress->setState("TX");
    $customerAddress->setZip($setZip);
    // $customerAddress->setCountry("USA");
    
    // Set the customer's identifying information
    $customerData = new AnetAPI\CustomerDataType();
    $customerData->setType("individual");
    $customerData->setId($getInfoData['order'][0]->houdinv_order_user_id);
    $customerData->setEmail($getInfoData['user'][0]->houdinv_user_email);
    
    // Add values for transaction settings
    $duplicateWindowSetting = new AnetAPI\SettingType();
    $duplicateWindowSetting->setSettingName("duplicateWindow");
    $duplicateWindowSetting->setSettingValue("60");
    
    // Add some merchant defined fields. These fields won't be stored with the transaction,
    // but will be echoed back in the response.
    $merchantDefinedField1 = new AnetAPI\UserFieldType();
    $merchantDefinedField1->setName("customerLoyaltyNum");
    $merchantDefinedField1->setValue("1128836273");
    
    $merchantDefinedField2 = new AnetAPI\UserFieldType();
    $merchantDefinedField2->setName("favoriteColor");
    $merchantDefinedField2->setValue("blue");
    
    // Create a TransactionRequestType object and add the previous objects to it
    $transactionRequestType = new AnetAPI\TransactionRequestType();
    $transactionRequestType->setTransactionType("authOnlyTransaction"); 
    $transactionRequestType->setAmount($amount);
    $transactionRequestType->setOrder($order);
    $transactionRequestType->setPayment($paymentOne);
    $transactionRequestType->setBillTo($customerAddress);
    $transactionRequestType->setCustomer($customerData);
    $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
    $transactionRequestType->addToUserFields($merchantDefinedField1);
    $transactionRequestType->addToUserFields($merchantDefinedField2);
    
    // Assemble the complete transaction request
    $request = new AnetAPI\CreateTransactionRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setTransactionRequest($transactionRequestType);
    
    // Create the controller and get the response
    $controller = new AnetController\CreateTransactionController($request);
    $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
    
    
    if ($response != null) {
        // Check to see if the API request was successfully received and acted upon
        if ($response->getMessages()->getResultCode() == "Ok") {
            // Since the API request was successful, look for a transaction response
            // and parse it to display the results of authorizing the card
            $tresponse = $response->getTransactionResponse();
        
            if ($tresponse != null && $tresponse->getMessages() != null) {
                $orderArray = array('houdinv_orders_total_paid'=>$getInfoData['order'][0]->houdinv_orders_total_Amount,'houdinv_orders_total_remaining'=>'0','houdinv_payment_status'=>'0');
                $setTransactionArray = array(
                    'houdinv_transaction_transaction_id'=>$tresponse->getTransId(),
                    'houdinv_transaction_type'=>'credit',
                    'houdinv_transaction_method'=>'online',
                    'houdinv_transaction_from'=>'authorize',
                    'houdinv_transaction_for'=>'order',
                    'houdinv_transaction_for_id'=>$main['orderId'],
                    'houdinv_transaction_amount'=>$getInfoData['order'][0]->houdinv_orders_total_Amount,
                    'houdinv_transaction_date'=>date('Y-m-d'),
                    'houdinv_transaction_status'=>'success'
                );
                $setOrderId = $main['orderId'];
                $getTransaction = updateTransaction($orderArray,$setOrderId,$setTransactionArray);
                $this->session->set_flashdata('success',$getTransaction['message']);
                redirect(base_url().'Orders', 'refresh'); 
                // echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
                // echo " Transaction Response Code: " . $tresponse->getResponseCode() . "\n";
                // echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "\n";
                // echo " Auth Code: " . $tresponse->getAuthCode() . "\n";
                // echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "\n";
            } else {
                $setTransactionId = rand(9999999999,1000000000);
                $orderArray = array();
                $setTransactionArray = array(
                    'houdinv_transaction_transaction_id'=>$setTransactionId,
                    'houdinv_transaction_type'=>'credit',
                    'houdinv_transaction_method'=>'online',
                    'houdinv_transaction_from'=>'authorize',
                    'houdinv_transaction_for'=>'order',
                    'houdinv_transaction_for_id'=>$main['orderId'],
                    'houdinv_transaction_amount'=>$getInfoData['order'][0]->houdinv_orders_total_Amount,
                    'houdinv_transaction_date'=>date('Y-m-d'),
                    'houdinv_transaction_status'=>'fail'
                );
                $setOrderId = $main['orderId'];
                $getTransaction = updateTransaction($orderArray,$setOrderId,$setTransactionArray);
                $this->session->set_flashdata('success',$getTransaction['message']);
                redirect(base_url().'Orders', 'refresh'); 

                // echo "Transaction Failed \n";
                // if ($tresponse->getErrors() != null) {
                //     echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
                //     echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                // }
            }
            // Or, print errors if the API request wasn't successful
        } else {
            // echo "Transaction Failed \n";
            // $tresponse = $response->getTransactionResponse();
        
            // if ($tresponse != null && $tresponse->getErrors() != null) {
            //     echo " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
            //     echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
            // } else {
            //     echo " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
            //     echo " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
            // }
            $orderArray = array();
            $setTransactionId = rand(9999999999,1000000000);
                $setTransactionArray = array(
                    'houdinv_transaction_transaction_id'=>$setTransactionId,
                    'houdinv_transaction_type'=>'credit',
                    'houdinv_transaction_method'=>'online',
                    'houdinv_transaction_from'=>'authorize',
                    'houdinv_transaction_for'=>'order',
                    'houdinv_transaction_for_id'=>$main['orderId'],
                    'houdinv_transaction_amount'=>$getInfoData['order'][0]->houdinv_orders_total_Amount,
                    'houdinv_transaction_date'=>date('Y-m-d'),
                    'houdinv_transaction_status'=>'fail'
                );
                $setOrderId = $main['orderId'];
                $getTransaction = updateTransaction($orderArray,$setOrderId,$setTransactionArray);
                $this->session->set_flashdata('error',$getTransaction['message']);
                redirect(base_url().'Orders', 'refresh'); 
        }      
    } else {
        $setTransactionId = rand(9999999999,1000000000);
                $orderArray = array();
                $setTransactionArray = array(
                    'houdinv_transaction_transaction_id'=>$setTransactionId,
                    'houdinv_transaction_type'=>'credit',
                    'houdinv_transaction_method'=>'online',
                    'houdinv_transaction_from'=>'authorize',
                    'houdinv_transaction_for'=>'order',
                    'houdinv_transaction_for_id'=>$main['orderId'],
                    'houdinv_transaction_amount'=>$getInfoData['order'][0]->houdinv_orders_total_Amount,
                    'houdinv_transaction_date'=>date('Y-m-d'),
                    'houdinv_transaction_status'=>'fail'
                );
                $getTransaction = updateTransaction($orderArray,$setOrderId,$setTransactionArray);
                $this->session->set_flashdata('error',$getTransaction['message']);
                redirect(base_url().'Orders', 'refresh'); 
    }

?>