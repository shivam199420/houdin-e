<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Calendar extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->auth_users->is_logged_in();
              $this->load->model('Workmodel');
              $this->load->model('Customermodel');
               $this->validation = array(
                  array("field"=>"customer_name",
                        "label"=>"customer name",
                        "rules"=>"required"),
                  
                  array("field"=>"customer_phone",
                        "label"=>"customer phone",
                        "rules"=>"required"),
                              
                  array("field"=>"customer_product",
                        "label"=>"product",
                        "rules"=>"required|integer"),
                        
                  array("field"=>"customer_quantity",
                        "label"=>"quantity",
                        "rules"=>"required|integer"),
                        
                  array("field"=>"Customer_pickup_type",
                        "label"=>"pickup type",
                        "rules"=>"required"), 
                        
                  array("field"=>"customer_address",
                        "label"=>"address",
                        "rules"=>"required"),
                        
                  array("field"=>"customer_payment_type",
                        "label"=>"payment type",
                        "rules"=>"required"),
                        
                  array("field"=>"customer_total_amount_paid",
                        "label"=>"total amount",
                        "rules"=>"required|numeric"), 
                        
            array("field"=>"Customer_Net_payble_amount",
                        "label"=>"Net payble amount",
                        "rules"=>"required|numeric"), 
                        
            array("field"=>"customer_discount",
                        "label"=>"discount",
                        "rules"=>"numeric"), 
                        
                  array("field"=>"order_delivery_date",
                        "label"=>"order delivery date",
                        "rules"=>"required"), 
                        
                  array("field"=>"order_delivery_time",
                        "label"=>"order delivery time",
                        "rules"=>"required")                                                    
                  );
    }
    public function index()
    {
            $defaultEvents = array();
            $data =  $this->Workmodel->AllWorkOrders();
            if($data)
            {
                foreach($data as $events)
                  {
                        $fg = strtotime($events['workorder']->houdinv_workorder_deliver_time);
                        $date = strtotime($events['workorder']->houdinv_workorder_deliver_date.",".date("h:i:s",$fg));
                        $defaultEvents[] =array(
                              "id"=>$events['workorder']->houdinv_workorder_id,
                              "orderId"=>$events['workorder']->houdinv_workorder_order_id,
					"title"=>'Hey!', 
                              "customer_id"=>$events['workorder']->houdinv_workorder_customer_name, 
					"name"=>$events['workorder']->houdinv_user_name,
					"contact"=>$events['workorder']->houdinv_user_contact,
					"product"=>$events['pname'],
					"quantity"=>$events['workorder']->houdinv_workorder_quantity,
					"coupon"=>$events['workorder']->houdinv_workorder_coupon_code, 
					"delDate"=>date("Y-m-d",$date),
					"delTime"=>$events['workorder']->houdinv_workorder_deliver_time,
					"delAddress"=>$events['workorder']->houdinv_user_address_user_address, 
					"order_pickuptype"=>$events['workorder']->houdinv_workorder_pickup,
					"paymentBy"=>$events['workorder']->houdinv_workorder_payment,
					"note"=>$events['workorder']->houdinv_workorder_note,  
					"totalAmount"=>$events['workorder']->houdinv_workorder_paid_amount,  
					"discountAmount"=>$events['workorder']->houdinv_workorder_coupon_discount,  
					"netPayAmount"=>$events['workorder']->houdinv_workorder_net_pay,  
                              "start"=>date("Y-m-d h:i:s",$date),
                              "coupon_id"=>$events['workorder']->houdinv_coupons_id,
                              "paymentStatus"=>$events['workorder']->houdinv_workorder_payment_status,
                              "className"=>'bg-purple');
                  }   
            }
            $response['Events'] = json_encode($defaultEvents);
            $response['getCustomerData'] = $this->Customermodel->fetchCustomerRecord();
            $response['getProductData'] = $this->Workmodel->FetchProduct();
            $this->load->view('calendar',$response);
    }
      public function WorkorderUpdate()
      {
            $gh =$this->input->post('savedata');
            if($gh)
            {
                  if($this->input->post('setPaymentStatus') == 1)
                  {
                        $setTransactionId = "TXN".rand(99999999,10000000);
                        $setTransactionArray = array('houdinv_transaction_transaction_id'=>$setTransactionId,
                        'houdinv_transaction_type'=>'cash','houdinv_transaction_from'=>'cash',
                        'houdinv_transaction_for'=>"Work Order",'houdinv_transaction_for_id'=>$this->input->post("edit_id"),'houdinv_transaction_amount'=>$this->input->post('Customer_Net_payble_amount'),'houdinv_transaction_date'=>date('Y-m-d'),'houdinv_transaction_status'=>'success');
                       $order =  $this->Workmodel->UpdateWorkOrder($setTransactionArray,$this->input->post('setorderid'),$this->input->post('edit_id'));  
                       if($order['message'] == 'yes')
                       {
                             $this->session->set_flashdata('success',"Payment Status updated successfully");
                             redirect(base_url().'Calendar', 'refresh');      
                       }   
                       else if($order['message'] == 'order')
                       {
                             $this->session->set_flashdata('message_name',"Transaction updated successfully. Something went wrong during order status updation");
                             redirect(base_url().'Calendar', 'refresh');     
                       } 
                       else
                       {
                              $this->session->set_flashdata('message_name',"Something went wrong. Please try again");
                              redirect(base_url().'Calendar', 'refresh');     
                       }
                  }     
                  else
                  {
                        $this->session->set_flashdata('message_name',"Please choose payment status to paid");
                        redirect(base_url().'Calendar', 'refresh');
                  } 
            }   
      }
      public function WorkorderDelete()
      {
            $gh = $this->input->post('edit_id');
            $getOrderID = $this->input->post('order_id');
            if($gh)
            {
                  $order =  $this->Workmodel->DeleteWorkOrder($gh,$getOrderID);  
                  echo "yes";
            }
      }
}
