-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 10, 2020 at 08:23 AM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `warrdelnimit`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_business_category`
--

CREATE TABLE `app_business_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `status` int(1) NOT NULL COMMENT '[1=&amp;gt;Yes,0=&amp;gt;No]',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_business_category`
--

INSERT INTO `app_business_category` (`id`, `name`, `description`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(3, 'Test Category', NULL, 1, '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_admin_contact`
--

CREATE TABLE `houdin_admin_contact` (
  `houdin_admin_contact_id` int(11) NOT NULL,
  `houdin_admin_contact_name` varchar(100) NOT NULL,
  `houdin_admin_contact_email` varchar(50) NOT NULL,
  `houdin_admin_contact_phone` varchar(20) NOT NULL,
  `houdin_admin_contact_message` text NOT NULL,
  `houdin_admin_contact_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_admin_department`
--

CREATE TABLE `houdin_admin_department` (
  `houdin_admin_department_id` int(11) NOT NULL,
  `houdin_admin_department_name` varchar(50) NOT NULL,
  `houdin_admin_department_access` text,
  `houdin_admin_department_status` enum('active','deactive') NOT NULL,
  `houdin_admin_department_created_date` varchar(30) DEFAULT NULL,
  `houdin_admin_department_updated_at` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_admin_department`
--

INSERT INTO `houdin_admin_department` (`houdin_admin_department_id`, `houdin_admin_department_name`, `houdin_admin_department_access`, `houdin_admin_department_status`, `houdin_admin_department_created_date`, `houdin_admin_department_updated_at`) VALUES
(1, 'Operational Manager', 'shop,setting,admin', 'active', '1576886400', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_admin_logo`
--

CREATE TABLE `houdin_admin_logo` (
  `houdin_admin_logo_id` int(11) NOT NULL,
  `houdin_admin_logo_image` varchar(100) NOT NULL,
  `houdin_admin_logo_created_at` varchar(30) NOT NULL,
  `houdin_admin_logo_updated_at` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_admin_logo`
--

INSERT INTO `houdin_admin_logo` (`houdin_admin_logo_id`, `houdin_admin_logo_image`, `houdin_admin_logo_created_at`, `houdin_admin_logo_updated_at`) VALUES
(1, '519432Logo_W.png', '1528701454', '1535707300');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_admin_transaction`
--

CREATE TABLE `houdin_admin_transaction` (
  `houdin_admin_transaction_id` int(11) NOT NULL,
  `houdin_admin_transaction_transaction_id` varchar(30) NOT NULL,
  `houdin_admin_transaction_from` enum('cash','payumoney') NOT NULL,
  `houdin_admin_transaction_type` enum('credit','debit') NOT NULL,
  `houdin_admin_transaction_for` enum('package') NOT NULL,
  `houdin_admin_transaction_for_id` int(11) NOT NULL DEFAULT '0',
  `houdin_admin_transaction_amount` float NOT NULL,
  `houdin_admin_transaction_status` enum('success','failure') NOT NULL,
  `houdin_admin_transaction_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_admin_transaction`
--

INSERT INTO `houdin_admin_transaction` (`houdin_admin_transaction_id`, `houdin_admin_transaction_transaction_id`, `houdin_admin_transaction_from`, `houdin_admin_transaction_type`, `houdin_admin_transaction_for`, `houdin_admin_transaction_for_id`, `houdin_admin_transaction_amount`, `houdin_admin_transaction_status`, `houdin_admin_transaction_date`) VALUES
(1, 'TXN23999505', 'cash', 'credit', 'package', 10, 85000, 'success', '2019-01-23'),
(2, 'TXN85555788', 'cash', 'credit', 'package', 16, 85000, 'success', '2019-01-23'),
(3, 'Txn69784408', 'payumoney', 'credit', 'package', 1, 60000, 'success', '2019-01-31'),
(4, 'TXN17444777', 'cash', 'credit', 'package', 1, 120000, 'success', '2019-10-02');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_admin_users`
--

CREATE TABLE `houdin_admin_users` (
  `houdin_admin_user_id` int(11) NOT NULL,
  `houdin_admin_user_name` varchar(50) NOT NULL,
  `houdin_admin_user_email` varchar(50) NOT NULL,
  `houdin_admin_user_phone` varchar(20) NOT NULL,
  `houdin_admin_user_password` varchar(100) NOT NULL,
  `houdin_admin_user_role_id` int(11) NOT NULL,
  `houdin_admin_user_department` int(11) NOT NULL DEFAULT '0',
  `houdin_admin_user_status` enum('deactive','active','block') NOT NULL,
  `houdin_admin_user_status_change_text` tinytext,
  `houdin_admin_user_created_at` varchar(30) DEFAULT NULL,
  `houdin_admin_user_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_admin_users`
--

INSERT INTO `houdin_admin_users` (`houdin_admin_user_id`, `houdin_admin_user_name`, `houdin_admin_user_email`, `houdin_admin_user_phone`, `houdin_admin_user_password`, `houdin_admin_user_role_id`, `houdin_admin_user_department`, `houdin_admin_user_status`, `houdin_admin_user_status_change_text`, `houdin_admin_user_created_at`, `houdin_admin_user_updated_at`) VALUES
(1, 'Hawkscode', 'nimit@warrdel.com', '+919672247772', '$2y$10$fjLX01oXD1IxPtY6Uv2RFOAVsRi3m8HXPjgORMFB5HmU3R5oJFpwa', 0, 0, 'active', NULL, NULL, '1583298624'),
(3, 'shivam singh sengar', 'shivam199420@gmail.com', '07790870946', '$2y$10$Oy5We/uTT6CJIeIra1D.DOKKY3Yl7Sv2.i1kDb7JD1G90nZFAvlVG', 1, 1, 'active', NULL, '1576886400', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_admin_user_second_auth`
--

CREATE TABLE `houdin_admin_user_second_auth` (
  `houdin_second_auth_id` int(11) NOT NULL,
  `houdin_second_auth_email` varchar(50) NOT NULL,
  `houdin_second_auth_token` int(5) NOT NULL,
  `houdin_second_auth_created_date` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_business_categories`
--

CREATE TABLE `houdin_business_categories` (
  `houdin_business_category_id` int(10) UNSIGNED NOT NULL,
  `houdin_business_category_name` varchar(250) DEFAULT NULL,
  `houdin_business_category_desc` tinytext,
  `houdin_business_categories_status` enum('deactive','active','block') NOT NULL,
  `houdin_business_category_created_at` varchar(150) DEFAULT NULL,
  `houdin_business_category_updated_at` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_business_categories`
--

INSERT INTO `houdin_business_categories` (`houdin_business_category_id`, `houdin_business_category_name`, `houdin_business_category_desc`, `houdin_business_categories_status`, `houdin_business_category_created_at`, `houdin_business_category_updated_at`) VALUES
(1, 'Departmental stores', 'Departmental stores', 'active', '1528442159', '1528971673'),
(3, 'Apparels', 'Apparels', 'active', '1528442159', '1528971682'),
(4, 'Stone and tiles', 'Stone and tiles', 'active', '1528442159', '1528971695'),
(6, 'Optical', 'Optical', 'active', '1528971719', '1528971719'),
(7, 'Work order stores (ex: bakery, pottery, etc)', 'Work order stores (ex: bakery, pottery, etc)', 'active', '1528971728', '1528971728'),
(8, 'Other general stores', 'Other general stores', 'active', '1528971739', '1528971739'),
(9, 'Electronics and Electrical Appliances', 'Electronics and electrical appliances', 'active', '1532151351', '1575781102');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_countries`
--

CREATE TABLE `houdin_countries` (
  `houdin_country_id` int(11) NOT NULL,
  `houdin_country_name` varchar(150) DEFAULT NULL,
  `houdin_country_status` enum('deactive','active','block') NOT NULL,
  `houdin_country_created_at` varchar(150) DEFAULT NULL,
  `houdin_country_updated_at` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_countries`
--

INSERT INTO `houdin_countries` (`houdin_country_id`, `houdin_country_name`, `houdin_country_status`, `houdin_country_created_at`, `houdin_country_updated_at`) VALUES
(1, 'Afghanistan', 'active', '0000-00-00 00:00:00', '1528767295'),
(2, 'Armenia', 'active', '1528451453', '1528451453'),
(4, 'Afghanistan', 'deactive', '1528453639', '1528453639'),
(5, 'Lithuania', 'active', '1528767286', '1529057069'),
(6, 'Albania', 'active', '1528767308', '1528767308'),
(7, 'India', 'active', '1544785903', '1544785903');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_coupons`
--

CREATE TABLE `houdin_coupons` (
  `houdin_coupon_id` int(10) UNSIGNED NOT NULL,
  `houdin_coupon_code` varchar(150) NOT NULL,
  `houdin_coupon_discount` float NOT NULL,
  `houdin_coupon_limit` int(11) NOT NULL,
  `houdin_coupon_reamining` int(11) NOT NULL,
  `houdin_coupon_type` int(11) NOT NULL,
  `houdin_coupon_status` enum('deactive','active','block') NOT NULL,
  `houdin_coupon_valid_from` varchar(255) NOT NULL,
  `houdin_coupon_valid_to` varchar(255) NOT NULL,
  `houdin_coupon_created_at` varchar(255) DEFAULT NULL,
  `houdin_coupon_updated_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_currencies`
--

CREATE TABLE `houdin_currencies` (
  `houdin_currency_id` int(11) NOT NULL,
  `houdin_currency_name` varchar(150) DEFAULT NULL,
  `houdin_currency_sumbol` varchar(150) DEFAULT NULL,
  `houdin_currency_created_at` timestamp NULL DEFAULT NULL,
  `houdin_currency_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_deleted_users`
--

CREATE TABLE `houdin_deleted_users` (
  `houdin_deleted_user_id` int(10) UNSIGNED NOT NULL,
  `houdin_deleted_user_email` varchar(150) DEFAULT NULL,
  `houdin_deleted_user_by` varchar(150) DEFAULT NULL COMMENT 'By Admin, Self Delete',
  `houdin_deleted_user_reason` tinytext,
  `houdin_deleted_user_delete_status` tinyint(4) DEFAULT '0' COMMENT '0 Tmp Delete, 1 Permanent Delete',
  `houdin_deleted_user_created_at` timestamp NULL DEFAULT NULL,
  `houdin_deleted_user_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_email_package`
--

CREATE TABLE `houdin_email_package` (
  `houdin_email_package_id` int(11) NOT NULL,
  `houdin_email_package_name` varchar(50) NOT NULL,
  `houdin_email_package_price` int(11) NOT NULL,
  `houdin_email_package_email_count` int(11) NOT NULL,
  `houdin_email_package_status` enum('active','deactive') NOT NULL,
  `houdin_email_package_created_at` varchar(30) DEFAULT NULL,
  `houdin_email_package_modified_at` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_email_package`
--

INSERT INTO `houdin_email_package` (`houdin_email_package_id`, `houdin_email_package_name`, `houdin_email_package_price`, `houdin_email_package_email_count`, `houdin_email_package_status`, `houdin_email_package_created_at`, `houdin_email_package_modified_at`) VALUES
(1, 'Package 1', 240, 240, 'active', '1530769498', '1530771541');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_forgot_password`
--

CREATE TABLE `houdin_forgot_password` (
  `houdin_forgot_password_email` varchar(150) NOT NULL,
  `houdin_forgot_password_token` varchar(50) NOT NULL,
  `houdin_forgot_password_created_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_languages`
--

CREATE TABLE `houdin_languages` (
  `houdin_language_id` int(10) UNSIGNED NOT NULL,
  `houdin_language_name` varchar(30) DEFAULT NULL,
  `houdin_language_name_value` varchar(30) NOT NULL,
  `houdin_language_status` enum('active','deactive') NOT NULL,
  `houdin_language_created_at` varchar(30) DEFAULT NULL,
  `houdin_language_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_languages`
--

INSERT INTO `houdin_languages` (`houdin_language_id`, `houdin_language_name`, `houdin_language_name_value`, `houdin_language_status`, `houdin_language_created_at`, `houdin_language_updated_at`) VALUES
(1, 'bg', 'Bulgarian', 'active', '1531993981', NULL),
(2, 'eu', 'Basque', 'active', '1531993989', NULL),
(3, 'en', 'English', 'active', '1531994131', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_packages`
--

CREATE TABLE `houdin_packages` (
  `houdin_package_id` int(10) UNSIGNED NOT NULL,
  `houdin_package_name` varchar(50) NOT NULL,
  `houdin_package_price` float NOT NULL,
  `houdin_package_product` int(11) NOT NULL DEFAULT '0',
  `houdin_package_staff_accounts` int(11) NOT NULL DEFAULT '0',
  `houdin_package_stock_location` int(11) NOT NULL DEFAULT '0',
  `houdin_package_app_access` enum('limited','full') NOT NULL,
  `houdin_package_order_pm` int(11) NOT NULL DEFAULT '0',
  `houdin_package_sms` int(11) NOT NULL DEFAULT '0',
  `houdin_package_email` int(11) NOT NULL DEFAULT '0',
  `houdin_package_accounting` enum('yes','no') NOT NULL,
  `houdin_package_pos` enum('yes','no') NOT NULL,
  `houdin_package_delivery` enum('yes','no') NOT NULL,
  `houdin_package_payment_gateway` enum('yes','no') NOT NULL,
  `houdin_package_templates` int(11) NOT NULL DEFAULT '0',
  `houdin_package_countries` longblob NOT NULL,
  `houdin_package_description` text NOT NULL,
  `houdin_package_status` enum('active','deactive') NOT NULL,
  `houdin_package_created_at` varchar(30) DEFAULT NULL,
  `houdin_package_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_packages`
--

INSERT INTO `houdin_packages` (`houdin_package_id`, `houdin_package_name`, `houdin_package_price`, `houdin_package_product`, `houdin_package_staff_accounts`, `houdin_package_stock_location`, `houdin_package_app_access`, `houdin_package_order_pm`, `houdin_package_sms`, `houdin_package_email`, `houdin_package_accounting`, `houdin_package_pos`, `houdin_package_delivery`, `houdin_package_payment_gateway`, `houdin_package_templates`, `houdin_package_countries`, `houdin_package_description`, `houdin_package_status`, `houdin_package_created_at`, `houdin_package_updated_at`) VALUES
(1, 'Standard', 85000, 10000, 5, 2, 'limited', 20000, 500, 500, 'yes', 'yes', 'yes', 'yes', 5, '', '<p>dsfsdfs</p>', 'active', '1556150400', NULL),
(2, 'Professional', 120000, 25000, 15, 4, 'full', 50000, 1000, 1000, 'yes', 'yes', 'yes', 'yes', 15, '', '<p>dfs</p>', 'active', '1556150400', NULL),
(5, 'Tailored', 150000, 100000, 100000, 100000, 'full', 100000, 100000, 100000, 'yes', 'yes', 'yes', 'yes', 100000, '', '<p>Houdin-e tailored just for you. Our team will contact to understand your requirements and deliver you the perfect solution for your business.</p>', 'active', '1556150400', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_package_plugins`
--

CREATE TABLE `houdin_package_plugins` (
  `houdin_package_plugin_id` int(10) UNSIGNED NOT NULL,
  `houdin_package_plugin_plu_id` varchar(30) DEFAULT NULL,
  `houdin_package_plugin_pkg_id` int(10) UNSIGNED DEFAULT NULL,
  `houdin_package_plugin_created_at` varchar(30) DEFAULT NULL,
  `houdin_package_plugin_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_package_plugins`
--

INSERT INTO `houdin_package_plugins` (`houdin_package_plugin_id`, `houdin_package_plugin_plu_id`, `houdin_package_plugin_pkg_id`, `houdin_package_plugin_created_at`, `houdin_package_plugin_updated_at`) VALUES
(1, '1,2,3', 1, '1536063856', NULL),
(2, '4,5,6', 2, '1536063882', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_payumoney`
--

CREATE TABLE `houdin_payumoney` (
  `houdin_payumoney` int(11) NOT NULL,
  `houdin_payumoney_key` varchar(100) NOT NULL,
  `houdin_payumoney_salt` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_payumoney`
--

INSERT INTO `houdin_payumoney` (`houdin_payumoney`, `houdin_payumoney_key`, `houdin_payumoney_salt`) VALUES
(1, 'MBVIetjo', 'U3YohhxRvu');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_plugins`
--

CREATE TABLE `houdin_plugins` (
  `houdin_plugin_id` int(10) UNSIGNED NOT NULL,
  `houdin_plugin_name` varchar(150) NOT NULL,
  `houdin_plugin_description` text NOT NULL,
  `houdin_plugin_countries` longblob NOT NULL,
  `houdin_plugin_image` varchar(100) NOT NULL,
  `houdin_plugin_active_status` enum('deactive','active') DEFAULT NULL,
  `houdin_plugin_created_at` varchar(30) DEFAULT NULL,
  `houdin_plugin_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_plugins`
--

INSERT INTO `houdin_plugins` (`houdin_plugin_id`, `houdin_plugin_name`, `houdin_plugin_description`, `houdin_plugin_countries`, `houdin_plugin_image`, `houdin_plugin_active_status`, `houdin_plugin_created_at`, `houdin_plugin_updated_at`) VALUES
(1, '250 SMS', '<p>dfdfg</p>', '', '', 'active', '1536062927', '1536062972'),
(2, '250 email', '<p>250 email</p>', '', '', 'active', '1536062984', '1536063118'),
(3, '6 month subscription', '<p>6 month subscription</p>', '', '', 'active', '1536063003', '1536063172'),
(4, '12 month subscription', '<p>12 month subscription</p>', '', '', 'active', '1536063017', '1536063182'),
(5, '24 month subscription', '<p>24 month subscription</p>', '', '', 'active', '1536063030', '1536063424'),
(6, '500 SMS', '<p>500 SMS</p>', '', '', 'active', '1536063045', '1536063437'),
(7, '1000 SMS', '<p>1000 SMS</p>', '', '', 'active', '1536063057', '1536063447'),
(8, '500 email', '<p>500 email</p>', '', '', 'active', '1536063071', '1536063458'),
(9, '1000 email', '<p>1000 email</p>', '', '', 'active', '1536063085', '1536063472');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_products`
--

CREATE TABLE `houdin_products` (
  `houdin_products_id` int(11) NOT NULL,
  `houdin_products_category` int(11) NOT NULL,
  `houdin_products_title` varchar(255) NOT NULL,
  `houdin_products_short_desc` text NOT NULL,
  `houdin_products_desc` longblob NOT NULL,
  `houdin_products_type` int(11) NOT NULL,
  `houdin_products_show_on` varchar(50) NOT NULL,
  `houdin_products_attributes` text NOT NULL,
  `houdin_products_created_date` varchar(255) NOT NULL,
  `houdin_products_updated_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_products_variants`
--

CREATE TABLE `houdin_products_variants` (
  `houdin_products_variants_id` int(11) NOT NULL,
  `houdin_products_variants_name` varchar(100) NOT NULL,
  `houdin_products_variants_value` varchar(100) NOT NULL,
  `houdin_products_variants_barcode` varchar(255) NOT NULL,
  `houdin_products_variants_prices` varchar(255) NOT NULL,
  `houdin_products_variants_sku` varchar(100) NOT NULL,
  `houdin_products_variants_stock` int(11) NOT NULL,
  `houdin_products_variants_minimum_order` int(11) NOT NULL,
  `houdin_products_variants_sort_order` int(11) NOT NULL,
  `houdin_products_variants_featured` int(1) NOT NULL,
  `houdin_products_variants_payment_options` varchar(255) NOT NULL,
  `houdin_products_variants_active_status` int(1) NOT NULL,
  `houdin_products_variants_product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_product_attributers`
--

CREATE TABLE `houdin_product_attributers` (
  `houdin_product_att_id` int(10) UNSIGNED NOT NULL,
  `houdin_product_att_name` varchar(150) NOT NULL,
  `houdin_product_att_desc` tinytext,
  `houdin_product_att_unit` varchar(20) DEFAULT NULL,
  `houdin_product_att_created_at` timestamp NULL DEFAULT NULL,
  `houdin_product_att_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_product_types`
--

CREATE TABLE `houdin_product_types` (
  `houdin_product_type_id` int(10) UNSIGNED NOT NULL,
  `houdin_product_type_businness_category` int(10) UNSIGNED NOT NULL,
  `houdin_product_type_name` varchar(150) NOT NULL,
  `houdin_product_type_description` tinytext,
  `houdin_product_type_created_at` timestamp NULL DEFAULT NULL,
  `houdin_product_type_updatedat` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_product_type_attributes`
--

CREATE TABLE `houdin_product_type_attributes` (
  `houdin_product_type_attribute_id` int(10) UNSIGNED NOT NULL,
  `houdin_product_type_attribute_type_id` int(10) UNSIGNED NOT NULL,
  `houdin_product_type_attribute_att_id` int(10) UNSIGNED NOT NULL,
  `houdin_product_type_attribute_created_at` timestamp NULL DEFAULT NULL,
  `houdin_product_type_attribute_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_roles`
--

CREATE TABLE `houdin_roles` (
  `houdin_role_id` int(11) NOT NULL,
  `houdin_role_name` varchar(150) NOT NULL,
  `houdin_role_description` varchar(200) DEFAULT NULL,
  `houdin_role_permissions` varchar(150) DEFAULT NULL,
  `houdin_role_created_at` timestamp NULL DEFAULT NULL,
  `houdin_role_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_sendgrid`
--

CREATE TABLE `houdin_sendgrid` (
  `houdin_sendgrid_id` int(11) NOT NULL,
  `houdin_sendgrid_username` varchar(100) NOT NULL,
  `houdin_sendgrid_password` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_sendgrid`
--

INSERT INTO `houdin_sendgrid` (`houdin_sendgrid_id`, `houdin_sendgrid_username`, `houdin_sendgrid_password`) VALUES
(1, 'nimits226', 'Nimit@123');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_sms_package`
--

CREATE TABLE `houdin_sms_package` (
  `houdin_sms_package_id` int(11) NOT NULL,
  `houdin_sms_package_name` varchar(50) NOT NULL,
  `houdin_sms_package_price` int(11) NOT NULL,
  `houdin_sms_package_sms_count` int(11) NOT NULL,
  `houdin_sms_package_status` enum('active','deactive') NOT NULL,
  `houdin_sms_package_created_at` varchar(30) NOT NULL,
  `houdin_sms_package_modified_at` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_sms_package`
--

INSERT INTO `houdin_sms_package` (`houdin_sms_package_id`, `houdin_sms_package_name`, `houdin_sms_package_price`, `houdin_sms_package_sms_count`, `houdin_sms_package_status`, `houdin_sms_package_created_at`, `houdin_sms_package_modified_at`) VALUES
(1, 'Package 1', 240, 240, 'active', '1530705600', '1530669123'),
(3, 'Package 2', 240, 240, 'active', '1530768239', '');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_tax`
--

CREATE TABLE `houdin_tax` (
  `houdin_tax_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_templates`
--

CREATE TABLE `houdin_templates` (
  `houdin_template_id` int(11) NOT NULL,
  `houdin_template_file` varchar(300) DEFAULT NULL COMMENT 'Location of template folder',
  `houdin_template_name` varchar(50) NOT NULL,
  `houdin_template_category` int(11) NOT NULL,
  `houdin_template_thumbnail` varchar(100) NOT NULL,
  `houdin_template_description` text NOT NULL,
  `houdin_template_status` enum('active','deactive') NOT NULL,
  `houdin_template_created_at` varchar(30) DEFAULT NULL,
  `houdin_template_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_templates`
--

INSERT INTO `houdin_templates` (`houdin_template_id`, `houdin_template_file`, `houdin_template_name`, `houdin_template_category`, `houdin_template_thumbnail`, `houdin_template_description`, `houdin_template_status`, `houdin_template_created_at`, `houdin_template_updated_at`) VALUES
(4, 'Departmental/home', 'Departmental', 1, '660789departmental.png', '<p>safsdfs</p>', 'active', '1531995484', NULL),
(5, 'General/home', 'General', 8, '662779departmental.png', '<p>General</p>', 'active', '1531962189', NULL),
(6, 'Apparels/home', 'Apparels Store', 3, '655904departmental.png', '<p>Apparels Store</p>', 'active', '1532080767', '1532080800'),
(7, 'Bakery/home', 'Bakery Store', 7, '634069departmental.png', '<p>Bakery</p>', 'active', '1532080832', NULL),
(8, 'Optical/home', 'Optical', 6, '167855departmental.png', '<p>Optical</p>', 'active', '1532080858', NULL),
(9, 'Stones/home', 'Stones', 4, '495814departmental.png', '<p>Stones</p>', 'active', '1532080878', NULL),
(10, 'Electronic/home', 'Electronics', 9, '754164departmental.png', '<p>Electronics</p>', 'active', '1532151396', NULL),
(11, 'Deepawali', 'Deepawali Offer', 1, '4435402.jpg', '<p>This is Deepawali offer</p>', 'deactive', '1539327181', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_twilio`
--

CREATE TABLE `houdin_twilio` (
  `houdin_twilio_id` int(11) NOT NULL,
  `houdin_twilio_sid` varchar(100) NOT NULL,
  `houdin_twilio_token` varchar(100) NOT NULL,
  `houdin_twilio_number` varchar(30) NOT NULL,
  `houdin_twilio_created_at` varchar(30) DEFAULT NULL,
  `houdin_twilio_modified_at` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_twilio`
--

INSERT INTO `houdin_twilio` (`houdin_twilio_id`, `houdin_twilio_sid`, `houdin_twilio_token`, `houdin_twilio_number`, `houdin_twilio_created_at`, `houdin_twilio_modified_at`) VALUES
(1, 'AC6322c8caf48fbf1d810baa191d2621ea', 'c466708ba972f98c2b66c137c18b5101', '+18162932951', '1530775414', '1540184594');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_users`
--

CREATE TABLE `houdin_users` (
  `houdin_user_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_name` varchar(150) NOT NULL,
  `houdin_user_email` varchar(150) DEFAULT NULL,
  `houdin_user_password` varchar(100) NOT NULL,
  `houdin_user_password_salt` varchar(100) NOT NULL,
  `houdin_users_package_id` int(11) DEFAULT '0',
  `houdin_users_package_expiry` date DEFAULT NULL,
  `houdin_user_reset_token` varchar(255) NOT NULL,
  `houdin_user_contact` varchar(15) NOT NULL,
  `houdin_user_address` text NOT NULL,
  `houdin_user_city` varchar(30) DEFAULT NULL,
  `houdin_user_country` int(11) NOT NULL,
  `houdin_users_currency` enum('INR','USD','AUD','EURO','POUND') NOT NULL,
  `houdin_user_registration_status` int(1) NOT NULL,
  `houdin_user_is_verified` tinyint(4) NOT NULL,
  `houdin_user_is_active` enum('active','deactive','block') NOT NULL,
  `houdin_user_created_at` varchar(30) DEFAULT NULL,
  `houdin_user_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_users`
--

INSERT INTO `houdin_users` (`houdin_user_id`, `houdin_user_name`, `houdin_user_email`, `houdin_user_password`, `houdin_user_password_salt`, `houdin_users_package_id`, `houdin_users_package_expiry`, `houdin_user_reset_token`, `houdin_user_contact`, `houdin_user_address`, `houdin_user_city`, `houdin_user_country`, `houdin_users_currency`, `houdin_user_registration_status`, `houdin_user_is_verified`, `houdin_user_is_active`, `houdin_user_created_at`, `houdin_user_updated_at`) VALUES
(1, 'shivam sengar', 'hawkscodeteam@gmail.com', '$2y$10$vSJUd4jtZsvJJCXQTrMxSOV9FnN2EqBbeoV.AVKqahBBqTE4smyxm', '$2y$10$vSJUd4jtZsvJJCXQTrMxSO4kf970B730YtNB2jXRK7h8m5ixhPhIG', 2, '2019-07-31', '', '+917790870947', '602 603 kailash tower lal kothi', NULL, 7, 'INR', 1, 1, 'active', '1545648039', '1545648093'),
(2, 'deepsharma909@gmail.com', 'deepsharma907@gmail.com', '$2y$10$abZPzuSCWWsVXx8RwnbZoOXB4fWGjMOLFvV8YuiSwQPdIrfGWO6di', '$2y$10$abZPzuSCWWsVXx8RwnbZoOvxXafHvMGQkdRxrqRh3x5mW.qgTGvUK', 2, '2019-01-13', '', '+9178915920090', '58 B Ganesh Colony Bas Badanpura, Australian Capital Territory, Vlc\r\nAustralian Capital Territory', NULL, 7, 'INR', 1, 1, 'active', '1545654307', '1551139200'),
(4, 'Aditya Agarwal', 'aditya@arthvcfo.com', '$2y$10$6Jg2r1ISaBZ6J.OvG8cvj.g5YxSZShh9nY.pYdqJbDcJDRW8Oqnde', '$2y$10$6Jg2r1ISaBZ6J.OvG8cvj.7WFiy7cg.FjkEq2NVCT79A1A.KcQEGS', 2, '2019-01-15', '', '+918947891771', 'Shyam nagar jaipur', NULL, 7, 'INR', 1, 1, 'active', '1545792423', '1545792550'),
(10, 'Nimit Sharma', 'nimit@warrdel.com', '$2y$10$qWN.aGJcOQptyfcJ.FAAYu6AmMuzR3VKiVf940LxUopm99VviK2M2', '$2y$10$qWN.aGJcOQptyfcJ.FAAYun4abuqBWWizbAn9t9KnU1rtMyoffynS', 2, '2020-01-16', '', '+919672247772', 'Jaipur', NULL, 7, 'INR', 1, 1, 'active', '1545873403', '1545873453'),
(11, 'Shivam Sengar', 'shivam199420@gmail.com', '$2y$10$Id/xXiMim/GmVIzgwtnRYO6c51SjISHFA./KBdyT.o2/eB73vujCy', '$2y$10$Id/xXiMim/GmVIzgwtnRYOHQGXmFG8Oi2ANVI4EN6ZDvVR2zS0222', 2, '2019-01-17', '', '+918218347967', 'dayal nagar colony behind hotel triple t', NULL, 7, 'INR', 1, 1, 'active', '1545998596', '1545998644'),
(13, 'Shivam Sengar', 'insta@gmail.com', '$2y$10$0i9FcGMvVDMUObStFO9N2eHs/SsBJ8AC1MzmuVoAv841H45zc9uC2', '$2y$10$0i9FcGMvVDMUObStFO9N2epAD9Thp1w6wWDeYCZuVUkVGmv27utJ6', 2, '2019-01-22', '', '+917790870948', '602 603 kailash tower', NULL, 7, 'INR', 1, 1, 'active', '1546411135', '1546411193'),
(14, 'Ram Taparia', NULL, '', '', 2, '2019-01-22', '', '+9166243100', '', NULL, 0, 'INR', 0, 0, 'active', '1546393137', NULL),
(15, 'RTM sol', NULL, '', '', 2, '2019-01-30', '', '+919166243100', '', NULL, 0, 'INR', 0, 0, 'active', '1547088085', NULL),
(16, 'Ram Taparia', 'taparia.rm@gmail.com', '$2y$10$C17PTrrash3NmrGCDdfrq.SH7CFPG2sC2k.j4oE4ldNva.uzopchS', '$2y$10$C17PTrrash3NmrGCDdfrq.DVKKQGe4aJ6VjAS.bk5yqSRyn/h1.TW', 2, '2020-01-22', '', '+919024110415', '', NULL, 7, 'INR', 1, 1, 'active', '1546394021', '1546394105'),
(17, 'testacc', NULL, '', '', 2, '2019-01-27', '', '+9176788998789', '', NULL, 0, 'INR', 0, 1, 'active', '1546865112', '1546865133'),
(19, 'Gauri Mundra', 'megasa.llp@gmail.com', '$2y$10$sx5ZtUdX4cPVFgdBlorBz.uKPrXB4iYZK8uv.gvrBAv2XasHftS4S', '$2y$10$sx5ZtUdX4cPVFgdBlorBz.A0l3ElEU8uWIuYHhPEjLFhkHAAaBuom', 1, '2020-01-31', '', '+919828836723', 'A-6', NULL, 7, 'INR', 1, 1, 'active', '1546913733', '1546914299'),
(23, 'Shivam Singh Sengar', 'shivam.hawkscode@gmail.com', '$2y$10$o7qz.7UJP1Oat0D5lXT0uOi27YuigSrPYfBaa4vulCBSckzLptqsW', '$2y$10$o7qz.7UJP1Oat0D5lXT0uOi27YuigSrPYfBaa4vulCBSckzLptqsW', 2, '2020-01-09', '', '+917790870946', '<p>602 603 kailash tower Lal kothi</p>', 'Jaipur', 7, 'INR', 1, 1, 'active', '1547017894', NULL),
(24, 'Aadi Mahajani', NULL, '', '', 2, '2019-04-07', '', '+919929555644', '', NULL, 0, 'INR', 0, 0, 'active', '1552902354', NULL),
(25, 'Aadi Mahajani', 'aadi28x6@gmail.com', '$2y$10$suZ8Y0t39uKOlktn9J1l8O.HX9Cm5OYCWgG4UuyCEG/zwR6fpWZUe', '$2y$10$suZ8Y0t39uKOlktn9J1l8O.HX9Cm5OYCWgG4UuyCEG/zwR6fpWZUe', 2, '2020-03-17', '', '9929555644', '<p>Bajaj Nagar Enclave</p>', 'Jaipur', 7, 'USD', 1, 1, 'active', '1552902605', NULL),
(26, 'Nupur Mittal', 'nupurm92@yahoo.in', '$2y$10$RRSKtNPOIhmx/q.Jc9AK7.BQqpjd8LUp/tLmDRZfUySt6YDVdZ0pS', '$2y$10$RRSKtNPOIhmx/q.Jc9AK7.BQqpjd8LUp/tLmDRZfUySt6YDVdZ0pS', 2, '2020-03-17', '', '9928335581', '<p>Jaipur</p>', 'Jaipur', 7, 'INR', 1, 1, 'active', '1552914764', NULL),
(27, 'Bigsell', 'deepak.hawkscode@gmail.com', '$2y$10$s1HYPdx.wzs2GeCnkWShEenAYoBvcavf6wfoXtWiLyh4jmeMZfi3e', '$2y$10$s1HYPdx.wzs2GeCnkWShEezODmuc0dTsdtAY/wLhmQzIe1Mk3DsKa', 2, '2019-05-10', '', '+917742100455', '58 B, Ganesh Colony ,\r\nfront of Manglam School\r\nBandanpura', NULL, 7, 'INR', 1, 1, 'active', '1555758868', '1555758952'),
(28, 'sarthak', NULL, '', '', 2, '2019-05-10', '', '+916350650081', '', NULL, 0, 'INR', 0, 0, 'active', '1555759067', NULL),
(29, 'amit bhagat', NULL, '', '', 2, '2019-05-15', '', '+919829251615', '', NULL, 0, 'INR', 0, 0, 'active', '1556189349', NULL),
(30, 'Amit Bhagat', 'amitbhagat82@hotmail.com', '$2y$10$94jD35/Ms.QijxzLCJ86suxaSjdFujctpexzI7C4bBF8RN2Aqe8xy', '$2y$10$94jD35/Ms.QijxzLCJ86suxaSjdFujctpexzI7C4bBF8RN2Aqe8xy', 5, '2020-04-24', '', '9829251615', '<p>Bhagat Mishthan Bhandar, opp Aakash vani, MI Road</p>', 'Jaipur', 7, 'INR', 1, 1, 'active', '1556189586', NULL),
(31, 'Vipin', NULL, '', '', 2, '2019-05-15', '', '789159200900', '', NULL, 0, 'INR', 0, 0, 'active', '1556194929', NULL),
(32, 'Bhhopandra', NULL, '', '', 2, '2019-05-16', '', '+919785539620', '', NULL, 0, 'INR', 0, 0, 'active', '1556270520', NULL),
(33, 'JASWANT SINGH', 'oshadhi.ved10@gmail.com', '$2y$10$6RY9/OZBdDmz1pAjC49XdOAYw0ZIXD3rhBOeODlAcfcPq4zcmIYei', '$2y$10$6RY9/OZBdDmz1pAjC49XdOfsXXeR6cV3u4WFKuceJ8U.s98NGiFXa', 2, '2019-06-20', '', '+917023109977', '7, DURGA NURSERY ROAD ', NULL, 7, 'INR', 1, 1, 'active', '1559291008', '1559291327'),
(34, 'Yash Thadani', 'yashthadani2000@gmail.com', '$2y$10$K49Xc5To5lrOglMC43YrM.MqQ3MythYunIsS5RRgfwHjdazBk.qVC', '$2y$10$K49Xc5To5lrOglMC43YrM.MqQ3MythYunIsS5RRgfwHjdazBk.qVC', 5, '2020-06-26', '', '9220214387', '<p>mumbai</p>', 'Mumbai', 7, 'INR', 1, 1, 'active', '1561621208', NULL),
(35, 'Yatharth Chopra', 'Cyatharth123@gmail.com', '$2y$10$ygb7HkQu1w605eXZONzxs.NwaN4OZJouLXds6iw/74.2Th9LrcPFu', '$2y$10$ygb7HkQu1w605eXZONzxs.NwaN4OZJouLXds6iw/74.2Th9LrcPFu', 5, '2020-06-26', '', '8779979517', '<p>Mumbai</p>', 'Mumbai', 7, 'INR', 1, 1, 'active', '1561624524', NULL),
(36, 'Ansh jain', 'Anshshah04@gmail.com', '$2y$10$k/bSpNpdwboEEYThDWwbIuic4CQSmuc71W0r1GWom2pSiCM5rRRoG', '$2y$10$k/bSpNpdwboEEYThDWwbIuic4CQSmuc71W0r1GWom2pSiCM5rRRoG', 5, '2020-06-26', '', '7506854655', '<p>Mumbai</p>', 'Mumbai', 7, 'INR', 1, 1, 'active', '1561624601', NULL),
(37, 'Lovesh Jetwani', 'Loveshjetwani21@gmail.com', '$2y$10$PitOtLG.otpkIsleP6opNOlyGTR05PUqfejTxnhqEmHcn/rHq8shC', '$2y$10$PitOtLG.otpkIsleP6opNOlyGTR05PUqfejTxnhqEmHcn/rHq8shC', 5, '2020-06-30', '', '7999606630', '<p>mumbai</p>', 'Mumbai', 7, 'INR', 1, 1, 'active', '1561975387', NULL),
(38, 'Siddarth Shah', 'Shah.sid99@gmail.com', '$2y$10$dMl7l3lavG03vKYkdAaTIOlxjpMUvftpIODWzFeqi0pdSstebillC', '$2y$10$dMl7l3lavG03vKYkdAaTIOlxjpMUvftpIODWzFeqi0pdSstebillC', 5, '2020-06-30', '', '8291797735', '<p>mumbai</p>', 'mumbai', 7, 'INR', 1, 1, 'active', '1561975494', NULL),
(39, 'Shyam Mittal', 'Shyam.mittal@ipsedu.co.in', '$2y$10$bSH9UuKneJu3l3bR/n3QA..12jXli7PgLBeWogWE9byKaoBMTR6Oe', '$2y$10$bSH9UuKneJu3l3bR/n3QA..12jXli7PgLBeWogWE9byKaoBMTR6Oe', 5, '2020-07-19', '', '8387021330', '<p>jaipur</p>', 'jaipur', 7, 'INR', 1, 1, 'active', '1563606413', NULL),
(40, 'Shrauteya Dixit', NULL, '', '', 2, '2019-09-02', '', '+919829759650', '', NULL, 0, 'INR', 0, 0, 'active', '1565668316', NULL),
(41, 'Shefali Modi', 'dreamteambyshefali@gmail.com', '$2y$10$UrU73HbdCyJXilbxoVgwse2ZQoRR7w1N8cATCSfv1g49oxbKE7yeO', '$2y$10$UrU73HbdCyJXilbxoVgwseshYj08czllLJ2ENWRghO5hrYxY/m8SW', 2, '2020-09-16', '', '9549290444', '<p>Jaipur</p>', 'Jaipur', 7, 'INR', 1, 1, 'active', '1568718845', '1568678400'),
(42, 'test', NULL, '', '', 2, '2019-10-15', '', '+91234455566677', '', NULL, 0, 'INR', 0, 0, 'active', '1569395657', NULL),
(43, 'test', NULL, '', '', 2, '2019-10-15', '', '9898989898', '', NULL, 0, 'INR', 0, 0, 'active', '1569396253', NULL),
(44, 'test', NULL, '', '', 2, '2019-10-15', '', '+91585858558', '', NULL, 0, 'INR', 0, 0, 'active', '1569396359', NULL),
(45, 'test', NULL, '', '', 2, '2019-10-16', '', '+9144777777', '', NULL, 0, 'INR', 0, 0, 'active', '1569487731', NULL),
(47, 'shivam singh sengar', 'easycord22@gmail.com', '$2y$10$XQxh42t65.qlsRzYU5jRnOc.1JTC3duzGK2j6hN3Rf6o6ytXD7Lk2', '$2y$10$XQxh42t65.qlsRzYU5jRnOjK/2uahnyklHjaU4OUSbCesRrGACiya', 2, '2019-10-22', '', '+917737578822', '21 uma colony', NULL, 7, 'INR', 1, 1, 'active', '1570008042', '1570008121'),
(48, 'Shivam', 'teststs@gmail.com', '$2y$10$G3RPPzZ/MbhRpkhN1FD8bOGyHCd4OdJ4/lTKTfQ8bsTEL6ZmbTSia', '$2y$10$G3RPPzZ/MbhRpkhN1FD8bOgupN4ihgfXD8HR/xDxbjAq2rctx/Yp6', 2, '2019-12-14', '', '+912136545878', 'bdhf dfuids c u ', NULL, 7, 'USD', 1, 1, 'active', '1574575019', '1574575129'),
(49, 'tets data', NULL, '', '', 2, '2019-12-14', '', '+918545214587', '', NULL, 0, 'INR', 0, 0, 'active', '1574580721', NULL),
(50, 'test df', 'kdfk@gmail.com', '$2y$10$JY4ufQBq1QF2VzLV3MMuvecskpfwACMXoKigZCPG6BwoCZXIIhgS6', '$2y$10$JY4ufQBq1QF2VzLV3MMuven2fhlv4q93UzedLuBmCDt7KGE//Bp52', 2, '2019-12-14', '', '+915412546955', 'kjsndjsjs vsd hsd  ds dsh', NULL, 7, 'INR', 1, 1, 'active', '1574581156', '1574581745'),
(51, 'shivam', NULL, '', '', 2, '2019-12-28', '', '+911298789876', '', NULL, 0, 'INR', 0, 0, 'active', '1575788475', NULL),
(52, 'shivam singh sengar', NULL, '', '', 2, '2019-12-28', '', '+917845125463', '', NULL, 0, 'INR', 0, 0, 'active', '1575788518', NULL),
(53, 'Gaurav Sharma', NULL, '', '', 2, '2020-02-03', '', '9650495885', '', NULL, 0, 'INR', 0, 0, 'active', '1579001592', NULL),
(54, 'gaurav', NULL, '', '', 2, '2020-02-11', '', '+919650495885', '', NULL, 0, 'INR', 0, 0, 'active', '1579676400', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_auth`
--

CREATE TABLE `houdin_user_auth` (
  `houdin_user_auth_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_auth_url_token` varchar(150) NOT NULL,
  `houdin_user_auth_auth_token` varchar(150) NOT NULL,
  `houdin_user_auth_user_id` varchar(150) NOT NULL,
  `houdin_user_auth_created_at` varchar(100) NOT NULL,
  `houdin_user_auth_updated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_user_auth`
--

INSERT INTO `houdin_user_auth` (`houdin_user_auth_id`, `houdin_user_auth_url_token`, `houdin_user_auth_auth_token`, `houdin_user_auth_user_id`, `houdin_user_auth_created_at`, `houdin_user_auth_updated_at`) VALUES
(1, '', '', '1', '1545638328', '1583298624'),
(2, 'a20253c275a12c7b4575309cddf74cb2', 'a20253c275a12c7b4575309cddf74cb2', '2', '1556270443', '1570000281');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_forgot_password`
--

CREATE TABLE `houdin_user_forgot_password` (
  `houdin_user_forgot_password_id` int(11) NOT NULL,
  `houdin_user_forgot_password_user_id` int(11) NOT NULL,
  `houdin_user_forgot_password_token` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_user_forgot_password`
--

INSERT INTO `houdin_user_forgot_password` (`houdin_user_forgot_password_id`, `houdin_user_forgot_password_user_id`, `houdin_user_forgot_password_token`) VALUES
(1, 9, 52286);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_logs`
--

CREATE TABLE `houdin_user_logs` (
  `houdin_user_log_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_log_userId` varchar(100) NOT NULL,
  `houdin_user_log_ip_address` varchar(20) NOT NULL,
  `houdin_user_log_browser` varchar(150) NOT NULL,
  `houdin_user_log_geo_location` varchar(50) NOT NULL,
  `houdin_user_log_created_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_user_logs`
--

INSERT INTO `houdin_user_logs` (`houdin_user_log_id`, `houdin_user_log_userId`, `houdin_user_log_ip_address`, `houdin_user_log_browser`, `houdin_user_log_geo_location`, `houdin_user_log_created_at`) VALUES
(1, '1', '103.66.73.15', 'Chrome', 'India', '1545638328'),
(2, '1', '103.66.73.15', 'Chrome', 'India', '1545615564'),
(3, '1', '117.97.132.203', 'Chrome', 'India', '1545890144'),
(4, '1', '103.66.73.15', 'Chrome', 'India', '1545906905'),
(5, '1', '117.97.132.203', 'Chrome', 'India', '1545908173'),
(6, '1', '103.66.73.15', 'Chrome', 'India', '1545872453'),
(7, '1', '117.97.132.203', 'Chrome', 'India', '1545872612'),
(8, '1', '117.97.132.203', 'Chrome', 'India', '1545873140'),
(9, '1', '103.66.73.15', 'Chrome', 'India', '1546080661'),
(10, '1', '103.66.73.15', 'Chrome', 'India', '1546427517'),
(11, '1', '43.224.1.143', 'Chrome', 'India', '1546393479'),
(12, '1', '27.54.163.31', 'Chrome', 'India', '1546950127'),
(13, '1', '27.54.163.31', 'Chrome', 'India', '1547009284'),
(14, '1', '182.68.57.145', 'Chrome', 'India', '1547087914'),
(15, '1', '182.68.57.145', 'Chrome', 'India', '1547088183'),
(16, '1', '117.97.185.88', 'Chrome', 'India', '1548243798'),
(17, '1', '103.66.73.15', 'Chrome', 'India', '1552530228'),
(18, '1', '171.79.77.209', 'Chrome', 'India', '1552902422'),
(19, '1', '171.79.77.209', 'Chrome', 'India', '1552871493'),
(20, '1', '103.66.73.15', 'Chrome', 'India', '1555758561'),
(21, '1', '182.68.98.254', 'Chrome', 'India', '1556179426'),
(22, '1', '103.66.73.15', 'Chrome', 'India', '1556179572'),
(23, '1', '182.68.98.254', 'Chrome', 'India', '1556179760'),
(24, '1', '182.68.68.15', 'Chrome', 'India', '1556189478'),
(25, '1', '103.66.73.15', 'Chrome', 'India', '1556155105'),
(26, '2', '103.66.73.15', 'Chrome', 'India', '1556270443'),
(27, '1', '182.68.98.254', 'Chrome', 'India', '1556436118'),
(28, '1', '182.68.98.254', 'Chrome', 'India', '1556587132'),
(29, '1', '182.68.98.254', 'Chrome', 'India', '1557135226'),
(30, '1', '103.117.124.100', 'Safari', 'India', '1561621131'),
(31, '1', '106.209.153.139', 'Safari', 'India', '1561975184'),
(32, '1', '223.189.167.190', 'Safari', 'India', '1563606316'),
(33, '1', '182.68.182.228', 'Chrome', 'India', '1563620936'),
(34, '1', '223.189.186.206', 'Safari', 'India', '1568595906'),
(35, '1', '117.203.16.118', 'Chrome', 'India', '1568717606'),
(36, '2', '::1', 'Chrome', '', '1570000281'),
(37, '1', '157.37.178.85', 'Chrome', 'India', '1574580146'),
(38, '1', '157.37.178.85', 'Chrome', 'India', '1574580604'),
(39, '1', '49.206.184.136', 'Chrome', 'India', '1574748637'),
(40, '1', '157.37.155.201', 'Chrome', 'India', '1574749403'),
(41, '1', '49.206.184.136', 'Chrome', 'India', '1574750150'),
(42, '1', '157.47.154.171', 'Chrome', 'India', '1575780656'),
(43, '1', '49.206.179.213', 'Chrome', 'India', '1575781647'),
(44, '1', '49.206.179.213', 'Chrome', 'India', '1575787141'),
(45, '1', '157.47.216.103', 'Chrome', 'India', '1576905640'),
(46, '1', '150.129.239.193', 'Chrome', 'India', '1579676931'),
(47, '1', '49.206.185.175', 'Chrome', 'India', '1583298624');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_payments`
--

CREATE TABLE `houdin_user_payments` (
  `houdin_user_payment_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_user_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_package_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_coupon_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_cost` float NOT NULL,
  `houdin_user_payment_currency` varchar(50) NOT NULL,
  `houdin_user_payment_transcaction_id` varchar(150) NOT NULL,
  `houdin_user_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 For Unsuccess, 1 For Success, 2 For cancelled, 2 For refund',
  `houdin_user_payment_time` datetime NOT NULL,
  `houdin_user_payment_start_date` date NOT NULL,
  `houdin_user_payment_end_date` date DEFAULT NULL,
  `houdin_user_payment_created_at` timestamp NULL DEFAULT NULL,
  `houdin_user_payment_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_payment_tracks`
--

CREATE TABLE `houdin_user_payment_tracks` (
  `houdin_user_payment_track_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_track_user_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_track_package_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_track_last_done` datetime NOT NULL,
  `houdin_user_payment_track_status` tinyint(4) NOT NULL,
  `houdin_user_payment_track_created_at` timestamp NULL DEFAULT NULL,
  `houdin_user_payment_track_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_second_auth`
--

CREATE TABLE `houdin_user_second_auth` (
  `houdin_user_second_auth_id` int(11) NOT NULL,
  `houdin_user_second_auth_user_id` int(11) NOT NULL,
  `houdin_user_second_auth_token` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_user_second_auth`
--

INSERT INTO `houdin_user_second_auth` (`houdin_user_second_auth_id`, `houdin_user_second_auth_user_id`, `houdin_user_second_auth_token`) VALUES
(13, 3, 18124),
(16, 7, 45957),
(17, 8, 22190),
(20, 12, 34669),
(21, 13, 81815),
(24, 14, 6367),
(25, 15, 34035),
(27, 15, 13860),
(30, 15, 36365),
(31, 24, 79432),
(32, 24, 50730),
(34, 28, 65361),
(35, 29, 78426),
(36, 29, 23673),
(37, 31, 19881),
(38, 32, 12218),
(40, 40, 2567),
(41, 42, 89963),
(42, 43, 51080),
(43, 43, 84237),
(44, 44, 18409),
(45, 45, 31452),
(49, 49, 61161),
(51, 51, 19079),
(52, 52, 45822),
(53, 53, 62814),
(54, 54, 68172);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_shops`
--

CREATE TABLE `houdin_user_shops` (
  `houdin_user_shop_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_shop_user_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_shop_shop_name` varchar(250) NOT NULL,
  `houdin_user_shop_email` varchar(150) DEFAULT NULL,
  `houdin_user_shop_website` varchar(250) DEFAULT NULL,
  `houdin_user_shop_phone` varchar(50) DEFAULT NULL,
  `houdin_user_shop_address` varchar(400) NOT NULL,
  `houdin_user_shop_city` varchar(150) NOT NULL,
  `houdin_user_shop_state` varchar(150) NOT NULL,
  `houdin_user_shop_country` varchar(150) NOT NULL,
  `houdin_user_shops_category` int(11) NOT NULL,
  `app_business_category` int(11) NOT NULL DEFAULT '0' COMMENT '// key to app_business_category',
  `houdin_user_shops_language` int(11) NOT NULL,
  `houdin_user_shop_db_name` varchar(150) NOT NULL,
  `domain_url` varchar(250) NOT NULL,
  `domain_name` varchar(250) NOT NULL,
  `houdin_user_account_type` enum('live','testing') NOT NULL,
  `houdin_user_shop_active_status` enum('active','deactive','block') NOT NULL COMMENT '0 For Deactive, 1 For Active',
  `houdin_user_shop_created_at` varchar(30) DEFAULT NULL,
  `houdin_user_shop_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_user_shops`
--

INSERT INTO `houdin_user_shops` (`houdin_user_shop_id`, `houdin_user_shop_user_id`, `houdin_user_shop_shop_name`, `houdin_user_shop_email`, `houdin_user_shop_website`, `houdin_user_shop_phone`, `houdin_user_shop_address`, `houdin_user_shop_city`, `houdin_user_shop_state`, `houdin_user_shop_country`, `houdin_user_shops_category`, `app_business_category`, `houdin_user_shops_language`, `houdin_user_shop_db_name`, `domain_url`, `domain_name`, `houdin_user_account_type`, `houdin_user_shop_active_status`, `houdin_user_shop_created_at`, `houdin_user_shop_updated_at`) VALUES
(1, 1, 'hawkscodedepa', NULL, NULL, NULL, '', '', '', '7', 3, 0, 3, 'warrdel2', 'hawks.houdine.com', 'hawks', 'live', 'active', '1545648093', NULL),
(2, 2, 'deepak', NULL, NULL, NULL, '', '', '', '7', 1, 0, 1, 'warrdel2', 'deepa.houdine.com', 'deepa', 'live', 'active', '1545654433', NULL),
(3, 4, 'Arth VCFO', NULL, NULL, NULL, '', '', '', '7', 1, 0, 3, 'warrdel2', 'Arth.houdine.com', 'arth', 'live', 'active', '1545792550', NULL),
(7, 10, 'Warrdel', NULL, NULL, NULL, '', '', '', '7', 7, 0, 3, 'warrdel2', 'Warrd.houdine.com', 'warrd', 'live', 'active', '1545873453', NULL),
(8, 11, 'shivamelectrical', NULL, NULL, NULL, '', '', '', '7', 1, 0, 3, 'warrdel2', 'shiva.houdine.com', 'shiva', 'live', 'active', '1545998644', NULL),
(9, 13, 'Hawksinsta', NULL, NULL, NULL, '', '', '', '7', 1, 0, 3, 'warrdel2', 'Hawks.houdine.com', 'hawks', 'live', 'active', '1546411193', NULL),
(10, 16, 'Shubh Financial Services', NULL, NULL, NULL, '', '', '', '7', 3, 0, 3, 'warrdel2', 'Shubh.houdine.com', 'shubh', 'live', 'active', '1546394105', NULL),
(12, 19, 'Megasa', NULL, NULL, NULL, '', '', '', '7', 1, 0, 3, 'warrdel2', 'Megas.warrdel.com', 'megas', 'live', 'active', '1546914299', NULL),
(16, 23, 'GymStore', NULL, NULL, NULL, '', '', '', '7', 3, 0, 3, 'warrdel2', 'GymSt.houdine.com', 'gymst', 'testing', 'active', '1547017894', NULL),
(17, 25, 'MaD Store', NULL, NULL, NULL, '', '', '', '7', 3, 0, 3, 'warrdel2', 'MaDS.houdine.com', 'mads', 'live', 'active', '1552902605', NULL),
(18, 26, 'Limod', NULL, NULL, NULL, '', '', '', '7', 3, 0, 3, 'warrdel2', 'Limod.houdine.com', 'limod', 'live', 'active', '1552871564', NULL),
(19, 27, 'BigSell', NULL, NULL, NULL, '', '', '', '7', 9, 0, 1, 'warrdel2', 'BigSe.houdine.com', 'bigse', 'live', 'active', '1555758952', NULL),
(20, 30, 'Bhagat Mishthan Bhandar', NULL, NULL, NULL, '', '', '', '7', 3, 0, 3, 'warrdel2', 'Bhaga.houdine.com', 'bhaga', 'live', 'active', '1556189586', NULL),
(21, 33, 'OSHADHI VED AYURVED', NULL, NULL, NULL, '', '', '', '7', 8, 0, 3, 'warrdel2', 'OSHAD.houdine.com', 'oshad', 'live', 'active', '1559291327', NULL),
(22, 34, 'Yash apparels', NULL, NULL, NULL, '', '', '', '7', 3, 0, 3, 'warrdel2', 'Yash.houdine.com', 'yash', 'testing', 'active', '1561621208', NULL),
(23, 35, 'YCA', NULL, NULL, NULL, '', '', '', '7', 1, 0, 3, 'warrdel2', 'YCA.houdine.com', 'yca', 'testing', 'active', '1561624524', NULL),
(24, 36, 'Xyz store', NULL, NULL, NULL, '', '', '', '7', 3, 0, 3, 'warrdel2', 'Xyzs.houdine.com', 'xyzs', 'testing', 'active', '1561624601', NULL),
(25, 37, 'Lovesh Designs', NULL, NULL, NULL, '', '', '', '7', 3, 0, 3, 'warrdel2', 'Loves.houdine.com', 'loves', 'testing', 'active', '1561975387', NULL),
(26, 38, 'Siddarth textiles', NULL, NULL, NULL, '', '', '', '7', 1, 0, 3, 'warrdel2', 'Sidda.houdine.com', 'sidda', 'testing', 'active', '1561975494', NULL),
(27, 39, 'Shyam', NULL, NULL, NULL, '', '', '', '7', 1, 0, 3, 'warrdel2', 'Shyam.houdine.com', 'shyam', 'testing', 'active', '1563606413', NULL),
(28, 41, 'Dreamteam', NULL, NULL, NULL, '', '', '', '7', 1, 0, 3, 'warrdel2', 'Dream.houdine.com', 'dream', 'live', 'active', '1568718845', NULL),
(30, 47, 'anushsar', NULL, NULL, NULL, '', '', '', '7', 3, 0, 3, 'warrdel2', 'anush.localhost', 'anush', 'live', 'active', '1570008121', NULL),
(31, 48, 'Test', NULL, NULL, NULL, '', '', '', '7', 4, 0, 1, 'warrdel2', 'Test.warrdel.com', 'test', 'live', 'active', '1574575129', NULL),
(32, 50, 'slkdfdsk', NULL, NULL, NULL, '', '', '', '7', 3, 0, 3, 'houdine_slkdf_db', 'slkdf.warrdel.com', 'slkdf', 'live', 'active', '1574581745', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_verifications`
--

CREATE TABLE `houdin_user_verifications` (
  `houdin_user_verification_email` varchar(150) NOT NULL,
  `houdin_user_verification_token` varchar(150) NOT NULL,
  `houdin_user_verification_created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_vendor_auth`
--

CREATE TABLE `houdin_vendor_auth` (
  `houdin_vendor_auth_id` int(11) NOT NULL,
  `houdin_vendor_auth_vendor_id` int(11) NOT NULL,
  `houdin_vendor_auth_token` varchar(50) NOT NULL,
  `houdin_vendor_auth_url_token` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_vendor_auth`
--

INSERT INTO `houdin_vendor_auth` (`houdin_vendor_auth_id`, `houdin_vendor_auth_vendor_id`, `houdin_vendor_auth_token`, `houdin_vendor_auth_url_token`) VALUES
(1, 1, '2RF0Kv', '2RF0Kv'),
(5, 2, 'Lmoz01', 'Lmoz01'),
(3, 1, 'k6w8x2', 'k6w8x2'),
(6, 1, 'a4n1u4', 'a4n1u4'),
(7, 1, 'm0x9w4', 'm0x9w4'),
(8, 1, 'y0e5c1', 'y0e5c1'),
(9, 1, 'u5j1h2', 'u5j1h2'),
(11, 4, 'i5X5X8', 'i5X5X8'),
(12, 1, 'z8c2z1', 'z8c2z1'),
(14, 6, '7ok0ad', '7ok0ad'),
(15, 6, 'c6u4z6', 'c6u4z6'),
(16, 4, 'f3r7y7', 'f3r7y7'),
(17, 9, 'w8y5e8', 'w8y5e8'),
(18, 4, 'w2u6h8', 'w2u6h8'),
(19, 4, 'k4u5w2', 'k4u5w2'),
(34, 4, 'i3e5f6', 'i3e5f6'),
(22, 4, 'f4k9v2', 'f4k9v2'),
(23, 4, 'm6j9s9', 'm6j9s9'),
(24, 4, 'a3w8b5', 'a3w8b5'),
(25, 4, 'c2s7p2', 'c2s7p2'),
(44, 9, 't4d5k5', 't4d5k5'),
(27, 1, 'p2j6o6', 'p2j6o6'),
(28, 1, 'm6n8c0', 'm6n8c0'),
(29, 1, 'u6i8p4', 'u6i8p4'),
(30, 1, 'a3h7l3', 'a3h7l3'),
(31, 4, 'w9e0u8', 'w9e0u8'),
(33, 4, 'k3a8y6', 'k3a8y6'),
(35, 1, 'y2h8w5', 'y2h8w5'),
(36, 4, 's0o0z0', 's0o0z0'),
(37, 1, 'u7l4p9', 'u7l4p9'),
(42, 10, '3vHg3a', '3vHg3a'),
(43, 9, 'e1n5z4', 'e1n5z4'),
(45, 9, 'f3r2m4', 'f3r2m4'),
(46, 9, 'lMlm5Q', 'lMlm5Q'),
(47, 9, 'g9w2r5', 'g9w2r5'),
(48, 9, 'tMQAg9', 'tMQAg9'),
(49, 9, 'i9SKmt', 'i9SKmt'),
(50, 1, 'l4s1t6', 'l4s1t6'),
(51, 10, 'q6e2h5', 'q6e2h5'),
(52, 1, 'd9f8k6', 'd9f8k6'),
(53, 10, 'Wugr3T', 'Wugr3T'),
(54, 4, 'y9b7o3', 'y9b7o3'),
(55, 4, 'Ik1lmq', 'Ik1lmq'),
(56, 11, '2KJklP', '2KJklP'),
(58, 10, 'y2p7q0', 'y2p7q0'),
(59, 10, 'l3h0o3', 'l3h0o3'),
(61, 4, 'izjYSv', 'izjYSv'),
(62, 4, 'x5o8g5', 'x5o8g5'),
(63, 10, 'i8w7d6', 'i8w7d6'),
(66, 1, 'RypCMM', 'RypCMM'),
(68, 1, 'z5j1j5', 'z5j1j5'),
(69, 10, 'h6i1e4', 'h6i1e4'),
(70, 16, '73BjAh', '73BjAh'),
(71, 10, 't5v5i3', 't5v5i3'),
(72, 1, 'w9r6q6', 'w9r6q6'),
(73, 1, 'u3a9p1', 'u3a9p1'),
(74, 1, 'k6q1i5', 'k6q1i5'),
(75, 10, 'u0i6z6', 'u0i6z6'),
(77, 1, 'VJBiLw', 'VJBiLw'),
(78, 10, 'y8y8z2', 'y8y8z2'),
(80, 16, 'o6l1k4', 'o6l1k4'),
(81, 10, 't7r7w0', 't7r7w0'),
(83, 1, 'T3p9s8', 'T3p9s8'),
(84, 10, 'z5i0u6', 'z5i0u6'),
(85, 1, 'y3r3h2', 'y3r3h2'),
(86, 10, 'b6b7z6', 'b6b7z6'),
(89, 1, 'QJKfz7', 'QJKfz7'),
(90, 1, 'm8f8m9', 'm8f8m9'),
(91, 10, 'd7e2e2', 'd7e2e2'),
(92, 19, 'i4ZG8b', 'i4ZG8b'),
(100, 1, 'PRGO54', 'PRGO54'),
(94, 1, 'a2y7l7', 'a2y7l7'),
(97, 19, 'k4y8q8', 'k4y8q8'),
(98, 19, 'a7o8q4', 'a7o8q4'),
(101, 16, 'd3h9v8', 'd3h9v8'),
(102, 16, 's5f5f8', 's5f5f8'),
(103, 1, 'v3b4y0', 'v3b4y0'),
(104, 1, '4uE4o5', '4uE4o5'),
(106, 19, '97G7Gs', '97G7Gs'),
(107, 1, 'i2m0a4', 'i2m0a4'),
(108, 1, 'h5x8p1', 'h5x8p1'),
(109, 1, 'f4j0w7', 'f4j0w7'),
(110, 19, 'z0i3n8', 'z0i3n8'),
(113, 16, 'e6f7m3', 'e6f7m3'),
(112, 10, 'r7t9n6', 'r7t9n6'),
(114, 16, 'm3i9v0', 'm3i9v0'),
(117, 19, '2Q2HAe', '2Q2HAe'),
(116, 1, 'f0p2q2', 'f0p2q2'),
(118, 19, 'y2m0r2', 'y2m0r2'),
(119, 16, 'a5w3l4', 'a5w3l4'),
(120, 10, 'f4v9a1', 'f4v9a1'),
(121, 1, 'g0r3m7', 'g0r3m7'),
(122, 19, 'q7u4a8', 'q7u4a8'),
(123, 16, 'z4x5j8', 'z4x5j8'),
(124, 10, 'n6k3r7', 'n6k3r7'),
(125, 11, 'f0y7l3', 'f0y7l3'),
(126, 16, 'h7i1p5', 'h7i1p5'),
(127, 16, 'u6y7m0', 'u6y7m0'),
(128, 19, 'q5c1q1', 'q5c1q1'),
(129, 10, 'f1z7g7', 'f1z7g7'),
(130, 10, 't5v7c4', 't5v7c4'),
(132, 10, 'FPw0hb', 'FPw0hb'),
(134, 19, 'A3wUOr', 'A3wUOr'),
(135, 10, 'P2dlBU', 'P2dlBU'),
(136, 19, '3l2Rid', '3l2Rid'),
(137, 19, '747wPh', '747wPh'),
(138, 19, 'l5e9b8', 'l5e9b8'),
(139, 10, 'h2x5b1', 'h2x5b1'),
(141, 19, 'eAfhAs', 'eAfhAs'),
(142, 19, 'r6e9c9', 'r6e9c9'),
(144, 19, 'r1t83k', 'r1t83k'),
(145, 19, 'p1p2e2', 'p1p2e2'),
(146, 19, 'o2v0f7', 'o2v0f7'),
(149, 19, 'JxLVQl', 'JxLVQl'),
(150, 19, 'dGCBU8', 'dGCBU8'),
(151, 19, 'a22W1e', 'a22W1e'),
(152, 19, '3w773O', '3w773O'),
(153, 19, 'kZYxRw', 'kZYxRw'),
(154, 19, '96SWvo', '96SWvo'),
(156, 19, '1DHzHF', '1DHzHF'),
(157, 19, 'm2s8x3', 'm2s8x3'),
(158, 16, 'z4a2r6', 'z4a2r6'),
(159, 10, 'q6j6o7', 'q6j6o7'),
(160, 10, 'a1p2u3', 'a1p2u3'),
(161, 16, 'x7p9t5', 'x7p9t5'),
(163, 19, 'i8z3vd', 'i8z3vd'),
(164, 19, 'z9o9e5', 'z9o9e5'),
(165, 19, 'z0s6x3', 'z0s6x3'),
(166, 19, 'l5j9e9', 'l5j9e9'),
(167, 19, 'p8l5f5', 'p8l5f5'),
(168, 19, 'j49C74', 'j49C74'),
(169, 19, 'j5n2t5', 'j5n2t5'),
(170, 19, 'r8m7d9', 'r8m7d9'),
(171, 16, 'w4u1o8', 'w4u1o8'),
(172, 16, 'l3v5i9', 'l3v5i9'),
(173, 19, 'p0e9a3', 'p0e9a3'),
(174, 19, 'i9l3q6', 'i9l3q6'),
(175, 19, 'v8d5d7', 'v8d5d7'),
(176, 19, 'm3l7z5', 'm3l7z5'),
(177, 19, 'b5r7k0', 'b5r7k0'),
(178, 19, 'g8e2r3', 'g8e2r3'),
(179, 19, 'l3x9r9', 'l3x9r9'),
(180, 19, 'o2z4q3', 'o2z4q3'),
(181, 19, 'k4w2b6', 'k4w2b6'),
(190, 19, 'w1q8q0', 'w1q8q0'),
(184, 2, 'I6G8rY', 'I6G8rY'),
(185, 2, '425UEK', '425UEK'),
(187, 19, 'hl9q98', 'hl9q98'),
(191, 16, 'h4m3e9', 'h4m3e9'),
(192, 19, 'i5c2h2', 'i5c2h2'),
(193, 19, 'e7x5w3', 'e7x5w3'),
(194, 19, 'q0l6x4', 'q0l6x4'),
(197, 19, 'o22dvN', 'o22dvN'),
(198, 19, 'n1l9v0', 'n1l9v0'),
(199, 19, 'f5u2w7', 'f5u2w7'),
(200, 19, 'v6s5w0', 'v6s5w0'),
(201, 19, 'f2x4q1', 'f2x4q1'),
(202, 19, 'i7p1m9', 'i7p1m9'),
(203, 19, 'v1m9w6', 'v1m9w6'),
(204, 19, 'g6g1c9', 'g6g1c9'),
(210, 19, 'z9w3r1', 'z9w3r1'),
(208, 10, '12ZcLp', '12ZcLp'),
(207, 10, 'n3c9r8', 'n3c9r8'),
(211, 16, 'c7j3n7', 'c7j3n7'),
(212, 19, 'r7k5s1', 'r7k5s1'),
(213, 19, 'y4f9p5', 'y4f9p5'),
(214, 19, 'g2b6g3', 'g2b6g3'),
(215, 19, 'f0i5m9', 'f0i5m9'),
(216, 19, 'c6a1v9', 'c6a1v9'),
(217, 16, 'y0w6u4', 'y0w6u4'),
(218, 19, 'f3x2k7', 'f3x2k7'),
(219, 19, 'n0b9y3', 'n0b9y3'),
(220, 19, '7zbAir', '7zbAir'),
(221, 19, 'd6i5e6', 'd6i5e6'),
(222, 19, 'g8u0p1', 'g8u0p1'),
(223, 16, 'v7e2w4', 'v7e2w4'),
(224, 19, 'r6D8ll', 'r6D8ll'),
(225, 16, 'k5w2w8', 'k5w2w8'),
(228, 19, 'c2m4j0', 'c2m4j0'),
(227, 25, 'V2V507', 'V2V507'),
(230, 19, 'g8s6c4', 'g8s6c4'),
(231, 19, 'p4p7s9', 'p4p7s9'),
(236, 19, 'i5f4x2', 'i5f4x2'),
(243, 19, 'x6u2o4', 'x6u2o4'),
(248, 19, 'y4h2g3', 'y4h2g3'),
(249, 25, 'D4J8wY', 'D4J8wY'),
(251, 19, 't7j8k7', 't7j8k7'),
(252, 19, 'c8r3j5', 'c8r3j5'),
(253, 19, 'h947DO', 'h947DO'),
(254, 19, 'v8h2v4', 'v8h2v4'),
(255, 19, 'c2y6l7', 'c2y6l7'),
(257, 19, 'h8f9j4', 'h8f9j4'),
(258, 1, 'h6c4n2', 'h6c4n2'),
(259, 1, 'i1c1f5', 'i1c1f5'),
(263, 19, 't8w1a6', 't8w1a6'),
(264, 19, 'w3v4e6', 'w3v4e6'),
(267, 19, '5YmdvC', '5YmdvC'),
(268, 19, 'pOABf4', 'pOABf4'),
(269, 19, 'v0p0t8', 'v0p0t8'),
(270, 19, 'wvHvsA', 'wvHvsA'),
(271, 19, 'x1e0b3', 'x1e0b3'),
(272, 19, 'h0m8g4', 'h0m8g4'),
(273, 19, 'r0o5c1', 'r0o5c1'),
(274, 25, 'Wh66Y8', 'Wh66Y8'),
(275, 19, 'l0f6e7', 'l0f6e7'),
(276, 19, 'u5c9k1', 'u5c9k1'),
(277, 19, 'g5h1s8', 'g5h1s8'),
(278, 19, 'i7q6i3', 'i7q6i3'),
(279, 19, 'f7h8w4', 'f7h8w4'),
(280, 19, 'f2e5p5', 'f2e5p5'),
(281, 19, 'm5i8e0', 'm5i8e0'),
(282, 19, 'w5k8b0', 'w5k8b0'),
(283, 19, 'O9V5qO', 'O9V5qO'),
(285, 19, 'z3o8e7', 'z3o8e7'),
(286, 19, 'm9d8w8', 'm9d8w8'),
(287, 19, 'x8l4y7', 'x8l4y7'),
(288, 19, 'u8l6k1', 'u8l6k1'),
(289, 19, 'r7a4t7', 'r7a4t7'),
(290, 19, 'u2h9h3', 'u2h9h3'),
(291, 19, 'q0l9n0', 'q0l9n0'),
(292, 19, 'b5l8s2', 'b5l8s2'),
(293, 19, 's4l5m1', 's4l5m1'),
(294, 19, 'RkFKZx', 'RkFKZx'),
(295, 19, 'e6o1u5', 'e6o1u5'),
(296, 19, 'd8b8e8', 'd8b8e8'),
(297, 19, 'q3b6v5', 'q3b6v5'),
(298, 19, 'o8n8q9', 'o8n8q9'),
(299, 19, 'l2p0o6', 'l2p0o6'),
(300, 4, 'z2x2i3', 'z2x2i3'),
(301, 19, 'u0y6c8', 'u0y6c8'),
(302, 19, 'm9f4i5', 'm9f4i5'),
(303, 19, 'a7x4h9', 'a7x4h9'),
(304, 19, 'r9r5o2', 'r9r5o2'),
(305, 19, 'w1w1s9', 'w1w1s9'),
(306, 33, 'bqlkDr', 'bqlkDr'),
(307, 19, 'y5r6k2', 'y5r6k2'),
(308, 19, 's0g4p9', 's0g4p9'),
(309, 19, 'c8o7n1', 'c8o7n1'),
(317, 19, 'OvB6rH', 'OvB6rH'),
(312, 4, 'z2s3b8', 'z2s3b8'),
(313, 1, 'i7b3r8', 'i7b3r8'),
(314, 10, 'u4a8j9', 'u4a8j9'),
(316, 10, 't8y2j2', 't8y2j2'),
(320, 19, 'z0y7d7', 'z0y7d7'),
(321, 19, 'x2u3u3', 'x2u3u3'),
(325, 19, 'o0c5u3', 'o0c5u3'),
(324, 19, 'c8f2y4', 'c8f2y4'),
(328, 19, 'bXD4F5', 'bXD4F5'),
(329, 19, 'f6z9b6', 'f6z9b6'),
(330, 19, 'UyT4wu', 'UyT4wu'),
(331, 19, 'k1a1c8', 'k1a1c8'),
(332, 19, 'h3y3x2', 'h3y3x2'),
(335, 19, 'psx4f9', 'psx4f9'),
(334, 19, 'i5m3j4', 'i5m3j4'),
(336, 19, 'o5o9y5', 'o5o9y5'),
(338, 19, 'z7q8x3', 'z7q8x3'),
(339, 19, 'u7h4n3', 'u7h4n3'),
(341, 19, 'f7h4d5', 'f7h4d5'),
(343, 19, 'u6z5q7', 'u6z5q7'),
(345, 34, 'o2k9e0', 'o2k9e0'),
(346, 19, 'h1t1x0', 'h1t1x0'),
(348, 34, 'h2n2c0', 'h2n2c0'),
(349, 19, 'EDhdHc', 'EDhdHc'),
(350, 34, 'z6t4a9', 'z6t4a9'),
(351, 35, 'Kz1UDo', 'Kz1UDo'),
(352, 35, 'MhGSMC', 'MhGSMC'),
(356, 1, 'w3k9k8', 'w3k9k8'),
(358, 16, 'u9d1t4', 'u9d1t4'),
(361, 19, 'o2j0a4', 'o2j0a4'),
(372, 19, 'w8n6s6', 'w8n6s6'),
(363, 39, '08OBzn', '08OBzn'),
(364, 19, 's4l6q4', 's4l6q4'),
(366, 19, 'j5f0r2', 'j5f0r2'),
(373, 19, 'o5p2f1', 'o5p2f1'),
(374, 19, 'i9e7r4', 'i9e7r4'),
(376, 34, 'e8g6g0', 'e8g6g0'),
(377, 39, 'AUvz89', 'AUvz89'),
(378, 19, 'i7x0b9', 'i7x0b9'),
(381, 41, 'j9n8m7', 'j9n8m7'),
(382, 41, 'GVAHTU', 'GVAHTU'),
(383, 19, 'y9f7b7', 'y9f7b7'),
(384, 10, 'd7m8z1', 'd7m8z1'),
(385, 10, 'a2a9f9', 'a2a9f9'),
(386, 10, 'a3y4r4', 'a3y4r4'),
(387, 19, 'j2n7g9', 'j2n7g9'),
(388, 19, 'c5s2u8', 'c5s2u8'),
(390, 41, 'j5g3r3', 'j5g3r3'),
(391, 41, 'o5r0v6', 'o5r0v6'),
(392, 41, 'x6y9y1', 'x6y9y1'),
(393, 41, 'l7w3j7', 'l7w3j7'),
(394, 41, 'y9f3y7', 'y9f3y7'),
(395, 46, 'm4l2e6', 'm4l2e6'),
(396, 47, 'a3x7x1', 'a3x7x1'),
(397, 47, 'c2h3t1', 'c2h3t1'),
(398, 47, 'r5w8b5', 'r5w8b5'),
(399, 47, 'u6e0o9', 'u6e0o9'),
(400, 47, 'r5w7z2', 'r5w7z2'),
(401, 19, 'k4y6f5', 'k4y6f5'),
(402, 19, 'aLM1Aj', 'aLM1Aj'),
(410, 19, 'pK9bZn', 'pK9bZn'),
(404, 19, 'v7q5g5', 'v7q5g5'),
(409, 19, 'Qo4thr', 'Qo4thr'),
(406, 10, 'l5e5j2', 'l5e5j2'),
(411, 19, '2FAj7u', '2FAj7u'),
(412, 19, 'x8n7y1', 'x8n7y1'),
(413, 19, 'EB4uTo', 'EB4uTo'),
(414, 19, 'GsknJw', 'GsknJw'),
(415, 19, '9P1GXd', '9P1GXd'),
(416, 19, 'uodFIy', 'uodFIy'),
(417, 19, 'p1l0v2', 'p1l0v2'),
(418, 19, 'k1j4f8', 'k1j4f8'),
(419, 19, 'Czpker', 'Czpker'),
(420, 19, '8AQckN', '8AQckN'),
(421, 19, '64r5NV', '64r5NV'),
(422, 19, 'g1j1b6', 'g1j1b6'),
(423, 19, 'J8OSYN', 'J8OSYN'),
(424, 19, 'D6tEPI', 'D6tEPI'),
(426, 19, 'CT7yT8', 'CT7yT8'),
(427, 19, 'j4e3m6', 'j4e3m6'),
(428, 19, 'c1i4o5', 'c1i4o5'),
(429, 19, 'sDIwP5', 'sDIwP5'),
(430, 19, 'i2u3x8', 'i2u3x8'),
(431, 19, '1omI9a', '1omI9a'),
(432, 19, 'Pu1jAz', 'Pu1jAz'),
(433, 19, 'ZGw92f', 'ZGw92f'),
(434, 19, 'W6yWed', 'W6yWed'),
(438, 19, '6O29NY', '6O29NY'),
(439, 19, 'Hd5SY6', 'Hd5SY6'),
(440, 19, 't8j4t7', 't8j4t7'),
(441, 19, 'v4x3w2', 'v4x3w2'),
(442, 19, 'Fum7J9', 'Fum7J9'),
(443, 19, 'enyvrN', 'enyvrN'),
(444, 19, 'uCHhoG', 'uCHhoG'),
(445, 19, '65N75R', '65N75R'),
(446, 19, 'JJtq17', 'JJtq17'),
(447, 19, '54U5KU', '54U5KU'),
(448, 19, 'bg5Qr7', 'bg5Qr7'),
(452, 19, '3Ju4Zt', '3Ju4Zt'),
(453, 19, 'vKZxqt', 'vKZxqt'),
(455, 19, 'aEv08o', 'aEv08o'),
(456, 19, 'kew4P8', 'kew4P8'),
(458, 10, '60Uw7y', '60Uw7y'),
(459, 10, 'x5t3p5', 'x5t3p5'),
(462, 19, 'q7k1w1', 'q7k1w1'),
(463, 10, 'm3n4j7', 'm3n4j7'),
(464, 10, 'k3h0x2', 'k3h0x2'),
(465, 19, 'z8m1a3', 'z8m1a3');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_vendor_staff`
--

CREATE TABLE `houdin_vendor_staff` (
  `vendor_staff` int(250) NOT NULL,
  `vendor_staff_email` varchar(250) NOT NULL,
  `vendor_staff_shop_name` varchar(250) NOT NULL,
  `vendor_staff_db_name` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_vendor_staff`
--

INSERT INTO `houdin_vendor_staff` (`vendor_staff`, `vendor_staff_email`, `vendor_staff_shop_name`, `vendor_staff_db_name`) VALUES
(1, 'deepsharma906@gmail.com', 'houdine_Megas_db', 'houdine_Megas_db'),
(2, 'AJ7500@GMAIL.COM', 'houdine_Yash_db', 'houdine_Yash_db');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_vendor_user_log`
--

CREATE TABLE `houdin_vendor_user_log` (
  `houdin_vendor_user_log_id` int(11) NOT NULL,
  `houdin_vendor_user_log_userid` int(11) NOT NULL,
  `houdin_vendor_user_log_ip_address` varchar(50) NOT NULL,
  `houdin_vendor_user_log_browser` varchar(30) NOT NULL,
  `houdin_vendor_user_log_location` varchar(30) NOT NULL,
  `houdin_vendor_user_log_created_at` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_vendor_user_log`
--

INSERT INTO `houdin_vendor_user_log` (`houdin_vendor_user_log_id`, `houdin_vendor_user_log_userid`, `houdin_vendor_user_log_ip_address`, `houdin_vendor_user_log_browser`, `houdin_vendor_user_log_location`, `houdin_vendor_user_log_created_at`) VALUES
(1, 1, '103.66.73.15', 'Chrome', 'India', '1545609600'),
(2, 1, '103.66.73.15', 'Chrome', 'India', '1545609600'),
(3, 1, '103.66.73.15', 'Chrome', 'India', '1545696000'),
(4, 1, '103.66.73.15', 'Chrome', 'India', '1545696000'),
(5, 1, '103.66.73.15', 'Chrome', 'India', '1545868800'),
(6, 4, '117.97.132.203', 'Chrome', 'India', '1545868800'),
(7, 4, '122.162.132.54', 'Chrome', 'India', '1545868800'),
(8, 1, '103.66.73.15', 'Chrome', 'India', '1545868800'),
(9, 1, '103.66.73.15', 'Firefox', 'India', '1545868800'),
(10, 4, '103.66.73.15', 'Chrome', 'India', '1545868800'),
(11, 4, '103.66.73.15', 'Safari', 'India', '1545868800'),
(12, 4, '103.66.73.15', 'Edge', 'India', '1545868800'),
(13, 4, '117.97.132.203', 'Chrome', 'India', '1545868800'),
(14, 1, '117.97.132.203', 'Chrome', 'India', '1545868800'),
(15, 9, '103.66.73.15', 'Firefox', 'India', '1545868800'),
(16, 9, '103.66.73.15', 'Firefox', 'India', '1545868800'),
(17, 10, '43.224.1.153', 'Chrome', 'India', '1545868800'),
(18, 1, '103.66.73.15', 'Chrome', 'India', '1545955200'),
(19, 4, '103.66.73.15', 'Chrome', 'India', '1545955200'),
(20, 10, '43.224.1.143', 'Chrome', 'India', '1545955200'),
(21, 10, '43.224.1.143', 'Chrome', 'India', '1545955200'),
(22, 10, '122.161.146.135', 'Chrome', 'India', '1546041600'),
(23, 10, '103.66.73.15', 'Chrome', 'India', '1546041600'),
(24, 4, '106.207.208.255', 'Chrome', 'India', '1546041600'),
(25, 10, '182.64.174.197', 'Chrome', 'India', '1546300800'),
(26, 13, '103.66.73.15', 'Chrome', 'India', '1546387200'),
(27, 13, '103.66.73.15', 'Chrome', 'India', '1546387200'),
(28, 1, '103.66.73.15', 'Chrome', 'India', '1546387200'),
(29, 10, '223.189.190.241', 'Chrome', 'India', '1546387200'),
(30, 10, '182.64.174.197', 'Chrome', 'India', '1546473600'),
(31, 1, '103.66.73.15', 'Chrome', 'India', '1546473600'),
(32, 1, '103.66.73.15', 'Chrome', 'India', '1546560000'),
(33, 10, '117.97.167.97', 'Chrome', 'India', '1546560000'),
(34, 11, '103.66.73.15', 'Chrome', 'India', '1546646400'),
(35, 10, '42.107.227.192', 'Chrome', 'India', '1546732800'),
(36, 16, '150.107.189.195', 'Chrome', 'India', '1546732800'),
(37, 16, '150.107.189.195', 'Chrome', 'India', '1546732800'),
(38, 10, '117.97.167.97', 'Chrome', 'India', '1546819200'),
(39, 1, '103.66.73.15', 'Chrome', 'India', '1546819200'),
(40, 10, '117.97.167.97', 'Chrome', 'India', '1546819200'),
(41, 1, '103.66.73.15', 'Chrome', 'India', '1546819200'),
(42, 10, '117.97.167.97', 'Chrome', 'India', '1546819200'),
(43, 1, '27.54.163.31', 'Chrome', 'India', '1546905600'),
(44, 10, '117.97.167.97', 'Chrome', 'India', '1546905600'),
(45, 1, '27.54.163.31', 'Chrome', 'India', '1546992000'),
(46, 1, '27.54.163.31', 'Chrome', 'India', '1546992000'),
(47, 19, '182.64.60.234', 'Chrome', 'India', '1546992000'),
(48, 16, '43.224.1.156', 'Chrome', 'India', '1546992000'),
(49, 16, '43.224.1.156', 'Chrome', 'India', '1546992000'),
(50, 1, '103.66.73.15', 'Chrome', 'India', '1547078400'),
(51, 19, '182.68.57.145', 'Safari', 'India', '1547078400'),
(52, 16, '150.107.189.192', 'Chrome', 'India', '1547078400'),
(53, 10, '182.68.57.145', 'Chrome', 'India', '1547078400'),
(54, 16, '150.107.189.192', 'Chrome', 'India', '1547078400'),
(55, 1, '103.82.80.162', 'Chrome', 'India', '1547164800'),
(56, 1, '103.82.80.162', 'Chrome', 'India', '1547164800'),
(57, 19, '182.68.57.145', 'Safari', 'India', '1547164800'),
(58, 16, '150.107.189.192', 'Chrome', 'India', '1547164800'),
(59, 10, '182.68.57.145', 'Chrome', 'India', '1547164800'),
(60, 1, '103.82.80.239', 'Chrome', 'India', '1547164800'),
(61, 19, '182.68.57.145', 'Chrome', 'India', '1547251200'),
(62, 16, '182.68.57.145', 'Chrome', 'India', '1547424000'),
(63, 16, '43.224.1.170', 'Chrome', 'India', '1548028800'),
(64, 16, '43.224.1.151', 'Chrome', 'India', '1548115200'),
(65, 19, '117.97.185.88', 'Chrome', 'India', '1548201600'),
(66, 10, '117.97.185.88', 'Chrome', 'India', '1548201600'),
(67, 19, '103.66.73.15', 'Chrome', 'India', '1548460800'),
(68, 10, '103.66.73.15', 'Chrome', 'India', '1548633600'),
(69, 10, '103.66.73.15', 'Chrome', 'India', '1548720000'),
(70, 10, '117.97.131.246', 'Chrome', 'India', '1548806400'),
(71, 10, '103.66.73.15', 'Chrome', 'India', '1548892800'),
(72, 10, '103.66.73.15', 'Chrome', 'India', '1548892800'),
(73, 10, '117.97.131.246', 'Chrome', 'India', '1548979200'),
(74, 19, '103.66.73.15', 'Chrome', 'India', '1549065600'),
(75, 16, '43.224.1.173', 'Chrome', 'India', '1549152000'),
(76, 10, '117.97.131.246', 'Safari', 'India', '1549238400'),
(77, 10, '117.97.131.246', 'Chrome', 'India', '1549324800'),
(78, 16, '43.224.1.149', 'Chrome', 'India', '1549324800'),
(79, 10, '117.97.131.246', 'Chrome', 'India', '1549411200'),
(80, 19, '103.66.73.15', 'Chrome', 'India', '1549497600'),
(81, 19, '182.64.161.245', 'Chrome', 'India', '1549670400'),
(82, 19, '103.66.73.15', 'Chrome', 'India', '1549843200'),
(83, 19, '103.66.73.15', 'Chrome', 'India', '1549929600'),
(84, 19, '182.64.161.245', 'Chrome', 'India', '1549929600'),
(85, 19, '103.66.73.15', 'Chrome', 'India', '1550102400'),
(86, 16, '150.107.189.214', 'Chrome', 'India', '1550188800'),
(87, 16, '150.107.189.214', 'Chrome', 'India', '1550275200'),
(88, 19, '103.66.73.15', 'Chrome', 'India', '1550275200'),
(89, 19, '103.66.73.15', 'Chrome', 'India', '1550448000'),
(90, 19, '182.64.161.245', 'Chrome', 'India', '1550534400'),
(91, 19, '182.64.161.245', 'Safari', 'India', '1550707200'),
(92, 19, '106.212.145.164', 'Chrome', 'India', '1550793600'),
(93, 19, '103.66.73.15', 'Chrome', 'India', '1550793600'),
(94, 19, '103.66.73.15', 'Chrome', 'India', '1551052800'),
(95, 19, '110.227.145.18', 'Chrome', 'India', '1551139200'),
(96, 19, '110.227.145.18', 'Chrome', 'India', '1551139200'),
(97, 19, '103.66.73.15', 'Chrome', 'India', '1551139200'),
(98, 19, '110.227.145.18', 'Chrome', 'India', '1551139200'),
(99, 16, '171.79.183.232', 'Chrome', 'India', '1551139200'),
(100, 19, '110.227.145.18', 'Chrome', 'India', '1551225600'),
(101, 19, '103.66.73.15', 'Chrome', 'India', '1551312000'),
(102, 19, '103.66.73.15', 'Chrome', 'India', '1551398400'),
(103, 19, '110.227.145.18', 'Chrome', 'India', '1551398400'),
(104, 19, '117.247.227.14', 'Firefox', 'India', '1551398400'),
(105, 19, '103.66.73.15', 'Chrome', 'India', '1551484800'),
(106, 19, '103.66.73.15', 'Chrome', 'India', '1551657600'),
(107, 19, '150.242.175.185', 'Chrome', 'India', '1551744000'),
(108, 16, '171.79.179.242', 'Chrome', 'India', '1551830400'),
(109, 19, '27.6.194.33', 'Chrome', 'India', '1551830400'),
(110, 19, '103.66.73.15', 'Chrome', 'India', '1552003200'),
(111, 16, '223.188.65.131', 'Chrome', 'India', '1552089600'),
(112, 19, '182.64.49.209', 'Safari', 'India', '1552348800'),
(113, 19, '103.66.73.15', 'Chrome', 'India', '1552435200'),
(114, 19, '103.66.73.15', 'Chrome', 'India', '1552435200'),
(115, 19, '103.66.73.15', 'Chrome', 'India', '1552521600'),
(116, 19, '103.66.73.15', 'Chrome', 'India', '1552521600'),
(117, 16, '223.178.157.177', 'Chrome', 'India', '1552521600'),
(118, 19, '103.66.73.15', 'Chrome', 'India', '1552608000'),
(119, 19, '103.66.73.15', 'Chrome', 'India', '1552608000'),
(120, 19, '103.66.73.15', 'Chrome', 'India', '1552694400'),
(121, 19, '182.64.49.209', 'Chrome', 'India', '1552694400'),
(122, 16, '223.188.211.113', 'Chrome', 'India', '1552694400'),
(123, 16, '223.188.227.206', 'Chrome', 'India', '1552780800'),
(124, 19, '171.79.77.209', 'Chrome', 'India', '1552867200'),
(125, 19, '171.79.77.209', 'Chrome', 'India', '1552867200'),
(126, 19, '171.79.77.209', 'Chrome', 'India', '1552867200'),
(127, 19, '171.79.77.209', 'Chrome', 'India', '1553212800'),
(128, 19, '49.36.150.29', 'Chrome', 'India', '1553558400'),
(129, 19, '110.227.152.51', 'Chrome', 'India', '1553644800'),
(130, 19, '110.227.152.51', 'Chrome', 'India', '1553644800'),
(131, 19, '110.227.152.51', 'Chrome', 'India', '1553644800'),
(132, 19, '110.227.152.51', 'Chrome', 'India', '1553644800'),
(133, 19, '110.227.152.51', 'Chrome', 'India', '1553644800'),
(134, 16, '103.82.80.212', 'Chrome', 'India', '1553644800'),
(135, 19, '110.227.152.51', 'Chrome', 'India', '1553731200'),
(136, 19, '110.227.152.51', 'Chrome', 'India', '1553731200'),
(137, 19, '110.227.152.51', 'Chrome', 'India', '1553731200'),
(138, 19, '110.227.152.51', 'Chrome', 'India', '1553731200'),
(139, 19, '110.227.152.51', 'Chrome', 'India', '1553731200'),
(140, 19, '110.227.152.51', 'Chrome', 'India', '1553731200'),
(141, 19, '182.64.187.203', 'Chrome', 'India', '1553990400'),
(142, 19, '182.64.187.203', 'Chrome', 'India', '1553990400'),
(143, 19, '182.64.187.203', 'Chrome', 'India', '1553990400'),
(144, 19, '182.64.187.203', 'Chrome', 'India', '1553990400'),
(145, 19, '182.64.187.203', 'Chrome', 'India', '1553990400'),
(146, 19, '182.64.175.242', 'Chrome', 'India', '1554336000'),
(147, 19, '103.66.73.15', 'Chrome', 'India', '1555545600'),
(148, 19, '103.66.73.15', 'Chrome', 'India', '1555718400'),
(149, 19, '182.68.98.254', 'Chrome', 'India', '1555891200'),
(150, 19, '117.247.227.14', 'Chrome', 'India', '1555891200'),
(151, 19, '117.247.227.14', 'Chrome', 'India', '1555891200'),
(152, 19, '182.68.98.254', 'Chrome', 'India', '1555977600'),
(153, 19, '182.68.98.254', 'Chrome', 'India', '1556150400'),
(154, 19, '182.68.98.254', 'Chrome', 'India', '1556150400'),
(155, 30, '182.68.68.15', 'Chrome', 'India', '1556150400'),
(156, 19, '103.66.73.15', 'Chrome', 'India', '1556236800'),
(157, 19, '103.66.73.15', 'Chrome', 'India', '1556236800'),
(158, 19, '182.68.98.254', 'Chrome', 'India', '1556409600'),
(159, 19, '103.66.73.15', 'Chrome', 'India', '1556582400'),
(160, 19, '182.68.98.254', 'Chrome', 'India', '1556582400'),
(161, 19, '182.68.98.254', 'Safari', 'India', '1556582400'),
(162, 19, '182.68.98.254', 'Chrome', 'India', '1556582400'),
(163, 19, '182.68.98.254', 'Chrome', 'India', '1556841600'),
(164, 19, '182.68.98.254', 'Chrome', 'India', '1556928000'),
(165, 19, '103.66.73.15', 'Chrome', 'India', '1556928000'),
(166, 19, '182.68.98.254', 'Chrome', 'India', '1557187200'),
(167, 19, '103.66.73.15', 'Chrome', 'India', '1557187200'),
(168, 19, '182.68.98.254', 'Chrome', 'India', '1557187200'),
(169, 19, '182.68.98.254', 'Chrome', 'India', '1557273600'),
(170, 19, '223.188.162.135', 'Chrome', 'India', '1557532800'),
(171, 19, '117.97.132.131', 'Chrome', 'India', '1557705600'),
(172, 19, '122.161.214.24', 'Chrome', 'India', '1557964800'),
(173, 19, '122.161.214.24', 'Chrome', 'India', '1558051200'),
(174, 19, '103.66.73.15', 'Chrome', 'India', '1558051200'),
(175, 19, '103.66.73.15', 'Chrome', 'India', '1558137600'),
(176, 19, '182.64.40.237', 'Safari', 'India', '1558137600'),
(177, 19, '182.64.40.237', 'Chrome', 'India', '1558137600'),
(178, 19, '117.99.170.121', 'Chrome', 'India', '1558310400'),
(179, 19, '103.66.73.15', 'Chrome', 'India', '1558310400'),
(180, 19, '122.162.211.10', 'Chrome', 'India', '1558483200'),
(181, 19, '122.162.211.10', 'Chrome', 'India', '1558569600'),
(182, 19, '103.66.73.15', 'Chrome', 'India', '1558569600'),
(183, 19, '171.79.26.109', 'Chrome', 'India', '1558569600'),
(184, 19, '122.162.211.120', 'Chrome', 'India', '1558828800'),
(185, 19, '103.66.73.15', 'Chrome', 'India', '1559174400'),
(186, 19, '49.36.148.123', 'Chrome', 'India', '1559174400'),
(187, 19, '103.66.73.15', 'Chrome', 'India', '1559260800'),
(188, 19, '117.97.137.85', 'Chrome', 'India', '1559347200'),
(189, 19, '103.66.73.15', 'Chrome', 'India', '1559606400'),
(190, 19, '182.68.95.145', 'Chrome', 'India', '1559606400'),
(191, 19, '182.68.95.145', 'Chrome', 'India', '1559779200'),
(192, 19, '103.66.73.15', 'Chrome', 'India', '1559779200'),
(193, 19, '182.68.95.145', 'Chrome', 'India', '1559865600'),
(194, 19, '122.252.245.38', 'Chrome', 'India', '1559865600'),
(195, 19, '122.252.245.38', 'Chrome', 'India', '1560124800'),
(196, 19, '182.64.216.69', 'Chrome', 'India', '1560124800'),
(197, 19, '182.64.216.69', 'Chrome', 'India', '1560124800'),
(198, 19, '182.64.216.69', 'Safari', 'India', '1560124800'),
(199, 19, '182.68.230.139', 'Chrome', 'India', '1560211200'),
(200, 19, '122.252.245.38', 'Chrome', 'India', '1560211200'),
(201, 19, '122.252.245.38', 'Chrome', 'India', '1560556800'),
(202, 19, '203.188.228.7', 'Safari', 'India', '1560729600'),
(203, 19, '59.153.121.143', 'Chrome', 'India', '1560816000'),
(204, 19, '13.234.74.140', 'Chrome', 'India', '1561075200'),
(205, 19, '122.252.245.38', 'Chrome', 'India', '1561075200'),
(206, 19, '103.117.124.100', 'Safari', 'India', '1561075200'),
(207, 19, '122.252.245.38', 'Chrome', 'India', '1561075200'),
(208, 19, '203.188.228.8', 'Safari', 'India', '1561248000'),
(209, 19, '203.188.228.8', 'Safari', 'India', '1561248000'),
(210, 19, '203.188.228.8', 'Safari', 'India', '1561507200'),
(211, 34, '103.117.124.100', 'Safari', 'India', '1561593600'),
(212, 19, '103.66.73.15', 'Chrome', 'India', '1561593600'),
(213, 19, '103.117.124.100', 'Safari', 'India', '1561593600'),
(214, 34, '49.32.209.30', 'Chrome', 'India', '1561593600'),
(215, 34, '49.32.207.222', 'Chrome', 'India', '1561766400'),
(216, 19, '182.64.211.131', 'Chrome', 'India', '1562889600'),
(217, 19, '117.99.170.133', 'Chrome', 'India', '1563235200'),
(218, 16, '43.224.1.178', 'Chrome', 'India', '1563321600'),
(219, 19, '103.66.73.15', 'Chrome', 'India', '1563408000'),
(220, 19, '223.189.167.190', 'Safari', 'India', '1563580800'),
(221, 19, '103.66.73.15', 'Chrome', 'India', '1563580800'),
(222, 19, '182.68.182.228', 'Chrome', 'India', '1563580800'),
(223, 19, '182.68.182.228', 'Chrome', 'India', '1563840000'),
(224, 19, '182.68.182.228', 'Chrome', 'India', '1564012800'),
(225, 19, '182.68.182.228', 'Chrome', 'India', '1564012800'),
(226, 19, '182.68.182.228', 'Chrome', 'India', '1564185600'),
(227, 19, '103.66.73.15', 'Chrome', 'India', '1564963200'),
(228, 19, '103.66.73.15', 'Chrome', 'India', '1564963200'),
(229, 19, '122.161.93.207', 'Chrome', 'India', '1565049600'),
(230, 19, '103.66.73.15', 'Chrome', 'India', '1565049600'),
(231, 19, '103.66.73.15', 'Chrome', 'India', '1565049600'),
(232, 34, '223.189.1.29', 'Chrome', 'India', '1565395200'),
(233, 19, '122.161.208.164', 'Chrome', 'India', '1568419200'),
(234, 41, '117.203.16.118', 'Chrome', 'India', '1568678400'),
(235, 41, '117.203.16.118', 'Chrome', 'India', '1568678400'),
(236, 19, '150.242.63.3', 'Chrome', 'India', '1568937600'),
(237, 10, '150.242.87.157', 'Chrome', 'India', '1569369600'),
(238, 19, '150.242.87.157', 'Chrome', 'India', '1569369600'),
(239, 19, '103.66.73.15', 'Chrome', 'India', '1569369600'),
(240, 10, '150.242.175.109', 'Chrome', 'India', '1569456000'),
(241, 41, '117.203.3.83', 'Chrome', 'India', '1569801600'),
(242, 47, '::1', 'Chrome', '', '1569967200'),
(243, 19, '49.206.179.96', 'Chrome', 'India', '1574553600'),
(244, 19, '157.37.178.85', 'Chrome', 'India', '1574553600'),
(245, 19, '49.206.184.136', 'Chrome', 'India', '1574726400'),
(246, 19, '49.206.187.118', 'Chrome', 'India', '1575072000'),
(247, 19, '49.206.187.118', 'Chrome', 'India', '1575072000'),
(248, 19, '49.206.187.118', 'Safari', 'India', '1575158400'),
(249, 19, '103.73.35.79', 'Chrome', 'India', '1575158400'),
(250, 19, '49.206.179.213', 'Chrome', 'India', '1575590400'),
(251, 19, '49.206.179.213', 'Chrome', 'India', '1575676800'),
(252, 19, '49.206.179.213', 'Chrome', 'India', '1575763200'),
(253, 19, '49.206.185.18', 'Chrome', 'India', '1576281600'),
(254, 19, '49.206.185.18', 'Chrome', 'India', '1576368000'),
(255, 19, '49.206.185.18', 'Chrome', 'India', '1576454400'),
(256, 19, '49.206.185.18', 'Chrome', 'India', '1576454400'),
(257, 10, '49.206.185.18', 'Chrome', 'India', '1577059200'),
(258, 19, '49.206.180.109', 'Chrome', 'India', '1579305600'),
(259, 19, '150.129.239.193', 'Chrome', 'India', '1579651200');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_business_category`
--
ALTER TABLE `app_business_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `houdin_admin_contact`
--
ALTER TABLE `houdin_admin_contact`
  ADD PRIMARY KEY (`houdin_admin_contact_id`);

--
-- Indexes for table `houdin_admin_department`
--
ALTER TABLE `houdin_admin_department`
  ADD PRIMARY KEY (`houdin_admin_department_id`);

--
-- Indexes for table `houdin_admin_logo`
--
ALTER TABLE `houdin_admin_logo`
  ADD PRIMARY KEY (`houdin_admin_logo_id`);

--
-- Indexes for table `houdin_admin_transaction`
--
ALTER TABLE `houdin_admin_transaction`
  ADD PRIMARY KEY (`houdin_admin_transaction_id`);

--
-- Indexes for table `houdin_admin_users`
--
ALTER TABLE `houdin_admin_users`
  ADD PRIMARY KEY (`houdin_admin_user_id`);

--
-- Indexes for table `houdin_admin_user_second_auth`
--
ALTER TABLE `houdin_admin_user_second_auth`
  ADD PRIMARY KEY (`houdin_second_auth_id`);

--
-- Indexes for table `houdin_business_categories`
--
ALTER TABLE `houdin_business_categories`
  ADD PRIMARY KEY (`houdin_business_category_id`);

--
-- Indexes for table `houdin_countries`
--
ALTER TABLE `houdin_countries`
  ADD PRIMARY KEY (`houdin_country_id`);

--
-- Indexes for table `houdin_coupons`
--
ALTER TABLE `houdin_coupons`
  ADD PRIMARY KEY (`houdin_coupon_id`);

--
-- Indexes for table `houdin_currencies`
--
ALTER TABLE `houdin_currencies`
  ADD PRIMARY KEY (`houdin_currency_id`);

--
-- Indexes for table `houdin_deleted_users`
--
ALTER TABLE `houdin_deleted_users`
  ADD PRIMARY KEY (`houdin_deleted_user_id`);

--
-- Indexes for table `houdin_email_package`
--
ALTER TABLE `houdin_email_package`
  ADD PRIMARY KEY (`houdin_email_package_id`);

--
-- Indexes for table `houdin_languages`
--
ALTER TABLE `houdin_languages`
  ADD PRIMARY KEY (`houdin_language_id`);

--
-- Indexes for table `houdin_packages`
--
ALTER TABLE `houdin_packages`
  ADD PRIMARY KEY (`houdin_package_id`);

--
-- Indexes for table `houdin_package_plugins`
--
ALTER TABLE `houdin_package_plugins`
  ADD PRIMARY KEY (`houdin_package_plugin_id`);

--
-- Indexes for table `houdin_payumoney`
--
ALTER TABLE `houdin_payumoney`
  ADD PRIMARY KEY (`houdin_payumoney`);

--
-- Indexes for table `houdin_plugins`
--
ALTER TABLE `houdin_plugins`
  ADD PRIMARY KEY (`houdin_plugin_id`);

--
-- Indexes for table `houdin_products`
--
ALTER TABLE `houdin_products`
  ADD PRIMARY KEY (`houdin_products_id`);

--
-- Indexes for table `houdin_products_variants`
--
ALTER TABLE `houdin_products_variants`
  ADD PRIMARY KEY (`houdin_products_variants_id`);

--
-- Indexes for table `houdin_product_attributers`
--
ALTER TABLE `houdin_product_attributers`
  ADD PRIMARY KEY (`houdin_product_att_id`);

--
-- Indexes for table `houdin_product_types`
--
ALTER TABLE `houdin_product_types`
  ADD PRIMARY KEY (`houdin_product_type_id`);

--
-- Indexes for table `houdin_product_type_attributes`
--
ALTER TABLE `houdin_product_type_attributes`
  ADD PRIMARY KEY (`houdin_product_type_attribute_id`);

--
-- Indexes for table `houdin_roles`
--
ALTER TABLE `houdin_roles`
  ADD PRIMARY KEY (`houdin_role_id`);

--
-- Indexes for table `houdin_sendgrid`
--
ALTER TABLE `houdin_sendgrid`
  ADD PRIMARY KEY (`houdin_sendgrid_id`);

--
-- Indexes for table `houdin_sms_package`
--
ALTER TABLE `houdin_sms_package`
  ADD PRIMARY KEY (`houdin_sms_package_id`);

--
-- Indexes for table `houdin_tax`
--
ALTER TABLE `houdin_tax`
  ADD PRIMARY KEY (`houdin_tax_id`);

--
-- Indexes for table `houdin_templates`
--
ALTER TABLE `houdin_templates`
  ADD PRIMARY KEY (`houdin_template_id`);

--
-- Indexes for table `houdin_twilio`
--
ALTER TABLE `houdin_twilio`
  ADD PRIMARY KEY (`houdin_twilio_id`);

--
-- Indexes for table `houdin_users`
--
ALTER TABLE `houdin_users`
  ADD PRIMARY KEY (`houdin_user_id`),
  ADD UNIQUE KEY `houdin_user_email` (`houdin_user_email`);

--
-- Indexes for table `houdin_user_auth`
--
ALTER TABLE `houdin_user_auth`
  ADD PRIMARY KEY (`houdin_user_auth_id`);

--
-- Indexes for table `houdin_user_forgot_password`
--
ALTER TABLE `houdin_user_forgot_password`
  ADD PRIMARY KEY (`houdin_user_forgot_password_id`);

--
-- Indexes for table `houdin_user_logs`
--
ALTER TABLE `houdin_user_logs`
  ADD PRIMARY KEY (`houdin_user_log_id`);

--
-- Indexes for table `houdin_user_payments`
--
ALTER TABLE `houdin_user_payments`
  ADD PRIMARY KEY (`houdin_user_payment_id`);

--
-- Indexes for table `houdin_user_payment_tracks`
--
ALTER TABLE `houdin_user_payment_tracks`
  ADD PRIMARY KEY (`houdin_user_payment_track_id`);

--
-- Indexes for table `houdin_user_second_auth`
--
ALTER TABLE `houdin_user_second_auth`
  ADD PRIMARY KEY (`houdin_user_second_auth_id`);

--
-- Indexes for table `houdin_user_shops`
--
ALTER TABLE `houdin_user_shops`
  ADD PRIMARY KEY (`houdin_user_shop_id`);

--
-- Indexes for table `houdin_vendor_auth`
--
ALTER TABLE `houdin_vendor_auth`
  ADD PRIMARY KEY (`houdin_vendor_auth_id`);

--
-- Indexes for table `houdin_vendor_staff`
--
ALTER TABLE `houdin_vendor_staff`
  ADD PRIMARY KEY (`vendor_staff`);

--
-- Indexes for table `houdin_vendor_user_log`
--
ALTER TABLE `houdin_vendor_user_log`
  ADD PRIMARY KEY (`houdin_vendor_user_log_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_business_category`
--
ALTER TABLE `app_business_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `houdin_admin_contact`
--
ALTER TABLE `houdin_admin_contact`
  MODIFY `houdin_admin_contact_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `houdin_admin_department`
--
ALTER TABLE `houdin_admin_department`
  MODIFY `houdin_admin_department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `houdin_admin_logo`
--
ALTER TABLE `houdin_admin_logo`
  MODIFY `houdin_admin_logo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `houdin_admin_transaction`
--
ALTER TABLE `houdin_admin_transaction`
  MODIFY `houdin_admin_transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `houdin_admin_users`
--
ALTER TABLE `houdin_admin_users`
  MODIFY `houdin_admin_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `houdin_admin_user_second_auth`
--
ALTER TABLE `houdin_admin_user_second_auth`
  MODIFY `houdin_second_auth_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `houdin_business_categories`
--
ALTER TABLE `houdin_business_categories`
  MODIFY `houdin_business_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `houdin_countries`
--
ALTER TABLE `houdin_countries`
  MODIFY `houdin_country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `houdin_coupons`
--
ALTER TABLE `houdin_coupons`
  MODIFY `houdin_coupon_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `houdin_currencies`
--
ALTER TABLE `houdin_currencies`
  MODIFY `houdin_currency_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `houdin_deleted_users`
--
ALTER TABLE `houdin_deleted_users`
  MODIFY `houdin_deleted_user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `houdin_email_package`
--
ALTER TABLE `houdin_email_package`
  MODIFY `houdin_email_package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `houdin_languages`
--
ALTER TABLE `houdin_languages`
  MODIFY `houdin_language_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `houdin_packages`
--
ALTER TABLE `houdin_packages`
  MODIFY `houdin_package_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `houdin_package_plugins`
--
ALTER TABLE `houdin_package_plugins`
  MODIFY `houdin_package_plugin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `houdin_payumoney`
--
ALTER TABLE `houdin_payumoney`
  MODIFY `houdin_payumoney` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `houdin_plugins`
--
ALTER TABLE `houdin_plugins`
  MODIFY `houdin_plugin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `houdin_products`
--
ALTER TABLE `houdin_products`
  MODIFY `houdin_products_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `houdin_products_variants`
--
ALTER TABLE `houdin_products_variants`
  MODIFY `houdin_products_variants_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `houdin_product_attributers`
--
ALTER TABLE `houdin_product_attributers`
  MODIFY `houdin_product_att_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `houdin_product_types`
--
ALTER TABLE `houdin_product_types`
  MODIFY `houdin_product_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `houdin_product_type_attributes`
--
ALTER TABLE `houdin_product_type_attributes`
  MODIFY `houdin_product_type_attribute_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `houdin_sendgrid`
--
ALTER TABLE `houdin_sendgrid`
  MODIFY `houdin_sendgrid_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `houdin_sms_package`
--
ALTER TABLE `houdin_sms_package`
  MODIFY `houdin_sms_package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `houdin_tax`
--
ALTER TABLE `houdin_tax`
  MODIFY `houdin_tax_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `houdin_templates`
--
ALTER TABLE `houdin_templates`
  MODIFY `houdin_template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `houdin_twilio`
--
ALTER TABLE `houdin_twilio`
  MODIFY `houdin_twilio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `houdin_users`
--
ALTER TABLE `houdin_users`
  MODIFY `houdin_user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `houdin_user_auth`
--
ALTER TABLE `houdin_user_auth`
  MODIFY `houdin_user_auth_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `houdin_user_forgot_password`
--
ALTER TABLE `houdin_user_forgot_password`
  MODIFY `houdin_user_forgot_password_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `houdin_user_logs`
--
ALTER TABLE `houdin_user_logs`
  MODIFY `houdin_user_log_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `houdin_user_second_auth`
--
ALTER TABLE `houdin_user_second_auth`
  MODIFY `houdin_user_second_auth_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `houdin_user_shops`
--
ALTER TABLE `houdin_user_shops`
  MODIFY `houdin_user_shop_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `houdin_vendor_auth`
--
ALTER TABLE `houdin_vendor_auth`
  MODIFY `houdin_vendor_auth_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=466;

--
-- AUTO_INCREMENT for table `houdin_vendor_staff`
--
ALTER TABLE `houdin_vendor_staff`
  MODIFY `vendor_staff` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `houdin_vendor_user_log`
--
ALTER TABLE `houdin_vendor_user_log`
  MODIFY `houdin_vendor_user_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
