<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php');
$getCurrency = getVendorCurrency();
if($getCurrency[0]->houdin_users_currency=="USD")
{
  $currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="AUD"){
  $currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="Euro"){
  $currencysymbol= "£";
}else if($getCurrency[0]->houdin_users_currency=="Pound"){
  $currencysymbol= "€";
}else if($getCurrency[0]->houdin_users_currency=="INR"){
  $currencysymbol= "₹";
}
?>
<style type="text/css">
.zoom img{

    transition-duration: 5s;
    margin: 0 auto;
}
img {
    vertical-align: middle;
    height: 40px;
    width: auto;
}

.zoom {
    transition: all 1s;
    -ms-transition: all 1s;
    -webkit-transition: all 1s;
    margin-top: 0px;
    padding-top: 0px;

}
.zoom:hover {
    -ms-transform: scale(2); /* IE 9 */
    -webkit-transform: scale(2); /* Safari 3-8 */
    transform: scale(2);
    margin-left: 40px;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
<!--START Page-Title -->
            <div class="row">
                <div class="col-md-8">
                   <h4 class="page-title">Product</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>

                  <li class="active">Product</li>
                  </ol>
                  </div>
            </div>
            <div class="row">
        <div class="col-sm-12">
        <?php
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
           <!--END Page-Title -->
            <!-- <div class="row m-t-20">
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="widget-panel widget-style-2 bg-white">
                                                <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Products.png">
                                                <h2 class="m-0 text-dark counter font-600"><?php echo $allStatus['AllProduct']; ?></h2>
                                                <div class="text-muted m-t-5">Total product</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="widget-panel widget-style-2 bg-white">
                                                <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Products.png">
                                                <h2 class="m-0 text-dark counter font-600"><?php echo $allStatus['AllActive']; ?></h2>
                                                <div class="text-muted m-t-5">Active Product </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="widget-panel widget-style-2 bg-white">
                                                <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Products.png">
                                                <h2 class="m-0 text-dark counter font-600"><?php echo $allStatus['AllDective']; ?></h2>
                                                <div class="text-muted m-t-5">Total Deactive Product</div>
                                            </div>
                                        </div>

                                      </div> -->


        <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">
              <div class="btn-group pull-right m-t-10 m-b-20">
              <?php
              $getUserPlan = getVendorplan();
              if(count($getUserPlan) > 0)
              {
                if($getUserPlan[0]->houdin_package_product > count($all))
                {
                ?>
                <a href="<?php echo base_url(); ?>Product/addproduct" class="btn btn-default m-r-5" title="Add Product" ><i class="fa fa-plus"></i></a>
                <?php }
              }
              else
              {
            ?>
            <a href="<?php echo base_url(); ?>Product/addproduct" class="btn btn-default m-r-5" title="Add Product" ><i class="fa fa-plus"></i></a>
             <?php }
              ?>
                  <!-- <button type="button" class="btn btn-default m-r-5" title="Delete Product" data-toggle="modal" data-target="#delete_product"><i class="fa fa-trash"></i></button> -->
                  <a href="<?php echo base_url(); ?>Product/exportdata" target="_blank" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
                 <!-- <button type="button" class="btn btn-default m-r-5" title="Export To CSV"><i class="fa fa-file-excel-o"></i></button>-->
                  <button type="button" class="btn btn-default m-r-5" title="Import Bulk" data-toggle="modal" data-target="#import_bulk"><i class="fa fa-cloud-download"></i></button>

              </div>
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>

                    <th style="width: 15%">Item</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                <?php foreach($all as $product)
                {
                  $inventory_total_stock = getVariantTotalstock($product['houdin_products_id']);
                    $image =json_decode($product['houdinv_products_main_images'],true);

                    $price = json_decode($product['houdin_products_price'],true);

                    ?>

                  <tr>


                     <td><?php 
                     if($image[0]) 
                     {
                      ?>
                      <img src="<?php echo base_url(); ?>upload/productImage/<?php echo $image[0];  ?>"><br/><?php echo $product['houdin_products_title'] ?>
                     <?php }
                     else
                     {
                      ?>
                      <img src="<?php echo base_url(); ?>images/no.png"/><br/><?php echo $product['houdin_products_title'] ?>
                     <?php }
                     ?>
                     
                     </td>
                    <td><?php echo $currencysymbol;?><?php echo ($price['price']);  ?></td>
                    <td><?php echo $product['houdinv_products_total_stocks'] + $inventory_total_stock;  ?></td>

                    <td><button type="button" class="btn btn-success btn btn-xs">
                    <?php if($product['houdin_products_status'] ==1)
                    {
                        echo "Active";

                    } else
                    {
                        echo "Deactive";
                    }?>
                    </button></td>
                    <td>
                      <a href="<?php echo base_url(); ?>Product/editproduct/<?php echo $product['houdin_products_id']; ?>" class="btn btn-info ">Edit</a>
                      <!-- <button type="button" class="btn btn-info delete_tab"  data-id="<?php //echo $product['houdin_products_id']; ?>" data-toggle="modal" data-target="#delete_product">Delete</button> -->
                      <button type="button" class="btn btn-info status_tab" data-id="<?php echo $product['houdin_products_id']; ?>"  data-toggle="modal" data-target="#Change-status">Change Status</button>
                    </td>
                  </tr>

                    <?php
                }?>
                </tbody>
              </table>

              <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--Delete-->

  <div id="delete_product" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
      <?php echo form_open(base_url().'Product/',array("id"=>"DeleteProduct","enctype"=>"multipart/form-data")); ?>

          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete product</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this product ?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
               <input type="hidden" name="delete_id" id="delete_id" />
          <input type="submit" class="btn btn-info" name="DeleteEntry" value="Delete">

          </div>

          </div>
       <?php echo form_close(); ?>
          </div>

          </div>
<!--change Status-->
<div id="Change-status" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
<?php echo form_open(base_url().'Product/',array("id"=>"ChangeProduct","enctype"=>"multipart/form-data")); ?>

        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Change Status</h4>

        </div>

        <div class="modal-body">
            <div class="row">
              <div class="col-md-12">

                <div class="form-group no-margin">

                <label for="field-7" class="control-label">Change Status</label>

                <select class="form-control " name="status_value">
                <option value="">Choose Status</option>
                <option value="1">Active</option>
                <option value="0">Deactive</option>
                <option value="2">Block</option></select>

                <input type="hidden" name="status_id" id="status_id" />

                </div>

              </div>
            </div>

        </div>

        <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info " name="ChangeEntry" value="Update Status">

        </div>

        </div>
       <?php echo form_close(); ?>
        </div>

        </div>

<!--Import Bulk-->
<div id="import_bulk" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <?php echo form_open(base_url('csv/customerimportcsv'), array( 'id' => 'importcsv', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>

        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Import Bulk Products</h4>

        </div>

        <div class="modal-body">
        <div class="row">
   <div class="col-md-8">
      <div class="form-group">
         <label for="field-7" class="control-label">Choose CSV File</label>
         <input type="file" name="userfile" required class="required_validation_for_add_single_tag" >
      </div>
   </div>
   <div class="col-md-4"> <label for="field-7" class="control-label">Products sample CSV</label>
      <a href="upload/Products sample.csv" download >Download</a>
   </div>
</div>
        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info" name="" value="Save">

        </div>

        </div>
        </div><?php echo form_close(); ?>
        </div>

        </div>


<?php $this->load->view('Template/footer.php') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<script>
$(document).on("click",".status_tab",function()
{

  $id_status = $(this).attr("data-id");
  $("#status_id").val($id_status);

});

$(document).on("click",".delete_tab",function()
{

  $id_delete = $(this).attr("data-id");
   $("#delete_id").val($id_delete);

});

</script>
