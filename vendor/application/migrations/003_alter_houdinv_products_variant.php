<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_houdinv_products_variant extends CI_Migration {
    
    /**
     * construct function
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }
    
    public function up()
    {
        $fields = array(
            'houdin_products_variants_new' => [
                    'name' => 'houdin_products_variants_new',
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => true,
                    'default' => null
            ],
            'houdinv_products_variants_barcode' => [
                'name' => 'houdinv_products_variants_barcode',
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => true,
                'default' => null
        ],
        'houdinv_products_variants_new_price' => [
            'name' => 'houdinv_products_variants_new_price',
            'type' => 'VARCHAR',
            'constraint' => '255',
            'null' => true,
            'default' => null
        ],
        'houdin_products_variants_default' => [
            'name' => 'houdin_products_variants_default',
            'type' => 'int',
            'constraint' => '1',
            'null' => true,
            'default' => null
        ],
    );
    $this->dbforge->modify_column('houdinv_products_variants', $fields);
    }

    public function down()
    {
    }
}