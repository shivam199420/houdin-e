<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Webserviceproductsearch extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopproductsearchmodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function index()
    {
         $this->form_validation->set_rules('shopName','text','required');
         $this->form_validation->set_rules('keyword','text','required');
         if($this->form_validation->run() == true)
         {
            $setArraySearch = array('shopname'=>$this->input->post('shopName'),'keyword'=>$this->input->post('keyword'));
            $getProductSearchData = $this->Shopproductsearchmodel->searchProduct($setArraySearch);
            print_r(json_encode($getProductSearchData));
         }
         else
         {
            $resultArray = array('message'=>'All fileds are mandatory');
            print_r(json_encode($resultArray));
         }
    }
}
