<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Delivery extends CI_Controller 
{
    function __construct()
	{
        parent::__construct();
        $this->auth_users->is_logged_in();
        $this->perPage = 100;
        $this->load->model('Deliverymodel');
    }
    public function index()
    {
        // Custom Pagination
        $getTotalCount = $this->Deliverymodel->getDeliveryTotalCount();
        $config['base_url']    = base_url().'Delivery/';
        $config['uri_segment'] = 2;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(2);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getDeliveryDetails = $this->Deliverymodel->getDeliverDetails($setArray);
        $this->load->view('delivery',$getDeliveryDetails);
    }
}
