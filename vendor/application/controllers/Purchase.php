<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Purchase extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->library('Pdf');
        $this->perPage = 100;
        $this->load->model('Purchasemodel');
        $this->auth_users->is_logged_in();
    }
    public function index()
    {
      // delete purchase
      if($this->input->post('deletePurchase'))
      {
        $this->form_validation->set_rules('deletePurchaseId','text','required');
        if($this->form_validation->run() == true)
        {
          $getDeleteStatus = $this->Purchasemodel->deletePurchaseData($this->input->post('deletePurchaseId'));
          if($getDeleteStatus['message'] == 'yes')
          {
            $this->session->set_flashdata('success','Purchase  order deleted successfully');
            $this->load->helper('url');
            redirect(base_url()."Purchase", 'refresh');
          }
          else
          {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            redirect(base_url()."Purchase", 'refresh');
          }
        }
        else
        {
          $this->session->set_flashdata('error','Something went wrong. Please try again');
          $this->load->helper('url');
          redirect(base_url()."Purchase", 'refresh');
        }
      }
      // Custom Pagination
      $getTotalCount = $this->Purchasemodel->getPurchaseTotalCount();
      $config['base_url']    = base_url().'Purchase/';
      $config['uri_segment'] = 2;
      $config['total_rows']  = $getTotalCount['purchaseList'];
      $config['per_page']    = $this->perPage;
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
      $config['cur_tag_close'] = '</a></li>';
      $config['next_link'] = 'Next';
      $config['prev_link'] = 'Prev';
      $config['next_tag_open'] = '<li class="pg-next">';
      $config['next_tag_close'] = '</li>';
      $config['prev_tag_open'] = '<li class="pg-prev">';
      $config['prev_tag_close'] = '</li>';
      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';
      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';
      $this->pagination->initialize($config);
      $page = $this->uri->segment(2);
      $offset = !$page?0:$page;
      // End Here
      $setArray = array('start'=>$offset,'limit'=>$this->perPage);
      $getPurchaseData = $this->Purchasemodel->fetchPurchaseDetails($setArray);
      $this->load->view('purchaseorder',$getPurchaseData);
    }
    public function add()
    {
      // add purchase order
      if($this->input->post('addPurchaseOrder'))
      {
        $this->form_validation->set_rules('creditPeriod','select option','required');
        $this->form_validation->set_rules('supplierIdData','text','required');
        $this->form_validation->set_rules('deliveryDate','text','required');
        $this->form_validation->set_rules('periodTime','text','required');
        if($this->form_validation->run() == true)
        {
          $setInsertArray = array('quantity'=>$this->input->post('productQuantity'),'id'=>$this->input->post('purchaseProductId'),
          'variant_id'=>$this->input->post('purchaseProductVariantId'),'credit'=>$this->input->post('creditPeriod'),
          'supplierId'=>$this->input->post('supplierIdData'),'deliveryDate'=>$this->input->post('deliveryDate'),'creditTime'=>$this->input->post('periodTime'));
          $getPurchaseStatus = $this->Purchasemodel->createPurchaseOrder($setInsertArray);
          if($getPurchaseStatus['message'] == 'yes')
          {
            $this->session->set_flashdata('success','Purchase order created successfully');
            $this->load->helper('url');
            redirect(base_url()."Purchase", 'refresh');
          }
          else
          {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            redirect(base_url()."Purchase/add", 'refresh');
          }
        }
        else
        {
          $this->session->set_flashdata('error','All field are mandatory');
          $this->load->helper('url');
          redirect(base_url()."Purchase/add", 'refresh');
        }
      }
      $getSupplierList = $this->Purchasemodel->getSupplierlist();
      $this->load->view('addpurchaseorder',$getSupplierList);
    }
    public function edit()
    {
      $getPurchaseId = $this->uri->segment('3');
      if($getPurchaseId == "")
      {
        $this->load->helper('url');
        redirect(base_url()."Purchase", 'refresh');
      }
      // edit function 
      if($this->input->post('editPurchaseOrder'))
      {
        $this->form_validation->set_rules('creditPeriod','select option','required|in_list[one day,one week,one month]');
        $this->form_validation->set_rules('supplierIdData','text','required');
        $this->form_validation->set_rules('deliveryDate','text','required');
        if($this->form_validation->run() == true)
        {
          $setUpdatePurchaseArray = array('quantity'=>$this->input->post('productQuantity'),'id'=>$this->input->post('purchaseProductId'),'credit'=>$this->input->post('creditPeriod'),'supplierId'=>$this->input->post('supplierIdData'),'deliveryDate'=>$this->input->post('deliveryDate'),'purchaseId'=>$getPurchaseId);
          $getPurchaseStatus = $this->Purchasemodel->updatePurchaseOrder($setUpdatePurchaseArray);
          if($getPurchaseStatus['message'] == 'yes')
          {
            $this->session->set_flashdata('success','Purchase updated successfully');
            $this->load->helper('url');
            redirect(base_url()."Purchase", 'refresh');
          }
          else
          {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            redirect(base_url()."Purchase/edit/".$getPurchaseId."", 'refresh');
          }
        }
        else
        {
          $this->session->set_flashdata('error','Something went wrong. Please try again');
          $this->load->helper('url');
          redirect(base_url()."Purchase/edit/".$getPurchaseId."", 'refresh');
        }
      }
      $getEditPurchaseInfo = $this->Purchasemodel->getPurchaseInfo($getPurchaseId);
      $this->load->view('editpurchaseorder',$getEditPurchaseInfo);
    }
    public function inward()
    {
      $getPurchaseId = $this->uri->segment('3');
      if(!$getPurchaseId)
      {
        $this->load->helper('url');
        redirect(base_url()."purchase", 'refresh');
      }
      $getInwardData = $this->Purchasemodel->fetchInwardData($getPurchaseId);
      $this->load->view('inward',$getInwardData);
    }
    public function fetchSupplierPorduct()
    {
      $this->form_validation->set_rules('supplierId','text','required|integer');
      if($this->form_validation->run() == true)
      {
        $getProductList = $this->Purchasemodel->fetchSupplierProductList($this->input->post('supplierId'));
        echo $getProductList['message'];
      }
      else
      {
        echo 'no';
      }
    }
    // Add inward function 
    public function addInwardata()
    {
      $getTotalPostData = $this->input->post();
      $getInwardStatus = $this->Purchasemodel->setInwardData($getTotalPostData);
      if($getInwardStatus['message'] == 'yes')
      {
        $this->session->set_flashdata('success','Inward updated successfully');
        $this->load->helper('url');
        redirect(base_url()."purchase", 'refresh');
      }
      else
      {
        $this->session->set_flashdata('error','Something went wrong. Please try again');
        $this->load->helper('url');
        redirect(base_url()."purchase", 'refresh');
      }
    }
    public function generateinvoice()
    {
      //get data from DB
      $getId = $this->uri->segment('3');
      if(!$getId)
      {
        $this->load->helper('url');
        redirect(base_url()."purchase", 'refresh');
      }
      $getLogo = fetchvedorsitelogo();

      $getPurchaeInvoice = $this->Purchasemodel->fetchInvoiceData($getId);
      // print_r($getPurchaeInvoice);
      $setMainArray = $getPurchaeInvoice['mainData'][0];
      // set supplier info
      $getSupplierName = $setMainArray->houdinv_supplier_contact_person_name;
      $getSupplierEmail = $setMainArray->houdinv_supplier_email;
      $getSupplierPhone = $setMainArray->houdinv_supplier_contact;
      $getSupplierAddress = $setMainArray->houdinv_supplier_address;
      $getName = $setMainArray->houdinv_supplier_comapany_name;
      $getInvoiceDate = date('d-m-Y',$getPurchaeInvoice['subdata'][0]['inwardData'][0]->houdinv_purchase_products_inward_craeted_at);

      // get product details data
      $setHtmlText = "";
      $subTotal = 0;
      $AmountPaid = 0;
      $RemainigAmout = 0;
      for($index = 0; $index < count($getPurchaeInvoice['subdata']); $index++)
      {
        $getTotalPayment = $getPurchaeInvoice['subdata'][$index]['inwardData'][0]->houdinv_purchase_products_inward_total_amount;
        $getTotalPaidPayment = $getPurchaeInvoice['subdata'][$index]['inwardData'][0]->houdinv_purchase_products_inward_amount_paid;
        $getDueBalance = $getTotalPayment-$getTotalPaidPayment;
        // basic accounting
        $subTotal = $subTotal+$getTotalPayment;
        $AmountPaid = $AmountPaid+$getTotalPaidPayment;
        $RemainigAmout = $RemainigAmout+$getDueBalance;
        if($setHtmlText)
        {
          $setHtmlText = $setHtmlText.'<tr>
          <td style="font-size:12px;font-weight:700;">'.$getPurchaeInvoice['subdata'][$index]['productTitle'].'</td>
            <td style="font-size:12px;font-weight:700;">'.$getPurchaeInvoice['subdata'][$index]['purchaseProductData']->houdinv_purchase_products_quantity.'</td>
              <td style="font-size:12px;font-weight:700;">'.$getPurchaeInvoice['subdata'][$index]['inwardData'][0]->houdinv_purchase_products_inward_quantity_recieved.'</td>
                <td style="font-size:12px;font-weight:700;">'.$getDueBalance.'</td>
          </tr>';
        }
        else
        {
          $setHtmlText = '<tr>
          <td style="font-size:12px;font-weight:700;">'.$getPurchaeInvoice['subdata'][$index]['productTitle'].'</td>
            <td style="font-size:12px;font-weight:700;">'.$getPurchaeInvoice['subdata'][$index]['purchaseProductData']->houdinv_purchase_products_quantity.'</td>
              <td style="font-size:12px;font-weight:700;">'.$getPurchaeInvoice['subdata'][$index]['inwardData'][0]->houdinv_purchase_products_inward_quantity_recieved.'</td>
                <td style="font-size:12px;font-weight:700;">'.$getDueBalance.'</td>
          </tr>';
        }
      }


      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Houdin-e');
      $pdf->SetTitle('Houdin-e - Supplier Invoice');
      $pdf->SetSubject('Houdin-e - Supplier Invoice');
      $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
      $pdf->setFooterData(array(0,64,0), array(0,64,128));
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = <<<EOD
<table style="width:100%;">
<tbody>
<tr>

<td style="width:70%;"><img style="display:block; margin:auto; width:150px; " src="$getLogo" alt="Logo"></td>
<td style="width:30%;"><table style="float:right; width:auto; margin-right:5px;">
<tbody>
<tr>
<td style="font-size:12px;"><h3>Supplier Information</h3></td>
</tr>
<tr>
<td style="font-size:12px;">$getSupplierName</td>
</tr>
<tr>
<td style="font-size:12px;">$getSupplierEmail</td>
</tr>
<tr>
<td style="font-size:12px;">$getSupplierPhone </td>
</tr>
<tr>
<td style="font-size:12px;">$getSupplierAddress</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
  <br/>
  <hr/>
  <br/>
  <br/>
  <table style="width:100%;">
  <tbody>
  <h4>
Invoice Statement</h4>
  <tr>
  <td style="width:67%; font-size:12px;">To<br/>
$getName </td>
  <td style="width:33%;"><table style="float:right; width:auto; margin-right:5px;">
  <tbody>

  <tr>
  <td style="font-size:12px;">Statement Date :  $getInvoiceDate
</td>
  </tr>
  </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
<table>
<tr><td>&nbsp;</td></tr>
</table>

  <table>
  <tr><td>&nbsp;</td></tr>
  </table>
  <table border="1" cellpadding="10">
  <tr>
  <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Product name</td>
    <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Quantity Ordered</td>
      <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Quantity Recieved</td>
        <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total due balance</td>

  </tr>
  $setHtmlText

  </table>
  <table>
  <tr><td>&nbsp;</td></tr>
  </table>

  <table style="width:98%;" cellpadding="10" cellspacing="10">
  <tbody>
  <tr>
  <td style="width:40%;"></td>
  <td style="width:60%;">
  <table border="1" cellpadding="10" style="text-align:right; width:98%;  border:1px solid #000;">
  <tbody>

 <tr style="padding:20px">  <td style="font-size:12px;color:#fff;" bgcolor="#000">Sub Total</td> 
  <td style="text-align:right;font-size:11px;">$subTotal</td>
  </tr>
  
  </tbody>
  </table>
  </td>
  </tr>
  <tr>
  <td style="width:40%;"></td>
  <td style="width:60%;">
  <table border="1" cellpadding="10" style="text-align:right; width:98%;  border:1px solid #000;">
  <tbody>

 <tr style="padding:20px">  <td style="font-size:12px;color:#fff;" bgcolor="#000">Amount Paid</td> 
  <td style="text-align:right;font-size:11px;">$AmountPaid</td>
  </tr>
  
  </tbody>
  </table>
  </td>
  </tr>
  <tr>
  <td style="width:40%;"></td>
  <td style="width:60%;">
  <table border="1" cellpadding="10" style="text-align:right; width:98%;  border:1px solid #000;">
  <tbody>

 <tr style="padding:20px">  <td style="font-size:12px;color:#fff;" bgcolor="#000">Remaining Amount</td> 
  <td style="text-align:right;font-size:11px;">$RemainigAmout</td>
  </tr>
  
  </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>





<br/>
<br/>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_supplier_invoice.pdf', 'I');
    }
}
