<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopusercontact extends CI_Model{
    function __construct() 
    {
    }
    public function fetchShopBasicDetails()
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getShopBasicInfo =  $getDynamicWebDb->select('houdinv_shop_address,houdinv_shop_contact_info,houdinv_shop_customer_care_email,houdinv_shop_communication_email')->from('houdinv_shop_detail')->get()->result();
        return array('shopInfo'=>$getShopBasicInfo);
    }
    public function insertCustomerQuery($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);
        $getInsertStatus = $getDynamicWebDb->insert('houdinv_shop_query',$data);
        if($getInsertStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
} 
?>