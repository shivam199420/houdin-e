<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>

<!-- Page item Area -->
<div id="page_item_area">
  <div class="page-location">
      <div class="page-location-layer">
          <div class="container">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="page-location-title">
                          <h3>New Address</h3>
                      </div>
                      <nav>
                          <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                              <li class="breadcrumb-item active" aria-current="page">New Address</li>
                          </ol>
                      </nav>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>


<!-- Login Page -->
  <div class="user-area">
<section class="checkout_page">
<div class="container">
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8">
<div class="title">
<h3>New address</h3>
</div>
<?php
if($this->session->flashdata('error'))
{
    echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
}
else if($this->session->flashdata('success'))
{
    echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
}
?>
<?php echo form_open(base_url( 'Address/adduseraddress' ), array( 'id' => 'addaddress', 'method'=>'post' ));?>
<div class="form-row">
<div class="form-group col-md-12">
<input name="username" placeholder="Name" class="form-control required_validation_for_address name_validation" type="text">
</div>

<div class="form-group col-md-12">
<label for="country">Country:</label>
<select class="form-control required_validation_for_address" name="usercountry">
<option value="">Choose Country</option>
<option value="<?php echo $countryList[0]->houdin_user_shop_country ?>"><?php echo $countryList[0]->houdin_country_name ?></option>
</select>
</div>
<div class="form-group col-md-12">
<label for="address">Address:</label>
<textarea rows="3" name="usermainaddress" placeholder="Street address. Apartment, suite, unit etc. (optional)" class="form-control required_validation_for_address name_validation"></textarea>
</div>
<div class="form-group col-md-12">
<input name="usercity" placeholder="Town/City*" class="form-control required_validation_for_address name_validation" type="text">
</div>
<div class="form-group col-md-12">
<input name="userpincode" placeholder="Post code / Zip" class="form-control required_validation_for_address name_validation" type="text">
</div>
 <div class="form-group col-md-12">
<input name="userphone" placeholder="Phone number" class="form-control required_validation_for_address name_validation" type="text">
</div>

<div class="form-group col-md-12 text-center">
    <input type="submit" class="btn btn-primary acc_btn" value="Add Address"/>
</div>

<?php echo form_close(); ?>
</div>
</div>
</div>
</section>
</div>

<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/footer'); ?>

<script type="text/javascript">
$(document).ready(function(){
    $(document).on('submit','#addaddress',function(e){
        var check_required_field='';
        $(this).find(".required_validation_for_address").each(function(){
            var val22 = $(this).val();
            if (!val22){
                check_required_field = 'error';
                $(this).css("border-color","#ccc");
                $(this).css("border-color","red");
            }
            $(this).on('keypress change',function(){
                $(this).css("border-color","#ccc");
            });
        });
        if(check_required_field)
        {
          e.preventDefault();
            return false;
        }
        else {
            return true;
        }
    });
});
</script>
