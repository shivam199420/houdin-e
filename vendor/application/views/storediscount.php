<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style>
.select2.select2-container
{
  width:100% !important;
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
           <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">Apply to store</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>discount/Discountlanding">Discount</a></li>
                  <li class="active">Apply to store</li>
                  </ol>
                  </div>
               
            </div>
           <!--END Page-Title -->  
<div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }

        //print_r($storediscount);
        ?>
        </div>
        </div>
              
        <div class="row m-t-20">
          <div class="col-md-12">

            <div class="card-box table-responsive">
              <div class="btn-group pull-right m-t-10 m-b-20">
                   <a href="<?php echo base_url(); ?>Discount/discountpdf" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
                  <button type="button" class="btn btn-default m-r-5" title="Add Discount" data-toggle="modal" data-target="#adddiscount"><i class="fa fa-tag"></i></button>
                 
                 

              </div>
              <?php if($storediscount){?>
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                     
                    <th>Discount name</th>
                    <th>Discount percentage</th>
                    <th>Discount applied on</th>
                    <th>Discount expiry</th>
                    <th>Discount Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                
                <tbody>

                <?php foreach ($storediscount as $key =>$storediscounts) {
                   

                   if($storediscounts->discount_status=='1'){
                    $statsu='<p style="color:green">Active</p>';
                   } else{
                     $statsu='<p style="color:red">Deactive</p>';
                   }
                ?>
                  <tr>
                     
                    <td><a href="<?php echo base_url(); ?>Discount/discountdetails"><?=$storediscounts->discount_name?></a></td>
                    <td><?=$storediscounts->discount_amount?></td>
                    <td><?=date("Y-m-d",$storediscounts->discount_start)?></td>
                    <td><?=date("Y-m-d",$storediscounts->discount_end)?></td>
                    <td><?=$statsu?></td>
                   
                    <td>
                      <input type="hidden" name="name" class="discount_id" value="<?=$storediscounts->discount_id;?>">
                      <button type="button" class="btn btn-default m-r-5 delete_dicount" title="Delete">Delete</button>
                      <button type="button"  class="btn btn-default m-r-5 edit_discount" title="Edit">Edit</button>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
              <?php } ?>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>


<!--Add Discount-->
<div id="adddiscount" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
          <?php echo form_open(base_url('Discount/adddiscount'), array( 'id' => 'adddiscount_frm', 'method'=>'POST'));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

        <h4 class="modal-title">Add Discount</h4>

        </div>
        <div class="modal-body">
        <div class="row form-horizontal">
        <div class="col-md-12">
 
        <div class="form-group">
        <label class="col-md-4 control-label">Category</label>
        <div class="col-md-8">
       <select class="form-control select2 multiselect_cate" name="discountcategory[]" multiple="">
            <option value="">Choose Category</option>
            <?php 
            foreach($categorieslist as $categorieslista)
            {
             ?> 
             <option value="<?php echo $categorieslista->houdinv_category_id ?>"> <?php echo $categorieslista->houdinv_category_name ?></option>  
           <?php }
            ?>
            </select>
        </div>
        </div>
            <div class="form-group">
        <label class="col-md-4 control-label"> Products Name</label>
        <div class="col-md-8">
       <select class="form-control select2" name="discountproducts[]" id="products_add" multiple="">
            <option value="">Choose Products</option>
           
            </select>
        </div>
        </div> 

 <div class="ajax_Loginresponse_result">

        <div class="form-group">
        <label class="col-md-4 control-label">Discount name</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control required_validation_for_add_custo" name="discountname" placeholder="Discount name">
        </div>
        </div>       

         <div class="form-group">
        <label class="col-md-4 control-label">Discount Value</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control required_validation_for_add_custo" name="discountvalue" placeholder="Discount Value">
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label">Discount started at</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control date_picker required_validation_for_add_custo" name="discountstarted" placeholder="Discount started at">
        </div>
        </div>
       
       </div>

        <div class="form-group">
        <label class="col-md-4 control-label">Discount end at</label>
        <div class="col-md-8">
        <input type="text" value="" class="form-control date_picker required_validation_for_add_custo" name="discountend" placeholder="Discount end at">
        </div>
        </div>
         <div class="form-group">
        <label class="col-md-4 control-label">Choose status</label>
        <div class="col-md-8">
        <select class="form-control required_validation_for_add_custo" name="discount_status" >
            <option value="1">Active</option>
            <option value="0">Deactive</option>
            
        </select>
        </div>
        </div>

        </div>
        </div> </div>

        <div class="modal-footer">
            
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Submit">

        </div>

        </div>
    <?php echo form_close(); ?>
        </div>

        </div>

       <!--Delete-->

  <div id="delete_discount" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
           <?php echo form_open(base_url('Discount/deletediscount'), array( 'id' => 'deletediscount', 'method'=>'POST'));?>
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete discount</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this discount?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">
          <input type="hidden" name="deletesdescount" id="delete_descountval">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Delete">

          </div>

          </div>
         <?php echo form_close(); ?>
          </div>

          </div>

                <!--Edit-->

  <div id="edit_discount" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
         <?php echo form_open(base_url('Discount/editdiscount_frm'), array( 'id' => 'editdiscount_frm', 'method'=>'POST'));?>
          <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

        <h4 class="modal-title">Edit Discount</h4>

        </div>
        <div class="modal-body">
        <div class="row form-horizontal">
        <div class="col-md-12">
 
        <div class="form-group">
        <label class="col-md-4 control-label">Category</label>
        <div class="col-md-8">
       <select class="form-control select2 multiselect_cate_ed" id="product_cat_id" name="discountcategory[]" multiple="">
            <option value="">Choose Category</option>
            <?php 
            foreach($categorieslist as $categorieslista)
            {
             ?> 
             <option value="<?php echo $categorieslista->houdinv_category_id ?>"> <?php echo $categorieslista->houdinv_category_name ?></option>  
           <?php }
            ?>
            </select>
        </div>
        </div>
            <div class="form-group">
        <label class="col-md-4 control-label">Products Name</label>
        <div class="col-md-8">
       <select class="form-control select2"   name="discountproducts[]" id="products_add_ed" multiple="">
            <option value="">Choose Products</option>
           
            </select>
        </div>
        </div> 

 <div class="edit_module_data">

        <div class="form-group">
        <label class="col-md-4 control-label">Discount name</label>
        <div class="col-md-8">
        <input type="text" id="discount_name" class="form-control required_validation_for_add_custo" name="discountname" placeholder="Discount name">
        </div>
        </div>       

         <div class="form-group">
        <label class="col-md-4 control-label">Discount Value</label>
        <div class="col-md-8">
        <input type="text"  id="discount_amount" class="form-control required_validation_for_add_custo" name="discountvalue" placeholder="Discount Value">
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-4 control-label">Discount started at</label>
        <div class="col-md-8">
        <input type="text"  id="discount_start" class="form-control date_picker required_validation_for_add_custo" name="discountstarted" placeholder="Discount started at">
        </div>
        </div>
       
       </div>

        <div class="form-group">
        <label class="col-md-4 control-label">Discount end at</label>
        <div class="col-md-8">
        <input type="text" id="discount_end" class="form-control date_picker required_validation_for_add_custo" name="discountend" placeholder="Discount end at">
        </div>
        </div>
         <div class="form-group">
        <label class="col-md-4 control-label">Choose status</label>
        <div class="col-md-8">
        <select class="form-control required_validation_for_add_custo" id="discount_status" name="discount_status" >
            <option value="1">Active</option>
            <option value="0">Deactive</option>
            
        </select>
        </div>
        </div>

        </div>
        </div> </div>

        <div class="modal-footer">
            <input type="hidden" name="discount_id" id="discount_id">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Submit">

        </div>

        </div>
         <?php echo form_close(); ?>
          </div>

          </div>
<?php echo form_open(base_url('Discount/edit_datasend'), array( 'id' => 'edit_datasend_frm', 'method'=>'POST'));?>
<input type="hidden" name="edit_data" id="edit_data">
 <?php echo form_close(); ?>

<?php $this->load->view('Template/footer.php') ?>
<script>
 $(document).ready(function(){
        $(".select2").select2();

        });
        </script>
<script>
$(document).ready(function(){


  $('.multiselect_cate').on('change',function(){

      $multiple_select= $(this).val();
     // alert($multiple_select);
  urls='<?=base_url();?>'+'Discount/Get_products';
          var dataString = $('#adddiscount_frm').serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:dataString,        
        success:function (data) {        
        if(data){
         $('#products_add').html(data);
        }else{

        
        }
        }
        });  



  });

    $('.multiselect_cate_ed').on('change',function(){

      $multiple_select= $(this).val();
     // alert($multiple_select);
  urls='<?=base_url();?>'+'Discount/Get_products';
          var dataString = $('#editdiscount_frm').serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:dataString,        
        success:function (data) {        
        if(data){
         $('#products_add_ed').html(data);
        }else{

        
        }
        }
        });  



  });
    
    $('#adddiscount_frm').submit(function(e){
    e.preventDefault(); 
 
 

var check_required_field='';
      $(this).find(".required_validation_for_add_custo").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {
          
        return false;
      }else{      
        


    urls='<?=base_url();?>'+'Discount/adddiscount';
          var dataString = $(this).serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".ajax_Loginresponse_result").html(data);
        }else{

        window.location.reload();
        }
        }
        });  


      } 

    });  
    
     $(".delete_dicount").click(function(){ 
     
       update_id=$(this).siblings('.discount_id').val();
       $('#delete_descountval').val(update_id);              
      $('#delete_discount').modal('show');

       });



$(".edit_discount").click(function(){ 
       
  var $data_button=$(this);
       $data_button.html("<i class='fa fa-spinner fa-spin '></i>");
        update_id=$(this).siblings('.discount_id').val();

       
         $('#edit_data').val(update_id);

   urls='<?=base_url();?>'+'Discount/GetEdit_products';
          var dataString = $('#edit_datasend_frm').serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:dataString,        
        success:function (data) {        
        if(data){
        
        var data = $.parseJSON(data);

        discount_id=data.discount_id;
        $('#discount_id').val(discount_id);
         discount_end=data.discount_end;
       $('#discount_end').val(discount_end);
          discount_amount=data.discount_amount;
          $('#discount_amount').val(discount_amount);
           discount_name=data.discount_name;
           $('#discount_name').val(discount_name);
            discount_start=data.discount_start;
            $('#discount_start').val(discount_start);
             discount_status=data.discount_status;
                $('#discount_status').val(discount_status);

            
     

               product_cat_id=data.product_cat_id;
               
                $('#product_cat_id').val(product_cat_id).select2();
              var product_id=data.product_id;  


             urls='<?=base_url();?>'+'Discount/Get_products';
          var dataString = $('#editdiscount_frm').serialize();     
        $.ajax({
        url:urls,
        type:"post",
        data:dataString,        
        success:function (data1) {        
        if(data1){
         $('#products_add_ed').html(data1);
         $('#products_add_ed').val(product_id).select2();
        }else{        
        }
        }
        });     
         $('#edit_discount').modal('show');
         $data_button.html("Edit");         
        }else{
      alert('technical Isuue please try agian later ');
        
        }
        }
        });  

      

       });

    

 $('#editdiscount_frm').submit(function(e){
    e.preventDefault(); 
 
 

    urls='<?=base_url();?>'+'Discount/editdiscount_frm';
          var dataString = $(this).serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".edit_module_data").html(data);
        }else{

        window.location.reload();
        }
        }
        });  
    });  

 
});
</script>
