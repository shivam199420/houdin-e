<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>

<div class="content-page">
    <!-- Start content --> 
    <div class="content">
        <div class="container">

            
            <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Discount</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                 
                  <li class="active">Discount</li>
                  </ol>
                  </div>
                 
            </div>
           <!--END Page-Title -->
         <div class="row m-t-30">
                <div class="col-md-12">
                      <!--team-1-->
                <div class="col-lg-3">
                <div class="our-team-main">
                
                <div class="team-front">
                <img src="<?php echo base_url(); ?>assets/images/landingpageicons/Apply_on_Store.png">
                <h3>Apply on Store</h3>
                <p>Discount</p>
                </div>
                
               <a href="<?php echo base_url();?>Discount/storeDiscount"> <div class="team-back">
                <span>
                <h2 class="font24">Apply on Store</h2>
                </span>
                </div></a>
                
                </div>
                </div> 
                <!--team-1-->
                
                <!--team-2-->
               <div class="col-lg-3">
                <div class="our-team-main">
                
                <div class="team-front">
               <img src="<?php echo base_url(); ?>assets/images/landingpageicons/Apply_by_Customer.png">
                <h3>Apply By Customer</h3>
                <p>Discount</p>
                </div>
                
                <a href="<?php echo base_url();?>Discount/customerDiscount"><div class="team-back">
                <span>
                <h2 class="font24">Apply By Customer</h2>
                </span>
                </div></a>
                
                </div>
                </div>
                <!--team-2-->
                
               

                </div>
            </div><!-- Row -->


      </div>
    </div>
  </div>
<!--Delete-->

  <div id="delete_order" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
          <form method="post">
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete Order</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this order ?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Delete">

          </div>

          </div>
          </form>
          </div>

          </div>

<!--change Status-->
<div id="Change-status" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <form method="post">
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Change Status</h4>

        </div>

        <div class="modal-body">



        <div class="row">
        <div class="col-md-12">

        <div class="form-group no-margin">

        <label for="field-7" class="control-label">Change Status</label>

        <select class="form-control " name=""><option value="">Choose Status</option><option value="1">Active</option><option value="0">Deactive</option><option value="2">Block</option></select>

        </div>

        </div>
        </div>





        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Update Status">

        </div>

        </div>
        </form>
        </div>

        </div> 

        <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Add purchase order</h4> 
                                                </div> 
                                                <div class="modal-body"> 
                                                    <div class="row"> 
                                                        <div class="col-md-6"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label">Select Product</label> 
                                                                <select class="form-control">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                </select>
                                                            </div> 
                                                        </div> 
                                                        <div class="col-md-6"> 
                                                            <div class="form-group"> 
                                                                <label for="field-2" class="control-label">Select Supplier</label> 
                                                                <select class="form-control">
                                                                    <option>Supplier1</option>
                                                                    <option>Supplier2</option>
                                                                    <option>Supplier3</option>
                                                                    <option>Supplier4</option>
                                                                    <option>Supplier5</option>
                                                                </select> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-3" class="control-label">Quantity</label> 
                                                                <input type="text" class="form-control" id="field-3" placeholder="Quantity"> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-6"> 
                                                            <div class="form-group"> 
                                                                <label for="field-4" class="control-label">Delivery date</label> 
                                                                <input type="text" class="form-control date_picker" id="field-4" placeholder="Delivery date"> 
                                                            </div> 
                                                        </div> 
                                                        <div class="col-md-6"> 
                                                            <div class="form-group"> 
                                                                <label for="field-5" class="control-label">Credit period</label> 
                                                                <input type="text" class="form-control" id="field-5" placeholder="Credit period"> 
                                                            </div> 
                                                        </div> 
                                                     
                                                    </div> 
                                                    <!--<div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group no-margin"> 
                                                                <label for="field-7" class="control-label">Personal Info</label> 
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
                                                            </div> 
                                                        </div> 
                                                    </div> -->
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                                </div> 
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->

<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
