<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Staff extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->auth_users->is_logged_in();
         $this->load->helper('url');
         $this->load->helper('setting');
         $this->load->model('Staffmodel'); 
         $this->load->model('Campaignmodel'); 
        $this->load->library('Pdf');
    }
    public function index()
    {
    	$Getstaff['staffData']= $this->Staffmodel->fetch_staffRecord();
         $Getstaff['WareHouse']= $this->Staffmodel->getWarhouse();
        $this->load->view('staffmanagement',$Getstaff);
    }
    public function add()
    {
        $getSmsEmail = smsemailcredentials();
     if($this->input->post('customSearchDriver')){

$this->form_validation->set_rules('staff_name','Staff Name','required|trim');
        $this->form_validation->set_rules('staff_contact_number','Staff Contact Number','required|trim|regex_match[/^[0-9]{10}$/]');
        $this->form_validation->set_rules('staff_email','Email','required');
        $this->form_validation->set_rules('staff_department','Staff Department','required|trim');
        // $this->form_validation->set_rules('staff_status','Staff Status','required|trim');
        $this->form_validation->set_rules('staff_address','Staff Address','required|trim');
        $this->form_validation->set_rules('staff_name','Staff Name','required|trim');


     if($this->form_validation->run() == true)
    {
        $staff_name = $this->input->post('staff_name');
        $staff_contact_number = $this->input->post('staff_contact_number');
        $staff_department = $this->input->post('staff_department');
        $staff_status = '1';
        $staff_warehouse= $this->input->post('staff_warehouse');
        $staff_email = $this->input->post('staff_email');
        $staff_alternat_contact = $this->input->post('staff_alternat_contact');
        $staff_address = $this->input->post('staff_address');
        $password_send = $this->input->post('password_send'); 

        $dashboard = $this->input->post('dashboard');
        $order = $this->input->post('order');
        $cataogry = $this->input->post('cataogry');
        $product = $this->input->post('product');
        $setting = $this->input->post('setting');
        $staff_members = $this->input->post('staff_members');
        $inventory = $this->input->post('inventory');
        $customer_management = $this->input->post('customer_management');
        $delivery = $this->input->post('delivery');
        $discount = $this->input->post('discount');
        $supplier_management = $this->input->post('supplier_management');
        $templates = $this->input->post('templates');
        $purchase = $this->input->post('purchase');
        $gift_voucher = $this->input->post('gift_voucher');
        $calendar = $this->input->post('calendar');
        $POS = $this->input->post('POS');
        $store_setting = $this->input->post('store_setting');
        $tax = $this->input->post('tax');
        $shipping = $this->input->post('shipping');
        $campaign = $this->input->post('campaign');
        $coupons=$this->input->post('coupons');  

           $getAdminP = $this->random_password();  
         //  $getAdminP = 'love2deepak';      
            $letters1='abcdefghijklmnopqrstuvwxyz'; 
            $string1=''; 
            for($x=0; $x<3; ++$x)
            {  
                $string1.=$letters1[rand(0,25)].rand(0,9); 
            }
            $staff_password_salt = password_hash($string1,PASSWORD_DEFAULT);

           $staff_password = crypt($getAdminP,$staff_password_salt);
           
           

  	$setData = strtotime(date('Y-m-d'));
        $setStafffArray = array("main_staff_data"=>array("staff_name"=>$staff_name,"staff_contact_number"=>$staff_contact_number,"staff_department"=>$staff_department,"staff_status"=>$staff_status,"staff_password"=>$staff_password,"staff_password_salt"=>$staff_password_salt,"staff_email"=>$staff_email,"staff_alternat_contact"=>$staff_alternat_contact,"staff_address"=>$staff_address,"password_send"=>$password_send,"create_date"=>$setData,"staff_warehouse"=>$staff_warehouse),
        	"staff_subdata"=>array("dashboard"=>$dashboard,"order"=>$order,"cataogry"=>$cataogry,"product"=>$product,"setting"=>$setting,"staff_members"=>$staff_members,"inventory"=>$inventory,"customer_management"=>$customer_management,"delivery"=>$delivery,"discount"=>$discount,"supplier_management"=>$supplier_management,"templates"=>$templates,"purchase"=>$purchase,"coupons"=>$coupons,"gift_voucher"=>$gift_voucher,"calendar"=>$calendar,"POS"=>$POS,"store_setting"=>$store_setting,"tax"=>$tax,"shipping"=>$shipping,"campaign"=>$campaign));
        //    $body = "Login URL: ".base_url()."  Username: ".$staff_email." Userpassword:  ".$getAdminP."";
      // echo '<script>alert("'.$body.'")</script>';       
 
        $getInsertStatus = $this->Staffmodel->insert_staff($setStafffArray);
        if($getInsertStatus){
    if($password_send=='sms'){
        $getReaminingSMS = remainingSMS();
        if($getReaminingSMS['sms'] > 0)
        {
            $getSMS = smsemailcredentials();
            if(count($getSMS) > 0)
            {   
                $id = $getSMS['twilio'][0]->houdin_twilio_sid;
                $token = $getSMS['twilio'][0]->houdin_twilio_token;
                $url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
                $from = $getSMS['twilio'][0]->houdin_twilio_number;
                $body = "Login URL: ".base_url()."  Username: ".$staff_email." Userpassword:  ".$getAdminP."";
                $data1 = array (
                    'From' => $from,
                    'To' => $this->input->post('staff_contact_number'),
                    'Body' => $body,
                );
                
                $post = http_build_query($data1);
                $x = curl_init($url );
                curl_setopt($x, CURLOPT_POST, true);
                curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
                curl_setopt($x, CURLOPT_POSTFIELDS, $post);
                $y = curl_exec($x);
                if (strpos($y, 'PIN') == false) 
                {
                    failureSMS();
                    $this->session->set_flashdata('success','Staff addedd successfully.Credentials not sent');             
                    redirect(base_url()."staff");
                }
                else
                {
                    succcessSMS($getReaminingSMS['sms']-1);
                    $this->session->set_flashdata('success','Staff addedd successfully.');             
                    redirect(base_url()."staff");
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');             
                redirect(base_url()."staff");
            }
        }
        else
        {
            $this->session->set_flashdata('error','Staff addedd successfully.Please upgrade your sms package.');             
            redirect(base_url()."staff");
        }
    }
    else
    {   
        $getRemainingEmail = getRemainingEmail();
        if($getRemainingEmail['remaining'] > 0)
        {
            // put validation
            $getLogo = fetchvedorsitelogo();
            $getInfo = fetchvendorinfo();
            $getSocial = fetchsocialurl(); 
            if(count($getSocial) > 0) 
            {
                $setSocialString = "";
                if($getSocial[0]->facebook_url)
                {
                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                }
                if($getSocial[0]->twitter_url)
                {
                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                }
                if($getSocial[0]->youtube_url)
                {
                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                }
            }
            // send email to admin user
            $url = 'https://api.sendgrid.com/';
            $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
            $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
            $json_string = array(
            'to' => array(
                $this->input->post('staff_email')
            ),
            'category' => 'test_category'
            );
            $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
            <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
            <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
            <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
            <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
            <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">


            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Login URL:</strong></td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.base_url().'</td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">


            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Your Login Email is:</strong></td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$staff_email.'</td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">

            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Your Password is:</strong></td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$getAdminP.'</td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
            <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
            <table width="100%" ><tr >
            <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
            <tr><td style="">
            '.$setSocialString.'</td>
            </tr></table>
            </div>
            </div></td></tr></table>';
            
            
            $params = array(
                'api_user'  => $user,
                'api_key'   => $pass,
                'x-smtpapi' => json_encode($json_string),
                'to'        => $this->input->post('staff_email'),
                'fromname'  => $getInfo['name'],
                'subject'   => 'Your  New Password',
                'html'      => $htm,
                'from'      => $getInfo['email'],
            );
            $request =  $url.'api/mail.send.json';
            $session = curl_init($request);
            curl_setopt ($session, CURLOPT_POST, true);
            curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
            curl_setopt($session, CURLOPT_HEADER, false);
            curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($session);
            curl_close($session);
            $getResponse = json_decode($response);
            if($getResponse->message == 'success')
            {
                updateSuceessEmailCount($getRemainingEmail['remaining']-1);
                $this->session->set_flashdata('success','Staff addedd successfully');             
                redirect(base_url()."staff");
            }
            else
            {
                updateErrorEmailLog();
                $this->session->set_flashdata('success','Staff addedd successfully. Credentials is not sent successfully');             
                redirect(base_url()."staff");
            }
        }   
        else
        {
            $this->session->set_flashdata('error','Staff addedd successfully.Please upgrade your email package.');             
            redirect(base_url()."staff");
        }    
        }
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."staff/add");
        }
      }else{ 
         $this->session->set_flashdata('error',validation_errors());
//$Getsmsemail= $this->Campaignmodel->fetch_customerRecord();
$GetData['WareHouse']= $this->Staffmodel->getWarhouse();
        $this->load->view('addstaffmanagement',$GetData);

      } 
 
     }else{
        $GetData['WareHouse']= $this->Staffmodel->getWarhouse();
    

     	$this->load->view('addstaffmanagement',$GetData);
     }
        
    }

    public function staff_delete()
    {
    	if($this->input->post('update_id')){

        $ids=$this->input->post('update_id');
         $getInsertStatus = $this->Staffmodel->staff_delete(array('staff_id'=>$ids));
        if($getInsertStatus)
        {           
          $this->session->set_flashdata('success','Staff Delete Successfully'); 
          redirect(base_url()."staff");              
                
        }
        else
        {
           $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."staff");
        }  
      }
    }

 public function staff_multidelete()
   {
    if($this->input->post('multiple_check')){
     $multiple_check_arrya= $this->input->post('multiple_check');
 
      $multiple_check_arryaStatus = $this->Staffmodel->Delete_multiSMSstaff($multiple_check_arrya);
       if($multiple_check_arryaStatus){
       $this->session->set_flashdata('success',count($multiple_check_arrya).' Staff  sucessfully Delete');  
       redirect(base_url()."staff/");
       }
        $this->session->set_flashdata('error','Something went wrong. Please try again');  
    redirect(base_url()."staff/");
    }else{

     $this->session->set_flashdata('error','Something went wrong. Please try again');  
    redirect(base_url()."staff/");
    }
   }

function random_password() 
{
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $password = array(); 
    $alpha_length = strlen($alphabet) - 1; 
    for ($i = 0; $i < 8; $i++) 
    {
        $n = rand(0, $alpha_length);
        $password[] = $alphabet[$n];
    }
    return implode($password); 
}



 public function edit(){
 	$getCouponId = $this->uri->segment('3');
      if($getCouponId == "")
      {
        $this->load->helper('url');
        redirect(base_url()."staff", 'refresh');
      }
  if($this->input->post('customSearchDriver')){

$this->form_validation->set_rules('staff_name','Staff Name','required|trim');
        $this->form_validation->set_rules('staff_contact_number','Staff Contact Number','required|trim|regex_match[/^[0-9]{10}$/]');
        $this->form_validation->set_rules('staff_email','Email','required|valid_email');
        $this->form_validation->set_rules('staff_department','Staff Department','required|trim');
        // $this->form_validation->set_rules('staff_status','Staff Status','required|trim');
        $this->form_validation->set_rules('staff_address','Staff Address','required|trim');
        $this->form_validation->set_rules('staff_name','Staff Name','required|trim');


     if($this->form_validation->run() == true)
    {
        $staff_name = $this->input->post('staff_name');
        $staff_contact_number = $this->input->post('staff_contact_number');
        $staff_department = $this->input->post('staff_department');
        $staff_status = '1';
        $staff_warehouse= $this->input->post('staff_warehouse');
        $staff_email = $this->input->post('staff_email');
        $staff_alternat_contact = $this->input->post('staff_alternat_contact');
        $staff_address = $this->input->post('staff_address');
        $password_send = $this->input->post('password_send'); 

        $dashboard = $this->input->post('dashboard');
        $order = $this->input->post('order');
        $cataogry = $this->input->post('cataogry');
        $product = $this->input->post('product');
        $setting = $this->input->post('setting');
        $staff_members = $this->input->post('staff_members');
        $inventory = $this->input->post('inventory');
        $customer_management = $this->input->post('customer_management');
        $delivery = $this->input->post('delivery');
        $discount = $this->input->post('discount');
        $supplier_management = $this->input->post('supplier_management');
        $templates = $this->input->post('templates');
        $purchase = $this->input->post('purchase');
        $gift_voucher = $this->input->post('gift_voucher');
        $calendar = $this->input->post('calendar');
        $POS = $this->input->post('POS');
        $store_setting = $this->input->post('store_setting');
        $tax = $this->input->post('tax');
        $shipping = $this->input->post('shipping');
        $campaign = $this->input->post('campaign'); 
        $coupons=$this->input->post('coupons');       


  	$setData = strtotime(date('Y-m-d'));
        $setStafffArray = array("main_staff_data"=>array("staff_name"=>$staff_name,"staff_contact_number"=>$staff_contact_number,"staff_department"=>$staff_department,"staff_status"=>$staff_status,"staff_email"=>$staff_email,"staff_alternat_contact"=>$staff_alternat_contact,"staff_address"=>$staff_address,"create_date"=>	$setData,"staff_warehouse"=>$staff_warehouse),"staff_subdata"=>array("dashboard"=>$dashboard,"order"=>$order,"cataogry"=>$cataogry,"product"=>$product,"setting"=>$setting,"staff_members"=>$staff_members,"inventory"=>$inventory,"customer_management"=>$customer_management,"delivery"=>$delivery,"discount"=>$discount,"supplier_management"=>$supplier_management,"templates"=>$templates,"purchase"=>$purchase,"coupons"=>$coupons,"gift_voucher"=>$gift_voucher,"calendar"=>$calendar,"POS"=>$POS,"store_setting"=>$store_setting,"tax"=>$tax,"shipping"=>$shipping,"campaign"=>$campaign));


        $getInsertStatus = $this->Staffmodel->update_staff($setStafffArray,$getCouponId);
        if($getInsertStatus){
                $this->session->set_flashdata('success','Staff Update suucessfully');               
                redirect(base_url()."staff");
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."staff/".$getCouponId."/");
        }
      }else{ 
         $this->session->set_flashdata('error',validation_errors());
         $edit_list= $this->Staffmodel->fetch_onestaffRecord($getCouponId);
     	$edit_list['edit_id']=$getCouponId;
        $edit_list['WareHouse']= $this->Staffmodel->getWarhouse();
         $this->load->view('editstaffmanagement',$edit_list);

      } 
 
     }else{
     	 $edit_list= $this->Staffmodel->fetch_onestaffRecord($getCouponId);
     	$edit_list['edit_id']=$getCouponId;
     $edit_list['WareHouse']= $this->Staffmodel->getWarhouse();
     	 $this->load->view('editstaffmanagement',$edit_list);
     }
        
    }
public function get_array()
{
   $getInsertStatus = $this->Staffmodel->get_array();
   $get_admin_user = fetchVendorDetails();
   $arrya_json[] = [
    '',
    $get_admin_user[0]->houdin_user_name,
    $get_admin_user[0]->houdin_user_email,
    'Admin',
    '<p style="color:green">Active</p>',
    ''
];
foreach ($getInsertStatus as $key => $value) {
  

//print_r($value);
  if($value->staff_status=='1'){
       $staff_statuss='<p style="color:green">Active</p>';
    }else{
        $staff_statuss='<p style="color:red">Deactive</p>';
    }

$arrya_json[] = [
    '<input type="checkbox" class="multiple_check" name="multiple_check[]" value="'.$value->staff_id.'"></td>',
    $value->staff_name, 
    $value->staff_email,
    $value->staff_department, 
    $staff_statuss,
    '<button type="button" class="btn btn-default  m-r-5 delete_staff"  data-id="'.$value->staff_id.'">Delete</button>
    <a href="'.base_url().'staff/edit/'.$value->staff_id.'/" class="btn btn-default  m-r-5" >Edit</a>'
];
}
echo json_encode(array('data'=>$arrya_json));
}


    public function stafflist()
    {
        $getLogo = fetchvedorsitelogo();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Staff List');
    $pdf->SetSubject('Houdin-e - Staff List');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$Getstaff= $this->Staffmodel->fetch_staffRecord();
 $shop_logo= $this->Campaignmodel->fetch_shop_logo_campaign();
 $shop_logos=$shop_logo[0]->shop_logo;

$Totalstaff=$Getstaff['Totalstaff'];
$totalActivestaff=$Getstaff['totalActivestaff'];
$TotalDeactiveCoupons=$Getstaff['TotalDeactiveCoupons'];

 
$url=base_url();
$table_tbody='';
//print_r($Getstaff);
foreach ($Getstaff['staffLsit'] as $key => $value) {
                      
                      if($value->staff_status=='1'){
                        $statsu_='<p style="color: green;">Active</p>';
                      }else{
                           $statsu_='<p style="color: red;">Deactive</p>';
                      }
       $table_tbody.='<tr>
 <td style="font-size:12px;font-weight:700;">'.$value->staff_name.'</td>
 <td style="font-size:12px;font-weight:700;">'.$value->staff_email.'</td>
 <td style="font-size:12px;font-weight:700;">'.$value->staff_department.'</td>
   <td style="font-size:12px;font-weight:700;">'.$statsu_.'</td>

       

 </tr>';               


}


$html = <<<EOD
 <table style="width:100%;">
<tbody>
<tr>

<td style="width:100%;text-align:center;">
<img style="display:block; margin:auto; width:150px;"  src="$getLogo" alt="Logo">
</td>

</tr>
</tbody>
</table>

<table>
<tr><td>&nbsp;</td></tr>
</table>

<table style="width:100%;" cellpadding="8" border="1">
<tr>
  <td style="text-align:center;">
  <img src=$url"assets/images/Total_Products.png" width="35px;"><br/>
  <span style="font-size:12px;font-weight:bold;">$Totalstaff</span><br/>
  <span style="font-size:12px;font-weight:bold; color:#008cb6">Total Staff</span>

   </td>
   <td style="text-align:center;">
   <img src=$url"assets/images/Total_Products.png" width="35px;"><br/>
   <span style="font-size:12px;font-weight:bold;">$totalActivestaff</span><br/>
   <span style="font-size:12px; font-weight:bold; color:#008cb6">Total Active Staff</span>

    </td>
    <td style="text-align:center;">
    <img src=$url"assets/images/Total_Products.png" width="35px;"><br/>
    <span style="font-size:12px;font-weight:bold;">$TotalDeactiveCoupons</span><br/>
    <span style="font-size:12px; font-weight:bold; color:#008cb6">Total Deactive Staff</span>

     </td>
     </tr>

   </table>


  <table>
  <tr><td>&nbsp;</td></tr>
  </table>
  <table border="1" cellpadding="10">
  <tr>
  <td style="font-size:12px;font-weight:700;" >Staff Name</td>
    <td style="font-size:12px;font-weight:700;" >Staff Email</td>

        <td style="font-size:12px;font-weight:700;">Department</td>
         <td style="font-size:12px;font-weight:700;">Status</td>

  </tr>
 $table_tbody
  </table>
  <table>
  <tr><td>&nbsp;</td></tr>
  </table>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_staff_list.pdf', 'I');
    }
}
 