<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebserviceExtra extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('ShopExtramodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function applogo()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
           $getAppLogo = $this->ShopExtramodel->getAppLogo($this->input->post('shopName'));
           print_r(json_encode($getAppLogo));  
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));  
        }
    }
    public function appsplash()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
           $getAppSplash = $this->ShopExtramodel->getAppSplash($this->input->post('shopName'));
           print_r(json_encode($getAppSplash));  
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));  
        }
    }
    public function aboutdata()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getAboutStatus = $this->ShopExtramodel->getAboutData($this->input->post('shopName'));
            print_r(json_encode($getAboutStatus));  
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));  
        }
    }
    public function termsdata()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getTermsData = $this->ShopExtramodel->getTermsData($this->input->post('shopName'));
            print_r(json_encode($getTermsData));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function faqdata()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getFaqData = $this->ShopExtramodel->getFaqData($this->input->post('shopName'));
            print_r(json_encode($getFaqData));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function fetchUserProfileImage()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('userId','text','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('id'=>$this->input->post('userId'),'shopName'=>$this->input->post('shopName'));
            $getUserProfile = $this->ShopExtramodel->getUserProfile($setArray);
            print_r(json_encode($getUserProfile));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function hashdata()
    {
        $this->form_validation->set_rules('transactionid','text','required');
        $this->form_validation->set_rules('amount','text','required');
        $this->form_validation->set_rules('name','text','required');
        $this->form_validation->set_rules('email','text','required');
        if($this->form_validation->run() == true)
        {
            $hash=hash('sha512', 'MBVIetjo|'.$this->input->post('transactionid').'|'.$this->input->post('amount').'|P01,P02|'.$this->input->post('name').'|'.$this->input->post('email').'||||||U3YohhxRvu');
            $resultArray = array('message'=>'Success','string'=>$hash);
            print_r(json_encode($resultArray));
        }
        else
        {
            $resultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
    public function payumoney()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getPayuStatus = $this->ShopExtramodel->setPayumoneyData($this->input->post('shopName'));
            print_r(json_encode($getPayuStatus));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function getOrderCount()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $this->load->helper('plan');
            $getPlanData = getVendorplan($this->input->post('shopName'));
            if(count($getPlanData) > 0)
            {
                $getPlanOrder = $getPlanData[0]->houdin_package_order_pm;
                $getTotalOrder = $this->ShopExtramodel->getOrderCountData($this->input->post('shopName'));
                if($getPlanOrder > $getTotalOrder['order'])
                {
                    $resultArray = array('message'=>'yes');
                    print_r(json_encode($resultArray));
                }
                else
                {
                    $resultArray = array('message'=>'no');
                    print_r(json_encode($resultArray));
                }
            }
            else
            {
                $resultArray = array('message'=>'no');
                print_r(json_encode($resultArray));
            }
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function fetchUserCurrency()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getCurrency = $this->ShopExtramodel->getVendorCurrency($this->input->post('shopName'));
            print_r(json_encode($getCurrency));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
}
