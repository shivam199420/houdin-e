<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <h4 class="page-title">Add Departments</h4>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-sm-12">
                        <?php 
                        if($this->session->flashdata('success'))
                        {
                            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                        }
                        if($this->session->flashdata('error'))
                        {
                            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                        }
                        ?>
                        </div>
                        </div>
                        <?php 
                        if(count($department) > 0)
                        {
                            echo form_open(base_url( 'Adminmanagement/adddepartment/'.$this->uri->segment('3').'' ), array( 'id' => 'addDepratmentForm' ));
                        }
                        else
                        {
                            echo form_open(base_url( 'Adminmanagement/adddepartment' ), array( 'id' => 'addDepratmentForm' ));
                        }
                        ?>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="card-box">
                              <div class="row">
                                <div class="col-md-12">
                                <div class="alert alert-danger showErrorMessage" style="display:none">Please choose atleast one module</div>
                                <div class="form-group"> 
                                <label for="field-1" class="control-label">Department Name</label>
                                <input type="hidden" name="departmentId" value="<?php echo $this->uri->segment('3') ?>"/>
                                <input type="text" value="<?php echo $department[0]->houdin_admin_department_name ?>" name="departmentName" class="form-control required_validation_for_add_department name_validation" placeholder="Department Name">
                                <select class="form-control required_validation_for_add_department" name="departmentStatus">
                                <option value="">Choose Department Status</option>
                                <option <?php if($department[0]->houdin_admin_department_status == 'active') { ?> selected="selected" <?php } ?> value="active">Active</option>
                                <option <?php if($department[0]->houdin_admin_department_status == 'deactive') { ?> selected="selected" <?php } ?> value="deactive">Deactive</option>
                                </select>
                                </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="">
                                <?php  $getExplodeData = explode(',',$department[0]->houdin_admin_department_access); ?>
                                <div class="col-md-4">
                                <div class="form-group">
                                <label for="field-1" class="control-label text-black">
                                <input type="checkbox" <?php if(in_array('shop',$getExplodeData)) {  ?> checked="checked" <?php } ?> class="commonCheckBoxClaas" name="acces[]" value="shop" class="m-r-10">&nbsp;Shop Management</label><br>
                                </div>
                                </div>
                                <div class="col-md-4">
                                <div class="form-group">
                                <label for="field-1" class="control-label text-black">
                                <input type="checkbox" <?php if(in_array('package',$getExplodeData)) {  ?> checked="checked" <?php } ?> class="commonCheckBoxClaas" name="acces[]" value="package" class="m-r-10">&nbsp;Package</label><br>
                                 
                                </div>
                                </div>
                                <div class="col-md-4">
                                <div class="form-group">
                                <label for="field-1" class="control-label text-black">
                                <input type="checkbox" <?php if(in_array('setting',$getExplodeData)) {  ?> checked="checked" <?php } ?> class="commonCheckBoxClaas" name="acces[]" value="setting" class="m-r-10">&nbsp;Setting</label><br>
                                </div>
                                </div>
                                <div class="col-md-4">
                                <div class="form-group">
                                <label for="field-1" class="control-label text-black">
                                <input type="checkbox" <?php if(in_array('admin',$getExplodeData)) {  ?> checked="checked" <?php } ?> class="commonCheckBoxClaas" name="acces[]" value="admin" class="m-r-10">&nbsp;Admin Management</label><br>
                                 
                                </div>
                                </div>

                                <div class="col-md-4">
                                <div class="form-group">
                                <label for="field-1" class="control-label text-black">
                                <input type="checkbox" <?php if(in_array('log',$getExplodeData)) {  ?> checked="checked" <?php } ?> class="commonCheckBoxClaas" name="acces[]" value="log" class="m-r-10">&nbsp;Admin Logs</label><br>
                                 
                                </div>
                                </div>

                                <div class="col-md-4">
                                <div class="form-group">
                                <label for="field-1" class="control-label text-black">
                                <input type="checkbox" <?php if(in_array('payment',$getExplodeData)) {  ?> checked="checked" <?php } ?> class="commonCheckBoxClaas" name="acces[]" value="payment" class="m-r-10">&nbsp;Payment</label><br>
                                 
                                </div>
                                </div>
                                <div class="col-md-12">
                                <div class="form-group pull-right">
                                <input type="submit" class="btn btn-primary" value="Add Department" name="addDepartmentData"/>
                                </div>
                                </div>
                                </div>
                                </div>
                            </div>
                          </div>
                          <?php echo form_close(); ?>
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->
<?php $this->load->view('Template/footer.php') ?>
<!-- CLient side form validation -->
<script type="text/javascript">
            $(document).ready(function(){
                $(document).on('submit','#addDepratmentForm',function(){
                    $('.showErrorMessage').hide();
                    var check_required_field='';
                    $(this).find(".required_validation_for_add_department").each(function(){
                        var val22 = $(this).val();
                        if (!val22){
                            check_required_field =$(this).size();
                            $(this).css("border-color","#ccc");
                            $(this).css("border-color","red");
                        }
                        $(this).on('keypress change',function(){
                            $(this).css("border-color","#ccc");
                        });
                    });
                    if(check_required_field)
                    {
                        return false;
                    }
                    else {
                        var setData = 0;
                        $('.commonCheckBoxClaas').each(function(){
                            if($(this).prop('checked') == true)
                            {
                                setData++;
                            }
                        });
                        if(setData != 0)
                        {
                            return true;
                            
                        }
                        else
                        {
                            $('.showErrorMessage').show();
                            return false;
                        }
                    }
                });
            });
            </script>