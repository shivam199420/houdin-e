<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<div id="content"> 
    
    <!-- Linking -->
    <div class="linking">
      <div class="container">
        <ol class="breadcrumb">
          <li class="active">Contact</li>
        </ol>
      </div>
    </div>
    
    <!-- Contact -->
    <section class="contact-sec padding-top-40 padding-bottom-80">
      <div class="container"> 
        <!-- Conatct -->
        <div class="contact">
          <div class="contact-form"> 
          <?php 
		if($this->session->flashdata('error'))
		{
			echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
		}
		else if($this->session->flashdata('success'))
		{
			echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
		}
		?>
            <!-- FORM  -->
            <!-- <form role="form" id="contact_form" class="contact-form" method="post" onsubmit="return false"> -->
            <?php echo form_open(base_url( 'Contact/addcontact' ), array( 'id' => 'contact_form','class'=>'contact-form' ,'method'=>'post','role'=>'form' ));?>
              <div class="row">
                <div class="col-md-8"> 
                  
                  <!-- Payment information -->
                  <div class="heading">
                    <h2>Dou You have a Question for Us ?</h2>
                  </div>
                  
                  <ul class="row">
                    <li class="col-sm-6">
                      <label>Name
                      <input type="text" name="userName" class="form-control required_validation_for_shop_contact name_validation" placeholder="Name*" />
                      </label>
                    </li>
                    <li class="col-sm-6">
                      <label>Email
                      <input type="text" name="userEmail" class="form-control required_validation_for_shop_contact email_validation name_validation" placeholder="Email*" />
                      </label>
                    </li>
                    <li class="col-sm-12">
                      <label>Subject
                      <input type="text" name="userEmailSub" class="form-control required_validation_for_shop_contact name_validation" placeholder="Subject" />
                      </label>
                    </li>
                    
                    <li class="col-sm-12">
                      <label>Message
                      <textarea name="userEmailBody" rows="5" class="form-control required_validation_for_shop_contact name_validation" placeholder="Message"></textarea>
                      </label>
                    </li>
                    <li class="col-sm-12 no-margin">
                    <input type="submit" class="btn-round btn btn-default" value="Send Message"/>
                    </li>
                  </ul>
                
                </div>
                
                <!-- Conatct Infomation -->
                <div class="col-md-4">
                  <div class="contact-info">
                    <h6> Address:</h6>
                    <p><?php echo $shopInfo[0]->houdinv_shop_address ?></p>
                    <h6>Phone:</h6>
                    <p><?php echo $shopInfo[0]->houdinv_shop_contact_info ?></p>
                    <h6>Email:</h6>
                    <p><a href="mailto:<?php echo $shopInfo[0]->houdinv_shop_customer_care_email ?>">Customer Care: <?php echo $shopInfo[0]->houdinv_shop_customer_care_email ?> </a>
                    <a href="mailto:<?php echo $shopInfo[0]->houdinv_shop_communication_email ?>">Communication: <?php echo $shopInfo[0]->houdinv_shop_communication_email ?> </a></p>
                  </div>
                </div>
                <?php echo form_close(); ?>
              </div>
          </div>
        </div>
      </div>
    </section>

  </div>
<?php 
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer') 
  ?>

  <script type="text/javascript">
		$(document).ready(function(){
			$(document).on('submit','#contact_form',function(){
				var check_required_field='';
				$(this).find(".required_validation_for_shop_contact").each(function(){
					var val22 = $(this).val();
					if (!val22){
						check_required_field =$(this).size();
						$(this).css("border-color","#ccc");
						$(this).css("border-color","red");
					}
					$(this).on('keypress change',function(){
						$(this).css("border-color","#ccc");
					});
				});
				if(check_required_field)
				{
					return false;
				}
				else {
					return true;
				}
			});
		});
		</script>	