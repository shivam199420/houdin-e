<?php
class Adminmanagementmodel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    public function updateDepartmentData($data,$id = false)
    {
        if($id)
        {
            $this->db->where('houdin_admin_department_id',$id);
            $getUpdateData = $this->db->update('houdin_admin_department',$data);
            return $getUpdateData;
        }
        else
        {
            $getUpdateData = $this->db->insert('houdin_admin_department',$data);
            return $getUpdateData;
        }
    }
    public function fetchTotalDepartment()
    {
        $getDepartmentData = $this->db->select('*')->from('houdin_admin_department')->get()->result();
        return array('department'=>$getDepartmentData);
    }
    public function getDepartmentData($data)
    {
        $getDepartmentData = $this->db->select('*')->from('houdin_admin_department')->where('houdin_admin_department_id',$data)->get()->result();
        return array('department'=>$getDepartmentData);
    }
    public function deleteDepartmentData($data)
    {
        $this->db->where('houdin_admin_department_id',$data);
        $deleteStatus = $this->db->delete('houdin_admin_department');
        return $deleteStatus;
    }
    public function getSubadminsDataValue()
    {
        $getDepartments = $this->db->select('*')->from('houdin_admin_department')->where('houdin_admin_department_status','active')->get()->result();
        // get sub admins
        $getAdmindata = $this->db->select('houdin_admin_users.*,houdin_admin_department.houdin_admin_department_name')->from('houdin_admin_users')->where('houdin_admin_user_role_id','1')
        ->join('houdin_admin_department','houdin_admin_department.houdin_admin_department_id = houdin_admin_users.houdin_admin_user_department','left outer')
        ->get()->result();
        return array('departmentsData'=>$getDepartments,'adminData'=>$getAdmindata);
    }
    public function insertsubAdminData($data,$id = false)
    {
        if($id)
        {
            $this->db->where('houdin_admin_user_id',$id);
            $setSubadmins = $this->db->update('houdin_admin_users',$data);
        }
        else
        {
            $setSubadmins = $this->db->insert('houdin_admin_users',$data);
        }
        return $setSubadmins;
    }
    public function deleteSubadmins($data)
    {
        $this->db->where('houdin_admin_user_id',$data);
        $getDeleteStatus = $this->db->delete('houdin_admin_users');
        return $getDeleteStatus;
    }
}