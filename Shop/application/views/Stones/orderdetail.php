<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header') ?>
<style type="text/css">
	/*order-table*/
table.table {
    padding: 20px 10px;
    box-shadow: 0px 0px 4px 0px #cecece;
    margin-bottom: 0px;
}

table.table.main-table tr:nth-child(2) th {
    color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;
    text-transform: capitalize;
    font-size: 20px;
    padding: 7px 15px;

}
table.table.second-table tr th {
    color: #333;
    border-color: #ddd;
    text-transform: capitalize;
    font-size: 14px;
    padding: 7px 15px;
}


section#order-detail {
        padding: 50px 0 40px;
}

button.button {
        background: #000;
    color: #fff;
    padding: 7px 25px;
    border: 1px solid #000;
    border-radius: 2px;
}
table.table td {
    padding: 15px 15px !important;
}
table.table.second-table tr td {
    padding: 8px 15px !important;
    font-size: 14px;
}
table.table-third tr td {
    padding: 5px 15px !important;
}
table.table.second-table tr:last-child {
    border-bottom: 2px solid #0000008f;
}

table.table.main-table ul li {
    text-transform: capitalize;
    line-height: 22px;
}
table.table.main-table ul li b {
    padding-right: 4px;
}

.order-heading h3 {
    text-align: center;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 31px;
    margin-bottom: 50px;
    position: relative;
}

.order-heading h3:after {
    position: absolute;
    border-bottom: 2px solid #000;
    width: 175px;
    height: 20px;
    left: 0;
    right: 0;
    margin: auto;
    content: "";
    bottom: -10px;
}

</style>
<?php
//print_R($orders);
?>
<section id="order-detail">
	<div class="container">
		<div class="row">

   <div class="col-md-12 order-heading">
   	<h3>order-detail</h3>
   </div>

			<div class="col-md-12">
      <div class="table-responsive">
<table class="table main-table" cellpadding="10" cellspacing="10">

<tbody>


	<tr>
		<td>
			<ul>
				<li><b>orderid:</b>#<?php echo $orders->houdinv_order_id ;?></li>
				<li><b>order status:</b><?php echo $orders->houdinv_order_confirmation_status ;?></li>
				<li><b>order date:</b><?php echo date("Y-m-d ,h:i:s",$orders->houdinv_order_created_at) ;?></li>
			</ul>

		</td>
  	<td align="right" style="vertical-align: middle;">
    <?php
$array_return = array("Delivered");
if( in_array($orders->houdinv_order_confirmation_status,$array_return))
{
 ?>
   <form id="return_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
      <input type="text" name="returnOrderId" value="<?php echo $this->uri->segment("3"); ?>"/>
    <input type="text" value="returnOrder" name="returnOrder"/>
     </form>
<button class="btn btn-default return_button">Return Order</button>
<?php
  }

$array_cancel = array("unbilled","Not Delivered","billed","assigned");
if( in_array($orders->houdinv_order_confirmation_status,$array_cancel))
{
 ?>

     <form id="cancel_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
      <input type="text" name="cancelOrderId" value="<?php echo $this->uri->segment("3"); ?>"/>
    <input type="text" value="cancelOrder" name="cancelOrder"/>
     </form>
<button class="btn button cancel_button">Cancel Order</button>
<?php
} ?>
</td>

	</tr>

	<tr><th  colspan="2">confirm products</th></tr>

	<tr>
		<td colspan="2"><table cellpadding="10" width="100%" class="table second-table">
			<tr bgcolor="#f5f5f5">
				<th>product name</th>
				<th>quality</th>
				<th>rate</th>
				<th>tax</th>
				<th>total price</th>
			</tr>
      <tbody>
      <?php
      $total_product_gross = 0;
      foreach($orders->product_all as $detail)
      { ?>
      <tr>
      <td> <?php echo $detail->title ; ?></td>
      <td><?php echo $detail->allDetail->product_Count ; ?></td>
      <td><?php echo $detail->allDetail->product_actual_price; ?></td>
      <td>0</td>
      <td><?php echo $detail->allDetail->total_product_paid_Amount; ?></td>
     </tr>
      <?php
      $total_product_gross = $total_product_gross + $detail->allDetail->total_product_paid_Amount;
      } ?>
      </tbody>


		</table>
	</td>
</tr>


			<tr>
				<td colspan="5" style="border: 0;padding-top: 0 !important;">
					<table class="table-third" cellpadding="10" width="100%" style="text-align: right;">

						<tr>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td><strong>Gross Amount</strong></td>
						<td><?php echo $total_product_gross;?></td>

						</tr>

						<tr>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td><strong>Additional Discount</strong></td>
						<td><?php echo  $orders->houdinv_orders_discount; ?></td>

						</tr>
						<tr>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td><strong>Delivery Charges</strong></td>
						<td><?php echo $orders->houdinv_delivery_charge; ?></td>

						</tr>
						<tr>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td><strong>Round Off</strong></td>
						<td><?php echo $orders->houdinv_orders_total_Amount; ?></td>

						</tr>
							<tr>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td><strong>Net Payable</strong></td>
						<td><?php echo $orders->houdinv_orders_total_Amount; ?></td>

						</tr>





					</table>
				</td>

			</tr>

</tbody>
		</table>

</div>

</div>
</div>
</div>
</section>
<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view("".$getShopName."/Template/footer"); ?>

      <script>
      $(document).on("click",".cancel_button",function()
      {
         $("#cancel_form").submit();

      });

        $(document).on("click",".return_button",function()
      {
         $("#return_form").submit();

      });

      </script>
