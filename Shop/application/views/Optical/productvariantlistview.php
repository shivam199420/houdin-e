<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');

$price_all = json_decode($productData['0']->houdin_products_price,true);
if($price_all['discount'] != 0 && $price_all['discount'] != "")
{
  $setDiscountedPrice = $variantData[0]->houdin_products_variants_prices-$price_all['discount'];
  $setOriginalPrice = $variantData[0]->houdin_products_variants_prices;
}
else
{
  $setDiscountedPrice = 0;
  $setOriginalPrice = $variantData[0]->houdin_products_variants_prices;
}
$overall_rating = $reviews_sum->houdinv_product_review_rating;
$total_rating = count($reviews);
$final_rating = $overall_rating/$total_rating;
$getImageData = json_decode($productData[0]->houdinv_products_main_images,true);
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<style>
.checked {
  color: orange;
}

</style>


<!-- About Banner Start -->

<section id="about">

  <div class="container">

    <div class="row">

      <div class="about-heading text-center">

        <h2>View Product</h2>

      </div>

    </div>

  </div>

</section>

<!-- About Banner End -->



<!-- Product Details Part start -->

<section id="details-of-product">

  <div class="container">

    <div class="row">

      <div class="col-md-5 tahsan2">

        <div class="xzoom-container">

          <img class="xzoom" id="xzoom-default" src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $variantData[0]->houdin_products_variants_image;?>" xoriginal="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $variantData[0]->houdin_products_variants_image;?>" alt="" />

          <div class="xzoom-thumbs">

            <?php  $j = 2;
            for($indexthumb = 0; $indexthumb < count($getImageData); $indexthumb++)
            {
              if($getImageData[$indexthumb])
              { ?>
                <a href="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $getImageData[$indexthumb]; ?>"><img class="xzoom-gallery" width="80" src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $getImageData[$indexthumb]; ?>"  xpreview="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $getImageData[$indexthumb]; ?>" alt=""></a>
                <?php $j++; }  } ?>




              </div>

            </div>

          </div>

          <div class="details-product-item col-md-7">

            <div class="product-details">

              <div>

                <h3><?php echo $variantData[0]->houdin_products_variants_title; ?></h3>

                <h4>

                  <?php for($i=1 ; $i<6 ;$i++)
                  {
                    if($i <= $final_rating)
                    {
                      $posi = strpos($final_rating,".");
                      if($posi>=0)
                      {
                        $exp =  explode(".",$final_rating);
                        if($exp[0]==$i)
                        {
                          ?>
                          <i class="fa fa-star-half-o"></i>
                          <?php
                        }
                        else
                        {
                          echo '<i class="fa fa-star"></i>';
                        }
                      }
                      else
                      {
                        ?>
                        <i class="fa fa-star"></i>
                        <?php
                      }
                    }
                    else
                    {
                      ?>

                      <i class="fa fa-star-o"></i>


                    <?php } } ?>

                    <span>| (<?php echo $total_rating; ?> Reviews)</span>

                  </h4>

                  <h5>      <?php
                  if($setDiscountedPrice != 0)
                  {
                    ?>
                    <span class="new"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $variantData[0]->houdinv_products_variants_final_price; ?></span>
                    <span class="old">(<?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $price_all['sale_price'];  ?>)</span>
                  <?php }
                  else
                  {
                    ?>
                    <span class="new">(<?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $setOriginalPrice;  ?>)</span>
                  <?php }
                  ?>
                </h5>

                <p><?php echo $variantData[0]->houdin_products_short_desc; ?></p>

              </div>

            </div>

            <div class="color-select category">

              <div class="checkbox chek2 checkbox-success check22">

                <ul>

                  <li><span>Availability</span></li>

                  <li>
                    <?php
                    if($variantData[0]->houdinv_products_total_stocks > 0)
                    {
                      $setStock = "In Stock";
                    }
                    else
                    {
                      $setStock = "Out of Stock";
                    }
                    ?>
                    <p><?php echo $setStock; ?></p>

                  </li>

                </ul>

              </div>


            </div>

            <div class="quantity">



              <div class="handle-counter" id="handleCounter">

                <div class="quan-head">

                  <h3>Quantity :</h3>

                </div>

                <div class="input">

                  <input value="1" min="1" data-max="<?php echo $variantData[0]->houdinv_products_total_stocks ?>" name="qttybutton" class="cart-plus-minus-box setCartQunatity" type="text">
                  <!-- <input type="text" value="1"> -->

                </div>

                <div class="click">

                  <button class="counter-plus btn btn-primary">+</button>

                  <button class="counter-minus btn btn-primary">-</button>

                </div>

                <div class="clearfix"></div>

              </div>

            </div>

            <div class="add-wishlist">

              <a href="javascript:;" class="Add_to_cart_button" data-variant="0"  data-cart="<?php echo $variantData[0]->houdin_products_id ?>" title="Add to Cart">
                <i class="fa fa-heart"></i>
              </a>
              <a href="javascript:;" class="Add_to_whishlist_button" data-variant="0"  data-cart="<?php echo $variantData[0]->houdin_products_id ?>" title="wishlist">
                <i class="fa fa-shopping-basket"></i>
              </a>

            </div>

            <!-- <div class="code-cate">

            <p>Code<a href="#">: SKY-12345678</a></p>

            <p>Category<a href="#" class="tahsan3">: Fashion</a></p>

            <p>Tags<a href="#" class="tahsan5">: T-shirt, Skirt</a></p>

          </div>

          <div class="share">

          <h6>Share:</h6>

          <a href="#"><i class="fa fa-facebook"></i></a>

          <a href="#"><i class="fa fa-twitter"></i></a>

          <a href="#"><i class="fa fa-behance"></i></a>

          <a href="#"><i class="fa fa-linkedin"></i></a>

          <a href="#"><i class="fa fa-pinterest-p"></i></a>

        </div> -->

      </div>

    </div>

  </div>

</section>

<!-- product details end -->



<!-- Product Discription Part start -->

<section id="discription">

  <div class="container">

    <div class="">

      <div class="">

        <ul class="nav nav-tabs" role="tablist">

          <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Description</a></li>

          <li><a href="#">|</a></li>

          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Reviews</a></li>

        </ul>

        <!-- Tab panes -->

        <div class="tab-content">

          <div role="tabpanel" class="tab-pane active" id="home">

            <p>  <?php echo $productData[0]->houdin_products_desc; ?>			</p>
          </div>

          <div role="tabpanel" class="tab-pane" id="profile"><div class="row">
            <div class="col-md-6">
                                <?php
                                if(!$final_rating > 0)
                                {
                                ?>
                                <p>There are no reviews yet.</p>
                                    <p class="text-bigger">Be the first to review</p>
                                <?php }
                                else
                                {
                                    foreach($reviews as $user_Review)
                                      {
                                ?>
                                 <div style="margin-bottom:10px">
                                          <div class=""><?php echo $user_Review->houdinv_product_review_user_name ?></div>


                                          <div class="strat-rating">
                                          <?php for($i=1 ; $i<6 ;$i++)
                                          {
                                          if($i <= $user_Review->houdinv_product_review_rating)
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star checked"></span>
                                          <?php }
                                          else
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star"></span>
                                          <?php } } ?>

                                            <span class="text-date"><?php echo $user_Review->houdinv_product_review_created_at ?></span>
                                          </div>


                                          <div class=""><?php echo  $user_Review->houdinv_product_review_message; ?></div>
                                        </div>

                                <?php } }
                                ?>


                                </div>
                <div class="col-md-6">
                  <div class="form-add table-responsive">
                    <?php echo form_open(base_url( 'Productlistview' ), array( 'id' => 'rtng_form',"class"=>"checkout_form", 'method'=>'post' ));?>
                    <div class="form-group">

                      <div class="strat-rating">
                        <span data-rating="1" style="font-size: 22px;" class="fa fa-star "></span>
                        <span data-rating="2" style="font-size: 22px;" class="fa fa-star "></span>
                        <span data-rating="3" style="font-size: 22px;" class="fa fa-star "></span>
                        <span data-rating="4" style="font-size: 22px;" class="fa fa-star "></span>
                        <span data-rating="5" style="font-size: 22px;" class="fa fa-star"></span>
                        <input type="hidden"  name="rating" class="rating_data" value="0" />
                        <input type="hidden"  name="product_id" class="product" value="0" />
                        <input type="hidden"  name="variant_id" class="" value="<?php echo $this->uri->segment('3'); ?>" />
                      </div>
                    </div>
                    <div class="form-group">

                      <input  class="required_validation_for_review name_validation form-control" type="text" name="user_name" placeholder="Your Name" />
                    </div>
                    <div class="form-group">

                      <input  class="required_validation_for_review name_validation email_validation form-control" type="text" name="user_email" placeholder="Your Email" />
                    </div>
                    <div class="form-group">

                      <textarea  class="required_validation_for_review form-control" name="user_message" rows="3" placeholder="Write a review"></textarea>
                    </div>
                    <input class="btn-round submit_form_review" type="submit" name="review_submit" value="Add Review" />
                    <?php echo form_close(); ?>
                  </div>
                </div>
              </div>
            </div>

          </div>


        </div>

      </div>

    </div>

  </section>

  <!-- product discription end -->

  <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer'); ?>

  <script type="text/javascript">
  $(document).ready(function(){
    $(document).on('keyup','.setCartQunatity',function(){
      if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
      {
        $(this).val($(this).attr('data-max'));
      }
    })
    $(document).on('change','.setCartQunatity',function(){
      if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
      {
        $(this).val($(this).attr('data-max'));
      }
    })
  })
  </script>



  <script src="<?php echo base_url() ?>Extra/optical/js/slick.js"></script>

  <script src="<?php echo base_url() ?>Extra/optical/js/jquery.countdown.min.js"></script>

  <script src="<?php echo base_url() ?>Extra/optical/js/handleCounter.js"></script>

  <script src="<?php echo base_url() ?>Extra/optical/js/xzoom.min.js"></script>

  <script src="<?php echo base_url() ?>Extra/optical/js/setup.js"></script>

  <script src="<?php echo base_url() ?>Extra/optical/js/jquery.meanmenu.min.js"></script>

  <script>

  $(function ($) {

    var options = {

      minimum: 1,

      maximize: 100,

      onChange: valChanged,

      onMinimum: function(e) {

        console.log('reached minimum: '+e)

      },

      onMaximize: function(e) {

        console.log('reached maximize'+e)

      }

    }

    $('#handleCounter').handleCounter(options)

    $('#handleCounter2').handleCounter(options)

    $('#handleCounter3').handleCounter({maximize: 100})

  })

  function valChanged(d) {

    //            console.log(d)

  }

  </script>
