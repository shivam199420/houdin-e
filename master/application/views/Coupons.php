<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <div class="btn-group pull-right m-t-15">
                                    <button type="button" class="btn btn-default btn-sm m-l-5" data-toggle="modal" data-target="#Add_coupons"> <i class="fa fa-plus m-r-5"></i>Add Coupon</button>
                                </div>
                                <h4 class="page-title">Coupons</h4>

                            </div>
                        </div>

<?php
                                    
            if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    ?>
           
        <div class="row">
            <div class="col-md-6 col-lg-4">
                  <div class="widget-panel widget-style-2 bg-light">
                      <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Coupon.png">
                      <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php echo $totalRec;?></b></h2>
                      <div class="text-muted m-t-5 custom_width_sm">Total Coupons</div>
                      <div class="clearfix"></div>
                  </div>
            </div>
            <div class="col-md-6 col-lg-4">
                  <div class="widget-panel widget-style-2 bg-light">
                      <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Coupon.png">
                      <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php echo $ActiveCoupon;?></b></h2>
                      <div class="text-muted m-t-5 custom_width_sm">Total Active Coupons</div>
                      <div class="clearfix"></div>
                  </div>
            </div>

            <div class="col-md-6 col-lg-4">
                  <div class="widget-panel widget-style-2 bg-light">
                      <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Coupon.png">
                      <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php echo $ExpireCoupon;?></b></h2>
                      <div class="text-muted m-t-5 custom_width_sm">Total Expired Coupons</div>
                      <div class="clearfix"></div>
                  </div>
            </div>
      
                        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card-box table-responsive">
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th>Coupon</th>
                    <th>Date of usages</th>
                    <th>Total Users</th>
                    <th>Discount</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                
               // print_R($Coupon);
                foreach($Coupon as $data) {
                    $data= (object)$data; 
                    ?>
                  <tr>
                    <td><?php echo $data->houdin_coupon_code;?></td>
                    <td> <?php echo date("d-m-Y",$data->houdin_coupon_valid_from); ?> - <?php echo date("d-m-Y",$data->houdin_coupon_valid_to);?></td>
                    <td>0</td>
                    <td><?php echo $data->houdin_coupon_discount;?> %</td>
                    <td><button type="button" class="btn btn-<?php if($data->houdin_coupon_status=='active'){ echo "success";} else { echo "warning"; }?> btn btn-xs"><?php echo $data->houdin_coupon_status;?></button></td>
                    <td>
                        <button type="button" class="btn btn-warning btn btn-sm edit_popup" data-toggle="modal" data-target="#edit_coupon" ><i class="fa fa-pencil m-r-5"></i>Edit</button>
                         <input type="hidden" id="edit_all_data" data-dis="<?php echo $data->houdin_coupon_discount;?>" data-limit="<?php echo $data->houdin_coupon_limit;?>" 
                         data-type="<?php echo $data->houdin_coupon_type;?>" data-from="<?php echo date("Y-m-d",$data->houdin_coupon_valid_from) ;?>" data-to="<?php echo date("Y-m-d",$data->houdin_coupon_valid_to);?>"  name="<?php echo $data->houdin_coupon_code;?>" data-id="<?php echo $data->houdin_coupon_id;?>" data-status="<?php echo $data->houdin_coupon_status;?>"/>
                        <button type="button" class="btn btn-warning btn btn-sm delete_popup" data-toggle="modal" data-target="#delete_coupon"><i class="fa fa-trash m-r-5"></i>Delete</button>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>


                    </div> <!-- container -->

                </div> <!-- content -->

                <div id="Add_coupons" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
<?php echo form_open(base_url( 'Coupons' ),  array( 'method' => 'post', 'id'=>"Add_form" ));?>
 
                            <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                  <h4 class="modal-title">Add Coupon</h4>
                                </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label col-sm-8">Coupon Code</label>
                                              <label for="field-7" class="control-label col-sm-4" style="text-align:right;"><input type="checkbox" class="code_generate" />Generate Code</label>
                                            <input type="text" class="form-control template_name required_validation_for_register" maxlength="6" name="name" placeholder="Template Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Discount(%)</label>
                                            <input type="text" class="form-control required_validation_for_register" name="discount" placeholder="Discount(%)" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Limit</label>
                                            <input type="text" class="form-control required_validation_for_register" name="limit" placeholder="Limit" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Type</label>
                                            <select class="form-control required_validation_for_register" name="type">
                                              <option  value="">1</option>
                                              <option>1</option>
                                              <option>1</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">vaild From</label>
                                            <input type="text" class="form-control date_picker required_validation_for_register from_date" name="date_From" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label ">Vaild To</label>
                                            <input type="text" class="form-control date_picker required_validation_for_register to_date" name="date_to" placeholder="" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Status</label>
                                            <select class="form-control required_validation_for_register" name="status">
                                              <option  value="">Choose Status</option>
                                              <option value="active">Active</option>
                                              <option value="deactive">Deactive</option>
                                              <option value="block">Blocked</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                              </div>

                          <div class="modal-footer">
                              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                              <input type="submit" class="btn btn-info" name="Add" value="Submit">
                          </div>

                     </div>
                <?php echo form_close();?>
                 </div>
                </div>

                <div id="edit_coupon" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
<?php echo form_open(base_url( 'Coupons' ),  array( 'method' => 'post', 'id'=>"Edit_form" ));?>
 
                            <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                  <h4 class="modal-title">Edit Coupon</h4>
                                </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label col-sm-8">Coupon Code</label>
                                              <label for="field-7" class="control-label col-sm-4" style="text-align:right;"><input type="checkbox" class="code_generate" />Generate Code</label>
                                            <input type="hidden" id="Edit_id" name="Edit_id">
                                            <input type="text" id="Edit_code" class="form-control template_name required_validation_for_register" maxlength="6" name="name"  placeholder="Template Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Discount(%)</label>
                                            <input type="text" id="Edit_discount" class="form-control required_validation_for_register" name="discount" placeholder="Discount(%)" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Limit</label>
                                            <input type="text" id="Edit_limit" class="form-control required_validation_for_register" name="limit" placeholder="Limit" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Type</label>
                                            <select class="form-control required_validation_for_register" name="type" id="Edit_type">
                                              <option value="">select</option>
                                              <option>1</option>
                                              <option>1</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">vaild From</label>
                                            <input type="text" id="Edit_from" class="form-control date_picker required_validation_for_register from_date" name="date_From" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Vaild To</label>
                                            <input type="text"  id="Edit_to" class="form-control date_picker required_validation_for_register to_date" name="date_to" placeholder="" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Status</label>
                                            <select class="form-control required_validation_for_register" name="status" id="Edit_status">
                                              <option value="">Choose Status</option>
                                              <option value="active">Active</option>
                                              <option value="deactive">Deactive</option>
                                              <option value="block">Blocked</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                              </div>

                          <div class="modal-footer">
                              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                              <input type="submit" class="btn btn-info" name="Edit" value="Submit">
                          </div>

                     </div>
               <?php echo form_close();?>
                 </div>
                </div>


  <div id="delete_coupon" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog">
 <?php echo form_open(base_url( 'Coupons' ),  array( 'method' => 'post', 'id'=>"delete_form" ));?>
  
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
  <h4 class="modal-title">Delete Entry</h4>
  </div>
  <div class="modal-body">
  <div class="row">
  <div class="col-md-12">
  <h4><b>Do you really want to delete this entry ?</b></h4>
  </div>
  </div>
  </div>
  <div class="modal-footer">
  <input type="hidden" id="delete_input" name="delete_input">
  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
  <input type="submit" class="btn btn-info" name="delete" value="Delete">
  </div>
  </div>
  <?php echo form_close();?>
  </div>
  </div>
  <?php $this->load->view('Template/footer.php') ?>
        <script type="text/javascript">
    /*==============Form Validation===========================*/
        $(document).ready(function(){

        $(document).on('submit','#Edit_form,#Add_form',function(c){
            
                               var rep_image_val='';
                 $(this).find(".required_validation_for_register").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                        
                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
                
                $(this).find('.required_validation_for_register').on('keyup blur',function()
                                {
                                    
                                        $(this).css("border-color","#ccc");
                                });
                                
                            if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                else
                {
                    var from = $(this).find('.from_date').val();
                    var to = $(this).find('.to_date').val();
                   
                    if(from>to)
                  {
                        c.preventDefault();
                      alert("valid from date must be less then valid to date for coupan");
                   }
                }  
            
          });
          
          
          // category edit detail fill 
          $(document).on('click','.edit_popup',function()
          {
            console.log($(this).children('#edit_all_data'));
            
            $('#Edit_code').val($(this).siblings('#edit_all_data').attr("name"));
             $('#Edit_id').val($(this).siblings('#edit_all_data').attr("data-id"));
             
              $('#Edit_discount').val($(this).siblings('#edit_all_data').attr("data-dis"));
               $('#Edit_limit').val($(this).siblings('#edit_all_data').attr("data-limit"));
                $('#Edit_type').val($(this).siblings('#edit_all_data').attr("data-type"));
                 $('#Edit_from').val($(this).siblings('#edit_all_data').attr("data-from"));
                 $('#Edit_to').val($(this).siblings('#edit_all_data').attr("data-to"));
            
               $('#Edit_status').val($(this).siblings('#edit_all_data').attr("data-status"));
          });
          
                    // category delete detail fill 
          $(document).on('click','.delete_popup',function()
          {
            console.log($(this).parents('#edit_all_data'));
            $('#delete_input').val($(this).siblings('#edit_all_data').attr("data-id"));
           
          });
          
        });
       
        
        $(document).on('change','.code_generate',function()
        {
            
if($(this).is(':checked')){
var rString = randomString(6, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
      $(this).parents('label').siblings('.template_name').val(rString);
      }
      else
      {
         $(this).parents('label').siblings('.template_name').val(''); 
      }
     // alert($(this).parents('lable').siblings('.template_name').val());
        });

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}      
        
            </script>