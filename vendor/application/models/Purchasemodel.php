<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Purchasemodel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
    }
    public function getSupplierlist()
    {
        $getSupplierList = $this->db->select('houdinv_supplier_id,houdinv_supplier_contact_person_name')->from('houdinv_suppliers')->where('houdinv_supplier_active_status','active')->get()->result();
        return array('supplierList'=>$getSupplierList);
    }
    public function fetchSupplierProductList($data)
    {
        $getSupplierProduct = $this->db->select('houdinv_supplier_products.houdinv_supplier_products_product_id,houdinv_products.houdin_products_title')->from('houdinv_supplier_products')->where('houdinv_supplier_products.houdinv_supplier_products_supplier_id',$data)
        ->join('houdinv_products','houdinv_products.houdin_products_id = houdinv_supplier_products.houdinv_supplier_products_product_id','left outer')->get()->result();
        if(count($getSupplierProduct) > 0)
        {
            $setMessgae = "";
            $i=0;
            foreach($getSupplierProduct as $getSupplierProductData)
            {
                $setMessgae .= '<tr class="parentRowClass">
                           <td>
                           <input type="checkbox" class="childSupplierProductCheck"  data-variant="0" data-id="'.$getSupplierProductData->houdinv_supplier_products_product_id.'">
                           </td>
                           <td>'.$getSupplierProductData->houdin_products_title.'</td>
                           <td>
                           <input type="text" class="form-control setProductQuantity name_validation number_validation" readonly="readonly" name="productQuantity[]"/>
                           <input type="hidden" class="setSelectedProductId" name="purchaseProductId[]"/>
                          <input type="hidden" class="setSelectedProductVariantId" name="purchaseProductVariantId[]"/>
                          
                           </td>
                            </tr>'.",";
                            
         $getAllData = $this->db->select("*")
                  ->from("houdinv_products_variants")
                  ->where("houdin_products_variants_product_id",$getSupplierProductData->houdinv_supplier_products_product_id)->get()->result();                   
        
          foreach($getAllData as $return)
          {
             $setMessgae .= '<tr class="parentRowClass">
                           <td>
                           <input type="checkbox" class="childSupplierProductCheck" data-variant="'.$return->houdin_products_variants_id.'" data-id="'.$getSupplierProductData->houdinv_supplier_products_product_id.'">
                           </td>
                           <td>'.$return->houdin_products_variants_title.'</td>
                           <td>
                           <input type="text" class="form-control setProductQuantity name_validation number_validation" readonly="readonly" name="productQuantity[]"/>
                           <input type="hidden" class="setSelectedProductId" name="purchaseProductId[]"/>
                           <input type="hidden" class="setSelectedProductVariantId" name="purchaseProductVariantId[]"/>
                           </td>
                            </tr>'.","; 
          }
          
          $i++;
            }
            $setMessgae = rtrim($setMessgae,",");
            return array('message'=>$setMessgae);
        }
        else
        {
            $setMessage = '<tr><td colspan="3" style="text-align: center;">No product assign to selected supplier</td></tr>';
            return array('message'=>$setMessage);
        }
    }
    public function createPurchaseOrder($data)
    {
        $setData = strtotime(date('Y-m-d'));
        $setInsertArray = array('houdinv_inventory_purchase_credit'=>$data['credit'],'houdinv_inventory_purchase_status'=>'in process','houdinv_inventory_purchase_created_at'=>$setData,'houdinv_inventory_purchase_supplier_id'=>$data['supplierId'],'houdinv_inventory_purchase_delivery'=>strtotime($data['deliveryDate']),'houdinv_inventory_purchase_time'=>$data['creditTime']);
        $this->db->insert('houdinv_inventory_purchase',$setInsertArray);
        $getPurchaseId = $this->db->insert_id();
        if($getPurchaseId)
        {
            for($index = 0; $index < count($data['id']); $index++)
            {
                if($data['id'][$index])
                {
                    if($data['variant_id'][$index] != 0 && $data['variant_id'][$index])
                    {
                        $setPurchaseProductArray = array('houdinv_purchase_products_purchase_id'=>$getPurchaseId,'houdinv_purchase_products_product_id'=>0,'houdinv_purchase_products_quantity'=>$data['quantity'][$index],'houdinv_purchase_products_product_variant_id'=>$data['variant_id'][$index]);
                    }
                    else
                    {
                        $setPurchaseProductArray = array('houdinv_purchase_products_purchase_id'=>$getPurchaseId,'houdinv_purchase_products_product_id'=>$data['id'][$index],'houdinv_purchase_products_quantity'=>$data['quantity'][$index],'houdinv_purchase_products_product_variant_id'=>0);
                    }
                    $this->db->insert('houdinv_purchase_products',$setPurchaseProductArray);
                }
            }
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function updatePurchaseOrder($data)
    {
        $setData = strtotime(date('Y-m-d'));
        $setUpdateArray = array('houdinv_inventory_purchase_credit'=>$data['credit'],'houdinv_inventory_purchase_modified_at'=>$setData,'houdinv_inventory_purchase_supplier_id'=>$data['supplierId'],'houdinv_inventory_purchase_delivery'=>strtotime($data['deliveryDate']));
        $this->db->where('houdinv_inventory_purchase_id',$data['purchaseId']);
        $getUpdateData = $this->db->update('houdinv_inventory_purchase',$setUpdateArray);
        if($getUpdateData == 1)
        {
            $this->db->where('houdinv_purchase_products_purchase_id',$data['purchaseId']);
            $this->db->delete('houdinv_purchase_products');
            for($index = 0; $index < count($data['id']); $index++)
            {
                $setPurchaseProductArray = array('houdinv_purchase_products_purchase_id'=>$data['purchaseId'],'houdinv_purchase_products_product_id'=>$data['id'][$index],'houdinv_purchase_products_quantity'=>$data['quantity'][$index]);
                $this->db->insert('houdinv_purchase_products',$setPurchaseProductArray);
            }
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function getPurchaseTotalCount()
    {
        $this->db->select('COUNT(houdinv_inventory_purchase_id) AS totalCount')->from('houdinv_inventory_purchase');
        $getTotalCount = $this->db->get()->result();
        return array('purchaseList'=>$getTotalCount[0]->totalCount);
    }
    public function fetchPurchaseDetails($data)
    {
        $this->db->select('houdinv_inventory_purchase.*,houdinv_suppliers.houdinv_supplier_contact_person_name')->from('houdinv_inventory_purchase')
        ->join('houdinv_suppliers','houdinv_suppliers.houdinv_supplier_id = houdinv_inventory_purchase.houdinv_inventory_purchase_supplier_id','left outer');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getPurchasedata = $this->db->get();
        $resultPurchasedata = ($getPurchasedata->num_rows() > 0)?$getPurchasedata->result():FALSE;
        // count total purchase
        $getTotalCount = $this->db->select('COUNT(houdinv_inventory_purchase_id) AS totalPurchase')->from('houdinv_inventory_purchase')->get()->result();
        return array('purchaseList'=>$resultPurchasedata,'totalPurchase'=>$getTotalCount[0]->totalPurchase);
    }
    public function deletePurchaseData($data)
    {
        $explodData = explode(',',$data);
        $this->db->where_in('houdinv_inventory_purchase_id',$explodData);
        $getDeleteStatus = $this->db->delete('houdinv_inventory_purchase');
        if($getDeleteStatus == 1)
        {
            $this->db->where_in('houdinv_purchase_products_purchase_id',$explodData);
            $getDeletePurchaseStatus = $this->db->delete('houdinv_purchase_products');
            if($getDeletePurchaseStatus == 1)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'yes');
            }
        }   
        else
        {
            return array('message'=>'no');
        }
    }
    public function getPurchaseInfo($data)
    {
        $getBasicDeatail = $this->db->select('houdinv_inventory_purchase.*,houdinv_suppliers.houdinv_supplier_contact_person_name')->from('houdinv_inventory_purchase')->where('houdinv_inventory_purchase.houdinv_inventory_purchase_id',$data)
        ->join('houdinv_suppliers','houdinv_suppliers.houdinv_supplier_id = houdinv_inventory_purchase.houdinv_inventory_purchase_supplier_id','left outer')->get()->result();
      
        $getProductDetail = $this->db->select('houdinv_purchase_products.*,houdinv_products.houdin_products_title')->from('houdinv_purchase_products')->where('houdinv_purchase_products.houdinv_purchase_products_purchase_id',$data)
        ->join('houdinv_products','houdinv_products.houdin_products_id = houdinv_purchase_products.houdinv_purchase_products_product_id','left outer')->get()->result();
       
       foreach($getProductDetail as $pro)
       {
        if(($pro->houdinv_purchase_products_product_variant_id) >0 )
        {
            
             
            $getAllData = $this->db->select("*")
                  ->from("houdinv_products_variants")
                  ->where("houdin_products_variants_id",$pro->houdinv_purchase_products_product_variant_id)->get()->row();                   
        
          $pro->variant_title= $getAllData->houdin_products_variants_title; 
        }
        else
        {
        $pro->variant_title= '';    
        }
       $getProductDetail1[] = $pro;
       }
        return array('basicList'=>$getBasicDeatail,'productList'=>$getProductDetail1);
    }
    public function fetchInwardData($data)
    {
        $mainArray = array();
        $getInwardProduct = $this->db->select('*')->from('houdinv_purchase_products')->where('houdinv_purchase_products_purchase_id',$data)->get()->result();
        foreach($getInwardProduct as $getInwardProductList)
        {
            $setArray = array();
            $getQuantityOrdered = $getInwardProductList->houdinv_purchase_products_quantity;
            $getPurchaseProductId = $getInwardProductList->houdinv_purchase_products_id;
            if($getInwardProductList->houdinv_purchase_products_product_id != 0)
            {
                $getProductData = $this->db->select('*')->from('houdinv_products')->where('houdin_products_id',$getInwardProductList->houdinv_purchase_products_product_id)->get()->result();
                $setArray['main'] = array('productId'=>$getProductData[0]->houdin_products_id,'variantId'=>0,'productName'=>$getProductData[0]->houdin_products_title,'quantity'=>$getQuantityOrdered,'purchaseProductId'=>$getPurchaseProductId);
                // fetch inward data
                $getInwardData = $this->db->select('*')->from('houdinv_purchase_products_inward')->where('houdinv_purchase_products_inward_purchase_product_id',$getProductData[0]->houdin_products_id)->where('houdinv_purchase_products_inward_purchase_id',$getInwardProductList->houdinv_purchase_products_purchase_id)->get()->result();
                // print_r($getInwardData);
                $setArray['child'] = $getInwardData;
            }   
            else
            {
                $getProductVariantData = $this->db->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getInwardProductList->houdinv_purchase_products_product_variant_id)->get()->result();
                $setArray['main'] = array('productId'=>0,'variantId'=>$getProductVariantData[0]->houdin_products_variants_id,'productName'=>$getProductVariantData[0]->houdin_products_variants_title,'quantity'=>$getQuantityOrdered,'purchaseProductId'=>$getPurchaseProductId);
                // fetch inward data
                $getInwardData = $this->db->select('*')->from('houdinv_purchase_products_inward')->where('houdinv_purchase_products_inward_variant_id',$getInwardProductList->houdinv_purchase_products_product_variant_id)->where('houdinv_purchase_products_inward_purchase_id',$getInwardProductList->houdinv_purchase_products_purchase_id)->get()->result();
                $setArray['child'] = $getInwardData;
            }
            array_push($mainArray,$setArray);
        }
        // fetch outlet data
        $getOutletList = $this->db->select('id,w_name')->from('houdinv_shipping_warehouse')->get()->result();
        // get transaction for purchase
        $getTransactionAmount = $this->db->select('houdinv_transaction_amount')->from('houdinv_transaction')->where('houdinv_transaction_for','inventory purchase')
        ->where('houdinv_transaction_for_id',$data)->get()->result();
        // fetch coinfirm inward data
        // $this->db->selc
        return array('purcahseProduct'=>$mainArray,'outletList'=>$getOutletList,'transactionAmount'=>$getTransactionAmount);
    }
    public function setInwardData($data)
    {
        $setData = strtotime(date('Y-m-d'));
        $setAmountPaid = 0;
        for($index = 0; $index < count($data['quantityRecieved']); $index++)
        {
            $setProduictId = "";
            $setVarinatId = "";
            // for product data
            if($data['mainProductId'][$index] != 0 && $data['mainProductId'][$index] != "")
            {
                // update product quantity
                $getProductStock = $this->db->select('houdinv_products_main_stock,houdin_products_price,houdinv_products_total_stocks')->from('houdinv_products')->where('houdin_products_id',$data['mainProductId'][$index])->get()->result();
                $getJsonDecodeProductData = json_decode($getProductStock[0]->houdinv_products_main_stock,true);
                $getJsonDecodeProductPriceData = json_decode($getProductStock[0]->houdin_products_price,true);
                $setProductQuantityArray = array();
                $setProductPriceArray = array();
                // set product quantity
                for($productQuantity = 0; $productQuantity < count($getJsonDecodeProductData); $productQuantity++)
                {
                    if($getJsonDecodeProductData[$productQuantity]['main'] == $data['outletId'][$index])
                    {
                        $getNewProductQuantity = $getJsonDecodeProductData[$productQuantity]['value']+$data['quantityRecieved'][$index];
                        $setProductQuantityArray[] = array('main'=>$getJsonDecodeProductData[$productQuantity]['main'],'value'=>$getNewProductQuantity);
                    }   
                    else
                    {
                        $setProductQuantityArray[] = array('main'=>$getJsonDecodeProductData[$productQuantity]['main'],'value'=>$getJsonDecodeProductData[$productQuantity]['value']);
                    }
                }
                // set product price
                for($productPrice = 0; $productPrice < count($getJsonDecodeProductPriceData); $productPrice++)
                {
                    $getDiscount = $getJsonDecodeProductPriceData[$productPrice]['discount'];
                    $setProductPriceArray[] = array('price'=>$data['mainprice'][$index],'discount'=>$getDiscount,'cost_price'=>$data['costPrice'][$index],'sale_price'=>$data['salePrice'][$index]);
                }
                $getFinalProductPrice = json_encode($setProductPriceArray);
                $getFinalProductQuantity = json_encode($setProductQuantityArray);
                $getFinalTotalStock = $getProductStock[0]->houdinv_products_total_stocks+$data['quantityRecieved'][$index];
                $setProductQuantiyUpdateArray = array('houdinv_products_main_stock'=>$getFinalProductQuantity,'houdinv_products_total_stocks'=>$getFinalTotalStock,'houdin_products_updated_date'=>$setdata,'houdin_products_price'=>$getFinalProductPrice);
                $this->db->where('houdin_products_id',$data['mainProductId'][$index]);
                $getQuantityUpdateStatus = $this->db->update('houdinv_products',$setProductQuantiyUpdateArray);
            }
            else
            {
                // update variant quantity
                $getProductStock = $this->db->select('houdin_products_variants_stock,houdin_products_variants_prices,houdinv_products_variants_total_stocks')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data['mainVariantId'][$index])->get()->result();
                $getJsonDecodeProductData = json_decode($getProductStock[0]->houdin_products_variants_stock,true);
                $getJsonDecodeProductPriceData = $getProductStock[0]->houdin_products_variants_prices;
                $setProductQuantityArray = array();
                $setProductPriceArray = array();
                // set product quantity
                for($productQuantity = 0; $productQuantity < count($getJsonDecodeProductData); $productQuantity++)
                {
                    if($getJsonDecodeProductData[$productQuantity]['main'] == $data['outletId'][$index])
                    {
                        $getNewProductQuantity = $getJsonDecodeProductData[$productQuantity]['value']+$data['quantityRecieved'][$index];
                        $setProductQuantityArray[] = array('main'=>$getJsonDecodeProductData[$productQuantity]['main'],'value'=>$getNewProductQuantity);
                    }   
                    else
                    {
                        $setProductQuantityArray[] = array('main'=>$getJsonDecodeProductData[$productQuantity]['main'],'value'=>$getJsonDecodeProductData[$productQuantity]['value']);
                    }
                }
                // set product price
                $setFinalPriceData = $data['salePrice'][$index];
                $getFinalProductQuantity = json_encode($setProductQuantityArray);
                $setFinalTotalStock = $getProductStock[0]->houdinv_products_variants_total_stocks+$data['quantityRecieved'][$index];
                $setProductQuantiyUpdateArray = array('houdin_products_variants_prices'=>$setFinalPriceData,'houdin_products_variants_stock'=>$getFinalProductQuantity,'houdinv_products_variants_total_stocks'=>$setFinalTotalStock);
                $this->db->where('houdin_products_variants_id',$data['mainVariantId'][$index]);
                $getQuantityUpdateStatus = $this->db->update('houdinv_products_variants',$setProductQuantiyUpdateArray);
            }
            $getInwardDataValue = $this->db->select('houdinv_purchase_products_inward_id')->from('houdinv_purchase_products_inward')
            ->where('houdinv_purchase_products_inward_purchase_product_id',$data['mainProductId'][$index])
            ->where('houdinv_purchase_products_inward_variant_id',$data['mainVariantId'][$index])
            ->where('houdinv_purchase_products_inward_purchase_id',$this->uri->segment('3'))->get()->result();
            if($data['mainProductId'][$index] != 0 && $data['mainProductId'][$index] != "")
            {
                $setProduictId = $data['mainProductId'][$index];
                $setVarinatId = 0;
            }
            else
            {
                $setProduictId = 0;
                $setVarinatId = $data['mainVariantId'][$index];
            }
            if(count($getInwardDataValue) > 0)
            {
                $getInwardId = $getInwardDataValue[0]->houdinv_purchase_products_inward_id;
                $getReturn = $data['totalOrderedQuantity'][$index]-$data['quantityRecieved'][$index];
                $setUpdatedArray = array(
                    'houdinv_purchase_products_inward_purchase_product_id'=>$setProduictId,
                    'houdinv_purchase_products_inward_variant_id'=>$setVarinatId,
                    'houdinv_purchase_products_inward_quantity_recieved'=>$data['quantityRecieved'][$index],
                    'houdinv_purchase_products_inward_quantity_return'=>$getReturn,
                    'houdinv_purchase_products_inward_sale_price'=>$data['salePrice'][$index],
                    'houdinv_purchase_products_inward_cost_price'=>$data['costPrice'][$index],
                    'houdinv_purchase_products_inward_retail_price'=>$data['mainprice'][$index],
                    'houdinv_purchase_products_inward_total_amount'=>$data['totalAmount'][$index],
                    'houdinv_purchase_products_inward_payment_status'=>$data['paymentStatus'][$index],
                    'houdinv_purchase_products_inward_amount_paid'=>$data['amountPaid'][$index],
                    'houdinv_purchase_products_inward_outlet_id'=>$data['outletId'][$index],
                    'houdinv_purchase_products_inward_purchase_id'=>$this->uri->segment('3'),
                    'houdinv_purchase_products_inward_payment_mode'=>$data['paymentMode'][$index],
                    'houdinv_purchase_products_inward_modified_at'=>$setData
                    );  
                $this->db->where('houdinv_purchase_products_inward_id',$getInwardId);
                $this->db->update('houdinv_purchase_products_inward',$setUpdatedArray);
            }
            else
            {
                $getReturn = $data['totalOrderedQuantity'][$index]-$data['quantityRecieved'][$index];
                $setInsertArray = array(
                    'houdinv_purchase_products_inward_purchase_product_id'=>$setProduictId,
                    'houdinv_purchase_products_inward_variant_id'=>$setVarinatId,
                    'houdinv_purchase_products_inward_quantity_recieved'=>$data['quantityRecieved'][$index],
                    'houdinv_purchase_products_inward_quantity_return'=>$getReturn,
                    'houdinv_purchase_products_inward_sale_price'=>$data['salePrice'][$index],
                    'houdinv_purchase_products_inward_cost_price'=>$data['costPrice'][$index],
                    'houdinv_purchase_products_inward_retail_price'=>$data['mainprice'][$index],
                    'houdinv_purchase_products_inward_total_amount'=>$data['totalAmount'][$index],
                    'houdinv_purchase_products_inward_payment_status'=>$data['paymentStatus'][$index],
                    'houdinv_purchase_products_inward_amount_paid'=>$data['amountPaid'][$index],
                    'houdinv_purchase_products_inward_outlet_id'=>$data['outletId'][$index],
                    'houdinv_purchase_products_inward_payment_mode'=>$data['paymentMode'][$index],
                    'houdinv_purchase_products_inward_purchase_id'=>$this->uri->segment('3'),
                    'houdinv_purchase_products_inward_craeted_at'=>$setData
                    );  
                $this->db->insert('houdinv_purchase_products_inward',$setInsertArray);
                $getInwardId = $this->db->insert_id();
            }
            // update accounting data
            if($data['paymentStatus'][$index] == 'paid')
            {
                if($data['paymentMode'][$index] == 'checque' || $data['paymentMode'][$index] == 'card')
                {
                    // add current fund
                    $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
                    if(count($getUndepositedAccount) > 0)
                    {
                        // get admin id
                        $getMaster = $this->load->database('master',true);
                        $getVendorId = $getMaster->select('houdin_vendor_auth_vendor_id')->from('houdin_vendor_auth')->where('houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))->get()->result();
                        if($getVendorId[0]->houdin_vendor_auth_vendor_id)
                        {
                            $setVendorData = $getVendorId[0]->houdin_vendor_auth_vendor_id;
                        }
                        else
                        {
                            $setVendorData = 0;
                        }
                        $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                        'houdinv_accounts_balance_sheet_ref_type'=>'Current',
                        'houdinv_accounts_balance_sheet_pay_account'=>$setVendorData,
                        'houdinv_accounts_balance_sheet_payee_type'=>'Vendor',
                        'houdinv_accounts_balance_sheet_account'=>0,
                        'houdinv_accounts_balance_sheet_order_id'=>0,
                        'houdinv_accounts_balance_sheet_purchase_id'=>$getInwardId,
                        'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                        'houdinv_accounts_balance_sheet__increase'=>0,
                        'houdinv_accounts_balance_sheet_decrease'=>$data['amountPaid'][$index],
                        'houdinv_accounts_balance_sheet_deposit'=>0,
                        'houdinv_accounts_balance_sheet_final_balance'=>0,
                        'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                        $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                        // get amount
                        $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                        ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                        if(count($getTotalAmount) > 0)
                        {
                            $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                        }
                        else
                        {
                            $setFinalAmount = 0;
                        }
                        $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                    }
                }
                else
                {
                    // add undeposited fund
                    $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->where('houdinv_accounts_detail_type_id','170')->get()->result();    
                    if(count($getUndepositedAccount) > 0)
                    {
                        // get admin id
                        $getMaster = $this->load->database('master',true);
                        $getVendorId = $getMaster->select('houdin_vendor_auth_vendor_id')->from('houdin_vendor_auth')->where('houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))->get()->result();
                        if($getVendorId[0]->houdin_vendor_auth_vendor_id)
                        {
                            $setVendorData = $getVendorId[0]->houdin_vendor_auth_vendor_id;
                        }
                        else
                        {
                            $setVendorData = 0;
                        }
                        $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                        'houdinv_accounts_balance_sheet_ref_type'=>'Undeposited Fund',
                        'houdinv_accounts_balance_sheet_pay_account'=>$setVendorData,
                        'houdinv_accounts_balance_sheet_payee_type'=>'Vendor',
                        'houdinv_accounts_balance_sheet_account'=>0,
                        'houdinv_accounts_balance_sheet_order_id'=>0,
                        'houdinv_accounts_balance_sheet_purchase_id'=>$getInwardId,
                        'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                        'houdinv_accounts_balance_sheet__increase'=>0,
                        'houdinv_accounts_balance_sheet_decrease'=>$data['amountPaid'][$index],
                        'houdinv_accounts_balance_sheet_deposit'=>0,
                        'houdinv_accounts_balance_sheet_final_balance'=>0,
                        'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                        $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                        // get amount
                        $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                        ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                        if(count($getTotalAmount) > 0)
                        {
                            $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                        }
                        else
                        {
                            $setFinalAmount = 0;
                        }
                        $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                    }
                }
                // set suppliers account
                // add undeposited fund
                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','14')->where('houdinv_accounts_detail_type_id','152')->get()->result();    
                if(count($getUndepositedAccount) > 0)
                {
                    // get admin id
                    $getMaster = $this->load->database('master',true);
                    $getVendorId = $getMaster->select('houdin_vendor_auth_vendor_id')->from('houdin_vendor_auth')->where('houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))->get()->result();
                    if($getVendorId[0]->houdin_vendor_auth_vendor_id)
                    {
                        $setVendorData = $getVendorId[0]->houdin_vendor_auth_vendor_id;
                    }
                    else
                    {
                        $setVendorData = 0;
                    }
                    $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                    'houdinv_accounts_balance_sheet_ref_type'=>'Purchase',
                    'houdinv_accounts_balance_sheet_pay_account'=>$setVendorData,
                    'houdinv_accounts_balance_sheet_payee_type'=>'Vendor',
                    'houdinv_accounts_balance_sheet_account'=>0,
                    'houdinv_accounts_balance_sheet_order_id'=>0,
                    'houdinv_accounts_balance_sheet_purchase_id'=>$getInwardId,
                    'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                    'houdinv_accounts_balance_sheet__increase'=>$data['amountPaid'][$index],
                    'houdinv_accounts_balance_sheet_decrease'=>0,
                    'houdinv_accounts_balance_sheet_deposit'=>0,
                    'houdinv_accounts_balance_sheet_final_balance'=>0,
                    'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                    $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                    // get amount
                    $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                    ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                    if(count($getTotalAmount) > 0)
                    {
                        $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                    }
                    else
                    {
                        $setFinalAmount = 0;
                    }
                    $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                }
            }
            // end accounting data
            $setAmountPaid = $setAmountPaid+$data['amountPaid'][$index];
        }
        //update main purchase data
        $masterDB = $this->load->database('master',true);
        $getVendorId = $masterDB->select('houdin_vendor_auth_vendor_id')->from('houdin_vendor_auth')->where('houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))->get()->result();
        $setPurchaseUpdateArray = array('houdinv_inventory_purchase_status'=>'recieve','houdinv_inventory_purchase_recieved_by'=>$getVendorId[0]->houdin_vendor_auth_vendor_id);
        $this->db->where('houdinv_inventory_purchase_id',$data['purchaseId']);
        $getUpdateStatus = $this->db->update('houdinv_inventory_purchase',$setPurchaseUpdateArray);
        if($getUpdateStatus == 1)
        {
            // set transaction array
            $setTxnId = "TXN".rand(99999999,10000000);
            $getFinalAmountPaid = $setAmountPaid-$data['transactionAmount'];
            $setTransactionArray = array('houdinv_transaction_transaction_id'=>$setTxnId,'houdinv_transaction_type'=>'debit','houdinv_transaction_method'=>'cash',
            'houdinv_transaction_from'=>'cash','houdinv_transaction_for'=>'inventory purchase','houdinv_transaction_for_id'=>$data['purchaseId'],'houdinv_transaction_amount'=>$getFinalAmountPaid,
            'houdinv_transaction_date'=>date('Y-m-d'),'houdinv_transaction_status'=>'success');
            $this->db->insert('houdinv_transaction',$setTransactionArray);
            return array('message'=>'yes');
        }
    }
    public function fetchInvoiceData($data)
    {
        $getInvoiceData = $this->db->select('houdinv_inventory_purchase.*,houdinv_suppliers.*')
        ->from('houdinv_inventory_purchase')
        ->where('houdinv_inventory_purchase.houdinv_inventory_purchase_id',$data)
        ->join('houdinv_suppliers','houdinv_suppliers.houdinv_supplier_id = houdinv_inventory_purchase.houdinv_inventory_purchase_supplier_id','left outer')->get()->result();
        $getPurchaseId = $getInvoiceData[0]->houdinv_inventory_purchase_id;
        $getPurchaseProductData = $this->db->select('*')->from('houdinv_purchase_products')->where('houdinv_purchase_products_purchase_id',$getPurchaseId)->get()->result();
        foreach($getPurchaseProductData as $getPurchaseProductDataList)
        {
            if($getPurchaseProductDataList->houdinv_purchase_products_product_id != 0 && $getPurchaseProductDataList->houdinv_purchase_products_product_id != "")
            {
                $getProductName = $this->db->select('houdin_products_title')->from('houdinv_products')->where('houdin_products_id',$getPurchaseProductDataList->houdinv_purchase_products_product_id)->get()->result();
                $setProductTitle = $getProductName[0]->houdin_products_title;
            }
            else
            {
                $getProductName = $this->db->select('houdin_products_variants_title')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getPurchaseProductDataList->houdinv_purchase_products_product_variant_id)->get()->result();
                $setProductTitle = $getProductName[0]->houdin_products_variants_title;
            }
            // get inward details
            $getInwardInfo = $this->db->select('*')->from('houdinv_purchase_products_inward')->where('houdinv_purchase_products_inward_purchase_product_id',$getPurchaseProductDataList->houdinv_purchase_products_id)->get()->result();
            $getSubDataArray[] = array('purchaseProductData'=>$getPurchaseProductDataList,'productTitle'=>$setProductTitle,'inwardData'=>$getInwardInfo);
        }
        return array('mainData'=>$getInvoiceData,'subdata'=>$getSubDataArray);
    }
}  

