<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">Change Password</h2>
                
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- login area end -->
    <div class="login-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="login-content">
                        <div class="login-title">
                            <h4>Change Password</h4>
                        </div>
                        <div class="login-form">
                        <?php 
                            if($this->session->flashdata('error'))
                            {
                                echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
                            }
                            else if($this->session->flashdata('success'))
                            {
                                echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
                            }
                            ?>
                        <?php echo form_open(base_url( 'Changepassword/updatepass' ), array( 'id' => 'changepass', 'method'=>'post' ));?>
                        <input type="password" placeholder="Old password" name="currentPass" class="required_validation_for_change_pass name_validation" />
                        <input type="password" name="newpass" placeholder="New password" class="required_validation_for_change_pass name_validation opass" />
                        <input type="password" name="newconfirmpass" placeholder="Re-enter new password" class="required_validation_for_change_pass name_validation cpass form-control" />
                                <div class="button-remember">
                                <input type="submit" class="btn btn-default acc_btn text-center setDisableData" value="update Password"/>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- login area end -->
    <?php 
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer') 
  ?>
  <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#changepass',function(){
                var check_required_field='';
                $(this).find(".required_validation_for_change_pass").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>