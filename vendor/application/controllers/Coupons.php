<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Coupons extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('Pdf');
        $this->perPage = 100;
        $this->load->model('Couponsmodel');
        $this->auth_users->is_logged_in();
    }
    
    public function Couponslanding()
    {
        $this->load->view('couponlandingpage');
    } 
    
    public function index()
    {
      // delete coupons data
      if($this->input->post('deleteCouponsData'))
      {
        $this->form_validation->set_rules('deleteCouponId','text','required');
        if($this->form_validation->run() == true)
        {
          $getDeleteStatus = $this->Couponsmodel->deleteCouponData($this->input->post('deleteCouponId'));
          if($getDeleteStatus['message'] == 'yes')
          {
            $this->session->set_flashdata('success','Coupons deleted successfully');
            $this->load->helper('url');
            redirect(base_url()."Coupons", 'refresh');
          }
          else
          {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            redirect(base_url()."Coupons", 'refresh');
          }
        }
        else
        {
          $this->session->set_flashdata('error','Something went wrong. Please try again');
          $this->load->helper('url');
          redirect(base_url()."Coupons", 'refresh');
        }
      }
      // Custom Pagination
      $getTotalCount = $this->Couponsmodel->getCouponsTotalCount();
      $config['base_url']    = base_url().'Coupons/';
      $config['uri_segment'] = 2;
      $config['total_rows']  = $getTotalCount['totalRows'];
      $config['per_page']    = $this->perPage;
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
      $config['cur_tag_close'] = '</a></li>';
      $config['next_link'] = 'Next';
      $config['prev_link'] = 'Prev';
      $config['next_tag_open'] = '<li class="pg-next">';
      $config['next_tag_close'] = '</li>';
      $config['prev_tag_open'] = '<li class="pg-prev">';
      $config['prev_tag_close'] = '</li>';
      $config['first_tag_open'] = '<li>';
      $config['first_tag_close'] = '</li>';
      $config['last_tag_open'] = '<li>';
      $config['last_tag_close'] = '</li>';
      $this->pagination->initialize($config);
      $page = $this->uri->segment(2);
      $offset = !$page?0:$page;
      // End Here
      $setArray = array('start'=>$offset,'limit'=>$this->perPage);
      $getCouponList = $this->Couponsmodel->fetchCouponsData($setArray);
      $this->load->view('coupons',$getCouponList);
    }
    public function add()
    {
      // add coupons
      if($this->input->post('addCouponsData'))
      {
        $this->form_validation->set_rules('couponName','Coupon Name','required|trim');
        $this->form_validation->set_rules('couponTagline','Coupon Tagline','required|trim');
        $this->form_validation->set_rules('couponValidFrom','Valid From Date','required');
        $this->form_validation->set_rules('couponValidTo','Valid Untill Date','required');
        $this->form_validation->set_rules('couponCode','Coupone Code','required|trim');
        $this->form_validation->set_rules('couponDiscount','Coupon Discount','required|trim|integer|greater_than[0]|less_than_equal_to[100]');
        $this->form_validation->set_rules('couponPaymentMethod','Coupon Payment Method','required|trim|in_list[both,cod,online payment]');
        $this->form_validation->set_rules('couponUsage','Coupon Usage','required|trim|integer');
        $this->form_validation->set_rules('couponStatus','Coupon Status','required|trim|in_list[active,deactive]');
        if($this->form_validation->run() == true)
        {
          // End Here
          $getCouponApplicable = $this->input->post('couponApplicableonapp');
          if($getCouponApplicable == 'on')
          {
            $setApplicableOn = 'app';
          }
          else
          {
            $setApplicableOn = 'both';
          }
          $setTime = strtotime(date('Y-m-d'));
          $setInsertArrayData = array('houdinv_coupons_name'=>$this->input->post('couponName'),'houdinv_coupons_tagline'=>$this->input->post('couponTagline'),'houdinv_coupons_valid_from'=>$this->input->post('couponValidFrom'),
          'houdinv_coupons_valid_to'=>$this->input->post('couponValidTo'),'houdinv_coupons_order_amount'=>$this->input->post('minOrderAmount'),'houdinv_coupons_code'=>$this->input->post('couponCode'),
          'houdinv_coupons_discount_precentage'=>$this->input->post('couponDiscount'),'houdinv_coupons_limit'=>$this->input->post('couponUsage'),'houdinv_coupons_payment_method'=>$this->input->post('couponPaymentMethod'),'houdinv_coupons_status'=>$this->input->post('couponStatus'),
          'houdinv_coupons_applicable'=>$setApplicableOn,'houdinv_coupons_created_at'=>$setTime);
          $setInsertcustomerProduct = array('userList'=>$this->input->post('couponCustomer'),'userGroup'=>$this->input->post('couponCustomerGroup'),'productList'=>$this->input->post('couponProducts'),'productCategory'=>$this->input->post('couponProductCategory'));
          $getInsertStatus  = $this->Couponsmodel->insertCouponData($setInsertArrayData,$setInsertcustomerProduct);
          if($getInsertStatus['message'] == 'yes')
          {
            $this->session->set_flashdata('success','Coupon successfully addedd');
            $this->load->helper('url');
            redirect(base_url()."Coupons", 'refresh');
          }
          else
          {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            redirect(base_url()."Coupons/add", 'refresh');
          }
        }
        else
        {
          $this->session->set_flashdata('error',validation_errors());
          $this->load->helper('url');
          redirect(base_url()."Coupons/add", 'refresh');
        }
      }
      $getBasicCoupinsInfo = $this->Couponsmodel->fetchBasicCouponsDetails();
        $this->load->view('addcoupons',$getBasicCoupinsInfo);
    }
    public function edit()
    {
      $getCouponId = $this->uri->segment('3');
      if($getCouponId == "")
      {
        $this->load->helper('url');
        redirect(base_url()."Coupons", 'refresh');
      }
      if($this->input->post('updateCouponsData'))
      {
        $this->form_validation->set_rules('couponName','Coupon Name','required|trim');
        $this->form_validation->set_rules('couponTagline','Coupon Tagline','required|trim');
        $this->form_validation->set_rules('couponValidFrom','Valid From Date','required');
        $this->form_validation->set_rules('couponValidTo','Valid Untill Date','required');
        $this->form_validation->set_rules('couponCode','Coupone Code','required|trim');
        $this->form_validation->set_rules('couponDiscount','Coupon Discount','required|trim|integer|greater_than[0]|less_than_equal_to[100]');
        $this->form_validation->set_rules('couponPaymentMethod','Coupon Payment Method','required|trim|in_list[both,cod,online payment]');
        $this->form_validation->set_rules('couponUsage','Coupon Usage','required|trim|integer');
        $this->form_validation->set_rules('couponStatus','Coupon Status','required|trim|in_list[active,deactive]');
        if($this->form_validation->run() == true)
        {
          $getCouponApplicable = $this->input->post('couponApplicableonapp');
          if($getCouponApplicable == 'on')
          {
            $setApplicableOn = 'app';
          }
          else
          {
            $setApplicableOn = 'both';
          }
          $setTime = strtotime(date('Y-m-d'));
          $setInsertArrayData = array('houdinv_coupons_name'=>$this->input->post('couponName'),'houdinv_coupons_tagline'=>$this->input->post('couponTagline'),'houdinv_coupons_valid_from'=>$this->input->post('couponValidFrom'),
          'houdinv_coupons_valid_to'=>$this->input->post('couponValidTo'),'houdinv_coupons_order_amount'=>$this->input->post('minOrderAmount'),'houdinv_coupons_code'=>$this->input->post('couponCode'),
          'houdinv_coupons_discount_precentage'=>$this->input->post('couponDiscount'),'houdinv_coupons_limit'=>$this->input->post('couponUsage'),'houdinv_coupons_payment_method'=>$this->input->post('couponPaymentMethod'),'houdinv_coupons_status'=>$this->input->post('couponStatus'),
          'houdinv_coupons_applicable'=>$setApplicableOn,'houdinv_coupons_created_at'=>$setTime);
          $setInsertcustomerProduct = array('userList'=>$this->input->post('couponCustomer'),'userGroup'=>$this->input->post('couponCustomerGroup'),'productList'=>$this->input->post('couponProducts'),'productCategory'=>$this->input->post('couponProductCategory'));
          $getUpdateStatus  = $this->Couponsmodel->updateCouponData($setInsertArrayData,$setInsertcustomerProduct,$getCouponId);
          if($getUpdateStatus['message'] == 'yes')
          {
            $this->session->set_flashdata('success','Coupon updated successfully');
            $this->load->helper('url');
            redirect(base_url()."Coupons/edit/".$getCouponId."", 'refresh');
          }
          else
          {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            redirect(base_url()."Coupons/edit/".$getCouponId."", 'refresh');
          }
        }
        else
        {
          $this->session->set_flashdata('error',validation_errors());
          $this->load->helper('url');
          redirect(base_url()."Coupons/edit/".$getCouponId."", 'refresh');
        }
      }
      $getEditListData = $this->Couponsmodel->fetchCouponsDetails($getCouponId);
      $this->load->view('editcoupons',$getEditListData);
    }
    public function view()
    {
        $this->load->view('viewcoupons');
    }

    public function viewmembership()
    {
      $GEtmember=$this->Couponsmodel->fetchmembership();
        $this->load->view('viewmembership',$GEtmember);
    }
    public function viewmembership_save()
   {
       // print_r($this->input->post());
        
        $this->form_validation->set_rules('Code','Code','required|trim');
        $this->form_validation->set_rules('DiscountPercentage','Discount Percentage','required|trim');
        $this->form_validation->set_rules('ExpiryDate','Expiry Date','required'); 

      if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="Membership";         
                $this->load->view('models_',$reg_pup);            
        }else{ 

        $customer_ids = $this->input->post('discountcustomer');
        $Code = $this->input->post('Code');
        $DiscountPercentage = $this->input->post('DiscountPercentage');
        $ExpiryDate = strtotime($this->input->post('ExpiryDate'));   
        $status = $this->input->post('status');
        $DiscountPercentage = $this->input->post('DiscountPercentage');
        
        $viewmembershipArray =array('customer_ids'=>$customer_ids,'code'=>$Code,'DiscountPercentage'=>$DiscountPercentage,'status'=>$status,'ExpiryDate'=>$ExpiryDate,'createdate'=>strtotime(date('Y-m-d')));
        $getInsertStatus = $this->Couponsmodel->viewmembership_save($viewmembershipArray);
        if($getInsertStatus)
        { 
                $this->session->set_flashdata('success','Membership addedd Successfully');               
                
        }
        else
        {
           $this->session->set_flashdata('error','Something went wrong. Please try again');             
            
        }
       
   } }


   public function viewmembership_updatestatus($value = false)
   {

    if($this->input->post('update_id')){

      $status=$this->input->post('status_changes');
           $ids=$this->input->post('update_id');

         $getInsertStatus = $this->Couponsmodel->viewmembership_update(array('id'=>$ids,'status'=>$status));
        if($getInsertStatus)
        { 
          
          $this->session->set_flashdata('success','Membership update status Successfully'); 
          redirect(base_url()."Coupons/viewmembership");              
                
        }
        else
        {
           $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Coupons/viewmembership");
        } 
      }
   }

   public function viewmembership_delete()
   {
      if($this->input->post('update_id')){

        $ids=$this->input->post('update_id');
         $getInsertStatus = $this->Couponsmodel->viewmembership_delete(array('id'=>$ids));
        if($getInsertStatus)
        {           
          $this->session->set_flashdata('success','Membership Delete Successfully'); 
          redirect(base_url()."Coupons/viewmembership");              
                
        }
        else
        {
           $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Coupons/viewmembership");
        }  
      }
     }
   



    public function generatecouponpdf()
    {
      $getCouponsData = $this->Couponsmodel->fetchCouponsPdfData();
      if($getCouponsData['totalCouponsData'])
      {
        $getTotalCoupons = $getCouponsData['totalCouponsData'];
      }
      else
      {
        $getTotalCoupons = 0;
      }
      if($getCouponsData['totalActiveCoupons'])
      {
        $getTotalActiveCoupons = $getCouponsData['totalActiveCoupons'];
      }
      else
      {
        $getTotalActiveCoupons = 0;
      }
      if($getCouponsData['totalDeactiveCoupons'])
      {
        $getTotalDeactiveCoupons = $getCouponsData['totalDeactiveCoupons'];
      }
      else
      {
        $getTotalDeactiveCoupons = 0;
      }
      $setTableContent = '';
      foreach($getCouponsData['couponsLsit'] as $couponsLsitData)
      {
        $setTableContent .= ' <tr>
        <td style="font-size:12px;font-weight:700;">'.$couponsLsitData->houdinv_coupons_name.'</td>
        <td style="font-size:12px;font-weight:700;">'.$couponsLsitData->houdinv_coupons_code.'</td>
        <td style="font-size:12px;font-weight:700;">'.$couponsLsitData->houdinv_coupons_discount_precentage.'</td>
        <td style="font-size:12px;font-weight:700;">'.$couponsLsitData->houdinv_coupons_status.'</td>
     
        </tr>';
      }
      // get dynamic logo
      $url = base_url();
      $getLogo = fetchvedorsitelogo();

      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Houdin-e');
      $pdf->SetTitle('Houdin-e - Coupons Detail');
      $pdf->SetSubject('Houdin-e - Coupons Detail');
      $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
      $pdf->setFooterData(array(0,64,0), array(0,64,128));
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
      if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
          require_once(dirname(__FILE__).'/lang/eng.php');
          $pdf->setLanguageArray($l);
      }
  $pdf->setFontSubsetting(true);
  $pdf->SetFont('dejavusans', '', 14, '', true);
  $pdf->AddPage();
  $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
  $html = <<<EOD
 <table style="width:100%;">
<tbody>
<tr>

<td>
<img style="display:block; margin:auto; width:150px; " src="$getLogo" alt="Logo"></td>



<td style="font-size:15px;text-align:right"><h3>Coupon</h3></td>

</tr>
</tbody>
</table>

<table>
<tr><td>&nbsp;</td></tr>
</table>

<table style="width:100%;" cellpadding="8" border="1">
<tr>
  <td style="text-align:center;">
  <img src=$url"assets/images/Total_Products.png" width="35px;"><br/>
  <span style="font-size:12px;font-weight:bold;">$getTotalCoupons</span><br/>
  <span style="font-size:12px;font-weight:bold; color:#008cb6">Total Customer</span>

   </td>
   <td style="text-align:center;">
   <img src=$url"assets/images/Total_Products.png" width="35px;"><br/>
   <span style="font-size:12px;font-weight:bold;">$getTotalActiveCoupons</span><br/>
   <span style="font-size:12px; font-weight:bold; color:#008cb6">Total Active Customer</span>

    </td>
    <td style="text-align:center;">
    <img src=$url"assets/images/Total_Products.png" width="35px;"><br/>
    <span style="font-size:12px;font-weight:bold;">$getTotalDeactiveCoupons</span><br/>
    <span style="font-size:12px; font-weight:bold; color:#008cb6">Total Deactive Customer</span>

     </td>
     </tr>

   </table>


   <table>
   <tr><td>&nbsp;</td></tr>
   </table>
   <table border="1" cellpadding="10">
   <tr>
   <td style="font-size:12px;font-weight:700;" >Coupon Name</td>
     <td style="font-size:12px;font-weight:700;" >Coupon Code </td>
         <td style="font-size:12px;font-weight:700;">Discount</td>
         <td style="font-size:12px;font-weight:700;">Status</td>

   </tr>
  $setTableContent
   </table>
   <table>
   <tr><td>&nbsp;</td></tr>
   </table>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_coupons_list.pdf', 'I');
    }
}
