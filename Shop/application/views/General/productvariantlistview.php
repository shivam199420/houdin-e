<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');

$price_all = json_decode($productData['0']->houdin_products_price,true);
if($price_all['discount'] != 0 && $price_all['discount'] != "")
{
    $setDiscountedPrice = $variantData[0]->houdin_products_variants_prices-$price_all['discount'];
    $setOriginalPrice = $variantData[0]->houdin_products_variants_prices;
}
else
{
    $setDiscountedPrice = 0;
    $setOriginalPrice = $variantData[0]->houdin_products_variants_prices;
}
$overall_rating = $reviews_sum->houdinv_product_review_rating;
$total_rating = count($reviews);
$final_rating = $overall_rating/$total_rating;
$getImageData = json_decode($productData[0]->houdinv_products_main_images,true);
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<style>
    .checked {
    color: orange;
}

</style>
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">View Product</h2>

            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- single product area start -->
    <div class="single-product-area ptb-100">
        <div class="container">
            <div class="row">
            <?php
            if($this->session->flashdata('error'))
            {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
            }
            if($this->session->flashdata('success'))
            {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
            }
            ?>
                <div class="col-md-5">
                    <div class="product-details-tab">
                        <!-- Tab panes -->
                        <div class="tab-content">
                        <div class="tab-pane active" id="product1">
                                <div class="large-img">
                                    <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $variantData[0]->houdin_products_variants_image;?>" alt="" />
                                </div>
        </div>
                            <?php
                            $i = 2;
                            for($index = 0; $index < count($getImageData); $index++)
                            {
                                if($getImageData[$index])
                                {
                            ?>
                            <div class="tab-pane" id="product<?php echo $i; ?>">
                                <div class="large-img">
                                    <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $getImageData[$index]; ?>" alt="" />
                                </div>
                            </div>
                            <?php $i++; } }
                            ?>
                        </div>
                        <!-- Nav tabs -->
                        <div class="details-tab owl-carousel">
                        <div class="active">
                            <a href="#product1" data-toggle="tab">
                                <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $variantData[0]->houdin_products_variants_image;?>>" alt="" /></a></div>
                            <?php
                            $j = 2;
                            for($indexthumb = 0; $indexthumb < count($getImageData); $indexthumb++)
                            {
                                if($getImageData[$indexthumb])
                                {
                            ?>
                            <div><a href="#product<?php echo $j; ?>" data-toggle="tab"><img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $getImageData[$indexthumb]; ?>" alt="" /></a></div>
                            <?php $j++; } } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="single-product-content">
                        <div class="single-product-dec pb-30  for-pro-border">
                            <h2><?php echo $variantData[0]->houdin_products_variants_title; ?></h2>
                            <span class="ratting">
                            <?php
                            for($i=1 ; $i<6 ;$i++)
                            {
                                if($i <= $final_rating)
                                {
                                    $posi = strpos($final_rating,".");
                                    if($posi>=0)
                                    {
                                        $exp =  explode(".",$final_rating);
                                        if($exp[0]==$i)
                                        {
                            ?>
                            <i class="fa fa-star-half-o"></i>
                            <?php
                            }
                            else
                            {
                                echo '<i class="fa fa-star"></i>';
                            }
                            }
                            else
                            {
                            ?>
                            <i class="fa fa-star"></i>
                            <?php
                            }
                        }
                        else
                        { ?>
                        <i class="fa fa-star-o"></i>  <?php } } echo "( ".$total_rating." )"; ?>

                                </span>
                                <?php
                                if($setDiscountedPrice != 0)
                                {
                                ?>
                                <h3><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $variantData[0]->houdinv_products_variants_final_price; ?>&nbsp;&nbsp;<strike><span><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $price_all['sale_price]; ?></span></strike></h3>
                                <?php }
                                else
                                {
                                ?>
                                <h3><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $setOriginalPrice; ?></h3>
                                <?php }
                                ?>

                            <p><?php echo $productData[0]->houdin_products_short_desc; ?></p>
                        </div>
                        <div class="single-cart-color for-pro-border">
                        <?php
                        if($variantData[0]->houdinv_products_variants_total_stocks > 0)
                        {
                            $settext = 'In Stock';
                        }
                        else
                        {
                            $settext = 'Out Of Stock';
                        }
                        ?>
                            <p>availability : <span><?php echo $settext; ?></span></p>



                            <div class="pro-quality pd_qntty_area">
                                <p>Quantity:</p>
                                <div class="pd_qty">
                                <input value="1" min="1" data-max="<?php echo $variantData[0]->houdinv_products_variants_total_stocks ?>" name="qttybutton" class="cart-plus-minus-box setCartQunatity" type="number">
                                </div>

                            </div>
                            <div class="single-pro-cart">
                                <a href="javascript:;" class="Add_to_cart_button" data-variant="<?php echo $variantData[0]->houdin_products_variants_id ?>"  data-cart="0" title="Add to Cart">
                                        <i class="pe-7s-cart"></i>
                                        add to cart
                                    </a>
                                <a href="javascript:;" class="Add_to_whishlist_button" data-variant="<?php echo $variantData[0]->houdin_products_variants_id ?>"  data-cart="0" title="wishlist">
                                        <i class="pe-7s-like"></i>
                                        wishlist
                                    </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- single product area end -->
    <!-- single product area end -->
    <div class="single-product-dec-area">
        <div class="container">
            <div class="single-product-dec-tab">
                <ul role="tablist">
                    <li class="active">
                        <a href="#description" data-toggle="tab">
                            description
                        </a>
                    </li>
                    <li>
                        <a href="#reviews" data-toggle="tab">
                            reviews
                        </a>
                    </li>
                </ul>
            </div>
            <div class="single-product-dec pb-100">
                <div class="tab-content">
                    <div class="tab-pane active" id="description">
                    <?php echo $productData[0]->houdin_products_desc; ?>
                    </div>
                    <div class="tab-pane" id="reviews">
                        <div class="customer-reviews-all">
                            <div class="row">
                                <div class="col-md-6">
                                <?php
                                if(!$final_rating > 0)
                                {
                                ?>
                                <p>There are no reviews yet.</p>
                                    <p class="text-bigger">Be the first to review</p>
                                <?php }
                                else
                                {
                                    foreach($reviews as $user_Review)
                                      {
                                ?>
                                 <div style="margin-bottom:10px">
                                          <div class=""><?php echo $user_Review->houdinv_product_review_user_name ?></div>


                                          <div class="strat-rating">
                                          <?php for($i=1 ; $i<6 ;$i++)
                                          {
                                          if($i <= $user_Review->houdinv_product_review_rating)
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star checked"></span>
                                          <?php }
                                          else
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star"></span>
                                          <?php } } ?>

                                            <span class="text-date"><?php echo $user_Review->houdinv_product_review_created_at ?></span>
                                          </div>


                                          <div class=""><?php echo  $user_Review->houdinv_product_review_message; ?></div>
                                        </div>

                                <?php } }
                                ?>


                                </div>
                                <div class="col-md-6">
                                    <div class="form-add table-responsive">
                                    <?php echo form_open(base_url( 'Productlistview' ), array( 'id' => 'rtng_form',"class"=>"checkout_form", 'method'=>'post' ));?>
                                        <div class="form-group">

                                        <div class="strat-rating">
                                            <span data-rating="1" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="2" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="3" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="4" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="5" style="font-size: 22px;" class="fa fa-star"></span>
                                            <input type="hidden"  name="rating" class="rating_data" value="0" />
                                            <input type="hidden"  name="product_id" class="product" value="0" />
                                            <input type="hidden"  name="variant_id" class="" value="<?php echo $this->uri->segment('3'); ?>" />
                                        </div>
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation form-control" type="text" name="user_name" placeholder="Your Name" />
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation email_validation form-control" type="text" name="user_email" placeholder="Your Email" />
                                        </div>
                                        <div class="form-group">

                                        <textarea  class="required_validation_for_review form-control" name="user_message" rows="3" placeholder="Write a review"></textarea>
                                        </div>
                                        <input class="btn-round submit_form_review" type="submit" name="review_submit" value="Add Review" />
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>

   <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('keyup','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        })
        $(document).on('change','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        })
    })
    </script>
