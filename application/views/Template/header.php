<?php $checkFront=false; if($this->router->fetch_method()=='index'){$checkFront=true;}?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">

    
    <title>Welcome to Houdin-e</title>

    <!-- Styles -->
    
    <link href="<?php echo base_url(); ?>assets/css/page.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/aos.css" rel="stylesheet">
	 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/intlTelInput.css">
   <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" /> 
  
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/img/apple-touch-icon.png">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png">
	  <style>
		.custom_effect:hover{
          background-color: #fff;
    min-height: 59px!important;
    
    border-bottom: 3px solid #6e3a51;
    border-radius: 0px 0px 0px 0px;
    width: auto;
    }
    .custom_effect{
    background-color: #fff;
    min-height: 59px!important;
    box-shadow:transparent;
    border-bottom:transparent;
    border-radius: 0px 0px 0px 0px;
    width: auto;
    }
    .nav-item:active{
         color:green!important;
    }
    .w-200 {
    width: 270px !important;
}
 
.custom-animation-title{
  font-size: 41px;
  text-align: center;
  color:#0c2a59 !important;
}
.m-b-5{
  margin-bottom: 5px;
}
.theme-btn{
    background-color:#5079ad !important;
}
@media(max-width:767px){
  .custom-animation-title {
    font-size: 39px;
    text-align: center;
    line-height: 39px;
}
.custom-left-title {
    margin-top: 0%!important;
    font-size: 36px!important;
}
.section-header {
    text-align: center; 
    margin: 0 auto 0px;
}
}
	  </style> 
  </head>

  <body>


    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg <?php if($checkFront){?>navbar-light <?php }?> navbar-stick-dark navbar-top" data-navbar="sticky">
      <div class="container">

        <div class="navbar-left">
          <button class="navbar-toggler" type="button">&#9776;</button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>" style="width:150px;">
            <img class="logo-dark" src="<?php echo base_url(); ?>assets/img/Logo_B.png" alt="logo">
           <?php if($checkFront){?> <img class="logo-light" src="<?php echo base_url(); ?>assets/img/Logo_B.png" alt="logo"><?php }?>
          </a>
        </div>

        <section class="navbar-mobile">         

          <ul class="nav nav-navbar">
            <li class="nav-item <?php if($this->router->fetch_method()=='index'){echo "active";} ?>" ><a class="nav-link custom_effect" href="<?php echo base_url(); ?>">Home</a> </li>

            <!-- <li class="nav-item <?php //if($this->router->fetch_method()=='about'){echo "active";} ?>"><a class="nav-link custom_effect" href="<?php echo base_url(); ?>home/about">About</a></li> -->

             <!-- <li class="nav-item <?php //if($this->router->fetch_method()=='themes'){echo "active";} ?>"><a class="nav-link custom_effect" href="<?php echo base_url(); ?>home/themes">Themes</a></li> -->

            <li class="nav-item <?php if($this->router->fetch_method()=='plans'){echo "active";} ?>">
              <a class="nav-link custom_effect" href="<?php echo base_url(); ?>home/plans">Plans</a>              
            </li>
			  <li class="nav-item <?php if($this->router->fetch_method()=='features'){echo "active";} ?>"><a class="nav-link custom_effect" href="<?php echo base_url(); ?>home/features">Features</a></li>
  

            <li class="nav-item nav-mega <?php if($this->router->fetch_method()=='contact'){echo "active";} ?>">
              <a class="nav-link custom_effect " href="<?php echo base_url(); ?>home/contact">Contact</a></li>

          </ul>
        </section>
        <?php if($_SERVER[REQUEST_URI] != '/Register/activate' && $_SERVER[REQUEST_URI] != '/Register/complete') 
        {
        ?>
        <a href="javascript:;" class="btn btn-sm  btn_theme_dark_blue btn-other-nav btn-login-nav hidden-xs" data-toggle="modal" data-target="#user-login"><i class="fa fa-sign-in"></i> Login </a>
        <a href="javascript:;" class="btn btn-sm  btn_theme_light btn-other-nav hidden-xs" data-toggle="modal" data-target="#user-register"><i class="fa fa-user"></i> Register </a>
        <?php }
        ?>

      </div>
    </nav><!-- /.navbar -->


    <!-- Header -->
	  
	  <?php if($checkFront){?>
   <header class="header text-white dark-overlay h-fullscreen overflow-hidden home-main-banner" style="background-image:url('<?php echo base_url(); ?>assets/images/Accounting.jpg');" >
   
     <div class="container position-static">
     <div class="row align-items-center h-100">

     <div class="col-lg-12 col-lg-offset-4 col-md-12 col-md-offset-4" style="text-align: center;">
        <h1 class="home-banner-heading">Everything you need to manage and grow your brick and mortar</h1>
        <!-- <span class="fw-400 pl-2" data-typing="your brick, your brick, your mortar, your mortar" data-type-speed="70"></span> -->
       <a class="btn_theme_bluelight btn-round  px-6 m-b-5 fw-700 btn-round2" data-toggle="modal" data-target="#user-register" href="javascript:;">TRY FOR FREE</a>
         <a class="btn_theme_bluelight btn-round  px-6 m-b-5 fw-700 btn-round2" href="<?php echo base_url(); ?>home/plans">EXPLORE PACKAGES</a>
     </div>
   </div>
    </div>
   </header>
	<?php }?>

    <!-- Main Content -->
    <main class="main-content">
		
		
 <!-- user Login modal	 -->
    <div class="modal fade" id="user-login" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <form id="loginForm" method="post">
              <h4 class="fw-200 text-center">Login</h4>
              <p class="text-center">Login into your account using your credentials.</p>
              <hr class="w-10">
              <div class="form-group showLoginError" style="display:none"></div>
              <div class="form-group">
              <input type="hidden" class="setToken" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                <input class="form-control required_validation_for_login_form name_validation email_validation" type="text" name="username" placeholder="Your Email Address">
              </div>

              <div class="form-group">
                <input class="form-control required_validation_for_login_form name_validation" type="password" name="password" placeholder="Your Password">
              </div>

              <div class="row align-items-center pt-3 pb-5">
                <!-- <div class="col-auto">
                  <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox">
                    <label class="custom-control-label">Remember me</label>
                  </div>
                </div> -->

                <div class="col text-right">
                  <p class="mb-0 fw-300"><a class="text-muted small-2" href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#user-forgot-password">Forgot password?</a></p>
                </div>
              </div>
              
              <button class="btn btn-lg btn-block btn-primary setLoginSubmit" type="submit">Login</button> 

              <p class="small-2 text-center mt-5 mb-0">Don't have an account? <a href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#user-register">Create one</a></p>

            </form>
          </div>

        </div>
      </div>
    </div>

		
		 <!-- Register modal -->
    <div class="modal fade" id="user-register" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <?php //echo form_open(array( 'id' => 'regitserFirstForm' ));?>
             <form id="regitserFirstForm" method="post">
              <h4 class="text-center">Sign up</h4> 
              <hr class="w-10">
              <input type="hidden" class="setToken" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
              <div class="form-group showCheckboxError" style="display:none">
                <div class="alert alert-danger">Please accept our terms &amp; condition</div>
              </div>   
              <!-- Form error data -->
              <div class="formErrorData" style="display:none"></div>
              <div class="form-group">
                <input class="form-control required_validation_for_register_first_form name_validation shopUserName" name="userName" type="text" placeholder="Your Name">
              </div>

              <div class="form-group">
                <input class="form-control Internationphonecode required_validation_for_register_first_form name_validation shopUserPhone" placeholder="Your Phone Number" name="userPhone" type="text"/>
              </div>
				
			<div class="form-group">	
				<div class="custom-control custom-checkbox">
                    <input class="custom-control-input checkTermCheckbox" type="checkbox">
                    <label class="custom-control-label">I agree with our  <a href="<?php echo base_url(); ?>home/terms" target="_blank">Term of Service </a> &nbsp; & &nbsp;  <a href="<?php echo base_url(); ?>home/privacy_policy" target="_blank">Privacy Policy</a></label>
                </div>
				</div>
               
              <input type="submit" class="btn btn-lg btn-block btn-primary setSignupData" name="firstRegistration" value="Sign up"/>
              <!-- <button class="btn btn-lg btn-block btn-primary" type="submit"></button> -->
 
			  <p class="small-2 text-center mt-5 mb-0">Already have an account? <a href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#user-login">Login</a></p>
    </form>
            <?php //echo form_close(); ?>
          </div>

        </div>
      </div>
    </div>
		
		
		 <!-- user forgot -->
    <div class="modal fade" id="user-forgot-password" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">

          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
           
            <form id="forgetFirstForm" method="post">
            <input type="hidden" class="setToken" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
              <h4 class="mb-5">Password Reset</h4>
              <p class="text-center">Email to reset your password</p>
              <div class="formErrorData" style="display:none"></div>
              <div class="form-group">
                <input class="form-control " type="email" name="email" placeholder="Your Email Address">
              <p class="messagesd"></p>
              </div>
             <!-- <button class="btn btn-lg btn-block btn-primary" name="userForgetPasswordBtn" value="yes" type="submit">Submit</button>-->
             <input type="submit" class="btn btn-lg btn-block btn-primary" name="userForgetPasswordBtn" value="submit"/> 
          </form>
          </div>

        </div>
      </div>
    </div>
<script type="text/javascript">
  (function() {
    var nav = document.getElementById('nav'),
        anchor = nav.getElementsByTagName('a'),
        current = window.location.pathname.split('/')[1];
        for (var i = 0; i < anchor.length; i++) {
        if(anchor[i].href == current) {
            anchor[i].className = "active";
        }
    }
})();
</script>