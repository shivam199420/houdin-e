<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <h4 class="page-title">Query</h4>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-sm-12">
                        <?php
                        if($this->session->flashdata('success'))
                        {
                            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                        }
                        if($this->session->flashdata('error'))
                        {
                            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                        }
                        ?>
                        </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="card-box table-responsive">
                              <table class="table table-striped table-bordered table_shop_custom">
                                <thead>
                                  <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Query</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <?php 
                                foreach($queryList as $queryListData)
                                {
                                ?>
                                <tr>
                                 <td><?php echo $queryListData->houdin_admin_contact_name ?></td>
                                 <td><?php echo $queryListData->houdin_admin_contact_email ?></td>
                                 <td><?php echo $queryListData->houdin_admin_contact_phone ?></td>
                                 <td><?php echo $queryListData->houdin_admin_contact_message ?></td>
                                 <td>
                                 <button type="bnutton" data-email="<?php echo $queryListData->houdin_admin_contact_email ?>" class="btn btn-primary btn-md sendEmaildata">Send Email</button>
                                 </td>
                                 </tr>
                                <?php }
                                ?>
                                </tbody>
                              </table>
                              <ul class="pagination pull-right"> <?php echo $this->pagination->create_links(); ?> </ul>
                            </div>
                          </div>
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->
 
<div id="sendEmailModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <?php echo form_open(base_url( 'Contactform' ),array( 'id' => 'sendemailform','class'=>'m-t-30','method'=>'post'));?>
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                  <h4 class="modal-title">Send Email</h4>
          </div> 
          <div class="modal-body">
              <div class="row">
                  <div class="col-md-12">
                     <div class="form-group no-margin">
                     <input type="hidden" class="userEmail" name="userEmail"/>
                        <label for="field-7" class="control-label">Subject</label>
                       <input type="text" class="form-control required_validation_for_contact_email name_validation" name="emailsubject" placeholder="Email Subject"/>
                      </div>
                    </div>
            </div>
            <div class="row">
                  <div class="col-md-12">
                     <div class="form-group no-margin">
                        <label for="field-7" class="control-label">Message</label>
                       <textarea class="form-control required_validation_for_contact_email name_validation" name="emailmessage"></textarea>
                      </div>
                    </div>
            </div>
         </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-info" name="sendemailbutton" value="Submit">
        </div>
    </div>
  </form>
 </div>
</div>
<?php $this->load->view('Template/footer.php') ?>
 <script type="text/javascript">
 $(document).ready(function(){
     $(document).on('click','.sendEmaildata',function(){
         $('.userEmail').val($(this).attr('data-email'));
         $('#sendEmailModal').show();
     })
 })
 </script>
 <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#sendemailform',function(){
			var check_required_field=0;
			$(".required_validation_for_contact_email").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field++;
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field != 0)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>