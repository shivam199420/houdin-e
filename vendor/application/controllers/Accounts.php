<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Accounts extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->auth_users->is_logged_in();
        $this->perPage = 50;
        $this->load->library('Pdf');
        $this->load->model('Accountsmodel'); 
        $this->load->model('AccountsNewmodel'); 

        $this->load->model('Customermodel');
        $this->load->model('Staffmodel'); 
        $this->load->model('Suppliermodel');
               $this->Validconfigs = 
       array(
            array(
                    'field' => 'account_type',
                    'label' => 'account type',
                    'rules' => 'integer'
                  )
                  ,
                  array(
                    'field' => 'detail_Type',
                    'label' => 'Detail type',
                    'rules' => 'integer'
                  ),
                   
                  array(
                    'field' => 'Depreciation',
                    'label' => 'Depreciation',
                    'rules' => 'numeric'
                  )
                  ,
                  array(
                    'field' => 'balance',
                    'label' => 'balance',
                    'rules' => 'numeric'
                  )
                  ,
            array(
                    'field' => 'Name',
                    'label' => 'Name',
                    'rules' => 'required'
                  )      
    
  );
    }
    public function index()
    { 
        $getAccountsData = $this->Accountsmodel->fetchAccountsData();
        $this->load->view('accountdata',$getAccountsData);
    }
    public function reports()
    {
        $this->load->view('accountreport');
    }
    public function accounting()
    {
        if($this->input->post('DeleteAccountEntry'))
        {
            $this->form_validation->set_rules('delete_id','text','required|integer');
            if($this->form_validation->run() == true)
            {
                $getDelete = $this->Accountsmodel->deleteMainAccounts($this->input->post('delete_id'));
                if($getDelete)
                {
                    $this->session->set_flashdata('success','Account deleted successfully');
                    redirect(base_url().'Accounts/accounting', 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Accounts/accounting', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url().'Accounts/accounting', 'refresh');  
            }
        }
        // Custom Pagination
        $All= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->Accountsmodel->getAccounts($conditions);
        //pagination config
        $config['base_url']    = base_url().'Accounts/accounting';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset; 
        $conditions['limit'] = $this->perPage;
        $data['all'] =  $this->Accountsmodel->getAccounts($conditions);
        $data['accountholder'] = $this->Accountsmodel->getCustomerAndSuplier();
        $data['parentAccount'] = $this->Accountsmodel->getParentAccount($this->uri->segment('3'));
        $this->load->view('accountingdata',$data);
    }
    
    
     public function journalentry_account()
    {
        $data['parentAccount'] = $this->Accountsmodel->getParentAccount();
       // $data['endingBalance'] = $this->Accountsmodel->getEndingBalanceData($this->uri->segment('3'));
        //$data['accountname'] = $this->Accountsmodel->getAccountNameData($this->uri->segment('3'));
        $this->load->view('journalentry_account',$data);
    }
    

    public function balancetransfer()
    { 
       // $data['accountholder'] = $this->Accountsmodel->getCustomerAndSuplier();
        $data['parentAccount'] = $this->Accountsmodel->getParentAccount();
       // $data['endingBalance'] = $this->Accountsmodel->getEndingBalanceData($this->uri->segment('3'));
        //$data['accountname'] = $this->Accountsmodel->getAccountNameData($this->uri->segment('3'));
        $this->load->view('balancetransfer',$data);
    }

    public function balancetransfers()
    { 
       // $data['accountholder'] = $this->Accountsmodel->getCustomerAndSuplier();
        $data['parentAccount'] = $this->Accountsmodel->getParentAccount();
       // $data['endingBalance'] = $this->Accountsmodel->getEndingBalanceData($this->uri->segment('3'));
        //$data['accountname'] = $this->Accountsmodel->getAccountNameData($this->uri->segment('3'));
        $this->load->view('balancetransfers',$data);
    }
    
    public function GetblanceShowtheblancetrafer()
    {
       
        if($this->input->post('textbox')){
     
     $data = $this->Accountsmodel->GetblanceShowtheblancetrafer($this->input->post('textbox'));
  
  print($data->houdinv_accounts_final_balance); 
  
    // $data['accountholder'] = $this->Accountsmodel->getCustomerAndSuplier();
   //     $data['parentAccount'] = $this->Accountsmodel->getParentAccount();
     }else{
           redirect(base_url()."accounts/balancetransfer","refresh");
     }
    }
    
    public function balancetransfersave()
    {
        $this->form_validation->set_rules('transferfrom','Transfer from','required');
        $this->form_validation->set_rules('transferto','Transfer to','required');
        $this->form_validation->set_rules('transferamount','Transfer Amount','required');
        $this->form_validation->set_rules('transferdate','Transfer Date','required');         

        if($this->form_validation->run() == true)
        {
            $transferamount = $this->input->post('transferamount');


            $setCreatedDate = strtotime(date('Y-m-d'));
            $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$this->input->post('transferfrom'), 
            'houdinv_accounts_balance_sheet_payee_type'=>'',
            'houdinv_accounts_balance_sheet_account'=>$this->input->post('transferto'),
             
            'houdinv_accounts_balance_sheet_created_at'=>$setCreatedDate,
            'houdinv_accounts_balance_sheet_memo'=>$this->input->post('transfermemo'),
            'houdinv_accounts_balance_sheet_decrease'=>0,
            'houdinv_accounts_balance_sheet__increase'=>$transferamount,
            'houdinv_accounts_balance_sheet_final_balance'=>'',
            'houdinv_accounts_balance_sheet_ref_type' => $this->input->post('refnumber'),
            'houdinv_accounts_balance_sheet_date'=>$this->input->post('transferdate'),
            );
            $getData = $this->Accountsmodel->balancetransfer($setInsertArray,$this->input->post('transferfrom'),$this->input->post('transferto'));
            
            $this->session->set_flashdata('success','Amount Transfer successfully');
           
            redirect(base_url()."Accounts/accounthistory/".$this->input->post('transferfrom'),"refresh");
        }else{
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."accounts/balancetransfer","refresh");
        }
    }
    
    


    public function balancetransferssave()
    {
        $this->form_validation->set_rules('transferfrom','Transfer from','required');
        $this->form_validation->set_rules('transferto','Transfer to','required');
        $this->form_validation->set_rules('transferamount','Transfer Amount','required');
        $this->form_validation->set_rules('transferdate','Transfer Date','required');         

        if($this->form_validation->run() == true)
        {
            $transferamount = $this->input->post('transferamount');


            $setCreatedDate = strtotime(date('Y-m-d'));
            $setInsertArray_From = array('houdinv_accounts_balance_sheet_account_id'=>$this->input->post('transferfrom'), 
            'houdinv_accounts_balance_sheet_payee_type'=>'',
            'houdinv_accounts_balance_sheet_account'=>$this->input->post('transferto'),
             
            'houdinv_accounts_balance_sheet_created_at'=>$setCreatedDate,
            'houdinv_accounts_balance_sheet_memo'=>$this->input->post('transfermemo'),
            'houdinv_accounts_balance_sheet_decrease'=>0,
            'houdinv_accounts_balance_sheet__increase'=>$transferamount,
            'houdinv_accounts_balance_sheet_final_balance'=>'',
            'houdinv_accounts_balance_sheet_ref_type' => $this->input->post('refnumber'),
            'houdinv_accounts_balance_sheet_date'=>$this->input->post('transferdate'),
            );


            $setInsertArray_To = array('houdinv_accounts_balance_sheet_account_id'=>$this->input->post('transferto'), 
            'houdinv_accounts_balance_sheet_payee_type'=>'',
            'houdinv_accounts_balance_sheet_account'=>$this->input->post('transferfrom'),
             
            'houdinv_accounts_balance_sheet_created_at'=>$setCreatedDate,
            'houdinv_accounts_balance_sheet_memo'=>$this->input->post('transfermemo'),
            'houdinv_accounts_balance_sheet_decrease'=>$transferamount,
            'houdinv_accounts_balance_sheet__increase'=>0,
            'houdinv_accounts_balance_sheet_final_balance'=>'',
            'houdinv_accounts_balance_sheet_ref_type' => $this->input->post('refnumber'),
            'houdinv_accounts_balance_sheet_date'=>$this->input->post('transferdate'),
            );
            $getData = $this->AccountsNewmodel->balancetransfers($setInsertArray_From,$setInsertArray_To);
            
            $this->session->set_flashdata('success','Amount Transfer successfully');
           
            redirect(base_url()."Accounts/accounthistorydetails/".$this->input->post('transferfrom'),"refresh");
        }else{
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."accounts/balancetransfers","refresh");
        }
    }












    
    public function journalentrysave()
    {
          
         
        
            
           $transferfromto= $this->input->post('transferfromto');
           $transferDebits=$this->input->post('transferDebits');
           $transferCredits=$this->input->post('transferCredits');
           $transfermemo=$this->input->post('transfermemo');
           $name=$this->input->post('name');
            $tax=$this->input->post('tax');
            
            
            $getAccountId=  $transferfromto[0];
            
          for ($x = 0; $x <count($transferfromto); $x++) {
              $transferfromto_ID= $transferfromto[$x];
              if(!empty($transferfromto_ID)) {
           
              
              $transferDebits_val=$transferDebits[$x];
              
               
              $transferCredits_val=$transferCredits[$x];
              $transfermemo_val=$transfermemo[$x];
               $name_val=$name[$x];
                $tax_val=$tax[$x];
                 
              
              
           
            
            
            $setCreatedDate = strtotime(date('Y-m-d'));
            $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$transferfromto_ID,
            'houdinv_accounts_balance_sheet_reference'=>$this->input->post('refnumber'),
                  'houdinv_accounts_balance_sheet_payee_type'=>'',
            'houdinv_accounts_balance_sheet_account'=>0,
             
            'houdinv_accounts_balance_sheet_created_at'=>$setCreatedDate,
            'houdinv_accounts_balance_sheet_memo'=>$transfermemo_val,
            'houdinv_accounts_balance_sheet_decrease'=>$transferDebits_val,
            'houdinv_accounts_balance_sheet__increase'=>$transferCredits_val,
            'houdinv_accounts_balance_sheet_final_balance'=>'',
            'houdinv_accounts_balance_sheet_ref_type' =>'journal Entry',
            'houdinv_accounts_balance_sheet_date'=>$this->input->post('transferdate'),
            );
           $getData = $this->AccountsNewmodel->balancetransfers($setInsertArray);
        } }
            
           $this->session->set_flashdata('success','journal entry transfer successfully');
           
            redirect(base_url()."Accounts/accounthistorydetails/".$getAccountId,"refresh");  
  
        
    }
    
    
    
    
    public function addaccounting()
    {
        if($this->input->post('save'))
        {
            $this->form_validation->set_rules($this->Validconfigs);
            if($this->form_validation->run() != true)
            {
                $this->session->set_flashdata('error',validation_errors() );
                redirect(base_url().'Accounts/addaccounting', 'refresh');      
            }

            $array2 = array();
            //if($this->input->post('balance'))
            //{
                if($this->input->post('account_type') == 2 || $this->input->post('account_type') == 4 || $this->input->post('account_type') == 5 || $this->input->post('account_type') == 1 
                || $this->input->post('account_type') == 3 || $this->input->post('account_type') == 13 || $this->input->post('account_type') == 14 || $this->input->post('account_type') == 15)
                {
                    $decrease =  $this->input->post('balance'); 
                    $increase =  0; 
                    $setMoney = $this->input->post('balance');
                }   
                else
                {
                    if($this->input->post('balance')<0)
                    {
                        $decrease =  $this->input->post('balance'); 
                        $increase =  0; 
                        $setMoney = "-".$this->input->post('balance');
                    }
                    else
                    {
                        $decrease =  0; 
                        $increase =  $this->input->post('balance');  
                        $setMoney = $this->input->post('balance');
                    }
                }                
            
                   if($this->input->post('account_type')!=3)
                   {
                      $array2[] = array("houdinv_accounts_balance_sheet_ref_type"=>'journal Entry',
                           "houdinv_accounts_balance_sheet_memo"=>'Opening Balance',
                           "houdinv_accounts_balance_sheet_decrease"=>$decrease,
                           "houdinv_accounts_balance_sheet__increase"=>$increase,
                          "houdinv_accounts_balance_sheet_transaction_added_status"=>'R',
                           "houdinv_accounts_balance_sheet_tax"=>'',
                           'houdinv_accounts_balance_sheet_decrease'=>$decrease,
                           'houdinv_accounts_balance_sheet__increase'=>$increase,
                           "houdinv_accounts_balance_sheet_date"=>$this->input->post('as_of_one'),
                            "houdinv_accounts_balance_sheet_created_at" => strtotime(date('Y-m-d'))
                        );  
                   }
                   else
                   {
                    $array2[] = array("houdinv_accounts_balance_sheet_ref_type"=>'journal Entry',
                           "houdinv_accounts_balance_sheet_memo"=>'Opening Balance',
                           "houdinv_accounts_balance_sheet_payment"=>$decrease,
                           "houdinv_accounts_balance_sheet_deposit"=>$increase,
                          "houdinv_accounts_balance_sheet_transaction_added_status"=>'R',
                          'houdinv_accounts_balance_sheet_decrease'=>$decrease,
                           'houdinv_accounts_balance_sheet__increase'=>$increase,
                           "houdinv_accounts_balance_sheet_tax"=>'',
                           "houdinv_accounts_balance_sheet_date"=>$this->input->post('as_of_one'),"houdinv_accounts_balance_sheet_created_at" => strtotime(date('Y-m-d')));  
                   }
           
           // }
            $date = strtotime(date("Y-m-d"));
            $array = array("houdinv_accounts_account_type_id"=>$this->input->post('account_type'),
                        "houdinv_accounts_detail_type_id"=>$this->input->post('detail_Type'),
                        "houdinv_accounts_name"=>$this->input->post('Name'),
                        "houdinv_accounts_description"=>$this->input->post('Description'),
                        "houdinv_accounts_default_tax_code"=>$this->input->post('my_tax'),
                        "houdinv_accounts_parent_account"=>$this->input->post('parents'),
                       "houdinv_accounts_date_add"=>$date,
                        "houdinv_accounts_date_update"=>$date); 
                        $result =   $this->Accountsmodel->SaveAccounts($array,$array2,$setMoney);
                        if($result)
                        {
                        
                            if($result=="already")
                            {
                                $this->session->set_flashdata('error',"This name already exist");
                            redirect(base_url().'Accounts/addaccounting', 'refresh'); 
                            }
                            else
                            {
                                $this->session->set_flashdata('success',"Data saved successfull");
                            redirect(base_url().'Accounts/accounting', 'refresh'); 
                            }
                        }
                        else
                        {
                        
                            $this->session->set_flashdata('error',"Something went wrong ! Try again");
                            redirect(base_url().'Accounts/addaccounting', 'refresh'); 
                        }
                            
                        }
       $data['parent'] = $this->Accountsmodel->getAccounts();      
      $data['type'] = $this->Accountsmodel->getAccountsType();
      $data['parentaccount'] = $this->Accountsmodel->getParentAccount();
        $this->load->view('addaccountingdata',$data);
    }
    // add journal  entry for account
    public function AddjournalEntry()
    {
        $lastEntry = getMainAccount($this->uri->segment('3'));
        $this->form_validation->set_rules('journaldate','date','required');
        $this->form_validation->set_rules('accoutnid','account id','required');
        if($this->form_validation->run() == true)
        {
            if($this->input->post('setDecrease'))
            {
                $setDecrease = $this->input->post('setDecrease');
                $setIncrease = 0;
            }
            else
            {
                $setDecrease = 0;
                $setIncrease = $this->input->post('setIncreaseData');
            }
            $setCreatedDate = strtotime(date('Y-m-d'));
            $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$this->input->post('getAccountId'),
            'houdinv_accounts_balance_sheet_reference'=>$this->input->post('getRefNumber'),
            'houdinv_accounts_balance_sheet_ref_type'=>$this->input->post('entrytype'),
            'houdinv_accounts_balance_sheet_pay_account'=>$this->input->post('setPayeeData'),
            'houdinv_accounts_balance_sheet_payee_type'=>$this->input->post('payeetype'),
            'houdinv_accounts_balance_sheet_account'=>$this->input->post('selectAccount'),
            'houdinv_accounts_balance_sheet_concile'=>$this->input->post('concileStatus'),
            'houdinv_accounts_balance_sheet_created_at'=>$setCreatedDate,
            'houdinv_accounts_balance_sheet_memo'=>$this->input->post('getMemoData'),
            'houdinv_accounts_balance_sheet_decrease'=>$setDecrease,
            'houdinv_accounts_balance_sheet__increase'=>$setIncrease,
            'houdinv_accounts_balance_sheet_final_balance'=>$this->input->post(''),
            'houdinv_accounts_balance_sheet_date'=>$this->input->post('getdate'),
            );
            $getData = $this->Accountsmodel->updateAccountData($setInsertArray,$this->input->post('getAccountId'),$this->input->post('parentaccount'));
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."accounts/accounting","refresh");
        }
         
    }
    public function accounttypedetailfetch()
    {
       $data = $_REQUEST['type'];
      
       if($data)
       {
        
       $result =  $this->Accountsmodel->getAccountsTypeDetail($data);
       } 
       echo json_encode($result);
    }
    
    
       public function accounttypedetailfullfetch()
    {
       $data = $_REQUEST['type'];
      
       if($data)
       {
        
       $result =  $this->Accountsmodel->getAccountsTypeDetailFull($data);
       } 
       echo json_encode($result);
    }
    
       public function accountTypeMatch()
    {
       $data = $_REQUEST['type'];
       $id = $_REQUEST['id'];
      
       if($data)
       {
        
       $result =  $this->Accountsmodel->getaccountTypeMatch($data,$id);
       if($result)
       {
        echo "yes";
       }
       } 
       
    }
    
    public function accuntingUpdate()
    {
        $array2 = array("houdinv_accounts_balance_sheet_reference"=>$_REQUEST['ref_name'],
        "houdinv_accounts_balance_sheet_pay_account"=>$_REQUEST['pay_account'],
                           "houdinv_accounts_balance_sheet_memo"=>$_REQUEST['memo'],
                           "houdinv_accounts_balance_sheet_decrease"=>$_REQUEST['decrease'],
                           "houdinv_accounts_balance_sheet__increase"=>$_REQUEST['increase'],
                          "houdinv_accounts_balance_sheet_transaction_added_status"=>'R',
                           "houdinv_accounts_balance_sheet_tax"=>'',
                           "houdinv_accounts_balance_sheet_date"=>strtotime($_REQUEST['date']));  
         
         $id = $_REQUEST['id'];  
         $account =   $_REQUEST['account_id']; 
         $result =  $this->Accountsmodel->UpdateAccounts($array2,$id,$account);
    }
    
        public function accuntingDElete()
    {
         
         $id = $_REQUEST['id'];  
         $account =   $_REQUEST['account_id']; 
         $result =  $this->Accountsmodel->DeleteAccounts($id,$account);
    }
    
    public  function report()
    {
        $id = $this->uri->segment(3);
        $data['all'] = $this->Accountsmodel->getAccountReport($id);
        $data['accountName'] = $this->Accountsmodel->getAccountName($id);
        $this->load->view('reportaccounting',$data);  
    }
    public function filterAccountData()
    {
        $setDataArray = array('from'=>$this->input->post('dateFrom'),'to'=>$this->input->post('dateTo'),'id'=>$this->input->post('accountId'));
        $getData = $this->Accountsmodel->getAccountReport($id,$setDataArray);
        print_r(json_encode($getData));
    }
    public function editaccountdata()
    {
        $data['parent'] = $this->Accountsmodel->getAccounts();      
        $data['type'] = $this->Accountsmodel->getAccountsType();
        $data['parentaccount'] = $this->Accountsmodel->getParentAccount();
        $this->load->view('editaccounts',$data);
    }
     
    public function balancesheet()
    {
        $getBalanceSheetData = $this->Accountsmodel->getBalanceSheetData();
        $this->load->view('balancesheet',$getBalanceSheetData);
    }
    public function accounthistory()
    {
         $id = $this->uri->segment(3);
        $data['all'] = $this->Accountsmodel->getAccountHistory($id);
        $data['accountholder'] = $this->Accountsmodel->getCustomerAndSuplier();
        $data['parentAccount'] = $this->Accountsmodel->getParentAccount($this->uri->segment('3'));
        $data['endingBalance'] = $this->Accountsmodel->getEndingBalanceData($this->uri->segment('3'));
        $data['accountname'] = $this->Accountsmodel->getAccountNameData($this->uri->segment('3'));
        $this->load->view('accounthistory',$data);
    }

    public function accounthistorydetails()
    {
         $id = $this->uri->segment(3);
        $data['AccountHistory'] = $this->AccountsNewmodel->getAccountHistory($id);       
        $data['endingBalance'] = $this->Accountsmodel->getEndingBalanceData($this->uri->segment('3'));
        $data['accountname'] = $this->Accountsmodel->getAccountNameData($this->uri->segment('3'));
        $this->load->view('accounthistorydetails',$data);
    }

    public function deletebalancesheet()
    {
        $getId = $this->uri->segment('3');
        $this->form_validation->set_rules('deletebalancesheetId','text','required|integer');
        if($this->form_validation->run() == true)
        {
            $getDeleteStatus = $this->Accountsmodel->deleteBalanceSheetdata($this->input->post('deletebalancesheetId'),$getId);
            if($getDeleteStatus)
            {
                $this->session->set_flashdata('success','Entry deleted successfully');
                redirect(base_url()."Accounts/accounthistory/".$getId."",'refresh');
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url()."Accounts/accounthistory/".$getId."",'refresh');    
            }
        }   
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(base_url()."Accounts/accounthistory/".$getId."",'refresh');
        }
    }
    public function updateBalanceSheeReport()
    {
        $this->form_validation->set_rules('fromdate','from date','required');
        $this->form_validation->set_rules('toDate','to date','required');
        if($this->form_validation->run() == true)
        {
            $resultArray = $this->Accountsmodel->getFilterBalanceSheetReports($this->input->post('fromdate'),$this->input->post('toDate'));
            print_r(json_encode($resultArray));
        }
        else
        {
            $resultArray = array('message'=>'validation','data'=>validation_errors());
            print_r(json_encode($resultArray));
        }
    }
    public function expence()
    {
        // Custom Pagination
        $getTotalCount = $this->Accountsmodel->getExpenseTotalCount();
        $config['base_url']    = base_url().'expence/';
        $config['uri_segment'] = 2;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(2);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getExpenseData = $this->Accountsmodel->getExpenseList($setArray);
        $this->load->view('expence',$getExpenseData);
    }
    // update Payment Status
    public function updateExpensePaymentStatus()
    {
        $this->form_validation->set_rules('totalExpenseAmount','amount','required');
        $this->form_validation->set_rules('paymentStatus','payment status','required');
        $this->form_validation->set_rules('transactionType','transaction type','required');
        $this->form_validation->set_rules('paymentType','payment type','required');
        if($this->form_validation->run() == true)
        {
            $getPaymentStatus = $this->input->post('paymentStatus');
            if($getPaymentStatus == 1)
            {
                $setTransactionId = "TXN".rand(99999999,10000000);
                $setTransactionArray = array('houdinv_transaction_transaction_id'=>$setTransactionId,
                'houdinv_transaction_type'=>$this->input->post('transactionType'),
                'houdinv_transaction_method'=>'cash',
                'houdinv_transaction_from'=>$this->input->post('paymentType'),
                'houdinv_transaction_for'=>'expense',
                'houdinv_transaction_for_id'=>$this->input->post('expenseIdData'),
                'houdinv_transaction_amount'=>$this->input->post('totalExpenseAmount'),
                'houdinv_transaction_date'=>date('Y-m-d'),
                'houdinv_transaction_status'=>'success');
                $getPaymentStatus = $this->Accountsmodel->updatePaymentStatus($setTransactionArray);
                if($getPaymentStatus)
                {
                    $this->session->set_flashdata('success','Payment status updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Accounts/expence", 'refresh'); 
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Accounts/expence", 'refresh'); 
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Accounts/expence", 'refresh');    
            }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            $this->load->helper('url');
            redirect(base_url()."Accounts/expence", 'refresh');
        }
    }
    public function addexpence()
    {
        $getPayeeData = $this->Accountsmodel->fetchAddExpenseData();
        $this->load->view('addexpence',$getPayeeData);
    }
    public function editexpence()
    {
        $getExpenseId = $this->uri->segment('3');
        if(!$getExpenseId)
        {
            $this->load->helper('url');
            redirect(base_url()."Accounts/expence", 'refresh');
        }
        $getPayeeData = $this->Accountsmodel->fetchAddExpenseData();
        $getPayeeData['expense'] = $this->Accountsmodel->getExpenseData($getExpenseId);
        $this->load->view('editexpence',$getPayeeData);
    }
    public function customreports()
    {
        $getprofitlossReport = $this->Accountsmodel->getProfitlossPercentageReports();
        $this->load->view('customreports',$getprofitlossReport);
    }
    public function updateCustomReports()
    {
        $this->form_validation->set_rules('fromdate','from date','required');
        $this->form_validation->set_rules('toDate','to date','required');
        if($this->form_validation->run() == true)
        {
            $resultArray = $this->Accountsmodel->getFilterCustomReports($this->input->post('fromdate'),$this->input->post('toDate'));
            print_r(json_encode($resultArray));
        }
        else
        {
            $resultArray = array('message'=>'validation','data'=>validation_errors());
            print_r(json_encode($resultArray));
        }
    }
    public function profitlossreports()
    {
        $getprofitlossReport = $this->Accountsmodel->getProfitlossReports();
        $this->load->view('profitlossreports',$getprofitlossReport);
    }
    public function updateProfitlosssdetailReport()
    {
        $this->form_validation->set_rules('fromdate','from date','required');
        $this->form_validation->set_rules('toDate','to date','required');
        if($this->form_validation->run() == true)
        {
            $resultArray = $this->Accountsmodel->getFilterProfitLossReports($this->input->post('fromdate'),$this->input->post('toDate'));
            print_r(json_encode($resultArray));
        }
        else
        {
            $resultArray = array('message'=>'validation','data'=>validation_errors());
            print_r(json_encode($resultArray));
        }
    }
    public function profitlosspercentagereports()
    {
        $getprofitlossReport = $this->Accountsmodel->getProfitlossPercentageReports();
        $this->load->view('profitlosspercentagereports',$getprofitlossReport);
    }
    public function updateProfitlossspercentagedetailReport()
    {
        $this->form_validation->set_rules('fromdate','from date','required');
        $this->form_validation->set_rules('toDate','to date','required');
        if($this->form_validation->run() == true)
        {
            $resultArray = $this->Accountsmodel->getFilterProfitlossPercentageReports($this->input->post('fromdate'),$this->input->post('toDate'));
            print_r(json_encode($resultArray));
        }
        else
        {
            $resultArray = array('message'=>'validation','data'=>validation_errors());
            print_r(json_encode($resultArray));
        }
    }
    // add expence data
    public function addexpencesdata()
    {
        $this->form_validation->set_rules('payeeId','select payee','required');
        $this->form_validation->set_rules('billDate','bill date','required');
        $this->form_validation->set_rules('paymentType','payment type','required');
        $this->form_validation->set_rules('paymentStatus','payment status','required');
        $this->form_validation->set_rules('refId','refrence id','required');
        if($this->form_validation->run() == true)
        {
            $setDateData = strtotime(date('Y-m-d'));
        //   add expense array data
            $setextraexpenceArray = array('houdinv_extra_expence_payee'=>$this->input->post('payeeId'),
            'houdinv_extra_expence_payee_type'=>$this->input->post('getPayeeTypeData'),
            'houdinv_extra_expence_payment_method'=>$this->input->post('paymentType'),
            'houdinv_extra_expence_subtotal'=>$this->input->post('setsubtotal'),
            'houdinv_extra_expence_tax'=>$this->input->post('setTaxData'),
            'houdinv_extra_expence_total_amount'=>$this->input->post('setNetAmount'),
            'houdinv_extra_expence_payment_status'=>$this->input->post('paymentStatus'),
            'houdinv_extra_expence_refrence_number'=>$this->input->post('refId'),
            'houdinv_extra_expence_payment_date'=>$this->input->post('billDate'),
            'houdinv_extra_expence_transaction_type'=>$this->input->post('transactionType'),
            'houdinv_extra_expence_created_at'=>$setDateData);
            // expense item data
             $setItemArray = array('houdinv_extra_expence_item_type'=>$this->input->post('setProductType'),
             'houdinv_extra_expence_item_quantity'=>$this->input->post('quantity'),
             'houdinv_extra_expence_item_rate'=>$this->input->post('rate'),
             'houdinv_extra_expence_item_tax'=>$this->input->post('tax'),
             'houdinv_extra_expence_item_total_amount'=>$this->input->post('amount'),
             'houdinv_extra_expence_item_item_id'=>$this->input->post('productId'));
             if($this->input->post('paymentStatus') == 1)
             {
                 $setTransactionId = 'TXN'.rand(99999999,10000000);
                 $setTransactionArray = array('houdinv_transaction_transaction_id'=>$setTransactionId,
                 'houdinv_transaction_type'=>$this->input->post('transactionType'),
                 'houdinv_transaction_method'=>'cash',
                 'houdinv_transaction_from'=>$this->input->post('paymentType'),
                 'houdinv_transaction_for'=>'expense',
                 'houdinv_transaction_amount'=>$this->input->post('setNetAmount'),
                 'houdinv_transaction_date'=>$this->input->post('billDate'),
                 'houdinv_transaction_status'=>'success');
             }
             else
             {
                 $setTransactionArray = array();
             }
             $getStatus = $this->Accountsmodel->updateExpenseData($setextraexpenceArray,$setItemArray,$setTransactionArray);
             if($getStatus['message'] == 'yes')
             {
                $this->session->set_flashdata('success','Expense addedd successfully');
                $this->load->helper('url');
                redirect(base_url()."Accounts/expence", 'refresh');
             }
             else if($getStatus['message'] == 'some items')
             {
                $this->session->set_flashdata('error','Expense addedd successfully. Some items are not addedd');
                $this->load->helper('url');
                redirect(base_url()."Accounts/expence", 'refresh');
             }
             else if($getStatus['message'] == 'transitems')
             {
                $this->session->set_flashdata('error','Expense addedd successfully. Some items and transaction are not addedd');
                $this->load->helper('url');
                redirect(base_url()."Accounts/expence", 'refresh');
             }
             else if($getStatus['message'] == 'trans')
             {
                $this->session->set_flashdata('error','Expense addedd successfully. Transaction are not addedd');
                $this->load->helper('url');
                redirect(base_url()."Accounts/expence", 'refresh');
             }
             else
             {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Accounts/addexpence", 'refresh');
             }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            $this->load->helper('url');
            redirect(base_url()."Accounts/addexpence", 'refresh');
        }
    }
    public function editexpencesdata()
    {
        $this->form_validation->set_rules('payeeId','select payee','required');
        $this->form_validation->set_rules('billDate','bill date','required');
        $this->form_validation->set_rules('paymentType','payment type','required');
        $this->form_validation->set_rules('paymentStatus','payment status','required');
        $this->form_validation->set_rules('refId','refrence id','required');
        if($this->form_validation->run() == true)
        {
            $setDateData = strtotime(date('Y-m-d'));
        //   add expense array data
            $setextraexpenceArray = array('houdinv_extra_expence_payee'=>$this->input->post('payeeId'),
            'houdinv_extra_expence_payee_type'=>$this->input->post('getPayeeTypeData'),
            'houdinv_extra_expence_payment_method'=>$this->input->post('paymentType'),
            'houdinv_extra_expence_subtotal'=>$this->input->post('setsubtotal'),
            'houdinv_extra_expence_tax'=>$this->input->post('setTaxData'),
            'houdinv_extra_expence_total_amount'=>$this->input->post('setNetAmount'),
            'houdinv_extra_expence_payment_status'=>$this->input->post('paymentStatus'),
            'houdinv_extra_expence_refrence_number'=>$this->input->post('refId'),
            'houdinv_extra_expence_payment_date'=>$this->input->post('billDate'),
            'houdinv_extra_expence_transaction_type'=>$this->input->post('transactionType'));
            // expense item data
             $setItemArray = array('houdinv_extra_expence_item_type'=>$this->input->post('setProductType'),
             'houdinv_extra_expence_item_quantity'=>$this->input->post('quantity'),
             'houdinv_extra_expence_item_rate'=>$this->input->post('rate'),
             'houdinv_extra_expence_item_tax'=>$this->input->post('tax'),
             'houdinv_extra_expence_item_total_amount'=>$this->input->post('amount'),
             'houdinv_extra_expence_item_item_id'=>$this->input->post('productId'));
             if($this->input->post('paymentStatus') == 1)
             {
                 $setTransactionId = 'TXN'.rand(99999999,10000000);
                 $setTransactionArray = array('houdinv_transaction_transaction_id'=>$setTransactionId,
                 'houdinv_transaction_type'=>$this->input->post('transactionType'),
                 'houdinv_transaction_method'=>'cash',
                 'houdinv_transaction_from'=>$this->input->post('paymentType'),
                 'houdinv_transaction_for'=>'expense',
                 'houdinv_transaction_amount'=>$this->input->post('setNetAmount'),
                 'houdinv_transaction_date'=>$this->input->post('billDate'),
                 'houdinv_transaction_status'=>'success');
             }
             else
             {
                 $setTransactionArray = array();
             }
             $getStatus = $this->Accountsmodel->updateExpenseDataSet($setextraexpenceArray,$setItemArray,$setTransactionArray,$this->input->post('expenseMainIdData'));
             if($getStatus['message'] == 'yes')
             {
                $this->session->set_flashdata('success','Expense addedd successfully');
                $this->load->helper('url');
                redirect(base_url()."Accounts/expence", 'refresh');
             }
             else if($getStatus['message'] == 'some items')
             {
                $this->session->set_flashdata('error','Expense addedd successfully. Some items are not addedd');
                $this->load->helper('url');
                redirect(base_url()."Accounts/expence", 'refresh');
             }
             else if($getStatus['message'] == 'transitems')
             {
                $this->session->set_flashdata('error','Expense addedd successfully. Some items and transaction are not addedd');
                $this->load->helper('url');
                redirect(base_url()."Accounts/expence", 'refresh');
             }
             else if($getStatus['message'] == 'trans')
             {
                $this->session->set_flashdata('error','Expense addedd successfully. Transaction are not addedd');
                $this->load->helper('url');
                redirect(base_url()."Accounts/expence", 'refresh');
             }
             else
             {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Accounts/editexpence/".$this->input->post('expenseMainIdData')."", 'refresh');
             }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            $this->load->helper('url');
            redirect(base_url()."Accounts/editexpence/".$this->input->post('expenseMainIdData')."", 'refresh');
        }
    }
    // add payee data
    public function addCustomer()
    {
        $this->form_validation->set_rules('customerName','text','required');
        $this->form_validation->set_rules('customerMobile','text','required');
        $this->form_validation->set_rules('customerEmail','text','required|valid_email');
        if($this->form_validation->run() == true)
        {
            $setInsertArray = array('name'=>$this->input->post('customerName'),'mobile'=>$this->input->post('customerMobile'),'customerAlternate'=>$this->input->post('customerAlternate'),'email'=>$this->input->post('customerEmail'),
            'address'=>$this->input->post('customerAddress'),'landmark'=>$this->input->post('customerLandmark'),'country'=>$this->input->post('customerCountry'),'state'=>$this->input->post('customerState'),
            'city'=>$this->input->post('customerCity'),'pin'=>$this->input->post('customerPin'),'status'=>$this->input->post('customerStatus'));
            $getUserStatus = $this->Customermodel->addCustomerData($setInsertArray);
            if($getUserStatus['message'] == 'yes')
            {
                $this->session->set_flashdata('success','Customer addedd successfully');
                $this->load->helper('url');
                redirect(base_url()."Accounts/addexpence", 'refresh');
            }
            else if($getUserStatus['message'] == 'email')
            {
                $this->session->set_flashdata('error','Email address is already exist');
                $this->load->helper('url');
                redirect(base_url()."Accounts/addexpence", 'refresh');
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Accounts/addexpence", 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('error','All fileds with * are mandatory');
            $this->load->helper('url');
            redirect(base_url()."Accounts/addexpence", 'refresh');
        }
    }
    public function addStaffmember()
    {
        $getSmsEmail = smsemailcredentials();
     if($this->input->post('customSearchDriver')){

$this->form_validation->set_rules('staff_name','Staff Name','required|trim');
        $this->form_validation->set_rules('staff_contact_number','Staff Contact Number','required|trim|regex_match[/^[0-9]{10}$/]');
        $this->form_validation->set_rules('staff_email','Email','required|valid_email|is_unique[houdinv_staff_management.staff_email]');
        $this->form_validation->set_rules('staff_department','Staff Department','required|trim');
        $this->form_validation->set_rules('staff_status','Staff Status','required|trim');
        $this->form_validation->set_rules('staff_address','Staff Address','required|trim');
        $this->form_validation->set_rules('staff_name','Staff Name','required|trim');


     if($this->form_validation->run() == true)
    {
        $staff_name = $this->input->post('staff_name');
        $staff_contact_number = $this->input->post('staff_contact_number');
        $staff_department = $this->input->post('staff_department');
        $staff_status = $this->input->post('staff_status');
        $staff_warehouse= $this->input->post('staff_warehouse');
        $staff_email = $this->input->post('staff_email');
        $staff_alternat_contact = $this->input->post('staff_alternat_contact');
        $staff_address = $this->input->post('staff_address');
        $password_send = $this->input->post('password_send'); 

        $dashboard = $this->input->post('dashboard');
        $order = $this->input->post('order');
        $cataogry = $this->input->post('cataogry');
        $product = $this->input->post('product');
        $setting = $this->input->post('setting');
        $staff_members = $this->input->post('staff_members');
        $inventory = $this->input->post('inventory');
        $customer_management = $this->input->post('customer_management');
        $delivery = $this->input->post('delivery');
        $discount = $this->input->post('discount');
        $supplier_management = $this->input->post('supplier_management');
        $templates = $this->input->post('templates');
        $purchase = $this->input->post('purchase');
        $gift_voucher = $this->input->post('gift_voucher');
        $calendar = $this->input->post('calendar');
        $POS = $this->input->post('POS');
        $store_setting = $this->input->post('store_setting');
        $tax = $this->input->post('tax');
        $shipping = $this->input->post('shipping');
        $campaign = $this->input->post('campaign');
        $coupons=$this->input->post('coupons');  

           $getAdminP = $this->random_password();       
            $letters1='abcdefghijklmnopqrstuvwxyz'; 
            $string1=''; 
            for($x=0; $x<3; ++$x)
            {  
                $string1.=$letters1[rand(0,25)].rand(0,9); 
            }
            $staff_password_salt = password_hash($string1,PASSWORD_DEFAULT);

           $staff_password = crypt($getAdminP,$staff_password_salt);
           
           

  	$setData = strtotime(date('Y-m-d'));
        $setStafffArray = array("main_staff_data"=>array("staff_name"=>$staff_name,"staff_contact_number"=>$staff_contact_number,"staff_department"=>$staff_department,"staff_status"=>$staff_status,"staff_password"=>$staff_password,"staff_password_salt"=>$staff_password_salt,"staff_email"=>$staff_email,"staff_alternat_contact"=>$staff_alternat_contact,"staff_address"=>$staff_address,"password_send"=>$password_send,"create_date"=>$setData,"staff_warehouse"=>$staff_warehouse),
        	"staff_subdata"=>array("dashboard"=>$dashboard,"order"=>$order,"cataogry"=>$cataogry,"product"=>$product,"setting"=>$setting,"staff_members"=>$staff_members,"inventory"=>$inventory,"customer_management"=>$customer_management,"delivery"=>$delivery,"discount"=>$discount,"supplier_management"=>$supplier_management,"templates"=>$templates,"purchase"=>$purchase,"coupons"=>$coupons,"gift_voucher"=>$gift_voucher,"calendar"=>$calendar,"POS"=>$POS,"store_setting"=>$store_setting,"tax"=>$tax,"shipping"=>$shipping,"campaign"=>$campaign));


        $getInsertStatus = $this->Staffmodel->insert_staff($setStafffArray);
        if($getInsertStatus){
    if($password_send=='sms'){
        $getReaminingSMS = remainingSMS();
        if($getReaminingSMS['sms'] > 0)
        {
            $getSMS = smsemailcredentials();
            if(count($getSMS) > 0)
            {   
                $id = $getSMS['twilio'][0]->houdin_twilio_sid;
                $token = $getSMS['twilio'][0]->houdin_twilio_token;
                $url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
                $from = $getSMS['twilio'][0]->houdin_twilio_number;
                $body = "Login URL: ".base_url()."  Username: ".$staff_email." Userpassword:  ".$getAdminP."";
                $data1 = array (
                    'From' => $from,
                    'To' => $this->input->post('staff_contact_number'),
                    'Body' => $body,
                );
                
                $post = http_build_query($data1);
                $x = curl_init($url );
                curl_setopt($x, CURLOPT_POST, true);
                curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
                curl_setopt($x, CURLOPT_POSTFIELDS, $post);
                $y = curl_exec($x);
                if (strpos($y, 'PIN') == false) 
                {
                    failureSMS();
                    $this->session->set_flashdata('success','Staff addedd successfully.Credentials not sent');             
                    redirect(base_url()."Accounts/addexpence");
                }
                else
                {
                    succcessSMS($getReaminingSMS['sms']-1);
                    $this->session->set_flashdata('success','Staff addedd successfully.');             
                    redirect(base_url()."Accounts/addexpence");
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');             
                redirect(base_url()."Accounts/addexpence");
            }
        }
        else
        {
            $this->session->set_flashdata('error','Staff addedd successfully.Please upgrade your sms package.');             
            redirect(base_url()."Accounts/addexpence");
        }
    }
    else
    {   
        $getRemainingEmail = getRemainingEmail();
        if($getRemainingEmail['remaining'] > 0)
        {
            // put validation
            $getLogo = fetchvedorsitelogo();
            $getInfo = fetchvendorinfo();
            $getSocial = fetchsocialurl(); 
            if(count($getSocial) > 0) 
            {
                $setSocialString = "";
                if($getSocial[0]->facebook_url)
                {
                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                }
                if($getSocial[0]->twitter_url)
                {
                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                }
                if($getSocial[0]->youtube_url)
                {
                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                }
            }
            // send email to admin user
            $url = 'https://api.sendgrid.com/';
            $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
            $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
            $json_string = array(
            'to' => array(
                $this->input->post('staff_email')
            ),
            'category' => 'test_category'
            );
            $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
            <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
            <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
            <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
            <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
            <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">


            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Login URL:</strong></td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.base_url().'</td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">


            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Your Login Email is:</strong></td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$staff_email.'</td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">

            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Your Password is:</strong></td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$getAdminP.'</td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
            <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
            <table width="100%" ><tr >
            <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
            <tr><td style="">
            '.$setSocialString.'</td>
            </tr></table>
            </div>
            </div></td></tr></table>';
            
            
            $params = array(
                'api_user'  => $user,
                'api_key'   => $pass,
                'x-smtpapi' => json_encode($json_string),
                'to'        => $this->input->post('staff_email'),
                'fromname'  => $getInfo['name'],
                'subject'   => 'Your  New Password',
                'html'      => $htm,
                'from'      => $getInfo['email'],
            );
            $request =  $url.'api/mail.send.json';
            $session = curl_init($request);
            curl_setopt ($session, CURLOPT_POST, true);
            curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
            curl_setopt($session, CURLOPT_HEADER, false);
            curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($session);
            curl_close($session);
            $getResponse = json_decode($response);
            if($getResponse->message == 'success')
            {
                updateSuceessEmailCount($getRemainingEmail['remaining']-1);
                $this->session->set_flashdata('success','Staff addedd successfully');             
                redirect(base_url()."Accounts/addexpence");
            }
            else
            {
                updateErrorEmailLog();
                $this->session->set_flashdata('success','Staff addedd successfully. Credentials is not sent successfully');             
                redirect(base_url()."Accounts/addexpence");
            }
        }   
        else
        {
            $this->session->set_flashdata('error','Staff addedd successfully.Please upgrade your email package.');             
            redirect(base_url()."Accounts/addexpence");
        }    
        }
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Accounts/addexpence");
        }
      }else{ 
         
        $this->session->set_flashdata('error','Something went wrong. Please try again');             
        redirect(base_url()."Accounts/addexpence");
      } 
 
     }else{
        $this->session->set_flashdata('error','Something went wrong. Please try again');             
        redirect(base_url()."Accounts/addexpence");
     }
    }
    public function addsupplierData()
    {
        $this->form_validation->set_rules('supplierCompanyName','text','trim|required');
        $this->form_validation->set_rules('supplierContactPerson','text','trim|required');
        $this->form_validation->set_rules('supplierEmail','text','trim|required|valid_email|is_unique[houdinv_suppliers.houdinv_supplier_email]');
        $this->form_validation->set_rules('supplierMobile','text','trim|required');
        $this->form_validation->set_rules('supplierAddress','text','trim|required');
        $this->form_validation->set_rules('supplierStatus','text','trim|required|in_list[active,deactive]');
        if($this->form_validation->run() == true)
        {
            $setSupplierArray = array('company'=>$this->input->post('supplierCompanyName'),'contactPerson'=>$this->input->post('supplierContactPerson'),
            'email'=>$this->input->post('supplierEmail'),'mobile'=>$this->input->post('supplierMobile'),'landline'=>$this->input->post('supplierlandline'),'address'=>$this->input->post('supplierAddress'),
            'products'=>$this->input->post('supplierProduct'),'status'=>$this->input->post('supplierStatus'));
            $getSupplierInsertStatus = $this->Suppliermodel->addSupplierData($setSupplierArray);
            if($getSupplierInsertStatus['message'] == 'yes')
            {
                $this->session->set_flashdata('success','Supplier addedd successfully');
                $this->load->helper('url');
                redirect(base_url()."Accounts/addexpence", 'refresh');
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Accounts/addexpence", 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('error','All fileds are mandatory or email address is already exist');
            $this->load->helper('url');
            redirect(base_url()."Accounts/addexpence", 'refresh');
        }
    }
    public function addNoninventoryProduct()
    {
        $this->form_validation->set_rules('productName','product name','required');
        $this->form_validation->set_rules('productDescription','product description','required');
        $this->form_validation->set_rules('productPrice','product price','required');
        if($this->form_validation->run() == true)
        {
            $setData = strtotime(date('Y-m-d'));
            $setProductArray = array('houdinv_noninventory_products_name'=>$this->input->post('productName'),'houdinv_noninventory_products_desc'=>$this->input->post('productDescription'),
            'houdinv_noninventory_products_price'=>$this->input->post('productPrice'),'houdinv_noninventory_products_created_at'=>$setData);
            $getInsertData = $this->Accountsmodel->addNoninventoryProduct($setProductArray);
            if($getInsertData)
            {
                $setArrayData = array('message'=>'yes','id'=>$getInsertData,'name'=>$this->input->post('productName'),'description'=>$this->input->post('productDescription'),'price'=>$this->input->post('productPrice'));
                echo json_encode($setArrayData);
            }
            else
            {
                $setArrayData = array('message'=>'no');
                echo json_encode($setArrayData);
            }
        }
        else
        {
            $setArrayData = array('message'=>'fields');
            echo json_encode($setArrayData);
            
        }
    }
    // add new entry
    public function addNewAccountJournal()
    {
        if($this->input->post('setDecrease'))
        {
            $setDecrease = $this->input->post('setDecrease');
            $setIncrease = 0;
        }
        else
        {
            $setDecrease = 0;
            $setIncrease = $this->input->post('setIncreaseData');
        }
        $setCreatedDate = strtotime(date('Y-m-d'));
        $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$this->input->post('getAccountId'),
        'houdinv_accounts_balance_sheet_reference'=>$this->input->post('getRefNumber'),
        'houdinv_accounts_balance_sheet_ref_type'=>$this->input->post('entrytype'),
        'houdinv_accounts_balance_sheet_pay_account'=>$this->input->post('setPayeeData'),
        'houdinv_accounts_balance_sheet_payee_type'=>$this->input->post('payeetype'),
        'houdinv_accounts_balance_sheet_account'=>$this->input->post('selectAccount'),
        'houdinv_accounts_balance_sheet_concile'=>$this->input->post('concileStatus'),
        'houdinv_accounts_balance_sheet_created_at'=>$setCreatedDate,
        'houdinv_accounts_balance_sheet_memo'=>$this->input->post('getMemoData'),
        'houdinv_accounts_balance_sheet_decrease'=>$setDecrease,
        'houdinv_accounts_balance_sheet__increase'=>$setIncrease,
        'houdinv_accounts_balance_sheet_final_balance'=>$this->input->post(''),
        'houdinv_accounts_balance_sheet_date'=>$this->input->post('getdate'),
        );
        $getData = $this->Accountsmodel->updateAccountData($setInsertArray,$this->input->post('getAccountId'),$this->input->post('parentaccount'));
        print_r(json_encode($getData));
    }
    // payable entry
    public function addPaybaleEntry()
    {
        $this->form_validation->set_rules('getPayeeType','payee','required');
        $this->form_validation->set_rules('getPayeeId','payee','required');
        $this->form_validation->set_rules('getPaymentDate','payment date','required');
        $this->form_validation->set_rules('getPaymentType','payment type','required');
        $this->form_validation->set_rules('getTransactionType','transaction type','required');
        $this->form_validation->set_rules('getTransactionAmount','transaction amount','required');
        $this->form_validation->set_rules('getRefrenceNumber','referencenumber','required');
        if($this->form_validation->run())
        {
            // set extra expense array
            $ExtraExpenseArray = array(
                'houdinv_extra_expence_payee'=>$this->input->post('getPayeeId'),
                'houdinv_extra_expence_payee_type'=>$this->input->post('getPayeeType'),
                'houdinv_extra_expence_payment_method'=>$this->input->post('getPaymentType'),
                'houdinv_extra_expence_subtotal'=>$this->input->post('getTransactionAmount'),
                'houdinv_extra_expence_tax'=>0,
                'houdinv_extra_expence_total_amount'=>$this->input->post('getTransactionAmount'),
                'houdinv_extra_expence_payment_status'=>1,
                'houdinv_extra_expence_transaction_type'=>$this->input->post('getTransactionType'),
                'houdinv_extra_expence_refrence_number'=>$this->input->post('getRefrenceNumber'),
                'houdinv_extra_expence_type'=>'payment',
                'houdinv_extra_expence_payment_date'=>$this->input->post('getPaymentDate'),
            );
            $getResponse = $this->Accountsmodel->updatePayableEntry($ExtraExpenseArray);
            print_r(json_encode($getResponse));
        }   
        else
        {
            $getResponse = array('response'=>'error','message'=>validation_errors());
            print_r(json_encode($getResponse));
        }     
    }
    public function taxreport()
    {
        $getTotalTax = $this->Accountsmodel->getTaxData();
        $this->load->view('taxdata',$getTotalTax);
    }
    public function viewreport()
    {
        if(!$this->uri->segment('3'))
        {
            $this->load->helper('url');
            redirect(base_url()."Accounts/taxreport", 'refresh');
        }
        $getTaxReports = $this->Accountsmodel->getTaxReports($this->uri->segment('3'));
        $this->load->view('viewtaxdata',$getTaxReports);
    }
    public function orderReport()
    {
        $getMainData = $this->uri->segment('3');
        $getSearchType = $this->uri->segment('4');
        if(!$getMainData || !$getSearchType)
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            redirect(base_url()."Accounts/reports", 'refresh');
        }
        if($getMainData == 'order')
        {
            if($getSearchType == 'ccd')
            {
                $setDateFrom = $this->uri->segment('5');
                $setDateTo = $this->uri->segment('6');
                $setArray = array('searchType'=>$getSearchType,'dateFrom'=>$setDateFrom,'dateTo'=>$setDateTo);
                $getDataValue = $this->Accountsmodel->fetchOrderReports($setArray);
            }
            else
            {
                $setArray = array('searchType'=>$getSearchType);
                $getDataValue = $this->Accountsmodel->fetchOrderReports($setArray);
            }
        }
        $getOrderData = $getDataValue['orderData'][0];
        $getTotalOrder = $getOrderData->totalOrderData;
        if($getTotalOrder)
        {
            $getTotalOrder = $getTotalOrder;
        }
        else
        {
            $getTotalOrder = 0;
        }
        $getTotalDiscount = number_format((float)$getOrderData->totalOrderDiscount, 2, '.', '');
        if($getTotalDiscount)
        {
            $getTotalDiscount = $getTotalDiscount;
        }
        else
        {
            $getTotalDiscount = 0;
        }
        $getTotalOrderAmount = number_format((float)$getOrderData->totalOrderAmount, 2, '.', '');
        if($getTotalOrderAmount)
        {
            $getTotalOrderAmount = $getTotalOrderAmount;
        }
        else
        {
            $getTotalOrderAmount = 0;
        }
        $getTotalPaidAmount = number_format((float)$getOrderData->totalPaidAmount, 2, '.', ''); 
        if($getTotalPaidAmount)
        {
            $getTotalPaidAmount = $getTotalPaidAmount;
        }
        else
        {
            $getTotalPaidAmount = 0;
        }
        $getTotalRemainingAmount = number_format((float)$getOrderData->totalRemainingAmount, 2, '.', '');
        if($getTotalRemainingAmount)
        {
            $getTotalRemainingAmount = $getTotalRemainingAmount;
        }
        else
        {
            $getTotalRemainingAmount = 0;
        }
        $getStatementDate = date('d-m-Y',$getDataValue['statementFrom'][0]->houdinv_order_created_at)." to ".date('d-m-Y',$getDataValue['statementTo'][0]->houdinv_order_created_at);

        // logo dynamic
        $getLogo = fetchvedorsitelogo();
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Houdin-e');
      $pdf->SetTitle('Houdin-e - Order Report');
      $pdf->SetSubject('Houdin-e - Order Report');
      $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
      $pdf->setFooterData(array(0,64,0), array(0,64,128));
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = <<<EOD
<table style="width:100%;">
<tbody>
<tr>

<td style="width:70%;"><img style="display:block; margin:auto; width:150px; " src="$getLogo" alt="Logo"></td>
<td style="width:30%;"> 
</td>
</tr>
</tbody>
</table>
  <br/>
  <hr/>
  <br/>
  <br/>
  <table style="width:100%;">
  <tbody>
  <h4>
$getMainData Statement</h4>
  <tr>
   
  <td style="width:50%;"><table style="float:right; width:auto; margin-right:5px;">
  <tbody>

  <tr>
  <td style="font-size:12px;">Statement Date :  $getStatementDate
</td>
  </tr>
  </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
<table>
<tr><td>&nbsp;</td></tr>
</table>

  <table>
  <tr><td>&nbsp;</td></tr>
  </table>
  <table border="1" cellpadding="10">
  <tr>
  <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total Order</td>
    <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total Amount</td>
      <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total Discount</td>
        <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total Paid</td>

  </tr>

  <tr>
  <td style="font-size:12px;font-weight:700;">$getTotalOrder</td>
    <td style="font-size:12px;font-weight:700;">$getTotalOrderAmount</td>
      <td style="font-size:12px;font-weight:700; ">$getTotalDiscount</td>
        <td style="font-size:12px;font-weight:700;">$getTotalPaidAmount</td>

  </tr>
  </table>
  <table>
  <tr><td>&nbsp;</td></tr>
  </table>

  <table style="width:98%;" cellpadding="10" cellspacing="10">
  <tbody>
  <tr>
  <td style="width:40%;"></td>
  <td style="width:60%;">
  <table border="1" cellpadding="10" style="text-align:right; width:98%;  border:1px solid #000;">
  <tbody>

 <tr style="padding:20px">  <td style="font-size:12px;color:#fff;" bgcolor="#000">Total Remaining</td> 
  <td style="text-align:right;font-size:11px;">$getTotalRemainingAmount</td>
  </tr>
  
  </tbody>
  </table>
  </td>
  </tr>
   
   
  </tbody>
  </table>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_order_report.pdf', 'I');
    }
    // transaction report
    public function transactionReport()
    {
        $getMainData = $this->uri->segment('3');
        $getSearchType = $this->uri->segment('4');
        if(!$getMainData || !$getSearchType)
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            redirect(base_url()."Accounts/reports", 'refresh');
        }
        if($getMainData == 'transaction')
        {
            if($getSearchType == 'ccd')
            {
                $setDateFrom = $this->uri->segment('5');
                $setDateTo = $this->uri->segment('6');
                $setArray = array('searchType'=>$getSearchType,'dateFrom'=>$setDateFrom,'dateTo'=>$setDateTo);
                $getDataValue = $this->Accountsmodel->fetchTransactionReports($setArray);
            }
            else
            {
                $setArray = array('searchType'=>$getSearchType);
                $getDataValue = $this->Accountsmodel->fetchTransactionReports($setArray);
            }
        }
        if($getDataValue['totalTransaction'])
        {
            $setTotalTransaction = number_format((float)$getDataValue['totalTransaction'], 2, '.', '');
        }
        else
        {
            $setTotalTransaction = 0;
        }
        if($getDataValue['totalCredit'])
        {
            $setTotalCredit = number_format((float)$getDataValue['totalCredit'], 2, '.', '');
        }
        else
        {
            $setTotalCredit = 0;
        }
        if($getDataValue['totalDebit'])
        {
            $setTotalDebit = number_format((float)$getDataValue['totalDebit'], 2, '.', '');
        }
        else
        {
            $setTotalDebit = 0;
        }
        if($setTotalCredit > $setTotalDebit)
        {
            $setText = "Profit";
            $setValueData = "+".$setTotalCredit-$setTotalDebit;
        }
        else
        {
            $setText = "Loss";
            $setValueData = "-".$setTotalDebit-$setTotalCredit;
        }
        $setString = date('d-m-Y',strtotime($getDataValue['from']))." to ".date('d-m-Y',strtotime($getDataValue['to'])); 
        // logo dynamic
        $getLogo = fetchvedorsitelogo();

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Houdin-e');
      $pdf->SetTitle('Houdin-e - Transaction Report');
      $pdf->SetSubject('Houdin-e - Transaction Report');
      $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
      $pdf->setFooterData(array(0,64,0), array(0,64,128));
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = <<<EOD
<table style="width:100%;">
<tbody>
<tr>

<td style="width:70%;"><img style="display:block; margin:auto; width:150px; " src="$getLogo" alt="Logo"></td>
<td style="width:30%;"> 
</td>
</tr>
</tbody>
</table>
  <br/>
  <hr/>
  <br/>
  <br/>
  <table style="width:100%;">
  <tbody>
  <h4>
$getMainData Statement</h4>
  <tr>
   
  <td style="width:50%;"><table style="float:right; width:auto; margin-right:5px;">
  <tbody>

  <tr>
  <td style="font-size:12px;">Statement Date : 
</td>
  </tr>
  </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
<table>
<tr><td>&nbsp;</td></tr>
</table>

  <table>
  <tr><td>&nbsp;</td></tr>
  </table>
  <table border="1" cellpadding="10">
  <tr>
  <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total Transaction</td>
    <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total Credit</td>
      <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total Debit</td>
        <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Profit / Loss</td>

  </tr>

  <tr>
  <td style="font-size:12px;font-weight:700;">$setTotalTransaction</td>
    <td style="font-size:12px;font-weight:700;">$setTotalCredit</td>
      <td style="font-size:12px;font-weight:700; ">$setTotalDebit</td>
        <td style="font-size:12px;font-weight:700;">$setText</td>

  </tr>
  </table>
  <table>
  <tr><td>&nbsp;</td></tr>
  </table>

  <table style="width:98%;" cellpadding="10" cellspacing="10">
  <tbody>
  <tr>
  <td style="width:40%;"></td>
  <td style="width:60%;">
  <table border="1" cellpadding="10" style="text-align:right; width:98%;  border:1px solid #000;">
  <tbody>

 <tr style="padding:20px">  <td style="font-size:12px;color:#fff;" bgcolor="#000">Total Profit / Loss</td> 
  <td style="text-align:right;font-size:11px;">$setValueData</td>
  </tr>
  
  </tbody>
  </table>
  </td>
  </tr>
   
   
  </tbody>
  </table>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_transaction_report.pdf', 'I');
    }
    public function companyReport()
    {
        $getMainData = $this->uri->segment('3');
        $getSearchType = $this->uri->segment('4');
        if(!$getMainData || !$getSearchType)
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            redirect(base_url()."Accounts/reports", 'refresh');
        }
        if($getMainData == 'company')
        {
            if($getSearchType == 'ccd')
            {
                $setDateFrom = $this->uri->segment('5');
                $setDateTo = $this->uri->segment('6');
                $setArray = array('searchType'=>$getSearchType,'dateFrom'=>$setDateFrom,'dateTo'=>$setDateTo);
                $getDataValue = $this->Accountsmodel->fetchTransactionReports($setArray);
            }
            else
            {
                $setArray = array('searchType'=>$getSearchType);
                $getDataValue = $this->Accountsmodel->fetchCompanyOverviewReports($setArray);
            }
        }
        if($getDataValue['totalOrder'])
        {
            $setTotalOrder = number_format((float)$getDataValue['totalOrder'], 2, '.', '');
        }
        else
        {
            $setTotalOrder = 0;
        }
        if($getDataValue['totalProducts'])
        {
            $setTotalProducts = number_format((float)$getDataValue['totalProducts'], 2, '.', '');
        }
        else
        {
            $setTotalProducts = 0;
        }
        if($getDataValue['totalPurchase'])
        {
            $setTotalPurchase = number_format((float)$getDataValue['totalPurchase'], 2, '.', '');
        }
        else
        {
            $setTotalPurchase = 0;
        }
        if($getDataValue['totalCustomer'])
        {
            $setTotalUser = number_format((float)$getDataValue['totalCustomer'], 2, '.', '');
        }
        else
        {
            $setTotalUser = 0;
        }
        if($getDataValue['totalSupplier'])
        {
            $setTotalSupplier = number_format((float)$getDataValue['totalSupplier'], 2, '.', '');
        }
        else
        {
            $setTotalSupplier = 0;
        }
        $setString = date('d-m-Y',strtotime($getDataValue['from']))." to ".date('d-m-Y',strtotime($getDataValue['to'])); 

        // logo dynamic
        $getLogo = fetchvedorsitelogo();

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Houdin-e');
      $pdf->SetTitle('Houdin-e - Company Report');
      $pdf->SetSubject('Houdin-e - Company Report');
      $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
      $pdf->setFooterData(array(0,64,0), array(0,64,128));
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = <<<EOD
<table style="width:100%;">
<tbody>
<tr>

<td style="width:70%;"><img style="display:block; margin:auto; width:150px; " src="$getLogo" alt="Logo"></td>
<td style="width:30%;"> 
</td>
</tr>
</tbody>
</table>
  <br/>
  <hr/>
  <br/>
  <br/>
  <table style="width:100%;">
  <tbody>
  <h4>
$getMainData Statement</h4>
  <tr>
   
  <td style="width:50%;"><table style="float:right; width:auto; margin-right:5px;">
  </td>
  </tr>
  </tbody>
  </table>
<table>
<tr><td>&nbsp;</td></tr>
</table>

  <table>
  <tr><td>&nbsp;</td></tr>
  </table>
  <table border="1" cellpadding="10">
  <tr>
  <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total Order</td>
    <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total Products</td>
      <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total Purchase</td>
        <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total User</td>
        <td style="font-size:12px;font-weight:700; color:#fff;" bgcolor="#000">Total Supplier</td>

  </tr>

  <tr>
  <td style="font-size:12px;font-weight:700;">$setTotalOrder</td>
    <td style="font-size:12px;font-weight:700;">$setTotalProducts</td>
      <td style="font-size:12px;font-weight:700; ">$setTotalPurchase</td>
        <td style="font-size:12px;font-weight:700;">$setTotalUser</td>
        <td style="font-size:12px;font-weight:700;">$setTotalSupplier</td>
  </tr>
  </table>
  <table>
  <tr><td>&nbsp;</td></tr>
  </table>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_transaction_report.pdf', 'I');
    }

    // profit loss report
    public function profitlossreport()
    {
        $getDateFrom = $this->uri->segment('3');
        $getDateTo = $this->uri->segment('4');
        if(!$getDateFrom || !$getDateTo)
        {
            $this->load->helper('url');
            redirect(base_url()."Accounts/profitlossreport", 'refresh'); 
        }
        $getVendorInfo = fetchvendorinfo();
        $setName = $getVendorInfo['name'];
        $setDateRange = $getDateFrom." - ".$getDateTo;
        $resultArray = $this->Accountsmodel->getFilterProfitLossReports($getDateFrom,$getDateTo);
        // print_r($resultArray);
        // creadit data
        $setCreditHtml = "";
        $setCreditAmount = 0;
        $setDebitHtml = "";
        $setDebitAmount = 0;
        for($index = 0; $index < count($resultArray['credit']); $index++)
        {
            if($setCreditHtml)
            {
                $setCreditHtml = $setCreditHtml.'<tr>
                <td style="font-size:12px; width:15%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.date('d-m-Y',strtotime($resultArray['credit'][$index]['main']->houdinv_transaction_date)).'</td>
                <td style="font-size:12px;">'.$resultArray['credit'][$index]['main']->houdinv_transaction_for.'</td>
                <td style="font-size:12px;">'.$resultArray['credit'][$index]['main']->houdinv_transaction_transaction_id.'</td>
                <td style="font-size:12px;">'.$resultArray['credit'][$index]['user'].'</td>
                <td style="font-size:12px;">'.$resultArray['credit'][$index]['main']->houdinv_transaction_amount.'</td>
                <td style="font-size:12px;">0.00</td>
                </tr>';
            }
            else
            {
                $setCreditHtml = '<tr>
                <td style="font-size:12px; width:15%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.date('d-m-Y',strtotime($resultArray['credit'][$index]['main']->houdinv_transaction_date)).'</td>
                <td style="font-size:12px;">'.$resultArray['credit'][$index]['main']->houdinv_transaction_for.'</td>
                <td style="font-size:12px;">'.$resultArray['credit'][$index]['main']->houdinv_transaction_transaction_id.'</td>
                <td style="font-size:12px;">'.$resultArray['credit'][$index]['user'].'</td>
                <td style="font-size:12px;">'.$resultArray['credit'][$index]['main']->houdinv_transaction_amount.'</td>
                <td style="font-size:12px;">0.00</td>
                </tr>';
            }
            $setCreditAmount = $setCreditAmount+$resultArray['credit'][$index]['main']->houdinv_transaction_amount;
        }
        for($index = 0; $index < count($resultArray['debit']); $index++)
        {
            if($setDebitHtml)
            {
                $setDebitHtml = $setDebitHtml.'<tr>
                <td style="font-size:12px; width:15%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.date('d-m-Y',strtotime($resultArray['debit'][$index]['main']->houdinv_transaction_date)).'</td>
                <td style="font-size:12px;">'.$resultArray['debit'][$index]['main']->houdinv_transaction_for.'</td>
                <td style="font-size:12px;">'.$resultArray['debit'][$index]['main']->houdinv_transaction_transaction_id.'</td>
                <td style="font-size:12px;">'.$resultArray['debit'][$index]['user'].'</td>
                <td style="font-size:12px;">'.$resultArray['debit'][$index]['main']->houdinv_transaction_amount.'</td>
                <td style="font-size:12px;">0.00</td>
                </tr>';
            }
            else
            {
                $setDebitHtml = '<tr>
                <td style="font-size:12px; width:20%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.date('d-m-Y',strtotime($resultArray['debit'][$index]['main']->houdinv_transaction_date)).'</td>
                <td style="font-size:12px;">'.$resultArray['debit'][$index]['main']->houdinv_transaction_for.'</td>
                <td style="font-size:12px;">'.$resultArray['debit'][$index]['main']->houdinv_transaction_transaction_id.'</td>
                <td style="font-size:12px;">'.$resultArray['debit'][$index]['user'].'</td>
                <td style="font-size:12px;">'.$resultArray['debit'][$index]['main']->houdinv_transaction_amount.'</td>
                <td style="font-size:12px;">0.00</td>
                </tr>';
            }
            $setDebitAmount = $setDebitAmount+$resultArray['debit'][$index]['main']->houdinv_transaction_amount;
        }
        $getFinalAmount = $setCreditAmount-$setDebitAmount;

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Houdin-e');
        $pdf->SetTitle('Houdin-e - Profit Loss Report');
        $pdf->SetSubject('Houdin-e - Profit Loss Report');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->setFooterData(array(0,64,0), array(0,64,128));
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
    $pdf->setFontSubsetting(true);
    $pdf->SetFont('dejavusans', '', 14, '', true);
    $pdf->AddPage();
    $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
    $html = '<table  cellpadding="7">
                <tr style="text-align:center;">
                <td><h4>'.$setName.'</h4></td>
                </tr>
                <tr style="text-align:center;">
                <td style="font-size:15px;">PROFIT & LOSS DETAIL</td>
                </tr>
                <tr style="text-align:center;">
                <td style="font-size:15px;">$setDateRange</td>
                </tr>
                
                </table>
                <table cellpadding="7">
                <tr>
                <td style="font-size:12px;border-top:1px solid #000;border-bottom:1px solid #000;"><strong>Date</strong></td>
                <td style="font-size:12px;border-top:1px solid #000;border-bottom:1px solid #000;"><strong>Transaction Type</strong></td>
                <td style="font-size:12px;border-top:1px solid #000;border-bottom:1px solid #000;"><strong>No.</strong></td>
                <td style="font-size:12px;border-top:1px solid #000;border-bottom:1px solid #000;"><strong>Name</strong></td>
                <td style="font-size:12px;border-top:1px solid #000;border-bottom:1px solid #000;"><strong>Amount</strong></td>
                <td style="font-size:12px;border-top:1px solid #000;border-bottom:1px solid #000;"><strong>Balance</strong></td>
                </tr>
                <tr>
                <td colspan="7" style="font-size:12px;">Ordinary Income/Expenses<br/>&nbsp;&nbsp;Income<br/>&nbsp;&nbsp;&nbsp;Service</td>
                </tr>
               '.$setCreditHtml.'
                </table>
                <table cellpadding="5">
                <tr>
                <td colspan="6"style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>&nbsp;&nbsp;&nbsp;Total For Services</strong></p></td>
                <td colspan="2" style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>'.$setCreditAmount.'</strong></p></td>
                </tr>
                <tr>
                <td colspan="6"style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>Total For Income</strong></p></td>
                <td colspan="2" style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>'.$setCreditAmount.'</strong></p></td>
                </tr>
                </table>
           
                <table cellpadding="5">
                <tr><td><p style="font-size:12px;">Expenses</p></td></tr>
                <tr><td><p style="font-size:12px;">&nbsp;&nbsp;Depreciation Expense</p></td></tr>
                </table>
                <table cellpadding="5">
                 
                  '.$setDebitHtml.'
                </table>
                   <table cellpadding="5">
                <tr>
                <td colspan="6"style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>&nbsp;&nbsp;&nbsp;Total For Depreciation Expenses</strong></p></td>
                <td colspan="2" style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>'.$setDebitAmount.'</strong></p></td>
                </tr>
                <tr>
                <td colspan="6"style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>&nbsp;&nbsp;&nbsp;Total For Expenses</strong></p></td>
                <td colspan="2" style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>'.$setDebitAmount.'</strong></p></td>
                </tr>
                 <tr>
                <td colspan="6"style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>Net Income</strong></p></td>
                <td colspan="2" style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>'.$getFinalAmount.'</strong></p></td>
                </tr>
                </table>
                ';
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    $pdf->Output('houdin-e_profit_loss_report.pdf', 'I');
    }
    // profit loss percentage
    public function profitlosspercentagedatareport()
    {
        $getDateFrom = $this->uri->segment('3');
        $getDateTo = $this->uri->segment('4');
        if(!$getDateFrom || !$getDateTo)
        {
            $this->load->helper('url');
            redirect(base_url()."Accounts/profitlosspercentagereports", 'refresh'); 
        }
        $getVendorInfo = fetchvendorinfo();
        $setDateString = $getDateFrom." - ".$getDateTo;
        $resultArray = $this->Accountsmodel->getFilterProfitLossPercenatageReports($getDateFrom,$getDateTo);
         if($resultArray['credit'] > $resultArray['debit'])
         {
             $setSign = "+";
         }
         else
         {
            $setSign = "-";
         }
        $getProfit = $resultArray['credit']-$resultArray['debit'];
        // debit percentage
        $getDebitPercenateg = ($resultArray['credit']/$resultArray['debit'])*100;
        // profit percenatge
        if($setSign == "+")
        {
            $getProfitPercenatage = $getDebitPercenateg-100;
        }
        else
        {
            $getProfitPercenatage = 100-$getDebitPercenateg;
        }

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Houdin-e');
        $pdf->SetTitle('Houdin-e - Profit Loss Percentage Detail');
        $pdf->SetSubject('Houdin-e - Profit Loss Percentage Detail');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->setFooterData(array(0,64,0), array(0,64,128));
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
    $pdf->setFontSubsetting(true);
    $pdf->SetFont('dejavusans', '', 14, '', true);
    $pdf->AddPage();
    $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
    $html = '<table  cellpadding="7">
                <tr style="text-align:center;">
                <td><h4>'.$getVendorInfo['name'].'</h4></td>
                </tr>
                <tr style="text-align:center;">
                <td style="font-size:15px;">PROFIT AND LOSS % OF TOTAL INCOME</td>
                </tr>
                <tr style="text-align:center;">
                <td style="font-size:15px;">'.$setDateString.'</td>
                </tr>
                
                </table>
                <table cellpadding="5" style="width:100%;">
                <tr>
                <td colspan ="2" style="text-align:right;border-top:1px solid #000;border-bottom:1px solid #000;font-size:12px;">TOTAL</td>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;font-size:12px;">&nbsp;</td>
                </tr>
                 <tr>
                <td colspan ="2" style="text-align:right;border-bottom:1px solid #000;font-size:12px;">'.$setDateString.' </td>
                <td style="text-align:right;border-bottom:1px solid #000;font-size:12px;">% OF INCOME</td>
                </tr>
                 <tr>
                <td colspan="3" style="font-size:12px;">Income</td>
                
                </tr>
                  <tr>
                <td  style="border-bottom:1px solid #000;font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;Service</td>
                <td style="text-align:right;border-bottom:1px solid #000;font-size:12px;">'.$resultArray['credit'].'</td>
                <td style="border-bottom:1px solid #000;font-size:12px;">&nbsp;</td>
                </tr>
                <tr>
                <td  style="border-bottom:1px solid #000;font-size:12px;"><strong>Total Income</strong></td>
                <td style="text-align:right;border-bottom:1px solid #000;font-size:12px;"><strong>'.$resultArray['credit'].'</strong></td>
                <td style="border-bottom:1px solid #000;font-size:12px;text-align:right;"><strong>100%</strong></td>
                </tr>
                  <tr>
                <td  style="font-size:12px;">GROSS PROFIT</td>
                <td style="text-align:right;font-size:12px;"><strong>'.$resultArray['credit'].'</strong></td>
                <td style="font-size:12px;text-align:right;"><strong>100%</strong></td>
                </tr>
                <tr>
                <td colspan="3" style="font-size:12px;">Expenses</td>
                
                </tr>
                  <tr>
                <td  style="border-bottom:1px solid #000;font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;Depreciation Expenses</td>
                <td style="text-align:right;border-bottom:1px solid #000;font-size:12px;">'.$resultArray['debit'].'</td>
                <td style="border-bottom:1px solid #000;font-size:12px;">&nbsp;</td>
                </tr>
                 <tr>
                <td  style="border-bottom:1px solid #000;font-size:12px;"><strong>Total Expenses</strong></td>
                <td style="text-align:right;border-bottom:1px solid #000;font-size:12px;"><strong>'.$resultArray['debit'].'</strong></td>
                <td style="border-bottom:1px solid #000;font-size:12px;text-align:right;"><strong>'.$setSign.''.$getDebitPercenateg.'%</strong></td>
                </tr>
                <tr>
                <td  style="font-size:12px;">PROFIT</td>
                <td style="text-align:right;font-size:12px;"><strong>'.$getProfit.'</strong></td>
                <td style="font-size:12px;text-align:right;"><strong>'.$getProfitPercenatage.'%</strong></td>
                </tr>
                </table>
                
                ';
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    $pdf->Output('houdin-e_profit_loss_percenatge.pdf', 'I');
    }
    // custome balance sheet
    public function custombalancesheetpdf()
    {
        $getDateFrom = $this->uri->segment('3');
        $getDateTo = $this->uri->segment('4');
        if(!$getDateFrom || !$getDateTo)
        {
            $this->load->helper('url');
            redirect(base_url()."Accounts/profitlosspercentagereports", 'refresh'); 
        }
        $resultArray = $this->Accountsmodel->getFilterCustomReports($getDateFrom,$getDateTo);
        $getVendorInfo = fetchvendorinfo();
        $setDateString = $getDateFrom." - ".$getDateTo;
        $getFinalAmount = $resultArray['credit']-$resultArray['debit'];
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Houdin-e');
        $pdf->SetTitle('Houdin-e - Custom Reports');
        $pdf->SetSubject('Houdin-e - Custom Reports');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->setFooterData(array(0,64,0), array(0,64,128));
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
    $pdf->setFontSubsetting(true);
    $pdf->SetFont('dejavusans', '', 14, '', true);
    $pdf->AddPage();
    $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
    $html = '<table  cellpadding="7">
                <tr style="text-align:center;">
                <td><h4>'.$getVendorInfo['name'].'</h4></td>
                </tr>
                <tr style="text-align:center;">
                <td style="font-size:15px;">CUSTOM SUMMARY REPORT</td>
                </tr>
                <tr style="text-align:center;">
                <td style="font-size:15px;">'.$setDateString.'</td>
                </tr>
                <tr style="text-align:right;">
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;">Total</td>
                </tr>
                </table>
                <table cellpadding="5">
                <tr><td><p style="font-size:12px;">Income</p></td></tr>
               
                </table>
                <table cellpadding="5">
                <tr>
                <td><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;Service</p></td>
                <td><p style="text-align:right;font-size:12px;">'.$resultArray['credit'].'</p></td></tr>
                                 
                  <tr>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>Total Income </strong></p></td>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="text-align:right;font-size:12px;"><strong>'.$resultArray['credit'].'</strong></p></td></tr>
                  <tr>
                <td><p style="font-size:12px;">Gross Profit</p></td>
                <td><p style="text-align:right;font-size:12px;">'.$resultArray['credit'].'</p></td></tr>
                </table>
                <table cellpadding="5">
                <tr><td><p style="font-size:12px;">Expenses</p></td></tr>
                </table>
                <table cellpadding="5">
                <tr><td><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Depreciation Expense</p></td><td><p style="text-align:right;font-size:12px;">'.$resultArray['debit'].'</p></td></tr>
                
                
                
                
                
                      <tr><td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>Total Expenses</strong></p></td><td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="text-align:right;font-size:12px;"><strong>'.$getFinalAmount.'</strong></p></td></tr> 
                       <tr><td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;">Profit</p></td><td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="text-align:right;font-size:12px;"><strong>'.$getFinalAmount.'</strong></p></td></tr>
                </table>
                ';
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    $pdf->Output('houdin-e_custom_reposrts.pdf', 'I');
    }
    public function balancesheetpdf()
    {
        $getDateFrom = $this->uri->segment('3');
        $getDateTo = $this->uri->segment('4');
        if(!$getDateFrom || !$getDateTo)
        {
            $this->load->helper('url');
            redirect(base_url()."Accounts/balancesheetpdf", 'refresh'); 
        }
        $resultArray = $this->Accountsmodel->getFilterBalanceSheetReports($getDateFrom,$getDateTo);
        
        if($resultArray['totalRemainingCredit']) { $totalRemainingCredit = $resultArray['totalRemainingCredit']; } else { $totalRemainingCredit = 0; }
        if($resultArray['totalCreditedAmount']) { $totalCreditedAmount = $resultArray['totalCreditedAmount']; } else { $totalCreditedAmount = 0; }
        $getTotalAccountRecievable = $totalRemainingCredit+$totalCreditedAmount;
        if($resultArray['finalPaidDebitAmount']) { $finalPaidDebitAmount = $resultArray['finalPaidDebitAmount']; } else { $finalPaidDebitAmount = 0; }
        if($resultArray['remainingDebitAmount']) { $remainingDebitAmount = $resultArray['remainingDebitAmount']; } else { $remainingDebitAmount = 0; }
        $getTotalCurrentLibalties = $finalPaidDebitAmount+$remainingDebitAmount;
        $getProfitData = $getTotalAccountRecievable-$getTotalCurrentLibalties;
        $getVendorInfo = fetchvendorinfo(); 

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Houdin-e');
        $pdf->SetTitle('Houdin-e - Balance Sheet');
        $pdf->SetSubject('Houdin-e - Balance Sheet');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->setFooterData(array(0,64,0), array(0,64,128));
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
    $pdf->setFontSubsetting(true);
    $pdf->SetFont('dejavusans', '', 14, '', true);
    $pdf->AddPage();
    $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
    $html = '<table  cellpadding="7">
                <tr style="text-align:center;">
                <td><h4>'.$getVendorInfo['name'].'</h4></td>
                </tr>
                <tr style="text-align:center;">
                <td style="font-size:15px;">BALANCE SHEET</td>
                </tr>
                <tr style="text-align:center;">
                <td style="font-size:15px;">'.$resultArray['from'].' - '.$resultArray['to'].'</td>
                </tr>
                <tr style="text-align:right;">
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;">Total</td>
                </tr>
                </table>
                <table cellpadding="5">
                <tr><td colspan="2"><p style="font-size:12px;">Assets</p></td></tr>
                <tr><td colspan="2"><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;Current Assets</p></td></tr>
                <tr><td><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Undeposit Funds</p></td><td><p style="text-align:right;font-size:12px;">'.$totalRemainingCredit.'</p></td></tr>
                <tr><td colspan="2"><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account recieved</p></td></tr>
                </table>
                <table cellpadding="5">
                 
                  
                <tr>
                <td><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Recieved</p></td>
                <td><p style="text-align:right;font-size:12px;">'.$totalCreditedAmount.'</p></td></tr>
                  <tr>
                <td style="border-top:1px solid #000; border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Accounts recieved </strong></p></td>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="text-align:right;font-size:12px;"><strong>'.$totalCreditedAmount.'</strong></p></td></tr>
                <tr>
                <td colspan="2"><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Receivable(Debtors)</p></td>
                </tr>
                <tr>
                <td><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Receivable(Debtors)</p></td>
                <td><p style="text-align:right;font-size:12px;">'.$totalRemainingCredit.'</p></td></tr>
                <tr>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Accounts receivable (Debtors) </strong></p></td>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="text-align:right;font-size:12px;"><strong>'.$totalRemainingCredit.'</strong></p></td></tr>
                 <tr>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Current Assets  </strong></p></td>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="text-align:right;font-size:12px;"><strong>'.$getTotalAccountRecievable.'</strong></p></td></tr>
                  <tr>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Assets </strong></p></td>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="text-align:right;font-size:12px;"><strong>'.$getTotalAccountRecievable.'</strong></p></td></tr>
                </table>
                <table cellpadding="5">
                <tr><td><p style="font-size:12px;">Liabilities and Equity</p></td></tr>
                
                </table>


                   <table cellpadding="5">
              
          
                <tr>
                <td colspan="2"><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Paid</p></td></tr> 
                      <tr><td><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Paid</p></td><td><p style="text-align:right;font-size:12px;">'.$finalPaidDebitAmount.' </p></td></tr>  
                      <tr><td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Account Paid</strong></p></td><td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="text-align:right;font-size:12px;"><strong>'.$finalPaidDebitAmount.'</strong></p></td></tr> 
                        
                </table>



                <table cellpadding="5">
              
          
                <tr>
                <td colspan="2"><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Payable</p></td></tr> 
                      <tr><td><p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Payable</p></td><td><p style="text-align:right;font-size:12px;">'.$remainingDebitAmount.' </p></td></tr>  
                      <tr><td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Account Payable</strong></p></td><td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="text-align:right;font-size:12px;"><strong>'.$remainingDebitAmount.'</strong></p></td></tr> 
                       <tr><td style="border-top:1px solid #000;"><p style="font-size:12px;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Current Liabilities</strong></p></td><td style="border-top:1px solid #000;"><p style="text-align:right;font-size:12px;"><strong>'.$getTotalCurrentLibalties.'</strong></p></td></tr> 
                </table>
                <table cellpadding="5">
                <tr><td><p style="font-size:12px;">Equity</p></td></tr>
                <tr><td><p style="font-size:12px;">&nbsp;&nbsp;Retained Earnings</p></td></tr>
                </table>
                <table cellpadding="5">
                <tr>
                <td><p style="font-size:12px;">&nbsp;&nbsp;Profit for the year</p></td>
                <td><p style="font-size:12px;text-align:right;">'.$getProfitData.'</p></td></tr>
                <tr>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;">&nbsp;&nbsp;<strong>Total Equity </strong></p></td>
                <td style="border-top:1px solid #000;border-bottom:1px solid #000;"><p style="font-size:12px;text-align:right;"><strong>'.$getProfitData.'</strong></p></td>
                </tr>
                <tr>
                <td style="border-bottom:1px solid #000;"><p style="font-size:12px;">&nbsp;&nbsp;<strong>Total Liabilities and Equity </strong></p></td>
                <td style="border-bottom:1px solid #000;"><p style="font-size:12px;text-align:right;"><strong>'.$getTotalAccountRecievable.'</strong></p></td>
                </tr>
                </table>';
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    $pdf->Output('houdin-e_balance_sheet.pdf', 'I');
    }
}
