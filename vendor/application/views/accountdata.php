    <?php $this->load->view('Template/header.php') ?>
    <?php $this->load->view('Template/accountsidebar.php'); ?>

    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">
    <!--START Page-Title -->
    <div class="row">
    
    <div class="col-md-8">
    <h4 class="page-title">Dashboard</h4>
    <ol class="breadcrumb"> 
    <li><a href="<?php echo base_url(); ?>">Home</a></li>
    <li><a href="<?php echo base_url(); ?>Accounts">Accounts</a></li>
 
    <li class="active">Dashboard</li>
    </ol>
    </div>
     <div class="clearfix"></div>
   
    <!--END Page-Title -->
    <div class="row m-t-20">

    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Earning.png">
    <h2 class="m-0 text-dark counter font-600"><?php echo number_format((float)$totalEarning, 2, '.', ''); ?></h2>
    <div class="text-muted m-t-5">Total Earning</div>
    </div>
    </div>
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Earning_From_Web.png">
    <h2 class="m-0 text-dark counter font-600"><?php echo number_format((float)$totalEarningWeb, 2, '.', ''); ?></h2>
    <div class="text-muted m-t-5">Total Earning from Web</div>
    </div>
    </div>
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Earning_From_App.png">
    <h2 class="m-0 text-dark counter font-600"><?php echo number_format((float)$totalEarningApp, 2, '.', ''); ?></h2>
    <div class="text-muted m-t-5">Total Earning From App</div>
    </div>
    </div>
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Earning_From_App.png">
    <h2 class="m-0 text-dark counter font-600"><?php echo number_format((float)$totalEarningStore, 2, '.', ''); ?></h2>
    <div class="text-muted m-t-5">Total Earning From Store</div>
    </div>
    </div>

    </div>
     
    <div class="row " id="display_advance">
    <div class="col-md-12">
    <div class="card-box">
    <div id="revenueGraph" style="min-width: 100%; height: 400px; margin: 0 auto"></div>
    </div>
    </div>
    <div class="col-md-12">
    <div class="card-box">
    <div id="revenueGraphData" style="min-width: 100%; height: 400px; max-width: 600px; margin: 0 auto"></div>
    </div>
    </div>
    <div class="col-md-12">
    <div class="card-box">
    <div id="transactionGraph" style="min-width: 100%; height: 400px; max-width: 600px; margin: 0 auto"></div>
    </div>
    </div>
    </div>
    <!-- <div class="row">
    <div class="col-md-12 ">
    <a href="<?php //echo base_url(); ?>accounts/pdfproduct" class="btn btn-info pull-right m-r-10"><i class="fa fa-file-pdf-o"></i>&nbsp;Generate PDF Top Selling Product</a>
    </div>
    </div> -->
            </div>


    </div> <!-- container -->

    </div> <!-- content -->
    <?php $this->load->view('Template/footer.php') ?>
    <script>
    $(document).ready(function(){
    $('#click_advance').click(function() {
    $('#display_advance').toggle('5000');
    $("i", this).toggleClass("fa fa-chevron-up fa fa-chevron-down");
    });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
        // Last 10 days revenue graph
        Highcharts.chart('revenueGraph', {
    chart: {
        type: 'area',
        spacingBottom: 30
    },
    title: {
        text: 'Last 10 Days Revenue Graph'
    },
    // subtitle: {
    //     text: '* Jane\'s banana consumption is unknown',
    //     floating: true,
    //     align: 'right',
    //     verticalAlign: 'bottom',
    //     y: 15
    // },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    xAxis: {
        // categories: ['Current Day', 'Current Month', 'Current Year', 'From Web', 'From App', 'From Store']
        categories: [<?php echo $revenueDate; ?>]
    },
    yAxis: {
        title: {
            text: 'Y-Axis'
        },
        labels: {
            formatter: function () {
                return this.value;
            }
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.x + ': ' + this.y;
        }
    },
    plotOptions: {
        area: {
            fillOpacity: 0.5
        }
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Earnings',
        data: [<?php echo $totalRevenue; ?>]
    }]
});

// revenue data


// Make monochrome colors
var pieColors = (function () {
    var colors = [],
        base = Highcharts.getOptions().colors[0],
        i;

    for (i = 0; i < 10; i += 1) {
        // Start out with a darkened base color (negative brighten), and end
        // up with a much brighter color
        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
    }
    return colors;
}());

// Build the chart

Highcharts.chart('revenueGraphData', {
    title: {
        text: 'Revenue Breakdown'
    },
    xAxis: {
        categories: ['Total Earnings', 'Earnings From Web', 'Earnings From App', 'Earnings From Store']
    },
    labels: {
        items: [{
            // html: 'Revenue Breakdown',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Earnings',
        data: [<?php if($totalEarning) { echo $totalEarning; } else { echo 0; } ?>, <?php if($totalEarningWeb) { echo $totalEarningWeb; } else { echo 0; } ?>, <?php  if($totalEarningApp) { echo $totalEarningApp; } else { echo 0; } ?>, <?php if($totalEarningStore) { echo $totalEarningStore; } else { echo 0; } ?>]
    },  {
        type: 'pie',
        name: 'Revenue Breakdown',
        data: [{
            name: 'Total Earnings',
            y: <?php if($totalEarning) { echo $totalEarning; } else { echo 0; } ?>,
            color: Highcharts.getOptions().colors[0] 
        }, {
            name: 'Earnings From Web',
            y: <?php if($totalEarningWeb) { echo $totalEarningWeb; } else { echo 0; } ?>,
            color: Highcharts.getOptions().colors[1] 
        }, {
            name: 'Earnings From App',
            y: <?php if($totalEarningApp) { echo $totalEarningApp; } else { echo 0; } ?>,
            color: Highcharts.getOptions().colors[2] 
        },
        {
            name: 'Earnings From Storep',
            y: <?php if($totalEarningStore) { echo $totalEarningStore; } else { echo 0; }
             ?>,
            color: Highcharts.getOptions().colors[3] 
        }],
        center: [100, 80],
        size: 100,
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }]
});
// transaction graph
Highcharts.chart('transactionGraph', {
    title: {
        text: 'Transaction Breakdown'
    },
    xAxis: {
        categories: ['Total Transaction', 'Total Credit', 'Total Debit']
    },
    labels: {
        items: [{
            // html: 'Revenue Breakdown',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Earnings',
        data: [<?php if($totalTransaction) { echo $totalTransaction; } else { echo 0; } ?>, <?php if($totalCredit) { echo $totalCredit; } else { echo 0; } ?>, <?php  if($totalDebit) { echo $totalDebit; } else { echo 0; } ?>]
    },  {
        type: 'pie',
        name: 'Transaction',
        data: [{
            name: 'Total Transaction',
            y: <?php if($totalTransaction) { echo $totalTransaction; } else { echo 0; } ?>,
            color: Highcharts.getOptions().colors[0] 
        }, {
            name: 'Total Credit',
            y: <?php if($totalCredit) { echo $totalCredit; } else { echo 0; } ?>,
            color: Highcharts.getOptions().colors[1] 
        }, {
            name: 'Total Debit',
            y: <?php if($totalDebit) { echo $totalDebit; } else { echo 0; } ?>,
            color: Highcharts.getOptions().colors[2] 
        }],
        center: [100, 80],
        size: 100,
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }]
});

    })
    </script>
