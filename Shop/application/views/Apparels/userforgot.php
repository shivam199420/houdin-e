        <?php
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/Template/header'); ?>

        <!-- Page item Area -->
        <div id="page_item_area">
        <div class="container">
        <div class="row">
        <div class="col-sm-6 text-left">
        <h3>Change password</h3>
        </div>
        </div>
        </div>
        </div>


        <!-- Login Page -->
        <div class="login_page_area">
        <div class="container">
        <div class="row">
        <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="create_account_area">
        <h2 class="caa_heading">Change Password</h2>
        <?php
            if($this->session->flashdata('error'))
            {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
            }
            if($this->session->flashdata('success'))
            {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
            }
            ?>
            <?php echo form_open(base_url( 'Forgot/updatePass' ), array( 'id' => 'forgotPass', 'method'=>'post' ));?>
        <div class="caa_form_area">
        <p>Please enter the following details.</p>
        <div class="caa_form_group">
        <div class="caf_form">
        <label>Pin</label>
        <div class="input-area">
            <input type="text" name="forgotPin" class="required_validation_for_user_forgot name_validation number_validation" maxlength="4" />
        </div>
        </div>
        <div class="caf_form">
        <label>New Password</label>
        <div class="input-area">
            <input type="password" name="forgotPass" class="required_validation_for_user_forgot name_validation opass" />
        </div>
        </div>
        <div class="caf_form">
        <label>Confirm Password</label>
        <div class="input-area">
            <input type="password" name="forgotConPass" class="required_validation_for_user_forgot name_validation cpass"  />
        </div>
        </div>
        <div class="caf_form">
        <input type="submit" class="btn btn-default acc_btn text-center" value="Submit"/>
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <?php
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/Template/footer'); ?>
         <!-- CLient side form validation -->
        <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#forgotPass',function(){
                var check_required_field='';
                $(this).find(".required_validation_for_user_forgot").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>
