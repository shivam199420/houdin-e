<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>

   <!-- About Banner Start -->

   <section id="about">

       <div class="container">

           <div class="row">

               <div class="about-heading text-center">

                   <h2>My Address</h2>



               </div>

           </div>

       </div>

   </section>

<!-- About Banner End -->



<!-- my account part Start -->

<section id="account">

    <div class="container">

        <div class="row">

          <?php
          if($this->session->flashdata('error'))
          {
              echo "<div class='alert alert-danger' style='width:100%;'>".$this->session->flashdata('error')."</div>";
          }
          else if($this->session->flashdata('success'))
          {
              echo "<div class='alert alert-success' style='width:100%;'>".$this->session->flashdata('success')."</div>";
          }
          ?>

            <div class="col-md-12">

               <div class="login-account">
                    <!--<h3>My address</h3>
                    <p>Add your new address just press button then add.</p>
                    <hr>-->

                   <div class="procd">

                     <a href="<?php echo base_url() ?>Address/add" class="btn btn-primary acc_btn text-center pull-right" style="margin-bottom: 10px;">Add New Address</a>

                    </div>

                </div>

            </div>

            <?php
            foreach($userAddress as $userAddressList)
            {
             ?>
            <div class="col-md-6">
            <div class="address-fatch">
              <div class="address-div">
                 <div class="col-md-8 col-xs-12">
                   <h4><?php echo $userAddressList->houdinv_user_address_name."(".$userAddressList->houdinv_user_address_phone.")" ?> </h4> </div>
                   <div class="col-md-4 col-xs-12 text-right">
                     <a href="<?php echo base_url() ?>Address/edit/<?php echo $userAddressList->houdinv_user_address_id ?>" >
                       <i class="fa fa-pencil"></i>
                     </a>
                     &nbsp;&nbsp;
                     <a href="<?php echo base_url() ?>Address/deleteAddress/<?php echo $userAddressList->houdinv_user_address_id ?>" >
                       <i class="fa fa-trash-o address_i"></i>
                     </a>
                     <!-- <i class="fa fa-pencil" aria-hidden="true"></i> -->
                     <!-- <i class="fa fa-trash-o" aria-hidden="true"></i> -->
                   </div>



              </div>

              <div class="address-div1">
                <address><?php echo $userAddressList->houdinv_user_address_user_address ?>,<br>
                <?php echo $userAddressList->houdinv_user_address_city ?>,<br>
                <?php echo $userAddressList->houdinv_user_address_zip ?>
                </address>
              </div>
            </div>



            </div>
          <?php } ?>

        </div>

    </div>

</section>

<!-- my account part End -->

  <!-- Footer Part Start -->
  <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer'); ?>
