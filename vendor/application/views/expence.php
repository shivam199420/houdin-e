    <?php $this->load->view('Template/header.php') ?>
    <?php $this->load->view('Template/accountsidebar.php');
    $getCurrency = getVendorCurrency();
    if($getCurrency[0]->houdin_users_currency=="USD")
    {
      $currencysymbol= "$";
    }else if($getCurrency[0]->houdin_users_currency=="AUD"){
      $currencysymbol= "$";
    }else if($getCurrency[0]->houdin_users_currency=="Euro"){
      $currencysymbol= "£";
    }else if($getCurrency[0]->houdin_users_currency=="Pound"){
      $currencysymbol= "€";
    }else if($getCurrency[0]->houdin_users_currency=="INR"){
      $currencysymbol= "₹";
    }
    ?>
    <style type="text/css">
    @media screen and (min-width: 768px){}
    .dropdown.dropdown-lg .dropdown-menu {
    min-width: 390px!important;
    }
    }
    </style>
    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">

    <!--START Page-Title -->
    <div class="row">

    <div class="col-md-8">
    <h4 class="page-title">Expenses</h4>
    <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>">Home</a></li>
    <li><a href="<?php echo base_url(); ?>Accounts">Accounts</a></li>
    <li class="active">Expenses</li>
    </ol>
    </div>
    <!-- <div class="col-md-4">
    <form role="search" class="navbar-left app-search pull-left custom_search_all">
    <input type="text" placeholder="Search..." class="form-control">
    <a href=""><i class="fa fa-search"></i></a>
    </form></div> -->

    </div>
    <!--END Page-Title -->


    <div class="row">
    <?php
    if($this->session->flashdata('error'))
    {
      echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
    }
    if($this->session->flashdata('success'))
    {
      echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    ?>
    <div class="col-md-12">

    <div class="card-box table-responsive">
    <div class="btn-group pull-right m-t-10 m-b-20">
    <a href="<?php echo base_url(); ?>accounts/addexpence" class="btn btn-default m-r-5" title="New Transaction"><i class="fa fa-plus"></i></a>
    </div>

    <table class="table table-striped table-bordered table_shop_custom">
    <thead>
    <tr>

    <th>Date</th>
    <th>Ref. No.</th>
    <th>Payee Detail</th>
    <th>Total Before Tax</th>
    <th>Tax</th>
    <th>Total</th>
    <th>Payment Status</th>
    <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($payeeResult as $payeeResultList)
    {
      $setMainArray = $payeeResultList['main'];
      $setChildPayee = $payeeResultList['payee'];
      ?>
      <tr>
    <td><?php echo date('d-m-Y',strtotime($setMainArray->houdinv_extra_expence_payment_date)) ?></td>
    <td><?php echo $setMainArray->houdinv_extra_expence_refrence_number ?></td>
    <td><?php echo $setChildPayee['name']."(".$setChildPayee['email'].")" ?></td>
    <td><?php echo $currencysymbol; ?><?php echo $setMainArray->houdinv_extra_expence_subtotal ?></td>
    <td><?php echo $currencysymbol; ?><?php echo $setMainArray->houdinv_extra_expence_tax ?></td>
    <td><?php echo $currencysymbol; ?><?php echo $setMainArray->houdinv_extra_expence_total_amount ?></td>
    <td>
    <?php
    if($setMainArray->houdinv_extra_expence_payment_status == 1)
    {
      $setText = 'Paid';
      $setClass = "success";
    }
    else
    {
      $setText = 'Not Paid';
      $setClass = "danger";
    }
    ?>
    <button type="button" class="btn btn-xs btn-<?php echo $setClass ?>"><?php echo $setText ?></button>
    </td>
    <td>
    <!-- <a href="<?php echo base_url(); ?>accounts/editexpence/<?php echo $setMainArray->houdinv_extra_expence_id ?>" class="btn btn-primary waves-effect waves-light">Edit/View</a> -->
    <?php
    if($setMainArray->houdinv_extra_expence_payment_status != 1)
    {
    ?>
    <button class="btn btn-primary waves-effect waves-light openPaymentStatus" data-amount="<?php echo $setMainArray->houdinv_extra_expence_total_amount ?>" data-id="<?php echo $setMainArray->houdinv_extra_expence_id ?>">Update payment Status</button>
    <?php }
    ?>
    </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>




        <!-- update status -->
          <div id="updatePaymentStatusModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
            <div class="modal-dialog">
            <?php echo form_open(base_url( 'Accounts/updateExpensePaymentStatus' ), array( 'id' => 'addExpenceDataForm', 'method'=>'post' ));?>
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Update Payment Status</h4>
            </div>
            <div class="modal-body">
            <div class="row">
            <input type="hidden" class="expenseIdData" name="expenseIdData"/>
            <div class="col-md-12">
            <div class="form-group">
            <input type="text" class="totalExpenseAmount form-control" name="totalExpenseAmount" readonly="readonly"/>
            </div>
            </div>

            <div class="col-md-12">
            <div class="form-group">
            <select class="form-control required_validation_for_add_expense_data" name="paymentStatus">
            <option value="">Choose Payment Status</option>
            <option value="1">Paid</option>
            </select>
            </div>
            </div>

            <div class="col-md-12">
            <div class="form-group">
            <select class="form-control required_validation_for_add_expense_data" name="transactionType">
            <option value="">Choose Transaction Type</option>
            <option value="credit">Credit</option>
            <option value="debit">Debit</option>
            </select>
            </div>
            </div>

            <div class="col-md-12">
            <div class="form-group">
            <select class="form-control required_validation_for_add_expense_data" name="paymentType">
            <option value="">Choose Payment Method</option>
            <option value="cash">Cash</option>
            <option value="card">Card</option>
            <option value="cheque">Cheque</option>
            </select>
            </div>
            </div>

            </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-info" name="" value="Update">
            </div>
            </div>
            <?php echo form_close(); ?>
              </div>

            </div>
    <?php $this->load->view('Template/footer.php') ?>


    <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addExpenceDataForm',function(){
			var check_required_field='';
			$(".required_validation_for_add_expense_data").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
			return true;
			}
		});
    $(document).on('click','.openPaymentStatus',function(){
      $('.expenseIdData').val($(this).attr('data-id'));
      $('.totalExpenseAmount').val($(this).attr('data-amount'));
      $('#updatePaymentStatusModal').modal('show');
    })
	});
	</script>
