        <?php $this->load->view('Template/header') ?>
        <?php $this->load->view('Template/sidebar') ?> 
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">

        <!--START Page-Title -->
        <div class="row">
     
        <div class="col-md-8">
        <h4 class="page-title">Deduct Inventory</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>inventory/Inventorylanding">Inventory</a></li>
        <li class="active">Deduct Inventory</li>
        </ol>
        </div>
        <!-- <div class="col-md-4">
        <form role="search" class="navbar-left app-search pull-left custom_search_all">
        <input type="text" placeholder="Search..." class="form-control">
        <a href=""><i class="fa fa-search"></i></a>
        </form></div> -->

        </div>
        <!--END Page-Title -->


        <div class="row m-t-20">
        <div class="col-md-12">

        <div class="card-box table-responsive">

        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th style="width: 15%">Purchase Id</th>
        <th>Product details</th>
        <th>Quantity Return</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            foreach($deductInventory as $deductInventoryList)
            {
            ?>
            <tr>
        <td><?php echo $deductInventoryList['purchaseId'] ?></td>
        <td><?php echo $deductInventoryList['productTitle'] ?></td>
        <td><?php echo $deductInventoryList['return'] ?></td>
        </tr>
            <?php }
            ?>
        </tbody>
        </table>
        </div> 
        </div>
        </div>  
        </div>
        </div>
        </div>

        <?php $this->load->view('Template/footer') ?>
         
