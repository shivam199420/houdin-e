<?php  $this->load->view('Template/header');?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css">
   
<!-- <link href="<?php echo base_url(); ?>assets/css/index.css" rel="stylesheet">-->
    <link href="<?php echo base_url(); ?>assets/css/mfb.css" rel="stylesheet">
 
<style type="text/css">
  .custom-left-title{
    margin-top: 4%;
    font-size: 46px;

  }
  .pb-8, .py-8 {
    padding-bottom: 2rem !important;
}
.font600{
  font-weight: 600;
}
.mb-1, .my-1 {
    margin-bottom: .25rem !important;
    line-height: 17px !important;
    font-size: 18px !important;
    color:#000;
}
.pricing-1 .plan-name {
        padding: 20px;
    text-transform: uppercase;
    font-size: 22px;
    font-weight: 600;
    border-bottom: 1px solid #f1f2f3;
    background: linear-gradient(to bottom,#9e6d83 0,#d4c5cc 101%);
    color:#ffffff;
    border-radius: 0px 25px 0px 25px;

}
.pricing-2-transform
{
    transform: scale(1.1);
}
.pricing-1 .small
{
        background: #a98093;
    padding: 9px;
    color: #fff!important;
    font-size: 15px;
}
.pricing-1 {
    background-color: #fff;
    border: 1px solid #f1f2f3;
    border-radius: 5px;
    text-align: center;
    -webkit-transition: .5s;
    transition: .5s;
    min-height: 728px;
}
.pricing-1 .price {
    font-size: 38px;
    font-weight: 600;
}
.font46{
  font-size: 38px;
    color: #5079ad;
}
.custom-min-height{
  min-height:296px;
}
.text-about {
  
  overflow:hidden;
  font-size:48px;
}
.section-header {
    text-align: center;
    max-width: 70%;
    margin: 0 auto 30px !important;
}
.text-about:first-of-type {    /* For increasing performance 
                       ID/Class should've been used. 
                       For a small demo 
                       it's okaish for now */
  animation: showup 6s infinite;
}

.text-about:last-of-type {
  width:0px;
  animation: reveal 7s infinite;
}

.text-about:last-of-type span {
  margin-left:-355px;
  animation: slidein 7s infinite;
}
.background--red {
    background-color: #5079ad;
}
.background {
    position: relative;
    padding: 30px 0;
}
.triangle--red:after {
    border-color: #5079ad transparent transparent transparent !important;
}
.bottom__triangle:after {
    border-color: #ffffff transparent transparent transparent;
    content: '';
    position: absolute;
    margin-top: 30px;
    transform: translateX(-50%);
    border-style: solid;
    border-width: 30px 75px 0 75px;
    margin-left: 50%;
    bottom: -30px;
    z-index: 100;
}
.h-fullscreen, .h-100vh {
    height: 80vh !important;
}
.custom_lead_p
{
    margin:10px 0px;
}
.btn_scroll_down {
    -webkit-animation: pulse 2s infinite;
    animation: pulse 2s infinite;
}
@-webkit-keyframes pulse {
  0% {
    -webkit-transform: translate(0, 0);
            transform: translate(0, 0); }
  50% {
    -webkit-transform: translate(0, 10px);
            transform: translate(0, 10px); }
  100% {
    -webkit-transform: translate(0, 0);
            transform: translate(0, 0); } }

@keyframes pulse {
  0% {
    -webkit-transform: translate(0, 0);
            transform: translate(0, 0); }
  50% {
    -webkit-transform: translate(0, 10px);
            transform: translate(0, 10px); }
  100% {
    -webkit-transform: translate(0, 0);
            transform: translate(0, 0); } }
}

@keyframes showup {
    0% {opacity:0;}
    20% {opacity:1;}
    80% {opacity:1;}
    100% {opacity:0;}
}

@keyframes slidein {
    0% { margin-left:-800px; }
    20% { margin-left:-800px; }
    35% { margin-left:0px; }
    100% { margin-left:0px; }
}

@keyframes reveal {
    0% {opacity:0;width:0px;}
    20% {opacity:1;width:0px;}
    30% {width:500px;}
    80% {opacity:1;}
    100% {opacity:0;width:500px;}
}
@media (max-width:767px){
  .h-fullscreen, .h-100vh {
    height: 64vh !important;
}
.font46 {
    font-size: 36px;
    line-height: 42px;
}
.custom_lead_p {
    font-size: 1.17188rem !important;
    line-height: 1.65;
}
.navbar-light .navbar-toggler, .navbar-stick-light.stick .navbar-toggler {
    color: black!important;
    z-index: 9999;
}
.navbar-expand-lg .nav-navbar:not(.nav-inline) {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    flex-direction: column;
    flex-wrap: nowrap;
    width: 100%;
    left: 40px;
}
.navbar-expand-lg .nav-navbar:not(.nav-inline)>.nav-item>.nav-link, .navbar-expand-lg .nav-navbar:not(.nav-inline)>.nav-link {
    min-height: inherit;
    /* line-height: inherit; */
    padding-top: 0.75rem;
    padding-bottom: 0.75rem;
    font-size: 0.95rem;
    text-transform: none;
    line-height: 33px;
    font-size: 19px;
}
}
.p-t-0{
    padding-top:0px!important;
}
}

.p-cut{
    line-height: 29px !important;
    font-size: 19px !important;
    color: #000;
    padding: 22px;
}
.text-muted p{
    line-height: 29px !important;
    font-size: 19px !important;
    color: #000;
    padding: 0px;
}
.btn_theme_light {
    background-color: #5479ad !important;
    color: #fff;
}
.text-blue{
    color:#5079ad;
}
.text-purple{
    color:#6e3951;
}
#site-footer {
    /* background-color: #34495d !important; */
    background-color: #5478ad !important;
}
.skw-pages {
  overflow: hidden;
  position: relative;
  height: 70vh;
}

.skw-page {linear-gradient(to bottom,#5079ad 0,#597bad 101%)
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
}
.skw-page__half {
  position: absolute;
  top: 0;
  width: 50%;
  height: 70vh;
  transition: -webkit-transform 1s;
  transition: transform 1s;
  transition: transform 1s, -webkit-transform 1s;
}
.skw-page__half--left {
  left: 0;
  -webkit-transform: translate3d(-32.4vh, 100%, 0);
          transform: translate3d(-32.4vh, 100%, 0);
}
.skw-page__half--right {
  left: 50%;
  -webkit-transform: translate3d(32.4vh, -100%, 0);
          transform: translate3d(32.4vh, -100%, 0);
}
.skw-page.active .skw-page__half {
  -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
}
.skw-page__skewed {
  overflow: hidden;
  position: absolute;
  top: 0;
  width: 140%;
  height: 100%;
  -webkit-transform: skewX(-18deg);
          transform: skewX(-18deg);
  background: #000;
}
.skw-page__half--left .skw-page__skewed {
  left: -40%;
}
.skw-page__half--right .skw-page__skewed {
  right: -40%;
}
.skw-page__content {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column wrap;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  padding: 0 30%;
  color: #fff;
  -webkit-transform: skewX(18deg);
          transform: skewX(18deg);
  transition: opacity 1s, -webkit-transform 1s;
  transition: transform 1s, opacity 1s;
  transition: transform 1s, opacity 1s, -webkit-transform 1s;
  background-size: cover;
}
.skw-page__half--left .skw-page__content {
  padding-left: 30%;
  padding-right: 30%;
  -webkit-transform-origin: 100% 0;
          transform-origin: 100% 0;
}
.skw-page__half--right .skw-page__content {
  padding-left: 30%;
  padding-right: 30%;
  -webkit-transform-origin: 0 100%;
          transform-origin: 0 100%;
}
.skw-page.inactive .skw-page__content {
  opacity: 0.5;
  -webkit-transform: skewX(18deg) scale(0.95);
          transform: skewX(18deg) scale(0.95);
}
.skw-page__heading {
  margin-bottom: 15px;
  text-transform: uppercase;
  font-size: 25px;
  text-align: center;
  color:#fff;
}
.skw-page__description {
  font-size: 18px;
  text-align: center;
}
.skw-page__link {
  color: #FFA0A0;
}
.skw-page-1 .skw-page__half--left .skw-page__content {
  background-image: url("<?php echo base_url(); ?>assets/img/slide_1.jpeg");
}
.skw-page-1 .skw-page__half--right .skw-page__content {
  background: #292929;
}
.skw-page-2 .skw-page__half--left .skw-page__content {
  background: #292929;
}
.skw-page-2 .skw-page__half--right .skw-page__content {
  background-image: url("<?php echo base_url(); ?>assets/img/slide1.jpeg");
}
.skw-page-3 .skw-page__half--left .skw-page__content {
  background-image: url("<?php echo base_url(); ?>assets/img/slide2.jpeg");
}
.skw-page-3 .skw-page__half--right .skw-page__content {
  background: #292929;
}
.skw-page-4 .skw-page__half--left .skw-page__content {
  background: #292929;
}
.skw-page-4 .skw-page__half--right .skw-page__content {
  background-image: url("<?php echo base_url(); ?>assets/img/slide3.jpeg");
}
.skw-page-5 .skw-page__half--left .skw-page__content {
  background-image: url("<?php echo base_url(); ?>assets/img/slide4.jpeg");
}
.skw-page-5 .skw-page__half--right .skw-page__content {
  background: #292929;
}

</style>

     
    <!--  <section class="section text-center" style="background-image: linear-gradient(135deg, #f9f7ff 0%, #fff 50%, #f6f3ff 100%);">
        <div class="container">

          <div class="row">
            <div class="col-md-6 mx-auto">
              <p><img src="<?php echo base_url(); ?>assets/img/icon/rocket2.png" alt="join us"></p>
              <br>
              <h3 class="mb-6"><strong>Join over 2,400 companies that trust us.</strong></h3>
              <p class="lead text-muted">Try it yourself for a while. Request a refund if you didn't find it useful and awesome as we advertised.</p>
              <br>
              <a class="btn btn-xl btn-round btn-info px-7" href="">Get a license</a>
            </div>
          </div>

        </div>
      </section>-->
<!--<section>
    <div class="skw-pages">
  <div class="skw-page skw-page-1 active">
    <div class="skw-page__half skw-page__half--left">
      <div class="skw-page__skewed">
        <div class="skw-page__content"></div>
      </div>
    </div>
    <div class="skw-page__half skw-page__half--right">
      <div class="skw-page__skewed">
        <div class="skw-page__content">
          <h2 class="skw-page__heading">Manage and grow your business</h2>
           <a class="btn btn-xl btn_round_border btn_theme_white w-200 px-6 m-b-5" data-toggle="modal" data-target="#user-register" href="javascript:;">Try for free</a>
           <a class="btn btn-xl btn_round_border btn_theme_white w-200 px-6 m-b-5" href="<?php echo base_url(); ?>home/plans">Explore Packages</a>
        </div>
      </div>
    </div>
  </div>
  <div class="skw-page skw-page-2">
    <div class="skw-page__half skw-page__half--left">
      <div class="skw-page__skewed">
        <div class="skw-page__content">
         <h2 class="skw-page__heading">Manage and grow your business</h2>
           <a class="btn btn-xl btn_round_border btn_theme_white w-200 px-6 m-b-5" data-toggle="modal" data-target="#user-register" href="javascript:;">Try for free</a>
           <a class="btn btn-xl btn_round_border btn_theme_white w-200 px-6 m-b-5" href="<?php echo base_url(); ?>home/plans">Explore Packages</a>
        </div>
      </div>
    </div>
    <div class="skw-page__half skw-page__half--right">
      <div class="skw-page__skewed">
        <div class="skw-page__content"></div>
      </div>
    </div>
  </div>
  <div class="skw-page skw-page-3">
    <div class="skw-page__half skw-page__half--left">
      <div class="skw-page__skewed">
        <div class="skw-page__content"></div>
      </div>
    </div>
    <div class="skw-page__half skw-page__half--right">
      <div class="skw-page__skewed">
        <div class="skw-page__content">
          <h2 class="skw-page__heading">Manage and grow your business</h2>
           <a class="btn btn-xl btn_round_border btn_theme_white w-200 px-6 m-b-5" data-toggle="modal" data-target="#user-register" href="javascript:;">Try for free</a>
           <a class="btn btn-xl btn_round_border btn_theme_white w-200 px-6 m-b-5" href="<?php echo base_url(); ?>home/plans">Explore Packages</a>
        </div>
      </div>
    </div>
  </div>
 

</div>
</section>-->
<!--<section class="background background--red triangle--red bottom__triangle">
<div class="container">
<div class="row ">
<div class="col-12">
<h2 style="color:#fff;">Explore Who We Are !</h2>
</div>
</div>
</div>
</section>-->
      
      <section class="section py-8" style="background:linear-gradient(to bottom,#5079ad 0,#597bad 101%)">
          <h1 style="text-align:center;color:#fff">Explore Who We Are !</h1>
        <div class="container" style="background-color:#fff;padding: 40px;">
       
           <div class="row"> 

            <div class="col-12 col-md-6 text-center " >
              <div class="text-about text_black  font600">Manage</div> 
              <div class="text-about text_black  font600 text-blue">Sell</div>
              <div class="text-about text_black  font600 text-purple">Grow</div>
<div class="text-about text_black  font600"> 
 
   <!--<span><sell style="color:#5079ad">sell</label><br/> <grow style="color:#6e3951">grow</label></span>-->
</div>
            </div>
        
         <div class="col-12 col-md-6 pb-70 align-self-center">
          
          <p class="lead-2 custom_lead_p text_black mt-20">Houdin-e is the only software that lets you easily and most efficiently manage your website, app and retail stores all under one software, which not only saves time but increases productivity and innovation.
If you have always wanted a dynamic shift in your business growth and hesitated due to technical complexities or budget factors, Houdin-e is just the right solution for you.
The world is going digital, allow us to help you to stay ahead.
 <br>
<b>Manage Easy, Sell More, Grow Higher.</b> </p>

        <!--<a class="btn btn-xl btn-round btn-second w-200 mr-3 px-6 d-none d-md-inline-block" href="" data-scrollto="section-intro">Get Started</a>-->

      


        </div>
          </div>
    

        </div>
      </section>
      

<section class="section text-white py-8 p-t-0">
<img src="assets/img/Pure White.jpg">
        
            </section>
           
            

		<!--<section class="section text-white py-8" style="background-image:linear-gradient(120deg, #244669 0%, #5079ad 100%);background-image:linear-gradient(120deg, #244669 0%, #5079ad 100%);">
        <div class="container">

          <div class="row"> 

            <div class="col-12 col-md-12 align-self-end text-center " >
               <div class="custom_all_circle">
    <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_left" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
        
        </a>
        <ul class="mfb-component__list" data-left>
          <li>
            <a href="#" data-mfb-label="View on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>
          <li>
            <a href="#" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>

          <li>
            <a href="#" data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>
          <li>
            <a href="#" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>

          <li>
            <a href="#" data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>
        </ul>
      </li>
      </ul>

      <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_right" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
       
        </a>
        <ul class="mfb-component__list" data-right>
          <li>
            <a href="#" data-mfb-label="View on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>
          <li>
            <a href="#" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>

          <li>
            <a href="#" data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>
          <li>
            <a href="#" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>

          <li>
            <a href="#" data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>
        </ul>
      </li>
      </ul>

      <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_bottom" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
       
        </a>
        <ul class="mfb-component__list" data-bottom>
          <li>
            <a href="#" data-mfb-label="View on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>
          <li>
            <a href="#" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>

          <li>
            <a href="#" data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>
          <li>
            <a href="#" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>

          <li>
            <a href="#" data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon "></i>
            </a>
          </li>
        </ul>
      </li>
      </ul>
     
      <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_center" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
      <a href="#" class="mfb-component__button--main custom_circle" style=""><span>houdin-e</span></a>
         
      </li>
      </ul>
    </div>
            </div>
			  
			   
          </div>

        </div>
      </section>-->

<!--<div class="clearfix"></div>
      <section class="section py-8"> 
        <div class="container">
          <div class="row"> 
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <div class="custom_all_circle">
    <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_left" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
        
        </a>
        <ul class="mfb-component__list" data-left>
          <li>
            <a href="https://github.com/nobitagit/material-floating-button/" data-mfb-label="View on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-github"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
        </ul>
      </li>
      </ul>

      <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_right" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
       
        </a>
        <ul class="mfb-component__list" data-right>
          <li>
            <a href="https://github.com/nobitagit/material-floating-button/" data-mfb-label="View on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-github"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
        </ul>
      </li>
      </ul>

      <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_bottom" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
       
        </a>
        <ul class="mfb-component__list" data-bottom>
          <li>
            <a href="https://github.com/nobitagit/material-floating-button/" data-mfb-label="View on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-github"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
        </ul>
      </li>
      </ul>
     
      <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_center" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
      <a href="#" class="mfb-component__button--main custom_circle" style=""><span>houdin-e</span></a>
         
      </li>
      </ul>
    </div>
            </div>
          </div>
        </div> 
      </section>    -->
      <div class="clearfix"></div> 




	  <section class="section py-8" style="background-image:url('<?php echo base_url(); ?>assets/img/Devil in Light Purple.jpg');background-size: contain;">
        <div class="container">
          <header class="section-header"> 
            <h2 class="font600 text_black font46">Choose your perfect plan</h2>
             
          </header> 


          <div class="row gap-y text-center">
              
            <?php 
            $i = 1;
            foreach($packageData as $packageDataList)
            {
              $getMainArray = $packageDataList;
              if($getMainArray->houdin_package_name == 'Tailored')
              {
              ?>
              <div class="col-md-<?php if(count($packageData) == 3) { echo "4"; } else if(count($packageData) == 4) { echo "3"; } ?>">
                  
              <div class="pricing-1 btn_scroll_down <?php if($i == 2){ echo "pricing-2-transform"; } ?>">
                <p class="plan-name"><?php echo $getMainArray->houdin_package_name ?></p>
                <br>
                <!-- <h2 class="price"> <span class="price-unit">&#8377;</span>
                  <span data-bind-radio="pricing-1"><?php //echo $getMainArray->houdin_package_price ?></span>
				  </h2>
               <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">Yearly</span>
                </p> -->
                <div class="custom-min-height">   
                <div class="text-muted">
                  <p class="mb-1"> <?php echo $getMainArray->houdin_package_description ?></p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                </div>
                </div>

                <br>
                <br>
                <br>
                <br>
                <br>
                 
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="<?php echo base_url() ?>home/contact">Contact Us</a></p>
              </div>
            </div>
              <?php }
              else
              {
              ?>
                <div class="col-md-<?php if(count($packageData) == 3) { echo "4"; } else if(count($packageData) == 4) { echo "3"; } ?>">
              <div class="pricing-1 btn_scroll_down <?php if($i == 2){ echo "pricing-2-transform"; } ?>">
                <p class="plan-name"><?php echo $getMainArray->houdin_package_name ?></p>
                <br>
                <h2 class="price"> <span class="price-unit">&#8377;</span>
                  <span data-bind-radio="pricing-1"><?php echo $getMainArray->houdin_package_price ?></span>
				  </h2>
               <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">Yearly</span>
                </p>
                <div class="custom-min-height">   
                <div class="text-muted">
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_product ?> Products</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_staff_accounts ?> Staff Accounts</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_stock_location ?> Stock Location</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_app_access ?> App Access</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_order_pm ?> Order Per Month</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_templates ?> Templates</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_sms ?> Free SMS</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_email ?> Free Email</p>
                  <?php if($getMainArray->houdin_package_accounting == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Accounting Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_pos == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> POS Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_delivery == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Delivery Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_payment_gateway == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Payment Gateway Integration</p><?php } ?>
                </div>
                </div>

               
                <?php 
                if($_COOKIE['tempUserPackageId'])
                {
                ?>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="<?php echo base_url() ?>Payumoney/<?php echo $getMainArray->houdin_package_id ?>/<?php echo $getMainArray->houdin_package_name ?>/<?php echo $getMainArray->houdin_package_price ?>">Upgrade Your Package</a></p>
                <?php }
                else
                {
                ?>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" data-toggle="modal" data-target="#user-register" href="javascript:;">Start Free Trial</a></p>
                <?php }
                ?>
              </div>
            </div>
              <?php }
            ?>
            <?php $i++; }  
            ?>
            


            <!-- <div class="col-md-4">
              <div class="pricing-1 popular">
                <p class="plan-name">Gold</p>
                <br>
                <h2 class="price text-second">
                  <span class="price-unit">$</span>
                  <span data-bind-radio="pricing-1">39</span>
                </h2>
                <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">monthly</span>
                </p>

                <div class="text-muted">
					
                 <p class="mb-1"><i class="ti-check text-second mr-2"></i> Enabled International Payments</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i> 50+ beautiful themes</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i> 25+ premium themes</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i>Payment enabled Facebook store</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i> Wholesale pricing. Wishlist</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i> 30-Day Onboarding support</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i>  Superfast website (CDN).</p>
					<p class="mb-1"><i class="ti-check text-second mr-2"></i> CRM, Marketing tools</p>
                  
                </div>

                <br>
                <p class="text-center py-3"><a class="btn btn-second btn-round" href="">Get started</a></p>
              </div>
            </div> -->


            <!-- <div class="col-md-4">
              <div class="pricing-1">
                <p class="plan-name">PLATINUM</p>
                <br>
                <h2 class="price">
                  <span class="price-unit">$</span>
                  <span data-bind-radio="pricing-1">69</span>
                </h2>
                <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">monthly</span>
                </p>

                <div class="text-muted">
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> Gold Plan with iOS app and Android app</p>
				<p class="mb-1"><i class="ti-check text-three mr-2"></i> Personal eCommerce consultant</p>
				<p class="mb-1"><i class="ti-check text-three mr-2"></i>Store setup cost included</p>
				<p class="mb-1"><i class="ti-check text-three mr-2"></i>  Coupon code promotions</p>
				<p class="mb-1"><i class="ti-check text-three mr-2"></i>   Digital marketing</p>
				<p class="mb-1"><i class="ti-check text-three mr-2"></i>  Integrated SSL</p> 
                </div>

                <br>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="">Get started</a></p>
              </div>
            </div> -->

          </div>


        </div>
      </section>




     
     
      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | CTA
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      
      <section class="section text-white py-8">
        <canvas class="overlay opacity-95" data-granim="#667eea,#764ba2,#00cdac,#3cba92"></canvas>

        <div class="container">
          <header class="section-header">
            <small><strong>Own it</strong></small>
            <h2 class="display-3 fw-400">Get It <span class="mark-underline">Now</span></h2>
            <hr>
            <p class="lead-2">If you have made your decision to purchase this template, go ahead and press on the following button to get a license in less than a minute.</p>
          </header>

          <p class="text-center">
            <a class="btn btn-xl btn-round btn-light w-250" href="">Purchase for <span class="pl-2">$19</span></a><br>
            <small><a class="text-white opacity-80" href="">or purchase an Extended License</a></small>
          </p>
        </div>
      </section>!-->

<?php  $this->load->view('Template/footer');?>
<script>
    $(document).ready(function() {

  var curPage = 1;
  var numOfPages = $(".skw-page").length;
  var animTime = 1000;
  var scrolling = false;
  var pgPrefix = ".skw-page-";

  function pagination() {
    scrolling = true;

    $(pgPrefix + curPage).removeClass("inactive").addClass("active");
    $(pgPrefix + (curPage - 1)).addClass("inactive");
    $(pgPrefix + (curPage + 1)).removeClass("active");

    setTimeout(function() {
      scrolling = false;
    }, animTime);
  };

  function navigateUp() {
    if (curPage === 1) return;
    curPage--;
    pagination();
  };

  function navigateDown() {
    if (curPage === numOfPages) return;
    curPage++;
    pagination();
  };

  $(document).on("mousewheel DOMMouseScroll", function(e) {
    if (scrolling) return;
    if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
      navigateUp();
    } else { 
      navigateDown();
    }
  });

  $(document).on("keydown", function(e) {
    if (scrolling) return;
    if (e.which === 38) {
      navigateUp();
    } else if (e.which === 40) {
      navigateDown();
    }
  });

});
</script>