<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contactform extends CI_Controller 
{
    function __construct()
    {
    	parent::__construct();
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        $this->load->model('Contactformmodel');
    	$this->load->helper('url');
    	$this->load->library('pagination');
        $this->perPage = 100;
        $this->load->helper('form');
        $this->from = fromemail();
        }
        public function index()
        {
            // send email
            if($this->input->post('sendemailbutton'))
            {
                $this->form_validation->set_rules('userEmail','text','required|valid_email');
                $this->form_validation->set_rules('emailsubject','subject','required');
                $this->form_validation->set_rules('emailmessage','message','required');
                if($this->form_validation->run() == true)
                {
                    $getsendgrid = sendgrid();
                    $url = 'https://api.sendgrid.com/';
                    $user = $getsendgrid[0]->houdin_sendgrid_username;
                    $pass = $getsendgrid[0]->houdin_sendgrid_password;
                    $json_string = array(
                    'to' => array(
                        $this->input->post('userEmail')
                    ),
                    'category' => 'test_category'
                    );
                    $htm = $this->input->post('emailmessage');
                    $params = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'x-smtpapi' => json_encode($json_string),
                        'to'        => $this->input->post('userEmail'),
                        'fromname'  => 'Houdin-e',
                        'subject'   => $this->input->post('emailsubject'),
                        'html'      => $htm,
                        'from'      => $this->from,
                    );
                    $request =  $url.'api/mail.send.json';
                    $session = curl_init($request);
                    curl_setopt ($session, CURLOPT_POST, true);
                    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                    curl_setopt($session, CURLOPT_HEADER, false);
                    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($session);
                    curl_close($session);
                    $getResponse = json_decode($response);
                    if($getResponse->message == 'success')
                    {
                        $this->session->set_flashdata('success','Email send successfully');
                        $this->load->helper('url');
                        redirect(base_url()."Contactform", 'refresh');
                    }
                    else
                    {
                        $this->session->set_flashdata('error','Something went wrong. Please try again');
                        $this->load->helper('url');
                        redirect(base_url()."Contactform", 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error',validation_errors());
                    redirect(base_url().'Contactform', 'refresh');   
                }
            }
            // Custom Pagination
            $getTotalCount = $this->Contactformmodel->countQueryListdata();
            $config['base_url']    = base_url().'Contactform/';
            $config['uri_segment'] = 2;
            $config['total_rows']  = $getTotalCount;
            $config['per_page']    = $this->perPage;
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['next_tag_open'] = '<li class="pg-next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li class="pg-prev">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page = $this->uri->segment(2);
            $offset = !$page?0:$page;
            // End Here
            $setArray = array('start'=>$offset,'limit'=>$this->perPage);
            $getResult = $this->Contactformmodel->getQueryList($setArray);
            $this->load->view('contactform',$getResult);
        }
}