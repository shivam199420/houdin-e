<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Cartmodel');
        $this->load->model('Shopproductlistviewmodel');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
     }
    public function index()
    {
        $cartData['AllCart'] =$this->Cartmodel->ShowCartData($this->session->userdata('userAuth'));
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/cart',$cartData);
    }
     public function AddToCart()
     {
         
        $item_id = $this->input->post('cart_item');
        $variantItem = $this->input->post('variantItem');
      $date = strtotime(date("Y-m-d, h:i:s"));
        $array = array("houdinv_users_cart_user_id"=>$this->session->userdata('userAuth'),
                       "houdinv_users_cart_item_id"=>$item_id,
                       "houdinv_users_cart_item_variant_id"=>$variantItem,
                       "houdinv_users_cart_item_count"=>1,
                       "houdinv_users_cart_add_date"=>$date,
                       "houdinv_users_cart_update_date"=>$date);
                       if($this->input->post('quantity'))
                       {
                        $quantity = $this->input->post('quantity');  
                       }
                       else
                       {
                        $quantity = 1;  
                       }
                 
        $wishData = $this->Cartmodel->AddToCartDB($array,$quantity);
        if($wishData)
        {
            $data = array("cart"=>$wishData,"count"=>count($wishData));
            echo json_encode($data);
        }
     }
    public function UpdateCart()
    {
        if($this->input->post('product'))
        {
            $items = $this->input->post('product');
            $CartUpdateData = $this->Cartmodel->UpdateCartDB($items);
            if(count($CartUpdateData) > 0)
            {
                $message_value = array();
                foreach($CartUpdateData as $esult)
                {
                    $product_name = $esult['title'];
                    $message_value[] = $product_name." have ".$esult['stock'].' item in stock can not update';
                }
                $this->session->set_flashdata("message_name",json_encode($message_value));
                redirect(base_url()."Cart", 'refresh');      
            }
            else
            {
                $this->session->set_flashdata("success","Cart updated");
                redirect(base_url()."Cart", 'refresh'); 
            }               
        } 
    }
     
     public function RemoveFromCart()
     {
        
           if($this->input->post('cart_item'))
           {
            $item_id = $this->input->post('cart_item');
            $CartData = $this->Cartmodel->RemoveFromCartDB($item_id);
            if($CartData)
            {
                     $data = array("cart"=>$CartData,"count"=>count($CartData));
                    echo json_encode($data);
            }
           }
        
     }
     
     
     

     
     
 }       