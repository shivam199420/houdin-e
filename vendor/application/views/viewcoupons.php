<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
  <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">View Coupon</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                 <li><a href="<?php echo base_url(); ?>coupons/Couponslanding">Coupons</a></li>
                  <li class="active">View Coupon</li>
                  </ol>
                  </div>
               
            </div>
           <!--END Page-Title -->  
            <div class="row m-t-20">
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="widget-panel widget-style-2 bg-white">
                                                <i class="md md-attach-money text-primary"></i>
                                                <h2 class="m-0 text-dark counter font-600">50568</h2>
                                                <div class="text-muted m-t-5">total Usage</div>
                                            </div>
                                        </div>
                                        
                                        <!--<div style="float:right;margin-right:10px">
                                          <button type="button" class="btn btn-info m-b-20">Generate PDF</button>
                                          <button type="button" class="btn btn-success m-b-20">Generate CSV</button>

                                        </div>-->
 
                                      </div>
                 
                                      
                                    
        <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">
             
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th><input type="checkbox"></th>
                    <th>Coupon name</th>
                    <th>Customer name</th>
                    <th>Product name</th>
                    <th>Date</th>
                    <th>Discount avail</th>
                    
                  </tr>
                  
                </thead>
                <tbody>
                  <tr>
                    <td><input type="checkbox"></td>
                    <td>Discount Coupon</td>
                    <td>John</td>
                    <td>Mobile</td> 
                    <td>DD/MM/YYYY</td>
                    <td>Yes</td>
                  </tr>
                 
                </tbody>
              </table>
            </div>
          </div>
        </div>


      </div>
    </div> 
  </div>



<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
