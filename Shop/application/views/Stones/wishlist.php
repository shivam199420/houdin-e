<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 
   <!-- Page item Area -->
   <div id="page_item_area">
     <div class="page-location">
         <div class="page-location-layer">
             <div class="container">
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="page-location-title">
                             <h3>Wishlist</h3>
                         </div>
                         <nav>
                             <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                 <li class="breadcrumb-item active" aria-current="page">Wishlist</li>
                             </ol>
                         </nav>
                     </div>
                 </div>
             </div>
         </div>
     </div>
   </div>

   <!-- Wishlist Page -->
     <div class="cart-area">
   <div class="wishlist-page cart-section">
     <div class="container">
       <div class="table-responsive col-md-12">
         <table class="table cart-table cart_prdct_table text-center">
           <thead>
             <tr class="cart-header porduct-place-order-btn">
               <th class="cpt_no">#</th>
               <th class="cpt_img">IMAGES</th>
               <th class="cpt_pn">PRODUCT TITLE</th>
               <th class="stock">STOCK STATUS</th>
               <th class="cpt_p">PRICE</th>
               <th class="add-cart">ADD TO CART</th>
               <th class="cpt_r">ACTION</th>
             </tr>
           </thead>
           <tbody>
           <?php

           // print_r($wishproducts);

                       $i=1;

                       foreach($wishproducts as $thisItem)
                       {
                         $image = json_Decode($thisItem->houdinv_products_main_images,true);
                         $stock = $thisItem->houdinv_products_total_stocks;
                         $price = json_DEcode($thisItem->houdin_products_price,true);
                         if($stock>0)
                         {
                           $status = "In stock";
                         }
                         else
                         {
                           $status = "Out of stock";
                         }
                       ?>
             <tr class="">
               <td ><span class="cart-number"><?php echo $i; ?></span></td>
               <td class="cart-img"><img style="max-width: 40%;" src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $image[0]; ?>" alt="" /></td>
               <td class="cart-product-title"><a href="javascript:;" class="cart-pro-title"><?php echo $thisItem->houdin_products_title; ?></a></td>
               <td class="cart-qty"><p class="stock in-stock"><?php echo $status; ?></p></td>
               <td class="cart-price"><p class="cart-pro-price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $price['sale_price']-$price['discount']; ?></p></td>
               <td><a   data-variant="0" data-cart="<?php echo $thisItem->houdin_products_id; ?>"  class="Add_to_cart_button cp_Wish_remove Addcard" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>">add to cart</a></td>
               <td><a class="cp_Wish_remove" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>"><i class="fa fa-trash text-danger"></i></a></td>
             </tr>
           <?php
                       $i++;
           }

           foreach($wishvarint as $wishvarints)
                       {
                         $image = $wishvarints->houdin_products_variants_image;
                         $stock = $wishvarints->houdinv_products_variants_total_stocks;
                         $price = $wishvarints->houdin_products_variants_prices;
                         if($stock>0)
                         {
                           $status = "In stock";
                         }
                         else
                         {
                           $status = "Out of stock";
                         }
                       ?>
             <tr class="">
               <td><span class="cart-number"><?php echo $i; ?></span></td>
               <td class="cart-img"><a href="#" class="cp_img"><img style="max-width: 40%;" src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $image; ?>" alt="" /></a></td>
               <td class="cart-product-title"><a href="#" class="cart-pro-title"><?php echo $wishvarints->houdin_products_variants_title; ?></a></td>
               <td class="cart-qty"><p class="stock in-stock"><?php echo $status; ?></p></td>
               <td class="cart-price"><p class="cart-pro-price"><?php echo $price; ?></p></td>
               <td><a  data-variant="<?php echo $wishvarints->houdinv_users_whishlist_item_variant_id; ?>" data-cart="0" class="Addcard Add_to_cart_button cp_Wish_remove" data-id="<?php echo $wishvarints->houdinv_users_whishlist_id; ?>">add to cart</a></td>
               <td><a class="cp_Wish_remove" data-id="<?php echo $wishvarints->houdinv_users_whishlist_id; ?>"><i class="fa fa-trash text-danger"></i></a></td>
             </tr>
           <?php
                       $i++;
                       } ?>

           </tbody>
         </table>
       </div>
     </div>
   </div>
 </div>
       <?php
       $getShopName = getfolderName($this->session->userdata('shopName'));
       $this->load->view(''.$getShopName.'/Template/footer'); ?>
