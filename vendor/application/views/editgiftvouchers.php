        <?php $this->load->view('Template/header') ?>
        <?php $this->load->view('Template/sidebar') ?>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">

        <!--START Page-Title -->
        <div class="row">
        
        <div class="col-md-8">
        <h4 class="page-title">Edit Gift Vouchers</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>giftvouchers">Gift Voucher</a></li>
        <li class="active">Edit Gift Vouchers</li>
        </ol>
        </div>
      
        </div>
        <!--END Page-Title -->

        <div class="row">
        <?php 
        if($this->session->flashdata('success'))
        {
        echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        if($this->session->flashdata('error'))
        {
        echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        ?>
        </div>
        <!--END Page-Title -->
        <div class="row m-t-20">
        <div class="col-lg-12">
        <div class="card-box">
        <?php echo form_open(base_url( 'Giftvouchers/edit/'.$this->uri->segment('3').'' ), array( 'id' => 'addGiftVoucherForm', 'method'=>'post' ));?>
        <div class="form-group">
        <label for="userName">Gift Voucher Name</label>
        <input type="text" value="<?php echo $voucherList[0]->houdinv_vouchers_name ?>" maxlength="50" name="voucherName" placeholder="Gift Voucher Name" class="form-control required_validation_for_add_gift_vouchers name_validation">
        </div>

        <div class="form-group">
        <label for="userName">Gift Voucher Valid From</label>
        <input type="text" value="<?php echo $voucherList[0]->houdinv_vouchers_valid_from ?>" name="voucherValidFrom" placeholder="Valid From" class="form-control date_picker required_validation_for_add_gift_vouchers name_validation">
        </div>
        <div class="form-group">
        <label for="userName">Gift Voucher Valid To</label>
        <input type="text" value="<?php echo $voucherList[0]->houdinv_vouchers_valid_to ?>" name="voucherValidTo" placeholder="Valid To" class="form-control date_picker required_validation_for_add_gift_vouchers name_validation">
        </div>
        <div class="form-group">
        <label for="userName">Gift Voucher Code</label>
        <label style="float: right;"><input type="checkbox" class="genearatePromoCodeCoupons">&nbsp;Generate Code</label>
        <input type="text" value="<?php echo $voucherList[0]->houdinv_vouchers_code ?>" maxlength="6" name="voucherCode" placeholder="Voucher Code" class="form-control setPromocodeCoupons required_validation_for_add_gift_vouchers name_validation">
        </div>
        <div class="form-group">
        <label for="userName">Gift Voucher Discount(%)</label>
        <input type="text"  value="<?php echo $voucherList[0]->houdinv_vouchers_discount ?>" name="voucherDiscount" placeholder="Discount" class="form-control required_validation_for_add_gift_vouchers name_validation number_validation">
        </div>
        <div class="form-group">
        <label for="userName">Choose Status</label>
        <select class="form-control required_validation_for_add_gift_vouchers" name="voucherStatus">
        <option value="">Choose Status</option>
        <option <?php if($voucherList[0]->houdinv_vouchers_status == 'active') { ?> selected="selected" <?php } ?> value="active">Active</option>
        <option <?php if($voucherList[0]->houdinv_vouchers_status == 'deactive') { ?> selected="selected" <?php } ?>  value="deactive">Deactive</option>
        </select>
        </div>
        <div class="form-group m-b-0">
        <input type="submit" value="Update" name="editGiftVoucher" class="btn btn-primary"/>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        </div>
        <?php $this->load->view('Template/footer') ?>
        <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#addGiftVoucherForm',function(){
                var check_required_field='';
                $(".required_validation_for_add_gift_vouchers").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>
