       <?php
       $getShopName = getfolderName($this->session->userdata('shopName'));
       $this->load->view(''.$getShopName.'/Template/header') ?>
       <?php
       $getCurrency = getShopCurrency();
       $currencysymbol=$getCurrency[0]->houdin_users_currency;
       ?>
       <?php
       if($currencysymbol=="USD")
       {
         $show_variable = "$";
       }else if($currencysymbol=="AUD"){
         $show_variable = "$";
       }else if($currencysymbol=="Euro"){
         $show_variable = "£";
       }else if($currencysymbol=="Pound"){
         $show_variable = "€";
       }else if($currencysymbol=="INR"){
         $show_variable = "₹";
       }
       ?>
       <?php
// $this->load->helper('setting');
$getData = stroeSetting($this->session->userdata('shopName'));
$cartFunction = $getData[0]->receieve_order_out;
?>

        <!-- Start Slider Area -->
        <?php
        if(count($sliderListData) > 0)
        {
        ?>
        <section id="slider_area" class="text-center">
        <div class="slider_active owl-carousel">
        <?php
        foreach($sliderListData as $sliderListDataList)
        {
          ?>
          <div class="single_slide" style="background-image: url(<?php echo $this->session->userdata('vendorURL') ?>upload/Slider/<?php echo $sliderListDataList->houdinv_custom_slider_image ?>); background-size: cover; background-position: center;">
        <div class="container">
        <div class="single-slide-item-table">
        <div class="single-slide-item-tablecell">
        <div class="slider_content text-left slider-animated-1">
        <!-- <p class="animated">New Year 2018</p> -->
        <h1 class="animated"><?php echo $sliderListDataList->houdinv_custom_slider_head ?></h1>
        <h4 class="animated"><?php echo $sliderListDataList->houdinv_custom_slider_sub_heading ?></h4>
        </div>
        </div>
        </div>
        </div>
        </div>
        <?php }
        ?>
        </div>
        </section>
        <?php }
        ?>
        <!-- End Slider Area -->
<?php

            if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
   // print_r($all_data);
    ?>
        <!--  Promo ITEM STRAT  -->
        <?php
        if(count($categoryData) > 0 && $customHome[0]->houdinv_custom_home_data_category)
        {
        ?>
        <section id="promo_area" class="section_padding">
        <div class="container">
        <div class="row">
        <div class="col-md-12 text-center">
        <div class="section_title">
        <h2><?php echo $customHome[0]->houdinv_custom_home_data_category; ?></h2>
        <div class="divider"></div>
        </div>
        </div>
        </div>
        <div class="row">
        <?php
        foreach($categoryData as $categoryDataList)
        {
          if($categoryDataList->houdinv_category_thumb)
          {
            $setImageData = $this->session->userdata('vendorURL')."images/category/".$categoryDataList->houdinv_category_thumb;
          }
          else
          {
            $setImageData = base_url()."Extra/noPhotoFound.png";
          }
        ?>
        <div class="col-xs-12 col-lg-3 col-md-3 col-sm-3">
        <a href="<?php echo base_url() ?>Productlist/<?php echo $categoryDataList->houdinv_category_id ?>">
        <div class="single_promo">
        <img class="image_w_h" src="<?php echo $setImageData; ?>" alt="">
        <div class="box-content">
        <h3 class="title"><?php echo $categoryDataList->houdinv_category_name ?></h3>
        <span class="post"><?php echo date("Y"); ?> Collection</span>
        </div>
        </div>
        </a>
        </div>
        <?php }
        ?>
        <!--  End Col -->


        <!--  End Col -->

        </div>
        </div>
        </section>
        <!--  Promo ITEM END -->
        <?php }
        ?>


        <!-- Start product Area -->
        <?php
        if($customHome[0]->houdinv_custom_home_data_latest_product && count($latestProducts) > 0)
        {
        ?>
        <section id="product_area" class="section_padding">
        <div class="container">
        <div class="row">
        <div class="col-md-12 text-center">
        <div class="section_title">
        <h2><?php echo $customHome[0]->houdinv_custom_home_data_latest_product; ?></h2>
        <div class="divider"></div>
        </div>
        </div>
        </div>
        <div class="text-center">
        <div class="product_item">
        <div class="row">
        <?php
        // print_r($latestProducts);
        foreach($latestProducts as $latestProductsList)
        {
          $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
          $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
        ?>
        <div class="col-lg-3 col-md-4 col-sm-6 mix sale">
        <div class="single_product">
        <div class="product_image">
        <?php 
        if($getProductImage[0])
        {
          $setProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
        }
        else
        {
          $setProductImage = base_url()."Extra/noPhotoFound.png";
        }
        ?>
        <img src="<?php echo $setProductImage ?>" alt="" style="width:253px!important;height:324px!important;"/>
        <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?><div class="new_badge">Out of Stock</div><?php } ?>

<?php
if($latestProductsList->houdinv_products_main_quotation == 1)
{
?>
    <div class="model_qute_show" data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal" style="position: absolute;display: inline-block;background: #33d286;color: #fff;z-index: 99;top: 15px;left: 8px;padding: 3px 12px;border-radius: 2px;cursor: pointer;">Ask Quotation
</div>
<?php
}
?>
        <div class="box-content">
        <a   class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" ><i class="fa fa-heart-o"></i></a>
        <?php

        if($latestProductsList->houdinv_products_total_stocks <= 0)
        {
          if($cartFunction == 1)
          {
            ?>
            <a   class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-cart-plus"></i></a>
         <?php  }
        }
        else
        {
          ?>
            <a   class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-cart-plus"></i></a>
        <?php }
        ?>
        <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-search"></i></a>
        </div>
        </div>

        <div class="product_btm_text">
        <h4><a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><?php echo substr($latestProductsList->houdin_products_title,0, 25).".." ; ?></a></h4>

        <span class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?> <?php  } ?></span>

        </div>
        </div>

        </div>
        <?php }
        ?>

        </div>
        </div>
        </div>
        </div>
        </section>
        <?php }
        ?>
        <!-- End product Area -->

        <!-- Special Offer Area -->
        <!-- End Special Offer Area -->

        <!-- Start Featured product Area -->
        <?php
        if($customHome[0]->houdinv_custom_home_data_featured_product)
        {
         ?>
          <section id="featured_product" class="featured_product_area section_padding">
        <div class="container">
        <div class="row">
        <div class="col-md-12 text-center">
        <div class="section_title">
        <h2><?php echo $customHome[0]->houdinv_custom_home_data_featured_product; ?></h2>
        <div class="divider"></div>
        </div>
        </div>
        </div>

        <div class="row text-center">
        <?php
        foreach($featuredProduct as $latestProductsList)
        {
          // print_r($latestProductsList);
          $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
          $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
        ?>
        <div class="col-lg-3 col-md-4 col-sm-6 sale">
        <div class="single_product">
        <div class="product_image">
        <?php 
        if($getProductImage[0])
        {
          $setFeaturedProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
        }
        else
        {
          $setFeaturedProductImage = base_url()."Extra/noPhotoFound.png";
        }
        ?>
        <img src="<?php echo $setFeaturedProductImage; ?>" alt="" style="width:253px!important;height:324px!important;"/>
        <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?><div class="new_badge">Out of Stock</div><?php } ?>
 <?php
if($latestProductsList->houdinv_products_main_quotation == 1)
{
?>
    <div class="model_qute_show" data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal" style="position: absolute;display: inline-block;background: #33d286;color: #fff;z-index: 99;top: 15px;left: 8px;padding: 3px 12px;border-radius: 2px;cursor: pointer;">Ask Quotation
</div>
<?php
}
?>
        <div class="box-content">
        <a   class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" ><i class="fa fa-heart-o"></i></a>
        <?php

        if($latestProductsList->houdinv_products_total_stocks <= 0)
        {
          if($cartFunction == 1)
          {
            ?>
            <a   class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-cart-plus"></i></a>
         <?php  }
        }
        else
        {
          ?>
            <a   class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-cart-plus"></i></a>
        <?php }
        ?>
        <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-search"></i></a>
        </div>
        </div>

        <div class="product_btm_text">
        <h4><a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><?php echo substr($latestProductsList->houdin_products_title,0, 25).".." ; ?></a></h4>

        <span class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo  $latestProductsList->houdin_products_final_price; ?> <?php  } ?></span>

        </div>
        </div>

        </div>
        <?php }
        ?>
       <!-- End Col -->
        </div>
        </div>
        </section>
        <?php }
        ?>
        <!-- End Featured Products Area -->

        <!-- Testimonials Area -->
        <?php if(count($storeTestimonial) > 0 && $customHome[0]->houdinv_custom_home_data_testimonial)
        {
        ?>
        <div class="container">
        <div class="row">
        <div class="col-md-12 text-center">
        <div class="section_title">
        <h2><?php echo $customHome[0]->houdinv_custom_home_data_testimonial; ?></h2>
        <div class="divider"></div>
        </div>
        </div>
        </div>
        </div>
         <section id="testimonials" class="testimonials_area section_padding" style="background: url(<?php echo base_url() ?>Extra/apparels/img/testimonial-bg.jpg); background-size: cover; background-attachment: fixed;">
        <div class="container">
        <div class="row">
        <div class="col-md-12">
        <div id="testimonial-slider" class="owl-carousel">
        <!-- start data -->
        <?php
        foreach($storeTestimonial as $storeTestimonialData)
        {
        ?>
        <div class="testimonial">
        <div class="pic">
        <img src="<?php if($storeTestimonialData->testimonials_image) { ?> <?php echo $this->session->userdata('vendorURL')."upload/testimonials/".$storeTestimonialData->testimonials_image; ?> <?php  } else { ?> <?php echo base_url() ?>Extra/apparels/img/testimonial/1.jpg <?php } ?>" alt="">
        </div>
        <div class="testimonial-content">
        <p class="description">
        <?php echo $storeTestimonialData->testimonials_feedback ?>
        </p>
        <h3 class="testimonial-title"><?php echo $storeTestimonialData->testimonials_name ?></h3>
        </div>
        </div>
        <?php }
        ?>

        </div>
        </div>
        </div>
        </div>
        </section>
        <?php  } ?>
        <!-- End STestimonials Area -->

      <?php
      $getShopName = getfolderName($this->session->userdata('shopName'));
      $this->load->view(''.$getShopName.'/Template/footer') ?>
