<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customermodel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
        $this->userTbl = '';
    }
    // add customer data
    public function addCustomerData($data)
    {
        $this->db->select('houdinv_user_email')->from('houdinv_users')->where('houdinv_user_email',$data['email']);
        $getEmailData = $this->db->get()->result();
        if(!count($getEmailData) > 0)
        {
            $getContact = $this->db->select('houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_contact',$data['mobile'])->get()->result();
            if(!count($getContact) > 0)
            {
                $letters1='abcdefghijklmnopqrstuvwxyz'; 
            $string1=''; 
            for($x=0; $x<3; ++$x)
            {  
                $string1.=$letters1[rand(0,25)].rand(0,9); 
            }
            $setCustomerPass = $string1;
            $saltdata = password_hash($string1,PASSWORD_DEFAULT);
            $pass = crypt($setCustomerPass,$saltdata);
            $setDate = strtotime(date('Y-m-d'));
            $setCustomerDataInsertArray = array('houdinv_user_name'=>$data['name'],'houdinv_user_email'=>$data['email'],'houdinv_user_password'=>$pass,'houdin_user_password_salt'=>$saltdata,'houdinv_user_contact'=>$data['mobile'],'houdinv_user_is_verified'=>'1','houdinv_user_is_active'=>$data['status'],'houdinv_user_created_at'=>$setDate);
            $this->db->insert('houdinv_users',$setCustomerDataInsertArray);
            $getUserId = $this->db->insert_id();
            if($getUserId != "")
            {
                if($data['address'])
                {
                    $setCustomerAddress = array('houdinv_user_address_user_id'=>$getUserId,'houdinv_user_address_contact_no'=>$data['customerAlternate'],'houdinv_user_address_street_line1'=>$data['address'],'houdinv_user_address_landmark'=>$data['landmark'],'houdinv_user_address_pincode'=>$data['pin'],'houdinv_user_address_city'=>$data['city'],'houdinv_user_address_state'=>$data['state'],'houdinv_user_address_country'=>$data['country'],'houdinv_user_address_created_at'=>$setDate);
                    $getAddressStatus = $this->db->insert('houdinv_user_addresses',$setCustomerAddress);
                    if($getAddressStatus == 1)
                    {
                        return array('message'=>'yes','email'=>$data['email'],'pass'=>$setCustomerPass);
                    }
                    else
                    {
                        return array('message'=>'no');    
                    }
                }
                else
                {
                    return array('message'=>'yes','email'=>$data['email'],'pass'=>$setCustomerPass);
                }
                
            }
            else
            {
                return array('message'=>'no');
            }
            }
            else
            {
                return array('message'=>'phone');
            }

        }
        else
        {
            return array('message'=>'email');
        }
    }
    // count customer data
    public function getCustomerTotalCount()
    {
        $this->db->select('COUNT(houdinv_user_id) AS TotalCustomerCount')->from('houdinv_users');
        $getCustomerCount = $this->db->get()->result();
        return array('totalRows'=>$getCustomerCount[0]->TotalCustomerCount);
    }
    
     // count customer privilage data
    public function getPrivilageCustomerTotalCount()
    {
        $this->db->select('COUNT(houdinv_user_id) AS TotalCustomerCount')->from('houdinv_users')
        ->where("houdinv_users_privilage >",0);
        $getCustomerCount = $this->db->get()->result();
        return array('totalRows'=>$getCustomerCount[0]->TotalCustomerCount);
    }
    
    
         // count customer pending data
    public function getCustomerPendingTotalCount()
    {
        $this->db->select('COUNT(houdinv_user_id) AS TotalCustomerCount')->from('houdinv_users')
        ->where("houdinv_users_pending_amount >",0);
        $getCustomerCount = $this->db->get()->result();
        return array('totalRows'=>$getCustomerCount[0]->TotalCustomerCount);
    }
    
    public function checkEmailRemaining()
    {
       $data =  $this->db->select("*")->from("houdinv_emailsms_stats")
        ->where("houdinv_emailsms_stats_type","email")
        ->get()->row();
        return $data;
    }
    
    public function emailDeduct()
    {
        $data =  $this->db->select("*")->from("houdinv_emailsms_stats")
        ->where("houdinv_emailsms_stats_type","email")
        ->get()->row();
     
        
        if(($data->houdinv_emailsms_stats_remaining_credits) > 0)
        {
            
        $update = array("houdinv_emailsms_stats_remaining_credits"=>($data->houdinv_emailsms_stats_remaining_credits)-1);
        $this->db->where("houdinv_emailsms_stats_type","email");
      $this->db->update("houdinv_emailsms_stats",$update); 
      } 
    }
    
    // fetch customer record
    public function fetchCustomerRecord($data = false)
    {
        // fetch customer data
        $this->db->select('*')->from('houdinv_users');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getCustomerdata = $this->db->get();
        $resultCustomerdata = ($getCustomerdata->num_rows() > 0)?$getCustomerdata->result():FALSE;
        // count total customer
        $this->db->select('COUNT(houdinv_user_id) AS totalCustomer')->from('houdinv_users');
        $getTotalCustomer = $this->db->get()->result();
        // count total active customer
        $this->db->select('COUNT(houdinv_user_id) AS totalActiveCustomer')->from('houdinv_users')->where('houdinv_user_is_active','active');
        $getTotalActiveCustomer = $this->db->get()->result();
        // count total deactivate customer
        $this->db->select('COUNT(houdinv_user_id) AS totalDeactiveCustomer')->from('houdinv_users')->where('houdinv_user_is_active','deactive');
        $getTotalDeactiveCustomer = $this->db->get()->result();

        // fetch privilage data
        $getCustomerSetting = $this->db->select('*')->from('houdinv_customersetting')->get()->result();
        return array('totalCustomer'=>$getTotalCustomer[0]->totalCustomer,'totalActiveCustomer'=>$getTotalActiveCustomer[0]->totalActiveCustomer,'totalDeactivateCustomer'=>$getTotalDeactiveCustomer[0]->totalDeactiveCustomer,'customerList'=>$resultCustomerdata,'customerSetting'=>$getCustomerSetting);
    }
    
    
     // fetch customer privilage record
    public function fetchPrivilageCustomerRecord($data = false)
    {
        // fetch customer data
        $this->db->select('*')->from('houdinv_users')
       ->where("houdinv_users_privilage >",0);
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getCustomerdata = $this->db->get();
        $resultCustomerdata = ($getCustomerdata->num_rows() > 0)?$getCustomerdata->result():FALSE;
        // count total customer
        $this->db->select('COUNT(houdinv_user_id) AS totalCustomer')->from('houdinv_users');
        $getTotalCustomer = $this->db->get()->result();
        // count total active customer
        $this->db->select('COUNT(houdinv_user_id) AS totalActiveCustomer')->from('houdinv_users')->where("houdinv_users_privilage >",0);
        $getTotalPrivilageCustomer = $this->db->get()->result();
        // fetch customer privilage setting
        $getCustomerSetting = $this->db->select('*')->from('houdinv_customersetting')->get()->result();
        return array('totalCustomer'=>$getTotalCustomer[0]->totalCustomer,'totalPrivilageCustomer'=>$getTotalPrivilageCustomer[0]->totalActiveCustomer,'customerList'=>$resultCustomerdata,'customerSettingData'=>$getCustomerSetting);
    }
    
    
     // fetch customer pending record
    public function fetchPendingCustomerRecord($data = false)
    {
        // fetch customer data
        $this->db->select('*')->from('houdinv_users')
        ->where("houdinv_users_pending_amount >",0);
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getCustomerdata = $this->db->get();
        $resultCustomerdata = ($getCustomerdata->num_rows() > 0)?$getCustomerdata->result():FALSE;
        // count total customer
        $this->db->select('COUNT(houdinv_user_id) AS totalCustomer')->from('houdinv_users')->where("houdinv_users_pending_amount !=",0);
        $getTotalCustomer = $this->db->get()->result();
        // count total active customer
        $this->db->select('COUNT(houdinv_user_id) AS totalActiveCustomer')->from('houdinv_users')
        ->where("houdinv_users_pending_amount >",0);
        $getTotalPendingCustomer = $this->db->get()->result();
        // Total pending amount
        $this->db->select('SUM(houdinv_users_pending_amount) AS totalPendingAmount')->from('houdinv_users');
        $getTotalPendingAmount = $this->db->get()->result();
       
        return array('totalCustomer'=>$getTotalCustomer[0]->totalCustomer,'totalPendingCustomer'=>$getTotalPendingCustomer[0]->totalActiveCustomer,'customerList'=>$resultCustomerdata,'totalPendingAmount'=>$getTotalPendingAmount[0]->totalPendingAmount);
    }
    
    // delete customer data
    public function deleteCustomerData($data)
    {
        $expldoeDeleteId = explode(',',$data);
        for($index = 0; $index < count($expldoeDeleteId); $index++)
        {
            $this->db->where('houdinv_user_id',$expldoeDeleteId[$index]);
            $getDeleteStatus = $this->db->delete('houdinv_users');   
        }
        return array('message'=>'yes');
    }
    // add customer tag
    public function addSingleCustomerTag($data)
    {
        $setDate = strtotime(date('Y-m-d'));
        $expldoeIdData = explode(',',$data['id']);
        $setArray = array();
        for($index = 0; $index < count($expldoeIdData); $index++)
        {
            $setInsertArray = array('houdinv_user_tag_user_id'=>$expldoeIdData[$index],'houdinv_user_tag_name'=>$data['tag'],'houdinv_user_tag_created_date'=>$setDate);
            $getInsertData = $this->db->insert('houdinv_user_tag',$setInsertArray);   
            if($getInsertData == 1)
            {
                array_push($setArray,'1');
            }
            else
            {
                array_push($setArray,'0');
            }
        }
        if(in_array('0',$setArray))
        {
            return array('message'=>'no');
        }
        else
        {
            return array('message'=>'yes');
        }
    }
    // update customer status
    public function updateCustomerStatus($data)
    {
        $setUpadteArray = array('houdinv_user_is_active'=>$data['status']);
        $this->db->where('houdinv_user_id',$data['id']);
        $getUpdateValue = $this->db->update('houdinv_users',$setUpadteArray);
        if($getUpdateValue == 1)
        {
          return array('message'=>'yes');  
        }
        else
        {
            return array('message'=>'no');
        }
    }
    
    
        // update payment pending
    public function updateCustomerPendingAmount($data)
    {
        $setUpadteArray = array('houdinv_users_pending_amount'=>$data['pending']);
        $this->db->where('houdinv_user_id',$data['id']);
        $getUpdateValue = $this->db->update('houdinv_users',$setUpadteArray);
        if($getUpdateValue == 1)
        {
          return array('message'=>'yes');  
        }
        else
        {
            return array('message'=>'no');
        }
    }
    
        // update customer privilage
    public function updateCustomerPrivilage($data)
    {
        $setUpadteArray = array('houdinv_users_privilage'=>$data['Privilage']);
        $this->db->where('houdinv_user_id',$data['id']);
        $getUpdateValue = $this->db->update('houdinv_users',$setUpadteArray);
        if($getUpdateValue == 1)
        {
           return array('message'=>'yes'); 
        }
        else
        {
            return array('message'=>'no');
        }
    }
    
    
    // advance search customer
    public function searchCustomerData($data)
    {
        $this->db->select('*')->from('houdinv_users');
        if($data['name']) { $this->db->where('houdinv_user_name',$data['name']); }
        if($data['contact']) { $this->db->or_where('houdinv_user_contact',$data['contact']); }
        if($data['email']) { $this->db->or_where('houdinv_user_email',$data['email']); }
        if($data['status']) { $this->db->or_where('houdinv_user_is_active',$data['status']); }
        $getSearchData = $this->db->get()->result();
        return $getSearchData;
    }
    // fetch customer data on view
    public function fetchCustomerDetails($data)
    {
        $this->db->select('houdinv_users.*,houdinv_user_addresses.*')->from('houdinv_users')->where('houdinv_users.houdinv_user_id',$data)->join('houdinv_user_addresses','houdinv_user_addresses.houdinv_user_address_user_id = houdinv_users.houdinv_user_id','left outer');
        $getCustomerData = $this->db->get()->result();
        $this->db->select('*')->from('houdinv_user_tag')->where('houdinv_user_tag_user_id',$data);
        $getCustomerTagData = $this->db->get()->result();
        //  fetch whole order
        $getFullOrder = $this->db->select('*')->from('houdinv_orders')->where('houdinv_order_user_id',$data)->get()->result();
        // count total order
        $getTotalOrder = $this->db->select('COUNT(houdinv_order_id) AS totalUserOrder')->from('houdinv_orders')->where('houdinv_order_user_id',$data)->get()->result();
        // count total completed order
        $getTotalCompletedOrder = $this->db->select('COUNT(houdinv_order_id) AS totalUserCompletedOrder')->from('houdinv_orders')->where('houdinv_order_user_id',$data)->where('houdinv_order_confirmation_status','Delivered')->get()->result();
        // count total cancel order
        $getTotalCancelOrder = $this->db->select('COUNT(houdinv_order_id) AS totalUserCancelOrder')->from('houdinv_orders')->where('houdinv_order_user_id',$data)->where('houdinv_order_confirmation_status','cancel')->get()->result();
        // fetch upcoming user order
        $getUpcomingUserOrder = $this->db->select('*')->from('houdinv_orders')->where('houdinv_order_user_id',$data)->where('houdinv_order_confirmation_status !=','Delivered')->where('houdinv_order_confirmation_status !=','cancel')->where('houdinv_order_confirmation_status !=','return')->get()->result();
        return array('customerData'=>$getCustomerData,'customerTagData'=>$getCustomerTagData,'totalOrder'=>$getFullOrder,'totalOrder'=>$getTotalOrder[0]->totalUserOrder,
        'totalCompletedOrder'=>$getTotalCompletedOrder[0]->totalUserCompletedOrder,'totalCancelOrder'=>$getTotalCancelOrder[0]->totalUserCancelOrder,'upcomingOrders'=>$getUpcomingUserOrder
        );
    }
    // add customer group
    public function addCustomerGroup($data)
    {
        $setTime = strtotime(date('Y-m-d'));
        $insertGroupArray = array('houdinv_users_group_name'=>$data['name'],'houdinv_users_group_status'=>$data['status'],'houdinv_users_group_created_at'=>$setTime);
        $getInsertStatus = $this->db->insert('houdinv_users_group',$insertGroupArray);
        if($getInsertStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // fetch number of group
    public function getCustomerGroupTotalCount()
    {
        $this->db->select('COUNT(houdinv_users_group_id) AS customerGroupCount')->from('houdinv_users_group');
        $getCustomerGroup = $this->db->get()->result();
        return array('totalRows'=>$getCustomerGroup[0]->customerGroupCount);
    }
    public function fetchCustomerGroupsData($data)
    {
        $this->db->select('houdinv_users_group.*')->from('houdinv_users_group');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getCustomerGroupdata = $this->db->get();
        $resultCustomerGroupdata = ($getCustomerGroupdata->num_rows() > 0)?$getCustomerGroupdata->result():FALSE;
        // fetch total group count
        $this->db->select('COUNT(houdinv_users_group_id) AS totalCustomerGroup')->from('houdinv_users_group');
        $getTotalCustomerGroup = $this->db->get()->result();
        // fetch total active group count
        $this->db->select('COUNT(houdinv_users_group_id) AS totalActiveCustomerGroup')->from('houdinv_users_group')->where('houdinv_users_group_status','active');
        $getTotalActiveCustomerGroup = $this->db->get()->result();
        // fetch total deactive group count
        $this->db->select('COUNT(houdinv_users_group_id) AS totalDeactiveCustomerGroup')->from('houdinv_users_group')->where('houdinv_users_group_status','deactive');
        $getTotalDeactiveCustomerGroup = $this->db->get()->result();
        return array('groupList'=>$resultCustomerGroupdata,'totalCustomer'=>$getTotalCustomerGroup[0]->totalCustomerGroup,'activeCustomer'=>$getTotalActiveCustomerGroup[0]->totalActiveCustomerGroup,'deactiveCustomer'=>$getTotalDeactiveCustomerGroup[0]->totalDeactiveCustomerGroup);
    }
    public function deleteCustomerGroup($data)
    {
        $this->db->where('houdinv_users_group_id',$data);
        $getDeleteStatus = $this->db->delete('houdinv_users_group');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function updateCustomerGroupData($data)
    {
        $setTime = strtotime(date('Y-m-d'));
        $updateGroupArray = array('houdinv_users_group_name'=>$data['name'],'houdinv_users_group_status'=>$data['status'],'houdinv_users_group_modified_at'=>$setTime);
        $this->db->where('houdinv_users_group_id',$data['id']);
        $getUpdateStatus = $this->db->update('houdinv_users_group',$updateGroupArray);
        if($getUpdateStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function getCustomerGroupUserTotalCount()
    {
        $this->db->select('COUNT(houdinv_users_group_users_list_id) AS totalCustomerInGroup')->from('houdinv_users_group_users_list');
        $getTotalGroupCustomer = $this->db->get()->result();
        return array('totalRows'=>$getTotalGroupCustomer[0]->totalCustomerInGroup);
    }
    public function fetchCustomerGroupData($data)
    {
        $this->db->select('houdinv_user_id,houdinv_user_name')->from('houdinv_users')->where('houdinv_user_is_active','active')->where('houdinv_user_id NOT IN (select houdinv_users_group_users_id from houdinv_users_group_users_list where houdinv_users_group__user_group_id='.$this->uri->segment('3').')');
        $getcustomers = $this->db->get()->result();
        //
        $this->db->select('houdinv_users_group_users_list.*,houdinv_users.houdinv_user_name')->from('houdinv_users_group_users_list')->where('houdinv_users_group_users_list.houdinv_users_group__user_group_id',$this->uri->segment('3'))->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_users_group_users_list.houdinv_users_group_users_id','left outer');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getCustomerGroupCustomerdata = $this->db->get();
        $resultCustomerGroupCustomerdata = ($getCustomerGroupCustomerdata->num_rows() > 0)?$getCustomerGroupCustomerdata->result():FALSE;
        // get total customer in group
        $this->db->select('COUNT(houdinv_users_group_users_list_id) AS totalCustomerGroup')->from('houdinv_users_group_users_list')->where('houdinv_users_group__user_group_id',$this->uri->segment('3'));
        $getTotalCountData = $this->db->get()->result();
        return array('customerList'=>$getcustomers,'GroupCustomerList'=>$resultCustomerGroupCustomerdata,'totalCount'=>$getTotalCountData[0]->totalCustomerGroup);
    }
    public function addCustomerGroupData($data)
    {
        $setData = strtotime(date('Y-m-d'));
        for($index = 0; $index < count($data); $index++)
        {
            $setInsertArray = array('houdinv_users_group__user_group_id'=>$this->uri->segment('3'),'houdinv_users_group_users_id'=>$data[$index],'houdinv_users_group_users_list_created_at'=>$setData);
            $this->db->insert('houdinv_users_group_users_list',$setInsertArray);
        }
        return array('message'=>'yes');
    }
    public function deleteGroupCustomerData($data)
    {
        $expldoeCustomerData = explode(",",$data);
        for($index = 0; $index < count($expldoeCustomerData); $index++)
        {
            $this->db->where('houdinv_users_group_users_list_id',$expldoeCustomerData[$index]);
            $this->db->delete('houdinv_users_group_users_list');
        }
        return array('message'=>'yes');
    }
    public function checkVendorEmailPackage()
    {
        $getEmailPackage = $this->db->select('houdinv_emailsms_stats_remaining_credits')->from('houdinv_emailsms_stats')->where('houdinv_emailsms_stats_type','email')->get()->result();
        return $getEmailPackage[0]->houdinv_emailsms_stats_remaining_credits;
    }
    public function updateVendorEmailPackage($data)
    {
        $updateEmailPackage = $data-1;
        $setData = strtotime(date('Y-m-d'));
        $this->db->where('houdinv_emailsms_stats_type',email);
        $this->db->update('houdinv_emailsms_stats',array('houdinv_emailsms_stats_remaining_credits'=>$updateEmailPackage));
        $setinsertArray = array('houdinv_email_log_credit_used'=>'1','houdinv_email_log_status'=>'1','houdinv_email_log_created_at'=>$setData);
        $this->db->insert('houdinv_email_log',$setinsertArray);
    }
    public function updateVendorEmaillog()
    {
        $setData = strtotime(date('Y-m-d'));
        $setinsertArray = array('houdinv_email_log_credit_used'=>'0','houdinv_email_log_status'=>'0','houdinv_email_log_created_at'=>$setData);
        $this->db->insert('houdinv_email_log',$setinsertArray);
    }
    public function getCustomerPdfData()
    {
        // total customer
        $getTotalCustomer = $this->db->select('COUNT(houdinv_user_id) AS totalCustomer')->from('houdinv_users')->get()->result();
        // total active customer
        $getTotalActiveCustomer = $this->db->select('COUNT(houdinv_user_id) AS totalActiveCustomer')->from('houdinv_users')->where('houdinv_user_is_active','active')->get()->result();
        // total deactive customer
        $getTotalDeactiveCustomer = $this->db->select('COUNT(houdinv_user_id) AS totalDeactiveCustomer')->from('houdinv_users')->where('houdinv_user_is_active','deactive')->get()->result();
        // get cuctomer list
        $getCustomerList = $this->db->select('*')->from('houdinv_users')->get()->result();
        return array('custoemrList'=>$getCustomerList,'totalCustomer'=>$getTotalCustomer[0]->totalCustomer,'totalActiveCustomer'=>$getTotalActiveCustomer[0]->totalActiveCustomer,'totalDeactiveCustomer'=>$getTotalDeactiveCustomer[0]->totalDeactiveCustomer);
    }
    }