<?php 
$this->load->view('Template/header.php');
$this->load->view('Template/sidebar.php');
?>
<div class="content-page">
<!-- Start content -->
<div class="content">
    <div class="container">
<!--START Page-Title -->
        <div class="row">
            <div class="col-md-8">
                <h4 class="page-title">Product</h4>
                <ol class="breadcrumb">
                <!-- <li><a href="<?php echo base_url(); ?>">Product/productLandingpage</a></li> -->

                <li class="active">Missing Category</li>
                </ol>
                </div>
        </div>
         
        <!--END Page-Title -->
        <div class="row">
          <div class="col-md-12">
          <?php 
           if ($this->session->flashdata('message_name')) {
                echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            }
          if ($this->session->flashdata('success')) {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
          ?>
            <div class="card-box table-responsive">
            <?php echo form_open(base_url().'Product/addmissingcategory',array("id"=>"add-category-form")); ?>
            <div class="pull-right">
            <input type="submit" class="btn btn-info " name="add_category" value="Add Category">
            </div>
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th style="width: 15%">Item</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                foreach ($all as $product) {
                    ?>
                    <tr>
                    <td>
                    <?php 
                    if ($image[0])  {
                    ?>
                    <img class="missing-category-image" src="<?php echo base_url(); ?>upload/productImage/<?php echo $image[0];  ?>"><br/><?php echo $product['houdin_products_title'] ?>
                    <?php } else {
                      ?>
                      <img class="missing-category-image" src="<?php echo base_url(); ?>images/no.png"/><br/><?php echo $product['houdin_products_title'] ?>
                     <?php }
                     ?>
                     </td>
                     <td class="add_category_row">
                     <!-- <button type="button" data-id="<?php //echo $product['houdin_products_id'] ?>" class="btn btn-default add_category_btn">Add Category</button> -->
                     <input type="hidden" class="product_id" name="product_id[]" value="<?php echo $product['houdin_products_id'] ?>" />
                     <input type="hidden" class="category_data" name="category_name[]"/>
                     <select class="form-control select2 category_select" style="width:100%" multiple="">
                      <option value="">Select Category</option>
                      <?php 
                      $category =  $new['category'];
                      foreach ($category as $values) {
                      ?>
                      <option value="<?php echo $values['main']->houdinv_category_id; ?>^^sub0"><?php echo $values['main']->houdinv_category_name; ?></option>
                      <?php
                      foreach ($values['sub'] as $sub1) {
                      ?>
                      <option style="margin-left:10px;" value="<?php echo $sub1['main']->houdinv_sub_category_one_id; ?>^^sub1">  - <?php echo $sub1['main']->houdinv_sub_category_one_name; ?></option>
                      <?php
                      foreach ($sub1['sub'] as $sub2) {
                          ?>
                      <option style="margin-left:20px;" value="<?php echo $sub2->houdinv_sub_category_two_id; ?>^^sub2">   -- <?php echo $sub2->houdinv_sub_category_two_name; ?></option>
                      <?php } } } ?>
                      </select>
                     </td>
                     </tr>
                <?php }
                ?>
                </tbody>
                <tbody>
                </tbody>
              </table>

              <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>


<!-- add category form -->
<div id="add-category-modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<?php echo form_open(base_url().'Product/addmissingcategory',array("id"=>"add-category-form")); ?>

<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

<h4 class="modal-title">Add Category</h4>

</div>

<div class="modal-body">
<div class="row">
<div class="col-md-12">

<div class="form-group no-margin">

<label for="field-7" class="control-label">Chosse Category</label>
<input type="hidden" name="product_id" id="product_id"/>
<select class="form-control select2" name="category_name[]" style="width:100%" multiple="">
<option value="">Select Category</option>
<?php 
$category =  $new['category'];
foreach ($category as $values) {
?>
<option value="<?php echo $values['main']->houdinv_category_id; ?>^^sub0"><?php echo $values['main']->houdinv_category_name; ?></option>
<?php
foreach ($values['sub'] as $sub1) {
?>
<option style="margin-left:10px;" value="<?php echo $sub1['main']->houdinv_sub_category_one_id; ?>^^sub1">  - <?php echo $sub1['main']->houdinv_sub_category_one_name; ?></option>
<?php
foreach ($sub1['sub'] as $sub2) {
    ?>
<option style="margin-left:20px;" value="<?php echo $sub2->houdinv_sub_category_two_id; ?>^^sub2">   -- <?php echo $sub2->houdinv_sub_category_two_name; ?></option>
<?php } } } ?>
</select>

<input type="hidden" name="status_id" id="status_id" />

</div>

</div>
</div>

</div>

<div class="modal-footer">

<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

<input type="submit" class="btn btn-info " name="add_category" value="Add Category">

</div>

</div>
<?php echo form_close(); ?>
</div>

</div>
<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom/page-js/missing-product/missing-category.js"></script>
<script>
$(".select2").select2();
</script>
<script>
$(document).on('click','.add_category_btn',function(){
    $('#product_id').val($(this).attr('data-id'));
    $('#add-category-modal').modal('show');
});
</script>