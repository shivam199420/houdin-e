<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>


   <!-- About Banner Start -->

   <section id="about">

       <div class="container">

           <div class="row">

               <div class="about-heading text-center">

                   <h2>Change Password</h2>

               </div>

           </div>

       </div>

   </section>

<!-- About Banner End -->



<!-- my account part Start -->

<section id="account">

    <div class="container">

        <div class="row">
          <?php
          if($this->session->flashdata('error'))
          {
              echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
          }
          else if($this->session->flashdata('success'))
          {
              echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
          }
          ?>
            <div class="col-md-3">

            </div>

            <div class="col-md-6">

                <div class="login-account">
                    <h3>Change password</h3>
                    <p>Fillbellow form change password.</p>
                    <hr>
                      <?php echo form_open(base_url( 'Changepassword/updatepass' ), array( 'id' => 'changepass', 'method'=>'post' ));?>

                    <div class="form-group">

                      <input type="password" name="password" id="password" placeholder="Enter Current Password" class="form-control required_validation_for_change_pass name_validation form-control" />

                    </div>

                    <div class="form-group">

                      <input type="password" class="required_validation_for_change_pass name_validation opass form-control" id="newpass" name="newpass" placeholder="Enter new password" required>

                    </div>

                    <div class="form-group">

                      <input type="password" name="newconfirmpass" class="required_validation_for_change_pass name_validation cpass form-control" />

                    </div>



                   <div class="procd">

                        <input type="submit" class="btn btn-primary acc_btn text-center setDisableData" value="update Password"/>

                    </div>

        <?php echo form_close(); ?>

                </div>

            </div>



        </div>

    </div>

</section>

<!-- my account part End -->
<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/footer'); ?>

<script type="text/javascript">
$(document).ready(function(){
    $(document).on('submit','#changepass',function(){
        var check_required_field='';
        $(this).find(".required_validation_for_change_pass").each(function(){
            var val22 = $(this).val();
            if (!val22){
                check_required_field =$(this).size();
                $(this).css("border-color","#ccc");
                $(this).css("border-color","red");
            }
            $(this).on('keypress change',function(){
                $(this).css("border-color","#ccc");
            });
        });
        if(check_required_field)
        {
            return false;
        }
        else {
            return true;
        }
    });
});
</script>
