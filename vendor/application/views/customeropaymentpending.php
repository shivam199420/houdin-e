﻿<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">
             
                <div class="col-md-8">
                   <h4 class="page-title">Payment pending</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                 <li><a href="<?php echo base_url(); ?>customer/Customerlanding">Customer Management</a></li>
                  <li class="active">Payment pending</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                  
            </div>
           <!--END Page-Title --> 
          
            <div class="row m-t-20">

               <div class="col-lg-4 col-sm-6">
                                            <div class="widget-panel widget-style-2 bg-white">
                                                <img src="<?php echo base_url(); ?>assets/images/allpageicon/Customer.png">
                                                <h2 class="m-0 text-dark counter font-600"><?php echo $totalCustomer; ?></h2>
                                                <div class="text-muted m-t-5">Total customers</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="widget-panel widget-style-2 bg-white">
                                                <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total)Payment.png">
                                                <h2 class="m-0 text-dark counter font-600"><?php echo $totalPendingAmount; ?></h2>
                                                <div class="text-muted m-t-5">Total payments</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="widget-panel widget-style-2 bg-white">
                                               <img src="<?php echo base_url(); ?>assets/images/allpageicon/Pending_Payment.png">
                                                <h2 class="m-0 text-dark counter font-600"><?php echo $totalPendingCustomer; ?></h2>
                                                <div class="text-muted m-t-5">Pending payments</div>
                                            </div>
                                        </div>
                                       
                                        
                                      </div>

                                      

          
                                      
         
              <!-- show messages -->
        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>

                                      

                                      
                                    
        <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">
              <div class="btn-group pull-right m-t-10 m-b-20">
              <!--<button type="button" class="btn btn-default m-r-5" title="Delete" data-toggle="modal" data-target="#delete_customer"><i class="fa fa-trash"></i></button>
              <button type="button" class="btn btn-default m-r-5" data-toggle="modal" data-target="#con-close-modal" title="Add customer"><i class="fa fa-plus"></i></button>-->
                <a href="<?php echo base_url(); ?>Customer/customerpaymentPending" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
                 <!-- <button type="button" class="btn btn-default m-r-5" title="Export To CSV"><i class="fa fa-file"></i></button>
                 <button type="button" class="btn btn-default m-r-5" data-toggle="modal" data-target="#con-close-modal" title="Add tag"><i class="fa fa-plus"></i></button>-->

              </div>
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th><input type="checkbox"></th>
                    <th>Customer</th>
                    <th>Credit contact</th>
                    <th>Pending amount</th>
                   <!--<th>Pending since</th>-->
                   <th>Action</th>
                     
                  </tr>
                  
                </thead>
                <tbody>
                
                 <?php
                  foreach($customerList as $customerListData)
                                {
                ?>
                
                  <tr>
                                       <td><input data-email="<?php echo $customerListData->houdinv_user_email ?>" data-id="<?php echo $customerListData->houdinv_user_id ?>" type="checkbox"></td>
                    <td><a href="<?php echo base_url(); ?>Customer/view/<?php echo $customerListData->houdinv_user_id ?>"><?php echo $customerListData->houdinv_user_name ?></a></td>
                     <td><?php echo $customerListData->houdinv_user_contact ?></td>
                    <td><?php echo $customerListData->houdinv_users_pending_amount; ?></td> 
                    <!--<td>14 days</td>-->
                   <td>
                   <button class="btn btn-success waves-effect waves-light notify_model"  data-name="<?php echo $customerListData->houdinv_user_name ?>" data-email="<?php echo $customerListData->houdinv_user_email ?>" data-toggle="modal" data-target="#notify">Notify</button>
                         <button class="btn btn-success waves-effect waves-light changeCustomerPendingAmountBtn" data-pending="<?php echo $customerListData->houdinv_users_pending_amount; ?>" data-id="<?php echo $customerListData->houdinv_user_id ?>">Change Pending amount</button>

                    </td>
                  </tr>
                 <?php }
                                ?>  
                </tbody>
              </table>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>


        <div id="notify" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog"> 
          
    <?php echo form_open(base_url('Customer/pendingAmountNotify' ), array( 'method'=>'post', 'id'=>'pendingAmountNotify' ));?>
               <div class="modal-content"> 
                  <div class="modal-header"> 
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                      <h4 class="modal-title">Notify</h4> 
                  </div> 
                  <div class="modal-body"> 
                      
                      <div class="row"> 
                          <div class="col-md-12"> 
                              <div class="form-group">
                                  <input type="text" class="form-control required_validation_for_add_single_tag" name="notify" id="field-3" placeholder="Notify"> 
                                   <input type="hidden" class="form-control" name="hidden_email" id="field-4"> 
                                     <input type="hidden" class="form-control" name="name" id="field-5"> 
                              </div> 
                          </div> 
                      </div> 
                      
                  </div> 
                  <div class="modal-footer"> 
                      <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                      <button type="submit" name="pendingAmountNotifyname" value="sub" class="btn btn-info waves-effect waves-light">Notify</button> 
                  </div> 
              </div> 
                   <?php echo form_close(); ?>
          </div>
      </div><!-- /.modal -->
      
      
      <!---Start Change Pending amount -->

        <div id="changeCustomerPendingModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <?php echo form_open(base_url( 'Customer/changePendingAmountPage' ), array( 'method'=>'post', 'id'=>'changePendingModalData' ));?>
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Change Pending amount</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group"> 
        <input type="hidden" class="changeCustomerPendingId" name="changeCustomerPendingId"/>
        <label for="field-1" class="control-label">Choose Pending amount</label> 
        <input class="form-control required_validation_for_add_single_tag choosePendingAmount number_validation" name="choosePendingAmount"/>
      
        </div> 
        </div> 
        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <input type="submit" class="btn btn-info" value="Change Pending amount" name="updateCustomerPendingAmount" />
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- End privilage -->

<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
        <script type="text/javascript">
        $(document).ready(function(){
                $(document).on('submit','#changePendingAmount','#pendingAmountNotify',function(c){
                        var rep_image_val='';
                        $(this).find(".required_validation_for_add_single_tag").each(function()
                        {
                                var val22 = jQuery(this).val();
                                if (!val22)
                                {
                                        rep_image_val = 'error form';
                                        $(this).css("border-color","red");
                                }
                        });
                        $(this).find('.required_validation_for_add_single_tag').on('keyup blur',function()
                        {
                                $(this).css("border-color","#ccc");
                        });
                        if(rep_image_val)
                        {
                                c.preventDefault();
                                return false;
                        }
                        else
                        {
                                return true;
                        }        
                });
        })
        </script>