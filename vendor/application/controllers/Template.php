<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Template extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->auth_users->is_logged_in();
        $this->load->model('Templatemodel');
        $this->validation = array(
                                   array("field"=>"subject",
                                         "label"=>"Subject",
                                         "rules"=>"required"
                                         ),  
                                   array("field"=>"message",
                                         "label"=>"Message",
                                         "rules"=>"required"
                                        )  
                                  );
    }
    
     public function Templatelanding()
    {
        $this->load->view('Templatelandingpage');
    }
    
    
    public function sms()
    {
        
        if($this->input->post('SmsTemplateSave'))
        {
           $this->form_validation->set_rules($this->validation); 
           if($this->form_validation->run() != true)
           {
            $this->session->set_flashdata("message_name",validation_errors());
             redirect(base_url().'Template/sms', 'refresh');  
           }
           $date = strtotime(date("Y-m-d"));
           $data_array = array("houdinv_sms_template_type"=>$this->input->post('SmsTemplateSave'),
                       "houdinv_sms_template_subject"=>$this->input->post('subject'),
                       "houdinv_sms_template_message"=>$this->input->post('message'),
                       "houdinv_sms_template_updated_date"=>$date
                       );
                       
           $output = $this->Templatemodel->SmsTemplateSave($data_array);  
           
           if($output)
           {
           $this->session->set_flashdata("success","Templat saved successfully");
             redirect(base_url().'Template/sms', 'refresh');   
           }          
           
           
        }
        $view['smsDataRegister'] = $this->Templatemodel->FetchAllSmsTemplate('register'); 
        $view['smsDataForget'] = $this->Templatemodel->FetchAllSmsTemplate('forget_password');  
        $view['smsDataOnOrder'] = $this->Templatemodel->FetchAllSmsTemplate('On_order'); 
        $view['smsDataOnShip'] = $this->Templatemodel->FetchAllSmsTemplate('On_ship'); 
        $view['smsDataOnOrderComplete'] = $this->Templatemodel->FetchAllSmsTemplate('On_order_complete');
         $view['smsDataOnCoupon'] = $this->Templatemodel->FetchAllSmsTemplate('On_coupon');
           $view['smsDataOnGift'] = $this->Templatemodel->FetchAllSmsTemplate('On_gift');    
        $this->load->view('sms',$view);
    }
    public function email()
    {
          if($this->input->post('EmailTemplateSave'))
        {
           $this->form_validation->set_rules($this->validation); 
           if($this->form_validation->run() != true)
           {
            $this->session->set_flashdata("message_name",validation_errors());
             redirect(base_url().'Template/email', 'refresh');  
           }
          $date = strtotime(date("Y-m-d"));
           $data_array = array("houdinv_email_template_type"=>$this->input->post('EmailTemplateSave'),
                       "houdinv_email_template_subject"=>$this->input->post('subject'),
                       "houdinv_email_template_message"=>$this->input->post('message'),
                       "houdinv_email_template_updated_date"=>$date
                       );
                       
           $output = $this->Templatemodel->EmailTemplateSave($data_array);  
           
           if($output)
           {
           $this->session->set_flashdata("success","Templat saved successfully");
             redirect(base_url().'Template/email', 'refresh');   
           }          
           
           
        }
        $view['emailDataRegister'] = $this->Templatemodel->FetchAllEmailTemplate('register'); 
        $view['emailDataForget'] = $this->Templatemodel->FetchAllEmailTemplate('forget_password');  
        $view['emailDataOnOrder'] = $this->Templatemodel->FetchAllEmailTemplate('On_order'); 
        $view['emailDataOnShip'] = $this->Templatemodel->FetchAllEmailTemplate('On_ship'); 
        $view['emailDataOnOrderComplete'] = $this->Templatemodel->FetchAllEmailTemplate('On_order_complete');
         $view['emailDataOnCoupon'] = $this->Templatemodel->FetchAllEmailTemplate('On_coupon');
           $view['emailDataOnGift'] = $this->Templatemodel->FetchAllEmailTemplate('On_gift'); 
        $this->load->view('email',$view);
    }
    public function push()
    {
          if($this->input->post('PushlTemplateSave'))
        {
           $this->form_validation->set_rules($this->validation); 
           if($this->form_validation->run() != true)
           {
            $this->session->set_flashdata("message_name",validation_errors());
             redirect(base_url().'Template/push', 'refresh');  
           }
           $date = strtotime(date("Y-m-d"));
           $data_array = array("houdinv_push_Template_type"=>$this->input->post('PushlTemplateSave'),
                       "houdinv_push_template_subject"=>$this->input->post('subject'),
                       "houdinv_push_template_message"=>$this->input->post('message'),
                       "houdinv_push_template_updated_date"=>$date
                       );
                       
           $output = $this->Templatemodel->PushTemplateSave($data_array);  
           
           if($output)
           {
           $this->session->set_flashdata("success","Templat saved successfully");
             redirect(base_url().'Template/push', 'refresh');   
           }          
           
           
        }
        $view['pushDataRegister'] = $this->Templatemodel->FetchAllPushTemplate('register'); 
        $view['pushDataForget'] = $this->Templatemodel->FetchAllPushTemplate('forget_password');  
        $view['pushDataOnOrder'] = $this->Templatemodel->FetchAllPushTemplate('On_order'); 
        $view['pushDataOnShip'] = $this->Templatemodel->FetchAllPushTemplate('On_ship'); 
        $view['pushDataOnOrderComplete'] = $this->Templatemodel->FetchAllPushTemplate('On_order_complete');
         $view['pushDataOnCoupon'] = $this->Templatemodel->FetchAllPushTemplate('On_coupon');
           $view['pushDataOnGift'] = $this->Templatemodel->FetchAllPushTemplate('On_gift'); 
        $this->load->view('push',$view);
    }
}
