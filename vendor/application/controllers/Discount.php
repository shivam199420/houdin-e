<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Discount extends CI_Controller 
{
    function __construct()
	{
        parent::__construct();
        $this->auth_users->is_logged_in();
        $this->load->helper('form');
        $this->load->library('Pdf');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->perPage = 100;
       $this->load->model('Discountmodel');
       $this->load->model('Campaignmodel');
    }
     public function Discountlanding()
    {
        $this->load->view('discountlandingpage');
    } 
    
    public function storeDiscount()
    {
    	 $GEtcate=$this->Discountmodel->fetchstore_category(array('customer_id'=>0));

        $this->load->view('storediscount',$GEtcate);
    }
 public function Get_products($value='')
 {
 	 $postData_cate = $this->input->post('discountcategory'); 
 
 	  $option_set='<option value="">Choose Products</option>';

 	if(count($postData_cate)>0){
 		$GEtproduct=$this->Discountmodel->fetchstore_products($postData_cate);
 	  if($GEtproduct){
      
  foreach ($GEtproduct as $key => $GEtproducts) {


  $option_set.='<option value="'.$GEtproducts->houdin_products_id.'">'.$GEtproducts->houdin_products_title.'</option>';
   
  }

 	  } 
 	
 	}

 echo $option_set; 
 }

 public function adddiscount()
 {
 	if($this->input->post()){
 		 
 		 
 	//is_unique check the unique email value in users table
         $this->form_validation->set_rules('discountname', 'Discount Name', 'trim|required');
        $this->form_validation->set_rules('discountvalue', 'Discount Value', 'trim|required|numeric');
        $this->form_validation->set_rules('discountstarted', 'Discount Start date', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="discount";         
                $this->load->view('models_',$reg_pup);            
        }else{ 

             $postData_shipping = $this->input->post(); 

                 $discountcategory= $postData_shipping['discountcategory'];  
                  $discountproducts= $postData_shipping['discountproducts'];                
                  $discountname= $postData_shipping['discountname'];
                  $discountvalue= $postData_shipping['discountvalue'];  
                  $discount_start= strtotime($postData_shipping['discountstarted']);                
                  $discount_end= strtotime($postData_shipping['discountend']);
                  $discount_status= $postData_shipping['discount_status'];
 

     
           
              $insert_data=array('product_cat_id'=>json_encode($discountcategory),'product_id' =>json_encode($discountproducts),'discount_name'=>$discountname,'discount_amount'=>$discountvalue,'discount_start'=>$discount_start,'discount_end'=>$discount_end,'discount_status'=>$discount_status,'createdate'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Discountmodel->save_storediscount($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Discount Successfully Save');        
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
            }
            }

        }else{
        	  $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
        	   redirect(base_url()."Discount/storeDiscount");  
        }
  
 }

public function deletediscount()
{ if($this->input->post()){
	$postData_delete = $this->input->post('deletesdescount'); 

	 
 $file_id = $this->Discountmodel->delete_storediscount($postData_delete);
            if($file_id)
            {$this->session->set_flashdata('success','Discount Successfully Delete');        
             
              redirect("Discount/storeDiscount");
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
                 redirect("Discount/storeDiscount");
            }
                
 		 }
	 
}
public function GetEdit_products()
{ if($this->input->post('edit_data')){
	 
	$edit_id = $this->input->post('edit_data'); 
 $file_id = $this->Discountmodel->editfatch_storediscount($edit_id);
  
   echo   json_encode($file_id);

 
	}  
}



 public function editdiscount_frm()
 {
 	if($this->input->post()){
 		 
 		 
 	//is_unique check the unique email value in users table
         $this->form_validation->set_rules('discountname', 'Discount Name', 'trim|required');
        $this->form_validation->set_rules('discountvalue', 'Discount Value', 'trim|required|numeric');
        $this->form_validation->set_rules('discountstarted', 'Discount Start date', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="discountedit";         
                $this->load->view('models_',$reg_pup);            
        }else{ 

             $postData_shipping = $this->input->post(); 

                 $discountcategory= $postData_shipping['discountcategory'];  
                  $discountproducts= $postData_shipping['discountproducts'];                
                  $discountname= $postData_shipping['discountname'];
                  $discountvalue= $postData_shipping['discountvalue'];  
                  $discount_start= strtotime($postData_shipping['discountstarted']);                
                  $discount_end= strtotime($postData_shipping['discountend']);
                  $discount_status= $postData_shipping['discount_status'];
 
                  $discount_id= $postData_shipping['discount_id'];
     
           
              $insert_data=array('discount_id'=>$discount_id,'product_cat_id'=>json_encode($discountcategory),'product_id' =>json_encode($discountproducts),'discount_name'=>$discountname,'discount_amount'=>$discountvalue,'discount_start'=>$discount_start,'discount_end'=>$discount_end,'discount_status'=>$discount_status,'createdate'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Discountmodel->edit_storediscount($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Discount Successfully update');        
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
            }
            }

        }else{
        	  $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
        	   redirect(base_url()."Discount/storeDiscount");  
        }
  
 }



// END Store Discount 





    public function customerDiscount()
    {
    	$GEtcate=$this->Discountmodel->fetchstore_category(array('customer_id !='=>'0'));

        $this->load->view('customerdiscount',$GEtcate);
    }

public function adddiscount_cus()
{
	if($this->input->post()){
 		 
 		 
 	//is_unique check the unique email value in users table
         $this->form_validation->set_rules('discountname', 'Discount Name', 'trim|required');
        $this->form_validation->set_rules('discountvalue', 'Discount Value', 'trim|required|numeric');
        $this->form_validation->set_rules('discountstarted', 'Discount Start date', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="discount";         
                $this->load->view('models_',$reg_pup);            
        }else{ 

             $postData_shipping = $this->input->post(); 
             $customer_id= $postData_shipping['discountcustomer'];
                 $discountcategory= $postData_shipping['discountcategory'];  
                  $discountproducts= $postData_shipping['discountproducts'];                
                  $discountname= $postData_shipping['discountname'];
                  $discountvalue= $postData_shipping['discountvalue'];  
                  $discount_start= strtotime($postData_shipping['discountstarted']);                
                  $discount_end= strtotime($postData_shipping['discountend']);
                  $discount_status= $postData_shipping['discount_status'];
 

     
           
              $insert_data=array('customer_id'=>json_encode($customer_id),'product_cat_id'=>json_encode($discountcategory),'product_id' =>json_encode($discountproducts),'discount_name'=>$discountname,'discount_amount'=>$discountvalue,'discount_start'=>$discount_start,'discount_end'=>$discount_end,'discount_status'=>$discount_status,'createdate'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Discountmodel->save_storediscount($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Discount Successfully Save');        
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
            }
            }

        }else{
        	  $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
        	   redirect(base_url()."Discount/storeDiscount");  
        } 
}


public function editdiscount_cus($value='')
{
	if($this->input->post()){
 		 
 		 
 	//is_unique check the unique email value in users table
         $this->form_validation->set_rules('discountname', 'Discount Name', 'trim|required');
        $this->form_validation->set_rules('discountvalue', 'Discount Value', 'trim|required|numeric');
        $this->form_validation->set_rules('discountstarted', 'Discount Start date', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="discountedit";         
                $this->load->view('models_',$reg_pup);            
        }else{ 

             $postData_shipping = $this->input->post(); 
                 $discountcustomer =$postData_shipping['discountcustomer'];
                 $discountcategory= $postData_shipping['discountcategory'];  
                  $discountproducts= $postData_shipping['discountproducts'];                
                  $discountname= $postData_shipping['discountname'];
                  $discountvalue= $postData_shipping['discountvalue'];  
                  $discount_start= strtotime($postData_shipping['discountstarted']);                
                  $discount_end= strtotime($postData_shipping['discountend']);
                  $discount_status= $postData_shipping['discount_status'];
 
                  $discount_id= $postData_shipping['discount_id'];
     
           
              $insert_data=array('customer_id'=>json_encode($customer_id),'discount_id'=>$discount_id,'product_cat_id'=>json_encode($discountcategory),'product_id' =>json_encode($discountproducts),'discount_name'=>$discountname,'discount_amount'=>$discountvalue,'discount_start'=>$discount_start,'discount_end'=>$discount_end,'discount_status'=>$discount_status,'createdate'=>strtotime(date('Y-m-d')));
               
            $file_id = $this->Discountmodel->edit_storediscount($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Discount Successfully update');        
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
            }
            }

        }else{
        	  $this->session->set_flashdata('error','Something went wrong when saving the data, please try again.');
        	   redirect(base_url()."Discount/customerDiscount");  
        }
}


    public function discountdetails()
    {
        $this->load->view('discountdetailsdata');
    }
    public function discountpdf()
    {
        $getLogo = fetchvedorsitelogo();

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Discount');
    $pdf->SetSubject('Houdin-e - Discount');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));



$GEtcate=$this->Discountmodel->fetchstore_category(array());
 
    
 
$total_discount=count($GEtcate['storediscount']);
$url=base_url();
$table_tbody='';
foreach ($GEtcate['storediscount'] as $key => $value) {
                      
                      if($value->discount_status=='1'){
                        $statsu_='<p style="color: green;">Active</p>';
                      }else{
                           $statsu_='<p style="color: red;">Deactive</p>';
                      }
       $table_tbody.='<tr>

<td style="font-size:12px;">'.$value->discount_name.'</td>
<td style="font-size:12px;">'.$value->discount_amount.'</td>
<td style="font-size:12px;">'.date("Y-m-d",$value->discount_start).'</td>
<td style="font-size:12px;">'.date("Y-m-d",$value->discount_end).'</td>
<td style="font-size:12px;">'.$statsu_.'</td>

</tr>';               


}


$html = <<<EOD
 <table style="width:100%;">
<tr style="text-align:center;margin-bottom:20px"><td><img src="$getLogo" width="150px"></td>
<td style="float:right;text-align:right"><p>Discount</p></td>
</tr>
</table>
<br/>
<br/>

 
<table style="width:50%;padding-left:20%!important" cellpadding="10" cellspacing="0" border="1">
<tr>
  <td width="100%">
   <table width="100%">
    <tr>
      <td style="width:20%" rowspan="3"><img src=$url"assets/images/Total_Products.png" width="50px;"></td>
       <td style="padding-top:20px!important;margin-top:20px!important">$total_discount</td>
     </tr>
     <tr>
       
       <td style="width:80%;font-size:14px;color:gray">Total Coupon Box</td>
     </tr>
    
   </table>
  </td>
 
 
  
  </tr>
 
</table>  
<br>
<hr>


<table style="width:100%;" cellpadding="10" border="1">
<tr>
<td style="font-size:12px;font-weight:bold;">Discount name</td>
<td style="font-size:12px;font-weight:bold;">Discount percentage</td>
<td style="font-size:12px;font-weight:bold;">Discount applied on</td>
<td style="font-size:12px;font-weight:bold;">Discount expiry</td>
<td style="font-size:12px;font-weight:bold;">Discount Status</td>

</tr>
  $table_tbody
 
</table>
<br/>
<br/>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_discount_list.pdf', 'I');
    }
    public function discountcustomerpdf()
    {
        $getLogo = fetchvedorsitelogo();
        $url=base_url();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Discount Detail');
    $pdf->SetSubject('Houdin-e - Discount Detail');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = <<<EOD
 <table style="width:100%;">
<tr style="text-align:center;margin-bottom:20px"><td><img src="$getLogo" width="150px"></td>
<td style="float:right;text-align:right"><p>Discount customer</p></td>
</tr>
</table>
<br/>
<br/>

 
<table style="width:100%;" cellpadding="10" cellspacing="0" border="1">
<tr>
  <td width="100%">
   <table width="100%">
    <tr>
      <td style="width:20%" rowspan="3"><img src=$url"assets/images/Total_Products.png" width="50px;"></td>
       <td style="padding-top:20px!important;margin-top:20px!important">1234</td>
     </tr>
     <tr>
       
       <td style="width:80%;font-size:14px;color:gray">Total Coupon Box</td>
     </tr>
    
   </table>
  </td>
 
 
  
  </tr>
 
</table>  
<br>
<hr>


<table style="width:100%;" cellpadding="10" border="1">
<tr>
<td style="font-size:12px;font-weight:bold;">Product Name</td>
<td style="font-size:12px;font-weight:bold;">Product Category</td>
<td style="font-size:12px;font-weight:bold;">Price</td>
<td style="font-size:12px;font-weight:bold;">Discount Price</td>

</tr>
<tr>

<td style="font-size:12px;">Wrist Watch</td>
<td style="font-size:12px;">Digital</td>
<td style="font-size:12px;">Latest</td>
<td style="font-size:12px;">-50%</td>

</tr>
 
</table>
<br/>
<br/>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_discount_list.pdf', 'I');
    }
}
