<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style type="text/css">
  .checkbox {
    padding-left: 0px;
}
</style> 
 
 <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
 
            <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">Partner Setting</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>shipping/Shippinglanding">Shipping</a></li>
                   <li><a href="<?php echo base_url(); ?>Shipping/partner">Courier Partner</a></li>
                  <li class="active">Partner Setting</li>
                  </ol>
                  </div>
              
            </div>
           <!--END Page-Title -->
            <div class="row m-t-20">
              <div class="col-lg-12">
                <div class="card-box">
                <h3 class="text_black">Fedex Shipping Integration</h3>
                <hr>
               <p class="text_black">In order to enable Fedex as a courier partner for your shipping needs,<br>
            We need the credentials of your Fedex account, namely: key, password, account number and meter number,<br>
             Which you get when you create a Web Service integration account at Fedex.<br>
            In addition to credentials, we need other settings that will be applied to all shipments that you create with Fedex.</p>
                <p class="text_black"><b>Please note that:</b></p>
                <p class="text_black">The best options have already been selected in the drop down fields for you.<br>
Whatever settings you choose in the drop-down fields, will be applied to all shipments you create with Fedex.<br>
Some choices may be applicable according to the type of your account with Fedex, and your country.</p>
                 <div class="checkbox checkbox-custom m-b-10">
                 <input id="checkbox11" type="checkbox" >
                 <label>Enabled</label>
              </div>
                                            
                  <form action="#" data-parsley-validate novalidate>
                    <div class="form-group">
                      <label for="userName">Key</label>
                      <input type="text" name="nick" parsley-trigger="change" required placeholder="" class="form-control" id="userName">
                      <p>Key for Fedex Web Services</p>
                    </div>
                    <div class="form-group">
                      <label for="userName">Password</label>
                      <input type="text" name="nick" parsley-trigger="change" required placeholder="" class="form-control" id="
                      userName">
                      <p>Password provided to you for your Fedex Web Services account</p>
                    </div>
                    <div class="form-group">
                      <label for="userName">Account number</label>
                      <input type="text" name="nick" parsley-trigger="change" required placeholder="" class="form-control" id="userName">
                     
                    </div>
                    <div class="form-group">
                      <label for="userName">Meter number</label>
                      <input type="text" name="nick" parsley-trigger="change" required placeholder="" class="form-control" id="
                      userName">
                    </div>
                    <div class="checkbox checkbox-custom m-b-10">
                       <input id="checkbox11" type="checkbox" >
                       <label>Is testing</label>
                       <p>Check this if your credentials are for test environment.</p>
                    </div>

                    <div class="form-group">
                      <label for="userName">Carrier code</label>
                     <select class="form-control">
                      <option>FedEx Cargo</option>
                      <option>FedEx Express</option>
                      <option>FedEx Ground</option>
                    </select>
                    <p>For India, only FedEx Express will work.</p>
                   
                    </div>
                    <div class="form-group">
                      <label for="userName">Dropoff type</label>
                     <select class="form-control">
                      <option>Business Service Center</option>
                      <option>Drop Box</option>
                      <option>Regular Pickup</option>
                    </select>
                    <p>How the package is received by Fedex - usually requesting a pickup is better.</p>
                   
                    </div>
                      <div class="form-group">
                      <label for="userName">Service type</label>
                     <select class="form-control">
                      <option>EUROPE_FIRST_INTERNATIONAL_PRIORITY</option>
                      <option>FEDEX_1_DAY_FREIGHT</option>
                      <option>FEDEX_2_DAY</option>
                    </select>
                    <p>Please ask your Fedex account manager about the services available in your location. FedEx Express Saver is advised to save costs.</p>
                   
                    </div>
                    <div class="form-group">
                      <label for="userName">Packaging type</label>
                     <select class="form-control">
                      <option>FEDEX_10KG_BOX</option>
                      <option>FEDEX_25KG_BOX</option>
                      <option>FEDEX_BOX</option>
                    </select>
                    <p>The default of YOUR_PACKAGING is advised to save costs.</p>
                   
                    </div>

                    <div class="form-group">
                      <label for="userName">Company close time</label>
                      <input type="text" name="nick" parsley-trigger="change" required placeholder="" class="form-control" id="userName">
                     <p>For eg: 06:30 PM</p>
                    </div>
                   
                    <div class="form-group m-b-0">
                      <button class="btn btn-primary waves-effect waves-light" type="Save">
                        Save
                      </button>
                      
                    </div>
                    
                  </form>
                </div>
              </div>
              
              
            </div>

                                      

                                       
      </div>
    </div>
  </div>

<?php $this->load->view('Template/footer.php') ?>

<script>
$(document).ready(function(){

    $(".select2").select2();
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
