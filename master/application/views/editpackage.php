<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <h4 class="page-title">Edit Package</h4>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-sm-12">
                        <?php 
                        if($this->session->flashdata('error'))
                        {
                            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                        }
                        if($this->session->flashdata('success'))
                        {
                            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                        }
                        ?>
                        </div>
                        </div>
        <div class="row">
        <div class="col-sm-12">
        <div class="card-box">
        <?php echo form_open(base_url( 'Package/editpackage/'.$this->uri->segment('3').'' ), array( 'id' => 'editPackageData','class'=>'m-t-30'));
        ?>
        <div class="row">
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2">Package Name</label>
        <div class="col-md-10">
        <!-- <input type="hidden" class="pluginIddata" name="editPluginIddata" value="<?php //echo $packageDataInfo[0]->houdin_package_plugin_plu_id ?>"/> -->
        <input type="text" name="packageName" class="form-control required_validation_for_edit_package name_validation" value="<?php echo $packageDataInfo[0]->houdin_package_name ?>" placeholder="Package Name">
        </div>
        </div>

        </div>
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2">Price</label>
        <div class="col-md-10">
        <input type="text" name="packagePrice" class="form-control required_validation_for_edit_package number_validation" value="<?php echo $packageDataInfo[0]->houdin_package_price ?>" placeholder="Price">
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">Products</label>
        <div class="col-md-10">
        <input type="text" class="form-control required_validation_for_edit_package name_validation number_validation" value="<?php echo $packageDataInfo[0]->houdin_package_product ?>" name="products" placeholder="Products">
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">Staff Account</label>
        <div class="col-md-10">
        <input type="text" class="form-control required_validation_for_edit_package name_validation number_validation" value="<?php echo $packageDataInfo[0]->houdin_package_staff_accounts ?>" name="staffaccount" placeholder="Staff Account">
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">Stock Location</label>
        <div class="col-md-10">
        <input type="text" class="form-control required_validation_for_edit_package name_validation number_validation" value="<?php echo $packageDataInfo[0]->houdin_package_stock_location ?>" name="stockLocation" placeholder="Stock Location">
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">App Store Access</label>
        <div class="col-md-10">
        <select class="form-control required_validation_for_edit_package" name="appaccess">
        <option value="">Choose Access</option>
        <option <?php if($packageDataInfo[0]->houdin_package_app_access == 'limited') { ?> selected="selected" <?php } ?> value="limited">Limited</option>
        <option <?php if($packageDataInfo[0]->houdin_package_app_access == 'full') { ?> selected="selected" <?php } ?> value="full">Full</option>
        </select>
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">Order(per month)</label>
        <div class="col-md-10">
        <input type="text" class="form-control required_validation_for_edit_package name_validation number_validation" value="<?php echo $packageDataInfo[0]->houdin_package_order_pm ?>" name="orderpm" placeholder="Orders">
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">Free SMS</label>
        <div class="col-md-10">
        <input type="text" class="form-control required_validation_for_edit_package name_validation number_validation" value="<?php echo $packageDataInfo[0]->houdin_package_sms ?>" name="freesms" placeholder="Free SMS">
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">Free Emails</label>
        <div class="col-md-10">
        <input type="text" class="form-control required_validation_for_edit_package name_validation number_validation" value="<?php echo $packageDataInfo[0]->houdin_package_email ?>" name="freeemails" placeholder="Free Emails">
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">Accounting Integration</label>
        <div class="col-md-10">
        <select class="form-control required_validation_for_edit_package" name="accountingintegration">
        <option value="">Choose Access</option>
        <option <?php if($packageDataInfo[0]->houdin_package_accounting == 'yes') { ?> selected="selected" <?php } ?> value="yes">Yes</option>
        <option <?php if($packageDataInfo[0]->houdin_package_accounting == 'no') { ?> selected="selected" <?php } ?> value="no">No</option>
        </select>
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">POS Integration</label>
        <div class="col-md-10">
        <select class="form-control required_validation_for_edit_package" name="posintegration">
        <option value="">Choose Access</option>
        <option <?php if($packageDataInfo[0]->houdin_package_pos == 'yes') { ?> selected="selected" <?php } ?> value="yes">Yes</option>
        <option <?php if($packageDataInfo[0]->houdin_package_pos == 'no') { ?> selected="selected" <?php } ?> value="no">No</option>
        </select>
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">Delivery Integration</label>
        <div class="col-md-10">
        <select class="form-control required_validation_for_edit_package" name="deliveryintegration">
        <option value="">Choose Access</option>
        <option <?php if($packageDataInfo[0]->houdin_package_delivery == 'yes') { ?> selected="selected" <?php } ?> value="yes">Yes</option>
        <option <?php if($packageDataInfo[0]->houdin_package_delivery == 'no') { ?> selected="selected" <?php } ?> value="no">No</option>
        </select>
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">Payment Gateway Integration</label>
        <div class="col-md-10">
        <select class="form-control required_validation_for_edit_package" name="pgintegration">
        <option value="">Choose Access</option>
        <option <?php if($packageDataInfo[0]->houdin_package_payment_gateway == 'yes') { ?> selected="selected" <?php } ?> value="yes">Yes</option>
        <option <?php if($packageDataInfo[0]->houdin_package_payment_gateway == 'no') { ?> selected="selected" <?php } ?> value="no">No</option>
        </select>
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 ">Templates</label>
        <div class="col-md-10">
        <input type="text" class="form-control required_validation_for_edit_package name_validation number_validation" value="<?php echo $packageDataInfo[0]->houdin_package_templates ?>" name="templates" placeholder="Templates">
        </div>
        </div>
        </div>

        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2">Status</label>
        <div class="col-md-10">
        <select name="packageStatus" class="form-control required_validation_for_edit_package">
            <option value="">Choose Status</option>
            <option <?php if($packageDataInfo[0]->houdin_package_status == 'active') { echo "selected"; } ?> value="active">Active</option>
            <option <?php if($packageDataInfo[0]->houdin_package_status == 'deactive') { echo "selected"; } ?> value="deactive">Deactive</option>
        </select>
        </div>
        </div>
        </div>
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2">Description</label>
        <div class="col-md-10">
        <textarea class="form-control required_validation_for_edit_package name_validation" rows="5" name="packageDescription"><?php echo $packageDataInfo[0]->houdin_package_description ?></textarea>
        </div>
        </div>
        </div>
        
        <div class="clearfix"></div>
        <div class="col-md-12 ">
        <input type="submit" class="btn btn-default pull-right m-r-10" name="editPackageInfoBtn" value="Submit">
        <button type="reset" class="btn btn-info pull-right m-r-10">Reset</button>
        </div>
        </div>
        <?php echo form_close();?>
          <?php 
        
//         $allupdateCountry = json_decode($packageDataInfo[0]->houdin_package_countries,true);
//       // $allupdateCountry = get_object_vars($allupdateCountry);
//       // foreach($allupdateCountry as )
// // print_R($allupdateCountry);
//         foreach($country as $value)
//         { 
//             $match='';
//             foreach($allupdateCountry as $mt)
//             {
                
//               if(trim($mt['country'])==trim($value['houdin_country_name']))
//               {
//             //    print_R($mt);
//                $match= $mt['price'];
//               }
//             }
            ?>
       <!-- <div class="col-md-6 checkboxClass" style="margin-bottom: 10px;">
          <div class="col-md-1" style="text-align: right;padding-top: 6px;">
              <input style="height: 16px;width: 16px;" type="checkbox" class="oncheck" name="country[]" value="<?php //echo $value['houdin_country_name']; ?>" <?php if($match){ echo 'checked'; } ?>>
          </div>
          <div class="col-md-3">
              <p style="padding-top: 7px;" class="country_name"><?php //echo $value['houdin_country_name']; ?></p>
          </div>
          <div class="col-md-8">
              <input type="text" value="<?php //if($match){ echo $match;}?>" class="form-control input_price required_validation_for_register number_validation" <?php if(!$match){ echo 'disabled="disabled"'; } ?>name="price[]" placeholder="price">
          </div>
        </div> -->
        <?php //} ?>
       
        </div>
        </div>
        </div>
        <?php 
       //  $getPlugindata = explode(',',$packageDataInfo[0]->houdin_package_plugin_plu_id);
        ?>
        <!-- <div id="products" class="row list-group">
        <div class="col-sm-12 m-b-15">
            <h4 class="page-title"><img src="<?php echo base_url(); ?>assets/images/folder.png" class="height_image">Choose Plugins
              <span class="pull-right">
                <input type="checkbox" <?php if(count($pluginListData) == count($getPlugindata)) { echo "checked"; } ?> class="masterCheckPackage"/>
                <label style="font-size:17px !important;">Select All</label>
              </span>

            </h4>
        </div>
        <?php 
        // foreach($pluginListData as $pluginListDataValue)
        // {
        ?>
       <div class="item  col-xs-12 col-lg-3">
           <div class="thumbnail">
            
               <div class="caption">
                   <h4 class="group inner list-group-item-heading">
                    <input type="checkbox" <?php //if(in_array($pluginListDataValue->houdin_plugin_id,$getPlugindata)) { echo "checked"; } ?> class="childCheckPackage" data-id="<?php //echo $pluginListDataValue->houdin_plugin_id ?>" />&nbsp;  <?php //echo $pluginListDataValue->houdin_plugin_name ?></h4>
               </div>
           </div>
       </div>
        <?php //}
        ?>
   </div> -->

                    </div> <!-- container -->
                </div> <!-- content -->
<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">
	$(document).ready(function(){
        $(document).ready(function(){
                $(document).on('submit','#editPackageData',function(){
                    var check_required_field='';
                    $(this).find(".required_validation_for_edit_package").each(function(){
                        var val22 = $(this).val();
                        if (!val22){
                            check_required_field =$(this).size();
                            $(this).css("border-color","#ccc");
                            $(this).css("border-color","red");
                        }
                        $(this).on('keypress change',function(){
                            $(this).css("border-color","#ccc");
                        });
                    });
                    if(check_required_field)
                    {
                        return false;
                    }
                    else {
                        return true;
                    }
                });
            });
		// $(document).on('submit','#editPackageData',function(){
        //     var check_required_field='';
        //     var checkCheckeddata = 0;
		// 	$(".required_validation_for_edit_package").each(function(){
		// 		var val22 = $(this).val();
		// 		if (!val22){
		// 			check_required_field =$(this).size();
		// 			$(this).css("border-color","#ccc");
		// 			$(this).css("border-color","red");
		// 		}
		// 		$(this).on('keypress change',function(){
		// 			$(this).css("border-color","#ccc");
		// 		});
		// 	});
            
            
        //       $(this).find(".required_validation_for_register").each(function()
        //        {
        //                if($(this).prop("disabled")==false)
        //                {
        //                 var val22 = jQuery(this).val();

        //                 if (!val22)
        //                 {
        //                         check_required_field =$(this).size();
        //                         $(this).css("border-color","red");


        //                 }
        //                 }
        //         });

            
        //                     $(this).find('.required_validation_for_register').on('keyup blur',function()
        //                         {

        //                                 $(this).css("border-color","#ccc");
        //                         });
            
            
		// 	if(check_required_field)
		// 	{
		// 		return false;
		// 	}
		// 	else {
        //         $('.childCheckPackage').each(function(){
        //             if($(this).prop('checked') == true)
        //             {
        //                 checkCheckeddata++;
        //             }
        //         });
        //         if(checkCheckeddata > 0)
        //         {
        //             return true;
        //         }
        //         else
        //         {
        //             alert('Please choose atleast one plugin');
        //             return false;
        //         }
		// 	}
		// });
	});
    
    
            	$(document).ready(function(){
		$(document).on('change','.oncheck',function(){


          if($(this).is(':checked')){
            $(this).parents('.checkboxClass').find('.input_price').prop("disabled",false);
            }
            else
            {
             $(this).parents('.checkboxClass').find('.input_price').prop("disabled",true);
            }
          });
          });
	</script>