<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller {
    function __construct()
    {
		parent::__construct();
		$this->auth_users->is_logged_in();
		$this->load->model('Dashboardmodel'); 
    } 
	public function index()
	{
	 	$arr = $this->getLastNDays(12);
		$desboard_data=$this->Dashboardmodel->fetchCustomerRecord();
		$this->load->view('dashboard',$desboard_data);
	}
	public function getLastNDays($days, $format = 'd/m/Y')
	{
		$m = date("m"); $de= date("d"); $y= date("Y");
		$dateArray = array();
		for($i=0; $i<=$days-1; $i++){
			$dateArray[] = '"' . date($format, mktime(0,0,0,$m,($de-$i),$y)) . '"'; 
		}
		return array_reverse($dateArray);
	}
}
