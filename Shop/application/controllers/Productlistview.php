<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Productlistview extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopproductlistviewmodel');
        $this->load->model('Shopsettingmodel');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
        $this->alwaysCheck = array(
                            array("field"=>"product_id",
                               "label"=>"product id",
                               "rules"=>"required"),
                               
                               
                            array("field"=>"user_message",
                               "label"=>"message",
                               "rules"=>"required"),
                                        
                            array("field"=>"user_email",
                               "label"=>"email",
                               "rules"=>"required"),
                               
                               
                            array("field"=>"user_name",
                               "label"=>"name",
                               "rules"=>"required"),  
                                 
                            array("field"=>"rating",
                               "label"=>"rating",
                               "rules"=>"required")   
                         );
    }
    public function index()
    {
        $product_id = $this->uri->segment('2');
        
        if($this->input->post("review_submit"))
        {
           
               $dateData = strtotime(date("Y-m-d h:i:s"));
            $array = array("houdinv_product_review_user_id"=>$this->session->userdata('userAuth'),
                           "houdinv_product_review_product_id"=>$this->input->post("product_id"),
                           "houdinv_product_reviews_variant_id"=>$this->input->post("variant_id"),
                           "houdinv_product_review_message"=>$this->input->post("user_message"),
                           "houdinv_product_review_rating"=>$this->input->post("rating"),
                           "houdinv_product_review_user_email"=>$this->input->post("user_email"),
                           "houdinv_product_review_user_name"=>$this->input->post("user_name")) ; 
                           
            $review =  $this->Shopproductlistviewmodel->SaveReview($array); 
            if($review)
            {
                
            $this->session->set_flashdata("success","Thanks for your review");
            if($this->input->post("variant_id"))
            {
                redirect(base_url()."Productlistview/variant".$this->input->post("variant_id"), 'refresh');     
            }
            else
            {
                redirect(base_url()."Productlistview/".$this->input->post("product_id"), 'refresh');     
            }
            
            }   
            else
            {
            $this->session->set_flashdata("message_name","Something went wrong ! try again");
            if($this->input->post("variant_id"))
            {
                redirect(base_url()."Productlistview/variant".$this->input->post("variant_id"), 'refresh');     
            }
            else
            {
                redirect(base_url()."Productlistview/".$this->input->post("product_id"), 'refresh');     
            }    
            }          
                            
        }
        
        
        $data['product'] = $this->Shopproductlistviewmodel->GetSingleProductList($product_id);
        if(!count($data['product']['main_product']) > 0)
        {
            redirect(base_url()."home/".$this->session->userdata('shopName'), 'refresh');     
        }
         $data['product_id'] = $product_id;
         $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view($getShopName.'/productlistview',$data);
    }
    public function variant()
    {
        // Add review
        if($this->input->post('review_submit'))
        {
            $dateData = strtotime(date("Y-m-d h:i:s"));
            $array = array("houdinv_product_review_user_id"=>$this->session->userdata('userAuth'),
                            "houdinv_product_reviews_variant_id"=>$this->input->post("variant_id"),
                            "houdinv_product_review_message"=>$this->input->post("user_message"),
                            "houdinv_product_review_rating"=>$this->input->post("rating"),
                            "houdinv_product_review_user_email"=>$this->input->post("user_email"),
                            "houdinv_product_review_user_name"=>$this->input->post("user_name")) ; 
                            
                $review =  $this->Shopproductlistviewmodel->SaveReview($array); 
                if($review)
                {
                $this->session->set_flashdata("success","Thanks for your review");
                redirect(base_url()."Productlistview/variant/".$this->uri->segment("3"), 'refresh');     
                }   
                else
                {
                $this->session->set_flashdata("message_name","Something went wrong ! try again");
                redirect(base_url()."Productlistview/variant/".$this->uri->segment("3"), 'refresh');       
                }    
        }      
        $getVariantInfo = $this->Shopproductlistviewmodel->getVariantData($this->uri->segment('3'));
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view($getShopName.'/productvariantlistview',$getVariantInfo);
    }
}
