<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();
		$getSubDomainData = checkMainUrl();
        if($getSubDomainData)
        {
            redirect(base_url(), 'refresh');
        }
		$this->load->model('Forgetmodel');
		$this->load->model('Packagemodel');
		$this->load->model('Contactmodel');
        $this->load->helper('form');
       
        $this->load->library('form_validation');
	} 
	public function index()
	{
		$getPackageData = $this->Packagemodel->fetchDynamicPackage();
		$this->load->view('Home',$getPackageData);
	}
	
	public function home2()
	{
		$this->load->view('home2');
	}
	
	public function setindex()
	{
		$this->load->view('setindex');
	}
	
	public function about()
	{
		$this->load->view('about');
	}
	
	
	public function contact()
	{
		if($this->input->post('sendContactMessage'))
		{
			$this->form_validation->set_rules('firstname','first name','required');
			$this->form_validation->set_rules('lastname','last name','required');
			$this->form_validation->set_rules('email','email','required|valid_email');
			$this->form_validation->set_rules('phone','phone','required');
			$this->form_validation->set_rules('message','message','required');
			if($this->form_validation->run() == true)
			{
				$setName = $this->input->post('firstname')." ".$this->input->post('lastname');
				$setMessageInsertArray = array('houdin_admin_contact_name'=>$setName,'houdin_admin_contact_email'=>$this->input->post('email'),'houdin_admin_contact_phone'=>$this->input->post('phone'),'houdin_admin_contact_message'=>$this->input->post('message'));
				$getResponse = $this->Contactmodel->insertcontactdata($setMessageInsertArray);
				if($getResponse)
				{
					$getsendgrid = sendgrid();
					$getLogo = logo();
					$getToemail = toemail();
					$getFromEmail = fromemail();
					// send email data
					$url = 'https://api.sendgrid.com/';
					$user = $getsendgrid[0]->houdin_sendgrid_username;
					$pass = $getsendgrid[0]->houdin_sendgrid_password;
					$json_string = array(
					'to' => array(
						$getToemail
					),
					'category' => 'test_category'
					);
					$htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
							<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
								<td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
								<td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
								<div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
									<table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
									<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
										<td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
										<img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
										<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
										<td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
											<table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
											<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
												<td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Hello Admin </strong></td></tr>
												
												<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
												<td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Name:</td>
												<td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$setName.'</td>
												</tr>
												<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
												<td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Email:</td>
												<td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->input->post('email').'</td>
												</tr>
												<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
												<td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Phone:</td>
												<td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->input->post('phone').'</td>
												</tr>

												<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
												<td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Message:</td>
												<td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->input->post('message').'</td>
												</tr>


												<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
													<td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
							<div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
							<table width="100%" ><tr >
							<td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e '.date('Y').'--All right reserverd </td></tr>
							<tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
							</tr></table>
							</div>
							</div></td></tr></table>';
					$params = array(
						'api_user'  => $user,
						'api_key'   => $pass,
						'x-smtpapi' => json_encode($json_string),
						'to'        => $getToemail,
						'fromname'  => 'Houdin-e',
						'subject'   => 'Houdin-e Contact Form',
						'html'      => $htm,
						'from'      => $getFromEmail,
					);
					$request =  $url.'api/mail.send.json';
					$session = curl_init($request);
					curl_setopt ($session, CURLOPT_POST, true);
					curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
					curl_setopt($session, CURLOPT_HEADER, false);
					curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
					curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
					$response = curl_exec($session);
					curl_close($session);
					$getResponse = json_decode($response);
					$this->load->helper('url');
					$this->session->set_flashdata('success','We received your message and will contact you back soon.');
            		redirect(base_url()."home/contact", 'refresh');	
				}
				else
				{
					$this->load->helper('url');
					$this->session->set_flashdata('error','Something went wrong. Please try again');
					redirect(base_url()."home/contact", 'refresh');	
				}
			}
			else
			{
				$this->load->helper('url');
				$this->session->set_flashdata('error',validation_errors());
            	redirect(base_url()."home/contact", 'refresh');
			}

		}
		$this->load->view('contact');
	}
	
	public function faq()
	{
		$this->load->view('faq');
	} 
	public function terms()
	{
		$this->load->view('terms');
	}
	public function privacy_policy()
	{
		$this->load->view('privacy');
	}
	public function blog()
	{
		$this->load->view('blog');
	}
	
	public function blog_detail()
	{
		$this->load->view('blog_detail');
	}
	public function themes()
	{
		$this->load->view('theme');
	}
	public function plans()
	{
		$getPackageData = $this->Packagemodel->fetchDynamicPackage();
		$this->load->view('plans',$getPackageData);
	}
	public function how_it_work()
	{
		$this->load->view('how_it_work');
	}
	
	public function features()
	{
		$this->load->view('features');
	}	
    
    	public function Reset()
	{
		  
               // check token data
        if($this->session->userdata('forgetuserTokenset') == "")
        {
            redirect(base_url(), 'refresh');
        }
        if($this->input->post('resetPasswordData'))
        {
            if($this->input->post('password') == $this->input->post('confirmPassword'))
            {
                $this->form_validation->set_rules('forgetPin','text','required|max_length[5]|integer');
                $this->form_validation->set_rules('password','text','required');
                $this->form_validation->set_rules('confirmPassword','text','required');
                if($this->form_validation->run() == true)
                {
                    if($this->session->userdata('forgetuserTokenset') == $this->input->post('forgetPin'))
                    {
                        $setArrayData = array('pin'=>$this->input->post('forgetPin'),'password'=>$this->input->post('password'));
                        $getUpdateStatus = $this->Forgetmodel->updateUserPassword($setArrayData);
                        if($getUpdateStatus['message'] == 'yes')
                        {
                            $this->session->unset_userdata('forgetuserTokenset');
                            $this->session->set_flashdata('success','Password updated successfully');
                            $this->load->helper('url');
                            redirect(base_url(), 'refresh'); 
                        }
                        else if($getUpdateStatus['message'] == 'pin')
                        {
                            $this->session->set_flashdata('error','Your pin is expired. Please generate new pin');
                            $this->load->helper('url');
                            redirect(base_url()."Home/Reset", 'refresh');  
                        }
                        else
                        {
                            $this->session->set_flashdata('error','Something went wrong. Please try again');
                            $this->load->helper('url');
                            redirect(base_url()."Home/Reset", 'refresh');  
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('error','Please enter correct pin');
                        $this->load->helper('url');
                        redirect(base_url()."Home/Reset", 'refresh');    
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','All fields are mandatory');
                    $this->load->helper('url');
                    redirect(base_url()."Home/Reset", 'refresh');
                }   
            }
            else
            {
                $this->session->set_flashdata('error','Password are not matched');
                $this->load->helper('url');
                redirect(base_url()."Home/Reset", 'refresh');
            }
        }
      
       
		$this->load->view('Reset_password');
	}
}
