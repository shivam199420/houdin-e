<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Webservicewishlist extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Userwishlistmodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
     }   
     public function addwishlist()
     {
         $this->form_validation->set_rules('shopName','text','required');
         $this->form_validation->set_rules('userId','text','required');
         if($this->form_validation->run() == true)
         {
            $setArray = array('shopName'=>$this->input->post('shopName'),'userId'=>$this->input->post('userId'),'product'=>$this->input->post('productId'),'variant'=>$this->input->post('variantId'),'cartId'=>$this->input->post('cartId'));
            $resultArray = $this->Userwishlistmodel->updateWishlistData($setArray);
            print_r(json_encode($resultArray));
         }
         else
         {
            $resultData = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultData));
         }
     }
}       