<?php $this->load->view('Template/header') ?>
<?php $this->load->view('Template/sidebar') ?>
<style>
        .searh-suggestionboxlistboxdata
        {
          list-style-type: none;
          padding: 0;
          margin: 0;
          position: absolute;
          top: 100%;
          width: 100%;
          height: 120px;
          overflow-y: scroll;
        }
        </style>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">

        <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Edit Supplier</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>Supplier">Supplier</a></li>
                  <li class="active">Edit Supplier</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                  
            </div>
           <!--END Page-Title --> 
        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
        <div class="row m-t-20">
        <div class="col-sm-12">
        <div class="card-box">
        <?php echo form_open(base_url( 'Supplier/editsupplier/'.$this->uri->segment('3').'' ), array( 'id' => 'addSupplierData', 'method'=>'post' ));?>
        <div class="row">
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 control-label">Company Name</label>
        <div class="col-md-10">
        <input type="text" value="<?php echo $fetchEditSupplierList[0]->houdinv_supplier_comapany_name ?>" class="form-control required_validation_for_add_supplier name_validation" name="supplierCompanyName" placeholder="Company Name">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Contact Person</label>
        <div class="col-md-10">
        <input type="text" value="<?php echo $fetchEditSupplierList[0]->houdinv_supplier_contact_person_name ?>" class="form-control required_validation_for_add_supplier name_validation" name="supplierContactPerson" placeholder="Contact Person Name">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Email</label>
        <div class="col-md-10">
        <input type="text" value="<?php echo $fetchEditSupplierList[0]->houdinv_supplier_email ?>" class="form-control required_validation_for_add_supplier name_validation email_validation" readonly="readonly" name="supplierEmail" placeholder="Email">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Mobile Number</label>
        <div class="col-md-10">
        <input type="text" value="<?php echo $fetchEditSupplierList[0]->houdinv_supplier_contact ?>" class="form-control Internationphonecode required_validation_for_add_supplier name_validation" name="supplierMobile" placeholder="Mobile Number">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Landline Number</label>
        <div class="col-md-10">
        <input type="text" value="<?php echo $fetchEditSupplierList[0]->houdinv_supplier_landline ?>" class="form-control name_validation" name="supplierlandline" placeholder="Landline Number">
        </div>
        </div>
        <div class="form-group setProductIdData" style="display:none">
        <?php 
        foreach($supplierProducts as $supplierProductsData)
        { ?>  
        <input type="hidden" value="<?php echo $supplierProductsData->houdinv_supplier_products_product_id ?>" class="getProiductIdData" name="supplierProduct[]">
        <?php } ?>
        </div>
        <!-- <div class="form-group">
        <label class="col-md-2 control-label">Choose Status</label>
        <div class="col-md-10">
        <select class="form-control required_validation_for_add_supplier" name="supplierStatus">
        <option value="">Choose Status</option>
        <option <?php //if($fetchEditSupplierList[0]->houdinv_supplier_active_status == 'active') { ?> selected="selected" <?php  //} ?> value="active">Active</option>
        <option <?php //if($fetchEditSupplierList[0]->houdinv_supplier_active_status == 'deactive') { ?> selected="selected" <?php // } ?> value="deactive">Deactive</option>
        </select>
        </div>
        </div> -->
        <div class="form-group">
        <label class="col-md-2 control-label">Address</label>
        <div class="col-md-10">
        <textarea class="form-control required_validation_for_add_supplier name_validation" name="supplierAddress" rows="5"><?php echo $fetchEditSupplierList[0]->houdinv_supplier_address ?></textarea>
        </div>
        </div>


        </div>

        <div class="col-md-12 ">
        <input type="submit" class="btn btn-default pull-right m-r-10" name="editSupplierBtn" value="Submit">
        <button type="reset" class="btn btn-info pull-right m-r-10">Reset</button>
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>

        <div class="card-box">
        <div class="row">
        <form method="post" id="searchKeywordData">
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 control-label">Search Product</label>
        <div class="col-md-10">

        <div class="searh-suggestionbox m-b-10">

        <input type="text" name="keyword" class="searh-suggestionboxinput form-control" placeholder="Search Product" />
        <input type="hidden" class="setToken" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
        <ul class="searh-suggestionboxlistboxdata" style="display:none;">
        <!-- <li class="searh-suggestionboxlistbox_row"><a href="#">Adele</a></li> -->
        </ul>
        </div>
        <!-- parent box -->
        <div class="setProductTag">
        <?php 
        foreach($supplierProducts as $supplierProductsData)
        { ?>   
        <div class="tag setParentData label label-info alert alert-dismissable m-r-5 pointer"><span class="tag_text"><?php echo $supplierProductsData->houdin_products_title ?></span><span data-id="<?php echo $supplierProductsData->houdinv_supplier_products_product_id ?>" class="removeProduct" style="cursor:pointer;" type="button" data-dismiss="alert" aria-hidden="true">×</span></div>
        <?php } ?>
        </div><!-- parent box end here -->
        </div>
        </div>

        </div>
        </div>
        </div>
        </form>
        </div>
        </div>
<?php $this->load->view('Template/footer') ?>
<script>
        $(document).ready(function(){
        $(document).on('keyup','.searh-suggestionboxinput',function(){
        var formData = $('#searchKeywordData').serialize();  
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>" + "Supplier/searchProductName",
        data: formData,
        success: function(data) {
          if($.trim(data) != 'no')
          {
            $('.searh-suggestionboxlistboxdata').show();
            $('.searh-suggestionboxlistboxdata').html('');
            $('.searh-suggestionboxlistboxdata').html(data);
          }
          else
          {
            $('.searh-suggestionboxlistboxdata').hide();
            $('.searh-suggestionboxlistboxdata').html('');
          }
        }
        });
        });
        });
        </script>
        <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addSupplierData',function(){
			var check_required_field='';
			$(".required_validation_for_add_supplier").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>

