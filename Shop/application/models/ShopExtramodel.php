<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ShopExtramodel extends CI_Model{
    function __construct() 
    {
    }
    public function getAppLogo($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getAppLogo = $getDynamicDB->select('applogo')->from('houdinv_shop_applogo')->get()->result();
        if($getAppLogo[0]->applogo)
        {
            $setAppLogo = $this->session->userdata('vendorURL')."upload/applogo/".$getAppLogo[0]->applogo;
        }
        else
        {
            $setAppLogo = $this->session->userdata('vendorURL')."upload/applogo/Logo_F_B.png";
        }
        return array('messgae'=>'Success','logo'=>$setAppLogo);
    }
    public function getAppSplash($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);
        $getAppSplash = $getDynamicDB->select('splashscreen')->from('houdinv_shop_applogo')->get()->result();
        if($getAppSplash[0]->splashscreen)
        {
            $setSplashLogo = $this->session->userdata('vendorURL')."upload/applogo/".$getAppSplash[0]->splashscreen;
        }
        else
        {
            $setSplashLogo = $this->session->userdata('vendorURL')."upload/applogo/Logo_F_B.png";
        }
        return array('messgae'=>'Success','splash'=>$setSplashLogo);
    }
    public function getUserProfile($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getUserProfile = $getDynamicDB->select('houdinv_users_profile,houdinv_user_name')->from('houdinv_users')->where('houdinv_user_id',$data['id'])->get()->result();
        if(count($getUserProfile) > 0)
        {
            if($getUserProfile[0]->houdinv_users_profile)
            {
                $getShop = shopurl();
                $setImage = "".$getShop."Extra/Uploads/userprofile/".$getUserProfile[0]->houdinv_users_profile;
            }
            else
            {
                $setImage = "";
            }
            return array('message'=>'Success','image'=>$setImage,'name'=>$getUserProfile[0]->houdinv_user_name);
        }
        else
        {
            return array('message'=>'Something went wrong. Please try again');
        }
    }
    public function getAboutData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);   
        $getAboutData = $getDynamicDB->select('about')->from('houdinv_storepolicies')->get()->result();
        if(count($getAboutData) > 0)
        {
            $getAbout = strip_tags($getAboutData[0]->about);
            return array('message'=>'Success','data'=>$getAbout);
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function getTermsData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);   

        $getTermsData = $getDynamicDB->select('Terms_Conditions')->from('houdinv_storepolicies')->get()->result();
        if(count($getTermsData) > 0)
        {
            $getTerms = strip_tags($getTermsData[0]->Terms_Conditions);
            return array('message'=>'Success','data'=>$getTerms);
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function getFaqData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);   

        $getFaqData = $getDynamicDB->select('faq')->from('houdinv_storepolicies')->get()->result();
        if(count($getFaqData) > 0)
        {
            $getFaq = strip_tags($getFaqData[0]->faq);
            return array('message'=>'Success','data'=>$getFaqData[0]->faq);
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function SaveQuatation($array,$shopName)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shopName);
        $getDynamicDB = $this->load->database($getAppDB,true); 
        if($array)
        {
            $getData = $getDynamicDB->insert("houdinv_shop_ask_quotation",$array);
            return $getData;
        }
    }
    public function setPayumoneyData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true); 

        $getPayuMoney = $getDynamicDB->select('*')->from('houdinv_payment_gateway')->where('houdinv_payment_gateway_type','payu')->get()->result();
        $getAuthStatus = $getDynamicDB->select('*')->from('houdinv_payment_gateway')->where('houdinv_payment_gateway_type','auth')->get()->result();
        if($getAuthStatus[0]->houdinv_payment_gateway_status == 'active')
        {
            $setAuthActive = 'yes';
        }
        else
        {
            $setAuthActive = 'no';
        }
        if(count($getPayuMoney) > 0)
        {
            if($getPayuMoney[0]->houdinv_payment_gateway_status == 'active')
            {
                return array('status'=>'yes','merchant'=>$getPayuMoney[0]->houdinv_payment_gateway_merchnat_key,'salt'=>$getPayuMoney[0]->houdinv_payment_gateway_merchant_salt,'auth'=>$setAuthActive);
            }
            else
            {
                return array('status'=>'no','auth'=>$setAuthActive);
            }
        }
        else
        {
            return array('status'=>'no','auth'=>$setAuthActive);
        }
    }
    public function getOrderCountData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true); 
        $getTotalOrder = $getDynamicDB->select('COUNT(houdinv_order_id) AS totalOrder')->from('houdinv_orders')->where('MONTH(FROM_UNIXTIME(houdinv_order_created_at))= MONTH(CURDATE())')->get()->result();
        if($getTotalOrder[0]->totalOrder)
        {
            $setOrder = $getTotalOrder[0]->totalOrder;
        }
        else
        {
            $setOrder = 0;
        }
        return array('order'=>$setOrder);
    }
    public function getVendorCurrency($data)
    {
        $masterDb = $this->load->database('master',true);
        $getCurrencyData = $masterDb->select('houdin_user_shops.houdin_user_shop_user_id,houdin_users.houdin_users_currency')->from('houdin_user_shops')->where('houdin_user_shops.domain_name',$data)
        ->join('houdin_users','houdin_users.houdin_user_id = houdin_user_shops.houdin_user_shop_user_id')->get()->result();
        $setArray = array('currency'=>$getCurrencyData[0]->houdin_users_currency);
        return $setArray;
    }
} 

?>