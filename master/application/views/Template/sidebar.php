<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
            <?php 
            if($this->session->userdata('role') == 0)
            {
            ?>
            <li class="has_sub">
                    <a href="<?php echo base_url(); ?>dashboard" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>

                </li>

                <li class="has_sub">
                    <a href="<?php echo base_url(); ?>shop" class="waves-effect"><i class="fa fa-object-group"></i> <span> Shop Management </span> </a>

                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cubes"></i> <span>  Package </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>package">Package Management</a></li>
                        <!-- <li><a href="<?php //echo base_url(); ?>package/plugin">Plugins Management</a></li> -->
                        <li><a href="<?php echo base_url(); ?>package/smspackage">SMS Package</a></li>
                        <li><a href="<?php echo base_url(); ?>package/emailpackage">Email Package</a></li>

                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog"></i> <span> Setting </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>setting/language">Language</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/country">Country</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/template">Template</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/businesscategory">Business Category</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/logomanagement">Logo Management</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/smscredentials">Twillio Credentials</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/emailcredentials">Sendgrid Credentias</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/payumoney">Payumoney Credentias</a></li>

                    </ul>
                </li>
                </li>
                   <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog"></i> <span>App Setting </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>AppSetting/businesscategory">Business Category</a></li>

                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-modx"></i> <span>  Admin Management </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>Adminmanagement/department">Departments</a></li>
                        <li><a href="<?php echo base_url(); ?>Adminmanagement/subadmins">Subadmins</a></li>

                    </ul>
                </li>
                 
                <li class="has_sub">
                    <a href="<?php echo base_url(); ?>Adminlogs" class="waves-effect"><i class="fa fa-database"></i> <span> Admin Logs </span></a>

                </li>
                <li class="has_sub">
                    <a href="<?php echo base_url(); ?>Payment" class="waves-effect"><i class="fa fa-money"></i> <span> Payments </span></a>

                </li>
                <li class="has_sub">
                    <a href="<?php echo base_url(); ?>Contactform" class="waves-effect"><i class="fa fa-question-circle"></i> <span> Query Logs </span></a>

                </li>
                <!-- <li class="has_sub">
                    <a href="<?php //echo base_url(); ?>Coupons" class="waves-effect"><i class="fa fa-500px"></i> <span> Coupons </span></a>

                </li> -->
            <?php }
            else
            {
            ?>
            <li class="has_sub">
                    <a href="<?php echo base_url(); ?>dashboard" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>

                </li>
                <?php 
                $getAccess = $this->db->select('houdin_admin_department_access')->from('houdin_admin_department')->where('houdin_admin_department_id',$this->session->userdata('department'))->get()->result();
                $setExplodeData = explode(',',$getAccess[0]->houdin_admin_department_access);
                
                ?>
                  <li class="has_sub" <?php if(!in_array('shop',$setExplodeData)) { ?> style="display:none" <?php  } ?>>
                    <a href="<?php echo base_url(); ?>shop" class="waves-effect"><i class="fa fa-object-group"></i> <span> Shop Management </span> </a>

                </li>
                <li class="has_sub" <?php if(!in_array('package',$setExplodeData)) { ?> style="display:none" <?php  } ?>>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cubes"></i> <span>  Package </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>package">Package Management</a></li>
                        <!-- <li><a href="<?php //echo base_url(); ?>package/plugin">Plugins Management</a></li> -->
                        <li><a href="<?php echo base_url(); ?>package/smspackage">SMS Package</a></li>
                        <li><a href="<?php echo base_url(); ?>package/emailpackage">Email Package</a></li>

                    </ul>
                </li>
                <li class="has_sub" <?php if(!in_array('setting',$setExplodeData)) { ?> style="display:none" <?php  } ?>>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog"></i> <span> Setting </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>setting/language">Language</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/country">Country</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/template">Template</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/businesscategory">Business Category</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/logomanagement">Logo Management</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/smscredentials">Twillio Credentials</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/emailcredentials">Sendgrid Credentias</a></li>
                        <li><a href="<?php echo base_url(); ?>setting/payumoney">Payumoney Credentias</a></li>

                    </ul>
                </li>
                   <li class="has_sub" <?php if(!in_array('setting',$setExplodeData)) { ?> style="display:none" <?php  } ?>>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog"></i> <span>App Setting </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>setting/businesscategory">Business Category</a></li>

                    </ul>
                </li>
                <li class="has_sub" <?php if(!in_array('admin',$setExplodeData)) { ?> style="display:none" <?php  } ?>>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-modx"></i> <span>  Admin Management </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>Adminmanagement/department">Departments</a></li>
                        <li><a href="<?php echo base_url(); ?>Adminmanagement/subadmins">Subadmins</a></li>

                    </ul>
                </li>
                 
                <li class="has_sub" <?php if(!in_array('log',$setExplodeData)) { ?> style="display:none" <?php  } ?>>
                    <a href="<?php echo base_url(); ?>Adminlogs" class="waves-effect"><i class="fa fa-database"></i> <span> Admin Logs </span></a>

                </li>
                <li class="has_sub" <?php if(!in_array('payment',$setExplodeData)) { ?> style="display:none" <?php  } ?>>
                    <a href="<?php echo base_url(); ?>Payment" class="waves-effect"><i class="fa fa-money"></i> <span> Payments </span></a>

                </li>
                <!-- <li class="has_sub">
                    <a href="<?php //echo base_url(); ?>Coupons" class="waves-effect"><i class="fa fa-500px"></i> <span> Coupons </span></a>

                </li> -->
                <li class="has_sub">
                    <a href="<?php echo base_url(); ?>Contactform" class="waves-effect"><i class="fa fa-question-circle"></i> <span> Query Logs </span></a>

                </li>
            <?php }
            ?>
                
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
