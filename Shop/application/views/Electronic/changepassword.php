<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
  <!-- Content -->
  <div id="content"> 
    
    <!-- Linking -->
    <div class="linking">
      <div class="container">
        <ol class="breadcrumb">
          <li class="active">Change Password</li>
        </ol>
      </div>
    </div>
    
    <!-- Blog -->
    <section class="login-sec padding-top-30 padding-bottom-100">
      <div class="container">
        <div class="row">
        <?php 
        if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        else if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        ?>
          <div class="col-md-6 col-md-offset-3"> 
            <!-- Login Your Account -->
            <h5>Change Password</h5>
            <hr>
            <!-- FORM -->
            <?php echo form_open(base_url( 'Changepassword/updatepass' ), array( 'id' => 'changepass', 'method'=>'post' ));?>
              <ul class="row text">
                <li class="col-sm-12">
                  <label>Current password *
                  <input type="password" name="currentPass" class="required_validation_for_change_pass name_validation form-control" />
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>New password *
                  <input type="password" name="newpass" class="required_validation_for_change_pass name_validation opass form-control" />
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Re-enter password *
                  <input type="password" name="newconfirmpass" class="required_validation_for_change_pass name_validation cpass form-control" />
                  </label>
                </li>
                <li class="col-sm-12 text-left">
                <input type="submit" class="btn-round setDisableData" style="    background: #0088cc;" value="update Password"/>
                </li>
              </ul>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </section>
  </div>
    <!-- End Content --> 
    <?php 
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer') 
  ?>
         <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#changepass',function(){
                var check_required_field='';
                $(this).find(".required_validation_for_change_pass").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>