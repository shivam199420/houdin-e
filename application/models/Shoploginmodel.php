<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shoploginmodel extends CI_Model{
    function __construct() {
        $this->userTbl = '';
    }
    public function checkUserLoginData($data)
    {
        $this->db->select('*')->from('houdin_users')->where('houdin_user_email',$data['username']);
        $getData = $this->db->get()->result();
        if(count($getData) > 0)
        {
            $getUserPassword = $data['pass'];
            $passwordSalt = $getData[0]->houdin_user_password_salt;
            $hash_input_user_pass= crypt($getUserPassword,$passwordSalt);
            if($hash_input_user_pass == $getData[0]->houdin_user_password)
            {
                if($getData[0]->houdin_user_is_active == 'active' && $getData[0]->houdin_user_registration_status == '1' && $getData[0]->houdin_user_is_verified == '1')
                {
                    $letters1='abcdefghijklmnopqrstuvwxyz'; 
                    $string1=''; 
                    for($x=0; $x<3; ++$x)
                    {  
                        $string1.=$letters1[rand(0,25)].rand(0,9); 
                    }
                    // update Auth data
                    $insertAuthData = array('houdin_vendor_auth_vendor_id'=>$getData[0]->houdin_user_id,'houdin_vendor_auth_token'=>$string1,'houdin_vendor_auth_url_token'=>$string1);
                    $getInsertStatus = $this->db->insert('houdin_vendor_auth',$insertAuthData);
                    if($getInsertStatus == 1)
                    {
                        if(strtotime($getData[0]->houdin_users_package_expiry) >= strtotime(date('Y-m-d')))
                        {
                            return array('message'=>'success','authData'=>$string1);
                        }
                        else
                        {
                            return array('message'=>'package');
                        }
                    } 
                    else
                    {
                        return array('message'=>'no');    
                    }
                }
                else
                {
                    return array('message'=>'support');
                }
            }
            else
            {
                return array('message'=>'pass');
            }
        }
        else
        {
            return array('message'=>'email');
        }
    }
}
?>