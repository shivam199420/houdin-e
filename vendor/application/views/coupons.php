        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <style type="text/css">
        @media screen and (min-width: 768px){}
        .dropdown.dropdown-lg .dropdown-menu {
        min-width: 390px!important;
        }
        }
        </style>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">

        <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Coupons</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>coupons/Couponslanding">Coupons</a></li>
                  <li class="active">Coupons</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                
            </div>
           <!--END Page-Title --> 
        <div class="row m-t-20">

        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Coupon.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($totalCouponsData) { echo $totalCouponsData; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total coupons</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Coupon.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($totalActiveCoupons) { echo $totalActiveCoupons; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total Active Coupons</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Coupon.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($totalDeactiveCoupons) { echo $totalDeactiveCoupons; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total Deactive Coupons</div>
        </div>
        </div>


        </div>

        
        <div class="row">
        <div class="col-md-12">

        <div class="card-box table-responsive"> 
        <div class="btn-group pull-right m-t-10 m-b-20">

        <a href="<?php echo base_url(); ?>Coupons/add" class="btn btn-default m-r-5" title="Add coupon"><i class="fa fa-plus"></i></a>

        <button type="button" class="btn btn-default m-r-5 setSupplierMultiBtn deleteCopounBtn" style="display:none" title="Delete"><i class="fa fa-trash"></i></button>
        <a href="<?php echo base_url(); ?>Coupons/generatecouponpdf" target="_blank" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>

        </div>
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th><input type="checkbox" class="masterSupplierCheck"></th>
        <th>Coupon name</th>
        <th>Coupon Code</th>
        <th>Valid From - To</th>
        <th>Discount</th>
        <th>Status</th>  
        <th>Action</th>

        </tr>

        </thead>
        <tbody>
        <?php 
        foreach($couponsLsit as $couponsLsitData)
        {
        ?>   
        <tr>
        <td><input type="checkbox" class="childSupplierCheck" data-id="<?php echo $couponsLsitData->houdinv_coupons_id ?>"></td>
        <td><a href="<?php echo base_url(); ?>coupons/view/<?php echo $couponsLsitData->houdinv_coupons_id ?>"><?php echo $couponsLsitData->houdinv_coupons_name ?></a></td>
        <td><?php echo $couponsLsitData->houdinv_coupons_code ?></td>
        <td><?php echo date('d-m-Y',strtotime($couponsLsitData->houdinv_coupons_valid_from))." - ".date('d-m-Y',strtotime($couponsLsitData->houdinv_coupons_valid_to)); ?></td> 
        <td><?php echo $couponsLsitData->houdinv_coupons_discount_precentage ?>%</td>
        <td><?php  
        if($couponsLsitData->houdinv_coupons_status == 'active')
        {
            $setText = "Active";
            $setColor = "success";
        }
        else if($couponsLsitData->houdinv_coupons_status == 'deactive')
        {
            $setText = "Deactive";
            $setColor = "warning";
        }
        ?>
    <button type="button" class="btn btn-xs btn-<?php echo $setColor ?>"><?php echo $setText; ?></button>    
    </td>
        <td>
        <a href="<?php echo base_url(); ?>Coupons/edit/<?php echo $couponsLsitData->houdinv_coupons_id ?>"><button class="btn btn-success waves-effect waves-light" >Edit</button></a>

        <button data-id="<?php echo $couponsLsitData->houdinv_coupons_id ?>" class="btn btn-success waves-effect waves-light deleteCopounBtn">Delete</button>


        <!-- <button data-id="<?php //echo $couponsLsitData->houdinv_coupons_id ?>" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#send">Send Email</button>

        <button data-id="<?php //echo $couponsLsitData->houdinv_coupons_id ?>" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#text">Send Text</button> -->
        </td>
        </tr>
        <?php }
        ?>
        </tbody>
        </table>
        <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
        </div>
        </div>
        </div>


        </div>
        </div>
        </div>
        <!-- Delete coupons modal -->
        <div id="deleteCouponsModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Coupons' ), array('method'=>'post' ));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title"></h4>

        </div>

        <div class="modal-body">

        <div class="row">
        <div class="col-md-12">
            <input type="hidden" class="deleteCouponId" name="deleteCouponId"/>
        <h4><b>Do you really want to Delete this coupon ?</b></h4>
        </div>
        </div>
        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info" name="deleteCouponsData" value="Delete">

        </div>

        </div>
        <?php echo form_close(); ?>
        </div>

        </div>
        <!---Start Send -->
        <!-- <div id="send" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Send Email</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12 form-horizontal"> 
        <div class="form-group">
        <label class="col-md-3 control-label">Subject</label>
        <div class="col-md-9">
        <input type="text" value="" class="form-control" name="" placeholder="Subject">
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-3 control-label">Message</label>
        <div class="col-md-9">
        <textarea style="width:100%" type="text"  value="" class="form-control" name="" placeholder="Message"></textarea>
        </div>
        </div>  
        </div> 

        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <button type="button" class="btn btn-info waves-effect waves-light">Send</button> 
        </div> 
        </div> 
        </div>
        </div> 
 
        <div id="text" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Send Text</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12 form-horizontal"> 
        <div class="form-group">
        <label class="col-md-3 control-label">Subject</label>
        <div class="col-md-9">
        <input type="text" value="" class="form-control" name="" placeholder="Subject">
        </div>
        </div>

        <div class="form-group">
        <label class="col-md-3 control-label">Message</label>
        <div class="col-md-9">
        <textarea style="width:100%" type="text"  value="" class="form-control" name="" placeholder="Message"></textarea>
        </div>
        </div>  
        </div> 

        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <button type="button" class="btn btn-info waves-effect waves-light">Send</button> 
        </div> 
        </div> 
        </div>
        </div> -->
        <!-- End Text -->
        <?php $this->load->view('Template/footer.php') ?>
