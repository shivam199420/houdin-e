<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Logout extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopsettingmodel');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $this->session->unset_userdata('userAuth');
        redirect(base_url()."Register", 'refresh');
    }
    public function visitorCount()
    {
        $this->Shopsettingmodel->updateVisitorCount();
    }
}
