<?php
class Paymentmodel extends CI_Model
{
    function __construct()
    {  
        parent::__construct();
    } 
    public function getTransactionData($data)
    {
        $getTotalTransactionData = $this->db->select('SUM(houdin_admin_transaction_amount) as TotalTransaction')->from('houdin_admin_transaction')->where('houdin_admin_transaction_status','success')->get()->result();
        $getTotalCreditData = $this->db->select('SUM(houdin_admin_transaction_amount) as TotalCreditTransaction')->from('houdin_admin_transaction')->where('houdin_admin_transaction_status','success')->where('houdin_admin_transaction_type','credit')->get()->result();
        $getTotalDebitData = $this->db->select('SUM(houdin_admin_transaction_amount) as TotalDebitTransaction')->from('houdin_admin_transaction')->where('houdin_admin_transaction_status','success')->where('houdin_admin_transaction_type','debit')->get()->result();
        
        $setDateData = "";
        $getTotalTransaction = "";
        $getTotalCredit = "";
        $getTotalDebit = "";
        for($index = 0; $index < 11; $index++)
        {
            $getDateData = date('Y-m-d', strtotime('-'.$index.' days'));
            $getDateDataValue = date('d-m-Y',strtotime($getDateData));
            // get total transaction
            $getTotalAmount = $this->db->select('SUM(houdin_admin_transaction_amount) as totalTransaction')->from('houdin_admin_transaction')
            ->where('houdin_admin_transaction_status','success')->where('houdin_admin_transaction_date',$getDateData)->get()->result();
            if($getTotalAmount[0]->totalTransaction)
            {
                $setTotalAmount = $getTotalAmount[0]->totalTransaction;
            }
            else
            {
                $setTotalAmount = 0;
            }
            // get total credit transaction
            $getTotalCreditAmount = $this->db->select('SUM(houdin_admin_transaction_amount) as totalCreditTransaction')->from('houdin_admin_transaction')
            ->where('houdin_admin_transaction_status','success')->where('houdin_admin_transaction_type','credit')
            ->where('houdin_admin_transaction_date',$getDateData)->get()->result();
            if($getTotalCreditAmount[0]->totalCreditTransaction)
            {
                $setTotalCredit = $getTotalCreditAmount[0]->totalCreditTransaction;
            }
            else
            {
                $setTotalCredit = 0;
            }
            // get total debit transaction
            $getTotalDebitAmount = $this->db->select('SUM(houdin_admin_transaction_amount) as totalDebitTransaction')->from('houdin_admin_transaction')
            ->where('houdin_admin_transaction_status','success')->where('houdin_admin_transaction_type','debit')
            ->where('houdin_admin_transaction_date',$getDateData)->get()->result();
            if($getTotalDebitAmount[0]->totalDebitTransaction)
            {
                $setTotalDebit = $getTotalDebitAmount[0]->totalDebitTransaction;
            }
            else
            {
                $setTotalDebit = 0;
            }

            if($setDateData)
            {
                $setDateData = $setDateData.","."'".$getDateDataValue."'";
            }
            else
            {
                $setDateData = "'".$getDateDataValue."'";
            }
            // get total transaction
            if($getTotalTransaction)
            {
                $getTotalTransaction = $getTotalTransaction.",".$setTotalAmount;
            }
            else
            {
                $getTotalTransaction = $setTotalAmount;
            }
            // get total credit
            if($getTotalCredit)
            {
                $getTotalCredit = $getTotalCredit.",".$setTotalCredit;
            }
            else
            {
                $getTotalCredit = $setTotalCredit;
            }
            // get total debit
            if($getTotalDebit)
            {
                $getTotalDebit = $getTotalDebit.",".$setTotalDebit;
            }
            else
            {
                $getTotalDebit = $setTotalDebit;
            }
        }
        // get total transaction
        $this->db->select('*')->from('houdin_admin_transaction');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getTransactiondata = $this->db->get();
        $resultTransactionData = ($getTransactiondata->num_rows() > 0)?$getTransactiondata->result():FALSE;
        return array(
            'totalTransactionData'=>$getTotalTransactionData[0]->TotalTransaction,
            'totalCreditTransaction'=>$getTotalCreditData[0]->TotalCreditTransaction,
            'totalDebitTransaction'=>$getTotalDebitData[0]->TotalDebitTransaction,
            'date'=>$setDateData,
            'totalTransaction'=>$getTotalTransaction,
            'totalCredit'=>$getTotalCredit,
            'totalDebit'=>$getTotalDebit,
            'transactionData'=>$resultTransactionData
        );
    }
    public function getTransactionTotalCount()
    {   
        $getTotalData = $this->db->select('COUNT(houdin_admin_transaction_id) as TotalTransaction')->from('houdin_admin_transaction')->get()->result();
        return array('totalRows'=>$getTotalData[0]->TotalTransaction);
    }
}
