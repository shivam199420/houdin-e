        <?php
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/Template/header'); ?>

        <!-- Page item Area -->
        <div id="page_item_area">
        <div class="container">
        <div class="row">
        <div class="col-sm-6 text-left">
        <h3>Chnage password</h3>
        </div>
        </div>
        </div>
        </div>


        <!-- Login Page -->
        <div class="login_page_area">
        <div class="container">
        <div class="row">
        <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="create_account_area">
        <h2 class="caa_heading">Change Password</h2>
        <?php
        if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        else if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        ?>
        <div class="caa_form_area">
        <p>Please enter the following details.</p>
        <div class="caa_form_group">
        <?php echo form_open(base_url( 'Changepassword/updatepass' ), array( 'id' => 'changepass', 'method'=>'post' ));?>
        <div class="caf_form">
        <label>Current Password</label>
        <input type="password" name="currentPass" class="required_validation_for_change_pass name_validation form-control" />
        </div>
        <div class="caf_form">
        <label>New Password</label>
        <input type="password" name="newpass" class="required_validation_for_change_pass name_validation opass form-control" />
        </div>
        <div class="caf_form">
        <label>Confirm Password</label>
        <input type="password" name="newconfirmpass" class="required_validation_for_change_pass name_validation cpass form-control" />
        </div>
        <div class="col-md-12 text-center">
            <input type="submit" class="btn btn-default acc_btn text-center setDisableData" value="update Password"/>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <?php
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/Template/footer'); ?>

        <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#changepass',function(){
                var check_required_field='';
                $(this).find(".required_validation_for_change_pass").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>
