<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style type="text/css">
    .widget-style-2 i {
    background: rgba(244, 248, 251, 0.6) !important;
    font-size: 48px;
    padding: 30px 8px;
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
              <!--START Page-Title -->
            <div class="row">
            
                <div class="col-md-8">
                   <h4 class="page-title">View Gift voucher</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                 
                   <li><a href="<?php echo base_url(); ?>giftvouchers">Gift voucher</a></li>
                  <li class="active">View Gift voucher</li>
                  </ol>
                  </div>
              
            </div>
           <!--END Page-Title --> 
          
             
        <div class="row m-t-20">
       
        <div class="col-md-12">
            <div class="card-box no-pad no-margin">
                 <div class="row"> 
                        
                            <div class="col-lg-12"> 
                                <ul class="nav nav-tabs navtab-bg nav-justified"> 
                                    <li class="active"> 
                                        <a href="#home1" data-toggle="tab" aria-expanded="false"> 
                                            <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                            <span class="hidden-xs">Customer information</span> 
                                        </a> 
                                    </li> 
                                    <li class=""> 
                                        <a href="#profile1" data-toggle="tab" aria-expanded="true"> 
                                            <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                            <span class="hidden-xs">Gift voucher detail</span> 
                                        </a> 
                                    </li> 
                                    <li class=""> 
                                        <a href="#messages1" data-toggle="tab" aria-expanded="false"> 
                                            <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> 
                                            <span class="hidden-xs">Usage of gift voucher</span> 
                                        </a> 
                                    </li> 
                                   
                                </ul> 
                                <div class="tab-content"> 
                                    <div class="tab-pane active" id="home1"> 
                                       
                                        <div class="row">
                                                 <div class="container">
                                                   <div class="table-responsive"> 
                                                  <table class="table table-striped table-bordered table_shop_custom">
                                                    <thead>
                                                      <tr>
                                                      
                                                        <th><input type="checkbox"></th>
                                                        <th>Customer ID</th>
                                                        <th>Customer name</th>
                                                        <th>DOB</th>
                                                        <th>Email</th>
                                                        <th>Payment Status</th>
                                                        <th>Action</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td><input type="checkbox"></td>
                                                        <td>H124568</td>
                                                        <td>Electronic</td> 
                                                        <td>DD/MM/YYYY</td>
                                                        <td>Cash</td>
                                                        <td>Pending</td>
                                                        <td><button type="button" class="btn btn-success btn btn-xs">Invoice</button></td>
                                                      
                                                      </tr>
                                                    </tbody>
                              </table>
                            </div>
                              </div>
                              </div>
                                    </div> 
                                    <div class="tab-pane" id="profile1"> 
                                        
                                        <div class="row">
                                                 <div class="container">
                                                   <div class="table-responsive"> 
                                                 	
                                                  <table class="table table-striped table-bordered table_shop_custom">
                                                    <thead>
                                                      <tr>
                                                      
                                                        <th><input type="checkbox"></th>
                                                        <th>Gift voucher ID</th>
                                                        <th>Product name</th>
                                                        <th>Voucher Type</th>
                                                        <th>Vadit till date</th>
                                                        
                                                        
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td><input type="checkbox"></td>
                                                        <td>H124568</td>
                                                        <td>Electronic</td> 
                                                        <td>Cloths only</td>
                                                        <td>DD/MM/YYYY</td>
                                                       
                                                        
                                                        
                                                      
                                                      </tr>
                                                    </tbody>
                              </table>
                            </div>
                              </div> 
                              </div>
                                    </div> 
                                    <div class="tab-pane" id="messages1"> 
                                       <div class="table-responsive"> 
                                      
                                   <div class="row">
                                                 <div class="container">

                                                  <table class="table table-striped table-bordered  table_shop_custom">
                                                    <thead>
                                                      <tr>
                                                      
                                                        <th><input type="checkbox"></th>
                                                        <th>Gift voucher ID</th>
                                                        <th>Voucher Vaid from</th>
                                                        <th>Voucher Vaid Till</th>
                                                        <th>Voucher Status</th>
                                                        
                                                        
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td><input type="checkbox"></td>
                                                        <td>H124568</td>
                                                        <td>DD/MM/YYYY</td> 
                                                        <td>DD/MM/YYYY</td>
                                                        <td>Pending</td>
                                                       
                                                        
                                                        
                                                      
                                                      </tr>
                                                    </tbody>
                              </table>
                            </div>
                              </div> 
                              </div>
                                    </div> 
                                    
                                </div> 

                                               
                            </div> 
                        </div>
                         <!-- end row --> 
            </div>
        </div>
        </div>

                                       <div id="con-close-modal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Add Inventory</h4> 
                                                </div> 
                                                <div class="modal-body"> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label">Choose Status</label> 
                                                                <select class="form-control">
                                                                    <option>Receive</option>
                                                                    <option>Return</option>
                                                                    
                                                                </select>
                                                            </div> 
                                                        </div> 
                                                     
                                                    </div> 
                                                    <!--<div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-3" class="control-label">Note</label> 
                                                                <input type="text" class="form-control" id="field-3" placeholder="Quantity"> 
                                                            </div> 
                                                        </div> 
                                                    </div> -->
                                                  
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group no-margin"> 
                                                                <label for="field-7" class="control-label">Note</label> 
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                                </div> 
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->
                              
      </div>
    </div>
  </div>
<!--Delete-->

  

<!--change Status-->


        

<?php $this->load->view('Template/footer.php') ?>
  <script>
            var resizefunc = [];
        </script>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
