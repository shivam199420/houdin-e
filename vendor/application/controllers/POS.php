<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class POS extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->auth_users->is_logged_in();
        $this->load->model('posmodel');
        $this->load->model('Staffmodel');
        $this->load->model('Productmodel');
    }
    public function index()
    {
        $getPosData = $this->posmodel->fetchPosData();
        $getPosData['WareHouse']= $this->Staffmodel->getWarhouse();
        $product_data['taxData'] = $this->Productmodel->getTaxData();
        $getPosData['possetting'] = fetchPosSetting();
        $getPosData['possetting'] = fetchPosSetting();
        $this->load->view('pos',$getPosData);
    }

    public function index_new()
    {
        $getPosData = $this->posmodel->fetchPosData();
        $getPosData['possetting'] = fetchPosSetting();
        $this->load->view('pos_new',$getPosData);
    }
    // check coupon validity
    public function getCouponValidity()
    {
        $this->form_validation->set_rules('coupon','text','required');
        $this->form_validation->set_rules('customerId','text','required');
        $this->form_validation->set_rules('getNetPayable','text','required');
        if($this->form_validation->run() == true)
        {
            $setCouponArray = array('code'=>$this->input->post('coupon'),'totalAmount'=>$this->input->post('getNetPayable'),'customerId'=>$this->input->post('customerId'));
            $isexist = $this->posmodel->IsCouponExist($setCouponArray);  
            print_r(json_encode($isexist));
        }
        else
        {
            $resultData = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultData));
        }
    }
    // get customer address
    public function getCustomerAddress()
    {
        $this->form_validation->set_rules('customerId','text','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('id'=>$this->input->post('customerId'));
            $getAddressdata = $this->posmodel->fetchUserAddressApp($setArray);
            print_r(json_encode($getAddressdata));
        }
        else
        {
            $resultData = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultData));
        }
    }
    public function addRegisteredUserAddress()
    {
        $this->form_validation->set_rules('userName','Name','required');
        $this->form_validation->set_rules('userPhone','Phone','required');
        $this->form_validation->set_rules('userMainAddress','Address','required');
        $this->form_validation->set_rules('userCity','City / Town','required');
        $this->form_validation->set_rules('userZip','Pin Code','required');
        $this->form_validation->set_rules('userCountry','Country','required');
        $this->form_validation->set_rules('addAddressCustomerId','Country','required');
        if($this->form_validation->run() == true)
        {
            $setData = strtotime(date('Y-m-d'));
            $setInsertArray = array('houdinv_user_address_user_id'=>$this->input->post('addAddressCustomerId'),'houdinv_user_address_name'=>$this->input->post('userName'),
            'houdinv_user_address_phone'=>$this->input->post('userPhone'),'houdinv_user_address_user_address'=>$this->input->post('userMainAddress'),'houdinv_user_address_city'=>$this->input->post('userCity'),
            'houdinv_user_address_zip'=>$this->input->post('userZip'),'houdinv_user_address_country'=>$this->input->post('userCountry'),'houdinv_user_address_created_at'=>$setData
            );
            $getInsertStatus = $this->posmodel->addUserAddressDataApp($setInsertArray);
            if($getInsertStatus['message'] == 'yes')
            {
                $resultArray = array('messgae'=>'Address is addedd successfully');
                print_r(json_encode($resultArray));
            }
            else
            {
                $resultArray = array('messgae'=>'Something went wrong. Please try again');
                print_r(json_encode($resultArray));
            }
        }
        else
        {
            $resultArray = array('messgae'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
    public function getCustomerPrivilage()
    {
        $this->form_validation->set_rules('customerId','text','required');
        if($this->form_validation->run() == true)
        {
            $getData = $this->posmodel->getCustomerPriv($this->input->post('customerId'));
            print_r(json_encode($getData));
        }
        else
        {
            $resultArray = array('messgae'=>'Please choose Customer first');
            print_r(json_encode($resultArray));
        }
    }
    public function addOrderData()
    {
        $setPendingAmountArray = array();
        $getUserType = $this->input->post('customerType');
        $getDeliveryType = $this->input->post('deliveryType');
       /*  if($getUserType == 'RC')
        {
            $this->form_validation->set_rules('registeredCustomerId','Customer Name','required|integer');
            if($getDeliveryType == 'HD')
            {
                $this->form_validation->set_rules('setRegisteredCustomerAddress','Address','required');
            }
        }
        else
        {
            $this->form_validation->set_rules('setNonRegisteredCustomerName','customer name','required');
            $this->form_validation->set_rules('setNonRegisteredCustomerContact','customer contanct','required');
            if($getDeliveryType == 'HD')
            {
                $this->form_validation->set_rules('setNonRegisteredCustomerContact','Address','required');
            }
        } */
        $this->form_validation->set_rules('paymentStatus','payment status','required|in_list[1,0]');
        if($this->form_validation->run() == true)
        {
            // setMain product array
            $setData = strtotime(date('Y-m-d'));
            $setProductArray = array();
            $setOtherUserData = array();
            $setTransactionArray = array();
            $getProductId = $this->input->post('inputProdcutId');
            $getVariantId = $this->input->post('inputVariantId');
            $getQuantity = $this->input->post('inputQuantity');
            $getActualPrice = $this->input->post('inputActualPrice');
            $getDiscountData = $this->input->post('inputDiscountedPrice');
            $getTotalPrice = $this->input->post('inputTotalPrice');   
            $getSpecialPrice = $this->input->post('specialPrice');
            $gettax = $this->input->post('inputtotaltax');
            // 'saleprice'=>$data['saleprice'][$index],'totaltax'=>$data['totalTax'][$index]
            for($index = 0; $index < count($getQuantity); $index++)
            {
                $this->posmodel->productStockmanage( array("product_id"=>$getProductId[$index],'variant_id'=>$getVariantId[$index],'product_Count'=>$getQuantity[$index]));
           
                $setProductArray[] = array("product_id"=>$getProductId[$index],'variant_id'=>$getVariantId[$index],'product_Count'=>$getQuantity[$index],
                'product_actual_price'=>$getActualPrice[$index],'total_product_paid_Amount'=>$getTotalPrice[$index],
                'discount'=>$getDiscountData[$index],'saleprice'=>$getActualPrice[$index],'totaltax'=>$gettax[$index]);
           
             
            } 
            $setUserId = $this->input->post('customer_id');
            /* if($getUserType == 'RC')
            {
                $setUserId = $this->input->post('registeredCustomerId');
            }
            else
            {
                $setOtherUserData = array('name'=>$this->input->post('setNonRegisteredCustomerName'),'contact'=>$this->input->post('setNonRegisteredCustomerContact'),'address'=>$this->input->post('setNonRegisteredCustomerAddress'));
                $setUserId = 0;
            } */
            $fetchPosSetting = fetchPosSetting();
            if($getDeliveryType == 'HD')
            {
                $setDeliveryType = 'deliver';
                $setOrderStatus = 'billed';
                $setDeliveryStatus = 0;
                if($fetchPosSetting[0]->delivery_charges)
                {
                    if($fetchPosSetting[0]->free_delivery == 1)
                    {
                        $getFreeDelivery = $fetchPosSetting[0]->free_delivery_val;
                        $getNetPayable = $this->input->post('finalAmountData')-$this->input->post('finalDiscountData');
                        if($getNetPayable < $getFreeDelivery)
                        {
                            $setDeliveryCharges = $fetchPosSetting[0]->delivery_charges;
                        }
                        else
                        {
                            $setDeliveryCharges = 0;
                        }
                    }
                    else
                    {
                        $setDeliveryCharges = $fetchPosSetting[0]->delivery_charges;
                    }
                }
                else
                {
                    $setDeliveryCharges = 0;
                }
            }
            else
            {
                $setDeliveryType = 'pick_up';
                $setOrderStatus = 'Delivered';
                $setDeliveryStatus = 1;
                $setDeliveryCharges = 0;
            }
            if($this->input->post('couponCodeId'))
            {
                $setCouponCode = $this->input->post('couponCodeId');
            }
            else
            {
                $setCouponCode = 0;
            }
            if($this->input->post('setRegisteredCustomerAddress'))
            {
                $setAddressId = $this->input->post('setRegisteredCustomerAddress');
            }
            else
            {
                $setAddressId = 0;
            }
            // additional adjustment
            if($this->input->post('paymentStatus') == 1)
            {
                if($this->input->post('pendingAmountData')>0)
                {
                    $getPendingAmount = $this->input->post('pendingAmountData');
                    $totalPaidAmount = $this->input->post('netPayableAmount');
                    $setPendingAmountArray = array('pendingAmount'=>$getPendingAmount,'userId'=>$this->input->post('registeredCustomerId'));
                }
                else
                {
                    $getPendingAmount = 0;
                    $totalPaidAmount = $this->input->post('netPayableAmount');
                }
                if($this->input->post('paymentOption') == 'cash' || $this->input->post('paymentOption') == 'cheque')
                {
                    $setTransactionMethod = 'cash';
                }
                else
                {
                    $setTransactionMethod = 'online';
                }
                $setTransactionArray = array('houdinv_transaction_type'=>'credit','houdinv_transaction_method'=>$setTransactionMethod,
                'houdinv_transaction_from'=>'pos','houdinv_transaction_for'=>'order','houdinv_transaction_amount'=>$this->input->post('netPayableAmount'));
            }
            else
            {
                $getPendingAmount = $this->input->post('netPayableAmount');
                $totalPaidAmount = 0;
            }
            $getUserId = fetchoutletDetailsData();
            $setOrderInsertArray = array('houdinv_order_user_id'=>$setUserId,'houdinv_order_product_detail'=>json_encode($setProductArray),
            'houdinv_confirm_order_product_detail'=>json_encode($setProductArray),
            'houdinv_order_confirm_outlet'=>$this->session->userdata('vendorOutlet'),
            'houdinv_orders_confirm_by'=>$getUserId['userId'],
            'houdinv_orders_confirm_by_role'=>$this->session->userdata('vendorRole'),
            'houdinv_order_type'=>'Store',
            'houdinv_order_delivery_type'=>$setDeliveryType,
            'houdinv_order_payment_method'=>$this->input->post('paymentOption'),
            'houdinv_payment_status'=>$this->input->post('paymentStatus'),
            'houdinv_orders_discount'=>$this->input->post('finalDiscountData'),
            'houdinv_orders_discount_detail_id'=>$setCouponCode,
            'houdinv_orders_total_Amount'=>$this->input->post('finalAmountData'),
            'houdinv_order_comments'=>$this->input->post('customerNote'),
            'houdinv_orders_deliverydate'=>date('Y-m-d'),
            'houdinv_order_confirmation_status'=>$setOrderStatus,'houdinv_orders_delivery_status'=>$setDeliveryStatus,
            'houdinv_orders_deliverydate'=>date('Y-m-d'),
            'houdinv_order_delivery_address_id'=>$setAddressId,
            'houdinv_order_billing_address_id'=>$setAddressId,
            'houdinv_order_created_at'=>$setData,'houdinv_delivery_charge'=>$setDeliveryCharges,'houdinv_orders_total_paid'=>$totalPaidAmount,
            'houdinv_orders_total_remaining'=>$getPendingAmount);
            $getDataStatus = $this->posmodel->setOrderData($setOrderInsertArray,$setOtherUserData,$setPendingAmountArray,$setTransactionArray);
            if($getDataStatus['message'] == 'yes')
            {
                $this->session->set_flashdata('success','order generated successfully <a href="'.base_url().'order/printinvoicestatement/'.$getDataStatus['orderId'].'" target="_blank">Print your invoice</a>');
                $this->load->helper('url');
                redirect(base_url()."POS", 'refresh');

            }
            else if($getDataStatus['message'] == 'user')
            {
                $this->session->set_flashdata('error','order generated successfully. something went wrong while adding user');
                $this->load->helper('url');
                redirect(base_url()."POS", 'refresh');
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."POS", 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            $this->load->helper('url');
            redirect(base_url()."POS", 'refresh');
        }
    }
    public function printinvoice()
    {
        $this->load->view('printinvoice');
    }

    // get customer address
    public function checkCustomerData()
    {
        $setArray = array('houdinv_user_contact'=>$this->input->post('mobile_number'));
        $getAddressdata = $this->posmodel->getCustomer($setArray);
        print_r(json_encode($getAddressdata));
    }

    public function addCustomer()
    {
        $this->form_validation->set_rules('mobile_number','Mobile Number','required|is_unique[houdinv_users.houdinv_user_contact]');
        $this->form_validation->set_rules('name','Customer Name','required');
        $this->form_validation->set_rules('email','Customer Name','required');
        if ($this->form_validation->run() == true) {
            $set_data = [
                'houdinv_user_name' => $this->input->post('name'),
                'houdinv_user_email' => $this->input->post('email'),
                'houdinv_user_contact' => $this->input->post('mobile_number'),
            ];
            $data = $this->posmodel->addCustomerData($set_data);
        } else {
            $data = ['status' => '402', 'message' => 'All fields are mandatory'];
        }
        print_r(json_encode($data));
    }

    public function getProduct()
    {
        print_r($this->input->post('q'));
    }
}


