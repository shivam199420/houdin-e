<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Werbserviceuserorder extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopuserordermodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function fetchUserWishlist()
    {
        $this->form_validation->set_rules('userId','text','required');
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $setWishlistArray = array('id'=>$this->input->post('userId'),'shop'=>$this->input->post('shopName'));
            $getWishlistData = $this->Shopuserordermodel->fetchUserWishlist($setWishlistArray);
            print_r(json_encode($getWishlistData));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function fetchcoupons()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getCouponsData = $this->Shopuserordermodel->fetchUserCoupons($this->input->post('shopName'));
            print_r(json_encode($getCouponsData));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function fetchUserCart()
    {
        $this->form_validation->set_rules('userId','text','required');
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $setCartArray = array('id'=>$this->input->post('userId'),'shop'=>$this->input->post('shopName'));
            $getCartItem = $this->Shopuserordermodel->fetchUserCartData($setCartArray);
            print_r(json_encode($getCartItem));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }    
    }
    public function checkout()
    {
        $this->form_validation->set_rules('userId','text','required');
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $setCheckoutArray = array('shopName'=>$this->input->post('shopName'),'id'=>$this->input->post('userId'));
            $getCheckoutFirst = $this->Shopuserordermodel->firstCheckOut($setCheckoutArray);
            print_r(json_encode($getCheckoutFirst));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function checkoutorder()
    {
        $this->form_validation->set_rules('userId','text','required');
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('billingAdd','text','required|integer');
        $this->form_validation->set_rules('shippingAdd','text','required|integer');
        $this->form_validation->set_rules('deliveryType','text','required|in_list[deliver,pick_up]');
        $this->form_validation->set_rules('paymentMethod','text','required');
        if($this->form_validation->run() == true)
        {
            $setArrayOrderData = array('id'=>$this->input->post('userId'),'shopName'=>$this->input->post('shopName'),'billingAdd'=>$this->input->post('billingAdd'),'shippingAdd'=>$this->input->post('shippingAdd'),'deliveryType'=>$this->input->post('deliveryType'),'paymentMethod'=>$this->input->post('paymentMethod'),'customerComment'=>$this->input->post('customerComment'),'couponId'=>$this->input->post('couponId'),'discount'=>$this->input->post('discount'));
            $getOrderStatus = $this->Shopuserordermodel->finalCheckOutData($setArrayOrderData);
            // send notifincation
            $getDeviceId = updategetUserDevice($this->input->post('shopName'),$this->input->post('userId'));
            if($getDeviceId[0]->houdinv_users_device_id)
            {
                define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                $to=$getDeviceId[0]->houdinv_users_device_id;
                $message= $getMessage;
                $msg = array(
                'title'     => '',
                'body'=> 'Your order is suuccessfully placed',
                'sound' => 'mySound'/*Default sound*/
                );
                $fields = array
                (
                    'to'            => $to,
                    'notification'  => $msg,
                    'priority'      =>'high',
                );
                $headers = array
                (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                );
                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt( $ch,CURLOPT_POST, true );
                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                $result = curl_exec($ch );
                curl_close( $ch );
                shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                $jsonDecodeData = json_decode($result,true);
            }
                print_r(json_encode($getOrderStatus));
        }
        else
        {
            $resultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
    // remove product from wish list
    public function removeproductwishlist()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('wishlistId','text','required');
        if($this->form_validation->run() == true)
        {
            $setWishlistArray = array('shopName'=>$this->input->post('shopName'),'wishlistId'=>$this->input->post('wishlistId'));
            $resultArray = $this->Shopuserordermodel->removeProductWishlistData($setWishlistArray);
            print_r(json_encode($resultArray));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
     // remove product from Cart
     public function removeproductcart()
     {
         $this->form_validation->set_rules('shopName','text','required');
         $this->form_validation->set_rules('cartid','text','required');
         if($this->form_validation->run() == true)
         {
             $setCartArray = array('shopName'=>$this->input->post('shopName'),'cartid'=>$this->input->post('cartid'));
             $resultArray = $this->Shopuserordermodel->removeProductcartData($setCartArray);
             print_r(json_encode($resultArray));
         }
         else
         {
             $resultArray = array('message'=>'Something went wrong. Please try again');
             print_r(json_encode($resultArray));
         }
     }
    //  fetch user order
    public function userorder()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('userid','text','required');
        if($this->form_validation->run() == true)
         {
             $setOrderArray = array('shopName'=>$this->input->post('shopName'),'userid'=>$this->input->post('userid'));
             $resultArray = $this->Shopuserordermodel->fetchUserOrders($setOrderArray);
             print_r(json_encode($resultArray));
         }
         else
         {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
         }
    }
    public function setTransaction()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('orderId','text','required|integer');
        $this->form_validation->set_rules('amount','text','required|integer');
        $this->form_validation->set_rules('transactionstatus','text','required|integer');
        $this->form_validation->set_rules('transactionid','text','required');
        if($this->form_validation->run() == true)
        {
            $setTransactionStatus = $this->input->post('transactionstatus');
            if($setTransactionStatus == 1)
            {
                $setStatus = 'success';
                $setOrderArray = array('houdinv_payment_status'=>"1",'houdinv_orders_total_paid'=>$this->input->post('amount'),'houdinv_orders_total_remaining'=>'0');
                $setTransactionArray = array('houdinv_transaction_transaction_id'=>$this->input->post('transactionid'),'houdinv_transaction_type'=>'credit',
                'houdinv_transaction_method'=>'online','houdinv_transaction_from'=>'payumoney','houdinv_transaction_for'=>'order',
                'houdinv_transaction_for_id'=>$this->input->post('orderId'),'houdinv_transaction_amount'=>$this->input->post('amount'),'houdinv_transaction_date'=>date('Y-m-d'),'houdinv_transaction_status'=>$setStatus);
            }
            else
            {
                $setOrderArray = array();
                $setStatus = 'fail';
                $setTransactionArray = array('houdinv_transaction_transaction_id'=>$this->input->post('transactionid'),'houdinv_transaction_type'=>'credit',
                'houdinv_transaction_method'=>'online','houdinv_transaction_from'=>'payumoney','houdinv_transaction_for'=>'order',
                'houdinv_transaction_for_id'=>$this->input->post('orderId'),'houdinv_transaction_amount'=>$this->input->post('amount'),'houdinv_transaction_date'=>date('Y-m-d'),'houdinv_transaction_status'=>$setStatus);
            }
            $getTransaction = $this->Shopuserordermodel->updateOrderTransaction($setOrderArray,$setTransactionArray,$this->input->post('shopName'),$this->input->post('orderId'));
            print_r(json_encode($getTransaction));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function orderTrackingDetail()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('orderId','text','required');
        if($this->form_validation->run() == true)
        {
            $getOrderStatus = $this->Shopuserordermodel->getTrackingDetails($this->input->post('shopName'),$this->input->post('orderId'));
            print_r(json_encode($getOrderStatus));
        }
        else
        {
            $resultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
    public function orderdetail()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('orderId','text','required');
        if($this->form_validation->run() == true)
        {
            $getOrderStatus = $this->Shopuserordermodel->getOrderDetails($this->input->post('shopName'),$this->input->post('orderId'));
            print_r(json_encode($getOrderStatus));
        }
        else
        {
            $resultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
    public function getDeliveryCharges()
    {
        $this->form_validation->set_rules('amount','text','required');
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('amount'=>$this->input->post('amount'),'shopName'=>$this->input->post('shopName'));
            $getData = $this->Shopuserordermodel->fetchDeliveryCharge($setArray);
            print_r(json_encode($getData));
        }
        else
        {
            $resultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
}

