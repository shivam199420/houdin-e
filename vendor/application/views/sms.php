<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">SmS</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li><a href="<?php echo base_url(); ?>Template/Templatelanding">Template</a></li>
                  <li class="active">SmS</li>
                  </ol>
                  </div>

            </div>
            <?php
                                    
            if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
   // print_r($all_data);
    ?>
           <!--END Page-Title --> 
           
                                    <div class="row m-t-20">
        <div class="col-sm-12">
        <div class="card-box">
      
                                <div class="panel-group panel-group-joined" id="accordion-test"> 
               
               
               
                                    <div class="panel panel-default"  style="margin-bottom: 10px;"> 
                                        <div class="panel-heading"> 
                                            <h4 class="panel-title"> 
                                                <a data-toggle="collapse" data-parent="#accordion-test" href="#collapseOne" class="collapsed">
                                                    Upon Registration
                                                </a> 
                                            </h4> 
                                        </div> 
                                        <div id="collapseOne" class="panel-collapse collapse in"> 
                                            <div class="panel-body" style="padding: 10px 0px;">
                                                <div class="col-lg-12">
                      
   
     <?php echo form_open(base_url().'Template/sms',array("id"=>"AddData1","class"=>"form-horizontal group-border-dashed","enctype"=>"multipart/form-data")); ?>
                 
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Subject</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control require_field" value="<?php echo $smsDataRegister->houdinv_sms_template_subject ;?>" name="subject"  placeholder="Congrats on Booking" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Message</label>
                          <div class="col-sm-10">
                            <textarea name="message"  class="form-control require_field" rows="5"><?php echo $smsDataRegister->houdinv_sms_template_message ;?></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label">Codes</label>
                          <div class="col-sm-10">
                           <span>{registration_date}</span>  - date of registration<br>
                            <span>{registration_contact}</span> - contact detail in case any query<br>
                            <span>{registration_handleby}</span>  -  handle by<br> 
                          </div>
                        </div>
                          
                   <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit"  name="SmsTemplateSave" value="register" class="btn btn-primary">
                              Save
                            </button>
                           </div>
                   </div>
<?php echo form_close(); ?>
                    </div>
                                            </div> 
                                        </div> 
                                    </div>
                                     
                      
           <div class="panel panel-default"> 
                                        <div class="panel-heading"> 
                                            <h4 class="panel-title"> 
                                                <a data-toggle="collapse" data-parent="#accordion-test" href="#collapseTwo">
                                                    On forget password
                                                </a> 
                                            </h4> 
                                        </div> 
                                        <div id="collapseTwo" class="panel-collapse collapse"> 
                                            <div class="panel-body" style="padding: 10px 0px;">
                                               <div class="col-lg-12">
                      
                   <?php echo form_open(base_url().'Template/sms',array("class"=>"form-horizontal group-border-dashed","enctype"=>"multipart/form-data")); ?>
          
                     
                     <div class="form-group">
                          <label class="col-sm-2 control-label">Subject</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control require_field" value="<?php echo $smsDataForget->houdinv_sms_template_subject ;?>" name="subject"  placeholder="Congrats on Booking" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Message</label>
                          <div class="col-sm-10">
                            <textarea name="message"  class="form-control require_field" rows="5"><?php echo $smsDataForget->houdinv_sms_template_message ;?></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label">Codes</label>
                          <div class="col-sm-10">
                           <span>{date}</span>  - date <br>
                            <span>{user_name}</span> - Name of user<br>
                            <span>{user_mail}</span>  - Email of user<br> 
                          </div>
                        </div>
                          
                   <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit"  name="SmsTemplateSave" value="forget_password" class="btn btn-primary">
                              Save
                            </button>
                           </div>
                   </div>
                    <?php echo form_close(); ?>
                    </div>
                                            </div> 
                                        </div> 
                                    </div> 
                                    
            <!--- new order--->                        
                        <div class="panel panel-default"> 
                                        <div class="panel-heading"> 
                                            <h4 class="panel-title"> 
                                                <a data-toggle="collapse" data-parent="#accordion-test" href="#collapseThree" class="collapsed">
                                                    On order
                                                </a> 
                                            </h4> 
                                        </div> 
                                        <div id="collapseThree" class="panel-collapse collapse"> 
                                            <div class="panel-body" style="padding: 10px 0px;">
                                               <div class="col-lg-12">
                      
           <?php echo form_open(base_url().'Template/sms',array("class"=>"form-horizontal group-border-dashed","enctype"=>"multipart/form-data")); ?>
          
                     
                     <div class="form-group">
                          <label class="col-sm-2 control-label">Subject</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control require_field" value="<?php echo $smsDataOnOrder->houdinv_sms_template_subject ;?>" name="subject"  placeholder="Congrats on Booking" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Message</label>
                          <div class="col-sm-10">
                            <textarea name="message"  class="form-control require_field" rows="5"><?php echo $smsDataOnOrder->houdinv_sms_template_message ;?></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label">Codes</label>
                          <div class="col-sm-10">
                           <span>{date}</span>  - date <br>
                            <span>{product_name}</span> - Name of product<br>
                            <span>{amount}</span>  - Total amount<br> 
                          </div>
                        </div>
                          
                   <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit"  name="SmsTemplateSave" value="On_order" class="btn btn-primary">
                              Save
                            </button>
                           </div>
                   </div>
                   <?php echo form_close(); ?>
                    </div>
                                            </div> 
                                        </div> 
                                    </div> 
                                    
               <!--- order shipped ---->
               
                           <div class="panel panel-default"> 
                                        <div class="panel-heading"> 
                                            <h4 class="panel-title"> 
                                                <a data-toggle="collapse" data-parent="#accordion-test" href="#collapseFour" class="collapsed">
                                                    On Shipping
                                                </a> 
                                            </h4> 
                                        </div> 
                                        <div id="collapseFour" class="panel-collapse collapse"> 
                                            <div class="panel-body" style="padding: 10px 0px;">
                                               <div class="col-lg-12">
                      
           <?php echo form_open(base_url().'Template/sms',array("class"=>"form-horizontal group-border-dashed","enctype"=>"multipart/form-data")); ?>
          
                     
                     <div class="form-group">
                          <label class="col-sm-2 control-label">Subject</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control require_field" value="<?php echo $smsDataOnShip->houdinv_sms_template_subject ;?>" name="subject"  placeholder="Congrats on Booking" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Message</label>
                          <div class="col-sm-10">
                            <textarea name="message"  class="form-control require_field" rows="5"><?php echo $smsDataOnShip->houdinv_sms_template_message ;?></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label">Codes</label>
                          <div class="col-sm-10">
                           <span>{date}</span>  - date <br>
                            <span>{product_name}</span> - Name of product<br>
                            <span>{amount}</span>  - Total amount<br> 
                          </div>
                        </div>
                          
                   <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit"  name="SmsTemplateSave" value="On_ship" class="btn btn-primary">
                              Save
                            </button>
                           </div>
                   </div>
                   <?php echo form_close(); ?>
                    </div>
                                            </div> 
                                        </div> 
                                    </div> 
               
                                    
                             <!--- order Complete ---->
               
                           <div class="panel panel-default"> 
                                        <div class="panel-heading"> 
                                            <h4 class="panel-title"> 
                                                <a data-toggle="collapse" data-parent="#accordion-test" href="#collapseFive" class="collapsed">
                                                    On order Complete
                                                </a> 
                                            </h4> 
                                        </div> 
                                        <div id="collapseFive" class="panel-collapse collapse"> 
                                            <div class="panel-body" style="padding: 10px 0px;">
                                               <div class="col-lg-12">
                      
           <?php echo form_open(base_url().'Template/sms',array("class"=>"form-horizontal group-border-dashed","enctype"=>"multipart/form-data")); ?>
          
                     
                     <div class="form-group">
                          <label class="col-sm-2 control-label">Subject</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control require_field" value="<?php echo $smsDataOnOrderComplete->houdinv_sms_template_subject ;?>" name="subject"  placeholder="Congrats on Booking" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Message</label>
                          <div class="col-sm-10">
                            <textarea name="message"  class="form-control require_field" rows="5"><?php echo $smsDataOnOrderComplete->houdinv_sms_template_message ;?></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label">Codes</label>
                          <div class="col-sm-10">
                           <span>{date}</span>  - date <br>
                            <span>{product_name}</span> - Name of product<br>
                            <span>{amount}</span>  - Total amount<br> 
                          </div>
                        </div>
                          
                   <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit"  name="SmsTemplateSave" value="On_order_complete" class="btn btn-primary">
                              Save
                            </button>
                           </div>
                   </div>
                   <?php echo form_close(); ?>
                    </div>
                                            </div> 
                                        </div> 
                                    </div> 
                                    
                            <!--- Coupon ---->
               
                           <div class="panel panel-default"> 
                                        <div class="panel-heading"> 
                                            <h4 class="panel-title"> 
                                                <a data-toggle="collapse" data-parent="#accordion-test" href="#collapseSix" class="collapsed">
                                                    On Coupon generate
                                                </a> 
                                            </h4> 
                                        </div> 
                                        <div id="collapseSix" class="panel-collapse collapse"> 
                                            <div class="panel-body" style="padding: 10px 0px;">
                                               <div class="col-lg-12">
                      
           <?php echo form_open(base_url().'Template/sms',array("class"=>"form-horizontal group-border-dashed","enctype"=>"multipart/form-data")); ?>
          
                     
                     <div class="form-group">
                          <label class="col-sm-2 control-label">Subject</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control require_field" value="<?php echo $smsDataOnCoupon->houdinv_sms_template_subject ;?>" name="subject"  placeholder="Congrats on Booking" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Message</label>
                          <div class="col-sm-10">
                            <textarea name="message"  class="form-control require_field" rows="5"><?php echo $smsDataOnCoupon->houdinv_sms_template_message ;?></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label">Codes</label>
                          <div class="col-sm-10">
                           <span>{date}</span>  - date <br>
                            <span>{product_name}</span> - Name of product<br>
                            <span>{amount}</span>  - Total amount<br> 
                          </div>
                        </div>
                          
                   <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit"  name="SmsTemplateSave" value="On_coupon" class="btn btn-primary">
                              Save
                            </button>
                           </div>
                   </div>
                   <?php echo form_close(); ?>
                    </div>
                                            </div> 
                                        </div> 
                                    </div> 
                                    
                                  <!--- Gift ---->
               
                           <div class="panel panel-default"> 
                                        <div class="panel-heading"> 
                                            <h4 class="panel-title"> 
                                                <a data-toggle="collapse" data-parent="#accordion-test" href="#collapseSeven" class="collapsed">
                                                    On  Gift Vouchers 
                                                </a> 
                                            </h4> 
                                        </div> 
                                        <div id="collapseSeven" class="panel-collapse collapse"> 
                                            <div class="panel-body" style="padding: 10px 0px;">
                                               <div class="col-lg-12">
                      
           <?php echo form_open(base_url().'Template/sms',array("class"=>"form-horizontal group-border-dashed","enctype"=>"multipart/form-data")); ?>
          
                     
                     <div class="form-group">
                          <label class="col-sm-2 control-label">Subject</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control require_field" value="<?php echo $smsDataOnGift->houdinv_sms_template_subject ;?>" name="subject"  placeholder="Congrats on Booking" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Message</label>
                          <div class="col-sm-10">
                            <textarea name="message"  class="form-control require_field" rows="5"><?php echo $smsDataOnGift->houdinv_sms_template_message ;?></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label">Codes</label>
                          <div class="col-sm-10">
                           <span>{date}</span>  - date <br>
                            <span>{product_name}</span> - Name of product<br>
                            <span>{amount}</span>  - Total amount<br> 
                          </div>
                        </div>
                          
                   <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit"  name="SmsTemplateSave" value="On_gift" class="btn btn-primary">
                              Save
                            </button>
                           </div>
                   </div>
                   <?php echo form_close(); ?>
                    </div>
                                            </div> 
                                        </div> 
                                    </div>                       
                                    
                                                                
                                    
                                    
                                    
                                    
                                                          
                                </div> 
                     

        </div>
        </div>
        </div>
        

      </div>
    </div>
  </div>




<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){ 
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
  
  
        $(document).on('submit','.group-border-dashed',function(c){
           
            
                               var rep_image_val='';
                 $(this).find(".require_field").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                       
                        if(!val22)
                        {
                             
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
     
                $('.require_field').on('keyup blur',function()
                                {
                                    
                                        $(this).css("border-color","#ccc");
                                      
                                });
                                
                     
                                
              if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                
          
                
            
          });
  
  
});
</script>
