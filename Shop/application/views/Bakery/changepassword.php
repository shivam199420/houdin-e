<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>
    <div id="back2top"><i class="fa fa-angle-up"></i></div>

    <div class="loader"></div>

    <div class="page-wrap">

      <!--section-->

        <div class="ps-section--hero"><img src="<?php echo base_url(); ?>Extra/Uploads/02.jpg" alt="">

        <div class="ps-section__content text-center">

          <h3 class="ps-section__title">Change Password</h3>

          <div class="ps-breadcrumb">

            <ol class="breadcrumb">

              <li><a href="<?php echo base_url(); ?>">Home</a></li>

              <li class="active">Change Password</li>

            </ol>

          </div>

        </div>

      </div>

      <div class="ps-section pt-80 pb-80">

        <?php
        if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        else if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        ?>

        <div class="container">

          <div class="ps-contact ps-contact--2">

            <div class="row">

                  <div class="col-lg-3 col-md-3"></div>

                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

                    <h3 class="text-center">Change Password</h3>

                      <hr>

                      <p class="text-center">Add details here..!!</p>

<?php echo form_open(base_url( 'Changepassword/updatepass' ), array( 'id' => 'changepass', 'method'=>'post' ));?>

                    <div class="ps-contact__form">

                      <div class="form-group">

                        <input type="password" name="currentPass" class="form-control required_validation_for_change_pass name_validation" placeholder="Currennt password">

                      </div>

                      <div class="form-group">

                        <input class="form-control required_validation_for_change_pass name_validation opass" name="newpass" type="password" placeholder="New password">

                      </div>

                      <div class="form-group">

                        <input class="form-control required_validation_for_change_pass name_validation cpass" name="newconfirmpass" type="password" placeholder="Confirm password">

                      </div>

                      <div class="form-group text-center mt-30">

                        <!-- <button class="ps-btn ps-btn--sm ps-contact__submit setDisableData">Update Password</button> -->
                          <input type="submit" class="ps-btn ps-btn--sm ps-contact__submit setDisableData" value="update Password"/>

                      </div>

                    </div>

                    <?php echo form_close(); ?>

                  </div>

            </div>

          </div>

        </div>

      </div>



      <!--footer-->
      <?php
      $getShopName = getfolderName($this->session->userdata('shopName'));
      $this->load->view(''.$getShopName.'/Template/footer'); ?>

      <script type="text/javascript">
      $(document).ready(function(){
          $(document).on('submit','#changepass',function(){
              var check_required_field='';
              $(this).find(".required_validation_for_change_pass").each(function(){
                  var val22 = $(this).val();
                  if (!val22){
                      check_required_field =$(this).size();
                      $(this).css("border-color","#ccc");
                      $(this).css("border-color","red");
                  }
                  $(this).on('keypress change',function(){
                      $(this).css("border-color","#ccc");
                  });
              });
              if(check_required_field)
              {
                  return false;
              }
              else {
                  return true;
              }
          });
      });
      </script>
