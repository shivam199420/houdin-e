<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function checkwarehouse($getdatabases=false)
{
    $CI =& get_instance();
    $getDb = switchDynamicDatabase($getdatabases);
    $getDatabase = $CI->load->database($getDb,true);
    // get warehouse data
    $getWareHouse = $getDatabase->select('id')->from('houdinv_shipping_warehouse')->get()->result();
    if(!count($getWareHouse) > 0)
    {
        // load master db
        $getMasterDB = $CI->load->database('master',true);
        // getShopdetail
        $getAuthToken = $CI->session->userdata('vendorAuth');
        $getShopDetail = $getMasterDB->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id,houdin_user_shops.*,houdin_users.*')
        ->from('houdin_vendor_auth')
        ->join('houdin_user_shops','houdin_user_shops.houdin_user_shop_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id','left outer')
        ->join('houdin_users','houdin_users.houdin_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id','left outer')
        ->where('houdin_vendor_auth.houdin_vendor_auth_token',$getAuthToken)->get()->result();
        $setInsertArray = array('w_name'=>$getShopDetail[0]->houdin_user_shop_shop_name,'w_pickup_name'=>$getShopDetail[0]->houdin_user_name,'w_pickup_phone'=>$getShopDetail[0]->houdin_user_contact,'w_warehouse'=>'0','w_pickup_country'=>$getShopDetail[0]->houdin_user_shop_country,'w_date_time'=>strtotime(date('Y-m-d')));
        $getDatabase->insert('houdinv_shipping_warehouse',$setInsertArray);
    }


    function Setwarehouse($warehouseid=false,$getdatabases)
    {
        $CI =& get_instance();
    $getDb = switchDynamicDatabase($getdatabases);
    $getDatabase = $CI->load->database($getDb,true);

        $getDatabase->select('*')->from('houdinv_shipping_warehouse');
        $getDatabase->where('id', $warehouseid);
         $getSearchData =$getDatabase->get()->row();
         $CI->session->set_userdata("WAREHOUSE",$getSearchData);
         return true;
    }

}