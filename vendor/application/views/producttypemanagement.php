<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">
         
                <div class="col-md-8">
                   <h4 class="page-title">Product type Management </h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>Category/Categorylanding">Category</a></li>
                  <li class="active">Product type Management</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                  
            </div>
           <!--END Page-Title -->  

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                  <div class="btn-group pull-right m-t-10">
                    
                      <button type="button" title="Add" class="btn btn-default m-r-5" data-toggle="modal" data-target="#add_product_type"><i class="fa fa-plus"></i></button>
                  </div>
                </div>
            </div>
<?php
                                    
            if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
   // print_r($all_data);
    ?>
        <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">

              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>

                    <th>Product Type Name</th>
                    <!-- <th style="width: 25%;">Product Type Image</th> -->
                    <th>Status</th>
                    <th>Action</th>

                  </tr>
                </thead>
                <tbody>
                <?php foreach($Product as $val)
                {?>
                  <tr>


                    <td><?php echo $val['houdinv_productType_name']; ?></td>
                    <!-- <td style="width: 25%;"><?php if($val['houdinv_productType_image']){ ?><img class="custom_img_detail" src="<?php echo base_url();?>images/product_type/<?php echo $val['houdinv_productType_image']; ?>"/> <?php } else {  ?> <img class="custom_img_detail" src="<?php echo base_url() ?>images/no.png"/> <?php  } ?></td> -->
                    <td><button type="button" class="btn btn-success btn btn-xs"><?php echo $val['houdinv_productType_status']; ?></button></td>
                    <td>
                      <button type="button" class="btn btn-info btn-sm m-r-10 edit_popup" data-toggle="modal" data-target="#edit_product_type">Edit</button>
                     <input type="hidden" id="edit_all_data" data-id="<?php echo $val['houdinv_productType_id']; ?>" 
                     name="<?php echo $val['houdinv_productType_name']; ?>" 
                     data-status="<?php echo $val['houdinv_productType_status']; ?>" 
                     data-des="<?php echo $val['houdinv_productType_description']; ?>" 
                     data-sub='<?php echo $val['houdinv_productType_subattr']; ?>'/>
                      <button type="button" class="btn btn-info btn-sm m-r-10 delete_popup" data-toggle="modal" data-target="#delete_product_type">Delete</button>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--Delete-->

  <div id="delete_product_type" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
       <?php echo form_open(base_url( 'Category/producttype' ),  array( 'method' => 'post', 'id'=>"delete_form" ));?>
 
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete Product Type</h4>

          </div>

          <div class="modal-body">
          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this order ?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">
<input type="hidden" id="delete_input" name="delete_input">
          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="delete" value="Delete">

          </div>

          </div>
        <?php echo form_close(); ?>
          </div>

          </div>

<!--change Status-->
<div id="edit_product_type" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
<?php echo form_open(base_url( 'Category/producttype' ),  array( 'method' => 'post', 'id'=>"Edit_form","enctype"=>"multipart/form-data" ));?>
 
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Edit Product Type</h4>

        </div>

        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <label for="field-7" class="control-label">Product Type</label>
        <input type="text" class="form-control required_validation_for_type" id="edit_type" name="type" placeholder="Product Type" />
       <input type="hidden" id="edit_id" name="edit_id" />
        </div>
        </div>
        </div>
        <!-- <div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <label for="field-7" class="control-label">Thumbnail</label>
        <input type="file" class="form-control" id="edit_attr" name="attribute"/>
        </div>
        </div>
        </div> -->
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <label for="field-7" class="control-label">Description</label>
      <textarea class="form-control required_validation_for_type"  id="edit_desc" name="description" rows="4"></textarea>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <label for="field-7" class="control-label">Status</label>
                <select class="form-control required_validation_for_type" id="edit_status" name="status">
                  <option value="">Choose Status</option>
                  <option value="active">Active</option>
                  <option value="deactive">Dective</option>
                  <option value="block">Block</option>
        </select>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <button type="button" class="btn btn-info " id="edit-sub"><i class="fa fa-plus"></i>&nbsp;Add More Attribute</button>

        </div>
        </div>
        </div>

        
            <div class="edit-new"></div>
        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="Edit_product" value="Submit">

        </div>

        </div>
       <?php echo form_close(); ?>
        </div>

        </div>
        <div id="add_product_type" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                <div class="modal-dialog">
                
 <?php echo form_open(base_url().'Category/producttype',array("id"=>'AddForm',"enctype"=>"multipart/form-data"))?>
         
                <div class="modal-content">

                <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h4 class="modal-title">Add Product Type</h4>

                </div>

                <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                <label for="field-7" class="control-label">Product Type</label>
                <input type="text" class="form-control required_validation_for_type" name="type" placeholder="Product Type" />
                </div>
                </div>
                </div>
                <!-- <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                <label for="field-7" class="control-label">Thumbnail</label>
                <input type="file" class="form-control required_validation_for_type" name="attribute"/>
                </div>
                </div>
                </div> -->
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                <label for="field-7" class="control-label">Description</label>
              <textarea class="form-control required_validation_for_type" name="description" rows="4"></textarea>
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                <label for="field-7" class="control-label">Status</label>
                <select class="form-control required_validation_for_type" name="status">
                  <option value="">Choose Status</option>
                  <option value="active">Active</option>
                  <option value="deactive">Dective</option>
                  <option value="block">Block</option>
                </select>
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                <button type="button" class="btn btn-info" id="add-sub"><i class="fa fa-plus"></i>&nbsp;Add More Attribute</button>

                </div>
                </div>
                </div>

                
                 <div class="add-new"></div>
                
                </div>

                <div class="modal-footer">

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                <input type="submit" class="btn btn-info "  name="Add_product" value="Submit">

                </div>

                </div>
            <?php echo form_close(); ?>
                </div>

                </div>

<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">
$(document).ready(function(){

jQuery("#add-sub").click(function () { 
   // alert('Add Experince');
   var htm ='<div class="row removeRow">'
                htm+='<div class="col-md-12 m-b-10">';
                htm+='<div class="form-group no-margin">';
                htm+='<label for="field-7" class="control-label">Attribute type</label>';
                htm+='<span><i class="fa fa-remove removebutton" style="font-size:12px;color:red;float:right"></i></span></div>';
                htm+='<input type="text" name="Attr_type[]" class="form-control required_validation_for_type" placeholder="Attribute type" />';
                
                htm+='</div>';
                htm+='</div>';
  jQuery(".add-new").
  append(htm);
});


jQuery("#edit-sub").click(function () { 
   // alert('Add Experince');
   var htm ='<div class="row removeRow">'
                htm+='<div class="col-md-12 m-b-10">';
                htm+='<div class="form-group no-margin">';
                htm+='<label for="field-7" class="control-label">Attribute type</label>';
                htm+='<span><i class="fa fa-remove removebutton" style="font-size:12px;color:red;float:right"></i></span></div>';
                htm+='<input type="text" name="Attr_type[]" class="form-control required_validation_for_type" placeholder="Attribute type" />';
                
                htm+='</div>';
                htm+='</div>';
  jQuery(".edit-new").
  append(htm);
});


 });
 
 
 $(document).on("click",'.removebutton',function()
 {
    //alert("ffd");
    $(this).parents('.removeRow').remove();
    
 });
</script>

        <script type="text/javascript">
    /*==============Form Validation===========================*/
        $(document).ready(function(){

        $(document).on('submit','#Edit_form,#AddForm',function(c){
            
                               var rep_image_val='';
                 $(this).find(".required_validation_for_type").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                        
                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
                
                $(this).find('.required_validation_for_type').on('keyup blur',function()
                                {
                                    
                                        $(this).css("border-color","#ccc");
                                });
                                
                            if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                else
                {
                }  
            
          });
          
          
          // category edit detail fill 
          $(document).on('click','.edit_popup',function()
          {
            $('.required_validation_for_category').css("border-color","#ccc");
                        $('#Edit_form')[0].reset();
                        jQuery(".edit-new").
  empty(''); 
            console.log($(this).children('#edit_all_data'));
             
            $('#edit_type').val($(this).siblings('#edit_all_data').attr("name"));
             $('#edit_id').val($(this).siblings('#edit_all_data').attr("data-id"));
            
               $('#edit_status').val($(this).siblings('#edit_all_data').attr("data-status"));
                $('#edit_desc').val($(this).siblings('#edit_all_data').attr("data-des"));
                
                
                            var ids = $(this).siblings('#edit_all_data').attr('data-sub');
                          //  console.log(ids);

            // $('.checkboxClass').find('.input_price').prop("disabled",true);
            var htm='';
            if(ids)
            {
          $.each($.parseJSON(ids), function(idx,obj) {
//console.log(obj);
                htm+='<div class="row removeRow">'
                htm+='<div class="col-md-12 m-b-10">';
                htm+='<div class="form-group no-margin">';
                htm+='<label for="field-7" class="control-label">Sub Catagory Name</label>';
                htm+='<span><i class="fa fa-remove removebutton" style="font-size:12px;color:red;float:right"></i></span></div>';
                htm+='<input type="text" value="'+obj+'" name="Attr_type[]" class="form-control required_validation_for_type" placeholder="Sub Catagory Name" />';
                
                htm+='</div>';
                htm+='</div>';
     }); 

        }   
                
            jQuery(".edit-new").
  append(htm);     
 
          });
          
                    // category delete detail fill 
          $(document).on('click','.delete_popup',function()
          {
            console.log($(this).parents('#edit_all_data'));
            $('#delete_input').val($(this).siblings('#edit_all_data').attr("data-id"));
           
          });
          
        });
            </script>
