<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/staffsidebar.php') ?>
<?php 
$staffLsit =$staffData['staffLsit'];
$Totalstaff = $staffData['Totalstaff'];
$totalActivestaff = $staffData['totalActivestaff'];
$TotalDeactiveCoupons = $staffData['TotalDeactiveCoupons'];

?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">
            
                <div class="col-md-8">
                   <h4 class="page-title">Staff Management</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                 
                  <li class="active">Staff Management</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left hidden-xs custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
              
            </div>
           <!--END Page-Title --> 

               <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }

       
        ?>
        </div>
        </div>
            <div class="row m-t-20">
	                    <div class="col-lg-4 col-sm-6">
	                        <div class="widget-panel widget-style-2 bg-white">
	                            <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Staff.png">
	                            <h2 class="m-0 text-dark counter font-600"><?=$Totalstaff?></h2>
	                            <div class="text-muted m-t-5">Total Staff</div>
	                        </div>
	                    </div>
	                    <div class="col-lg-4 col-sm-6">
	                        <div class="widget-panel widget-style-2 bg-white">
	                            <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Staff.png">
	                            <h2 class="m-0 text-dark counter font-600"><?=$totalActivestaff?></h2>
	                            <div class="text-muted m-t-5">Total Active Staff </div>
	                        </div>
	                    </div>
                      <div class="col-lg-4 col-sm-6">
                         <div class="widget-panel widget-style-2 bg-white">
                             <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Staff.png">
                             <h2 class="m-0 text-dark counter font-600"><?=$TotalDeactiveCoupons?></h2>
                             <div class="text-muted m-t-5">Total Deactive Staff </div>
                         </div>
                     </div>

	                  </div>
                    <div class="row">
                      <div class="col-md-12">
                       <!-- <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" id="example-input1-group1" name="example-input1-group1" class="form-control" placeholder="Search">
                                                <span class="input-group-btn">
                                                	<button type="button" class="btn waves-effect btn-white" style="margin: 0px!important;"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </div>-->
                      </div>
                    </div>

                    <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">
              <div class="btn-group pull-right m-t-10 m-b-20">
              <?php 
              $getUserPlan = getVendorplan();
              
              if(count($getUserPlan) > 0)
              {
                    if($getUserPlan[0]->houdin_package_staff_accounts > $Totalstaff)
                    {
                    ?>   
                    <a href="<?php echo base_url(); ?>staff/add" class="btn btn-default m-r-5" title="Add Staff"><i class="fa fa-plus "></i></a>     
                    <?php }
              }
              else
              {
                ?>
                <a href="<?php echo base_url(); ?>staff/add" class="btn btn-default m-r-5" title="Add Staff"><i class="fa fa-plus "></i></a>
              <?php }
              ?>
                 
                  <a href="<?php echo base_url(); ?>staff/stafflist" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
                  <button type="button" class="btn btn-default m-r-5 delete_campaign_mlti"><i class="fa fa-trash"></i></button>
              </div>
                <?php echo form_open(base_url('staff/staff_multidelete'), array( 'id' => 'staff_multidelete', 'method'=>'POST' ));?>
            
              <table id="example"  style="width:100%" class="table table-striped table-bordered table_shop_custom display">
                <thead>

                  <tr>
                  <th>
                       <input type="checkbox" class="all_select" id="all_select">
                       </th>
                    <th>Staff Name</th>
                    <th>Staff Email</th>
                    <th>Department</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>


                </thead>
                <tbody>
<!--
   <?php foreach($staffLsit as $key => $staffLsits) {
                  


    if($staffLsits->staff_status=='1'){
       $staff_statuss='<p style="color:green">Active</p>';
    }else{
        $staff_statuss='<p style="color:red">Deactive</p>';
    }
                ?>
                  <tr>
                   <td><input type="checkbox" class="multiple_check" name="multiple_check[]" value="<?=$staffLsits->staff_id?>"></td>
                    
                    <td><?=$staffLsits->staff_name?></td>
                    <td><?=$staffLsits->staff_email?></td>
                    <td><?=$staffLsits->staff_department?></td>
                    <td><?=$staff_statuss?></td>

                    <td>
                       <button type="button" class="btn btn-default  m-r-5 delete_staff"  data-id="<?=$staffLsits->staff_id;?>">Delete</button>
                        <a href="<?php echo base_url(); ?>staff/edit/<?=$staffLsits->staff_id;?>/" class="btn btn-default  m-r-5" >Edit</a>
                    </td>
                  </tr>
                  <?php } ?>
                  -->

                </tbody>
              </table>
               <?php echo form_close(); ?>
            </div>
          </div>
        </div>




                      </div>
                    </div>
                  </div>





 





                  <div id="delete_staff" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >

                  <div class="modal-dialog">
                   <?php echo form_open(base_url('staff/staff_delete'), array( 'id' => 'staff_delete', 'method'=>'POST'));?>
        
                  <div class="modal-content">

                  <div class="modal-header">

                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                  <h4 class="modal-title">Delete Order</h4>

                  </div>

                  <div class="modal-body">



                  <div class="row">
                  <div class="col-md-12">
                  <h4><b>Do you really want to Delete this Staff ?</b></h4>
                  </div>
                  </div>
                  </div>

                  <div class="modal-footer">
                       <input type="hidden" name="update_id" id="delete_val">
                  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                  <input type="submit" class="btn btn-info" name="" value="Delete">

                  </div>

                  </div>
                  <?php echo form_close(); ?>
                  </div>

                  </div>
<div id="delete_campaign_mlti" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                      <div class="modal-dialog">
                  
                      <div class="modal-content">

                      <div class="modal-header">

                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                      <h4 class="modal-title">Delete Staff</h4>

                      </div>

                      <div class="modal-body">



                      <div class="row">
                      <div class="col-md-12">
                      <h4><b>Do you really want to Delete Select Staff ?</b></h4>
                      </div>
                      </div>
                      </div>
                     
                      <div class="modal-footer">
                      <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

                      <input type="button" id="delete_allcheckrows" class="btn btn-info" name="" value="Delete">

                      </div>

                      </div>
                    
                      </div>

                      </div>


<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">
  $(document).ready(function(){

 //$(".delete_staff").click(function(){ 
 $(document).on('click',".delete_staff",function(){   
       data_id=$(this).attr('data-id');

       $('#delete_val').val(data_id);              
      $('#delete_staff').modal('show');

       });








  $("#all_select").click(function () {
     $('.multiple_check').not(this).prop('checked', this.checked);
 });

   $('.delete_campaign_mlti').click(function(){  
    if($('.multiple_check:checkbox:checked').length == 0)
   alert('At least one checkbox checked then delete ');
else
  // at least one checkbox checked...
      $('#delete_campaign_mlti').modal('toggle');   

    });
 
 $('#delete_allcheckrows').click(function(){
 
  $("#staff_multidelete").submit();
 });

  });

</script>
<script>
  $(document).ready(function() {
    $('#example').DataTable( {
        "ajax": "<?php echo base_url(); ?>staff/get_array"
    } );
} );
  
  </script>
