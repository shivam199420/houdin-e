<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
  <!-- Content -->
  <div id="content"> 
    
    <!-- Linking -->
    <div class="linking">
      <div class="container">
        <ol class="breadcrumb">
          <li class="active">Authentication</li>
        </ol>
      </div>
    </div>
    
    <!-- Blog -->
    <section class="login-sec padding-top-30 padding-bottom-100">
      <div class="container">
        <div class="row">
        <?php 
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
          <div class="col-md-6"> 
            <!-- Login Your Account -->
            <h5>Login Your Account</h5>
            <hr>
            <!-- FORM -->
            <?php echo form_open(base_url( 'Register/checkUserAuth' ), array( 'id' => 'authShopUsers', 'method'=>'post' ));?>
              <ul class="row">
                <li class="col-sm-12">
                  <label>Email
                  <input type="email"  name="userEmail" class="required_validation_for_user name_validation email_validation form-control" placeholder="Enter Email Address"/>
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Password
                  <input type="password" name="userPass" class="required_validation_for_user name_validation form-control" placeholder="Enter Password"/>
                  </label>
                </li>
                <li class="col-sm-6">
                 
                </li>
                <li class="col-sm-6"> <a href="#" title="Recover your forgotten password" rel="" data-toggle="modal" data-target="#myModal" style="float: right;">Forgot your password?</a> </li>
                <li class="col-sm-12 text-left">
                <input type="submit" class="btn-round acc_btn" style="background: #0088cc;" value="Login"/>
                </li>
              </ul>
            <?php echo form_close(); ?>
          </div>
         
          <!-- Don’t have an Account? Register now -->
          <div class="col-md-6">
            <h5>Don’t have an Account? Register now</h5>
            <hr>
            <!-- FORM -->
            <?php echo form_open(base_url( 'Register/addShopUsers' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
              <ul class="row">
                <li class="col-sm-12">
                  <label>Firstname
                  <input type="text" class="required_validation_for_user name_validation form-control" name="userFirstName" placeholder="Enter Firstname" />
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Lastname
                  <input type="text" class="required_validation_for_user name_validation form-control" name="userLastName" placeholder="Enter Lastname"/>
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Email Address
                  <input type="email" class="required_validation_for_user name_validation email_validation form-control" name="userEmail" placeholder="Enter Emailaddress"/>
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Contact Number
                  <input type="text" class="required_validation_for_user name_validation form-control" name="userContact" placeholder="Enter Contact Number"/>
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Password
                  <input type="password" class="required_validation_for_user name_validation opass form-control" name="userPass" placeholder="Enter password"/>
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Confirm Password
                  <input type="password" class="required_validation_for_user name_validation cpass form-control" name="userRetypePass" placeholder="Conform password" />
                  </label>
                </li>
                <li class="col-sm-12 text-left">
                <input type="submit" class="btn-round acc_btn setDisableData" style="background: #0088cc;" type="submit" value="Create an account"/>
                </li>
              </ul>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- End Content --> 
   <!-- modal -->
   <div id="myModal" class="modal fade" role="dialog" aria-hidden="true" style="display: none;">
                          <div class="modal-dialog">
                          <?php echo form_open(base_url( 'Register/checkUserForgot' ), array( 'id' => 'forgotShopUsers', 'method'=>'post' ));?>
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h6 class="modal-title">Forgot your password</h6>
                                <button type="button" class="close" data-dismiss="modal" style="float: right;margin-top: -30px;">×</button>
                              </div>
                              <div class="modal-body">
                              <input placeholder="Please enter your Email address" name="emailForgot" class="required_validation_for_user name_validation email_validation form-control" type="text" style="width:100%;">
                              </div>
                              <div class="modal-footer">
                              <input type="submit" style="background: #0088cc !important;" class="btn-round" value="Submit"/>
                              </div>
                            </div>
                            <?php echo form_close(); ?>
                          </div>
                        </div>
                        <!-- end modal -->
  <?php 
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer') 
  ?>
   <script type="text/javascript">
        $(document).ready(function(){
                $(document).on('submit','#addShopUsers,#authShopUsers,#forgotShopUsers',function(){
                        var check_required_field='';
                        $(this).find(".required_validation_for_user").each(function(){
                                var val22 = $(this).val();
                                if (!val22){
                                        check_required_field =$(this).size();
                                        $(this).css("border-color","#ccc");
                                        $(this).css("border-color","red");
                                }
                                $(this).on('keypress change',function(){
                                        $(this).css("border-color","#ccc");
                                });
                        });
                        if(check_required_field)
                        {
                                return false;
                        }
                        else {
                                return true;
                        }
                });
        });
                </script>