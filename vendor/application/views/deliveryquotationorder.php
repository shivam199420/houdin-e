<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style type="text/css">
  .tabs li.tab {
    background-color: #ffffff;
    display: block;
    float: left;
    margin: 0;
    text-align: center;
    width: 20%;
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
        <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Order Quotation</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>order/Orderlanding">Order</a></li>
                  <li class="active">Order Quotation</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
             
            </div>
           <!--END Page-Title -->
          <!-- START TAB -->
          <div class="row m-t-20">
    <div class="col-sm-12">
    <?php 
    if($this->session->flashdata('success'))
    {
        echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
    }
    if($this->session->flashdata('error'))
    {
        echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
    }
    ?>
    </div>
    </div>
            <div class="row m-t-20">
                    <div class="col-lg-12"> 
                <!-- START TAB QUOTATION -->

         <div class="tab-pane active" id="quotation">
          <div class="row">
             <div class="col-md-12">

                          <div class="card-box table-responsive">
                            
                            <table class="table table-striped table-bordered table_shop_custom">
                              <thead> 
                                <tr> 
                                  <th>User Details</th>
                                  <th>Produdct Details</th>
                                  <th>Quotation</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php 
                                foreach($quotaion as $quotaionList)
                                {

                                ?>
                                <tr>
                                  <td><?php echo $quotaionList->houdinv_shop_ask_quotation_name."(".$quotaionList->houdinv_shop_ask_quotation_email.", ".$quotaionList->houdinv_shop_ask_quotation_phone.")" ?></td>
                                   <td><a href="<?php echo base_url() ?>Product/editproduct/<?php echo $quotaionList->houdinv_shop_ask_quotation_product_id ?>" target="_blank" title="<?php echo $quotaionList->houdin_products_title ?>"><?php echo substr($quotaionList->houdin_products_title,0,15).".." ?></a></td>
                                  <td><?php echo $quotaionList->houdinv_shop_ask_quotation_product_count ?></td>
                                  <td>
                                    <button type="button" class="btn btn-sm btn-primary deleteQuotationBtn" data-id="<?php echo $quotaionList->houdinv_shop_ask_quotation_id ?>">Delete</button>
                                    <button type="button" class="btn btn-sm btn-primary sendEmailData" data-email="<?php echo $quotaionList->houdinv_shop_ask_quotation_email ?>">Send Email</button>
                                  </td> 
                                </tr>
                                <?php }
                                ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
          </div> 

          <!-- END TAB QUOTATION -->

                                </div> 
                              
                            </div> 
      
  </div>
  <!-- END TAB -->
            
         
      </div>
    </div>
  </div>
<!--Delete-->

  <div id="deleteQuotationModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
          <?php echo form_open(base_url( 'Order/deliveryquotation'), array('method'=>'post' ));?>
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Delete Quotation</h4>
          </div>
          <div class="modal-body">
          <div class="row">
          <div class="col-md-12">
            <input type="hidden" class="deleteQuotationId" name="deleteQuotationId"/>
          <h4><b>Do you really want to Delete this order ?</b></h4>
          </div>
          </div>
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-info" name="deleteQuotation" value="Delete">

          </div>

          </div>
          <?php echo form_close(); ?>
          </div>

          </div>

      <!--change Status-->
      <div id="sendQuotationemailModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Order/deliveryquotation'), array('id'=>'sendQuotationEmail','method'=>'post' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Send Email</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
          <input type="hidden" class="emailQuotationData" name="emailQuotationData"/>
        <label for="field-7" class="control-label">Subject</label>
        <input type="text" class="form-control required_validation_for_quotation name_validation" name="emailSubject" placeholder="Email Subject"/>
        </div>
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Body</label>
        <textarea class="form-control summernote required_validation_for_quotation" name="emailBodyData" rows="5"></textarea>
        </div>
        </div>
        </div>
        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="sendEmailtoUser" value="Update Status">

        </div>

        </div>
        <?php echo form_close() ?>
        </div>

        </div>

<?php $this->load->view('Template/footer.php') ?>
 <!-- CLient side form validation -->
 <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#sendQuotationEmail',function(){
			var check_required_field='';
			$(this).find(".required_validation_for_quotation").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>
