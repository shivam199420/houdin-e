<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->library('Pdf');
        $this->auth_users->is_logged_in();
        $this->load->library('form_validation'); 
        $this->load->model('Staffmodel'); 
        $this->load->model('Categorymodel'); 
        $this->load->model('Productmodel');
        $this->load->model('Shippingmodel');
        $this->load->helper('url');
		//$this->load->library('pagination');
        $this->perPage=25;
        $this->Validconfigs = 
        array(
            array(
                    'field' => 'title',
                    'label' => 'Title',
                    'rules' => 'required'
                ),
            
            array(
                    'field' => 'Main_price',
                    'label' => 'Price',
                    'rules' => 'required|numeric'
                ),
            array(
                    'field' => 'discount_price',
                    'label' => 'Discount',
                    'rules' => 'numeric'
                ),
            array(
                    'field' => 'order_quantity',
                    'label' => 'Quantity',
                    'rules' => 'integer'
                ),
            array(
                    'field' => 'order_sort',
                    'label' => 'Sort',
                    'rules' => 'numeric'
                ),
            array(
                    'field' => 'length',
                    'label' => 'Length',
                    'rules' => 'numeric'
                ),
            array(
                    'field' => 'height',
                    'label' => 'height',
                    'rules' => 'numeric'
                ),
            array(
                    'field' => 'breath',
                    'label' => 'breath',
                    'rules' => 'numeric'
                ),
            array(
                    'field' => 'weight',
                    'label' => 'weight',
                    'rules' => 'numeric'
                ) 
        );
        $this->categoryValidConfig = [
            [
                'field' => 'category_name',
                'label' => 'Category Name',
                'rules' => 'required'
            ]
        ];
    }

    /**
     * productLandingpage()
     * @purpose: to show landing page for product
     * @created by: shivam singh sengar
     * @created at: 1 Dec 2019 06:41 pm
     * @param NONE
     * @return HTML ( view page )
     */
    public function productLandingpage()
    {
        $data['allStatus'] =  $this->Productmodel->fetchAllProductStatus();
        $this->load->view('productlandingpage',$data);
    }
    public function index()
    {
        
        if($this->input->post('DeleteEntry'))
        {
            
            $delete_id = $this->input->post('delete_id');
          $this->Productmodel->deleteProduct($delete_id);
        }
        
        
        if($this->input->post('ChangeEntry'))
        {
            $stus_id = $this->input->post('status_id');
             $stus_value = $this->input->post('status_value');
           $this->Productmodel->ChangeStatus($stus_id,$stus_value);
        }
        
        
              // Custom Pagination
      $All= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->Productmodel->FetchProductData($conditions);
      
        //pagination config
        $config['base_url']    = base_url().'product/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        $page = $this->uri->segment(2);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset; 
        $conditions['limit'] = $this->perPage;
    
       $data['all'] =  $this->Productmodel->FetchProductData($conditions);
        $data['allStatus'] =  $this->Productmodel->fetchAllProductStatus();
        $this->load->view('productdata',$data);
    }
    public function detail()
    {
        $this->load->view('productdetail');
    }
   public function set_barcode()
	{
	   $code = rand(10000, 99999);
		//load library
		$this->load->library('zend');
		//load in folder Zend
		$this->zend->load('Zend/Barcode');
		//generate barcode
	$file =	Zend_Barcode::draw('code128', 'image', array('text'=>$code), array());
           $code = time().$code;
   $store_image = imagepng($file,realpath(__DIR__ . '/../..')."/images/barcode/{$code}.png");
   echo $code.'.png';
	}
    
    
    public function addproduct()
    {
        // check form is submit or not
        if ($this->input->post('formSubmit')) { 
            $total_stock = 0;
            $total_price = 0;
           // set product status
           if (!$this->input->post('product_status')) {
            $ty=0;
           } else {
            $ty=1;
           }
           // Upload product image   
            $images_data = $_FILES['images'];
            for ($i=0; $i < count($images_data['name']); $i++) {
                if (!empty($_FILES['images']['name'][$i])) {
                    $_FILES['file']['name']= $_FILES['images']['name'][$i];
                    $_FILES['file']['type']= $_FILES['images']['type'][$i];
                    $_FILES['file']['tmp_name']= $_FILES['images']['tmp_name'][$i];
                    $_FILES['file']['error']= $_FILES['images']['error'][$i];
                    $_FILES['file']['size']= $_FILES['images']['size'][$i]; 
                    $newFileName = rand(100000,999999).$i.$_FILES['file']['name'];
                    $config['upload_path'] = "upload/productImage/";
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $config['file_name'] = $newFileName;
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);

                    // upload image file
                    if ($this->upload->do_upload('file')) {                                  
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                    } else {
                        $error = array('error' => $this->upload->display_errors());
                        $picture = '';  
                    }
                } else {
                    $picture ='';
                    }
                    $pictureData[$i]= $picture;
           }
            $this->form_validation->set_rules($this->Validconfigs);
            // check if form validaations are true or rnot
            if ($this->form_validation->run() != true) {
                $this->session->set_flashdata('message_name',validation_errors() );
                redirect(base_url().'Product/addproduct', 'refresh');      
            }
            // check prodcut category
            $cat = $this->input->post('category_name');
                $cat1=array();
                $cat2=array();
                $cat3=array();
                foreach ($cat as $subpartsArray) {
                    $subparts = explode("^^",$subpartsArray);
                    if ($subparts[1]=='sub0') {
                        array_push($cat1,$subparts[0]);
                    } else if ($subparts[1]=='sub1') {
                        array_push($cat2,$subparts[0]);
                    } else if ($subparts[1]=='sub2') {
                        array_push($cat3,$subparts[0]);
                    }
                }
            // set product type
            $attr_type = $this->input->post('product_type');  
            $attr_val ='';
            if ($attr_type) {
                $attr_val = $this->input->post('Attr'); 
            }   

            // set main product quantity
            $quanti_val = $this->input->post('quanti');
            $setTotalStock = 0;
            for ($index = 0; $index < count($quanti_val); $index++) {
                $setTotalStock = $setTotalStock+$quanti_val[$index]['value'];
            }

            $date = strtotime(date("Y-m-d"));
            // set main product qunatity
            $payment = json_encode($this->input->post('product_payment'));
            
            // set main product discount price
            if ($this->input->post('discount_price')) {
              $setDiscountPrice = $this->input->post('discount_price');
           } else {
                $setDiscountPrice = 0;
            }
            // set main prodcut sales tax
            if ($this->input->post('sales_tax')) {
                $getTaxDataValue = $this->Productmodel->getSalesTax($this->input->post('sales_tax'));
                $getsalePrice = $this->input->post('sale_price');
                $getTaxData = ($getsalePrice*$getTaxDataValue)/100;
            } else {
                $getTaxData = 0;
            }

            // encode the main product price
            $price  = json_encode(array("price"=>$this->input->post('sale_price'),
                               "discount"=>$setDiscountPrice,
                               "cost_price"=>$this->input->post('cost_price'),
                               "tax_price"=>$this->input->post('sales_tax'),
                               'purchase_tax'=>$this->input->post('purchase_tax'),
                               "sale_price"=>$this->input->post('Main_price')));
            if ($this->input->post('discount_price') != 0 && $this->input->post('discount_price') != "") {
                $setFinalPrice = ($this->input->post('sale_price')-$this->input->post('discount_price'))+$getTaxData;
            } else {
                $setFinalPrice = $this->input->post('sale_price')+$getTaxData;
            }

            // encode the joson
            $shipping = json_encode(array("length"=>$this->input->post('length'),
                                          "height"=>$this->input->post('height'),
                                          "breath"=>$this->input->post('breath'),
                                          "weight"=>$this->input->post('weight')));
                                          
                                          
            //   upload variant image
            $images_data = $_FILES['new_image'];

            // check if variant image exists or not
            for ($i=0; $i < count($images_data['name']); $i++) {
                if (!empty($_FILES['new_image']['name'][$i])) {
                    // upload variant image
                    $_FILES['file']['name']= $_FILES['new_image']['name'][$i];
                    $_FILES['file']['type']= $_FILES['new_image']['type'][$i];
                    $_FILES['file']['tmp_name']= $_FILES['new_image']['tmp_name'][$i];
                    $_FILES['file']['error']= $_FILES['new_image']['error'][$i];
                    $_FILES['file']['size']= $_FILES['new_image']['size'][$i];  
                    $newFileName = rand(100000,999999).$i.$_FILES['file']['name'];
                    $config['upload_path'] = "upload/productImage/";
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $config['file_name'] = $newFileName;
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);

                    // upload image
                    if ($this->upload->do_upload('file')) {
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                    } else {
                        $error = array('error' => $this->upload->display_errors());
                        $picture = '';  
                    }
                } else
                {
                    $picture ='';
                }
                $pictureDataVariant[$i]= $picture;
            }

            // check variant exist or not
            $setArrayVariant = [];
            if ($this->input->post('variantData')) {
                $setArrayVariant = [
                    'variantData'=>$this->input->post('variantData'),
                    'new_title'=>$this->input->post('new_title'),
                    'new_qty'=>$this->input->post('new_qty'),
                    'new_mrp'=>$this->input->post('new_mrp'),
                    'new_cp'=>$this->input->post('new_cp'),
                    'new_margin'=>$this->input->post('new_margin'),
                    'new_sp'=>$this->input->post('new_sp'),
                    'barcode_data'=>$this->input->post('barcode_data'),
                    'pictureDataVariant'=>$pictureDataVariant ,
                    'variant_warehouse' => $this->input->post('product_variant_warehouse') ? implode(',',$this->input->post('product_variant_warehouse')) : '',
                ];
            }
        // set quotation status
        $setQuotation = 0;
        if ($this->input->post('product_quotation')) {
            $setQuotation = $this->input->post('product_quotation');
        }
        $array =array("houdin_products_category"=>implode(",",$cat1),
                        "houdin_products_sub_category_level_1"=>implode(",",$cat2),
                        "houdin_products_sub_category_level_2"=>implode(",",$cat3),
                        "houdin_products_title"=>$this->input->post('title'),
                        "houdin_products_short_desc"=>$this->input->post('short_desc'),
                        "houdin_products_desc"=>$this->input->post('short_desc'),
                        "houdin_products_pag_title"=>'test',
                        "houdin_products_price"=>$price,
                        "houdin_products_final_price"=>$setFinalPrice,
                        "houdin_product_type"=>$attr_type,
                        "houdin_product_type_attr_value"=>json_encode($attr_val),
                        "houdinv_products_show_on"=>$this->input->post('show_on'),
                        "houdin_products_main_sku"=>'sku',
                        'houdinv_products_sell_as'=>$this->input->post('sell_as'),
                        'houdinv_products_unit'=>$this->input->post('productUnit'),
                        'houdinv_products_batch_number'=>$this->input->post('batchNumber'),
                        'houdinv_products_expiry'=>$this->input->post('expiry'),
                        // "houdinv_products_main_stock"=>$this->input->post('orde    r_stock'),
                        "houdinv_products_main_stock"=>json_encode($quanti_val),
                        'houdinv_products_total_stocks'=>$setTotalStock,
                        "houdinv_products_main_minimum_order"=>'1',
                        "houdinv_products_main_sort_order"=>$this->input->post('order_sort'),
                        "houdinv_products_main_featured"=>$this->input->post('product_feature'),
                        "houdinv_products_main_quotation"=>$setQuotation,
                        "houdinv_products_main_payment"=>json_encode($this->input->post('product_payment')),
                        "houdinv_products_main_images"=>json_encode($pictureData),
                        "houdin_products_status"=>$ty,
                        "houdin_products_warehouse"=>$this->input->post('product_warehouse') ? implode(",",$this->input->post('product_warehouse')) : '',
                        "houdinv_products_main_shipping"=>$shipping,
                        "houdin_products_barcode_image"=>$this->input->post('barcode') ? $this->input->post('barcode') : '',
                        "houdin_products_created_date"=>$date,
                        "houdin_products_updated_date"=>$date);   
            $modelData = $this->Productmodel->InsertProductData($array,$this->input->post('MainSuplier'),$setArrayVariant,$this->input->post('sales_tax'));
            if ($modelData) {
                $this->session->set_flashdata('success',"Product added successfully");
                $redirect_url = base_url().'Product/addproduct';
                if ($this->input->post('pos_id')) {
                    $redirect_url = base_url().'POS';
                }
                redirect($redirect_url, 'refresh');           
            }
        }
        $returnData    = $this->Productmodel->FetchSingleProductData($id);
        $product_data['All_one_var'] = $returnData['variant'];
        $product_data['supplier'] = $this->Productmodel->fetchAllSupplier();
        $product_data['skuVar'] = $this->Productmodel->FetchSkuPrefix(); 
	    $product_data['new'] = $this->Categorymodel->getCategoryTypeData('both'); // fetch category and type of product
        $product_data['WareHouse']= $this->Staffmodel->getWarhouse();
        $product_data['taxData'] = $this->Productmodel->getTaxData();
        $this->load->view('addproductdata',$product_data);
    }
    public function editproduct()
    {
        if($this->input->post('formSubmit'))
        {
            // product details
            $setTotalStock = 0;
            $ids = $this->input->post('ids');
            $ones = $this->Productmodel->FetchSingleProductData($ids);
            $images12 = json_decode($ones['product']->houdinv_products_main_images,true);
            if(!$this->input->post('product_status'))
            {
                $ty=0;
            }
            else
            {
                $ty=1;
            }
            $images_data = $_FILES['images'];
            for($i=0; $i < count($images_data['name']); $i++)
            {
                if(!empty($_FILES['images']['name'][$i]))
                {
                    $_FILES['file']['name']= $_FILES['images']['name'][$i];
                    $_FILES['file']['type']= $_FILES['images']['type'][$i];
                    $_FILES['file']['tmp_name']= $_FILES['images']['tmp_name'][$i];
                    $_FILES['file']['error']= $_FILES['images']['error'][$i];
                    $_FILES['file']['size']= $_FILES['images']['size'][$i];  
                    $newFileName = rand(100000,999999).$i.$_FILES['file']['name'];
                    $config['upload_path'] = "upload/productImage/";
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $config['file_name'] = $newFileName;
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('file'))
                    {
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                    }
                    else
                    {
                        $error = array('error' => $this->upload->display_errors());
                        $picture = $images12[$i];  
                    }
                    }
                    else
                    {
                        $picture =$images12[$i];
                    }
                    $pictureData[$i]= $picture;
            }
            $this->form_validation->set_rules($this->Validconfigs);
            // if($this->input->post("new_var_name"))
            // {
            //     $this->form_validation->set_rules("new_title[]"," Variant Title","required");
            //     $this->form_validation->set_rules("new_price[]"," Variant Price","required");
            // }
            if($this->form_validation->run() != true)
            {
                $this->session->set_flashdata('message_name',validation_errors() );
                redirect(base_url().'Product/editproduct/'.$ids, 'refresh');      
            }
            $final_var_array =array();   
            $all_new_variant_name = $this->input->post("variantData");
            $all_new_title = $this->input->post("new_title");
            $all_new_quantity = $this->input->post("new_qty");
            $all_new_price = $this->input->post("new_price");
            $all_new_cp = $this->input->post("new_cp");
            $all_new_margin = $this->input->post("new_margin");
            $all_new_sp = $this->input->post("new_sp");
            $all_new_barcode_data = $this->input->post("barcode_data");
            $all_new_image = $_FILES['new_image'];
            $default_already_id = $this->input->post("already_id");              

            // if($this->input->post("new_var_name"))
            // {
                for($gh=0; $gh < count($all_new_variant_name); $gh++ )
                {
                    if(!empty($_FILES['new_image']['name'][$gh]))
                    {
                        $_FILES['file']['name']= $_FILES['new_image']['name'][$gh];
                        $_FILES['file']['type']= $_FILES['new_image']['type'][$gh];
                        $_FILES['file']['tmp_name']= $_FILES['new_image']['tmp_name'][$gh];
                        $_FILES['file']['error']= $_FILES['new_image']['error'][$gh];
                        $_FILES['file']['size']= $_FILES['new_image']['size'][$gh];  
                        $newFileName = rand(100000,999999).$gh.'variants'.$_FILES['file']['name'];
                        $config['upload_path'] = "upload/productImage/";
                        $config['allowed_types'] = 'jpg|jpeg|png';
                        $config['file_name'] = $newFileName;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('file'))
                        {
                            $uploadData12 = $this->upload->data();
                            $picture12 = $uploadData12['file_name'];
                        }
                        else
                        {
                            $error = array('error' => $this->upload->display_errors());
                            $picture12 = '';  
                        }
                    }
                    else
                    {
                        $picture12 ='';
                    }
                    $pictureVariantData[$gh]= $picture12;                      
                } 
            // }
            $date = strtotime(date("Y-m-d"));
            if($this->input->post('discount_price'))
            {
                $setFinalDuscountPrice = $this->input->post('discount_price');
            }
            else
            {
                $setFinalDuscountPrice = 0;
            }
            $payment = json_encode($this->input->post('product_payment'));
            $price  = json_encode(array("price"=>$this->input->post('sale_price'),
                                "discount"=>$setFinalDuscountPrice,
                                "cost_price"=>$this->input->post('cost_price'),
                                "tax_price"=>$this->input->post('sales_tax'),
                               'purchase_tax'=>$this->input->post('purchase_tax'),
                                "sale_price"=>$this->input->post('Main_price')));

            $shipping = json_encode(array("length"=>$this->input->post('length'),
                                            "height"=>$this->input->post('height'),
                                            "breath"=>$this->input->post('breath'),
                                            "weight"=>$this->input->post('weight')));
            $attr_type = $this->input->post('product_type');  
            $attr_val ='';
            if($attr_type)
            {
            $attr_val = $this->input->post('Attr'); 
            }                                                                       
            $cat = $this->input->post('category_name');
            $cat1=array();
            $cat2=array();
            $cat3=array();
            foreach($cat as $subpartsArray)
            {
                $subparts = explode("^^",$subpartsArray);
                if($subparts[1]=='sub0')
                {
                    array_push($cat1,$subparts[0]);
                }
                else if($subparts[1]=='sub1')
                {
                    array_push($cat2,$subparts[0]);
                }
                else if($subparts[1]=='sub2')
                {
                    array_push($cat3,$subparts[0]);
                }
            } 
            $quanti_val = $this->input->post('quanti'); 
            for($index = 0; $index < count($quanti_val); $index++)
            {
                $setTotalStock = $setTotalStock+$quanti_val[$index]['value'];
            }    
            if($this->input->post('sales_tax'))
          {
              $getTaxDataValue = $this->Productmodel->getSalesTax($this->input->post('sales_tax'));
              $getTaxData = ($this->input->post('sale_price')*$getTaxDataValue)/100;
          }
          else
          {
                $getTaxData = 0;
          }
            if($this->input->post('discount_price') != 0 && $this->input->post('discount_price') != "")
            {
                $setFinalPrice = $this->input->post('sale_price') - $this->input->post('discount_price')+$getTaxData;
            }
            else
            {
                $setFinalPrice = $this->input->post('sale_price');
            }
            if($this->input->post('product_quotation'))
            {
                $setQuotation = $this->input->post('product_quotation');
            }
            else
            {
                $setQuotation = 0;
            }
            $array =array("houdin_products_category"=>implode(",",$cat1),
                                "houdin_products_sub_category_level_1"=>implode(",",$cat2),
                                "houdin_products_sub_category_level_2"=>implode(",",$cat3),
                                "houdin_products_title"=>$this->input->post('title'),
                                "houdin_products_short_desc"=>$this->input->post('short_desc'),
                                "houdin_products_desc"=>$this->input->post('desc'),
                                "houdin_products_pag_title"=>$this->input->post('page_title'),
                                "houdin_products_price"=>$price,
                                "houdin_product_type"=>$attr_type,
                                "houdin_product_type_attr_value"=>json_encode($attr_val),
                                "houdinv_products_show_on"=>$this->input->post('show_on'),
                                "houdin_products_main_sku"=>$this->input->post('sku'),
                                "houdinv_products_main_stock"=>json_encode($quanti_val),
                                "houdinv_products_main_minimum_order"=>$this->input->post('order_quantity'),
                                "houdinv_products_main_sort_order"=>$this->input->post('order_sort'),
                                "houdinv_products_main_featured"=>$this->input->post('product_feature'),
                                "houdinv_products_main_quotation"=>$setQuotation,
                                "houdinv_products_main_payment"=>json_encode($this->input->post('product_payment')),
                                "houdinv_products_main_images"=>json_encode($pictureData),
                                "houdin_products_status"=>$ty,
                                "houdin_products_warehouse"=>implode(",",$this->input->post('product_warehouse')),
                                'houdinv_products_main_other_variant'=>$default_valu_Apply,
                                "houdinv_products_main_shipping"=>$shipping,
                                "houdin_products_barcode_image"=>$this->input->post('barcode'),
                                "houdinv_products_total_stocks"=>$setTotalStock,
                                "houdin_products_final_price"=>$setFinalPrice,
                                "houdin_products_created_date"=>$date,
                                "houdin_products_updated_date"=>$date);

                                $final_var_array = array('variantData'=>$all_new_variant_name,
                                'all_new_title'=>$all_new_title,
                                'all_new_quantity'=>$all_new_quantity,
                                'all_new_price'=>$all_new_price,
                                'all_new_cp'=>$all_new_cp,
                                'all_new_margin'=>$all_new_margin,
                                'all_new_sp'=>$all_new_sp,
                                'all_new_barcode_data'=>$all_new_barcode_data,
                                'pictureVariantData'=>$pictureVariantData,
                                'id'=>$default_already_id);
            $modelData = $this->Productmodel->UpdateProductData($array,$ids,$final_var_array,$this->input->post('MainSuplier'));
            if($modelData)
            {
                $this->session->set_flashdata('success',"Product updated successfully");
                redirect(base_url().'Product/editproduct/'.$ids, 'refresh');                     
            }
}
// delete product
if($this->input->post('delet_id'))
         {
            $if = $this->input->post('delet_id');
             $modelData = $this->Productmodel->DeleteProductDatavarint($if);
            
            
         }
          $id = $this->uri->segment(3);
              $returnData    = $this->Productmodel->FetchSingleProductData($id);
         $Data['one'] = $returnData['product'];
         $Data['All_one_var'] = $returnData['variant'];
         $Data['supplierAdd'] = $returnData['supplierAdd'];
         $Data['skuVar'] = $this->Productmodel->FetchSkuPrefix();
         $product_data1 = $this->Categorymodel->getCategoryTypeData('both');
         $product_data = $product_data1['category'];
         
         $Data['supplier'] = $this->Productmodel->fetchAllSupplier();
         
        $Data['WareHouse']= $this->Staffmodel->getWarhouse();
        $Data['category']=$product_data;
        $Data['type']=$product_data1['type'];
        $Data['taxData'] = $this->Productmodel->getTaxData();
        $this->load->view('editproductdata',$Data);
    }
    public function addProductInventory()
    {
        $this->load->view('addproductinventoryData');
    }
   
    public function exportdata()
    {
        $all =  $this->Productmodel->FetchProductData();
       $allStatus =  $this->Productmodel->fetchAllProductStatus();
       $getLogo = fetchvedorsitelogo();
       $url = base_url();
          
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Order List');
    $pdf->SetSubject('Houdin-e - Order List');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal')); 
$html ='<table style="width:100%;">
<tr style="text-align:center;"><td><img src="'.$getLogo.'" width="160px"></td></tr>
</table>
<br/>
<br/>
<table style="width:100%" border="1" cellpadding="10">
<tr>
<td style="text-align:center;">
<img src=$url"assets/images/product.png"/>
<h5>'.$allStatus['AllProduct'].'</h5>
<h5 style="color:#FF5722; font-size:10px;">Total Product</h5>
</td>
<td style="text-align:center;">
<img src=$url"assets/images/Active_p.png"/>
<h5>'.$allStatus['AllActive'].'</h5>
<h5 style="color:#FF5722; font-size:10px;">Total Active product</h5>
</td>
<td style="text-align:center;">
<img src=$url"assets/images/deactive_p.png"/>
<h5>'.$allStatus['AllDective'].'</h5>
<h5 style="color:#FF5722; font-size:10px;">Total Deactive Product</h5>
</td>

</tr>
</table>
<br/>
<br/>    
<table style="width:100%;" cellpadding="10" border="1">
<tr>
<td style="font-size:12px;font-weight:bold;">Item</td>
<td style="font-size:12px;font-weight:bold;">Price</td>
<td style="font-size:12px;font-weight:bold;">Stock</td>
<td style="font-size:12px;font-weight:bold;">Status</td>

</tr>';


foreach($all as $data)
{
$images = json_decode($data['houdinv_products_main_images'],true);

$price = json_decode($data['houdin_products_price'],true);

$stats=$data['houdin_products_status'];
if($stats==1)
{
  $bn = "Active";  
}
else
{
  $bn = "Deactive";   
}
if($images[0])
{
$rt = base_url()."upload/productImage/".$images[0];
}
else
{
$rt = "".$url."assets/images/users/avatar-1.jpg";    
}

$html .='<tr>
<td style="font-size:12px;">
<img src="'.$rt.'">
</td>
<td style="font-size:12px;">
$'.$price['price'].'
</td>
<td style="font-size:12px;">
'.$data['houdinv_products_main_stock'].'
</td>
<td style="font-size:12px;">
'.$bn.'
</td>

</tr>';

}

$html .='</table>
<br/>
<br/>
<table style="width:100%;"><tr><td style="font-size:10px;"><b>Note:</b>&nbsp;incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit</td></tr></table>';

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_product_list.pdf', 'I');
    }

/**
 * missingCategory()
 * @purpose: to fetch missing category and show
 * @created by: shivam singh sengar
 * @created at: 17 Dec 2019 22:37 PM
 * @param NONE
 * @return ARRAY
 */
public function missingcategory()
{
    // Custom Pagination
    $All= array();
    //get rows count
    $conditions['returnType'] = 'count';
    $totalRec = $this->Productmodel->missingProductData($conditions);

    //pagination config
    $config['base_url']    = base_url().'product/';
    $config['uri_segment'] = 3;
    $config['total_rows']  = $totalRec;
    $config['per_page']    = $this->perPage;
    //styling
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
    $config['cur_tag_close'] = '</a></li>';
    $config['next_link'] = 'Next';
    $config['prev_link'] = 'Prev';
    $config['next_tag_open'] = '<li class="pg-next">';
    $config['next_tag_close'] = '</li>';
    $config['prev_tag_open'] = '<li class="pg-prev">';
    $config['prev_tag_close'] = '</li>';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    //initialize pagination library
    $this->pagination->initialize($config);
    //define offset
    $page = $this->uri->segment(2);
    $offset = !$page?0:$page;
    //get rows
    $conditions['returnType'] = '';
    $conditions['start'] = $offset; 
    $conditions['limit'] = $this->perPage;

    $data['all'] =  $this->Productmodel->missingProductData($conditions);
    $data['new'] = $this->Categorymodel->getCategoryTypeData('both'); // fetch category and type of product
    $this->load->view('missing-category',$data);
}

public function addmissingcategory()
{
    if ($this->input->post('add_category')) { 
        $product_id = $this->input->post('product_id');
        $cat = $this->input->post('category_name');
        for ($i = 0; $i < count($product_id); $i++) {
            $cat = $cat[$i];
            $cat = explode(',',$cat);
            $cat1=array();
            $cat2=array();
            $cat3=array();
            foreach($cat as $subpartsArray)
            {
                $subparts = explode("^^",$subpartsArray);
                if($subparts[1]=='sub0')
                {
                    array_push($cat1,$subparts[0]);
                }
                else if($subparts[1]=='sub1')
                {
                    array_push($cat2,$subparts[0]);
                }
                else if($subparts[1]=='sub2')
                {
                    array_push($cat3,$subparts[0]);
                }
            }
            $updated_data[] = [
                "houdin_products_category"=>implode(",",$cat1),
                "houdin_products_sub_category_level_1"=>implode(",",$cat2),
                "houdin_products_sub_category_level_2"=>implode(",",$cat3),
                'product_id' => $product_id[$i]
            ];
        }
        $this->Productmodel->updateMissingCategory($updated_data);
        $this->session->set_flashdata('success','Category has been added successfully');
        redirect(base_url().'Product/missingcategory', 'refresh');   
    }    
}
public function missingImages()
{
    // Custom Pagination
    $All= array();
    //get rows count
    $conditions['returnType'] = 'count';
    $totalRec = $this->Productmodel->missingImagesData($conditions);

    //pagination config
    $config['base_url']    = base_url().'product/';
    $config['uri_segment'] = 3;
    $config['total_rows']  = $totalRec;
    $config['per_page']    = $this->perPage;
    //styling
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
    $config['cur_tag_close'] = '</a></li>';
    $config['next_link'] = 'Next';
    $config['prev_link'] = 'Prev';
    $config['next_tag_open'] = '<li class="pg-next">';
    $config['next_tag_close'] = '</li>';
    $config['prev_tag_open'] = '<li class="pg-prev">';
    $config['prev_tag_close'] = '</li>';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    //initialize pagination library
    $this->pagination->initialize($config);
    //define offset
    $page = $this->uri->segment(2);
    $offset = !$page?0:$page;
    //get rows
    $conditions['returnType'] = '';
    $conditions['start'] = $offset; 
    $conditions['limit'] = $this->perPage;

    $data['all'] =  $this->Productmodel->missingImagesData($conditions);
    $this->load->view('missing-images',$data);    
}

public function addMissingImages()
{
    if ($this->input->post('add_image')) { 
        $images_data = $_FILES['images'];
        $get_product_id = $this->input->post('product_id');
        for ($i=0; $i < count($images_data['name']); $i++) {
            if (!empty($_FILES['images']['name'][$i])) {
                $_FILES['file']['name']= $_FILES['images']['name'][$i];
                $_FILES['file']['type']= $_FILES['images']['type'][$i];
                $_FILES['file']['tmp_name']= $_FILES['images']['tmp_name'][$i];
                $_FILES['file']['error']= $_FILES['images']['error'][$i];
                $_FILES['file']['size']= $_FILES['images']['size'][$i]; 
                $newFileName = rand(100000,999999).$i.$_FILES['file']['name'];
                $config['upload_path'] = "upload/productImage/";
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $newFileName;
                $this->load->library('upload',$config);
                $this->upload->initialize($config);

                // upload image file
                if ($this->upload->do_upload('file')) {                                  
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                } else {
                    $error = array('error' => $this->upload->display_errors());
                    $picture = '';  
                }
                $update_images[] = [
                    'image' => $picture,
                    'id' => $get_product_id[$i]
                ];
            }
       }
       $this->Productmodel->updateMissingImages($update_images);
    $this->session->set_flashdata('success','images has been added successfully');
    redirect(base_url().'Product/missingImages', 'refresh');   
    }
}
}
