<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends CI_Controller {
	function __construct() 
    {
        parent::__construct();
        $this->load->library('form_validation'); 
        $this->load->model('Shopregistermodel');
        $this->load->model('Forgetmodel');
        $this->from = fromemail();
     }
	public function index()
	{
		$this->load->view('registershop');
	}
	public function activate()
	{
		if($this->session->userdata('tempString') == "")
		{
			$this->load->helper('url');
			redirect(base_url(), 'refresh');
		}
		// check otp data
		if($this->input->post('submitOtpData'))
		{
			$this->form_validation->set_rules('otp','text','required|max_length[5]|integer');
			if($this->form_validation->run() == true)
			{
				$getUserOtp = $this->input->post('otp');
				if($getUserOtp == $this->session->userdata('tempString'))
				{
					$getOTPStatus = $this->Shopregistermodel->checkUserOtpData($getUserOtp);
					if($getOTPStatus['message'] == 'yes')
					{
						$this->session->unset_userdata('tempPhone');
						$this->session->unset_userdata('tempString');
						$this->session->unset_userdata('temPname');
						$this->load->helper('url');
						redirect(base_url()."Register/complete", 'refresh');	
					}
					else
					{
						$this->session->set_flashdata('error','Something went wrong. Please try again');
						$this->load->helper('url');
						redirect(base_url()."Register/activate", 'refresh');	
					}
				}
				else
				{
					$this->session->set_flashdata('error','Please enter correct otp');
					$this->load->helper('url');
					redirect(base_url()."Register/activate", 'refresh');	
				}
			}
			else
			{
				$this->session->set_flashdata('error','All fields are mandatroy');
				$this->load->helper('url');
				redirect(base_url()."Register/activate", 'refresh');
			}
		}
		// update mobile number
		if($this->input->post('updateMobileNumber'))
		{
			$this->form_validation->set_rules('userAnotherPhone','text','required');
			if($this->form_validation->run() == true)
			{
				$setArrayData = array('phone'=>$this->input->post('userAnotherPhone'),'userId'=>$this->session->userdata('tempId'));
				$getMobileUpdateStatus = $this->Shopregistermodel->updateUserPhone($setArrayData);
				if($getMobileUpdateStatus['message'] == 'phone')
				{
					$this->session->set_flashdata('success','Mobile number is already exist');
					$this->load->helper('url');
					redirect(base_url()."Register/activate", 'refresh');
				}
				else if($getMobileUpdateStatus['message'] == 'yes')
				{
					// send message
					$getTwillio = gettwillioDetails();

					$getVarificationPin = $this->session->userdata('tempString');
					//$this->session->unset_userdata('tempPhone');
					$this->session->set_userdata('tempPhone',$this->input->post('userAnotherPhone'));
				  
					$id = $getTwillio[0]->houdin_twilio_sid;
					$token = $getTwillio[0]->houdin_twilio_token;
					$url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
					$from = $getTwillio[0]->houdin_twilio_number;

					$body = "Your houdin-e verification PIN is: ".$getVarificationPin."";
					$data1 = array (
							'From' => $from,
							'To' => $this->input->post('userAnotherPhone'),
							'Body' => $body,
					);
				 
					$post = http_build_query($data1);
					$x = curl_init($url );
					curl_setopt($x, CURLOPT_POST, true);
					curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
					curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
					curl_setopt($x, CURLOPT_POSTFIELDS, $post);
					$y = curl_exec($x);





					$this->session->set_flashdata('success','Mobile number updated successfully');
					$this->load->helper('url');
					redirect(base_url()."Register/activate", 'refresh');
				}
				else
				{
					$this->session->set_flashdata('error','Something went wrong. Please try again');
					$this->load->helper('url');
					redirect(base_url()."Register/activate", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('error','All fields are mandatroy');
				$this->load->helper('url');
				redirect(base_url()."Register/activate", 'refresh');
			}
		}
		// resend otp data
		if($this->input->post('resendOtpForms'))
		{
			//echo$this->session->userdata('tempPhone');
		//	echo 'hawkscode';
			$getTwillio = gettwillioDetails();

					$getVarificationPin = $this->session->userdata('tempString');
					// $this->session->unset_userdata('tempPhone');
					// $this->session->set_userdata('tempPhone',$this->input->post('userAnotherPhone'));
				  
					$id = $getTwillio[0]->houdin_twilio_sid;
					$token = $getTwillio[0]->houdin_twilio_token;
					$url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
					$from = $getTwillio[0]->houdin_twilio_number;

					$body = "Your houdin-e verification PIN is: ".$getVarificationPin."";
					$data1 = array (
							'From' => $from,
							'To' => $this->session->userdata('tempPhone'),
							'Body' => $body,
					);
				 
					$post = http_build_query($data1);
					$x = curl_init($url );
					curl_setopt($x, CURLOPT_POST, true);
					curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
					curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
					curl_setopt($x, CURLOPT_POSTFIELDS, $post);
					$y = curl_exec($x);
			// print_r($y);

			// die;
			$this->session->set_flashdata('success','OTP send successfully');
			$this->load->helper('url');
			redirect(base_url()."Register/activate", 'refresh');
		}
		$this->load->view('activate');
	}
	public function complete()
	{
		if($this->session->userdata('tempId') == "")
		{
			$this->load->helper('url');
			redirect(base_url(), 'refresh');
		}
		// insert shop data
		if($this->input->post('startYourShop'))
		{
			$this->form_validation->set_rules('storename','text','required');
			$this->form_validation->set_rules('email','text','required|valid_email');
			$this->form_validation->set_rules('city','text','required');
			$this->form_validation->set_rules('country','select option','required');
			$this->form_validation->set_rules('password','text','required');
			$this->form_validation->set_rules('confirmPassword','text','required');
			$this->form_validation->set_rules('sell','select option','required');
			$this->form_validation->set_rules('businessCategory','select option','required');
			$this->form_validation->set_rules('appbusinessCategory','select option','required');
			$this->form_validation->set_rules('businessLanguage','select option','required');
			$this->form_validation->set_rules('currency','select option','required');
			if($this->form_validation->run() == true)
			{
				$getPassword = $this->input->post('password');
				$getConfirmPassword = $this->input->post('confirmPassword');
				if($getPassword == $getConfirmPassword)
				{
					$setInsertArrayData = array(
						'storeName'=>$this->input->post('storename'),
						'email'=>$this->input->post('email'),
						'address'=>$this->input->post('address'),
						'City'=>$this->input->post('City'),
						'country'=>$this->input->post('country'),
						'password'=>$this->input->post('password'),
						'sell'=>$this->input->post('sell'),
						'category'=>$this->input->post('businessCategory'),
						'userId'=>$this->session->userdata('tempId'),
						'businessLanguage'=>$this->input->post('businessLanguage'),
						'currency'=>$this->input->post('currency'),
						'app_business_category' => $this->input->post('appbusinessCategory')
					);
					$getFinalShopDataStatus = $this->Shopregistermodel->updateShopDetail($setInsertArrayData);
					if($getFinalShopDataStatus['message'] == 'yes')
					{
						$this->session->unset_userdata('tempId');
						$this->session->set_flashdata('success','Shop created successsfully');
						$this->load->helper('url');
						redirect(base_url()."/vendor", 'refresh');
					}
					else if($getFinalShopDataStatus['message'] == 'shop')
					{
						$this->session->set_flashdata('error','Store name is not available');
						$this->load->helper('url');
						redirect(base_url()."Register/complete", 'refresh');
					}
					else if($getFinalShopDataStatus['message'] == 'email')
					{
						$this->session->set_flashdata('error','Email is already in used. Please try with different email address.');
						$this->load->helper('url');
						redirect(base_url()."Register/complete", 'refresh');
					}
					else if($getFinalShopDataStatus['message'] == 'noshop')
					{
						$this->session->set_flashdata('error','Something went wrong during shop creation. Please contact to our support');
						$this->load->helper('url');
						redirect(base_url()."Register/complete", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('error','Something went wrong. Please try again');
						$this->load->helper('url');
						redirect(base_url()."Register/complete", 'refresh');
					}
				}
				else
				{
					$this->session->set_flashdata('error','Password are not matched');
					$this->load->helper('url');
					redirect(base_url()."Register/complete", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('error','All fields are mandatroy');
				$this->load->helper('url');
				redirect(base_url()."Register/complete", 'refresh');
			}
		}
		$getExtraDataCategory = $this->Shopregistermodel->getExtraData();
		$this->load->view('complete',$getExtraDataCategory);
	}
	public function registerFirst()
	{
		$this->form_validation->set_rules('userName','text','required');
		$this->form_validation->set_rules('full_phone','text','required');
		if($this->form_validation->run() == true)
		{
			$setArrayData = array('name'=>$this->input->post('userName'),'phone'=>$this->input->post('full_phone'));
			$getInsertStatus = $this->Shopregistermodel->insertFirstProcess($setArrayData);
			if($getInsertStatus['message'] == 'success')
			{
				$randomString = $getInsertStatus['randomString'];
				$this->session->set_userdata('tempId',$getInsertStatus['id']);
				$this->session->set_userdata('tempPhone',$this->input->post('full_phone'));
				$this->session->set_userdata('tempString',$randomString);
				$this->session->set_userdata('temPname',$this->input->post('userName'));
				$getTwillio = gettwillioDetails();
				$id = $getTwillio[0]->houdin_twilio_sid;
				$token = $getTwillio[0]->houdin_twilio_token;
				$url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
				$from = $getTwillio[0]->houdin_twilio_number;

				$body = "Your houdin-e verification PIN is: ".$getVarificationPin."";
				$data1 = array (
						'From' => $from,
						'To' => $this->session->userdata('tempPhone'),
						'Body' => $body,
				);
			 
				$post = http_build_query($data1);
				$x = curl_init($url );
				curl_setopt($x, CURLOPT_POST, true);
				curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
				curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
				curl_setopt($x, CURLOPT_POSTFIELDS, $post);
				$y = curl_exec($x);
				echo 'success';
			}
			else if($getInsertStatus['message'] == 'phone')
			{
				echo 'Phone number is already exist';
			}
			else
			{
				echo 'Something went wrong. Please try again';	
			}
		}
		else
		{
			echo 'All fields are mandatory';
		}
	}
    
    public function ForgetMailSend()
    {
        
                if($this->input->post('email'))
        {
            $this->form_validation->set_rules('email','text','required|valid_email');
            if($this->form_validation->run() == true)
            {
              $getForgetUpdate = $this->Forgetmodel->sendForgetTokenData($this->input->post('email'));
                 if($getForgetUpdate['message'] == 'success')
                {
                    // send email to admin user
                    $url = 'https://api.sendgrid.com/';
                    $user = 'anuankita';
                    $pass = 'india@123';
                    $json_string = array(
                    'to' => array(
                        $getForgetUpdate['emaildata']
                    ),
                    'category' => 'test_category'
                    );
                    
                    
                    $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
  <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
    <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
      <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
        <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
          <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
              <img src="'.base_url().'assets/images/main-logo.png" style="max-width: 53%;width:100%"/></td></tr>
            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
              <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                  <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Your Security Pin is:</strong></td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                      <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$getForgetUpdate['tokendata'].'</td></tr>
                      <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
  <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
  <table width="100%" ><tr >
  <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e 2018--All right reserverd </td></tr>
  <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
  </tr></table>
  </div>
  </div></td></tr></table>';
                    
                    
                    $params = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'x-smtpapi' => json_encode($json_string),
                        'to'        => $getForgetUpdate['emaildata'],
                        'fromname'  => 'Houdin-e',
                        'subject'   => 'Recover Your Password',
                        'html'      => $htm,
                        'from'      => $this->from,
                    );
                    $request =  $url.'api/mail.send.json';
                    $session = curl_init($request);
                    curl_setopt ($session, CURLOPT_POST, true);
                    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                    curl_setopt($session, CURLOPT_HEADER, false);
                    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($session);
                    curl_close($session);
                    $getResponse = json_decode($response);
                    if($getResponse->message == 'success')
                    {
                       // $this->session->set_flashdata('success','Please check your mail');
                        $this->session->set_userdata('forgetuserTokenset',$getForgetUpdate['tokendata']);
                       echo "success";
                    }
                    else
                    {
echo "error";
                    }
                   
                }
                else
                {
                    $this->session->set_flashdata('error','Please enter correct email');
                    echo "error2";
                }
                
            }
            else
            {
             //   $this->session->set_flashdata('error','All fields are mandatory or please use proper format of email');
   echo "error4";
            }
        }
        else
        {
          echo "error4";  
        }
    }
    
    
    
    

    
    
}
