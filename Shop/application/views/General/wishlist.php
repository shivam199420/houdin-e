<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">Wishlist</h2>

            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- shopping-cart-area start -->
    <div class="cart-area ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="table-content table-responsive">
                            <table class="cart-table cart_prdct_table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                    <th class="cpt_img">image</th>
                                    <th class="cpt_pn">product name</th>
                                    <th class="stock">stock status</th>
                                    <th class="cpt_p">price</th>
                                    <th class="add-cart">add to cart</th>
                                    <th class="cpt_r">remove</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php

						// print_r($wishproducts);

                        $i=1;

                        foreach($wishproducts as $thisItem)
                        {
                          $image = json_Decode($thisItem->houdinv_products_main_images,true);
                          $stock = $thisItem->houdinv_products_total_stocks;
                          $price = json_DEcode($thisItem->houdin_products_price,true);
                          if($stock>0)
                          {
                            $status = "In stock";
                          }
                          else
                          {
                            $status = "Out of stock";
                          }
                        ?>
							<tr class="tr_row">
								<td><span class="cart-number"><?php echo $i; ?></span></td>
								<td><img style="width: 80px;" src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $image[0]; ?>" alt="" /></td>
								<td><a href="javascript:;" class="cart-pro-title"><?php echo $thisItem->houdin_products_title; ?></a></td>
								<td><p class="stock in-stock"><?php echo $status; ?></p></td>
								<td><p class="cart-pro-price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $price['sale_price']-$price['discount']; ?></p></td>
								<td><a   data-variant="0" data-cart="<?php echo $thisItem->houdin_products_id; ?>"  class="btn border-btn Add_to_cart_button cp_Wish_remove add_cart_custom" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>">add to cart</a></td>
								<td><a class="cp_Wish_remove" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>"><i class="fa fa-trash"></i></a></td>
							</tr>
						<?php
                        $i++;
						}

						foreach($wishvarint as $wishvarints)
                        {
                          $image = $wishvarints->houdin_products_variants_image;
                          $stock = $wishvarints->houdinv_products_variants_total_stocks;
                          $price = $wishvarints->houdin_products_variants_prices;
                          if($stock>0)
                          {
                            $status = "In stock";
                          }
                          else
                          {
                            $status = "Out of stock";
                          }
                        ?>
							<tr class="tr_row">
								<td><span class="cart-number"><?php echo $i; ?></span></td>
								<td><a href="#" class="cp_img"><img style="max-width: 40%;" src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $image; ?>" alt="" /></a></td>
								<td><a href="#" class="cart-pro-title"><?php echo $wishvarints->houdin_products_variants_title; ?></a></td>
								<td><p class="stock in-stock"><?php echo $status; ?></p></td>
								<td><p class="cart-pro-price"><?php echo $price; ?></p></td>
								<td><a  data-variant="<?php echo $wishvarints->houdinv_users_whishlist_item_variant_id; ?>" data-cart="0" class="btn border-btn Add_to_cart_button cp_Wish_remove" data-id="<?php echo $wishvarints->houdinv_users_whishlist_id; ?>">add to cart</a></td>
								<td><a class="cp_Wish_remove" data-id="<?php echo $wishvarints->houdinv_users_whishlist_id; ?>"><i class="fa fa-trash"></i></a></td>
							</tr>
						<?php
                        $i++;
                        } ?>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- shopping-cart-area end -->
    <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>
