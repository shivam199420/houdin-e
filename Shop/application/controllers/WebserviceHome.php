<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebserviceHome extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shophomemodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function categorydata()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getCategoryData = $this->Shophomemodel->fetchShopCategory($this->input->post('shopName'));
            print_r(json_encode($getCategoryData));  
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));  
        }
    }
    public function latestproductdata()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getProductData = $this->Shophomemodel->fetchShoplatestPorudct($this->input->post('shopName'));
            print_r(json_encode($getProductData));  
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray)); 
        }
    }
    public function getShopName()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getShopData = $this->Shophomemodel->fetchShopName($this->input->post('shopName'));
            print_r(json_encode($getShopData)); 
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray)); 
        }
    }
    public function getsliderinfo()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getShopData = $this->Shophomemodel->fetchSliderData($this->input->post('shopName'));
            print_r(json_encode($getShopData)); 
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray)); 
        }
    }
    public function getSubCategoryData()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('categoryId','text','required');
        if($this->form_validation->run() == true)
        {
            $setCategoryArray = array('shopName'=>$this->input->post('shopName'),'categoryId'=>$this->input->post('categoryId'));
            $resultArray = $this->Shophomemodel->fetchBaseCategory($setCategoryArray);
            print_r(json_encode($resultArray)); 
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray)); 

        }
    }
    public function getFinalCategoryLevel()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('categoryId','text','required');
        $this->form_validation->set_rules('subCategoryId','text','required');
        if($this->form_validation->run() == true)
        {
            $setCategoryArray = array('shopName'=>$this->input->post('shopName'),'categoryId'=>$this->input->post('categoryId'),'subCategoryId'=>$this->input->post('subCategoryId'));
            $resultArray = $this->Shophomemodel->fetchFinalSubCategory($setCategoryArray);
            print_r(json_encode($resultArray)); 
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray)); 

        }
    }

}
