        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/storesetting') ?>
        <link href="<?php echo base_url() ?>assets/plugins/nestable/jquery.nestable.css" rel="stylesheet" />
        <style type="text/css">
        .add_div_box {
        border: 1px solid #e6e6e6;
        border-radius: 0px 0px 5px 5px;
        background-color: #ffffff;
        padding: 10px;
        }
        .dd {
        position: relative;
        display: block;
        margin: 0;
        padding: 0;
        max-width: 100%!important;
        list-style: none;
        font-size: 13px;
        line-height: 20px;
        }
        .custom-dd .dd-list .dd-item .dd-handle {
        background: #f4f8fb!important;
        box-shadow: 0px 0px 2px 0px #607ec2;
        border: none;
        padding: 10px 16px!important;
        height: auto;
        color: #000!important;
        font-weight: 400!important;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        -moz-border-radius: 3px;
        background-clip: padding-box;
        margin-bottom: 12px;
        }
        .custom_dropdown {
        padding-left: 6px;
        display: block;
        background-color: rgb(240, 80, 80);
        margin: 0px;
        height: 22px;
        line-height: 20px;
        width: 22px; 
        color: rgb(255, 255, 255);
        border-radius: 2px;
        }
        .custom-dd .dd-list .dd-item button {
        background-color: rgb(248, 161, 0);
        margin: 10px;
        height: 22px;
        line-height: 20px;
        width: 22px;
        color: rgb(255, 255, 255);
        border-radius: 2px;
        }
        @media screen and (min-width: 768px){}
        .dropdown.dropdown-lg .dropdown-menu {
        min-width: 390px!important;
        }
        .checkbox{
        padding-left: 0px;
        }
        .checkbox, .radio {
        position: relative;
        display: block;
        margin-top: 0px;
        margin-bottom: 10px;
        }
        }
        </style>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">

        <!--START Page-Title -->
        <div class="row">

        <div class="col-md-8">
        <h4 class="page-title">Store Navigation</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>storesetting/Storesettinglanding">Store setting</a></li>
        <li class="active">Store Navigation</li>
        </ol>
        </div>

        </div>
        <!--END Page-Title --> 
        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>

        <div class="row m-t-20">
        <div class="col-sm-12 col-md-3">
        <div class="card-box" style="padding: 10px 20px;">

        <div class="row">
        <div class="row">
        <div class="row"> 
        <div class="col-lg-12"> 
        <div class="panel-group" id="accordion-test-2" style="padding:10px"> 
        <div class="panel panel-default"> 
        <div class="panel-heading"> 
        <h4 class="panel-title"> 
        <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseOne-2" aria-expanded="false" class="collapsed">
        Store Pages
        </a> 
        </h4> 
        </div> 
        <div id="collapseOne-2" class="panel-collapse collapse in"> 
        <div class="panel-body">
        <div class="row">
        <div class="col-md-12 form-horizontal">
        <?php 
        $setNavArray = array();
        foreach($customNav as $navigationdataList)
        {
            array_push($setNavArray,$navigationdataList->houdinv_navigation_store_pages_name);
        }
        ?>
        <div class="checkbox checkbox-custom">
        <input id="checkbox11" <?php if(in_array('home',$setNavArray)) { ?> checked="checked" <?php  } ?> class="customNavigation" type="checkbox" data-id="home">
        <label>Home</label>
        </div>
        <div class="checkbox checkbox-custom">
        <input id="checkbox11" <?php if(in_array('about',$setNavArray)) { ?> checked="checked" <?php  } ?> class="customNavigation" data-id="about" type="checkbox">
        <label>About Us</label>
        </div>
        <div class="checkbox checkbox-custom">
        <input id="checkbox11" <?php if(in_array('contact',$setNavArray)) { ?> checked="checked" <?php  } ?> class="customNavigation" data-id="contact" type="checkbox">
        <label>Contact Us</label>
        </div>
        <div class="checkbox checkbox-custom">
        <input id="checkbox11" <?php if(in_array('privacy',$setNavArray)) { ?> checked="checked" <?php  } ?> class="customNavigation" data-id="privacy" type="checkbox">
        <label>Privacy Policy</label>
        </div>
        <div class="checkbox checkbox-custom">
        <input id="checkbox11" <?php if(in_array('disclaimer',$setNavArray)) { ?> checked="checked" <?php  } ?> class="customNavigation" data-id="disclaimer" type="checkbox">
        <label>Disclaimer</label>
        </div>
        <div class="checkbox checkbox-custom">
        <input id="checkbox11" <?php if(in_array('terms',$setNavArray)) { ?> checked="checked" <?php  } ?> class="customNavigation" data-id="terms" type="checkbox">
        <label>Terms and conditions</label>
        </div>
        <div class="checkbox checkbox-custom">
        <input id="checkbox11" <?php if(in_array('shipping',$setNavArray)) { ?> checked="checked" <?php  } ?> class="customNavigation" data-id="shipping" type="checkbox">
        <label>Shipping and delivery policy</label>
        </div>
        <div class="checkbox checkbox-custom">
        <input id="checkbox11" <?php if(in_array('refund',$setNavArray)) { ?> checked="checked" <?php  } ?> class="customNavigation" data-id="refund" type="checkbox">
        <label>Cancellation and refund policy</label>
        </div>
        </div>
        </div>
        <div class="col-md-12 m-t-20 p-l-0">
        <button type="button" class="btn btn-default pull-left m-r-10 updateCustomNavigationData">Add to menu&nbsp;<i class="fa fa-spinner fa-spin showspin" style="display:none"></i></button>
        </div>
        </div> 
        </div> 
        </div>
        <!-- <div class="panel panel-default"> 
        <div class="panel-heading"> 
        <h4 class="panel-title"> 
        <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseTwo-2" class="collapsed" aria-expanded="false">
        Custom Pages
        </a> 
        </h4> 
        </div> 
        <div id="collapseTwo-2" class="panel-collapse collapse "> 
        <div class="panel-body">
        <div class="row">
        <div class="col-md-12 form-horizontal">
        <p>No Custom Pages Available!</p>
        </div>
        </div>
        <div class="col-md-12 m-t-20 p-l-0">
        <input type="submit" class="btn btn-default pull-left m-r-10" name="customSearchDriver" value="Add custom page">

        </div>
        </div> 
        </div> 
        </div>  -->
        <div class="panel panel-default"> 
        <div class="panel-heading"> 
        <h4 class="panel-title"> 
        <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseThree-2" class="collapsed" aria-expanded="false">
        Top level category list
        </a> 
        </h4> 
        </div> 
        <div id="collapseThree-2" class="panel-collapse collapse"> 
        <div class="panel-body">
        <div class="row">
        <div class="col-md-12 form-horizontal">
        <?php 
        $setCategoryArray = array();
        foreach($categoryNav as $categoryNavList)
        {
            array_push($setCategoryArray,$categoryNavList->houdinv_navigation_store_pages_category_id);
        }
        foreach($categoryData as $categoryDataList)
        {
        ?> 
        <div class="checkbox checkbox-custom">
        <input class="storeCategoryNav" <?php if(in_array($categoryDataList->houdinv_category_id,$setCategoryArray)) { ?> checked="checked" <?php  } ?> type="checkbox" data-name="<?php echo $categoryDataList->houdinv_category_name ?>" data-id="<?php echo $categoryDataList->houdinv_category_id ?>">
        <label><?php echo $categoryDataList->houdinv_category_name ?></label>
        </div>
        <?php }
        ?>
        </div>
        </div>
        <div class="col-md-12 m-t-20 p-l-0">
        <button type="button" class="btn btn-default pull-left m-r-10 updatedynamicCategory">Add to menu&nbsp;<i class="fa fa-spinner fa-spin showspinCat" style="display:none"></i></button>

        </div>
        </div> 
        </div> 
        </div> 
        <div class="panel panel-default"> 
        <div class="panel-heading"> 
        <h4 class="panel-title"> 
        <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseFour-2" class="collapsed" aria-expanded="false">
        Custom Links
        </a>  
        </h4> 
        </div> 
        <div id="collapseFour-2" class="panel-collapse collapse"> 
        <div class="panel-body">
        <form method="post" id="addCustomLinkFormData">
        <input type="hidden" class="setToken" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
        <div class="row">
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="control-label">Title</label>
        <input class="form-control required_validation_for_add_custom_links name_validation" maxlength="30" type="text" name="title">
        </div>

        <div class="form-group">
        <label class="control-label">URL</label>
        <input class="form-control required_validation_for_add_custom_links name_validation" maxlength="100" type="text" name="url">
        </div>
        <div class="form-group">
        <label class="control-label">Target</label>
        <div class="">
        <select class="form-control required_validation_for_add_custom_links" name="target">
        <option value="">Choose Target</option>
        <option value="current">Current Tab</option>
        <option value="new">New Tab</option>
        </select>
        </div>
        </div>
        <div class="form-group">
        <button type="submit" class="btn btn-default pull-left m-r-10 setCustomLinkBtn">Add to menu&nbsp;<i class="fa fa-spinner fa-spin showspinCustomLinks" style="display:none"></i></button>
        </div>
        </div>
        </div>
        </form>
        </div> 
        </div> 
        </div>
        </div> 
        </div>

        </div> <!-- end row -->
        </div>



        </div>


        </div>
        </div>
        <div class="col-sm-12 col-md-9">
        <div class="card-box" style="border-radius: 5px 5px 0px 0px!important;margin-bottom: 0px!important;">
        <div class="row">
        <div class="pull-left">
        <h5 style="margin-left: 10px">Menu</h5>
        </div>
        <!-- <div class="pull-right">
        <button type="button" class="btn btn-info waves-effect waves-light m-5">Expand All</button>
        <button type="button" class="btn btn-warning waves-effect waves-light m-5">Collapse All</button>
        <button type="button" class="btn btn-danger waves-effect waves-light m-5">Reset to Default</button>
        <button type="button" class="btn btn-success waves-effect waves-light m-5">Save</button>
        </div> -->
        </div>
        </div>

        <div class="add_div_box">
        <div class="custom-dd dd" id="nestable_list_1">
        <ol class="dd-list appendDataValue connected-sortable droppable-area1">
        <?php 
        foreach($navigationdata as $navigationdataValue)
        {
            if($navigationdataValue->houdinv_navigation_store_pages_name == 'home')
            {
                $setNavigationName = 'Home';
            }
            else if($navigationdataValue->houdinv_navigation_store_pages_name == 'about')
            {
                $setNavigationName = 'About Us';
            }
            else if($navigationdataValue->houdinv_navigation_store_pages_name == 'contact')
            {
                $setNavigationName = 'Contact Us';
            }
            else if($navigationdataValue->houdinv_navigation_store_pages_name == 'privacy')
            {
                $setNavigationName = 'Privacy';
            }
            else if($navigationdataValue->houdinv_navigation_store_pages_name == 'disclaimer')
            {
                $setNavigationName = 'Disclaimer';
            }
            else if($navigationdataValue->houdinv_navigation_store_pages_name == 'terms')
            {
                $setNavigationName = 'Terms & Conditions';
            }
            else if($navigationdataValue->houdinv_navigation_store_pages_name == 'shipping')
            {
                $setNavigationName = 'Shipping and Delivery Policy';
            }
            else if($navigationdataValue->houdinv_navigation_store_pages_name == 'refund')
            {
                $setNavigationName = 'Cancellation and refund policy';
            }
            else if($navigationdataValue->houdinv_navigation_store_pages_category_id != 0)
            {
                $setNavigationName = $navigationdataValue->houdinv_navigation_store_pages_category_name;
            }
            else if($navigationdataValue->houdinv_navigation_store_pages_custom_link_id != 0)
            {
                $setNavigationName = $navigationdataValue->houdinv_navigation_store_pages_custom_link_name;
            }   
            ?>

            <?php 
            if($navigationdataValue->houdinv_navigation_store_pages_custom_link_id == 0)
            {
            ?>
            <li class="dd-item draggable-item" data-id="<?php echo $navigationdataValue->houdinv_navigation_store_pages_id ?>">
                <div class="dd-handle ">
                <?php echo $setNavigationName ?> <span class="pull-right"><a href="javascript:;" data-id="<?php echo $navigationdataValue->houdinv_navigation_store_pages_id ?>" class="removeMenu btn btn-xs btn-danger"><i class="fa fa-remove"></i></a></span>
                </div>
                </li>
            <?php }
            else
            {
            ?>
            <li class="dd-item draggable-item" data-id="<?php echo $navigationdataValue->houdinv_navigation_store_pages_id ?>">
                <div class="dd-handle">
                <?php echo $setNavigationName ?> <span class="pull-right"><a href="javascript:;" data-id="<?php echo $navigationdataValue->houdinv_navigation_store_pages_id ?>" class="removeCustomLinkMenu btn btn-xs btn-danger"><i class="fa fa-remove"></i></a></span>
                </div>
                </li>
            <?php }
            ?>
        <?php }
        ?>
        <!-- <li class="dd-item">
        <div class="dd-handle">
        Home <span class="pull-right custom_dropdown"><i class="fa fa-remove"></i></span>
        </div>
        </li>
        <li class="dd-item">
        <div class="dd-handle">
        About us
        </div>
        </li>
        <li class="dd-item">
        <div class="dd-handle">
        Contact us
        </div>
        </li>

        <li class="dd-item">
        <div class="dd-handle ">
        Dresses
        </div>
        <ol class="dd-list">
        <li class="dd-item">
        <div class="dd-handle">
        Test 1
        </div>
        </li>
        <li class="dd-item">
        <div class="dd-handle">
        Test 2
        </div>
        </li> -->

        </ol>
        </li>
        </ol>
        </div>
        <!--<ul class="custom_ul_navigation">
        <li class="custom_li_navigation"></li>

        </ul>-->
        </div>
        </div>

        </div>
        <!-- Delete navigation data -->
        <div id="deleteNavigationModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Storesetting/deleteNavigation' ), array( 'method'=>'post' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Customer</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleteNavigationId" name="deleteNavigationId"/>
        <h4><b>Do you really want to Delete this navigation ?</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="" value="Delete">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>

        </div>
        <!-- delete custom links data -->
        <div id="deleteCustomNavigationModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Storesetting/deleteCustomLinkNavigation' ), array( 'method'=>'post' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Customer</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleteCustomMenuNavigationId" name="deleteCustomMenuNavigationId"/>
        <h4><b>Do you really want to Delete this navigation ?</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="" value="Delete">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>

        </div>

        <?php $this->load->view('Template/footer.php') ?>
        <script src="<?php echo base_url() ?>assets/plugins/nestable/jquery.nestable.js"></script>
        <!-- <script src="https://tech.hawkscode.in/houdin-e/vendor/assets/pages/nestable.js"></script> -->
        <script>
$( init );

function init() {
  $( ".droppable-area1, .droppable-area2" ).sortable({
      stop: function(event, ui) {
      //  alert("New position: " + ui.item.index());
//alert("Data Id : " + ui.item.attr('data-id'));
    },
      connectWith: ".connected-sortable",
      stack: '.connected-sortable ol'
    }).disableSelection();
}

        </script>
