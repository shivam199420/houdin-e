<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">Forgot your password</h2>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- login area end -->
    <div class="login-area ptb-100">
        <div class="container">
            <div class="row">
            <?php
            if($this->session->flashdata('error'))
            {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
            }
            if($this->session->flashdata('success'))
            {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
            }
                ?>
                <div class="col-md-6 col-md-offset-3">
                    <div class="login-content">
                        <div class="login-title">
                            <h4>Change Password</h4>
                        </div>
                        <div class="login-form">
                        <?php echo form_open(base_url( 'Forgot/updatePass' ), array( 'id' => 'forgotPass', 'method'=>'post' ));?>
                        <input type="text" name="forgotPin" placeholder="Please Enter Pin" class="required_validation_for_user_forgot name_validation number_validation" maxlength="4" />
                        <input type="password" name="forgotPass" placeholder="Password" class="required_validation_for_user_forgot name_validation opass" />
                        <input type="password" name="forgotConPass" placeholder="Re-enter Password" class="required_validation_for_user_forgot name_validation cpass"  />
                                
                                <div class="button-remember">
                                 
                                    <button class="login-btn setDisableData" type="submit">Reset Password</button>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer') 
  ?>


  <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#forgotPass',function(){
                var check_required_field='';
                $(this).find(".required_validation_for_user_forgot").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>