<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Webservicefetchproductlistview extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopproductlistmodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function index()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('productId','text','required');
        $this->form_validation->set_rules('productVariantId','text','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('shopname'=>$this->input->post('shopName'),'productId'=>$this->input->post('productId'),'productVariantId'=>$this->input->post('productVariantId'),'userId'=>$this->input->post('userId'));
            $getProductArray = $this->Shopproductlistmodel->fetchProductListViewModelApp($setArray);
            print_r(json_encode($getProductArray));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function fetchRelatedProducts()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('productId','text','required');
        if($this->form_validation->run() == true)
        {
            $setProductData = array('shopName'=>$this->input->post('shopName'),'productId'=>$this->input->post('productId'));
            $resultArray = $this->Shopproductlistmodel->fetchRelatedProducts($setProductData);
            print_r(json_encode($resultArray));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
}
