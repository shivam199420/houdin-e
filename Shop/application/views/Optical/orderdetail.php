<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view("".$getShopName."/Template/header"); ?>

<section id="order-detail">
	<div class="container">
		<div class="row">

   <div class="col-md-12 order-heading">
   	<h3>order-detail</h3>
   </div>



			<div class="col-md-12">
			<div class="table-responsive">
<table class="table main-table" cellpadding="10" cellspacing="10">

<tbody>

	<tr>
		<td>
			<ul>
				<li><b>orderid:</b>#<?php echo $orders->houdinv_order_id ;?></li>
				<li><b>order status:</b><?php echo $orders->houdinv_order_confirmation_status ;?></li>
				<li><b>order date:</b><?php echo date("Y-m-d ,h:i:s",$orders->houdinv_order_created_at) ;?></li>
			</ul>

		</td>
		<td align="right" style="vertical-align: middle;">
		 						<?php
		 		 $array_return = array("Delivered");
		 		 if( in_array($orders->houdinv_order_confirmation_status,$array_return))
		 		 {
		 				 ?>
		 					 <form id="return_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
		 							<input type="text" name="returnOrderId" value="<?php echo $this->uri->segment("3"); ?>"/>
		 						<input type="text" value="returnOrder" name="returnOrder"/>
		 						 </form>
		 	 <button class="btn return_button button">Return Order</button>
		 	<?php
		 					}

		 		 $array_cancel = array("unbilled","Not Delivered","billed","assigned");
		 		 if( in_array($orders->houdinv_order_confirmation_status,$array_cancel))
		 		 {
		 				 ?>

		 						 <form id="cancel_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
		 							<input type="text" name="cancelOrderId" value="<?php echo $this->uri->segment("3"); ?>"/>
		 						<input type="text" value="cancelOrder" name="cancelOrder"/>
		 						 </form>
		 	 <button class="btn cancel_button button">Cancel Order</button>
		 	<?php
		 		 } ?>

		</td>

	</tr>

	<tr><th  colspan="2">confirm products</th></tr>

	<tr>
		<td colspan="2"><table cellpadding="10" width="100%" class="table second-table">
			<tr bgcolor="#f5f5f5">
				<th>product name</th>
				<th>quality</th>
				<th>rate</th>
				<th>tax</th>
				<th>total price</th>
			</tr>



			<?php
			$total_product_gross = 0;
			foreach($orders->product_all as $detail)
			{ ?>
			<tr>
			<td> <?php echo $detail->title ; ?></td>
			<td><?php echo $detail->allDetail->product_Count ; ?></td>
			<td><?php echo $detail->allDetail->product_actual_price; ?></td>
			<td>0</td>
			<td><?php echo $detail->allDetail->total_product_paid_Amount; ?></td>
		 </tr>
			<?php
			$total_product_gross = $total_product_gross + $detail->allDetail->total_product_paid_Amount;
			} ?>

		</table>
	</td>
</tr>


			<tr>
				<td colspan="5" style="border: 0;padding-top: 0 !important;">
					<table class="table-third" cellpadding="10" width="100%" style="text-align: right;">

						<tr>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td><strong>Gross Amount</strong></td>
						<td>&#8377; <?php echo $total_product_gross;?></td>

						</tr>

						<tr>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td><strong>Additional Discount</strong></td>
						<td>&#8377; <?php echo  $orders->houdinv_orders_discount; ?></td>

						</tr>
						<tr>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td><strong>Delivery Charges</strong></td>
						<td>&#8377; <?php echo $orders->houdinv_delivery_charge; ?></td>

						</tr>
						<tr>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td><strong>Round Off</strong></td>
						<td>&#8377; <?php echo $orders->houdinv_orders_total_Amount; ?></td>

						</tr>
							<tr>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td class="thick-line">&nbsp;</td>
						<td><strong>Net Payable</strong></td>
						<td>&#8377; <?php echo $orders->houdinv_orders_total_Amount; ?></td>

						</tr>





					</table>
				</td>

			</tr>

</tbody>
		</table>
		</div>



</div>
</div>
</div>
</section>


<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view("".$getShopName."/Template/footer"); ?>

			<script>
			$(document).on("click",".cancel_button",function()
			{
				 $("#cancel_form").submit();

			});

				$(document).on("click",".return_button",function()
			{
				 $("#return_form").submit();

			});

			</script>
