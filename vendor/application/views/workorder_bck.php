<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/possidebar') ?>
<link href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
 <style>
 
 .p_success
 {
    
    color:green;
    padding-left: 20px;
 }
 
  .p_danger
 {
    
    color:red;
     padding-left: 20px;
 }
 </style>
 <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!--START Page-Title --> 
            <div class="row">
             
                <div class="col-md-8">
                   <h4 class="page-title">Workorder</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">POS</a></li>
                  <li class="active">Workorder</li>
                  </ol>
                  </div>
                 
            </div>
            
                                            
       <?php     
                     if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    ?>
    <div class="setErrorMessage" style="display:none"></div>    
            
           <!--END Page-Title --> 
            <div class="row m-t-20">
              <div class="col-lg-12">
                <div class="card-box">
                 
                                  
 
    <?php echo form_open(base_url().'Workorder',array("id"=>"AddWorkorder","enctype"=>"multipart/form-data")); ?>
 
         
                    <div class="form-group">
                      <label for="userName">Customer Name</label>
                      <select class="form-control select_name select1 getCustomerAddress  required_validation_for_workorder" tabindex="0" name="customer_name">
<option value="">Choose one</option>
<?php foreach($getCustomerData['customerList'] as $value)
{
   ?>
 
      
         <option value="<?php echo $value->houdinv_user_id; ?>" data-phone="<?php echo $value->houdinv_user_contact; ?>"><?php echo $value->houdinv_user_name; ?></option>
                                                <?php 
}?>
                                           
                      </select>
                    
                    </div>
                    <div class="form-group">
                      <label for="userName">Customer Contact</label>
                      <input type="text" name="customer_phone" tabindex="1" parsley-trigger="change"  placeholder="Customer Contact" class="form-control required_validation_for_workorder" id="userPhone">
                    </div>
                    <div class="form-group">
                      <label for="userName">Choose Product</label>
                      <input type="hidden" class="productIdData" name="productIdData"/>
                      <input type="hidden" class="variantIdData" name="variantIdData"/>
                     <select class="form-control select1 product_change"  tabindex="2">
                      <option value="">Choose One</option>
                      <?php foreach($getProductData as $value)
                      {
                      ?>
                      <option value="<?php echo $value['houdin_products_id']; ?>" data-stock="<?php echo $value['productStock']; ?>" data-variant="<?php echo $value['variantId']; ?>" data-product="<?php echo $value['productId']; ?>" data-price="<?php echo $value['productPrice']; ?>"><?php echo $value['productName']; ?></option>
                      <?php 
                      }?>
                      </select>
                   
                    </div>
                    <div class="form-group">
                      <label for="userName">Choose Quantity</label>
       <input type="text" id="order_customerquantity" tabindex="3" name="customer_quantity" parsley-trigger="change"  placeholder="Product Quantity" class="form-control quantityData required_validation_for_workorder number_validation" >
                            
                   
                    </div>
                    <div class="form-group">
                      <label for="userName">Coupon</label>
                       <div class="row">
                <div class="col-sm-12">
                <div class="col-md-11">
                  <input type="hidden" class="couponIdData" name="couponIdData"/>
                      <input type="text" tabindex="4" name="customer_coupon" parsley-trigger="change"  placeholder="Coupon Code" class="form-control coupon_code" 
                      >
                    
                      <p class="change_p "></p>
                      </div>
                      <div class="col-md-1">
                      <button class="btn btn-primary waves-effect waves-light Apply_coupon" tabindex="5" type="button">
                        Apply
                      </button>
                      </div>
                      </div>
                      </div>
                     
                    </div>
                     <div class="form-group">
                      <label for="userName">Pick Up Type</label>
                     <select class="form-control  required_validation_for_workorder" tabindex="6" name="Customer_pickup_type">
                     <option value="">Choose One</option>
                      <option value="deliver">Home Delivery</option>
                      <option value="pick_up">Customer Pickup</option>
                    </select>
                   
                    </div>
                    <div class="form-group">
                      <label for="userName">Delivery Address</label>
                      <select class="form-control required_validation_for_workorder setUserAddress" tabindex="7" name="customer_address">

                      </select>
                      
                      
                    </div>
                    <div class="form-group">
                      <label for="userName">Note</label>
                      <textarea rows="3"  name="customer_note" tabindex="8" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="userName">Payment Option</label>
                     <select class="form-control  required_validation_for_workorder" tabindex="9" name="customer_payment_type">
                      <option value="">Choose one</option>
                      <option value="cash">Cash</option>
                      <option value="card">Card</option>
                      <option value="cheque">Cheque</option>
                    </select>
                   
                    </div>

                    <div class="form-group">
                      <label for="userName">Work Completion Details</label>
                      <input type="text" name="customer_work_completion_detail" tabindex="10" parsley-trigger="change"  placeholder="" class="form-control" id="userName">
                     
                    </div>
                    <div class="form-group">
                      <label for="userName">Total Amount Paid</label>
                      <input type="text" onkeyup="checkDec(this);" readonly="" name="customer_total_amount_paid" parsley-trigger="change"  placeholder="" class="form-control required_validation_for_workorder total_paid" id="userName">
                      
                    </div>
                    <div class="form-group">
                      <label for="userName">Discount Applied</label>
                      <input type="text" onkeyup="checkDec(this);" readonly="" name="customer_discount" parsley-trigger="change"  placeholder="" class="form-control coupon_discount " id="userName">
               
                    </div>
                    <div class="form-group">
                      <label for="userName">Net Payable Amount</label>
                      <input type="text" onkeyup="checkDec(this);" readonly="" name="Customer_Net_payble_amount" parsley-trigger="change"  placeholder="" class="form-control required_validation_for_workorder net_paid" id="userName">
                      
                    </div>
                    <div class="form-group">
                      <label for="userName">Payment Status</label>
                      <select class="form-control required_validation_for_workorder" tabindex="11" name="paymentStatus">
                        <option value="">Choose Status</option>
                        <option value="1">Paid</option>
                        <option value="0">Not Paid</option>
                      </select>
                      
                    </div>
                    <div class="form-group">
                      <label for="userName">Payment Status</label>
                      <select class="form-control required_validation_for_workorder" tabindex="12" name="outletData">
                        <option value="">Choose Warehouse</option>
                        <?php 
                         
                         if($this->session->userdata('vendorOutlet') == 0)
                         {
                        foreach($getOutlet as $getOutletList)
                        {
                          ?>
                          <option value="<?php echo $getOutletList->id ?>"><?php echo $getOutletList->w_name ?></option>
                        <?php } }
                        else
                        {
                            foreach($warehouseData as $warehouseDataList)
                            {
                                if($this->session->userdata('vendorOutlet') == $warehouseDataList->id)
                                {
                                ?>
                                <option value="<?php echo $warehouseDataList->id ?>"><?php echo $warehouseDataList->w_name ?></option>
                                <?php }
                                ?>
            
                            <?php }
                        }
                        ?>
                      </select>
                      
                    </div>
                    	<div class="form-group">
								<label>Delivery Date & Time</label>
								<div>
									<div class="input-group">
													
														<input type="text" tabindex="13" class="form-control datepicker required_validation_for_workorder" id="order_delivery_date" name="order_delivery_date" placeholder="Date"/>
														<span class="input-group-addon">&</span>
														<input type="text" tabindex="14" class="form-control timepicker required_validation_for_workorder" id="order_delivery_time" name="order_delivery_time" placeholder="Time"/>
									</div>									
									  
								</div>
							</div>

                   

                    <div class="form-group m-b-0">
                    
                      <button class="btn btn-primary waves-effect waves-light" tabindex="14" name="savedata" value="submit" type="submit">
                        Save
                      </button>
                      
                    </div>
                    
      <?php echo form_close(); ?>
                </div>
              </div>
              
              
            </div>

                                      

                    
                                    
        


      </div>
    </div>
  </div>




 

<?php $this->load->view('Template/footer.php') ?>
<script src="<?php echo base_url() ?>assets/plugins/moment/moment.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<script>
                  function checkDec(el){
 var ex = /^[0-9]+\.?[0-9]*$/;
 if(ex.test(el.value)==false){
   el.value = el.value.substring(0,el.value.length - 1);
  }
 }
 
 
 function checkINt(el){
    

 var ex = /^[0-9]*$/;
 if(ex.test(el.value)==false){
   el.value = el.value.substring(0,el.value.length - 1);
  }
 }


	jQuery('.timepicker').timepicker({defaultTIme : true,minuteStep : 15,autoclose: true,});
	jQuery('.datepicker').datepicker({
			autoclose: true,
			todayHighlight: true,
			format: 'dd-mm-yyyy',
	});


$(document).ready(function(){

    $(".select1").select2({
    minimumInputLength: 2
});
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
  
  
  
        $(document).on('submit','#AddWorkorder',function(c){
           
            
                               var rep_image_val='';
                 $(this).find(".required_validation_for_workorder").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                        
                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
                
   
                $('.required_validation_for_workorder').on('keyup blur change',function()
                                {
                                    
                                        $(this).css("border-color","#ccc");
                                        $(this).siblings('.message_text').text('');
                                });
                                
                               
                if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                else 
                {
                  return true;
                }
        
                
            
          });
  
  
  
  
});

$(".select_name").on("change",function()
{
    var option = $('option:selected', this).attr('data-phone');
  //  alert(option);
   $("#userPhone").val(option); 
});


$(".product_change").on("change",function()
{
  var coupon_val =  $(".coupon_discount").val();
  var quantity = $("#order_customerquantity").val();
  var option1 = parseFloat($(this).children('option:selected').attr('data-price'));
  var productId = $(this).children('option:selected').attr('data-product');
  var variantId = $(this).children('option:selected').attr('data-variant');
  $(".total_paid").val(option1); 
  if(quantity)
  {
    quantity = parsefloat(quantity);
  }
  else 
  {
    quantity = 0
  }
option1 = option1*quantity;
if(!coupon_val)
{
$(".net_paid").val(option1); 
}
else
{
   var overall1 = ((coupon_val)/100)*option1;
   var finals1 = option1 - overall1; 
   $(".net_paid").val(finals1); 
}
$('.quantityData').attr('data-max',$('.product_change').children('option:selected').attr('data-stock'));
$('.productIdData').val(productId);
$('.variantIdData').val(variantId);
});


$("#order_customerquantity").on("keyup change paste blur",function()
{
     var coupon_val =  $(".coupon_discount").val();
      var total_amount = $('option:selected', '.product_change').attr('data-price');  
     
    var quantity = $(this).val();
      var option1 = total_amount*quantity;
      if(!coupon_val)
{
    $(".total_paid").val(option1); 
$(".net_paid").val(option1); 
}
else
{
    $(".total_paid").val(option1); 
   var overall1 = ((coupon_val)/100)*option1;
   var finals1 = option1 - overall1; 
   $(".net_paid").val(finals1); 
}
      
      
});

$(".Apply_coupon").on("click",function()
{
  $('.Apply_coupon').prop('disabled',true);
    var coupon = $(".coupon_code").val();
    if($('.productIdData').val())
    {
      var product = $('.productIdData').val();
    }
    else
    {
      var product = $('.variantIdData').val();
    }
 
 if(product)
 {
    if(coupon)
    {
  $.ajax({
  type: 'POST',
  url: '<?php echo base_url(); ?>Workorder/checkCoupon',
  data:{code:coupon},
  success: function(data_res) {
    $('.Apply_coupon').prop('disabled',false);
  var datas = $.parseJSON(data_res);
  console.log(datas);
  if(datas.msg =="no")
  {
      var total_amount = $(".total_paid").val(); 
 
   $(".net_paid").val(total_amount);
     $(".coupon_discount").val("");
 $(".change_p").replaceWith('<p class="change_p p_danger">No such coupon code</p>');
  }
  else if(datas.msg =="yes")
  {
   var total_amount = $(".total_paid").val(); 
   var overall = ((datas.percent)/100)*total_amount;
   var finals = total_amount - overall;
   $(".net_paid").val(finals); 
    $(".coupon_discount").val(datas.percent);
    $('.couponIdData').val(datas.id);
   $(".change_p").replaceWith('<p class="change_p p_success">Discount '+datas.percent+'% Applied successfully </p>'); 
  }
  else
  {
      var total_amount = $(".total_paid").val(); 
 
   $(".net_paid").val(total_amount);
     $(".coupon_discount").val("");
 $(".change_p").replaceWith('<p class="change_p p_danger">Something went wrong ! Try Again</p>');
  }

  
  
  }
 });
 }
 else
 {
  $('.Apply_coupon').prop('disabled',false);
    alert("Please fill coupon code first");
 }
 }
 else
 {
  $('.Apply_coupon').prop('disabled',false);
    alert("Please select product first");
 }
 
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('change','.getCustomerAddress',function(){
    $('.setErrorMessage').hide().text('');
    var customerId = $(this).val();
    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>POS/getCustomerAddress",
        data: {'customerId':customerId },
        success: function(data) {
            var getJsonData = jQuery.parseJSON(data);
            if(getJsonData.message == 'Success')
            {
                var setCustomerAddress = '<option value="">Choose Address</option>';
                $.each(getJsonData.data,function(){
                    setCustomerAddress += '<option value="'+this.addressId+'">'+this.mainAddress+', '+this.city+', '+this.zip+'</option>'
                });
                $('.setUserAddress').html(setCustomerAddress);
            }
            else
            {
                $('.setErrorMessage').show().text(getJsonData.message);
            }
        }
        }); 
  });
  $(document).on('keyup','.quantityData',function(){
    if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
    {
      $(this).val($(this).attr('data-max'));
    }
  })
})
</script>