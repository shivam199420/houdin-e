<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function getfolderName($getShopName)
{
    $CI =& get_instance();
    $masterDB = $CI->load->database('master',true);
    $getFolderName = $masterDB->select('houdin_user_shops.houdin_user_shops_category,houdin_templates.houdin_template_file')->from('houdin_user_shops')->where('houdin_user_shops.domain_name',$getShopName)
    ->join('houdin_templates','houdin_templates.houdin_template_category = houdin_user_shops.houdin_user_shops_category')->get()->result();
    $getExplodeData = explode('/',$getFolderName[0]->houdin_template_file);
    return $getExplodeData[0];

}