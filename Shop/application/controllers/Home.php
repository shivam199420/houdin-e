<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopsettingmodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function index()
    {
        $getShopName = 'karma';
        // $this->session->set_userdata('shopName',$this->setShopName);
        $this->session->set_userdata('shopName',$getShopName);
        $getData = $this->Shopsettingmodel->fetchShopData($getShopName);
        $getShopNameData = $getData['shopData'][0]->houdin_template_file;
        $getShopNameData = getfolderName($this->session->userdata('shopName'));
        $this->load->view($getShopNameData."/home",$getData);
    }
}
