<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 
   <!-- About Banner Start -->

   <section id="about">

       <div class="container">

           <div class="row">

               <div class="about-heading text-center">

                   <h2>Wish List</h2>

               </div>

           </div>

       </div>

   </section>

<!-- About Banner End -->



<!-- Shopping Cart Start -->

<section id="card">

    <div class="container">

        <div class="row ">

            <div class="col-md-12">

                <div class="cart-info">
                  <div class="table-responsive">

                    <table class="table table-striped table-hover table-bordered">

                        <tbody>

                            <tr>

                                <th class="cpt_no">#</th>

                                <th class="tl">Product Descriptions</th>

                                <th>Stock Status</th>

                                <th>Total Price</th>

                                <th>Remove Item</th>

                            </tr>

                            <?php

                						// print_r($wishproducts);

                                        $i=1;

                                        foreach($wishproducts as $thisItem)
                                        {
                                          $image = json_Decode($thisItem->houdinv_products_main_images,true);
                                          $stock = $thisItem->houdinv_products_total_stocks;
                                          $price = json_DEcode($thisItem->houdin_products_price,true);
                                          if($stock>0)
                                          {
                                            $status = "In stock";
                                          }
                                          else
                                          {
                                            $status = "Out of stock";
                                          }
                                        ?>

                            <tr>

                              <td><span class="cart-number"><?php echo $i; ?></span></td>

                                <td>

                                    <div class="col-md-12 pl">

                                        <div class="col-md-3 pl images_width">

                                            <img src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $image[0]; ?>" alt="cart1" class="img-responsive" style="height: 100%;">

                                        </div>

                                        <div class="col-md-9 pro-info pl text-left">

                                            <h3><?php echo $thisItem->houdin_products_title; ?></h3>

                                        </div>

                                    </div>

                                </td>



                                <td>

                                  <p class="stock in-stock"><?php echo $status; ?></p>

                                </td>

                                  <td><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $price['sale_price']-$price['discount']; ?></td>

                                <td><a class="cp_Wish_remove" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>"><i class="fa fa-trash-o"></i></a></td>

                            </tr>
                            <?php
                                        $i++;
                                        }
                                        foreach($wishvarint as $wishvarints)
                                                      {
                                                        $image = $wishvarints->houdin_products_variants_image;
                                                        $stock = $wishvarints->houdinv_products_variants_total_stocks;
                                                        $price = $wishvarints->houdin_products_variants_prices;
                                                        if($stock>0)
                                                        {
                                                          $status = "In stock";
                                                        }
                                                        else
                                                        {
                                                          $status = "Out of stock";
                                                        }
                                                      ?>
                                                      <tr>

                                                        <td><span class="cart-number"><?php echo $i; ?></span></td>

                                                          <td>

                                                            <div class="col-md-12 pl">

                                                                <div class="col-md-3 pl">

                                                                    <img src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $image[0]; ?>" alt="cart1" class="img-responsive">

                                                                </div>

                                                                <div class="col-md-9 pro-info pl text-left">

                                                                    <h3><?php echo $thisItem->houdin_products_title; ?></h3>

                                                                </div>

                                                            </div>

                                                          </td>



                                                          <td>

                                                            <p class="stock in-stock"><?php echo $status; ?></p>

                                                          </td>

                                                    <td><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $price['sale_price']-$price['discount']; ?></td>

                                                          <td><a class="cp_Wish_remove" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>"><i class="fa fa-trash-o"></i></a></td>

                                                      </tr>
                                                      <?php
                                                                  $i++;
                                                                  } ?>


                        </tbody>

                    </table>
                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

<!-- Shopping Cart End -->

<!-- Footer Part Start -->
<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/footer'); ?>
