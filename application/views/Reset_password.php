<?php  $this->load->view('Template/header');?> 

<header class="header bg-gray header-page-top">
  <div class="header-page-top-inner">
	  <div class="container">
		<h1 class="display-4">Term of Service</h1> 
  </div>
	</div>
</header>


	<section class="section py-8">
			<div class="container">
			  <div class="row gap-y align-items-center">

				<div class="col-md-12">

<div class="panel-body">
<?php
if($this->session->flashdata('success'))
{
    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
}
if($this->session->flashdata('error'))
{
    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
} 
?>
<?php echo form_open(base_url( 'Home/Reset' ), array( 'id' => 'forgetPasswordForm', 'class' => 'form-horizontal' ));?>
<div class="form-group ">
<div class="col-xs-12">
<input class="form-control required_validation_for_forget_password number_validation" name="forgetPin" maxlength="5" type="text" placeholder="Pin">
</div>
</div>
<div class="form-group">
<div class="col-xs-12">
<input class="form-control required_validation_for_forget_password name_validation orgPass" name="password" type="password" placeholder="Password">
</div>
</div>
<div class="form-group">
<div class="col-xs-12">
<input class="form-control required_validation_for_forget_password name_validation conPass" name="confirmPassword" type="password" placeholder="Confirm Password">
</div>
</div>
<div class="form-group text-center m-t-40">
<div class="col-xs-12">
<input type="submit" class="btn btn-pink btn-block text-uppercase resetButton" name="resetPasswordData" value="Reset"/>
</div>
</div>
<?php echo form_close();?>
</div>

				</div>




			  </div>
			</div>
		  </section>

  	

<?php  $this->load->view('Template/footer');?>

        <script type="text/javascript">
    /*==============Form Validation===========================*/
        $(document).ready(function(){

        $(document).on('submit','#forgetPasswordForm',function(c){
                
             //  c.preventDefault();
                 var rep_image_val='';
                 $(".required_validation_for_forget_password").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                        
                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
                
                $('.required_validation_for_forget_password').on('keyup',function()
                                {
                                    
                                        $(this).css("border-color","#ccc");
                                });
                
                





               var current_value = $('.orgPass').val();
                var originalValue = $('.conPass').val();


                if(current_value != originalValue)
                {
                $('.conPass').css('border-bottom','1px solid red');
                        $('.conPass').val('');
                       // $('.confirm_password').attr('placeholder','Set password and Confirm password must match');
                        rep_image_val = 'error form';
                }
                else
                {
                        $(this).css('border-bottom','1px solid #d3d3d3');
                        $('.conPass').attr('placeholder','Confirm Password');
                }

                
                if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                else
                {
                }

        });
        });
      </script>