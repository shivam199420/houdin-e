<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tax extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
    }
    
       public function Taxlanding()
    {
        $this->load->view('taxlandingpage');
    }
    
    
    public function Gstinfo()
    {
        $this->load->view('Gstinfo');
    }
    public function Taxitem()
    {
        $this->load->view('Taxitem');
    }
}
