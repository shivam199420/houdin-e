<?php $this->load->view('Template/header') ?>
    <?php $this->load->view('Template/sidebar') ?>
    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">
    <!--START Page-Title -->
    <div class="row">
   
    <div class="col-md-8">
    <h4 class="page-title">Edit Purchase</h4>
    <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>">Home</a></li>
    <li><a href="<?php echo base_url(); ?>purchase">Purchase</a></li>

    <li class="active">Edit</li>
    </ol>  
    </div>
    
    </div>
    <div class="row setErrorMessage" style="display:none">
    
    </div>
    <!--END Page-Title --> 
    <div class="row">
    <div class="col-sm-12">
    <?php 
    if($this->session->flashdata('success'))
    {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    else if($this->session->flashdata('error'))
    {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
    }
    ?>
    </div>
    </div>
    <div class="row m-t-20">
    <div class="col-lg-12">
    <div class="card-box">
    <div class="form-group">
    <label for="userName">Supplier Name</label>
    <input type="text" class="form-control" readonly="readonly" value="<?php echo $basicList[0]->houdinv_supplier_contact_person_name ?>"/>
    </div>
    </div>
    </div>
    </div>
    <?php echo form_open(base_url( 'Purchase/edit/'.$this->uri->segment('3').'' ), array( 'id' => 'editPurchaseDetails', 'method'=>'post' ));?>
    <div class="row">
    <div class="col-md-12">
    <div class="card-box table-responsive">
    <table class="table table-striped table-bordered table_shop_custom">
    <thead>
    <tr>
    <input type="hidden" class="supplierIdData" value="<?php echo $basicList[0]->houdinv_inventory_purchase_supplier_id ?>" name="supplierIdData"/>
    <!-- <input type="hidden" class="setSupplierMultiBtn" name="setProductPurchase"/> -->
    <th>Product name</th>
    <th>Quantity</th>
    </tr>
    </thead>
    <tbody class="setTableData">
     <?php 
     
     
     foreach($productList as $productListData)
     {
    ?>
    <tr class="parentRowClass">
    
    <td><?php
    if($productListData->variant_title)
    {
    echo $productListData->variant_title; 
     }
     else
     {
         echo $productListData->houdin_products_title;
       
     } ?></td>
    <td>
    <input type="text" class="form-control setProductQuantity name_validation number_validation required_validation_for_prchase_customer" value="<?php echo $productListData->houdinv_purchase_products_quantity ?>" name="productQuantity[]"/>
    <input type="hidden" class="setSelectedProductId" value="<?php echo $productListData->houdinv_purchase_products_product_id ?>" name="purchaseProductId[]"/></td> </tr>
     <?php }  
     ?>
    </tbody>
    </table>
    <div class="form-group">
    <label>Credit Period</label>
    <select class="form-control required_validation_for_prchase_customer" name="creditPeriod">
    <option value="">Choose Credit Period</option>
    <option <?php if($basicList[0]->houdinv_inventory_purchase_credit == 'one day') { ?> selected="selected" <?php } ?> value="one day">One Day</option>
    <option <?php if($basicList[0]->houdinv_inventory_purchase_credit == 'one week') { ?> selected="selected" <?php } ?> value="one week">One Week</option>
    <option <?php if($basicList[0]->houdinv_inventory_purchase_credit == 'one month') { ?> selected="selected" <?php } ?> value="one month">One Month</option>
    </select>
    </div>
    <div class="form-group">
    <label>Expected Delivery Date</label>
    <input type="text" value="<?php echo date('Y-m-d',$basicList[0]->houdinv_inventory_purchase_delivery) ?>" class="form-control required_validation_for_prchase_customer date_picker name_validation" name="deliveryDate"/>
    </div>
    <div class="pull-right">
    <input type="submit" class="btn btn-success" name="editPurchaseOrder" value="Update Purchase Order"/>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <?php echo form_close(); ?>
    <?php $this->load->view('Template/footer') ?>
    <script>
    $(document).ready(function(){
    $(".select2").select2();
	$(document).on('submit','#editPurchaseDetails',function(){
      var setData = 0;
      var checkData = 0;
			var check_required_field='';
			$(".required_validation_for_prchase_customer").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
      else 
      {
          return true;
      }
		});
    });
    </script>
    
