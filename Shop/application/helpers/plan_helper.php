<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function getVendorplan($getShopName)
{
    $CI =& get_instance();
    $masterDB = $CI->load->database('master',true);

    $getPackageInfoData = $masterDB->select('houdin_user_shops.houdin_user_shop_user_id,houdin_users.houdin_users_package_id,houdin_packages.*')
    ->from('houdin_user_shops')->where('houdin_user_shops.domain_name',$getShopName)
    ->join('houdin_users','houdin_users.houdin_user_id = houdin_user_shops.houdin_user_shop_user_id')
    ->join('houdin_packages','houdin_packages.houdin_package_id = houdin_users.houdin_users_package_id')->get()->result();
    return $getPackageInfoData;
}