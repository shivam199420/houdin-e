<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopsettingmodel extends CI_Model{
function __construct() 
{
}
public function fetchShopData($data)
{
    $getWebDB = switchDynamicDatabase();
    $getDynamicWebDb = $this->load->database($getWebDB,true);
    // $getShopDb = $this->load->database($config_app,TRUE);
    $masterDB = $this->load->database('master', TRUE);
    $getShopThemeData = $masterDB->select('houdin_user_shops.houdin_user_shops_category,houdin_templates.houdin_template_file')->from('houdin_user_shops')->where('houdin_user_shops.houdin_user_shop_shop_name',$data)
    ->join('houdin_templates','houdin_templates.houdin_template_category = houdin_user_shops.houdin_user_shops_category','left outer')
    ->get()->result();
    // get shop testimonial
    $getTestimonialData = $getDynamicWebDb->select('*')->from('houdinv_testimonials')->where('testimonials_publish',1)->get()->result();
    // get shop category data
    $getCategoryData = $getDynamicWebDb->select('*')->from('houdinv_categories')->where('houdinv_category_status','active')->order_by('houdinv_category_id','DESC')->limit('4')->get()->result();
    // fetch custom home data
    $getCustomHomeData = $getDynamicWebDb->select('*')->from('houdinv_custom_home_data')->get()->result();
    // fetch slider data
    $getSliderImageData = $getDynamicWebDb->select('*')->from('houdinv_custom_slider')->get()->result();
    // fetch latest product
    $getLateststProducts = $getDynamicWebDb->select('*')->from('houdinv_products')->where('houdin_products_status','1')->limit('8')->order_by('houdin_products_id','DESC')->get()->result();
    // fetch featured product
    $getFeaturedProduct = $getDynamicWebDb->select('*')->from('houdinv_products')->where('houdin_products_status','1')->where('houdinv_products_main_featured','1')->limit('8')->order_by('houdin_products_id','DESC')->get()->result();
    return array('shopData'=>$getShopThemeData,'storeTestimonial'=>$getTestimonialData,'categoryData'=>$getCategoryData,'customHome'=>$getCustomHomeData,'sliderListData'=>$getSliderImageData,'latestProducts'=>$getLateststProducts,'featuredProduct'=>$getFeaturedProduct);
}
public function updateVisitorCount()
{
    $getWebDB = switchDynamicDatabase();
    $getDynamicWebDb = $this->load->database($getWebDB,true);

    $setData = strtotime(date('Y-m-d'));
    $getCount = $getDynamicWebDb->select('*')->from('houdinv_visitors')->where('visitors_date',$setData)->get()->result();
    if(count($getCount) > 0)
    {
        $getCountData = $getCount[0]->counts+1;
        $getDynamicWebDb->where('visitors_id',$getCount[0]->visitors_id);
        $getDynamicWebDb->update('houdinv_visitors',array('counts'=>$getCountData));
    }
    else
    {
        $setInsertArray = array('counts'=>'1','visitors_date'=>$setData);
        $getDynamicWebDb->insert('houdinv_visitors',$setInsertArray);
    }
}
} 
?>
