<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName.'/Template/header'); ?>
<!-- About Banner Start -->

<section id="about">

  <div class="container">

    <div class="row">

      <div class="about-heading text-center">

        <h2>Account</h2>

      </div>

    </div>

  </div>

</section>

<!-- About Banner End -->



<!-- my account part Start -->

<section id="account">

  <div class="container">

    <div class="row">

      <?php
          if($this->session->flashdata('error'))
          {
              echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
          }
          if($this->session->flashdata('success'))
          {
              echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
          }
          ?>

      <div class="col-md-6">

        <div class="login-account">

          <h3>login your account</h3>

          <?php echo form_open(base_url( 'Register/checkUserAuth' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>

          <div class="form-group">

            <input type="email" class="form-control required_validation_for_user name_validation email_validation" id="name" name="userEmail" placeholder="Enter Email Address" required>

          </div>

          <div class="form-group">

            <input type="password" name="userPass" class=" form-control required_validation_for_user name_validation" placeholder="Enter Password"/></div>

          </div>

          <div class="tahsan3">

            <div class="checkbox chek2 checkbox-success check33">

              <a href="javascript:;" title="Recover your forgotten password" rel="" data-toggle="modal" data-target="#myModal">Forgot your password?</a>

            </div>

          </div>



          <div class="procd">

            <button type="submit" class="btn btn-primary">Login</button>

          </div>

          <?php echo form_close(); ?>

        </div>



        <div class="col-md-6">

          <div class="register">

            <h3>Register Now <a href="#">(If don’t have any account)</a></h3>

            <?php echo form_open(base_url( 'Register/addShopUsers' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>

            <div class="row">

              <div class="col-md-6 login-account">

                <div class="form-group">

                  <input type="text" class="form-control required_validation_for_user name_validation" name="userFirstName" placeholder="Enter Firstname" />

                </div>

              </div>
              <div class="col-md-6 login-account">

                <div class="form-group">

                  <input type="text" class="form-control required_validation_for_user name_validation" name="userLastName" placeholder="Enter Lastname"/>

                </div>

              </div>


              <div class="col-md-12 login-account">

                <div class="form-group">

                  <input type="email" class="form-control required_validation_for_user name_validation email_validation" name="userEmail" placeholder="Enter Emailaddress"/>

                </div>

              </div>

            </div>

            <div class="row">

              <div class="col-md-6 login-account">

                <div class="form-group">

                  <input type="password" class="form-control required_validation_for_user name_validation opass" name="userPass" placeholder="Enter password"/>

                </div>

              </div>

              <div class="col-md-6 login-account">

                <div class="form-group">

                  <input type="password" class="form-control required_validation_for_user name_validation cpass" name="userRetypePass" placeholder="Conform password" />

                </div>

              </div>
              <div class="col-md-12 login-account">

                <div class="form-group">

                  <input type="text" class="form-control required_validation_for_user name_validation" name="userContact" placeholder="Enter Contact Number"/>

                </div>

              </div>

            </div>


            <div class="register-now">

              <div class="procd">

                <input type="submit" class="btn btn-primary acc_btn setDisableData" type="submit" value="Create an account"/>

              </div>

            </div>

          </form>

        </div>

      </div>

    </div>

  </div>

</div>

</section>

<!-- modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <?php echo form_open(base_url( 'Register/checkUserForgot' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Forgot your password</h4>
        <button type="button" class="close" data-dismiss="modal" style="float: right;">×</button>
      </div>
      <div class="modal-body">
        <input placeholder="Please enter your Email address" name="emailForgot" class="form-control required_validation_for_user name_validation email_validation" type="text">
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-default acc_btn" value="Submit"/>
      </div>
    </div>
    <?php echo form_close(); ?>
  </div>
</div>
<!-- end modal -->


<!-- my account part End -->

<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName.'/Template/footer'); ?>
<!-- CLient side form validation -->
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('submit','#addShopUsers',function(){
    var check_required_field='';
    $(this).find(".required_validation_for_user").each(function(){
      var val22 = $(this).val();
      if (!val22){
        check_required_field =$(this).size();
        $(this).css("border-color","#ccc");
        $(this).css("border-color","red");
      }
      $(this).on('keypress change',function(){
        $(this).css("border-color","#ccc");
      });
    });
    if(check_required_field)
    {
      return false;
    }
    else {
      return true;
    }
  });
});
</script>
