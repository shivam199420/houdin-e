<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Inventory extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
        $this->load->library('Pdf');
        $this->perPage = 100;
        $this->load->model('Inventorymodel');
         $this->load->model('Campaignmodel'); 
        $this->auth_users->is_logged_in();
        $this->load->helper('url');
	}
    
    
     public function Inventorylanding()
    { 	
        $this->load->view('inventrylandingpage');
    }
    public function addinventory()
    { 	
        $getlowinventory_total = $this->Inventorymodel->lowinventory_total();
        $this->load->view('addinventory',$getlowinventory_total);
    }
    public function addinventory_api()
    {
        $getPurchaseData = $this->Inventorymodel->fetchaddinventory_api();
        foreach ($getPurchaseData as $key => $value) {
            $setDeliverydate = date('d-m-Y',$value->houdinv_inventory_purchase_delivery);
            $setSupplierDetail = $value->houdinv_supplier_comapany_name."( ".$value->houdinv_supplier_email.", ".$value->houdinv_supplier_contact.")";
            $setStatus = "<button class='btn btn-xs btn-danger text-uppercase'>".$value->houdinv_inventory_purchase_status."</button>";
            $setAction = "<a href='".base_url()."Purchase/inward/".$value->houdinv_inventory_purchase_id."' class='btn btn-primary'>Inward</a>";
            $arrya_json[] = array('#'.$value->houdinv_inventory_purchase_id.'',$setDeliverydate,$setSupplierDetail,$setStatus,$setAction);
    } 
     echo json_encode(array('data'=>$arrya_json));
    }
    public function manageinventory_api()
    {
        $getManageinventory = $this->Inventorymodel->fetchManageInventory(); 
        foreach ($getManageinventory as $key => $value) {
        $houdin_products_id = $value->houdin_products_id; 
            // set title             
                $setTitle = $value->houdin_products_title; 
            // set product image 
                $getJsonData = json_decode($value->houdinv_products_main_images,true);
                $setImage = base_url()."upload/productImage/".$getJsonData[0];
             
            // set stock 
                $setStock = $value->houdinv_products_total_stocks;
                         
         $getVarintInvontry = $this->Inventorymodel->fetchVarintInventory($houdin_products_id); 
         
         $prefix = $Stockssetdata = '';
  foreach($getVarintInvontry as $Stocksset){
      
      $Stockssetdata .= $prefix . '' . $Stocksset->houdinv_products_variants_total_stocks. '';
      $prefix = ', ';
  }

            $arrya_json[] = array('<img src="'.$setImage.'"/>',$setTitle,$Stockssetdata,'<button class="btn btn-success waves-effect waves-light stockcheckBtn" datap-name="'.$setTitle.'" datap-id="'.$houdin_products_id.'">variants</button>');
    } 
     echo json_encode(array('data'=>$arrya_json));
    }

    public function manageinventory_api_varints()
    {  
          


              $getManageinventory = $this->Inventorymodel->fetchVarintInventory($this->input->get('pid')); 
        foreach ($getManageinventory as $key => $value) {
            // set title
            
                $setTitle = $value->houdin_products_variants_title;
             
            // set product image
            
                $setImage = base_url()."upload/productImage/".$value->houdin_products_variants_image;
            
            // set stock
             
                $setStock = $value->houdinv_products_variants_total_stocks;
             
            
            $arrya_json[] = array('<img src="'.$setImage.'"/>',$setTitle,$setStock);
    } 
     echo json_encode(array('data'=>$arrya_json));

    }

public function deleteinvontry(){

    if($this->input->post('deletePurchaseId')){

    $getDeleteStatus = $this->Inventorymodel->deletePurchaseData($this->input->post('deletePurchaseId'));
    if($getDeleteStatus)
    {
      $this->session->set_flashdata('success','Inventory  order deleted successfully');
     
      redirect(base_url()."inventory/addinventory");
    }
    else
    {
      $this->session->set_flashdata('error','Something went wrong. Please try again');
     
      redirect(base_url()."inventory/addinventory");
    }
  }
}
    public function lowinventory()
    {
    	$getlowinventory_total = $this->Inventorymodel->lowinventory_total();
        $this->load->view('lowinventory',$getlowinventory_total);
    }

    public function lowinventory_api()
    {
        $getInsertStatus = $this->Inventorymodel->lowinventorys_api();
        foreach ($getInsertStatus as $key => $value) { 
            if($value['product']->houdinv_products_total_stocks)
            {
                $setTotalStock = $value['product']->houdinv_products_total_stocks;
            }
            else if($value['product']->houdinv_products_variants_total_stocks)
            {
                $setTotalStock = $value['product']->houdinv_products_variants_total_stocks;
            }
            else
            {
                $setTotalStock = 0;
            }
            // set image
            if($value['product']->houdinv_products_main_images)
            {
                $image=$value['product']->houdinv_products_main_images;
                $json_image= json_decode($image);
                $image_url= base_url().'upload/productImage/'.$json_image[0];
            }
            else
            {
                $image_url= base_url().'upload/productImage/'.$value->houdin_products_variants_image;
            }
            // set product title
            if($value['product']->houdin_products_title)
            {
                $setTitle = $value['product']->houdin_products_title;
            }
            else
            {
                $setTitle = $value['product']->houdin_products_variants_title;
            }
            $arrya_json[] = array('<img src="'.$image_url.'">',$setTitle,$setTotalStock );
        } 
        echo json_encode(array('data'=>$arrya_json));
    }
    public function reorderproduct()
    {
    	$getreorder['reorder'] = $this->Inventorymodel->lowinventorys_api();
    	//$getreorder['suppliers']=$this->Inventorymodel->suppliersGet();
    	//print_r($getreorder);
        $this->load->view('reorderproduct',$getreorder);
    }
public function reorderproduct_send($value='')
{

  //print_r($this->input->post());
        $this->form_validation->set_rules('creditPeriod','Credit Period','required|trim');
        $this->form_validation->set_rules('deliveryDate','Delivery Date','required|trim');       

     if($this->form_validation->run() == true)
    {
    $All_reorder_GET= $this->input->post();
    
    // print_r($All_reorder_GET['supplierss']);
    $setSupplierId = implode(',',$All_reorder_GET['supplierss']);
    $setMainProduct = $All_reorder_GET['single__reorderproduct'];
    // print_r($setMainProduct);
   $supplierss= $All_reorder_GET['supplierss'];
   $p_setarrya=array();
foreach($setMainProduct as $keys=>$getproduct){

$supplers_id['product']=$getproduct;
$supplers_id['suppl']=$supplierss[$keys];
array_push($p_setarrya,$supplers_id);
}

//print_r($p_setarrya);
$setMainArrayData = array();
for($index = 0; $index < count($p_setarrya); $index++)
{
    $childArray = array();
    if(count($setMainArrayData) > 0)
    {
        $set = 0;
        for($count = 0; $count < count($setMainArrayData); $count++)
        {
          //  echo $setMainArrayData[$count]['supplier']."::::".$p_setarrya[$index]['suppl']."<br/>";
            if($setMainArrayData[$count]['supplier'] == $p_setarrya[$index]['suppl'])
            {
                 
                array_push($setMainArrayData[$count]['product'],$p_setarrya[$index]['product']);
                $set++;
            }
            else
            {
                // echo "inelse<br/>";
                // $childArray['supplier'] = $p_setarrya[$index]['suppl'];
                // $childArray['product'][] = $p_setarrya[$index]['product'];
                // array_push($setMainArrayData,$childArray);
            }
        }
        if($set == 0)
        {
            $childArray['supplier'] = $p_setarrya[$index]['suppl'];
            $childArray['product'][] = $p_setarrya[$index]['product'];
            array_push($setMainArrayData,$childArray);
        }
    }
    else
    {
        $childArray['supplier'] = $p_setarrya[$index]['suppl'];
        $childArray['product'][] = $p_setarrya[$index]['product'];
        array_push($setMainArrayData,$childArray);
    }
  
}
	
	$setQuantity = $All_reorder_GET['quantity'];
	$setCreditPeriod = $All_reorder_GET['creditPeriod'];
	$setDeliveryDate = $All_reorder_GET['deliveryDate'];
	// $setSupplierData = array('supplierId'=>$setSupplierId,'product'=>$setMainProduct,'quantity'=>$setQuantity,'creditPeriod'=>$setCreditPeriod,'deliveryDate'=>$setDeliveryDate);
	 $setSupplierData = array( 'productall'=>$setMainArrayData,'quantity'=>$setQuantity,'creditPeriod'=>$setCreditPeriod,'deliveryDate'=>$setDeliveryDate);
      
    $this->Inventorymodel->addPurchaseOrderData($setSupplierData);

	$this->session->set_flashdata('success','Reorder product addedd suucessfully');   
   redirect(base_url()."inventory/reorderproduct");
    }else{
     $this->session->set_flashdata('error',validation_errors());

       redirect(base_url()."inventory/reorderproduct");
    }	


}



    // public function addinventorypurchase()
    // {
    //     $this->load->view('addinventorypurchasedata');
    // }
    public function deductinventory()
    {
        $getDeductInventoryData = $this->Inventorymodel->fetchDeductInventoryData();
        $this->load->view('deductinventory',$getDeductInventoryData);
    }
    public function manageinventory()
    {
        $this->load->view('manageinventory');
    }
    public function stocktransafer()
    {
        // Custom Pagination
        $getTotalCount = $this->Inventorymodel->getproductVariantTotalCount();
        $config['base_url']    = base_url().'inventory/stocktransafer/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getProductList = $this->Inventorymodel->fetchProductsDataStockTransfer($setArray);
        $this->load->view('stocktransafer',$getProductList);
    }
    public function fetchStockTransferProduct()
    {
        $this->form_validation->set_rules('productId','text','required');
        $this->form_validation->set_rules('getWarehouseId','text','required');
        if($this->form_validation->run() == true)
        {
            $setStockArray = array('productId'=>$this->input->post('productId'),'warehouseId'=>$this->input->post('getWarehouseId'));
            $getData = $this->Inventorymodel->fetchProductListData($setStockArray);
            if($getData == 'no')
            {
                echo 'no';
            }
            else
            {
                echo $getData;
            }
        }
        else
        {
            echo 'all';
        }
    }


public function fetchStockTransferProducts()
    {
        $result=array();
        if($this->input->get('warehouse_id')){
          $result=$this->Inventorymodel->fetchProductsListData($this->input->get('warehouse_id'));
        }
        echo json_encode($result);
    }


    public function sendStockTransfersData()
    {
        $this->load->helper('url');
        // print_r($this->input->post());
        $this->form_validation->set_rules('warehouseFrom','text','required');
        $this->form_validation->set_rules('warehouseTo','text','required');         
        if($this->form_validation->run() == true)
        {
            $getWarehouseFrom = $this->input->post('warehouseFrom');
            $getWarehouseTo = $this->input->post('warehouseTo');
            if($getWarehouseFrom != $getWarehouseTo)
            {
                $setStockArray = array('Alldata'=>$this->input->post(),'warehouseFrom'=>$getWarehouseFrom,'warehouseTo'=>$getWarehouseTo,'productId'=>$this->input->post('Allproduct_idandvarints'),'quantity'=>$this->input->post('quantity'));
                $getStockStatus = $this->Inventorymodel->sendStocksData($setStockArray);
                if($getStockStatus['message'] == 'success')
                {
                    $this->session->set_flashdata('success','Stock successfully transfered');   
                    //redirect(base_url()."inventory/stocktransafer");
                    echo '<script>window.location.href="'.base_url().'inventory/stocktransafer"</script>';
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');   
                    echo '<script>window.location.href="'.base_url().'inventory/stocktransafer"</script>';
                }
            }
            else
            {
                $this->session->set_flashdata('error','Please choose different warehouse to send stock');   
                echo '<script>window.location.href="'.base_url().'inventory/stocktransafer"</script>';
            }
        }
        else
        {
            $this->session->set_flashdata('error','All fields are mandatory');   
            echo '<script>window.location.href="'.base_url().'inventory/stocktransafer"</script>';
        }
    }


    public function sendStockTransferData()
    {
        $this->load->helper('url');
        // print_r($this->input->post());
        $this->form_validation->set_rules('warehouseFrom','text','required');
        $this->form_validation->set_rules('warehouseTo','text','required');         
        if($this->form_validation->run() == true)
        {
            $getWarehouseFrom = $this->input->post('warehouseFrom');
            $getWarehouseTo = $this->input->post('warehouseTo');
            if($getWarehouseFrom != $getWarehouseTo)
            {
                $setStockArray = array('warehouseFrom'=>$getWarehouseFrom,'warehouseTo'=>$getWarehouseTo,'productId'=>$this->input->post('productId'),'variantId'=>$this->input->post('variantId'),'quantity'=>$this->input->post('quantity'));
                $getStockStatus = $this->Inventorymodel->sendStockData($setStockArray);
                if($getStockStatus['message'] == 'success')
                {
                    $this->session->set_flashdata('success','Stock successfully transfered');   
                    //redirect(base_url()."inventory/stocktransafer");
                    echo '<script>window.location.href="'.base_url().'inventory/stocktransafer"</script>';
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');   
                    echo '<script>window.location.href="'.base_url().'inventory/stocktransafer"</script>';
                }
            }
            else
            {
                $this->session->set_flashdata('error','Please choose different warehouse to send stock');   
                echo '<script>window.location.href="'.base_url().'inventory/stocktransafer"</script>';
            }
        }
        else
        {
            $this->session->set_flashdata('error','All fields are mandatory');   
            echo '<script>window.location.href="'.base_url().'inventory/stocktransafer"</script>';
        }
    }
    public function viewinventory()
    {
        $this->load->view('viewinventory');
    }
    public function generatepdflowinventory()
    {
        $getLogo = fetchvedorsitelogo();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Low Inventory Detail');
    $pdf->SetSubject('Houdin-e - Low Inventory Detail');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$url=base_url();
   $getlowinventory_total = $this->Inventorymodel->lowinventory_total();
   

 

$totalproduct =$getlowinventory_total['totalproduct'];
$outofstock =$getlowinventory_total['lowstock'];
$lowstock =$getlowinventory_total['lowstock'];
$table_tbody='';
$getInsertStatus = $this->Inventorymodel->lowinventorys_api();
foreach ($getInsertStatus as $key => $value) { 

$image=$value->houdinv_products_main_images;
 
 $json_image= json_decode($image,true); 
 $image_url= base_url().'upload/productImage/'.$json_image[0];
 if($value->houdinv_products_total_stocks)
            {
                $setTotalStock = $value->houdinv_products_total_stocks;
            }
            else if($value->houdinv_products_variants_total_stocks)
            {
                $setTotalStock = $value->houdinv_products_variants_total_stocks;
            }
            else
            {
                $setTotalStock = 0;
            }
 $arrya_json[] = array('<img style="width:10px;     height: 40px;"  src="'.$image_url.'">',$value->houdin_products_title,$setTotalStock );


       $table_tbody.='<tr>
 <td style="font-size:12px;font-weight:700;"><img  src="'.$image_url.'"></td>
 <td style="font-size:12px;font-weight:700;">'.$value->houdin_products_title.'</td>
 <td style="font-size:12px;font-weight:700;">'.$setTotalStock.'</td>

 </tr>';               


}

$html = <<<EOD

 <table style="width:100%;">
<tbody>
<tr>

<td>
<img style="display:block; margin:auto; width:150px;" src="$getLogo" alt="Logo"></td>



<td style="font-size:15px;text-align:right"><h3>Low Inventory</h3></td>

</tr>
</tbody>
</table>

<table>
<tr><td>&nbsp;</td></tr>
</table>

<table style="width:100%;" cellpadding="8" border="1">
<tr>
 <td style="text-align:center;">
 <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
 <span style="font-size:12px;font-weight:bold;">$totalproduct </span><br/>
 <span style="font-size:12px;font-weight:bold; color:#008cb6">Total product </span>

  </td>
  <td style="text-align:center;">
  <img src=$url"/assets/images/Total_Products.png" width="35px;"><br/>
  <span style="font-size:12px;font-weight:bold;">$outofstock</span><br/>
  <span style="font-size:12px; font-weight:bold; color:#008cb6">Total out of stock product </span>

   </td>
   <td style="text-align:center;">
   <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
   <span style="font-size:12px;font-weight:bold;">$lowstock</span><br/>
   <span style="font-size:12px; font-weight:bold; color:#008cb6">Total low stock product</span>

    </td>
    </tr>

  </table>


   <table>
   <tr><td>&nbsp;</td></tr>
   </table>

   <table style="width:100%;" cellpadding="10" border="1">
<tr>
<td style="font-size:12px;font-weight:bold;">Item Image</td>
<td style="font-size:12px;font-weight:bold;">Item name</td>
<td style="font-size:12px;font-weight:bold;">Stock</td>
 

</tr>
 
$table_tbody
</table>

   <table>
   <tr><td>&nbsp;</td></tr>
   </table> 
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_low_inventoery.pdf', 'I');
    }



public function generatepdfaddinventory()
    {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Inventory Detail');
    $pdf->SetSubject('Houdin-e - Inventory Detail');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$url=base_url();
   $getlowinventory_total = $this->Inventorymodel->lowinventory_total();
    

 

$totalproduct =$getlowinventory_total['totalproduct'];
$outofstock =$getlowinventory_total['lowstock'];
$lowstock =$getlowinventory_total['lowstock'];
$table_tbody='';
$getPurchaseData = $this->Inventorymodel->fetchaddinventory_api();
  //  print_r($getInsertStatus);
  $getLogo = fetchvedorsitelogo();
foreach ($getPurchaseData as $key => $value) { 
        $setDeliverydate = date('d-m-Y',$value->houdinv_inventory_purchase_delivery);
        $setSupplierDetail = $value->houdinv_supplier_comapany_name."( ".$value->houdinv_supplier_email.", ".$value->houdinv_supplier_contact.")";
        $setStatus = "<button class='btn btn-xs btn-danger text-uppercase'>".$value->houdinv_inventory_purchase_status."</button>";
       $table_tbody.='<tr>
 <td style="font-size:12px;font-weight:700;">#'.$value->houdinv_inventory_purchase_id.'</td>
 <td style="font-size:12px;font-weight:700;">'.$setDeliverydate.'</td>
 <td style="font-size:12px;font-weight:700;">'.$setSupplierDetail.'</td>
 <td style="font-size:12px;font-weight:700; color:red">'.$setStatus.'</td>

 </tr>';               


}

$html = <<<EOD

 <table style="width:100%;">
<tbody>
<tr>

<td>
<img style="display:block; margin:auto; width:150px;" src="$getLogo" alt="Logo"></td>



<td style="font-size:15px;text-align:right"><h3>Add Inventory</h3></td>

</tr>
</tbody>
</table>

<table>
<tr><td>&nbsp;</td></tr>
</table>
<table style="width:100%;" cellpadding="8" border="1">
<tr>
 <td style="text-align:center;">
 <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
 <span style="font-size:12px;font-weight:bold;">$totalproduct </span><br/>
 <span style="font-size:12px;font-weight:bold; color:#008cb6">Total product </span>

  </td>
  <td style="text-align:center;">
  <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
  <span style="font-size:12px;font-weight:bold;">$outofstock</span><br/>
  <span style="font-size:12px; font-weight:bold; color:#008cb6">Total out of stock product </span>

   </td>
   <td style="text-align:center;">
   <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
   <span style="font-size:12px;font-weight:bold;">$lowstock</span><br/>
   <span style="font-size:12px; font-weight:bold; color:#008cb6">Total low stock product</span>

    </td>
    </tr>

  </table>


   <table>
   <tr><td>&nbsp;</td></tr>
   </table>

   <table style="width:100%;" cellpadding="10" border="1">
   <tr>
   <td style="font-size:12px;font-weight:bold;">Order Detail</td>
   <td style="font-size:12px;font-weight:bold;">Delivery Date</td>
   <td style="font-size:12px;font-weight:bold;">Supplier</td>
   <td style="font-size:12px;font-weight:bold;">Status</td>
    
   
   </tr>
    
   $table_tbody
   </table>
   
      <table>
      <tr><td>&nbsp;</td></tr>
      </table> 

   <table>
   <tr><td>&nbsp;</td></tr>
   </table>









EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_inventory_list.pdf', 'D');
    }
}

