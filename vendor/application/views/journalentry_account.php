<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); ?>
<?php 
$getCurrency = getVendorCurrency();
if($getCurrency[0]->houdin_users_currency=="USD")
{
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="AUD"){
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="Euro"){
	$currencysymbol= "£";
}else if($getCurrency[0]->houdin_users_currency=="Pound"){
	$currencysymbol= "€";
}else if($getCurrency[0]->houdin_users_currency=="INR"){
	$currencysymbol= "₹";
}
?>
<style type="text/css">
.zoom img{
    
    transition-duration: 5s;
    margin: 0 auto;
}
img {
    vertical-align: middle;
    height: 40px;
    width: auto; 
} 

.zoom { 
    transition: all 1s;
    -ms-transition: all 1s;
    -webkit-transition: all 1s;
    margin-top: 0px;
    padding-top: 0px;
    
}
.zoom:hover {
    -ms-transform: scale(2); /* IE 9 */
    -webkit-transform: scale(2); /* Safari 3-8 */
    transform: scale(2); 
    margin-left: 40px;
}
.m-b-10{
    margin-bottom: 10px;
}
.table-bordered > tbody > tr > td {
    vertical-align: text-top !important;
}
.remove-tr
{
    display:none;
}
.select2.select2-container
{
    margin-bottom:4px;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" /> 
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
<!--START Page-Title -->
            <div class="row">
            <?php 
            
            $lastEntry = getMainAccount($this->uri->segment('3'));
            ?>
                <div class="col-sm-12">
                <div class="col-md-8">
                   <h4 class="page-title">Account / <?php echo $accountname; ?></h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>accounts/accounting">Accounting </a></li>
                  <li class="active">Journal Entry Account</li>
                  </ol>
                  </div>
                  <div class="col-md-4">
                  <!-- <select class="form-control navbar-left app-search pull-left custom_search_all setNewEntry">
                  <option value="">Add Entry</option>
               
                  <option value="Transfer">Transfer</option>
                  <option value="journal Entry">journal Entry</option>
                  </select> -->
                  
                      </div>
                  
                </div>
                <?php 
           
                ?>
            </div>
           <!--END Page-Title --> 
           

                                    
        <div class="row">
          <div class="col-md-12">
          <?php 
          if($this->session->flashdata('success'))
          {
              echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
          }
          if($this->session->flashdata('error'))
          {
              echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
          }
          ?>
             
          <div class="clearfix"></div>
            <div class="card-box table-responsive">
              
  <?php echo form_open(base_url('Accounts/journalentrysave'), array( 'id' => 'balancetransfersave', 'method'=>'POST' ));?>

  <div class="row m-t-20">
<div class="col-sm-12">
<div class="card-box">


<div class="row">
 
 <div class="col-md-12 form-horizontal">

<div class="form-group">
<label class="col-md-2 control-label">Transfer From</label>
<div class="col-md-4">
 <input type="text" value="<?=set_value('transferdate');?>" class="form-control date_picker required_validation_for_vandor" name="transferdate" placeholder="Transfer Date">
</div>
<label class="col-md-2 control-label"> Ref No. </label> <div class="col-md-4"><input type="text" value="<?=set_value('refnumber');?>" class="form-control required_validation_for_vandor" name="refnumber" placeholder="Ref No."> </div>  


</div>
 </div>
 
 
 
 
  <table class="table table-hover table-bordered table_shop_custom">
                <thead>
                  <tr>

                    <th style="width: 15%">Account</th>
                    <th>Debits</th>
                    <th>Credits</th>
                    <th>Discription</th>
                    <th>Name</th>
                 
                    <th>Tax</th>
                    <th>Action</th>  
                  </tr>
                </thead>
                <tbody>
  <?php    for ($x = 1; $x <= 5; $x++) { ?>
    <tr>
        <td>
            <select class="form-control select1 required_validation_for_vandor transferfrom" name="transferfromto[]">
 
                    <option value="">Select Account</option>
                    <?php 
                    foreach($parentAccount as $parentAccountList)
                    {
                    ?>
                  <option  value="<?php echo $parentAccountList->houdinv_accounts_id ?>"><?php echo $parentAccountList->houdinv_accounts_name; ?></option>
                    <?php
                    }?>
                    
</select></td>
        <td> <input type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="<?=set_value('transferDebits');?>" class="form-control transferDebits" name="transferDebits[]" placeholder="Transfer Debits"></td>
        <td><input type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="<?=set_value('transferCredits');?>" class="form-control transferCredits" name="transferCredits[]" placeholder="Transfer Credits"></td>
        <td> <textarea class="form-control required_validation_for_vandor" placeholder="Memo type here..." name="transfermemo[]"> <?=set_value('transfermemo');?></textarea></td>
        <td>
            <input type="text" value="<?=set_value('name');?>" class="form-control required_validation_for_vandor" name="name[]" placeholder="Name">
            </td>
         <td><input type="text" value="" class="form-control required_validation_for_vandor" name="tax[]" placeholder="Tax"></td>  
        <td>
         
          <a class="btn btn-default m-r-5 <?php if($x!=1){ echo "removerow";} ?>" title="Delete Account"  href="#"><i class="fa fa-trash" aria-hidden="true"></i></a> 
         
         
        
    </tr> 
     <?php } ?>
   
                </tbody>
              </table>
 
 
 
 <div class="col-md-6 ">
 
<button type="button" class="btn btn-info pull-right m-r-10 apandnewrow" >Add new Row</button>
</div>
 <div class="col-md-6 ">
     <p class="journalentrydebitcreditbalance" style="color:red;display:none;">please manage your journal entry debit credit balance</p>
<input type="submit" class="btn btn-default pull-right m-r-10" name="savejournalentry" value="save Entry">
 
</div>
</div>
</div>
</div>
</div>
<?php echo form_close(); ?>
              
           
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <table class="newrowapand" style="display:none">
     
     <tr>
        <td>
            <select class="form-control select1 required_validation_for_vandor transferfrom" name="transferfromto[]">
 
                    <option value="">Select Account</option>
                    <?php 
                    foreach($parentAccount as $parentAccountList)
                    {
                    ?>
                  <option  value="<?php echo $parentAccountList->houdinv_accounts_id ?>"><?php echo $parentAccountList->houdinv_accounts_name; ?></option>
                    <?php
                    }?>
                    
</select></td>
        <td> <input type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="<?=set_value('transferDebits');?>" class="form-control transferDebits" name="transferDebits[]" placeholder="Transfer Debits"></td>
        <td><input type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="<?=set_value('transferCredits');?>" class="form-control transferCredits" name="transferCredits[]" placeholder="Transfer Credits"></td>
        <td> <textarea class="form-control required_validation_for_vandor" placeholder="Memo type here..." name="transfermemo[]"> <?=set_value('transfermemo');?></textarea></td>
        <td>
            <input type="text" value="<?=set_value('name');?>" class="form-control required_validation_for_vandor" name="name[]" placeholder="Name">
            </td>
         <td><input type="text" value="" class="form-control required_validation_for_vandor" name="tax[]" placeholder="Tax"></td>  
        <td>
         
          <a class="btn btn-default m-r-5 removerow" title="Delete Account"  href="#"><i class="fa fa-trash" aria-hidden="true"></i></a> 
         
         
        
    </tr>
     
 </table>
  
  
  
  
<!--Delete-->

  <div id="delete_product" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
      <?php echo form_open(base_url().'Product/',array("id"=>"DeleteProduct","enctype"=>"multipart/form-data")); ?>
 
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete product</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this product ?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
               <input type="hidden" name="delete_id" id="delete_id" />
          <input type="submit" class="btn btn-info" name="DeleteEntry" value="Delete">

          </div>

          </div>
       <?php echo form_close(); ?> 
          </div>

          </div>
<!--change Status-->
<div id="deleteBalanceSheetModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <?php echo form_open(base_url( 'Accounts/deletebalancesheet/'.$this->uri->segment('3').'' ), array('method'=>'post'));?>
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Delete Purchase</h4>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-md-12">
    <input type="hidden" class="deletebalancesheetId" name="deletebalancesheetId"/>
    <h4><b>Do you really want to Delete this purchase ?</b></h4>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-info" name="deletePurchase" value="Delete">
    </div>
    </div>
    <?php echo form_close(); ?>
    </div>
    </div>



<?php $this->load->view('Template/footer.php') ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
 
<script type="text/javascript">

$(document).ready(function() {
    $(".transferfrom").change(function () {    
        var transferfrom=this.value;               
        if(transferfrom){
            $("select.transferto option[value='"+transferfrom+"']").prop('disabled',true);
        }
          $.ajax({
         type: "POST",
         url:   "<?=base_url()?>Accounts/GetblanceShowtheblancetrafer", 
         data: {textbox:transferfrom},
         success: 
              function(data){
                 
                $(".blanceshowcurrentaccount_show").html('<p style="background-color: #5392c9; width: 79px;height: 23px;padding: 2px;color: #ffff;">'+data+'</p>');
              }
          });// you have missed this bracket
        
        
        
    });



$(document).on("click",".apandnewrow",function() {
  var newHtmldata=  $('.newrowapand tbody').html();
    
     $(".table tbody tr:last").after(newHtmldata);
});

$(document).on("click",".removerow",function() {
   $(this).parents("tr").remove();
    return false;
});


    $('#balancetransfersave').submit(function(e){ 
   var mainfrm=  $(this);
var check_required_field='';
      $(this).find(".required_validation_for_vandor").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {  
        e.preventDefault(); 
        return false;
      }else{
          
          var transferDebits=0;
          var transferCredits=0;
          
    mainfrm.find(".transferDebits").each(function(){ 
        if($(this).val()){
        transferDebits= parseInt($(this).val())+parseInt(transferDebits); 
        }
     //   alert('current '+$(this).val()+'total ' + transferDebits);
    });
    //alert(transferDebits);
    
     mainfrm.find(".transferCredits").each(function(){ 
         if($(this).val()){
         transferCredits = parseInt($(this).val())+parseInt(transferCredits); 
            //alert('current '+$(this).val()+'total ' + transferCredits);
         }
     });
          
          
         // alert(transferCredits);
          
    if(transferDebits==transferCredits){      
          
      return true;
     }else{ e.preventDefault(); $('.journalentrydebitcreditbalance').show();   return false;}
     
      }  
    });  




});
 
 

</script>