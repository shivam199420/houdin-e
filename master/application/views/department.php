<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <div class="btn-group pull-right m-t-15">
                                    <a href="<?php echo base_url(); ?>Adminmanagement/adddepartment" class="btn btn-default btn-sm m-l-5" > <i class="fa fa-plus m-r-5"></i>Add Departments</a>
                                </div>
                                <h4 class="page-title">Departments</h4>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-sm-12">
                        <?php 
                        if($this->session->flashdata('success'))
                        {
                            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                        }
                        if($this->session->flashdata('error'))
                        {
                            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                        }
                        ?>
                        </div>
                        </div>  
                        <div class="row">
                          <div class="col-md-12">
                            <div class="card-box table-responsive">
                              <table class="table table-striped table-bordered table_shop_custom">
                                <thead>
                                  <tr>
                                    <th>Department</th>
                                    <th>Modules</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <?php 
                                foreach($department as $departmentData)
                                {
                                ?>
                                <tr>
                                    <td><?php echo $departmentData->houdin_admin_department_name ?></td>
                                    <td><?php echo $departmentData->houdin_admin_department_access ?></td>
                                    <td>
                                    <?php 
                                    if($departmentData->houdin_admin_department_status == 'active')
                                    {
                                        $setText = 'Active';
                                        $setClass = 'success';
                                    }
                                    else
                                    {
                                        $setText = 'Deactive';
                                        $setClass = 'warning';
                                    }
                                    ?>
                                    <button type="button" class="btn btn-<?php echo $setClass ?> btn btn-xs"><?php echo $setText ?></button></td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>Adminmanagement/adddepartment/<?php echo $departmentData->houdin_admin_department_id ?>" class="btn btn-warning btn btn-sm"><i class="fa fa-pencil m-r-5"></i>Edit</a>
                                        <button data-id="<?php echo $departmentData->houdin_admin_department_id ?>" type="button" class="btn btn-warning btn btn-sm deleteBtn"><i class="fa fa-trash m-r-5"></i>Delete</button>

                                    </td>
                                  </tr>
                                <?php }
                                ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->



<div id="deletmoal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >

<div class="modal-dialog">
<?php echo form_open(base_url( 'Adminmanagement/department' )); ?>
<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

<h4 class="modal-title">Delete Entry</h4>

</div>

<div class="modal-body">
<div class="row">
<div class="col-md-12">
<input type="hidden" class="deleteId" name="deleteId"/>
<h4><b>Do you really want to delete this entry ?</b></h4>
</div>
</div>
</div>

<div class="modal-footer">

<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

<input type="submit" class="btn btn-info" name="deleteDepartment" value="Delete">

</div>

</div>
<?php echo form_close(); ?>
</div>

</div>
<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">
$(document).on('click','.deleteBtn',function(){
    $('.deleteId').val($(this).attr('data-id'));
    $('#deletmoal').modal('show');
})
</script>