<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/settingsidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">Inventory Setting</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li><a href="javascript:void(0);">General</a></li>
                  <li class="active">Inventory Setting</li>
                  </ol>
                  </div>
                
               
            </div>
           <!--END Page-Title -->  
             <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
<?php 
        $inventorysetting_data=$inventorysetting[0];
 
    if($inventorysetting_data->Universal_rl=='1'){

      $Universal_rl='checked';
    }else{
      $Universal_rl='';
    }

        ?>
        <?php echo form_open(base_url('Setting/inventorysetting_save'), array( 'id' => 'inventorysetting_save', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
            <div class="row m-t-20">
        <div class="col-sm-12">
            <div class="card-box">
          <div class="col-md-12">
            <div class="form-group">
            <input type="checkbox" value="1"  name="Universal_rl" <?=$Universal_rl;?> class="">&nbsp;Universal Reorder Level
            </div>
          </div>
          <div class="col-md-12">
          <div class="ajax_response_result">
            <div class="form-group">
        <label>Universal Reorder Quantity</label>
        <input class="form-control required_validation_for_add_custo" value="<?=$inventorysetting_data->inventorysetting_number?>" name="inventorysetting_number"  type="text" placeholder="Universal Reorder Quantity">
        </div>
 </div>
          </div>
          <input type="hidden" name="update_id" value="<?=$inventorysetting_data->id?>">
          <div class="row">
        <input type="submit" class="btn btn-default pull-right m-r-10" name="" value="Submit">
        <button type="reset" class="btn btn-info pull-right m-r-10" id="enable_custome_reset">Reset</button>

        </div>

        </div>
      </div>
    </div>


        <?php echo form_close(); ?>
  </div>
</div>

<?php $this->load->view('Template/footer.php') ?>

<script type="text/javascript">
  
$(document).ready(function(){

 
 $("#enable_custome_reset").click(function () { 
  
 
$('.required_validation_for_add_custo').each(function() {
  
        $(this).val('');
         
    });
 });


 $('#inventorysetting_save').submit(function(e){
    e.preventDefault(); 
 

var check_required_field='';
      $(this).find(".required_validation_for_add_custo").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {
        return false;
      }else{
      


    urls='<?=base_url();?>'+'Setting/inventorysetting_save';
           
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".ajax_response_result").html(data);
        }else{
        window.location.reload();
        }
        }
        }); } 
    });  


 }); 

</script>
