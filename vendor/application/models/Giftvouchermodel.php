<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Giftvouchermodel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
    }
    public function addGiftVouchersData($data)
    {
        $getInsertStatus = $this->db->insert('houdinv_vouchers',$data);
        if($getInsertStatus == 1)
        {
            return array('message'=>'yes');
        }   
        else
        {
            return array('message'=>'no');
        }
    }
    public function getGiftVoucherTotalCount()
    {
        $this->db->select('COUNT(houdinv_vouchers_id) AS totalCount')->from('houdinv_vouchers');
        $getToatalData = $this->db->get()->result();
        return array('totalRows'=>$getToatalData[0]->totalCount);
    }
    public function fetchVoucherDetail($data)
    {
        $this->db->select('*')->from('houdinv_vouchers');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getGiftVoucherData = $this->db->get();
        $resultGiftVoucherdata = ($getGiftVoucherData->num_rows() > 0)?$getGiftVoucherData->result():FALSE;
        // get total gift voucher
        $getTotalGiftVoucher = $this->db->select('COUNT(houdinv_vouchers_id) as totalGiftVoucher')->from('houdinv_vouchers')->get()->result();
        // get total active gift voucher
        $getTotalActiveGiftVoucher = $this->db->select('COUNT(houdinv_vouchers_id) as totalActiveGiftVoucher')->from('houdinv_vouchers')->where('houdinv_vouchers_status','active')->get()->result();
        // get total deactive voucher
        $getTotalDeativeGiftVoucher = $this->db->select('COUNT(houdinv_vouchers_id) as totalDeactiveGiftVoucher')->from('houdinv_vouchers')->where('houdinv_vouchers_status','deactive')->get()->result();
        
        return array('voucherList'=>$resultGiftVoucherdata,'total'=>$getTotalGiftVoucher[0]->totalGiftVoucher,'totalActive'=>$getTotalActiveGiftVoucher[0]->totalActiveGiftVoucher,'totalDeactive'=>$getTotalDeativeGiftVoucher[0]->totalDeactiveGiftVoucher);
    }
    public function deleteGiftVocher($data)
    {
        $explodeId = explode(",",$data);
        $this->db->where_in('houdinv_vouchers_id',$explodeId);
        $getDeleteStatus = $this->db->delete('houdinv_vouchers');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function updateGiftVoucherStatus($data)
    {
        $setData = strtotime(date('Y-m-d'));
        $explodeData = explode(',',$data['id']);
        $setUpdateArray = array('houdinv_vouchers_status'=>$data['editGiftVoucherStatus'],'houdinv_vouchers_modified_at'=>$setData);
        $this->db->where_in('houdinv_vouchers_id',$explodeData);
        $getUpdateStatus = $this->db->update('houdinv_vouchers',$setUpdateArray);
        if($getUpdateStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function fetchGiftVoucherData($data)
    {
        $getGiftVoucherData = $this->db->select('*')->from('houdinv_vouchers')->where('houdinv_vouchers_id',$data)->get()->result();
        return array('voucherList'=>$getGiftVoucherData);
    }
    public function updateGiftVoucher($data,$id)
    {
        $this->db->where('houdinv_vouchers_id',$id);
        $getUpdateStatus = $this->db->update('houdinv_vouchers',$data);
        if($getUpdateStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
}  

