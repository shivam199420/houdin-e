<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/settingsidebar.php') ?>

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Online Store Setting</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li><a href="javascript:void(0);">General</a></li>
                  <li class="active">Online Store Setting</li>
                  </ol>
                  </div>
               
            </div>
           <!--END Page-Title --> 
            <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
         <?php 
 
         $storesetting=$storesetting[0];

         if($storesetting->display_out_stock=='1'){
    $display_out_stock='checked';
  }else{
    $display_out_stock='';
  }
  if($storesetting->receieve_order_out=='1'){
    $receieve_order_out='checked';
  }else{
    $receieve_order_out='';
  }
  if($storesetting->custom_html_footer=='1'){
    $custom_html_footer='checked';
  }else{
    $custom_html_footer='';
  }
  if($storesetting->add_to_cart_button=='1'){
    $add_to_cart_button='checked';
  }else{
    $add_to_cart_button='';
  }
  if($storesetting->email_mandatory=='1'){
    $email_mandatory='checked';
  }else{
    $email_mandatory='';
  } 
  if($storesetting->verify_mobile_numer=='1'){
    $verify_mobile_numer='checked';
  }else{
    $verify_mobile_numer='';
  } 
  if($storesetting->otp_=='1'){
    $otp_='checked';
  }else{
    $otp_='';
  } 
  if($storesetting->otp_setting=='never_ask'){
    $never_ask='checked';
  }else{
    $never_ask='';
  } 

  if($storesetting->otp_setting=='always_ask'){
    $always_ask='checked';
  }else{
    $always_ask='';
  } 

  if($storesetting->otp_setting=='ask_otp'){
    $ask_otp='checked';
  }else{
    $ask_otp='';
  } 
    ?> 

           <?php echo form_open(base_url('Setting/storesettingAdd'), array( 'id' => 'storesettingadd', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
    
            <div class="row m-t-20">
              <div class="col-md-12">
                <div class="card-box row">


  <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?=$display_out_stock;?> name="display_out_stock">&nbsp;Display Out Of Stock Products
</div>
</div>
<div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?=$receieve_order_out;?>  name="receieve_order_out">&nbsp;Receive Order On Out Of Stock Product
</div>
</div>
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?=$custom_html_footer;?>  name="custom_html_footer">&nbsp;Insert Custom HTML Footer On Pages
</div>
</div> -->
<div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1"  <?=$add_to_cart_button;?> name="add_to_cart_button">&nbsp;Show Add to Cart Button On Search Suggestation
</div>
</div>
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1"  <?=$email_mandatory;?> name="email_mandatory">&nbsp;Make Email Mandatory For Orders Placed Through Website a App
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1"  <?=$verify_mobile_numer;?> name="verify_mobile_numer">&nbsp;Verify Mobile Number Before Sending Enquaries
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?=$otp_;?>  name="otp_">&nbsp;OTP Setting For Checkout
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<div class="row">

                            			<div class="col-sm-12">


                            				<form class="form-inline">
												<div class="form-group m-r-10">
                          <input type="radio"  <?=$ask_otp;?>  name="otp_setting" value="ask_otp" id="ask_otp">
													<label for="ask_otp">Ask For OTP Only When Customer Data is Not Present</label>

												</div>
												<div class="form-group m-r-10">
                            <input type="radio"  <?=$always_ask;?>  name="otp_setting" id="always_ask" value="always_ask">
													<label for="always_ask">Always Ask</label>

												</div>
                        <div class="form-group m-r-10">
                            <input type="radio"  <?=$never_ask;?>  name="otp_setting" value="never_ask">
													<label for="exampleInputEmail2">Never Ask</label>

												</div>
                    
											</form>
                            			</div>

                            		</div>
</div>
</div> -->
<div class="col-md-12">
  <div class="form-group">
    <button type="submit" class="btn btn-default">Submit</button>
  </div>

</div>












                </div>
              </div>
            </div>









          </div>
        </div>
      </div>
    <?php $this->load->view('Template/footer.php') ?>
