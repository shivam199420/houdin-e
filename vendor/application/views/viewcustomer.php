    <?php $this->load->view('Template/header.php') ?>
    <?php $this->load->view('Template/sidebar.php') ?>
    <style type="text/css">
    .widget-style-2 i {
    background: rgba(244, 248, 251, 0.6) !important;
    font-size: 48px;
    padding: 30px 8px;
    }
.tabs li.tab {
    background-color: #c3c2c2;
    display: block;
    float: left;
    margin: 0;
    text-align: center;
    width: auto;
    font-size: 13px
}
.tabs {
    background-color: #244666;
    margin: 0 auto;
    padding: 0px;
    position: relative;
    white-space: nowrap;
}

.nav.nav-tabs > li.active > a {
    background-color: #ffffff!important;
    color: #000!important;
    border: 0;
}
.custom_h3 {
    background-color: #244667;
    line-height: 36px!important;
    margin-top: 0px;
    color: #fff;
    padding: 0px 10px;
    font-size: 18px!important;
}

    </style>
    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">
    <!--START Page-Title -->
            <div class="row">
      
                <div class="col-md-8">
                   <h4 class="page-title">View Customer</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                   <li><a href="<?php echo base_url(); ?>Customer">Customer</a></li>
                  <li class="active">View Customer</li>
                  </ol>
                  </div>
            
            </div>
           <!--END Page-Title --> 

   

<!-- START TAB -->
            
  <!-- END TAB -->
    <div class="row m-t-20">
    <div class="col-sm-4">


    <div class="card-box no-pad no-margin"> 


    <div class="row">
    <div class="col-md-12 form-horizontal pad-10">
    <h4 class="custom_h3">Personal Info</h4>
    <div class="form-group m-b-0">
    <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Name : </label>
    <div class="col-md-8 col-xs-8 col-sm-8">
    <p class="line-height-form-desktop" ><?php if($customerData[0]->houdinv_user_name) { echo $customerData[0]->houdinv_user_name; } else { echo "--"; } ?></p>
    </div>
    </div>

    <div class="form-group m-b-0">
    <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Email : </label>
    <div class="col-md-8 col-xs-8 col-sm-8">
    <p class="line-height-form-desktop"><?php if($customerData[0]->houdinv_user_email) { echo $customerData[0]->houdinv_user_email; } else { echo "--"; } ?></p>
    </div>
    </div>

    <div class="form-group m-b-0">
    <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Contact : </label>
    <div class="col-md-8 col-xs-8 col-sm-8 ">
    <p class="line-height-form-desktop"><?php if($customerData[0]->houdinv_user_contact) { echo $customerData[0]->houdinv_user_contact; } else { echo "--"; } ?></p>
    </div>
    </div>

    <div class="form-group m-b-0">
    <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Address : </label>
    <div class="col-md-8 col-xs-8 col-sm-8">
    <p class="line-height-form-desktop"><?php if($customerData[0]->houdinv_user_address_street_line1) { echo $customerData[0]->houdinv_user_address_street_line1; } else { echo "--"; } ?></p>
    </div>
    </div>

    <div class="form-group m-b-0">
    <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">City : </label>
    <div class="col-md-8 col-xs-8 col-sm-8">
    <p class="line-height-form-desktop"><?php if($customerData[0]->houdinv_user_address_city) { echo $customerData[0]->houdinv_user_address_city; } else { echo "--"; } ?></p>
    </div>
    </div>

    <div class="form-group m-b-0">
    <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">State : </label>
    <div class="col-md-8 col-xs-8 col-sm-8">
    <p class="line-height-form-desktop"><?php if($customerData[0]->houdinv_user_address_state) { echo $customerData[0]->houdinv_user_address_state; } else { echo "--"; } ?></p>
    </div>
    </div>

    <div class="form-group m-b-0">
    <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Country : </label>
    <div class="col-md-8 col-xs-8 col-sm-8">
    <p class="line-height-form-desktop"><?php if($customerData[0]->houdinv_user_address_country) { echo $customerData[0]->houdinv_user_address_country; } else { echo "--"; } ?></p>
    </div>
    </div>

    <div class="form-group m-b-0">
    <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Landmark:</label>
    <div class="col-md-8 col-xs-8 col-sm-8">
    <p class="line-height-form-desktop"><?php if($customerData[0]->houdinv_user_address_landmark) { echo $customerData[0]->houdinv_user_address_landmark; } else { echo "--"; } ?></p>
    </div>
    </div>

    <div class="form-group m-b-0">
    <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Pincode : </label>
    <div class="col-md-8 col-xs-8 col-sm-8">
    <p class="line-height-form-desktop"><?php if($customerData[0]->houdinv_user_address_pincode) { echo $customerData[0]->houdinv_user_address_pincode; } else { echo "--"; } ?></p>
    </div>
    </div>



    </div>

    </div>
    </div>

    </div>

    <div class="col-md-8 custom-mt-20-xs">
    <div class="card-box table-responsive no-pad no-margin">
    <div class="row">
                    <div class="col-lg-12"> 
                                <ul class="nav nav-tabs tabs">
                                    <li class="active tab">
                                        <a style="" href="#home1" data-toggle="tab" aria-expanded="true"> 
                                            <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                            <span class="hidden-xs">Order Details</span> 
                                        </a> 
                                    </li>     
                                    <li class="tab"> 
                                        <a href="#profile1" data-toggle="tab" aria-expanded="true"> 
                                            <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                            <span class="hidden-xs">Upcoming order details</span> 
                                        </a> 
                                    </li> 
                                    <!-- <li class="tab"> 
                                        <a href="#messages1" data-toggle="tab" aria-expanded="true"> 
                                            <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> 
                                            <span class="hidden-xs">Account status</span> 
                                        </a> 
                                    </li>  -->
                                    <li class="tab"> 
                                        <a href="#tags" data-toggle="tab" aria-expanded="true"> 
                                            <span class="visible-xs"><i class="fa fa-cog"></i></span> 
                                            <span class="hidden-xs">Tags</span> 
                                        </a> 
                                    </li> 
                                     
                                </ul> 

          <!-- START TAB ALLORDER -->
          <div class="tab-content"> 
              <div class="tab-pane active" id="home1"> 
    <div class="row m-t-20">
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Order.png">
    <h2 class="m-0 text-dark counter font-600"><?php if($totalOrder) { echo $totalOrder; } else { echo 0; } ?></h2>
    <div class="text-muted m-t-5">Total order</div>
    </div>
    </div>
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Order_Completed.png">
    <h2 class="m-0 text-dark counter font-600"><?php if($totalCompletedOrder) { echo $totalCompletedOrder; } else { echo 0; } ?></h2>
    <div class="text-muted m-t-5">Total Completed order </div>
    </div>
    </div>
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Order_Cancel.png">
    <h2 class="m-0 text-dark counter font-600"><?php if($totalCancelOrder) { echo $totalCancelOrder; } else { echo 0; } ?></h2>
    <div class="text-muted m-t-5">Total cancelled order</div>
    </div>
    </div>


    </div>
    <div class="row">
    <div class="container">
    <table class="table table-striped table-bordered table_shop_custom">
    <thead>
    <tr>
    <th>Order ID</th>
    <th>Delivery Type</th>
    <th>Total Amount</th>
    <th>Payment Status</th>
    <th>Order Status</th>
    </tr>
    </thead>
    <tbody>
    <?php 
    foreach($totalOrder as $totalOrderList)
    {
    ?>
    <tr>
    <td><?php echo $totalOrderList->houdinv_order_id ?></td>
    <td><?php if($totalOrderList->houdinv_order_delivery_type == 'deliver') { echo "Home Delivery"; } else { echo "Customer Pickup"; } ?></td> 
    <td><?php echo $totalOrderList->houdinv_orders_total_Amount ?></td>
    <td><?php if($totalOrderList->houdinv_payment_status == 1) { ?> <button type="button" class="btn btn-xs btn-success">Paid</button> <?php  } else { ?> <button type="button" class="btn btn-xs btn-danger">Not Paid</button> <?php } ?></td>
    <td><button type="button" class="btn btn-default btn-xs"><?php if($totalOrderList->houdinv_order_confirmation_status) { echo $totalOrderList->houdinv_order_confirmation_status; } else  { echo  "--"; } ?></button></td>
    </tr>
    <?php }
    ?>
    </tbody>
    </table>
    </div>
    </div>
    </div> 
          <!-- END TAB ALLORDER -->


          <!-- START TAB DELIVERY -->
          <div class="tab-pane" id="profile1"> 
    <div class="row">
    <div class="container">
    <table class="table table-striped table-bordered table_shop_custom">
    <thead>
    <tr>
    <th>Order ID</th>
    <th>Delivery Type</th>
    <th>Total Amount</th>
    <th>Payment Status</th>
    <th>Order Status</th>
    </tr>
    </thead>
    <tbody>
    <?php 
    foreach($upcomingOrders as $upcomingOrdersList)
    {
     ?>   
     <tr>
     <td><?php echo $upcomingOrdersList->houdinv_order_id ?></td>
    <td><?php if($upcomingOrdersList->houdinv_order_delivery_type == 'deliver') { echo "Home Delivery"; } else { echo "Customer Pickup"; } ?></td> 
    <td><?php echo $upcomingOrdersList->houdinv_orders_total_Amount ?></td>
    <td><?php if($upcomingOrdersList->houdinv_payment_status == 1) { ?> <button type="button" class="btn btn-xs btn-success">Paid</button> <?php  } else { ?> <button type="button" class="btn btn-xs btn-danger">Not Paid</button> <?php } ?></td>
    <td><button type="button" class="btn btn-default btn-xs"><?php if($upcomingOrdersList->houdinv_order_confirmation_status) { echo $upcomingOrdersList->houdinv_order_confirmation_status; } else  { echo  "--"; } ?></button></td>
    </tr>
    <?php }
    ?>
    </tbody>
    </table>
    </div> 
    </div>
    </div>  
            <!-- END TAB DELIVERY -->

            <!-- START TAB COMPLETED -->

            <!-- <div class="tab-pane" id="messages1">  
    <div class="row m-t-20">
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <i class="md md-attach-money text-primary"></i>
    <h2 class="m-0 text-dark counter font-600">50568</h2>
    <div class="text-muted m-t-5">Total upcoming order</div>
    </div>
    </div>
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <i class="md md-add-shopping-cart text-pink"></i>
    <h2 class="m-0 text-dark counter font-600">1256</h2>
    <div class="text-muted m-t-5">Total Completed order </div>
    </div>
    </div>
    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <i class="md md-store-mall-directory text-info"></i>
    <h2 class="m-0 text-dark counter font-600">18</h2>
    <div class="text-muted m-t-5">Total cancelled order</div>
    </div>
    </div>


    </div>
    <div class="row">
    <div class="container">
    <table class="table table-striped table-bordered table_shop_custom">
    <thead>
    <tr>

    <th><input type="checkbox"></th>
    <th>Order ID</th>
    <th>Product name</th>
    <th>Expected Delivery date</th>
    <th>Order Status</th>

    </tr>
    </thead>
    <tbody>
    <tr>
    <td><input type="checkbox"></td>
    <td>H124568</td>
    <td>Electronic</td> 
    <td>DD/MM/YYYY</td>

    <td>Pending</td>


    </tr>
    </tbody>
    </table>
    </div> 
    </div>
    </div>  -->
          <!-- END TAB COMPLETED -->



           <!-- END TAB PICKUP -->

          <div class="tab-pane" id="tags">
    <?php 
    foreach($customerTagData as $customerTagDataValue)
    {
    ?>    
    <div class="row custom_div">
    <div class="col-sm-12">
    <div class="clearfix">
    <div class="row">
    <div class="pull-left">
    <span><?php echo $customerTagDataValue->houdinv_user_tag_name ?></span><br>
    </div>
    </div>
    <div class="row">
    <div class="pull-left"><h5 class="text-right"><i class="fa fa-user"></i> <b class="custom_admin">Vendor</b></h5></div>
    <div class="pull-right">
    <h5>
    <strong><i class="fa fa-clock-o"></i> <?php echo date('d-m-Y h:i:s',$customerTagDataValue->houdinv_user_tag_created_date); ?></strong>
    </h5>
    </div>
    </div>
    </div>
    </div>
    </div>
    <?php }
    ?>

    </div> 
          <!-- END TAB PICKUP -->

   

                                </div> 
                              
                            </div> 

                            
  </div>
    </div>
    </div>

    <div id="con-close-modal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
    <div class="modal-content"> 
    <div class="modal-header"> 
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
    <h4 class="modal-title">Add Inventory</h4> 
    </div> 
    <div class="modal-body"> 
    <div class="row"> 
    <div class="col-md-12"> 
    <div class="form-group"> 
    <label for="field-1" class="control-label">Choose Status</label> 
    <select class="form-control">
    <option>Receive</option>
    <option>Return</option>

    </select>
    </div> 
    </div> 

    </div> 
    <div class="row"> 
    <div class="col-md-12"> 
    <div class="form-group no-margin"> 
    <label for="field-7" class="control-label">Note</label> 
    <textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
    </div> 
    </div> 
    </div> 
    </div> 
    <div class="modal-footer"> 
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
    </div> 
    </div> 
    </div>
    </div><!-- /.modal -->

    </div>
    </div>
    </div>
    <!--Delete-->
    <?php $this->load->view('Template/footer.php') ?>
    <script>
    var resizefunc = [];
    </script>