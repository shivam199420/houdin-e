 </main>
 <?php 
 $getvendorURL = getvendorurl();
 ?>
 <style>
     small {
    font-size: 100% !important;
}
.font900{
    font-weight:900;
}
 </style>
<footer id="site-footer" class="footer text-white">
        <div class="container py-7">
          <div class="row gap-y">

            <!-- <div class="col-12 col-md-4 col-xl-4">
              <h5 class="mb-4"><strong>About</strong></h5>
              <p>We’re a team of experienced designers and developers. We can combine beautiful, modern designs with clean, functional and high-performance code to produce stunning websites.</p>
            </div> -->

            <div class="col-6 col-md-3 col-xl-3">
              <h5 class="mb-4"><strong class="font900">Product</strong></h5>
              <div class="nav flex-column">
                <!-- <a class="nav-link" href="<?php echo base_url(); ?>home/about">Why Houdin-e?</a> -->
                
                <a class="nav-link" href="<?php echo base_url(); ?>home/plans">Pricing</a>
                
              </div> 
            </div>

            

            <div class="col-6 col-md-3 col-xl-3">
              <h5 class="mb-4"><strong class="font900">Community</strong></h5>
              <div class="nav flex-column">
               <!-- <a class="nav-link" href="<?php echo base_url(); ?>home/blog">Blog</a>		-->		  
              <!-- <a class="nav-link" href="<?php echo base_url(); ?>home/how_it_work">How it works</a> -->
                <a class="nav-link" href="<?php echo base_url(); ?>home/privacy_policy">Privacy & Policy</a>
                <a class="nav-link" href="<?php echo base_url(); ?>home/terms">Terms & Conditions</a>
              </div>
            </div>

            <div class="col-6 col-md-3 col-xl-3">
              <h5 class="mb-4"><strong class="font900">Support</strong></h5>
              <div class="nav flex-column">
                <a class="nav-link" href="">Help</a> 
                <a class="nav-link" href="<?php echo base_url(); ?>home/faq">FAQ</a>
              </div>
            </div>

            <div class="col-6 col-md-3 col-xl-3">
              <h5 class="mb-4"><strong class="font900">Apps</strong></h5>
              <div class="nav flex-column">
                <a class="mt-1" href=""><img src="https://d1jnq44ocxfstb.cloudfront.net/img/index/google_play.png" width="120px"/></a>
                <a class="mt-2" href=""><img src="https://d1jnq44ocxfstb.cloudfront.net/img/index/appleico.png" width="120px"/></a> 
              </div>
            </div>

          </div>
        </div>

        <hr>

        <div class="container">
          <div class="row gap-y">

            <div class="col-md-12 text-center  ">
           <small>&COPY; <?php echo date('Y') ?> Warrdel Solutions LLP  All rights reserved.</small>
            </div>
            <p class="col-md-12 text-center">
           		<small>Design & Developed by <a href="https://hawkscode.com/" target="_blank">HawksCode Softwares Pvt. Ltd.</a></small>
            </p>

            <div class="col-md-4 text-center text-md-right">
              <div class="social">
                <a class="social-facebook" target="_blank" href=""><i class="fa fa-facebook"></i></a>
                <a class="social-twitter" target="_blank" href=""><i class="fa fa-twitter"></i></a>
                <a class="social-youtube" target="_blank" href=""><i class="fa fa-youtube"></i></a>
                <a class="social-instagram" target="_blank" href=""><i class="fa fa-instagram"></i></a>
              </div>
            </div>

          </div>
        </div>
      </footer><!-- /.footer -->

    <!-- Scripts -->
    <script src="<?php echo base_url(); ?>assets/js/page.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/intlTelInput.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/aos.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/script.js"></script> 
    <script src="<?php echo base_url() ?>assets/js/validation.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.min.js" type="text/javascript"></script><script src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
    <!-- <script src="<?php //echo base_url() ?>assets/js/jquery.min.js"></script> -->
<script>AOS.init();</script> 
<script>
	$(document).ready(function(){
    $(".Internationphonecode").intlTelInput({
        hiddenInput: "full_phone",
          initialCountry: "auto",
          geoIpLookup: function(callback) {
            $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              callback(countryCode);
            });
          },
    // allowDropdown: false,
    // autoHideDialCode: false,
    // autoPlaceholder: "off",
    // dropdownContainer: "body",
    // excludeCountries: ["us"],
    // formatOnDisplay: false,
    // geoIpLookup: function(callback) {
    //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    //     var countryCode = (resp && resp.country) ? resp.country : "";
    //     callback(countryCode);
    //   });
    // },
    // hiddenInput: "full_number",
    // initialCountry: "auto",
    // nationalMode: false,
    // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    // placeholderNumberType: "MOBILE",
    preferredCountries: ['in', 'us'],
    // separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/js/utils.js"
    });
});	
  </script>
  <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#regitserFirstForm',function(e){
			var check_required_field='';
			$(".required_validation_for_register_first_form").each(function(){
				var val22 = $(this).val();
				if (!val22){
          // check_required_field =$(this).size();
          check_required_field =$(this).length;
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
        if($('.checkTermCheckbox').prop('checked') == true)
        {
          e.preventDefault();
          $('.showCheckboxError').hide();
          $('.setSignupData').attr('disabled',true);
          var formData = $('#regitserFirstForm').serialize();
          jQuery.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "Register/registerFirst",
          data: formData,
          success: function(data) {
            if($.trim(data) == 'Phone number is already exist')
            {
              $('.setSignupData').attr('disabled',false);
              $('.formErrorData').show();
              $('.formErrorData').html('')
              $('.formErrorData').html('<div class="alert alert-danger">Phone number is already exist</div>');
              setTimeout(function(){
                window.location.href='<?php echo base_url(); ?>';
              },2000)
            }
            else if($.trim(data) == 'success')
            {
              $('.setSignupData').attr('disabled',false);
              window.location.href='<?php echo base_url(); ?>'+'Register/activate';
            }
            else if($.trim(data) == 'All fields are mandatory')
            {
              $('.setSignupData').attr('disabled',false);
              $('.formErrorData').show();
              $('.formErrorData').html('')
              $('.formErrorData').html('<div class="alert alert-danger">All fields are mandatory</div>');
              setTimeout(function(){
                window.location.href='<?php echo base_url(); ?>';
              },2000)
            }
            else
            {
              $('.setSignupData').attr('disabled',false);
              $('.formErrorData').show();
              $('.formErrorData').html('')
              $('.formErrorData').html('<div class="alert alert-danger">Something went wrong. Please try again</div>');
              setTimeout(function(){
                window.location.href='<?php echo base_url(); ?>';
              },2000)
            }
          }
          }); 
        }
        else
        {
          $('.showCheckboxError').show();
          return false;
        }
			}
		});
	});
	</script>
  <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#loginForm',function(e){
			var check_required_field='';
			$(".required_validation_for_login_form").each(function(){
				var val22 = $(this).val();
				if (!val22){
          // check_required_field =$(this).size();
          check_required_field =$(this).length;
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
        e.preventDefault();
        var formData = $('#loginForm').serialize();
        $('.setLoginSubmit').attr('disabled',true);
        jQuery.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "Login/loginCheckData",
          data: formData,
          success: function(data) {
              var vendorURL = '<?php echo $getvendorURL ?>';
            if($.trim(data) == 'mandatory')
            {
              $('.setLoginSubmit').attr('disabled',false);
              $('.showLoginError').show();
              $('.showLoginError').html('');
              $('.showLoginError').html('<div class="alert alert-danger">All fields are mandatory</div>');
              setTimeout(function(){
                window.location.href='<?php echo base_url(); ?>';
              },2000)
            }
            else if($.trim(data) == 'success')
            {
              $('.setLoginSubmit').attr('disabled',false);
               window.location.href = ''+vendorURL+'Login/setLoginAuth';
            }
            else if($.trim(data) == 'You are not active from admin side. Please contact to support')
            {
              $('.setLoginSubmit').attr('disabled',false);
              $('.showLoginError').show();
              $('.showLoginError').html('');
              $('.showLoginError').html('<div class="alert alert-danger">You are not active from admin side. Please contact to support</div>');
              setTimeout(function(){
                window.location.href='<?php echo base_url(); ?>';
              },2000)
            }
            else if($.trim(data) == 'Please enter correct password')
            {
              $('.setLoginSubmit').attr('disabled',false);
              $('.showLoginError').show();
              $('.showLoginError').html('');
              $('.showLoginError').html('<div class="alert alert-danger">Please enter correct password</div>');
              setTimeout(function(){
                window.location.href='<?php echo base_url(); ?>';
              },2000)
            }
            else if($.trim(data) == 'Your package is expired. Please upgrade your package')
            {
              $('.setLoginSubmit').attr('disabled',false);
              $('.showLoginError').show();
              $('.showLoginError').html('');
              $('.showLoginError').html('<div class="alert alert-danger">Your package is expired. Please upgrade your package</div>');
              setTimeout(function(){
                window.location.href='<?php echo base_url(); ?>';
              },2000)
            }
            else if($.trim(data) == 'Please enter correct email address')
            {
              $('.setLoginSubmit').attr('disabled',false);
              $('.showLoginError').show();
              $('.showLoginError').html('');
              $('.showLoginError').html('<div class="alert alert-danger">Please enter correct email address</div>');
              setTimeout(function(){
                window.location.href='<?php echo base_url(); ?>';
              },2000)
            }
          }
          }); 
			}
		});
	});
    
    
    
    
    
      /*==============Form Validation===========================*/
        $(document).ready(function(){

        $(document).on('submit','#forgetFirstForm',function(c){
                // alert("h");
              c.preventDefault();
                 var rep_image_val='';
                 $(".required_validation_for_register").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                        
                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
                
                $('.required_validation_for_register').on('keyup',function()
                                {
                                    
                                        $(this).css("border-color","#ccc");
                                });
                
                
                 $('.required_validation_email').on('blur',function()
                                {
                                    if(!validateEmail($(this).val()))
                                       {
                                        
                                        $(this).css("border-color","red");
                                        $('.messagesd').text('please fill correct email  address');
                                        }
                                        else
                                        {
                                           $(this).css("border-color","#ccc"); 
                                           $('.messagesd').text('');
                                        }
                                });
                
                   function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
  }



                
                if(rep_image_val)
                {
                      
                        return false;
                }
                else
                {
                              var formData = $('#forgetFirstForm').serialize();
          jQuery.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "Register/ForgetMailSend",
          data: formData,
          success: function(data) {
            if($.trim(data) == 'error')
            {
             
              $('.formErrorData').show();
              $('.formErrorData').html('')
              $('.formErrorData').html('<div class="alert alert-danger">Somthing went wrong</div>');
              setTimeout(function(){
                window.location.href='<?php echo base_url(); ?>';
              },4000)
            }
            else if($.trim(data) == 'success')
            {
              alert("success");
              window.location.href='<?php echo base_url(); ?>'+'Home/Reset';
            }
            else if($.trim(data) == 'error2')
            {
             // $('.setSignupData').attr('disabled',false);
              $('.formErrorData').show();
              $('.formErrorData').html('')
              $('.formErrorData').html('<div class="alert alert-danger">Please enter correct email</div>');
              setTimeout(function(){
                window.location.href='<?php echo base_url(); ?>';
              },4000)
            }
            else
            {
              //$('.setSignupData').attr('disabled',false);
              $('.formErrorData').show();
              $('.formErrorData').html('')
              $('.formErrorData').html('<div class="alert alert-danger">All fields are mandatory or please use proper format of email</div>');
              setTimeout(function(){
              window.location.href='<?php echo base_url(); ?>';
              },4000)
            }
          }
          }); 
                }

        });
        });
	</script>

  </body>
</html>
