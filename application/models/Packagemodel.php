<?php
class Packagemodel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    public function fetchDynamicPackage()
    {
        $getPackageData = $this->db->select('houdin_packages.*')->from('houdin_packages')->where('houdin_packages.houdin_package_status','active')->get()->result();
        return array('packageData'=>$getPackageData);
    }    
}