		<?php
		$getShopName = getfolderName($this->session->userdata('shopName'));
		$this->load->view(''.$getShopName.'/Template/header'); ?>

		<!-- Page item Area -->
		<div id="page_item_area">
		<div class="container">
		<div class="row">
		<div class="col-sm-6 text-left">
		<h3>Contact</h3>
		</div>


		</div>
		</div>
		</div>

		<!-- Contact Page -->
		<div class="contact_page_area fix">
		<div class="container">
		<div class="row">
		<div class="contact_frm_area text-left col-lg-6 col-md-12 col-xs-12">
		<h3>Get in Touch</h3>
		<?php
		if($this->session->flashdata('error'))
		{
			echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
		}
		else if($this->session->flashdata('success'))
		{
			echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
		}
		?>
		<?php echo form_open(base_url( 'Contact/addcontact' ), array( 'id' => 'shopContact', 'method'=>'post' ));?>
		<div class="form-row">
		<div class="form-group col-sm-6">
			<input type="text" name="userName" class="form-control required_validation_for_shop_contact name_validation" placeholder="Name*" /></div>
		<div class="form-group col-sm-6"><input type="text" name="userEmail" class="form-control required_validation_for_shop_contact email_validation name_validation" placeholder="Email*" /></div>
		</div>

		<div class="form-group">
		<input type="text" name="userEmailSub" class="form-control required_validation_for_shop_contact name_validation" placeholder="Subject" />
		</div>

		<div class="form-group">
		<textarea name="userEmailBody" rows="5" class="form-control required_validation_for_shop_contact name_validation" placeholder="Message"></textarea>
		</div>

		<div class="input-area submit-area">
		<input type="submit" class="btn border-btn btn-default text-center" value="Send Message"/>
		</div>
		<?php echo form_close(); ?>
		</div>

		<div class="contact_info col-lg-6 col-md-12 col-xs-12">
		<h3>Contact Info</h3>
		<div class="single_info">
		<div class="con_icon"><i class="fa fa-map-marker"></i></div>
		<p><?php echo $shopInfo[0]->houdinv_shop_address ?> </p>
		<p style="visibility: hidden;">sdffs</p>
		</div>
		<div class="single_info">
		<div class="con_icon"><i class="fa fa-phone"></i></div>
		<p>Phone : <?php echo $shopInfo[0]->houdinv_shop_contact_info ?></p>
		<p style="visibility: hidden;">sdffs</p>
		</div>
		<div class="single_info">
		<div class="con_icon"><i class="fa fa-envelope"></i></div>
		<a href="mailto:<?php echo $shopInfo[0]->houdinv_shop_customer_care_email ?>">Customer Care: <?php echo $shopInfo[0]->houdinv_shop_customer_care_email ?> </a> <br />
		<a href="mailto:<?php echo $shopInfo[0]->houdinv_shop_communication_email ?>">Communication: <?php echo $shopInfo[0]->houdinv_shop_communication_email ?> </a>
		</div>

		</div>
		</div>
		</div>


 

		</div>
		<?php
		$getShopName = getfolderName($this->session->userdata('shopName'));
		$this->load->view(''.$getShopName.'/Template/footer') ?>
		 
		<script type="text/javascript">
		$(document).ready(function(){
			$(document).on('submit','#shopContact',function(){
				var check_required_field='';
				$(this).find(".required_validation_for_shop_contact").each(function(){
					var val22 = $(this).val();
					if (!val22){
						check_required_field =$(this).size();
						$(this).css("border-color","#ccc");
						$(this).css("border-color","red");
					}
					$(this).on('keypress change',function(){
						$(this).css("border-color","#ccc");
					});
				});
				if(check_required_field)
				{
					return false;
				}
				else {
					return true;
				}
			});
		});
		</script>
