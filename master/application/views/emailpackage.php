        <?php $this->load->view('Template/header') ?>
        <?php $this->load->view('Template/sidebar') ?>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">
        <!-- Page-Title -->
        <div class="row">
        <div class="col-sm-12 m-b-15">
        <div class="btn-group pull-right m-t-15">
        <button type="button" class="btn btn-default btn-sm m-l-5" data-toggle="modal" data-target="#addEmailPackage"> <i class="fa fa-plus m-r-5"></i>Add Email Package</button>
        </div>
        <h4 class="page-title">Email Package</h4>

        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        else if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        ?>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="card-box table-responsive">
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th>Package Name </th>
        <th>Package Price</th>
        <th>Total SMS</th>
        <th>Action</th>
        <th>Status</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        foreach($emailPackageList as $emailPackageListData)
        {
         ?>   
         <tr>
        <td><?php echo $emailPackageListData->houdin_email_package_name ?></td>
        <td><?php echo $emailPackageListData->houdin_email_package_price ?></td>
        <td><?php echo $emailPackageListData->houdin_email_package_email_count ?></td>
        <td>
        <?php 
        if($emailPackageListData->houdin_email_package_status == 'active') { $setText = 'Active'; $setClass="success"; }
        else if($emailPackageListData->houdin_email_package_status == 'deactive') { $setText = 'Deactive'; $setClass="warning"; }
        ?>
        <button class="btn btn-xs btn-<?php echo $setClass; ?>"><?php echo $setText; ?></button>
        </td>
        <td>
        <button type="button" data-status="<?php echo $emailPackageListData->houdin_email_package_status ?>" data-count="<?php echo $emailPackageListData->houdin_email_package_email_count ?>" data-price="<?php echo $emailPackageListData->houdin_email_package_price ?>" data-name="<?php echo $emailPackageListData->houdin_email_package_name ?>" data-id="<?php echo $emailPackageListData->houdin_email_package_id ?>" class="btn btn-warning btn btn-sm editEmailPackageData"><i class="fa fa-pencil m-r-5"></i>Edit</button>
        <button type="button" data-id="<?php echo $emailPackageListData->houdin_email_package_id ?>" class="btn btn-warning btn btn-sm deleteEmailPackageBtn"><i class="fa fa-trash m-r-5"></i>Delete</button>
        </td>
        </tr>
        <?php }
        ?>
        </tbody>
        </table>
        </div>
        </div>
        </div>


        </div> <!-- container -->

        </div> <!-- content -->

        <div id="addEmailPackage" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Package/emailpackage' ), array( 'id' => 'addEmailPackageForm' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title">Add Email Package</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Package Name</label>
        <input type="text" class="form-control required_validation_for_email_package name_validation" maxlength="50" name="emailPackageName" placeholder="Package Name" />
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Package price</label>
        <input type="text" class="form-control required_validation_for_email_package number_validation" name="emailPackagePrice" placeholder="Package price" />
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Total Email contain</label>
        <input type="text" class="form-control required_validation_for_email_package number_validation" name="emailPackageCount" placeholder="Total sms contain" />
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Status</label>
        <select class="form-control required_validation_for_email_package" name="emailPackageStatus">
        <option value="">Choose Status</option>
        <option value="active">Active</option>
        <option value="deactive">Deactive</option>
        </select>
        </div>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="addEmailPackage" value="Submit">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>

        <div id="editEmailPackageModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Package/emailpackage' ), array( 'id' => 'editEmailPackageForm' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title">Edit Email Package</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <input type="hidden" class="EditEmailpackageId" name="EditEmailpackageId"/>
        <label for="field-7" class="control-label">Package Name</label>
        <input type="text" class="form-control required_validation_for_email_package name_validation editemailPackageName" maxlength="50" name="editemailPackageName" placeholder="Package Name" />
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Package price</label>
        <input type="text" class="form-control required_validation_for_email_package number_validation editemailPackagePrice" name="editemailPackagePrice" placeholder="Package price" />
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Total Email contain</label>
        <input type="text" class="form-control required_validation_for_email_package number_validation editemailPackageCount" name="editemailPackageCount" placeholder="Total sms contain" />
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Status</label>
        <select class="form-control required_validation_for_email_package editemailPackageStatus" name="editemailPackageStatus">
        <option value="">Choose Status</option>
        <option value="active">Active</option>
        <option value="deactive">Deactive</option>
        </select>
        </div>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="editEmailPackage" value="Submit">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        <div id="deleteEmailPackageModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Package/emailpackage'));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title">Delete Entry</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleteEmailPackageId" name="deleteEmailPackageId"/>
        <h4><b>Do you really want to delete this entry ?</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="deleteEmailPackage" value="Delete">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        <?php $this->load->view('Template/footer') ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $(document).on('submit','#addEmailPackageForm,#editEmailPackageForm',function(){
                    var check_required_field='';
                    $(this).find(".required_validation_for_email_package").each(function(){
                        var val22 = $(this).val();
                        if (!val22){
                            check_required_field =$(this).size();
                            $(this).css("border-color","#ccc");
                            $(this).css("border-color","red");
                        }
                        $(this).on('keypress change',function(){
                            $(this).css("border-color","#ccc");
                        });
                    });
                    if(check_required_field)
                    {
                        return false;
                    }
                    else {
                        return true;
                    }
                });
            });
            </script>
