<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Resetdbmodel extends CI_Model{
  function __construct() 
  {
    parent::__construct();
    $this->load->helper('database');
    $getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
  }
  public function getDb()
  {
        $tables = $this->db->list_tables();   
        for($index = 0; $index< count($tables); $index++)
        {
            $this->db->truncate($tables[$index]);
        }
  }
}