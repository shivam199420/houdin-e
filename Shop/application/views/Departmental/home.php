<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header') ?>
<?php
// $this->load->helper('setting');
$getData = stroeSetting($this->session->userdata('shopName'));
$cartFunction = $getData[0]->receieve_order_out;
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
    <!-- Slider -->
    <?php
        if(count($sliderListData) > 0)
        {
        ?>
        <style>
            .rev_slider_wrapper {
    height: 450px !important;
}
.forcefullwidth_wrapper_tp_banner
{
     height: 450px !important;
}
        </style>
    <section>
        <div class="rev_slider_wrapper fullscreen-container">
            <div class="rev-slider fullscreenbanner" id="slide-1" data-version="5.0">
                <ul>
                <?php
                foreach($sliderListData as $sliderListDataList)
                {
                  ?>
                    <li data-transition="slide">
                        <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/Slider/<?php echo $sliderListDataList->houdinv_custom_slider_image ?>" height="500px" alt="Slide 1" class="rev-slidebg">
                        <div class="tp-caption slider-heading-1" data-x="left" data-y="center" data-transform_in="y:-80px;opacity:0;s:800;e:easeInOutCubic;"
                            data-transform_out="y:-80px;opacity:0;s:300;" data-voffset="[-35,-35,-75,-70]" data-fontsize="['70','60','55','42']"
                            data-hoffset="['60','100','50','20']" data-start="900">
                            <?php echo $sliderListDataList->houdinv_custom_slider_head ?>
                        </div>
                        <div class="tp-caption slider-heading-2" data-x="left" data-y="center" data-transform_in="x:80px;opacity:0;s:800;e:easeInOutCubic;"
                            data-transform_out="x:80px;opacity:0;s:300;" data-whitespace="normal" data-width="['900','900','900','700','520'"
                            data-voffset="[45,45,10,10,-15,-10]" data-fontsize="['25','25','25','21']" data-hoffset="['60','100','50','20']"
                            data-start="1100">
                            <?php echo $sliderListDataList->houdinv_custom_slider_sub_heading ?>
                        </div>
                    </li>
                    
                <?php } ?>
                </ul>
      
            </div>
        </div>
    </section>
        <?php } ?>
    <!-- End Slider -->
    <?php
    if($this->session->flashdata('message_name'))
    {
        echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
    ?>
    <?php }
    if($this->session->flashdata('success'))
    {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    ?>
    <!-- Sale off -->
    <!-- Grid Product -->
    <?php
        if(count($categoryData) > 0 && $customHome[0]->houdinv_custom_home_data_category)
        {
        ?>
    <section class="py-50">
        <div class="port-title">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 text-center">
                        <h2 class="title"><?php echo $customHome[0]->houdinv_custom_home_data_category; ?></h2>
                        <div class="title-border mx-auto m-b-35"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="port-body">
            <div class="container">
                <div class="grid">
                    <div class="grid-body row" data-layout="fitRows">
                    <?php
                    foreach($categoryData as $categoryDataList)
                    {
                    if($categoryDataList->houdinv_category_thumb)
                    {
                        $setImageData = $this->session->userdata('vendorURL')."images/category/".$categoryDataList->houdinv_category_thumb;
                    }
                    else
                    {
                        $setImageData = base_url()."Extra/noPhotoFound.png";
                    }
                    ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 grid-item chair">
                            <div class="grid-product">
                                <div class="image bg-lightblue">
                                    <a href="<?php echo base_url() ?>Productlist/<?php echo $categoryDataList->houdinv_category_id ?>">
                                        <img src="<?php echo $setImageData; ?>" alt="<?php echo $categoryDataList->houdinv_category_name ?>" style="width: 100%;">
                                    </a>
                                </div>
                                <a href="<?php echo base_url() ?>Productlist/<?php echo $categoryDataList->houdinv_category_id ?>" class="name"><?php echo $categoryDataList->houdinv_category_name ?></a>
                                <div class="price"><?php echo date("Y"); ?> Collection</div>
                            </div>
                        </div>
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <?php } ?>



        <!-- latest product -->
        <?php
        if($customHome[0]->houdinv_custom_home_data_latest_product && count($latestProducts) > 0)
        {
        ?>
        <section class="py-50">
        <div class="port-title">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 text-center">
                        <h2 class="title"><?php echo $customHome[0]->houdinv_custom_home_data_latest_product; ?></h2>
                        <div class="title-border mx-auto m-b-35"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="port-body">
            <div class="container">
                <div class="grid">
                    <div class="grid-body row" data-layout="fitRows">
                    <?php
                    foreach($latestProducts as $latestProductsList)
                    {
                      $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
                      $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
                    ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 grid-item lamp">
                            <div class="grid-product">
                                <div class="image bg-lightblue">
                                  <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>">
                                  <?php 
                                    if($getProductImage[0])
                                    {
                                        $setLatestIMageData = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                                    }
                                    else
                                    {
                                        $setLatestIMageData = base_url()."Extra/noPhotoFound.png";
                                    }
                                    ?>
                                        <img src="<?php echo $setLatestIMageData; ?>" alt="Chair">
                                    </a>
                                    <!-- banner -->
                                     <div class="<?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?> out-of-stock <?php  } ?>">
                                         <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?><p>Out of Stock</p><?php } ?>

                                    </div>

                                    <div class="banner__inner banner-style-4">

                                    <?php
                                    if($latestProductsList->houdinv_products_main_quotation == 1)
                                    {
                                    ?>
                                        <span class="model_qute_show quotation_span" data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal">Ask Quotation</span>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <!-- end banner -->
                                <?php
                                if($latestProductsList->houdinv_products_total_stocks <= 0)
                                {
                                if($cartFunction == 1)
                                {
                                    ?>
                                    <div class="addcart">
                                        <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" href="Javascript:;">Add to cart</a>
                                    </div>
                                <?php  }
                                }
                                else
                                {
                                ?>
                                <div class="addcart">
                                        <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" href="Javascript:;">Add to cart</a>
                                    </div>
                                <?php }
                                ?>

                                </div>
                                <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>" class="name">
                                <?php echo substr($latestProductsList->houdin_products_title,0, 25).".." ; ?>
                            </a>
                                <div class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;
<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price;; ?> <?php  } ?></div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>

    <!-- Featured product -->
    <?php
        if($customHome[0]->houdinv_custom_home_data_featured_product)
        {
         ?>
    <section class="py-50">
        <div class="port-title">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 text-center">
                        <h2 class="title"><?php echo $customHome[0]->houdinv_custom_home_data_featured_product; ?></h2>
                        <div class="title-border mx-auto m-b-35"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="port-body">
            <div class="container">
                <div class="grid">
                    <div class="grid-body row" data-layout="fitRows">
                    <?php
                        foreach($featuredProduct as $latestProductsList)
                        {
                        // print_r($latestProductsList);
                        $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
                        $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
                        ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 grid-item lamp">
                            <div class="grid-product">
                                <div class="image bg-lightblue">
                                    <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>">
                                    <?php 
                                    if($getProductImage[0])
                                    {
                                        $setfeturedIMageData = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                                    }
                                    else
                                    {
                                        $setfeturedIMageData = base_url()."Extra/noPhotoFound.png";
                                    }
                                    ?>
                                        <img src="<?php echo $setfeturedIMageData ?>" alt="Chair">
                                    </a>
                                    <!-- banner -->
                                    <div class="<?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?> out-of-stock <?php  } ?>">
                                         <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?><p class="m-b-0">Out of Stock</p><?php } ?>

                                    </div>
                                    <div class="banner__inner banner-style-4">

                                    <?php
                                    if($latestProductsList->houdinv_products_main_quotation == 1)
                                    {
                                    ?>
                                        <span class="model_qute_show quotation_span" data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal">Ask Quotation</span>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <!-- end banner -->
                                <?php
                                if($latestProductsList->houdinv_products_total_stocks <= 0)
                                {

                                if($cartFunction == 1)
                                {
                                    ?>
                                    <div class="addcart">
                                        <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" href="Javascript:;">Add to cart</a>
                                    </div>
                                <?php  }
                                }
                                else
                                {
                                ?>
                                <div class="addcart">
                                        <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" href="Javascript:;">Add to cart</a>
                                    </div>
                                <?php }
                                ?>

                                </div>
                                <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>" class="name">
                                <?php echo substr($latestProductsList->houdin_products_title,0, 25).".." ; ?>
                            </a>
                                <div class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?> <?php  } ?></div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
     <!-- Testimonial -->
     <?php if(count($storeTestimonial) > 0 && $customHome[0]->houdinv_custom_home_data_testimonial)
        {
        ?>
     <section class="testimonial-1">
     <div class="port-title">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 text-center">
                        <h2 class="title text-white"><?php echo $customHome[0]->houdinv_custom_home_data_testimonial; ?></h2>
                        <div class="title-border mx-auto m-b-35"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-lg-offset-3">
                    <div class="testi-slick">
                    <?php
                    foreach($storeTestimonial as $storeTestimonialData)
                    {
                    ?>
                        <div class="testi-body text-center slick-animate animated">
                            <div class="testi-image ">
                                    <img class="testi-img" src="<?php if($storeTestimonialData->testimonials_image) { ?> <?php echo $this->session->userdata('vendorURL')."upload/testimonials/".$storeTestimonialData->testimonials_image; ?> <?php  } else { ?> <?php echo base_url() ?>Extra/apparels/img/testimonial/1.jpg <?php } ?>" style="width:200px!important;height:190px!important;" alt="Person 1">
                            </div>
                            <div class="testi-text">
                                <p class="detail"><?php echo $storeTestimonialData->testimonials_feedback ?></p>
                                <p class="author"><?php echo $storeTestimonialData->testimonials_name ?></p>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <?php } ?>
    <!-- End Tesimonial -->
     <?php
     $getShopName = getfolderName($this->session->userdata('shopName'));
     $this->load->view(''.$getShopName.'/Template/footer') ?>
