<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/settingsidebar.php') ?>

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Company Detail</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li><a href="javascript:void(0);">General</a></li>
                  <li class="active">Company Detail</li>
                  </ol>
                  </div>
                 
                 
            </div>
           <!--END Page-Title -->  
<?php
                                    
            if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
   // print_r($all_data);
    ?>
<?php echo form_open(base_url().'Setting/compnaydetails',array( 'id' => 'ShopInfoForm','enctype'=>'multipart/form-data' ));?>
        <div class="row">
        <div class="col-sm-12 m-t-20">
        <div class="card-box">
          <div class="form-group">
            <label>Shop Title</label>
            <input type="text" class="form-control required_validation_for_shop" value="<?php echo $all_data[0]->houdinv_shop_title; ?>" name="title" />
          </div>
          <div class="form-group">
            <label>Business Name</label>
          <input type="text" class="form-control required_validation_for_shop" value="<?php echo $all_data[0]->houdinv_shop_business_name; ?>"  name="business_name"/>
          </div>
          <div class="form-group">
            <label>Shop Address</label>
          <input type="text" value="<?php echo $all_data[0]->houdinv_shop_address; ?>"  class="form-control required_validation_for_shop" name="shop_address" />
          <p>If you have a physical store apart from this virtual store then add your shop address here</p>
          </div>
          <div class="form-group">
            <label>Shop Contact Info</label>
          <input type="text" value="<?php echo $all_data[0]->houdinv_shop_contact_info; ?>"  class="form-control"  name="shop_Contact" />
          <p>Share your Store's contact information here</p>
          </div>
          <div class="form-group">
            <label>Shop communication email</label>
          <input type="text" class="form-control required_validation_email" value="<?php echo $all_data[0]->houdinv_shop_communication_email; ?>"  name="shop_communication_email" />
          <p style="color: red;" class="messagesd"></p>
          <p>Take the professional communication to a next levelwith your company's business email id. Example- abc@xyz.com</p>
          </div>

          <div class="form-group">
            <label>Shop order email</label>
          <input type="text" class="form-control required_validation_email" value="<?php echo $all_data[0]->houdinv_shop_order_email; ?>"  name="shop_order_email" />
          <p style="color: red;" class="messagesd"></p>
          <p>Give instant solutions to customer's regarding orders and delivery with your shoporder email id. E.g. orders@xyz.com</p>
          </div>
          <div class="form-group">
            <label>Shop customer care email</label>
          <input type="text" class="form-control required_validation_email" value="<?php echo $all_data[0]->houdinv_shop_customer_care_email; ?>"  name="shop_customer_email" />
         <p style="color: red;" class="messagesd"></p>
          <p>Help your customers regarding any product query by providing your customersupport email id here. E.g. support@xyz.com</p>
          </div>
          <div class="form-group">
            <label>Shop PAN</label>
          <input type="text" class="form-control" value="<?php echo $all_data[0]->houdinv_shop_pan; ?>"  name="shop_pan"/>
          <p>Write your physical store's PAN number here (Optional)</p>
          </div>

          <div class="form-group">
            <label>Shop TIN</label>
            <input type="text" name="shop_tin" value="<?php echo $all_data[0]->houdinv_shop_tin; ?>"  class="form-control" />
          </div>
          <!-- <div class="form-group">
            <label>Shop CST</label>
            <input type="text" name="shop_cst" value="<?php echo $all_data[0]->houdinv_shop_cst; ?>"  class="form-control"  />
          </div> -->
          <div class="form-group">
            <label>Shop VAT</label>
            <input type="text" name="shop_vat" value="<?php echo $all_data[0]->houdinv_shop_vat; ?>"  class="form-control"  />
          </div>
          <div class="form-group">
            <label>Vendor Profile Image</label>
            <input type="file" class="form-control" name="name"/>
          </div>
          <!-- <div class="form-group">
            <label>Custom domain</label>
            <input type="text" name="shop_custom_domain" value="<?php echo $all_data[0]->houdinv_shop_custom_domain; ?>"  class="form-control"  />
          </div>
          <div class="form-group">

            <input type="checkbox" value="1" name="ssl" <?php if($all_data[0]->houdinv_shop_ssl==1){echo "checked";} ?> />&nbsp;SSL
          </div> -->


        </div>
        <!--pricing-->

        <!--inventory-->

      </div>
      
        <div class="col-md-12">
          <button type="submit" value="save" name="save" class="btn btn-info">Save</button>
        </div>
        </div>
         <?php echo form_close(); ?>

      </div>
    </div>
  </div>
<!--Delete-->


<?php $this->load->view('Template/footer.php') ?>

    <script type="text/javascript">
    /*==============Form Validation===========================*/
        $(document).ready(function(){
            
            
                            function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
  }

   $(document).on('blur','.required_validation_email',function()
                                {
                                
                                    if(!validateEmail($(this).val()))
                                       {
                                        rep_image_val = 'error form';
                                        $(this).css("border-color","red");
                                        $(this).siblings('.messagesd').text('please fill correct email  address');
                                        }
                                        else
                                        {
                                            
                                        
                                           $(this).css("border-color","#ccc"); 
                                           $(this).siblings('.messagesd').text('');
                                        }
                                });
        $(document).on('submit','#ShopInfoForm',function(c){
            
                               var rep_image_val='';
                 $(this).find(".required_validation_for_shop").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                        
                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
                
                $('.required_validation_for_shop').on('keyup blur',function()
                                {
                                    
                                        $(this).css("border-color","#ccc");
                                });
                                
                                
                        
                
   
                      
                                
                                
                            if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                else
                {

                }  
            
          });
          
          });
          </script>
