<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
        <!--START Page-Title -->
            <div class="row">
        
                <div class="col-md-8">
                   <h4 class="page-title">View Inventory</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Inventory</a></li>
                  <li><a href="<?php echo base_url(); ?>inventory/lowinventory">Low Inventory</a></li>
                 
                  <li class="active">View Inventory</li>
                  </ol>  
                  </div>
                 
            </div>
           <!--END Page-Title -->

           <!-- <div class="row">
                <div class="col-sm-12">
                  <div class="btn-group pull-right m-t-10 m-b-20">
                 
                      <a href="<?php echo base_url(); ?>order/orderinvoicepdf" class="btn btn-default m-r-5" title="Generate invoice"><i class="fa fa-file"></i></a>
                      <a href="<?php echo base_url(); ?>order/printinvoicestatement" class="btn btn-default m-r-5" title="Print invoice"><i class="fa fa-print"></i></a>
                      <button type="button" class="btn btn-default m-r-5" title="Update Status" data-toggle="modal" data-target="#Change-status"><i class="fa fa-toggle-on "></i></button>

                  </div>
                   

                </div>
            </div>-->
            <div class="row">
              <div class="container">
	<div class="row">


        <div class="receipt-main col-xs-12 col-sm-12 col-md-12">

           <div class="col-xs-12 col-sm-12 col-md-13 custom_title_view0inventory">
            <div class="receipt-left text-center" style="padding: 10px;">
              <h5 class="text_white"><b>Inventory ID : <span class="text_lightblue">#12523652</span></b></h5>
              <h5 class="text_white"><b>Order Status : <span class="text_lightblue">Shipped</span></b></h5>
            
            </div>
          </div>
            <div class="row" style="border-bottom: 1px solid #b7b7b7;">
              <div class="col-md-3 text-center" style="border-right: 1px solid #b8b8b8;">
                <div class="inner_p">
    			
          <img class="img_product_view" src="<?php echo base_url(); ?>assets/images/hard_drives.jpg">
           <h4 class="text_black">All About You</h4>
            <p class="text_black">Product Category :Jeans</p>
            <p class="text_black">QTY :5</p>
            
        </div>
      </div>
      <div class="col-md-3 text-center" style="border-right: 1px solid #b8b8b8;">
        <div class="inner_p">
        <img class="img_product_view" src="<?php echo base_url(); ?>assets/images/product.jpeg">
       <h4 class="text_black">All About You</h4>
                  <p class="text_black">Product Category :Jeans</p>
                  <p class="text_black">QTY :5</p>
      </div>
      </div>
       <div class="col-md-3 text-center" style="border-right: 1px solid #b8b8b8;">
        <div class="inner_p">
        <img class="img_product_view" src="<?php echo base_url(); ?>assets/images/hard_drives.jpg">
       <h4 class="text_black">All About You</h4>
                  <p class="text_black">Product Category :Jeans</p>
                  <p class="text_black">QTY :5</p>
      </div>
      </div>
       <div class="col-md-3 text-center">
        <div class="inner_p">
        <img class="img_product_view" src="<?php echo base_url(); ?>assets/images/product.jpeg">
       <h4 class="text_black">All About You</h4>
                  <p class="text_black">Product Category :Jeans</p>
                  <p class="text_black">QTY :5</p>
      </div>
      </div>
         
          </div>

			<div class="row">
				<div class="receipt-header receipt-header-mid" style="padding:0px 20px;">
					<div class="col-xs-8 col-sm-8 col-md-8 text-left">
						<div class="receipt-right">
							<h5>Joen Belly </h5>
							<p><b>Mobile :</b> +91 12345-6789</p>
							<p><b>Email :</b> info@gmail.com</p>
							
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4">
						<div class="receipt-left text-right">
             <p><b>Address :</b><br> Lorem ipsum dolor sit amet,<br>sed do eiusmod</p>
						</div>
					</div>
				</div>
            </div>

            <div>

            </div>

        </div>
	</div>
</div>
        </div>
</div>
</div>
</div>

<!--Confirm model-->
<div id="Confirm_order" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <form method="post">
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Confirm Order</h4>

        </div>

        <div class="modal-body">

        <div class="row">
        <div class="col-md-12">
        <h4><b>Do you really want to Confirm this order ?</b></h4>
        </div>
        </div>
        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info" name="" value="Confirm">

        </div>

        </div>
        </form>
        </div>

        </div>
<!--cancel order-->
<div id="Cancel_order" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <form method="post">
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Cancel Order</h4>

        </div>

        <div class="modal-body">

        <div class="row">
        <div class="col-md-12">
        <h4><b>Do you really want to Cancel this order ?</b></h4>
        </div>
        </div>
        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info" name="" value="Cancel Order">

        </div>

        </div>
        </form>
        </div>

        </div>

<!--change Status-->
<div id="Change-status" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <form method="post">
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Update Status</h4>

        </div>

        <div class="modal-body">



        <div class="row">
        <div class="col-md-12">

        <div class="form-group no-margin">

        <label for="field-7" class="control-label">Change Status</label>

        <select class="form-control " name=""><option value="">Choose Status</option><option value="1">Active</option><option value="0">Deactive</option><option value="2">Block</option></select>

        </div>

        </div>
        </div>

        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Update Status">

        </div>

        </div>
        </form>
        </div>

        </div>

<?php $this->load->view('Template/footer.php') ?>
