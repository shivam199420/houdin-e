<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Campaign extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('Pdf');
        $this->auth_users->is_logged_in();
         $this->load->helper('url');
         $this->load->model('Campaignmodel'); 
    }
    public function index()
    {
        $this->load->view('campaignlanding');
    }
    public function sms()
    { 
        $current_campaign= $this->Campaignmodel->fetch_current_campaign();
        $this->load->view('smscampaign', $current_campaign);

    } 
    public function addsms()
    {

  $Getsmsemail= $this->Campaignmodel->fetch_customerRecord();
        $this->load->view('addsmscampaign',$Getsmsemail);
    }

    public function campaign_addsms()
    {
        //print_r($this->input->post());
        $this->form_validation->set_rules('campaign_name','Campaign Name','required|trim');
        $this->form_validation->set_rules('campaign_status','Campaign  Status Select','required|trim');
        $this->form_validation->set_rules('campaign_text','Campaign message','required'); 

     if($this->form_validation->run() == true)
    {
        $campaign_name = $this->input->post('campaign_name');
        $campaign_status = $this->input->post('campaign_status');
        $campaign_text = $this->input->post('campaign_text');
        $campaign_date = $this->input->post('campaign_date');
        $campaignCustomer = $this->input->post('campaignCustomer');
        $campaignCustomerGroup = $this->input->post('campaignCustomerGroup');
        $setSMSCampaignArray = array('name'=>$campaign_name,'text'=>$campaign_text,'status'=>$campaign_status,'date'=>$campaign_date,'customer'=>$campaignCustomer,'customerGroup'=>$campaignCustomerGroup);
        $getInsertStatus = $this->Campaignmodel->insertSMSCampaign($setSMSCampaignArray);
        if($getInsertStatus['message'] == 'yes')
        {
                $this->session->set_flashdata('success','Campaign addedd suucessfully');               
                redirect(base_url()."Campaign/sms");
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Campaign/campaign_addsms");
        }
      }else{ 
         $this->session->set_flashdata('error',validation_errors());
$Getsmsemail= $this->Campaignmodel->fetch_customerRecord();
        $this->load->view('addsmscampaign',$Getsmsemail);

      } 
  }
    public function campaign_addsms_delete()
      {
          $campaign_name = $this->input->post('hidden_campagian_id');

           $getInsertStatus = $this->Campaignmodel->deleteSMSCampaign($campaign_name);
          if($getInsertStatus){
      $this->session->set_flashdata('success','Campaign Delete suucessfully');               
                redirect(base_url()."Campaign/sms");
          }else{
       $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Campaign/sms");
          } 
      }
       
    public function editsms()
    {
     $edit_Campaign=   $this->uri->segment('3');
     $GetCampaign_edit= $this->Campaignmodel->fetch_CampaignEditRecord($edit_Campaign);
     print_r($GetCampaign_edit);
        $this->load->view('editsmscampaign',$GetCampaign_edit);
    }

   public function campaign_editsms()
   {
       //print_r($this->input->post());
        $this->form_validation->set_rules('campaign_name','Campaign Name','required|trim');
        $this->form_validation->set_rules('campaign_status','Campaign  Status Select','required|trim');
        $this->form_validation->set_rules('campaign_text','Campaign message','required'); 

     if($this->form_validation->run() == true)
    {
        $campaign_name = $this->input->post('campaign_name');
        $campaign_status = $this->input->post('campaign_status');
        $campaign_text = $this->input->post('campaign_text');
        $campaign_date = $this->input->post('campaign_date');
        $campaignCustomer = $this->input->post('campaignCustomer');
        $campaignCustomerGroup = $this->input->post('campaignCustomerGroup');
        $campaignsms_id = $this->input->post('campaignsms_id');
        $setSMSCampaignArray =array('campaignsms_id'=>$campaignsms_id,'name'=>$campaign_name,'text'=>$campaign_text,'status'=>$campaign_status,'date'=>$campaign_date,'customer'=>$campaignCustomer,'customerGroup'=>$campaignCustomerGroup);
        $getInsertStatus = $this->Campaignmodel->updateSMSCampaign($setSMSCampaignArray);
        if($getInsertStatus['message'] == 'yes')
        {
                $this->session->set_flashdata('success','Campaign addedd suucessfully');               
                redirect(base_url()."Campaign/sms");
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Campaign/campaign_addsms");
        }
      }else{ 
         $this->session->set_flashdata('error',validation_errors());
$Getsmsemail= $this->Campaignmodel->fetch_customerRecord();
        $this->load->view('addsmscampaign',$Getsmsemail);

      }  
   }
   public function campaign_multidelete()
   {
    if($this->input->post('multiple_check')){
     $multiple_check_arrya= $this->input->post('multiple_check');

    // print_r($multiple_check_arrya);
      $multiple_check_arryaStatus = $this->Campaignmodel->Delete_multiSMSCampaign($multiple_check_arrya);
       
       $this->session->set_flashdata('success',count($multiple_check_arrya).' Campaign  sucessfully Delete');  
       redirect(base_url()."Campaign/sms");
    }else{

     $this->session->set_flashdata('error','Something went wrong. Please try again');  
    redirect(base_url()."Campaign/sms");
    }
   }

    public function email()
    {
       $current_campaign= $this->Campaignmodel->fetch_current_campaignemail();
        $this->load->view('emailcampaign',$current_campaign);
    }
 public function campaignemail_multidelete()
   {
    if($this->input->post('multiple_check')){
     $multiple_check_arrya= $this->input->post('multiple_check');

    // print_r($multiple_check_arrya);
      $multiple_check_arryaStatus = $this->Campaignmodel->Delete_multiEMAILCampaign($multiple_check_arrya);
       
       $this->session->set_flashdata('success',count($multiple_check_arrya).' Campaign  sucessfully Delete');  
       redirect(base_url()."Campaign/email");
    }else{

     $this->session->set_flashdata('error','Something went wrong. Please try again');  
    redirect(base_url()."Campaign/email");
    }
   }
   public function campaign_addemail_delete()
      {
          $campaign_name = $this->input->post('hidden_campagian_id');

           $getInsertStatus = $this->Campaignmodel->deleteEMAILCampaign($campaign_name);
          if($getInsertStatus){
      $this->session->set_flashdata('success','Campaign Delete suucessfully');               
                redirect(base_url()."Campaign/email");
          }else{
       $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Campaign/email");
          } 
      }
       




    public function addemail()
    { 
      $Getsmsemail= $this->Campaignmodel->fetch_customerRecord();
        $this->load->view('addemailcampaign',$Getsmsemail);
    }
public function campaign_addemail()
    {
        //print_r($this->input->post());
        $this->form_validation->set_rules('campaign_name','Campaign Name','required|trim');
        $this->form_validation->set_rules('campaign_status','Campaign  Status Select','required|trim');
        $this->form_validation->set_rules('campaign_text','Campaign message','required'); 

     if($this->form_validation->run() == true)
    {
        $campaign_name = $this->input->post('campaign_name');
        $campaign_status = $this->input->post('campaign_status');
        $campaign_text = $this->input->post('campaign_text');
        $campaign_date = $this->input->post('campaign_date');
        $campaignCustomer = $this->input->post('campaignCustomer');
        $campaignCustomerGroup = $this->input->post('campaignCustomerGroup');
        $setSMSCampaignArray = array('name'=>$campaign_name,'text'=>$campaign_text,'status'=>$campaign_status,'date'=>$campaign_date,'customer'=>$campaignCustomer,'customerGroup'=>$campaignCustomerGroup);
        $getInsertStatus = $this->Campaignmodel->insertEMAILCampaign($setSMSCampaignArray);
        if($getInsertStatus['message'] == 'yes')
        {
                $this->session->set_flashdata('success','Campaign addedd suucessfully');               
                redirect(base_url()."Campaign/email");
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Campaign/addemail");
        }
      }else{ 
         $this->session->set_flashdata('error',validation_errors());
$Getsmsemail= $this->Campaignmodel->fetch_customerRecord();
        $this->load->view('addemailcampaign',$Getsmsemail);

      } 
  }


    public function editemail()
    {
      $edit_Campaign=   $this->uri->segment('3');
     $GetCampaign_edit= $this->Campaignmodel->fetch_CampaignEdit_emailRecord($edit_Campaign);
        $this->load->view('editemailcampaign',$GetCampaign_edit);
    }

public function campaign_editemail()
   {
       //print_r($this->input->post());
        $this->form_validation->set_rules('campaign_name','Campaign Name','required|trim');
        $this->form_validation->set_rules('campaign_status','Campaign  Status Select','required|trim');
        $this->form_validation->set_rules('campaign_text','Campaign message','required'); 

     if($this->form_validation->run() == true)
    {
        $campaign_name = $this->input->post('campaign_name');
        $campaign_status = $this->input->post('campaign_status');
        $campaign_text = $this->input->post('campaign_text');
        $campaign_date = $this->input->post('campaign_date');
        $campaignCustomer = $this->input->post('campaignCustomer');
        $campaignCustomerGroup = $this->input->post('campaignCustomerGroup');
        $campaignsms_id = $this->input->post('campaignsms_id');
        $setSMSCampaignArray =array('campaignsms_id'=>$campaignsms_id,'name'=>$campaign_name,'text'=>$campaign_text,'status'=>$campaign_status,'date'=>$campaign_date,'customer'=>$campaignCustomer,'customerGroup'=>$campaignCustomerGroup);
        $getInsertStatus = $this->Campaignmodel->updateEMAILCampaign($setSMSCampaignArray);
        if($getInsertStatus['message'] == 'yes')
        {
                $this->session->set_flashdata('success','Campaign Email addedd suucessfully');               
                redirect(base_url()."Campaign/email");
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Campaign/editemail/".$campaignsms_id."/");
        }
      }else{ 
         $this->session->set_flashdata('error',validation_errors());
         redirect(base_url()."Campaign/editemail/".$campaignsms_id."/");

      }  
   }

    public function push()
    {
    $current_campaign_push= $this->Campaignmodel->fetch_current_campaign_push();         
        $this->load->view('pushcampaign', $current_campaign_push);
    }


    public function campaign_push_multidelete()
   {
    if($this->input->post('multiple_check')){
     $multiple_check_arrya= $this->input->post('multiple_check');

    // print_r($multiple_check_arrya);
      $multiple_check_arryaStatus = $this->Campaignmodel->Delete_multiPUSHCampaign($multiple_check_arrya);
       
       $this->session->set_flashdata('success',count($multiple_check_arrya).' Campaign push  sucessfully Delete');  
       redirect(base_url()."Campaign/push");
    }else{

     $this->session->set_flashdata('error','Something went wrong. Please try again');  
    redirect(base_url()."Campaign/push");
    }
   }
    public function campaign_addpush_delete()
      {
          $campaign_name = $this->input->post('hidden_campagian_id');

           $getInsertStatus = $this->Campaignmodel->deletePUSHCampaign($campaign_name);
          if($getInsertStatus){
      $this->session->set_flashdata('success','Campaign Delete suucessfully');               
                redirect(base_url()."Campaign/sms");
          }else{
       $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Campaign/push");
          } 
      }
  

       

    public function addpush()
    {
      $Getsmsemail= $this->Campaignmodel->fetch_customerRecord();
        $this->load->view('addpushcampaign',$Getsmsemail);
    }


public function campaign_addpush()
    {
        //print_r($this->input->post());
        $this->form_validation->set_rules('campaign_name','Campaign Name','required|trim');
        $this->form_validation->set_rules('campaign_status','Campaign  Status Select','required|trim');
        $this->form_validation->set_rules('campaign_text','Campaign message','required'); 

     if($this->form_validation->run() == true)
    {
        $campaign_name = $this->input->post('campaign_name');
        $campaign_status = $this->input->post('campaign_status');
        $campaign_text = $this->input->post('campaign_text');
        $campaign_date = $this->input->post('campaign_date');
        $campaignCustomer = $this->input->post('campaignCustomer');
        $campaignCustomerGroup = $this->input->post('campaignCustomerGroup');
        $setSMSCampaignArray = array('name'=>$campaign_name,'text'=>$campaign_text,'status'=>$campaign_status,'date'=>$campaign_date,'customer'=>$campaignCustomer,'customerGroup'=>$campaignCustomerGroup);
        $getInsertStatus = $this->Campaignmodel->insertPUSHCampaign($setSMSCampaignArray);
        if($getInsertStatus['message'] == 'yes')
        {
                $this->session->set_flashdata('success','Push Campaign  addedd suucessfully');               
                redirect(base_url()."Campaign/push");
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Campaign/addpushcampaign");
        }
      }else{ 
         $this->session->set_flashdata('error',validation_errors());
 
       redirect(base_url()."Campaign/addpushcampaign");
        

      } 
  }

    public function editpush()
    {
      $edit_Campaign=   $this->uri->segment('3');
     $GetCampaign_edit= $this->Campaignmodel->fetch_CampaignEdit_pushRecord($edit_Campaign);
        $this->load->view('editpushcampaign',$GetCampaign_edit);
    }

public function campaign_editpush()
   {
       //print_r($this->input->post());
        $this->form_validation->set_rules('campaign_name','Campaign Name','required|trim');
        $this->form_validation->set_rules('campaign_status','Campaign  Status Select','required|trim');
        $this->form_validation->set_rules('campaign_text','Campaign message','required'); 

     if($this->form_validation->run() == true)
    {
        $campaign_name = $this->input->post('campaign_name');
        $campaign_status = $this->input->post('campaign_status');
        $campaign_text = $this->input->post('campaign_text');
        $campaign_date = $this->input->post('campaign_date');
        $campaignCustomer = $this->input->post('campaignCustomer');
        $campaignCustomerGroup = $this->input->post('campaignCustomerGroup');
        $campaignsms_id = $this->input->post('campaignsms_id');
        $setSMSCampaignArray =array('campaignsms_id'=>$campaignsms_id,'name'=>$campaign_name,'text'=>$campaign_text,'status'=>$campaign_status,'date'=>$campaign_date,'customer'=>$campaignCustomer,'customerGroup'=>$campaignCustomerGroup);
        $getInsertStatus = $this->Campaignmodel->updatePUSHCampaign($setSMSCampaignArray);
        if($getInsertStatus['message'] == 'yes')
        {
                $this->session->set_flashdata('success','Campaign addedd suucessfully');               
                redirect(base_url()."Campaign/push");
        }
        else
        {
           $this->session->set_flashdata('error','Something went wrong. Please try again');             
            redirect(base_url()."Campaign/editpush/".$campaignsms_id."/");
        }
      }else{ 
         $this->session->set_flashdata('error',validation_errors());
         redirect(base_url()."Campaign/editpush/".$campaignsms_id."/");

      }  
   }

    public function generatecampaignpdf()
    {
        // logo dynamic
        $getLogo = fetchvedorsitelogo();
        $url = base_url();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Campaign List');
    $pdf->SetSubject('Houdin-e - Campaign List');
    $pdf->SetKeywords('TCPDF, PDF, Campaign, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

 $current_campaign= $this->Campaignmodel->fetch_current_campaign();
  
$total_campags =$current_campaign['total_campags'];
$total_campagss=$total_campags->total_campag;
$Active_campags =$current_campaign['Active_campag'];
$Active_campagss=$Active_campags->Active_campag;

$Deactive_campags =$current_campaign['Deactive_campag'];
$Deactive_campagss=$Deactive_campags->Deactive_campag;
$table_tbody='';
foreach ($current_campaign['campaignsms'] as $key => $value) {
                      
                      if($value->campaign_status=='1'){
                        $statsu_='<p style="color: green;">Active</p>';
                      }else{
                           $statsu_='<p style="color: red;">Deactive</p>';
                      }
       $table_tbody.='<tr>
 <td style="font-size:12px;font-weight:700;">'.$value->campaign_name.'</td>
   <td style="font-size:12px;font-weight:700;">'.$statsu_.'</td>

       <td style="font-size:12px;font-weight:700;">0</td>

 </tr>';               


}
$html = <<<EOD
<table style="width:100%;">
<tbody>
<tr>

<td style="width:100%;text-align:center;">
<img style="display:block; margin:auto; width:150px;" src="$getLogo" alt="Logo">
</td>

</tr>
</tbody>
</table>

<table>
<tr><td>&nbsp;</td></tr>
</table>

<table style="width:100%;" cellpadding="8" border="1">
<tr>
 <td style="text-align:center;">
 <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
 <span style="font-size:12px;font-weight:bold;">$total_campagss</span><br/>
 <span style="font-size:12px;font-weight:bold; color:#008cb6">Total SMS Campaign </span>

  </td>
  <td style="text-align:center;">
  <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
  <span style="font-size:12px;font-weight:bold;">$Active_campagss</span><br/>
  <span style="font-size:12px; font-weight:bold; color:#008cb6">SMS Campaign Active </span>

   </td>
   <td style="text-align:center;">
   <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
   <span style="font-size:12px;font-weight:bold;">$Deactive_campagss</span><br/>
   <span style="font-size:12px; font-weight:bold; color:#008cb6">SMS Campaign Deactive </span>

    </td>
    </tr>

  </table>


 <table>
 <tr><td>&nbsp;</td></tr>
 </table>
 <table border="1" cellpadding="10">
 <tr>
 <td style="font-size:12px;font-weight:700;" >Campaign name</td>
       <td style="font-size:12px;font-weight:700;">Status</td>
          <td style="font-size:12px;font-weight:700;" >Send count </td>

 </tr>
  $table_tbody
 </table>
 <table>
 <tr><td>&nbsp;</td></tr>
 </table>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_campaign_list.pdf', 'I');
    }



 public function generatecampaignpdf_email()
    {
        $url = base_url();
        // logo dynamic
        $getLogo = fetchvedorsitelogo();

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Campaign List');
    $pdf->SetSubject('Houdin-e - Campaign list');
    $pdf->SetKeywords('TCPDF, PDF, Campaign, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

 $current_campaign= $this->Campaignmodel->fetch_current_campaignemail();
  
$total_campags =$current_campaign['total_campags'];
$total_campagss=$total_campags->total_campag;
$Active_campags =$current_campaign['Active_campag'];
$Active_campagss=$Active_campags->Active_campag;

$Deactive_campags =$current_campaign['Deactive_campag'];
$Deactive_campagss=$Deactive_campags->Deactive_campag;
 
$table_tbody='';
foreach ($current_campaign['campaignsms'] as $key => $value) {
                      
                      if($value->campaign_status=='1'){
                        $statsu_='<p style="color: green;">Active</p>';
                      }else{
                           $statsu_='<p style="color: red;">Deactive</p>';
                      }
       $table_tbody.='<tr>
 <td style="font-size:12px;font-weight:700;">'.$value->campaign_name.'</td>
   <td style="font-size:12px;font-weight:700;">'.$statsu_.'</td>

       <td style="font-size:12px;font-weight:700;">0</td>

 </tr>';               


}
$html = <<<EOD
<table style="width:100%;">
<tbody>
<tr>

<td style="width:100%;text-align:center;">
<img style="display:block; margin:auto; width:150px;" src="$getLogo" alt="Logo">
</td>

</tr>
</tbody>
</table>

<table>
<tr><td>&nbsp;</td></tr>
</table>

<table style="width:100%;" cellpadding="8" border="1">
<tr>
 <td style="text-align:center;">
 <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
 <span style="font-size:12px;font-weight:bold;">$total_campagss</span><br/>
 <span style="font-size:12px;font-weight:bold; color:#008cb6">Total Email Campaign </span>

  </td>
  <td style="text-align:center;">
  <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
  <span style="font-size:12px;font-weight:bold;">$Active_campagss</span><br/>
  <span style="font-size:12px; font-weight:bold; color:#008cb6">Email Campaign Active </span>

   </td>
   <td style="text-align:center;">
   <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
   <span style="font-size:12px;font-weight:bold;">$Deactive_campagss</span><br/>
   <span style="font-size:12px; font-weight:bold; color:#008cb6">Email Campaign Deactive </span>

    </td>
    </tr>

  </table>


 <table>
 <tr><td>&nbsp;</td></tr>
 </table>
 <table border="1" cellpadding="10">
 <tr>
 <td style="font-size:12px;font-weight:700;" >Campaign name</td>
       <td style="font-size:12px;font-weight:700;">Status</td>
          <td style="font-size:12px;font-weight:700;" >Send count </td>

 </tr>
  $table_tbody
 </table>
 <table>
 <tr><td>&nbsp;</td></tr>
 </table>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_campaign_list.pdf', 'I');
    }

 



 public function generatecampaignpdf_push()
    {
        $url = base_url();
        // logo dynamic
        $getLogo = fetchvedorsitelogo();

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Campaign Push Detail');
    $pdf->SetSubject('Houdin-e - Campaign Push Detail');
    $pdf->SetKeywords('TCPDF, PDF, Campaign push, push, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

 $current_campaign= $this->Campaignmodel->fetch_current_campaign_push();

$total_campags =$current_campaign['total_campags'];
$total_campagss=$total_campags->total_campag;
$Active_campags =$current_campaign['Active_campag'];
$Active_campagss=$Active_campags->Active_campag;

$Deactive_campags =$current_campaign['Deactive_campag'];
$Deactive_campagss=$Deactive_campags->Deactive_campag;
$table_tbody='';
foreach ($current_campaign['campaignsms'] as $key => $value) {
                      
                      if($value->campaign_status=='1'){
                        $statsu_='<p style="color: green;">Active</p>';
                      }else{
                           $statsu_='<p style="color: red;">Deactive</p>';
                      }
       $table_tbody.='<tr>
 <td style="font-size:12px;font-weight:700;">'.$value->campaign_name.'</td>
   <td style="font-size:12px;font-weight:700;">'.$statsu_.'</td>

       <td style="font-size:12px;font-weight:700;">0</td>

 </tr>';               


}
$html = <<<EOD
<table style="width:100%;">
<tbody>
<tr>

<td style="width:100%;text-align:center;">
<img style="display:block; margin:auto; width:150px;" src="$getLogo" alt="Logo">
</td>

</tr>
</tbody>
</table>

<table>
<tr><td>&nbsp;</td></tr>
</table>

<table style="width:100%;" cellpadding="8" border="1">
<tr>
 <td style="text-align:center;">
 <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
 <span style="font-size:12px;font-weight:bold;">$total_campagss</span><br/>
 <span style="font-size:12px;font-weight:bold; color:#008cb6">Total Email Campaign </span>

  </td>
  <td style="text-align:center;">
  <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
  <span style="font-size:12px;font-weight:bold;">$Active_campagss</span><br/>
  <span style="font-size:12px; font-weight:bold; color:#008cb6">Email Campaign Active </span>

   </td>
   <td style="text-align:center;">
   <img src="$url/assets/images/Total_Products.png" width="35px;"><br/>
   <span style="font-size:12px;font-weight:bold;">$Deactive_campagss</span><br/>
   <span style="font-size:12px; font-weight:bold; color:#008cb6">Email Campaign Deactive </span>

    </td>
    </tr>

  </table>


 <table>
 <tr><td>&nbsp;</td></tr>
 </table>
 <table border="1" cellpadding="10">
 <tr>
 <td style="font-size:12px;font-weight:700;" >Campaign name</td>
       <td style="font-size:12px;font-weight:700;">Status</td>
          <td style="font-size:12px;font-weight:700;" >Send count </td>

 </tr>
  $table_tbody
 </table>
 <table>
 <tr><td>&nbsp;</td></tr>
 </table>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_campaign_push_list.pdf', 'I');
    }


}

