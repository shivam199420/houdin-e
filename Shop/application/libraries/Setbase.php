<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setbase
{
    function __construct()
	{
        $CI =& get_instance();
        $this->getBaseData();
	}
    function getBaseData()
    {
        $CI =& get_instance();
        $getData = $CI->session->userdata('shopName');
        $masterDB = $CI->load->database('master', TRUE);
        $getDatabse = $masterDB->select('houdin_user_shop_db_name')->from('houdin_user_shops')->where('houdin_user_shops.houdin_user_shop_shop_name',$getData)->get()->result();
        $CI->session->set_userdata('setdatabase',$getDatabse[0]->houdin_user_shop_db_name);
    }
}
?>