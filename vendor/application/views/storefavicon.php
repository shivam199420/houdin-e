<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/storesetting') ?>
<style type="text/css">
  @media screen and (min-width: 768px){}
.dropdown.dropdown-lg .dropdown-menu {
    min-width: 390px!important;
}
.checkbox{
  padding-left: 10px;
}
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 10px;
}
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
  <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">Add Favicon</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li><a href="<?php echo base_url(); ?>storesetting/Storesettinglanding">Store setting</a></li>
                  <li class="active">Add favicon</li>
                  </ol>
                  </div>
                  
            </div>
           <!--END Page-Title -->  
             <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>

         <div class="row m-t-20">
        <div class="col-sm-12">
        <div class="card-box">
        
       <div class="row">
        
     <?php echo form_open(base_url( 'Storesetting/do_upload_favicon' ), array( 'id' => 'logo_form', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
          <div class="row">
      <div class="col-md-12 form-horizontal">
                <div class="form-group">
                   
                  <div class="col-md-9">
                  </div>
                </div> 
                <?php if($storelogos){  
                  if($storelogos[0]->shop_favicon){
          
                 ?>
                <div class="form-group">
                  <label class="col-md-1 control-label">Currently</label>
                  <div class="col-md-9">
                   <img class="custom_logo_choose" src="<?php echo base_url();?>upload/logo/<?=$storelogos[0]->shop_favicon?>">
                  </div>
                </div>

               <div class="col-md-6 checkbox checkbox-custom m-b-10">
                 <input id="checkbox_clear" type="checkbox" value="1" name="checkbox_clear">
                 <label>Clear</label>
              </div>
              <?php  }
                 } ?>
             
              </div>
              <div class="form-group">
                <div class="col-md-12 m-t-10">
                  <label class="col-md-12 control-label">Add Favicon</label>
                  <div class="col-md-12">
                  <input type="file" name="favicon_image" class="filestyle" data-buttonname="btn-white">
                </div><br>
                
                </div>
                <p style="margin-left: 20px"><i class="fa fa-info-circle"></i>  Upload image with equal length and breadth and maximum size upto 32 x 32 px(width x height) for best fit..</p>
              </div>
 
                              
              <div class="col-md-12 m-l-10">
              <input type="submit" class="btn btn-default pull-left m-r-10" name="favicon" value="Save">
              
              </div>
          </div>
                  
        <?php echo form_close(); ?>

        
        </div>
        
            
        </div>
        </div>
        </div>

     

<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
    
    $('#logo_form').submit(function(e){
    e.preventDefault(); 
 

  if($('#checkbox_clear').is(':checked')){
    status=true; 
  }
  else{
     status=''; 
  } 


    urls='<?=base_url();?>'+'Storesetting/do_upload_favicon?clear_check='+status;
         $.ajax({
             url:urls,
             type:"post",
             data:new FormData(this),
             processData:false,
             contentType:false,
             cache:false,
             async:false,
              success: function(data){
                 window.location.reload();
           }
         });
    });  
    
    
    
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>

