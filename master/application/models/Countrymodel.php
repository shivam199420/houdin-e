<?php
class Countrymodel extends CI_Model
{
  function __construct()
    {    parent::__construct();
        $this->countryTbale="houdin_countries";
    }
public function getAllCountry($params=false)
{
        $this->db->select("*")
        ->from($this->countryTbale);
                        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit'],$params['start']);
            }
            elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit']);
            }
                    elseif(array_key_exists("active",$params))
            {
                $this->db->where('houdin_country_status','active');
            }

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
            {
                $result = $this->db->count_all_results();
            }else
            {
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
            
           
            return $result;
                        

        
    }
    
    public function CountryAdd($data=false)
    {
        $this->db->insert($this->countryTbale,$data);
        return true;
    }
    
    public function CountryEdit($data=false,$id)
    {
        $this->db->where("houdin_country_id",$id);
        $this->db->update($this->countryTbale,$data);
        return true;
    }
    public function CountryDelete($data=false)
    {
        $this->db->delete($this->countryTbale,array("houdin_country_id"=>$data));
        return true;
    }
    public function insertSMSPackage($data)
    {
        $getInsertStatus = $this->db->insert('houdin_sms_package',$data);
        if($getInsertStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function fetchSmsPackageData($data=false)
    {
        $this->db->select('*')->from('houdin_sms_package');
        $getSMSPackage = $this->db->get()->result();
        return array('packageList'=>$getSMSPackage);
    }
    public function deleteSMSPackage($data)
    {
        $this->db->where('houdin_sms_package_id',$data);
        $getDeleteStatus = $this->db->delete('houdin_sms_package');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function updateSMSPackage($data,$id)
    {
        $this->db->where('houdin_sms_package_id',$id['houdin_sms_package_id']);
        $getUpdateStatus = $this->db->update('houdin_sms_package',$data);
        if($getUpdateStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function insertEmailPackageData($data)
    {
        $getInsertStatus = $this->db->insert('houdin_email_package',$data);
        if($getInsertStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function fetchEmailPackageDetail()
    {
        $this->db->select('*')->from('houdin_email_package');
        $getEmailPackage = $this->db->get()->result();
        return array('emailPackageList'=>$getEmailPackage);
    }
    public function deleteEmailPackageId($data)
    {
        $this->db->where('houdin_email_package_id',$data);
        $getDeleteStatus = $this->db->delete('houdin_email_package');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function updateEmailPackage($data,$updateId)
    {
        $this->db->where('houdin_email_package_id',$updateId['houdin_email_package_id']);
        $getUpdateStatus = $this->db->update('houdin_email_package',$data);
        if($getUpdateStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
}