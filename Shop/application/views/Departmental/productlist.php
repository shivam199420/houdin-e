<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header") ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>

   <!-- Page Title -->
   <section>
        <div class="container py-85 py-tn-50">
            <div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-12 ">
                            <h2 class="title text-center">Product List</h2>
                             <div class="title-border mx-auto m-b-tn-35 text-center"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </section>

    <!-- End Page Title -->

    <section>
        <div class="container p-t-100 p-b-70">
            <div class="row">
            <div class="col-sm-12">
            <?php
            if($this->session->flashdata('error'))
            {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
            }
            if($this->session->flashdata('success'))
            {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
            }
            ?>
            </div>
                <div class="col-md-12">
                  <?php
                  $count_product=count($productList);
                  if($count_product==0)
                  {?>
                    <div class="container"><div class="col-sm-12 text-center"><h3>Products are not available</h3></div></div>
                  <?php } else { ?>
                    <div class="shop-list">
                        <div class="shop-list-heading">
                            <div class="number-product col-sm-4 line40">
                                Total Results: <?php echo $count; ?>
                            </div>
                            <div class="col-sm-8">

                            <?php echo form_open(base_url( 'Productlist/'.$this->uri->segment('2').'/'.$this->uri->segment('3').'/'.$this->uri->segment('4')), array( 'id' => 'Productlist', 'method'=>'get' ));?>
                            <select class="sort-select form-control pull-right " style="width:25%; font-size:16px" name="sort">
                            <option value="">select</option>
                            <option value="name" <?php if($_REQUEST['sort'] =="name") { echo "selected"; }?> >Name Descending</option>
                            <option value="date" <?php if($_REQUEST['sort'] =="date") { echo "selected"; }?>>Date Descending</option>
                            <option value="pricehl" <?php if($_REQUEST['sort'] =="pricehl") { echo "selected"; }?>>Price High to Low</option>
                            <option value="pricelh" <?php if($_REQUEST['sort'] =="pricelh") { echo "selected"; }?>>Price Low to High</option>
                            </select>
                            <label class="pull-right custom-lable-select">short by:&nbsp;&nbsp;</label>
                            <?php echo form_close(); ?>

                            </div>
                        </div>
                        <div class="shop-list-body shop-grid appendProductList">
                        <?php
                        foreach($productList as $productListData)
                        {
                            $getProductImage = json_decode($productListData->houdinv_products_main_images,true);
                            $getProductPrice = json_decode($productListData->houdin_products_price,true);
                        ?>
                             <div class="col-lg-3 col-md-4 col-sm-6 grid-item lamp">
                            <div class="grid-product">
                                <div class="image bg-lightblue">
                                    <a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>">
                                    <?php 
                                    if($getProductImage[0])
                                    {
                                        $setImageData = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                                    }
                                    else
                                    {
                                        $setImageData = base_url()."Extra/noPhotoFound.png";
                                    }
                                    ?>
                                        <img src="<?php echo $setImageData ?>" alt="Product 1">
                                    </a>
                                    <!-- banner -->
                                    <div <?php if($productListData->houdinv_products_total_stocks <= 0) { ?> class="out-of-stock" <?php } ?> >
                                    	<?php if($productListData->houdinv_products_total_stocks <= 0) { ?><p>Out of Stock</p><?php } ?>
                                    </div>
                                    <div class="banner__inner banner-style-4">

                                    <?php
                                    if($productListData->houdinv_products_main_quotation == 1)
                                    {
                                    ?>
                                        <span class="model_qute_show quotation_span" data-id="<?php echo $productListData->houdin_products_id ?>"  data-toggle="modal">Ask Quotation</span>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <!-- end banner -->
                                <div class="addcart">
                                <?php
                                    if($productListData->houdinv_products_total_stocks <= 0)
                                    {
                                    if($storeSetting[0]->receieve_order_out == 1)
                                    {
                                        ?>
                                        <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>" href="Javascript:;" style="    width: 50%;height: 100%;float: left;display: block;">Add to cart</a>

                                    <?php  }
                                    }
                                    else
                                    {
                                    ?>
                                      <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>" href="Javascript:;" style="    width: 50%;height: 100%;float: left;display: block;">Add to cart</a>
                                    <?php }
                                    ?>

                                    <a class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>" href="Javascript:;" style="    width: 50%;height: 100%;float: left;display: block;border-left:1px solid #333;">wishlist</a>

                                    <!-- <a class="Add_to_cart_button" data-variant="0" data-cart="35" href="Javascript:;">Wishlist</a> -->
                                </div>
                                </div>
                                <div class="product-body">
                                    <a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>" class="name"><?php echo substr($productListData->houdin_products_title,0, 25).".." ; ?></a>
                                    <p class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $productListData->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $productListData->houdin_products_final_price; ?> <?php  } ?></p>
                                </div>
                            </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>


            </div>
        </div>
    </section>
    <section>
        <div class="container p-t-100 p-b-70">
            <div class="row">
            <?php
        if($count > 0)
        {
            if($count >= 12)
            {
                $remain = $count-12;
            }
            else
            {
                $remain = 0;
            }
        ?>
        <?php
        if($remain > 0)
        {
        ?>
        <div class="col-sm-12"><button type="button" data-remain="<?php echo $remain ?>" data-last="12" class="btn btn-sm btn-default acc_btn setLoadMoredata load-more">Load More&nbsp;<i class="fa fa-spinner fa-spin setSpinnerData" style="display:none;"></i></button></div>
        <?php }
        ?>
        <?php }
        ?>
        </div>
        </div>
        </section>

    <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer") ?>
     <script>
        $('.sort-select').on('change',function()
        {
          document.getElementById('Productlist').submit();
        });
        </script>
        <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('click','.setLoadMoredata',function(){
                $(this).prop('disabled',true);
                $('.setSpinnerData').show();
                var getCategory = '<?php echo $this->uri->segment('2') ?>';
                var getSubCategory = '<?php echo  $this->uri->segment('3') ?>';
                var getSubSubCategory = '<?php echo $this->uri->segment('4') ?>';
                var sortBy = '<?php echo $_REQUEST['sort'] ?>';
                var dataLast = $(this).attr('data-last');
                var dataRemain = $(this).attr('data-remain');
                $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>Ajaxcontroller/fetchProductPaginationData",
                data: {"dataLast": dataLast,"dataRemain":dataRemain,"getCategory":getCategory,"getSubCategory":getSubCategory,"getSubSubCategory":getSubSubCategory,"sortBy":sortBy},
                success: function(datas) {
                    $('.setSpinnerData').hide();
                    $('.setLoadMoredata').prop('disabled',false);
                    var setData = jQuery.parseJSON(datas);
                       var grid = "";
                    for(var i=0; i < setData.main.length ; i++)
                    {
                        var $productListData = setData.main[i];
                        var textsplit = $productListData.houdin_products_title;
                        var $setCartDesign = "";
                          var $setStockBadge = "";
                          var $setWishlistButton ="";
                          var $settingData = setData.settingData;
                            if($productListData.houdinv_products_total_stocks <= 0)
                                {
                                    if($settingData[0].receieve_order_out == 1)
                                    {
                                        $setCartDesign = '<a class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'" href="Javascript:;" style="    width: 50%;height: 100%;float: left;display: block;">Add to cart</a>';
                                    }
                                }
                                else
                                {
                                    $setCartDesign = '<a class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'" href="Javascript:;" style="    width: 50%;height: 100%;float: left;display: block;">Add to cart</a>';
                                }
                                $setWishlistButton = ' <a class="Add_to_whishlist_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'" href="Javascript:;" style="    width: 50%;height: 100%;float: left;display: block;border-left:1px solid #333;">wishlist</a>'
                                if($productListData.houdinv_products_total_stocks <= 0)
                                {
                                    $setStockBadge = '<p>Out of Stock</p>';
                                }

                          var   $getProductImage = jQuery.parseJSON($productListData.houdinv_products_main_images);
                           var     $getProductPrice = jQuery.parseJSON($productListData.houdin_products_price);
                                if($getProductPrice.discount != 0 && $getProductPrice.discount  != "")
                                {
                                   var $DiscountedPrice = $getProductPrice.price-$getProductPrice.discount;
                                   var $originalPrice = $getProductPrice.price;
                                }
                                else
                                {
                                  var  $DiscountedPrice = 0;
                                  var  $originalPrice = $getProductPrice.price;
                                }
                                if($DiscountedPrice == 0)
                                {
                                  var  $setPrice = $originalPrice;
                                }
                                else
                                {
                                   var $setPrice = "<strike>"+$originalPrice+"</strike>&nbsp;"+$DiscountedPrice+"";
                                }
                                if($getProductImage[0])
                                {
                                    var setImageData = '<?php echo $this->session->userdata('vendorURL'); ?>upload/productImage/'+$getProductImage[0]+'';
                                }
                                else
                                {
                                    var setImageData = '<?php echo base_url() ?>Extra/noPhotoFound.png';
                                }
                                $setWishlistData = '<a href="javascript:;" class="add-to-wishlist Add_to_whishlist_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'">Add tp wishlist</a>';
                       grid+=' <div class="col-lg-3 col-md-4 col-sm-6 grid-item lamp"><div class="grid-product"><div class="image bg-lightblue">'
                                    +'<a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'"><img src="'+setImageData+'" alt="Product 1"></a>'
                                    +'<div class="banner__inner banner-style-4">'+$setStockBadge+'</div><div class="addcart">'+$setCartDesign+''+$setWishlistButton+'</div></div>'
                                    +'<div class="product-body"><a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'" class="name"><?php echo substr($productListData->houdin_products_title,0, 25).".." ; ?></a>'
                                    +'<p class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;'+$setPrice+'</p>'
                                    +'</div></div></div>';

                    }
                    $('.appendProductList').append(grid);


                    $('.setLoadMoredata').attr('data-last',setData.last);
                }
                });
            });
        });
        </script>
