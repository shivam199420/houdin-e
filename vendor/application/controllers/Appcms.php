<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Appcms extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
         $this->load->library('Pdf');
        $this->auth_users->is_logged_in();
         $this->load->helper('url');
         $this->load->model('Appcmsmodel'); 
    }
    
    public function logo()
    {
        $getlogo['storelogos'] = $this->Appcmsmodel->fetchstorelogoRecord('aaplogo');

        $this->load->view('applogo',$getlogo);
    }



      public function AppCmsLanding()
    {
        $this->load->view('applycmslandingpage');
    }
    
public function do_upload(){

   $alredy_data= $this->Appcmsmodel->fetchstorelogoRecord('aaplogo');

 $applogo_id= $alredy_data[0]->applogo_id;

          if($this->input->post('checkbox_clear')){
               
        $insert_data= array('applogo_id'=>$applogo_id,'applogo_clear' =>1,'date_time'=>strtotime(date('Y-m-d')));

            $file_id = $this->Appcmsmodel->insert_logo($insert_data);
            if($file_id)
            {       
              $this->session->set_flashdata('success','Logo Successfully Add');
            
                redirect(base_url()."Appcms/logo");

            }else{              
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');

                redirect(base_url()."Appcms/logo");
            }
          
            return;
          }
       $File_name='applogo-'.strtotime(date('Y-m-d'));

        $config['upload_path'] = APPPATH . '../upload/applogo'; 
        $config['file_name'] = $File_name;
        $config['overwrite'] = TRUE;
        $config["allowed_types"] = 'jpg|jpeg|png';
        $config["max_size"] = 1024;
        // $config["max_width"] = 400;
        // $config["max_height"] = 400;
        // $config['$min_width'] = 200;
        // $config['min_height'] = 100;
        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('image_logo')) { 
          //$this->data['error'] = $this->upload->display_errors();
          $this->session->set_flashdata('error',$this->upload->display_errors());
          redirect(base_url()."Appcms/logo");
        }else {
         $dataimage_return = $this->upload->data();  
         $insert_data= array('applogo_id'=>$applogo_id,'applogo' =>$dataimage_return['file_name'],'applogo_clear' =>0,'date_time'=>strtotime(date('YmdHis')));

            $file_id = $this->Appcmsmodel->insert_logo($insert_data);
            if($file_id)
            {          
               $this->session->set_flashdata('success','Logo Successfully Add');
               redirect(base_url()."Appcms/logo");
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
                redirect(base_url()."Appcms/logo");
            }
 $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
                redirect(base_url()."Appcms/logo");
                                               
        }  
       // echo "1";


     }


    public function splashscreen()
    {

    $getlogo['storelogos'] = $this->Appcmsmodel->fetchstorelogoRecord('splashscreen');


        $this->load->view('splashscreen',$getlogo);
    }
public function do_upload_splashscreen(){

   $alredy_data= $this->Appcmsmodel->fetchstorelogoRecord('splashscreen');

 $applogo_id= $alredy_data[0]->applogo_id;

          if($this->input->post('checkbox_clear')){
                
        $insert_data= array('applogo_id'=>$applogo_id,'splashscreen_clear' =>1,'date_time'=>strtotime(date('Y-m-d')));

            $file_id = $this->Appcmsmodel->insert_logo_splashscreen($insert_data);
            if($file_id)
            {          
              $this->session->set_flashdata('success','splashscreen Successfully Add');
            
                redirect(base_url()."Appcms/splashscreen");

            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');

                redirect(base_url()."Appcms/splashscreen");
            }
            return;
          }
       $File_name='splashscreen-'.strtotime(date('Y-m-d'));

        $config['upload_path'] = APPPATH . '../upload/applogo'; 
        $config['file_name'] = $File_name;
        $config['overwrite'] = TRUE;
        $config["allowed_types"] = 'jpg|jpeg|png|gif';
        // $config["max_size"] = 1024;
        // $config["max_width"] = 400;
        // $config["max_height"] = 400;
        // $config['$min_width'] = 200;
        // $config['min_height'] = 100;
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('image_logo')) { 

          //$this->data['error'] = $this->upload->display_errors();
          $this->session->set_flashdata('error',$this->upload->display_errors());
          redirect(base_url()."Appcms/splashscreen");
        } else {
         $dataimage_return = $this->upload->data();  
         $insert_data= array('applogo_id'=>$applogo_id,'splashscreen' =>$dataimage_return['file_name'],'splashscreen_clear' =>0,'date_time'=>strtotime(date('YmdHis')));

            $file_id = $this->Appcmsmodel->insert_logo_splashscreen($insert_data);
            if($file_id)
            {          
               $this->session->set_flashdata('success','Splashscreen Successfully Add');
               redirect(base_url()."Appcms/splashscreen");
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
                redirect(base_url()."Appcms/splashscreen");
            }

 $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
                redirect(base_url()."Appcms/splashscreen");
                                               
        }  
    


     }




    public function storesetting()
    {
        $this->load->view('appstoresetting');
    }
}
