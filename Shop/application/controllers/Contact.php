<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contact extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopusercontact');
        $getSubDomain = checkShopUrl();
        $this->to = toemail();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $getShopInfo = $this->Shopusercontact->fetchShopBasicDetails();
        $getShopInfo['Template']=$this->Daynamic_templat;
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view($getShopName.'/contact',$getShopInfo);
    }
    public function addcontact()
    {
        $this->form_validation->set_rules('userName','Name','required');
        $this->form_validation->set_rules('userEmail','Email','required|valid_email');
        $this->form_validation->set_rules('userEmailSub','Subject','required');
        $this->form_validation->set_rules('userEmailBody','Message','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('houdinv_shop_query_name'=>$this->input->post('userName'),'houdinv_shop_query_email'=>$this->input->post('userEmail'),'houdinv_shop_query_subject'=>$this->input->post('userEmailSub'),'houdinv_shop_query_message'=>$this->input->post('userEmailBody'));
            $getStatus = $this->Shopusercontact->insertCustomerQuery($setArray);
            if($getStatus['message'] == 'yes')
            {
                $getEmailCount = fetchEmailCount($this->session->userdata('shopName'));
                if($getEmailCount[0]->houdinv_emailsms_stats_remaining_credits > 0)
                {
                    $url = 'https://api.sendgrid.com/';
                    $user = 'anuankita';
                    $pass = 'india@123';
                    $json_string = array(
                    'to' => array(
                        $this->to
                    ),
                    'category' => 'test_category'
                    );
                    $htm = '
                    <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                    <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                    <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                    <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                    <img src="'.base_url().'assets/images/main-logo.png" style="max-width: 53%;width:100%"/></td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                    <strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">User Name:</strong></td>
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                    <strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">'.$this->input->post('userName').'</strong></td>
                    </tr>
    
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                    <strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">User Email:</strong></td>
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                    <strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">'.$this->input->post('userEmail').'</strong></td>
                    </tr>
    
    
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                    <strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">User Query:</strong></td>
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                    <strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">'.$this->input->post('userEmailBody').'</strong></td>
                    </tr>
    
    
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$getForgotStatus['token'].'</td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                    <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                    <table width="100%" ><tr >
                    <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e 2018--All right reserverd </td></tr>
                    <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
                    </tr></table>
                    </div>
                    </div></td></tr></table>';
                    $params = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'x-smtpapi' => json_encode($json_string),
                        'to'        => $this->to,
                        'fromname'  => $this->input->post('userName'),
                        'subject'   => $this->input->post('userEmailSub'),
                        'html'      => $htm,
                        'from'      => $this->input->post('userEmail'),
                    );
                    $request =  $url.'api/mail.send.json';
                    $session = curl_init($request);
                    curl_setopt ($session, CURLOPT_POST, true);
                    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                    curl_setopt($session, CURLOPT_HEADER, false);
                    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($session);
                    curl_close($session);
                    $getDecodeData = json_decode($response,true);
                    if($getDecodeData['message'] == 'success')
                    {
                        $getRemaining = $getEmailCount[0]->houdinv_emailsms_stats_remaining_credits-1;
                        updateSuceessEmailCount($getRemaining,$this->session->userdata('shopName'));
                        $this->session->set_flashdata('success','Your query is successfully submite. We will revert you back soon.');
                        redirect(base_url()."Contact", 'refresh');
                    }
                    else
                    {
                        updateErrorEmailLog($this->session->userdata('shopName'));
                        $this->session->set_flashdata('success','Your query is successfully submite. We will revert you back soon.');
                        redirect(base_url()."Contact", 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('success','Your query is successfully submite. We will revert you back soon.');
                    redirect(base_url()."Contact", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url()."Contact", 'refresh');    
            }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."Contact", 'refresh');
        }
    }
}
