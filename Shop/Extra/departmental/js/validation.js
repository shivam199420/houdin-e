
$(".number_validation").keydown(function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
    (e.keyCode >= 35 && e.keyCode <= 40)) {
    return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
    }
    });
    $('.characterValidation').keypress(function(key) {
        if((key.charCode < 97 || key.charCode > 122) && (key.charCode < 65 || key.charCode > 90) && (key.charCode != 45)) return false;
        });
    // Name validation
    $(document).on('keydown', '.name_validation', function(e) {
    if (e.which === 32 &&  e.target.selectionStart === 0) {return false;}  });
    //Email Validation
    function ValidateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
    };
    jQuery(".email_validation").blur(function () {
    if (!ValidateEmail(jQuery(this).val())) {
    jQuery(this).val("");
    }
    else {
    return  true;
    }
    });
    $(document).on('keyup','.cpass',function(){
        if($(this).val() == $('.opass').val())
        {
            $(this).css('border','1px solid #d3d3d3');
            $('.setDisableData').attr('disabled',false);
        }
        else
        {
            $(this).css('border','1px solid red');
            $('.setDisableData').attr('disabled',true);
        }
    });
    $(document).on('keyup','.opass',function(){
        if($(this).val() == $('.cpass').val())
        {
            $('.setDisableData').attr('disabled',false);
        }
        else
        {
            $('.setDisableData').attr('disabled',true);
        }
    });


    // add to whishlist

    $(".Add_to_whishlist_button").on("click",function()
    {
        var whishlist_item = $(this).attr("data-cart");
        var variantItem = $(this).attr("data-variant");

       if(!whishlist_item)
       {
        alert("Something went wrong ! try again");
       }
       else
       {
        var userAuth = My_variable;
        if(!userAuth)
        {
            window.location.href=base_url+"/Register";
        }
        else
        {
           $.ajax({
                type: "POST",
                url: base_url+"Wishlist/AddToWhishlist",
                data: {"whishlist_item": whishlist_item,"variantItem":variantItem},
                success: function(data) {

                }
                });
        }
       }


    });

    // remove from wishlist
    $(".cp_Wish_remove").on("click",function()
    {
      if($(this).attr('data-id'))
      {
        var that = $(this);
             $.ajax({
                type: "POST",
                url: base_url+"Wishlist/RemoveFromWhishlist",
                data: {"whishlist_item": that.attr('data-id')},
                success: function(data) {
                    that.parents(".tr_row").remove();

                }
                });
      }
    });

   // remove from cart
    $(document).on("click",".cp_cart_remove",function()
    {
      var currencyData = "";
      if(setCurrency == 'USD')
      {
          currencyData = '$';
      }else if(currencyData=="AUD"){
        currencyData = "$";
      }else if(currencyData=="Euro"){
        currencyData = "£";
      }else if(currencyData=="Pound"){
        currencyData = "€";
      }else if(currencyData=="INR"){
        currencyData = "₹";
      }
      if($(this).attr('data-id'))
      {
           var that = $(this);
        var  data_id = that.attr('data-id');
     var  data_replace = that.attr('data-replace');

             $.ajax({
                type: "POST",
                url: base_url+"Cart/RemoveFromCart",
                data: {"cart_item": that.attr('data-id')},
                success: function(datas) {
                console.log(datas);
                if(that.parents(".cart_menu_area"))
                {
                     $(".cart_prdct_table").find(".cp_cart_remove[data-id='"+data_id+"']").parents(".tr_row").remove();
                }
               if($(".mcart_subtotal_class").html())
               {
                   var subtotl_replace = $("#mcart_subtotal_class").text();
                 subtotl_replace =  subtotl_replace.match(/[\d\.]+/g);

                var change =  subtotl_replace- data_replace;


                 $(".mcart_subtotal_class").text("Rs "+change);
               }

                  var html ='';
                   var $final_price =0;
                   var allcart =0;
                 if(datas)
                 {
                    that.parents(".tr_row").remove();

                         var allData = $.parseJSON(datas);

                    allcart = allData.count;

                   $.each(allData.cart,function(index,value)
                   {
                        
                    var $image = value.productImage ;


                    var $count = value.count;
                    var $main_price = value.productprice;

                   var  $total_price = $main_price*$count;
                  $final_price =$final_price+$total_price;
                  var setProductTitle = value.productName;
                  html+='<li class="item"><div class="item-image"><img src="https://houdine.com/vendor/upload/productImage/'+$image+'" style="width:100%" alt="Item 1">'
                  +'</div><div class="item-detail"><p class="name">'+setProductTitle.substr(1,12)+'</p><p class="price">'+currencyData+''+$main_price+'</p>'
                  +'<p class="amount">x'+$count+'</p></div><span class="pro-del cp_cart_remove remove"  data-id="'+value.houdinv_users_cart_id+'"></span>'
                  +'</li>'
                   });

                   $(".shop-cart-button").find(".amount").text(allcart);
                   $(".shop-cart").find(".sub-total").html(' '+currencyData+' '+$final_price);
                     $(".shop-cart__list").html('').html(html);
                     $(".shop-total-body").find('.sub-total-span').html('').html(' '+currencyData+' '+$final_price);

                  }
                  else
                  {
                    window.location.reload();
                  }

                }
                });
      }
    });

        // add to cart
    $(document).on('click','.Add_to_cart_button',function()
    {
      var currencyData = "";
      if(setCurrency == 'USD')
      {
          currencyData = '$';
      }else if(currencyData=="AUD"){
        currencyData = "$";
      }else if(currencyData=="Euro"){
        currencyData = "£";
      }else if(currencyData=="Pound"){
        currencyData = "€";
      }else if(currencyData=="INR"){
        currencyData = "₹";
      }
        var cart_item = $(this).attr("data-cart");
        var variantItem = $(this).attr("data-variant");
        var quantity = $(".setCartQunatity").val();

       if(!cart_item)
       {
        alert("Something went wrong ! try again");
       }
       else
       {
        var userAuth = My_variable;
        if(!userAuth)
        {
            window.location.href=base_url+"/Register";
        }
        else
        {
           $.ajax({
                type: "POST",
                url: base_url+"Cart/AddToCart",
                data: {"cart_item": cart_item,"variantItem":variantItem,"quantity":quantity},
                success: function(datas) {

                   var allData = $.parseJSON(datas);
                   var allcart = allData.count;
                   console.log("total"+allcart);
                   var html ='';
                   var $final_price =0;
                   $.each(allData.cart,function(index,value)
                   {

                    var $image = value.productImage ;


             var $count = value.count;
             var $main_price = value.productprice;

            var  $total_price = $main_price*$count;
           $final_price =$final_price+$total_price;
           var setProductTitle = value.productName;

                    html+='<li class="item"><div class="item-image"><img src="https://houdine.com/vendor/upload/productImage/'+$image+'" style="width:100%" alt="Item 1">'
                    +'</div><div class="item-detail"><p class="name">'+setProductTitle.substr(1,12)+'</p><p class="price">'+currencyData+''+$main_price+' </p>'
                    +'<p class="amount">x'+$count+'</p></div><span class="pro-del cp_cart_remove remove"  data-id="'+value.houdinv_users_cart_id+'"></span>'
                    +'</li>'



                   });
             $(".shop-cart-button").find(".amount").text(allcart);
             $(".shop-cart").find(".sub-total").html(' '+currencyData+' '+$final_price);

               $(".shop-cart__list").html('').html(html);
                }
                });
        }
       }


    });


    $(".update_cart").on("click",function()
    {
       document.getElementById('UpdateCart').submit();
     //   $("#UpdateCart").submit();

    });



    $(".submit_form_checkout").on("click",function(c)
    {


                               var rep_image_val='';
            if($(".billing_form").is(":visible"))
            {
                 $(this).parents('#UpdateCheckout').find(".required_validation_for_checkout").each(function()
               {



                        var val22 = jQuery(this).val();

                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");


                        }
                });
            }
            else
            {

                        if($('input[name=billing_radio]:checked').length<=0)
                        {
                            alert("please select the billing address");
                        rep_image_val = 'error form';


                        }
            }
    if($('input[name=billing_checkbox]:checked').length>0)
                        {

              if($(".fill_Shipping").is(":visible"))
            {
                  $(this).parents('#UpdateCheckout').find(".required_validation_for_checkout1").each(function()
               {

                        var val2 = jQuery(this).val();

                        if (!val2)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");


                        }
                });


                }
                else
                {
                      if($('input[name=shipping_radio]:checked').length<=0)
                        {
                            alert("please select the shipping address");
                        rep_image_val = 'error form';


                        }

                }

                        }

                $('.required_validation_for_checkout,.required_validation_for_checkout1').on('keyup blur change',function()
                                {

                                        $(this).css("border-color","#ccc");
                                        $(this).siblings('.message_text').text('');
                                });

                  if($('input[name=customRadio]:checked').length<=0)
                        {
                            alert("please select the any payment method");
                        rep_image_val = 'error form';
                                $(this).css("border-color","red");

                        }

                          if($('input[name=order_delivery_type]:checked').length<=0)
                        {
                            alert("please select the any delivery type method");
                        rep_image_val = 'error form';
                                $(this).css("border-color","red");

                        }
                if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                else
                {
                    document.getElementById('UpdateCheckout').submit();
                }




          });



       $("#UpdateCheckout").find("#customCheck1").on("change",function()
       {

        if($(this).is(":checked"))
        {
            $('.shipping_form').css("display","block");
        }
        else
        {
            $('.shipping_form').css("display","none");
        }



       });

       var already_coupon = [];
       $(document).on("click",".checkout_coupon_apply",function()
       {
         var thats =$(this);
        var coupon_code = $(this).siblings(".coupon_text").val();
       if(coupon_code)
        {
            console.log(already_coupon);
            console.log(coupon_code);
            console.log($.inArray(coupon_code,already_coupon));
            if($.inArray(coupon_code,already_coupon)>-1)
            {
              $(".coupon_msg_text").text("You can use coupon code only single time");
               $(".coupon_msg_text1").text("");

            }
            else
            {



                 $.ajax({
                type: "POST",
                url: base_url+"Checkout/CheckoutCouponValidationCheck",
                data: {"coupon_code": coupon_code},
                success: function(datas) {

                  var datas = JSON.parse(datas);
                   console.log(datas);
                   if(datas.code==200)
                   {
                    thats.siblings(".coupon_text").val('');
                       already_coupon.push(coupon_code);
                    var percent_off = datas.amount;
                    var all_discount = $(".your-order-table").find(".Discount_Span").text();

                      if(datas.valid_for == "all")
                      {
                        var deliver_Amount = $(".your-order-table").find(".shipping_Span").text();
                        var deduct_amount = $(".your-order-table").find(".amount_span").text();
                        var percent_Amount = (percent_off/100)*(deduct_amount - deliver_Amount);

                        var final_amount_Deduce =  deduct_amount - percent_Amount;
                        $(".your-order-table").find(".amount_span").text(final_amount_Deduce);
                        $(".your-order-table").find(".Discount_Span").text(all_discount+percent_Amount);
                        $(".dont_remove_if_Want_discount_on").val("all");
                          $(".dont_remove_if_Want_discount_discount").val(all_discount+percent_Amount);
                            $(".dont_remove_if_Want_discount_id").val(datas.coupon_id);
                      }
                      else
                      {
                        var deliver_Amount = $(".your-order-table").find(".shipping_Span").text();
                        var product_id = datas.product_id;
                        var deduct_amount = $(".your-order-table").find(".product_total_"+product_id+"").text();
                      //  alert(deduct_amount);
                        var percent_Amount = (percent_off/100)*deduct_amount;
                        var final_amount_Deduce =  deduct_amount - percent_Amount;
                      //  alert(final_amount_Deduce);
                         var total_amount = $(".your-order-table").find(".amount_span").text();
                         var sub_total = $(".your-order-table").find(".subtotal_Span").text();
                        $(".your-order-table").find(".product_total_"+product_id).text(final_amount_Deduce);
                         $(".your-order-table").find(".amount_span").text(total_amount-percent_Amount);
                        $(".your-order-table").find(".Discount_Span").text(all_discount+percent_Amount);
                         $(".your-order-table").find(".subtotal_Span").text(sub_total-percent_Amount);
                         $(".dont_remove_if_Want_discount_on").val("product");
                          $(".dont_remove_if_Want_discount_discount").val(all_discount+percent_Amount);
                            $(".dont_remove_if_Want_discount_id").val(datas.coupon_id);
                      }
                      $(".coupon_msg_text1").text("Coupon applied");
                       $(".coupon_msg_text").text("");
                   }
                   else
                   {
                    $(".coupon_msg_text1").text("");
                    $(".coupon_msg_text").text(datas.msg);
                   }

                    }
                    });

            }
        }
        else
        {
            alert("please fill the coupon code first");
        }

       });


   $(document).on("click",".billing_Change",function()
   {
    console.log("dfdgg");
    $(".billing_already").css("display","none");
    $(".billing_form").css("display","block");
     $("input:radio[class^=billing_already_radio]").each(function(i) {

	      this.checked = false;
			});
   });

   $(document).on("click",".shipping_Change",function()
   {

    $(".shipping_already").css("display","none");
    $(".fill_Shipping").css("display","block");
     $("input:radio[class^=shipping_already_radio]").each(function(i) {

	      this.checked = false;
			});
   });




      $(document).on("click",".billing_Change1",function()
   {
    console.log("dfdgg");
    $(".billing_already").css("display","block");
    $(".billing_form").css("display","none");

   });

   $(document).on("click",".shipping_Change1",function()
   {

    $(".shipping_already").css("display","block");
    $(".fill_Shipping").css("display","none");

   });


       $(document).on('click','.strat-rating .fa-star',function(){

        $(this).nextAll('.fa-star').removeClass('checked');
         $(this).addClass('checked').prevAll('.fa-star').addClass('checked');
         if($(this).attr('data-rating')){
         $(this).parents('.strat-rating').find('input.rating_data').val($(this).attr('data-rating'));
         }
    });



    $(".submit_form_review").on("click",function(c)
    {


                               var rep_image_val='';

                 $(this).parents('#rtng_form').find(".required_validation_for_review").each(function()
               {



                        var val22 = jQuery(this).val();

                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");


                        }
                });



                $('.required_validation_for_review').on('keyup blur change',function()
                                {

                                        $(this).css("border-color","#ccc");
                                        $(this).siblings('.message_text').text('');
                                });



                if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                else
                {

                }




          });



 $(document).on("click",".model_qute_show",function()
 {
    $("#add_Quotation_data").modal("show");
    $("#product_quete_id").val($(this).attr("data-id"));
 });
          $(document).on('submit','#add_Quotation_dataform',function(c){


                               var rep_image_val='';
                 $(this).find(".require_quote").each(function()
               {

                        var val22 = jQuery(this).val();

                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");


                        }
                });



                $('.require_quote').on('keyup blur change',function()
                                {

                                        $(this).css("border-color","#ccc");
                                        $(this).siblings('.message_text').text('');
                                });



                if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                else
                {

                }




          });
