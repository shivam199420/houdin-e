<?php
class Couponsmodel extends CI_Model
{
  function __construct()

    {

        parent::__construct();
        $this->CouponTbale="houdin_coupons";
    }
    

    public function getAllCoupon($params=false)
    {
        $this->db->select("*")
        ->from($this->CouponTbale);
                        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit'],$params['start']);
            }
            elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit']);
            }

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
            {
                $result = $this->db->count_all_results();
            }else
            {
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
            
           
            return $result;
                        

        
    }
    
    public function CouponAdd($data=false)
    {
        $this->db->insert($this->CouponTbale,$data);
        return true;
    }
    
    public function CouponEdit($data=false,$id)
    {
        $this->db->where("houdin_coupon_id",$id);
        $this->db->update($this->CouponTbale,$data);
        return true;
    }
    public function CouponDelete($data=false)
    {
        $this->db->delete($this->CouponTbale,array("houdin_coupon_id"=>$data));
        return true;
    }
    
    public function CouponActive($data=false)
    {
        $this->db->select("*")
        ->from($this->CouponTbale)->where("houdin_coupon_status","active");
        
        $result = $this->db->count_all_results();
        return $result;
    }
    
    public function CouponExpired($data=false)
    {
        $this->db->select("*")
        ->from($this->CouponTbale)->where("houdin_coupon_valid_to<",strtotime(date("Y-m-d")));
        $result = $this->db->count_all_results();
        return $result;
    }
}