<?php $this->load->view("Bakery/Template/header");
$images_all = json_decode($product['main_product']->houdinv_products_main_images);
$price_all = json_decode($product['main_product']->houdin_products_price,true);
$final_price = $product['main_product']->houdin_products_final_price;
$overall_rating = $product['reviews_sum']->houdinv_product_review_rating;
$total_rating = count($product['reviews']);
$final_rating = $overall_rating/$total_rating;
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
if($currencysymbol=="USD")
{
  $setCurrencySymbol = "$";
}else if($currencysymbol=="AUD"){
  $setCurrencySymbol = "$";
}else if($currencysymbol=="Euro"){
  $setCurrencySymbol = "£";
}else if($currencysymbol=="Pound"){
  $setCurrencySymbol = "€";
}else if($currencysymbol=="INR"){
  $setCurrencySymbol = "₹";
}
?>
<style>
.checked {
  color: orange;
}

</style>

<div id="back2top"><i class="fa fa-angle-up"></i></div>

<div class="loader"></div>

<div class="page-wrap">

  <!-- Heros-->

  <div class="ps-section--hero"><img src="<?php echo base_url(); ?>Extra/Uploads/02.jpg" alt="">

    <div class="ps-section__content text-center">

      <h3 class="ps-section__title"><?php echo $product['main_product']->houdin_products_title; ?></h3>

      <div class="ps-breadcrumb">

        <ol class="breadcrumb">

          <li><a href="<?php echo base_url(); ?>">Home</a></li>

          <li><a href="<?php echo base_url(); ?>Productlist">Shop</a></li>

          <li class="active"><?php echo $product['main_product']->houdin_products_title; ?></li>

        </ol>

      </div>

    </div>

  </div>

  <div class="ps-section pt-80 pb-80">

    <div class="container">

      <div class="ps-product--detail">
        <div class="row">
          <div class="container">
            <div class="col-sm-12">
              <?php
              if($this->session->flashdata('error'))
              {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
              }
              if($this->session->flashdata('success'))
              {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
              }
              ?>
            </div>
          </div>
        </div>
        <div class="row">

          <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 ">

            <div class="ps-product__thumbnail">
              <?php if($product['main_product']->houdinv_products_total_stocks <= 0) { ?>
                <div class="ps-badge" style="width: 86px;height: 86px;"><span>Out of Stock</span></div>

              <?php } ?>
              <?php
              if($product['main_product']->houdinv_products_main_quotation == 1)
              {
                ?>
                <div class="ps-badge ps-badge--new acc_btn model_qute_show" style="width: 86px;height: 86px;" data-id="<?php echo $product['main_product']->houdin_products_id ?>">
                  <span style="text-align: center;">Ask Quotation</span></div>
                  <?php
                }
                ?>

                <div class="owl-slider primary" data-owl-auto="true" data-owl-loop="false" data-owl-speed="10000" data-owl-gap="0" data-owl-nav="false" data-owl-dots="false" data-owl-animate-in="" data-owl-animate-out="" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-nav-left="&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;" data-owl-nav-right="&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;">

                  <?php

                  foreach($images_all as $imagesd)
                  {
                    if($imagesd)
                    {


                      ?>

                      <div class="ps-product__image"><a href="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $imagesd; ?>"><img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $imagesd; ?>" alt=""></a></div>
                      <?php
                    }
                  }?>

                </div>

                <div class="owl-slider second mb-30" data-owl-auto="true" data-owl-loop="false" data-owl-speed="10000" data-owl-gap="20" data-owl-nav="false" data-owl-dots="false" data-owl-animate-in="" data-owl-animate-out="" data-owl-item="4" data-owl-item-xs="2" data-owl-item-sm="3" data-owl-item-md="4" data-owl-item-lg="4" data-owl-nav-left="&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;" data-owl-nav-right="&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;">
                  <?php

                  foreach($images_all as $imagesd)
                  {
                    if($imagesd)
                    {


                      ?>
                      <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $imagesd; ?>" alt="">


                      <?php
                    }
                  }?>


                </div>

              </div>

            </div>

            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 ">

              <header>

                <h3 class="ps-product__name"><?php echo $product['main_product']->houdin_products_title; ?></h3>

                <?php for($i=1 ; $i<6 ;$i++)
                {
                  if($i <= $final_rating)
                  {
                    $posi = strpos($final_rating,".");
                    if($posi>=0)
                    {
                      $exp =  explode(".",$final_rating);
                      if($exp[0]==$i)
                      {
                        ?>
                        <i class="fa fa-star-half-o"></i>
                        <?php
                      }
                      else
                      {
                        echo '<i class="fa fa-star"></i>';
                      }
                    }
                    else
                    {
                      ?>
                      <i class="fa fa-star"></i>
                      <?php
                    }
                  }
                  else
                  {
                    ?>

                    <i class="fa fa-star-o"></i>


                  <?php } } ?>

                  <p class="ps-product__price"><?php echo $setCurrencySymbol."".$final_price; ?> <del><?php echo $setCurrencySymbol."".$price_all['sale_price'];  ?></del></p><a class="ps-product__quickview popup-modal" href="#quickview-modal" data-effect="mfp-zoom-out">QUICK OVERVIEW</a>

                  <div class="ps-product__description">

                    <p><?php echo $product['main_product']->houdin_products_short_desc; ?></p>

                  </div>

                  <div class="ps-product__meta">

                    <p><span> Availability: </span>
                      <?php if($product['main_product']->houdinv_products_total_stocks <= 0) { ?>
                        out of stock
                      <?php }
                      else
                      {
                        ?>
                        In stock
                        <?php
                      } ?></p>

                      
                    </div>

                    <div class="form-group ps-product__size">

                    <!--  <label>Size:</label>-->

<?php if(!empty($product['related'])){?> 
<select style="margin-bottom: 36px;width: 50%;" title="Pick a number" class="form-control variantsselect">
<option>Select variants </option>
<?php 
foreach($product['related'] as $related){?>
<option value="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>"><?php echo $related->houdinv_products_variants_name; ?></option>
<?php } ?>
</select>
<?php  }?> 

                    </div>

                    <div class="ps-product__shop">

                      <div class="form-group--number">

                        <button class="minus"><span>-</span></button>

                        <input class="form-control cart-plus-minus-box setCartQunatity" type="number" value="1" min="1" data-max="<?php echo $product['main_product']->houdinv_products_total_stocks ?>" name="qttybutton">

                        <button class="plus"><span>+</span></button>

                      </div>

                      <ul class="ps-product__action">

                        <?php
                        if($product['setWishlistStatus'] == 'yes')
                        {
                          ?>
                            <li data-variant="0" data-cart="<?php echo $product['main_product']->houdin_products_id ?>"><a href="#" data-tooltip="Already in wishlist"><i class="ps-icon--heart" style="color:red;"></i></a></li>
                        <?php }
                        else {
                          ?>
                          <li class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $product['main_product']->houdin_products_id ?>"><a href="#" data-tooltip="Add to wishlist"><i class="ps-icon--heart"></i></a></li>
                        <?php }
                        ?>

                        <?php
                        if($product['setcartStatus'] == 'yes')
                        {
                          ?>
                          <li data-variant="0" data-cart="<?php echo $product['main_product']->houdin_products_id ?>"><a href="#" data-tooltip="Already in cart"><i class="ps-icon--shopping-cart" style="color:red;" ></i></a></li>
                        <?php }
                        else {
                          ?>
                          <li class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $product['main_product']->houdin_products_id ?>"><a href="#" data-tooltip="Add to cart"><i class="ps-icon--shopping-cart"></i></a></li>
                        <?php }
                        ?>

                      </ul>

                    </div>

                  </header>

                </div>

              </div>

              <div class="ps-product__info">

                <ul class="tab-list" role="tablist">

                  <li class="active"><a href="#tab_01" aria-controls="tab_01" role="tab" data-toggle="tab">Overview</a></li>

                  <li><a href="#tab_02" aria-controls="tab_02" role="tab" data-toggle="tab">Review</a></li>

                  <!-- <li><a href="#tab_03" aria-controls="tab_03" role="tab" data-toggle="tab">PRODUCT TAG</a></li> -->

                  <!-- <li><a href="#tab_04" aria-controls="tab_04" role="tab" data-toggle="tab">ADDITIONAL</a></li> -->

                </ul>

              </div>

              <div class="tab-content mb-60">

                <div class="tab-pane active" role="tabpanel" id="tab_01">

                  <p><?php echo $product['main_product']->houdin_products_desc; ?>	</p>

                </div>

                <div class="tab-pane" role="tabpanel" id="tab_02">

                  <p><?php echo $total_rating; ?> review for <?php echo $product['main_product']->houdin_products_title; ?></p>


                  <?php
                  print_R($user_Review);
                  foreach($product['reviews'] as $user_Review)
                  {
                    ?>
                    <div class="ps-review-box">

                      <div class="ps-review-box__thumbnail">
                        <img src="<?php echo base_url(); ?>/Extra/Uploads/user-dummy-pic.png" alt=""></div>

                        <div class="ps-review-box__content">

                          <header>

                            <select class="ps-rating">
                              <?php for($i=1 ; $i<6 ;$i++)
                              {
                                if($i <= $user_Review->houdinv_product_review_rating)
                                {


                                  ?>
                                  <option value="1"><?php echo $i; ?></option>
                                  <?php
                                }
                                else
                                {
                                  ?>

                                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } } ?>
                              </select>

                              <p>By<a href="#"> <?php echo $user_Review->houdinv_product_review_user_name ?></a> - <?php echo $user_Review->houdinv_product_review_created_at ?></p>

                            </header>

                            <p><?php echo  $user_Review->houdinv_product_review_message; ?></p>

                          </div>

                        </div>
                        <?php
                      }
                      ?>
                      <?php echo form_open(base_url( 'Productlistview' ), array( 'id' => 'rtng_form',"class"=>"checkout_form", 'method'=>'post' ));?>

                      <h4>ADD YOUR REVIEW</h4>

                      <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">

                          <div class="form-group">

                            <label>Name:*<span>*</span></label>

                            <input class="form-control required_validation_for_review name_validation"  name="user_name" type="text" placeholder="">

                          </div>

                          <div class="form-group">

                            <label>Email: *<span>*</span></label>

                            <input class="form-control required_validation_for_review email_validation" name="user_email" type="email" placeholder="">

                          </div>

                          <div class="form-group">

                            <label>Your rating<span></span></label>

                            <div class="strat-rating" style="padding: 25px;">
                              <span data-rating="1" style="font-size: 22px;" class="fa fa-star "></span>
                              <span data-rating="2" style="font-size: 22px;" class="fa fa-star "></span>
                              <span data-rating="3" style="font-size: 22px;" class="fa fa-star "></span>
                              <span data-rating="4" style="font-size: 22px;" class="fa fa-star "></span>
                              <span data-rating="5" style="font-size: 22px;" class="fa fa-star"></span>
                              <input type="hidden"  name="rating" class="rating_data" value="0" />
                              <input type="hidden"  name="product_id" class="product" value="<?php echo $product_id; ?>" />
                            </div>

                          </div>

                        </div>

                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 ">

                          <div class="form-group">

                            <label>Your Review:</label>

                            <textarea class="form-control required_validation_for_review" name="user_message" rows="6"></textarea>

                          </div>

                          <div class="form-group">

                            <button class="ps-btn ps-btn--sm submit_form_review" name="review_submit" value="Add Review">Submit</button>

                          </div>

                        </div>

                      </div>

                      <?php echo form_close(); ?>

                    </div>

                    <div class="tab-pane" role="tabpanel" id="tab_03">

                      <p>Add your tag <span> *</span></p>

                      <form class="ps-product__tags" action="http://nouthemes.com/html/bakery/_action" method="post">

                        <div class="form-group">

                          <input class="form-control" type="text" placeholder="">

                          <button class="ps-btn ps-btn--sm">Add Tag</button>

                        </div>

                      </form>

                    </div>

                    <div class="tab-pane" role="tabpanel" id="tab_04">

                      <div class="form-group">

                        <textarea class="form-control" rows="6" placeholder="Enter your addition here..."></textarea>

                      </div>

                      <div class="form-group">

                        <button class="ps-btn" type="button">Submit</button>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>


            <!-- Related Product Area -->
            <?php
            if(count($product['related']) > 0)
            {
              ?>


              <section class="ps-section ps-section--best-seller pt-40 pb-100">

                <div class="container">

                  <div class="ps-section__header text-center mb-50">

                    <h4 class="ps-section__top"><?php echo $product['main_product']->houdin_products_title; ?></h4>

                    <h3 class="ps-section__title ps-section__title--full">Related products</h3>

                  </div>

                  <div class="ps-section__content">

                    <div class="owl-slider owl-slider--best-seller" data-owl-auto="true" data-owl-loop="true" data-owl-speed="100000" data-owl-gap="30" data-owl-nav="true" data-owl-dots="false" data-owl-animate-in="" data-owl-animate-out="" data-owl-item="4" data-owl-item-xs="1" data-owl-item-sm="2" data-owl-item-md="3" data-owl-item-lg="4" data-owl-nav-left="&lt;i class=&quot;ps-icon--back&quot;&gt;&lt;/i&gt;" data-owl-nav-right="&lt;i class=&quot;ps-icon--next&quot;&gt;&lt;/i&gt;">

                      <?php foreach($product['related'] as $related)
                      {
                        ?>

                        <div class="ps-product">

                          <div class="ps-product__thumbnail">
                            <a class="ps-product__overlay" href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>"></a>
                            <?php 
                            if($related->houdin_products_variants_image)
                            {
                                $setVariantIMageData = $this->session->userdata('vendorURL')."upload/productImage/".$related->houdin_products_variants_image;
                            }
                            else
                            {
                                $setVariantIMageData = base_url()."Extra/noPhotoFound.png";
                            }
                            ?>
                            <img style="width: 253px!important;
                            height: 310px!important;" src="<?php echo $setVariantIMageData; ?>" alt="" />

                            <ul class="ps-product__action">

                              <li><a class="popup-modal" href="<?php echo base_url() ?>Productlistview/<?php echo $related->houdin_products_variants_id; ?>" data-effect="mfp-zoom-out" data-tooltip="View"><i class="ps-icon--search"></i></a></li>

                              <li class="Add_to_whishlist_button" data-variant="<?php echo $related->houdin_products_variants_id ?>"  data-cart="0"><a href="#" data-tooltip="Add to wishlist"><i class="ps-icon--heart"></i></a></li>



                              <li class="Add_to_cart_button" data-variant="<?php echo $related->houdin_products_variants_id ?>"  data-cart="0"><a href="#" data-tooltip="Add to cart"><i class="ps-icon--shopping-cart"></i></a></li>

                            </ul>

                          </div>

                          <div class="ps-product__content">
                            <a class="ps-product__title" href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>"><?php echo $related->houdin_products_variants_title; ?></a>




                            <p class="ps-product__price"> <?php echo $setCurrencySymbol."".$related->houdinv_products_variants_final_price; ?></p>

                          </div>

                        </div>
                        <?php
                      }?>


                    </div>

                  </div>

                </div>

              </section>
            <?php } ?>

            <?php $this->load->view("Bakery/Template/footer"); ?>

            <script type="text/javascript">
            $(document).ready(function(){
              $(document).on('keyup','.setCartQunatity',function(){

                if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
                {
                  console.log("changedsf");
                  $(this).val($(this).attr('data-max'));
                }
              })
              $(document).on('change','.setCartQunatity',function(){
                if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
                {
                  $(this).val($(this).attr('data-max'));
                }
              });

              $(document).on("click",".plus",function()
              {
                var val =   parseInt($('.setCartQunatity').val())+1;

                if(parseInt(val) > parseInt($('.setCartQunatity').attr('data-max')))
                {
                  val = $('.setCartQunatity').attr('data-max');
                }
                $('.setCartQunatity').val(val);
              });


              $(document).on("click",".minus",function()
              {
                var val1 =   $('.setCartQunatity').val()-1;
                if(val1<0)
                {
                  val1 = 0;
                }
                $('.setCartQunatity').val(val1);
              });


              $('.variantsselect').on('change', function() {
            var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
});

            });
            </script>
