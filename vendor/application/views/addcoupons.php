            <?php $this->load->view('Template/header.php') ?>
            <?php $this->load->view('Template/sidebar.php') ?>


            <div class="content-page">
            <!-- Start content -->
            <div class="content">
            <div class="container">
            <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Create A New Coupon Code</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <!--<li><a href="javascript:void(0);">Coupons</a></li>-->
                  <li><a href="<?php echo base_url(); ?>Coupons">Coupons</a></li>
                  <li class="active">Create A New Coupon Code</li>
                  </ol>
                  </div>
            
            </div>
           <!--END Page-Title --> 

            <div class="row">
            <?php 
            if($this->session->flashdata('success'))
            {
              echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
            }
            if($this->session->flashdata('error'))
            {
              echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
            }
            ?>
            </div>
            <div class="row m-t-20">
            <div class="col-lg-12">
            <div class="card-box">
            <?php echo form_open(base_url( 'Coupons/add' ), array( 'id' => 'addCouponsForm', 'method'=>'post' ));?>
            <div class="form-group">
            <label for="userName">Name</label>
            <input type="text" placeholder="Coupon Name" name="couponName" maxlength="50" class="form-control required_validation_for_add_coupons name_validation">
            <p>Give a title to your deal</p>
            </div>
            <div class="form-group">
            <label for="userName">Tagline</label>
            <input type="text" placeholder="Coupon Tagline" name="couponTagline" maxlength="50" class="form-control required_validation_for_add_coupons name_validation">

            </div>
            <div class="form-group">
            <label for="userName">Valid From Date</label>
            <input type="text" placeholder="Valid From" name="couponValidFrom" class="form-control date_picker required_validation_for_add_coupons">
            <p>Format: YYYY-MM-DD</p>
            </div>
            <div class="form-group">
            <label for="userName">Valid Until Date</label>
            <input type="text" placeholder="Valid To" name="couponValidTo" class="form-control date_picker required_validation_for_add_coupons name_validation">
            <p>Format: YYYY-MM-DD</p> 
            </div>
            <div class="form-group">
            <label for="userName">Minimum Eligible Order Amount</label>
            <input type="text" name="minOrderAmount" placeholder="Minimum Order Amount" class="form-control name_validation number_validation">
            <p>Minimum order total for this promotion discount to be applicable. Leave this field empty if you want to discount orders of any amount.</p>
            </div>
            <div class="form-group">
            <label for="userName" style="width: 100%;">Promo code<label class="pull-right"><input type="checkbox" class="genearatePromoCodeCoupons">&nbsp;Generate Promocode</label></label>
            <input type="text" maxlength="6" name="couponCode" placeholder="Enter Promo code" class="form-control setPromocodeCoupons  name_validation">
            <p>Promo code</p> 
            </div>
            <div class="form-group">
            <label for="userName">Discount(%)</label>
            <input type="text" name="couponDiscount" min="1" max="100" class="form-control required_validation_for_add_coupons name_validation number_validation" placeholder="Discount(%)"/>
            <p>Fix a flat discount percentage based discount</p>
            </div>
            <div class="form-group">
            <label for="userName">Customers</label>
            <select class="form-control select2" name="couponCustomer[]" multiple="">
            <option value="">Choose Customers</option>
            <?php 
            foreach($customerList as $customerListData)
            {
             ?> 
             <option value="<?php echo $customerListData->houdinv_user_id ?>"><?php echo $customerListData->houdinv_user_name ?></option>  
           <?php }
            ?>
            </select>
            </div>
            <div class="form-group">
            <label for="userName">Customer Group</label>
            <?php 
            if($this->uri->segment('3') == 'customergroup')
            {
            ?>
            <select class="form-control" name="couponCustomerGroup[]">
            <option value="<?php echo $this->uri->segment('5') ?>"><?php echo $this->uri->segment('4') ?></option>
            </select>
            <?php }
            else
            {
            ?>
            <select class="form-control select2" name="couponCustomerGroup[]" multiple="">
            <option value="">Choose Customer Group</option>
            <?php 
            foreach($customerGroupLits as $customerGroupLitsData)
            {
             ?> 
             <option value="<?php echo $customerGroupLitsData->houdinv_users_group_id ?>"><?php echo $customerGroupLitsData->houdinv_users_group_name ?></option>  
           <?php }
            ?>
            </select>
            <?php }
            ?>
            </div>

            <div class="form-group">
            <label for="userName">Order payment method</label>
            <select class="form-control required_validation_for_add_coupons" name="couponPaymentMethod">
            <option value="">Choose Payment Method</option>
            <option value="both">Both</option>
            <option value="cod">COD</option>
            <option value="online payment">Online Payment</option>
            </select>

            </div>

            <div class="form-group">
            <label for="userName">Eligible categories</label>
            <select multiple="" class="form-control select2" name="couponProductCategory[]">
            <option>Choose Product Categories</option>
            <?php 
            foreach($categoryList as $categoryListData)
            {
            ?>    
            <option value="<?php echo $categoryListData->houdinv_category_id ?>"><?php echo $categoryListData->houdinv_category_name ?></option>
            <?php }
            ?>
            </select>
            </div>
            <div class="form-group">
            <label for="userName">Eligible products</label>
            <select multiple="" class="form-control select2" name="couponProducts[]">

           <option>Choose Product</option>
            <?php 
            foreach($productList as $productListData)
            {
            ?>    
            <option value="<?php echo $productListData->houdin_products_id ?>"><?php echo $productListData->houdin_products_title ?></option>
            <?php }
            ?>
            </select>
            </div>
            <div class="form-group">
            <label for="userName">Count allowed</label>
            <input type="text" placeholder="Number of Usage" name="couponUsage" class="form-control required_validation_for_add_coupons number_validation">
            <p>Number of times the code can be used. Leave it blank if there is no such restriction.</p> 
            </div>
            <div class="form-group">
            <label for="userName">Choose Status</label>
            <select class="form-control required_validation_for_add_coupons" name="couponStatus">
            <option value="">Choose Status</option>
            <option value="active">Active</option>
            <option value="deactive">Deactive</option>
            </select>
            <p>Choose status of your promo code</p> 
            </div>
            <div class="form-group">
            <div class="checkbox" style="padding-left: 0px!important">
            <input id="remember-1" name="couponApplicableonapp" type="checkbox">
            <label for="remember-1">  Applicable only on app?  </label>
            <p>Check this if you have, your store's app from Houdin-e and want the promo code to be applicable only on the app</p>
            </div>
            </div>
            <div class="form-group m-b-0">
            <input type="submit" class="btn btn-primary" name="addCouponsData" value="Add Coupons"/>
            </div>
            <?php echo form_close(); ?>
            </div>
            </div>
            </div>
            <?php $this->load->view('Template/footer') ?>
            <script>
        $(document).ready(function(){
        $(".select2").select2();

        });
        </script>
        <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addCouponsForm',function(){
			var check_required_field='';
			$(".required_validation_for_add_coupons").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>