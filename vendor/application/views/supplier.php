        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">

          <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">Supplier</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li class="active">Supplier</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                 
            </div>
           <!--END Page-Title --> 
        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        else if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>
        <div class="row m-t-20">
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Supplier.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($totalSuppliers) { echo $totalSuppliers; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total Supplier</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Supplier.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($activeSuppliers) { echo $activeSuppliers; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total active supplier</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Supplier.png">
        <h2 class="m-0 text-dark counter font-600"><?php if($deactiveSuppliers) { echo $deactiveSuppliers; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total deactive supplier</div>
        </div>
        </div>
        </div>
        
        <div class="row">
        <div class="col-md-12">

        <div class="card-box table-responsive"> 
        <div class="btn-group pull-right m-t-10 m-b-20">

        <a style="float: left;margin-right: 3px;" href="<?php echo base_url(); ?>supplier/addsupplier" class="btn btn-default m-r-5" title="Add Tag"><i class="fa fa-plus"></i></a>
        <button type="button" class="btn btn-default m-r-5 setSupplierMultiBtn chnageMultipleStatusData" title="Chnage Status" style="display:none"><i class="fa fa-toggle-on "></i></button>
        <button type="button" class="btn btn-default m-r-5 setSupplierMultiBtn deleteMultipleSupplier" title="Delete" style="display:none"><i class="fa fa-trash"></i></button>
        <!--<a href="<?php //echo base_url(); ?>supplier/viewsupplier" class="btn btn-default m-r-5" title="Add Tag"><i class="fa fa-eye"></i></a>-->
        <!-- <a href="<?php //echo base_url(); ?>supplier/supplierlist" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a> -->

        </div>
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th><input type="checkbox" class="masterSupplierCheck"></th>
        <th>Company Name</th>
        <th>Contact Person</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Status</th>
        <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        foreach($supplierList as $supplierListData)
        {
        ?>
        <tr>
        <td><input class="childSupplierCheck" data-id="<?php echo $supplierListData->houdinv_supplier_id ?>" type="checkbox"></td>
        <td><a href="<?php echo base_url(); ?>Supplier/viewsupplier/<?php echo $supplierListData->houdinv_supplier_id; ?>"><?php echo $supplierListData->houdinv_supplier_comapany_name ?></a></td>
        <td><?php echo $supplierListData->houdinv_supplier_contact_person_name ?></td>
        <td><?php echo $supplierListData->houdinv_supplier_email ?></td> 
        <td><?php echo $supplierListData->houdinv_supplier_contact ?> </td>
        <td><?php echo $supplierListData->houdinv_supplier_address ?></td>
        <td><?php if($supplierListData->houdinv_supplier_active_status == 'active') { $setClass='success';$setText='Active'; }
        else if($supplierListData->houdinv_supplier_active_status == 'deactive') { $setClass='warning';$setText='Deactive'; }
        ?>
        <button type="button" class="btn btn-<?php echo $setClass ?> btn-xs"><?php echo $setText; ?></buuton>
        </td>
        <td><a href="<?php echo base_url(); ?>supplier/editsupplier/<?php echo $supplierListData->houdinv_supplier_id; ?>"><button class="btn btn-success waves-effect waves-light" >Edit</button></a>
        <button data-id="<?php echo $supplierListData->houdinv_supplier_id ?>" class="btn btn-success waves-effect waves-light deleteSupplierBtn">Delete</button>
        <!-- <a href="<?php //echo base_url(); ?>supplier/tagsupplier/<?php //echo $supplierListData->houdinv_supplier_id ?>"><button class="btn btn-success waves-effect waves-light" >Add Tag</button></a> -->
        
        <button data-email="<?php echo $supplierListData->houdinv_supplier_email ?>" class="btn btn-success waves-effect waves-light sendEmailSupplierBtn">Send Email</button>
        
        <button data-id="<?php echo $supplierListData->houdinv_supplier_id ?>" data-status="<?php echo $supplierListData->houdinv_supplier_active_status ?>" class="btn btn-success waves-effect waves-light updateSupplierStatus">Change status</button>
        </td>
        </tr>
        <?php }
        ?>
        </tbody>
        </table>
        <!-- <ul class="pagination pull-right"> <?php  //echo $this->pagination->create_links(); ?> </ul> -->
        </div>
        </div>
        </div>

        </div>
        </div>
        </div>

        <div id="statusModalData" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <?php echo form_open(base_url( 'Supplier' ), array( 'method'=>'post', 'id'=>'changeSingleStatus' ));?>
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Add Inventory</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group"> 
        <label for="field-1" class="control-label">Choose Status</label> 
        <input type="hidden" class="changeStatusSupplierId" name="changeStatusSupplierId"/>
        <select class="form-control required_validation_for_form_data supplierStatus" name="supplierStatus">
        <option value="">Choose Status</option>
        <option value='active'>Active</option>
        <option value='deactive'>Deactive</option>
        </select>
        </div> 
        </div> 
        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <input type="submit" class="btn btn-info" value="Update Status" name="updateSingleSupplierStatus"/>
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- /.modal -->
        <!--Delete-->

        <div id="deletSupplierModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Supplier' ), array( 'method'=>'post' ));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title"></h4>

        </div>

        <div class="modal-body">

        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleteSupplierId" name="deleteSupplierId"/>
        <h4><b>Do you really want to Delete this customer ?</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info" name="deleteSuppplier" value="Delete">

        </div>

        </div>
        <?php echo form_close(); ?>
        </div>

        </div>
         <!---Start Send -->
         <div id="sendEmailModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <?php echo form_open(base_url( 'Supplier' ), array( 'method'=>'post', 'id'=>'sendEmailForm' ));?>
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Send Email</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12 form-horizontal"> 
        <input type="hidden" class="supplierEmailData" name="supplierEmailData"/>
        <div class="form-group">
        <label class="col-md-3 control-label">Subject</label>
        <div class="col-md-9">
        <input type="text" class="form-control required_validation_for_form_data_email_value name_validation" name="emailSubject" placeholder="Enter Subject">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-3 control-label">Message</label>
        <div class="col-md-9">
        <textarea type="text" class="form-control setEditor required_validation_for_form_data_email_value" name="emailMessage" placeholder="Your Message"></textarea>
        </div>
        </div>  
        </div> 
        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <input type="submit" class="btn btn-info" name="sendSupplierEmail" value="Send Email"/>
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- End Send -->
        <!---Start Change Status -->
        <div id="changeMultipleststatus" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <?php echo form_open(base_url( 'Supplier' ), array( 'method'=>'post', 'id'=>'updateMultipleStatusForm' ));?>
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Change Status</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group"> 
        <label for="field-1" class="control-label">Choose Status</label> 
        <input type="hidden" class="changeStatusMultipleSupplierId" name="changeStatusMultipleSupplierId"/>
        <select class="form-control required_validation_for_form_data_multiple_status_value" name="multipleSupplierStatus">
        <option value="">Choose Status</option>
        <option value='active'>Active</option>
        <option value='deactive'>Deactive</option>
        </select>
        </div> 
        </div> 
        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <input type="submit" class="btn btn-info" name="updateMultipleStatusData" value="Update Status"/>
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- End Status -->
        <!-- Delete multiple suppplier  -->
        <div id="deletMultipleSupplierModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Supplier' ), array( 'method'=>'post' ));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title"></h4>

        </div>

        <div class="modal-body">

        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleteMultipleSupplierId" name="deleteMultipleSupplierId"/>
        <h4><b>Do you really want to Delete this customer ?</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info" name="deleteMultipleSuppplier" value="Delete">

        </div>

        </div>
        <?php echo form_close(); ?>
        </div>

        </div>
        <!-- End Here -->
        <!---Start Tag -->
        <!-- <div id="tag" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Add Tag</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12 form-horizontal"> 
        <div class="form-group">
        <label class="col-md-3 control-label">Add Tag</label>
        <div class="col-md-9">
        <input type="text" value="" class="form-control" name="" placeholder="Add Tag">
        </div>
        </div>

        </div> 

        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
        </div> 
        </div> 
        </div>
        </div> -->
        <!-- End Tag -->
        <!--change Status-->
        <!-- <div id="Change-status" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <form method="post">
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Change Status</h4>

        </div>

        <div class="modal-body">



        <div class="row">
        <div class="col-md-12">

        <div class="form-group no-margin">

        

        <select class="form-control " name=""><option value="">Choose Status</option><option value="1">Active</option><option value="0">Deactive</option><option value="2">Block</option></select>

        </div>

        </div>
        </div>





        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Update Status">

        </div>

        </div>
        </form>
        </div>

        </div>  -->

        <!-- <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Add purchase order</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-6"> 
        <div class="form-group"> 
        <label for="field-1" class="control-label">Select Product</label> 
        <select class="form-control">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
        </select>
        </div> 
        </div> 
        <div class="col-md-6"> 
        <div class="form-group"> 
        <label for="field-2" class="control-label">Select Supplier</label> 
        <select class="form-control">
        <option>Supplier1</option>
        <option>Supplier2</option>
        <option>Supplier3</option>
        <option>Supplier4</option>
        <option>Supplier5</option>
        </select> 
        </div> 
        </div> 
        </div> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group"> 
        <label for="field-3" class="control-label">Quantity</label> 
        <input type="text" class="form-control" id="field-3" placeholder="Quantity"> 
        </div> 
        </div> 
        </div> 
        <div class="row"> 
        <div class="col-md-6"> 
        <div class="form-group"> 
        <label for="field-4" class="control-label">Delivery date</label> 
        <input type="text" class="form-control date_picker" id="field-4" placeholder="Delivery date"> 
        </div> 
        </div> 
        <div class="col-md-6"> 
        <div class="form-group"> 
        <label for="field-5" class="control-label">Credit period</label> 
        <input type="text" class="form-control" id="field-5" placeholder="Credit period"> 
        </div> 
        </div> 

        </div> 
         
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
        </div> 
        </div> 
        </div>
        </div> -->
        <!-- /.modal -->

        <?php $this->load->view('Template/footer.php') ?>
        <script>
        $(document).ready(function(){
        $(".click_show").click(function(){
        $(".hide_div").show();
        $(".click_show").hide();
        });
        $(".click_hide").click(function(){
        $(".hide_div").hide();
        $(".click_show").show();
        });
        });
        </script>
        <!-- CLient side form validation -->
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#changeSingleStatus',function(){
			var check_required_field='';
			$(".required_validation_for_form_data").each(function(){
                                var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>
        <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#sendEmailForm',function(){
			var check_required_field='';
			$(".required_validation_for_form_data_email_value").each(function(){
                                var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>
        <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#updateMultipleStatusForm',function(){
			var check_required_field='';
			$(".required_validation_for_form_data_multiple_status_value").each(function(){
                                var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>
