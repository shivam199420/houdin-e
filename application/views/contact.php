<?php  $this->load->view('Template/header');?> 

<header class="header bg-gray header-page-top">
  <div class="header-page-top-inner">
	  <div class="container">
		<h1 class="display-4">Contact</h1> 
  </div>
	</div>
</header>
<!-- 
	<section class="section py-0">
        <div class="row no-gutters">  
          <div class="col-md-12">
             <div class="h-100" style="min-height: 350px" data-provide="map" data-lat="44.540" data-lng="-78.556" data-marker-lat="44.540" data-marker-lng="-78.556" data-info="&lt;strong&gt;Our office&lt;/strong&gt;&lt;br&gt;3652 Seventh Avenue, Los Angeles, CA" data-style="light"></div> 
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3558.4848003017632!2d75.8004693150444!3d26.888105083138207!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396db42edba3e5eb%3A0xb062a4bccdeb1bd6!2sHawksCode+Softwares+Pvt.+Ltd!5e0!3m2!1sen!2sin!4v1542085970164" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
      </section> -->


      <div class="row" style="margin:20px 20px; background-color:#5079ad;">
        <div class="col-md-6">
          <img src="<?php echo base_url() ?>assets/images/Contactus-vector.png" alt="contact image">
        </div>
        <div class="col-md-6">
        <section class="section py-8">
        <div class="container text-center"> 

          <form class="row gap-y" id="contactForm" action="" method="POST">
            <div class="col-lg-6" style="margin: 0 auto;">
              <?php 
              if($this->session->flashdata('success'))
              {
                echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
              }
              if($this->session->flashdata('error'))
              {
                echo "<div type='button' class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
              }
              ?>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <input class="form-control form-control-lg required_validation_for_contact name_validation" type="text" name="firstname" placeholder="First Name">
                </div>

                <div class="form-group col-md-6">
                  <input class="form-control form-control-lg required_validation_for_contact name_validation" type="text" name="lastname" placeholder="Last Name">
                </div>

                <div class="form-group col-md-6">
                  <input class="form-control form-control-lg required_validation_for_contact email_validation name_validation" type="email" name="email" placeholder="Email">
                </div>

                <div class="form-group col-md-6">
                  <input class="form-control form-control-lg required_validation_for_contact name_validation" type="text" name="phone" placeholder="Phone">
                </div>
              </div>

              <div class="form-group">
                <textarea class="form-control form-control-lg required_validation_for_contact name_validation" rows="4" placeholder="What do you have in mind?" name="message"></textarea>
              </div>
              <input  class="btn btn-lg btn-primary-1 btn-round" name="sendContactMessage" type="submit" class="Send message"/>
            </div>
             

            <!-- <div class="col-lg-5 ml-auto text-center text-lg-left">
              <hr class="d-lg-none">
              <h5>Seattle, WA</h5>
              <p>2651 Main Street, Suit 124<br>Seattle, WA, 1534</p>
              <p>+1 (321) 654 2222<br>+1 (987) 123 1111</p>
              <p>contact@houdine.com</p> 
            </div> -->
          </form>

        </div>

      </section> 
        
        </div>
      </div>

  

 

  


<?php  $this->load->view('Template/footer');?>


<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#contactForm',function(){
			var check_required_field=0;
			$(".required_validation_for_contact").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field++;
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field != 0)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>