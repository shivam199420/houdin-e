<?php $this->load->view('Bakery/Template/header'); ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 

 <div id="back2top"><i class="fa fa-angle-up"></i></div>

    <div class="loader"></div>

    <div class="page-wrap">

      <!--section-->

      <div class="ps-section--hero"><img src="<?php echo base_url(); ?>Extra/Uploads/02.jpg" alt="">

        <div class="ps-section__content text-center">

          <h3 class="ps-section__title">My Wishlist</h3>

          <div class="ps-breadcrumb">

            <ol class="breadcrumb">

              <li><a href="<?php echo base_url(); ?>">Home</a></li>

              <li class="active">Wishlist</li>

            </ol>

          </div>

        </div>

      </div>

      <div class="ps-section--cart pt-100 pb-100">

        <div class="container">

          <div class="ps-cart-listing">

            <p class="hidden-lg"><i>Slide right to view</i></p>

            <div class="table-responsive">

              <table class="table table-bordered">

                <thead>

                  <tr>

                    <th>All Products</th>

                    <th>Price</th>



                    	<th class="add-cart">add to cart</th>

                    <th></th>

                  </tr>

                </thead>

                <tbody>
          		<?php

						// print_r($wishproducts);

                        $i=1;

                        foreach($wishproducts as $thisItem)
                        {

                          $image = json_Decode($thisItem->houdinv_products_main_images,true);
                          $stock = $thisItem->houdinv_products_total_stocks;
                          $price = json_DEcode($thisItem->houdin_products_price,true);
                          if($stock>0)
                          {
                            $status = "In stock";
                          }
                          else
                          {
                            $status = "Out of stock";
                          }
                        ?>
                  <tr class="tr_row">

                    <td>

                      <div class="">
                      <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $image[0]; ?>" alt="" style="height: 80px;width: 80px;display: block;">
                      <a href="<?php echo base_url() ?>Productlistview/<?php echo $thisItem->houdin_products_id ?>"><?php echo $thisItem->houdin_products_title; ?></a></div>

                    </td>

                    <td>Rs. <?php echo $price['sale_price']-$price['discount']; ?></td>



                    	<td><a style="box-shadow: none;
    background-color: #ee7560;
    color: #fff;"  data-variant="0" data-cart="<?php echo $thisItem->houdin_products_id; ?>"  class="btn border-btn Add_to_cart_button cp_Wish_remove" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>">add to cart</a></td>

                    <td>

                      <div class="ps-cart-listing__remove cp_Wish_remove" data-id="<?php echo $thisItem->houdinv_users_whishlist_id; ?>"></div>

                    </td>

                  </tr>

               	<?php
                        $i++;
						}

						foreach($wishvarint as $wishvarints)
                        {

                          $image = $wishvarints->houdin_products_variants_image;
                          $stock = $wishvarints->houdinv_products_variants_total_stocks;
                          $price = $wishvarints->houdin_products_variants_prices;
                          if($stock>0)
                          {
                            $status = "In stock";
                          }
                          else
                          {
                            $status = "Out of stock";
                          }
                        ?>

                      <tr class="tr_row">

                    <td>

                      <div class="ps-product--cart">
                      <img src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $image; ?>" alt="">
                      <a href="<?php echo base_url() ?>Productlistview/variant<?php echo $wishvarints->houdin_products_variants_id ?>"><?php echo $wishvarints->houdin_products_variants_title; ?></a></div>

                    </td>

                    <td>rs. <?php echo $price; ?></td>



                    	<td><a  style="box-shadow: none;
    background-color: #ee7560;
    color: #fff;"  data-variant="0" data-cart="<?php echo $wishvarints->houdin_products_variants_id; ?>"  class="btn border-btn Add_to_cart_button cp_Wish_remove" data-id="<?php echo $wishvarints->houdinv_users_whishlist_id; ?>">add to cart</a></td>

                    <td>

                      <div class="ps-cart-listing__remove cp_Wish_remove" data-id="<?php echo $wishvarints->houdinv_users_whishlist_id; ?>"></div>

                    </td>

                  </tr>
						<?php
                        $i++;
                        } ?>


                </tbody>

              </table>

            </div>



          </div>

        </div>

      </div>
<?php $this->load->view('Bakery/Template/footer'); ?>
