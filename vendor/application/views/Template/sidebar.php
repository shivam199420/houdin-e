        <style>
                    .list-unstyled-custom
                    {
                        display:none;
                        position: absolute !important;
                        left: 100%;
                        background: #fff;
                        top: 0;
                        width: 240px;
                        z-index: 9999999;
                        border: 1px solid #ddd !important;
                        border-radius: 5px;
                    }
                    
                    .has_sub_hover:hover .list-unstyled-custom
                    {
                        display:block !important;
                    }
                    .slimScrollDiv,.slimscrollleft
                    {
                      /*  overflow:visible !important;*/
                    }
                    .side-menu
                    {
                        z-index:11 !important;
                    }
                </style>
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
<?php
$getCurrency = getVendorCurrency();
if($getCurrency[0]->houdin_users_currency=="USD")
{
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="AUD"){
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="Euro"){
	$currencysymbol= "£";
}else if($getCurrency[0]->houdin_users_currency=="Pound"){
	$currencysymbol= "€";
}else if($getCurrency[0]->houdin_users_currency=="INR"){
	$currencysymbol= "₹";
}

 $this->load->helper('database');
 $getDatabaseData = switchDynamicDatabase();
 $this->db = $this->load->database($getDatabaseData,true);

  if($this->session->userdata('vendorRole')==1)
    {
$sidebar_permission =  $this->db->select("*")->from("houdinv_staff_management_sub")
    ->join("houdinv_staff_management","houdinv_staff_management.staff_id=houdinv_staff_management_sub.staff_id","right outer")
    ->where("houdinv_staff_management.houdinv_staff_auth_token",$this->session->userdata('vendorAuth'))
    ->get()->row();
    }
    else
    {
        $sidebar_permission = '';
    }
    // get unbilled order
    $getTotalUNbiled = $this->db->select('COUNT(houdinv_order_id) AS totalOrder')->from('houdinv_orders')->where('houdinv_order_confirmation_status','unbilled')->get()->result();
    if($getTotalUNbiled[0]->totalOrder)
    {
        $unbilledOrder = $getTotalUNbiled[0]->totalOrder;
    }
    else
    {
        $unbilledOrder = 0;
    }
    $getTotalPendinAmount = $this->db->select('SUM(houdinv_orders_total_remaining) AS totalPendingAmount')->from('houdinv_orders')->get()->result();
    if($getTotalPendinAmount[0]->totalPendingAmount)
    {
        $pendingAmount = $getTotalPendinAmount[0]->totalPendingAmount;
    }
    else
    {
        $pendingAmount = 0;
    }
  //  print_R($sidebar_permission);
?>
        <div id="sidebar-menu">
            <ul>
    <?php if(!$sidebar_permission || ($sidebar_permission->dashboard) !=''){ 
        ?>
               <li class="has_sub" >
                    <a href="<?php echo base_url(); ?>dashboard" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>
                </li>
       <?php  
         }
          ?>
          
          <?php if(!$sidebar_permission || ($sidebar_permission->order) !=''){ 
        ?>    
        
      
                <li class="has_sub has_sub_hover">
                    <a href="<?php echo base_url(); ?>Order/Orderlanding" class="waves-effect <?php if($this->router->fetch_class()=="order"){echo 'active';}?>"><i class="fa fa-twitch"></i> 
                    <span> Orders </span>
                    &nbsp;<span class="label label-primary" title="Total Unbilled Order"><?php echo $unbilledOrder; ?></span>&nbsp;
                    <span class="label label-danger" title="Total Pending Amount"><?php echo $currencysymbol." ".$pendingAmount ?></span><span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" style="">
                             <li><a href="<?php echo base_url(); ?>Order/allorders">All Order</a></li>
                             <li><a href="<?php echo base_url(); ?>Order/unbilled">Unbilled Order</a></li>
                             <li><a href="<?php echo base_url(); ?>Order/paymentpending">Payment Pending</a></li>
                              <li><a href="<?php echo base_url(); ?>Order/deliverypending">Delivery Pending</a></li>
                              <li><a href="<?php echo base_url(); ?>Order/deliverycompleted">Completed</a></li>
                              <li><a href="<?php echo base_url(); ?>Order/deliverypickup">Pick Up</a></li>
                              <li><a href="<?php echo base_url(); ?>Order/deliveryquotation">Quotation</a></li>
                    </ul>
                        <ul class="list-unstyled-custom" style="">
                             <li><a href="<?php echo base_url(); ?>Order/allorders">All Order</a></li>
                             <li><a href="<?php echo base_url(); ?>Order/unbilled">Unbilled Order</a></li>
                             <li><a href="<?php echo base_url(); ?>Order/paymentpending">Payment Pending</a></li>
                              <li><a href="<?php echo base_url(); ?>Order/deliverypending">Delivery Pending</a></li>
                              <li><a href="<?php echo base_url(); ?>Order/deliverycompleted">Completed</a></li>
                              <li><a href="<?php echo base_url(); ?>Order/deliverypickup">Pick Up</a></li>
                              <li><a href="<?php echo base_url(); ?>Order/deliveryquotation">Quotation</a></li>
                    </ul>
                    
                </li>
        
                <?php  
         }
          ?>
                
                <?php if(!$sidebar_permission || ($sidebar_permission->product) !=''){ 

                    $getreorderStatus = getloworderamount();
                    if($getreorderStatus['message'] == 'yes')
                    {
                        $getCount = $this->db->select('COUNT(houdin_products_id) AS totalLow')->from('houdinv_products')->where('houdinv_products_total_stocks <= '.$getreorderStatus['quantity'].'')->get()->row();
                    
                    
                        $this->db->select('COUNT(houdin_products_variants_id) AS totalvariantsLow')->from('houdinv_products_variants as A');
                        //$this->db->join('houdinv_products as B', 'B.houdin_products_id = A.houdin_products_variants_product_id');
                        $this->db->where('A.houdinv_products_variants_total_stocks<=',$getreorderStatus['quantity']); 
                        $lowvariants =$this->db->get()->row();

                       // print_r($lowvariants->totalvariantsLow);
                    $totalLowInvantry=$getCount->totalLow+$lowvariants->totalvariantsLow;
                    
                    
                    }
                    $getMissingDetailProducts = $this->db->select('houdin_products_title,houdin_products_final_price,houdinv_products_total_stocks,houdinv_products_main_images')->from('houdinv_products')->get()->result();
                    $setCount = 0;
                    foreach($getMissingDetailProducts as $getMissingDetailProductsList)
                    {
                        if($getMissingDetailProductsList->houdin_products_title == "" || $getMissingDetailProductsList->houdin_products_final_price == "" || $getMissingDetailProductsList->houdinv_products_total_stocks == "")
                        {
                            $setCount++;   
                        }
                        else
                        {
                            $getMainIMagesData = json_decode($getMissingDetailProductsList->houdinv_products_main_images,true);
                            if($getMainIMagesData[0] == "")
                            {
                                $setCount++;   
                            }
                        }
                        
                    }
        ?>
                <li class="has_sub">
                    <a href="<?php echo base_url(); ?>Product/productLandingpage" class="waves-effect <?php if($this->router->fetch_class()=="Product"){echo 'active';}?>">
                    <i class="fa fa-pie-chart"></i> <span> Product </span>
                    <?php 
                    if($getreorderStatus['message'] == 'yes')
                    {
                    ?>
                    &nbsp;<span class="label label-danger" title="Total Low Inventory Product"><?php echo $totalLowInvantry; ?></span>
                    <?php }
                    ?>
                    &nbsp;<span class="label label-danger" title="Missing Details"><?php echo $setCount; ?></span>
                    </a>
                </li>
                
                <?php  
         }
          ?>
                
                <?php if(!$sidebar_permission || ($sidebar_permission->inventory) !=''){ 
        ?>
                 
                <li class="has_sub has_sub_hover">
                    <a href="<?php echo base_url(); ?>Inventory/Inventorylanding" class="waves-effect <?php if($this->router->fetch_class()=="inventory"){echo 'active';}?>"><i class="fa fa-snowflake-o"></i> <span> Inventory </span>&nbsp;<span class="label label-danger" title="Total Low Inventory Product"><?php echo $totalLowInvantry; ?></span><span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                    <li><a href="<?php echo base_url(); ?>Inventory/lowinventory">Low Inventory</a></li>
                    <li><a href="<?php echo base_url(); ?>Inventory/addinventory">Add Inventory</a></li>
                    <li><a href="<?php echo base_url(); ?>Inventory/deductinventory">Deduct Inventory</a></li>
                    <li><a href="<?php echo base_url(); ?>Inventory/manageinventory">Manage Inventory</a></li>
                    <li><a href="<?php echo base_url(); ?>Inventory/stocktransafer">Stock Transfer</a></li>
                    </ul>
                       <ul class="list-unstyled-custom">
                    <li><a href="<?php echo base_url(); ?>Inventory/lowinventory">Low Inventory</a></li>
                    <li><a href="<?php echo base_url(); ?>Inventory/addinventory">Add Inventory</a></li>
                    <li><a href="<?php echo base_url(); ?>Inventory/deductinventory">Deduct Inventory</a></li>
                    <li><a href="<?php echo base_url(); ?>Inventory/manageinventory">Manage Inventory</a></li>
                    <li><a href="<?php echo base_url(); ?>Inventory/stocktransafer">Stock Transfer</a></li>
                    </ul>
                </li>
              
                <?php  
         }
          ?>
          <?php if(!$sidebar_permission || ($sidebar_permission->purchase) !=''){ 
        ?>
                 <li class="has_sub">
                    <a href="<?php echo base_url(); ?>purchase" class="waves-effect <?php if($this->router->fetch_class()=="purchase"){echo 'active';}?>"><i class="fa fa-shopping-cart"></i> <span> Purchase </span></a>
                </li>
                
                <?php  
         }
          ?>
            <?php if(!$sidebar_permission || ($sidebar_permission->discount) !=''){ 
        ?>
                 <li class="has_sub has_sub_hover">
                    <a href="<?php echo base_url(); ?>discount/Discountlanding" class="waves-effect <?php if($this->router->fetch_class()=="Discount"){echo 'active';}?>"><i class="fa fa-link"></i> <span> Discount </span><span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url();?>discount/storeDiscount">Apply on Store</a></li>
                        <li><a href="<?php echo base_url();?>discount/customerDiscount">Apply By Customer</a></li>

                    </ul>
                        <ul class="list-unstyled-custom">
                        <li><a href="<?php echo base_url();?>discount/storeDiscount">Apply on Store</a></li>
                        <li><a href="<?php echo base_url();?>discount/customerDiscount">Apply By Customer</a></li>

                    </ul>
                </li>

                
                <?php  
         }
          ?>
           <?php if(!$sidebar_permission || ($sidebar_permission->coupons) !=''){ 
        ?>       
                 <li class="has_sub has_sub_hover">
                    <a href="<?php echo base_url(); ?>coupons/Couponslanding" class="waves-effect <?php if($this->router->fetch_class()=="coupons"){echo 'active';}?>"><i class="fa fa-tags"></i> <span> Coupons </span><span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url();?>Coupons">Coupons</a></li>
                        <li><a href="<?php echo base_url();?>Coupons/viewmembership">Membership</a></li>


                    </ul>
                      <ul class="list-unstyled-custom">
                        <li><a href="<?php echo base_url();?>Coupons">Coupons</a></li>
                        <li><a href="<?php echo base_url();?>Coupons/viewmembership">Membership</a></li>


                    </ul>
                </li>
                
                <?php  
         }
          ?>
 <?php if(!$sidebar_permission || ($sidebar_permission->campaign) !=''){ 
        ?>

                 <li class="has_sub has_sub_hover">
                    <a href="<?php echo base_url(); ?>Campaign" class="waves-effect <?php if($this->router->fetch_class()=="Campaign"){echo 'active';}?>"><i class="fa fa-connectdevelop"></i> <span> Campaign </span><span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url();?>campaign/sms">SMS Campaign</a></li>
                        <li><a href="<?php echo base_url();?>campaign/email">Email Campaign</a></li>
                        <li><a href="<?php echo base_url();?>campaign/push">Push Campaign</a></li>


                    </ul>
                     <ul class="list-unstyled-custom">
                        <li><a href="<?php echo base_url();?>campaign/sms">SMS Campaign</a></li>
                        <li><a href="<?php echo base_url();?>campaign/email">Email Campaign</a></li>
                        <li><a href="<?php echo base_url();?>campaign/push">Push Campaign</a></li>


                    </ul>
                </li>

                
                
                <?php  
         }
          ?>
          <?php if(!$sidebar_permission || ($sidebar_permission->customer_management) !=''){ 
        ?>     
                
                 <li class="has_sub has_sub_hover">
                    <a href="<?php echo base_url(); ?>customer/Customerlanding" class="waves-effect <?php if($this->router->fetch_class()=="Customer"){echo 'active';}?>"><i class="fa fa-users"></i> <span> Customer</span><span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url();?>customer">Customer </a></li>
                        <li><a href="<?php echo base_url();?>customer/paymentPending">Payment Pending</a></li>
                        <li><a href="<?php echo base_url();?>customer/privilegedcustomer">Privileged customer</a></li>
                        <li><a href="<?php echo base_url();?>customer/customergroup">Customer Group</a></li>

                    </ul>
                    
                     <ul class="list-unstyled-custom">
                        <li><a href="<?php echo base_url();?>customer">Customer </a></li>
                        <li><a href="<?php echo base_url();?>customer/paymentPending">Payment Pending</a></li>
                        <li><a href="<?php echo base_url();?>customer/privilegedcustomer">Privileged customer</a></li>
                        <li><a href="<?php echo base_url();?>customer/customergroup">Customer Group</a></li>

                    </ul>
                </li>


             <?php  
         }
          ?>
           <?php if(!$sidebar_permission || ($sidebar_permission->cataogry) !=''){ 
        ?>
                 <!-- <li class="has_sub has_sub_hover">
                    <a href="<?php //echo base_url(); ?>Category/Categorylanding" class="waves-effect <?php if($this->router->fetch_class()=="Catagory"){echo 'active';}?>"><i class="fa fa-database"></i> <span> Category </span><span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                              <li><a href="<?php //echo base_url(); ?>Category/categorymanagement">Category </a></li>
                              <li><a href="<?php //echo base_url(); ?>Category/producttype">Product Type </a></li>
                    </ul>
                      <ul class="list-unstyled-custom">
                              <li><a href="<?php //echo base_url(); ?>Category/categorymanagement">Category </a></li>
                              <li><a href="<?php //echo base_url(); ?>Category/producttype">Product Type </a></li>
                    </ul>
                </li> -->

                <?php  
         }
          ?>
          <?php if(!$sidebar_permission || ($sidebar_permission->supplier_management) !=''){ 
        ?>
                <li class="has_sub">
                    <a href="<?php echo base_url(); ?>Supplier" class="waves-effect <?php if($this->router->fetch_class()=="supplier"){echo 'active';}?>"><i class="fa fa-th-list"></i> <span> Supplier </span></a>
                </li>
                

               
            <?php  
         }
          ?>
          <?php if(!$sidebar_permission || ($sidebar_permission->calendar) !=''){ 
        ?>
               <li class="has_sub" >
                    <a href="<?php echo base_url(); ?>Calendar" class="waves-effect"><i class="fa fa-calendar"></i> <span> Calendar </span></a>
                </li>
       <?php  
         }
          ?>

            <?php if(!$sidebar_permission || ($sidebar_permission->templates) !=''){ 
        ?>         
                 <li class="has_sub has_sub_hover">
                    <a href="<?php echo base_url(); ?>Template/Templatelanding" class="waves-effect <?php if($this->router->fetch_class()=="Templates"){echo 'active';}?>"><i class="fa fa-cubes"></i> <span> Templates </span><span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url();?>Template/sms">SMS</a></li>
                        <li><a href="<?php echo base_url();?>Template/email">Email</a></li>
                         <li><a href="<?php echo base_url();?>Template/push">Push</a></li>

                    </ul>
                     <ul class="list-unstyled-custom">
                        <li><a href="<?php echo base_url();?>Template/sms">SMS</a></li>
                        <li><a href="<?php echo base_url();?>Template/email">Email</a></li>
                         <li><a href="<?php echo base_url();?>Template/push">Push</a></li>

                    </ul>
                </li>

               
           <?php  
         }
          ?> 

          <?php if(!$sidebar_permission || ($sidebar_permission->gift_voucher) !=''){ 
        ?>
                   <li class="has_sub">
                    <a href="<?php echo base_url(); ?>giftvouchers" class="waves-effect <?php if($this->router->fetch_class()=="giftvouchers"){echo 'active';}?>"><i class="fa fa-gift"></i> <span> Gift Vouchers </span></a>
                </li>
                
                
              <?php  
         }
          ?> 

         <!-- <li class="has_sub">
                 <a href="<?php echo base_url(); ?>transaction" class="waves-effect <?php if($this->router->fetch_class()=="transaction"){echo 'active';}?>"><i class="fa fa-money"></i> <span> Transaction </span></a>
             </li>-->

   <?php if(!$sidebar_permission || ($sidebar_permission->tax) !=''){ 
        ?>
                   <li class="has_sub hidden-md hidden-sm hidden-lg hidden-xs has_sub_hover">
                    <a href="<?php echo base_url(); ?>Tax/Taxlanding" class="waves-effect <?php if($this->router->fetch_class()=="tax"){echo 'active';}?>"><i class="fa fa-th"></i> <span> Tax </span><span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url();?>Tax/Gstinfo">GST Info</a></li>
                        <li><a href="<?php echo base_url();?>Tax/Taxitem">Tax Item</a></li>


                    </ul>
                     <ul class="list-unstyled-custom">
                        <li><a href="<?php echo base_url();?>Tax/Gstinfo">GST Info</a></li>
                        <li><a href="<?php echo base_url();?>Tax/Taxitem">Tax Item</a></li>


                    </ul>
                </li>

                <?php  
         }
          ?>


            <?php if(!$sidebar_permission || ($sidebar_permission->shipping) !=''){ 
        ?>
                <li class="has_sub has_sub_hover">
                    <a href="<?php echo base_url(); ?>shipping/Shippinglanding" class="waves-effect <?php if($this->router->fetch_class()=="shipping"){echo 'active';}?>"><i class="fa fa-shekel"></i> <span> Shipping </span><span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                    <?php 
                    $getUserPlan = getVendorplan();
                    if($getUserPlan[0]->houdin_package_delivery == 'yes')
                    {
                    ?>
                    <li><a href="<?php echo base_url();?>Shipping/partner">Courier Partner</a></li>
                    <?php }
                    ?>
                        <li><a href="<?php echo base_url();?>Shipping/shippingrule">Shipping rule</a></li>
                        <li><a href="<?php echo base_url();?>Shipping/warehouselist">Warehouse list</a></li>


                    </ul>
                    <ul class="list-unstyled-custom">
                    <?php 
                    $getUserPlan = getVendorplan();
                    if($getUserPlan[0]->houdin_package_delivery == 'yes')
                    {
                    ?>
                    <li><a href="<?php echo base_url();?>Shipping/partner">Courier Partner</a></li>
                    <?php }
                    ?>
                        <li><a href="<?php echo base_url();?>Shipping/shippingrule">Shipping rule</a></li>
                        <li><a href="<?php echo base_url();?>Shipping/warehouselist">Warehouse list</a></li>


                    </ul>
                </li>


               <?php  
         }
          ?>

        

          

          <!-- Main section end here -->

   <?php if(!$sidebar_permission || ($sidebar_permission->delivery) !=''){ 
        ?>
                <li class="has_sub">
                    <a href="<?php echo base_url(); ?>Delivery" class="waves-effect <?php if($this->router->fetch_class()=="Delivery"){echo 'active';}?>"><i class="fa fa-handshake-o"></i> <span> Delivery </span></a>
                </li>
                
                
                <?php  
         }
          ?>
            
                
              <?php if(!$sidebar_permission || ($sidebar_permission->setting) !=''){ 
        ?>  


                <li class="has_sub hidden-md hidden-lg has_sub_hover" style="display:none">
                    <a href="javascript:void(0);" class="waves-effect <?php if($this->router->fetch_class()=="Setting"){echo 'active';}?>" ><i class="fa fa-life-saver"></i> <span> Setting </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="javascript:void(0);"> <span> General</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                              <li><a href="<?php echo base_url(); ?>Setting/compnaydetails">Company Details</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/ordersetting">Orders Setting</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/possetting">POS Setting</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/customersetting">Customer Setting</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/inventorysetting">Inventory Setting</a></li>
                                <li><a href="<?php echo base_url(); ?>Setting/taxsetting">Tax Setting</a></li>
                                  <li><a href="<?php echo base_url(); ?>Setting/skusetting">SKU Setting</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/invoicesetting">Invoice Setting</a></li>
                              <!--<li><a href="#">Supplier Setting</a></li>-->
                              <li><a href="<?php echo base_url(); ?>Setting/storesetting">Store Setting</a></li>
                              <!--<li><a href="#">Online Store Setting</a></li>-->
                            </ul>
                                   <ul class="list-unstyled-custom">
                        <li><a href="javascript:void(0);"> <span> General</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                              <li><a href="<?php echo base_url(); ?>Setting/compnaydetails">Company Details</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/ordersetting">Orders Setting</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/possetting">POS Setting</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/customersetting">Customer Setting</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/inventorysetting">Inventory Setting</a></li>
                                <li><a href="<?php echo base_url(); ?>Setting/taxsetting">Tax Setting</a></li>
                                  <li><a href="<?php echo base_url(); ?>Setting/skusetting">SKU Setting</a></li>
                              <li><a href="<?php echo base_url(); ?>Setting/invoicesetting">Invoice Setting</a></li>
                              <!--<li><a href="#">Supplier Setting</a></li>-->
                              <li><a href="<?php echo base_url(); ?>Setting/storesetting">Store Setting</a></li>
                              <!--<li><a href="#">Online Store Setting</a></li>-->
                            </ul>
                        </li> 
                          <li><a href="<?php echo base_url(); ?>setting/resetaccountsetting"> <span> Reset Account</span></a></li>
                        <li><a href="<?php echo base_url(); ?>setting/smsemail">SMS & Email</a></li>
                        <!--<li><a href="#">Tax</a></li>-->
                          <li><a href="<?php echo base_url(); ?>setting/analytics">Analytics Key</a></li>
                       <!-- <li><a href="<?php echo base_url(); ?>setting/memberShiptype">Membership Type</a></li>-->
                      

                    </ul>
                </li>
               
               

         <?php  
         }
          ?>

          <?php if(!$header_permission || ($header_permission->staff_members) !=''){ 
        ?>       
                             <li class="has_sub hidden-md hidden-lg" style="display:none">
                    <a href="<?php echo base_url(); ?>staff" class="waves-effect <?php if($this->router->fetch_class()=="staff"){echo 'active';}?>"><i class="fa fa-database"></i> <span> Staff Members </span></a>
                </li>      
                   

                        <?php  
         }
          ?>
              
              <?php 
                    $getUserPlan = getVendorplan();
                    if($getUserPlan[0]->houdin_package_payment_gateway == 'yes')
                    {
                    ?>
                 <li class="has_sub">
                    <a href="<?php echo base_url(); ?>Paymentgateway" class="waves-effect <?php if($this->router->fetch_class()=="Paymentgateway"){echo 'active';}?>"><i class="fa fa-credit-card"></i> <span> Payment </span></a>
                </li>
                    <?php } ?>

     
                
                  


          

                
                <li class="has_sub hidden-lg hidden-md has_sub_hover" style="display:none">
                                   
                                    <a href="javascript:void(0);" class="waves-effect <?php if($this->router->fetch_class()=="Outlet"){echo 'active subdrop';}?>"><i class="fa fa-handshake-o"></i> <span>Outlet</span> <span class="menu-arrow"></span> </a>

                                    <ul class="list-unstyled">
                                       <li><a href="javascript:void(0)">Outlet-1</a></li>
                                        <li><a href="javascript:void(0)">Outlet-1</a></li>
                                    </ul>
                                         <ul class="list-unstyled-custom">
                                       <li><a href="javascript:void(0)">Outlet-1</a></li>
                                        <li><a href="javascript:void(0)">Outlet-1</a></li>
                                    </ul>
                                </li>

            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
