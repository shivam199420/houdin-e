<?php
class Dashboardmodel extends CI_Model
{
    function __construct()
    {  
        parent::__construct();
    }
    public function fetchDahsboardData()
    {
        $getTotalTransactionData = $this->db->select('SUM(houdin_admin_transaction_amount) as TotalTransaction')->from('houdin_admin_transaction')->where('houdin_admin_transaction_status','success')->get()->result();
        $getTotalCreditData = $this->db->select('SUM(houdin_admin_transaction_amount) as TotalCreditTransaction')->from('houdin_admin_transaction')->where('houdin_admin_transaction_status','success')->where('houdin_admin_transaction_type','credit')->get()->result();
        $getTotalDebitData = $this->db->select('SUM(houdin_admin_transaction_amount) as TotalDebitTransaction')->from('houdin_admin_transaction')->where('houdin_admin_transaction_status','success')->where('houdin_admin_transaction_type','debit')->get()->result();
        if($getTotalCreditData[0]->TotalCreditTransaction >= $getTotalDebitData[0]->TotalDebitTransaction)
        {
            $setProfit = $getTotalCreditData[0]->TotalCreditTransaction-$getTotalDebitData[0]->TotalDebitTransaction;
            $setLoss = 0;       
        }
        else
        {
            $setProfit = 0;
            $setLoss = $getTotalDebitData[0]->TotalDebitTransaction-$getTotalCreditData[0]->TotalCreditTransaction;
        }
        // get total shops
        $getTotalUser = $this->db->select('COUNT(houdin_user_id) as totalUsers')->from('houdin_users')->get()->result();
        return array(
            'totalTransaction'=>$getTotalTransactionData[0]->TotalTransaction,
            'totalTransactionCredit'=>$getTotalCreditData[0]->TotalCreditTransaction,
            'totalTransactionDebit'=>$getTotalDebitData[0]->TotalDebitTransaction,
            'totalProfit'=>$setProfit,
            'totalLoss'=>$setLoss,
            'totalUser'=>$getTotalUser[0]->totalUsers
        );
    }
}
