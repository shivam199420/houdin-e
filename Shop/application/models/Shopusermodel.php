<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopusermodel extends CI_Model{
    function __construct() 
    {
    }
    public function addUserData($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);
        $getEmailCheck = $getDynamicWebDb->select('houdinv_user_id')->from('houdinv_users')->where('houdinv_user_email',$data['houdinv_user_email'])->get()->result();
        if(count($getEmailCheck) > 0)
        {
            return array('message'=>'email');
        }
        else
        {   
            // contact number check 
            $getContactNumber = $getDynamicWebDb->select('houdinv_user_id')->from('houdinv_users')->where('houdinv_user_contact',$data['houdinv_user_contact'])->get()->result();
            if(count($getContactNumber) > 0)
            {
                return array('message'=>'contact');
            }
            else
            {
                $getInsertStatus = $getDynamicWebDb->insert('houdinv_users',$data);
                if($getInsertStatus == 1)
                {
                    return array('message'=>'yes');
                }
            }
        }
    }
    public function addUserDataApp($data,$shop)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shop['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getEmailCheck = $getDynamicDB->select('houdinv_user_id')->from('houdinv_users')->where('houdinv_user_email',$data['houdinv_user_email'])->get()->result();
        if(count($getEmailCheck) > 0)
        {
            return array('message'=>'email');
        }
        else
        {
            $getInsertStatus = $getDynamicDB->insert('houdinv_users',$data);
            if($getInsertStatus == 1)
            {
                return array('message'=>'yes');
            }
        }
    }
    public function checkUserAuth($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $checkEmailData = $getDynamicWebDb->select('houdin_user_password_salt')->from('houdinv_users')->where('houdinv_user_email',$data['email'])->get()->result();
        if(count($checkEmailData) > 0)
        {
            $pass = crypt($data['password'],$checkEmailData[0]->houdin_user_password_salt);
            $checkPasswordData = $getDynamicWebDb->select('houdinv_user_id')->from('houdinv_users')->where('houdinv_user_password',$pass)->get()->result();
            if(count($checkPasswordData) > 0)
            {
                return array('message'=>'yes','userId'=>$checkPasswordData[0]->houdinv_user_id);
            }
            else
            {
                return array('message'=>'pass');    
            }
        }
        else
        {
            return array('message'=>'email');
        }
    }
    public function checkUserAuthApp($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $checkEmailData = $getDynamicDB->select('houdin_user_password_salt')->from('houdinv_users')->where('houdinv_user_email',$data['email'])->get()->result();
        if(count($checkEmailData) > 0)
        {
            $pass = crypt($data['password'],$checkEmailData[0]->houdin_user_password_salt);
            $checkPasswordData = $getDynamicDB->select('houdinv_user_id')->from('houdinv_users')->where('houdinv_user_password',$pass)->get()->result();
            if(count($checkPasswordData) > 0)
            {
                $getCartCount = $getDynamicDB->select('COUNT(houdinv_users_cart_id) AS totalCart')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$checkPasswordData[0]->houdinv_user_id)->get()->result();
                if($getCartCount[0]->totalCart)
                {
                    $setCount = $getCartCount[0]->totalCart;
                }
                else
                {
                    $setCount = 0;
                }
                $getDynamicDB->where('houdinv_user_id',$checkPasswordData[0]->houdinv_user_id);
                $getDynamicDB->update('houdinv_users',array('houdinv_users_device_id'=>$data['deviceId']));
                return array('message'=>'Success','userId'=>$checkPasswordData[0]->houdinv_user_id,'cartCount'=>$setCount);
            }
            else
            {
                return array('message'=>'Please enter correct password');
            }
        }
        else
        {
            return array('message'=>'Please enter correct email address');
        }
    }
    public function forgotPass($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getEmailCount = $getDynamicWebDb->select('houdinv_emailsms_stats_remaining_credits')->from('houdinv_emailsms_stats')->where('houdinv_emailsms_stats_type','email')->get()->result();
        if(count($getEmailCount) > 0)
        {
            if($getEmailCount[0]->houdinv_emailsms_stats_remaining_credits > 0)
            {
                $getEmailStatus = $getDynamicWebDb->select('houdinv_user_id')->from('houdinv_users')->where('houdinv_user_email',$data)->get()->result();
                if(count($getEmailStatus) > 0)
                {
                    $setToken = rand(9999,1000);
                    $setInsertArray = array('houdinv_users_forgot_user_id'=>$getEmailStatus[0]->houdinv_user_id,'houdinv_users_forgot_token'=>$setToken);
                    $getDynamicWebDb->insert('houdinv_users_forgot',$setInsertArray);
                    return array('message'=>'yes','token'=>$setToken,'credit'=>$getEmailCount[0]->houdinv_emailsms_stats_remaining_credits);
                }
                else
                {
                    return array('message'=>'email');       
                }
            }
            else
            {
                return array('message'=>'credit');    
            }
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function forgotPassApp($email,$data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getEmailCount = $getDynamicDB->select('houdinv_emailsms_stats_remaining_credits')->from('houdinv_emailsms_stats')->where('houdinv_emailsms_stats_type','email')->get()->result();
        if(count($getEmailCount) > 0)
        {
            if($getEmailCount[0]->houdinv_emailsms_stats_remaining_credits > 0)
            {
                $getEmailStatus = $getDynamicDB->select('houdinv_user_id')->from('houdinv_users')->where('houdinv_user_email',$email)->get()->result();
                if(count($getEmailStatus) > 0)
                {
                    $setToken = rand(9999,1000);
                    $setInsertArray = array('houdinv_users_forgot_user_id'=>$getEmailStatus[0]->houdinv_user_id,'houdinv_users_forgot_token'=>$setToken);
                    $getDynamicDB->insert('houdinv_users_forgot',$setInsertArray);
                    return array('message'=>'yes','token'=>$setToken,'credit'=>$getEmailCount[0]->houdinv_emailsms_stats_remaining_credits);
                }
                else
                {
                    return array('message'=>'email');       
                }
            }
            else
            {
                return array('message'=>'credit');    
            }
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function updateForgotApp($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getPinData = $getDynamicDB->select('houdinv_users_forgot_user_id')->from('houdinv_users_forgot')->where('houdinv_users_forgot_token',$data['pin'])->get()->result();
        if(count($getPinData) > 0)
        {
            $getUserId = $getPinData[0]->houdinv_users_forgot_user_id;
            $getAdminP = $data['pass'];
            $letters1='abcdefghijklmnopqrstuvwxyz'; 
            $string1=''; 
            for($x=0; $x<3; ++$x)
            {  
                $string1.=$letters1[rand(0,25)].rand(0,9); 
            }
            $saltdata = password_hash($string1,PASSWORD_DEFAULT);
            $pass = crypt($getAdminP,$saltdata);
            $setUpadtedArray = array('houdinv_user_password'=>$pass,'houdin_user_password_salt'=>$saltdata);
            $getDynamicDB->where('houdinv_user_id',$getUserId);
            $getStatus = $getDynamicDB->update('houdinv_users',$setUpadtedArray);
            if($getStatus)
            {
                $getDynamicDB->where('houdinv_users_forgot_token',$data['pin']);
                $getDynamicDB->delete('houdinv_users_forgot');
                return array('message'=>'Password updated successfully');
            }
            else
            {
                return array('message'=>'Something went wrong. Please try again');    
            }
        }
        else
        {
            return array('message'=>'Something went wrong. Please try again');
        }
    }
    public function updateEmailLog($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getData = $data-1;
        $setUpdateArray = array('houdinv_emailsms_stats_remaining_credits'=>$getData);
        $getDynamicWebDb->where('houdinv_emailsms_stats_type','email');
        $getDynamicWebDb->update('houdinv_emailsms_stats',$setUpdateArray);
        $setData = strtotime(date('Y-m-d H:i:s'));
        $setLogArray = array('houdinv_email_log_credit_used'=>'1','houdinv_email_log_status'=>'1','houdinv_email_log_created_at'=>$setData);
        $getDynamicWebDb->insert('houdinv_email_log',$setLogArray);
    }
    public function getTableData()
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);
        $tables = $getDynamicWebDb->list_tables();   
        for($index = 0; $index< count($tables); $index++)
        {
            $this->db->truncate($tables[$index]);
        }
    }
    public function updatePasswordApp($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getSalt = $getDynamicDB->select('houdin_user_password_salt,houdinv_user_password')->from('houdinv_users')->where('houdinv_user_id',$data['id'])->get()->result();
        if(count($getSalt) > 0)
        {
            $getOldSalt = $getSalt[0]->houdin_user_password_salt;
            $hash_input_user_pass= crypt($data['oldpass'],$getOldSalt);
            if($hash_input_user_pass == $getSalt[0]->houdinv_user_password)
            {
                $getAdminP = $data['newpass'];
                $letters1='abcdefghijklmnopqrstuvwxyz'; 
                $string1=''; 
                for($x=0; $x<3; ++$x)
                {  
                    $string1.=$letters1[rand(0,25)].rand(0,9); 
                }
                $saltdata = password_hash($string1,PASSWORD_DEFAULT);
                $pass = crypt($getAdminP,$saltdata);
                $getDynamicDB->where('houdinv_user_id',$data['id']);
                $getUpadte = $getDynamicDB->update('houdinv_users',array('houdinv_user_password'=>$pass,'houdin_user_password_salt'=>$saltdata));
                if($getUpadte)
                {
                    return array('message'=>'Success');
                }
                else
                {
                    return array('message'=>'Something went wrong. Please try again');
                }
            }
            else
            {
                return array('message'=>'Please enter correct password');
            }
        }
        else
        {
            return array('message'=>'Something went wrong. Please try again');
        }
    }
} 
?>