<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar') ?>

<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                         <!--START Page-Title -->
            <div class="row">

                <div class="col-md-8">
                   <h4 class="page-title">Transaction</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li class="active">Transaction</li>
                  </ol>
                  </div>
                   
                 
            </div>
           <!--END Page-Title --> 
                        <!-- <div class="row">
        <div class="col-md-12 m-t-30">
        <div class="col-md-3">
        <div class="form-group">
        <button type="button"  class="btn btn-default  btn-block">Daily</button>
        </div>
        </div>
        <div class="col-md-3">
        <div class="form-group">
        <button type="button"  class="btn btn-default  btn-block ">This Week</button>
        </div>
        </div>
        <div class="col-md-3">
        <div class="form-group">
        <button type="button"  class="btn btn-default btn-block ">This Month</button>
        </div>
        </div>
        <div class="col-md-3">
        <div class="form-group">
        <button type="button" data-set="year" class="btn btn-default btn-block ">This Year</button>
        </div>
        </div>
        </div>
        <div class="col-md-12">
        <!-- <div class="col-md-3">
        <div class="form-group">



        <div class="input-group m-b-15">

        <div class="bootstrap-timepicker">
        <input id="timepicker" type="text" class="form-control date_picker " placeholder="Enter Details">
        </div>
        <span class="input-group-addon btn btn-default b-0 text-white"><i class="fa fa-calendar"></i></span>
        </div>


        </div>
        </div>
        <div class="col-md-3">
        <div class="form-group">



        <div class="input-group m-b-15">

        <div class="bootstrap-timepicker">
        <input id="timepicker" type="text" class="form-control date_picker" placeholder="Enter Details">
        </div>
        <span class="input-group-addon btn btn-default b-0 text-white"><i class="fa fa-calendar"></i></span>
        </div>


        </div>
        </div>
        <div class="col-md-3">
        <div class="form-group">
        <button type="button" class="btn btn-info  btn-block">Submit</button>
        </div>
        </div>  
        <div class="col-md-3">
        <div class="form-group">
        <button type="button" class="btn btn-success pull-right" id="click_advance"><i class="fa fa-chevron-up"></i></button>
        </div>
        </div>
        </div>
        </div> -->
                        <div class="row " id="display_advance">
                          <div class="col-md-12">
                            <div class="card-box">
                             
                            <div id="transactionData" style="min-width: 100%; height: 400px; margin: 0 auto"></div> 
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12 ">
                          <a href="<?php echo base_url(); ?>Transaction/transactionlist" title="Generate PDF" class="btn btn-info pull-right m-r-10"><i class="fa fa-file-pdf-o"></i>&nbsp;Generate PDF</a>
                          </div>
                        </div>
                        <div class="row m-t-20">
        <div class="col-md-12">

        <div class="card-box table-responsive">

        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th>Tansaction id</th>
        <th>Transaction Date</th>
        <th>Transation Method</th>
        <th>Transaction For</th>
        <th> Transaction Amount</th>
        <th> Transaction Status</th>
        <!-- <th>Action</th> -->
        </tr>
        </thead>
        <tbody>
        <?php 
        foreach($transactionData as $transactionDataList)
        {
          if($transactionDataList->houdinv_transaction_type == 'credit')
          {
            $setTransactionType = 'Credit';
            $setColor = "green";
          }
          else
          {
            $setTransactionType = 'Debit';
            $setColor = "red";
          }
          // transaction status
          if($transactionDataList->houdinv_transaction_status == 'success')
          {
            $setTransactionStatus = 'Success';
            $setClass = "btn-success";
          }
          else
          {
            $setTransactionStatus = 'Fail';
            $setClass = "btn-danger";
          }
        ?>
         <tr>
        <td><?php echo $transactionDataList->houdinv_transaction_transaction_id."<br/><span style='color:".$setColor."'>".$setTransactionType." (".$transactionDataList->houdinv_transaction_method.")</span>" ?></td>
        <td><?php echo date('d-m-Y',strtotime($transactionDataList->houdinv_transaction_date)); ?></td>
        <td class="text-uppercase"><?php echo $transactionDataList->houdinv_transaction_from ?></td>
        <td class="text-uppercase"><?php echo $transactionDataList->houdinv_transaction_for ?></td>
        <td><?php echo $transactionDataList->houdinv_transaction_amount ?></td>
        <td><button type="button" class="btn btn-xs <?php echo $setClass ?>"><?php echo $setTransactionStatus ?> </button></td>
        <!-- <td>
        
        <a href="<?php //echo base_url(); ?>Transaction/invoice/"><button class="btn btn-success waves-effect waves-light">Generate Invoice</button></a>
        </td> -->
        </tr>
        <?php }
        ?>
       
        </tbody>
        </table>
        <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
        </div>
        </div>
        </div>        </div>


                                  </div> <!-- container -->

                              </div> <!-- content -->
<?php $this->load->view('Template/footer.php') ?>
  <script>
  // $(document).ready(function(){
  // $('#click_advance').click(function() {
  // $('#display_advance').toggle('5000');
  // $("i", this).toggleClass("fa fa-chevron-up fa fa-chevron-down");
  // });
  // });
  </script>
  <script type="text/javascript">
  $(document).on('ready',function(){

Highcharts.chart('transactionData', {
    chart: {
        type: 'area',
        spacingBottom: 30
    },
    title: {
        text: 'Transaction Breakdown'
    },
    // subtitle: {
    //     text: '* Jane\'s banana consumption is unknown',
    //     floating: true,
    //     align: 'right',
    //     verticalAlign: 'bottom',
    //     y: 15
    // },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    xAxis: {
        categories: [<?php echo $transactionDate ?>]
    },
    yAxis: {
        title: {
            text: 'Amount'
        },
        labels: {
            formatter: function () {
                return this.value;
            }
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.x + ': ' + this.y;
        }
    },
    plotOptions: {
        area: {
            fillOpacity: 0.5
        }
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Total Amount',
        data: [<?php echo $totalAmount ?>]
    }, {
        name: 'Total Credit',
        data: [<?php echo $totalCreditAmount ?>]
    },
    {
        name: 'Total Debit',
        data: [<?php echo $totalDebitAmount ?>]
    }
    ]
});
  })
  </script>
