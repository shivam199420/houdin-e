    <?php $this->load->view('Template/header') ?>
    <?php $this->load->view('Template/sidebar') ?>
    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">
    <!-- Page-Title -->
    <div class="row">
    <div class="col-sm-12 m-b-15">
    <h4 class="page-title">Twillio Credentials</h4>
    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
    <?php 
        if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        else if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        ?>
  </div>
  </div>
    <div class="row">
    <?php echo form_open(base_url( 'Setting/smscredentials' ), array( 'id' => 'addSMSCredentials' ));?>
    <div class="col-md-12">
    <div class="card-box row">
    <div class="form-group">
    <label>Twilio SID</label>
    <input type="text" name="twilioSid" value="<?php echo $twilioList[0]->houdin_twilio_sid ?>" class="form-control required_validation_for_add_sms_credentials name_validation" placeholder="Enter SID" />
    </div>
    <div class="form-group">
    <label>Twilio Number</label>
    <input type="text" name="twilioNumber" value="<?php echo $twilioList[0]->houdin_twilio_number ?>" class="form-control required_validation_for_add_sms_credentials name_validation" placeholder="Enter Number" />
    </div>
    <div class="form-group">
    <label>Twilio Token</label>
    <input type="text" name="twilioToken" value="<?php echo $twilioList[0]->houdin_twilio_token ?>" class="form-control required_validation_for_add_sms_credentials name_validation" placeholder="Enter Token" />
    </div>
    <input type="hidden" name="smsCredentialsId" value="<?php echo $twilioList[0]->houdin_twilio_id ?>"/>
    <div class="col-md-12 ">
    <input type="submit" class="btn btn-default pull-right m-r-10" name="addSMSCredentials" value="Submit">
    <button type="reset" class="btn btn-info pull-right m-r-10">Reset</button>
    </div>
    </div>
    </div>
    </div>

    </div> <!-- container -->

    </div> <!-- content -->

    <?php $this->load->view('Template/footer') ?>

<!-- CLient side form validation -->
<script type="text/javascript">
            $(document).ready(function(){
                $(document).on('submit','#addSMSCredentials',function(){
                    var check_required_field='';
                    $(this).find(".required_validation_for_add_sms_credentials").each(function(){
                        var val22 = $(this).val();
                        if (!val22){
                            check_required_field =$(this).size();
                            $(this).css("border-color","#ccc");
                            $(this).css("border-color","red");
                        }
                        $(this).on('keypress change',function(){
                            $(this).css("border-color","#ccc");
                        });
                    });
                    if(check_required_field)
                    {
                        return false;
                    }
                    else {
                        return true;
                    }
                });
            });
            </script>