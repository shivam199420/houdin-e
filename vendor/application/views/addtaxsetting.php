<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/settingsidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!--START Page-Title -->
            <div class="row">
              
                <div class="col-md-8">
                   <h4 class="page-title">Tax Setting</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li><a href="javascript:void(0);">General</a></li>
                  <li class="active">Tax Setting</li>
                  </ol>
                  </div>
            
            </div>
           <!--END Page-Title -->  
            <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
        <?php 
        $addtaxsetting_data=$taxsetting[0];
        ?>
            <div class="row m-t-20">
        <div class="col-sm-12">
<?php echo form_open(base_url('Setting/taxsetting_save'), array( 'id' => 'taxsetting_save', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
            <div class="card-box">
          <div class="col-md-12">
          <div class="ajax_response_result">
            <div class="form-group">
        <label>Tax Registration Id</label>
        <input class="form-control required_validation_for_add_custo" value="<?=$addtaxsetting_data->taxsetting_number?>" name="taxsetting_number"  type="text" placeholder="Tax Registration Id">
        </div>
        </div>

          </div>
          <input type="hidden" name="update_id" value="<?=$addtaxsetting_data->id?>">
          <div class="row">
        <input type="submit" class="btn btn-default pull-right m-r-10" name="" value="Submit">
        <button type="reset" id="enable_custome_reset" class="btn btn-info pull-right m-r-10">Reset</button>

        </div>
      
        </div>
        <?php echo form_close(); ?>
      </div>
      <!-- start multiple tax data -->
      <div class="col-sm-12">

      <div class="card-box table-responsive">
            <div class="btn-group pull-right m-t-10 m-b-20">
            <button type="button" class="btn btn-default m-r-5 addNewTaxBtn" title="Add New Tax"><i class="fa fa-plus"></i></a>
            </div>
            <table class="table table-striped table-bordered table_shop_custom">
            <thead>
            <tr>
            <th>Tax Name</th>
            <th>Tax Percentage</th>
            <th>Action</th>
            </tr>
            </thead>
            <tbody>
              <?php 
              foreach($fetchtax as $fetchtaxList)
              {
              ?>
              <tr>
              <td><?php echo $fetchtaxList->houdinv_tax_name ?></td>
              <td><?php echo $fetchtaxList->houdinv_tax_percenatge ?>%</td>
              <td>
                <button type="button" data-id="<?php echo $fetchtaxList->houdinv_tax_id ?>" data-name="<?php echo $fetchtaxList->houdinv_tax_name ?>" data-percentage="<?php echo $fetchtaxList->houdinv_tax_percenatge ?>" class="btn btn-primary btn-sm editTaxBtn">Edit</button>
                <button type="button" data-id="<?php echo $fetchtaxList->houdinv_tax_id ?>" class="btn btn-primary btn-sm deletetexbtn">Delete</button>
            </td>
            </tr>
              <?php }
              ?>
            </tbody>
            </table>
            <!-- <ul class="pagination pull-right"> <?php  //echo $this->pagination->create_links(); ?> </ul> -->
            </div>
      </div>
      <!-- end multiple tax data -->
    </div>
  </div>
</div>

<!-- add tax -- >
<!--change Status-->
<div id="addNewTax" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<?php echo form_open(base_url( 'Setting/addnewtax'), array( 'id' => 'addNewTaxForm', 'method'=>'post' ));?>
<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

<h4 class="modal-title">Add New Tax</h4>

</div>

<div class="modal-body">



<div class="row">
<div class="col-md-12">


<div class="form-group">
<input type="hidden" class="taxId" name="taxId"/>
<label for="field-7" class="control-label">Tax Name</label>
<input type="text" name="taxname" class="form-control required_validation_for_add_tax name_validation taxname" placeholder="Please enter tax name"/>
</div>

<div class="form-group">
<label for="field-7" class="control-label">Tax Perecentage(%)</label>
<input type="text" name="taxpercentage" class="form-control required_validation_for_add_tax number_validation taxpercentage" placeholder="Please enter tax percentage"/>
</div>

</div>
</div>

</div>

<div class="modal-footer">

<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

<input type="submit" class="btn btn-info " name="addtaxpercentage" value="Submit">

</div>

</div>
<?php echo form_close(); ?>
</div>

</div>

<!-- delete tax -->
<div id="deleteTax" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<?php echo form_open(base_url( 'Setting/deletetax'), array( 'id' => '', 'method'=>'post' ));?>
<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

<h4 class="modal-title">Delete Tax</h4>

</div>

<div class="modal-body">



<div class="row">
<div class="col-md-12">
  <input type="hidden" class="deleteTaxId" name="deleteTaxId"/>
<h3>Do you really want to delete this entry?</h3>
</div>
</div>

</div>

<div class="modal-footer">

<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

<input type="submit" class="btn btn-info " name="addtaxpercentage" value="Submit">

</div>

</div>
<?php echo form_close(); ?>
</div>

</div>

<?php $this->load->view('Template/footer.php') ?>

<script type="text/javascript">
  
$(document).ready(function(){

 
 $("#enable_custome_reset").click(function () { 
  
 
$('.required_validation_for_add_custo').each(function() {
  
        $(this).val('');
         
    });
 });


 $('#taxsetting_save').submit(function(e){
    e.preventDefault(); 
 

var check_required_field='';
      $(this).find(".required_validation_for_add_custo").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {
        return false;
      }else{
      


    urls='<?=base_url();?>'+'Setting/taxsetting_save';
           
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".ajax_response_result").html(data);
        }else{
        window.location.reload();
        }
        }
        }); } 
    });  
  $(document).on('click','.addNewTaxBtn',function(){
    $('#addNewTax').modal('show');
  });
  $(document).on('click','.editTaxBtn',function(){
    $('.taxId').val($(this).attr('data-id'));
    $('.taxname').val($(this).attr('data-name'));
    $('.taxpercentage').val($(this).attr('data-percentage'));
    $('#addNewTax').modal('show');
  });
  $(document).on('click','.deletetexbtn',function(){
    $('.deleteTaxId').val($(this).attr('data-id'));
    $('#deleteTax').modal('show');
  })
  $(document).on('submit','#addNewTaxForm',function(){
			var check_required_field='';
			$(".required_validation_for_add_tax").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
 }); 

</script>
