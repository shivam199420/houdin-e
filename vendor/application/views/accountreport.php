<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); ?>

    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">
    <!--START Page-Title -->
    <div class="row">
    
    <div class="col-md-8">
    <h4 class="page-title">Reports</h4>
    <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>">Home</a></li>
    <li><a href="<?php echo base_url(); ?>Accounts">Accounts</a></li>
    <li class="active">Reports</li>
    </ol>
    </div>
     <div class="clearfix"></div>  
    <div class="row " id="display_advance">
    <div class="col-md-12">
    <div class="card-box">
    <div class="alert alert-danger showErrorMessage" style="display:none"></div>
    <table class="table table-striped table-bordered table_shop_custom">
    <thead>
    <tr>
    <th>Name</th>
    <th>Report Period</th>
    <th>Custom Period</th>
    <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <tr class="mainParentRow">
    <td>Company Overview</td>
    <td><select class="form-control checkReportPeriod" data-report="company">
    <option value="">Choose Period</option>
    <option value="tcy">This Calendar Year</option>
    <option value="cm">Current Month</option>
    <option value="cw">Current Week</option>
    <option value="cd">Current Day</option>
    <option value="ccd">Custom Date</option>
    </select></td>
    <td><label class="col-sm-5"><input type="text" disabled="disabled" class="form-control date_picker setDateValue setDateValueFrom"/></label>
    <label class="col-sm-1 text-center">to</label>
    <label class="col-sm-5"><input type="text" disabled="disabled" class="form-control date_picker setDateValue setDateValueto"/></label>
    </td>
    <td><button type="button" disabled="disabled" class="btn btn-sm btn-default viewBtnData">View</button></td>
    </tr>
    <tr class="mainParentRow">
    <td>Sales</td>
    <td><select class="form-control checkReportPeriod" data-report="order">
    <option value="">Choose Period</option>
    <option value="tcy">This Calendar Year</option>
    <option value="cm">Current Month</option>
    <option value="cw">Current Week</option>
    <option value="cd">Current Day</option>
    <option value="ccd">Custom Date</option>
    </select></td>
    <td><label class="col-sm-5"><input type="text" disabled="disabled" class="form-control date_picker setDateValue setDateValueFrom"/></label>
    <label class="col-sm-1 text-center">to</label>
    <label class="col-sm-5"><input type="text" disabled="disabled" class="form-control date_picker setDateValue setDateValueto"/></label>
    </td>
    <td><button type="button" disabled="disabled" class="btn btn-sm btn-default viewBtnData">View</button></td>
    </tr>

    <tr class="mainParentRow">
    <td>Transaction</td>
    <td><select class="form-control checkReportPeriod" data-report="transaction">
    <option value="">Choose Period</option>
    <option value="tcy">This Calendar Year</option>
    <option value="cm">Current Month</option>
    <option value="cw">Current Week</option>
    <option value="cd">Current Day</option>
    <option value="ccd">Custom Date</option>
    </select></td>
    <td><label class="col-sm-5"><input type="text" disabled="disabled" class="form-control date_picker setDateValue setDateValueFrom"/></label>
    <label class="col-sm-1 text-center">to</label>
    <label class="col-sm-5"><input type="text" disabled="disabled" class="form-control date_picker setDateValue setDateValueto"/></label>
    </td>
    <td><button type="button" disabled="disabled" class="btn btn-sm btn-default viewBtnData">View</button></td>
    </tr>
    </tbody>
    </table>
    </div>
    </div>
    
            </div>


    </div> <!-- container -->

    </div> <!-- content -->
    <?php $this->load->view('Template/footer.php') ?>
    <script type="text/javascript">
    $(document).ready(function(){
        var getReportId = "";
        var getValue = "";
        $(document).on('change','.checkReportPeriod',function(){
            getReportId = $(this).attr('data-report');
            getValue = $(this).val();
            if(getValue)
            {
                $(this).parents('.mainParentRow').find('.viewBtnData').prop('disabled',false);
            }
            else
            {
                $(this).parents('.mainParentRow').find('.viewBtnData').prop('disabled',true);
            }
            if(getValue == 'ccd')
            {
                $(this).parents('.mainParentRow').find('.setDateValue').prop('disabled',false);
            }
            else
            {
                $(this).parents('.mainParentRow').find('.setDateValue').val('').prop('disabled',true);
            }  
        });
        $(document).on('click','.viewBtnData',function(){
            var siteBaseUrl = "<?php echo base_url() ?>";
            var getReportId = $(this).parents('.mainParentRow').find('.checkReportPeriod').attr('data-report');
            if(getReportId == 'order')
            {
                var setPageName = 'orderReport';
            }
            else if(getReportId == 'transaction')
            {
                var setPageName = 'transactionReport';
            }
            else if(getReportId == 'company')
            {
                var setPageName = 'companyReport';
            }

            var getReportValue = $(this).parents('.mainParentRow').find('.checkReportPeriod').children('option:selected').val();
            if(getReportValue == 'ccd')
            {
                var getDateFrom = $(this).parents('.mainParentRow').find('.setDateValueFrom').val();
                var getDateTo = $(this).parents('.mainParentRow').find('.setDateValueto').val();
                if(getDateFrom && getDateTo)
                {
                    $('.showErrorMessage').hide().text('');
                    var setData = siteBaseUrl+"Accounts/"+setPageName+"/"+getReportId+"/"+getReportValue+"/"+getDateFrom+"/"+getDateTo;
                    window.open(setData, '_blank');
                }
                else
                {
                    $('.showErrorMessage').show().text('').text('Please choose date');
                }
            }
            else
            {
                var setData = siteBaseUrl+"Accounts/"+setPageName+"/"+getReportId+"/"+getReportValue;
                window.open(setData, '_blank');
            }
        });
    });
    </script>
    
