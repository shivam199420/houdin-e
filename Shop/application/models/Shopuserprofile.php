<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopuserprofile extends CI_Model{
    function __construct() 
    {
         $this->getAppDBsite = switchDynamicDatabase();
      
        $this->getDynamicDBsite = $this->load->database($this->getAppDBsite,true);
    }
    public function fetchUserProfileApp($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shop']);
        $getDynamicDB = $this->load->database($getAppDB,true);
        
        $getUserProfile = $getDynamicDB->select('*')->from('houdinv_users')->where('houdinv_user_id',$data['id'])->get()->result();
        if($getUserProfile[0]->houdinv_users_profile)
        {
            $getShop = shopurl();
            $setImageData = "".$getShop."Extra/Uploads/userprofile/".$getUserProfile[0]->houdinv_users_profile;
        }
        else
        {
            $setImageData = "";
        }
        $setArrayData = array('image'=>$setImageData,'name'=>$getUserProfile[0]->houdinv_user_name,'gender'=>$getUserProfile[0]->houdinv_users_gender,'mobile'=>$getUserProfile[0]->houdinv_user_contact,'email'=>$getUserProfile[0]->houdinv_user_email,'dob'=>$getUserProfile[0]->houdinv_users_dob);
        return array('messgae'=>'Success','data'=>$setArrayData);
    }
    public function updateUserProfileData($data,$shopData)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shopData['shopname']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getDynamicDB->where('houdinv_user_id',$shopData['id']);
        $getStatus = $getDynamicDB->update('houdinv_users',$data);
        if($getStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function updateUserProfileImageData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopname']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getDynamicDB->where('houdinv_user_id',$data['id']);
        $setUpdateArray = array('houdinv_users_profile'=>$data['image']);
        $getImageUpdate = $getDynamicDB->update('houdinv_users',$setUpdateArray);
        if($getImageUpdate == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    
    public function getUserAllData($id)
    {
     $getUserProfile = $this->getDynamicDBsite->select('*')->from('houdinv_users')
     ->where('houdinv_user_id',$id)->get()->row();
     return $getUserProfile;
          
    }
} 

?>