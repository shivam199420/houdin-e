<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <?php
  $getDB = getDBName();
  $this->load->helper('dynamicdatabaseapp');
    $getDBDetail = switchDynamicDatabaseApp($this->session->userdata('shopName'));
    $this->db = $this->load->database($getDBDetail,true);
  $vendorURL = $this->session->userdata('vendorURL');
  $con = mysqli_connect("localhost","root","houdine123",$getDB);
  $getImages = mysqli_query($con,"select shop_logo,shop_favicon from houdinv_shop_logo limit 1");
  $getImageData = mysqli_fetch_array($getImages);
  // get username
  $getUserId = $this->session->userdata('userAuth');
  $getUser = mysqli_query($con,"select houdinv_user_name from houdinv_users where houdinv_user_id='$getUserId'");
  $getUserData = mysqli_fetch_array($getUser);
  ?>
  <?php
  $getCurrency = getShopCurrency();
  $currencysymbol=$getCurrency[0]->houdin_users_currency;
  ?>
  <script type="text/javascript">
      var base_url = "<?php echo base_url() ?>";
  </script>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php 
  $getShopName = getshopname();
  if($getShopName)
  {
      $setShopName = $getShopName;
  }
  else
  {
      $setShopName = 'Houdin-e';
  }
  ?>
  <title>Welcome To <?php echo $setShopName; ?></title>
  <link rel="shortcut icon" type="image/x-icon" href="<?php if($getImageData['shop_favicon']) { ?><?=$vendorURL?>upload/logo/<?php echo $getImageData['shop_favicon'];  ?> <?php } else { ?> <?php echo base_url() ?>Extra/apparels/img/hundin-e-favicon.png <?php  } ?> ">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,600,700,800" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Handlee" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url() ?>Extra/apparels/css/animate.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>Extra/apparels/css/owl.theme.default.min.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>Extra/apparels/css/owl.carousel.min.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>Extra/apparels/css/meanmenu.min.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>Extra/apparels/css/venobox.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>Extra/apparels/css/font-awesome.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>Extra/apparels/css/bootstrap.min.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>Extra/apparels/css/style.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>Extra/apparels/css/responsive.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>Extra/apparels/css/intlTelInput.css" />

  <style type="text/css">
  @media (max-width: 767px){
    .owl-nav .owl-prev {
      position: absolute;
      top: 91%!important;
      }#slider_area .owl-nav .owl-next, #slider_area .owl-nav .owl-prev {
        position: absolute;
        top: 91% !important;
      }

      .ftr_btm_area {
        text-align: center;
      }
      .payment_mthd_icon{
        display: inline-block;
      }
      .sub-menu {
        opacity: 1 !important;

      }
      .mean-container .mean-nav ul {
        padding: 0;
        margin: 0;
        width: 100%;
        list-style-type: none;
        padding-bottom: 100px !important;
        position: initial;
      }
    }
    .intl-tel-input.allow-dropdown
    {
      width:100%;
    }
    /* #textbox {padding:10px;border-radius:5px;border:0;box-shadow:0 0 4px 0 rgba(0,0,0,0.2)}
    .row img {width:40px;height:40px;border-radius:50%;vertical-align:middle;}
    .row {padding:10px;} .row:hover {background-color:#eee} */
  </style>
  <style type="text/css">
  .desktop-showContent {
    visibility: visible !important;
    opacity: 1 !important;
  }
  .desktop-query {
    width: 300px;
    float: right;
    line-height: 0;
    margin: 20px 10px 20px 0;
  }
  .desktop-query > input.desktop-searchBar {
    font-family: Whitney;
    display: inline-block;
    float: left;
    font-size: 16px;
    height: 20px;
    line-height: 24px;
    width: 235px;
    color: #696E79;
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    padding: 8px 10px 10px 10px;
    margin: 0;
    outline: 0;
    border: 1px solid #D5D6D9;
    -webkit-border-radius: 4px 0 0 4px;
    -moz-border-radius: 4px 0 0 4px;
    border-radius: 4px 0 0 4px;
    border-right: 0;
    background: #F5F5F6;
  }
  input.desktop-searchBar:focus {
    background-color: #FFFFFF;
    border-color: #14CDA8;
  }
  input.desktop-searchBar:focus + a.desktop-submit {
    background-color: #FFFFFF;
    border-color: #14CDA8;
  }
  a.desktop-submit {
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    display: inline-block;
    height: 28px;
    width: 40px;
    text-align: center;
    padding: 8px 0 2px 0;
    background: #F5F5F6;
    border: 1px solid #D4D5D9;
    border-left: none;
    -webkit-border-radius: 0 4px 4px 0;
    -moz-border-radius: 0 4px 4px 0;
    border-radius: 0 4px 4px 0;
  }
  .desktop-autoSuggest {
    z-index: 4;
    position: relative;
    display: block;
    -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0);
    -moz-box-shadow: 0 1px 6px rgba(0, 0, 0, 0);
    box-shadow: 0 1px 6px rgba(0, 0, 0, 0);
    /* rgba(0,0,0,0.3) */
    font-size: 11px;
    color: #696E79;
    background-color: #fff;
    -webkit-font-smoothing: antialiased;
    visibility: hidden;
    opacity: 0;
    -webkit-box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.3);
    -moz-box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.3);
    width:100%;
  }
  .desktop-autoSuggest .desktop-group {
    margin: 0 0 10px 0;
    padding: 0;
    max-height: 300px;
    overflow-y: auto;
  }

  .desktop-group > li {
    list-style: none;
    padding-left: 12px;
    text-align: left;
    display: flex;
    border-bottom: 1px solid #ddd;
  }
  .desktop-group > li >a{}
    .desktop-group > li >a.linkcat_search{
      width: 100%;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    .desktop-group > li >a.linkcat_search_cart{
      width: 35px;
      text-align: center;
      background-color:#eee;
      border-left:1px solid #ddd;
    }
    .desktop-suggestion {
      background-color: #FFFFFF;
    }
    .desktop-suggestion:hover {
      background-color: #D4D5D9;
      cursor: pointer;
    }
    .desktop-suggestionTitle {
      font-weight: bold;
      background-color: #F5F5F6;
      cursor: pointer;
    }
    .search-box {
      line-height: 38px;
    }
  </style>

</head>
<body>

  <!--  Preloader  -->

  <div class="preloader">
    <div class="status-mes">
      <div class="bigSqr">
        <div class="square first"></div>
        <div class="square second"></div>
        <div class="square third"></div>
        <div class="square fourth"></div>
      </div>
      <div class="text_loading text-center">loading</div>
    </div>
  </div>

  <!--  Start Header  -->
  <header id="header_area">

    <?php

    if($this->session->userdata('userAuth') == "")
    {
      ?>
      <div class="header_top_area">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="hdr_tp_left">
                <div class="call_area">

                </div>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6">

              <ul class="hdr_tp_right text-right">

                <span class="login_register"><a href="<?php echo base_url() ?>register"><i class="fa fa-sign-in"></i>Login/Register</a></span>
              </ul>
            </div>
          </div>
        </div>
      </div>
    <?php }
    ?>
    <!--  HEADER START  -->

    <div class="header_btm_area">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-3">
            <a class="logo" href="<?php echo base_url() ?>home/"> <img alt="Shop Logo" src="<?php if($getImageData['shop_logo']) { ?><?=$vendorURL;?>upload/logo/<?php echo $getImageData['shop_logo']  ?> <?php } else { ?> <?php echo base_url() ?>Extra/apparels/img/hundin_Logo_Gray.png <?php  } ?>" style="max-width:100%; height: 47px!important;"></a>
            </div><!--  End Col -->

            <div class="col-xs-12 col-sm-12 col-md-9 text-right">
              <div class="menu_wrap">
                <div class="main-menu">
                  <nav>
                    <ul>
                      <?php
                      //get header navigation
                      $getHeadernav = mysqli_query($con,"select * from houdinv_navigation_store_pages where houdinv_navigation_store_pages_name='home' || houdinv_navigation_store_pages_category_id !=0 || houdinv_navigation_store_pages_custom_link_id !=0");
                      while($getHeaderNavData = mysqli_fetch_array($getHeadernav))
                      {
                        if($getHeaderNavData['houdinv_navigation_store_pages_name'] == 'home')
                        {
                          ?>
                          <li><a href="<?php echo base_url() ?>home/">HOME</a></li>
                        <?php }
                        else if($getHeaderNavData['houdinv_navigation_store_pages_custom_link_id'] != 0)
                        {
                          $setCustomLinkId = $getHeaderNavData['houdinv_navigation_store_pages_custom_link_id'];
                          $getCustomLinkData = mysqli_query($con,"select * from houdinv_custom_links where houdinv_custom_links_id = $setCustomLinkId");
                          $fetchCustomLinkData = mysqli_fetch_array($getCustomLinkData);
                          ?>
                          <li><a href="<?php echo $fetchCustomLinkData['houdinv_custom_links_url'] ?>" <?php if($fetchCustomLinkData['houdinv_custom_links_target'] == 'new') { ?> target="_blank" <?php  } ?>><?php echo $fetchCustomLinkData['houdinv_custom_links_title'] ?></a></li>
                        <?php }
                        else
                        {
                          $getMainCategoryId = $getHeaderNavData['houdinv_navigation_store_pages_category_id'];
                          $getSubCategoryData = mysqli_query($con,"select houdinv_sub_category_one_name,houdinv_sub_category_one_id from houdinv_sub_categories_one where houdinv_sub_category_one_main_id = $getMainCategoryId");
                          $totalCategoryData=mysqli_num_rows($getSubCategoryData);
                          ?>
                          <li><a href="<?php echo base_url() ?>Productlist/<?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>"><?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_name'] ?><?php if($totalCategoryData > 0) { ?> <i class="fa fa-angle-down"></i> <?php  } ?></a>
                            <?php
                            if($totalCategoryData > 0)
                            {
                              ?>
                              <div class="mega-menu mm-4-column mm-left">
                                <?php
                                while($fetchSubCategory = mysqli_fetch_assoc($getSubCategoryData))
                                {
                                  $getSubCategoryId = $fetchSubCategory['houdinv_sub_category_one_id'];
                                  ?>

                                  <div class="mm-column mm-column-link float-left">
                                    <h3><a href="<?php echo base_url() ?>Productlist/<?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>/<?php echo $getSubCategoryId ?>"><?php echo $fetchSubCategory['houdinv_sub_category_one_name'] ?><a/></h3>
                                      <?php
                                      $getSubSubCategory = mysqli_query($con,"select houdinv_sub_category_two_name,houdinv_sub_category_two_id from houdinv_sub_categories_two where houdinv_sub_category_two_sub1_id = $getSubCategoryId");
                                      while($fetchSubSubCategory = mysqli_fetch_assoc($getSubSubCategory))
                                      {
                                        $setSubSubCategoryId = $fetchSubSubCategory['houdinv_sub_category_two_id'];
                                        ?>
                                        <a href="<?php echo base_url() ?>Productlist/<?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>/<?php echo $getSubCategoryId ?>/<?php echo $setSubSubCategoryId ?>"><?php echo $fetchSubSubCategory['houdinv_sub_category_two_name']; ?></a>
                                      <?php }
                                      ?>
                                    </div>
                                  <?php }
                                  ?>
                                </div>
                              <?php }
                              ?>

                            </li>
                          <?php }
                        }
                        ?>
                        <?php
                        if($this->session->userdata('userAuth') != "")
                        {
                          ?>
                          <li class="account_area"><a href="javascript:;"><i class="fa fa-lock"></i> <?php echo $getUserData['houdinv_user_name'] ?></a>
                            <ul class="sub-menu">
                              <!-- <li><a href="<?php //echo base_url() ?>Profile">My account</a></li> -->
                              <li><a href="<?php echo base_url(); ?>Orders">My order</a></li>
                              <li><a href="<?php echo base_url() ?>Address">My address</a></li>
                              <li><a href="<?php echo base_url() ?>Changepassword">Change password</a></li>
                              <li><a href="<?php echo base_url() ?>wishlist">Wishlist</a></li>
                              <li><a href="<?php echo base_url() ?>Logout">Logout</a></li>
                            </ul>
                          </li>
                        <?php }
                        ?>
                        <!-- <li><a href="contact.html">CONTACT</a></li> -->
                      </ul>
                    </nav>
                  </div> <!--  End Main Menu -->
                  <!-- mobile menu -->
                  <div class="mobile-menu text-right ">
                    <nav>
                      <ul>
                        <?php
                        //get header navigation
                        $getHeadernav = mysqli_query($con,"select * from houdinv_navigation_store_pages where houdinv_navigation_store_pages_name='home' || houdinv_navigation_store_pages_category_id !=0 || houdinv_navigation_store_pages_custom_link_id !=0");
                        while($getHeaderNavData = mysqli_fetch_array($getHeadernav))
                        {
                          if($getHeaderNavData['houdinv_navigation_store_pages_name'] == 'home')
                          {
                            ?>
                            <li><a href="<?php echo base_url() ?>home/">HOME</a></li>
                          <?php }
                          else if($getHeaderNavData['houdinv_navigation_store_pages_custom_link_id'] != 0)
                          {
                            $setCustomLinkId = $getHeaderNavData['houdinv_navigation_store_pages_custom_link_id'];
                            $getCustomLinkData = mysqli_query($con,"select * from houdinv_custom_links where houdinv_custom_links_id = $setCustomLinkId");
                            $fetchCustomLinkData = mysqli_fetch_array($getCustomLinkData);
                            ?>
                            <li><a href="<?php echo $fetchCustomLinkData['houdinv_custom_links_url'] ?>" <?php if($fetchCustomLinkData['houdinv_custom_links_target'] == 'new') { ?> target="_blank" <?php  } ?>><?php echo $fetchCustomLinkData['houdinv_custom_links_title'] ?></a></li>
                          <?php }
                          else
                          {
                            $getMainCategoryId = $getHeaderNavData['houdinv_navigation_store_pages_category_id'];
                            $getSubCategoryData = mysqli_query($con,"select houdinv_sub_category_one_name,houdinv_sub_category_one_id from houdinv_sub_categories_one where houdinv_sub_category_one_main_id = $getMainCategoryId");
                            $totalCategoryData=mysqli_num_rows($getSubCategoryData);
                            ?>
                            <li><a href="<?php echo base_url() ?>Productlist/<?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>"><?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_name'] ?></a></li>
                          <?php }
                        }
                        ?>
                        <?php
                        if($this->session->userdata('userAuth') == "")
                        {
                          ?>
                          <li><a href="<?php echo base_url() ?>Register">Login/Register</a></li>
                        <?php }
                        ?>
                        <?php
                        if($this->session->userdata('userAuth') != "")
                        {
                          ?>
                          <li class="account_area"><a href=""><i class="fa fa-lock"></i> <?php echo $getUserData['houdinv_user_name'] ?></a>
                            <ul class="sub-menu">
                              <!-- <li><a href="profile.html">My Account</a></li> -->
                              <li><a href="<?php echo base_url(); ?>Orders">My order</a></li>
                              <li><a href="<?php echo base_url() ?>Address">My address</a></li>
                              <li><a href="<?php echo base_url() ?>Changepassword">Change password</a></li>
                              <li><a href="<?php echo base_url() ?>wishlist">Wishlist</a></li>
                              <li><a href="<?php echo base_url() ?>Logout">Logout</a></li>
                            </ul>
                          </li>
                        <?php } ?>
                        <!-- <li><a href="contact.html">contact us</a></li> -->
                      </ul>
                    </nav>
                  </div>
                  <!--  End mobile-menu -->

                  <div class="right_menu">
                    <ul class="nav justify-content-end">
                      <li>
                        <div class="search_icon">
                          <i class="fa fa-search search_btn" aria-hidden="true"></i>
                          <div class="search-box">
                            <?php //echo form_open(base_url( 'SearchResult' ), array( 'id' => 'search', 'method'=>'post' ));?>

                            <div class="input-group m-l-5" id="searchboxsuggestionproduct">
                              <input type="hidden" class="form-control" value="category1" name="shopName" placeholder="enter keyword"/>
                              <input type="text" class="form-control" name="typeahead" id="searchfiltertext"  autocomplete="off" spellcheck="false" placeholder="enter keyword"/>

                              <button type="submit" name="submit" class="btn btn-default"><i class="fa fa-search"></i></button><br>

                              <div id="searchfilterdataheader" class=" desktop-autoSuggest desktop-showContent" ></div>
                            </div>
                            <?php //echo form_close(); ?>
                          </div>

                        </div>
                      </li>

                      <li>
                        <?php
                        $AllMiniCart =array();
                        $totalCartData =0;
                        if($this->session->userdata('userAuth'))
                        {
                          $data_All = $this->db->select("*")->from("houdinv_users_cart") ->where("houdinv_users_cart.houdinv_users_cart_user_id",$this->session->userdata('userAuth'))->get()->result();
                          $setCartArray = array();
                          foreach($data_All as $data_AllData)
                          {
                            if($data_AllData->houdinv_users_cart_item_id != 0 && $data_AllData->houdinv_users_cart_item_id != "")
                            {
                              $getProductData = $this->db->select('*')->from('houdinv_products')->where('houdin_products_id',$data_AllData->houdinv_users_cart_item_id)->get()->result();
                              // setImageData
                              $getImage = json_decode($getProductData[0]->houdinv_products_main_images,true);
                              // set price
                              $getPrice = json_decode($getProductData[0]->houdin_products_price,true);
                              if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
                              {
                                $setPrice = $getProductData[0]->houdin_products_final_price;
                              }
                              else
                              {
                                $setPrice = $getProductData[0]->houdin_products_final_price;
                              }
                              $setCartArray[] = array('count'=>$data_AllData->houdinv_users_cart_item_count,'productName'=>$getProductData[0]->houdin_products_title,'productprice'=>$setPrice,'productImage'=>$getImage[0],'cartId'=>$data_AllData->houdinv_users_cart_id);
                            }
                            else
                            {
                              $getProductData = $this->db->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data_AllData->houdinv_users_cart_item_variant_id)->get()->result();
                              // get product
                              $getExtraProduct = $this->db->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getProductData[0]->houdin_products_variants_product_id)->get()->result();
                              // set price
                              $getPrice = json_decode($getExtraProduct[0]->houdin_products_price,true);
                              if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
                              {
                                $setPrice = $getProductData[0]->houdin_products_variants_prices-$getPrice['discount'];
                              }
                              else
                              {
                                $setPrice = $getProductData[0]->houdin_products_variants_prices;
                              }

                              $setCartArray[] = array('count'=>$data_AllData->houdinv_users_cart_item_count,'productName'=>$getProductData[0]->houdin_products_variants_title,'productprice'=>$setPrice,'productImage'=>$getProductData[0]->houdin_products_variants_image,'cartId'=>$data_AllData->houdinv_users_cart_id);
                            }
                          }
                          $totalCartData=count($setCartArray);

                          //  $AllMiniCart =$this->Cartmodel->ShowCartData1($this->session->userdata('userAuth'));
                        }

                        ?>
                        <div class="cart_menu_area">
                          <div class="cart_icon">
                            <a href="<?php echo base_url() ?>cart"><i class="fa fa-shopping-bag " aria-hidden="true"></i></a>
                            <span class="cart_number"><?php  echo $totalCartData; ?></span>
                          </div>


                          <!-- Mini Cart Wrapper -->
                          <div class="mini-cart-wrapper">
                            <!-- Product List -->


                            <div class="mc-pro-list fix">
                              <?php
                              if($totalCartData>0)
                              {

                                $i=1;
                                $final_price =0;
                                foreach($setCartArray as $setCartArrayList)
                                {
                                  $image = $setCartArrayList['productImage'];
                                  $count = $setCartArrayList['count'];
                                  $main_price = $setCartArrayList['productprice'];
                                  $total_price = $main_price*$count;
                                  $final_price =$final_price+$total_price;?>
                                  <div class="mc-sin-pro fix tr_row">
                                    <a href="javascript:;" class="mc-pro-image float-left" style="width: 49px;"><img src="<?php echo $this->session->userdata('vendorUrl') ?>/upload/productImage/<?php echo $image; ?>" alt="" /></a>
                                    <div class="mc-pro-details fix">
                                      <a href="javascript:;" style="font-size:11px"><?php echo substr($setCartArrayList['productName'],0,12)."(".$count.")"; ?></a>
                                      <div class="clearfix"></div>
                                      <span><?php
                                      if($currencysymbol=="USD")
                                      {
                                        echo "$";
                                      }else if($currencysymbol=="AUD"){
                                        echo "$";
                                      }else if($currencysymbol=="Euro"){
                                        echo "£";
                                      }else if($currencysymbol=="Pound"){
                                        echo "€";
                                      }else if($currencysymbol=="INR"){
                                        echo "₹";
                                      }
                                      ?><?php echo $main_price; ?></span>
                                      <a class="pro-del cp_cart_remove" data-id="<?php echo $setCartArrayList['cartId']; ?>" data-replace="<?php echo $total_price; ?>"  href="javascript:;"><i class="fa fa-times-circle"></i></a>
                                    </div>
                                  </div>

                                <?php } ?>

                              </div>
                              <!-- Sub Total -->
                              <div class="mc-subtotal fix">
                                <h4>Subtotal <span class="minicart_subtotal_class pull-right"><?php
                                if($currencysymbol=="USD")
                                {
                                  echo "$";
                                }else if($currencysymbol=="AUD"){
                                  echo "$";
                                }else if($currencysymbol=="Euro"){
                                  echo "£";
                                }else if($currencysymbol=="Pound"){
                                  echo "€";
                                }else if($currencysymbol=="INR"){
                                  echo "₹";
                                }
                                ?><?php echo $final_price; ?></span></h4>
                              </div>
                              <!-- Cart Button -->
                              <div class="mc-button">
                                <a href="<?php echo base_url();?>/Cart" class="checkout_btn">checkout</a>
                              </div>
                            <?php  }
                            else
                            {
                              ?>

                              <div class="mc-sin-pro fix">
                                Your cart is empty
                              </div>

                              <?php
                            }

                            ?>
                          </div>


                        </div>

                      </li>
                    </ul>
                  </div>
                </div>
              </div><!--  End Col -->
            </div>
          </div>
        </div>
      </header>
      <!--  End Header  -->
