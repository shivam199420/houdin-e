<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">shopping cart</h2>

            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- shopping-cart-area start -->
    <div class="cart-area ptb-100">
        <div class="container">
        <?php
            if(count($AllCart)<1)
            {
            ?>
            <div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h3 class="text-center">Your cart is empty</h3></div></div>
            <?php
            }
            else
            {
            ?>
            <div class="row">
            <?php
            if($this->session->flashdata('error'))
            {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
            }

            if($this->session->flashdata('message_name'))
            {
                    echo '<div class="alert alert-danger">'.implode("</br>",json_Decode($this->session->flashdata('message_name'))).'</div>';
            }
            if($this->session->flashdata('success'))
            {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
            }
            ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="table-content table-responsive cart_table_area">
                            <table class="table-shop cart_prdct_table">
                                <thead>
                                    <tr>
                                        <th class="product-price cpt_img">images</th>
                                        <th class="product-name cpt_pn">Product</th>
                                        <th class="product-quantity cpt_q">Quantity</th>
                                        <th class="product-price cpt_p">Price</th>
                                        <th class="product-subtotal cpt_t">Total</th>
                                        <th class="product-name cpt_r">remove</th>
                                    </tr>
                                </thead>
                                <?php echo form_open(base_url( 'Cart/UpdateCart' ), array( 'id' => 'UpdateCart', 'method'=>'post' ));?>
                                <tbody>
                                <?php
                                $i=1;
                                $final_price =0;
                                foreach($AllCart as $thisItem)
                                {
                                  if($thisItem['stock'] <= 0)
        													{
        														$setStockData = '(Out of stock)';
        													}
                                $count = $thisItem['count'];
                                $main_price = $thisItem['productPrice'];
                                $total_price = $main_price*$count;
                                $final_price =$final_price+$total_price;

                                ?>
                                    <tr class="tr_row">
                                        <td class="product-thumbnail">
                                            <a href="javascript:;"><img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $thisItem['productImage']; ?>" style="width:80px" alt=""></a>
                                        </td>
                                        <td class="product-name"><a href="<?php if($thisItem['productId'] != 0) { echo base_url()."Productlistview/".$thisItem['productId'];  } else { echo base_url()."Productlistview/variant/".$thisItem['productId']; }  ?>"><?php echo $thisItem['productName']; ?><span style="color:red"><?php echo $setStockData; ?></span></a></td>
                                        <td class="">
                                        <div class="cp_quntty">
                                            	<input name="product[<?php echo  $i-1; ?>][product_id]" value="<?php echo $thisItem['cartId']; ?>" size="2" type="hidden">
												<input name="product[<?php echo  $i-1; ?>][quantity]" value="<?php echo $thisItem['count']; ?>" size="2" type="number">
											</div>
                                        </td>
                                        <td class="product-price"><p class="cp_price"><?php echo $main_price; ?></p></td>

                                        <td class="product-subtotal"><p class="cpp_total"><?php echo $total_price; ?></p></td>
                                        <td class="product-remove"><a href="javascript:;" class="cp_cart_remove" data-replace="<?php echo $total_price; ?>" data-id="<?php echo $thisItem['cartId']; ?>" ><i class="fa fa-times"></i></a></td>

                                    </tr>
                                <?php $i++; } ?>
                                </tbody>
                                <?php echo form_close(); ?>
                            </table>
                        </div>

                </div>
            </div>

            <div class="row mt-50">
                <div class="col-md-7 col-sm-12 col-xs-12">

                </div>
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="cart-total">
                        <ul>
                            <li>Subtotal<span id="mcart_subtotal_class" class="mcart_subtotal_class"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span></li>
                            <li class="cart-black">Total<span class="mcart_subtotal_class"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span></li>
                        </ul>
                        <div class="cart-total-btn">
                            <div class="cart-total-btn1 f-left">
                                <a href="<?php echo base_url() ?>Checkout">Proceed to checkout</a>
                            </div>
                            <div class="cart-total-btn2 f-right">
                                <a href="javascript:;" class="update_cart">Update cart</a>
                            </div>
                        </div>
                    </div>

                </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>
