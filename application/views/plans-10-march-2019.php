<?php  $this->load->view('Template/header');?> 

<header class="header bg-gray header-page-top">
  <div class="header-page-top-inner">
	  <div class="container">
		<h1 class="display-4">Plans</h1> 
  </div>
	</div>
</header>


	 <section class="section py-8">
        <div class="container">
          <header class="section-header"> 
            <h2>Choose your perfect plan</h2>
             
          </header> 


          <div class="row gap-y text-center">
          <?php 
          if($_COOKIE['tempUserPackageId'])
          {
          ?>
          <p style="font-size: 18px;color: black;">
          Your trial period has expired. But don't you worry! Upgrade now!</p>
        <p style="font-size: 18px;color: black;">Have some doubts about whether you should continue? Get in touch with us now at support@houdine.com and let us help you find the best plan for you.</p>
          <?php }
          ?>
           <?php 
            foreach($packageData as $packageDataList)
            {
              $getMainArray = $packageDataList;
              if($getMainArray->houdin_package_name == 'Tailored')
              {
              ?>
              <div class="col-md-<?php if(count($packageData) == 3) { echo "4"; } else if(count($packageData) == 4) { echo "3"; } ?>">
              <div class="pricing-1">
                <p class="plan-name"><?php echo $getMainArray->houdin_package_name ?></p>
                <br>
                <!-- <h2 class="price"> <span class="price-unit">&#8377;</span>
                  <span data-bind-radio="pricing-1"><?php //echo $getMainArray->houdin_package_price ?></span>
				  </h2>
               <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">Yearly</span>
                </p> -->
                <div class="custom-min-height">   
                <div class="text-muted">
                  <p class="mb-1"> <?php echo $getMainArray->houdin_package_description ?></p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"></p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                  <p class="mb-1"> </p>
                </div>
                </div>

                <br>
                <br>
                <br>
                <br>
                <br>
                 
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="<?php echo base_url() ?>home/contact">Contact Us</a></p>
              </div>
            </div>
              <?php }
              else
              {
              ?>
                <div class="col-md-<?php if(count($packageData) == 3) { echo "4"; } else if(count($packageData) == 4) { echo "3"; } ?>">
              <div class="pricing-1">
                <p class="plan-name"><?php echo $getMainArray->houdin_package_name ?></p>
                <br>
                <h2 class="price"> <span class="price-unit">&#8377;</span>
                  <span data-bind-radio="pricing-1"><?php echo $getMainArray->houdin_package_price ?></span>
				  </h2>
               <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">Yearly</span>
                </p>
                <div class="custom-min-height">   
                <div class="text-muted">
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_product ?> Products</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_staff_accounts ?> Staff Accounts</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_stock_location ?> Stock Location</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_app_access ?> App Access</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_order_pm ?> Order Per Month</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_templates ?> Templates</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_sms ?> Free SMS</p>
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> <?php echo $getMainArray->houdin_package_email ?> Free Email</p>
                  <?php if($getMainArray->houdin_package_accounting == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Accounting Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_pos == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> POS Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_delivery == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Delivery Integration</p><?php } ?>
                  <?php if($getMainArray->houdin_package_payment_gateway == 'yes') { ?><p class="mb-1"><i class="ti-check text-three mr-2"></i> Payment Gateway Integration</p><?php } ?>
                </div>
                </div>

                <br>
                <?php 
                if($_COOKIE['tempUserPackageId'])
                {
                ?>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="<?php echo base_url() ?>Payumoney/<?php echo $getMainArray->houdin_package_id ?>/<?php echo $getMainArray->houdin_package_name ?>/<?php echo $getMainArray->houdin_package_price ?>">Upgrade Your Package</a></p>
                <?php }
                else
                {
                ?>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" data-toggle="modal" data-target="#user-register" href="javascript:;">Start Free Trial</a></p>
                <?php }
                ?>
              </div>
            </div>
              <?php }
            ?>
            <?php }  
            ?>


            <!-- <div class="col-md-4">
              <div class="pricing-1 popular">
                <p class="plan-name">Gold</p>
                <br>
                <h2 class="price text-second">
                  <span class="price-unit">$</span>
                  <span data-bind-radio="pricing-1">39</span>
                </h2>
                <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">Monthly</span>
                </p>

                <div class="text-muted">
					
                 <p class="mb-1"><i class="ti-check text-success mr-2"></i> Enabled International Payments</p>
					<p class="mb-1"><i class="ti-check text-success mr-2"></i> 50+ beautiful themes</p>
					<p class="mb-1"><i class="ti-check text-success mr-2"></i> 25+ premium themes</p>
					<p class="mb-1"><i class="ti-check text-success mr-2"></i>Payment enabled Facebook store</p>
					<p class="mb-1"><i class="ti-check text-success mr-2"></i> Wholesale pricing. Wishlist</p>
					<p class="mb-1"><i class="ti-check text-success mr-2"></i> 30-Day Onboarding support</p>
					<p class="mb-1"><i class="ti-check text-success mr-2"></i>  Superfast website (CDN).</p>
					<p class="mb-1"><i class="ti-check text-success mr-2"></i> CRM, Marketing tools</p>
                  
                </div>

                <br>
                <p class="text-center py-3"><a class="btn btn-success" href="">Get started</a></p>
              </div>
            </div>


            <div class="col-md-4">
              <div class="pricing-1">
                <p class="plan-name">PLATINUM</p>
                <br>
                <h2 class="price">
                  <span class="price-unit">$</span>
                  <span data-bind-radio="pricing-1">69</span>
                </h2>
                <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">Monthly</span>
                </p>

                <div class="text-muted">
                  <p class="mb-1"><i class="ti-check text-success mr-2"></i> Gold Plan with iOS app and Android app</p>
				<p class="mb-1"><i class="ti-check text-success mr-2"></i> Personal eCommerce consultant</p>
				<p class="mb-1"><i class="ti-check text-success mr-2"></i>Store setup cost included</p>
				<p class="mb-1"><i class="ti-check text-success mr-2"></i>  Coupon code promotions</p>
				<p class="mb-1"><i class="ti-check text-success mr-2"></i>   Digital marketing</p>
				<p class="mb-1"><i class="ti-check text-success mr-2"></i>  Integrated SSL</p> 
                </div>

                <br>
                <p class="text-center py-3"><a class="btn btn-primary" href="">Get started</a></p>
              </div>
            </div> -->

          </div>


        </div>
      </section>

  	

<?php  $this->load->view('Template/footer');?>