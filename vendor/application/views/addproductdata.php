<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<!-- product page style -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom/product/add-product.css"/>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

           <!--START Page-Title -->
   
            <div class="row">
                <div class="col-md-8">
                   <h4 class="page-title">Product</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>product">Product</a></li>
                  <li class="active">Add Product</li>
                  </ol>
                  </div>
                 
            </div>
           <!--END Page-Title -->

           <?php     
          //  check for error and success message
           if ($this->session->flashdata('message_name')) {
             echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
           }
           if ($this->session->flashdata('success')) {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
          }
          // get category
          $category =  $new['category'];
          // start form here
          echo form_open(base_url().'Product/addproduct',array("id"=>"Addproductn","enctype"=>"multipart/form-data"));
           ?>    
        <div class="row">
        <!-- set modal -->
        <div class="col-sm-12">
        <label class="radio-inline">
      <input type="radio" value="1" class="product_modal" name="optradio" checked>Basic
    </label>
    <label class="radio-inline">
      <input type="radio" value="2" class="product_modal" name="optradio">Advance
    </label>
        </div>
        <div class="col-sm-8 m-t-20">
        <div class="card-box">

          <!-- product name -->
          <div class="form-group">
          <label>Title</label>
          <input type="text" class="form-control required_validation_for_product" name="title" placeholder="Please enter product title" />
          </div>

          <!-- Product category -->
          <div class="form-group">
          <label for="userName">Choose a Category<span class="add-descrption"><a href="javascript;;" data-target="#add_catagory" data-toggle="modal"><i class="fa fa-plus-circle add_new_category text-success"></i></a></span></label>
          <?php  ?>
          <select class="form-control select2" name="category_name[]" multiple="">
          <option value="">Select Category</option>
          <?php 
          foreach ($category as $values) {
            if ($values['main']->houdinv_category_thumb != 'Others.png') {
          ?>
          <option value="<?php echo $values['main']->houdinv_category_id; ?>^^sub0"><?php echo $values['main']->houdinv_category_name; ?></option>
          <?php
          foreach ($values['sub'] as $sub1) {
          ?>
          <option style="margin-left:10px;" value="<?php echo $sub1['main']->houdinv_sub_category_one_id; ?>^^sub1">  - <?php echo $sub1['main']->houdinv_sub_category_one_name; ?></option>
           <?php
           foreach ($sub1['sub'] as $sub2) {
            ?>
          <option style="margin-left:20px;" value="<?php echo $sub2->houdinv_sub_category_two_id; ?>^^sub2">   -- <?php echo $sub2->houdinv_sub_category_two_name; ?></option>
          <?php } } } } ?>
          </select>
          </div>

          <!-- product type -->
          <div class="form-group">
          <label>Product type</label>
          <select class="form-control " id="type_change" name="product_type">
          <option value="">Select Product Type</option>
          <?php 
          $type =  $new['type'];
          foreach ($type as $values) {
          ?>
          <option value="<?php echo $values->houdinv_productType_id; ?>" data-sub ='<?php echo ($values->houdinv_productType_subattr); ?>' ><?php echo $values->houdinv_productType_name; ?></option>
          <?php } ?>
          </select>
          </div>

          <!-- product attribute according to product type -->
          <div class="form-group Add_attribut"></div>

          <!-- product description -->
          <div class="form-group d-none advance_modal">
          <label>Description<span class="add-descrption"><i class="fa fa-plus-circle text-success show_description"></i><i class="fa fa-minus-circle d-none text-danger hide_description"></i></span></label>
          <textarea style="min-height: auto;" id="editor1" class="form-control descption_box d-none" rows="2" placeholder="Enter product description" name="short_desc"></textarea>
          </div> 

          <!-- sell product as -->
          <div class="form-group d-none advance_modal">
          <label>Sell Product as</label>  
          <div > <input class="form-control" type="radio" name="sell_as"  style="float:left;" value="Packaged"/><span style="margin-left:2%">Packaged</span></div>
          <div > <input class="form-control" type="radio" name="sell_as" style="float:left;" value="Loose"/><span style="margin-left:2%">Loose</span></div>
          <div ><input class="form-control" type="radio" checked name="sell_as" style="float:left;" value="Both"/><span style="margin-left:2%">Both</span></div>
        </div>

       <!-- Product attributes -->
       <div class="form-group d-none advance_modal">
       <!-- is product featured -->
       <label>Product Features </label><div class="clearfix"></div>
      <div class="col-md-4">
      <div class="checkbox checkbox-inline" style="padding-left: 0px!important;margin-left: 0px!important">
      <input type="checkbox" id="inlineRadio1" value="1" name="product_feature" checked="">
      <label for="inlineRadio1">  Featured </label>
      </div>
      </div>
      <!-- if product ask for quotation -->
      <div class="col-md-4">
      <div class="checkbox checkbox-inline" style="padding-left: 0px!important;margin-left: 0px!important">
      <input type="checkbox" id="inlineRadio5" value="1" name="product_quotation" checked="">
      <label for="inlineRadio5">  Quotation </label>
      </div>
      </div>                
      <!-- is product active  -->
    <div class="col-md-4">
    <div class="checkbox checkbox-inline" style="padding-left: 0px!important;margin-left: 0px!important">
    <input type="checkbox" id="inlineRadio2" checked=""  name="product_status">
    <label for="inlineRadio2"> Active </label>
    </div>
    </div>
      </div> 

      <!-- product units -->
      <div class="form-group d-none advance_modal">
      <label>Product Units</label>
        <select class="form-control" name="productUnit">
          <option value="">Choose Product Unit</option>
          <option value="BAG">BAGS</option>
          <option value="BALE">BALE</option>
          <option value="BUNDLES">BUNDLES</option>
          <option value="BUCKLES">BUCKLES</option>
          <option value="BILLIONS OF UNITS">BILLIONS OF UNITS</option>
          <option value="BOX">BOX</option>
          <option value="BOTTLES">BOTTLES</option>
          <option value="BUNCHES">BUNCHES</option>
          <option value="CANS">CANS</option>
          <option value="CUBIC METER">CUBIC METER</option>
          <option value="CUBIC CENTIMETER">CUBIC CENTIMETER</option>
          <option value="CENTIMETER">CENTIMETER</option>
          <option value="CARTONS">CARTONS</option>
          <option value="DOZEN">DOZEN</option>
          <option value="DRUM">DRUM</option>
          <option value="GREAT GROSS">GREAT GROSS</option>
          <option value="GRAMS">GRAMS</option>
          <option value="GROSS">GROSS</option>
          <option value="GROSS YARDS">GROSS YARDS</option>
          <option value="KILOGRAMS">KILOGRAMS</option>
          <option value="KILOLITER">KILOLITER</option>
          <option value="KILOMETRE">KILOMETRE</option>
          <option value="MILLILITRE">MILLILITRE</option>
          <option value="METERS">METERS</option>
          <option value="NUMBERS">NUMBERS</option>
          <option value="PACKS">PACKS</option>
          <option value="PIECES">PIECES</option>
          <option value="PAIRS">PAIRS</option>
          <option value="QUINTAL">QUINTAL</option>
          <option value="ROLLS">ROLLS</option>
          <option value="SETS">SETS</option>
          <option value="SQUARE FEET">SQUARE FEET</option>
          <option value="SQUARE METERS">SQUARE METERS</option>
          <option value="SQUARE YARDS">SQUARE YARDS</option>
          <option value="TABLETS">TABLETS</option>
          <option value="TEN GROSS">TEN GROSS</option>
          <option value="THOUSANDS">THOUSANDS</option>
          <option value="TONNES">TONNES</option>
          <option value="TUBES">TUBES</option>
          <option value="US GALLONS">US GALLONS</option>
          <option value="UNITS">UNITS</option>
          <option value="YARDS">YARDS</option>
          <option value="OTHERS">OTHERS</option>
        </select>
    </div> 

          <!-- product visibility-->
          <div class="form-group d-none advance_modal">
          <label>Show product on</label>
          <div > <input class="form-control" type="radio" name="show_on"  style="float:left;" value="App"/><span style="margin-left:2%">App</span></div>
          <div > <input class="form-control" type="radio" name="show_on" style="float:left;" value="Website"/><span style="margin-left:2%">Website</span></div>
          <div ><input class="form-control" type="radio" checked name="show_on" style="float:left;" value="Both"/><span style="margin-left:2%">Both</span></div>
       </div>
        </div>
        <!--pricing-->
    </div>
    <!-- product imaages  -->
        <div class="col-md-4 m-t-20 ">
          <div class="card-box pull-left image_section" style="width: 100%;">
            <h4>Images<span class="add-product-image"><i class="fa fa-plus-circle text-success add-more-image"></i></span></h4>
           <div class="images_preview_row">
            <div class="col-md-8">
            <div class="form-group">
              <lable>Image 1</lable>
              <!-- first_image -->
              <input type="file" class="form-control images_class first_image"  name="images[0]" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />
            </div>
          </div>
            <div class="col-md-4" >
              <img  class="thunbnail_image images_preview" src="<?php echo base_url() ?>assets/images/users/avatar-1.jpg"/>
            </div>
            </div>
            
          </div>
       
</div>
          </div>
       

        <!-- varinat data -->
        <div class="row">
             <div class="col-md-12">
              <h4 class="page-title">Variants</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">
                  <p>Does this product have variations like color, size etc</p>
                   <button type="button" class="btn btn-info" data-toggle="modal" data-target="#delete_product">Add Variants</button> 
                </div>
              </div> 
        </div>

        <!-- add variant option -->
        <div class="row">
             <div class="col-md-12"> 
       <div class="combinations"> </div>
  </div>
  </div>
  <!-- add variant option end -->
  
         <!--Proudtc information -->      
         <div class="row variant_hide">
             <div class="col-md-12">
              <h4 class="page-title">Product Information</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">
                <div class="form-group d-none advance_modal">
            <label>Barcode</label>
          <input type="hidden"  name="barcode"  class="form-control barcode_input" name="barcode" placeholder="Enter Bar Code"/>
          <a  class="btn btn-default btn-xs m-r-5 barcode_generate">
          <i class="fa fa-barcode"></i>
          Generate Barcode
          </a>
          <img class="barcode_images" src="" />
      <p style="color:red" class="message_text"></p>
        </div>
                     
        <div class="form-group">
        <label>WareHouse</label>
        
        <select class="form-control ware_house select2 select_input_requred " multiple="" name="product_warehouse[]">
        <option value="">Choose Warehouse</option>
        <?php foreach($WareHouse as $house)
        {
          $warehous_list = "";
          $warehous_list .= "<option value='".$house->id."' data-name='".$house->w_name."'>". $house->w_name."</option>";
          ?>
        <option value="<?php echo $house->id; ?>" data-name="<?php echo $house->w_name; ?>"><?php echo $house->w_name; ?></option>
        
        <?php } ?>
        </select>
        </div>

        <!-- show store quantity when user choose warehouse -->
        <div class="form-group Add_cost_quantity" ></div>                                               
       </div>
       </div>
       </div> 
        <!--Produict information ends here --> 


        <!--START ADD Inventory SECTION -->      
        <div class="row d-none advance_modal">
             <div class="col-md-12">
              <h4 class="page-title">Inventory</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">
                <div class="form-group">
                        <label>Low Stock Alert</label>
                        <input type="text" onkeyup="checkINt(this);" class="form-control validated_integer " name="order_sort" placeholder="1" />
                      </div>
                <div class="form-group">
                        <label>Batch/Lot no </label>
                        <input type="text" class="form-control" name="batchNumber" placeholder="Enter Batch/Lot no" />
                      </div>

                      <div class="form-group">
                        <label>Product Expiry </label>
                        <input type="text" class="form-control date_picker" name="expiry" placeholder="Product Expiry Date" />
        </div>     
                    </div>
              </div> 
        </div>
       <!--END ADD Inventory SECTION -->


         <!--START ADD Pricing SECTION -->      
         <div class="row">
             <div class="col-md-12">
              <h4 class="page-title">Pricing</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">
                
                    <div class="form-group">
                        <div class="col-md-6">
                          <div class="form-group">
                        <label>MRP</label>
                        <input type="text"  onkeyup="checkDec(this);"  class="form-control required_validation_for_product validated_numeric" name="Main_price" placeholder="Enter product price here, only numbers" />
                      </div>
                     
                        </div>
                        <div class="col-md-6" style="margin-top: 25px;">
                         <div class="input-group">
                        <span class="input-group-addon">
                        <input type="checkbox" class="discount_Click">
                      </span>
                      <input type="text" class="form-control discount_price"  onkeyup="checkDec(this);"  name="discount_price" placeholder="Enter discounted price here, only numbers">
                      </div>
                     </div>
                     
                     
                                            <div class="col-md-12">


                                            <div class="col-md-6">
                      <div class="form-group">
                        <label>Sale price</label>
                        <input type="text"  onkeyup="checkDec(this);"  class="form-control required_validation_for_product" name="sale_price" placeholder="Enter product sale price" />
                      </div>
                       </div>

                       <div class="col-md-6">
                      <div class="form-group">
                        <label>Sales Tax&nbsp;&nbsp;<input type="checkbox" name="sales_tax_check"/></label>
                        <select class="form-control" name="sales_tax">
                        <option value="">Choose Sale Tax</option>
                        <?php 
                        foreach($taxData as $taxDataList)
                        {
                        ?>
                        <option value="<?php echo $taxDataList->houdinv_tax_id ?>"><?php echo $taxDataList->houdinv_tax_name ?></option>
                        <?php }
                        ?>
                        </select>
                      </div>
                       </div>
                       </div>
                        <div class="col-md-12">
                        <div class="col-md-6">
                          <div class="form-group">
                        <label>Cost Price</label>
                        <input type="text" onkeyup="checkDec(this);" id="costP"  class="required_validation_for_product form-control" name="cost_price" placeholder="Enter product cost price" />
                      </div>
                       </div>
                       <div class="col-sm-6">
                      <div class="form-group">
                        <label>Purchase Tax&nbsp;&nbsp;<input type="checkbox" name="purchase_tax_check"/></label>
                        <select class="form-control" name="purchase_tax">
                        <option value="">Choose Purchase Tax</option>
                        <?php 
                        foreach($taxData as $taxDataList)
                        {
                        ?>
                        <option value="<?php echo $taxDataList->houdinv_tax_id ?>"><?php echo $taxDataList->houdinv_tax_name ?></option>
                        <?php }
                        ?>
                        </select>
                      </div>
                      </div>
                       </div>                                                                               
                    </div>
                 
                 
                </div>
              </div> 
        </div>
        
 
       <!--END ADD Pricing SECTION -->   

       <!-- supplier -->

                        <div class="row d-none advance_modal">
             <div class="col-md-12">
              <h4 class="page-title">Supplier</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">
                    <div class="form-group">
                         
                          <div class="form-group">
                        <label>Supplier</label>
         <select class="form-control select2 select_input_requred" name="MainSuplier[]" multiple="">
         <option value="">Choose Supplier</option>
            <?php 
            foreach($supplier as $customerListData)
            {
             ?> 
           
            <option value="<?php echo $customerListData->houdinv_supplier_id ?>"><?php echo $customerListData->houdinv_supplier_contact_person_name; ?></option> 
           <?php 
           }
            ?>
            </select> 
            </div>
    
                    </div>
                  </div>
              </div> 
        </div>

        <!--START ADD Shipping SECTION -->      
        <div class="row d-none advance_modal">
             <div class="col-md-12">
              <h4 class="page-title">Shipping</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">
                    <div class="form-group">
                        <div class="col-md-6">
                          <div class="form-group">
                        <label>Length</label>
                        <input type="text" class="form-control  validated_numeric"  onkeyup="checkDec(this);" name="length" placeholder="Enter product length in cm here, only numbers" />
                      </div>
                      <div class="form-group">
                        <label>Height</label>
                        <input type="text" class="form-control  validated_numeric" onkeyup="checkDec(this);"  name="height" placeholder="Enter product height in cm here, only numbers" />
                      </div>
                        </div>
                        <div class="col-md-6">
                         <div class="form-group">
                        <label>Breath</label>
                        <input type="text" class="form-control  validated_numeric" onkeyup="checkDec(this);"  name="breath" placeholder="Enter product breadth in cm here, only numbers" />
                      </div>
                      <div class="form-group">
                        <label>Weight</label>
                        <input type="text" class="form-control  validated_numeric" onkeyup="checkDec(this);" name="weight" placeholder="Enter product weight here (in gm) only numbers" />
                      </div>
                        </div>
                    </div>
                 
                 
                </div>
              </div> 
        </div>
       <!--END ADD Shipping SECTION -->   

      
  <!-- add variants-->
  <div class="append-card-box"></div>
        <div class="col-md-12">
          <input type="submit" name="formSubmit"  value="submit" class="btn btn-info">
        </div>
        </div>
        <!-- end form -->
<?php echo form_close(); ?>
      </div>
    </div>
  </div>
<!--Delete-->

<!--add option modal-->

  <div id="delete_product" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

<div class="modal-dialog">

<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

<h4 class="modal-title">Option</h4>

</div>

<div class="modal-body">



<div class="row">
<div class="col-md-12">
<table class="append_table table houdin_addvariants_box" >
     <?php  
      $ij =0;
      if ($All_one_var) {
        $i1=1;
        $count1 = count($All_one_var);
        foreach ($All_one_var as $each) {
          $new_variant1 = json_decode($each->houdin_products_variants_new);
          $n=0;
          foreach ($new_variant1 as $new_v1) {
          if (!in_array(($new_v1->variant_name),$ary)) {
            $ary[]=$new_v1->variant_name;
      ?>
            <tr class="table_tr houdin_addvariants_box_row"> 
              <td><div class="input-group">
            <input type="text" class="form-control save_option_name_text" data-name="<?php echo  $new_v->variant_name ;?>" value="<?php echo  $new_v->variant_name ;?>" placeholder="option name like color, size, size" max-length="100" required="">
            <span class="input-group-btn">
              <button style="margin:0px!important;" class="btn btn-success save_option_name" type="submit"><i class="fa fa-check"></i></button>
            </span>
          </div></td>
         
          <td class="delete_option_append"><div class="input-group">
            <input type="text" class="form-control option_val_text" placeholder="option values" max-length="100" required="">
            <span class="input-group-btn">
              <button style="margin:0px!important;" class="btn btn-success save_option_val" type="submit"><i class="fa fa-check"></i></button>
            </span>
          </div>
          <?php 
     foreach($All_one_var as $each1)  
         {
          $new_variant11 = json_decode($each1->houdin_products_variants_new);

               $n1=0;
               foreach($new_variant11 as $new_v11)
             {
             if(($new_v11->variant_name)==$new_v1->variant_name) 
             {
              $vb = $new_v11->variant_value;
          
          ?>
          <div class="btn-group div_group added_option_btn" data-name=" <?php echo $vb; ?>">
          <button type="button" class="btn btn-primary btn-sm"><span class="default-text"> <?php echo $vb; ?></span>&nbsp;&nbsp;<span class="remove_option_value"><i class="fa fa-trash"></i></span></button>
        </div>
        
        <?php } } } ?>
         
          </td> 
          <td class="remove_option" style="padding-left: 10px"><span class="input-group-btn">
              <button style="margin:0px!important;" class="btn btn-success" type="submit"><i class="fa fa-trash"></i></button>
            </span></td>
            </tr>

           <?php } } }  ?>
            <?php } ?>

          </table>
          <div class="row">
            <div class="col-md-12">
                <input type="button" class="btn btn-success add_option" name="" value="Add Option">
            </div>
          </div>
</div>
</div>
</div>

<div class="modal-footer">

<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>



</div>

</div>

</div>

</div>

<!--- add stock popup-->
<div class="modal fade" id="add_stock_data" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title">Add stock Options</h4>
        </div>
            <form class="modal-body" id="addstockvariantsform">
                  <div class="Add_cost_quantity_of_variant">


                  </div>
                
                
          </form>
           
           
         
        
      </div>
    </div>
  </div>
<!--END Option-->

<!-- add category from add product --> 

        
<div id="add_catagory" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

<div class="modal-dialog">

<?php echo form_open(base_url().'Category/categorymanagement',array("id"=>'categoryForm',"enctype"=>"multipart/form-data"))?>
<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

<h4 class="modal-title">Add Category</h4>

</div>
<div class="modal-body">
<div class="row">
<div class="col-md-12">
<div class="form-group">
<input type="hidden" name="product_type" value="1"/>
<label for="field-7" class="control-label">Category Name</label>
<input type="text" name="category_name" class="form-control required_validation_for_category" placeholder="Catagory Name" />
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label for="field-7" class="control-label">Category Thumbnail</label>
<input type="file" name="category_thumb" class="form-control" />
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label for="field-7" class="control-label">Category Description</label>
<textarea class="form-control required_validation_for_category" name="category_desc" rows="4"></textarea>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label for="field-7" class="control-label">Category Status</label>
<select class="form-control required_validation_for_category" name="category_status">
<option value="">Choose Status</option>
<option value="active">Active</option>
<option value="deactive">Dective</option>
<option value="block">Block</option>
</select>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<button type="button" class="btn btn-info" id="add-sub" >Add Sub Category</button>
</div>
</div>
</div>


<div class="add-new"></div>
</div>

<div class="modal-footer">

<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

<input type="submit" class="btn btn-info " name="add_Form" value="Submit">

</div>

</div>
<?php echo form_close(); ?>
</div>

</div>
<!-- end add category -->
<input type="hidden" id="today-date" value="<?php echo date('Y-m-d') ?>" />
<input type="hidden" id="base-url" value="<?php echo base_url(); ?>" />
<input type="hidden" id="warehouse-list" value="<?php echo $warehous_list; ?>" />
<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom/validation-js/add-product/add-product-validation.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom/page-js/add-product/add-product.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom/page-js/product-category/add-category.js"></script>
<script type="text/javascript">
function checkDec(el){
  var ex = /^[0-9]+\.?[0-9]*$/;
  if(ex.test(el.value)==false) {
    el.value = el.value.substring(0,el.value.length - 1);
  }
}
function checkINt(el){
  var ex = /^[0-9]*$/;
  if(ex.test(el.value)==false) {
   el.value = el.value.substring(0,el.value.length - 1);
  }
}
</script>