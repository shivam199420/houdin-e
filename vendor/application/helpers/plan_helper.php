<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function getVendorplan()
{
    $CI =& get_instance();
    $masterDB = $CI->load->database('master',true);

    $getPackageInfoData = $masterDB->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id,houdin_users.houdin_users_package_id,houdin_packages.*')
    ->from('houdin_vendor_auth')->where('houdin_vendor_auth.houdin_vendor_auth_token',$CI->session->userdata('vendorAuth'))
    ->join('houdin_users','houdin_users.houdin_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id')
    ->join('houdin_packages','houdin_packages.houdin_package_id = houdin_users.houdin_users_package_id')->get()->result();
    return $getPackageInfoData;
}