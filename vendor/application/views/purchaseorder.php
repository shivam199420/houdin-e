    <?php $this->load->view('Template/header.php') ?>
    <?php $this->load->view('Template/sidebar.php') ?>
    <style type="text/css">
    @media screen and (min-width: 768px){}
    .dropdown.dropdown-lg .dropdown-menu {
    min-width: 390px!important;
    }
    }
    </style>
    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">

    <!--START Page-Title -->
    <div class="row">
  
    <div class="col-md-8">
    <h4 class="page-title">Purchase</h4>
    <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>">Home</a></li>
    <li class="active">Purchase</li>
    </ol>
    </div>
    <!-- <div class="col-md-4">
    <form role="search" class="navbar-left app-search pull-left custom_search_all">
    <input type="text" placeholder="Search..." class="form-control">
    <a href=""><i class="fa fa-search"></i></a>
    </form></div> -->

    </div>
    <!--END Page-Title -->
    <div class="row">
    <div class="col-sm-12">
    <?php 
    if($this->session->flashdata('success'))
    {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    else if($this->session->flashdata('error'))
    {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
    }
    ?>
    </div>
    </div>
    <div class="row m-t-20">

    <div class="col-lg-4 col-sm-6">
    <div class="widget-panel widget-style-2 bg-white">
    <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_purchase.png">
    <h2 class="m-0 text-dark counter font-600"><?php if($totalPurchase) { echo $totalPurchase; } else { echo 0; } ?></h2>
    <div class="text-muted m-t-5">Total purchase order</div>
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-12"> 

    <div class="card-box table-responsive"> 
    <div class="btn-group pull-right m-t-10 m-b-20">
    <a href="<?php echo base_url(); ?>Purchase/add" class="btn btn-default m-r-5" title="Add purchase order"><i class="fa fa-plus"></i></a>
    <button type="button" class="btn btn-default m-r-5 setSupplierMultiBtn deleteMultiplePurchase" title="Delete" style="display:none"><i class="fa fa-trash"></i></button>
    <button type="button" class="btn btn-default m-r-5 setSupplierMultiBtn" style="display:none" title="Send email to supplier" data-toggle="modal" data-target="#send"><i class="fa fa-envelope "></i></button>
    </div>
    <table class="table table-striped table-bordered table_shop_custom">
    <thead>
    <tr>
    <th><input type="checkbox" class="masterSupplierCheck"></th>
    <th>Order Id</th>
    <th>Supplier name</th>
    <th>Delivery date</th>
    <th>Credit period</th>
    <th>Status</th>
    <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php 
    foreach($purchaseList as $purchaseListData)
    {
        ?>   
        <tr>
    <td><input type="checkbox" class="childSupplierCheck" data-id="<?php echo $purchaseListData->houdinv_inventory_purchase_id ?>"></td>
    <td><?php echo $purchaseListData->houdinv_inventory_purchase_id ?></td>
    <td><?php echo $purchaseListData->houdinv_supplier_contact_person_name ?></td> 
    <td><?php echo date('d-m-Y',$purchaseListData->houdinv_inventory_purchase_delivery); ?> </td>
    <td class="text-uppercase"><?php echo $purchaseListData->houdinv_inventory_purchase_time." ".$purchaseListData->houdinv_inventory_purchase_credit ?></td>
    <td>
    <?php 
    if($purchaseListData->houdinv_inventory_purchase_status == 'recieve')
    {
        $setText = $purchaseListData->houdinv_inventory_purchase_status;
        $setClass = 'success';
    }
    else
    {
        $setText = $purchaseListData->houdinv_inventory_purchase_status;
        $setClass = 'warning';
    }
    ?>
    <button type="button" class="btn btn-<?php echo $setClass ?> text-uppercase btn-xs"><?php echo $setText ?></button>
    </td>
    <td>
    <button data-id="<?php echo $purchaseListData->houdinv_inventory_purchase_id ?>" class="btn btn-primary waves-effect waves-light deletePurcahseBtn">Delete</button>
    <!-- <a href="<?php echo base_url() ?>Purchase/edit/<?php //echo $purchaseListData->houdinv_inventory_purchase_id ?>" class="btn btn-primary waves-effect waves-light">Edit</a> -->
    <button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#send">Send Email</button>
    <a href="<?php echo base_url() ?>Purchase/inward/<?php echo $purchaseListData->houdinv_inventory_purchase_id ?>" class="btn btn-primary waves-effect waves-light">Inward</a>
    <?php 
    if($purchaseListData->houdinv_inventory_purchase_status == 'recieve')
    {
    ?>
    <a href="<?php echo base_url() ?>purchase/generateinvoice/<?php echo $purchaseListData->houdinv_inventory_purchase_id ?>" class="btn btn-primary" target="_blank">Generate Invoice</a>
    <?php }
    ?>
    
    </td>
    </tr>
    <?php }
    ?>
    </tbody>
    </table>
    <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>

 <!--Delete-->

    <div id="deletePurchaseModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <?php echo form_open(base_url( 'Purchase' ), array('method'=>'post'));?>
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Delete Purchase</h4>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-md-12">
    <input type="hidden" class="deletePurchaseId" name="deletePurchaseId"/>
    <h4><b>Do you really want to Delete this purchase ?</b></h4>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-info" name="deletePurchase" value="Delete">
    </div>
    </div>
    <?php echo form_close(); ?>
    </div>
    </div>
<!---Start Change Status -->
<div id="con-close-modal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
    <div class="modal-content"> 
    <div class="modal-header"> 
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
    <h4 class="modal-title">Inward</h4> 
    </div> 
    <div class="modal-body"> 
    <div class="row"> 
    <div class="col-md-12"> 
    <div class="form-group"> 
    <label for="field-1" class="control-label">Choose Status</label> 
    <select class="form-control">
    <option>Receive</option>
    <option>Return</option>

    </select>
    </div> 

    <div class="form-group">
    <label class="control-label">Note</label>

    <textarea type="text"  value="" class="form-control" name="" placeholder="Note"></textarea>

    </div>  
    </div> 

    </div> 



    </div> 
    <div class="modal-footer"> 
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
    </div> 
    </div> 
    </div>
    </div><!-- End Status -->

    <!---Start Add -->
    <div id="add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
    <div class="modal-content"> 
    <div class="modal-header"> 
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
    <h4 class="modal-title">Add purcahse order</h4> 
    </div> 
    <div class="modal-body"> 
    <div class="row"> 
    <div class="col-md-12 form-horizontal"> 

    <div class="form-group">
    <label class="col-md-3 control-label">Supplier</label>
    <div class="col-md-9">
    <select class="form-control">
    <option>Choose supplier</option>
    <option>Supplier-1</option>
    <option>Supplier-2</option>
    </select>
    </div>
    </div>
    <div class="form-group">
    <label class="col-md-3 control-label">Expected Delivery</label>
    <div class="col-md-9">
    <input type="text" value="" class="form-control" name="" placeholder="Expected Delivery">
    </div>
    </div>
    <div class="form-group">
    <label class="col-md-3 control-label">Credit period</label>
    <div class="col-md-9">
    <select class="form-control select2">

    <option>Select</option>
    <optgroup label="Alaskan/Hawaiian Time Zone">
    <option value="AK">Alaska</option>
    <option value="HI">Hawaii</option>
    </optgroup>
    <optgroup label="Pacific Time Zone">
    <option value="CA">California</option>
    <option value="NV">Nevada</option>
    <option value="OR">Oregon</option>
    <option value="WA">Washington</option>
    </optgroup>
    <optgroup label="Mountain Time Zone">
    <option value="AZ">Arizona</option>
    <option value="CO">Colorado</option>
    <option value="ID">Idaho</option>
    <option value="MT">Montana</option>
    <option value="NE">Nebraska</option>
    <option value="NM">New Mexico</option>
    <option value="ND">North Dakota</option>
    <option value="UT">Utah</option>
    <option value="WY">Wyoming</option>
    </optgroup>
    <optgroup label="Central Time Zone">
    <option value="AL">Alabama</option>
    <option value="AR">Arkansas</option>
    <option value="IL">Illinois</option>
    <option value="IA">Iowa</option>
    <option value="KS">Kansas</option>
    <option value="KY">Kentucky</option>
    <option value="LA">Louisiana</option>
    <option value="MN">Minnesota</option>
    <option value="MS">Mississippi</option>
    <option value="MO">Missouri</option>
    <option value="OK">Oklahoma</option>
    <option value="SD">South Dakota</option>
    <option value="TX">Texas</option>
    <option value="TN">Tennessee</option>
    <option value="WI">Wisconsin</option>
    </optgroup>
    <optgroup label="Eastern Time Zone">
    <option value="CT">Connecticut</option>
    <option value="DE">Delaware</option>
    <option value="FL">Florida</option>
    <option value="GA">Georgia</option>
    <option value="IN">Indiana</option>
    <option value="ME">Maine</option>
    <option value="MD">Maryland</option>
    <option value="MA">Massachusetts</option>
    <option value="MI">Michigan</option>
    <option value="NH">New Hampshire</option>
    <option value="NJ">New Jersey</option>
    <option value="NY">New York</option>
    <option value="NC">North Carolina</option>
    <option value="OH">Ohio</option>
    <option value="PA">Pennsylvania</option>
    <option value="RI">Rhode Island</option>
    <option value="SC">South Carolina</option>
    <option value="VT">Vermont</option>
    <option value="VA">Virginia</option>
    <option value="WV">West Virginia</option>
    </optgroup>
    </select>
    </div>
    </div>

    <div class="form-group">
    <div class="col-md-3">  <label for="userName">Search</label></div>
    <div class="col-md-9"> 
    <select class="form-control select2">
    <option>Select</option>
    <optgroup label="Alaskan/Hawaiian Time Zone">
    <option value="AK">Alaska</option>
    <option value="HI">Hawaii</option>
    </optgroup>
    <optgroup label="Pacific Time Zone">
    <option value="CA">California</option>
    <option value="NV">Nevada</option>
    <option value="OR">Oregon</option>
    <option value="WA">Washington</option>
    </optgroup>
    <optgroup label="Mountain Time Zone">
    <option value="AZ">Arizona</option>
    <option value="CO">Colorado</option>
    <option value="ID">Idaho</option>
    <option value="MT">Montana</option>
    <option value="NE">Nebraska</option>
    <option value="NM">New Mexico</option>
    <option value="ND">North Dakota</option>
    <option value="UT">Utah</option>
    <option value="WY">Wyoming</option>
    </optgroup>
    <optgroup label="Central Time Zone">
    <option value="AL">Alabama</option>
    <option value="AR">Arkansas</option>
    <option value="IL">Illinois</option>
    <option value="IA">Iowa</option>
    <option value="KS">Kansas</option>
    <option value="KY">Kentucky</option>
    <option value="LA">Louisiana</option>
    <option value="MN">Minnesota</option>
    <option value="MS">Mississippi</option>
    <option value="MO">Missouri</option>
    <option value="OK">Oklahoma</option>
    <option value="SD">South Dakota</option>
    <option value="TX">Texas</option>
    <option value="TN">Tennessee</option>
    <option value="WI">Wisconsin</option>
    </optgroup>
    <optgroup label="Eastern Time Zone">
    <option value="CT">Connecticut</option>
    <option value="DE">Delaware</option>
    <option value="FL">Florida</option>
    <option value="GA">Georgia</option>
    <option value="IN">Indiana</option>
    <option value="ME">Maine</option>
    <option value="MD">Maryland</option>
    <option value="MA">Massachusetts</option>
    <option value="MI">Michigan</option>
    <option value="NH">New Hampshire</option>
    <option value="NJ">New Jersey</option>
    <option value="NY">New York</option>
    <option value="NC">North Carolina</option>
    <option value="OH">Ohio</option>
    <option value="PA">Pennsylvania</option>
    <option value="RI">Rhode Island</option>
    <option value="SC">South Carolina</option>
    <option value="VT">Vermont</option>
    <option value="VA">Virginia</option>
    <option value="WV">West Virginia</option>
    </optgroup>
    </select>
    </div></div>
    <div class="form-group">
    <label class="col-md-3 control-label">Quantity</label>
    <div class="col-md-9">
    <input type="text" value="" class="form-control" name="" placeholder="Quantity">
    </div>
    </div>

    </div> 

    </div> 
    </div>  
    <div class="modal-footer"> 
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
    </div> 
    </div> 
    </div>
    </div><!-- End Add -->

    <!---Start Send -->
    <div id="send" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
    <div class="modal-content"> 
    <div class="modal-header"> 
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
    <h4 class="modal-title">Send Email</h4> 
    </div> 
    <div class="modal-body"> 
    <div class="row"> 
    <div class="col-md-12 form-horizontal"> 
    <div class="form-group">
    <label class="col-md-3 control-label">Subject</label>
    <div class="col-md-9">
    <input type="text" value="" class="form-control" name="" placeholder="Subject">
    </div>
    </div>

    <div class="form-group">
    <label class="col-md-3 control-label">Message</label>
    <div class="col-md-9">
    <textarea style="width:100%" type="text"  value="" class="form-control" name="" placeholder="Message"></textarea>
    </div>
    </div>  
    </div> 

    </div> 
    </div> 
    <div class="modal-footer"> 
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
    <button type="button" class="btn btn-info waves-effect waves-light">Send</button> 
    </div> 
    </div> 
    </div>
    </div><!-- End Send -->
    <div id="con-close-modal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
    <div class="modal-content"> 
    <div class="modal-header"> 
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
    <h4 class="modal-title">Add Inventory</h4> 
    </div> 
    <div class="modal-body"> 
    <div class="row"> 
    <div class="col-md-12"> 
    <div class="form-group"> 
    <label for="field-1" class="control-label">Choose Status</label> 
    <select class="form-control">
    <option>Receive</option>
    <option>Return</option>

    </select>
    </div> 
    </div> 

    </div> 
    <!--<div class="row"> 
    <div class="col-md-12"> 
    <div class="form-group"> 
    <label for="field-3" class="control-label">Note</label> 
    <input type="text" class="form-control" id="field-3" placeholder="Quantity"> 
    </div> 
    </div> 
    </div> -->

    <div class="row"> 
    <div class="col-md-12"> 
    <div class="form-group no-margin"> 
    <label for="field-7" class="control-label">Note</label> 
    <textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
    </div> 
    </div> 
    </div> 
    </div> 
    <div class="modal-footer"> 
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
    </div> 
    </div> 
    </div>
    </div><!-- /.modal -->
   
    <!---Start Send -->
    <div id="inward" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
    <div class="modal-content">  
    <div class="modal-header"> 
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
    <h4 class="modal-title">Inward</h4> 
    </div> 
    <div class="modal-body"> 
    <div class="row"> 
    <div class="col-md-12 form-horizontal"> 
    <div class="form-group">
    <label class="col-md-3 control-label">Status</label>
    <div class="col-md-9">
    <select class="form-control " name=""><option value="">Receive</option><option value="1">Return</option></select>
    </div>
    </div>

    <div class="form-group">
    <label class="col-md-3 control-label">Note</label>
    <div class="col-md-9">
    <textarea style="width:100%" type="text"  value="" class="form-control" name="" placeholder=""></textarea>
    </div>
    </div>  
    </div> 

    </div> 
    </div> 
    <div class="modal-footer"> 
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
    <button type="button" class="btn btn-info waves-effect waves-light">Send</button> 
    </div> 
    </div> 
    </div>
    </div><!-- End Send -->

    <!--change Status-->
    <div id="Change-status" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">
    <form method="post">
    <div class="modal-content">

    <div class="modal-header">

    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

    <h4 class="modal-title">Change Status</h4>

    </div>

    <div class="modal-body">



    <div class="row">
    <div class="col-md-12">

    <div class="form-group no-margin">

    <!-- <label for="field-7" class="control-label">Change Status</label>-->

    <select class="form-control " name=""><option value="">Choose Status</option><option value="1">Active</option><option value="0">Deactive</option><option value="2">Block</option></select>

    </div>

    </div>
    </div>





    </div>

    <div class="modal-footer">

    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

    <input type="submit" class="btn btn-info " name="" value="Update Status">

    </div>

    </div>
    </form>
    </div>

    </div> 

    <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
    <div class="modal-content"> 
    <div class="modal-header"> 
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
    <h4 class="modal-title">Add purchase order</h4> 
    </div> 
    <div class="modal-body"> 
    <div class="row"> 
    <div class="col-md-6"> 
    <div class="form-group"> 
    <label for="field-1" class="control-label">Select Product</label> 
    <select class="form-control">
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
    <option>5</option>
    </select>
    </div> 
    </div> 
    <div class="col-md-6"> 
    <div class="form-group"> 
    <label for="field-2" class="control-label">Select Supplier</label> 
    <select class="form-control">
    <option>Supplier1</option>
    <option>Supplier2</option>
    <option>Supplier3</option>
    <option>Supplier4</option>
    <option>Supplier5</option>
    </select> 
    </div> 
    </div> 
    </div> 
    <div class="row"> 
    <div class="col-md-12"> 
    <div class="form-group"> 
    <label for="field-3" class="control-label">Quantity</label> 
    <input type="text" class="form-control" id="field-3" placeholder="Quantity"> 
    </div> 
    </div> 
    </div> 
    <div class="row"> 
    <div class="col-md-6"> 
    <div class="form-group"> 
    <label for="field-4" class="control-label">Delivery date</label> 
    <input type="text" class="form-control date_picker" id="field-4" placeholder="Delivery date"> 
    </div> 
    </div> 
    <div class="col-md-6"> 
    <div class="form-group"> 
    <label for="field-5" class="control-label">Credit period</label> 
    <input type="text" class="form-control" id="field-5" placeholder="Credit period"> 
    </div> 
    </div> 

    </div> 
    <!--<div class="row"> 
    <div class="col-md-12"> 
    <div class="form-group no-margin"> 
    <label for="field-7" class="control-label">Personal Info</label> 
    <textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
    </div> 
    </div> 
    </div> -->
    </div> 
    <div class="modal-footer"> 
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
    </div> 
    </div> 
    </div>
    </div><!-- /.modal -->

    <?php $this->load->view('Template/footer.php') ?>
    <script>
    $(document).ready(function(){
    $(".select2").select2();
    $(".click_show").click(function(){
    $(".hide_div").show();
    $(".click_show").hide();
    });
    $(".click_hide").click(function(){
    $(".hide_div").hide();
    $(".click_show").show();
    });
    });
    </script>
