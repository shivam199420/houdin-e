<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                               
                                <h4 class="page-title">Payments</h4>
                            </div>
                        </div>
                        <div class="row">
                    
	                       <div class="col-md-6 col-lg-4">
	                          <div class="widget-panel widget-style-2 bg-light">
	                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Transaction.png">
	                              <h2 class="m-0 text-dark font-600"><b class="">&#8377;<?php if($totalTransactionData) { echo $totalTransactionData; } else { echo 0; } ?></b></h2>
	                              <div class="text-muted m-t-5 custom_width_sm">Total Transcation</div>
	                          </div>
	                       </div>

	                       <div class="col-md-6 col-lg-4">
	                          <div class="widget-panel widget-style-2 bg-light">
	                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Credit.png">
	                              <h2 class="m-0 text-dark font-600"><b class="">&#8377;<?php if($totalCreditTransaction) { echo $totalCreditTransaction; } else { echo 0; } ?></b></h2>
	                              <div class="text-muted m-t-5 custom_width_sm">Total Credit</div>
	                          </div>
	                       </div>

	                       <div class="col-md-6 col-lg-4">
	                          <div class="widget-panel widget-style-2 bg-light">
	                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Debit.png">
	                              <h2 class="m-0 text-dark font-600"><b class="">&#8377;<?php if($totalDebitTransaction) { echo $totalDebitTransaction; } else { echo 0; } ?></b></h2>
	                              <div class="text-muted m-t-5 custom_width_sm">Total Debit</div>
	                          </div>
	                       </div>


                        </div>
                        <div class="row">
                          <div class="col-md-12 m-t-30">
                            <div class="card-box">
                          <div id="setTransaction" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                            </div></div></div>
                        <div class="row">
                          <div class="col-md-12 m-t-30">
                            <div class="card-box table-responsive">
                              <table class="table table-striped table-bordered table_shop_custom">
                                <thead>
                                  <tr>
                                    <th>Transaction</th>
                                    <th>Transaction For</th>
                                    <th>Transaction Amount</th>
                                    <th>Transaction Date</th>
                                    <th>Transaction Status</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <?php 
                                foreach($transactionData as $transactionDataList)
                                {
                                ?>
                                 <tr>
                                    <td><?php 
                                    if($transactionDataList->houdin_admin_transaction_type == 'debit')
                                    {
                                      $setText = "Debit";
                                      $setColor = "red";
                                    }
                                    else
                                    {
                                      $setText = "Credit";
                                      $setColor = "green";
                                    }
                                    echo $transactionDataList->houdin_admin_transaction_transaction_id ?><br/><label style="color:<?php echo $setColor ?> !important"><?php echo $setText; ?></label></td>
                                    <td><?php echo $transactionDataList->houdin_admin_transaction_for ?></td>
                                    <td><?php echo $transactionDataList->houdin_admin_transaction_amount ?></td>
                                    <td><?php echo date('d-m-Y',strtotime($transactionDataList->houdin_admin_transaction_date)) ?></td>
                                    <td>
                                    <?php
                                     if($transactionDataList->houdin_admin_transaction_status == 'success')
                                     {
                                        $setText = 'Success';
                                        $setClass = 'success';
                                     }
                                     else
                                     {
                                      $setText = 'Fail';
                                      $setClass = 'danger';
                                     }
                                     ?>
                                    <button type="button" class="btn btn-<?php echo $setClass ?> btn btn-xs"><?php echo $setText; ?></button></td>
                                  </tr>
                                <?php }
                                ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->
<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">

Highcharts.chart('setTransaction', {
    chart: {
        type: 'area',
        spacingBottom: 30
    },
    title: {
        text: 'Transaction'
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    xAxis: {
        categories: [<?php echo $date ?>]
    },
    yAxis: {
        title: {
            text: 'Y-Axis'
        },
        labels: {
            formatter: function () {
                return this.value;
            }
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.x + ': ' + this.y;
        }
    },
    plotOptions: {
        area: {
            fillOpacity: 0.5
        }
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Total Transaction',
        data: [<?php echo $totalTransaction; ?>]
    }, {
        name: 'Total Credit',
        data: [<?php echo $totalCredit; ?>]
    },
    {
        name: 'Total debit',
        data: [<?php echo $totalDebit ?>]
    }]
});
</script>
