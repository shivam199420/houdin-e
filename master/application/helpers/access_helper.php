<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function access($department)
{
    $CI =& get_instance();
    $getDepartment = $CI->db->select('houdin_admin_department_access')->from('houdin_admin_department',$department)->get()->result();
    $setExplodeValue = explode(',',$getDepartment[0]->houdin_admin_department_access);
    return $setExplodeValue;
}

