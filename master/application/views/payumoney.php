<?php $this->load->view('Template/header.php') ?>
    <?php $this->load->view('Template/sidebar.php') ?>
    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">
    <!-- Page-Title -->
    <div class="row">
    <div class="col-sm-12 m-b-15">
    <h4 class="page-title">PayuMoney Credentias</h4>

    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
    <?php 
        if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        else if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        ?>
  </div>
  </div>
    <div class="row">
    <?php echo form_open(base_url( 'Setting/payumoney' ), array( 'id' => 'addpayumoneyCredentials' ));?>
    <div class="col-md-12">
    <div class="card-box row">
    <div class="form-group">
    <label>Merchant Key</label>
    <input type="hidden" name="payumoneyId" value="<?php echo $payu[0]->houdin_payumoney ?>"/>
    <input type="text" name="merchantKey" value="<?php echo $payu[0]->houdin_payumoney_key ?>" class="form-control required_validation_for_add_payumoney_credentials name_validation" placeholder="Enter Merchant Key" />
    </div>
    <div class="form-group">
    <label>Merchant Salt</label>
    <input type="text" name="merchantSalt" value="<?php echo $payu[0]->houdin_payumoney_salt ?>" class="form-control required_validation_for_add_payumoney_credentials name_validation" placeholder="Enter Merchant Salt" />
    </div>

    <div class="col-md-12 ">
    <input type="submit" class="btn btn-default pull-right m-r-10" name="updatepayumoneyCredentials" value="Submit">
    <button type="reset" class="btn btn-info pull-right m-r-10">Reset</button>
    </div>
    </div>
    </div>
    <?php echo form_close(); ?>
    </div>


    </div> <!-- container -->

    </div> <!-- content -->

    <?php $this->load->view('Template/footer.php') ?>
<!-- CLient side form validation -->
<script type="text/javascript">
            $(document).ready(function(){
                $(document).on('submit','#addpayumoneyCredentials',function(){
                    var check_required_field='';
                    $(this).find(".required_validation_for_add_payumoney_credentials").each(function(){
                        var val22 = $(this).val();
                        if (!val22){
                            check_required_field =$(this).size();
                            $(this).css("border-color","#ccc");
                            $(this).css("border-color","red");
                        }
                        $(this).on('keypress change',function(){
                            $(this).css("border-color","#ccc");
                        });
                    });
                    if(check_required_field)
                    {
                        return false;
                    }
                    else {
                        return true;
                    }
                });
            });
            </script>