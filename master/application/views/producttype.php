<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <div class="btn-group pull-right m-t-15">
                                    <a href="<?php echo base_url(); ?>Product/addproducttype" class="btn btn-default btn-sm m-l-5" > <i class="fa fa-plus m-r-5"></i>Add Product Type</a>
                                </div>
                                <h4 class="page-title">Product Type</h4>

                            </div>
                        </div>


        <div class="row">
          <div class="col-md-12">
            <div class="card-box table-responsive">
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th>Type Name</th>
                    <th>Cataogry</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Surbhi</td>
                    <td>30-12-2018</td>
                    <td><button type="button" class="btn btn-warning btn btn-xs">Deactive</button></td>
                    <td>
                        <a href="<?php echo base_url(); ?>Product/editproducttype" class="btn btn-warning btn btn-sm"><i class="fa fa-pencil m-r-5"></i>Edit</a>
                        <button type="button" class="btn btn-warning btn btn-sm" data-toggle="modal" data-target="#delete_product_type"><i class="fa fa-trash m-r-5"></i>Delete</button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>


                    </div> <!-- container -->

                </div> <!-- content -->





  <div id="delete_product_type" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog">
  <form method="">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
  <h4 class="modal-title">Delete Entry</h4>
  </div>
  <div class="modal-body">
  <div class="row">
  <div class="col-md-12">
  <h4><b>Do you really want to delete this entry ?</b></h4>
  </div>
  </div>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
  <input type="submit" class="btn btn-info" name="" value="Delete">
  </div>
  </div>
  </form>
  </div>
  </div>
  <?php $this->load->view('Template/footer.php') ?>
