<?php  $this->load->view('Template/header');?> 
 
<header class="header bg-gray header-page-top">
  <div class="header-page-top-inner">
	  <div class="container">
		<h1 class="display-4">Ecommerce Themes</h1> 
  </div>
	</div>
</header>


  <section class="section bg-gray">
        <div class="container">
          <div class="row gap-y">
			  <aside class="col-md-3 col-xl-3 col-xs-12">
			   <h5 class="sidebar-title">Categories</h5> 
					 
                 
				  <div class="nav flex-column sidebar_link_box">
					<a class="nav-link" href="">Premium</a>
					<a class="nav-link" href="">Art & Crafts</a>
					<a class="nav-link" href="">Books & Stationary</a>
					<a class="nav-link" href="">Fashion & Accessories</a>
					<a class="nav-link" href="">Gifts & Flowers</a>
					<a class="nav-link" href="">Food & Drinks</a>
					<a class="nav-link" href="">Footwear</a>
					<a class="nav-link" href="">Jewellery</a>
					<a class="nav-link" href="">Furniture</a>
					<a class="nav-link" href="">Apparel</a>
					<a class="nav-link" href="">Electronics</a>
					<a class="nav-link" href="">Health & Beauty</a>
					<a class="nav-link" href="">Home & Decor</a>
					<a class="nav-link" href="">Toys & Games</a>
					<a class="nav-link" href="">Sports</a>
					<a class="nav-link" href="">Informative</a> 
					  </div>
				  
			  </aside> <!-- end sidebar -->
			  
			  	<div class="col-md-9 col-xl-9">
					<div class="row gap-y">
						 <div class="col-md-6 col-xl-4">
						  <div class="product-3 mb-3">
							<a class="product-media theme-product-overlay" data-caption="Apple EarPods" href="<?php echo base_url(); ?>assets/img/blog/1.jpg" data-fancybox="ecomerce-theme">
							  <span class="badge badge-pill badge-primary badge-pos-left">New</span>
							  <img src="<?php echo base_url(); ?>assets/img/blog/1.jpg" alt="product">
							</a>

							<div class="product-detail">
							  <h6>Apple EarPods</h6> 
							</div>
						  </div>
						</div>

						<div class="col-md-6 col-xl-4">
						  <div class="product-3 mb-3">
							<a class="product-media theme-product-overlay" href="<?php echo base_url(); ?>assets/img/blog/2.jpg" data-caption="Sony PlayStation 4" data-fancybox="ecomerce-theme">
							  <span class="badge badge-pill badge-success badge-pos-right">-25%</span>
							  <img src="<?php echo base_url(); ?>assets/img/blog/2.jpg" alt="product">
							</a>

							<div class="product-detail">
							  <h6>Sony PlayStation 4</h6> 
							</div>
						  </div>
						</div>


						<div class="col-md-6 col-xl-4">
						  <div class="product-3 mb-3">
							<a class="product-media theme-product-overlay" href="<?php echo base_url(); ?>assets/img/blog/3.jpg" data-caption="Hanging Speaker" data-fancybox="ecomerce-theme">
							  <span class="badge badge-pill badge-success badge-pos-right">-20%</span>
							  <img src="<?php echo base_url(); ?>assets/img/blog/3.jpg" alt="product">
							</a>

							<div class="product-detail">
							  <h6>Hanging Speaker</h6>
							  
							</div>
						  </div>
						</div>


						<div class="col-md-6 col-xl-4">
						  <div class="product-3 mb-3">
							<a class="product-media theme-product-overlay" href="<?php echo base_url(); ?>assets/img/blog/4.jpg" data-caption="Beats On-Ear" data-fancybox="ecomerce-theme">
							  <img src="<?php echo base_url(); ?>assets/img/blog/4.jpg" alt="product">
							</a>
							<div class="product-detail">
							  <h6>Beats On-Ear</h6>
							   
							</div>
						  </div>
						</div>


							<div class="col-md-6 col-xl-4">
								<div class="product-3 mb-3">                
									<a class="product-media theme-product-overlay" href="<?php echo base_url(); ?>assets/img/blog/1.jpg" data-caption="Shiny Shoe" data-fancybox="ecomerce-theme">
									  <img src="<?php echo base_url(); ?>assets/img/blog/5.jpg" alt="product">
									</a>
									<div class="product-detail">
									  <h6>Shiny Shoe</h6>
									   
									</div>
							  </div>
							</div>
					</div>
          		</div> <!-- end content -->
			  
			  
			</div> <!-- end row main -->
		 

        </div>
      </section>

  

  	

<?php  $this->load->view('Template/footer');?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" /> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>