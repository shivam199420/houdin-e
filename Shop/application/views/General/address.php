<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<style type="text/css">
    .address_border{
        /*border: 1px solid gray;
    padding: 2%;
    border-radius: 10px;
    margin: 2%;*/
        padding-bottom: 5%;
    }
    .address_bottom{
    border-bottom: 1px solid gray;
     width: 100%;
    float: left;
    }
    .billing-details-area {
    background-color: #f6f6f6;
    padding: 17px 20px;
    box-shadow: 2px 4px 3px #80808075;
    margin-bottom: 15px;
}
.M_T{
    margin-top: 5%;
}
@media(max-width: 767px)
{
   .address_bottom{
   
    margin-bottom: 10px !important;
    }  
}

</style>
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">My Address</h2>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- login area end -->
    <div class="login-area ptb-100">
        <div class="container">
            <div class="row">
            <div class="col-md-12">
            <?php
            if($this->session->flashdata('error'))
            {
                echo "<div class='alert alert-danger' style='width:100%;'>".$this->session->flashdata('error')."</div>";
            }
            else if($this->session->flashdata('success'))
            {
                echo "<div class='alert alert-success' style='width:100%;'>".$this->session->flashdata('success')."</div>";
            }
            ?>
             <?php
            if(count($userAddress) > 0)
            {
            ?>
           <a href="<?php echo base_url() ?>Address/add" class="search-button2 pull-right" style="margin-bottom:10px">Add New Address</a>
            <?php } ?>
             <?php
            if(!count($userAddress) > 0)
            {
            ?>
                <div class="col-md-6">
                    <div class="login-content">
                        <div class="login-title">
                            <h4>Add address to shop better</h4>
                            <h4 class="text-center">and quick.</h4>
                        </div>
                        <div class="login-form">

                                <div class="button-remember text-center">
                                    <a href="<?php echo base_url() ?>Address/add" style="font-size: 15px;color: #fff;font-weight: 500;" class="login-btn">Add new address</a>
                                </div>

                        </div>
                    </div>
                </div>
            <?php }
            else {
                ?>

            <?php
             }
            ?>
            </div>
            </div>
            <div class="row">
              <?php
              foreach($userAddress as $userAddressList)
              {
               ?>
                <div class="col-md-4 ">
                    <div class="col-md-12 billing-details-area">
                        <div class="col-md-12 address_bottom">
                        <div class="pull-left">
                            <h5 ><?php echo $userAddressList->houdinv_user_address_name; ?></h5>
                        </div>
                        <div class="pull-right">
                                <a href="<?php echo base_url() ?>Address/edit/<?php echo $userAddressList->houdinv_user_address_id ?>">
                                  <i class="fa fa-edit" style="padding-right: 5px;padding-left:5px;font-size: 25px;"></i>
                                  </a>&nbsp;&nbsp;
                                  <a href="<?php echo base_url() ?>Address/deleteAddress/<?php echo $userAddressList->houdinv_user_address_id ?>">
                                <i class="fa fa-trash" style="padding-right: 5px;padding-left: 5px;font-size: 25px;"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12 M_T">
                            <form action="#">
                                <div class="">
                                    <h5>Address</h5>
                                    <address><?php echo $userAddressList->houdinv_user_address_user_address ?>,<br>
                                    <?php echo $userAddressList->houdinv_user_address_city ?>,<br>
                                    <?php echo $userAddressList->houdinv_user_address_zip ?>
                                    </address>
                                    <p class=""><?php echo $userAddressList->houdinv_user_address_phone;?></p>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
              <?php } ?>

            </div>
        </div>
    </div>

    <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>
