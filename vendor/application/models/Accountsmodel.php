<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class accountsmodel extends CI_Model{
  function __construct() 
  {
    parent::__construct();
    $this->load->helper('database');
    $getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
  }
  public function fetchAccountsData()
  {
      $getTotalEarning = $this->db->select('SUM(houdinv_orders_total_paid) as totalEarning')->from('houdinv_orders')->get()->result();
      $getTotalEarningWeb = $this->db->select('SUM(houdinv_orders_total_paid) as totalEarningWeb')->from('houdinv_orders')->where('houdinv_order_type','Website')->get()->result();
      $getTotalEarningApp = $this->db->select('SUM(houdinv_orders_total_paid) as totalEarningApp')->from('houdinv_orders')->where('houdinv_order_type','App')->get()->result();
      $getTotalEarningStore = $this->db->select('SUM(houdinv_orders_total_paid) as totalEarningStore')->from('houdinv_orders')->where('houdinv_order_type','Store')->get()->result();
    //   fetch transaction data
    $getTotalTransaction = $this->db->select('SUM(houdinv_transaction_amount) AS totalTransaction')->from('houdinv_transaction')->get()->result();
    $setTotalTransaction = number_format((float)$getTotalTransaction[0]->totalTransaction, 2, '.', '');
    // fetch total debit
    $getTotalDebitTransaction = $this->db->select('SUM(houdinv_transaction_amount) AS totalDebitTransaction')->from('houdinv_transaction')->where('houdinv_transaction_type','debit')->get()->result();
    $setTotalDebitTransaction = number_format((float)$getTotalDebitTransaction[0]->totalDebitTransaction, 2, '.', '');
    // fetch total credit 
    $getTotalCreditTransaction = $this->db->select('SUM(houdinv_transaction_amount) AS totalCreditTransaction')->from('houdinv_transaction')->where('houdinv_transaction_type','credit')->get()->result();
    $setTotalCreditTransaction = number_format((float)$getTotalCreditTransaction[0]->totalCreditTransaction, 2, '.', '');
      // fetch last 10 days earning
      $setDateData = "";
      $setRevenueData = "";
      for($index = 0; $index < 11; $index++)
      {
        $getDateData = date('Y-m-d', strtotime('-'.$index.' days'));
        $getDateDataValue = date('d-m-Y', strtotime('-'.$index.' days'));
        $getStrDate = strtotime($getDateData);
        $getEarning = $this->db->select('SUM(houdinv_orders_total_paid) as totalrevenueData')->from('houdinv_orders')->where('houdinv_order_created_at',$getStrDate)->get()->result();
        if($getEarning[0]->totalrevenueData)
        {
            $setRevenue = $getEarning[0]->totalrevenueData;
        }
        else
        {
            $setRevenue = 0;
        }
        if($setDateData)
        {
            $setDateData = $setDateData.","."'".$getDateDataValue."'";
        }
        else
        {
            $setDateData = "'".$getDateDataValue."'";
        }
        if($setRevenueData)
        {
            $setRevenueData = $setRevenueData.",".$setRevenue;   
        }
        else
        {
            $setRevenueData = $setRevenue;  
        }
    }
      return array('totalEarning'=>number_format((float)$getTotalEarning[0]->totalEarning, 2, '.', ''),
      'totalEarningWeb'=>number_format((float)$getTotalEarningWeb[0]->totalEarningWeb, 2, '.', ''),
      'totalEarningApp'=>number_format((float)$getTotalEarningWeb[0]->totalEarningApp, 2, '.', ''),
      'totalEarningStore'=>number_format((float)$getTotalEarningStore[0]->totalEarningStore, 2, '.', ''),
      'revenueDate'=>$setDateData,'totalRevenue'=>$setRevenueData,'totalTransaction'=>$setTotalTransaction,'totalDebit'=>$setTotalDebitTransaction,'totalCredit'=>$setTotalCreditTransaction);
  }
  public function fetchOrderReports($data)
  {
      if($data['searchType'] == 'ccd')
      {
        $getDateFrom = strtotime($data['dateFrom']);
        $getDateTo = strtotime($data['dateTo']);
        $getOrderData = $this->db->select('COUNT(houdinv_order_id) AS totalOrderData,SUM(houdinv_orders_discount) AS totalOrderDiscount,SUM(houdinv_orders_total_Amount) AS totalOrderAmount,
        SUM(houdinv_orders_total_paid) AS totalPaidAmount,SUM(houdinv_orders_total_remaining) AS totalRemainingAmount')
        ->from('houdinv_orders')->where('houdinv_order_created_at >=',$getDateFrom)->where('houdinv_order_created_at <=',$getDateTo)->get()->result();
        //   get statement from date
        $getStatetmentFromDate = $this->db->select('houdinv_order_created_at')->from('houdinv_orders')->where('houdinv_order_created_at >=',$getDateFrom)->where('houdinv_order_created_at <=',$getDateTo)
        ->order_by('houdinv_order_id','ASC')->limit('1')->get()->result();
        //   get statement to date
        $getStatetmentToDate = $this->db->select('houdinv_order_created_at')->from('houdinv_orders')->where('houdinv_order_created_at >=',$getDateFrom)->where('houdinv_order_created_at <=',$getDateTo)
        ->order_by('houdinv_order_id','DESC')->limit('1')->get()->result();
        return array('orderData'=>$getOrderData,'statementFrom'=>$getStatetmentFromDate,'statementTo'=>$getStatetmentToDate);
      }
      else if($data['searchType'] == 'cm')
      {
        //   get order data
          $getOrderData = $this->db->select('COUNT(houdinv_order_id) AS totalOrderData,SUM(houdinv_orders_discount) AS totalOrderDiscount,SUM(houdinv_orders_total_Amount) AS totalOrderAmount,
          SUM(houdinv_orders_total_paid) AS totalPaidAmount,SUM(houdinv_orders_total_remaining) AS totalRemainingAmount')
          ->from('houdinv_orders')->where('MONTH(FROM_UNIXTIME(houdinv_order_created_at))= MONTH(CURDATE())')->get()->result();
        //   get statement from date
          $getStatetmentFromDate = $this->db->select('houdinv_order_created_at')->from('houdinv_orders')->where('MONTH(FROM_UNIXTIME(houdinv_order_created_at))= MONTH(CURDATE())')
          ->order_by('houdinv_order_id','ASC')->limit('1')->get()->result();
          //   get statement to date
          $getStatetmentToDate = $this->db->select('houdinv_order_created_at')->from('houdinv_orders')->where('MONTH(FROM_UNIXTIME(houdinv_order_created_at))= MONTH(CURDATE())')
          ->order_by('houdinv_order_id','DESC')->limit('1')->get()->result();
          return array('orderData'=>$getOrderData,'statementFrom'=>$getStatetmentFromDate,'statementTo'=>$getStatetmentToDate);
      }
      else if($data['searchType'] == 'tcy')
      {
          //   get order data
          $getOrderData = $this->db->select('COUNT(houdinv_order_id) AS totalOrderData,SUM(houdinv_orders_discount) AS totalOrderDiscount,SUM(houdinv_orders_total_Amount) AS totalOrderAmount,
          SUM(houdinv_orders_total_paid) AS totalPaidAmount,SUM(houdinv_orders_total_remaining) AS totalRemainingAmount')
          ->from('houdinv_orders')->where('YEAR(FROM_UNIXTIME(houdinv_order_created_at))= YEAR(CURDATE())')->get()->result();
        //   get statement from date
          $getStatetmentFromDate = $this->db->select('houdinv_order_created_at')->from('houdinv_orders')->where('YEAR(FROM_UNIXTIME(houdinv_order_created_at))= YEAR(CURDATE())')
          ->order_by('houdinv_order_id','ASC')->limit('1')->get()->result();
          //   get statement to date
          $getStatetmentToDate = $this->db->select('houdinv_order_created_at')->from('houdinv_orders')->where('YEAR(FROM_UNIXTIME(houdinv_order_created_at))= YEAR(CURDATE())')
          ->order_by('houdinv_order_id','DESC')->limit('1')->get()->result();
          return array('orderData'=>$getOrderData,'statementFrom'=>$getStatetmentFromDate,'statementTo'=>$getStatetmentToDate);
      }
      else if($data['searchType'] == 'cw')
      {
          //   get order data
          $getOrderData = $this->db->select('COUNT(houdinv_order_id) AS totalOrderData,SUM(houdinv_orders_discount) AS totalOrderDiscount,SUM(houdinv_orders_total_Amount) AS totalOrderAmount,
          SUM(houdinv_orders_total_paid) AS totalPaidAmount,SUM(houdinv_orders_total_remaining) AS totalRemainingAmount')
          ->from('houdinv_orders')->where('WEEK(FROM_UNIXTIME(houdinv_order_created_at))= WEEK(CURDATE())')->get()->result();
        //   get statement from date
          $getStatetmentFromDate = $this->db->select('houdinv_order_created_at')->from('houdinv_orders')->where('WEEK(FROM_UNIXTIME(houdinv_order_created_at))= WEEK(CURDATE())')
          ->order_by('houdinv_order_id','ASC')->limit('1')->get()->result();
          //   get statement to date
          $getStatetmentToDate = $this->db->select('houdinv_order_created_at')->from('houdinv_orders')->where('WEEK(FROM_UNIXTIME(houdinv_order_created_at))= WEEK(CURDATE())')
          ->order_by('houdinv_order_id','DESC')->limit('1')->get()->result();
          return array('orderData'=>$getOrderData,'statementFrom'=>$getStatetmentFromDate,'statementTo'=>$getStatetmentToDate);
      }
      else if($data['searchType'] == 'cd')
      {
          //   get order data
          $getCurrentDay = strtotime(date('Y-m-d'));
          $getOrderData = $this->db->select('COUNT(houdinv_order_id) AS totalOrderData,SUM(houdinv_orders_discount) AS totalOrderDiscount,SUM(houdinv_orders_total_Amount) AS totalOrderAmount,
          SUM(houdinv_orders_total_paid) AS totalPaidAmount,SUM(houdinv_orders_total_remaining) AS totalRemainingAmount')
          ->from('houdinv_orders')->where('houdinv_order_created_at',$getCurrentDay)->get()->result();
        //   get statement from date
          $getStatetmentFromDate = $this->db->select('houdinv_order_created_at')->from('houdinv_orders')->where('houdinv_order_created_at',$getCurrentDay)
          ->order_by('houdinv_order_id','ASC')->limit('1')->get()->result();
          //   get statement to date
          $getStatetmentToDate = $this->db->select('houdinv_order_created_at')->from('houdinv_orders')->where('houdinv_order_created_at',$getCurrentDay)
          ->order_by('houdinv_order_id','DESC')->limit('1')->get()->result();
          return array('orderData'=>$getOrderData,'statementFrom'=>$getStatetmentFromDate,'statementTo'=>$getStatetmentToDate);
      }
  }
  public function fetchTransactionReports($data)
  {
    if($data['searchType'] == 'ccd')
    {
        $getDateFrom = $data['dateFrom'];
        $getDateTo = $data['dateTo'];
        $getTotalTransaction = $this->db->select('COUNT(houdinv_transaction_id) AS totalTransaction')->from('houdinv_transaction')->where('houdinv_transaction_date >=',$getDateFrom)->where('houdinv_transaction_date <=',$getDateTo)->get()->result();
        $getTotalCredit = $this->db->select('SUM(houdinv_transaction_amount) AS totalCreditTransaction')->from('houdinv_transaction')->where('houdinv_transaction_date >=',$getDateFrom)->where('houdinv_transaction_date <=',$getDateTo)->where('houdinv_transaction_type','credit')->get()->result();
        $getTotalDebit = $this->db->select('SUM(houdinv_transaction_amount) AS totalDebitTransaction')->from('houdinv_transaction')->where('houdinv_transaction_date >=',$getDateFrom)->where('houdinv_transaction_date <=',$getDateTo)->where('houdinv_transaction_type','debit')->get()->result();
        $getStattmentFromDate = $this->db->select('houdinv_transaction_date')->from('houdinv_transaction')->where('houdinv_transaction_date >=',$getDateFrom)->where('houdinv_transaction_date <=',$getDateTo)->order_by('houdinv_transaction_date','ASC')->limit('1')->get()->result();
        $getStattmentToDate = $this->db->select('houdinv_transaction_date')->from('houdinv_transaction')->where('houdinv_transaction_date >=',$getDateFrom)->where('houdinv_transaction_date <=',$getDateTo)->order_by('houdinv_transaction_date','DESC')->limit('1')->get()->result();
        return array('totalTransaction'=>$getTotalTransaction[0]->totalTransaction,'totalCredit'=>$getTotalCredit[0]->totalCreditTransaction,
        'totalDebit'=>$getTotalDebit[0]->totalDebitTransaction,'from'=>$getStattmentFromDate[0]->houdinv_transaction_date,'to'=>$getStattmentToDate[0]->houdinv_transaction_date);
    }
    else if($data['searchType'] == 'cm')
    {
        $getTotalTransaction = $this->db->select('COUNT(houdinv_transaction_id) AS totalTransaction')->from('houdinv_transaction')->where('MONTH(houdinv_transaction_date)= MONTH(CURDATE())')->get()->result();
        $getTotalCredit = $this->db->select('SUM(houdinv_transaction_amount) AS totalCreditTransaction')->from('houdinv_transaction')->where('MONTH(houdinv_transaction_date)= MONTH(CURDATE())')->where('houdinv_transaction_type','credit')->get()->result();
        $getTotalDebit = $this->db->select('SUM(houdinv_transaction_amount) AS totalDebitTransaction')->from('houdinv_transaction')->where('MONTH(houdinv_transaction_date)= MONTH(CURDATE())')->where('houdinv_transaction_type','debit')->get()->result();
        $getStattmentFromDate = $this->db->select('houdinv_transaction_date')->from('houdinv_transaction')->where('MONTH(houdinv_transaction_date)= MONTH(CURDATE())')->order_by('houdinv_transaction_date','ASC')->limit('1')->get()->result();
        $getStattmentToDate = $this->db->select('houdinv_transaction_date')->from('houdinv_transaction')->where('MONTH(houdinv_transaction_date)= MONTH(CURDATE())')->order_by('houdinv_transaction_date','DESC')->limit('1')->get()->result();
        return array('totalTransaction'=>$getTotalTransaction[0]->totalTransaction,'totalCredit'=>$getTotalCredit[0]->totalCreditTransaction,
        'totalDebit'=>$getTotalDebit[0]->totalDebitTransaction,'from'=>$getStattmentFromDate[0]->houdinv_transaction_date,'to'=>$getStattmentToDate[0]->houdinv_transaction_date);
    }
    else if($data['searchType'] == 'cw')
    {
        $getTotalTransaction = $this->db->select('COUNT(houdinv_transaction_id) AS totalTransaction')->from('houdinv_transaction')->where('WEEK(houdinv_transaction_date)= WEEK(CURDATE())')->get()->result();
        $getTotalCredit = $this->db->select('SUM(houdinv_transaction_amount) AS totalCreditTransaction')->from('houdinv_transaction')->where('WEEK(houdinv_transaction_date)= WEEK(CURDATE())')->where('houdinv_transaction_type','credit')->get()->result();
        $getTotalDebit = $this->db->select('SUM(houdinv_transaction_amount) AS totalDebitTransaction')->from('houdinv_transaction')->where('WEEK(houdinv_transaction_date)= WEEK(CURDATE())')->where('houdinv_transaction_type','debit')->get()->result();
        $getStattmentFromDate = $this->db->select('houdinv_transaction_date')->from('houdinv_transaction')->where('WEEK(houdinv_transaction_date)= WEEK(CURDATE())')->order_by('houdinv_transaction_date','ASC')->limit('1')->get()->result();
        $getStattmentToDate = $this->db->select('houdinv_transaction_date')->from('houdinv_transaction')->where('WEEK(houdinv_transaction_date)= WEEK(CURDATE())')->order_by('houdinv_transaction_date','DESC')->limit('1')->get()->result();
        return array('totalTransaction'=>$getTotalTransaction[0]->totalTransaction,'totalCredit'=>$getTotalCredit[0]->totalCreditTransaction,
        'totalDebit'=>$getTotalDebit[0]->totalDebitTransaction,'from'=>$getStattmentFromDate[0]->houdinv_transaction_date,'to'=>$getStattmentToDate[0]->houdinv_transaction_date);
    }
    else if($data['searchType'] == 'tcy')
    {
        $getTotalTransaction = $this->db->select('COUNT(houdinv_transaction_id) AS totalTransaction')->from('houdinv_transaction')->where('YEAR(houdinv_transaction_date)= YEAR(CURDATE())')->get()->result();
        $getTotalCredit = $this->db->select('SUM(houdinv_transaction_amount) AS totalCreditTransaction')->from('houdinv_transaction')->where('YEAR(houdinv_transaction_date)= YEAR(CURDATE())')->where('houdinv_transaction_type','credit')->get()->result();
        $getTotalDebit = $this->db->select('SUM(houdinv_transaction_amount) AS totalDebitTransaction')->from('houdinv_transaction')->where('YEAR(houdinv_transaction_date)= YEAR(CURDATE())')->where('houdinv_transaction_type','debit')->get()->result();
        $getStattmentFromDate = $this->db->select('houdinv_transaction_date')->from('houdinv_transaction')->where('YEAR(houdinv_transaction_date)= YEAR(CURDATE())')->order_by('houdinv_transaction_date','ASC')->limit('1')->get()->result();
        $getStattmentToDate = $this->db->select('houdinv_transaction_date')->from('houdinv_transaction')->where('YEAR(houdinv_transaction_date)= YEAR(CURDATE())')->order_by('houdinv_transaction_date','DESC')->limit('1')->get()->result();
        return array('totalTransaction'=>$getTotalTransaction[0]->totalTransaction,'totalCredit'=>$getTotalCredit[0]->totalCreditTransaction,
        'totalDebit'=>$getTotalDebit[0]->totalDebitTransaction,'from'=>$getStattmentFromDate[0]->houdinv_transaction_date,'to'=>$getStattmentToDate[0]->houdinv_transaction_date);
    }
    else if($data['searchType'] == 'cd')
    {
        $getTotalTransaction = $this->db->select('COUNT(houdinv_transaction_id) AS totalTransaction')->from('houdinv_transaction')->where('houdinv_transaction_date= CURDATE()')->get()->result();
        $getTotalCredit = $this->db->select('SUM(houdinv_transaction_amount) AS totalCreditTransaction')->from('houdinv_transaction')->where('houdinv_transaction_date= CURDATE()')->where('houdinv_transaction_type','credit')->get()->result();
        $getTotalDebit = $this->db->select('SUM(houdinv_transaction_amount) AS totalDebitTransaction')->from('houdinv_transaction')->where('houdinv_transaction_date= CURDATE()')->where('houdinv_transaction_type','debit')->get()->result();
        $getStattmentFromDate = $this->db->select('houdinv_transaction_date')->from('houdinv_transaction')->where('houdinv_transaction_date= CURDATE()')->order_by('houdinv_transaction_date','ASC')->limit('1')->get()->result();
        $getStattmentToDate = $this->db->select('houdinv_transaction_date')->from('houdinv_transaction')->where('houdinv_transaction_date= CURDATE()')->order_by('houdinv_transaction_date','DESC')->limit('1')->get()->result();
        return array('totalTransaction'=>$getTotalTransaction[0]->totalTransaction,'totalCredit'=>$getTotalCredit[0]->totalCreditTransaction,
        'totalDebit'=>$getTotalDebit[0]->totalDebitTransaction,'from'=>$getStattmentFromDate[0]->houdinv_transaction_date,'to'=>$getStattmentToDate[0]->houdinv_transaction_date);
    }
  }
  public function fetchCompanyOverviewReports($data)
  {
    if($data['searchType'] == 'ccd')
    {
        $getDateFrom = strtotime($data['dateFrom']);
        $getDateTo = strtotime($data['dateTo']);
        $getTotalOrder = $this->db->select('COUNT(houdinv_order_id) AS totalOrders')->from('houdinv_orders')->where('houdinv_order_created_at >=',$getDateFrom)->where('houdinv_order_created_at <=',$getDateTo)->get()->result();
        $getTotalProducts = $this->db->select('COUNT(houdin_products_id) AS totalProducts')->from('houdinv_products')->where('houdin_products_created_date >=',$getDateFrom)->where('houdin_products_created_date <=',$getDateTo)->get()->result();
        $getInventortyPurchase = $this->db->select('COUNT(houdinv_inventory_purchase_id) AS totalPurchase')->from('houdinv_inventory_purchase')->where('houdinv_inventory_purchase_created_at <=',$getDateFrom)->where('houdinv_inventory_purchase_created_at <=',$getDateTo)->get()->result();
        $getTotalCustomer = $this->db->select('COUNT(houdinv_user_id) AS totalUsers')->from('houdinv_users')->where('houdinv_user_created_at >=',$getDateFrom)->where('houdinv_user_created_at <=',$getDateTo)->get()->result();
        $getTotalSupplier = $this->db->select('COUNT(houdinv_supplier_id) AS totalSupplier')->from('houdinv_suppliers')->where('houdinv_supplier_created_at >=',$getDateFrom)->where('houdinv_supplier_created_at <=',$getDateTo)->get()->result();

        return array('totalOrder'=>$getTotalOrder[0]->totalOrders,'totalProducts'=>$getTotalProducts[0]->totalProducts,'totalPurchase'=>$getInventortyPurchase[0]->totalPurchase,
        'totalCustomer'=>$getTotalCustomer[0]->totalUsers,'totalSupplier'=>$getTotalSupplier[0]->totalSupplier);
    }
    else if($data['searchType'] == 'cm')
    {
        $getTotalOrder = $this->db->select('COUNT(houdinv_order_id) AS totalOrders')->from('houdinv_orders')->where('MONTH(FROM_UNIXTIME(houdinv_order_created_at))= MONTH(CURDATE())')->get()->result();
        $getTotalProducts = $this->db->select('COUNT(houdin_products_id) AS totalProducts')->from('houdinv_products')->where('MONTH(FROM_UNIXTIME(houdin_products_created_date))= MONTH(CURDATE())')->get()->result();
        $getInventortyPurchase = $this->db->select('COUNT(houdinv_inventory_purchase_id) AS totalPurchase')->from('houdinv_inventory_purchase')->where('MONTH(FROM_UNIXTIME(houdinv_inventory_purchase_created_at))= MONTH(CURDATE())')->get()->result();
        $getTotalCustomer = $this->db->select('COUNT(houdinv_user_id) AS totalUsers')->from('houdinv_users')->where('MONTH(FROM_UNIXTIME(houdinv_user_created_at))= MONTH(CURDATE())')->get()->result();
        $getTotalSupplier = $this->db->select('COUNT(houdinv_supplier_id) AS totalSupplier')->from('houdinv_suppliers')->where('MONTH(FROM_UNIXTIME(houdinv_supplier_created_at))= MONTH(CURDATE())')->get()->result();

        return array('totalOrder'=>$getTotalOrder[0]->totalOrders,'totalProducts'=>$getTotalProducts[0]->totalProducts,'totalPurchase'=>$getInventortyPurchase[0]->totalPurchase,
        'totalCustomer'=>$getTotalCustomer[0]->totalUsers,'totalSupplier'=>$getTotalSupplier[0]->totalSupplier);
    }
    else if($data['searchType'] == 'cw')
    {
        $getTotalOrder = $this->db->select('COUNT(houdinv_order_id) AS totalOrders')->from('houdinv_orders')->where('WEEK(FROM_UNIXTIME(houdinv_order_created_at))= WEEK(CURDATE())')->get()->result();
        $getTotalProducts = $this->db->select('COUNT(houdin_products_id) AS totalProducts')->from('houdinv_products')->where('WEEK(FROM_UNIXTIME(houdin_products_created_date))= WEEK(CURDATE())')->get()->result();
        $getInventortyPurchase = $this->db->select('COUNT(houdinv_inventory_purchase_id) AS totalPurchase')->from('houdinv_inventory_purchase')->where('WEEK(FROM_UNIXTIME(houdinv_inventory_purchase_created_at))= WEEK(CURDATE())')->get()->result();
        $getTotalCustomer = $this->db->select('COUNT(houdinv_user_id) AS totalUsers')->from('houdinv_users')->where('WEEK(FROM_UNIXTIME(houdinv_user_created_at))= WEEK(CURDATE())')->get()->result();
        $getTotalSupplier = $this->db->select('COUNT(houdinv_supplier_id) AS totalSupplier')->from('houdinv_suppliers')->where('WEEK(FROM_UNIXTIME(houdinv_supplier_created_at))= WEEK(CURDATE())')->get()->result();

        return array('totalOrder'=>$getTotalOrder[0]->totalOrders,'totalProducts'=>$getTotalProducts[0]->totalProducts,'totalPurchase'=>$getInventortyPurchase[0]->totalPurchase,
        'totalCustomer'=>$getTotalCustomer[0]->totalUsers,'totalSupplier'=>$getTotalSupplier[0]->totalSupplier);
    }
    else if($data['searchType'] == 'tcy')
    {
        $getTotalOrder = $this->db->select('COUNT(houdinv_order_id) AS totalOrders')->from('houdinv_orders')->where('YEAR(FROM_UNIXTIME(houdinv_order_created_at))= YEAR(CURDATE())')->get()->result();
        $getTotalProducts = $this->db->select('COUNT(houdin_products_id) AS totalProducts')->from('houdinv_products')->where('YEAR(FROM_UNIXTIME(houdin_products_created_date))= YEAR(CURDATE())')->get()->result();
        $getInventortyPurchase = $this->db->select('COUNT(houdinv_inventory_purchase_id) AS totalPurchase')->from('houdinv_inventory_purchase')->where('YEAR(FROM_UNIXTIME(houdinv_inventory_purchase_created_at))= YEAR(CURDATE())')->get()->result();
        $getTotalCustomer = $this->db->select('COUNT(houdinv_user_id) AS totalUsers')->from('houdinv_users')->where('YEAR(FROM_UNIXTIME(houdinv_user_created_at))= YEAR(CURDATE())')->get()->result();
        $getTotalSupplier = $this->db->select('COUNT(houdinv_supplier_id) AS totalSupplier')->from('houdinv_suppliers')->where('YEAR(FROM_UNIXTIME(houdinv_supplier_created_at))= YEAR(CURDATE())')->get()->result();

        return array('totalOrder'=>$getTotalOrder[0]->totalOrders,'totalProducts'=>$getTotalProducts[0]->totalProducts,'totalPurchase'=>$getInventortyPurchase[0]->totalPurchase,
        'totalCustomer'=>$getTotalCustomer[0]->totalUsers,'totalSupplier'=>$getTotalSupplier[0]->totalSupplier);
    }
    else if($data['searchType'] == 'cd')
    {
        $setData = strtotime(date('Y-m-d'));
        $getTotalOrder = $this->db->select('COUNT(houdinv_order_id) AS totalOrders')->from('houdinv_orders')->where('houdinv_order_created_at',$setData)->get()->result();
        $getTotalProducts = $this->db->select('COUNT(houdin_products_id) AS totalProducts')->from('houdinv_products')->where('houdin_products_created_date',$setData)->get()->result();
        $getInventortyPurchase = $this->db->select('COUNT(houdinv_inventory_purchase_id) AS totalPurchase')->from('houdinv_inventory_purchase')->where('houdinv_inventory_purchase_created_at',$setData)->get()->result();
        $getTotalCustomer = $this->db->select('COUNT(houdinv_user_id) AS totalUsers')->from('houdinv_users')->where('houdinv_user_created_at',$setData)->get()->result();
        $getTotalSupplier = $this->db->select('COUNT(houdinv_supplier_id) AS totalSupplier')->from('houdinv_suppliers')->where('houdinv_supplier_created_at',$setData)->get()->result();

        return array('totalOrder'=>$getTotalOrder[0]->totalOrders,'totalProducts'=>$getTotalProducts[0]->totalProducts,'totalPurchase'=>$getInventortyPurchase[0]->totalPurchase,
        'totalCustomer'=>$getTotalCustomer[0]->totalUsers,'totalSupplier'=>$getTotalSupplier[0]->totalSupplier);
    }
  }
  public function fetchAddExpenseData()
  {
      $setPayeeArray = array();
        $getCustomerData = $this->db->select('houdinv_user_id,houdinv_user_name')->from('houdinv_users')->get()->result();
        foreach($getCustomerData as $getCustomerDataList)
        {
            $setPayeeArray[] = array('id'=>$getCustomerDataList->houdinv_user_id,'name'=>$getCustomerDataList->houdinv_user_name,'type'=>'customer');
        }
        $getSupplier = $this->db->select('houdinv_supplier_id,houdinv_supplier_contact_person_name')->from('houdinv_suppliers')->get()->result();
        foreach($getSupplier as $getSupplierList)
        {
        $setPayeeArray[] = array('id'=>$getSupplierList->houdinv_supplier_id,'name'=>$getSupplierList->houdinv_supplier_contact_person_name,'type'=>'supplier');
        }
        $getStaffMember = $this->db->select('staff_id,staff_name')->from('houdinv_staff_management')->get()->result();
        foreach($getStaffMember as $getStaffMemberList)
        {
        $setPayeeArray[] = array('id'=>$getStaffMemberList->staff_id,'name'=>$getStaffMemberList->staff_name,'type'=>'staff');
        }
        //   fetch product
        $getProductList = $this->db->select('houdin_products_id,houdin_products_title,houdin_products_final_price,houdin_products_short_desc')->from('houdinv_products')->get()->result();
        // fetch non inventory products data
        $getNoninventoryProducts = $this->db->select('houdinv_noninventory_products_id,houdinv_noninventory_products_name,houdinv_noninventory_products_price,houdinv_noninventory_products_desc')->from('houdinv_noninventory_products')->get()->result();
        // get warehouse daat
        $getWarehouseData =  $this->db->select("*")->from("houdinv_shipping_warehouse")->get()->result();
      return array('payeeData'=>$setPayeeArray,'productListData'=>$getProductList,'warehouseData'=>$getWarehouseData,'noninventoryProduct'=>$getNoninventoryProducts);
  }
  public function addNoninventoryProduct($data)
  {
      $this->db->insert('houdinv_noninventory_products',$data);
      $getStatus = $this->db->insert_id();
      return $getStatus;
  }
  public function updateExpenseData($expense,$item,$transaction)
  {
      $this->db->insert('houdinv_extra_expence',$expense);
      $getExpense = $this->db->insert_id();
      if($getExpense)
      {
          $setArrayDataValue = array();
          $setArrayData = array();
         for($index = 0; $index < count($item['houdinv_extra_expence_item_quantity'][$index]); $index++)
         {
             $setInsertArray = array('houdinv_extra_expence_item_item_id'=>$item['houdinv_extra_expence_item_item_id'][$index],
             'houdinv_extra_expence_item_type'=>$item['houdinv_extra_expence_item_type'][$index],
             'houdinv_extra_expence_item_quantity'=>$item['houdinv_extra_expence_item_quantity'][$index],
             'houdinv_extra_expence_item_rate'=>$item['houdinv_extra_expence_item_rate'][$index],
             'houdinv_extra_expence_item_tax'=>$item['houdinv_extra_expence_item_tax'][$index],
             'houdinv_extra_expence_item_total_amount'=>$item['houdinv_extra_expence_item_total_amount'][$index],
             'houdinv_extra_expence_id'=>$getExpense);
             $getItemInsert = $this->db->insert('houdinv_extra_expence_item',$setInsertArray);
            array_push($setArrayData,$getItemInsert);
         }
         if(in_array('0',$setArrayData))
         {
            if(count($transaction) > 0)
            {
                if(count($transaction) > 0)
                {
                    $setTransactionArray = array('houdinv_transaction_transaction_id'=>$transaction['houdinv_transaction_transaction_id'],
                    'houdinv_transaction_type'=>$transaction['houdinv_transaction_type'],
                    'houdinv_transaction_method'=>$transaction['houdinv_transaction_method'],
                    'houdinv_transaction_from'=>$transaction['houdinv_transaction_from'],
                    'houdinv_transaction_for'=>$transaction['houdinv_transaction_for'],
                    'houdinv_transaction_for_id'=>$getExpense,
                    'houdinv_transaction_amount'=>$transaction['houdinv_transaction_amount'],
                    'houdinv_transaction_date'=>$transaction['houdinv_transaction_date'],
                    'houdinv_transaction_status'=>$transaction['houdinv_transaction_status']);
                    $getTransactionStatus = $this->db->insert('houdinv_transaction',$setTransactionArray);
                    if($getTransactionStatus)
                    {
                        return array('message'=>'some items');
                    }
                    else
                    {
                        return array('message'=>'transitems');
                    }
            }
            else
            {
                return array('message'=>'some items');
            }
         }
        }
         else
         {
            if(count($transaction) > 0)
            {
                $setTransactionArray = array('houdinv_transaction_transaction_id'=>$transaction['houdinv_transaction_transaction_id'],
                    'houdinv_transaction_type'=>$transaction['houdinv_transaction_type'],
                    'houdinv_transaction_method'=>$transaction['houdinv_transaction_method'],
                    'houdinv_transaction_from'=>$transaction['houdinv_transaction_from'],
                    'houdinv_transaction_for'=>$transaction['houdinv_transaction_for'],
                    'houdinv_transaction_for_id'=>$getExpense,
                    'houdinv_transaction_amount'=>$transaction['houdinv_transaction_amount'],
                    'houdinv_transaction_date'=>$transaction['houdinv_transaction_date'],
                    'houdinv_transaction_status'=>$transaction['houdinv_transaction_status']);
                    $getTransactionStatus = $this->db->insert('houdinv_transaction',$setTransactionArray);
                if($getTransactionStatus)
                {
                    // add accounting data
                    if($expense['houdinv_extra_expence_payment_status'] == 1)
                    {
                        // if payment mode is online
                        if($expense['houdinv_extra_expence_payment_method'] == 'card' || $expense['houdinv_extra_expence_payment_method'] == 'cheque')
                        {
                            if($expense['houdinv_extra_expence_transaction_type'] == 'credit')
                            {
                                $setIncrease = $expense['houdinv_extra_expence_total_amount'];
                                $setDecrease = 0;
                            }
                            else
                            {
                                $setIncrease = 0;
                                $setDecrease = $expense['houdinv_extra_expence_total_amount'];
                            }
                            // add current fund
                            $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
                            if(count($getUndepositedAccount) > 0)
                            {
                                $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                                'houdinv_accounts_balance_sheet_ref_type'=>'Current',
                                'houdinv_accounts_balance_sheet_pay_account'=>$expense['houdinv_extra_expence_payee'],
                                'houdinv_accounts_balance_sheet_payee_type'=>$expense['houdinv_extra_expence_payee_type'],
                                'houdinv_accounts_balance_sheet_account'=>0,
                                'houdinv_accounts_balance_sheet_order_id'=>0,
                                'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                                'houdinv_accounts_balance_sheet__increase'=>$setIncrease,
                                'houdinv_accounts_balance_sheet_decrease'=>$setDecrease,
                                'houdinv_accounts_balance_sheet_deposit'=>0,
                                'houdinv_accounts_balance_sheet_final_balance'=>0,
                                'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'),'houdinv_accounts_balance_sheet_reference'=>$expense['houdinv_extra_expence_refrence_number']);
                                $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                                // get amount
                                $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                                ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                                if(count($getTotalAmount) > 0)
                                {
                                    $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                                }
                                else
                                {
                                    $setFinalAmount = 0;
                                }
                                $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                            }
                        }
                        // if payment mode is offline
                        else
                        {
                            if($expense['houdinv_extra_expence_transaction_type'] == 'credit')
                            {
                                $setIncrease = $expense['houdinv_extra_expence_total_amount'];
                                $setDecrease = 0;
                            }
                            else
                            {
                                $setIncrease = 0;
                                $setDecrease = $expense['houdinv_extra_expence_total_amount'];
                            }
                            // add undeposited fund
                            $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->get()->result();    
                            if(count($getUndepositedAccount) > 0)
                            {
                                $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                                'houdinv_accounts_balance_sheet_ref_type'=>'Undeposited Fund',
                                'houdinv_accounts_balance_sheet_pay_account'=>$expense['houdinv_extra_expence_payee'],
                                'houdinv_accounts_balance_sheet_payee_type'=>$expense['houdinv_extra_expence_payee_type'],
                                'houdinv_accounts_balance_sheet_account'=>0,
                                'houdinv_accounts_balance_sheet_order_id'=>0,
                                'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                                'houdinv_accounts_balance_sheet__increase'=>$setIncrease,
                                'houdinv_accounts_balance_sheet_decrease'=>$setDecrease,
                                'houdinv_accounts_balance_sheet_deposit'=>0,
                                'houdinv_accounts_balance_sheet_final_balance'=>0,
                                'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'),'houdinv_accounts_balance_sheet_reference'=>$expense['houdinv_extra_expence_refrence_number']);
                                $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                                // get amount
                                $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                                ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                                if(count($getTotalAmount) > 0)
                                {
                                    $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                                }
                                else
                                {
                                    $setFinalAmount = 0;
                                }
                                $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                            }
                        }
                    }
                    // end accouting data
                    return array('message'=>'yes');
                }
                else
                {
                    return array('message'=>'trans');
                }
            }
            else
            {
                 // add accounting data
                 if($expense['houdinv_extra_expence_payment_status'] == 1)
                 {
                     // if payment mode is online
                     if($expense['houdinv_extra_expence_payment_method'] == 'card' || $expense['houdinv_extra_expence_payment_method'] == 'cheque')
                     {
                         if($expense['houdinv_extra_expence_transaction_type'] == 'credit')
                         {
                             $setIncrease = $expense['houdinv_extra_expence_total_amount'];
                             $setDecrease = 0;
                         }
                         else
                         {
                             $setIncrease = 0;
                             $setDecrease = $expense['houdinv_extra_expence_total_amount'];
                         }
                         // add current fund
                         $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
                         if(count($getUndepositedAccount) > 0)
                         {
                             $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                             'houdinv_accounts_balance_sheet_ref_type'=>'Current',
                             'houdinv_accounts_balance_sheet_pay_account'=>$expense['houdinv_extra_expence_payee'],
                             'houdinv_accounts_balance_sheet_payee_type'=>$expense['houdinv_extra_expence_payee_type'],
                             'houdinv_accounts_balance_sheet_account'=>0,
                             'houdinv_accounts_balance_sheet_order_id'=>0,
                             'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                             'houdinv_accounts_balance_sheet__increase'=>$setIncrease,
                             'houdinv_accounts_balance_sheet_decrease'=>$setDecrease,
                             'houdinv_accounts_balance_sheet_deposit'=>0,
                             'houdinv_accounts_balance_sheet_final_balance'=>0,
                             'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'),'houdinv_accounts_balance_sheet_reference'=>$expense['houdinv_extra_expence_refrence_number']);
                             $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                             // get amount
                             $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                             ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                             if(count($getTotalAmount) > 0)
                             {
                                 $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                             }
                             else
                             {
                                 $setFinalAmount = 0;
                             }
                             $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                         }
                     }
                     // if payment mode is offline
                     else
                     {
                         if($expense['houdinv_extra_expence_transaction_type'] == 'credit')
                         {
                             $setIncrease = $expense['houdinv_extra_expence_total_amount'];
                             $setDecrease = 0;
                         }
                         else
                         {
                             $setIncrease = 0;
                             $setDecrease = $expense['houdinv_extra_expence_total_amount'];
                         }
                         // add undeposited fund
                         $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->get()->result();    
                         if(count($getUndepositedAccount) > 0)
                         {
                             $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                             'houdinv_accounts_balance_sheet_ref_type'=>'Undeposited Fund',
                             'houdinv_accounts_balance_sheet_pay_account'=>$expense['houdinv_extra_expence_payee'],
                             'houdinv_accounts_balance_sheet_payee_type'=>$expense['houdinv_extra_expence_payee_type'],
                             'houdinv_accounts_balance_sheet_account'=>0,
                             'houdinv_accounts_balance_sheet_order_id'=>0,
                             'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                             'houdinv_accounts_balance_sheet__increase'=>$setIncrease,
                             'houdinv_accounts_balance_sheet_decrease'=>$setDecrease,
                             'houdinv_accounts_balance_sheet_deposit'=>0,
                             'houdinv_accounts_balance_sheet_final_balance'=>0,
                             'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'),'houdinv_accounts_balance_sheet_reference'=>$expense['houdinv_extra_expence_refrence_number']);
                             $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                             // get amount
                             $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                             ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                             if(count($getTotalAmount) > 0)
                             {
                                 $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                             }
                             else
                             {
                                 $setFinalAmount = 0;
                             }
                             $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                         }
                     }
                 }
                 // end accouting data
                return array('message'=>'yes');
            }
         }
  }
}
public function updateExpenseDataSet($expense,$item,$transaction,$expenseId)
{
    $this->db->where('houdinv_extra_expence_id',$expenseId);
    $this->db->update('houdinv_extra_expence',$expense);
      $getExpense = $expenseId ;
      if($getExpense)
      {
          $setArrayDataValue = array();
          $setArrayData = array();
          $this->db->where('houdinv_extra_expence_id',$expenseId);
          $this->db->delete('houdinv_extra_expence_item');
         for($index = 0; $index < count($item['houdinv_extra_expence_item_quantity']); $index++)
         {
             $setInsertArray = array('houdinv_extra_expence_item_item_id'=>$item['houdinv_extra_expence_item_item_id'][$index],
             'houdinv_extra_expence_item_type'=>$item['houdinv_extra_expence_item_type'][$index],
             'houdinv_extra_expence_item_quantity'=>$item['houdinv_extra_expence_item_quantity'][$index],
             'houdinv_extra_expence_item_rate'=>$item['houdinv_extra_expence_item_rate'][$index],
             'houdinv_extra_expence_item_tax'=>$item['houdinv_extra_expence_item_tax'][$index],
             'houdinv_extra_expence_item_total_amount'=>$item['houdinv_extra_expence_item_total_amount'][$index],
             'houdinv_extra_expence_id'=>$getExpense);
             $getItemInsert = $this->db->insert('houdinv_extra_expence_item',$setInsertArray);
            array_push($setArrayData,$getItemInsert);
         }
         if(in_array('0',$setArrayData))
         {
            if(count($transaction) > 0)
            {
                if(count($transaction) > 0)
                {
                    $setTransactionArray = array('houdinv_transaction_transaction_id'=>$transaction['houdinv_transaction_transaction_id'],
                    'houdinv_transaction_type'=>$transaction['houdinv_transaction_type'],
                    'houdinv_transaction_method'=>$transaction['houdinv_transaction_method'],
                    'houdinv_transaction_from'=>$transaction['houdinv_transaction_from'],
                    'houdinv_transaction_for'=>$transaction['houdinv_transaction_for'],
                    'houdinv_transaction_for_id'=>$getExpense,
                    'houdinv_transaction_amount'=>$transaction['houdinv_transaction_amount'],
                    'houdinv_transaction_date'=>$transaction['houdinv_transaction_date'],
                    'houdinv_transaction_status'=>$transaction['houdinv_transaction_status']);
                    $getTransactionStatus = $this->db->insert('houdinv_transaction',$setTransactionArray);
                    if($getTransactionStatus)
                    {
                        return array('message'=>'some items');
                    }
                    else
                    {
                        return array('message'=>'transitems');
                    }
            }
            else
            {
                return array('message'=>'some items');
            }
         }
        }
         else
         {
            if(count($transaction) > 0)
            {
                $setTransactionArray = array('houdinv_transaction_transaction_id'=>$transaction['houdinv_transaction_transaction_id'],
                    'houdinv_transaction_type'=>$transaction['houdinv_transaction_type'],
                    'houdinv_transaction_method'=>$transaction['houdinv_transaction_method'],
                    'houdinv_transaction_from'=>$transaction['houdinv_transaction_from'],
                    'houdinv_transaction_for'=>$transaction['houdinv_transaction_for'],
                    'houdinv_transaction_for_id'=>$getExpense,
                    'houdinv_transaction_amount'=>$transaction['houdinv_transaction_amount'],
                    'houdinv_transaction_date'=>$transaction['houdinv_transaction_date'],
                    'houdinv_transaction_status'=>$transaction['houdinv_transaction_status']);
                    $getTransactionStatus = $this->db->insert('houdinv_transaction',$setTransactionArray);
                if($getTransactionStatus)
                {
                    return array('message'=>'yes');
                }
                else
                {
                    return array('message'=>'trans');
                }
            }
            else
            {
                return array('message'=>'yes');
            }
         }
  }
}
public function getExpenseTotalCount()
{
    $getToatalCount = $this->db->select('COUNT(houdinv_extra_expence_id) as totalExpense')->from('houdinv_extra_expence')->get()->result();
    return array('totalRows'=>$getToatalCount);
}
public function getExpenseList($data)
{
    $this->db->select('*')->from('houdinv_extra_expence');
    if($data['start'] && $data['limit'])
    {
        $this->db->limit($data['limit'],$data['start']);
    }
    elseif(!$data['start'] && $data['limit'])
    {
        $this->db->limit($data['limit']);
    }  
    $getExpensedata = $this->db->get();
    $resultExpensedata = ($getExpensedata->num_rows() > 0)?$getExpensedata->result():FALSE;
    $setParentArray = array();
    foreach($resultExpensedata as $getExpenseDataList)
    {
        $setChildArray = array();
        $setChildArray['main'] = $getExpenseDataList;
        // get payee details
        if($getExpenseDataList->houdinv_extra_expence_payee_type == 'customer')
        {
            $getCustomerList = $this->db->select('houdinv_user_name,houdinv_user_email')->from('houdinv_users')->where('houdinv_user_id',$getExpenseDataList->houdinv_extra_expence_payee)->get()->result();
            $setChildArray['payee'] = array('name'=>$getCustomerList[0]->houdinv_user_name,'email'=>$getCustomerList[0]->houdinv_user_email);
        }
        else if($getExpenseDataList->houdinv_extra_expence_payee_type == 'staff')
        {
            $getStaffList = $this->db->select('staff_name,staff_contact_number')->from('houdinv_staff_management')->where('staff_id',$getExpenseDataList->houdinv_extra_expence_payee)->get()->result();
            $setChildArray['payee'] = array('name'=>$getStaffList[0]->staff_name,'email'=>$getStaffList[0]->staff_contact_number);
        }
        else if($getExpenseDataList->houdinv_extra_expence_payee_type == 'supplier')
        {
            $getSupplierList = $this->db->select('houdinv_supplier_contact_person_name,houdinv_supplier_email')->from('houdinv_suppliers')->where('houdinv_supplier_id',$getExpenseDataList->houdinv_extra_expence_payee)->get()->result();
            $setChildArray['payee'] = array('name'=>$getSupplierList[0]->houdinv_supplier_contact_person_name,'email'=>$getSupplierList[0]->houdinv_supplier_email);
        }
        else
        {
            $setChildArray['payee'] = array();
        }
        array_push($setParentArray,$setChildArray);
    }
    return array('payeeResult'=>$setParentArray);
}
public function updatePaymentStatus($data)
{
    $getTransactionStatus = $this->db->insert('houdinv_transaction',$data);
    if($getTransactionStatus)
    {
        $updateArray = array('houdinv_extra_expence_payment_status'=>'1');
        $this->db->where('houdinv_extra_expence_id',$data['houdinv_transaction_for_id']);
        $getUpdateData = $this->db->update('houdinv_extra_expence',$updateArray);
        // get expense data
        $getexpenseData = $this->db->select('*')->from('houdinv_extra_expence')->where('houdinv_extra_expence_id',$data['houdinv_transaction_for_id'])->get()->result();
        // if payment mode is online
        if($data['houdinv_transaction_from'] == 'card' || $data['houdinv_transaction_from'] == 'cheque')
        {
            if($data['houdinv_transaction_type'] == 'credit')
            {
                $setIncrease = $data['houdinv_transaction_amount'];
                $setDecrease = 0;
            }
            else
            {
                $setIncrease = 0;
                $setDecrease = $data['houdinv_transaction_amount'];
            }
            // add current fund
            $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
            if(count($getUndepositedAccount) > 0)
            {
                $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                'houdinv_accounts_balance_sheet_ref_type'=>'Current',
                'houdinv_accounts_balance_sheet_pay_account'=>$getexpenseData[0]->houdinv_extra_expence_payee,
                'houdinv_accounts_balance_sheet_payee_type'=>$getexpenseData[0]->houdinv_extra_expence_payee_type,
                'houdinv_accounts_balance_sheet_account'=>0,
                'houdinv_accounts_balance_sheet_order_id'=>0,
                'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                'houdinv_accounts_balance_sheet__increase'=>$setIncrease,
                'houdinv_accounts_balance_sheet_decrease'=>$setDecrease,
                'houdinv_accounts_balance_sheet_deposit'=>0,
                'houdinv_accounts_balance_sheet_final_balance'=>0,
                'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                // get amount
                $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                if(count($getTotalAmount) > 0)
                {
                    $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                }
                else
                {
                    $setFinalAmount = 0;
                }
                $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
            }
        }
        // if payment mode is offline
        else
        {
            if($data['houdinv_transaction_type'] == 'credit')
            {
                $setIncrease = $data['houdinv_transaction_amount'];
                $setDecrease = 0;
            }
            else
            {
                $setIncrease = 0;
                $setDecrease = $data['houdinv_transaction_amount'];
            }
            // add undeposited fund
            $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->get()->result();    
            if(count($getUndepositedAccount) > 0)
            {
                $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                'houdinv_accounts_balance_sheet_ref_type'=>'Undeposited Fund',
                'houdinv_accounts_balance_sheet_pay_account'=>$getexpenseData[0]->houdinv_extra_expence_payee,
                'houdinv_accounts_balance_sheet_payee_type'=>$getexpenseData[0]->houdinv_extra_expence_payee_type,
                'houdinv_accounts_balance_sheet_account'=>0,
                'houdinv_accounts_balance_sheet_order_id'=>0,
                'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                'houdinv_accounts_balance_sheet__increase'=>$setIncrease,
                'houdinv_accounts_balance_sheet_decrease'=>$setDecrease,
                'houdinv_accounts_balance_sheet_deposit'=>0,
                'houdinv_accounts_balance_sheet_final_balance'=>0,
                'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                // get amount
                $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                if(count($getTotalAmount) > 0)
                {
                    $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                }
                else
                {
                    $setFinalAmount = 0;
                }
                $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
            }
        }
        return $getUpdateData;
    }
}


public function getExpenseData($data)
{
    $getExpenseData = $this->db->select('*')->from('houdinv_extra_expence')->where('houdinv_extra_expence_id',$data)->get()->result();
    $getExpenseItem = $this->db->select('*')->from('houdinv_extra_expence_item')->where('houdinv_extra_expence_id',$data)->get()->result();
    return array('expenseData'=>$getExpenseData,'item'=>$getExpenseItem);
}
public function GetblanceShowtheblancetrafer($accountid)
{
   return $getCurrentAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_id',$accountid)->get()->row();
    
}
public function getProfitlossReports()
{
    $to = date('Y-m-d');
    $from = date('Y-m-d', strtotime('-40 days'));

    // get Assets data
    // get current assets data
    $getCurrentAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','2')->get()->result();
    
    if(count($getCurrentAssets) > 0)
    {
        foreach($getCurrentAssets as $getCurrentAssetsList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getCurrentAssetsList->houdinv_accounts_id)->get()->result();
            $setCurrentAssestsArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setCurrentAssestsArray = array();
    }
    // get fixed assets
    $getFixedAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','4')->get()->result();
    if(count($getFixedAssets) > 0)
    {
        foreach($getFixedAssets as $getFixedAssetsList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getFixedAssetsList->houdinv_accounts_id)->get()->result();
            $setFixedAssestsArray[] = array('name'=>$getData);
            
        }
    }
    else
    {
        $setFixedAssestsArray = array();
    }
    // get non current assest
    $getNonAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','5')->get()->result();
    if(count($getNonAssets) > 0)
    {
        foreach($getNonAssets as $getNonAssetsList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getNonAssetsList->houdinv_accounts_id)->get()->result();
            $setNonAssestsArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setNonAssestsArray = array();
    }
    // get account receivebale and paybale
    // get receivable
    $getReceivable = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','1')->get()->result();
    if(count($getReceivable) > 0)
    {
        foreach($getReceivable as $getReceivableList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getReceivableList->houdinv_accounts_id)->get()->result();
            $setReceivableArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setReceivableArray = array();
    }
    // get payablke
    $getPayable = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','6')->get()->result();
    if(count($getPayable) > 0)
    {
        foreach($getPayable as $getPayableList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getPayableList->houdinv_accounts_id)->get()->result();
            $setPayableArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setPayableArray = array();
    }
    // bank and credit card
    // bank
    $getBank = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->get()->result();
    if(count($getBank) > 0)
    {
        foreach($getBank as $getBankList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getBankList->houdinv_accounts_id)->get()->result();
            $setBankArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setBankArray = array();
    }
    // credit card
    $getCreditCard = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','7')->get()->result();
    if(count($getCreditCard) > 0)
    {
        foreach($getCreditCard as $getCreditCardList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getCreditCardList->houdinv_accounts_id)->get()->result();
            $setCreditArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setCreditArray = array();
    }
    // libalties and equity
    // libalties
    $getLibalties = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','8')->or_where('houdinv_accounts_account_type_id','9')->get()->result();
    if(count($getLibalties) > 0)
    {
        foreach($getLibalties as $getLibaltiesList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getLibaltiesList->houdinv_accounts_id)->get()->result();
            $setLibaltiesArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setLibaltiesArray = array();
    }
    // equity
    $getEquity = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','10')->get()->result();
    if(count($getEquity) > 0)
    {
        foreach($getEquity as $getEquityList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getEquityList->houdinv_accounts_id)->get()->result();
            $setEquityArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setEquityArray = array();
    }
    // expense and other expense
    // expense
    $getExpense = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','14')->get()->result();
    if(count($getExpense) > 0)
    {
        foreach($getExpense as $getExpenseList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getExpenseList->houdinv_accounts_id)->get()->result();
            $setExpenseArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setExpenseArray = array();
    }
    // other expense
    $getOtherExpense = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','15')->get()->result();
    if(count($getOtherExpense) > 0)
    {
        foreach($getOtherExpense as $getOtherExpenseList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getOtherExpenseList->houdinv_accounts_id)->get()->result();
            $setOtherExpenseArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setOtherExpenseArray = array();
    }
    // undeposited fund
    $getUndepositedFund = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->get()->result();
    if(count($getUndepositedFund) > 0)
    {
        foreach($getUndepositedFund as $getUndepositedFundList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedFundList->houdinv_accounts_id)->get()->result();
            $setUndepositedArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setUndepositedArray = array();
    }
    // income and other income
    // income
    $getIncome = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','11')->get()->result();
    if(count($getIncome) > 0)
    {
        foreach($getIncome as $getIncomeList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getIncomeList->houdinv_accounts_id)->get()->result();
            $setIncomeArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setIncomeArray = array();
    }
    // other income
    $getOtherIncome = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','12')->get()->result();
    if(count($getOtherIncome) > 0)
    {
        foreach($getOtherIncome as $getOtherIncomeList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getOtherIncomeList->houdinv_accounts_id)->get()->result();
            $setOtherIncomeArray[] = array('name'=>$getData);   
        }
    }
    else
    {
        $setOtherIncomeArray = array();
    }
    // other data 
    $getOtherData = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','13')->get()->result();
    if(count($getOtherData) > 0)
    {
        foreach($getOtherData as $getOtherDataList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getOtherDataList->houdinv_accounts_id)->get()->result();
            $setOtherArray[] = array('name'=>$getData); 
        }
    }
    else
    {
        $setOtherArray = array();
    }
    return array('from'=>$from,'to'=>$to,'currentAssets'=>$setCurrentAssestsArray,'fixedAssets'=>$setFixedAssestsArray,
    'receiveable'=>$setReceivableArray,'payable'=>$setPayableArray,'bank'=>$setBankArray,'credit'=>$setCreditArray,'liblties'=>$setLibaltiesArray,'equity'=>$setEquityArray,
    'expense'=>$setExpenseArray,'otherExpense'=>$setOtherExpenseArray,'undeposited'=>$setUndepositedArray,'income'=>$setIncomeArray,'otherincome'=>$setOtherIncomeArray,
    'other'=>$setOtherArray,'non'=>$setNonAssestsArray,'message'=>'Success'); 
}
public function getProfitlossPercentageReports()
{
    $getCurrentDate = date('Y-m-d');
    $getWeekDate = date('Y-m-d', strtotime('-7 days'));
    // get credit info
    $getCreditincome = $this->db->select('SUM(houdinv_transaction_amount) as creditAmount')->from('houdinv_transaction')->where('houdinv_transaction_type','credit')
    ->where('houdinv_transaction_date <=',$getCurrentDate)->where('houdinv_transaction_date >=',$getWeekDate)
    ->get()->result();
    // get debit info
    $getDebitincome = $this->db->select('SUM(houdinv_transaction_amount) as debitAmount')->from('houdinv_transaction')->where('houdinv_transaction_type','debit')->where('houdinv_transaction_date <=',$getCurrentDate)->where('houdinv_transaction_date >=',$getWeekDate)
    ->get()->result();
    return array('credit'=>$getCreditincome[0]->creditAmount,'debit'=>$getDebitincome[0]->debitAmount,'from'=>$getWeekDate,'to'=>$getCurrentDate);
}
public function getFilterProfitlossPercentageReports($from,$to)
{
    $getCurrentDate = $to;
    $getWeekDate = $from;
    // get credit info
    $getCreditincome = $this->db->select('SUM(houdinv_transaction_amount) as creditAmount')->from('houdinv_transaction')->where('houdinv_transaction_type','credit')
    ->where('houdinv_transaction_date <=',$getCurrentDate)->where('houdinv_transaction_date >=',$getWeekDate)
    ->get()->result();
    // get debit info
    $getDebitincome = $this->db->select('SUM(houdinv_transaction_amount) as debitAmount')->from('houdinv_transaction')->where('houdinv_transaction_type','debit')->where('houdinv_transaction_date <=',$getCurrentDate)->where('houdinv_transaction_date >=',$getWeekDate)
    ->get()->result();
    return array('credit'=>$getCreditincome[0]->creditAmount,'debit'=>$getDebitincome[0]->debitAmount,'from'=>$getWeekDate,'to'=>$getCurrentDate,'message'=>'success');
}
public function getFilterProfitLossPercenatageReports($from,$to)
{
    $getCurrentDate = $to;
    $getWeekDate = $from;
    // get credit info
    $getCreditincome = $this->db->select('SUM(houdinv_transaction_amount) as creditAmount')->from('houdinv_transaction')->where('houdinv_transaction_type','credit')
    ->where('houdinv_transaction_date <=',$getCurrentDate)->where('houdinv_transaction_date >=',$getWeekDate)
    ->get()->result();
    // get debit info
    $getDebitincome = $this->db->select('SUM(houdinv_transaction_amount) as debitAmount')->from('houdinv_transaction')->where('houdinv_transaction_type','debit')->where('houdinv_transaction_date <=',$getCurrentDate)->where('houdinv_transaction_date >=',$getWeekDate)
    ->get()->result();
    return array('credit'=>$getCreditincome[0]->creditAmount,'debit'=>$getDebitincome[0]->debitAmount,'from'=>$getWeekDate,'to'=>$getCurrentDate,'message'=>'success');
}

// get filter profit loss report
public function getFilterProfitLossReports($from,$to)
{
    // get Assets data
    // get current assets data
    $getCurrentAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','2')->get()->result();
    
    if(count($getCurrentAssets) > 0)
    {
        foreach($getCurrentAssets as $getCurrentAssetsList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getCurrentAssetsList->houdinv_accounts_id)->get()->result();
            $setCurrentAssestsArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setCurrentAssestsArray = array();
    }
    // get fixed assets
    $getFixedAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','4')->get()->result();
    if(count($getFixedAssets) > 0)
    {
        foreach($getFixedAssets as $getFixedAssetsList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getFixedAssetsList->houdinv_accounts_id)->get()->result();
            $setFixedAssestsArray[] = array('name'=>$getData);
            
        }
    }
    else
    {
        $setFixedAssestsArray = array();
    }
    // get non current assest
    $getNonAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','5')->get()->result();
    if(count($getNonAssets) > 0)
    {
        foreach($getNonAssets as $getNonAssetsList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getNonAssetsList->houdinv_accounts_id)->get()->result();
            $setNonAssestsArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setNonAssestsArray = array();
    }
    // get account receivebale and paybale
    // get receivable
    $getReceivable = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','1')->get()->result();
    if(count($getReceivable) > 0)
    {
        foreach($getReceivable as $getReceivableList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getReceivableList->houdinv_accounts_id)->get()->result();
            $setReceivableArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setReceivableArray = array();
    }
    // get payablke
    $getPayable = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','6')->get()->result();
    if(count($getPayable) > 0)
    {
        foreach($getPayable as $getPayableList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getPayableList->houdinv_accounts_id)->get()->result();
            $setPayableArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setPayableArray = array();
    }
    // bank and credit card
    // bank
    $getBank = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->get()->result();
    if(count($getBank) > 0)
    {
        foreach($getBank as $getBankList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getBankList->houdinv_accounts_id)->get()->result();
            $setBankArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setBankArray = array();
    }
    // credit card
    $getCreditCard = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','7')->get()->result();
    if(count($getCreditCard) > 0)
    {
        foreach($getCreditCard as $getCreditCardList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getCreditCardList->houdinv_accounts_id)->get()->result();
            $setCreditArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setCreditArray = array();
    }
    // libalties and equity
    // libalties
    $getLibalties = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','8')->or_where('houdinv_accounts_account_type_id','9')->get()->result();
    if(count($getLibalties) > 0)
    {
        foreach($getLibalties as $getLibaltiesList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getLibaltiesList->houdinv_accounts_id)->get()->result();
            $setLibaltiesArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setLibaltiesArray = array();
    }
    // equity
    $getEquity = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','10')->get()->result();
    if(count($getEquity) > 0)
    {
        foreach($getEquity as $getEquityList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getEquityList->houdinv_accounts_id)->get()->result();
            $setEquityArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setEquityArray = array();
    }
    // expense and other expense
    // expense
    $getExpense = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','14')->get()->result();
    if(count($getExpense) > 0)
    {
        foreach($getExpense as $getExpenseList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getExpenseList->houdinv_accounts_id)->get()->result();
            $setExpenseArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setExpenseArray = array();
    }
    // other expense
    $getOtherExpense = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','15')->get()->result();
    if(count($getOtherExpense) > 0)
    {
        foreach($getOtherExpense as $getOtherExpenseList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getOtherExpenseList->houdinv_accounts_id)->get()->result();
            $setOtherExpenseArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setOtherExpenseArray = array();
    }
    // undeposited fund
    $getUndepositedFund = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->get()->result();
    if(count($getUndepositedFund) > 0)
    {
        foreach($getUndepositedFund as $getUndepositedFundList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedFundList->houdinv_accounts_id)->get()->result();
            $setUndepositedArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setUndepositedArray = array();
    }
    // income and other income
    // income
    $getIncome = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','11')->get()->result();
    if(count($getIncome) > 0)
    {
        foreach($getIncome as $getIncomeList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getIncomeList->houdinv_accounts_id)->get()->result();
            $setIncomeArray[] = array('name'=>$getData);
        }
    }
    else
    {
        $setIncomeArray = array();
    }
    // other income
    $getOtherIncome = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','12')->get()->result();
    if(count($getOtherIncome) > 0)
    {
        foreach($getOtherIncome as $getOtherIncomeList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getOtherIncomeList->houdinv_accounts_id)->get()->result();
            $setOtherIncomeArray[] = array('name'=>$getData);   
        }
    }
    else
    {
        $setOtherIncomeArray = array();
    }
    // other data 
    $getOtherData = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','13')->get()->result();
    if(count($getOtherData) > 0)
    {
        foreach($getOtherData as $getOtherDataList)
        {
            $getData = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_users.houdinv_user_name')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)
            ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_pay_account','left outer')
            ->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getOtherDataList->houdinv_accounts_id)->get()->result();
            $setOtherArray[] = array('name'=>$getData); 
        }
    }
    else
    {
        $setOtherArray = array();
    }
    return array('from'=>$from,'to'=>$to,'currentAssets'=>$setCurrentAssestsArray,'fixedAssets'=>$setFixedAssestsArray,
    'receiveable'=>$setReceivableArray,'payable'=>$setPayableArray,'bank'=>$setBankArray,'credit'=>$setCreditArray,'liblties'=>$setLibaltiesArray,'equity'=>$setEquityArray,
    'expense'=>$setExpenseArray,'otherExpense'=>$setOtherExpenseArray,'undeposited'=>$setUndepositedArray,'income'=>$setIncomeArray,'otherincome'=>$setOtherIncomeArray,
    'other'=>$setOtherArray,'non'=>$setNonAssestsArray,'message'=>'Success'); 
    // // get credit info
    // $getincome = $this->db->select('*')->from('houdinv_transaction')->where('houdinv_transaction_type','credit')
    // ->where('houdinv_transaction_date <=',$getCurrentDate)->where('houdinv_transaction_date >=',$getWeekDate)
    // ->get()->result();
    // $setCreditArray = array();
    // foreach($getincome as $getincomeList)
    // {
    //     $setChildArray = array();
    //     if($getincomeList->houdinv_transaction_for == 'order' || $getincomeList->houdinv_transaction_for == 'order refund')
    //     {
    //         $getUserData = $this->db->select('houdinv_orders.houdinv_order_user_id,houdinv_users.houdinv_user_name')->from('houdinv_orders')->where('houdinv_orders.houdinv_order_id',$getincomeList->houdinv_transaction_for_id)
    //         ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_orders.houdinv_order_user_id','left outer')->get()->result();
    //         $setChildArray['user'] = $getUserData[0]->houdinv_user_name;
    //     }
    //     else if($getincomeList->houdinv_transaction_for == 'sms package' || $getincomeList->houdinv_transaction_for == 'email package')
    //     {
    //         $getUserData = $this->db->select('houdinv_shop_business_name')->from('houdinv_shop_detail')->where('houdinv_shop_id','1')->get()->result();
    //         $setChildArray['user'] = $getUserData[0]->houdinv_shop_business_name;
    //     }
    //     else if($getincomeList->houdinv_transaction_for == 'inventory purchase')
    //     {
    //         $getUserData = $this->db->select('houdinv_inventory_purchase.houdinv_inventory_purchase_supplier_id,houdinv_suppliers.houdinv_supplier_comapany_name')->from('houdinv_inventory_purchase')->where('houdinv_inventory_purchase.houdinv_inventory_purchase_id',$getincomeList->houdinv_transaction_for_id)
    //         ->join('houdinv_suppliers','houdinv_suppliers.houdinv_supplier_id = houdinv_inventory_purchase.houdinv_inventory_purchase_supplier_id','left outer')
    //         ->get()->result();
    //         $setChildArray['user'] = $getUserData[0]->houdinv_supplier_comapany_name;
    //     }
    //     else if($getincomeList->houdinv_transaction_for == 'Work Order')
    //     {
    //         $getUserData = $this->db->select('houdinv_workorder.houdinv_workorder_customer_name,houdinv_users.houdinv_user_name')->from('houdinv_workorder')->where('houdinv_workorder.houdinv_workorder_id',$getincomeList->houdinv_transaction_for_id)
    //         ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_workorder.houdinv_workorder_customer_name','left outer')
    //         ->get()->result();
    //         $setChildArray['user'] = $getUserData[0]->houdinv_user_name;
    //     }
    //     else if($getincomeList->houdinv_transaction_for == 'expense')
    //     {
    //         $getUserData =  $this->db->select("houdinv_extra_expence_payee,houdinv_extra_expence_payee_type")->from('houdinv_extra_expence')->where('houdinv_extra_expence_id',$getincomeList->houdinv_transaction_for_id)->get()->result();
    //         if($getUserData[0]->houdinv_extra_expence_payee_type == 'customer')
    //         {
    //             $getCustomerData = $this->db->select('houdinv_user_name AS userName')->from('houdinv_users')->where('houdinv_user_id',$getUserData[0]->houdinv_extra_expence_payee)->get()->result();
    //         }
    //         else if($getUserData[0]->houdinv_extra_expence_payee_type == 'supplier')
    //         {
    //             $getCustomerData = $this->db->select('houdinv_supplier_comapany_name AS userName')->from('houdinv_suppliers')->where('houdinv_supplier_id',$getUserData[0]->houdinv_extra_expence_payee)->get()->result();
    //         }
    //         else if($getUserData[0]->houdinv_extra_expence_payee_type == 'staff')
    //         {
    //             $getCustomerData = $this->db->select('staff_name AS userName')->from('houdinv_staff_management')->where('staff_id',$getUserData[0]->houdinv_extra_expence_payee)->get()->result();
    //         }
    //         $setChildArray['user'] = $getCustomerData[0]->userName;
    //     }
    //     else
    //     {
    //         $setChildArray['user'] = 'N/A';
    //     }
    //     $setChildArray['main'] = $getincomeList;
    //     array_push($setCreditArray,$setChildArray);
    // }
    // // get debit info
    // $getincome = $this->db->select('*')->from('houdinv_transaction')->where('houdinv_transaction_type','debit')->where('houdinv_transaction_date <=',$getCurrentDate)->where('houdinv_transaction_date >=',$getWeekDate)
    // ->get()->result();
    // $setDebitArray = array();
    // foreach($getincome as $getincomeList)
    // {
    //     $setChilddebitArray = array();
    //     $setChilddebitArray['main'] == $getincomeList;
    //     if($getincomeList->houdinv_transaction_for == 'order' || $getincomeList->houdinv_transaction_for == 'order refund')
    //     {
    //         $getUserData = $this->db->select('houdinv_orders.houdinv_order_user_id,houdinv_users.houdinv_user_name')->from('houdinv_orders')->where('houdinv_orders.houdinv_order_id',$getincomeList->houdinv_transaction_for_id)
    //         ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_orders.houdinv_order_user_id','left outer')->get()->result();
    //         $setChilddebitArray['user'] = $getUserData[0]->houdinv_user_name;
    //     }
    //     else if($getincomeList->houdinv_transaction_for == 'sms package' || $getincomeList->houdinv_transaction_for == 'email package')
    //     {
    //         $getUserData = $this->db->select('houdinv_shop_business_name')->from('houdinv_shop_detail')->where('houdinv_shop_id','1')->get()->result();
    //         $setChilddebitArray['user'] = $getUserData[0]->houdinv_shop_business_name;
    //     }
    //     else if($getincomeList->houdinv_transaction_for == 'inventory purchase')
    //     {
    //         $getUserData = $this->db->select('houdinv_inventory_purchase.houdinv_inventory_purchase_supplier_id,houdinv_suppliers.houdinv_supplier_comapany_name')->from('houdinv_inventory_purchase')->where('houdinv_inventory_purchase.houdinv_inventory_purchase_id',$getincomeList->houdinv_transaction_for_id)
    //         ->join('houdinv_suppliers','houdinv_suppliers.houdinv_supplier_id = houdinv_inventory_purchase.houdinv_inventory_purchase_supplier_id','left outer')
    //         ->get()->result();
    //         $setChilddebitArray['user'] = $getUserData[0]->houdinv_supplier_comapany_name;
    //     }
    //     else if($getincomeList->houdinv_transaction_for == 'Work Order')
    //     {
    //         $getUserData = $this->db->select('houdinv_workorder.houdinv_workorder_customer_name,houdinv_users.houdinv_user_name')->from('houdinv_workorder')->where('houdinv_workorder.houdinv_workorder_id',$getincomeList->houdinv_transaction_for_id)
    //         ->join('houdinv_users','houdinv_users.houdinv_user_id = houdinv_workorder.houdinv_workorder_customer_name','left outer')
    //         ->get()->result();
    //         $setChilddebitArray['user'] = $getUserData[0]->houdinv_user_name;
    //     }
    //     else if($getincomeList->houdinv_transaction_for == 'expense')
    //     {
    //         $getUserData =  $this->db->select("houdinv_extra_expence_payee,houdinv_extra_expence_payee_type")->from('houdinv_extra_expence')->where('houdinv_extra_expence_id',$getincomeList->houdinv_transaction_for_id)->get()->result();
    //         if($getUserData[0]->houdinv_extra_expence_payee_type == 'customer')
    //         {
    //             $getCustomerData = $this->db->select('houdinv_user_name AS userName')->from('houdinv_users')->where('houdinv_user_id',$getUserData[0]->houdinv_extra_expence_payee)->get()->result();
    //         }
    //         else if($getUserData[0]->houdinv_extra_expence_payee_type == 'supplier')
    //         {
    //             $getCustomerData = $this->db->select('houdinv_supplier_comapany_name AS userName')->from('houdinv_suppliers')->where('houdinv_supplier_id',$getUserData[0]->houdinv_extra_expence_payee)->get()->result();
    //         }
    //         else if($getUserData[0]->houdinv_extra_expence_payee_type == 'staff')
    //         {
    //             $getCustomerData = $this->db->select('staff_name AS userName')->from('houdinv_staff_management')->where('staff_id',$getUserData[0]->houdinv_extra_expence_payee)->get()->result();
    //         }
    //         $setChilddebitArray['user'] = $getCustomerData[0]->userName;
    //     }
    //     else
    //     {
    //         $setChilddebitArray['user'] = 'N/A';
    //     }
    //     array_push($setDebitArray,$setChildArray);
    // }
    // return array('message'=>'success','credit'=>$setCreditArray,'debit'=>$setDebitArray,'from'=>$getWeekDate,'to'=>$getCurrentDate);
}
public function getFilterCustomReports($from,$to)
{
    $getCurrentDate = $to;
    $getWeekDate = $from;
    // get credit info
    $getCreditincome = $this->db->select('SUM(houdinv_transaction_amount) as creditAmount')->from('houdinv_transaction')->where('houdinv_transaction_type','credit')
    ->where('houdinv_transaction_date <=',$getCurrentDate)->where('houdinv_transaction_date >=',$getWeekDate)
    ->get()->result();
    // get debit info
    $getDebitincome = $this->db->select('SUM(houdinv_transaction_amount) as debitAmount')->from('houdinv_transaction')->where('houdinv_transaction_type','debit')->where('houdinv_transaction_date <=',$getCurrentDate)->where('houdinv_transaction_date >=',$getWeekDate)
    ->get()->result();
    return array('credit'=>$getCreditincome[0]->creditAmount,'debit'=>$getDebitincome[0]->debitAmount,'from'=>$getWeekDate,'to'=>$getCurrentDate,'message'=>'success');
}
public function getBalanceSheetData()
{
    $getCurrentDate = date('Y-m-d');
    $getWeekDate = date('Y-m-d', strtotime('-7 days'));
    // unix time stamp
    $getCurrentDateUnix = strtotime(date('Y-m-d'));
    $getWeekDateUnix = strtotime(date('Y-m-d', strtotime('-7 days')));
    // get Assets data
    // get current assets data
    $getCurrentAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','2')->get()->result();
    if(count($getCurrentAssets) > 0)
    {
        foreach($getCurrentAssets as $getCurrentAssetsList)
        {
            $setCurrentAssestsArray[] = array('name'=>$getCurrentAssetsList->houdinv_accounts_name,'balance'=>$getCurrentAssetsList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setCurrentAssestsArray = array();
    }
    // get fixed assets
    $getFixedAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','4')->get()->result();
    if(count($getFixedAssets) > 0)
    {
        foreach($getFixedAssets as $getFixedAssetsList)
        {
            $setFixedAssestsArray[] = array('name'=>$getFixedAssetsList->houdinv_accounts_name,'balance'=>$getFixedAssetsList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setFixedAssestsArray = array();
    }
    // get non current assest
    $getNonAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','5')->get()->result();
    if(count($getNonAssets) > 0)
    {
        foreach($getNonAssets as $getNonAssetsList)
        {
            $setNonAssestsArray[] = array('name'=>$getNonAssetsList->houdinv_accounts_name,'balance'=>$getNonAssetsList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setNonAssestsArray = array();
    }
    // get account receivebale and paybale
    // get receivable
    $getReceivable = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','1')->get()->result();
    if(count($getReceivable) > 0)
    {
        foreach($getReceivable as $getReceivableList)
        {
            $setReceivableArray[] = array('name'=>$getReceivableList->houdinv_accounts_name,'balance'=>$getReceivableList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setReceivableArray = array();
    }
    // get payablke
    $getPayable = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','6')->get()->result();
    if(count($getPayable) > 0)
    {
        foreach($getPayable as $getPayableList)
        {
            $setPayableArray[] = array('name'=>$getPayableList->houdinv_accounts_name,'balance'=>$getPayableList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setPayableArray = array();
    }
    // bank and credit card
    // bank
    $getBank = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->get()->result();
    if(count($getBank) > 0)
    {
        foreach($getBank as $getBankList)
        {
            $setBankArray[] = array('name'=>$getBankList->houdinv_accounts_name,'balance'=>$getBankList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setBankArray = array();
    }
    // credit card
    $getCreditCard = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','7')->get()->result();
    if(count($getCreditCard) > 0)
    {
        foreach($getCreditCard as $getCreditCardList)
        {
            $setCreditArray[] = array('name'=>$getCreditCardList->houdinv_accounts_name,'balance'=>$getCreditCardList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setCreditArray = array();
    }
    // libalties and equity
    // libalties
    $getLibalties = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','8')->or_where('houdinv_accounts_account_type_id','9')->get()->result();
    if(count($getLibalties) > 0)
    {
        foreach($getLibalties as $getLibaltiesList)
        {
            $setLibaltiesArray[] = array('name'=>$getLibaltiesList->houdinv_accounts_name,'balance'=>$getLibaltiesList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setLibaltiesArray = array();
    }
    // equity
    $getEquity = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','10')->get()->result();
    if(count($getEquity) > 0)
    {
        foreach($getEquity as $getEquityList)
        {
            $setEquityArray[] = array('name'=>$getEquityList->houdinv_accounts_name,'balance'=>$getEquityList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setEquityArray = array();
    }
    // expense and other expense
    // expense
    $getExpense = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','14')->get()->result();
    if(count($getExpense) > 0)
    {
        foreach($getExpense as $getExpenseList)
        {
            $setExpenseArray[] = array('name'=>$getExpenseList->houdinv_accounts_name,'balance'=>$getExpenseList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setExpenseArray = array();
    }
    // other expense
    $getOtherExpense = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','15')->get()->result();
    if(count($getOtherExpense) > 0)
    {
        foreach($getOtherExpense as $getOtherExpenseList)
        {
            $setOtherExpenseArray[] = array('name'=>$getOtherExpenseList->houdinv_accounts_name,'balance'=>$getOtherExpenseList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setOtherExpenseArray = array();
    }
    // undeposited fund
    $getUndepositedFund = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->get()->result();
    if(count($getUndepositedFund) > 0)
    {
        foreach($getUndepositedFund as $getUndepositedFundList)
        {
            $setUndepositedArray[] = array('name'=>$getUndepositedFundList->houdinv_accounts_name,'balance'=>$getUndepositedFundList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setUndepositedArray = array();
    }
    // income and other income
    // income
    $getIncome = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','11')->get()->result();
    if(count($getIncome) > 0)
    {
        foreach($getIncome as $getIncomeList)
        {
            $setIncomeArray[] = array('name'=>$getIncomeList->houdinv_accounts_name,'balance'=>$getIncomeList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setIncomeArray = array();
    }
    // other income
    $getOtherIncome = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','12')->get()->result();
    if(count($getOtherIncome) > 0)
    {
        foreach($getOtherIncome as $getOtherIncomeList)
        {
            $setOtherIncomeArray[] = array('name'=>$getOtherIncomeList->houdinv_accounts_name,'balance'=>$getOtherIncomeList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setOtherIncomeArray = array();
    }
    // other data 
    $getOtherData = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','13')->get()->result();
    if(count($getOtherData) > 0)
    {
        foreach($getOtherData as $getOtherDataList)
        {
            $setOtherArray[] = array('name'=>$getOtherDataList->houdinv_accounts_name,'balance'=>$getOtherDataList->houdinv_accounts_final_balance);
        }
    }
    else
    {
        $setOtherArray = array();
    }
return array('from'=>$getWeekDate,'to'=>$getCurrentDate,'currentAssets'=>$setCurrentAssestsArray,'fixedAssets'=>$setFixedAssestsArray,
'receiveable'=>$setReceivableArray,'payable'=>$setPayableArray,'bank'=>$setBankArray,'credit'=>$setCreditArray,'liblties'=>$setLibaltiesArray,'equity'=>$setEquityArray,
'expense'=>$setExpenseArray,'otherExpense'=>$setOtherExpenseArray,'undeposited'=>$setUndepositedArray,'income'=>$setIncomeArray,'otherincome'=>$setOtherIncomeArray,
'other'=>$setOtherArray,'non'=>$setNonAssestsArray
);



    // // get credit info data
    // // get credited amount from transaction
    // $getCreditedAmountFromTransaction = $this->db->select('SUM(houdinv_transaction_amount) AS transactionCredit')->from('houdinv_transaction')->where('houdinv_transaction_date >=',$getWeekDate)
    // ->where('houdinv_transaction_date <=',$getCurrentDate)
    // ->where('houdinv_transaction_type','credit')
    // ->group_start()
    // ->where('houdinv_transaction_for','expense')->or_where('houdinv_transaction_for','sms package')->or_where('houdinv_transaction_for','email package')
    // ->group_end()->get()->result();
    // if($getCreditedAmountFromTransaction[0]->transactionCredit)
    // {
    //     $getCreditAmountFromTransaction = $getCreditedAmountFromTransaction[0]->transactionCredit;
    // }
    // else
    // {
    //     $getCreditAmountFromTransaction = 0;
    // }
    // // get credited amount from orders
    // $getCreditedAmountFromOrders = $this->db->select('SUM(houdinv_orders_total_paid) totalPaidAmount')->from('houdinv_orders')->where('houdinv_order_created_at >=',$getWeekDateUnix)->where('houdinv_order_created_at <=',$getCurrentDateUnix)->get()->result();
    // if($getCreditedAmountFromOrders[0]->totalPaidAmount)
    // {
    //     $getCreditedAmountFromOrders = $getCreditedAmountFromOrders[0]->totalPaidAmount;
    // }
    // else
    // {
    //     $getCreditedAmountFromOrders = 0;
    // }
    // $getFinalCreditedAmount = $getCreditAmountFromTransaction+$getCreditedAmountFromOrders;
    // // get unpaid credit amount
    // $getUnpaidCreditedAmountFromOrders = $this->db->select('SUM(houdinv_orders_total_remaining) totalRemainingAmount')->from('houdinv_orders')->where('houdinv_order_created_at >=',$getWeekDateUnix)->where('houdinv_order_created_at <=',$getCurrentDateUnix)->get()->result();

    // // get debit info data
    // // get debited amount from transaction
    // $getDebitedAmountFromTransaction = $this->db->select('SUM(houdinv_transaction_amount) AS transactionDebit')->from('houdinv_transaction')->where('houdinv_transaction_date >=',$getWeekDate)
    // ->where('houdinv_transaction_date <=',$getCurrentDate)
    // ->where('houdinv_transaction_type','debit')
    // ->group_start()
    // ->where('houdinv_transaction_for','expense')->or_where('houdinv_transaction_for','sms package')->or_where('houdinv_transaction_for','email package')
    // ->group_end()->get()->result();
    // if($getDebitedAmountFromTransaction[0]->transactionDebit)
    // {
    //     $getDebitedAmountFromTransaction = $getDebitedAmountFromTransaction[0]->transactionDebit;
    // }
    // else
    // {
    //     $getDebitedAmountFromTransaction = 0;
    // }
    // // get debited amount from inventory purchase
    // $getDebitedAmountFromInventoryPurchase = $this->db->select('SUM(houdinv_purchase_products_inward_amount_paid) as totalAmountDebit')->from('houdinv_purchase_products_inward')
    // ->where('houdinv_purchase_products_inward_craeted_at >=',$getWeekDateUnix)->where('houdinv_purchase_products_inward_craeted_at <=',$getCurrentDateUnix)->get()->result();
    // if($getDebitedAmountFromInventoryPurchase[0]->totalAmountDebit)
    // {
    //     $getDebitedAmountFromInventoryPurchase = $getDebitedAmountFromInventoryPurchase[0]->totalAmountDebit;
    // }
    // else
    // {
    //     $getDebitedAmountFromInventoryPurchase = 0;
    // }
    // $getFinalPaidAmount = $getDebitedAmountFromTransaction+$getDebitedAmountFromInventoryPurchase;
    // // get remaining debit amount
    // $setRemainingAmount = 0;
    // $getRemainingDebitedAmountFromInventoryPurchase = $this->db->select('houdinv_purchase_products_inward_total_amount,houdinv_purchase_products_inward_amount_paid')->from('houdinv_purchase_products_inward')
    // ->where('houdinv_purchase_products_inward_craeted_at >=',$getWeekDateUnix)->where('houdinv_purchase_products_inward_craeted_at <=',$getCurrentDateUnix)->get()->result();
    // foreach($getRemainingDebitedAmountFromInventoryPurchase as $getRemainingDebitedAmountFromInventoryPurchaseList)
    // {
    //     $getTotalAmount = $getRemainingDebitedAmountFromInventoryPurchaseList->houdinv_purchase_products_inward_total_amount;
    //     $getPaidAmount = $getRemainingDebitedAmountFromInventoryPurchaseList->houdinv_purchase_products_inward_amount_paid;
    //     $setRemainingAmount = $setRemainingAmount+($getTotalAmount-$getPaidAmount);
    // }
    // return array('totalCreditedAmount'=>$getFinalCreditedAmount,'totalRemainingCredit'=>$getUnpaidCreditedAmountFromOrders[0]->totalRemainingAmount,
    // 'finalPaidDebitAmount'=>$getFinalPaidAmount,'remainingDebitAmount'=>$setRemainingAmount,'from'=>$getWeekDate,'to'=>$getCurrentDate);

}
public function getFilterBalanceSheetReports($from,$to)
{
    $getCurrentDate = $to;
    $getWeekDate = $from;

    // get Assets data
    // get current assets data
    $getCurrentAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','2')->get()->result();
    if(count($getCurrentAssets) > 0)
    {
        foreach($getCurrentAssets as $getCurrentAssetsList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getCurrentAssetsList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setCurrentAssestsArray[] = array('name'=>$getCurrentAssetsList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setCurrentAssestsArray = array();        
            }
        }
    }
    else
    {
        $setCurrentAssestsArray = array();
    }
    // get fixed assets
    $getFixedAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','4')->get()->result();
    if(count($getFixedAssets) > 0)
    {
        foreach($getFixedAssets as $getFixedAssetsList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getFixedAssetsList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setFixedAssestsArray[] = array('name'=>$getFixedAssetsList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setFixedAssestsArray = array();        
            }
            
        }
    }
    else
    {
        $setFixedAssestsArray = array();
    }
    // get non current assest
    $getNonAssets = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','5')->get()->result();
    if(count($getNonAssets) > 0)
    {
        foreach($getNonAssets as $getNonAssetsList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getNonAssetsList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);

            if($balance > 0)
            {
                $setNonAssestsArray[] = array('name'=>$getNonAssetsList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setNonAssestsArray = array();        
            }
        }
    }
    else
    {
        $setNonAssestsArray = array();
    }
    // get account receivebale and paybale
    // get receivable
    $getReceivable = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','1')->get()->result();
    if(count($getReceivable) > 0)
    {
        foreach($getReceivable as $getReceivableList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getReceivableList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);


            if($balance > 0)
            {
                $setReceivableArray[] = array('name'=>$getReceivableList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setReceivableArray = array();        
            }
        }
    }
    else
    {
        $setReceivableArray = array();
    }
    // get payablke
    $getPayable = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','6')->get()->result();
    if(count($getPayable) > 0)
    {
        foreach($getPayable as $getPayableList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getPayableList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setPayableArray[] = array('name'=>$getPayableList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setPayableArray = array();        
            }
            
        }
    }
    else
    {
        $setPayableArray = array();
    }
    // bank and credit card
    // bank
    $getBank = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->get()->result();
    if(count($getBank) > 0)
    {
        foreach($getBank as $getBankList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getBankList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setBankArray[] = array('name'=>$getBankList->houdinv_accounts_name,'balance'=>$balance);
            }         
            else
            {
                $setBankArray = array();        
            }   
        }
    }
    else
    {
        $setBankArray = array();
    }
    // credit card
    $getCreditCard = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','7')->get()->result();
    if(count($getCreditCard) > 0)
    {
        foreach($getCreditCard as $getCreditCardList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getCreditCardList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setCreditArray[] = array('name'=>$getCreditCardList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setCreditArray = array();        
            } 
        }
    }
    else
    {
        $setCreditArray = array();
    }
    // libalties and equity
    // libalties
    $getLibalties = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','8')->or_where('houdinv_accounts_account_type_id','9')->get()->result();
    if(count($getLibalties) > 0)
    {
        foreach($getLibalties as $getLibaltiesList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getLibaltiesList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setLibaltiesArray[] = array('name'=>$getLibaltiesList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setLibaltiesArray = array();        
            } 
        }
    }
    else
    {
        $setLibaltiesArray = array();
    }
    // equity
    $getEquity = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','10')->get()->result();
    if(count($getEquity) > 0)
    {
        foreach($getEquity as $getEquityList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getEquityList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setEquityArray[] = array('name'=>$getEquityList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setEquityArray = array();        
            } 
        }
    }
    else
    {
        $setEquityArray = array();
    }
    // expense and other expense
    // expense
    $getExpense = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','14')->get()->result();
    if(count($getExpense) > 0)
    {
        foreach($getExpense as $getExpenseList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getExpenseList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setExpenseArray[] = array('name'=>$getExpenseList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setExpenseArray = array();        
            } 
        }
    }
    else
    {
        $setExpenseArray = array();
    }
    // other expense
    $getOtherExpense = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','15')->get()->result();
    if(count($getOtherExpense) > 0)
    {
        foreach($getOtherExpense as $getOtherExpenseList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getOtherExpenseList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setOtherExpenseArray[] = array('name'=>$getOtherExpenseList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setOtherExpenseArray = array();        
            } 
        }
    }
    else
    {
        $setOtherExpenseArray = array();
    }
    // undeposited fund
    $getUndepositedFund = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->get()->result();
    if(count($getUndepositedFund) > 0)
    {
        foreach($getUndepositedFund as $getUndepositedFundList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedFundList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setUndepositedArray[] = array('name'=>$getUndepositedFundList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setUndepositedArray = array();        
            }
        }
    }
    else
    {
        $setUndepositedArray = array();
    }
    // income and other income
    // income
    $getIncome = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','11')->get()->result();
    if(count($getIncome) > 0)
    {
        foreach($getIncome as $getIncomeList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getIncomeList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setIncomeArray[] = array('name'=>$getIncomeList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setIncomeArray = array();        
            }
        }
    }
    else
    {
        $setIncomeArray = array();
    }
    // other income
    $getOtherIncome = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','12')->get()->result();
    if(count($getOtherIncome) > 0)
    {
        foreach($getOtherIncome as $getOtherIncomeList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getOtherIncomeList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setOtherIncomeArray[] = array('name'=>$getOtherIncomeList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setOtherIncomeArray = array();        
            }
        }
    }
    else
    {
        $setOtherIncomeArray = array();
    }
    // other data 
    $getOtherData = $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','13')->get()->result();
    if(count($getOtherData) > 0)
    {
        foreach($getOtherData as $getOtherDataList)
        {
            $getData = $this->db->select('SUM(houdinv_accounts_balance_sheet_decrease) AS decreaseData, SUM(houdinv_accounts_balance_sheet__increase) AS increaseData')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_date >=',$from)->where('houdinv_accounts_balance_sheet_date <=',$to)
            ->where('houdinv_accounts_balance_sheet_account_id',$getOtherDataList->houdinv_accounts_id)->get()->result();
            if($getData[0]->increaseData != "")
            {
                $setIncrease = $getData[0]->increaseData;
            }
            if($getData[0]->decreaseData != "")
            {
                $setDecrease = $getData[0]->decreaseData;
            }
            $balance = ($setIncrease)-($setDecrease);
            if($balance > 0)
            {
                $setOtherArray[] = array('name'=>$getOtherDataList->houdinv_accounts_name,'balance'=>$balance);
            }
            else
            {
                $setOtherArray = array();        
            }
        }
    }
    else
    {
        $setOtherArray = array();
    }
    return array('from'=>$getWeekDate,'to'=>$getCurrentDate,'currentAssets'=>$setCurrentAssestsArray,'fixedAssets'=>$setFixedAssestsArray,
    'receiveable'=>$setReceivableArray,'payable'=>$setPayableArray,'bank'=>$setBankArray,'credit'=>$setCreditArray,'liblties'=>$setLibaltiesArray,'equity'=>$setEquityArray,
    'expense'=>$setExpenseArray,'otherExpense'=>$setOtherExpenseArray,'undeposited'=>$setUndepositedArray,'income'=>$setIncomeArray,'otherincome'=>$setOtherIncomeArray,
    'other'=>$setOtherArray,'non'=>$setNonAssestsArray,'message'=>'Success');








    // unix time stamp
    // $getCurrentDateUnix = strtotime($to);
    // $getWeekDateUnix = strtotime($from);
    // // get credit info data
    // // get credited amount from transaction
    // $getCreditedAmountFromTransaction = $this->db->select('SUM(houdinv_transaction_amount) AS transactionCredit')->from('houdinv_transaction')->where('houdinv_transaction_date >=',$getWeekDate)
    // ->where('houdinv_transaction_date <=',$getCurrentDate)
    // ->where('houdinv_transaction_type','credit')
    // ->group_start()
    // ->where('houdinv_transaction_for','expense')->or_where('houdinv_transaction_for','sms package')->or_where('houdinv_transaction_for','email package')
    // ->group_end()->get()->result();
    // if($getCreditedAmountFromTransaction[0]->transactionCredit)
    // {
    //     $getCreditAmountFromTransaction = $getCreditedAmountFromTransaction[0]->transactionCredit;
    // }
    // else
    // {
    //     $getCreditAmountFromTransaction = 0;
    // }
    // // get credited amount from orders
    // $getCreditedAmountFromOrders = $this->db->select('SUM(houdinv_orders_total_paid) totalPaidAmount')->from('houdinv_orders')->where('houdinv_order_created_at >=',$getWeekDateUnix)->where('houdinv_order_created_at <=',$getCurrentDateUnix)->get()->result();
    // if($getCreditedAmountFromOrders[0]->totalPaidAmount)
    // {
    //     $getCreditedAmountFromOrders = $getCreditedAmountFromOrders[0]->totalPaidAmount;
    // }
    // else
    // {
    //     $getCreditedAmountFromOrders = 0;
    // }
    // $getFinalCreditedAmount = $getCreditAmountFromTransaction+$getCreditedAmountFromOrders;
    // // get unpaid credit amount
    // $getUnpaidCreditedAmountFromOrders = $this->db->select('SUM(houdinv_orders_total_remaining) totalRemainingAmount')->from('houdinv_orders')->where('houdinv_order_created_at >=',$getWeekDateUnix)->where('houdinv_order_created_at <=',$getCurrentDateUnix)->get()->result();

    // // get debit info data
    // // get debited amount from transaction
    // $getDebitedAmountFromTransaction = $this->db->select('SUM(houdinv_transaction_amount) AS transactionDebit')->from('houdinv_transaction')->where('houdinv_transaction_date >=',$getWeekDate)
    // ->where('houdinv_transaction_date <=',$getCurrentDate)
    // ->where('houdinv_transaction_type','debit')
    // ->group_start()
    // ->where('houdinv_transaction_for','expense')->or_where('houdinv_transaction_for','sms package')->or_where('houdinv_transaction_for','email package')
    // ->group_end()->get()->result();
    // if($getDebitedAmountFromTransaction[0]->transactionDebit)
    // {
    //     $getDebitedAmountFromTransaction = $getDebitedAmountFromTransaction[0]->transactionDebit;
    // }
    // else
    // {
    //     $getDebitedAmountFromTransaction = 0;
    // }
    // // get debited amount from inventory purchase
    // $getDebitedAmountFromInventoryPurchase = $this->db->select('SUM(houdinv_purchase_products_inward_amount_paid) as totalAmountDebit')->from('houdinv_purchase_products_inward')
    // ->where('houdinv_purchase_products_inward_craeted_at >=',$getWeekDateUnix)->where('houdinv_purchase_products_inward_craeted_at <=',$getCurrentDateUnix)->get()->result();
    // if($getDebitedAmountFromInventoryPurchase[0]->totalAmountDebit)
    // {
    //     $getDebitedAmountFromInventoryPurchase = $getDebitedAmountFromInventoryPurchase[0]->totalAmountDebit;
    // }
    // else
    // {
    //     $getDebitedAmountFromInventoryPurchase = 0;
    // }
    // $getFinalPaidAmount = $getDebitedAmountFromTransaction+$getDebitedAmountFromInventoryPurchase;
    // // get remaining debit amount
    // $setRemainingAmount = 0;
    // $getRemainingDebitedAmountFromInventoryPurchase = $this->db->select('houdinv_purchase_products_inward_total_amount,houdinv_purchase_products_inward_amount_paid')->from('houdinv_purchase_products_inward')
    // ->where('houdinv_purchase_products_inward_craeted_at >=',$getWeekDateUnix)->where('houdinv_purchase_products_inward_craeted_at <=',$getCurrentDateUnix)->get()->result();
    // foreach($getRemainingDebitedAmountFromInventoryPurchase as $getRemainingDebitedAmountFromInventoryPurchaseList)
    // {
    //     $getTotalAmount = $getRemainingDebitedAmountFromInventoryPurchaseList->houdinv_purchase_products_inward_total_amount;
    //     $getPaidAmount = $getRemainingDebitedAmountFromInventoryPurchaseList->houdinv_purchase_products_inward_amount_paid;
    //     $setRemainingAmount = $setRemainingAmount+($getTotalAmount-$getPaidAmount);
    // }
    // return array('totalCreditedAmount'=>$getFinalCreditedAmount,'totalRemainingCredit'=>$getUnpaidCreditedAmountFromOrders[0]->totalRemainingAmount,
    // 'finalPaidDebitAmount'=>$getFinalPaidAmount,'remainingDebitAmount'=>$setRemainingAmount,'from'=>$getWeekDate,'to'=>$getCurrentDate,'message'=>'Success');
}


//accounts type

public function getAccountsType()
{
 return $this->db->select('*')->from('houdinv_account_type')->get()->result();  
}

public function getAccountsTypeDetail($data=false)
 {
    if($data)
    {
 $details = $this->db->select('*')->from('houdinv_account_type')->where("account_type_id",$data)
 ->get()->row();  
 if($details)
 {
  //  $details_type = explode(",",$details)
   return  $this->db->select('houdinv_detail_type.houdinv_detail_type_name,houdinv_detail_type.houdinv_detail_type_id')->from('houdinv_account_type,houdinv_detail_type')

    ->where("FIND_IN_SET(houdinv_detail_type.houdinv_detail_type_id,houdinv_account_type.detail_type) !=", 0)
    ->where("houdinv_account_type.account_type_id",$data)
 ->get()->result(); 
 }
 
 
 }
 }
 
 
 public function getAccountsTypeDetailFull($data=false)
 {
    if($data)
    {

  //  $details_type = explode(",",$details)
   return  $this->db->select('*')->from('houdinv_detail_type')
      ->where("houdinv_detail_type.houdinv_detail_type_id",$data)
     ->get()->row(); 
 
 
 
    }
 }
 
 
  public function getaccountTypeMatch($data=false,$id)
 {
    if($data)
    {

  //  $details_type = explode(",",$details)
   return  $this->db->select('*')->from('houdinv_accounts')
      ->where("houdinv_accounts_name",$id)
      ->where("houdinv_accounts_account_type_id",$data)
     ->get()->row(); 
 
 
 
    }
 }
 
 
 
  public function getAccounts($params=false)
 {
  
  $this->db->select("houdinv_accounts.*,houdinv_account_type.account_type_name,houdinv_detail_type.houdinv_detail_type_name,houdinv_detail_type.houdinv_detail_type_report")
  ->from("houdinv_accounts")->where('houdinv_accounts_delete_status','no')
  ->join("houdinv_account_type","houdinv_account_type.account_type_id=houdinv_accounts.houdinv_accounts_account_type_id")->order_by('houdinv_accounts_id','DESC')
  ->join("houdinv_detail_type","houdinv_detail_type.houdinv_detail_type_id=houdinv_accounts.houdinv_accounts_detail_type_id");
     if(array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit'],$params['start']);
            }
            elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit']);
            }
               

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count')
            {
                $result = $this->db->count_all_results();
            }else
            {
                
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
            
           
            return $result;
 }
public function SaveAccounts($data=false,$array2=false,$money = false)
{
    if($data)
    {
        $already = $this->db->select("*")->from('houdinv_accounts')->where("houdinv_accounts_name",$data['houdinv_accounts_name'])->where('houdinv_accounts_delete_status','no')
        ->get()->result();
        if($already)
        {
            return "already";  
        }
        else    
        { 
            if($data['houdinv_accounts_parent_account'])
            {
                $id_get = $this->db->select("*")->from('houdinv_accounts')->where("houdinv_accounts_name",$data['houdinv_accounts_parent_account'])
                ->get()->row();
                $data['houdinv_accounts_parent_account'] =  $id_get->houdinv_accounts_id;
            }
            else
            {
                $data['houdinv_accounts_parent_account'] =  0;
            }
            $this->db->insert("houdinv_accounts",$data);
            $last_id =  $this->db->insert_id();
            if($money)
            {
                $setFinalBalance = $money;
            }
            else
            {
                $setFinalBalance = 0;
            }
            if($last_id)
            {
                $this->db->where('houdinv_accounts_id',$last_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalBalance));
            }
            if(count($array2))
            {
                for($i=0;$i<count($array2);$i++)
                {
                    $row =  $this->db->select("*")->from('houdinv_accounts')->where("houdinv_accounts_id",$last_id)->get()->row();
                    if($row)
                    {
                        $last_balance =$row->houdinv_accounts_final_balance; 
                    }
                    else
                    {
                        $last_balance =0; 
                    }
                    if($data['houdinv_accounts_account_type_id']!=3)
                    { 
                        $final_balance = $last_balance - $array2[$i]['houdinv_accounts_balance_sheet_decrease'];
                        $final_balance = $final_balance + $array2[$i]['houdinv_accounts_balance_sheet__increase'];
                    }
                    else
                    {
                        $final_balance = $last_balance - $array2['houdinv_accounts_balance_sheet_payment'];
                        $final_balance = $final_balance + $array2['houdinv_accounts_balance_sheet_deposit'];  
                    }
                    $array_update = array("houdinv_accounts_final_balance"=>$final_balance);
                    $array2[$i]['houdinv_accounts_balance_sheet_final_balance'] = $final_balance;
                    $array2[$i]['houdinv_accounts_balance_sheet_account_id'] = $last_id;
                    $this->db->insert("houdinv_accounts_balance_sheet",$array2[$i]);
                    $array_id = $this->db->insert_id();
                    if($array_id)
                    {
             
                    }
                
                }
            }
            return $last_id; 
        }
    }
}
 
 
 public function UpdateAccounts($array2=false,$id,$account)
 {
    
  

  if($array2)
  {
    

  
         
                
              $array_id =  $this->db->where("houdinv_accounts_balance_sheet_id",$id)
              ->update("houdinv_accounts_balance_sheet",$array2);
    
 $row =  $this->db->select("*")->from('houdinv_accounts_balance_sheet')
        ->where("houdinv_accounts_balance_sheet_account_id",$account)
      ->get()->result();
      
      $final_balance =0;
      
      foreach($row as $vals)
      {
        $final_balance = $final_balance - $vals->houdinv_accounts_balance_sheet_decrease;
        $final_balance = $final_balance + $vals->houdinv_accounts_balance_sheet__increase;
       
      }
   
    
    
    
     
         $final_balance;
       
               $array_update = array("houdinv_accounts_final_balance"=>$final_balance); 
              
             
              $this->db->where("houdinv_accounts_id",$account)->update("houdinv_accounts",$array_update);
             
              
                
        
    
  }
   
  return $account; 
 
 
 
 
    
 }
 
 
  public function DeleteAccounts($id,$account)
 {

    

  $this->db->delete("houdinv_accounts_balance_sheet", array('houdinv_accounts_balance_sheet_id' => $id));  
         
         
    
 $row =  $this->db->select("*")->from('houdinv_accounts_balance_sheet')
        ->where("houdinv_accounts_balance_sheet_account_id",$account)
      ->get()->result();
      
      $final_balance =0;
      
      foreach($row as $vals)
      {
        $final_balance = $final_balance - $vals->houdinv_accounts_balance_sheet_decrease;
        $final_balance = $final_balance + $vals->houdinv_accounts_balance_sheet__increase;
       
      }
   
    
    
    
     
     echo $final_balance;
       
               $array_update = array("houdinv_accounts_final_balance"=>$final_balance); 
              
             
              $this->db->where("houdinv_accounts_id",$account)->update("houdinv_accounts",$array_update);
             
              
                
        
    
 
   
  return $account; 
 
 
 
 
    
 }
 
 
 public function getAccountHistory($id)
 {
    //   
    $getData = $this->db->select("houdinv_accounts_balance_sheet.*,houdinv_accounts.houdinv_accounts_final_balance as balance,houdinv_accounts.houdinv_accounts_account_type_id")->from("houdinv_accounts_balance_sheet")
   ->join("houdinv_accounts","houdinv_accounts.houdinv_accounts_id=houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account_id",'left outer')
   ->where("houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account_id",$id)->or_where('houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account',$id)
   ->order_by("houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_id","ASC")
  ->get()->result_array(); 
   $setParentArray = array();
   $setTempBal = "";
    foreach($getData as $getDataList)
    {
        // get account name
        $getAccountName = $this->db->select('houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account,houdinv_accounts.houdinv_accounts_name,
        houdinv_accounts.houdinv_accounts_delete_status,houdinv_accounts.houdinv_accounts_account_type_id')
        ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_id',$getDataList['houdinv_accounts_balance_sheet_id'])
        ->join('houdinv_accounts','houdinv_accounts.houdinv_accounts_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account_id','left outer')->get()->result();
        if(count($getAccountName) > 0)
        {
            if($getAccountName[0]->houdinv_accounts_name)
            {
                if($getAccountName[0]->houdinv_accounts_delete_status == 'no')
                {   
                    $setAccountName = $getAccountName[0]->houdinv_accounts_name;
                }
                else
                {
                    $setAccountName = $getAccountName[0]->houdinv_accounts_name."(deleted)";
                }
            }
            else
            {
                $setAccountName = "";    
            }
        }
        else
        {
            $setAccountName = "";
        }
        // account balance start
        if($getAccountName[0]->houdinv_accounts_account_type_id == 2 || $getAccountName[0]->houdinv_accounts_account_type_id == 4 || $getAccountName[0]->houdinv_accounts_account_type_id == 5
        || $getAccountName[0]->houdinv_accounts_account_type_id == 1 || $getAccountName[0]->houdinv_accounts_account_type_id == 3 || $getAccountName[0]->houdinv_accounts_account_type_id == 13 ||
        $getAccountName[0]->houdinv_accounts_account_type_id == 14 || $getAccountName[0]->houdinv_accounts_account_type_id == 15)
        {
            if($getDataList['houdinv_accounts_balance_sheet_account']==$id){
            $setIncrease =false;
            $setDecrease = $getDataList['houdinv_accounts_balance_sheet__increase'];
        }else{
            $setIncrease = $getDataList['houdinv_accounts_balance_sheet_decrease'];
            $setDecrease = $getDataList['houdinv_accounts_balance_sheet__increase'];
        }
        }
        else
        {

            if($getDataList['houdinv_accounts_balance_sheet_account']==$id){
                $setIncrease = false;
                $setDecrease = -$getDataList['houdinv_accounts_balance_sheet__increase'];
                 
            }else{
                $setIncrease = $getDataList['houdinv_accounts_balance_sheet__increase'];
                $setDecrease = $getDataList['houdinv_accounts_balance_sheet_decrease'];
                 
            }
           
       
       
        }
        if($setTempBal)
        {
            if($getDataList['houdinv_accounts_balance_sheet_ref_type'] == 'journal Entry' && $getDataList['houdinv_accounts_balance_sheet_account'] == $id)
            {
                if($setIncrease)
                {
                    $setTempBal = $setTempBal-$setIncrease;
                }
                else
                {
                    $setTempBal = $setTempBal+$setDecrease;
                }
            }
            else if($getDataList['houdinv_accounts_balance_sheet_account'] == $id)
            {  
                if($setIncrease)
                {
                    $setTempBal = $setTempBal-$setIncrease;
                }
                else
                {
                    $setTempBal = $setTempBal+$setDecrease;
                }
            }
            else
            {
                if($setIncrease)
                {
                    $setTempBal = $setTempBal+$setIncrease;
                }
                else
                {
                    $setTempBal = $setTempBal-$setDecrease;
                }
            }
             
        }
        else
        {
           
            if($getDataList['houdinv_accounts_balance_sheet_ref_type'] == 'journal Entry' && $getDataList['houdinv_accounts_balance_sheet_account'] == $id)
            {
                if($setIncrease)
                {
                    $setTempBal = $setTempBal-$setIncrease;
                }
                else
                {
                    $setTempBal = $setTempBal+$setDecrease;
                }
            }
            else if($getDataList['houdinv_accounts_balance_sheet_account'] == $id)
            {
                if($setIncrease)
                {
                    $setTempBal = $setTempBal-$setIncrease;
                }
                else
                {
                    $setTempBal = $setTempBal+$setDecrease;
                }
            }
            else
            {
                if($setIncrease)
                {
                    $setTempBal = $setTempBal+$setIncrease;
                }
                else
                {
                    $setTempBal = $setTempBal-$setDecrease;
                }
            }
        }
        // accoutn balance end
        if($getDataList['houdinv_accounts_balance_sheet_pay_account'])
        {
            if($getDataList['houdinv_accounts_balance_sheet_payee_type'] == 'supplier')
            {
                $getSupplier = $this->db->select('houdinv_supplier_contact_person_name,houdinv_supplier_id')->from('houdinv_suppliers')->where('houdinv_supplier_id',$getDataList['houdinv_accounts_balance_sheet_pay_account'])->get()->result();
                $setData = array('main'=>$getDataList,'name'=>$getSupplier[0]->houdinv_supplier_contact_person_name,'id'=>$getSupplier[0]->houdinv_supplier_id,'type'=>'supplier','balance'=>$setTempBal,'account'=>$setAccountName);
            }
            else
            {
                $getUser = $this->db->select('houdinv_user_name,houdinv_user_id')->from('houdinv_users')->where('houdinv_user_id',$getDataList['houdinv_accounts_balance_sheet_pay_account'])->get()->result();
                $setData = array('main'=>$getDataList,'name'=>$getUser[0]->houdinv_user_name,'id'=>$getUser[0]->houdinv_user_id,'type'=>'customer','balance'=>$setTempBal,'account'=>$setAccountName);
            }
        }
        else
        {
            $setData = array('main'=>$getDataList,'name'=>'','id'=>'','type'=>'customer','balance'=>$setTempBal,'account'=>$setAccountName);
        }
        array_push($setParentArray,$setData); 
    }
    // print_r($setParentArray);
    return array_reverse($setParentArray);
 }
 public function getAccountReport($id,$data = false)
 {
    if($data)
    {
        $getData = $this->db->select("houdinv_accounts_balance_sheet.*,houdinv_accounts.houdinv_accounts_final_balance as balance,houdinv_accounts.houdinv_accounts_account_type_id")->from("houdinv_accounts_balance_sheet")
        ->join("houdinv_accounts","houdinv_accounts.houdinv_accounts_id=houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account_id",'left outer')
        ->where('houdinv_accounts_balance_sheet_date >=',$data['from'])
        ->where('houdinv_accounts_balance_sheet_date <=',$data['to'])
        ->group_start()
        ->where("houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account_id",$data['id'])->or_where('houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account',$data['id'])
        ->group_end()
        ->order_by("houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_id","ASC")
        ->get()->result_array(); 
        $setParentArray = array();
        $setTempBal = "";
        foreach($getData as $getDataList)
        {
            // get account name
            $getAccountName = $this->db->select('houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account,houdinv_accounts.houdinv_accounts_name,houdinv_accounts.houdinv_accounts_delete_status')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_id',$getDataList['houdinv_accounts_balance_sheet_id'])
            ->join('houdinv_accounts','houdinv_accounts.houdinv_accounts_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account','left outer')->get()->result();
            if(count($getAccountName) > 0)
            {
                if($getAccountName[0]->houdinv_accounts_name)
                {
                    if($getAccountName[0]->houdinv_accounts_delete_status == 'no')
                    {   
                        $setAccountName = $getAccountName[0]->houdinv_accounts_name;
                    }
                    else
                    {
                        $setAccountName = $getAccountName[0]->houdinv_accounts_name."(deleted)";
                    }
                }
                else
                {
                    $setAccountName = "";    
                }
            }
            else
            {
                $setAccountName = "";
            }
            if($setTempBal)
            {
                if($getDataList['houdinv_accounts_balance_sheet_ref_type'] == 'journal Entry' && $getDataList['houdinv_accounts_balance_sheet_account'] == $id)
                {
                    if($getDataList['houdinv_accounts_balance_sheet__increase'])
                    {
                        $setTempBal = $setTempBal-$getDataList['houdinv_accounts_balance_sheet__increase'];
                    }
                    else
                    {
                        $setTempBal = $setTempBal+$getDataList['houdinv_accounts_balance_sheet_decrease'];
                    }
                }
                else if($getDataList['houdinv_accounts_balance_sheet_ref_type'] == 'Transfer' && $getDataList['houdinv_accounts_balance_sheet_account'] == $id)
                {
                    if($getDataList['houdinv_accounts_balance_sheet__increase'])
                    {
                        $setTempBal = $setTempBal-$getDataList['houdinv_accounts_balance_sheet__increase'];
                    }
                    else
                    {
                        $setTempBal = $setTempBal+$getDataList['houdinv_accounts_balance_sheet_decrease'];
                    }
                }
                else
                {
                    if($getDataList['houdinv_accounts_balance_sheet__increase'])
                    {
                        $setTempBal = $setTempBal+$getDataList['houdinv_accounts_balance_sheet__increase'];
                    }
                    else
                    {
                        $setTempBal = $setTempBal-$getDataList['houdinv_accounts_balance_sheet_decrease'];
                    }
                }
            }
            else
            {
                $setTempBal = $getDataList['houdinv_accounts_balance_sheet__increase'];
            }
            if($getDataList['houdinv_accounts_balance_sheet_pay_account'])
            {
                if($getDataList['houdinv_accounts_balance_sheet_payee_type'] == 'supplier')
                {
                    $getSupplier = $this->db->select('houdinv_supplier_contact_person_name,houdinv_supplier_id')->from('houdinv_suppliers')->where('houdinv_supplier_id',$getDataList['houdinv_accounts_balance_sheet_pay_account'])->get()->result();
                    $setData = array('main'=>$getDataList,'name'=>$getSupplier[0]->houdinv_supplier_contact_person_name,'id'=>$getSupplier[0]->houdinv_supplier_id,'type'=>'supplier','balance'=>$setTempBal,'account'=>$setAccountName);
                }
                else
                {
                    $getUser = $this->db->select('houdinv_user_name,houdinv_user_id')->from('houdinv_users')->where('houdinv_user_id',$getDataList['houdinv_accounts_balance_sheet_pay_account'])->get()->result();
                    $setData = array('main'=>$getDataList,'name'=>$getUser[0]->houdinv_user_name,'id'=>$getUser[0]->houdinv_user_id,'type'=>'customer','balance'=>$setTempBal,'account'=>$setAccountName);
                }
            }
            else
            {
                $setData = array('main'=>$getDataList,'name'=>'','id'=>'','type'=>'customer','balance'=>$setTempBal,'account'=>$setAccountName);
            }
            array_push($setParentArray,$setData);  
        }
        return $setParentArray;
    }
    else
    {
        $getData = $this->db->select("houdinv_accounts_balance_sheet.*,houdinv_accounts.houdinv_accounts_final_balance as balance,houdinv_accounts.houdinv_accounts_account_type_id")->from("houdinv_accounts_balance_sheet")
        ->join("houdinv_accounts","houdinv_accounts.houdinv_accounts_id=houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account_id",'left outer')->where('houdinv_accounts_balance_sheet_date',date('Y-m-d'))
        ->where("houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account_id",$id)->or_where('houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account',$id)
        ->order_by("houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_id","ASC")
        ->get()->result_array(); 
        $setParentArray = array();
        $setTempBal = "";
        foreach($getData as $getDataList)
        {
            // get account name
            $getAccountName = $this->db->select('houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account,houdinv_accounts.houdinv_accounts_name,houdinv_accounts.houdinv_accounts_delete_status')
            ->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_id',$getDataList['houdinv_accounts_balance_sheet_id'])
            ->join('houdinv_accounts','houdinv_accounts.houdinv_accounts_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account','left outer')->get()->result();
            if(count($getAccountName) > 0)
            {
                if($getAccountName[0]->houdinv_accounts_name)
                {
                    if($getAccountName[0]->houdinv_accounts_delete_status == 'no')
                    {   
                        $setAccountName = $getAccountName[0]->houdinv_accounts_name;
                    }
                    else
                    {
                        $setAccountName = $getAccountName[0]->houdinv_accounts_name."(deleted)";
                    }
                }
                else
                {
                    $setAccountName = "";    
                }
            }
            else
            {
                $setAccountName = "";
            }
            if($setTempBal)
            {
                if($getDataList['houdinv_accounts_balance_sheet_ref_type'] == 'journal Entry' && $getDataList['houdinv_accounts_balance_sheet_account'] == $id)
                {
                    if($getDataList['houdinv_accounts_balance_sheet__increase'])
                    {
                        $setTempBal = $setTempBal-$getDataList['houdinv_accounts_balance_sheet__increase'];
                    }
                    else
                    {
                        $setTempBal = $setTempBal+$getDataList['houdinv_accounts_balance_sheet_decrease'];
                    }
                }
                else if($getDataList['houdinv_accounts_balance_sheet_ref_type'] == 'Transfer' && $getDataList['houdinv_accounts_balance_sheet_account'] == $id)
                {
                    if($getDataList['houdinv_accounts_balance_sheet__increase'])
                    {
                        $setTempBal = $setTempBal-$getDataList['houdinv_accounts_balance_sheet__increase'];
                    }
                    else
                    {
                        $setTempBal = $setTempBal+$getDataList['houdinv_accounts_balance_sheet_decrease'];
                    }
                }
                else
                {
                    if($getDataList['houdinv_accounts_balance_sheet__increase'])
                    {
                        $setTempBal = $setTempBal+$getDataList['houdinv_accounts_balance_sheet__increase'];
                    }
                    else
                    {
                        $setTempBal = $setTempBal-$getDataList['houdinv_accounts_balance_sheet_decrease'];
                    }
                }
            }
            else
            {
                $setTempBal = $getDataList['houdinv_accounts_balance_sheet__increase'];
            }
            if($getDataList['houdinv_accounts_balance_sheet_pay_account'])
            {
                if($getDataList['houdinv_accounts_balance_sheet_payee_type'] == 'supplier')
                {
                    $getSupplier = $this->db->select('houdinv_supplier_contact_person_name,houdinv_supplier_id')->from('houdinv_suppliers')->where('houdinv_supplier_id',$getDataList['houdinv_accounts_balance_sheet_pay_account'])->get()->result();
                    $setData = array('main'=>$getDataList,'name'=>$getSupplier[0]->houdinv_supplier_contact_person_name,'id'=>$getSupplier[0]->houdinv_supplier_id,'type'=>'supplier','balance'=>$setTempBal,'account'=>$setAccountName);
                }
                else
                {
                    $getUser = $this->db->select('houdinv_user_name,houdinv_user_id')->from('houdinv_users')->where('houdinv_user_id',$getDataList['houdinv_accounts_balance_sheet_pay_account'])->get()->result();
                    $setData = array('main'=>$getDataList,'name'=>$getUser[0]->houdinv_user_name,'id'=>$getUser[0]->houdinv_user_id,'type'=>'customer','balance'=>$setTempBal,'account'=>$setAccountName);
                }
            }
            else
            {
                $setData = array('main'=>$getDataList,'name'=>'','id'=>'','type'=>'customer','balance'=>$setTempBal,'account'=>$setAccountName);
            }
            array_push($setParentArray,$setData);  
        }
        return $setParentArray;
    }
}
public function getAccountName($data)
{
    $getAccountName = $this->db->select('houdinv_accounts_name')->from('houdinv_accounts')->where('houdinv_accounts_id',$data)->get()->result();
    return $getAccountName[0]->houdinv_accounts_name;

}
 public function getCustomerAndSuplier()
 {
    
  $user =  $this->db->select("*")->from("houdinv_users")
   ->where("houdinv_user_is_active",'active')->get()->result_array(); 
   
   $supplier =  $this->db->select("*")->from("houdinv_suppliers")
   ->where("houdinv_supplier_active_status",'active')->get()->result_array();  
   
   foreach($user as $data)
   {
    $array[] = array("name"=>$data['houdinv_user_name'],"type"=>"customer","id"=>$data['houdinv_user_id']);
   }
   
    foreach($supplier as $data)
   {
    $array[] = array("name"=>$data['houdinv_supplier_contact_person_name'],"type"=>"supplier","id"=>$data['houdinv_supplier_id']);
   }
    return $array;
 }
 public function getParentAccount($data = false)
 {
     $this->db->select('*')->from('houdinv_accounts')->where('houdinv_accounts_delete_status','no');
     if($data) { $this->db->where('houdinv_accounts_id !=',$data); }
     $gteParentAccount = $this->db->get()->result();
     return $gteParentAccount;
 }


 public function balancetransfer($data,$id,$parent = false)
 {
    $getData = $this->db->insert('houdinv_accounts_balance_sheet',$data);
    $getLastId = $this->db->insert_id();
   $Accountfrom= $id;


    if($getLastId)
    {
        $getTotalSum = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_accounts.houdinv_accounts_account_type_id')->from('houdinv_accounts_balance_sheet')
        ->join('houdinv_accounts','houdinv_accounts.houdinv_accounts_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account_id','left outer')
        ->where('houdinv_accounts_balance_sheet_account_id',$id)->or_where('houdinv_accounts_balance_sheet_account',$id)->get()->result();
        if(count($getTotalSum) > 0)
        {
            $getProfitData = 0;
            $getLossData = 0;
            foreach($getTotalSum as $getTotalSumList)
            {
                if($getTotalSumList->houdinv_accounts_account_type_id == 2 || $getTotalSumList->houdinv_accounts_account_type_id == 4 || $getTotalSumList->houdinv_accounts_account_type_id == 5
                || $getTotalSumList->houdinv_accounts_account_type_id == 1 || $getTotalSumList->houdinv_accounts_account_type_id == 3 || $getTotalSumList->houdinv_accounts_account_type_id == 13
                || $getTotalSumList->houdinv_accounts_account_type_id == 14 || $getTotalSumList->houdinv_accounts_account_type_id == 15)
                {
                    $setIncreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet_decrease;
                    $setDecreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet__increase;
                }
                else
                {
                    $setIncreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet__increase;
                    $setDecreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet_decrease;
                }
                
                if($getTotalSumList->houdinv_accounts_balance_sheet_ref_type == 'journal Entry' && $getTotalSumList->houdinv_accounts_balance_sheet_account == $id)
                {
                    $setIncrease = $setDecreaseValue;
                    $setDecrese = $setIncreaseValue;
                }
                else if($getTotalSumList->houdinv_accounts_balance_sheet_account == $id)
                {
                    $setIncrease = $setDecreaseValue;
                    $setDecrese = $setIncreaseValue;
                }
                else
                {
                    $setDecrese = $setDecreaseValue;
                    $setIncrease = $setIncreaseValue;
                }
               
                $getProfitData = $getProfitData+$setIncrease;
                $getLossData = $getLossData+$setDecrese;
            }
            $getFinalBalanceData = $getProfitData-$getLossData;
            $this->db->where('houdinv_accounts_balance_sheet_id',$getLastId);
            $this->db->update('houdinv_accounts_balance_sheet',array('houdinv_accounts_balance_sheet_final_balance'=>$getFinalBalanceData));
        }
        $filneldatauodateFrom=  $this->getEndingBalanceData($Accountfrom);

        $this->db->where('houdinv_accounts_id',$id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$filneldatauodateFrom['balance']));   
        if($parent)
        {
            $setCreatedDate = strtotime(date('Y-m-d'));
            
            $getparentAccount = $this->db->select('*')
            ->from('houdinv_accounts')
            //->join('houdinv_accounts as B','B.houdinv_accounts_id = A.houdinv_accounts_balance_sheet_account_id','left outer')
            ->where('houdinv_accounts_id',$parent)
            ->get()->row();

            $getFinalBalance = $getparentAccount->houdinv_accounts_final_balance+$data['houdinv_accounts_balance_sheet__increase'];
               

            $ToAccountdata = array('houdinv_accounts_balance_sheet_account_id'=>$parent,            
            'houdinv_accounts_balance_sheet_account'=>$Accountfrom,             
            'houdinv_accounts_balance_sheet_created_at'=>$setCreatedDate,
            'houdinv_accounts_balance_sheet_memo'=>$data['houdinv_accounts_balance_sheet_memo'],
            'houdinv_accounts_balance_sheet_decrease'=>0,
            'houdinv_accounts_balance_sheet__increase'=>$data['houdinv_accounts_balance_sheet__increase'],
            'houdinv_accounts_balance_sheet_final_balance'=>$getFinalBalance,
            'houdinv_accounts_balance_sheet_ref_type' => $data['houdinv_accounts_balance_sheet_ref_type'],
            'houdinv_accounts_balance_sheet_date'=>$data['houdinv_accounts_balance_sheet_date'],
            ); 


           // $ToAccountdataQ = $this->db->insert('houdinv_accounts_balance_sheet',$ToAccountdata);
        $filneldatauodate=  $this->getEndingBalanceData($parent);


          $this->db->where('houdinv_accounts_id',$parent)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$filneldatauodate['balance']));
            
        }
         
        return array('message'=>'yes');  
    }
    else
    {
        return array('message'=>'no');
    }

 } 

public function updateAccountData($data,$id,$parent = false)
{
    $getData = $this->db->insert('houdinv_accounts_balance_sheet',$data);
    $getLastId = $this->db->insert_id();
    if($getLastId)
    {
        $getTotalSum = $this->db->select('houdinv_accounts_balance_sheet.*,houdinv_accounts.houdinv_accounts_account_type_id')->from('houdinv_accounts_balance_sheet')
        ->join('houdinv_accounts','houdinv_accounts.houdinv_accounts_id = houdinv_accounts_balance_sheet.houdinv_accounts_balance_sheet_account_id','left outer')
        ->where('houdinv_accounts_balance_sheet_account_id',$id)->or_where('houdinv_accounts_balance_sheet_account',$id)->get()->result();
        if(count($getTotalSum) > 0)
        {
            $getProfitData = 0;
            $getLossData = 0;
            foreach($getTotalSum as $getTotalSumList)
            {
                if($getTotalSumList->houdinv_accounts_account_type_id == 2 || $getTotalSumList->houdinv_accounts_account_type_id == 4 || $getTotalSumList->houdinv_accounts_account_type_id == 5
                || $getTotalSumList->houdinv_accounts_account_type_id == 1 || $getTotalSumList->houdinv_accounts_account_type_id == 3 || $getTotalSumList->houdinv_accounts_account_type_id == 13
                || $getTotalSumList->houdinv_accounts_account_type_id == 14 || $getTotalSumList->houdinv_accounts_account_type_id == 15)
                {
                    $setIncreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet_decrease;
                    $setDecreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet__increase;
                }
                else
                {
                    $setIncreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet__increase;
                    $setDecreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet_decrease;
                }
                
                if($getTotalSumList->houdinv_accounts_balance_sheet_ref_type == 'journal Entry' && $getTotalSumList->houdinv_accounts_balance_sheet_account == $id)
                {
                    $setIncrease = $setDecreaseValue;
                    $setDecrese = $setIncreaseValue;
                }
                else if($getTotalSumList->houdinv_accounts_balance_sheet_ref_type == 'Transfer' && $getTotalSumList->houdinv_accounts_balance_sheet_account == $id)
                {
                    $setIncrease = $setDecreaseValue;
                    $setDecrese = $setIncreaseValue;
                }
                else
                {
                    $setDecrese = $setDecreaseValue;
                    $setIncrease = $setIncreaseValue;
                }
               
                $getProfitData = $getProfitData+$setIncrease;
                $getLossData = $getLossData+$setDecrese;
            }
            $getFinalBalanceData = $getProfitData-$getLossData;
            $this->db->where('houdinv_accounts_balance_sheet_id',$getLastId);
            $this->db->update('houdinv_accounts_balance_sheet',array('houdinv_accounts_balance_sheet_final_balance'=>$getFinalBalanceData));
        }
        $this->db->where('houdinv_accounts_id',$id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$getFinalBalanceData));   
        if($parent)
        {
            $getparentAccount = $this->db->select('houdinv_accounts_balance_sheet_id.*.houdinv_accounts.houdinv_accounts_account_type_id')
            ->from('houdinv_accounts_balance_sheet')
            ->join('houdinv_accounts','houdinv_accounts.houdinv_accounts_id = houdinv_accounts_balance_sheet_id.houdinv_accounts_balance_sheet_account_id','left outer')
            ->where('houdinv_accounts_balance_sheet_account_id',$parent)
            ->get()->result();
            if(count($getparentAccount) > 0)
            {
                $setIncreaseValue = 0;
                $setDecreaseValue = 0;
                foreach($getparentAccount as $getparentAccountList)
                {
                    if($getparentAccount[0]->decreaseData)
                    {
                        $setDecreaseData = $getparentAccount[0]->decreaseData;
                    }
                    else
                    {
                        $setDecreaseData = 0;
                    }
                    $setDecreaseValue = $setDecreaseValue+$setDecreaseData;
                    if($getparentAccount[0]->increaseData)
                    {
                        $setIncreaseData = $getparentAccount[0]->increaseData;
                    }
                    else
                    {
                        $setIncreaseData = 0;
                    }
                    $setIncreaseValue = $setIncreaseValue+$setIncreaseData;
                }
                $getFinalBalance = $setIncreaseValue-$setDecreaseValue;
                $this->db->where('houdinv_accounts_id',$parent)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$getFinalBalance));
            }
        }
        // update new account data
        // if($data['houdinv_accounts_balance_sheet_account'])
        // {
        //     $getAccountId = $data['houdinv_accounts_balance_sheet_account'];
        //     $getTotalSum = $this->db->select('*')->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_account_id',$getAccountId)->or_where('houdinv_accounts_balance_sheet_account',$getAccountId)->get()->result();
        //     if(count($getTotalSum) > 0)
        //     {
        //         $getProfitData = 0;
        //         $getLossData = 0;
        //         foreach($getTotalSum as $getTotalSumList)
        //         {
        //             if($getTotalSumList->houdinv_accounts_balance_sheet_ref_type == 'journal Entry' && $getTotalSumList->houdinv_accounts_balance_sheet_account == $getAccountId)
        //             {
        //                 $setIncrease = $getTotalSumList->houdinv_accounts_balance_sheet_decrease;
        //                 $setDecrese = $getTotalSumList->houdinv_accounts_balance_sheet__increase;
        //             }
        //             else if($getTotalSumList->houdinv_accounts_balance_sheet_ref_type == 'Transfer' && $getTotalSumList->houdinv_accounts_balance_sheet_account == $getAccountId)
        //             {
        //                 $setIncrease = $getTotalSumList->houdinv_accounts_balance_sheet_decrease;
        //                 $setDecrese = $getTotalSumList->houdinv_accounts_balance_sheet__increase;
        //             }
        //             else
        //             {
        //                 $setDecrese = $getTotalSumList->houdinv_accounts_balance_sheet_decrease;
        //                 $setIncrease = $getTotalSumList->houdinv_accounts_balance_sheet__increase;
        //             }
        //             $getProfitData = $getProfitData+$setIncrease;
        //             $getLossData = $getLossData+$setDecrese;
        //         }
        //         $getFinalBalance = $getProfitData-$getLossData;
        //         $this->db->where('houdinv_accounts_id',$getAccountId)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$getFinalBalance));   
        //     }
        // }
        // else
        // {
            
        // }
        return array('message'=>'yes');  
    }
    else
    {
        return array('message'=>'no');
    }
}
public function getEndingBalanceData($data)
{
    $getTotalSum = $this->db->select('*')->from('houdinv_accounts_balance_sheet')->where('houdinv_accounts_balance_sheet_account_id',$data)->or_where('houdinv_accounts_balance_sheet_account',$data)->get()->result();
    if(count($getTotalSum) > 0)
    {
        $getProfitData = 0;
        $getLossData = 0;
        foreach($getTotalSum as $getTotalSumList)
        {
            $getAccountType = $this->db->select('houdinv_accounts.houdinv_accounts_account_type_id')->from('houdinv_accounts')
            ->where('houdinv_accounts.houdinv_accounts_id',$getTotalSumList->houdinv_accounts_balance_sheet_account_id)->get()->result();
            if($getAccountType[0]->houdinv_accounts_account_type_id == 2 || $getAccountType[0]->houdinv_accounts_account_type_id == 4 || $getAccountType[0]->houdinv_accounts_account_type_id == 5
            || $getAccountType[0]->houdinv_accounts_account_type_id == 1 || $getAccountType[0]->houdinv_accounts_account_type_id == 3 || $getAccountType[0]->houdinv_accounts_account_type_id == 13
            || $getAccountType[0]->houdinv_accounts_account_type_id == 14 || $getAccountType[0]->houdinv_accounts_account_type_id == 15)
            {
                if($getTotalSumList->houdinv_accounts_balance_sheet_account==$data){


                $setIncreaseValue =false;
                $setDecreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet__increase;
            }else{
                $setIncreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet_decrease;
                $setDecreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet__increase;
            }
            }
            else
            {
                if($getTotalSumList->houdinv_accounts_balance_sheet_account==$data){

                $setIncreaseValue =false;
                $setDecreaseValue =  -$getTotalSumList->houdinv_accounts_balance_sheet__increase;

                }else{
                    $setIncreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet__increase;
                    $setDecreaseValue = $getTotalSumList->houdinv_accounts_balance_sheet_decrease;
                }
            }
            if($getTotalSumList->houdinv_accounts_balance_sheet_ref_type == 'journal Entry' && $getTotalSumList->houdinv_accounts_balance_sheet_account == $data)
            {
                $setIncrease = $setDecreaseValue;
                $setDecrese = $setIncreaseValue;
            } 
            if($getTotalSumList->houdinv_accounts_balance_sheet_account == $data)
            {
                $setIncrease = $setDecreaseValue;
                $setDecrese = $setIncreaseValue;
            } 
            else
            {
                $setDecrese = $setDecreaseValue;
                $setIncrease = $setIncreaseValue;
            }
            $getProfitData = ($getProfitData)+($setIncrease);
            $getLossData = ($getLossData)+($setDecrese);
        }
        $getFinalBalance = ($getProfitData)-($getLossData);
        return array('balance'=>$getFinalBalance);
    }
    else
    {
        return array('balance'=>'0');
    }
}
public function getAccountNameData($data)
{
    $getAccountName = $this->db->select('houdinv_accounts_name')->from('houdinv_accounts')->where('houdinv_accounts_id',$data)->get()->result();
    return $getAccountName[0]->houdinv_accounts_name;
}
public function deleteMainAccounts($data)
{
    return $this->db->where('houdinv_accounts_id',$data)->update('houdinv_accounts',array('houdinv_accounts_delete_status'=>'yes'));
}
// delete balance sheet
public function deleteBalanceSheetdata($sheetid,$accountId)
{
    $getBalanceSheetDeleteStatus = $this->db->where('houdinv_accounts_balance_sheet_id',$sheetid)->delete('houdinv_accounts_balance_sheet');
    if($getBalanceSheetDeleteStatus)
    {
        // get amount
        $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
        ->where('houdinv_accounts_balance_sheet_account_id',$accountId)->get()->result();
        if(count($getTotalAmount) > 0)
        {
            $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
        }
        else
        {
            $setFinalAmount = 0;
        }
        $getAccount = $this->db->where('houdinv_accounts_id',$accountId)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
        return $getAccount;
    }
}
// update payable entry
    public function updatePayableEntry($data)
    {
        $getExtraExpense = $this->db->insert('houdinv_extra_expence',$data);
        if($getExtraExpense) 
        {
            // if payment mode is online
            if($data['houdinv_extra_expence_payment_method'] == 'card' || $data['houdinv_extra_expence_payment_method'] == 'cheque')
            {
                if($data['houdinv_extra_expence_transaction_type'] == 'credit')
                {
                    $setIncrease = $data['houdinv_extra_expence_total_amount'];
                    $setDecrease = 0;
                }
                else
                {
                    $setIncrease = 0;
                    $setDecrease = $data['houdinv_extra_expence_total_amount'];
                }
                // add current fund
                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
                if(count($getUndepositedAccount) > 0)
                {
                    $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                    'houdinv_accounts_balance_sheet_ref_type'=>'Current',
                    'houdinv_accounts_balance_sheet_pay_account'=>$data['houdinv_extra_expence_payee'],
                    'houdinv_accounts_balance_sheet_payee_type'=>$data['houdinv_extra_expence_payee_type'],
                    'houdinv_accounts_balance_sheet_account'=>0,
                    'houdinv_accounts_balance_sheet_order_id'=>0,
                    'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                    'houdinv_accounts_balance_sheet__increase'=>$setIncrease,
                    'houdinv_accounts_balance_sheet_decrease'=>$setDecrease,
                    'houdinv_accounts_balance_sheet_deposit'=>0,
                    'houdinv_accounts_balance_sheet_final_balance'=>0,
                    'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'),'houdinv_accounts_balance_sheet_reference'=>$data['houdinv_extra_expence_refrence_number']);
                    $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                    // get amount
                    $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                    ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                    if(count($getTotalAmount) > 0)
                    {
                        $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                    }
                    else
                    {
                        $setFinalAmount = 0;
                    }
                    $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                }
            }
            // if payment mode is offline
            else
            {
                if($data['houdinv_extra_expence_transaction_type'] == 'credit')
                {
                    $setIncrease = $data['houdinv_extra_expence_total_amount'];
                    $setDecrease = 0;
                }
                else
                {
                    $setIncrease = 0;
                    $setDecrease = $data['houdinv_extra_expence_total_amount'];
                }
                // add undeposited fund
                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->get()->result();    
                if(count($getUndepositedAccount) > 0)
                {
                    $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                    'houdinv_accounts_balance_sheet_ref_type'=>'Undeposited Fund',
                    'houdinv_accounts_balance_sheet_pay_account'=>$data['houdinv_extra_expence_payee'],
                    'houdinv_accounts_balance_sheet_payee_type'=>$data['houdinv_extra_expence_payee_type'],
                    'houdinv_accounts_balance_sheet_account'=>0,
                    'houdinv_accounts_balance_sheet_order_id'=>0,
                    'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                    'houdinv_accounts_balance_sheet__increase'=>$setIncrease,
                    'houdinv_accounts_balance_sheet_decrease'=>$setDecrease,
                    'houdinv_accounts_balance_sheet_deposit'=>0,
                    'houdinv_accounts_balance_sheet_final_balance'=>0,
                    'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'),'houdinv_accounts_balance_sheet_reference'=>$data['houdinv_extra_expence_refrence_number']);
                    $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                    // get amount
                    $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                    ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                    if(count($getTotalAmount) > 0)
                    {
                        $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                    }
                    else
                    {
                        $setFinalAmount = 0;
                    }
                    $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                }
            }
            // entry for account payable
            if($data['houdinv_extra_expence_transaction_type'] == 'credit')
            {
                $setIncrease = $data['houdinv_extra_expence_total_amount'];
                $setDecrease = 0;
            }
            else
            {
                $setIncrease = 0;
                $setDecrease = $data['houdinv_extra_expence_total_amount'];
            }
            // add Account payable entry
            $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','6')->where('	houdinv_accounts_detail_type_id','52')->get()->result();    
            if(count($getUndepositedAccount) > 0)
            {
                $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                'houdinv_accounts_balance_sheet_ref_type'=>'Accounts payable (Creditors)',
                'houdinv_accounts_balance_sheet_pay_account'=>$data['houdinv_extra_expence_payee'],
                'houdinv_accounts_balance_sheet_payee_type'=>$data['houdinv_extra_expence_payee_type'],
                'houdinv_accounts_balance_sheet_account'=>0,
                'houdinv_accounts_balance_sheet_order_id'=>0,
                'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                'houdinv_accounts_balance_sheet__increase'=>$setIncrease,
                'houdinv_accounts_balance_sheet_decrease'=>$setDecrease,
                'houdinv_accounts_balance_sheet_deposit'=>0,
                'houdinv_accounts_balance_sheet_final_balance'=>0,
                'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'),'houdinv_accounts_balance_sheet_reference'=>$data['houdinv_extra_expence_refrence_number']);
                $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                // get amount
                $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                if(count($getTotalAmount) > 0)
                {
                    $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                }
                else
                {
                    $setFinalAmount = 0;
                }
                $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
            }
            return array('response'=>'success','message'=>'Entry added successfully');
        }
        else
        {
            return array('response'=>'error','message'=>'Something went wrong. Please try again');
        }
    }
    public function getTaxData()
    {
        $getTaxReports = $this->db->select('*')->from('houdinv_taxes')->get()->result();
        return array('tax'=>$getTaxReports);
    }
    public function getTaxReports($data)
    {
        $getTaxReports = $this->db->select('*')->from('houdinv_taxes')->where('houdinv_tax_id',$data)->get()->result();
        $getOrdersData = $this->db->select('*')->from('houdinv_orders')->where('houdinv_payment_status','1')->get()->result();
        $setParentArray = array();
        foreach($getOrdersData as $getOrdersDataList)
        {
            if($getOrdersDataList->houdinv_confirm_order_product_detail)
            {
                $getProductDetails = json_decode($getOrdersDataList->houdinv_confirm_order_product_detail,true);
                for($index = 0; $index < count($getProductDetails); $index++)
                {
                    if($getProductDetails[$index]['product_id'])
                    {
                        $getProductInfo = $this->db->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getProductDetails[$index]['product_id'])->get()->result();
                        $getPriceData = json_decode($getProductInfo[0]->houdin_products_price,true);
                        if($getPriceData['tax_price'] != "")
                        {
                            if($getPriceData['tax_price'] == $data)
                            {
                                $getTaxAmount = $getTaxReports[0]->houdinv_tax_percenatge;
                                $gettax = (($getProductDetails[$index]['product_actual_price']*$getTaxAmount)/100)*$getProductDetails[$index]['product_Count'];
                                $getTaxPercentage = $getTaxAmount;
                                
                            }   
                        }
                    }
                    else
                    {
                        $getVariantinfo = $this->db->select('houdin_products_variants_product_id')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getProductDetails[$index]['variant_id'])->get()->result();
                        $getProductInfo = $this->db->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getVariantinfo[0]->houdin_products_variants_product_id)->get()->result();
                        $getPriceData = json_decode($getProductInfo[0]->houdin_products_price,true);
                        if($getPriceData['tax_price'] != "")
                        {
                            if($getPriceData['tax_price'] == $data)
                            {
                                $getTaxAmount = $getTaxReports[0]->houdinv_tax_percenatge;
                                $gettax = (($getProductDetails[$index]['product_actual_price']*$getTaxAmount)/100)*$getProductDetails[$index]['product_Count'];
                                $getTaxPercentage = $getTaxAmount;
                            }   
                        }
                    }
                    
                }
            }
            else
            {
                $getProductDetails = json_decode($getOrdersDataList->houdinv_confirm_order_product_detail,true);
                for($index = 0; $index < count($getProductDetails); $index++)
                {
                    if($getProductDetails[$index]['product_id'])
                    {
                        $getProductInfo = $this->db->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getProductDetails[$index]['product_id'])->get()->result();
                        $getPriceData = json_decode($getProductInfo[0]->houdin_products_price,true);
                        if($getPriceData['tax_price'] != "")
                        {
                            if($getPriceData['tax_price'] == $data)
                            {
                                $getTaxAmount = $getTaxReports[0]->houdinv_tax_percenatge;
                                $gettax = (($getProductDetails[$index]['product_actual_price']*$getTaxAmount)/100)*$getProductDetails[$index]['product_Count'];
                                $getTaxPercentage = $getTaxAmount;
                            }   
                        }
                    }
                    else
                    {
                        $getVariantinfo = $this->db->select('houdin_products_variants_product_id')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getProductDetails[$index]['variant_id'])->get()->result();
                        $getProductInfo = $this->db->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getVariantinfo[0]->houdin_products_variants_product_id)->get()->result();
                        $getPriceData = json_decode($getProductInfo[0]->houdin_products_price,true);
                        if($getPriceData['tax_price'] != "")
                        {
                            if($getPriceData['tax_price'] == $data)
                            {
                                $getTaxAmount = $getTaxReports[0]->houdinv_tax_percenatge;
                                $gettax = (($getProductDetails[$index]['product_actual_price']*$getTaxAmount)/100)*$getProductDetails[$index]['product_Count'];
                                $getTaxPercentage = $getTaxAmount;
                            }   
                        }
                    }
                    
                }
            }
            if($gettax)
            {
                $setChildArary[] = array('orderid'=>$getOrdersDataList->houdinv_order_id,'orderdate'=>$getOrdersDataList->houdinv_order_created_at,'taxpercenatge'=>$getTaxPercentage,'taxamount'=>round($gettax));   
            }
        }
        return array('maindata'=>$setChildArary,'taxdata'=>$getTaxReports);
    }

    public function test(){
      return  $getProductInfo = $this->db->select('*')->from('houdinv_accounts_balance_sheet')->get()->result();
        
    }
  } 


  ?>