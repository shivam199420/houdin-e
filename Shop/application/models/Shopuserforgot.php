<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopuserforgot extends CI_Model{
    function __construct() 
    {
    }
    public function updateUserPassword($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getUserId = $getDynamicWebDb->select('houdinv_users_forgot_user_id')->from('houdinv_users_forgot')->where('houdinv_users_forgot_token',$data['pin'])->get()->result();
        if(count($getUserId) > 0)
        {
            $letters1='abcdefghijklmnopqrstuvwxyz'; 
            $string1=''; 
            for($x=0; $x<3; ++$x)
            {  
                $string1.=$letters1[rand(0,25)].rand(0,9); 
            }
            $saltdata = password_hash($string1,PASSWORD_DEFAULT);
            $pass = crypt($data['password'],$saltdata);
            $setData = strtotime(date('Y-m-d H:i:s'));
            $setUpdateArray = array('houdinv_user_password'=>$pass,'houdin_user_password_salt'=>$saltdata,'houdinv_user_updated_at'=>$setData);
            $getDynamicWebDb->where('houdinv_user_id',$getUserId[0]->houdinv_users_forgot_user_id);
            $getUpdateStatus = $getDynamicWebDb->update('houdinv_users',$setUpdateArray);
            if($getUpdateStatus == 1)
            {
                $getDynamicWebDb->where('houdinv_users_forgot_token',$data['pin']);
                $getDynamicWebDb->delete('houdinv_users_forgot');
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }
        }
        else
        {
            return array('pin');
        }
    }
} 
?>