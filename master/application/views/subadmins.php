<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <div class="btn-group pull-right m-t-15">
                                    <button type="button" class="btn btn-default btn-sm m-l-5" data-toggle="modal" data-target="#add_Subadmins"> <i class="fa fa-plus m-r-5"></i>Add Subadmins</button>
                                </div>
                                <h4 class="page-title">Subadmins</h4>

                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                        <?php 
                        if($this->session->flashdata('error'))
                        {
                            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
                        }
                        if($this->session->flashdata('success'))
                        {
                            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
                        }
                        ?>
                        </div>
                        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card-box table-responsive">
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th>User Detail</th>
                    <th>Department</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                foreach($adminData as $adminDataList)
                {
                ?>
                 <tr>
                    <td><?php echo $adminDataList->houdin_admin_user_name."(".$adminDataList->houdin_admin_user_email.", ".$adminDataList->houdin_admin_user_phone.")" ?></td>
                    <td><?php echo $adminDataList->houdin_admin_department_name ?></td>
                    <td>
                    <?php 
                    if($adminDataList->houdin_admin_user_status == 'active')
                    {
                        $setText = 'Active';
                        $setClass = "success";
                    }
                    else
                    {
                        $setText = 'Deactive';
                        $setClass = "warning";
                    }
                    ?>
                    <button type="button" class="btn btn-<?php echo $setClass; ?> btn btn-xs"><?php echo $setText; ?></button></td>
                    <td>
                        <button data-id="<?php echo $adminDataList->houdin_admin_user_id ?>" data-name="<?php echo $adminDataList->houdin_admin_user_name ?>" data-email="<?php echo $adminDataList->houdin_admin_user_email ?>" data-phone="<?php echo $adminDataList->houdin_admin_user_phone ?>" data-status="<?php echo $adminDataList->houdin_admin_user_status ?>" data-department="<?php echo $adminDataList->houdin_admin_user_department ?>" type="button"  class="btn btn-warning btn btn-sm editSubAdminBtn"><i class="fa fa-pencil m-r-5"></i>Edit</button>
                        <button type="button" data-id="<?php echo $adminDataList->houdin_admin_user_id ?>" class="btn btn-warning btn btn-sm deleteSubadminBtn"><i class="fa fa-trash m-r-5"></i>Delete</button>
                    </td>
                  </tr>
                <?php }
                ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>


                    </div> <!-- container -->
                </div> <!-- content -->
                <div id="add_Subadmins" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                          <?php echo form_open(base_url( 'Adminmanagement/subadmins' ), array( 'id' => 'addSubadminsForm' )); ?>
                            <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                  <h4 class="modal-title setNewTitle">Add Subadmin</h4>
                                </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                        <input type="hidden" class="subadminid" name="subadminid"/>
                                            <label for="field-7" class="control-label">Name</label>
                                            <input type="text" name="username" class="form-control username required_validation_for_add_subadmins name_validation" placeholder="First Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Email</label>
                                            <input type="text" name="useremail" class="form-control useremail required_validation_for_add_subadmins name_validation email_validation" placeholder="Email" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Mobile</label>
                                            <div class="clearfix"></div>
                                            <input type="text" name="usercontact" style="width:100%" class="form-control usercontact Internationphonecode required_validation_for_add_subadmins name_validation" placeholder="Mobile" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Choose Status</label>
                                            <select class="form-control required_validation_for_add_subadmins userstatus" name="userstatus">
                                            <option value="">Choose Status</option>
                                            <option value="active">Active</option>
                                            <option value="deactive">Deactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                        <div class="form-group no-margin">
                                            <label for="field-7" class="control-label">Department</label>
                                            <select class="form-control required_validation_for_add_subadmins departments" name="departments">
                                              <option>Choose Department</option>
                                              <?php foreach($departmentsData as $departmentsDataList) 
                                              {
                                                ?>
                                                <option value="<?php echo $departmentsDataList->houdin_admin_department_id ?>"><?php echo $departmentsDataList->houdin_admin_department_name ?></option>
                                              <?php }
                                              ?>
                                               
                                            </select>
                                        </div>

                                    </div>
                                </div>
                              </div>

                          <div class="modal-footer">
                              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                              <input type="submit" class="btn btn-info" name="addSubAdminData" value="Submit">
                          </div>

                     </div>
                  <?php echo form_close(); ?>
                 </div>
                </div>
  <div id="deleteSubadminsModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog">
  <?php echo form_open(base_url( 'Adminmanagement/subadmins' )); ?>
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
  <h4 class="modal-title">Delete Entry</h4>
  </div>
  <div class="modal-body">
  <div class="row">
  <div class="col-md-12">
  <input type="hidden" class="deleteSubadminId" name="deleteSubadminId"/>
  <h4><b>Do you really want to delete this entry ?</b></h4>
  </div>
  </div>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
  <input type="submit" class="btn btn-info" name="deleteSubadmins" value="Delete">
  </div>
  </div>
  <?php echo form_close(); ?>
  </div>
  </div>
  <?php $this->load->view('Template/footer.php') ?>
<!-- CLient side form validation -->
<script type="text/javascript">
            $(document).ready(function(){
                $(document).on('submit','#addSubadminsForm',function(){
                    $('.showErrorMessage').hide();
                    var check_required_field='';
                    $(this).find(".required_validation_for_add_subadmins").each(function(){
                        var val22 = $(this).val();
                        if (!val22){
                            check_required_field =$(this).size();
                            $(this).css("border-color","#ccc");
                            $(this).css("border-color","red");
                        }
                        $(this).on('keypress change',function(){
                            $(this).css("border-color","#ccc");
                        });
                    });
                    if(check_required_field)
                    {
                        return false;
                    }
                    else {
                        return true;
                    }
                });
                $(document).on('click','.editSubAdminBtn',function(){
                    $('.setNewTitle').text('').text('Edit Subadmin');
                    $('.username').val($(this).attr('data-name'));
                    $('.subadminid').val($(this).attr('data-id'));
                    $('.useremail').val($(this).attr('data-email'));
                    $('.usercontact').val($(this).attr('data-phone'));
                    $('.userstatus').val($(this).attr('data-status'));
                    $('.departments').val($(this).attr('data-department'));
                    $('#add_Subadmins').modal('show');
                });
                $(document).on('click','.deleteSubadminBtn',function(){
                    $('.deleteSubadminId').val($(this).attr('data-id'));
                    $('#deleteSubadminsModal').modal('show');
                })
            });
            </script>