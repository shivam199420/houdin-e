<!DOCTYPE html>
<html lang="en">
<?php
$getDB = getDBName();
$vendorURL = $this->session->userdata('vendorURL');
$this->load->helper('dynamicdatabaseapp');
$getDBDetail = switchDynamicDatabaseApp($this->session->userdata('shopName'));
$this->db = $this->load->database($getDBDetail,true);
$con = mysqli_connect("localhost","root","houdine123",$getDB);
$getImages = mysqli_query($con,"select shop_logo,shop_favicon from houdinv_shop_logo limit 1");
$getImageData = mysqli_fetch_array($getImages);
// get username
$getUserId = $this->session->userdata('userAuth');
$getUser = mysqli_query($con,"select houdinv_user_name from houdinv_users where houdinv_user_id='$getUserId'");
$getUserData = mysqli_fetch_array($getUser);
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<script type="text/javascript">
      var base_url = "<?php echo base_url() ?>";
  </script>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php 
  $getShopName = getshopname();
  if($getShopName)
  {
      $setShopName = $getShopName;
  }
  else
  {
      $setShopName = 'Houdin-e';
  }
  ?>
  <title>Welcome To <?php echo $setShopName; ?></title>
    <link rel="icon" type="image/png" href="<?php if($getImageData['shop_favicon']) { ?><?=$vendorURL?>upload/logo/<?php echo $getImageData['shop_favicon'];  ?> <?php } else { ?> <?php echo base_url() ?>Extra/apparels/img/hundin-e-favicon.png <?php  } ?> ">
    <!-- CSS -->

    <!-- Plugin -->
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/vendor/owlcarousel/dist/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/vendor/slick/slick.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/vendor/animate/animate.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/vendor/lightcase/lightcase.css">

    <!-- Slider Revolution CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/vendor/revolution/css/settings.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/vendor/revolution/css/layers.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/vendor/revolution/css/navigation.css">

    <!-- Fonts -->
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/vendor/fonts/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/css/font-face.min.css">

    <!-- Customize -->
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/css/theme.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/vendor/slide100/slide100.css">

  <link rel="stylesheet" href="<?php echo base_url() ?>Extra/departmental/css/intlTelInput.css" />
</head>
<style >
.footer.footer-1 .sub-footer .copy-right {
    color: #808080;
    height: 50px;
    padding-top: 10px;
}
 .banner__inner {
    position: absolute;
    z-index: 1;
    color: #333;
}
 .banner__inner.banner-style-4 {
    top: 8px;
    left: 50%;
    -webkit-transform: translateX(-50%);
    -moz-transform: translateX(-50%);
    -ms-transform: translateX(-50%);
    -o-transform: translateX(-50%);
    transform: translateX(-50%);
    text-align: center;

}
 .banner__inner.banner-style-4 p {
    text-align: center;
    color: red;
    font-size: 19px;
    text-transform: uppercase;
    font-weight: 600;
    white-space: nowrap;
    line-height: 1.1;
    margin-bottom: 23px;
}
.quotation_span {
    text-transform: initial;
    font-weight: normal;
    font-size: 18px;
    white-space: nowrap;
    line-height: 24px;
    color: red;
}
</style>
<style type="text/css">
.desktop-showContent {
  visibility: visible !important;
  opacity: 1 !important;
}
.desktop-query {
  width: 300px;
  float: right;
  line-height: 0;
  margin: 20px 10px 20px 0;
}
.desktop-query > input.desktop-searchBar {
  font-family: Whitney;
  display: inline-block;
  float: left;
  font-size: 16px;
  height: 20px;
  line-height: 24px;
  width: 235px;
  color: #696E79;
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  padding: 8px 10px 10px 10px;
  margin: 0;
  outline: 0;
  border: 1px solid #D5D6D9;
  -webkit-border-radius: 4px 0 0 4px;
  -moz-border-radius: 4px 0 0 4px;
  border-radius: 4px 0 0 4px;
  border-right: 0;
  background: #F5F5F6;
}
input.desktop-searchBar:focus {
  background-color: #FFFFFF;
  border-color: #14CDA8;
}
input.desktop-searchBar:focus + a.desktop-submit {
  background-color: #FFFFFF;
  border-color: #14CDA8;
}
a.desktop-submit {
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  display: inline-block;
  height: 28px;
  width: 40px;
  text-align: center;
  padding: 8px 0 2px 0;
  background: #F5F5F6;
  border: 1px solid #D4D5D9;
  border-left: none;
  -webkit-border-radius: 0 4px 4px 0;
  -moz-border-radius: 0 4px 4px 0;
  border-radius: 0 4px 4px 0;
}
.desktop-autoSuggest {
  z-index: 4;
  position: relative;
  display: block;
  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0);
  -moz-box-shadow: 0 1px 6px rgba(0, 0, 0, 0);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0);
  /* rgba(0,0,0,0.3) */
  font-size: 11px;
  color: #696E79;
  background-color: #fff;
  -webkit-font-smoothing: antialiased;
  visibility: hidden;
  opacity: 0;
  -webkit-box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.3);
  -moz-box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.3);
  box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.3);
  width:100%;
}
.desktop-autoSuggest .desktop-group {
  margin: 0 0 10px 0;
  padding: 0;
  max-height: 300px;
  overflow-y: auto;
}

.desktop-group > li {
  list-style: none;
  padding-left: 12px;
  padding:10px;
  text-align: left;
  display: block!important;
    border-bottom: 1px solid #ddd;
}
.desktop-group > li >a{}
  .desktop-group > li >a.linkcat_search{
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  .desktop-group > li >a.linkcat_search_cart{
    width: 35px;
    text-align: center;

    float: right;
  }
  .desktop-group > li >a.linkcat_search_cart > .fa{
    font-size: 17px!important;
  }
.desktop-suggestion {
  background-color: #FFFFFF;
}
.desktop-suggestion:hover {
  background-color: #D4D5D9;
  cursor: pointer;
}
.desktop-suggestionTitle {
  font-weight: bold;
  background-color: #000000c2;
  cursor: pointer;
  color: #F5F5F6;
      font-size: 14px;
}
.search-box {
    line-height: 38px;
}
.fa .fa-shopping-bag{
  font-size: 15px!important;
}

</style>
<body>

    <!-- Preloader -->
    <div class="loading">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
    <!-- End Pre Loader -->

     <!-- start header -->
<header>
        <div class="header-wrapper">
            <!-- Desktop Menu -->
            <div class="header-wrapper-desktop d-none d-lg-block">
                <div class="header header-style-1">
                    <div class="header-main">
                        <div class="header__logo">
                            <a href="<?php echo base_url() ?>home/">

                            <img alt="Shop Logo" src="<?php if($getImageData['shop_logo']) { ?><?=$vendorURL;?>upload/logo/<?php echo $getImageData['shop_logo']  ?> <?php } else { ?> <?php echo base_url() ?>Extra/apparels/img/hundin_Logo_Gray.png <?php  } ?>" style="max-width:100%; height: 47px;">
                            </a>
                        </div>
                        <nav class="header__navbar">
                            <ul class="navbar-menu">
                            <?php
         //get header navigation
         $getHeadernav = mysqli_query($con,"select * from houdinv_navigation_store_pages where houdinv_navigation_store_pages_name='home' || houdinv_navigation_store_pages_category_id !=0 || houdinv_navigation_store_pages_custom_link_id !=0");
        while($getHeaderNavData = mysqli_fetch_array($getHeadernav))
        {
            if($getHeaderNavData['houdinv_navigation_store_pages_name'] == 'home')
            {
            ?>
            <li class="hashsub"><a href="<?php echo base_url() ?>home/">Home</a></li>
            <?php }
            else if($getHeaderNavData['houdinv_navigation_store_pages_custom_link_id'] != 0)
            {
                    $setCustomLinkId = $getHeaderNavData['houdinv_navigation_store_pages_custom_link_id'];
                    $getCustomLinkData = mysqli_query($con,"select * from houdinv_custom_links where houdinv_custom_links_id = $setCustomLinkId");
                    $fetchCustomLinkData = mysqli_fetch_array($getCustomLinkData);
            ?>
            <li class="hashsub"><a href="<?php echo $fetchCustomLinkData['houdinv_custom_links_url'] ?>" <?php if($fetchCustomLinkData['houdinv_custom_links_target'] == 'new') { ?> target="_blank" <?php  } ?>><?php echo $fetchCustomLinkData['houdinv_custom_links_title'] ?></a>


            </li>
            <?php }
            else
            {
                    $getMainCategoryId = $getHeaderNavData['houdinv_navigation_store_pages_category_id'];
                    $getSubCategoryData = mysqli_query($con,"select houdinv_sub_category_one_name,houdinv_sub_category_one_id from houdinv_sub_categories_one where houdinv_sub_category_one_main_id = $getMainCategoryId");
                    $totalCategoryData=mysqli_num_rows($getSubCategoryData);
            ?>
            <li class="hashsub"><a href="<?php echo base_url() ?>Productlist/<?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>"><?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_name'] ?><?php if($totalCategoryData > 0) { ?> <i class="fa fa-angle-down"></i> <?php  } ?></a>





             <?php
            if($totalCategoryData > 0)
            {
            ?>
             <ul class="sub-menu">
                  <div class="container submenu-inner">
            <?php
            while($fetchSubCategory = mysqli_fetch_assoc($getSubCategoryData))
            {
                    $getSubCategoryId = $fetchSubCategory['houdinv_sub_category_one_id'];
                ?>

                <li><a href="<?php echo base_url() ?>Productlist/<?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>/<?php echo $getSubCategoryId ?>"><?php echo $fetchSubCategory['houdinv_sub_category_one_name'] ?></a>
            <?php
            $getSubSubCategory = mysqli_query($con,"select houdinv_sub_category_two_name,houdinv_sub_category_two_id from houdinv_sub_categories_two where houdinv_sub_category_two_sub1_id = $getSubCategoryId");
            ?>
             <ul class="sub-subdropdown">
            <?php
            while($fetchSubSubCategory = mysqli_fetch_assoc($getSubSubCategory))
            {
                    $setSubSubCategoryId = $fetchSubSubCategory['houdinv_sub_category_two_id'];
            ?>
            <li><a href="<?php echo base_url() ?>Productlist/<?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>/<?php echo $getSubCategoryId ?>/<?php echo $setSubSubCategoryId ?>"><?php echo $fetchSubSubCategory['houdinv_sub_category_two_name']; ?></a>
            <?php }
            ?>
            </ul>
              </li>
            <?php }
            ?>
             </div>
                </ul>
            <?php }
            ?>

            </li>
            <?php }
    }
    ?>
    <?php
    if($this->session->userdata('userAuth') != "")
    {
        ?>
        <li class="relative">
        <a href="javascript:;"><?php echo $getUserData['houdinv_user_name'] ?></a>
        <ul class="sub-menu">
        <div class="container">
        <li><a href="<?php echo base_url(); ?>Orders">My order</a></li>
        <li><a href="<?php echo base_url() ?>Address">My address</a></li>
        <li><a href="<?php echo base_url() ?>Changepassword">Change password</a></li>
        <li><a href="<?php echo base_url() ?>wishlist">Wishlist</a></li>
        <li><a href="<?php echo base_url() ?>Logout">Logout</a></li>
        </div>
        </ul>
    </li>
    <?php }
    ?>
    <?php
    if($this->session->userdata('userAuth') == "")
    {
    ?>
    <li>
        <a href="<?php echo base_url() ?>Register">LOGIN/REGISTER</a>
    </li>
    <?php } ?>
                            </ul>
                        </nav>
                        <div class="header__button">
                            <ul>
                                <li class="header-search">
                                    <div class="search-button ">
                                        <img src="<?php echo base_url() ?>Extra/departmental/images/icon/header-search.png" alt="Search">
                                    </div>
                                    <div class="search-input custom_search_input" id="searchboxsuggestionproduct">
                                        <input type="text" name="typeahead" id="searchfiltertext"  autocomplete="off" spellcheck="false" placeholder="Start typing here...">
                                        <a href="#"></a>


                                    </div>
                                    <div id="searchfilterdataheader" class=" desktop-autoSuggest desktop-showContent" ></div>

                                </li>

                                    <!-- shopping cart -->
                                    <?php
                                    $AllMiniCart =array();
                                    $totalCartData =0;
                                    if($this->session->userdata('userAuth'))
                                    {
                                        $data_All = $this->db->select("*")->from("houdinv_users_cart") ->where("houdinv_users_cart.houdinv_users_cart_user_id",$this->session->userdata('userAuth'))->get()->result();
                                        $setCartArray = array();
                                        foreach($data_All as $data_AllData)
                                        {
                                            if($data_AllData->houdinv_users_cart_item_id != 0 && $data_AllData->houdinv_users_cart_item_id != "")
                                            {
                                                $getProductData = $this->db->select('*')->from('houdinv_products')->where('houdin_products_id',$data_AllData->houdinv_users_cart_item_id)->get()->result();
                                                // setImageData
                                                $getImage = json_decode($getProductData[0]->houdinv_products_main_images,true);
                                                // set price
                                                $getPrice = json_decode($getProductData[0]->houdin_products_price,true);
                                                if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
                                                {
                                                    $setPrice = $getProductData[0]->houdin_products_final_price;
                                                }
                                                else
                                                {
                                                    $setPrice = $getProductData[0]->houdin_products_final_price;
                                                }
                                                $setCartArray[] = array('count'=>$data_AllData->houdinv_users_cart_item_count,'productName'=>$getProductData[0]->houdin_products_title,'productprice'=>$setPrice,'productImage'=>$getImage[0],'cartId'=>$data_AllData->houdinv_users_cart_id);
                                            }
                                            else
                                            {
                                                $getProductData = $this->db->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data_AllData->houdinv_users_cart_item_variant_id)->get()->result();
                                                // get product
                                                $getExtraProduct = $this->db->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getProductData[0]->houdin_products_variants_product_id)->get()->result();
                                                // set price
                                                $getPrice = json_decode($getExtraProduct[0]->houdin_products_price,true);
                                                if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
                                                {
                                                    $setPrice = $getProductData[0]->houdin_products_variants_prices-$getPrice['discount'];
                                                }
                                                else
                                                {
                                                    $setPrice = $getProductData[0]->houdin_products_variants_prices;
                                                }

                                                $setCartArray[] = array('count'=>$data_AllData->houdinv_users_cart_item_count,'productName'=>$getProductData[0]->houdin_products_variants_title,'productprice'=>$setPrice,'productImage'=>$getProductData[0]->houdin_products_variants_image,'cartId'=>$data_AllData->houdinv_users_cart_id);
                                            }
                                        }
                                        $totalCartData=count($setCartArray);

                                        //  $AllMiniCart =$this->Cartmodel->ShowCartData1($this->session->userdata('userAuth'));
                                }

                                ?>
                                <li class="header-shop-cart">
                                    <div class="shop-cart-button">
                                        <a href="javascript:;"><img src="<?php echo base_url() ?>Extra/departmental/images/icon/header-cart.png" alt="Cart"></a>
                                        <span class="amount"><?php if($totalCartData) { echo $totalCartData; } else { echo 0; } ?></span>
                                    </div>
                                    <div class="shop-cart">
                                        <ul class="shop-cart__list">
                                            <?php
                                            if($totalCartData > 0)
                                            {
                                                $i=1;
                                                $final_price =0;
                                                foreach($setCartArray as $setCartArrayList)
                                                {
                                                    $image = $setCartArrayList['productImage'];
                                                    $count = $setCartArrayList['count'];
                                                    $main_price = $setCartArrayList['productprice'];
                                                    $total_price = $main_price*$count;
                                                    $final_price =$final_price+$total_price;
                                                    ?>
                                            <li class="item">
                                                <div class="item-image">
                                                    <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $image; ?>" style="width:100%" alt="Item 1">
                                                </div>
                                                <div class="item-detail">
                                                    <p class="name"><?php echo substr($setCartArrayList['productName'],0,12)."(".$count.")"; ?></p>
                                                    <p class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $main_price; ?></p>
                                                    <p class="amount">x<?php echo $count;
                                                     ?></p>
                                                </div>
                                                <span class="pro-del cp_cart_remove remove" data-id="<?php echo $setCartArrayList['cartId']; ?>" data-replace="<?php echo $total_price; ?>"></span>
                                            </li>
                                            <?php } }
                                            else
                                            {
                                            ?>
                                            <li class="item">No Item</li>
                                            <?php }
                                            ?>
                                        </ul>
                                        <?php
                                        // if($totalCartData > 0)
                                        // {
                                        ?>
                                        <div class="checkout m-t-26">
                                            <p>Subtotal
                                                <span class="sub-total"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $final_price ?></span>
                                            </p>
                                            <a href="<?php echo base_url();?>/Cart">Checkout</a>
                                        </div>
                                        <?php //} ?>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- end shoppin cart -->
                    </div>
                </div>
            </div>
            <!-- End Desktop Menu -->

            <!-- Mobile Menu -->
            <div class="header-wrapper-mobile d-block d-lg-none">
                <div class="header-mobile__bar">
                    <div class="header-mobile__logo">
                    <a href="#">
                        <img src="<?php echo base_url() ?>Extra/departmental/images/hundin_Logo_Gray.png" alt="Hundine">
                        </a>
                    </div>
                    <div class="header-mobile__button">
                        <span class="humburger-box">
                            <span class="hamburger__inner"></span>
                        </span>
                    </div>
                </div>
                <nav class="header-mobile__navbar">
                    <ul>
                    <?php
         //get header navigation
         $getHeadernav = mysqli_query($con,"select * from houdinv_navigation_store_pages where houdinv_navigation_store_pages_name='home' || houdinv_navigation_store_pages_category_id !=0 || houdinv_navigation_store_pages_custom_link_id !=0");
        while($getHeaderNavData = mysqli_fetch_array($getHeadernav))
        {
            if($getHeaderNavData['houdinv_navigation_store_pages_name'] == 'home')
            {
            ?>
            <li class="hashsub"><a href="<?php echo base_url() ?>home/<?php echo $this->session->userdata('shopName') ?>">Home</a></li>
            <?php }
            else if($getHeaderNavData['houdinv_navigation_store_pages_custom_link_id'] != 0)
            {
                    $setCustomLinkId = $getHeaderNavData['houdinv_navigation_store_pages_custom_link_id'];
                    $getCustomLinkData = mysqli_query($con,"select * from houdinv_custom_links where houdinv_custom_links_id = $setCustomLinkId");
                    $fetchCustomLinkData = mysqli_fetch_array($getCustomLinkData);
            ?>
            <li class="hashsub"><a href="<?php echo $fetchCustomLinkData['houdinv_custom_links_url'] ?>" <?php if($fetchCustomLinkData['houdinv_custom_links_target'] == 'new') { ?> target="_blank" <?php  } ?>><?php echo $fetchCustomLinkData['houdinv_custom_links_title'] ?></a>


            </li>
            <?php }
            else
            {
                    $getMainCategoryId = $getHeaderNavData['houdinv_navigation_store_pages_category_id'];
                    $getSubCategoryData = mysqli_query($con,"select houdinv_sub_category_one_name,houdinv_sub_category_one_id from houdinv_sub_categories_one where houdinv_sub_category_one_main_id = $getMainCategoryId");
                    $totalCategoryData=mysqli_num_rows($getSubCategoryData);
            ?>
            <li class="hashsub"><a href="<?php echo base_url() ?>Productlist/<?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_id'] ?>"><?php echo $getHeaderNavData['houdinv_navigation_store_pages_category_name'] ?> </a></li>
            <?php }
    }
    ?>
                        <?php
                        if($this->session->userdata('userAuth') != "")
                        {
                            ?>
                        <li>
                            <a><?php echo $getUserData['houdinv_user_name'] ?></a>
                            <ul class="sub-menu">
                            <li><a href="<?php echo base_url(); ?>Orders">My order</a></li>
                            <li><a href="<?php echo base_url() ?>Address">My address</a></li>
                            <li><a href="<?php echo base_url() ?>Changepassword">Change password</a></li>
                            <li><a href="<?php echo base_url() ?>wishlist">Wishlist</a></li>
                            <li><a href="<?php echo base_url() ?>Logout">Logout</a></li>
                                    </ul>
                        </li>
                        <?php }
                        else
                        { ?>
                         <li> <a href="<?php echo base_url() ?>Register">LOGIN/REGISTER</a> </li>
                        <?php } ?>

                    </ul>
                </nav>
            </div>
            <!-- End Mobile Menu -->
        </div>
</header>
<!-- End header -->
