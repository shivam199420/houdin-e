-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 04, 2018 at 09:58 AM
-- Server version: 5.6.40
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hawksind_houdinadmindb`
--

-- --------------------------------------------------------

--
-- Table structure for table `houdin_admin_department`
--

CREATE TABLE `houdin_admin_department` (
  `houdin_admin_department_id` int(11) NOT NULL,
  `houdin_admin_department_name` varchar(50) NOT NULL,
  `houdin_admin_department_access` text,
  `houdin_admin_department_status` enum('active','deactive') NOT NULL,
  `houdin_admin_department_created_date` varchar(30) DEFAULT NULL,
  `houdin_admin_department_updated_at` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_admin_department`
--

INSERT INTO `houdin_admin_department` (`houdin_admin_department_id`, `houdin_admin_department_name`, `houdin_admin_department_access`, `houdin_admin_department_status`, `houdin_admin_department_created_date`, `houdin_admin_department_updated_at`) VALUES
(1, 'Accounts', 'shop,package,setting', 'active', '1536019200', '1536019200'),
(3, 'HRM', 'shop,admin', 'active', '1537833600', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_admin_logo`
--

CREATE TABLE `houdin_admin_logo` (
  `houdin_admin_logo_id` int(11) NOT NULL,
  `houdin_admin_logo_image` varchar(100) NOT NULL,
  `houdin_admin_logo_created_at` varchar(30) NOT NULL,
  `houdin_admin_logo_updated_at` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_admin_logo`
--

INSERT INTO `houdin_admin_logo` (`houdin_admin_logo_id`, `houdin_admin_logo_image`, `houdin_admin_logo_created_at`, `houdin_admin_logo_updated_at`) VALUES
(1, '519432Logo_W.png', '1528701454', '1535707300');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_admin_transaction`
--

CREATE TABLE `houdin_admin_transaction` (
  `houdin_admin_transaction_id` int(11) NOT NULL,
  `houdin_admin_transaction_transaction_id` varchar(30) NOT NULL,
  `houdin_admin_transaction_from` enum('cash','payumoney') NOT NULL,
  `houdin_admin_transaction_type` enum('credit','debit') NOT NULL,
  `houdin_admin_transaction_for` enum('package') NOT NULL,
  `houdin_admin_transaction_for_id` int(11) NOT NULL DEFAULT '0',
  `houdin_admin_transaction_amount` float NOT NULL,
  `houdin_admin_transaction_status` enum('success','failure') NOT NULL,
  `houdin_admin_transaction_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_admin_transaction`
--

INSERT INTO `houdin_admin_transaction` (`houdin_admin_transaction_id`, `houdin_admin_transaction_transaction_id`, `houdin_admin_transaction_from`, `houdin_admin_transaction_type`, `houdin_admin_transaction_for`, `houdin_admin_transaction_for_id`, `houdin_admin_transaction_amount`, `houdin_admin_transaction_status`, `houdin_admin_transaction_date`) VALUES
(1, 'TXN25648149', 'cash', 'credit', 'package', 1, 500, 'success', '2018-09-05'),
(2, 'Txn18947105', 'payumoney', 'credit', 'package', 3, 700, 'success', '2018-09-05'),
(3, 'Txn18947105', 'payumoney', 'credit', 'package', 3, 700, 'success', '2018-09-05'),
(4, 'Txn11948382', 'payumoney', 'credit', 'package', 2, 54000, 'success', '2018-10-01');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_admin_users`
--

CREATE TABLE `houdin_admin_users` (
  `houdin_admin_user_id` int(11) NOT NULL,
  `houdin_admin_user_name` varchar(50) NOT NULL,
  `houdin_admin_user_email` varchar(50) NOT NULL,
  `houdin_admin_user_phone` varchar(20) NOT NULL,
  `houdin_admin_user_password` varchar(100) NOT NULL,
  `houdin_admin_user_role_id` int(11) NOT NULL,
  `houdin_admin_user_department` int(11) NOT NULL DEFAULT '0',
  `houdin_admin_user_status` enum('deactive','active','block') NOT NULL,
  `houdin_admin_user_status_change_text` tinytext,
  `houdin_admin_user_created_at` varchar(30) DEFAULT NULL,
  `houdin_admin_user_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_admin_users`
--

INSERT INTO `houdin_admin_users` (`houdin_admin_user_id`, `houdin_admin_user_name`, `houdin_admin_user_email`, `houdin_admin_user_phone`, `houdin_admin_user_password`, `houdin_admin_user_role_id`, `houdin_admin_user_department`, `houdin_admin_user_status`, `houdin_admin_user_status_change_text`, `houdin_admin_user_created_at`, `houdin_admin_user_updated_at`) VALUES
(1, 'Hawkscode', 'hawkscodeteam@gmail.com', '+917790870946', '$2y$10$nsQ4pCGHAcGTr9CtptnofeJT4KY2NVlWE7lGZyGoZ5cYjmY51qr9i', 0, 0, 'active', '', '2018-06-06 00:00:00', '1538455229'),
(3, 'shivam', 'shivam.hawkscode@gmail.com', '+917790870946', '$2y$10$wdj81aMxa.c8CgXCblD.D.ViKpvWt5VTUjJhTYrrZJkRnolcZbAX6', 1, 1, 'active', NULL, '1536019200', '1536059383');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_admin_user_second_auth`
--

CREATE TABLE `houdin_admin_user_second_auth` (
  `houdin_second_auth_id` int(11) NOT NULL,
  `houdin_second_auth_email` varchar(50) NOT NULL,
  `houdin_second_auth_token` int(5) NOT NULL,
  `houdin_second_auth_created_date` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_business_categories`
--

CREATE TABLE `houdin_business_categories` (
  `houdin_business_category_id` int(10) UNSIGNED NOT NULL,
  `houdin_business_category_name` varchar(250) DEFAULT NULL,
  `houdin_business_category_desc` tinytext,
  `houdin_business_categories_status` enum('deactive','active','block') NOT NULL,
  `houdin_business_category_created_at` varchar(150) DEFAULT NULL,
  `houdin_business_category_updated_at` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_business_categories`
--

INSERT INTO `houdin_business_categories` (`houdin_business_category_id`, `houdin_business_category_name`, `houdin_business_category_desc`, `houdin_business_categories_status`, `houdin_business_category_created_at`, `houdin_business_category_updated_at`) VALUES
(1, 'Departmental stores', 'Departmental stores', 'active', '1528442159', '1528971673'),
(3, 'Apparels', 'Apparels', 'active', '1528442159', '1528971682'),
(4, 'Stone and tiles', 'Stone and tiles', 'active', '1528442159', '1528971695'),
(6, 'Optical', 'Optical', 'active', '1528971719', '1528971719'),
(7, 'Work order stores (ex: bakery, pottery, etc)', 'Work order stores (ex: bakery, pottery, etc)', 'active', '1528971728', '1528971728'),
(8, 'Other general stores', 'Other general stores', 'active', '1528971739', '1528971739'),
(9, 'Electronics and Electrical Appliances', 'Electronics and electrical appliances', 'active', '1532151351', '1532151351');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_countries`
--

CREATE TABLE `houdin_countries` (
  `houdin_country_id` int(11) NOT NULL,
  `houdin_country_name` varchar(150) DEFAULT NULL,
  `houdin_country_status` enum('deactive','active','block') NOT NULL,
  `houdin_country_created_at` varchar(150) DEFAULT NULL,
  `houdin_country_updated_at` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_countries`
--

INSERT INTO `houdin_countries` (`houdin_country_id`, `houdin_country_name`, `houdin_country_status`, `houdin_country_created_at`, `houdin_country_updated_at`) VALUES
(1, 'Afghanistan', 'active', '0000-00-00 00:00:00', '1528767295'),
(2, 'Armenia', 'active', '1528451453', '1528451453'),
(4, 'Afghanistan', 'deactive', '1528453639', '1528453639'),
(5, 'Lithuania', 'active', '1528767286', '1529057069'),
(6, 'Albania', 'active', '1528767308', '1528767308');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_coupons`
--

CREATE TABLE `houdin_coupons` (
  `houdin_coupon_id` int(10) UNSIGNED NOT NULL,
  `houdin_coupon_code` varchar(150) NOT NULL,
  `houdin_coupon_discount` float NOT NULL,
  `houdin_coupon_limit` int(11) NOT NULL,
  `houdin_coupon_reamining` int(11) NOT NULL,
  `houdin_coupon_type` int(11) NOT NULL,
  `houdin_coupon_status` enum('deactive','active','block') NOT NULL,
  `houdin_coupon_valid_from` varchar(255) NOT NULL,
  `houdin_coupon_valid_to` varchar(255) NOT NULL,
  `houdin_coupon_created_at` varchar(255) DEFAULT NULL,
  `houdin_coupon_updated_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_coupons`
--

INSERT INTO `houdin_coupons` (`houdin_coupon_id`, `houdin_coupon_code`, `houdin_coupon_discount`, `houdin_coupon_limit`, `houdin_coupon_reamining`, `houdin_coupon_type`, `houdin_coupon_status`, `houdin_coupon_valid_from`, `houdin_coupon_valid_to`, `houdin_coupon_created_at`, `houdin_coupon_updated_at`) VALUES
(2, 'WnUXKO', 13, 5, 0, 1, 'active', '1523232000', '1527033600', '1528788907', '1528768941'),
(3, 'FnUsNz', 2, 2, 0, 1, 'deactive', '1528761600', '1875916800', '1528788984', '1528768948');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_currencies`
--

CREATE TABLE `houdin_currencies` (
  `houdin_currency_id` int(11) NOT NULL,
  `houdin_currency_name` varchar(150) DEFAULT NULL,
  `houdin_currency_sumbol` varchar(150) DEFAULT NULL,
  `houdin_currency_created_at` timestamp NULL DEFAULT NULL,
  `houdin_currency_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_deleted_users`
--

CREATE TABLE `houdin_deleted_users` (
  `houdin_deleted_user_id` int(10) UNSIGNED NOT NULL,
  `houdin_deleted_user_email` varchar(150) DEFAULT NULL,
  `houdin_deleted_user_by` varchar(150) DEFAULT NULL COMMENT 'By Admin, Self Delete',
  `houdin_deleted_user_reason` tinytext,
  `houdin_deleted_user_delete_status` tinyint(4) DEFAULT '0' COMMENT '0 Tmp Delete, 1 Permanent Delete',
  `houdin_deleted_user_created_at` timestamp NULL DEFAULT NULL,
  `houdin_deleted_user_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_email_package`
--

CREATE TABLE `houdin_email_package` (
  `houdin_email_package_id` int(11) NOT NULL,
  `houdin_email_package_name` varchar(50) NOT NULL,
  `houdin_email_package_price` int(11) NOT NULL,
  `houdin_email_package_email_count` int(11) NOT NULL,
  `houdin_email_package_status` enum('active','deactive') NOT NULL,
  `houdin_email_package_created_at` varchar(30) DEFAULT NULL,
  `houdin_email_package_modified_at` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_email_package`
--

INSERT INTO `houdin_email_package` (`houdin_email_package_id`, `houdin_email_package_name`, `houdin_email_package_price`, `houdin_email_package_email_count`, `houdin_email_package_status`, `houdin_email_package_created_at`, `houdin_email_package_modified_at`) VALUES
(1, 'Package 1', 240, 240, 'active', '1530769498', '1530771541');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_forgot_password`
--

CREATE TABLE `houdin_forgot_password` (
  `houdin_forgot_password_email` varchar(150) NOT NULL,
  `houdin_forgot_password_token` varchar(50) NOT NULL,
  `houdin_forgot_password_created_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_languages`
--

CREATE TABLE `houdin_languages` (
  `houdin_language_id` int(10) UNSIGNED NOT NULL,
  `houdin_language_name` varchar(30) DEFAULT NULL,
  `houdin_language_name_value` varchar(30) NOT NULL,
  `houdin_language_status` enum('active','deactive') NOT NULL,
  `houdin_language_created_at` varchar(30) DEFAULT NULL,
  `houdin_language_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_languages`
--

INSERT INTO `houdin_languages` (`houdin_language_id`, `houdin_language_name`, `houdin_language_name_value`, `houdin_language_status`, `houdin_language_created_at`, `houdin_language_updated_at`) VALUES
(1, 'bg', 'Bulgarian', 'active', '1531993981', NULL),
(2, 'eu', 'Basque', 'active', '1531993989', NULL),
(3, 'en', 'English', 'active', '1531994131', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_packages`
--

CREATE TABLE `houdin_packages` (
  `houdin_package_id` int(10) UNSIGNED NOT NULL,
  `houdin_package_name` varchar(50) NOT NULL,
  `houdin_package_price` float NOT NULL,
  `houdin_package_product` int(11) NOT NULL DEFAULT '0',
  `houdin_package_staff_accounts` int(11) NOT NULL DEFAULT '0',
  `houdin_package_stock_location` int(11) NOT NULL DEFAULT '0',
  `houdin_package_app_access` enum('limited','full') NOT NULL,
  `houdin_package_order_pm` int(11) NOT NULL DEFAULT '0',
  `houdin_package_sms` int(11) NOT NULL DEFAULT '0',
  `houdin_package_email` int(11) NOT NULL DEFAULT '0',
  `houdin_package_accounting` enum('yes','no') NOT NULL,
  `houdin_package_pos` enum('yes','no') NOT NULL,
  `houdin_package_delivery` enum('yes','no') NOT NULL,
  `houdin_package_payment_gateway` enum('yes','no') NOT NULL,
  `houdin_package_templates` int(11) NOT NULL DEFAULT '0',
  `houdin_package_countries` longblob NOT NULL,
  `houdin_package_description` text NOT NULL,
  `houdin_package_status` enum('active','deactive') NOT NULL,
  `houdin_package_created_at` varchar(30) DEFAULT NULL,
  `houdin_package_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_packages`
--

INSERT INTO `houdin_packages` (`houdin_package_id`, `houdin_package_name`, `houdin_package_price`, `houdin_package_product`, `houdin_package_staff_accounts`, `houdin_package_stock_location`, `houdin_package_app_access`, `houdin_package_order_pm`, `houdin_package_sms`, `houdin_package_email`, `houdin_package_accounting`, `houdin_package_pos`, `houdin_package_delivery`, `houdin_package_payment_gateway`, `houdin_package_templates`, `houdin_package_countries`, `houdin_package_description`, `houdin_package_status`, `houdin_package_created_at`, `houdin_package_updated_at`) VALUES
(1, 'Standard', 36000, 10000, 5, 2, 'limited', 20000, 500, 500, 'yes', 'yes', 'yes', 'yes', 5, '', '<p>dsfsdfs</p>', 'active', '1536192000', NULL),
(2, 'Professional', 54000, 25000, 15, 4, 'full', 50000, 1000, 1000, 'yes', 'yes', 'yes', 'yes', 15, '', '<p>dfs</p>', 'active', '1536192000', NULL),
(3, 'Test', 120, 10, 2, 1, 'limited', 12, 10, 10, 'yes', 'yes', 'no', 'no', 1, '', '<p>Test</p>', 'active', '1536192000', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_package_plugins`
--

CREATE TABLE `houdin_package_plugins` (
  `houdin_package_plugin_id` int(10) UNSIGNED NOT NULL,
  `houdin_package_plugin_plu_id` varchar(30) DEFAULT NULL,
  `houdin_package_plugin_pkg_id` int(10) UNSIGNED DEFAULT NULL,
  `houdin_package_plugin_created_at` varchar(30) DEFAULT NULL,
  `houdin_package_plugin_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_package_plugins`
--

INSERT INTO `houdin_package_plugins` (`houdin_package_plugin_id`, `houdin_package_plugin_plu_id`, `houdin_package_plugin_pkg_id`, `houdin_package_plugin_created_at`, `houdin_package_plugin_updated_at`) VALUES
(1, '1,2,3', 1, '1536063856', NULL),
(2, '4,5,6', 2, '1536063882', NULL),
(3, '5,6,7', 3, '1536063908', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_payumoney`
--

CREATE TABLE `houdin_payumoney` (
  `houdin_payumoney` int(11) NOT NULL,
  `houdin_payumoney_key` varchar(100) NOT NULL,
  `houdin_payumoney_salt` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_payumoney`
--

INSERT INTO `houdin_payumoney` (`houdin_payumoney`, `houdin_payumoney_key`, `houdin_payumoney_salt`) VALUES
(1, 'MBVIetjo', 'U3YohhxRvu');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_plugins`
--

CREATE TABLE `houdin_plugins` (
  `houdin_plugin_id` int(10) UNSIGNED NOT NULL,
  `houdin_plugin_name` varchar(150) NOT NULL,
  `houdin_plugin_description` text NOT NULL,
  `houdin_plugin_countries` longblob NOT NULL,
  `houdin_plugin_image` varchar(100) NOT NULL,
  `houdin_plugin_active_status` enum('deactive','active') DEFAULT NULL,
  `houdin_plugin_created_at` varchar(30) DEFAULT NULL,
  `houdin_plugin_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_plugins`
--

INSERT INTO `houdin_plugins` (`houdin_plugin_id`, `houdin_plugin_name`, `houdin_plugin_description`, `houdin_plugin_countries`, `houdin_plugin_image`, `houdin_plugin_active_status`, `houdin_plugin_created_at`, `houdin_plugin_updated_at`) VALUES
(1, '250 SMS', '<p>dfdfg</p>', '', '', 'active', '1536062927', '1536062972'),
(2, '250 email', '<p>250 email</p>', '', '', 'active', '1536062984', '1536063118'),
(3, '6 month subscription', '<p>6 month subscription</p>', '', '', 'active', '1536063003', '1536063172'),
(4, '12 month subscription', '<p>12 month subscription</p>', '', '', 'active', '1536063017', '1536063182'),
(5, '24 month subscription', '<p>24 month subscription</p>', '', '', 'active', '1536063030', '1536063424'),
(6, '500 SMS', '<p>500 SMS</p>', '', '', 'active', '1536063045', '1536063437'),
(7, '1000 SMS', '<p>1000 SMS</p>', '', '', 'active', '1536063057', '1536063447'),
(8, '500 email', '<p>500 email</p>', '', '', 'active', '1536063071', '1536063458'),
(9, '1000 email', '<p>1000 email</p>', '', '', 'active', '1536063085', '1536063472');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_products`
--

CREATE TABLE `houdin_products` (
  `houdin_products_id` int(11) NOT NULL,
  `houdin_products_category` int(11) NOT NULL,
  `houdin_products_title` varchar(255) NOT NULL,
  `houdin_products_short_desc` text NOT NULL,
  `houdin_products_desc` longblob NOT NULL,
  `houdin_products_type` int(11) NOT NULL,
  `houdin_products_show_on` varchar(50) NOT NULL,
  `houdin_products_attributes` text NOT NULL,
  `houdin_products_created_date` varchar(255) NOT NULL,
  `houdin_products_updated_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_products_variants`
--

CREATE TABLE `houdin_products_variants` (
  `houdin_products_variants_id` int(11) NOT NULL,
  `houdin_products_variants_name` varchar(100) NOT NULL,
  `houdin_products_variants_value` varchar(100) NOT NULL,
  `houdin_products_variants_barcode` varchar(255) NOT NULL,
  `houdin_products_variants_prices` varchar(255) NOT NULL,
  `houdin_products_variants_sku` varchar(100) NOT NULL,
  `houdin_products_variants_stock` int(11) NOT NULL,
  `houdin_products_variants_minimum_order` int(11) NOT NULL,
  `houdin_products_variants_sort_order` int(11) NOT NULL,
  `houdin_products_variants_featured` int(1) NOT NULL,
  `houdin_products_variants_payment_options` varchar(255) NOT NULL,
  `houdin_products_variants_active_status` int(1) NOT NULL,
  `houdin_products_variants_product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_product_attributers`
--

CREATE TABLE `houdin_product_attributers` (
  `houdin_product_att_id` int(10) UNSIGNED NOT NULL,
  `houdin_product_att_name` varchar(150) NOT NULL,
  `houdin_product_att_desc` tinytext,
  `houdin_product_att_unit` varchar(20) DEFAULT NULL,
  `houdin_product_att_created_at` timestamp NULL DEFAULT NULL,
  `houdin_product_att_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_product_types`
--

CREATE TABLE `houdin_product_types` (
  `houdin_product_type_id` int(10) UNSIGNED NOT NULL,
  `houdin_product_type_businness_category` int(10) UNSIGNED NOT NULL,
  `houdin_product_type_name` varchar(150) NOT NULL,
  `houdin_product_type_description` tinytext,
  `houdin_product_type_created_at` timestamp NULL DEFAULT NULL,
  `houdin_product_type_updatedat` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_product_type_attributes`
--

CREATE TABLE `houdin_product_type_attributes` (
  `houdin_product_type_attribute_id` int(10) UNSIGNED NOT NULL,
  `houdin_product_type_attribute_type_id` int(10) UNSIGNED NOT NULL,
  `houdin_product_type_attribute_att_id` int(10) UNSIGNED NOT NULL,
  `houdin_product_type_attribute_created_at` timestamp NULL DEFAULT NULL,
  `houdin_product_type_attribute_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_roles`
--

CREATE TABLE `houdin_roles` (
  `houdin_role_id` int(11) NOT NULL,
  `houdin_role_name` varchar(150) NOT NULL,
  `houdin_role_description` varchar(200) DEFAULT NULL,
  `houdin_role_permissions` varchar(150) DEFAULT NULL,
  `houdin_role_created_at` timestamp NULL DEFAULT NULL,
  `houdin_role_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_sendgrid`
--

CREATE TABLE `houdin_sendgrid` (
  `houdin_sendgrid_id` int(11) NOT NULL,
  `houdin_sendgrid_username` varchar(100) NOT NULL,
  `houdin_sendgrid_password` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_sendgrid`
--

INSERT INTO `houdin_sendgrid` (`houdin_sendgrid_id`, `houdin_sendgrid_username`, `houdin_sendgrid_password`) VALUES
(1, 'anuankita', 'india@123');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_sms_package`
--

CREATE TABLE `houdin_sms_package` (
  `houdin_sms_package_id` int(11) NOT NULL,
  `houdin_sms_package_name` varchar(50) NOT NULL,
  `houdin_sms_package_price` int(11) NOT NULL,
  `houdin_sms_package_sms_count` int(11) NOT NULL,
  `houdin_sms_package_status` enum('active','deactive') NOT NULL,
  `houdin_sms_package_created_at` varchar(30) NOT NULL,
  `houdin_sms_package_modified_at` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_sms_package`
--

INSERT INTO `houdin_sms_package` (`houdin_sms_package_id`, `houdin_sms_package_name`, `houdin_sms_package_price`, `houdin_sms_package_sms_count`, `houdin_sms_package_status`, `houdin_sms_package_created_at`, `houdin_sms_package_modified_at`) VALUES
(1, 'Package 1', 240, 240, 'active', '1530705600', '1530669123'),
(3, 'Package 2', 240, 240, 'active', '1530768239', '');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_tax`
--

CREATE TABLE `houdin_tax` (
  `houdin_tax_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_templates`
--

CREATE TABLE `houdin_templates` (
  `houdin_template_id` int(11) NOT NULL,
  `houdin_template_file` varchar(300) DEFAULT NULL COMMENT 'Location of template folder',
  `houdin_template_name` varchar(50) NOT NULL,
  `houdin_template_category` int(11) NOT NULL,
  `houdin_template_thumbnail` varchar(100) NOT NULL,
  `houdin_template_description` text NOT NULL,
  `houdin_template_status` enum('active','deactive') NOT NULL,
  `houdin_template_created_at` varchar(30) DEFAULT NULL,
  `houdin_template_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_templates`
--

INSERT INTO `houdin_templates` (`houdin_template_id`, `houdin_template_file`, `houdin_template_name`, `houdin_template_category`, `houdin_template_thumbnail`, `houdin_template_description`, `houdin_template_status`, `houdin_template_created_at`, `houdin_template_updated_at`) VALUES
(4, 'Departmental/home', 'Departmental', 1, '660789departmental.png', '<p>safsdfs</p>', 'active', '1531995484', NULL),
(5, 'General/home', 'General', 8, '662779departmental.png', '<p>General</p>', 'active', '1531962189', NULL),
(6, 'Apparels/home', 'Apparels Store', 3, '655904departmental.png', '<p>Apparels Store</p>', 'active', '1532080767', '1532080800'),
(7, 'Bakery/home', 'Bakery Store', 7, '634069departmental.png', '<p>Bakery</p>', 'active', '1532080832', NULL),
(8, 'Optical/home', 'Optical', 6, '167855departmental.png', '<p>Optical</p>', 'active', '1532080858', NULL),
(9, 'Stones/home', 'Stones', 4, '495814departmental.png', '<p>Stones</p>', 'active', '1532080878', NULL),
(10, 'Electronic/home', 'Electronics', 9, '754164departmental.png', '<p>Electronics</p>', 'active', '1532151396', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_twilio`
--

CREATE TABLE `houdin_twilio` (
  `houdin_twilio_id` int(11) NOT NULL,
  `houdin_twilio_sid` varchar(100) NOT NULL,
  `houdin_twilio_token` varchar(100) NOT NULL,
  `houdin_twilio_number` varchar(30) NOT NULL,
  `houdin_twilio_created_at` varchar(30) DEFAULT NULL,
  `houdin_twilio_modified_at` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_twilio`
--

INSERT INTO `houdin_twilio` (`houdin_twilio_id`, `houdin_twilio_sid`, `houdin_twilio_token`, `houdin_twilio_number`, `houdin_twilio_created_at`, `houdin_twilio_modified_at`) VALUES
(1, 'AC48b8bf0d881343a918bc7149a6cec081', '4cb2ee3b5e386fe672cd515c90d843f1', '+15124883746', '1530775414', '1530775644');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_users`
--

CREATE TABLE `houdin_users` (
  `houdin_user_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_name` varchar(150) NOT NULL,
  `houdin_user_email` varchar(150) DEFAULT NULL,
  `houdin_user_password` varchar(100) NOT NULL,
  `houdin_user_password_salt` varchar(100) NOT NULL,
  `houdin_users_package_id` int(11) DEFAULT '0',
  `houdin_users_package_expiry` date DEFAULT NULL,
  `houdin_user_reset_token` varchar(255) NOT NULL,
  `houdin_user_contact` varchar(15) NOT NULL,
  `houdin_user_address` text NOT NULL,
  `houdin_user_city` varchar(30) NOT NULL,
  `houdin_user_country` int(11) NOT NULL,
  `houdin_users_currency` enum('INR','USD','AUD','EURO','POUND') NOT NULL,
  `houdin_user_registration_status` int(1) NOT NULL,
  `houdin_user_is_verified` tinyint(4) NOT NULL,
  `houdin_user_is_active` enum('active','deactive','block') NOT NULL,
  `houdin_user_created_at` varchar(30) DEFAULT NULL,
  `houdin_user_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_users`
--

INSERT INTO `houdin_users` (`houdin_user_id`, `houdin_user_name`, `houdin_user_email`, `houdin_user_password`, `houdin_user_password_salt`, `houdin_users_package_id`, `houdin_users_package_expiry`, `houdin_user_reset_token`, `houdin_user_contact`, `houdin_user_address`, `houdin_user_city`, `houdin_user_country`, `houdin_users_currency`, `houdin_user_registration_status`, `houdin_user_is_verified`, `houdin_user_is_active`, `houdin_user_created_at`, `houdin_user_updated_at`) VALUES
(1, 'shivam sengar', 'info@hawkscode.com', '$2y$10$gVMXKaudg.LzilZkWrIRVe2yO5uQSEmILBnmRN21Pf.Io/yAH3Xc.', '$2y$10$gVMXKaudg.LzilZkWrIRVeYeFGxD1KINgGZh9SowdxLfJ0qzAOF26', 2, '2019-10-01', '', '+917790870946', 'hawkscode softwares', '', 2, 'USD', 1, 1, 'active', '1528981073', '1529977575'),
(2, 'shivam', 'shivam.hawkscode@gmail.com', '$2y$10$dxenMpWZJxoWHhLLihJ6n.H1rCBJHQJI3PT0XPCg85scQ/7jd19uK', '$2y$10$dxenMpWZJxoWHhLLihJ6n.QYxndi1l99VXO47H4PIdYSRqTVRRFHu', 0, '2018-09-23', '', '+918218347967', 'sdkjfnj', '', 1, 'USD', 1, 1, 'active', '1531993014', '1531994494'),
(3, 'category 1', 'category1@gmail.com', '$2y$10$gFttEvi4K5ysa3WkRFsox.z8TNTY1IIKWB9Mr.vi0qZPPs7rcNOlK', '$2y$10$gFttEvi4K5ysa3WkRFsox.NWL55jRh2Bg3vWTuzccZrVOBX65zb6W', 1, '2018-09-23', '', '+918561049836', 'category1', '', 6, 'USD', 1, 1, 'active', '1532081015', '1532081097'),
(4, 'category 2', 'category2@gmail.com', '$2y$10$4F7TKvxY1Dx1ao1UlKNci.9eO8xi6et5sYYQMxTA4qqO0X2SHerw2', '$2y$10$4F7TKvxY1Dx1ao1UlKNci.F1Se07VRlgL3jqrd1GaY4QLaiqFUxX.', 0, '2018-09-23', '', '+918561049837', 'category1', '', 6, 'USD', 1, 1, 'active', '1532081150', '1532081198'),
(5, 'category3', 'category4@gmail.com', '$2y$10$cWh1Lc3V9rh2p9U1SePnjOmmrqg7diJsfB5dxZt1fIcwHlRoAGBEC', '$2y$10$cWh1Lc3V9rh2p9U1SePnjOVXn9TMzADRfbFDYrUChRoe08Bj5Iwxm', 0, '2018-09-23', '', '+918561049838', 'category4', '', 6, 'USD', 1, 1, 'active', '1532081424', '1532081473'),
(7, 'category5', 'category5@gmail.com', '$2y$10$Odc7eNdWHeOjT5MzFPuVNex5W0lWJcmm6s7RBMZyRnr7NU/x6UExm', '$2y$10$Odc7eNdWHeOjT5MzFPuVNeB514pD6lcBkjcvaY9NBv5Dbi.PXQmV.', 0, '2018-09-23', '', '+918561049888', 'category5', '', 6, 'USD', 1, 1, 'active', '1532081777', '1532081827'),
(8, 'Electronics Shop', 'shivamelectricals@gmail.com', '$2y$10$qFVOvwyiCFKkjhn8lztHvO30hySqhObJub2p/LfMSd/69kDG7w4se', '$2y$10$qFVOvwyiCFKkjhn8lztHvOv5IZadxjNiJZGO4TOWoM/u2WTd20nju', 0, '2018-09-23', '', '+918654322178', 'shivamelectricals', '', 6, 'USD', 1, 1, 'active', '1532151471', '1532151536'),
(10, 'Houdin', 'houdin@gmail.com', '$2y$10$MU0ewd5C8cMyyM16PKbICeML6SzzDa4AU8PGHtlEC5MQKv9/tczEW', '$2y$10$MU0ewd5C8cMyyM16PKbICe9je9GOn5Mee3SOQ5tad5YeVO336uGr.', 0, '2018-09-23', '', '+918212340987', '602 kailsh tower', '', 6, 'USD', 1, 1, 'active', '1535505579', '1535505655'),
(16, 'userios', 'userios@gmail.com', '$2y$10$85/J65w3EDr5sb/Svfdnd.wDd5dhN3XqJC.8.bUZKPFZqrP5RZO4W', '$2y$10$85/J65w3EDr5sb/Svfdnd.wDd5dhN3XqJC.8.bUZKPFZqrP5RZO4W', 1, '2019-09-04', '', '+91234567890', '<p>xvx</p>', 'jaipur', 1, 'USD', 1, 1, 'active', '1536069120', NULL),
(23, 'shivam199420@gmail.com', 'hawkselectricals@hawkselectricals.com', '$2y$10$IxRF9d/RTFdd99VWZB/Q1ec8sY767chwRsVPTQ07Vj2C45hXaJ6eW', '$2y$10$IxRF9d/RTFdd99VWZB/Q1ehSiD4cEEdhRn2ZQqOR2Bidt.Ahlg09i', 0, '2018-10-22', '', '+918218312345', '602 603 kailash tower', '', 5, 'INR', 1, 1, 'active', '1538454537', '1538454979'),
(24, 'currency', 'currency@currency.com', '$2y$10$ZTBGp6YSzp2W0gw9TlxftegEuCoqb3LeW165XciyZSTykBQ0SDLC.', '$2y$10$ZTBGp6YSzp2W0gw9TlxftegEuCoqb3LeW165XciyZSTykBQ0SDLC.', 0, '2018-10-22', '', '+912345671289', '<p>d 35 doctors</p>', 'jaipur', 2, 'USD', 1, 1, 'active', '1538455574', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_auth`
--

CREATE TABLE `houdin_user_auth` (
  `houdin_user_auth_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_auth_url_token` varchar(150) NOT NULL,
  `houdin_user_auth_auth_token` varchar(150) NOT NULL,
  `houdin_user_auth_user_id` varchar(150) NOT NULL,
  `houdin_user_auth_created_at` varchar(100) NOT NULL,
  `houdin_user_auth_updated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_user_auth`
--

INSERT INTO `houdin_user_auth` (`houdin_user_auth_id`, `houdin_user_auth_url_token`, `houdin_user_auth_auth_token`, `houdin_user_auth_user_id`, `houdin_user_auth_created_at`, `houdin_user_auth_updated_at`) VALUES
(1, '', '', '1', '1528369455', '1538455229'),
(2, '', '', '3', '1536059383', '1536059383');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_forgot_password`
--

CREATE TABLE `houdin_user_forgot_password` (
  `houdin_user_forgot_password_id` int(11) NOT NULL,
  `houdin_user_forgot_password_user_id` int(11) NOT NULL,
  `houdin_user_forgot_password_token` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_user_forgot_password`
--

INSERT INTO `houdin_user_forgot_password` (`houdin_user_forgot_password_id`, `houdin_user_forgot_password_user_id`, `houdin_user_forgot_password_token`) VALUES
(2, 1, 99943),
(3, 1, 39035),
(4, 1, 20293),
(5, 1, 69108);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_logs`
--

CREATE TABLE `houdin_user_logs` (
  `houdin_user_log_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_log_userId` varchar(100) NOT NULL,
  `houdin_user_log_ip_address` varchar(20) NOT NULL,
  `houdin_user_log_browser` varchar(150) NOT NULL,
  `houdin_user_log_geo_location` varchar(50) NOT NULL,
  `houdin_user_log_created_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_user_logs`
--

INSERT INTO `houdin_user_logs` (`houdin_user_log_id`, `houdin_user_log_userId`, `houdin_user_log_ip_address`, `houdin_user_log_browser`, `houdin_user_log_geo_location`, `houdin_user_log_created_at`) VALUES
(1, '1', '103.66.73.15', 'Chrome', 'India', '1528369455'),
(2, '1', '103.66.73.15', 'Chrome', 'India', '1528370053'),
(3, '1', '103.66.73.15', 'Chrome', 'India', '1528374243'),
(4, '1', '103.66.73.15', 'Chrome', 'India', '1528333532'),
(5, '1', '103.66.73.15', 'Chrome', 'India', '1528333926'),
(6, '1', '103.66.73.15', 'Chrome', 'India', '1528432238'),
(7, '1', '103.66.73.15', 'Chrome', 'India', '1528433117'),
(8, '1', '103.66.73.15', 'Chrome', 'India', '1528437801'),
(9, '1', '103.66.73.15', 'Chrome', 'India', '1528440240'),
(10, '1', '103.66.73.15', 'Chrome', 'India', '1528454940'),
(11, '1', '103.66.73.15', 'Chrome', 'India', '1528690462'),
(12, '1', '103.66.73.15', 'Chrome', 'India', '1528696190'),
(13, '1', '103.66.73.15', 'Chrome', 'India', '1528707159'),
(14, '1', '103.66.73.15', 'Chrome', 'India', '1528710645'),
(15, '1', '103.66.73.15', 'Chrome', 'India', '1528716686'),
(16, '1', '103.66.73.15', 'Chrome', 'India', '1528679447'),
(17, '1', '103.66.73.15', 'Chrome', 'India', '1528779296'),
(18, '1', '103.66.73.15', 'Chrome', 'India', '1528781203'),
(19, '1', '103.66.73.15', 'Chrome', 'India', '1528783329'),
(20, '1', '103.66.73.15', 'Chrome', 'India', '1528800417'),
(21, '1', '103.66.73.15', 'Chrome', 'India', '1528807075'),
(22, '1', '103.66.73.15', 'Chrome', 'India', '1528766884'),
(23, '1', '103.66.73.15', 'Chrome', 'India', '1528870502'),
(24, '1', '103.66.73.15', 'Chrome', 'India', '1528880678'),
(25, '1', '103.66.73.15', 'Chrome', 'India', '1528882013'),
(26, '1', '103.66.73.15', 'Chrome', 'India', '1528883001'),
(27, '1', '103.66.73.15', 'Chrome', 'India', '1528887502'),
(28, '1', '103.66.73.15', 'Chrome', 'India', '1528887529'),
(29, '1', '103.66.73.15', 'Chrome', 'India', '1528888151'),
(30, '1', '103.66.73.15', 'Chrome', 'India', '1528888176'),
(31, '1', '103.66.73.15', 'Chrome', 'India', '1528893010'),
(32, '1', '103.66.73.15', 'Chrome', '', '1528853785'),
(33, '1', '103.66.73.15', 'Chrome', '', '1528854890'),
(34, '1', '103.66.73.15', 'Chrome', 'India', '1528950175'),
(35, '1', '103.66.73.15', 'Chrome', 'India', '1528951262'),
(36, '1', '103.66.73.15', 'Chrome', 'India', '1528971649'),
(37, '1', '103.66.73.15', 'Chrome', 'India', '1528977673'),
(38, '1', '103.66.73.15', 'Chrome', 'India', '1528978587'),
(39, '1', '103.66.73.15', 'Chrome', 'India', '1529037265'),
(40, '1', '103.66.73.15', 'Chrome', 'India', '1529049238'),
(41, '1', '103.66.73.15', 'Chrome', 'India', '1529053068'),
(42, '1', '103.66.73.15', 'Chrome', 'India', '1529060592'),
(43, '1', '103.66.73.15', 'Chrome', 'India', '1529062672'),
(44, '1', '103.66.73.15', 'Chrome', 'India', '1529065137'),
(45, '1', '103.66.73.15', 'Chrome', 'India', '1529283834'),
(46, '1', '103.66.73.15', 'Chrome', 'India', '1529402256'),
(47, '1', '103.66.73.15', 'Chrome', 'India', '1529491428'),
(48, '1', '103.66.73.15', 'Chrome', 'India', '1530680928'),
(49, '1', '103.66.73.15', 'Chrome', 'India', '1530681956'),
(50, '1', '103.66.73.15', 'Chrome', 'India', '1530702287'),
(51, '1', '103.66.73.15', 'Chrome', 'India', '1530704052'),
(52, '1', '103.66.73.15', 'Chrome', 'India', '1530768181'),
(53, '1', '103.66.73.15', 'Chrome', 'India', '1530841821'),
(54, '1', '103.66.73.15', 'Chrome', 'India', '1531309824'),
(55, '1', '103.66.73.15', 'Chrome', 'India', '1531469564'),
(56, '1', '103.66.73.15', 'Chrome', 'India', '1531716016'),
(57, '1', '103.66.73.15', 'Chrome', 'India', '1531716136'),
(58, '1', '103.66.73.15', 'Chrome', 'India', '1531992827'),
(59, '1', '103.66.73.15', 'Chrome', 'India', '1531962147'),
(60, '1', '104.131.241.248', 'Chrome', 'United States', '1532150264'),
(61, '1', '103.66.73.15', 'Chrome', 'India', '1534138047'),
(62, '1', '103.66.73.15', 'Chrome', 'India', '1534138425'),
(63, '1', '103.66.73.15', 'Chrome', 'India', '1534229592'),
(64, '1', '103.66.73.15', 'Chrome', 'India', '1534229693'),
(65, '1', '103.66.73.15', 'Chrome', 'India', '1534237390'),
(66, '1', '66.85.185.88', 'Chrome', 'United States', '1535780458'),
(67, '1', '184.164.146.5', 'Chrome', 'United States', '1536035059'),
(68, '1', '184.164.146.5', 'Chrome', 'United States', '1536058739'),
(69, '1', '184.164.146.5', 'Chrome', 'United States', '1536059204'),
(70, '3', '184.164.146.5', 'Chrome', 'United States', '1536059383'),
(71, '1', '184.164.146.5', 'Chrome', 'United States', '1536060113'),
(72, '1', '184.164.146.5', 'Chrome', 'United States', '1536062586'),
(73, '1', '184.164.146.5', 'Chrome', 'United States', '1536025612'),
(74, '1', '66.85.185.70', 'Chrome', 'United States', '1536123958'),
(75, '1', '66.85.185.70', 'Chrome', 'United States', '1536152039'),
(76, '1', '66.85.185.70', 'Chrome', 'United States', '1536152080'),
(77, '1', '66.85.185.70', 'Chrome', 'United States', '1536152147'),
(78, '1', '66.85.185.81', 'Chrome', 'United States', '1536208371'),
(79, '1', '162.243.163.100', 'Chrome', 'United States', '1536584192'),
(80, '1', '103.66.73.15', 'Chrome', 'India', '1536639998'),
(81, '1', '103.66.73.15', 'Chrome', 'India', '1536641737'),
(82, '1', '103.66.73.15', 'Chrome', 'India', '1537176317'),
(83, '1', '103.66.73.15', 'Chrome', 'India', '1537871033'),
(84, '1', '103.66.73.15', 'Chrome', 'India', '1538455229');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_payments`
--

CREATE TABLE `houdin_user_payments` (
  `houdin_user_payment_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_user_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_package_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_coupon_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_cost` float NOT NULL,
  `houdin_user_payment_currency` varchar(50) NOT NULL,
  `houdin_user_payment_transcaction_id` varchar(150) NOT NULL,
  `houdin_user_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 For Unsuccess, 1 For Success, 2 For cancelled, 2 For refund',
  `houdin_user_payment_time` datetime NOT NULL,
  `houdin_user_payment_start_date` date NOT NULL,
  `houdin_user_payment_end_date` date DEFAULT NULL,
  `houdin_user_payment_created_at` timestamp NULL DEFAULT NULL,
  `houdin_user_payment_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_payment_tracks`
--

CREATE TABLE `houdin_user_payment_tracks` (
  `houdin_user_payment_track_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_track_user_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_track_package_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_payment_track_last_done` datetime NOT NULL,
  `houdin_user_payment_track_status` tinyint(4) NOT NULL,
  `houdin_user_payment_track_created_at` timestamp NULL DEFAULT NULL,
  `houdin_user_payment_track_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_second_auth`
--

CREATE TABLE `houdin_user_second_auth` (
  `houdin_user_second_auth_id` int(11) NOT NULL,
  `houdin_user_second_auth_user_id` int(11) NOT NULL,
  `houdin_user_second_auth_token` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_user_second_auth`
--

INSERT INTO `houdin_user_second_auth` (`houdin_user_second_auth_id`, `houdin_user_second_auth_user_id`, `houdin_user_second_auth_token`) VALUES
(12, 14, 72432),
(9, 9, 40961),
(14, 17, 67753),
(15, 18, 34537),
(16, 19, 65031),
(17, 20, 96592),
(18, 21, 35839),
(19, 22, 59140);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_shops`
--

CREATE TABLE `houdin_user_shops` (
  `houdin_user_shop_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_shop_user_id` int(10) UNSIGNED NOT NULL,
  `houdin_user_shop_shop_name` varchar(250) NOT NULL,
  `houdin_user_shop_email` varchar(150) DEFAULT NULL,
  `houdin_user_shop_website` varchar(250) DEFAULT NULL,
  `houdin_user_shop_phone` varchar(50) DEFAULT NULL,
  `houdin_user_shop_address` varchar(400) NOT NULL,
  `houdin_user_shop_city` varchar(150) NOT NULL,
  `houdin_user_shop_state` varchar(150) NOT NULL,
  `houdin_user_shop_country` varchar(150) NOT NULL,
  `houdin_user_shops_category` int(11) NOT NULL,
  `houdin_user_shops_language` int(11) NOT NULL,
  `houdin_user_shop_db_name` varchar(150) NOT NULL,
  `houdin_user_shop_active_status` enum('active','deactive','block') NOT NULL COMMENT '0 For Deactive, 1 For Active',
  `houdin_user_shop_created_at` varchar(30) DEFAULT NULL,
  `houdin_user_shop_updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_user_shops`
--

INSERT INTO `houdin_user_shops` (`houdin_user_shop_id`, `houdin_user_shop_user_id`, `houdin_user_shop_shop_name`, `houdin_user_shop_email`, `houdin_user_shop_website`, `houdin_user_shop_phone`, `houdin_user_shop_address`, `houdin_user_shop_city`, `houdin_user_shop_state`, `houdin_user_shop_country`, `houdin_user_shops_category`, `houdin_user_shops_language`, `houdin_user_shop_db_name`, `houdin_user_shop_active_status`, `houdin_user_shop_created_at`, `houdin_user_shop_updated_at`) VALUES
(1, 1, 'hawkscodeteam', NULL, NULL, NULL, '', '', '', '2', 8, 3, 'hawksind_houdinca', 'active', '1528938282', '1529058893'),
(2, 2, 'shivamhawskcode', NULL, NULL, NULL, '', '', '', '6', 1, 3, 'hawksind_houdinca', 'active', '1531994494', NULL),
(3, 3, 'category1', NULL, NULL, NULL, '', '', '', '6', 3, 3, 'hawksind_houdinca', 'active', '1532081097', NULL),
(4, 4, 'category2', NULL, NULL, NULL, '', '', '', '6', 4, 3, 'hawksind_houdinca', 'active', '1532081198', NULL),
(5, 5, 'category4', NULL, NULL, NULL, '', '', '', '6', 6, 3, 'hawksind_houdinca', 'active', '1532081473', NULL),
(6, 7, 'category5', NULL, NULL, NULL, '', '', '', '6', 7, 3, 'hawksind_houdinca', 'active', '1532081827', NULL),
(7, 8, 'shivamelectricals', NULL, NULL, NULL, '', '', '', '6', 9, 3, 'hawksind_houdinca', 'active', '1532151536', NULL),
(8, 10, 'houdin', NULL, NULL, NULL, '', '', '', '6', 3, 3, 'hawksind_houdinvendordb', 'active', '1535505655', NULL),
(9, 13, 'techhawks', NULL, NULL, NULL, '', '', '', '2', 1, 3, 'hawksind_houdinca', 'active', '1536043518', NULL),
(10, 15, 'techhawkscode', NULL, NULL, NULL, '', '', '', '2', 1, 3, 'houdin_techhawkscode_db', 'active', '1536025497', NULL),
(11, 16, 'userios', NULL, NULL, NULL, '', '', '', '1', 1, 3, 'hawksind_houdinca', 'active', '1536069120', NULL),
(12, 23, 'hawkselectricals', NULL, NULL, NULL, '', '', '', '5', 9, 3, 'houdin_hawkselectricals_db', 'active', '1538454979', NULL),
(13, 24, 'currency Store', NULL, NULL, NULL, '', '', '', '2', 1, 3, 'hawksind_houdinca', 'active', '1538455574', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `houdin_user_verifications`
--

CREATE TABLE `houdin_user_verifications` (
  `houdin_user_verification_email` varchar(150) NOT NULL,
  `houdin_user_verification_token` varchar(150) NOT NULL,
  `houdin_user_verification_created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `houdin_vendor_auth`
--

CREATE TABLE `houdin_vendor_auth` (
  `houdin_vendor_auth_id` int(11) NOT NULL,
  `houdin_vendor_auth_vendor_id` int(11) NOT NULL,
  `houdin_vendor_auth_token` varchar(50) NOT NULL,
  `houdin_vendor_auth_url_token` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_vendor_auth`
--

INSERT INTO `houdin_vendor_auth` (`houdin_vendor_auth_id`, `houdin_vendor_auth_vendor_id`, `houdin_vendor_auth_token`, `houdin_vendor_auth_url_token`) VALUES
(1, 1, 'dXuBO6', 'dXuBO6'),
(4, 1, 's3v29m', 's3v29m'),
(5, 1, 'rEWU6y', 'rEWU6y'),
(6, 1, '91mt4m', '91mt4m'),
(7, 1, 'jrg6XL', 'jrg6XL'),
(8, 1, 'euwp8R', 'euwp8R'),
(9, 1, 'a8h5nD', 'a8h5nD'),
(10, 1, '1amLQg', '1amLQg'),
(11, 1, '4BY9iN', '4BY9iN'),
(12, 1, 'BjC7nQ', 'BjC7nQ'),
(13, 1, 'kini6s', 'kini6s'),
(14, 1, 'P8ihEH', 'P8ihEH'),
(15, 1, '4C5Wi7', '4C5Wi7'),
(16, 1, 'pw193M', 'pw193M'),
(17, 1, '91YxqQ', '91YxqQ');

-- --------------------------------------------------------

--
-- Table structure for table `houdin_vendor_user_log`
--

CREATE TABLE `houdin_vendor_user_log` (
  `houdin_vendor_user_log_id` int(11) NOT NULL,
  `houdin_vendor_user_log_userid` int(11) NOT NULL,
  `houdin_vendor_user_log_ip_address` varchar(50) NOT NULL,
  `houdin_vendor_user_log_browser` varchar(30) NOT NULL,
  `houdin_vendor_user_log_location` varchar(30) NOT NULL,
  `houdin_vendor_user_log_created_at` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `houdin_vendor_user_log`
--

INSERT INTO `houdin_vendor_user_log` (`houdin_vendor_user_log_id`, `houdin_vendor_user_log_userid`, `houdin_vendor_user_log_ip_address`, `houdin_vendor_user_log_browser`, `houdin_vendor_user_log_location`, `houdin_vendor_user_log_created_at`) VALUES
(1, 1, '103.66.73.15', 'Chrome', 'India', '1529053028'),
(2, 1, '103.66.73.15', 'Chrome', 'India', '1529063332'),
(3, 1, '103.66.73.15', 'Chrome', 'India', '1529065948'),
(4, 1, '103.66.73.15', 'Chrome', 'India', '1529126099'),
(5, 1, '103.66.73.15', 'Chrome', 'India', '1529321136'),
(6, 1, '103.66.73.15', 'Chrome', 'India', '1529321993'),
(7, 1, '103.66.73.15', 'Chrome', 'India', '1529284566'),
(8, 1, '103.66.73.15', 'Chrome', 'India', '1529383821'),
(9, 1, '103.66.73.15', 'Chrome', 'India', '1529383944'),
(10, 1, '103.66.73.15', 'Chrome', 'India', '1529384285'),
(11, 1, '103.66.73.15', 'Chrome', 'India', '1529407185'),
(12, 1, '103.66.73.15', 'Chrome', 'India', '1529412027'),
(13, 1, '103.66.73.15', 'Chrome', 'India', '1529372999'),
(14, 1, '103.66.73.15', 'Chrome', 'India', '1529469717'),
(15, 1, '103.66.73.15', 'Chrome', 'India', '1529472751'),
(16, 1, '103.66.73.15', 'Chrome', 'India', '1529473500'),
(17, 1, '103.66.73.15', 'Chrome', 'India', '1529477246'),
(18, 1, '103.66.73.15', 'Chrome', 'India', '1529487640'),
(19, 1, '103.66.73.15', 'Chrome', 'India', '1529493369'),
(20, 1, '103.66.73.15', 'Chrome', 'India', '1529493370'),
(21, 1, '103.66.73.15', 'Chrome', 'India', '1529556586'),
(22, 1, '103.66.73.15', 'Chrome', 'India', '1529559143'),
(23, 1, '103.66.73.15', 'Chrome', 'India', '1529563276'),
(24, 1, '103.66.73.15', 'Chrome', 'India', '1529644831'),
(25, 1, '103.66.73.15', 'Chrome', 'India', '1529646533'),
(26, 1, '103.66.73.15', 'Chrome', 'India', '1529661863'),
(27, 1, '103.66.73.15', 'Chrome', 'India', '1529665468'),
(28, 1, '103.66.73.15', 'Chrome', 'India', '1529990525'),
(29, 1, '103.66.73.15', 'Chrome', 'India', '1530348203'),
(30, 1, '103.66.73.15', 'Chrome', 'India', '1530524964'),
(31, 1, '103.66.73.15', 'Chrome', 'India', '1530593018'),
(32, 1, '103.66.73.15', 'Chrome', 'India', '1530841210'),
(33, 1, '103.66.73.15', 'Chrome', 'India', '1530842073'),
(34, 1, '103.66.73.15', 'Chrome', 'India', '1531899476'),
(35, 1, '184.164.146.5', 'Chrome', 'United States', '1536019200'),
(36, 1, '66.85.185.70', 'Chrome', 'United States', '1536105600'),
(37, 1, '66.85.185.70', 'Chrome', 'United States', '1536105600'),
(38, 1, '66.85.185.70', 'Chrome', 'United States', '1536105600'),
(39, 1, '66.85.185.70', 'Chrome', 'United States', '1536105600'),
(40, 1, '66.85.185.70', 'Chrome', 'United States', '1536105600'),
(41, 1, '66.85.185.70', 'Chrome', 'United States', '1536105600'),
(42, 1, '66.85.185.70', 'Chrome', 'United States', '1536105600'),
(43, 1, '184.164.146.2', 'Chrome', 'United States', '1536710400'),
(44, 1, '103.66.73.15', 'Chrome', 'India', '1537920000'),
(45, 1, '66.85.185.94', 'Chrome', 'United States', '1538352000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `houdin_admin_department`
--
ALTER TABLE `houdin_admin_department`
  ADD PRIMARY KEY (`houdin_admin_department_id`);

--
-- Indexes for table `houdin_admin_logo`
--
ALTER TABLE `houdin_admin_logo`
  ADD PRIMARY KEY (`houdin_admin_logo_id`);

--
-- Indexes for table `houdin_admin_transaction`
--
ALTER TABLE `houdin_admin_transaction`
  ADD PRIMARY KEY (`houdin_admin_transaction_id`);

--
-- Indexes for table `houdin_admin_users`
--
ALTER TABLE `houdin_admin_users`
  ADD PRIMARY KEY (`houdin_admin_user_id`);

--
-- Indexes for table `houdin_admin_user_second_auth`
--
ALTER TABLE `houdin_admin_user_second_auth`
  ADD PRIMARY KEY (`houdin_second_auth_id`);

--
-- Indexes for table `houdin_business_categories`
--
ALTER TABLE `houdin_business_categories`
  ADD PRIMARY KEY (`houdin_business_category_id`);

--
-- Indexes for table `houdin_countries`
--
ALTER TABLE `houdin_countries`
  ADD PRIMARY KEY (`houdin_country_id`);

--
-- Indexes for table `houdin_coupons`
--
ALTER TABLE `houdin_coupons`
  ADD PRIMARY KEY (`houdin_coupon_id`);

--
-- Indexes for table `houdin_currencies`
--
ALTER TABLE `houdin_currencies`
  ADD PRIMARY KEY (`houdin_currency_id`);

--
-- Indexes for table `houdin_deleted_users`
--
ALTER TABLE `houdin_deleted_users`
  ADD PRIMARY KEY (`houdin_deleted_user_id`);

--
-- Indexes for table `houdin_email_package`
--
ALTER TABLE `houdin_email_package`
  ADD PRIMARY KEY (`houdin_email_package_id`);

--
-- Indexes for table `houdin_languages`
--
ALTER TABLE `houdin_languages`
  ADD PRIMARY KEY (`houdin_language_id`);

--
-- Indexes for table `houdin_packages`
--
ALTER TABLE `houdin_packages`
  ADD PRIMARY KEY (`houdin_package_id`);

--
-- Indexes for table `houdin_package_plugins`
--
ALTER TABLE `houdin_package_plugins`
  ADD PRIMARY KEY (`houdin_package_plugin_id`);

--
-- Indexes for table `houdin_payumoney`
--
ALTER TABLE `houdin_payumoney`
  ADD PRIMARY KEY (`houdin_payumoney`);

--
-- Indexes for table `houdin_plugins`
--
ALTER TABLE `houdin_plugins`
  ADD PRIMARY KEY (`houdin_plugin_id`);

--
-- Indexes for table `houdin_products`
--
ALTER TABLE `houdin_products`
  ADD PRIMARY KEY (`houdin_products_id`);

--
-- Indexes for table `houdin_products_variants`
--
ALTER TABLE `houdin_products_variants`
  ADD PRIMARY KEY (`houdin_products_variants_id`);

--
-- Indexes for table `houdin_product_attributers`
--
ALTER TABLE `houdin_product_attributers`
  ADD PRIMARY KEY (`houdin_product_att_id`);

--
-- Indexes for table `houdin_product_types`
--
ALTER TABLE `houdin_product_types`
  ADD PRIMARY KEY (`houdin_product_type_id`);

--
-- Indexes for table `houdin_product_type_attributes`
--
ALTER TABLE `houdin_product_type_attributes`
  ADD PRIMARY KEY (`houdin_product_type_attribute_id`);

--
-- Indexes for table `houdin_roles`
--
ALTER TABLE `houdin_roles`
  ADD PRIMARY KEY (`houdin_role_id`);

--
-- Indexes for table `houdin_sendgrid`
--
ALTER TABLE `houdin_sendgrid`
  ADD PRIMARY KEY (`houdin_sendgrid_id`);

--
-- Indexes for table `houdin_sms_package`
--
ALTER TABLE `houdin_sms_package`
  ADD PRIMARY KEY (`houdin_sms_package_id`);

--
-- Indexes for table `houdin_tax`
--
ALTER TABLE `houdin_tax`
  ADD PRIMARY KEY (`houdin_tax_id`);

--
-- Indexes for table `houdin_templates`
--
ALTER TABLE `houdin_templates`
  ADD PRIMARY KEY (`houdin_template_id`);

--
-- Indexes for table `houdin_twilio`
--
ALTER TABLE `houdin_twilio`
  ADD PRIMARY KEY (`houdin_twilio_id`);

--
-- Indexes for table `houdin_users`
--
ALTER TABLE `houdin_users`
  ADD PRIMARY KEY (`houdin_user_id`),
  ADD UNIQUE KEY `houdin_user_email` (`houdin_user_email`);

--
-- Indexes for table `houdin_user_auth`
--
ALTER TABLE `houdin_user_auth`
  ADD PRIMARY KEY (`houdin_user_auth_id`);

--
-- Indexes for table `houdin_user_forgot_password`
--
ALTER TABLE `houdin_user_forgot_password`
  ADD PRIMARY KEY (`houdin_user_forgot_password_id`);

--
-- Indexes for table `houdin_user_logs`
--
ALTER TABLE `houdin_user_logs`
  ADD PRIMARY KEY (`houdin_user_log_id`);

--
-- Indexes for table `houdin_user_payments`
--
ALTER TABLE `houdin_user_payments`
  ADD PRIMARY KEY (`houdin_user_payment_id`);

--
-- Indexes for table `houdin_user_payment_tracks`
--
ALTER TABLE `houdin_user_payment_tracks`
  ADD PRIMARY KEY (`houdin_user_payment_track_id`);

--
-- Indexes for table `houdin_user_second_auth`
--
ALTER TABLE `houdin_user_second_auth`
  ADD PRIMARY KEY (`houdin_user_second_auth_id`);

--
-- Indexes for table `houdin_user_shops`
--
ALTER TABLE `houdin_user_shops`
  ADD PRIMARY KEY (`houdin_user_shop_id`);

--
-- Indexes for table `houdin_vendor_auth`
--
ALTER TABLE `houdin_vendor_auth`
  ADD PRIMARY KEY (`houdin_vendor_auth_id`);

--
-- Indexes for table `houdin_vendor_user_log`
--
ALTER TABLE `houdin_vendor_user_log`
  ADD PRIMARY KEY (`houdin_vendor_user_log_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `houdin_admin_department`
--
ALTER TABLE `houdin_admin_department`
  MODIFY `houdin_admin_department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `houdin_admin_logo`
--
ALTER TABLE `houdin_admin_logo`
  MODIFY `houdin_admin_logo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `houdin_admin_transaction`
--
ALTER TABLE `houdin_admin_transaction`
  MODIFY `houdin_admin_transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `houdin_admin_users`
--
ALTER TABLE `houdin_admin_users`
  MODIFY `houdin_admin_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `houdin_admin_user_second_auth`
--
ALTER TABLE `houdin_admin_user_second_auth`
  MODIFY `houdin_second_auth_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `houdin_business_categories`
--
ALTER TABLE `houdin_business_categories`
  MODIFY `houdin_business_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `houdin_countries`
--
ALTER TABLE `houdin_countries`
  MODIFY `houdin_country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `houdin_coupons`
--
ALTER TABLE `houdin_coupons`
  MODIFY `houdin_coupon_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `houdin_currencies`
--
ALTER TABLE `houdin_currencies`
  MODIFY `houdin_currency_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `houdin_deleted_users`
--
ALTER TABLE `houdin_deleted_users`
  MODIFY `houdin_deleted_user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `houdin_email_package`
--
ALTER TABLE `houdin_email_package`
  MODIFY `houdin_email_package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `houdin_languages`
--
ALTER TABLE `houdin_languages`
  MODIFY `houdin_language_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `houdin_packages`
--
ALTER TABLE `houdin_packages`
  MODIFY `houdin_package_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `houdin_package_plugins`
--
ALTER TABLE `houdin_package_plugins`
  MODIFY `houdin_package_plugin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `houdin_payumoney`
--
ALTER TABLE `houdin_payumoney`
  MODIFY `houdin_payumoney` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `houdin_plugins`
--
ALTER TABLE `houdin_plugins`
  MODIFY `houdin_plugin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `houdin_products`
--
ALTER TABLE `houdin_products`
  MODIFY `houdin_products_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `houdin_products_variants`
--
ALTER TABLE `houdin_products_variants`
  MODIFY `houdin_products_variants_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `houdin_product_attributers`
--
ALTER TABLE `houdin_product_attributers`
  MODIFY `houdin_product_att_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `houdin_product_types`
--
ALTER TABLE `houdin_product_types`
  MODIFY `houdin_product_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `houdin_product_type_attributes`
--
ALTER TABLE `houdin_product_type_attributes`
  MODIFY `houdin_product_type_attribute_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `houdin_sendgrid`
--
ALTER TABLE `houdin_sendgrid`
  MODIFY `houdin_sendgrid_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `houdin_sms_package`
--
ALTER TABLE `houdin_sms_package`
  MODIFY `houdin_sms_package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `houdin_tax`
--
ALTER TABLE `houdin_tax`
  MODIFY `houdin_tax_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `houdin_templates`
--
ALTER TABLE `houdin_templates`
  MODIFY `houdin_template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `houdin_twilio`
--
ALTER TABLE `houdin_twilio`
  MODIFY `houdin_twilio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `houdin_users`
--
ALTER TABLE `houdin_users`
  MODIFY `houdin_user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `houdin_user_auth`
--
ALTER TABLE `houdin_user_auth`
  MODIFY `houdin_user_auth_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `houdin_user_forgot_password`
--
ALTER TABLE `houdin_user_forgot_password`
  MODIFY `houdin_user_forgot_password_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `houdin_user_logs`
--
ALTER TABLE `houdin_user_logs`
  MODIFY `houdin_user_log_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `houdin_user_second_auth`
--
ALTER TABLE `houdin_user_second_auth`
  MODIFY `houdin_user_second_auth_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `houdin_user_shops`
--
ALTER TABLE `houdin_user_shops`
  MODIFY `houdin_user_shop_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `houdin_vendor_auth`
--
ALTER TABLE `houdin_vendor_auth`
  MODIFY `houdin_vendor_auth_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `houdin_vendor_user_log`
--
ALTER TABLE `houdin_vendor_user_log`
  MODIFY `houdin_vendor_user_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
