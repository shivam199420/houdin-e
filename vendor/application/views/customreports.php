<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); 
$getCurrency = getVendorCurrency();
if($getCurrency[0]->houdin_users_currency=="USD")
{
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="AUD"){
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="Euro"){
	$currencysymbol= "£";
}else if($getCurrency[0]->houdin_users_currency=="Pound"){
	$currencysymbol= "€";
}else if($getCurrency[0]->houdin_users_currency=="INR"){
	$currencysymbol= "₹";
}
?>
<style type="text/css">
	.heading_content ul{list-style: none;    padding-left: 0px;}
	.heading_content ul li a{    padding: 7px;
    display: block;}
	.heading_content>ul>li>ul>li> a{ padding-left: 20px;}


	.border-t{border-top: 1px solid #ddd;}
	.border-b{border-bottom: 1px solid #ddd;}
	.text-gray{color: #797979;}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">

                <div class="col-md-12">
                   <h4 class="page-title">Custom Report</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>Accounts">Accounts</a></li>
                  <li class="active">Custom Report Summory</li>
                  </ol>
                  </div>
            </div>
            <div class="row m-t-20">
           				<div class="col-sm-12">
                            				<div class="col-md-4">
												<div class="form-group m-r-10">
												<label for="exampleInputEmail2">From</label>
													<input type="text" class="form-control date_picker commonChangeClassData dateFromText" value="<?php echo $from ?>" id="" placeholder="">
												</div>
												</div>
												<div class="col-md-4">
												<div class="form-group m-r-10">
													<label for="exampleInputEmail2">To</label>
													<input type="text" class="form-control date_picker commonChangeClassData dateToText" value="<?php echo $to ?>" id="" >
												</div>
												</div>

                            			</div>
                      </div>
                      <div class="m-t-40 ">
                      <div class="col-md-12 bg-white">
                      <div class="col-md-2"></div>
                      <div class="col-lg-8 m-t-30">
								<div class="panel panel-default" style="border: 1px solid #ddd;">
									<div class="panel-heading pull-left" style="width: 100%;">
										<h3 class="panel-title pull-left">Custom Summary Report</h3>
										<div class="icon-right pull-right">
										<a href="<?php echo base_url(); ?>Accounts/custombalancesheetpdf/<?php echo $from ?>/<?php echo $to ?>" class="btn btn-default setPdfUrl"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>

										</div>
									</div>
									<div class="panel-body">
									<div class="heading_sheet text-center">
									<?php $getVendorInfo = fetchvendorinfo(); ?>
									<h4><?php echo $getVendorInfo['name']; ?></h4>
									<h5><strong>Custom Summary Report</strong></h5>
									<p class="setReportsdate"><?php echo $from ." - ".$to ?></p>
									</div>
									<div class="heading_content">
									<ul class="">
									<li class=" border-b border-t pull-left" style="width: 100%;"><a class="text-gray pull-right"><strong>TOTAL</strong></a></li></ul>
									<ul class="">
									<li><a class="text-gray">Income</a>
										<ul class="">
											<li><a class="text-gray">Service</a>

											</li>
										</ul>
									</li>
									<li class="border-b border-t"> <a class="text-gray"><strong><span>Total Income</span><span class="pull-right setToatlIncome"><?php echo $currencysymbol; ?><?php echo $credit; ?></span></strong></a></li>
									<li class="border-b"></li>
									<li><a class="text-gray">Gross Profit</a>

									</li>
									<li><a class="text-gray">Expenses</a>
									<ul>
									<li><a class="text-gray">Depreciation Expense</a></li>
									</ul>
									</li>
									<li class="border-b border-t"><a class="text-gray"><strong><span class="">Total Expense</span><span class="pull-right setToatlDebitIncome"><?php echo $currencysymbol; ?><?php echo $debit ?></span></strong></a></li>
									<li class="border-b"></li>
									<li class="border-b"><a class="text-gray"><strong><span class="">Profit</span><span class="pull-right setFinalIncome">
									<?php
									$gteFinalPayment = $credit-$debit;
									?>
									<?php echo $currencysymbol; ?><?php echo $gteFinalPayment; ?></span></strong></a></li>
									<li class="border-b"></li>
									</ul>
									</div>
									</div>
								</div>
							</div>
							<div class="col-md-2"></div>
                      </div>
                      </div>
                    </div>
                  </div>
 <?php $this->load->view('Template/footer.php') ?>

 <script type="text/javascript">
		$(document).ready(function(){
			var setBaseData = "<?php echo base_url(); ?>";
			$(document).on('change','.commonChangeClassData',function(){
				$('.setErrorMessage').html('');
				var fromdate = $('.dateFromText').val();
				var toDate = $('.dateToText').val();
				var setPDFUrl = setBaseData+"Accounts/custombalancesheetpdf/"+fromdate+"/"+toDate;
				$('.setPdfUrl').attr('href',setPDFUrl);
				jQuery.ajax({
                type: "POST",
                url: setBaseData+"Accounts/updateCustomReports",
                data: {"fromdate": fromdate,"toDate": toDate},
                success: function(data) {
					var getFinalData = $.parseJSON(data);
					var fromDate = getFinalData.from;
					var toDate = getFinalData.to;
					var finalIncome = parseFloat(getFinalData.credit)-parseFloat(getFinalData.debit)
					$('.setReportsdate').text('').text(fromDate+" - "+toDate);
					$('.setToatlIncome').text('').text(getFinalData.credit);
					$('.setToatlDebitIncome').text('').text(getFinalData.debit);
					$('.setFinalIncome').text('').text(finalIncome)
                }
                });
			})

		})
		</script>
