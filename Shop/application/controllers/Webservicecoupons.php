<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Webservicecoupons extends CI_Controller
{
    function __construct()
	{
        parent::__construct();      
        $this->load->model('Shopuserordermodel');     
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }           
    }
    public function index()
    {
        $this->form_validation->set_rules('couponCode','text','required');
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('totalAmount','text','required');
        $this->form_validation->set_rules('customerId','text','required');
        if($this->form_validation->run() == true)
        {
            $setCouponArray = array('code'=>$this->input->post('couponCode'),'shopName'=>$this->input->post('shopName'),'totalAmount'=>$this->input->post('totalAmount'),'customerId'=>$this->input->post('customerId'));
            $isexist = $this->Shopuserordermodel->IsCouponExist($setCouponArray);  
            print_r(json_encode($isexist));
        }
        else
        {
            $resultData = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultData));
        }
         
    }
 }    