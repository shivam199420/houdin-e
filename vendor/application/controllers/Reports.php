<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends CI_Controller 
{
    function __construct() 
    {
        $this->auth_users->is_logged_in();
        parent::__construct();
    }
    public function index()
    {
        $this->load->view('reportsdata');
    }
}
