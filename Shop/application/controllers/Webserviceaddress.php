<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Webserviceaddress extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopuseraddress');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function fetchUserCountry()
    { 
        $this->form_validation->set_rules('shopName','Country','required');
        if($this->form_validation->run() == true)
        {
            $getCountryList = $this->Shopuseraddress->fetchUserCounrtyApp($this->input->post('shopName'));
            print_r(json_encode($getCountryList));
        }
        else
        {
            $resultArray = array('messgae'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function adduseraddressapp()
    {
        $this->form_validation->set_rules('username','Name','required');
        $this->form_validation->set_rules('userphone','Phone','required');
        $this->form_validation->set_rules('usermainaddress','Address','required');
        $this->form_validation->set_rules('usercity','City / Town','required');
        $this->form_validation->set_rules('userpincode','Pin Code','required');
        $this->form_validation->set_rules('usercountry','Country','required');
        $this->form_validation->set_rules('userid','Country','required');
        $this->form_validation->set_rules('shopName','Country','required');
        if($this->form_validation->run() == true)
        {
            $setData = strtotime(date('Y-m-d h:i:s'));
            $setInsertArray = array('houdinv_user_address_user_id'=>$this->input->post('userid'),'houdinv_user_address_name'=>$this->input->post('username'),
            'houdinv_user_address_phone'=>$this->input->post('userphone'),'houdinv_user_address_user_address'=>$this->input->post('usermainaddress'),'houdinv_user_address_city'=>$this->input->post('usercity'),
            'houdinv_user_address_zip'=>$this->input->post('userpincode'),'houdinv_user_address_country'=>$this->input->post('usercountry'),'houdinv_user_address_created_at'=>$setData
            );
            $setExtraData = array('shopName'=>$this->input->post('shopName'));
            $getInsertStatus = $this->Shopuseraddress->addUserAddressDataApp($setInsertArray,$setExtraData);
            if($getInsertStatus['message'] == 'yes')
            {
                $resultArray = array('messgae'=>'Address is addedd successfully');
                print_r(json_encode($resultArray));
            }
            else
            {
                $resultArray = array('messgae'=>'Something went wrong. Please try again');
                print_r(json_encode($resultArray));
            }
        }
        else
        {
            $resultArray = array('messgae'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
    public function fetchUserAddress()
    {
        $this->form_validation->set_rules('userid','Country','required');
        $this->form_validation->set_rules('shopName','Country','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('shopName'=>$this->input->post('shopName'),'id'=>$this->input->post('userid'));
            $getAddressdata = $this->Shopuseraddress->fetchUserAddressApp($setArray);
            print_r(json_encode($getAddressdata));
        }
        else
        {
            $resultArray = array('messgae'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
    public function deleteUserAddress()
    {
        $this->form_validation->set_rules('addressId','Country','required');
        $this->form_validation->set_rules('shopName','Country','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('shopName'=>$this->input->post('shopName'),'id'=>$this->input->post('addressId'));
            $getDelete = $this->Shopuseraddress->deleteUserAddressApp($setArray);
            print_r(json_encode($getDelete));
        }
        else
        {
            $resultArray = array('messgae'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
    public function updateUserAddress()
    {
        $this->form_validation->set_rules('username','Name','required');
        $this->form_validation->set_rules('userphone','Phone','required');
        $this->form_validation->set_rules('usermainaddress','Address','required');
        $this->form_validation->set_rules('usercity','City / Town','required');
        $this->form_validation->set_rules('userpincode','Pin Code','required');
        $this->form_validation->set_rules('usercountry','Country','required');
        $this->form_validation->set_rules('addressId','Country','required');
        $this->form_validation->set_rules('shopName','Country','required');
        if($this->form_validation->run() == true)
        {
            $setData = strtotime(date('Y-m-d h:i:s'));
            $setInsertArray = array('houdinv_user_address_name'=>$this->input->post('username'),
            'houdinv_user_address_phone'=>$this->input->post('userphone'),'houdinv_user_address_user_address'=>$this->input->post('usermainaddress'),'houdinv_user_address_city'=>$this->input->post('usercity'),
            'houdinv_user_address_zip'=>$this->input->post('userpincode'),'houdinv_user_address_country'=>$this->input->post('usercountry'),'houdinv_user_address_modified_at'=>$setData
            );
            $setExtraData = array('shopName'=>$this->input->post('shopName'),'addressId'=>$this->input->post('addressId'));
            $getUpdateStatus = $this->Shopuseraddress->updateUserAddressApp($setInsertArray,$setExtraData);
            print_r(json_encode($getUpdateStatus));
        }
        else
        {
            $resultArray = array('messgae'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
}
