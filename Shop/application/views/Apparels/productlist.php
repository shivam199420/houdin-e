        <?php
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view("".$getShopName."/Template/header"); ?>

        <?php
        $getCurrency = getShopCurrency();
        $currencysymbol=$getCurrency[0]->houdin_users_currency;
        ?>
        <!-- Page item Area -->
        <div id="page_item_area">
        <div class="container">
        <div class="row">
        <div class="col-sm-6 text-left">
        <h3>Products</h3>
        </div>
        </div>
                    <div class="row">
        <div class="container">
        <div class="col-sm-12">
        <?php
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>
        </div>
        </div>
        </div>


        <!-- Shop Product Area -->
        <div class="shop_page_area">
          <?php
          $count_product=count($productList);
          if($count_product==0)
          {?>
            <div class="container"><div class="col-sm-12 text-center"><h3>Products are not available</h3></div></div>
          <?php } else { ?>
        <div class="container">
        <div class="shop_bar_tp fix">
        <div class="row">
        <div class="col-sm-6 col-xs-12 short_by_area">
        <div class="short_by_inner">
        <label>short by:</label>
        <?php echo form_open(base_url( 'Productlist/'.$this->uri->segment('2').'/'.$this->uri->segment('3').'/'.$this->uri->segment('4')), array( 'id' => 'Productlist', 'method'=>'get' ));?>

        <select class="sort-select" name="sort">
        <option value="">select</option>
        <option value="name" <?php if($_REQUEST['sort'] =="name") { echo "selected"; }?> >Name Ascending</option>
        <option value="date" <?php if($_REQUEST['sort'] =="date") { echo "selected"; }?>>Date Ascending</option>
        <option value="pricehl" <?php if($_REQUEST['sort'] =="pricehl") { echo "selected"; }?>>Price High to Low</option>
        <option value="pricelh" <?php if($_REQUEST['sort'] =="pricelh") { echo "selected"; }?>>Price Low to High</option>
        </select>

         <?php echo form_close(); ?>
        </div>
        </div>


        </div>
        </div>

        <div class="shop_details text-center">
        <div class="row appendProductList">
            <?php
            foreach($productList as $productListData)
            {
                $getProductImage = json_decode($productListData->houdinv_products_main_images,true);
                $getProductPrice = json_decode($productListData->houdin_products_price,true);
            ?>
            <div class="col-md-3 col-sm-6">
            <div class="single_product">
            <div class="product_image">
            <?php 
            if($getProductImage[0])
            {
              $setProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
            }
            else
            {
              $setProductImage = base_url()."Extra/noPhotoFound.png";
            }
            ?>
            <img src="<?php echo $setProductImage; ?>" alt="" style="width: 250px!important; height: 300px!important;"/>
            <?php if($productListData->houdinv_products_total_stocks <= 0) { ?><div class="new_badge">Out of Stock</div><?php } ?>
             <?php
if($productListData->houdinv_products_main_quotation == 1)
{
?>
    <div class="model_qute_show" data-id="<?php echo $productListData->houdin_products_id ?>"  data-toggle="modal" style="position: absolute;display: inline-block;background: #33d286;color: #fff;z-index: 99;top: 15px;left: 8px;padding: 3px 12px;border-radius: 2px;cursor: pointer;">Ask Quotation
</div>
<?php
}
?>

            <div class="box-content">
            <a href="javascript:;" class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>" ><i class="fa fa-heart-o"></i></a>
            <?php
            if($productListData->houdinv_products_total_stocks <= 0)
            {
            if($storeSetting[0]->receieve_order_out == 1)
            {
                ?>
                <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-cart-plus"></i></a>
            <?php  }
            }
            else
            {
            ?>
                <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-cart-plus"></i></a>
            <?php }
            ?>

            <a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-search"></i></a>
            </div>
            </div>

            <div class="product_btm_text">
            <h4><a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>"><?php echo substr($productListData->houdin_products_title,0, 25).".." ; ?></a></h4>
            <span class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $productListData->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $productListData->houdin_products_final_price; ?> <?php  } ?></span>
            </div>
            </div>
            </div>
            <?php }
            ?>

        </div>
        </div>
        <?php
        if($count > 0)
        {
            if($count >= 12)
            {
                $remain = $count-12;
            }
            else
            {
                $remain = 0;
            }
        ?>
        <div class="row">
        <div class="col-sm-2"><button class="btn btn-sm btn-success" style="width:100%;margin-bottom: 10px;">Total Product:<?php echo $count; ?></button></div>
        <?php
        if($remain > 0)
        {
        ?>
        <div class="col-sm-10"><button type="button" data-remain="<?php echo $remain ?>" data-last="12" class="btn btn-sm btn-default acc_btn setLoadMoredata" style="width:100%;margin-top: 0px;padding: 4px;border-radius: 4px;margin-bottom: 10px;">Load More&nbsp;<i class="fa fa-spinner fa-spin setSpinnerData" style="display:none;"></i></button></div>
        <?php }
        ?>
        </div>
        <?php }
        ?>
        </div>
      <?php } ?>
        </div>
        <?php
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view("".$getShopName."/Template/footer"); ?>

        <script>
        $('.sort-select').on('change',function()
        {
          document.getElementById('Productlist').submit();
        });
        </script>
        <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('click','.setLoadMoredata',function(){
                $(this).prop('disabled',true);
                $('.setSpinnerData').show();
                var getCategory = '<?php echo $this->uri->segment('2') ?>';
                var getSubCategory = '<?php echo  $this->uri->segment('3') ?>';
                var getSubSubCategory = '<?php echo $this->uri->segment('4') ?>';
                var sortBy = '<?php echo $_REQUEST['sort'] ?>';
                var dataLast = $(this).attr('data-last');
                var dataRemain = $(this).attr('data-remain');
                $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>Ajaxcontroller/fetchProductPaginationData",
                data: {"dataLast": dataLast,"dataRemain":dataRemain,"getCategory":getCategory,"getSubCategory":getSubCategory,"getSubSubCategory":getSubSubCategory,"sortBy":sortBy},
                success: function(datas) {
                    $('.setSpinnerData').hide();
                    $('.setLoadMoredata').prop('disabled',false);
                    var setData = jQuery.parseJSON(datas);

                       var grid = "";
                    for(var i=0; i < setData.main.length ; i++)
                    {
                        var $productListData = setData.main[i];
                        var textsplit = $productListData.houdin_products_title;
                        var $setCartDesign = "";
                          var $setStockBadge = "";
                          var $settingData = setData.settingData;
                            if($productListData.houdinv_products_total_stocks <= 0)
                                {
                                    if($settingData[0].receieve_order_out == 1)
                                    {
                                        $setCartDesign = '<a class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><i class="fa fa-cart-plus"></i></a>';
                                    }
                                }
                                else
                                {
                                    $setCartDesign = '<a class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><i class="fa fa-cart-plus"></i></a>';
                                }
                                if($productListData.houdinv_products_total_stocks <= 0)
                                {
                                    $setStockBadge = '<div class="new_badge">Out of Stock</div>';
                                }

                          var   $getProductImage = jQuery.parseJSON($productListData.houdinv_products_main_images);
                           var     $getProductPrice = jQuery.parseJSON($productListData.houdin_products_price);
                              console.log($getProductPrice);
                                if($getProductPrice.discount != 0 && $getProductPrice.discount  != "")
                                {
                                   var $DiscountedPrice = $getProductPrice.price-$getProductPrice.discount;
                                   var $originalPrice = $getProductPrice.price;
                                }
                                else
                                {
                                  var  $DiscountedPrice = 0;
                                  var  $originalPrice = $getProductPrice.price;
                                }
                                if($DiscountedPrice == 0)
                                {
                                  var  $setPrice = $originalPrice;
                                }
                                else
                                {
                                   var $setPrice = "<strike>"+$originalPrice+"</strike>&nbsp;"+$DiscountedPrice+"";
                                }
                                if($getProductImage[0])
                                {
                                  var setIMageData = '<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/'+$getProductImage[0]+'';
                                }
                                else
                                {
                                  var setIMageData = '<?php echo base_url() ?>Extra/noPhotoFound.png';
                                }
                       grid+='<div class="col-md-3 col-sm-6">'
                            +'<div class="single_product">'
                            +'<div class="product_image">'
                            +'<img src="'+setIMageData+'" alt="" style="width: 250px!important; height: 300px!important;"/>'
                            +$setStockBadge
                            +'<div class="box-content">'
                            +'<a href="javascript:;" class="Add_to_whishlist_button" data-cart="'+$productListData.houdin_products_id+'" ><i class="fa fa-heart-o"></i></a>'
                             +$setCartDesign

                            +'<a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'"><i class="fa fa-search"></i></a>'
                            +'</div>'
                            +'</div>'
                              +'<div class="product_btm_text">'
                            +'<h4><a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'">'+textsplit.substring(0,25)+'</a></h4>'
                            +'<span class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;'+$setPrice+'</span>'
                            +'</div>'
                            +'</div>'
                            +'</div>';

                    }
                    $('.appendProductList').append(grid);


                    $('.setLoadMoredata').attr('data-last',setData.last);
                }
                });
            });
        });
        </script>
