<?php $this->load->view('Template/header') ?>
    <?php $this->load->view('Template/possidebar') ?>
     <style type="text/css">
         .m-t-0-btn
         {
            margin: 0px !important;
         }
          .radio.radio-inline {
        margin-top: 0;
        margin-left: 0px;
        }
        label.btn-primary.active{
            background-color: #964460 !important;  
            border: 1px solid #964460 !important;  
        }
        .btn-default.active
        {
            background: #fff !important;
            color: #000 !important;
        }
        </style>

<style type="text/css">
                 .section_order_pos_content{
                    display: flex;
                    border-top: 1px solid #5079ad !important;
                }
                
                .section_order_pos_content .leftContent_pos_ordercontent{
                    border-right: 1px solid #5079ad !important;
                    padding: 0px;
                }
                .section_order_pos_content .leftContent_pos_ordercontent_table{

                }
                .section_order_pos_content .leftContent_pos_ordercontent_table thead{
                    background-color: #5079ad !important;
                    font-size: 14px;
                }
                .section_order_pos_content .leftContent_pos_ordercontent_table thead th{
                    font-size: 12px;
                    color: #f4f8fb;
                    height: 70px;
                    vertical-align: middle;
                    text-align: center;
                    border: 1px solid #fff;
                }
            .section_order_pos_content .leftContent_pos_ordercontent_table tbody{}
            .section_order_pos_content .leftContent_pos_ordercontent_table tbody td{}
            .section_order_pos_content .leftContent_pos_ordercontent_table td,
            .section_order_pos_content .leftContent_pos_ordercontent_table th{}
            .section_order_pos_content .sidebar_pos_ordercontent{
                padding:0px;
                margin: 0px;
                padding-bottom: 150px;
                position: relative;
            }
            .section_order_pos_content .sidebar_pos_ordercontent .sidebar_pos_ordercontent_footer{
                position: absolute;
                left: 0px;
                right: 0px;
                bottom: 0px;
                background-color: #f5f9fb;
                padding: 10px;
            }
                .section_order_pos_payment_sidebar_table{
                        background-color: #fff;
                        margin-bottom: 0px;
                }
                .section_order_pos_payment_sidebar_table_label{
                    text-align: right;
                    width: 50%;
                }
                .section_order_pos_payment_sidebar_table_value{
                    text-align: right;
                }
                .section_order_pos_payment_sidebar_table td,
                .section_order_pos_payment_sidebar_table th{
                    border: 0px;
                    color: #000;
                    border-top:0px !important;
                    border-bottom: 1px solid #fff !important;
                    padding: 8px 15px!important;
                    font-size: 13px;
                }
                 .section_order_pos_payment_system_table{
                            width: 100%;
                                position: absolute;
    bottom: 0;
    box-shadow: 0px 0px 11px -3px;
                    }
                    .section_order_pos_payment_system_table th,
                    .section_order_pos_payment_system_table td{
                        padding: 10px;
                        color: #000;
                        font-size: 13px;
                    } 
                     .section_order_pos_payment_system_table th{}
                     .section_order_pos_payment_system_table td{}
                     .order-checkout-payment-method{
                            width: 100%;
                            float: right;
                            margin-bottom: 20px;
                           
                        }
                        .order-checkout-payment-method .order-checkout-payment-box-row-item{
                          float: left;
                          width: 100%; 
                          border-bottom: 1px solid #efefef;
                          margin: 0px; 
                        }
                        
                        .order-checkout-payment-method .order-checkout-payment-box-row-item .order-checkout-payment-box-row-content{
                          float: left;
                          width: 100%;
                          padding: 15px;  
                          font-size: 14px;
                          display:none;
                        }
                        .order-checkout-payment-method .order-checkout-payment-box-row{
                            float: left;
                            width: 50%;
                            padding: 15px 0px;
                            border-bottom: 1px solid #5079ad !important;
                            position: relative;
                            margin: 0px;
                            cursor: pointer;
                                border: 1px solid #ddd;
                        }
                        
                        .order-checkout-payment-method .order-checkout-payment-box-row input[type="radio"]{
                         display: none;
                        } 
                       
                      
                        .order-checkout-payment-method .order-checkout-payment-box-row input[type="radio"]:checked {
                          border: none;
                          -webkit-box-shadow: 0 0 0 10px #244667 inset;
                          box-shadow: 0 0 0 10px #244667 inset;
                        }
                        .order-checkout-payment-method .order-checkout-payment-box-row input[type="radio"]:checked + .order-checkout-payment-box-row-title{
                          color: #244667    ;
                          font-weight: bold;    
                        }
                        
                          .order-checkout-payment-method .order-checkout-payment-box-row input[type="radio"]:checked + .order-checkout-payment-box-row-title:before{
                            background: #e3f0fd;
                          }
                        .order-checkout-payment-method .order-checkout-payment-box-row-title{
                          font-weight: normal;
                          font-size: 14px;
                          margin-left: 10px;
                        }
                        .order-checkout-payment-method .order-checkout-payment-box-row-icon{
                             z-index: 999999;
    width: 24px;margin-right: 10px;
                        }
                          .order-checkout-payment-method-heading{
                            white-space: nowrap;
                            vertical-align: top;
                            padding: 10px;
                            color: #f4f8fb;
                            font-weight: bold;
                            background: #5079ad !important;
                          }
                            .order-checkout-payment-method .order-checkout-payment-box-row-title:before{
                                content: '';
                                background: #fff;
                                left: 0px;
                                right: 0px;
                                top: 0px;
                                bottom: 0px;
                                z-index: 0;
                          }
                            .order-checkout-payment-method .order-checkout-payment-box-row-title span{
                                position: relative;
                                z-index: 1;
                                font-size: 13px;
                           }
                          .mainParentRow{
                                background-color: rgb(245, 249, 251);
                          }

                          .pending_amount_display{
                                padding: 10px;
                                float: left;
                                width: 100%;
                                font-weight: bold;
                                color: #000000;
                                font-size: 16px;
                                margin-bottom: 10px;
                          }
                          .pending_amount_display label{
                             font-size: 13px;
                          }
                          .bill-subhead{    font-size: 21px;
    padding: 10px;}
    .btn-group-custom>.btn:first-child:not(:last-child):not(.dropdown-toggle) {
    border-top-left-radius: 14px;
    border-bottom-left-radius: 14px;
}
.btn-group-custom>.btn:not(:first-child):not(:last-child):not(.dropdown-toggle) {
    border-radius: 0;
    border-top-right-radius: 14px;
    border-bottom-right-radius: 14px;
}
.btn-group-custom>.btn:last-child:not(:first-child), .btn-group>.dropdown-toggle:not(:first-child) {
    border-top-left-radius: 14px;
    border-bottom-left-radius: 14px;
    border-top-right-radius: 14px;
    border-bottom-right-radius: 14px;
}
.registerdCustomerData{float: left;width: 100%;}
.tab-content{padding: 30px 8px;}
.custom-cust< span{width: 100% !important;}

 
     </style>
     <link href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container"> 
        <!--START Page-Title -->
        <div class="row">
        <div class="col-md-8">
                   <h4 class="page-title">Workorder</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">POS</a></li>
                  <li class="active">Workorder</li>
                  </ol>
                  </div>
        </div>

        <?php echo form_open(base_url( 'Workorder/saveworkorder' ), array( 'id' => 'submitMainForm', 'method'=>'post' ));?>
        <?php     
         if($this->session->flashdata('message_name'))
            {
         echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?> 
    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    ?>
        <div class="alert alert-danger setErrorMessage" style="display:none;"></div>
        <div class="alert alert-success setSucccessMessage" style="display:none;"></div>
        <div class="row m-b-20 m-t-20">
            <span class="bill-subhead">Workorder Create</span>
            <div class="btn-group btn-group-custom" data-toggle="btns">
  <a class="btn btn-default pos_type_btn active m-t-0-btn deliveryTypeData" href="#first" data-toggle="tab" data-value="0">Cash N Carry</a>
  <a class="btn btn-default pos_type_btn m-t-0-btn deliveryTypeData" href="#second" data-toggle="tab" data-value="1">Home Delivery</a>
   <a class="btn btn-default pos_type_btn"  id="addCustomerbtntext" href="#third" data-toggle="tab" data-value="2" style="margin-top: 0px !important;">Add Customer</a>
</div>

<!-- Tab panes -->

        </div>



        <div class="section_order_pos_content"> 
     
        <div class="col-md-9 leftContent_pos_ordercontent">
        
       <div class="tab-content">
  <div class="tab-pane active" id="first">
      

      
        
  
      <div class="table-responsive">
        <table class="table leftContent_pos_ordercontent_table able-bordered table_shop_custom">
        <thead>
        <tr> 
         <th style="width:30%">Product Detail</th>
         <th style="width:8%">MRP</th>
         <th style="width:8%">CP</th>
        <th style="width:8%">SP</th>
        <th style="width:8%">Dis.P.</th>
        <th style="width:8%">Tax</th>
        <th style="width:8%">Qty</th>
        <th style="width:8%">Total</th>
        </tr>
        </thead>
        <tbody class="setRowData">
        <tr class="mainParentRow">
        <input type="hidden" class="inputProdcutId" name="inputProdcutId[]">
        <input type="hidden" class="inputVariantId" name="inputVariantId[]">
        <input type="hidden" class="inputQuantity" name="inputQuantity[]">
        <input type="hidden" class="inputActualPrice" name="inputActualPrice[]">
        <input type="hidden" class="inputDiscountedPrice" name="inputDiscountedPrice[]">
        <input type="hidden" class="inputTotalPrice" name="inputTotalPrice[]">
        <input type="hidden" class="inputtotaltax" name="inputtotaltax[]">
        <td>
            <div class="form-group" style="margin-bottom: 0px!important;">
         <?php 
          // print_r($getProductData['productData']);
         ?>
            <select class="form-control setproductData select1" tabindex="14">
            <option value="">Choose Product</option>
            <?php 
                foreach($getProductData['productData'] as $productDataList)
                {
            ?>
                <option data-price="<?php echo $productDataList['productPrice'] ?>" data-stock="<?php echo $productDataList['stock'] ?>" 
                data-variant="<?php echo $productDataList['variantId'] ?>" 
                data-product="<?php echo $productDataList['produtcId'] ?>" 
                data-tax="<?php echo $productDataList['tax_price'] ?>" data-cp="<?php echo $productDataList['cp'] ?>" data-taxprice="<?php echo $productDataList['tax_pricedata']; ?>" data-mrp="<?php echo $productDataList['mrp'] ?>" ><?php echo $productDataList['productName'] ?></option>
            <?php 
            }
            ?>
        </select>

              </div>
    </td>
    <td class="mrp"></td>
    <td class="cp"></td>
        <td class="setMainPrice"></td>
        <td><input type="text" value="" class="form-control specialPrice number_validation" name="" placeholder=""></td>
        <td class="tax"></td>
        <td><input type="text" value="" class="form-control getQuantity number_validation" name="" placeholder=""></td>
        <td class="setFinalPrice">0</td>
        </tr>
        </tbody>
        </table>
        <div class="pull-right">
    <button type="button" class="btn btn-default addnewproductRow" style="border-radius: 14px;">Add New Product</button>
</div>
        </div>
  </div>
  <div class="tab-pane" id="second">
      <div class="row form-horizontal">
          <div class="form-group setAddressData" >
        <label class="col-md-3 control-label">Choose Address</label>
        <div class="col-md-9">
        <select class="form-control setRegisteredCustomerAddress requiredValidationForRegisteredAddress" tabindex="9" name="customer_address">
        
        </select>
        </div>
        </div>
        <div class="form-group setAddressData">
        <div class="col-sm-12">
            <label class="pull-right addRegistredUserAddress" style="cursor:pointer">Add Another Address</label></div>
        </div>
      </div>
  </div>
  <div class="tab-pane" id="third">
    <div class="row form-horizontal">
      <div class="form-group" style="display:none">
        <label class="col-md-3 control-label">Customer Type</label>
        <div class="col-md-4">
        <input type="radio" tabindex="5" id="inlineRadio3" class="checkCustomerTypeData" value="RC" name="customerType" checked="">&nbsp;&nbsp;Registered
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4">
        <input type="radio" id="inlineRadio4" class="checkCustomerTypeData"  tabindex="6" value="NRC" name="customerType">&nbsp;&nbsp;Not Registered
        </div>
         
        </div>
        <!-- Registered customer -->
        <div class="registerdCustomerData">
        <div class="form-group">
        <label class="col-md-3 control-label">Customer Name</label>
        <div class="col-md-9">
        <select class="form-control select1 custom-cust selectCustomerData requiredValidationForCustomerName" tabindex="7" name="registeredCustomerId"> 
        <option value="">Choose one</option>
<?php foreach($getCustomerData['customerList'] as $value)
{
   ?>
 
      
         <option value="<?php echo $value->houdinv_user_id; ?>" data-name="<?=$value->houdinv_user_name;?>" data-contact="<?php echo $value->houdinv_user_contact; ?>"><?php echo $value->houdinv_user_name; ?></option>
                                                <?php 
}?>
                                           
                      </select>
        </div>


        </div>

        <div class="form-group">
        <label class="col-md-3 control-label">Contact No.</label>
        <div class="col-md-9">
        <input type="text" value="" tabindex="8" readonly="readonly" class="form-control setCustomerContact" name="customer_phone" placeholder="">
        </div>
        </div>
        

        </div>
        <!-- Non registred customer -->
        <div class="nonRegisterdCustomerData" style="display:none;"> 
        <div class="form-group">
        <label class="col-md-3 control-label">Customer Name</label>
        <div class="col-md-9">
        <input type="text" tabindex="10" class="form-control requiredValidationForNonRegisteredUser" name="setNonRegisteredCustomerName" placeholder="Customer Name">
        </div>


        </div>

        <div class="form-group">
        <input type="hidden" class="setNonRegisteredCustomerAddress" name="setNonRegisteredCustomerAddress"/>
        <label class="col-md-3 control-label">Contact No.</label>
        <div class="col-md-9">
        <input type="text" tabindex="11" class="form-control requiredValidationForNonRegisteredUser" name="setNonRegisteredCustomerContact" placeholder="Customer Contact">
        </div>
        </div>
       
        </div>
</div>
</div>
       
    

        
        </div>
    </div>
        <div class="col-md-3 sidebar_pos_ordercontent">
        


       
        <table class="table able-striped section_order_pos_payment_sidebar_table">
            <tbody><tr>
                <td class="section_order_pos_payment_sidebar_table_label">Total sales</td>
                <td class="section_order_pos_payment_sidebar_table_value"><input type="hidden" class="finalAmountData" name="finalAmountData"><div class="setFinalTotal">0</div></td>
            </tr>
           

             <tr>
                <td class="section_order_pos_payment_sidebar_table_label">Discount</td>
                <td class="section_order_pos_payment_sidebar_table_value"><input type="hidden" class="finalDiscountData" name="finalDiscountData"><div class="setCouponDiscount">0</div></td>
            </tr>
              <tr>
                <td class="section_order_pos_payment_sidebar_table_label">Tax</td>
                <td class="section_order_pos_payment_sidebar_table_value"><input type="hidden" class="finalDiscountData" name="finalDiscountData"><div class="setCouponDiscount">0</div></td>
            </tr>
             <tr>
                <td class="section_order_pos_payment_sidebar_table_label">Add. Adjustment</td>
                <td class="section_order_pos_payment_sidebar_table_value"><input type="text" value="" class="form-control setPrivMaxValue" name="pendingAmountData" placeholder="0.00"></td>
            </tr>
              
             <tr>
                <td class="section_order_pos_payment_sidebar_table_label">Rounding Off</td>
                <td class="section_order_pos_payment_sidebar_table_value"><input type="hidden" class="finalDiscountData" name="finalDiscountData"><div class="setCouponDiscount">0</div></td>
            </tr>
            <tr>
                <th class="section_order_pos_payment_sidebar_table_label">Net Payable:</th>
                <th class="section_order_pos_payment_sidebar_table_value"><input type="hidden" class="netPayableAmount" name="netPayableAmount"><div class="setNetPayable">0</div></th>
            </tr>

        </tbody></table> 
 
        

                
 
                
         
               

                <div class="paymnet_option_pos">
                     <div class="order-checkout-payment-method-heading">
                        Invoice
                    </div>
                      <div class="order-checkout-payment-method">
                    
                          
                      <label class="order-checkout-payment-box-row">
                                <input type="radio" id="inlineRadio1" value="cash" name="paymentOption" checked="" class="order-checkout-payment-method-type">
                                 
                              <span class="order-checkout-payment-box-row-title"> <img src="https://houdine.com/vendor/assets/images/wallet.png" class="order-checkout-payment-box-row-icon"><span>Cash</span></span>
                             
                              </label> 
    <label class="order-checkout-payment-box-row">                            
                            <input type="radio" id="inlineRadio2" value="credit card" name="paymentOption" class="order-checkout-payment-method-type"> 
                              <span class="order-checkout-payment-box-row-title"> <img src="https://houdine.com/vendor/assets/images/credit-card.png" class="order-checkout-payment-box-row-icon"><span>Credit Card </span></span>
                                               
                            </label> 
<label class="order-checkout-payment-box-row">
                                <input type="radio" id="inlineRadio2" value="debit card" name="paymentOption" class="order-checkout-payment-method-type">  
                              <span class="order-checkout-payment-box-row-title"><img src="https://houdine.com/vendor/assets/images/payment.png" class="order-checkout-payment-box-row-icon"><span>Debit Card</span></span> 
                               
                            </label>
                            <label class="order-checkout-payment-box-row">
                                <input type="radio" id="inlineRadio2" value="cheque" name="paymentOption" class="order-checkout-payment-method-type">  
                              <span class="order-checkout-payment-box-row-title"> <img src="https://houdine.com/vendor/assets/images/cheque.png" class="order-checkout-payment-box-row-icon"><span>Cheque</span></span> 
                              
                            </label>

                            <label class="order-checkout-payment-box-row">
                                <input type="radio" id="inlineRadio2" value="Gift Card" name="paymentOption" class="order-checkout-payment-method-type">  
                              <span class="order-checkout-payment-box-row-title"><img src="https://houdine.com/vendor/assets/images/add-user.png" class="order-checkout-payment-box-row-icon"><span>Gift Card</span></span> 
                               
                            </label> 

                           <label class="order-checkout-payment-box-row">
                                <input type="radio" id="inlineRadio2" value="customer Credit" name="paymentOption" class="order-checkout-payment-method-type">  
                              <span class="order-checkout-payment-box-row-title"><img src="https://houdine.com/vendor/assets/images/ticket.png" class="order-checkout-payment-box-row-icon"><span>Customer Credit</span></span> 
                               
                            </label>
                    </div>  
                </div>
      
                <div class="form-group">
        <label  for="field-1" class="control-label">Coupon Disc</label>
         
        <input type="hidden" class="couponCodeId" name="couponCodeId"/>
        <input type="text" value="" tabindex="3" class="form-control couponCodeData" name="couponCodeData" placeholder="Coupon Code">
         


          <div class="form-group" style="padding: 10px;"> 
          <label for="field-1" class="control-label">Note</label>
          <textarea class="form-control" rows="4" name="customerNote"></textarea> 
          </div>

          <div class="form-group" style="padding: 10px;"> 
          <label for="field-1" class="control-label">Outlat</label>
          <select class="form-control required_validation_for_workorder" tabindex="12" name="outletData">
                        <option value="">Choose Warehouse</option>
                        <?php 
                         
                         if($this->session->userdata('vendorOutlet') == 0)
                         {
                        foreach($getOutlet as $getOutletList)
                        {
                          ?>
                          <option value="<?php echo $getOutletList->id ?>"><?php echo $getOutletList->w_name ?></option>
                        <?php } }
                        else
                        {
                            foreach($warehouseData as $warehouseDataList)
                            {
                                if($this->session->userdata('vendorOutlet') == $warehouseDataList->id)
                                {
                                ?>
                                <option value="<?php echo $warehouseDataList->id ?>"><?php echo $warehouseDataList->w_name ?></option>
                                <?php }
                                ?>
            
                            <?php }
                        }
                        ?>
                      </select>
          </div>

        <div class="form-group" style="padding: 10px;"> 
        <label for="field-1" class="control-label">Delivery Date & Time</label>
<div class="input-group">
<input type="hidden" name="customer_work_completion_detail"  value="10"  class="form-control" id="userName">
                     
<input type="text" tabindex="13" class="form-control datepicker required_validation_for_workorder" id="order_delivery_date" name="order_delivery_date" placeholder="Date"/>
<span class="input-group-addon">&</span>
<input type="text" tabindex="14" class="form-control timepicker required_validation_for_workorder" id="order_delivery_time" name="order_delivery_time" placeholder="Time"/>
</div>

        </div>

        <div class="form-group" style="padding: 10px;"> 
        <label for="field-1" class="control-label">Pick Up Type</label>
            <div class="input-group">
            <select class="form-control  required_validation_for_workorder" tabindex="6" name="Customer_pickup_type">             
            <option value="deliver">Home Delivery</option>
            <option value="pick_up">Customer Pickup</option>
            </select>
            </div>

            </div>

        


            <table class="section_order_pos_payment_system_table">
               
                <tbody><tr style="">
                    <th>Pay Pending Amount</th>
                    <td>   <input type="text" value="" class="form-control setPrivMaxValue" name="pendingAmountData" placeholder="0.00">
                    <select style="display:none" class="form-control checkPaymentStatus" name="paymentStatus">
                            <option value="">Choose Payment Status</option>
                            <option value="1" selected>Paid</option>
                            <option value="0">Not Paid</option>
                            </select>
                   </td>
                </tr> 
  
                <tr><td colspan="2"> <div class="text-center hidden-print">
        <!-- <a href="POS/printinvoice" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i> Print</a> -->
        <input type="button" name="savedata"  class="btn btn-primary btn-round btn-block submitMainFormData generateOrderBtn" disabled="disabled" value="WorkOrder Save">
        </div></td></tr>

            </tbody></table>

            <div class="alert alert-danger setErrorMessage" style="display:none;"></div>
        <div class="alert alert-success setSucccessMessage" style="display:none;"></div>
                
           
        </div>
      
        </div>


            </div>
        </div>
    </div>


    <?php echo form_close(); ?>
        <!-- add registred user address modal -->
        <div id="addRegistredUserAddressModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
            <?php echo form_open(base_url( 'POS/addUserAddress' ), array( 'id' => 'addPosForm', 'method'=>'post' ));?>
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title custom_title"><b>Add Address</b></h4> 
                    </div> 
                    <div class="modal-body"> 
                            <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                <input type="hidden" class="addAddressCustomerId" name="addAddressCustomerId"/>
                                    <label for="field-3" class="control-label">Name</label> 
                                    <input type="text" name="userName" class=" form-control required_validation_for_pos name_validation" placeholder="User Name"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Contact</label> 
                                    <input type="text" name="userPhone" class=" form-control required_validation_for_pos name_validation" placeholder="Contact"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Address</label> 
                                    <textarea class="form-control required_validation_for_pos name_validation" rows="5" name="userMainAddress"> </textarea>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">City</label> 
                                    <input type="text" name="userCity" class=" form-control required_validation_for_pos name_validation" placeholder="City"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Zip</label> 
                                    <input type="text" name="userZip" class=" form-control required_validation_for_pos name_validation" placeholder="Zip"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Country</label> 
                                    <select class="form-control required_validation_for_pos" name="userCountry">
                                    <option value="">Choose Country</option>
                                    <option value="<?php echo $countryData['id'] ?>"><?php echo $countryData['country'] ?></option>
                                    </select>
                                </div> 
                            </div> 
                        </div> 
                         
                    </div> 
                    <div class="modal-footer"> 
                    <input type="submit" name="payumoneyAdd" class="btn btn-info addRegisteredUserBtnData" value="Add"/>
                    </div> 
                </div> 
                <?php echo form_close(); ?>
            </div>
        </div><!-- /.modal -->

 



<?php $this->load->view('Template/footer') ?>
<script src="<?php echo base_url() ?>assets/plugins/moment/moment.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
jQuery('.timepicker').timepicker({defaultTIme : true,minuteStep : 15,autoclose: true,});
	jQuery('.datepicker').datepicker({
			autoclose: true,
			todayHighlight: true,
			format: 'dd-mm-yyyy',
	});
//ExtratrAppand
$(document).on('click','.addnewproductRow',function(){
 
    var getEmptyValue = emptyValueCheck()  // call empty valur check function
     
        if(getEmptyValue == 0)
        {
            var setText = 'add';
            var dataset = $(this);
            sethtmldata(setText)  // call html append function
        }
});


$(".select1").select2({
minimumInputLength: 2
});
// Select serach function 
// set product data
$(document).on('change','.setproductData',function(){
    $(this).parents('.mainParentRow').find('.setMainPrice').text('').text($(this).children('option:selected').attr('data-price'));
    $(this).parents('.mainParentRow').find('.specialPrice').val('').val($(this).children('option:selected').attr('data-price'));
    $(this).parents('.mainParentRow').find('.getQuantity').attr('data-max',$(this).children('option:selected').attr('data-stock'));
    
    $(this).parents('.mainParentRow').find('.mrp').text('').text($(this).children('option:selected').attr('data-mrp'));
    $(this).parents('.mainParentRow').find('.cp').text('').text($(this).children('option:selected').attr('data-cp'));
    $(this).parents('.mainParentRow').find('.tax').text('').text($(this).children('option:selected').attr('data-tax'));
    // Call final price function
    var quantity = $(this).parents('.mainParentRow').find('.getQuantity').val();
    var getSpecialPriceData = $(this).parents('.mainParentRow').find('.specialPrice').val();
    var dataSet = $(this);
    if(quantity && getSpecialPriceData)
    {
        finalPrice(quantity,getSpecialPriceData,dataSet); 
    }
    setProductArrayData(dataSet);
});
// get quantity
$(document).on('keyup','.getQuantity',function(e){
    var getSpecialPriceData = $(this).parents('.mainParentRow').find('.specialPrice').val();
    if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
    {
        var quantity = $(this).attr('data-max');
        $(this).val($(this).attr('data-max'));
    }
    else
    {
        var quantity = $(this).val();
    }
    var dataSet = $(this);
    finalPrice(quantity,getSpecialPriceData,dataSet); // Call final price function
    setProductArrayData(dataSet);
    // check enter is pressed or not on quantity
 
    if(e.which == 13){
        var getEmptyValue = emptyValueCheck()  // call empty valur check function
        if(getEmptyValue == 0)
        {
            var setText = 'add';
            var dataset = $(this);
            sethtmldata(setText)  // call html append function
        }
    }
   
});
// get coupon code ajax
$(document).on('change','.couponCodeData',function(){
    var getCouponCode = $('.couponCodeData').val();
    var getNetPayable = $('.setNetPayable').text();
    couponCodeAjax(getCouponCode,1,getNetPayable);  // check coupon code ajax
})
// get special price
$(document).on('keyup','.specialPrice',function(){
    var getSpecialPriceData = $(this).val();
    var quantity = $(this).parents('.mainParentRow').find('.getQuantity').val();
    var dataSet = $(this);
    finalPrice(quantity,getSpecialPriceData,dataSet);// Call final price function
    setProductArrayData(dataSet);
});
// set customer type
$(document).on('click','.checkCustomerTypeData',function(){
    // call to check customer type
    checkCustomerTypeData();
    // call to check delivery type
    setDeliveryType();
});

// get customer contact
$(document).on('change','.selectCustomerData',function(){
    $('.setCustomerContact').val($(this).children('option:selected').attr('data-contact'));
    fetchCustomerAddress($(this).val());
    checkCustomerPriv($(this).val());
    
    $('#addCustomerbtntext').html($(this).children('option:selected').attr('data-name'));
});
// set delivery type
$(document).on('click','.deliveryTypeData',function(){
    // call delivery type function
    setDeliveryType(); 
    // call to check customer type
    checkCustomerTypeData();
});
// add registred user address
$(document).on('click','.addRegistredUserAddress',function(){
     
    if($('.selectCustomerData').val())
    {
        $('.addAddressCustomerId').val($('.selectCustomerData').val());
        $('#addRegistredUserAddressModal').modal('show');

        $('.setErrorMessage').hide();
    }
    else
    {
        $('.setErrorMessage').show();
        $('.setErrorMessage').text('').text('Please choose customer');
    }   
});
// client side validaion
$(document).on('submit','#addPosForm',function(e){
    $('.setErrorMessage').hide();
    $('.setSucccessMessage').hide();
var check_required_field='';
$(this).find(".required_validation_for_pos").each(function(){
    var val22 = $(this).val();
    if (!val22){
        check_required_field =$(this).size();
        $(this).css("border-color","#ccc");
        $(this).css("border-color","red");
    }
    $(this).on('keypress change',function(){
        $(this).css("border-color","#ccc");
    });
});
if(check_required_field)
{
    return false;
}
else {
    e.preventDefault();
    $('.addRegisteredUserBtnData').prop('disabled',true);
    var formValue = $('#addPosForm').serializeArray();
    jQuery.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>POS/addRegisteredUserAddress",
    data: formValue,
    success: function(data) {
        var getJsonData = jQuery.parseJSON(data);
        if(getJsonData.messgae == 'Address is addedd successfully')
        {
            $('#addRegistredUserAddressModal').modal('hide');
            $('.setSucccessMessage').show()
            $('.setSucccessMessage').text('').text(getJsonData.messgae);
            // call custoemr address function
            fetchCustomerAddress($('.selectCustomerData').val());
        }
        else
        {
            $('#addRegistredUserAddressModal').modal('hide');
            $('.setErrorMessage').show();
            $('.setErrorMessage').text('').text(getJsonData.messgae);
        }
    }
    }); 
    
}
});
// add address for non registered users
$(document).on('click','.addNonRegistredUserAddress',function(){
$('#addNonRegistredUserAddressModal').modal('show');
});
// edit current address of non registered user
$(document).on('click','.setAddressDataValue',function(){
$('.nonUserMainAddress').text($(this).attr('data-address'));
$('.nonUserCity').val($(this).attr('data-city'));
$('.nonUserZip').val($(this).attr('data-zip'));
$('#addNonRegistredUserAddressModal').modal('show');
})
// set validation for non registred user
$(document).on('submit','#addNonRegisteredForm',function(e){
    $('.setErrorMessage').hide();
    $('.setSucccessMessage').hide();
    $('.addNonRegisteredUserBtnData').prop('disabled',true);
var check_required_field='';
$(this).find(".required_validation_for_pos_non").each(function(){
    var val22 = $(this).val();
    if (!val22){
        check_required_field =$(this).size();
        $(this).css("border-color","#ccc");
        $(this).css("border-color","red");
    }
    $(this).on('keypress change',function(){
        $(this).css("border-color","#ccc");
    });
});
if(check_required_field)
{
    $('.addNonRegisteredUserBtnData').prop('disabled',false);
    return false;
}
else 
{
    e.preventDefault();
    $('.addNonRegisteredUserBtnData').prop('disabled',false);
    var formValue = $('#addNonRegisteredForm').serializeArray();
    var getMainAddress = formValue[1].value;
    var getCity = formValue[2].value;
    var getZip = formValue[3].value;
    $('.setAddressDataValue').val('').val(getMainAddress+", "+getCity+", "+getZip);
    $('.setAddressDataValue').attr('data-address',getMainAddress);
    $('.setAddressDataValue').attr('data-city',getCity);
    $('.setAddressDataValue').attr('data-zip',getZip);
    $('#addNonRegistredUserAddressModal').modal('hide');
    var setAddressString = getMainAddress+"^"+getCity+"^"+getZip;
    $('.setNonRegisteredCustomerAddress').val(setAddressString);
}
});
// set additionla adjustment
$(document).on('click','.additionalAdjustmentCheckboxData',function(){
$('.setPrivMaxValue').val('');
if($(this).prop('checked') == true)
{
    $('.setPrivMaxValue').val($('.setPrivMaxValue').attr('data-max'));
    $('.additionalAdjustmentText').show();
}    
else
{
    $('.setPrivMaxValue').val('');
    $('.additionalAdjustmentText').hide();
}
setFinalPriceValue();
});
// set adjustment on price
$(document).on('keyup','.setPrivMaxValue',function(){
var getAdjustMentValue = $(this).val();
setPendingAmountData(getAdjustMentValue);
})
// final price function 
function finalPrice(quantity,specialprice,dataSet)
{
    if(specialprice)
    {
        if(quantity)
        {
            var setQuantity = quantity;
        }
        else
        {
            setQuantity = 0;
            dataSet.val(setQuantity);
        }
        var setFinalPrice = (parseFloat(specialprice)*parseFloat(setQuantity))
        dataSet.parents('.mainParentRow').find('.setFinalPrice').text('').text(setFinalPrice);
        dataSet.parents('.mainParentRow').find('.specialPrice').css('border','1px solid #d3d3d3');
    }
    else
    {
        
        dataSet.val(dataSet.parents('.mainParentRow').find('.setMainPrice').text());
        dataSet.parents('.mainParentRow').find('.specialPrice').css('border','1px solid red');
        dataSet.parents('.mainParentRow').find('.setFinalPrice').text('0');
    }
    setFinalPriceValue(); // set final value
}
// add html or remove html
function sethtmldata(setText,dataset)
{
    if(setText == 'add')
    {
        var countRows = ($('.mainParentRow').length)+1;
        $('.setRowData').append('<tr ddd class="mainParentRow"><input type="hidden" class="inputProdcutId" name="inputProdcutId[]"/><input type="hidden" class="inputDiscountedPrice" name="inputDiscountedPrice[]"/><input type="hidden" class="inputVariantId" name="inputVariantId[]"/><input type="hidden" class="inputQuantity" name="inputQuantity[]"/><input type="hidden" class="inputActualPrice" name="inputActualPrice[]"/><input type="hidden" class="inputtotaltax" name="inputtotaltax[]"/><input type="hidden" class="inputTotalPrice" name="inputTotalPrice[]"/><td><div class="form-group" style="margin-bottom: 0px!important;"><select class="form-control setproductData select_auto'+countRows+'"><option value="">Choose Product</option><?php foreach($getProductData['productData'] as $productDataList){?><option data-tax="<?php echo $productDataList['tax_price'] ?>"data-cp="<?php echo $productDataList['cp'] ?>" data-mrp="<?php echo $productDataList['mrp'] ?>" data-price="<?php echo $productDataList['productPrice'] ?>" data-stock="<?php echo $productDataList['stock'] ?>" data-variant="<?php echo $productDataList['variantId'] ?>" data-product="<?php echo $productDataList['produtcId'] ?>"><?php echo $productDataList['productName'] ?></option><?php } ?></select></div></td><td class="mrp"></td><td class="cp"></td><td class="setMainPrice"></td><td><input type="text" value="" class="form-control specialPrice number_validation" name="" placeholder=""></td><td class="tax"></td><td><input type="text" value="" class="form-control getQuantity number_validation" name="" placeholder=""></td><td class="setFinalPrice">0</td></tr>');
        $(".select_auto"+countRows+"").select2({
            minimumInputLength: 2
        });
    }
}
// calculate final price value
function setFinalPriceValue()
{
    // each function to calculate total
    var setTotalData = 0;
    var minimumorder = '<?php echo $possetting[0]->minimum_order_amount; ?>';
    if(minimumorder)
    {
        minimumorder = minimumorder;    
    }
    else
    {
        minimumorder = 0;    
    }
    $('.setFinalPrice').each(function(){
        setTotalData = setTotalData+parseFloat($(this).text());
    });
    // set subtotal
    $('.setFinalTotal').text('').text(setTotalData);
    var getDiscount = $('.setCouponDiscount').text();
    var getFinalPayable = setTotalData-parseFloat(getDiscount);
    $('.setNetPayable').text(getFinalPayable);
    if(parseFloat(getFinalPayable) < parseFloat(minimumorder))
    {
        $('.generateOrderBtn').prop('disabled',false);
    }
    else
    {
        $('.generateOrderBtn').prop('disabled',false);
    }
    // update coupon code data
    var getCouponCode = $('.couponCodeData').val();
    var getCustomer = $('.selectCustomerData').val();
    var getNetPayable = $('.setFinalTotal').text();
    if(getCouponCode && getCustomer && getNetPayable)
    {
        couponCodeAjax(getCouponCode,getCustomer,getNetPayable);
    }
    var getAdjustMentValue = $('.setPrivMaxValue').val();
    if(getAdjustMentValue && getAdjustMentValue > 0)
    {
        setPendingAmountData(getAdjustMentValue)
    }
    // set amount
    $('.finalAmountData').val($('.setFinalTotal').text());
    $('.finalDiscountData').val($('.setCouponDiscount').text());
    $('.netPayableAmount').val($('.setNetPayable').text());
}
// check all empty value
function emptyValueCheck()
{
    // product select 
    var checkData = 0;
    $('.setproductData').each(function(){
        if($(this).val() == '')
        {
            $(this).css('border','1px solid red');
            checkData++;
        }
        else
        {
            $(this).css('border','1px solid #d3d3d3');
        }
    });
    // check special price value
    $('.specialPrice').each(function(){
        if($(this).val() == 0 || $(this).val() == "")
        {
            $(this).css('border','1px solid red');
            checkData++;
        }
        else
        {
            $(this).css('border','1px solid #d3d3d3');
        }
    });
    // check quantity
    $('.getQuantity').each(function(){
        if($(this).val() == 0 || $(this).val() == "")
        {
            $(this).css('border','1px solid red');
            checkData++;
        }
        else
        {
            $(this).css('border','1px solid #d3d3d3');
        }
    });
     return checkData;
}
// check coupon ajax data
function couponCodeAjax(coupon,customerId,getNetPayable)
{
    $('.setErrorMessage').hide();
    $('.setSucccessMessage').hide();
    $('.applyCouponCodeData').prop('disabled',true);
    jQuery.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>POS/getCouponValidity",
    data: {"coupon": coupon,'customerId':customerId,'getNetPayable':getNetPayable},
    success: function(data) {
       var getJsonData = jQuery.parseJSON(data);
       var getMinimumOrder = '<?php echo $possetting[0]->minimum_order_amount ?>';
       if(getMinimumOrder)
       {
           getMinimumOrder = getMinimumOrder;
       }
       else
       {
           getMinimumOrder = 0;
       }
       if(getJsonData.message == 'Success')
       {
           $('.setSucccessMessage').show().text('Coupon code applied Succefully')
           var getSubTotal = $('.setFinalTotal').text();
           var couponDiscount = getJsonData.discount;
           var appliedCouponId = getJsonData.couponId;
           $('.couponCodeId').val(appliedCouponId);
           if(couponDiscount > 0)
           {
               var setNetDiscount = (parseFloat(getSubTotal)*parseFloat(couponDiscount))/100;
               var setNetPayable = parseFloat(getSubTotal)-setNetDiscount;
           }
           else
           {
               var setNetDiscount = 0;
               var setNetPayable = getSubTotal;
           }
           $('.setCouponDiscount').text(setNetDiscount);
           $('.setNetPayable').text(setNetPayable);
           if(parseFloat(setNetPayable) < parseFloat(getMinimumOrder))
            {
                $('.generateOrderBtn').prop('disabled',true);
            }
            else
            {
                $('.generateOrderBtn').prop('disabled',false);
            }
           $('.applyCouponCodeData').prop('disabled',false);
           var getAdjustMentValue = $('.setPrivMaxValue').val();
           if(getAdjustMentValue && getAdjustMentValue > 0)
           {
               setPendingAmountData(getAdjustMentValue)
            }
            // set amount
            $('.finalAmountData').val($('.setFinalTotal').text());
            $('.finalDiscountData').val($('.setCouponDiscount').text());
            $('.netPayableAmount').val($('.setNetPayable').text());
       }
       else
       {
            $('.applyCouponCodeData').prop('disabled',false);
            $('.setErrorMessage').show().text(getJsonData.message);
       }
    }
    }); 
    
}
// fetch customer registred address
function fetchCustomerAddress(customerId)
{
    var getDeliveryType = setDeliveryType();
    if(getDeliveryType == 'HD')
    {
        $('.setErrorMessage').hide();
        $('.setSucccessMessage').hide();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>POS/getCustomerAddress",
        data: {'customerId':customerId },
        success: function(data) {
            var getJsonData = jQuery.parseJSON(data);
            if(getJsonData.message == 'Success')
            {
                var setCustomerAddress = '<option value="">Choose Address</option>';
                $.each(getJsonData.data,function(){
                    setCustomerAddress += '<option value="'+this.addressId+'">'+this.mainAddress+', '+this.city+', '+this.zip+'</option>'
                });
                $('.setRegisteredCustomerAddress').html(setCustomerAddress);
            }
            else
            {
                $('.setErrorMessage').show().text(getJsonData.message);
            }
        }
        }); 
    }
}
// check deliveryType Data
function setDeliveryType()
{
    var setPickupValue= "";
    $('.deliveryTypeData').each(function(){
        if($(this).prop('checked') == true)
        {
            if($(this).val() == 'HD')
            {
                $('.setAddressData').show();
                setPickupValue = 'HD';
            }
            else
            {
                $('.setAddressData').hide();
                setPickupValue = 'PC';
            }
        }
    });
    return 'HD';
}
// check customer type function
function checkCustomerTypeData()
{
    $('.additionalAdjustmentCheck').hide();
    //$('.selectCustomerData').val('');
    $('.setCustomerContact').val('');
    $('.checkCustomerTypeData').each(function(){
    if($(this).prop('checked') == true)
    {
     
        var getValue = $(this).val();
        if(getValue == 'RC')
        {
            $('.registerdCustomerData').show();
            $('.nonRegisterdCustomerData').hide();
        }
        else
        {
            $('.registerdCustomerData').hide();
            $('.nonRegisterdCustomerData').show();
        }
    }
})
}
function checkCustomerPriv(customerId)
{
    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>POS/getCustomerPrivilage",
        data: {'customerId':customerId },
        success: function(data) {
            var getData = jQuery.parseJSON(data);
            if(getData.message == 'Success')
            {
                if(getData.dataValuePriv > 0)
                {
                    $('.additionalAdjustmentCheck').show();
                    $('.setPrivMaxValue').attr('data-max',getData.dataValuePriv)
                }
                else
                {
                    $('.additionalAdjustmentCheck').hide();
                    $('.setPrivMaxValue').attr('data-max','')
                }
            }
        }
        }); 
}
function setPendingAmountData(priceData)
{
    $('.setErrorMessage').hide();
    if(parseFloat($('.setPrivMaxValue').attr('data-max')) < parseFloat(priceData))
    {
        $('.setPrivMaxValue').val($('.setPrivMaxValue').attr('data-max'));
    }
    var getTotalAmount = $('.setFinalTotal').text();
    var getDiscount = $('.setCouponDiscount').text();
    var getFianlNetPayable = (parseFloat(getTotalAmount)-parseFloat(getDiscount));
    var additionalAdjustment = $('.setPrivMaxValue').val();
    var getMinimumOrder = '<?php echo $possetting[0]->minimum_order_amount ?>';
    if(getMinimumOrder)
    {
        getMinimumOrder = getMinimumOrder;
    }
    else
    {
        getMinimumOrder = 0;
    }
    if(getFianlNetPayable && getFianlNetPayable > 0 && additionalAdjustment && additionalAdjustment > 0)
    {
        if(getFianlNetPayable > additionalAdjustment)
        {
            var setNewNetPayable = getFianlNetPayable - additionalAdjustment;
        }
        else
        {
            var setNewNetPayable = additionalAdjustment - getFianlNetPayable;
        }
        $('.setNetPayable').text('').text(setNewNetPayable);
        if(parseFloat(setNewNetPayable) < parseFloat(getMinimumOrder))
        {
            $('.generateOrderBtn').prop('disabled',true);
        }
        else
        {
            $('.generateOrderBtn').prop('disabled',false);
        }
    }
    else
    {
        $('.setPrivMaxValue').val('1');
    }
}
// set product value
function setProductArrayData(dataset)
{
    var getProductId = dataset.parents('.mainParentRow').find('.setproductData').children('option:selected').attr('data-product');
    var getVariantId = dataset.parents('.mainParentRow').find('.setproductData').children('option:selected').attr('data-variant');
    var actualPrice = dataset.parents('.mainParentRow').find('.specialPrice').val();
    var quantityData = dataset.parents('.mainParentRow').find('.getQuantity').val();
    var finalPriceData = dataset.parents('.mainParentRow').find('.setFinalPrice').text();
    var productInitialPrice = dataset.parents('.mainParentRow').find('.setMainPrice').text();
    var productTax = dataset.parents('.mainParentRow').find('.setproductData').children('option:selected').attr('data-taxprice');
    if(productTax)
    {
        var setTaxdata = parseFloat(productTax)*quantityData;
    }
    else
    {
        var setTaxdata = 0;
    }
    var findDiscountData = parseFloat(productInitialPrice)-parseFloat(actualPrice);
    dataset.parents('.mainParentRow').find('.inputProdcutId').val(getProductId);
    dataset.parents('.mainParentRow').find('.inputVariantId').val(getVariantId);
    dataset.parents('.mainParentRow').find('.inputQuantity').val(quantityData);
    dataset.parents('.mainParentRow').find('.inputActualPrice').val(actualPrice);
    dataset.parents('.mainParentRow').find('.inputTotalPrice').val(finalPriceData);
    dataset.parents('.mainParentRow').find('.inputDiscountedPrice').val(findDiscountData);
    dataset.parents('.mainParentRow').find('.inputtotaltax').val(setTaxdata);
}
$(document).on('click','.submitMainFormData',function(){
    var setData = 0;

    $('.checkCustomerTypeData').each(function(){
        
        if($(this).prop('checked') == true)
        { 
            if($(this).val() == 'RC')
            { 
                
                $('.requiredValidationForCustomerName').each(function(){
                    if($(this).val() == "")
                    {
                        setData++;
                        $(this).css('border','1px solid red');
            $('.setErrorMessage').show();
$('.setErrorMessage').text('').text('Please choose customer');
                    }
                    else
                    {
                        $(this).css('border','1px solid #d3d3d3');
                    }
                });
                $('.deliveryTypeData').each(function(){
                if($(this).prop('checked') == true)
                {
                    if($(this).val() == 'HD')
                    {
                        if($('.checkCustomerTypeData').val() == 'RC')
                        {
                            if($('.requiredValidationForRegisteredAddress').val() == "")
                            {
                                setData++;
                                $('.requiredValidationForRegisteredAddress').css('border','1px solid red');
                            }
                            else
                            {
                                $('.requiredValidationForRegisteredAddress').css('border','1px solid #d3d3d3');
                            }
                        }
                        else
                        {
                            if($('.requiredValidationForNonRegisteredAddress').val() == "")
                            {
                                setData++;
                                $('.requiredValidationForNonRegisteredAddress').css('border','1px solid red');
                            }
                            else
                            {
                                $('.requiredValidationForNonRegisteredAddress').css('border','1px solid #d3d3d3');
                            }
                        }
                    }
                    else
                    {
                        
                    }
                }
            })
            }
            else
            {
                $('.requiredValidationForNonRegisteredUser').each(function(){
                    if($(this).val() == "")
                    {
                        setData++;
                        $(this).css('border','1px solid red');
                    }
                    else
                    {
                        $(this).css('border','1px solid #d3d3d3');
                    }
                });
            }            
        }
            
    });
    var getValueData = emptyValueCheck();
    // setData = getValueData;
    if($('.checkPaymentStatus').val() == "")
    {
        setData++;
        $('.checkPaymentStatus').css('border','1px solid red');
    }
    else
    {
        $('.checkPaymentStatus').css('border','1px solid #d3d3d3');
    }
    if(setData == 0 && getValueData == 0)
    {
        generateOrderData();
    }
    else
    {
        return false;
    }
});
function generateOrderData()
{
    $('#submitMainForm').submit();
}
    </script>
    <script type="text/javascript">
        $('[data-toggle="btns"] .btn').on('click', function(){
    var $this = $(this);
    $this.parent().find('.active').removeClass('active');
    $this.addClass('active');
});
    </script>
    
