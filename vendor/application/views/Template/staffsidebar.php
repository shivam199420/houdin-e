<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
<?php
$getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
  if($this->session->userdata('vendorRole')==1)
    {
$sidebar_permission =  $this->db->select("*")->from("houdinv_staff_management_sub")
    ->join("houdinv_staff_management","houdinv_staff_management.staff_id=houdinv_staff_management_sub.staff_id","right outer")
    ->where("houdinv_staff_management.houdinv_staff_auth_token",$this->session->userdata('vendorAuth'))
    ->get()->row();
    }
    else
    {
        $sidebar_permission = '';
    }
    
  //  print_R($sidebar_permission);
?>
        <div id="sidebar-menu">
            <ul>
            <?php if(!$header_permission || ($header_permission->staff_members) !=''){ 
        ?>       
                             <li class="has_sub">
                    <a href="<?php echo base_url(); ?>staff" class="waves-effect <?php if($this->router->fetch_class()=="staff"){echo 'active';}?>"><i class="fa fa-database"></i> <span> Staff Members </span></a>
                </li>     
                <?php  
         }
          ?> 
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
