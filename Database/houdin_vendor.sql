/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.19-MariaDB : Database - houdin_vendor
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`houdin_vendor` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `houdin_vendor`;

/*Table structure for table `houdinv_block_users` */

DROP TABLE IF EXISTS `houdinv_block_users`;

CREATE TABLE `houdinv_block_users` (
  `houdinv_block_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_block_user_email` varchar(150) DEFAULT NULL,
  `houdinv_block_user_reason` text,
  `houdinv_block_user_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_block_user_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_block_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_block_users` */

/*Table structure for table `houdinv_coupons` */

DROP TABLE IF EXISTS `houdinv_coupons`;

CREATE TABLE `houdinv_coupons` (
  `houdinv_coupon_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_coupon_code` varchar(150) NOT NULL,
  `houdinv_coupon_discount` float NOT NULL,
  `houdinv_coupon_limit` int(11) NOT NULL,
  `houdinv_coupon_reamining` int(11) NOT NULL,
  `houdinv_coupon_type` int(11) NOT NULL,
  `houdinv_coupon_valid_from` datetime NOT NULL,
  `houdinv_coupon_valid_to` datetime NOT NULL,
  `houdinv_coupon_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_coupon_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_coupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_coupons` */

/*Table structure for table `houdinv_deleted_users` */

DROP TABLE IF EXISTS `houdinv_deleted_users`;

CREATE TABLE `houdinv_deleted_users` (
  `houdinv_deleted_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_deleted_user_email` varchar(150) DEFAULT NULL,
  `houdinv_deleted_user_by` varchar(150) DEFAULT NULL COMMENT 'By Admin, Self Delete',
  `houdinv_deleted_user_reason` tinytext,
  `houdinv_deleted_user_delete_status` tinyint(4) DEFAULT '0' COMMENT '0 Tmp Delete, 1 Permanent Delete',
  `houdinv_deleted_user_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_deleted_user_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_deleted_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_deleted_users` */

/*Table structure for table `houdinv_deliveries` */

DROP TABLE IF EXISTS `houdinv_deliveries`;

CREATE TABLE `houdinv_deliveries` (
  `houdinv_delivery_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_delivery_order_id` int(10) unsigned NOT NULL,
  `houdinv_delivery_user_id` int(10) unsigned NOT NULL,
  `houdinv_delivery_by` enum('Delivery Boy','Delivery Service') NOT NULL,
  `houdinv_delivery_code` varchar(50) NOT NULL,
  `houdinv_delivery_to` varchar(200) DEFAULT NULL,
  `houdinv_delivery_to_verifiacation` varchar(200) DEFAULT NULL,
  `houdinv_delivery_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_delivery_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_delivery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_deliveries` */

/*Table structure for table `houdinv_delivery_tracks` */

DROP TABLE IF EXISTS `houdinv_delivery_tracks`;

CREATE TABLE `houdinv_delivery_tracks` (
  `houdinv_delivery_track_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_delivery_track_order_id` int(10) unsigned NOT NULL,
  `houdinv_delivery_track_delivery_id` int(10) unsigned NOT NULL,
  `houdinv_delivery_track_updated_by` int(10) unsigned NOT NULL,
  `houdinv_delivery_track_date` datetime NOT NULL,
  `houdinv_delivery_track_status` tinyint(4) NOT NULL,
  `houdinv_delivery_track_comment` text NOT NULL,
  `houdinv_delivery_track_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_delivery_track_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_delivery_track_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_delivery_tracks` */

/*Table structure for table `houdinv_delivery_users` */

DROP TABLE IF EXISTS `houdinv_delivery_users`;

CREATE TABLE `houdinv_delivery_users` (
  `houdinv_delivery_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_delivery_user_name` varchar(150) DEFAULT NULL,
  `houdinv_delivery_user_email` varchar(150) DEFAULT NULL,
  `houdinv_delivery_user_contact` varchar(20) DEFAULT NULL,
  `houdinv_delivery_user_attributes` text,
  `houdinv_delivery_user_address` varchar(350) DEFAULT NULL,
  `houdinv_delivery_user_active_status` tinyint(4) DEFAULT '0',
  `houdinv_delivery_user_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_delivery_user_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_delivery_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_delivery_users` */

/*Table structure for table `houdinv_forgot_password` */

DROP TABLE IF EXISTS `houdinv_forgot_password`;

CREATE TABLE `houdinv_forgot_password` (
  `houdinv_forgot_password_email` varchar(150) NOT NULL,
  `houdinv_forgot_password_token` varchar(50) NOT NULL,
  `houdinv_forgot_password_created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_forgot_password` */

/*Table structure for table `houdinv_inventories` */

DROP TABLE IF EXISTS `houdinv_inventories`;

CREATE TABLE `houdinv_inventories` (
  `houdinv_inventory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_inventory_supplier_id` int(10) unsigned NOT NULL,
  `houdinv_inventory_supplier_product_id` int(10) unsigned NOT NULL,
  `houdinv_inventory_quantity` decimal(10,0) DEFAULT NULL,
  `houdinv_inventory_quantity_left` decimal(10,0) NOT NULL,
  `houdinv_inventory_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_inventory_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_inventory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_inventories` */

/*Table structure for table `houdinv_inventory_purchases` */

DROP TABLE IF EXISTS `houdinv_inventory_purchases`;

CREATE TABLE `houdinv_inventory_purchases` (
  `houdinv_inventory_purchase_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_inventory_purchase_inventory_id` int(10) unsigned NOT NULL,
  `houdinv_inventory_purchase_quantity` int(11) NOT NULL,
  `houdinv_inventory_purchase_order_status` tinyint(4) NOT NULL,
  `houdinv_inventory_purchase_order_date` datetime DEFAULT NULL,
  `houdinv_inventory_purchase_order_completion_date` datetime DEFAULT NULL,
  `houdinv_inventory_purchase_comment` text,
  `houdinv_inventory_purchase_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `houdinv_inventory_purchase_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`houdinv_inventory_purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_inventory_purchases` */

/*Table structure for table `houdinv_inventory_tracks` */

DROP TABLE IF EXISTS `houdinv_inventory_tracks`;

CREATE TABLE `houdinv_inventory_tracks` (
  `houdinv_inventory_track_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_inventory_track_product_id` int(10) unsigned NOT NULL,
  `houdinv_inventory_track_supplier_id` int(10) unsigned NOT NULL,
  `houdinv_inventory_track_product_qty` decimal(10,0) NOT NULL,
  `houdinv_inventory_track_product_cost` float NOT NULL,
  `houdinv_inventory_track_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_inventory_track_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_inventory_track_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_inventory_tracks` */

/*Table structure for table `houdinv_offline_payment_settings` */

DROP TABLE IF EXISTS `houdinv_offline_payment_settings`;

CREATE TABLE `houdinv_offline_payment_settings` (
  `houdinv_offline_payment_setting_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_offline_payment_setting_user_id` int(10) unsigned NOT NULL,
  `houdinv_offline_payment_setting_holder_name` varchar(150) DEFAULT NULL,
  `houdinv_offline_payment_setting_holder_number` varchar(150) DEFAULT NULL,
  `houdinv_offline_payment_setting_country` varchar(150) DEFAULT NULL,
  `houdinv_offline_payment_setting_bank_name` varchar(200) DEFAULT NULL,
  `houdinv_offline_payment_setting_iban` varchar(50) DEFAULT NULL,
  `houdinv_offline_payment_setting_shift_code` varchar(50) DEFAULT NULL,
  `houdinv_offline_payment_setting_additional_information` text,
  `houdinv_offline_payment_setting_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_offline_payment_setting_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_offline_payment_setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_offline_payment_settings` */

/*Table structure for table `houdinv_online_payment_settings` */

DROP TABLE IF EXISTS `houdinv_online_payment_settings`;

CREATE TABLE `houdinv_online_payment_settings` (
  `houdinv_online_payment_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `houdinv_online_payment_setting_gateway_name` varchar(150) NOT NULL,
  `houdinv_online_payment_setting_client_id` varchar(150) NOT NULL,
  `houdinv_online_payment_setting_secret_id` varchar(150) NOT NULL,
  `houdinv_online_payment_setting_mode` varchar(20) NOT NULL,
  `houdinv_online_payment_setting_active_status` tinyint(4) NOT NULL DEFAULT '0',
  `houdinv_online_payment_setting_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_online_payment_setting_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_online_payment_setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_online_payment_settings` */

/*Table structure for table `houdinv_orders` */

DROP TABLE IF EXISTS `houdinv_orders`;

CREATE TABLE `houdinv_orders` (
  `houdinv_order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_order_user_id` int(10) unsigned NOT NULL,
  `houdinv_order_product_id` int(10) unsigned NOT NULL,
  `houdinv_order_type` enum('App','Website','Store') DEFAULT NULL,
  `houdinv_order_quantity` tinyint(3) unsigned NOT NULL,
  `houdinv_order_product_price` float NOT NULL,
  `houdinv_order_total_price` float NOT NULL,
  `houdinv_order_comments` tinytext,
  `houdinv_order_confirmation_status` tinyint(4) NOT NULL DEFAULT '0',
  `houdinv_order_delivery_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 Not Delivered, 1 Processing, 2 Processed, 3 Delivered, 4 Delivered To Reference',
  `houdinv_order_delivery_address_id` int(10) unsigned NOT NULL,
  `houdinv_order_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_order_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_orders` */

/*Table structure for table `houdinv_product_categories` */

DROP TABLE IF EXISTS `houdinv_product_categories`;

CREATE TABLE `houdinv_product_categories` (
  `houdinv_product_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_product_category_icon` varchar(20) DEFAULT NULL,
  `houdinv_product_category_slug` varchar(150) DEFAULT NULL,
  `houdinv_product_category_name` varchar(150) NOT NULL,
  `houdinv_product_category_image` varchar(150) DEFAULT NULL,
  `houdinv_product_category_parent_id` int(10) unsigned DEFAULT '0',
  `houdinv_product_category_createa_at` timestamp NULL DEFAULT NULL,
  `houdinv_product_category_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_product_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_product_categories` */

/*Table structure for table `houdinv_product_reviews` */

DROP TABLE IF EXISTS `houdinv_product_reviews`;

CREATE TABLE `houdinv_product_reviews` (
  `houdinv_product_review_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_product_review_user_id` int(10) unsigned NOT NULL,
  `houdinv_product_review_product_id` int(10) unsigned NOT NULL,
  `houdinv_product_review_message` text NOT NULL,
  `houdinv_product_review_images` text,
  `houdinv_product_review_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_product_review_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_product_review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_product_reviews` */

/*Table structure for table `houdinv_product_types` */

DROP TABLE IF EXISTS `houdinv_product_types`;

CREATE TABLE `houdinv_product_types` (
  `houdinv_product_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_product_type_name` varchar(150) NOT NULL,
  `houdinv_product_type_description` tinytext,
  `houdinv_product_type_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_product_type_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_product_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_product_types` */

/*Table structure for table `houdinv_products` */

DROP TABLE IF EXISTS `houdinv_products`;

CREATE TABLE `houdinv_products` (
  `houdinv_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_product_inventory_id` int(11) unsigned NOT NULL,
  `houdinv_product_category_id` int(10) unsigned NOT NULL,
  `houdinv_product_type_id` int(10) unsigned NOT NULL,
  `houdinv_product_price` float NOT NULL,
  `houdinv_product_barcode` varchar(300) DEFAULT NULL,
  `houdinv_product_quantity` float NOT NULL,
  `houdinv_product_cost_price` float DEFAULT NULL,
  `houdinv_product_selling_price` float DEFAULT NULL,
  `houdinv_product_custom_shipping` float DEFAULT NULL,
  `houdinv_product_feature_image` varchar(150) DEFAULT NULL,
  `houdinv_product_other_images` tinytext,
  `houdinv_product_description` text,
  `houdinv_product_sales_tax` float DEFAULT NULL,
  `houdinv_product_purchase_tax` float DEFAULT NULL,
  `houdinv_product_supplier` int(10) unsigned NOT NULL,
  `houdinv_product_variant` varchar(500) NOT NULL,
  `houdinv_product_comment` varchar(150) DEFAULT NULL COMMENT 'In Stock, Out of Stock',
  `houdinv_product_show_status` varchar(20) DEFAULT NULL COMMENT 'Comma seperated values',
  `houdinv_product_active_atatus` tinyint(4) NOT NULL DEFAULT '1',
  `houdinv_product_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_product_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_products` */

/*Table structure for table `houdinv_shipping_charges` */

DROP TABLE IF EXISTS `houdinv_shipping_charges`;

CREATE TABLE `houdinv_shipping_charges` (
  `houdinv_shipping_charge_id` int(11) NOT NULL AUTO_INCREMENT,
  `houdinv_shipping_charge_country` varchar(150) DEFAULT NULL,
  `houdinv_shipping_charge_cost` float DEFAULT NULL,
  `houdinv_shipping_charge_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_shipping_charge_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_shipping_charge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_shipping_charges` */

/*Table structure for table `houdinv_shop_configurations` */

DROP TABLE IF EXISTS `houdinv_shop_configurations`;

CREATE TABLE `houdinv_shop_configurations` (
  `houdinv_shop_configuration_google_analytics` varchar(150) DEFAULT NULL,
  `houdinv_shop_configuration_bing_analytics` varchar(150) DEFAULT NULL,
  `houdinv_shop_configuration_product_review` tinyint(4) DEFAULT '0',
  `houdinv_shop_configuration_category_direction` varchar(10) DEFAULT NULL,
  `houdinv_shop_configuration_shop_logo` varchar(150) DEFAULT NULL,
  `houdinv_shop_configuration_product_limit` tinyint(4) DEFAULT '4',
  `houdinv_shop_configuration_tag_line` varchar(200) DEFAULT NULL,
  `houdinv_shop_configuration_ad_active` tinyint(4) DEFAULT '0',
  `houdinv_shop_configuration_currency` int(11) DEFAULT NULL,
  `houdinv_shop_configuration_language` int(11) DEFAULT NULL,
  `houdinv_shop_configuration_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_shop_configuration_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_shop_configurations` */

/*Table structure for table `houdinv_social_links` */

DROP TABLE IF EXISTS `houdinv_social_links`;

CREATE TABLE `houdinv_social_links` (
  `houdinv_social_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `houdinv_social_link_name` varchar(200) NOT NULL,
  `houdinv_social_link_url` varchar(200) NOT NULL,
  `houdinv_social_link_icon` varchar(200) NOT NULL,
  `houdinv_social_link_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_social_link_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_social_link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_social_links` */

/*Table structure for table `houdinv_supplier_products` */

DROP TABLE IF EXISTS `houdinv_supplier_products`;

CREATE TABLE `houdinv_supplier_products` (
  `houdinv_supplier_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_supplier_product_category_id` int(10) unsigned NOT NULL,
  `houdinv_supplier_product_name` varchar(150) NOT NULL,
  `houdinv_supplier_product_type` int(10) unsigned NOT NULL,
  `houdinv_supplier_product_cost` float NOT NULL,
  `houdinv_supplier_product_sales_tax` float DEFAULT NULL,
  `houdinv_supplier_product_purchase_tax` float DEFAULT NULL,
  `houdinv_supplier_product_discount` float DEFAULT NULL,
  `houdinv_supplier_product_images` text,
  `houdinv_supplier_product_status` tinyint(4) DEFAULT NULL,
  `houdinv_supplier_product_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_supplier_product_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_supplier_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_supplier_products` */

/*Table structure for table `houdinv_suppliers` */

DROP TABLE IF EXISTS `houdinv_suppliers`;

CREATE TABLE `houdinv_suppliers` (
  `houdinv_supplier_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_supplier_comapany_name` varchar(150) DEFAULT NULL,
  `houdinv_supplier_contact_person_name` varchar(150) NOT NULL,
  `houdinv_supplier_email` varchar(150) NOT NULL,
  `houdinv_supplier_contact` varchar(20) NOT NULL,
  `houdinv_supplier_address` varchar(500) NOT NULL,
  `houdinv_supplier_city` varchar(150) NOT NULL,
  `houdinv_supplier_state` varchar(150) NOT NULL,
  `houdinv_supplier_country` varchar(150) NOT NULL,
  `houdinv_supplier_active_status` tinyint(4) NOT NULL DEFAULT '0',
  `houdinv_supplier_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `houdinv_supplier_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`houdinv_supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_suppliers` */

/*Table structure for table `houdinv_taxes` */

DROP TABLE IF EXISTS `houdinv_taxes`;

CREATE TABLE `houdinv_taxes` (
  `houdinv_tax_id` int(11) NOT NULL AUTO_INCREMENT,
  `houdinv_tax_percentage` float NOT NULL,
  `houdinv_tax_country` varchar(150) NOT NULL,
  `houdinv_tax_description` text NOT NULL,
  `houdinv_tax_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_tax_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_tax_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_taxes` */

/*Table structure for table `houdinv_template_pages` */

DROP TABLE IF EXISTS `houdinv_template_pages`;

CREATE TABLE `houdinv_template_pages` (
  `houdinv_template_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `houdinv_template_page_name` varchar(200) NOT NULL,
  `houdinv_template_page_slug` varchar(250) NOT NULL,
  `houdinv_template_page_body` text NOT NULL,
  `houdinv_template_page_meta` text,
  `houdinv_template_page_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_template_page_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_template_page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_template_pages` */

/*Table structure for table `houdinv_templates` */

DROP TABLE IF EXISTS `houdinv_templates`;

CREATE TABLE `houdinv_templates` (
  `houdinv_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `houdinv_template_header_image` text,
  `houdinv_template_header_text` tinytext,
  `houdinv_template_header_sub_text` text,
  `houdinv_template_footer` tinytext,
  `houdinv_template_copyright` varchar(200) DEFAULT NULL,
  `houdinv_template_active_status` tinyint(4) DEFAULT '0',
  `houdinv_template_meta` text,
  `houdinv_template_file` varchar(300) DEFAULT NULL COMMENT 'Location of template folder',
  `houdinv_template_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_template_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_templates` */

/*Table structure for table `houdinv_user_addresses` */

DROP TABLE IF EXISTS `houdinv_user_addresses`;

CREATE TABLE `houdinv_user_addresses` (
  `houdinv_user_address_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_user_address_user_id` int(10) unsigned NOT NULL,
  `houdinv_user_address_contact_no` varchar(15) DEFAULT NULL,
  `houdinv_user_address_type` varchar(150) DEFAULT NULL,
  `houdinv_user_address_street_line1` varchar(200) NOT NULL,
  `houdinv_user_address_street_line2` varchar(200) DEFAULT NULL,
  `houdinv_user_address_landmark` varchar(200) DEFAULT NULL,
  `houdinv_user_address_pincode` decimal(10,0) NOT NULL,
  `houdinv_user_address_city` varchar(150) NOT NULL,
  `houdinv_user_address_state` varchar(150) NOT NULL,
  `houdinv_user_address_country` varchar(150) NOT NULL,
  `houdinv_user_address_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_user_address_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_user_address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_user_addresses` */

/*Table structure for table `houdinv_user_auth` */

DROP TABLE IF EXISTS `houdinv_user_auth`;

CREATE TABLE `houdinv_user_auth` (
  `houdinv_user_auth_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_user_auth_url_token` varchar(150) NOT NULL,
  `houdinv_user_auth_auth_token` varchar(150) NOT NULL,
  `houdinv_user_auth_email` varchar(150) NOT NULL,
  `houdinv_user_auth_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `houdinv_user_auth_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`houdinv_user_auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_user_auth` */

/*Table structure for table `houdinv_user_bank_details` */

DROP TABLE IF EXISTS `houdinv_user_bank_details`;

CREATE TABLE `houdinv_user_bank_details` (
  `houdinv_user_bank_detail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_user_bank_detail_user_id` int(10) unsigned NOT NULL,
  `houdinv_user_bank_detail_holder_name` varchar(150) NOT NULL,
  `houdinv_user_bank_detail_holder_number` varchar(150) NOT NULL,
  `houdinv_user_bank_detail_bank_name` varchar(200) NOT NULL,
  `houdinv_user_bank_detail_swift_no` varchar(150) DEFAULT NULL,
  `houdinv_user_bank_detail_iban_no` varchar(150) DEFAULT NULL,
  `houdinv_user_bank_detail_country` varchar(150) NOT NULL,
  `houdinv_user_bank_detail_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_user_bank_detail_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_user_bank_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Bank details of user for refund';

/*Data for the table `houdinv_user_bank_details` */

/*Table structure for table `houdinv_user_logs` */

DROP TABLE IF EXISTS `houdinv_user_logs`;

CREATE TABLE `houdinv_user_logs` (
  `houdinv_user_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_user_log_user_id` int(10) unsigned NOT NULL,
  `houdinv_user_log_ip_address` varchar(20) NOT NULL,
  `houdinv_user_log_browser` varchar(150) NOT NULL,
  `houdinv_user_log_geo_location` varchar(50) NOT NULL,
  `houdinv_user_log_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `houdinv_user_log_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`houdinv_user_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_user_logs` */

/*Table structure for table `houdinv_user_payments` */

DROP TABLE IF EXISTS `houdinv_user_payments`;

CREATE TABLE `houdinv_user_payments` (
  `houdinv_user_payment_id` int(10) unsigned NOT NULL,
  `houdinv_user_payment_user_id` int(10) unsigned NOT NULL,
  `houdinv_user_payment_product_id` int(10) unsigned NOT NULL,
  `houdinv_user_payment_coupon_id` int(10) unsigned NOT NULL,
  `houdinv_user_payment_cost` float NOT NULL,
  `houdinv_user_payment_currency` varchar(50) NOT NULL,
  `houdinv_user_payment_transcaction_id` varchar(150) NOT NULL,
  `houdinv_user_payment_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 For Unsuccess, 1 For Success, 2 For cancelled, 2 For refund',
  `houdinv_user_payment_mode` enum('COD','Online','Store') DEFAULT NULL,
  `houdinv_user_payment_time` datetime NOT NULL,
  `houdinv_user_payment_created_at` timestamp NULL DEFAULT NULL,
  `houdinv_user_payment_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`houdinv_user_payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_user_payments` */

/*Table structure for table `houdinv_user_verifications` */

DROP TABLE IF EXISTS `houdinv_user_verifications`;

CREATE TABLE `houdinv_user_verifications` (
  `houdinv_user_verification_email` varchar(150) NOT NULL,
  `houdinv_user_verification_token` varchar(150) NOT NULL,
  `houdinv_user_verification_created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_user_verifications` */

/*Table structure for table `houdinv_users` */

DROP TABLE IF EXISTS `houdinv_users`;

CREATE TABLE `houdinv_users` (
  `houdinv_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `houdinv_user_name` varchar(150) NOT NULL,
  `houdinv_user_email` varchar(150) NOT NULL,
  `houdinv_user_password` varchar(150) NOT NULL,
  `houdinv_user_contact` varchar(150) NOT NULL,
  `houdinv_user_is_verified` tinyint(4) NOT NULL DEFAULT '0',
  `houdinv_user_is_active` tinyint(4) NOT NULL DEFAULT '0',
  `houdinv_user_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `houdinv_user_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`houdinv_user_id`),
  UNIQUE KEY `houdinv_user_email` (`houdinv_user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `houdinv_users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
