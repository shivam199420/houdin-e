<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style type="text/css">
  .checkbox {
    padding-left: 0px;
}
</style> 
 
 <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
 
             <!--START Page-Title -->
            <div class="row">
       
                <div class="col-md-8">
                   <h4 class="page-title">Shipping Rule</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>shipping/Shippinglanding">Shipping</a></li>
                  <li class="active">Shipping Rule</li>
                  </ol>
                  </div>
                
            </div>
           <!--END Page-Title -->  
             <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
     <?php 
      

     $data_shippingrule=$shippingrule[0];

      $shipping_active_=$data_shippingrule->shiping_active;

      if($shipping_active_=='1'){
    $shipping_active_='checked';
      }else{
$shipping_active_='';
      }
     ?>

            <?php echo form_open(base_url('Shipping/shippingrule_save'), array( 'id' => 'shippingrule_save', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
            <div class="row m-t-20">
              <div class="col-lg-12">
                <div class="card-box">
                <input type="hidden" name="shiping_update_id" value="<?=$data_shippingrule->id?>">
                <h3 class="text_black">Shipping Rule</h3>
                <hr>
               <p class="text_black">Shipping fee will be charged on total order smaller than mimimum amount.</p>
               
           
                 <div class="checkbox checkbox-custom m-b-10">
                 <input id="shipping_active" name="shiping_active" <?=$shipping_active_;?> value="1" type="checkbox" >
                 <label>Is active</label>
              </div>
                                            
                <div class="ajax_Loginresponse_result">

                  <div class="form-group">
                      <label for="shipping_min_value">Minimum order</label>
                      <input type="text" name="shipping_min_value" parsley-trigger="order" value="<?=$data_shippingrule->shipping_min_value?>"  placeholder="" class="form-control" id="shipping_min_value">
                    
                    </div>
                    <div class="form-group">
                      <label for="shipping_charge">Shipping charge</label>
                      <input type="text" name="shipping_charge"  value="<?=$data_shippingrule->shipping_charge?>"  parsley-trigger="change"  placeholder="" class="form-control" id="shipping_charge">
                    
                    </div>
                   </div>
                   
                    <div class="form-group m-b-0">
                      <button class="btn btn-primary waves-effect waves-light" type="submit">
                        Save
                      </button>
                      
                    </div>
                     
                </div>
              </div>            
              
            </div>
             <?php echo form_close(); ?>
                                      

                                       
      </div>
    </div>
  </div>

<?php $this->load->view('Template/footer.php') ?>

<script>
$(document).ready(function(){


   $('#shippingrule_save').submit(function(e){
    e.preventDefault(); 
 

    urls='<?=base_url();?>'+'Shipping/shippingrule_save';
          var dataString = $(this).serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".ajax_Loginresponse_result").html(data);
        }else{
        window.location.reload();
        }
        }
        });  
    });  

    $(".select2").select2();
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
