<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Wishlist extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Cartmodel');
        $this->load->model('Shopproductlistviewmodel');
        $this->load->model('Shopsettingmodel');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $wishData= $this->Shopproductlistviewmodel->ShowWhishlistData($this->session->userdata('userAuth'));
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view($getShopName.'/wishlist',$wishData);
    }
     
    public function AddToWhishlist()
     {
        $item_id = $this->input->post('whishlist_item');
        $product_detail = $this->Shopproductlistviewmodel->GetSingleProductList($item_id);
        $product_price_array = json_decode($product_detail['main_product']->houdin_products_price);
       $actual_price = $product_price_array->sale_price - $product_price_array->discount;
      $date = strtotime(date("Y-m-d, h:i:s"));
       if($this->input->post('variantItem'))
       {
        $item_id =0;
       }
       
        $array = array("houdinv_users_whishlist_user_id"=>$this->session->userdata('userAuth'),
                       "houdinv_users_whishlist_item_id"=>$item_id,
                       "houdinv_users_whishlist_item_variant_id"=>$this->input->post('variantItem'),
                       "houdinv_users_whishlist_final_price"=>$actual_price,
                       "houdinv_users_whishlist_add_date"=>$date,
                       "houdinv_users_whishlist_update_date"=>$date);
                       
        $wishData = $this->Cartmodel->AddToWishlistDB($array);
       
                       
        
       }
       
       public function RemoveFromWhishlist()
       {
         if($this->input->post('whishlist_item'))
           {
            $item_id = $this->input->post('whishlist_item');
            $wishData = $this->Cartmodel->RemoveFromWhishlistDB($item_id);
            
           }
       }
       
        
      
     
     
}
