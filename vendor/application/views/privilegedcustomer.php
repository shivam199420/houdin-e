﻿<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
             <!--START Page-Title -->
            <div class="row">
        
                <div class="col-md-8">
                   <h4 class="page-title">Privilege Customer</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>customer/Customerlanding">Customer Management</a></li>
                  <li class="active">Privilege Customer</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                
            </div>
           <!--END Page-Title --> 
           
            <div class="row m-t-20">

              
                                        <div class="col-lg-4 col-sm-6">
                                            <div class="widget-panel widget-style-2 bg-white">
                                                <img src="<?php echo base_url(); ?>assets/images/allpageicon/Privilege_Customer.png">
                                                <h2 class="m-0 text-dark counter font-600"><?php echo $totalPrivilageCustomer; ?></h2>
                                                <div class="text-muted m-t-5">Total privilege customer</div>
                                            </div>
                                        </div>
                                        
                                      </div>



     <!-- show messages -->
        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>

                                      
                                    
        <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">
              <div class="btn-group pull-right m-t-10 m-b-20">
             
                <a href="<?php echo base_url(); ?>Customer/customerprivileged" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
              
              </div>
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th><input type="checkbox"></th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>email</th>
                   <th>Privalge Amount</th>
                 
                    <th>Action</th>
                    
                  </tr>
                  
                </thead>
                <tbody>
                <?php
                  foreach($customerList as $customerListData)
                                {
                ?>
                  <tr>
                    <td><input data-email="<?php echo $customerListData->houdinv_user_email ?>" data-id="<?php echo $customerListData->houdinv_user_id ?>" type="checkbox"></td>
                    <td><a href="<?php echo base_url(); ?>Customer/view/<?php echo $customerListData->houdinv_user_id ?>"><?php echo $customerListData->houdinv_user_name ?></a></td>
                     <td><?php echo $customerListData->houdinv_user_contact ?></td>
                    <td><?php echo $customerListData->houdinv_user_email ?></td> 
                    <td><?php echo $customerListData->houdinv_users_privilage ?></td>
               
                    
                    <td>
                    <?php 
                    if($customerSettingData[0]->pay_panding_amount > 0)
                    {
                    ?>
                    <button data-max="<?php echo $customerSettingData[0]->pay_panding_amount ?>" class="btn btn-success waves-effect waves-light changeCustomerPrivilageBtn" data-privilage="<?php echo $customerListData->houdinv_users_privilage; ?>" data-id="<?php echo $customerListData->houdinv_user_id ?>">Change Privilage</button>
                    <?php }
                    else
                    {
                    ?>
                    <button class="btn btn-success waves-effect waves-light changeCustomerPrivilageBtn" data-privilage="<?php echo $customerListData->houdinv_users_privilage; ?>" data-id="<?php echo $customerListData->houdinv_user_id ?>">Change Privilage</button>
                    <?php }
                    ?>

                    </td>
                  </tr>
                  <?php }
                                ?>  
                </tbody>
              </table>
              
                <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
<!--Delete-->

<!---Start Change Privilage -->
        <div id="changeCustomerPrivilageModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
        <?php echo form_open(base_url( 'Customer/changePrivilagePage' ), array( 'method'=>'post', 'id'=>'changePrivilageModalData' ));?>
        <div class="modal-content"> 
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
        <h4 class="modal-title">Change Privilaged amount</h4> 
        </div> 
        <div class="modal-body"> 
        <div class="row"> 
        <div class="col-md-12"> 
        <div class="form-group"> 
        <input type="hidden" class="changeCustomerPrivilageId" name="changeCustomerPrivilageId"/>
        <label for="field-1" class="control-label">Choose Privilage amount</label> 
        <input class="form-control required_validation_for_add_single_tag choosePrivilage number_validation setmaxValue" name="choosePrivilage"/>
      
        </div> 
        </div> 
        </div> 
        </div> 
        <div class="modal-footer"> 
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        <input type="submit" class="btn btn-info" value="Change Privilage" name="updateCustomerPrivilage" />
        </div> 
        </div> 
        <?php echo form_close(); ?>
        </div>
        </div><!-- End privilage -->

<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
        <script type="text/javascript">
        $(document).ready(function(){
                $(document).on('submit','#changeCustomerPrivilageModal',function(c){
                        var rep_image_val='';
                        $(this).find(".required_validation_for_add_single_tag").each(function()
                        {
                                var val22 = jQuery(this).val();
                                if (!val22)
                                {
                                        rep_image_val = 'error form';
                                        $(this).css("border-color","red");
                                }
                        });
                        $(this).find('.required_validation_for_add_single_tag').on('keyup blur',function()
                        {
                                $(this).css("border-color","#ccc");
                        });
                        if(rep_image_val)
                        {
                                c.preventDefault();
                                return false;
                        }
                        else
                        {
                                return true;
                        }        
                });
        })
        </script>