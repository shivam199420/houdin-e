<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function stroeSetting($shop)
{
    $CI =& get_instance($shop);
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);
    $dynamicDb = $CI->load->database($getDatatbase,true);
    $getStoreSettingData = $dynamicDb->select('*')->from('houdinv_storesetting')->where('id',1)->get()->result();
    return $getStoreSettingData;
}
function orderSetting($shop)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);
    $dynamicDb = $CI->load->database($getDatatbase,true);
    $getOrderSettingData = $dynamicDb->select('*')->from('houdinv_shop_order_configuration')->where('houdinv_shop_order_configuration_id',1)->get()->result();
    return $getOrderSettingData;
}
function fetchEmailCount($shop)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);
    $dynamicDb = $CI->load->database($getDatatbase,true);
    $getEmailCountData = $dynamicDb->select('*')->from('houdinv_emailsms_stats')->where('houdinv_emailsms_stats_type','email')->get()->result();
    return $getEmailCountData;
}
function updateSuceessEmailCount($remaining,$shop)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);
    $dynamicDb = $CI->load->database($getDatatbase,true);
    $setUpdatedArray = array('houdinv_emailsms_stats_remaining_credits'=>$remaining);
    $dynamicDb->where('houdinv_emailsms_stats_type','email');
    $setData = strtotime(date('Y-m-d'));
    $dynamicDb->update('houdinv_emailsms_stats',$setUpdatedArray);
    $setInsertArray = array('houdinv_email_log_credit_used'=>'1','houdinv_email_log_status'=>'1','houdinv_email_log_created_at'=>$setData);
    $dynamicDb->insert('houdinv_email_log',$setInsertArray);
}
function updateErrorEmailLog($shop)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);
    $dynamicDb = $CI->load->database($getDatatbase,true);
    $setData = strtotime(date('Y-m-d'));
    $setInsertArray = array('houdinv_email_log_credit_used'=>'0','houdinv_email_log_status'=>'0','houdinv_email_log_created_at'=>$setData);
    $dynamicDb->insert('houdinv_email_log',$setInsertArray);
}
function fetchEmailSMSCredentials($shop)
{
    $CI =& get_instance();
    $dynamicDb = $CI->load->database('master',true);
    $getTwillioCredentails = $dynamicDb->select('*')->from('houdin_twilio')->where('houdin_twilio_id','1')->get()->result();
    $getSendGridCredentails = $dynamicDb->select('*')->from('houdin_sendgrid')->where('houdin_sendgrid_id','1')->get()->result();
    return array('twillio'=>$getTwillioCredentails,'sendGrid'=>$getSendGridCredentails);
}
function fetchRegisterTemplate($shop)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);
    $dynamicDb = $CI->load->database($getDatatbase,true);
    $getEmailTemplate = $dynamicDb->select('*')->from('houdinv_email_Template')->where('houdinv_email_template_type','register')->get()->result();
    return $getEmailTemplate;
}
function fetchOrderTemaplTemplate($shop)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);

    $dynamicDb = $CI->load->database($getDatatbase,true);
    $getOrderTemplate = $dynamicDb->select('*')->from('houdinv_email_Template')->where('houdinv_email_template_type','On_order')->get()->result();
    return $getOrderTemplate;
}
function fetchUsername($data,$shop)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);
    $dynamicDb = $CI->load->database($getDatatbase,true);

    $getUserName = $dynamicDb->select('houdinv_user_name,houdinv_user_email')->from('houdinv_users')->where('houdinv_user_id',$data)->get()->result();
    return array('name'=>$getUserName[0]->houdinv_user_name,'email'=>$getUserName[0]->houdinv_user_email);
}
function getPayuCredentials($shop)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);

    $dynamicDb = $CI->load->database($getDatatbase,true);

    $getPayU = $dynamicDb->select('*')->from('houdinv_payment_gateway')->where('houdinv_payment_gateway_type','payu')->where('houdinv_payment_gateway_status','active')->get()->result();
    return $getPayU;
}
function  getShopDetails($shop)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);
    $dynamicDb = $CI->load->database($getDatatbase,true);
    $getShopDetails = $dynamicDb->select('houdinv_shop_communication_email')->from('houdinv_shop_detail')->where('houdinv_shop_id','1')->get()->result();
    return $getShopDetails;
}
function getAuthCredentials($shop)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);

    $dynamicDb = $CI->load->database($getDatatbase,true);

    $getAuth = $dynamicDb->select('*')->from('houdinv_payment_gateway')->where('houdinv_payment_gateway_type','auth')->where('houdinv_payment_gateway_status','active')->get()->result();
    return $getAuth;
}
function getShopCurrency()
{
    $CI =& get_instance();
    $getMasterDb = $CI->load->database('master',true);
    $getCurrency = $getMasterDb->select('houdin_user_shops.houdin_user_shop_user_id,houdin_users.houdin_users_currency')->from('houdin_user_shops')->where('houdin_user_shops.houdin_user_shop_shop_name',$CI->session->userdata('shopName'))
    ->join('houdin_users','houdin_users.houdin_user_id = houdin_user_shops.houdin_user_shop_user_id')->get()->result();
    return $getCurrency;

}
function updategetUserDevice($shop,$userid)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);
    $dynamicDb = $CI->load->database($getDatatbase,true);
    $getDeviceId = $dynamicDb->select('houdinv_users_device_id')->from('houdinv_users')->where('houdinv_user_id',$userid)->get()->result();
    return $getDeviceId;
}
function shippingRules($shop)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp');
    $getDatatbase = switchDynamicDatabaseApp($shop);

    // $getDatatbase->select()
}
function getDBName()
{
    $CI =& get_instance();
	$getData = $CI->session->userdata('shopName');
    $masterDB = $CI->load->database('master', TRUE);
    $getDatabse = $masterDB->select('houdin_user_shop_db_name')->from('houdin_user_shops')->where('houdin_user_shops.domain_name',$getData)->get()->result();
    return $getDatabse[0]->houdin_user_shop_db_name;
}
function fromemail()
{
    return "noreply@houdine.com";
}
function toemail()
{
    return "nimit@warrdel.com";
}
function getvendorurl()
{
    return "http://3.22.189.222/houdin-e/vendor/";
}
function getshopname()
{
    $CI =& get_instance();
	$getData = $CI->session->userdata('shopName');
    $masterDB = $CI->load->database('master', TRUE);
    $getDatabse = $masterDB->select('houdin_user_shop_shop_name')->from('houdin_user_shops')->where('houdin_user_shops.domain_name',$getData)->get()->result();
    return $getDatabse[0]->houdin_user_shop_shop_name;
}
function shopurl($shop)
{
    return "http://3.22.189.222/houdin-e/Shop/";
}
