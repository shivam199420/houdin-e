<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopuseraddress extends CI_Model{
    function __construct() 
    {
    }
    public function fetchAddressData()
    {
        $masterDB = $this->load->database('master', TRUE);
        $getShopName = $this->session->userdata('shopName');

        $getCountryName = $masterDB->select('houdin_user_shops.houdin_user_shop_country,houdin_countries.houdin_country_name')->from('houdin_user_shops')->where('houdin_user_shops.domain_name',$getShopName)
        ->join('houdin_countries','houdin_countries.houdin_country_id = houdin_user_shops.houdin_user_shop_country')
        ->get()->result();
        return array('countryList'=>$getCountryName);
    }
    public function addUserAddressData($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getInsertData = $getDynamicWebDb->insert('houdinv_user_address',$data);
        if($getInsertData == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function fetchUserAddress()
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);


        $getUserAddressData = $getDynamicWebDb->select('*')->from('houdinv_user_address')->where('houdinv_user_address_user_id',$this->session->userdata('userAuth'))->get()->result();
        return array('userAddress'=>$getUserAddressData);
    }
    public function fetchUserCounrtyApp($data)
    {
        $getMasterDB = $this->load->database('master',true);
        $getCountryData = $getMasterDB->select('houdin_user_shops.houdin_user_shop_user_id,houdin_users.houdin_user_country,houdin_countries.houdin_country_name')
        ->from('houdin_user_shops')->where('houdin_user_shops.domain_name',$data)
        ->join('houdin_users','houdin_users.houdin_user_id = houdin_user_shops.houdin_user_shop_user_id','left outer')
        ->join('houdin_countries','houdin_countries.houdin_country_id = houdin_users.houdin_user_country','left outer')
        ->get()->result();
        if(count($getCountryData) > 0)
        {
            return array('countryId'=>$getCountryData[0]->houdin_user_country,'countryName'=>$getCountryData[0]->houdin_country_name);
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function addUserAddressDataApp($data,$shop)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shop['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $insertStatus = $getDynamicDB->insert('houdinv_user_address',$data);
        if($insertStatus == 1)
        {
            return array('message'=>'yes');
        }
    }
    public function fetchUserAddressApp($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getUserAddress = $getDynamicDB->select('*')->from('houdinv_user_address')->where('houdinv_user_address_user_id',$data['id'])->get()->result();
        if(count($getUserAddress) > 0)
        {
            foreach($getUserAddress as $getUserAddressList)
            {
                $getMasterDB = $this->load->database('master',true);
                $getUserCountryName = $getMasterDB->select('houdin_country_name')->from('houdin_countries')->where('houdin_country_id',$getUserAddressList->houdinv_user_address_country)->get()->result();
                $mainAddress = str_replace(","," ",$getUserAddressList->houdinv_user_address_user_address);
                $setAddressArray[] = array('addressId'=>$getUserAddressList->houdinv_user_address_id,'name'=>$getUserAddressList->houdinv_user_address_name,
                'phone'=>$getUserAddressList->houdinv_user_address_phone,'mainAddress'=>$mainAddress,'city'=>$getUserAddressList->houdinv_user_address_city,
                'zip'=>$getUserAddressList->houdinv_user_address_zip,'country'=>$getUserCountryName[0]->houdin_country_name);
            }
            return array('message'=>'Success','data'=>$setAddressArray);
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function deleteUserAddressApp($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getDynamicDB->where('houdinv_user_address_id',$data['id']);
        $getDeleteStatus = $getDynamicDB->delete('houdinv_user_address');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'Address deleted successfully');
        }
        else
        {
            return array('message'=>'Something went wrong. Please try again');
        }
    }
    public function updateUserAddressApp($data,$shop)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shop['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getDynamicDB->where('houdinv_user_address_id',$shop['addressId']);
        $getUpdateStatus = $getDynamicDB->update('houdinv_user_address',$data);
        if($getUpdateStatus == 1)
        {
            return array('message'=>'Address updated successfully');
        }
        else
        {
            return array('message'=>'Something went wrong. Please try again');
        }
    }
    public function getUserAddressUpdate($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getAddress = $getDynamicWebDb->select('*')->from('houdinv_user_address')->where('houdinv_user_address_id',$data)->get()->result();
        return $getAddress;
    }
    public function updateUserAddress($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getDynamicWebDb->where('houdinv_user_address_id',$this->uri->segment('3'));
        $getData = $getDynamicWebDb->update('houdinv_user_address',$data);
        if($getData == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function deleteUserAddress($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getDynamicWebDb->where('houdinv_user_address_id',$data);
        $getDeleteStatus = $getDynamicWebDb->delete('houdinv_user_address');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
} 
?>