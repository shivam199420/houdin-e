<?php 
$getDB = getDBName();
$con = mysqli_connect("localhost","root","houdine123",$getDB);
  $getShopData = mysqli_query($con,"select houdinv_shop_business_name,houdinv_shop_address,houdinv_shop_communication_email,houdinv_shop_contact_info from houdinv_shop_detail where houdinv_shop_id=1");
  $getShopValue = mysqli_fetch_array($getShopData);
  // get footer social links
  $getShopSocialLink = mysqli_query($con,'select * from houdinv_social_links');
  $getSocialLinks = mysqli_fetch_array($getShopSocialLink);
  // get footer url
  $getUrl = mysqli_query($con,"select * from houdinv_navigation_store_pages");
  $setFooterLinks = array();
  while($getFooterUrl = mysqli_fetch_array($getUrl))
  {
    array_push($setFooterLinks,$getFooterUrl['houdinv_navigation_store_pages_name']);
  }
  ?>
  <?php
  $getCurrency = getShopCurrency();
  $currencysymbol=$getCurrency[0]->houdin_users_currency;
  ?>

  <div class="modal fade" id="add_Quotation_data" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title">Ask Quotation</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>

        </div>
            <form class="modal-body" id="add_Quotation_dataform" method="post" action="<?php echo base_url(); ?>Orders/quatationAdd">

		        <div class="row">
             <div class="col-md-12">

                <div class="card-box" style="float:left;width: 100%;">
                    <div class="form-group">
                        <div class="col-md-12">
                          <div class="form-group">
                        <label>Name</label>
                        <input type="text"  class="form-control require_quote required_validation_for_quotation name_validation characterValidation" name="name" placeholder="name" />
                      </div>
                       </div>
                      <div class="col-md-12">
                      <div class="form-group">
                        <label>Email</label>
                         <input type="email"  class="form-control require_quote required_validation_for_quotation email_validation" name="email" placeholder="email" />
                      </div>
                        </div>

                      <div class="col-md-12">
                      <div class="form-group">
                        <label>Phone</label>
                         <input type="text"  pattern="^[-+]?\d+$"  class="phone_telephone form-control require_quote required_validation_for_quotation" name="phone" placeholder="phone" />
                      </div>
                        </div>

                        <div class="col-md-12">
                      <div class="form-group">
                        <label>For product count</label>
                         <input type="text"  class="form-control require_quote required_validation_for_quotation number_validation" name="count" placeholder="product count" />
                      </div>
                        </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Description</label>
                         <textarea class="form-control require_quote required_validation_for_quotation name_validation
                         " name="comment"></textarea>
                      </div>
                        </div>


                    <div class="col-md-12">
                      <div class="form-group">
                      <input type="hidden" value="" id="product_quete_id" name="product_id"/>
                        <input type="submit" style="background: #33d286;color: #fff;z-index: 99999999;top: 15px;left: 8px;padding: 3px 12px;border-radius: 2px;" class="form-control"  value="submit" name="quote_submit"  />
                        </div>
                        </div>


                </div>
              </div>
        </div>

          </form>




      </div>
    </div>
  </div>
</div>

<footer class="ps-footer">

<div class="container">

  <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center ">

          <div class="ps-widget ps-widget--footer ps-widget--worktime">

            <div class="ps-widget__header">

              <h3 class="ps-widget__title">INFORMATION</h3>

            </div>

            <div class="ps-widget__content">

                  <ul class="ps-list--line">

                    <?php
        if(in_array('about',$setFooterLinks))
        {
          echo '<li><a href="'.base_url().'About">About Us</a></li>';
        }
        if(in_array('terms',$setFooterLinks))
        {
          echo '<li><a href="'.base_url().'Terms">Terms & Conditions</a></li>';
        }
        if(in_array('privacy',$setFooterLinks))
        {
          echo '<li><a href="'.base_url().'Privacy">Privacy Policy</a></li>';
        }
        if(in_array('faq',$setFooterLinks))
        {
          echo '<li><a href="'.base_url().'Faq">FAQs</a></li>';
        }
        if(in_array('contact',$setFooterLinks))
        {
          echo '<li><a href="'.base_url().'Contact">Contact Us</a></li>';
        }
        ?>

                  </ul>

            </div>

          </div>

        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center ">

          <div class="ps-widget ps-widget--footer ps-widget--order">

            <div class="ps-widget__header">

              <h3 class="ps-widget__title">Services</h3>

            </div>

            <div class="ps-widget__content">

                  <ul class="ps-list--line">

                   <?php
        if(in_array('disclaimer',$setFooterLinks))
        {
          echo '<li><a href="'.base_url().'Disclaimer">Disclaimer</a></li>';
        }
        if(in_array('shipping',$setFooterLinks))
        {
          echo '<li><a href="'.base_url().'Shippingpolicy">Shipping & Delivery</a></li>';
        }
        if(in_array('refund',$setFooterLinks))
        {
          echo '<li><a href="'.base_url().'Refund">Cancellation & Refund</a></li>';
        }
        ?> <?php
        if($this->session->userdata('userAuth') == "")
        {
          echo '<li><a href="'.base_url().'register">Register </a></li>';
        }
        ?>

                  </ul>

            </div>

          </div>

        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center ">

          <div class="ps-widget ps-widget--footer ps-widget--connect">

            <div class="ps-widget__header">

              <h3 class="ps-widget__title">CONNECT US</h3>

            </div>

            <div class="ps-widget__content">

            <ul>
            <li><?php if($getShopValue['houdinv_shop_address']) { echo $getShopValue['houdinv_shop_address']; } else { echo "Hundin-e"; } ?></li>
        <li><?php if($getShopValue['houdinv_shop_contact_info']) { echo $getShopValue['houdinv_shop_contact_info']; } else { echo "Hundin-e"; } ?></li>
        <li><?php if($getShopValue['houdinv_shop_communication_email']) { echo $getShopValue['houdinv_shop_communication_email']; } else { echo "Hundin-e"; } ?></li>
        <li>
          <a href="#" class="fa fa-facebook social_icon"></a>
<a href="#" class="fa fa-twitter social_icon"></a>
<a href="#" class="fa fa-linkedin social_icon"></a>
<a href="#" class="fa fa-instagram social_icon"></a>
        </li>
          </ul>



            </div>

          </div>

        </div>

  </div>

</div>

</footer>
<div class="footer-bottom">
  <p class="copyright_text text-center" style="color: #000;">&copy; 2018 All Rights Reserved <?php if($getShopValue['houdinv_shop_business_name']) { echo $getShopValue['houdinv_shop_business_name']; } else  { echo "Houdin-e"; } ?> </p>
</div>

<div class="modal-popup mfp-with-anim mfp-hide" id="quickview-modal" tabindex="-1">

<button class="modal-close"><i class="fa fa-remove"></i></button>

<div class="ps-product-modal ps-product--detail clearfix">

      <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 ">

        <div class="ps-product__thumbnail">

          <div class="quickview--main" data-owl-auto="true" data-owl-loop="false" data-owl-speed="10000" data-owl-gap="0" data-owl-nav="false" data-owl-dots="false" data-owl-animate-in="" data-owl-animate-out="" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-nav-left="&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;" data-owl-nav-right="&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;">

            <div class="ps-product__image"><img src="images/cake/img-cake-12.jpg" alt=""></div>

            <div class="ps-product__image"><img src="images/cake/img-cake-11.jpg" alt=""></div>

            <div class="ps-product__image"><img src="images/cake/img-cake-10.jpg" alt=""></div>

            <div class="ps-product__image"><img src="images/cake/img-cake-6.jpg" alt=""></div>

            <div class="ps-product__image"><img src="images/cake/img-cake-5.jpg" alt=""></div>

          </div>

          <div class="quickview--thumbnail" data-owl-auto="true" data-owl-loop="false" data-owl-speed="10000" data-owl-gap="20" data-owl-nav="false" data-owl-dots="false" data-owl-animate-in="" data-owl-animate-out="" data-owl-item="4" data-owl-item-xs="2" data-owl-item-sm="3" data-owl-item-md="4" data-owl-item-lg="4" data-owl-nav-left="&lt;i class=&quot;fa fa-angle-left&quot;&gt;&lt;/i&gt;" data-owl-nav-right="&lt;i class=&quot;fa fa-angle-right&quot;&gt;&lt;/i&gt;">
          <img src="<?php echo base_url() ?>Extra/bakery/images/cake/img-cake-12.jpg" alt="">
          <img src="<?php echo base_url() ?>Extra/bakery/images/cake/img-cake-11.jpg" alt="">
          <img src="<?php echo base_url() ?>Extra/bakery/images/cake/img-cake-10.jpg" alt="">
          <img src="<?php echo base_url() ?>Extra/bakery/images/cake/img-cake-6.jpg" alt="">
          <img src="<?php echo base_url() ?>Extra/bakery/images/cake/img-cake-5.jpg" alt=""></div>

        </div>

      </div>

      <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 ">

        <header>

          <h3 class="ps-product__name">Anytime Cakes</h3>

          <select class="ps-rating">

            <option value="1">1</option>

            <option value="1">2</option>

            <option value="1">3</option>

            <option value="1">4</option>

            <option value="5">5</option>

          </select>

          <p class="ps-product__price">£15.00 <del>£25.00</del></p>

          <div class="ps-product__meta">

            <p><span> Availability: </span> In stock</p>

            <p class="category"><span>CATEGORIES: </span><a href="product-grid.html">Cupcake</a>,<a href="product-grid.html">organic</a>,<a href="product-grid.html"> sugar</a>,<a href="product-grid.html"> sweet</a>,<a href="product-grid.html"> bio</a></p>

          </div>

          <div class="form-group ps-product__size">

            <label>Size:</label>

            <select class="ps-select" data-placeholder="Popupar product">

              <option value="01">Choose a option</option>

              <option value="01">Item 01</option>

              <option value="02">Item 02</option>

              <option value="03">Item 03</option>

            </select>

          </div>

          <div class="ps-product__shop">

            <div class="form-group--number">

              <button class="minus"><span>-</span></button>

              <input class="form-control" type="text" value="1">

              <button class="plus"><span>+</span></button>

            </div>

            <ul class="ps-product__action">

              <li><a href="#" data-tooltip="Add to wishlist"><i class="ps-icon--heart"></i></a></li>

              <li><a href="#" data-tooltip="Compare"><i class="ps-icon--reload"></i></a></li>

            </ul>

          </div>

        </header>

        <footer><a class="ps-btn--fullwidth ps-btn ps-btn--sm" href="#">Purchase<i class="fa fa-angle-right"></i></a>

          <p class="ps-product__sharing">Share with:<a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-google-plus"></i></a><a href="#"><i class="fa fa-twitter"></i></a></p>

        </footer>

      </div>

</div>

</div>
<!--
<div class="mfp-with-anim modal-popup mfp-hide" id="modal--subscribe">

<button class="modal-close"><i class="fa fa-remove"></i></button><img src="images/img-demo-4.png" alt="">

<form action="http://nouthemes.com/html/bakery/_action" method="post">

  <h3>STAY UP-TO-DATE  WITH OUR NEWLETTER</h3>

  <p>Follow us & get <span> 20% OFF </span> coupon for first purchase !!!!!</p>

  <div class="form-group">

    <input class="form-control" type="text" placeholder="Type your email...">

    <button class="ps-btn ps-btn--sm">Subscribe</button>

  </div>

</form>

</div> -->

</div>

<!-- JS Library-->
 <script type="text/javascript">
 var My_variable='<?php echo  $this->session->userdata('userAuth'); ?>';
  var base_url='<?php echo base_url(); ?>';
  var setCurrency = '<?php echo $currencysymbol; ?>';
    </script>

<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/overflow-text.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/imagesloaded.pkgd.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/Magnific-Popup/dist/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/slick/slick/slick.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/lightGallery-master/dist/js/lightgallery-all.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAx39JFH5nhxze1ZydH-Kl8xXM3OK4fvcg&amp;region=GB"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/js/main.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>Extra/bakery/js/validation.js"></script>

<script src="<?php echo base_url() ?>Extra/bakery/js/maininit.js"></script>
<script src="<?php echo base_url() ?>Extra/bakery/js/intlTelInput.js"></script>
<!-- <script src="<?php echo base_url() ?>Extra/searchkeyword.js"></script> -->
<script type="text/javascript">


    $('#searchboxsuggestionproduct #searchfiltertext').on('keyup',function(){


     var val = $(this).val();
     val = val.replace(/^\s|\s+$/, "");
     if (val) {
      $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-spinner fa-spin"></i>');
       $.ajax({
       type: "Get",
       url: '<?=base_url();?>search?key='+val,
       dataType: "json",
       success: function(data) {
         if(data){
         var entery = data['entries'];
         var cartstatus = data['cart'];
         $('#searchfilterdataheader').html('').show();
         $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
         if(entery){
           var allcat='';
           var cat='';

           for (var i=0;i<entery.length;i++) {
             if(cartstatus=='yes'){
               var cartbtn='<a   data-variant="'+entery[i]['clientLevelId']+'" data-cart="'+entery[i]['id']+'" title="Add cart" class="linkcat_search_cart Add_to_cart_button"><i class="fa fa-shopping-bag" aria-hidden="true"></i></a>';
             }

           if(entery[i]['category']=="Category"){
                   cat+=' <li class="desktop-suggestion null"><a  href="'+entery[i]['action']+'" class="linkcat_search" title="'+entery[i]['name']+'">'+entery[i]['name']+'</a></li>'
               }
               else{
                 allcat+=' <li class="desktop-suggestion null"><a href="'+entery[i]['action']+'" class="linkcat_search" title="'+entery[i]['name']+'">'+entery[i]['name']+'</a>'+cartbtn+'</li>'
               }

             }
             // console.log(cat);
             if(cat || allcat){

                 if(allcat){
                     // <ul class="desktop-group">
                     allcat = ' <li class="desktop-suggestionTitle">All Others</li>'+allcat;

                 }
                 if(cat){
                   cat = ' <li class="desktop-suggestionTitle">Categories</li>'+cat;
                 }

                   $('#searchfilterdataheader').html('<ul class="desktop-group">'+allcat+cat+'</ul>');

             }
             else{
               $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
             }

         }
         else{
           $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
         }
       //  console.log(data['entries']);

          }
       },
       error: function(){
         $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
         $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
       }
   });



     } else {
       $('#searchfilterdataheader').html('').hide();
       $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
     }
   });


   function searchForData(value, isLoadMoreMode) {
     // create the ajax object
     ajax = new XMLHttpRequest();
     // the function to execute on ready state is changed
     ajax.onreadystatechange = function() {
       if (this.readyState === 4 && this.status === 200) {
         try {
           var json = JSON.parse(this.responseText)
         } catch (e) {
           noUsers();
           return;
         }

         if (json.length === 0) {
           if (isLoadMoreMode) {
             alert('No more to load');
           } else {
             noUsers();
           }
         } else {
           showUsers(json);
         }


       }
     }
     // open the connection
     ajax.open('GET', '<?=base_url();?>search?key=' + value + '&startFrom=' + loadedUsers , true);
     // send
     ajax.send();
   }

   function showUsers(data) {
     // the function to create a row
     //console.log(data);


     // loop through the data

   }



 </script>

 <!-- update customer visit -->
 <script type="text/javascript">
 $(document).ready(function(){
   $.ajax({
               type: "POST",
               url: base_url+"Logout/visitorCount",
               success: function(data) {

               }
               });
 })
 </script>
 <!-- CLient side form validation -->
 <script type="text/javascript">
 $(document).ready(function(){
   $(document).on('submit','#add_Quotation_dataform',function(){
     var check_required_field='';
     $(".required_validation_for_quotation").each(function(){
       var val22 = $(this).val();
       if (!val22){
         check_required_field =$(this).size();
         $(this).css("border-color","#ccc");
         $(this).css("border-color","red");
       }
       $(this).on('keypress change',function(){
         $(this).css("border-color","#ccc");
       });
     });
     if(check_required_field)
     {
       return false;
     }
     else {
       return true;
     }
   });
 });
 </script>

 <script>
     $(".phone_telephone").intlTelInput({
       // allowDropdown: false,
       // autoHideDialCode: false,
       // autoPlaceholder: "off",
       // dropdownContainer: "body",
       // excludeCountries: ["us"],
       // formatOnDisplay: false,
       // geoIpLookup: function(callback) {
       //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
       //     var countryCode = (resp && resp.country) ? resp.country : "";
       //     callback(countryCode);
       //   });
       // },
       // hiddenInput: "full_number",
       // initialCountry: "auto",
       // nationalMode: false,
       // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
       // placeholderNumberType: "MOBILE",
       // preferredCountries: ['cn', 'jp'],
       // separateDialCode: true,
       utilsScript: "<?php echo base_url() ?>Extra/apparels/js/utils.js"
     });
   </script>


</body>

</html>
