<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shippingmodel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
    }
    public function Getwarehouse($warehousId){
        $this->db->select('*')->from('houdinv_shipping_warehouse');
        $this->db->where('id', $warehousId);
         $getSearchData = $this->db->get()->row();
                return $getSearchData; 

    }

     public function save_shipingruls($data){
     	if($data['id']){
          $this->db->where('id', $data['id']);
     	
$getInsertData = $this->db->update('houdinv_shipping_ruels',$data);  
         }else{
         	$getInsertData = $this->db->insert('houdinv_shipping_ruels',$data);
         }
            if($getInsertData == 1)
            {
                 return ture;
            }
            else
            {
                return false;
            }}

             public function fetchshippingruleRecord(){
$this->db->select('*')->from('houdinv_shipping_ruels');
 $getSearchData = $this->db->get()->result();
        return $getSearchData;

 }


 public function save_Warehouses_list($data){
     	if($data['id']){
          $this->db->where('id', $data['id']);
     	
$getInsertData = $this->db->update('houdinv_shipping_warehouse',$data);  
         }else{
         	$getInsertData = $this->db->insert('houdinv_shipping_warehouse',$data);
         }
            if($getInsertData == 1)
            {
                 return ture;
            }
            else
            {
                return false;
            }}

public function fetchwarehouseRecord(){
/*$this->db->select('*')->from('houdinv_shipping_warehouse');
 $getSearchData = $this->db->get()->result();
        return $getSearchData;*/


        $this->db->select('*');    
$this->db->from('houdinv_shipping_warehouse as A');
$this->db->join('houdinv_countries as B', 'A.w_pickup_country = B.country_id');
$this->db->join('houdinv_states as C', 'A.w_pickup_state = C.state_id');
$getSearchData = $this->db->get()->result();
 return $getSearchData;
 }

 public function country_addlist($value='')
 {
 	$this->db->select('*')->from('houdinv_countries');
 $getSearchData = $this->db->get()->result();
        return $getSearchData;
 }


 public function state_addlist($value='')
 {
 	$this->db->select('*')->from('houdinv_states');
 	$this->db->or_where('country_id',$value);
 $getSearchData = $this->db->get()->result();
        return $getSearchData;
 }

public function delete_warehouse($id)
 {
      $this->db->where('id', $id);
        $delete_data= $this->db->delete('houdinv_shipping_warehouse');
      if($delete_data == 1)
            {
                 return ture;
            }
            else
            {
                return false;
            }
 }


 public function fetchwarehouseRecord_edit($value_data){

        $this->db->select('*');    
$this->db->from('houdinv_shipping_warehouse as A');
$this->db->join('houdinv_countries as B', 'A.w_pickup_country = B.country_id');
$this->db->join('houdinv_states as C', 'A.w_pickup_state = C.state_id');
$this->db->where('A.id',$value_data);
$getSearchData = $this->db->get()->result();
 return $getSearchData;
 }
 public function updatepostmenKey($data,$id)
 {
    if($id)
    {
        $this->db->where('houdinv_shipping_credentials_id',$id);
        $getStatus = $this->db->update('houdinv_shipping_credentials',$data);
    }
    else
    {
        $getStatus = $this->db->insert('houdinv_shipping_credentials',$data);
    }
    return $getStatus;
 }
 public function getPostemanKey()
 {
     $getPostmenkey = $this->db->select('*')->from('houdinv_shipping_credentials')->where('houdinv_shipping_credentials_id','1')->get()->result();
     return array('data'=>$getPostmenkey);
 }

}
?>