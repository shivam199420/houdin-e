<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Discountmodel extends CI_Model{
  function __construct() 
  {
    parent::__construct();
    $this->load->helper('database');
    $getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
    $this->userTbl = '';
    $this->masterDB = $this->load->database('master', TRUE);
  }


  public function fetchstore_category($data)
  {
 $this->db->select('*')->from('houdinv_categories')->where(array('houdinv_category_status'=>'active')); 
 $getcategories = $this->db->get()->result();

   $this->db->select('*')->from('houdinv_storediscount')->where($data);
   $_storediscount=$this->db->get()->result();

   $this->db->select('*')->from('houdinv_users')->where(array('houdinv_user_is_active'=>'active'));
   $houdinv_users=$this->db->get()->result();


 return array('categorieslist'=>$getcategories,'storediscount'=>$_storediscount,'List_users'=>$houdinv_users);
}
public function fetchstore_products($value)
{
 $this->db->select('houdin_products_id,houdin_products_title')->from('houdinv_products')->where_in('houdin_products_category',$value); 
 $getproducts = $this->db->get()->result();
 return $getproducts;
}


public function save_storediscount($value)
{
	$getInsertData = $this->db->insert('houdinv_storediscount',$value); 
 if($getInsertData == 1)
            { return ture;}else{return false;}}


public function delete_storediscount($value='')
{

$this->db->where('discount_id', $value);
$delete_data= $this->db->delete('houdinv_storediscount');
if($delete_data == 1)
{return ture;}else{return false;}}


public function editfatch_storediscount($values)
{
	 $this->db->select('*')->from('houdinv_storediscount')->where('discount_id',$values);;
   $editfatch=$this->db->get()->result();
    $editfatchs= $editfatch[0];   
        $discount_start=  date("Y-m-d",$editfatchs->discount_start);
         $discount_end=  date("Y-m-d",$editfatchs->discount_end);


              $product_cat_id=$editfatchs->product_cat_id;
                $product_id=$editfatchs->product_id;     
   $product_cat_ids=json_decode($product_cat_id);
    $product_ids=json_decode($product_id); 
    $customer_id=$editfatchs->customer_id;
    $customer_ids=json_decode($customer_id);
 return $arrayName = array('customer_id'=>$customer_ids,'discount_id' => $editfatchs->discount_id,'discount_amount'=> $editfatchs->discount_amount,'discount_name'=> $editfatchs->discount_name,'discount_status'=> $editfatchs->discount_status,'discount_start'=> $discount_start,'discount_end'=> $discount_end,'product_cat_id'=>$product_cat_ids,'product_id'=>$product_ids );
}


public function edit_storediscount($value)
{
	$this->db->where('discount_id',$value['discount_id']);
$getInsertData = $this->db->update('houdinv_storediscount',$value); 
 if($getInsertData == 1)
            { return ture;}else{return false;}}



 } ?>