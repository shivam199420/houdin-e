<?php

class Migrate extends CI_Controller
{

        public function index()
        {
                $this->load->library('migration');
                $this->migration->version(0);
                if ($this->migration->current() === FALSE)
                {
                        show_error($this->migration->error_string());
                }
        }

}