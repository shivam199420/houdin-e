<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!--START Page-Title -->
            <div class="row">
               
                <div class="col-md-8">
                   <h4 class="page-title">Delivery Setting</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li class="active">Delivery Setting</li>
                  </ol>
                  </div>
                
            </div>
           <!--END Page-Title -->


        <div class="row m-t-20">
          <div class="col-md-12">

            <div class="card-box table-responsive">
              <div class="btn-group pull-right m-t-10 m-b-20">
                   <a href="" data-toggle="modal" data-target="#Bulkupload" class="btn btn-default m-r-5" title="Bulk Upload"><i class="fa fa-file-pdf-o"></i></a>
                  <button type="button" class="btn btn-default m-r-5" title="Add Pincode" data-toggle="modal" data-target="#addpincode"><i class="fa fa-plus"></i></button>



              </div>
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>

                    <th>Pincode</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  <tr>

                    <td>Wrist Watch</td>
                    <td>Digital</td>



                    <td>

                      <button type="button" class="btn btn-default m-r-5" title="Delete" data-toggle="modal" data-target="#delete">Delete</button>

                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>


<!--Add Discount-->
<div id="addpincode" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <form method="post">
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

        <h4 class="modal-title">Add Pincode</h4>

        </div>

        <div class="modal-body">



        <div class="row form-horizontal">
        <div class="col-md-12">

       <div class="form-group">
        <label class="col-md-4 control-label">Pincode</label>
        <div class="col-md-8">
        <input type="text" class="form-control" placeholder="Pincode" />
        </div>
        </div>
        </div>
        </div>
        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Submit">

        </div>

        </div>
        </form>
        </div>

        </div>

       <!--Delete-->

  <div id="delete" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
          <form method="post">
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete Pincode</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this discount?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Delete">

          </div>

          </div>
          </form>
          </div>

          </div>

                <!--Edit-->

  <div id="Bulkupload" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
          <form method="post">
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Bulk Upload</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
            <div class="row form-horizontal text-center m-b-20">
              <button type="button" class="btn btn-default">Download CSV</button>
            </div>
          <div class="row form-horizontal">
        <div class="col-md-12">


                    <div class="form-group">
        <label class="col-md-4 control-label">Upload CSV</label>
        <div class="col-md-8">
        <input type="File" value="" class="form-control" name="">
        </div>
        </div>




        </div>
        </div>
          </div>
          </div>
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Save">

          </div>

          </div>
          </form>
          </div>

          </div>


<?php $this->load->view('Template/footer.php') ?>
