<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Adminmanagement extends CI_Controller 
{ 	 
    function __construct()
    {
		parent::__construct();
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        $this->load->helper('passwordhash');
        $this->load->model('Adminmanagementmodel');
        $this->from = fromemail();
	}  
    public function department()
    {
        // delete department
        if($this->input->post('deleteDepartment'))
        {
            $this->form_validation->set_rules('deleteId','text','required|integer');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->Adminmanagementmodel->deleteDepartmentData($this->input->post('deleteId'));
                if($getDeleteStatus)
                {
                    $this->session->set_flashdata('success','Department deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Adminmanagement/department", 'refresh');
                }
                else    
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Adminmanagement/department", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Adminmanagement/department", 'refresh');
            }
        }
        $getDepartmentdata = $this->Adminmanagementmodel->fetchTotalDepartment();
        $this->load->view('department',$getDepartmentdata);
    }
    public function adddepartment()
    {
        // add department
        if($this->input->post('addDepartmentData'))
        {
            $this->form_validation->set_rules('departmentName','department name','required');
            $this->form_validation->set_rules('departmentStatus','department status','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $getAccess = $this->input->post('acces');
                if(count($getAccess) > 0)
                {
                   $setData = strtotime(date('Y-m-d'));
                   $getDepartmentId = $this->input->post('departmentId');
                   $setAccess = implode(',',$getAccess);
                   if($getDepartmentId)
                   {
                       $setUpdatedArray = array('houdin_admin_department_name'=>$this->input->post('departmentName'),
                       'houdin_admin_department_access'=>$setAccess,'houdin_admin_department_status'=>$this->input->post('departmentStatus'),'houdin_admin_department_updated_at'=>$setData);
                       $getDepartmentUpdate = $this->Adminmanagementmodel->updateDepartmentData($setUpdatedArray,$getDepartmentId);
                   }
                   else
                   {
                       $setUpdatedArray = array('houdin_admin_department_name'=>$this->input->post('departmentName'),
                       'houdin_admin_department_access'=>$setAccess,'houdin_admin_department_status'=>$this->input->post('departmentStatus'),'houdin_admin_department_created_date'=>$setData);
                       $getDepartmentUpdate = $this->Adminmanagementmodel->updateDepartmentData($setUpdatedArray);
                   }
                   if($getDepartmentUpdate)
                   {
                       $this->session->set_flashdata('success','Department addedd successfully');
                       $this->load->helper('url');
                       redirect(base_url()."Adminmanagement/department", 'refresh');
                   }
                   else
                   {
                       $this->session->set_flashdata('error','Something went wrong. Please try again');
                       $this->load->helper('url');
                       redirect(base_url()."Adminmanagement/adddepartment", 'refresh');
                   }
                }
                else
                {
                    $this->session->set_flashdata('error','The department access field is required');
                    $this->load->helper('url');
                    redirect(base_url()."Adminmanagement/adddepartment", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."Adminmanagement/adddepartment", 'refresh');
            }
            
        }
        if($this->uri->segment('3'))
        {
            $getEditData = $this->Adminmanagementmodel->getDepartmentData($this->uri->segment('3'));
            $this->load->view('adddepartment',$getEditData);
        }
        else
        {
            $this->load->view('adddepartment');
        }
    }
    public function subadmins()
    {
        // add subadmin 
        if($this->input->post('addSubAdminData'))
        {
            $this->form_validation->set_rules('username','user name','required');
            $this->form_validation->set_rules('usercontact','user contact','required');
            $this->form_validation->set_rules('useremail','user email','required|valid_email');
            $this->form_validation->set_rules('departments','departments','required');
            $this->form_validation->set_rules('userstatus','user status','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setData = strtotime(date('Y-m-d'));
                $getSubadminId = $this->input->post('subadminid');
                if($getSubadminId)
                {
                    $setInserArray = array('houdin_admin_user_name'=>$this->input->post('username'),'houdin_admin_user_email'=>$this->input->post('useremail'),
                    'houdin_admin_user_phone'=>$this->input->post('usercontact'),'houdin_admin_user_department'=>$this->input->post('departments'),
                    'houdin_admin_user_status'=>$this->input->post('userstatus'),'houdin_admin_user_updated_at'=>$setData);
                    $getUpdateStatus = $this->Adminmanagementmodel->insertsubAdminData($setInserArray,$getSubadminId);
                }
                else
                {
                    $this->form_validation->set_rules('useremail','user email','required|valid_email|is_unique[houdin_admin_users.houdin_admin_user_email]');
                    if($this->form_validation->run() == true)
                    {
                        $letters1='abcdefghijklmnopqrstuvwxyz'; 
                        $string1=''; 
                        for($x=0; $x<3; ++$x)
                        {  
                            $string1.=$letters1[rand(0,25)].rand(0,9); 
                        }
                        $getAdminP = $string1;
                        $getEncryptedPass = getHashedPassword($getAdminP);
                        $setInserArray = array('houdin_admin_user_name'=>$this->input->post('username'),'houdin_admin_user_email'=>$this->input->post('useremail'),
                        'houdin_admin_user_phone'=>$this->input->post('usercontact'),'houdin_admin_user_password'=>$getEncryptedPass,'houdin_admin_user_role_id'=>'1',
                        'houdin_admin_user_department'=>$this->input->post('departments'),'houdin_admin_user_status'=>$this->input->post('userstatus'),'houdin_admin_user_created_at'=>$setData);
                        $getUpdateStatus = $this->Adminmanagementmodel->insertsubAdminData($setInserArray);
                    }
                    else
                    {
                        $this->session->set_flashdata('error',validation_errors());
                        $this->load->helper('url');
                        redirect(base_url()."Adminmanagement/subadmins", 'refresh');
                    }
                    
                }
                if($getUpdateStatus)
                {
                    $getsendgrid = sendgrid();
                    $getLogo = logo();
                    // send email data
                    $url = 'https://api.sendgrid.com/';
                    $user = $getsendgrid[0]->houdin_sendgrid_username;
                    $pass = $getsendgrid[0]->houdin_sendgrid_password;
                    $json_string = array(
                    'to' => array(
                        $this->input->post('useremail')
                    ),
                    'category' => 'test_category'
                    );
                    $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                                <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                                <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                                    <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                                        <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Hello '.$this->input->post('username').' </strong></td></tr>
                                            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Your shop details are:</strong></td></tr>
                                                
                                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Login URL:</td>
                                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.base_url().'</td>
                                                </tr>
                                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Username:</td>
                                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->input->post('useremail').'</td>
                                                </tr>
                                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Password:</td>
                                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$getAdminP.'</td>
                                                </tr>
                                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                            <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                            <table width="100%" ><tr >
                            <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e '.date('Y').'--All right reserverd </td></tr>
                            <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
                            </tr></table>
                            </div>
                            </div></td></tr></table>';
                    $params = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'x-smtpapi' => json_encode($json_string),
                        'to'        => $this->input->post('useremail'),
                        'fromname'  => 'Houdin-e',
                        'subject'   => 'Subadmin Credentials',
                        'html'      => $htm,
                        'from'      => $this->from,
                    );
                    $request =  $url.'api/mail.send.json';
                    $session = curl_init($request);
                    curl_setopt ($session, CURLOPT_POST, true);
                    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                    curl_setopt($session, CURLOPT_HEADER, false);
                    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($session);
                    curl_close($session);
                    $getResponse = json_decode($response);
                    if($getResponse->message == 'success')
                    {
                        $this->session->set_flashdata('success','Subadmins are addedd successfully');
                        $this->load->helper('url');
                        redirect(base_url()."Adminmanagement/subadmins", 'refresh');
                    }
                    else
                    {
                        $this->session->set_flashdata('error','Subadmins are addedd successfully.Email is not sent');
                        $this->load->helper('url');
                        redirect(base_url()."Adminmanagement/subadmins", 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Adminmanagement/subadmins", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."Adminmanagement/subadmins", 'refresh');
            }
        }
        // delete subamdoin
        if($this->input->post('deleteSubadmins'))
        {
            $this->form_validation->set_rules('deleteSubadminId','text','required');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->Adminmanagementmodel->deleteSubadmins($this->input->post('deleteSubadminId'));
                if($getDeleteStatus)
                {
                    $this->session->set_flashdata('success','Subadmin deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."Adminmanagement/subadmins", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."Adminmanagement/subadmins", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."Adminmanagement/subadmins", 'refresh');
            }
        }
        $getSubAdminsdata = $this->Adminmanagementmodel->getSubadminsDataValue();
        $this->load->view('subadmins',$getSubAdminsdata);
    }
}
