<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <div class="btn-group pull-right m-t-15">
                                <?php 
                                if(!count($masterLogo) > 0)
                                {
                                ?>    
                                <a href="" class="btn btn-default btn-sm m-l-5" data-toggle="modal" data-target="#addMasterLogo"> <i class="fa fa-plus m-r-5"></i>Add Logo</a>
                                <?php }
                                ?>
                                </div>
                                <h4 class="page-title">Logo Management</h4>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                          <?php  ?>
                            <div class="card-box table-responsive">
                              <table class="table table-striped table-bordered table_shop_custom">
                                <thead>
                                  <tr>
                                    <th>Logo</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td><img src="../uploads/logo/<?php echo $masterLogo[0]->houdin_admin_logo_image ?>" style="height:60px;width:60px"/></td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-warning btn btn-sm" data-toggle="modal" data-target="#updateAdminLogoData"><i class="fa fa-refresh m-r-5"></i>Update</a>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->
                <div id="updateAdminLogoData" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                      <?php echo form_open(base_url( 'Setting/logomanagement' ), array( 'id' => 'editLogoModal','enctype'=>'multipart/form-data'));?>
                            <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                  <h4 class="modal-title">Update Logo</h4>
                                </div>
                            <div class="modal-body">
                              <div class="row">
                                  <div class="col-md-12">
                                      <div class="form-group no-margin">
                                          <label for="field-7" class="control-label">Logo</label>
                                          <input type="file" class="form-control required_validation_for_edit_master_logo" name="name" />
                                      </div>
                                  </div>
                              </div>
                              </div>

                          <div class="modal-footer">
                              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                              <input type="submit" class="btn btn-info" name="updateAdminLogoBtn" value="Submit">
                          </div>

                     </div>
                  </form>
                 </div>
                </div>
<div id="addMasterLogo" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
      <?php echo form_open(base_url( 'Setting/logomanagement' ), array( 'id' => 'addLogoModal','enctype'=>'multipart/form-data'));?>
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                  <h4 class="modal-title">Add Logo</h4>
                </div>
            <div class="modal-body">
              <div class="row">
                  <div class="col-md-12">
                      <div class="form-group no-margin">
                          <label for="field-7" class="control-label">Logo</label>
                          <input type="file" class="form-control required_validation_for_add_master_logo" name="name" />
                      </div>
                  </div>
              </div>
              </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-info" name="addMasterAdminLogoBtn" value="Submit">
          </div>

     </div>
  <?php echo form_close(); ?>
 </div>
</div>
<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addLogoModal',function(){
            var check_required_field='';
            var checkCheckeddata = 0;
			$(".required_validation_for_add_master_logo").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
                return true;
			}
		});
	});
	</script>
    <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#editLogoModal',function(){
            var check_required_field='';
            var checkCheckeddata = 0;
			$(".required_validation_for_edit_master_logo").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
                return true;
			}
		});
	});
	</script>