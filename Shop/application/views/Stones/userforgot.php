<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>

<!-- Page item Area -->
<div id="page_item_area">
  <div class="page-location">
      <div class="page-location-layer">
          <div class="container">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="page-location-title">
                          <h3>change Password</h3>
                      </div>

                  </div>
              </div>
          </div>
      </div>
  </div>
</div>


<!-- Login Page -->
<div class="login_page_area">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3"></div>
<div class="col-md-6 col-sm-6 col-xs-12">
<div class="create_account_area">
<h2 class="caa_heading">Change Password</h2>
<?php
    if($this->session->flashdata('error'))
    {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
    }
    if($this->session->flashdata('success'))
    {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    ?>
    <?php echo form_open(base_url( 'Forgot/updatePass' ), array( 'id' => 'forgotPass', 'method'=>'post' ));?>
<div class="caa_form_area">
<p>Please enter the following details.</p>
<div class="caa_form_group">
<div class="form-group">
<label>Pin</label>
<div class="input-area">
    <input type="text" name="forgotPin" class="required_validation_for_user_forgot name_validation number_validation form-control" maxlength="4" />
</div>
</div>
<div class="form-group">
<label>New Password</label>
<div class="input-area">
    <input type="password" name="forgotPass" class="required_validation_for_user_forgot name_validation opass form-control" />
</div>
</div>
<div class="form-group">
<label>Confirm Password</label>
<div class="input-area">
    <input type="password" name="forgotConPass" class="required_validation_for_user_forgot name_validation cpass form-control"  />
</div>
</div>
<div class="form-group">
<input type="submit" class="btn porduct-place-order-btn acc_btn text-center" value="Submit"/>
</div>
</div>
<?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</div>
</div>
<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/footer'); ?>
 <!-- CLient side form validation -->
<script type="text/javascript">
$(document).ready(function(){
    $(document).on('submit','#forgotPass',function(){
        var check_required_field='';
        $(this).find(".required_validation_for_user_forgot").each(function(){
            var val22 = $(this).val();
            if (!val22){
                check_required_field ='error';
                $(this).css("border-color","#ccc");
                $(this).css("border-color","red");
            }
            $(this).on('keypress change',function(){
                $(this).css("border-color","#ccc");
            });
        });
        if(check_required_field)
        {
            return false;
        }
        else {
            return true;
        }
    });
});
</script>
