<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName.'/Template/header'); ?>

<?php // $this->load->view($Template.'/Template/header'); ?>




    <div id="back2top"><i class="fa fa-angle-up"></i></div>

<div class="loader"></div>

<div class="page-wrap">

  <!-- Heros-->

  <div class="ps-section--hero"><img src="<?=base_url();?>Extra/<?=strtolower($Template);?>/images/hero/02.jpg" alt="">

    <div class="ps-section__content text-center">

      <h3 class="ps-section__title">Login Form Details</h3>

      <div class="ps-breadcrumb">

        <ol class="breadcrumb">

          <li><a href="<?=base_url();?>">Home</a></li>

          <li><a href="<?=base_url();?>Register">Login</a></li>

          <li class="active">Register</li>

        </ol>

      </div>

    </div>

  </div>


  <div class="ps-section ps-section--order-form pt-80 pb-80">
  <div class="row">
        <div class="container">
        <div class="col-sm-12">
        <?php
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>
        </div>
    <div class="container">

      <div class="ps-section__header text-center mb-50">

        <div class="ps-section__top">Making People Happy</div>



      </div>

      <div class="ps-section__content">


          <div class="row">


   <?php echo form_open(base_url( 'Register/checkUserAuth' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

                  <h3 class="text-center">LOGIN FORM</h3>

                  <hr>

                  <div class="form-group">

                    <label>Email Address <span>*</span></label>

                    <input  name="userEmail" class="required_validation_for_user name_validation email_validation form-control" type="text" placeholder=" Enter your email here.">

                  </div>

                  <div class="form-group">

                    <label>Password <span>*</span></label>

                    <input name="userPass" class="required_validation_for_user name_validation form-control" type="password" placeholder=" Enter your password here.">

                  </div>

                  <div class="form-group">

                    <p><a href="#"title="Recover your forgotten password"  data-toggle="modal" data-target="#myModal">Forgot password ?</a></p>

                  </div>

                  <div class="form-group text-center">

                    <button class="ps-btn acc_btn">Login<i class="fa fa-angle-right"></i></button>

                  </div>

                </div>

  <?php echo form_close(); ?>
  <?php echo form_open(base_url( 'Register/addShopUsers' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

                  <h3 class="text-center">Register FORM</h3>

                  <hr>

                  <div class="form-group">

                    <label>Firstname<span>*</span></label>

                    <input class="form-control required_validation_for_user name_validation" name="userFirstName" type="text" placeholder=" Enter your firstname here. ">

                  </div>

                  <div class="form-group">

                    <label>Lastname <span>*</span></label>

                    <input class="form-control required_validation_for_user name_validation" name="userLastName" type="text" placeholder=" Enter your lastname here. ">

                  </div>


                  <div class="form-group">

                    <label>Your Email</label>

                    <input class="form-control required_validation_for_user name_validation email_validation" name="userEmail" type="text" placeholder=" Enter your email here. ">

                  </div>

                  <div class="form-group">

                    <label>Password</label>

                    <input class="form-control required_validation_for_user name_validation opass" name="userPass" type="password" placeholder=" Enter your password here. ">

                  </div>

                  <div class="form-group">

                    <label>Confirm Password</label>

                    <input class="form-control required_validation_for_user name_validation cpass" name="userRetypePass"  type="password" placeholder=" Enter your confirm password here. ">

                  </div>


<div class="form-group">
        <label>Contact Number</label>
        <div class="form-control"><input type="text" class="form-control required_validation_for_user name_validation" name="userContact" placeholder="Enter Contact Number"/></div>
        </div>
                  <div class="form-group text-center">

                    <button class="ps-btn setDisableData" type="submit">Register<i class="fa fa-angle-right"></i></button>

                  </div>

                </div>
                <?php echo form_close(); ?>

          </div>



      </div>

    </div>

  </div>

  <!-- modal -->

          <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <?php echo form_open(base_url( 'Register/checkUserForgot' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Forgot your password</h4>
        <button type="button" class="close" data-dismiss="modal" style="float: right;">&times;</button>
        </div>
        <div class="modal-body">
        <input placeholder="Please enter your Email address" name="emailForgot" class="form-control required_validation_for_user name_validation email_validation" type="text" style="width:100%;">
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-default acc_btn ps-btn" style="float: left" value="Submit"/>
        </div>
        </div>
        <?php echo form_close(); ?>

            </div>
          </div>







          <?php
          $getShopName = getfolderName($this->session->userdata('shopName'));
          $this->load->view($getShopName.'/Template/footer'); ?>
<?php // $this->load->view($Template.'/Template/footer'); ?>
        <!-- CLient side form validation -->
        <script type="text/javascript">
        $(document).ready(function(){
                $(document).on('submit','#addShopUsers',function(){
                        var check_required_field='';
                        $(this).find(".required_validation_for_user").each(function(){
                                var val22 = $(this).val();
                                if (!val22){
                                        check_required_field =$(this).size();
                                        $(this).css("border-color","#ccc");
                                        $(this).css("border-color","red");
                                }
                                $(this).on('keypress change',function(){
                                        $(this).css("border-color","#ccc");
                                });
                        });
                        if(check_required_field)
                        {
                                return false;
                        }
                        else {
                                return true;
                        }
                });
        });
                </script>
