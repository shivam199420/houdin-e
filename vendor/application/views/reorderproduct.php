<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style type="text/css">
  @media(max-width:767px){
    .width100{
      width: 100%;
    }
  }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

           <!--START Page-Title -->
            <div class="row">
             
                <div class="col-md-8">
                   <h4 class="page-title">Reorder</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
              <li><a href="<?php echo base_url(); ?>inventory/Inventorylanding">Inventory</a></li>
                  <li><a href="<?php echo base_url(); ?>inventory/lowinventory">Low Inventory</a></li>
                 
                  <li class="active">Reorder</li>
                  </ol>  
                  </div>
                 
            </div>

<div class="row">
    <div class="col-sm-12">
    <?php 
    if($this->session->flashdata('success'))
    {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    else if($this->session->flashdata('error'))
    {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
    }
    ?>
    </div>
    </div>

           <!--END Page-Title --> 
     <?php echo form_open(base_url('inventory/reorderproduct_send'), array( 'id' => 'reorderproduct_send', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
              
        <div class="row m-t-20">
          <div class="col-md-12">

            <div class="card-box table-responsive">
             
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th><input type="checkbox" class="All_check" id="All_check_reorderproduct"></th>
                    <th>Product Name</th>
                    <th>present Quantity</th>
                    <th>Select supplier</th>
                    <th>Quantity</th>
                   
                  </tr>
                </thead>
                <tbody>  

                <?php 
    
       foreach ($reorder as $mainproducts) {

       //print_r($mainproducts['product']);
          // foreach ($mainproducts as $key => $value) {
            $value= $mainproducts['product'];

               if ($value->houdinv_products_total_stocks) {
                   $setTotalStock = $value->houdinv_products_total_stocks;
               } elseif ($value->houdinv_products_variants_total_stocks) {
                   $setTotalStock = $value->houdinv_products_variants_total_stocks;
               } else {
                   $setTotalStock = 0;
               }
               // set product title
               if ($value->houdin_products_title) {
                   $setTitle = $value->houdin_products_title;
               } else {
                   $setTitle = $value->houdin_products_variants_title;
               } 
               //houdin_products_variants_product_id
               if($value->houdin_products_id){
               $main_product_id=$value->houdin_products_id;
               }else{
                $main_product_id=$value->houdin_products_variants_product_id;
               }
               ?>
                  <tr class="row_reorderproduct">
                    <td><input type="checkbox" class="single_check_reorderproduct" name="single__reorderproduct[]" value="<?=$main_product_id; ?>"></td>
                    <td><?=$setTitle; ?></td>
                    <td><?=$setTotalStock; ?></td>
                   
                    
                    <td>
                    <div class="form-group" style="margin-bottom: 0px;padding-bottom: 0px">
                    
                      <select class="form-control select2 suplier_option_reorderproduct" disabled="true" name="supplierss[]" id="">

                      <option value="">Select</option>
                      <?php 
                      foreach ( $mainproducts['supplier'] as $key => $supplierss) {
                          echo ' <option value="'.$supplierss->houdinv_supplier_id.'">'.$supplierss->houdinv_supplier_comapany_name.' ('.$supplierss->houdinv_supplier_contact_person_name.')</option>';
                      } ?>
                                            
                        </select>
                    </div> 
                    </td>

                <td>
                <input type="hidden" name="products_id[]" value="<?php if ($value->houdin_products_id) {
                          echo $value->houdin_products_id;
                      } else {
                          echo 0;
                      } ?>">
                <input type="hidden" name="variations_id[]" value="<?php if ($value->houdin_products_variants_id) {
                          echo $value->houdin_products_variants_id;
                      } else {
                          echo 0;
                      } ?>">
                <input type="text" class="form-control suplier_quantity_reorderproduct" placeholder="Quantity" disabled="true" name="quantity[]"></td>
                  </tr>

                  <?php
           }
      // }  ?>
                </tbody>

              </table>

 <div class="form-group">
    <label>Credit Period</label>
    <select class="form-control required_validation_for_reorderproduct" name="creditPeriod">
    <option value="">Choose Credit Period</option>
    <option value="one day">One Day</option>
    <option value="one week">One Week</option>
    <option value="one month">One Month</option>
    </select>
    </div>
    <div class="form-group">
    <label>Expected Delivery Date</label>
    <input type="text" class="form-control required_validation_for_reorderproduct date_picker name_validation" name="deliveryDate"/>
    </div>

              <div class="pull-right" style="width:100%">
              <button type="submit" class="btn btn-success waves-effect waves-light width100" name="purchase_order">Create purchase order</button>
              </div>
            </div>
          </div>
        </div>

<?php echo form_close(); ?>
      </div>
    </div>
  </div>



<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $('.row_reorderproduct .single_check_reorderproduct').on('change',function(){ 
    if($(this).is(':checked')){
    $(this).parents('.row_reorderproduct').find('select.suplier_option_reorderproduct,.suplier_quantity_reorderproduct').prop('disabled',false).addClass('required_validation_for_reorderproduct');
    }
    else{
        $(this).parents('.row_reorderproduct').find('select.suplier_option_reorderproduct,.suplier_quantity_reorderproduct').prop('disabled',true).removeClass('required_validation_for_reorderproduct');

    }
  });

  $('input[type="checkbox"]#All_check_reorderproduct').on('change',function(){
      if($(this).is(':checked')){
          $('.row_reorderproduct .single_check_reorderproduct').prop('checked',true).trigger('change');
      }
      else{
          $('.row_reorderproduct .single_check_reorderproduct').prop('checked',false).trigger('change');
      }
  });




$(document).on('submit','#reorderproduct_send',function(){
   
      var check_required_field='';
      $(".required_validation_for_reorderproduct").each(function(){
        var val22 = $(this).val();
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      if(check_required_field)
      {
        return false;
      }
      else 
      {
         return true;
      }

    });



});
</script>
