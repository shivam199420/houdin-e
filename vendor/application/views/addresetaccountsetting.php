<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/settingsidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Reset Account</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li class="active">Reset Account</li>
                  </ol>
                  </div>
                
            </div>
           <!--END Page-Title -->  
            <div class="row m-t-20">
        <div class="col-sm-12">
            <div class="card-box">

          <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
              <img src="<?php echo base_url(); ?>assets/images/24-hours-support.png" /><br/>
          <button type="button" class="btn btn-default btn-block m-t-20" style="background:#000 !important;border:1px solid #000 !important;">Contact houdin-e support</button>
          </div>
        </div>


        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('Template/footer.php') ?>
