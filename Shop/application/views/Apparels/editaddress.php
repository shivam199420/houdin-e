<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>

<!-- Page item Area -->
<div id="page_item_area">
<div class="container">
<div class="row">
<div class="col-sm-6 text-left">
<h3>Edit address</h3>
</div>
</div>
</div>
</div>


<!-- Login Page -->
<section class="checkout_page">
<div class="container">
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8">
<div class="title">
<h3>Edit address</h3>
</div>
<?php
if($this->session->flashdata('error'))
{
    echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
}
else if($this->session->flashdata('success'))
{
    echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
}
?>
<?php echo form_open(base_url( 'Address/edit/'.$this->uri->segment('3').'' ), array( 'id' => 'addaddress', 'method'=>'post' ));?>
<div class="form-row">
<div class="form-group col-md-12">
<input name="username" value="<?php echo $address[0]->houdinv_user_address_name ?>" placeholder="Name" class="form-control characterValidation required_validation_for_address name_validation" type="text">
</div>

 <div class="form-group col-md-12">
<input name="userphone" value="<?php echo $address[0]->houdinv_user_address_phone ?>" placeholder="Phone number" pattern="^[-+]?\d+$" class="form-control phone_telephone required_validation_for_address name_validation" type="text">
</div>

<div class="form-group col-md-12">
<textarea rows="3" name="usermainaddress" placeholder="Street address. Apartment, suite, unit etc. (optional)" class="form-control required_validation_for_address name_validation"><?php echo $address[0]->houdinv_user_address_user_address ?></textarea>
</div>

<div class="form-group col-md-12">
<input name="usercity" value="<?php echo $address[0]->houdinv_user_address_city ?>" placeholder="Town/City*" class="form-control required_validation_for_address name_validation" type="text">
</div>
<div class="form-group col-md-12">
<input name="userpincode" value="<?php echo $address[0]->houdinv_user_address_zip ?>" placeholder="Post code / Zip" class="form-control required_validation_for_address name_validation" type="text">
</div>
<div class="form-group col-md-12">
<select class="form-control required_validation_for_address" name="usercountry">
<option value="">Choose Country</option>
<option <?php if($address[0]->houdinv_user_address_country == $countryList[0]->houdin_user_shop_country) { ?> selected="selected" <?php  } ?> value="<?php echo $countryList[0]->houdin_user_shop_country ?>"><?php echo $countryList[0]->houdin_country_name ?></option>
</select>
</div>


<div class="form-group col-md-12 text-center">
    <input type="submit" name="editAddress" class="btn btn-default acc_btn" value="Update Address"/>
</div>

<?php echo form_close(); ?>
</div>
</div>
</div>
</section>
<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/footer'); ?>

<script type="text/javascript">
$(document).ready(function(){
    $(document).on('submit','#addaddress',function(){
        var check_required_field='';
        $(this).find(".required_validation_for_address").each(function(){
            var val22 = $(this).val();
            if (!val22){
                check_required_field =$(this).size();
                $(this).css("border-color","#ccc");
                $(this).css("border-color","red");
            }
            $(this).on('keypress change',function(){
                $(this).css("border-color","#ccc");
            });
        });
        if(check_required_field)
        {
            return false;
        }
        else {
            return true;
        }
    });
});
</script>
