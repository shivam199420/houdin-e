<?php 
$this->load->view('Template/header');
$this->load->view('Template/sidebar');
?>
<style>
.select2-container--default
{
    width:100% !important;
}
</style>
<div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">
        <!--START Page-Title -->
            <div class="row">
            <div class="col-md-8">
            <h4 class="page-title">Billed Order / <?php echo '<a href="'.base_url().'Order/vieworder/'.$mainArray[0]->houdinv_order_id.'">#'.$mainArray[0]->houdinv_order_id.'</a>' ?><small><?php echo date('d-m-Y',$mainArray[0]->houdinv_user_address_created_at) ?></small></h4>
            <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li><a href="<?php echo base_url(); ?>order/Orderlanding">Order</a></li>
            <li class="active">Billed Order</li>
            </ol>
            </div>
            
            </div>
            <!--END Page-Title --> 
        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>          
        <?php echo form_open(base_url( 'Order/billedconfirmation/'.$this->uri->segment('3').'' ), array( 'id' => 'billedOrderData', 'method'=>'post' ));?>
        <div class="row m-t-20">
        <div class="col-md-4">
        <div class="panel panel-default">
        <!-- <div class="panel-heading">
        <h4>Invoice</h4>
        </div> -->
        <div class="panel-body">

        <div class="clearfix"></div>
        <div class="multiple-section">
        <h4 class="multiple-section-h4">Customer Detail</h4>
        <div class="col-md-1"><i class="fa fa-user"></i></div>
        <div class="col-md-10"><p> <?php echo $mainArray[0]->houdinv_user_address_name?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-1"><i class="fa fa-phone"></i></div>
        <div class="col-md-10"><p><?php echo $mainArray[0]->houdinv_user_address_phone?></p></div>
        <div class="clearfix"></div>
        <!--<div class="col-md-1"><i class="fa fa-map-marker"></i></div>-->
        <!--<div class="col-md-10"><p><?php //echo $mainArray[0]->houdinv_user_address_user_address.",".$mainArray[0]->houdinv_user_address_city.",".$mainArray[0]->houdinv_user_address_zip ?></p></div>-->
        <!--<div class="clearfix"></div>-->
        </div>

        <div class="clearfix"></div>
        <div class="multiple-section">
        <h4 class="multiple-section-h4">Delivery Detail</h4>
        <div class="col-md-10"><p> <?php echo $mainArray[0]->houdinv_user_address_user_address.",<br/>".$mainArray[0]->houdinv_user_address_city." ".$mainArray[0]->houdinv_user_address_zip ?></p></div>
        <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>
        <div class="multiple-section">
        <h4 class="multiple-section-h4">Sub Total</h4>
        <div class="col-md-6">Additional Discount</div>
        <div class="col-md-6"><p> <?php echo $mainArray[0]->houdinv_orders_discount ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-6">Delivery Charges</div>
        <div class="col-md-6"><p><?php echo $mainArray[0]->houdinv_delivery_charge ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-6">Round Off</div>
        <div class="col-md-6"><p><?php echo floor($mainArray[0]->houdinv_orders_total_Amount) ?></p></div>
        <div class="clearfix"></div>
        <div class="col-md-6">Net Payable</div>
        <div class="col-md-6"><p><?php echo floor($mainArray[0]->houdinv_orders_total_Amount) ?></p></div>
        <div class="clearfix"></div>
        </div>
        
        </div>
        <!-- End panel body  -->
        </div>
        </div>
        <!-- end col-sm-4 -->
        <div class="col-md-8">
        <div class="card-box table-responsive">
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th>Product</th>
        <th>In Stock</th>
        <th>Ordered</th>
        <th>Confirmed</th>
        <th>Selling Price/Tax</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        print_r($unbilledData);
        ?>
        </tbody>
        </table>
        <div class="form-group">
        <label>Payment Status</label>
        <select class="form-control required_validation_for_billed" name="paymentStatus">
        <option value="">Choose Payment Status</option>
        <option <?php if($mainArray[0]->paymentStatus == 1) { ?> selected="selected" <?php } ?> value="1">Paid</option>
        <option <?php if($mainArray[0]->paymentStatus == 0) { ?> selected="selected" <?php } ?> value="0">Not Paid</option>
        </select>
        </div>
        <!-- <div class="form-group">
        <label>Delivery Date</label>
       <input type="text" class="form-control name_validation date_picker required_validation_for_billed" name="deliveryDate"/>
        </div> -->
        <div class="form-group">
        <label>Outlets</label>
        <select class="form-control" name="outletIdData">
        <option value="">Choose Outlet</option>
        <?php 
        if($this->session->userdata('vendorOutlet') == 0)
        {
            foreach($warehouseData as $warehouseDataList)
            {
            ?>
            <option value="<?php echo $warehouseDataList->id ?>"><?php echo $warehouseDataList->w_name ?></option>
            <?php } }
            else
            {
                foreach($warehouseData as $warehouseDataList)
                {
                    if($this->session->userdata('vendorOutlet') == $warehouseDataList->id)
                    {
                    ?>
                    <option value="<?php echo $warehouseDataList->id ?>"><?php echo $warehouseDataList->w_name ?></option>
                    <?php }
                    ?>

                <?php }
            }
            ?>
        </select>
        </div>
        <input type="submit" name="confirmBilledData" class="btn btn-success pull-right subClassBtnData" value="Bill It">
        
        </div>
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        </div>


        <!-- update delivery status -->
        <div id="updateDeliveryStatus" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title"><?php echo '#'.$mainArray[0]->houdinv_order_id ?></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleteCustomerId" name="deleteCustomerId"/>
        <h4><b>Do you want to Assign a Delivery for this order</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <a href="<?php echo base_url() ?>Order/unbilled" class="btn btn-default waves-effect pull-left">Assgin Later</a>
        <a href="javascript:;" class="btn btn-default waves-effect pull-right assignDelivery" data-dismiss="modal">Assgin for Delivery</a>
        </div>
        </div>
        </div>

        </div>
        <!-- end delievery statua -->
        <!-- tag delivery status -->
        <!--Start Delivery-->
        <div id="deliveryTagModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Order/billedconfirmation/'.$this->uri->segment('3').''), array( 'id'=>"deliveryTagData", 'method'=>'post' ));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Delivery Tag</h4>

        </div>

        <div class="modal-body">


        <div class="row">
        <div class="col-md-12">
        <form action="#">
        <div class="form-group setErrorMessage">

        </div>
        <div class="form-group">
        <label>Choose Delivery Type</label>
        <input type="hidden" value="<?php echo $this->uri->segment('3'); ?>" class="deliveryOrderId" name="deliveryOrderId"/>
        <input type="hidden" class="deliveryboyEmail" name="deliveryboyEmail"/>
        <select class="form-control required_validation_for_all_order setDeliveryType" name="deliveryType">
        <option value="">Choose Delivery Types</option>
        <option value="1">Courier Service</option>
        <option value="2">Delivery Boy</option>
        <option value="3">Customer Pickup</option>
        </select>
        </div>
        <div class="form-group">
        <label style="width:100% !important;">Choose Delivery Service</label>
        <select class="form-control select2 setAllOrdersdata" name="deliveryBoyId">
        
        </select>
        </div>
        </div>
        </div>

        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info" name="tagDeliveryBoys" value="Add tag">

        </div>

        </div>
        <?php echo form_close(); ?>
        </div>

        </div>
        <!-- end delivery status -->
<?php $this->load->view('Template/footer'); ?>
<script type="text/javascript">
$(document).ready(function(){
    $(".select2").select2();
    // $(document).on('click','.masterUnbilleCheck',function(){
    //     if($(this).prop('checked') == true)
    //     {
    //         $('.childUnbilleCheck').prop('checked',true);
    //         $('.quantityData').prop('disabled',false);
    //     }
    //     else
    //     {
    //         $('.childUnbilleCheck').prop('checked',false);
    //         $('.quantityData').prop('disabled',true);
    //     }
    // });
    // $(document).on('click','.childUnbilleCheck',function(){
    //     var setDataValue = 0;
    //     if($(this).prop('checked') == true)
    //     {   
    //         $(this).parents('.mainUnbilledRow').find('.quantityData').prop('disabled',false);
    //     }
    //     else
    //     {
    //         $(this).parents('.mainUnbilledRow').find('.quantityData').prop('disabled',true).val('');
    //     }
    //     $('.childUnbilleCheck').each(function(){
    //         if($(this).prop('checked') == false)
    //         {
    //             setDataValue++;
    //         }
    //     });
    //     if(setDataValue > 0)
    //     {
    //         $('.masterUnbilleCheck').prop('checked',false);
    //     }
    //     else
    //     {
    //         $('.masterUnbilleCheck').prop('checked',true);
    //     }
    // })
})
</script>
<!-- CLient side form validation -->
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#billedOrderData',function(){
			var check_required_field='';
			$(".required_validation_for_billed").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
        var getSessionData = '<?php echo $this->session->flashdata('deliveryStatus') ?>';
        if(getSessionData == '1')
        {
            $('#updateDeliveryStatus').modal({
                backdrop: 'static',
                keyboard: false,
                show: true 
            });
        }
        $(document).on('click','.assignDelivery',function(){
            $('#deliveryTagModal').modal({
                backdrop: 'static',
                keyboard: false,
                show: true 
            })
        })
	});
	</script>
    <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#deliveryTagData',function(){
			var check_required_field='';
			$(this).find(".required_validation_for_all_order").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
    </script>