<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopuserchangepass extends CI_Model{
    function __construct() 
    {
    }
    public function updatePassword($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getUserPass = $getDynamicWebDb->select('houdin_user_password_salt')->from('houdinv_users')->where('houdinv_user_id',$this->session->userdata('userAuth'))->get()->result();
        if(count($getUserPass) > 0)
        {
            $passwordSalt = $getUserPass[0]->houdin_user_password_salt;
            $cryptData= crypt($data['oldpass'],$passwordSalt);
            $checkPass = $getDynamicWebDb->select('houdinv_user_id')->from('houdinv_users')->where('houdinv_user_password',$cryptData)->get()->result();
            if(count($checkPass) > 0)
            {
                $getUserPassData = $data['newpass'];
                $letters1='abcdefghijklmnopqrstuvwxyz'; 
                $string1=''; 
                for($x=0; $x<3; ++$x)
                {  
                    $string1.=$letters1[rand(0,25)].rand(0,9); 
                }
                $saltdata = password_hash($string1,PASSWORD_DEFAULT);
                $pass = crypt($getUserPassData,$saltdata);
                $setData = strtotime(date('Y-m-d H:i:s'));
                $setUpdateArray = array('houdinv_user_password'=>$pass,'houdin_user_password_salt'=>$saltdata,'houdinv_user_updated_at'=>$setData);
                $getDynamicWebDb->where('houdinv_user_id',$checkPass[0]->houdinv_user_id);
                $getUpdateData = $getDynamicWebDb->update('houdinv_users',$setUpdateArray);
                if($getUpdateData == 1)
                {
                    return array('message'=>'yes');
                }
                else
                {
                    return array('message'=>'no');
                }
            }
            else
            {
                return array('message'=>'oldpass');
            }
        }
        else
        {
            return array('message'=>'no');
        }
    }
} 

?>