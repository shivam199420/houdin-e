<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style>
.select2.select2-container
{
  width:100% !important;
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

             <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Membership</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>coupons/Couponslanding">Coupons</a></li>
                  <li class="active">Membership</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                
            </div>
           <!--END Page-Title -->  
            <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
 
        ?>
        </div>
        </div>
            <div class="row m-t-20">
                    <div class="col-lg-4 col-sm-6">
                        <div class="widget-panel widget-style-2 bg-white">
                            <img src="<?php echo base_url(); ?>assets/images/allpageicon/Staff_member.png">
                            <h2 class="m-0 text-dark counter font-600"><?=$count_total->total;?></h2>
                            <div class="text-muted m-t-5">Total membership card </div>
                        </div> 
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="widget-panel widget-style-2 bg-white">
                            <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_membership.png">
                            <h2 class="m-0 text-dark counter font-600"><?=$count_active->active;?></h2>
                            <div class="text-muted m-t-5">Total active membership</div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="widget-panel widget-style-2 bg-white">
                            <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Membership.png">
                            <h2 class="m-0 text-dark counter font-600"><?=$count_deactive->deactive;?></h2>
                            <div class="text-muted m-t-5">Total deactivate membership</div>
                        </div>
                    </div>
                    
                  </div>
                                      
                                    
        <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">
              <div class="btn-group pull-right m-t-10 m-b-20">
                  
                  <button type="button" class="btn btn-default m-r-5" title="Add Membership" data-toggle="modal" data-target="#addmembership"><i class="fa fa-plus"></i></button>

              </div>
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                     
                    <th>Membership ID</th>
                    <th>Discount</th>
                    <th>Code</th>
                    <th>Expiry date</th>
                    <th>Status</th>
                    <th>Action</th>
                    
                  </tr>
                </thead>
                <tbody>
    <?php  foreach ($viewmembership as $key => $value) {
          


                   if($value->status=='1'){
                    $statsu='<p style="color:green">Active</p>';
                   } else{
                     $statsu='<p style="color:red">Deactive</p>';
                   }
     ?>
                <tr><td>MEMB-00<?=$value->id?></td>
                    <td><?=$value->DiscountPercentage?></td>
                    <td><?=$value->code?></td>
                    <td><?=date("Y-m-d",$value->ExpiryDate);?></td>
                    <td><?=$statsu;?></td>                   
                    <td>
                       <button type="button" class="btn btn-info m-r-5 status_change" data-val="<?=$value->status;?>" data-id="<?=$value->id;?>" title="Change status" >Change Status</button>

                       <button type="button" class="btn btn-default m-r-5 delete_membership"  data-id="<?=$value->id;?>" title="Delete" >Delete</button>
                    </td>
                  </tr>

                  <?php  } ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>

<!--Start Delivery-->
<div id="addmembership" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">        
<?php echo form_open(base_url('Coupons/viewmembership_save'), array( 'id' => 'viewmembership_save', 'method'=>'POST'));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

       <h4 class="modal-title">Add Membership</h4>

        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">

        <label for="field-7" class="control-label">Choose Customer</label>

        <select class="form-control select2 multiselect_customer_ed" id="customer_id_edit" name="discountcustomer" multiple="">
            <option value="">Choose Customer</option>
            <?php 
            foreach($List_users as $List_user)
            {
             ?> 
             <option value="<?php echo $List_user->houdinv_user_id ?>"> <?php echo $List_user->houdinv_user_name ?> </option>  
           <?php }
            ?>
            </select>

        </div>
        <div class="ajax_Loginresponse_result">
        <div class="form-group">
           <label for="field-7" class="control-label">Code</label>
           <input class="form-control required_validation_for_vandor" type="text" name="Code">
        </div>
         <div class="form-group">
           <label for="field-7" class="control-label">Discount Percentage</label>
           <input class="form-control required_validation_for_vandor" type="text" name="DiscountPercentage">
        </div>
         <div class="form-group">
           <label for="field-7" class="control-label">Expiry Date</label>
           <input class="form-control date_picker required_validation_for_vandor" type="text" name="ExpiryDate">
        </div>
      </div>
        <div class="form-group">
        <label for="field-7" class="control-label">Change Status</label>
              <select class="selectpicker show-tick" name="status" data-style="btn-white">
                          <option value="1" >Active</option>
                          <option value="0" >Deactive</option>                          
              </select>                        
        </div>

        </div>
        </div>

        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Add Membership">

        </div>

        </div>
     <?php echo form_close(); ?>
        </div>

        </div>

    <!-- END Delivery --> 
    <!--Delete-->

  <div id="membership_delete" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
         <?php echo form_open(base_url('Coupons/viewmembership_delete'), array( 'id' => 'viewmembership_delete', 'method'=>'POST'));?>
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete Membership</h4>

          </div>

          <div class="modal-body">

          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this membership ?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">
          <input type="hidden" name="update_id" id="delete_val">
          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Delete">

          </div>

          </div>
          <?php echo form_close(); ?>
          </div>

          </div>

<!--change Status-->
<div id="Change-status" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
       <?php echo form_open(base_url('Coupons/viewmembership_updatestatus'), array( 'id' => 'viewmembership_updatestatus', 'method'=>'POST'));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Change Status</h4>

        </div>

        <div class="modal-body">

        <div class="row">
        <div class="col-md-12">

        <div class="form-group no-margin">

        <label for="field-7" class="control-label">Change Status</label>

        <select class="form-control " name="status_changes" id="status_value_id">         
        <option value="1">Active</option>
        <option value="0">Deactive</option>
        </select>

        </div>

        </div>
        </div>

        </div>

        <div class="modal-footer">
        <input type="hidden" name="update_id" id="update_status_ids">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Update Status">

        </div>

        </div>
       <?php echo form_close(); ?>
        </div>

        </div>
   
<?php $this->load->view('Template/footer.php') ?>
<script>
 $(document).ready(function(){
        $(".select2").select2();

        });
        </script>

        <script>
$(document).ready(function(){    
 $('#viewmembership_save').submit(function(e){
    e.preventDefault(); 
      var check_required_field='';
      $(this).find(".required_validation_for_vandor").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {
          
        return false;
      }else{      


    urls='<?=base_url();?>'+'Coupons/viewmembership_save';
          var dataString = $(this).serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".ajax_Loginresponse_result").html(data);
        }else{

        window.location.reload();
        }
        }
        });  


      } 

    });  
    //data-toggle="modal" data-target="#Change-status"
     $(".delete_membership").click(function(){      
       data_id=$(this).attr('data-id');

       $('#delete_val').val(data_id);              
      $('#membership_delete').modal('show');

       });


       $(".status_change").click(function(){ 
     
       data_id=$(this).attr('data-id');
        data_val=$(this).attr('data-val');
 

      $('#status_value_id').val(data_val);  
       $('#update_status_ids').val(data_id);  


      $('#Change-status').modal('show');

       });



$(".edit_discount").click(function(){ 
       
  var $data_button=$(this);
       $data_button.html("<i class='fa fa-spinner fa-spin '></i>");
        update_id=$(this).siblings('.discount_id').val();

       
         $('#edit_data').val(update_id);

   urls='<?=base_url();?>'+'Discount/GetEdit_products';
          var dataString = $('#edit_datasend_frm').serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:dataString,        
        success:function (data) {        
        if(data){
        
        var data = $.parseJSON(data);

        discount_id=data.discount_id;
        $('#discount_id').val(discount_id);
         discount_end=data.discount_end;
       $('#discount_end').val(discount_end);
          discount_amount=data.discount_amount;
          $('#discount_amount').val(discount_amount);
           discount_name=data.discount_name;
           $('#discount_name').val(discount_name);
            discount_start=data.discount_start;
            $('#discount_start').val(discount_start);
             discount_status=data.discount_status;
                $('#discount_status').val(discount_status);          
              customer_id=data.customer_id;
            $('#customer_id_edit').val(customer_id).select2();
               product_cat_id=data.product_cat_id;
               
                $('#product_cat_id').val(product_cat_id).select2();
              var product_id=data.product_id;  


             urls='<?=base_url();?>'+'Discount/Get_products';
          var dataString = $('#editdiscount_frm').serialize();     
        $.ajax({
        url:urls,
        type:"post",
        data:dataString,        
        success:function (data1) {        
        if(data1){
         $('#products_add_ed').html(data1);
         $('#products_add_ed').val(product_id).select2();
        }else{        
        }
        }
        });     
         $('#edit_discount').modal('show');
         $data_button.html("Edit");         
        }else{
      alert('technical Isuue please try agian later ');
        
        }
        }
        });  

      

       });

    

 $('#editdiscount_frm').submit(function(e){
    e.preventDefault(); 
 
 

    urls='<?=base_url();?>'+'Discount/editdiscount_cus';
          var dataString = $(this).serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".edit_module_data").html(data);
        }else{

        window.location.reload();
        }
        }
        });  
    });  

 
});
</script>
