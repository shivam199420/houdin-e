$('#searchboxsuggestionproduct #searchfiltertext').on('keyup', function() {


    var val = $(this).val();

    val = val.replace(/^\s|\s+$/, "");
    if (val) {
        $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-spinner fa-spin"></i>');
        $.ajax({
            type: "Get",
            url: base_url + 'search?key=' + val,
            dataType: "json",
            success: function(data) {
                alert('hi');
                if (data) {
                    var entery = data['entries'];
                    var cartstatus = data['cart'];
                    $('#searchfilterdataheader').html('').show();
                    $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
                    if (entery) {
                        var allcat = '';
                        var cat = '';

                        for (var i = 0; i < entery.length; i++) {
                            if (cartstatus == 'yes') {
                                var cartbtn = '<a   data-variant="' + entery[i]['clientLevelId'] + '" data-cart="' + entery[i]['id'] + '" title="Add cart" class="linkcat_search_cart Add_to_cart_button"><i class="fa fa-shopping-bag" aria-hidden="true"></i></a>';
                            }

                            if (entery[i]['category'] == "Category") {
                                cat += ' <li class="desktop-suggestion null"><a  href="' + entery[i]['action'] + '" class="linkcat_search" title="' + entery[i]['name'] + '">' + entery[i]['name'] + '</a></li>'
                            } else {
                                allcat += ' <li class="desktop-suggestion null"><a href="' + entery[i]['action'] + '" class="linkcat_search" title="' + entery[i]['name'] + '">' + entery[i]['name'] + '</a>' + cartbtn + '</li>'
                            }

                        }
                        // console.log(cat);
                        if (cat || allcat) {

                            if (allcat) {
                                // <ul class="desktop-group">
                                allcat = ' <li class="desktop-suggestionTitle">All Others</li>' + allcat;

                            }
                            if (cat) {
                                cat = ' <li class="desktop-suggestionTitle">Categories</li>' + cat;
                            }

                            $('#searchfilterdataheader').html('<ul class="desktop-group">' + allcat + cat + '</ul>');

                        } else {
                            $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
                        }

                    } else {
                        $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
                    }
                    //  console.log(data['entries']);

                }
            },
            error: function() {
                $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
                $('#searchfilterdataheader').html('<ul class="desktop-group"> <li class="desktop-suggestionTitle">No search result found</li></ul>');
            }
        });



    } else {
        $('#searchfilterdataheader').html('').hide();
        $('#searchboxsuggestionproduct button[type="submit"]').html('<i class="fa fa-search"></i>');
    }
});


function searchForData(value, isLoadMoreMode) {
    // create the ajax object
    ajax = new XMLHttpRequest();
    // the function to execute on ready state is changed
    ajax.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                try {
                    var json = JSON.parse(this.responseText)
                } catch (e) {
                    noUsers();
                    return;
                }

                if (json.length === 0) {
                    if (isLoadMoreMode) {
                        alert('No more to load');
                    } else {
                        noUsers();
                    }
                } else {
                    showUsers(json);
                }


            }
        }
        // open the connection
    ajax.open('GET', base_url + 'search?key=' + value + '&startFrom=' + loadedUsers, true);
    // send
    ajax.send();
}

function showUsers(data) {
    // the function to create a row
    //console.log(data);


    // loop through the data

}


$(document).ready(function() {
    $.ajax({
        type: "POST",
        url: base_url + "Logout/visitorCount",
        success: function(data) {

        }
    });
})