 <?php $this->load->view("Bakery/Template/header"); ?>
 <?php
 $getCurrency = getShopCurrency();
 $currencysymbol=$getCurrency[0]->houdin_users_currency;
 ?>
<style>

.ps-select
{
    width:50%;
}
</style>

   <div id="back2top"><i class="fa fa-angle-up"></i></div>

    <div class="loader"></div>

    <div class="page-wrap">

      <!-- Heros-->

      <div class="ps-section--hero"><img src="<?php echo base_url(); ?>Extra/Uploads/01.jpg" alt="">

        <div class="ps-section__content text-center">

          <h3 class="ps-section__title">Product List</h3>

        </div>

      </div>

      <div class="ps-section">

        <div class="container">
            <div class="row">
                    <div class="container">
                    <div class="col-sm-12">
                    <?php
                    if($this->session->flashdata('error'))
                    {
                            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                    }
                    if($this->session->flashdata('success'))
                    {
                            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                    }
                    ?>
                    </div>
                    </div>
                    </div>
          <div class="row">

            <div class="col-md-12">
              <?php
              $count_product=count($productList);
              if($count_product==0)
              {?>
                <div class="container"><div class="col-sm-12 text-center"><h3>Products are not available</h3></div></div>
              <?php } else { ?>
              <div class="ps-shop-grid ">

                    <!-- <div class="ps-shop-features">

                      <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 "><img class="mb-30" src="<?php echo base_url(); ?>Extra/Uploads/012x.jpg" alt="">

                            </div>

                            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 "><img class="mb-30" src="<?php echo base_url(); ?>Extra/Uploads/022x.jpg" alt="">

                            </div>

                      </div>

                    </div> -->

                    <div class="ps-shop-filter">

                      <div class="row">

                            <div class="col-md-8">

                              <div class="form-group">

                                <label>Short by:</label>


                                 <?php echo form_open(base_url( 'Productlist/'.$this->uri->segment('2').'/'.$this->uri->segment('3').'/'.$this->uri->segment('4')), array( 'id' => 'Productlist', 'method'=>'get' ));?>

                                    <select class="sort-select ps-select" name="sort" >
                                    <option value="">select</option>
                                    <option value="name" <?php if($_REQUEST['sort'] =="name") { echo "selected"; }?> >Name Ascending</option>
                                    <option value="date" <?php if($_REQUEST['sort'] =="date") { echo "selected"; }?>>Date Ascending</option>
                                    <option value="pricehl" <?php if($_REQUEST['sort'] =="pricehl") { echo "selected"; }?>>Price High to Low</option>
                                    <option value="pricelh" <?php if($_REQUEST['sort'] =="pricelh") { echo "selected"; }?>>Price Low to High</option>
                                    </select>

                                     <?php echo form_close(); ?>

                              </div>

                            </div>



                    </div>
 </div>
                <div class="ps-shop ps-col-tiny">

                  <div class="row appendProductList">

               <?php
            foreach($productList as $productListData)
            {
                $getProductImage = json_decode($productListData->houdinv_products_main_images,true);
                $getProductPrice = json_decode($productListData->houdin_products_price,true);
            ?>
                        <div class="col-md-3">

                          <div class="ps-product">

                            <div class="ps-product__thumbnail">




                                <?php if($productListData->houdinv_products_total_stocks <= 0) { ?>
                                <div class="ps-badge" style="width: 86px;height: 86px;"><span>Out of Stock</span></div>

                                <?php } ?>
                              <?php
                            if($productListData->houdinv_products_main_quotation == 1)
                            {
                            ?>
                                <div class="model_qute_show ps-badge ps-badge--new" style="width: 86px;height: 86px;" data-id="<?php echo $productListData->houdin_products_id ?>"  data-toggle="modal"><span style="text-align: center;">Ask Quotation</span>
                            </div>
                            <?php
                            }
                            ?>

                              <a class="ps-product__overlay" href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>"></a>
                              <?php 
                              if($getProductImage[0])
                              {
                                  $setlatestIMageDataValue = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                              }
                              else
                              {
                                  $setlatestIMageDataValue = base_url()."Extra/noPhotoFound.png";
                              }
                              ?>
                              <img style="width: 262px;height:283px" src="<?php echo $setlatestIMageDataValue ?>" alt="" />

                              <ul class="ps-product__action">

                                <li><a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>" data-effect="mfp-zoom-out" data-tooltip="View"><i class="ps-icon--search"></i></a></li>

                                <li class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><a  data-tooltip="Add to wishlist"><i class="ps-icon--heart"></i></a></li>

                                <?php
            if($productListData->houdinv_products_total_stocks <= 0)
            {
            if($storeSetting[0]->receieve_order_out == 1)
            {
                ?>


            <?php  }
            }
            else
            {
            ?>
                <li class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><a  data-tooltip="Add to cart"><i class="ps-icon--shopping-cart"></i></a></li>

            <?php }
            ?>


                              </ul>

                            </div>

                            <div class="ps-product__content"><a class="ps-product__title" href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>"><?php echo substr($productListData->houdin_products_title,0, 25).".." ; ?></a>




                              <p class="ps-product__price">
<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "")
 { echo $productListData->houdin_products_final_price; }
  else {
    $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?>
    <del><?php echo $getProductPrice['sale_price']; ?>
</del><?php echo $productListData->houdin_products_final_price; ?> <?php  } ?>
                          </p>  </div>

                          </div>

                        </div>




                  <?php
                  }
                   ?>

                   <?php
        if($count > 0)
        {
            if($count >= 12)
            {
                $remain = $count-12;
            }
            else
            {
                $remain = 0;
            }
        ?>


                </div>


                 <div class="row">
        <div class="col-sm-2"><button class="btn btn-sm btn-success" style="width:100%">Total Product:<?php echo $count; ?></button></div>
        <?php
        if($remain > 0)
        {
        ?>
        <div class="col-sm-10"><button type="button" data-remain="<?php echo $remain ?>" data-last="12" class="btn btn-sm btn-default acc_btn setLoadMoredata" style="width:100%;margin-top: 0px;padding: 4px;border-radius: 4px;">Load More&nbsp;<i class="fa fa-spinner fa-spin setSpinnerData" style="display:none;"></i></button></div>
        <?php }
        ?>
        </div>
        <?php }
        ?>

              </div>

            </div>
          <?php } ?>


          </div>

        </div>

      </div>






  <?php $this->load->view("Bakery/Template/footer"); ?>
        <script>
        $('.sort-select').on('change',function()
        {
          document.getElementById('Productlist').submit();
        });
        </script>
        <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('click','.setLoadMoredata',function(){
                $(this).prop('disabled',true);
                $('.setSpinnerData').show();
                var getCategory = '<?php echo $this->uri->segment('2') ?>';
                var getSubCategory = '<?php echo  $this->uri->segment('3') ?>';
                var getSubSubCategory = '<?php echo $this->uri->segment('4') ?>';
                var sortBy = '<?php echo $_REQUEST['sort'] ?>';
                var dataLast = $(this).attr('data-last');
                var dataRemain = $(this).attr('data-remain');
                $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>Ajaxcontroller/fetchProductPaginationData",
                data: {"dataLast": dataLast,"dataRemain":dataRemain,"getCategory":getCategory,"getSubCategory":getSubCategory,"getSubSubCategory":getSubSubCategory,"sortBy":sortBy},
                success: function(datas) {
                    $('.setSpinnerData').hide();
                    $('.setLoadMoredata').prop('disabled',false);
                    var setData = jQuery.parseJSON(datas);

                    var grid = "";
                    for(var i=0; i < setData.main.length ; i++)
                    {
                        var $productListData = setData.main[i];
                        var textsplit = $productListData.houdin_products_title;
                        var $setCartDesign = "";
                          var $setStockBadge = "";
                          var $settingData = setData.settingData;
                            if($productListData.houdinv_products_total_stocks <= 0)
                                {
                                    if($settingData[0].receieve_order_out == 1)
                                    {
                                        $setCartDesign = '<li class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><a  data-tooltip="Add to cart"><i class="ps-icon--shopping-cart"></i></a></li>';
                                    }
                                }
                                else
                                {
                                    $setCartDesign = '<li class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><a  data-tooltip="Add to cart"><i class="ps-icon--shopping-cart"></i></a></li>';
                                }
                                if($productListData.houdinv_products_total_stocks <= 0)
                                {
                                    $setStockBadge = '<div class="ps-badge" style="width: 86px;height: 86px;"><span>Out of Stock</span></div>';
                                }

                          var   $getProductImage = jQuery.parseJSON($productListData.houdinv_products_main_images);
                           var     $getProductPrice = jQuery.parseJSON($productListData.houdin_products_price);
                              console.log($getProductPrice);
                                if($getProductPrice.discount != 0 && $getProductPrice.discount  != "")
                                {
                                   var $DiscountedPrice = $getProductPrice.price-$getProductPrice.discount;
                                   var $originalPrice = $getProductPrice.price;
                                }
                                else
                                {
                                  var  $DiscountedPrice = 0;
                                  var  $originalPrice = $getProductPrice.price;
                                }
                                if($DiscountedPrice == 0)
                                {
                                  var  $setPrice = $originalPrice;
                                }
                                else
                                {
                                   var $setPrice = "<del>"+$originalPrice+"</del>&nbsp;"+$DiscountedPrice+"";
                                }
                              if($getProductImage[0])
                              {
                                  var setlatestIMageDataValue = '<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/'+$getProductImage[0]+'';
                              }
                              else
                              {
                                  var setlatestIMageDataValue = '<?php echo base_url() ?>Extra/noPhotoFound.png';
                              }

                       grid+='<div class="col-md-3">'

                          +'<div class="ps-product">'
                           +$setStockBadge
                            +'<div class="ps-product__thumbnail">';


                            if($productListData.houdinv_products_main_quotation == 1)
                            {

                                grid+='<div class="model_qute_show ps-badge ps-badge--new" style="width: 86px;height: 86px;" data-id="'+$productListData.houdin_products_id+'"  data-toggle="modal"><span style="text-align: center;">Ask Quotation</span>'
                            +'</div>';

                            }


                              grid+='<a class="ps-product__overlay" href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>"></a>'
                              +'<img style="width: 262px;height:283px" src="'+setlatestIMageDataValue+'" alt="" />'

                              +'<ul class="ps-product__action">'

                                +'<li><a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'" data-effect="mfp-zoom-out" data-tooltip="View"><i class="ps-icon--search"></i></a></li>'

                                +'<li class="Add_to_whishlist_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><a  data-tooltip="Add to wishlist"><i class="ps-icon--heart"></i></a></li>'
                                +$setCartDesign
                                +'</ul>'

                            +'</div>'

                            +'<div class="ps-product__content">'
                            +'<a class="ps-product__title" href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'">'+textsplit.substring(0,25)+'</a>'
                              +'<p class="ps-product__price">'
                           +$setPrice
                          +'</p>'
                          +'</div>'

                          +'</div>'

                        +'</div>';

                    }
                    $('.appendProductList').append(grid);
                   $('.setLoadMoredata').attr('data-last',setData.last);
                }
                });
            });
        });
        </script>
