<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payumoneyresponse extends CI_Controller {
	function __construct() 
    {
        parent::__construct();
        if(!$_COOKIE['tempUserPackageId'])
        {
            $this->load->helper('url');
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(''.base_url().'/vendor', 'refresh'); 
        }
        $this->load->model('Shopregistermodel');
    }
    public function index()
    {
        $getUserId = $_COOKIE['tempUserPackageId'];
        // set user update array
        $getExpiryDate = date('Y-m-d', strtotime('+365 days', strtotime(date('Y-m-d'))));
        $setUserArray = array('houdin_users_package_id'=>$this->session->userdata('temppackageId'),'houdin_users_package_expiry'=>$getExpiryDate); 
        // set transaction array
        if($_POST['txnStatus'] == 'SUCCESS')
        {
            $setTransactionStatus = 'success';
        }
        else
        {
            $setTransactionStatus = 'failure';
        }
        $setTransactionArray = array('houdin_admin_transaction_transaction_id'=>$_POST['txnid'],'houdin_admin_transaction_from'=>'payumoney',
        'houdin_admin_transaction_type'=>'credit','houdin_admin_transaction_for'=>'package','houdin_admin_transaction_for_id'=>$this->session->userdata('temppackageId'),
        'houdin_admin_transaction_amount'=>$_POST['amount'],'houdin_admin_transaction_status'=>$setTransactionStatus,'houdin_admin_transaction_date'=>date('Y-m-d'));

        $getResponseData = $this->Shopregistermodel->upgradeUserPackage($setUserArray,$setTransactionArray,$getUserId);
        if($getResponseData['message'] == 'yes')
        {
            unset($_COOKIE['tempUserPackageId']);
            $this->session->unset_userdata('temppackageId');
            $this->load->helper('url');
            $this->session->set_flashdata('success','Package upgraded successfully');
            redirect(''.base_url().'/vendor', 'refresh'); 
        }
        else if($getResponseData['message'] == 'user')
        {
            unset($_COOKIE['tempUserPackageId']);
            $this->session->unset_userdata('temppackageId');
            $this->load->helper('url');
            $this->session->set_flashdata('error','Transaction is successfull. Order is not updated, please contact to our support with txn id '.$_POST['txnid'].'');
            redirect(''.base_url().'/vendor', 'refresh'); 
        }
        else
        {
            unset($_COOKIE['tempUserPackageId']);
            $this->session->unset_userdata('temppackageId');
            $this->load->helper('url');
            $this->session->set_flashdata('success','Package upgraded successfully');
            redirect(''.base_url().'/vendor', 'refresh'); 
        }
    }
}