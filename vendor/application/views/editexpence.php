<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); 
$getCurrency = getVendorCurrency();
if($getCurrency[0]->houdin_users_currency=="USD")
{
  $currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="AUD"){
  $currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="Euro"){
  $currencysymbol= "£";
}else if($getCurrency[0]->houdin_users_currency=="Pound"){
  $currencysymbol= "€";
}else if($getCurrency[0]->houdin_users_currency=="INR"){
  $currencysymbol= "₹";
}
?>
<style type="text/css">.total_div{    border: 1px solid #ddd;
    border-radius: 5px;
    background: #fff;}</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">

    <!--START Page-Title -->
    <div class="row">

    <div class="col-md-8">
    <h4 class="page-title">Expenses</h4>
    <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>">Home</a></li>
     <li><a href="<?php echo base_url(); ?>accounts/expence">Expenses</a></li>
    <li class="active">Edit Expenses</li>
    </ol>
    </div>
    </div>
    <!--END Page-Title -->
    <?php echo form_open(base_url( 'Accounts/editexpencesdata' ), array( 'id' => 'addExpenceDataForm', 'method'=>'post' ));?>
    <div class="row">
    <?php
    if($this->session->flashdata('error'))
    {
      echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
    }
    if($this->session->flashdata('success'))
    {
      echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    ?>

    <?php
    $setMainData = $expense['expenseData'];
    $setItemData = $expense['item'];
    ?>
    <div class="col-md-10">
    <input type="hidden" name="expenseMainIdData" value="<?php echo $this->uri->segment('3'); ?>"/>
    <div class="form-group" style="margin-bottom: 0px!important;">
      <input type="hidden" value="<?php echo $setMainData[0]->houdinv_extra_expence_payee_type ?>" class="getPayeeTypeData" name="getPayeeTypeData"/>
            <select class="form-control select1 setPayetype required_validation_for_add_expense_data" name="payeeId">
            <option value="">Choose Payee</option>
            <?php for($index = 0; $index < count($payeeData); $index++)
            {
            ?>
            <option <?php if($setMainData[0]->houdinv_extra_expence_payee == $payeeData[$index]['id']) { ?> selected="selected" <?php  } ?> data-type="<?php echo $payeeData[$index]['type'] ?>" value="<?php echo $payeeData[$index]['id'] ?>"><?php echo $payeeData[$index]['name'] ?> (<?php echo $payeeData[$index]['type'] ?>)</option>
            <?php } ?>
        </select>
        </div>
    </div>
    <div class="col-md-2">
    <div class="form-group">
    <?php if($setMainData[0]->houdinv_extra_expence_payment_status == '0') { ?>
    <button type="button" data-toggle="modal" data-target="#addPayeeModaal" class="btn btn-success btn-xs"><i class="fa fa-plus"></i>Add New Payee</button> <?php } ?>
    </div>
    </div>
    </div>
    <div class="row m-t-20">
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-8">
    <div class="row">
    <div class="col-md-3">
    	<div class="form-group">
    	<label>Payment Date</label>
    	<input type="text" value="<?php echo $setMainData[0]->houdinv_extra_expence_payment_date ?>" class="form-control date_picker required_validation_for_add_expense_data" name="billDate">
    	</div>
    </div>
    <div class="col-md-3">
    	<div class="form-group">
    	<label>Payment Method</label>
    	<select class="form-control required_validation_for_add_expense_data" name="paymentType">
      <option value="">Choose Payment Method</option>
      <option <?php if($setMainData[0]->houdinv_extra_expence_payment_method == 'cash') { ?> selected="selected" <?php } ?> value="cash">Cash</option>
      <option <?php if($setMainData[0]->houdinv_extra_expence_payment_method == 'card') { ?> selected="selected" <?php } ?> value="card">Card</option>
      <option <?php if($setMainData[0]->houdinv_extra_expence_payment_method == 'cheque') { ?> selected="selected" <?php } ?> value="cheque">Cheque</option>
      </select>
    	</div>
    </div>
    <div class="col-md-3">
    	<div class="form-group">
    	<label>Transaction Type</label>
    	<select class="form-control required_validation_for_add_expense_data" name="transactionType">
      <option value="">Choose Transaction Type</option>
      <option <?php if($setMainData[0]->houdinv_extra_expence_transaction_type == 'credit') { ?> selected="selected" <?php } ?> value="credit">Credit</option>
      <option <?php if($setMainData[0]->houdinv_extra_expence_transaction_type == 'debit') { ?> selected="selected" <?php } ?> value="debit">Debit</option>
      </select>
    	</div>
    </div>
    <div class="col-md-3">
    	<div class="form-group">
    	<label>Payment Status</label>
    	<select class="form-control required_validation_for_add_expense_data" name="paymentStatus">
      <option value="">Choose Payment Status</option>
      <option <?php if($setMainData[0]->houdinv_extra_expence_payment_status == '1') { ?> selected="selected" <?php } ?> value="1">Paid</option>
      <option <?php if($setMainData[0]->houdinv_extra_expence_payment_status == '0') { ?> selected="selected" <?php } ?> value="0">Not Paid</option>
      </select>
    	</div>
    </div>
    </div>
    </div>
    <div class="col-md-2 pull-right">
    <div class="form-group">
    	<label>Ref no.</label>
    	<input type="text" value="<?php echo $setMainData[0]->houdinv_extra_expence_refrence_number ?>" class="form-control required_validation_for_add_expense_data name_validation" name="refId">
    	</div>
    </div>
    </div>
    </div>
    </div>
    <div class="row">
            <div class="col-md-12">

              <div class="card-box table-responsive">
                <table class="table table-striped table-bordered table_shop_custom">
                  <thead>
                    <tr>

                      <th>Product Service</th>

                      <th>QTY</th>
                      <th>Rate</th>
                      <th>Amount</th>
                      <th>Tax</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <input type="hidden" value="<?php echo $setMainData[0]->houdinv_extra_expence_subtotal ?>" class="setsubtotal" name="setsubtotal"/>
                  <input type="hidden" value="<?php echo $setMainData[0]->houdinv_extra_expence_tax ?>" class="setTaxData" name="setTaxData"/>
                  <input type="hidden" value="<?php echo $setMainData[0]->houdinv_extra_expence_total_amount ?>" class="setNetAmount" name="setNetAmount"/>
                  <tbody class="appendRowData">
                      <?php
                      for($index = 0; $index < count($setItemData); $index++)
                      {
                      ?>
                      <tr class="parentRowData">

                      <td>
                      <?php if($setMainData[0]->houdinv_extra_expence_payment_status == '0') { ?><button type="button" data-toggle="modal" data-target="#addNonInventoryProducts" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i>&nbsp;Add Non inventory product</button><?php } ?>
                      <input type="hidden" class="setProductType" value="<?php echo $setItemData[$index]->houdinv_extra_expence_item_type ?>" name="setProductType[]"/>
                        <select class="form-control select1 setNoninventoryProduct" name="productId[]">
                    <option value="">Choose Item</option>
                    <?php
                    foreach($productListData as $productListDataValue)
                    {
                    ?>
                    <option <?php if($setItemData[$index]->houdinv_extra_expence_item_item_id == $productListDataValue->houdin_products_id) { ?> selected="selected" <?php  } ?> data-type="inv" data-description="<?php echo $productListDataValue->houdin_products_short_desc ?>" data-price="<?php echo $productListDataValue->houdin_products_final_price ?>" value="<?php echo $productListDataValue->houdin_products_id ?>"><?php echo $productListDataValue->houdin_products_title."(inventory)" ?></option>
                    <?php }
                    foreach($noninventoryProduct as $noninventoryProductList)
                    {
                    ?>
                    <option <?php if($setItemData[$index]->houdinv_extra_expence_item_item_id == $noninventoryProductList->houdinv_noninventory_products_id) { ?> selected="selected" <?php  } ?> data-type="ninv" data-description="<?php echo $noninventoryProductList->houdinv_noninventory_products_desc ?>" data-price="<?php echo $noninventoryProductList->houdinv_noninventory_products_price ?>" value="<?php echo $noninventoryProductList->houdinv_noninventory_products_id ?>"><?php echo $noninventoryProductList->houdinv_noninventory_products_name ."(non inventory)" ?></option>
                    <?php }
                    ?>
                      </select></td>

                      <td><input type="text" value="<?php echo $setItemData[$index]->houdinv_extra_expence_item_quantity ?>" class="form-control setQuantity" name="quantity[]"> </td>
                      <td><input type="text" value="<?php echo $setItemData[$index]->houdinv_extra_expence_item_rate ?>" class="form-control setRate" readonly="readonlyt" name="rate[]"> </td>
                      <td><input type="text" value="<?php echo $setItemData[$index]->houdinv_extra_expence_item_total_amount ?>" class="form-control setAmount" name="amount[]"> </td>
                      <td><input type="text" value="<?php echo $setItemData[$index]->houdinv_extra_expence_item_tax ?>" class="form-control setTax" name="tax[]">

                    </td>
                      <td>
                      <!-- <button type="button" class="btn btn-default removeCurrentRowData  m-r-5"  ><i class="fa fa-trash"></i></button> -->
                      <button type="button" class="btn btn-default addNewRowData m-r-5"><i class="fa fa-plus"></i></button></td>
                    </tr>
                      <?php }
                      ?>
                    </tbody>
                  </table>

                  </div>
              <div class="col-md-12">
              <div class="row">
              <div class="col-md-3 pull-right total_div">
              <div class="col-md-6 text-right">
              <h4 class="text_black"><strong>SUBTOTAL</strong></h4>
              </div>
              <div class="col-md-6 text-right">
              <h4 class="setSubtotal"><?php echo $currencysymbol; ?><?php echo $setMainData[0]->houdinv_extra_expence_subtotal ?></h4>
              </div>
              <div class="col-md-6 text-right">
              <h4 class="text_black"><strong>TAX</strong></h4>
              </div>
              <div class="col-md-6 text-right">
              <h4 class="setTax"><?php echo $currencysymbol; ?><?php echo $setMainData[0]->houdinv_extra_expence_tax ?></h4>

              </div>
              <div class="col-md-6 text-right">
              <h4 class="text_black"><strong>TOTAL</strong></h4>
              </div>
              <div class="col-md-6 text-right">
              <h4 class="setTotal"><?php echo $currencysymbol; ?><?php echo $setMainData[0]->houdinv_extra_expence_total_amount ?></h4>

              </div>
              </div>
              </div>
              </div>
              <?php if($setMainData[0]->houdinv_extra_expence_payment_status == '0') { ?>
              <div class="row">
              <div class="col-md-12 m-t-20">
              <button type="submit" class="btn btn-default pull-right">Save</button>
              </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <?php echo form_close(); ?>
    </div>
    </div>
    </div>





    <!-- add payee modal -->
    <div id="addPayeeModaal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Add Payee</h4>
    </div>
    <div class="modal-body">
    <div class="row">
    <div class="col-md-12">
    <label>Choose Payee Type</label>
    <select class="form-control checkPayeeData" name="checkPayeeData">
    <option value="">Choose Payee</option>
    <option value="customer">Customer</option>
    <option value="staff">Staff</option>
    <option value="supplier">Supplier</option>
    </select>
    </div>
    </div>
    <!-- customer form -->

    <div class="row customerFormData" style="display:none">
    <?php echo form_open(base_url( 'Accounts/addCustomer' ), array( 'id' => 'addCustomerData', 'method'=>'post' ));?>
    <div class="col-sm-12">
    <label>Customer Name</label>
    <input type="text" value="" class="form-control name_validation required_validation_for_add_customer" name="customerName" placeholder="Customer Name">
    </div>
    <div class="col-sm-12">
    <label>Mobile Number</label>
    <input type="text" value="" class="form-control Internationphonecode required_validation_for_add_customer name_validation" name="customerMobile" placeholder="Mobile Number" autocomplete="off">
    </div>
    <div class="col-sm-12">
    <label>Email</label>
    <input type="text" value="" class="form-control required_validation_for_add_customer email_validation name_validation" name="customerEmail" placeholder="Email Address">
    </div>
    <div class="col-sm-12">
    <label> Choose Status</label>
    <select class="form-control required_validation_for_register" name="customerStatus">
        <option value="">Choose Status</option>
        <option value="active">Active</option>
        <option value="deactive">Deactive</option>
        </select>
    </div>
    <div class="col-sm-12">
    <input type="submit" class="btn btn-info" name="" value="Add Payee">
    </div>
    <?php echo form_close(); ?>
    </div>

    <!-- end customer form -->
    <!-- staff form -->

    <div class="row staffMemberData" style="display:none">

    <?php echo form_open(base_url('Accounts/addStaffmember'), array( 'id' => 'staff_add', 'method'=>'POST' ));?>
    <div class="col-sm-12">
    <label>Staff Name</label>
    <input type="text" value="" class="form-control required_validation_for_vandor" name="staff_name" placeholder="Staff Name" >
    </div>
    <div class="col-sm-12">
    <label>Email</label>
    <input type="text" value="" class="form-control required_validation_for_vandor" name="staff_email" placeholder="Staff Email">
    </div>
    <div class="col-sm-12">
    <label>Contact Number</label>
    <input type="text" value="" class="form-control required_validation_for_vandor" name="staff_contact_number" placeholder="Staff Number" >
    </div>
    <div class="col-sm-12">
    <label>Department</label>
    <select class="form-control required_validation_for_vandor" name="staff_department">
        <option value="">Choose Department</option>
        <option value="Account">Account</option>
        <option value="DeliveryBoy">Delivery Boy</option>
        <option value="DataEntry">Data Entry</option>
        </select>
    </div>
    <div class="col-sm-12">
    <label>Status</label>
    <select class="form-control required_validation_for_vandor" name="staff_status" >
        <option value="">Choose Status</option>
        <option value="1">Active</option>
        <option value="0">Deactive</option>
        </select>
    </div>
    <div class="col-sm-12">
    <label>Warehouse</label>
    <select class="form-control required_validation_for_vandor" name="staff_warehouse" >
        <option value="">Choose one</option>
        <?php
        foreach($warehouseData as $warehouseDataList)
        {
        ?>
          <option value="<?php echo $warehouseDataList->id ?>"><?php echo $warehouseDataList->w_name ?></option>
        <?php }
        ?>

                </select>
    </div>
    <div class="col-sm-12">
    <label>Address</label>
    <textarea class="form-control required_validation_for_vandor" placeholder="Address type here..." name="staff_address" ></textarea>
    </div>
    <div class="col-sm-12">
    <label>Password sent on</label>
    <input type="radio" value="email" class="" checked="" name="password_send">
    </div>
    <div class="col-sm-12">
    <input type="submit" class="btn btn-info" name="" value="Add Payee">
    </div>
    <?php echo form_close(); ?>
    </div>

    <!-- end here -->
    <!-- add supplier data -->

    <div class="row supplierMemberData" style="display:none">
    <?php echo form_open(base_url( 'Accounts/addsupplierData' ), array( 'id' => 'addSupplierData', 'method'=>'post' ));?>
    <div class="col-md-12">
    <label>Company Name</label>
    <input type="text" value="" class="form-control required_validation_for_add_supplier name_validation" name="supplierCompanyName" placeholder="Company Name">
    </div>

    <div class="col-md-12">
    <label>Contact Person</label>
    <input type="text" value="" class="form-control required_validation_for_add_supplier name_validation" name="supplierContactPerson" placeholder="Contact Person Name">
    </div>

    <div class="col-md-12">
    <label>Email</label>
    <input type="text" value="" class="form-control required_validation_for_add_supplier name_validation email_validation" name="supplierEmail" placeholder="Email">
    </div>

    <div class="col-md-12">
    <label>Contact</label>
    <input type="text" value="" class="form-control Internationphonecode required_validation_for_add_supplier name_validation" name="supplierMobile" placeholder="Mobile Number">
    </div>

    <div class="col-md-12">
    <label>Choose Status</label>
    <select class="form-control required_validation_for_add_supplier" name="supplierStatus">
        <option value="">Choose Status</option>
        <option value="active">Active</option>
        <option value="deactive">Deactive</option>
        </select>
    </div>

    <div class="col-md-12">
    <label>Address</label>
    <textarea class="form-control required_validation_for_add_supplier name_validation" name="supplierAddress" rows="5"></textarea>
    </div>
    <div class="col-sm-12">
    <input type="submit" class="btn btn-info" name="" value="Add Payee">

    </div>
 <?php echo form_close() ?>
    </div>

    <!-- end here -->
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

    </div>
    </div>
      </div>

    </div>
    <!-- non inventory products -->
    <div id="addNonInventoryProducts" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
    <form method="post" id="addNoninventoryProduct">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Non inventory products</h4>
    </div>
    <div class="modal-body">
    <div class="row">
      <div class="col-sm-12 showNoninventoryError" style="display:none">

      </div>
    <div class="col-md-12">
      <label>Product Name</label>
      <input type="text" class="form-control required_validation_for_add_supplier name_validation" name="productName"/>
    </div>
    <div class="col-md-12">
      <label>Product Description</label>
      <input type="text" class="form-control required_validation_for_add_supplier name_validation" name="productDescription"/>
    </div>
    <div class="col-md-12">
      <label>Product Price</label>
      <input type="text" class="form-control required_validation_for_add_supplier name_validation" name="productPrice"/>
    </div>

    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-info" name="" value="Add Product">
    </div>
    </div>
    </form>
      </div><?php echo $setMainData[0]->houdinv_extra_expence_subtotal ?>
    <!-- end here -->
 <?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">
$(document).ready(function(){
  $(".select1").select2({
    minimumInputLength: 2
});
})
</script>
 <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addSupplierData,#addCustomerData,#staff_add',function(){
			var check_required_field='';
			$(this).find(".required_validation_for_add_supplier").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>

  <!-- non inventory products -->
  <script type="text/javascript">
	$(document).ready(function(){
    var setBaseData = "<?php echo base_url() ?>";
		$(document).on('submit','#addNoninventoryProduct',function(e){
      $('.showNoninventoryError').show().html('');
			var check_required_field='';
			$(this).find(".required_validation_for_add_supplier").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
        e.preventDefault();
        var getFormData = $('#addNoninventoryProduct').serializeArray();
        jQuery.ajax({
          type:"POST",
          url: setBaseData+"Accounts/addNoninventoryProduct",
          data: getFormData,
          success: function(data) {
              var getJsonData = $.parseJSON(data);
               if(getJsonData.message == 'yes')
               {
                 $('#addNonInventoryProducts').modal('hide');
                 var setHtmlData = '<option data-type="ninv" data-description="'+getJsonData.description+'" data-price="'+getJsonData.price+'" value="'+getJsonData.id+'">'+getJsonData.name+'(non inventory)</option>';
                 $('.setNoninventoryProduct').append(setHtmlData);

               }
               else if(getJsonData.message == 'fields')
               {
                  $('.showNoninventoryError').show().html('').html('<div class="alert alert-danger">All fields are mandatory</div>');
               }
               else
               {
                $('.showNoninventoryError').show().html('').html('<div class="alert alert-danger">Something went wrong. Please try again</div>');
               }
          }
      });
				// return true;
			}
		});
	});
  </script>
    <!-- manage js of product data -->
    <script type="text/javascript">
      $(document).ready(function(){
        $(document).on('click','.addNewRowData',function(){
          addNewrow();
        });
        $(document).on('click','.removeCurrentRowData',function(){
          $(this).parents('.parentRowData').remove();
          finalCalculation();
        })
        $(document).on('change','.setNoninventoryProduct',function(){

          $(this).parents('.parentRowData').find('.setProductType').val($(this).children('option:selected').attr('data-type'));
          var getThis = $(this);
          setProductPrice(getThis,'product');
        });
        $(document).on('keyup','.setQuantity',function(){
          var getThis = $(this);
          var getQunatity = $(this).val();
          if(getQunatity)
          {
            getQunatity = getQunatity;
          }
          else
          {
            getQunatity = 0;
          }
          setProductPrice(getThis,'qty',getQunatity);
        });
        $(document).on('keyup','.setAmount',function(){
          var getThis = $(this);
          var getAmount = $(this).val();
          if(getAmount)
          {
            getAmount = getAmount;
          }
          else
          {
            getAmount = 0;
          }
          setProductPrice(getThis,'amt',getAmount);
        });
        $(document).on('keyup','.setTax',function(){
          finalCalculation();
        })
        // update product row
        function setProductPrice(current,data,extraData)
        {
            if(data == 'product')
            {
              var price = current.children("option:selected").attr('data-price');
              var Description = current.children("option:selected").attr('data-description');
              current.parents('.parentRowData').find('.setQuantity').val(1);
              current.parents('.parentRowData').find('.setRate').val(price);
              current.parents('.parentRowData').find('.setAmount').val(price);
              current.parents('.parentRowData').find('.setTax').val(0);
            }
            else if(data == 'qty')
            {
               var getRate = current.parents('.parentRowData').find('.setRate').val();
               var setFinalAmount = parseFloat(getRate)*parseFloat(extraData);
               current.parents('.parentRowData').find('.setAmount').val(setFinalAmount);
            }
            else if(data == 'amt')
            {
                var getQuantity = current.parents('.parentRowData').find('.setQuantity').val();
                if(getQuantity)
                {
                  var getRateValue = parseFloat(extraData)/parseFloat(getQuantity);
                  current.parents('.parentRowData').find('.setRate').val(getRateValue);
                }
                else
                {
                  current.parents('.parentRowData').find('.setRate').val(extraData);
                }

            }
            finalCalculation();
        }
        // set final calculation
        function finalCalculation()
        {
          var subTotal = 0;
          var tax = 0;
          var finalAmount = 0;
          $('.setAmount').each(function(){
            var getTax = $(this).parents('.parentRowData').find('.setTax').val();
            if(getTax)
            {
              getTax = getTax;
            }
            else
            {
              getTax = 0;
            }
            var getAmount = $(this).parents('.parentRowData').find('.setAmount').val();
            // set subtotal
            subTotal = subTotal+parseFloat(getAmount);
            // calculate tax data
            var getax = (parseFloat(getTax)*parseFloat(getAmount))/100;
            tax = tax+getax;
            finalAmount = finalAmount+parseFloat(getAmount)+getax;
          });
          $('.setSubtotal').text('').text(subTotal);
          $('.setTax').text('').text(tax);
          $('.setTotal').text('').text(finalAmount);

          $('.setsubtotal').val('').val(subTotal);
          $('.setTaxData').val('').val(tax);
          $('.setNetAmount').val('').val(finalAmount);


        }
        function addNewrow()
        {
          var setHtmlData = "";
            var countRows = ($('.parentRowData').length)+1;
            var maineSelectdata = "<?php echo $setMainData; ?>";
            setHtmlData+='<tr class="parentRowData"><td> <input type="hidden" class="setProductType" value="" name="setProductType[]"/>'
            +'<button type="button" data-toggle="modal" data-target="#addNonInventoryProducts" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i>'
            +'&nbsp;Add Non inventory product</button><select class="form-control select_auto'+countRows+' setNoninventoryProduct"  name="productId[]" >'
            +'<option value="">Choose Item</option> <?php  foreach($productListData as $productListDataValue) { ?>'
            +'<option data-type="inv" data-price="<?php echo $productListDataValue->houdin_products_final_price ?>" value="<?php echo $productListDataValue->houdin_products_id ?>"><?php echo $productListDataValue->houdin_products_title."(inventory)" ?></option> <?php } foreach($noninventoryProduct as $noninventoryProductList) { ?><option data-type="ninv"   data-price="<?php echo $noninventoryProductList->houdinv_noninventory_products_price ?>" value="<?php echo $noninventoryProductList->houdinv_noninventory_products_id ?>"><?php echo $noninventoryProductList->houdinv_noninventory_products_name ."(non inventory)" ?></option><?php } ?></select>'
            +'</td><td><input type="text" class="form-control setQuantity" name="quantity[]"> </td><td>'
            +'<input type="text" class="form-control setRate" readonly="readonly" name="rate[]"> </td><td>'
            +'<input type="text" class="form-control setAmount" name="amount[]"> </td><td><input type="text" class="form-control setTax" name="tax[]"> </td><td>'
            +'<button type="button" class="btn btn-default  m-r-5 removeCurrentRowData"  ><i class="fa fa-trash"></i></button>'
            +'<button type="button" class="btn btn-default addNewRowData m-r-5"><i class="fa fa-plus"></i></button></td></tr>';
            $('.appendRowData').append(setHtmlData);
            $(".select_auto"+countRows+"").select2({
              minimumInputLength: 2
          });

        }
        // set and get payee
        $(document).on('change','.setPayetype',function(){
          $('.getPayeeTypeData').val($(this).children('option:selected').attr('data-type'));
        })
      });
      </script>
      <!-- CLient side form validation -->
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addExpenceDataForm',function(){
			var check_required_field='';
			$(".required_validation_for_add_expense_data").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				// return true;
        var quant = 0;
        $('.setQuantity').each(function(){
          if(!$(this).val())
          {
            $(this).css('border','1px solid red');
            quant++;
          }
        });
        $('.setAmount').each(function(){
          var getAmountData = $(this).val();
          if(getAmountData < 0 && getAmountData == "")
          {
            $(this).css('border','1px solid red');
            quant++;
          }
        });
        $('.setProductType').each(function(){
          if(!$(this).val())
          {
            $(this).css('border','1px solid red');
            quant++;
          }
        })
        if(quant == 0)
        {
          return true;
        }
        else
        {
          return false;
        }
			}
		});
	});
	</script>
