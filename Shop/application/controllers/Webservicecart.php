<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Webservicecart extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Cartmodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function addCart()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('productId','text','required');
        $this->form_validation->set_rules('variantId','text','required');
        $this->form_validation->set_rules('userId','text','required');
        $this->form_validation->set_rules('quantity','text','required');
        if($this->form_validation->run() == true)
        {
            $setData = strtotime(date('Y-m-d h:i:s'));
            $setCartInsertArray = array('houdinv_users_cart_user_id'=>$this->input->post('userId'),'houdinv_users_cart_item_id'=>$this->input->post('productId'),
            'houdinv_users_cart_item_variant_id'=>$this->input->post('variantId'),'houdinv_users_cart_item_count'=>$this->input->post('quantity'),'houdinv_users_cart_add_date'=>$setData);
            $setExtraData = array('shopname'=>$this->input->post('shopName'),'dataValue'=>$setData);
            $getCartStatus = $this->Cartmodel->addProductCartApp($setCartInsertArray,$setExtraData,$this->input->post('wishlistId'));
            print_r(json_encode($getCartStatus));
        }
        else
        {
            $resultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
}
