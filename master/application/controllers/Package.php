<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Package extends CI_Controller 
{
    function __construct()
	{
        parent::__construct();
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        if($this->session->userdata('role') != 0)
        {
            $this->load->helper('access');
            $getData = access($this->session->userdata('department'));
            if(!in_array('package',$getData))
            {
                $this->load->helper('url');
                redirect(base_url()."dashboard", 'refresh');
            }
        }
		$this->load->model('Packagemodel');
        $this->load->model('Countrymodel');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->perPage = 100;
	} 
    public function index()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // delete package data
        if($this->input->post('deletePackageBtn'))
        {
            $this->form_validation->set_rules('deletePackageId','text','required|integer');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->Packagemodel->deletePackageData($this->input->post('deletePackageId'));
                if($getDeleteStatus['message'] == 'success')
                {
                    $this->session->set_flashdata('success','Package deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."package", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong.Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong.Please try again');
                $this->load->helper('url');
                redirect(base_url()."package", 'refresh');
            }
        }
        
        
        
                
        if($this->input->post('saveCountries'))
        {
           // print_R($this->input->post('country'));
            //print_R($this->input->post('price'));
          // echo  $count_Country = count($this->input->post('country'));
          // echo  $count_Price = count($this->input->post('price'));
           
           $this->form_validation->set_rules('country[]', 'Country', 'required');
           $this->form_validation->set_rules('price[]','Price','required');
           
              if($this->form_validation->run() != true)
              {
//echo validation_errors();
               $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."package", 'refresh');
                }
            $array = array();
            $countryss = $this->input->post('country');
            $price = $this->input->post('price');
            for($t=0;$t<count($countryss);$t++)
            {
               $arrays['country'] =  $countryss[$t];
                $arrays['price'] =  $price[$t];
                array_push($array,$arrays);
             //  print_R($countryss);
               
            }
     $datafill = array("houdin_plugin_countries"=>json_encode($array));
            
           $hidden_id =  $this->input->post('hidden_id');
           $this->Packagemodel->setPluginPriceValue($datafill,$hidden_id);
           //
          //print_R($datafill);
        }
         // Custom Pagination
         $getTotalCount = $this->Packagemodel->getPackageTotalCount();
         $config['base_url']    = base_url().'package/';
         $config['uri_segment'] = 2;
         $config['total_rows']  = $getTotalCount['totalRows'];
         $config['per_page']    = $this->perPage;
         $config['num_tag_open'] = '<li>';
         $config['num_tag_close'] = '</li>';
         $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
         $config['cur_tag_close'] = '</a></li>';
         $config['next_link'] = 'Next';
         $config['prev_link'] = 'Prev';
         $config['next_tag_open'] = '<li class="pg-next">';
         $config['next_tag_close'] = '</li>';
         $config['prev_tag_open'] = '<li class="pg-prev">';
         $config['prev_tag_close'] = '</li>';
         $config['first_tag_open'] = '<li>';
         $config['first_tag_close'] = '</li>';
         $config['last_tag_open'] = '<li>';
         $config['last_tag_close'] = '</li>';
         $this->pagination->initialize($config);
         $page = $this->uri->segment(2);
         $offset = !$page?0:$page;
         // End Here
         $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getPackageList = $this->Packagemodel->getPackageDataValue($setArray);
        
 
        
        $this->load->view('package',$getPackageList);
    }
    // add package view page
    public function addpackage()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // add package Data
        if($this->input->post('addPackageData'))
        {
            $this->form_validation->set_rules('packageName','text','required');
            $this->form_validation->set_rules('packagePrice','text','required|numeric');
            $this->form_validation->set_rules('packageDescription','text','required');
            $this->form_validation->set_rules('packageStatus','text','required');
            $this->form_validation->set_rules('products','text','required');
            $this->form_validation->set_rules('staffaccount','text','required');
            $this->form_validation->set_rules('stockLocation','text','required');
            $this->form_validation->set_rules('appaccess','text','required|in_list[limited,full]');
            $this->form_validation->set_rules('orderpm','text','required');
            $this->form_validation->set_rules('freesms','text','required');
            $this->form_validation->set_rules('freeemails','text','required');
            $this->form_validation->set_rules('accountingintegration','text','required|in_list[yes,no]');
            $this->form_validation->set_rules('posintegration','text','required|in_list[yes,no]');
            $this->form_validation->set_rules('deliveryintegration','text','required|in_list[yes,no]');
            $this->form_validation->set_rules('pgintegration','text','required|in_list[yes,no]');
            $this->form_validation->set_rules('templates','text','required');
            if($this->form_validation->run() == true)
            {
                $setInsertArrayData = array(
                    'houdin_package_name'=>$this->input->post('packageName'),
                    'houdin_package_price'=>$this->input->post('packagePrice'),
                    'houdin_package_product'=>$this->input->post('products'),
                    'houdin_package_staff_accounts'=>$this->input->post('staffaccount'),
                    'houdin_package_stock_location'=>$this->input->post('stockLocation'),
                    'houdin_package_app_access'=>$this->input->post('appaccess'),
                    'houdin_package_order_pm'=>$this->input->post('orderpm'),
                    'houdin_package_sms'=>$this->input->post('freesms'),
                    'houdin_package_email'=>$this->input->post('freeemails'),
                    'houdin_package_accounting'=>$this->input->post('accountingintegration'),
                    'houdin_package_pos'=>$this->input->post('posintegration'),
                    'houdin_package_delivery'=>$this->input->post('deliveryintegration'),
                    'houdin_package_payment_gateway'=>$this->input->post('pgintegration'),
                    'houdin_package_templates	'=>$this->input->post('templates'),
                    'houdin_package_description'=>$this->input->post('packageDescription'),
                    'houdin_package_status	'=>$this->input->post('packageStatus'),
                    'houdin_package_created_at'=>strtotime(date('Y-m-d'))
                );
                $getInsertStatus = $this->Packagemodel->setPackageData($setInsertArrayData);
                if($getInsertStatus['message'] == 'success')
                {
                    // $array = array();
                    // $countryss = $this->input->post('country');
                    // $price = $this->input->post('price');
                    // for($t=0;$t<count($countryss);$t++)
                    // {
                    //    $arrays['country'] =  $countryss[$t];
                    //     $arrays['price'] =  $price[$t];
                    //     array_push($array,$arrays);
                    //  //  print_R($datafill);
                    
                    // }
                    // $datafill = array("houdin_package_countries"=>json_encode($array));
                    // $hidden_id =  $getInsertStatus['id'];
                    // $this->Packagemodel->setPackagePriceValue($datafill,$hidden_id);
                    $this->session->set_flashdata('success','Package addedd successfully');
                    $this->load->helper('url');
                    redirect(base_url()."package/addpackage", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package/addpackage", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory or fields are not in proper manner');
                $this->load->helper('url');
                redirect(base_url()."package/addpackage", 'refresh');
            }
        } 
        
        
        $getPackagepageList = $this->Packagemodel->fetchPackageData();
       $country = $this->Countrymodel->getAllCountry(array('active'=>''));
       // print_R($country);
       
        $getPackagepageList['country'] = $country;
        
        $this->load->view('addpackage',$getPackagepageList);
    }
    // edit package view page
    public function editpackage()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        $getPackgeID = $this->uri->segment('3');
        if($getPackgeID == "")
        {
            $this->load->helper('url');
            redirect(base_url()."package", 'refresh');
        }
        // update package information
        if($this->input->post('editPackageInfoBtn'))
        {
            $this->form_validation->set_rules('packageName','text','required');
            $this->form_validation->set_rules('packagePrice','text','required|numeric');
            $this->form_validation->set_rules('packageDescription','text','required');
            $this->form_validation->set_rules('packageStatus','text','required');
            $this->form_validation->set_rules('products','text','required');
            $this->form_validation->set_rules('staffaccount','text','required');
            $this->form_validation->set_rules('stockLocation','text','required');
            $this->form_validation->set_rules('appaccess','text','required|in_list[limited,full]');
            $this->form_validation->set_rules('orderpm','text','required');
            $this->form_validation->set_rules('freesms','text','required');
            $this->form_validation->set_rules('freeemails','text','required');
            $this->form_validation->set_rules('accountingintegration','text','required|in_list[yes,no]');
            $this->form_validation->set_rules('posintegration','text','required|in_list[yes,no]');
            $this->form_validation->set_rules('deliveryintegration','text','required|in_list[yes,no]');
            $this->form_validation->set_rules('pgintegration','text','required|in_list[yes,no]');
            $this->form_validation->set_rules('templates','text','required');
            if($this->form_validation->run() == true)
            {
                $setInsertArrayData = array(
                    'houdin_package_name'=>$this->input->post('packageName'),
                    'houdin_package_price'=>$this->input->post('packagePrice'),
                    'houdin_package_product'=>$this->input->post('products'),
                    'houdin_package_staff_accounts'=>$this->input->post('staffaccount'),
                    'houdin_package_stock_location'=>$this->input->post('stockLocation'),
                    'houdin_package_app_access'=>$this->input->post('appaccess'),
                    'houdin_package_order_pm'=>$this->input->post('orderpm'),
                    'houdin_package_sms'=>$this->input->post('freesms'),
                    'houdin_package_email'=>$this->input->post('freeemails'),
                    'houdin_package_accounting'=>$this->input->post('accountingintegration'),
                    'houdin_package_pos'=>$this->input->post('posintegration'),
                    'houdin_package_delivery'=>$this->input->post('deliveryintegration'),
                    'houdin_package_payment_gateway'=>$this->input->post('pgintegration'),
                    'houdin_package_templates	'=>$this->input->post('templates'),
                    'houdin_package_description'=>$this->input->post('packageDescription'),
                    'houdin_package_status	'=>$this->input->post('packageStatus'),
                    'houdin_package_created_at'=>strtotime(date('Y-m-d'))
                );
                $getUpadtePackageStatus = $this->Packagemodel->updatePackageDetailsData($setInsertArrayData,$getPackgeID);
                if($getUpadtePackageStatus['message'] == 'success')
                {
                    //     $array = array();
                    //     $countryss = $this->input->post('country');
                    //     $price = $this->input->post('price');
                    //     for($t=0;$t<count($countryss);$t++)
                    //     {
                    //         $arrays['country'] =  $countryss[$t];
                    //         $arrays['price'] =  $price[$t];
                    //         array_push($array,$arrays);
                    // }
                    // $datafill = array("houdin_package_countries"=>json_encode($array));
                    // $hidden_id =  $getUpadtePackageStatus['id'];  
                    // $this->Packagemodel->setPackagePriceValue($datafill,$hidden_id);
                
                    $this->session->set_flashdata('success','Package updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."package/editpackage/".$this->uri->segment('3')."", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package/editpackage/".$this->uri->segment('3')."", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory');
                $this->load->helper('url');
                redirect(base_url()."package/editpackage/".$this->uri->segment('3')."", 'refresh');
            }
        }
        // get edit package data
        $getEditPackageData = $this->Packagemodel->getEditPackgeList($getPackgeID);
        
         $country = $this->Countrymodel->getAllCountry(array('active'=>''));
        $getEditPackageData['country']=$country;
        $this->load->view('editpackage',$getEditPackageData);
    }
    public function plugin()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // add plugin data
        if($this->input->post('addPluginBtn'))
        {
            $this->form_validation->set_rules('pluginName','text','required');
            $this->form_validation->set_rules('plugindescription','text','required');
            $this->form_validation->set_rules('pluginStatus','text','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $insertPluginData = array('pluginName'=>$this->input->post('pluginName'),'status'=>$this->input->post('pluginStatus'),'description'=>$this->input->post('plugindescription'));
                $getInsertStatus = $this->Packagemodel->insertPluginData($insertPluginData);
                

                
                
                if($getInsertStatus['message'] == 'success')
                {
                    // upload plugin image
                    if(!empty($_FILES['name']['name']))
                    {
                        $newFileName = rand(100000,999999)."".$_FILES['name']['name'];
                        $config['upload_path'] = "uploads/plugins/";
                        $config['allowed_types'] = 'jpg|jpeg|png';
                        $config['max_size'] = '6144';
                        $config['file_name'] = $newFileName;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('name'))
                        {
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                        $setUpadteArray = array('imageData'=>$picture,'pluginId'=>$getInsertStatus['id']);
                        $getImageStatus = $this->Packagemodel->updatePluginImage($setUpadteArray);
                        if($getImageStatus['message'] == 'success')
                        {
                            $this->session->set_flashdata('success','Plugin addedd sucessfully');
                            $this->load->helper('url');
                            redirect(base_url()."package/plugin", 'refresh');
                        }
                        else
                        {
                            $this->session->set_flashdata('error','Something went wrong during image. Please click on edit button and update the plugin image');
                            $this->load->helper('url');
                            redirect(base_url()."package/plugin", 'refresh');
                        }
                        }
                        else
                        {
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('error',$error);
                            $this->load->helper('url');
                            redirect(base_url()."package/plugin", 'refresh');
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Plugin addedd sucessfully');
                        $this->load->helper('url');
                        redirect(base_url()."package/plugin", 'refresh');
                    } // End Here.
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package/plugin", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory or fields are not in proper manner');
                $this->load->helper('url');
                redirect(base_url()."package/plugin", 'refresh');
            }
        }
        // edit plugin
        if($this->input->post('editPluginData'))
        {
            $this->form_validation->set_rules('editPluginName','text','required');
            $this->form_validation->set_rules('editPluginDescription','text','required');
            $this->form_validation->set_rules('editPluginStatus','text','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $updatePluginData = array('pluginName'=>$this->input->post('editPluginName'),'status'=>$this->input->post('editPluginStatus'),'description'=>$this->input->post('editPluginDescription'),'pluginId'=>$this->input->post('editPluginId'));
                $getupdateStatus = $this->Packagemodel->updatePluginData($updatePluginData);
                if($getupdateStatus['message'] == 'success')
                {
                    // upload plugin image
                    if(!empty($_FILES['name']['name']))
                    {
                        $newFileName = rand(100000,999999)."".$_FILES['name']['name'];
                        $config['upload_path'] = "uploads/plugins/";
                        $config['allowed_types'] = 'jpg|jpeg|png';
                        $config['max_size'] = '6144';
                        $config['file_name'] = $newFileName;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('name'))
                        {
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                        $setUpadteArray = array('imageData'=>$picture,'pluginId'=>$this->input->post('editPluginId'));
                        $getImageStatus = $this->Packagemodel->updatePluginImage($setUpadteArray);
                        if($getImageStatus['message'] == 'success')
                        {
                            $this->session->set_flashdata('success','Plugin addedd sucessfully');
                            $this->load->helper('url');
                            redirect(base_url()."package/plugin", 'refresh');
                        }
                        else
                        {
                            $this->session->set_flashdata('error','Something went wrong during image. Please click on edit button and update the plugin image');
                            $this->load->helper('url');
                            redirect(base_url()."package/plugin", 'refresh');
                        }
                        }
                        else
                        {
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('error',$error);
                            $this->load->helper('url');
                            redirect(base_url()."package/plugin", 'refresh');
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Plugin addedd sucessfully');
                        $this->load->helper('url');
                        redirect(base_url()."package/plugin", 'refresh');
                    } // End Here.
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package/plugin", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory or fields are not in proper manner');
                $this->load->helper('url');
                redirect(base_url()."package/plugin", 'refresh');
            }
        }
        // delete plugin 
        if($this->input->post('deletePluginBtn'))
        {
            $this->form_validation->set_rules('deletePluginId','text','required');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->Packagemodel->deletePlugindataValue($this->input->post('deletePluginId'));
                if($getDeleteStatus['message'] == 'success')
                {
                    $this->session->set_flashdata('success','Plugins deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."package/plugin", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package/plugin", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fields are mandatory or fields are not in proper manner');
                $this->load->helper('url');
                redirect(base_url()."package/plugin", 'refresh');
            }
        }
        
                if($this->input->post('saveCountries'))
        {
           // print_R($this->input->post('country'));
            //print_R($this->input->post('price'));
          // echo  $count_Country = count($this->input->post('country'));
          // echo  $count_Price = count($this->input->post('price'));
           
           $this->form_validation->set_rules('country[]', 'Country', 'required');
           $this->form_validation->set_rules('price[]','Price','required');
           
              if($this->form_validation->run() != true)
              {
//echo validation_errors();
               $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."package", 'refresh');
                }
            $array = array();
            $countryss = $this->input->post('country');
            $price = $this->input->post('price');
            for($t=0;$t<count($countryss);$t++)
            {
               $arrays['country'] =  $countryss[$t];
                $arrays['price'] =  $price[$t];
                array_push($array,$arrays);
             //  print_R($countryss);
               
            }
     $datafill = array("houdin_plugin_countries"=>json_encode($array));
            
           $hidden_id =  $this->input->post('hidden_id');
           $this->Packagemodel->setPluginPriceValue($datafill,$hidden_id);
           //
          //print_R($datafill);
        }

        // get Plugin data
        // Custom Pagination
        $getTotalCount = $this->Packagemodel->getTotalCount();
        $config['base_url']    = base_url().'package/plugin/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        // End Here
        $country = $this->Countrymodel->getAllCountry(array('active'=>''));
      //  print_R($country);
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getPluginData = $this->Packagemodel->getPluginDataValue($setArray);
        $getPluginData['country'] = $country;
        $this->load->view('plugin',$getPluginData);
    }
    public function smspackage()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // add sms package
        if($this->input->post('addSmsPackage'))
        {
            $this->form_validation->set_rules('smsPackageName','Package Name','required');
            $this->form_validation->set_rules('smsPackagePrice','Package Price','required|integer');
            $this->form_validation->set_rules('smsPackagecount','Package Count','required|integer');
            $this->form_validation->set_rules('smsPackageStatus','Package Status','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setData = strtotime(date('Y-m-d h:i:s'));
                $setInsertArray = array('houdin_sms_package_name'=>$this->input->post('smsPackageName'),'houdin_sms_package_price'=>$this->input->post('smsPackagePrice'),'houdin_sms_package_sms_count'=>$this->input->post('smsPackagecount'),'houdin_sms_package_status'=>$this->input->post('smsPackageStatus'),'houdin_sms_package_created_at'=>$setData);
                $getSMSInsertStatus = $this->Countrymodel->insertSMSPackage($setInsertArray);
                if($getSMSInsertStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','SMS Package Updated Successfully');
                    $this->load->helper('url');
                    redirect(base_url()."package/smspackage", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package/smspackage", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."package/smspackage", 'refresh');
            }
        }
        // delete sms package
        if($this->input->post('deleteSMSpackage'))
        {
            $this->form_validation->set_rules('deleSMSPackageId','text','required|integer');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->Countrymodel->deleteSMSPackage($this->input->post('deleSMSPackageId'));
                if($getDeleteStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Package deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."package/smspackage", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package/smspackage", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."package/smspackage", 'refresh');
            }
        }
        // edit sms package data
        if($this->input->post('editSMSPackage'))
        {
            $this->form_validation->set_rules('editSMSPackageName','Package Name','required');
            $this->form_validation->set_rules('editSMSPackageId','text','required|integer');
            $this->form_validation->set_rules('editSMSPackagePrice','Package Price','required|integer');
            $this->form_validation->set_rules('editSMSPackageCount','Package Count','required|integer');
            $this->form_validation->set_rules('editSMSPackageStatus','Package Status','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setData = strtotime(date('Y-m-d h:i:s'));
                $setUpdateArray = array('houdin_sms_package_name'=>$this->input->post('editSMSPackageName'),'houdin_sms_package_price'=>$this->input->post('editSMSPackagePrice'),'houdin_sms_package_sms_count'=>$this->input->post('editSMSPackageCount'),'houdin_sms_package_status'=>$this->input->post('editSMSPackageStatus'),'houdin_sms_package_modified_at'=>$setData);
                $setUpdateId = array('houdin_sms_package_id'=>$this->input->post('editSMSPackageId'));
                $getUpdateSMSPackageStatus = $this->Countrymodel->updateSMSPackage($setUpdateArray,$setUpdateId);
                if($getUpdateSMSPackageStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Package updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."package/smspackage", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package/smspackage", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."package/smspackage", 'refresh');
            }
        }
        $getSmsPackageDetails = $this->Countrymodel->fetchSmsPackageData();
        $this->load->view('smspackage',$getSmsPackageDetails);
    }
    public function emailpackage()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // add email package
        if($this->input->post('addEmailPackage'))
        {
            $this->form_validation->set_rules('emailPackageName','Package Name','required');
            $this->form_validation->set_rules('emailPackagePrice','Package Price','required|integer');
            $this->form_validation->set_rules('emailPackageCount','Package Count','required|integer');
            $this->form_validation->set_rules('emailPackageStatus','Package Status','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setData = strtotime(date('Y-m-d h:i:s'));
                $setEmailPackageInsertArray = array('houdin_email_package_name'=>$this->input->post('emailPackageName'),'houdin_email_package_price'=>$this->input->post('emailPackagePrice'),'houdin_email_package_email_count'=>$this->input->post('emailPackageCount'),'houdin_email_package_status'=>$this->input->post('emailPackageStatus'),'houdin_email_package_created_at'=>$setData);
                $getInsertStatus = $this->Countrymodel->insertEmailPackageData($setEmailPackageInsertArray);
                if($getInsertStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Email Package added successfully');
                    $this->load->helper('url');
                    redirect(base_url()."package/emailpackage", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package/emailpackage", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."package/emailpackage", 'refresh');
            }
        }
        // delete package 
        if($this->input->post('deleteEmailPackage'))
        {
            $this->form_validation->set_rules('deleteEmailPackageId','text','required|integer');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->Countrymodel->deleteEmailPackageId($this->input->post('deleteEmailPackageId'));
                if($getDeleteStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Package deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."package/emailpackage", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package/emailpackage", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."package/emailpackage", 'refresh');
            }
        }
        // update email package
        if($this->input->post('editEmailPackage'))
        {
            $this->form_validation->set_rules('editemailPackageName','Package Name','required');
            $this->form_validation->set_rules('EditEmailpackageId','text','required|integer');
            $this->form_validation->set_rules('editemailPackagePrice','Package Price','required|integer');
            $this->form_validation->set_rules('editemailPackageCount','Package Count','required|integer');
            $this->form_validation->set_rules('editemailPackageStatus','Package Status','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $setData = strtotime(date('Y-m-d h:i:s'));
                $setUpdateEmailArray = array('houdin_email_package_name'=>$this->input->post('editemailPackageName'),'houdin_email_package_price'=>$this->input->post('editemailPackagePrice'),'houdin_email_package_email_count'=>$this->input->post('editemailPackageCount'),'houdin_email_package_status'=>$this->input->post('editemailPackageStatus'),'houdin_email_package_modified_at'=>$setData);
                $setUpdateCondition = array('houdin_email_package_id'=>$this->input->post('EditEmailpackageId'));
                $getUpdateStatus = $this->Countrymodel->updateEmailPackage($setUpdateEmailArray,$setUpdateCondition);
                if($getUpdateStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Package updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."package/emailpackage", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."package/emailpackage", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."package/emailpackage", 'refresh');
            }
        }
        $getEmailPackageDetails = $this->Countrymodel->fetchEmailPackageDetail();
        $this->load->view('emailpackage',$getEmailPackageDetails);
    }
}
