<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); 
$getCurrency = getVendorCurrency();
if($getCurrency[0]->houdin_users_currency=="USD")
{
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="AUD"){
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="Euro"){
	$currencysymbol= "£";
}else if($getCurrency[0]->houdin_users_currency=="Pound"){
	$currencysymbol= "€";
}else if($getCurrency[0]->houdin_users_currency=="INR"){
	$currencysymbol= "₹";
}
?>
<style type="text/css">
	.heading_content ul{list-style: none;    padding-left: 0px;}
	.heading_content ul li a{    padding: 7px;
    display: block;}
	.heading_content>ul>li>ul>li> a{ padding-left: 10px;}
	.heading_content>ul>li>ul>li>ul>li> a{ padding-left: 25px;}
	.heading_content>ul>li>ul>li>ul>li>ul>li a{ padding-left: 40px;}

	.border-t{border-top: 1px solid #ddd;}
	.border-b{border-bottom: 1px solid #ddd;}
	.text-gray{color: #797979;}
	ul.tree, .tree li {
    list-style: none;
    margin:0;
    padding:0;
    cursor: pointer;
}

.tree ul {
  display:none;
}

.tree > li {
  display:block;
  background:#f4f8fb;
  margin-bottom:2px;
}

.tree span {
  display:block;
  padding:10px 12px;

}

.icon {
  display:inline-block;
}

.tree .hasChildren > .expanded {
  background:#f4f8fb;
}

.tree .hasChildren > .expanded a {
  color:#464646;
}

.icon:before {
  content:"+";
  display:inline-block;
  min-width:20px;
  text-align:center;
}
.tree .icon.expanded:before {
  content:"-";
}

.show-effect {
  display:block!important;
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">

                <div class="col-md-12">
                   <h4 class="page-title">Balance Sheet</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url() ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>Accounts">Accounts</a></li>
                  <li class="active">Balance Sheet</li>
                  </ol>
                  </div>
            </div>
            <div class="row m-t-20">
           				<div class="col-sm-12">
						   <div class="col-md-4">
												<div class="form-group m-r-10">
												<label for="exampleInputEmail2">From</label>
													<input type="text" value="<?php echo $from ?>" class="form-control date_picker dateFromText commonChangeClassData" id="" placeholder="">
												</div>
												</div>
												<div class="col-md-4">
												<div class="form-group m-r-10">
													<label for="exampleInputEmail2">To</label>
													<input type="text" value="<?php echo $to ?>" class="form-control date_picker dateToText commonChangeClassData" id="" >
												</div>
												</div>

                            			</div>
                      </div>
                      <div class="m-t-40 ">
					  <div class="setErrorMessage"></div>
					  <!-- set final amoint -->
					  <?php
						   if($totalRemainingCredit) { $totalRemainingCredit = $totalRemainingCredit; } else { $totalRemainingCredit = 0; }
						   if($totalCreditedAmount) { $totalCreditedAmount = $totalCreditedAmount; } else { $totalCreditedAmount = 0; }
						   $getTotalAccountRecievable = $totalRemainingCredit+$totalCreditedAmount;
						   if($finalPaidDebitAmount) { $finalPaidDebitAmount = $finalPaidDebitAmount; } else { $finalPaidDebitAmount = 0; }
						   if($remainingDebitAmount) { $remainingDebitAmount = $remainingDebitAmount; } else { $remainingDebitAmount = 0; }
						   $getTotalCurrentLibalties = $finalPaidDebitAmount+$remainingDebitAmount;
						   $getProfitData = $getTotalAccountRecievable-$getTotalCurrentLibalties;
					  ?>
                      <div class="col-md-12 bg-white">

                      <!-- <div class="col-md-2"></div> -->
                      <div class="col-lg-12 m-t-30"> 
								<div class="panel panel-default" style="border: 1px solid #ddd;">
									<div class="panel-heading pull-left" style="width: 100%;">
										<h3 class="panel-title pull-left">Balance sheet</h3>
										<div class="icon-right pull-right">
										<!-- <a href="<?php //echo base_url(); ?>Accounts/balancesheetpdf/<?php //echo $from ?>/<?php //echo $to; ?>" class="btn setPdfUrl btn-default"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a> -->

										</div>
									</div>
									<div class="panel-body">

                               	<!-- <ul class="tree">
 

  
	<li class="tree__item hasChildren" style="margin-top: 40px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Assets</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Fixed asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Non Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>


		</ul>

	</li>
	


	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Account Receivable and Payable</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>
	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Bank and Credit card</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>
	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Liabltied and Equity</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>

	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Expense and Other expense</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>

	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Undeposited fund</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>
	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Income and Other income</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>
	
	<li class="tree__item hasChildren">

		<span>
	        <div class="icon"></div>
			<a href="#">Other Data</a>
		</span>

		<ul>
			<li>
				<span><a href="#">Acting</a></span>
			</li>

			<li>
				<span><a href="#">Biomechanics</a></span>
			</li>

			<li>
				<span><a href="#">Improvisation</a></span>
			</li>

		</ul>

	</li>

	
</ul> -->


									<div class="heading_sheet text-center">
									<?php $getVendorInfo = fetchvendorinfo(); ?>
									<h4><?php echo $getVendorInfo['name'] ?></h4>
									<h5><strong>BALANCE SHEET</strong></h5>
									<p class="setDateRange">Till <?php echo date('d-m-Y') ?><?php //echo $from." - ".$to ?></p>
									</div>
									<div class="heading_content">

									<ul class="">
									<li class=" border-b border-t pull-left" style="width: 100%;"><a class="text-gray pull-right"><strong>TOTAL</strong></a></li></ul>
									<ul class="">
										<!-- Assets Data -->
										<?php  
										if(count($currentAssets) > 0 || count($fixedAssets) > 0 || count($non) > 0)
										{
											$setCurrentBal = 0;
											$setFixedBal = 0;
											$setNonBal = 0;
										?>
										<li class="mainAssetsData"><a class="text-gray">Assets</a>
										<ul class="">
											<?php 
											if(count($currentAssets) > 0)
											{
											?>
											<li class="currentAssets"><a class="text-gray">Current assets</a>
												<ul class="currentAssetUl">
													<?php 
													
													for($index = 0; $index < count($currentAssets); $index++)
													{
													?>
													<li><a><span class="text-gray"><?php echo $currentAssets[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $currentAssets[$index]['balance'] ?></span></a></li>
													<?php $setCurrentBal = ($setCurrentBal)+($currentAssets[$index]['balance']); 
													}
													?>
												<li class="border-b totalCurrentAssets"><a class="text-gray"><strong><span>Total Current assets</span><span class="pull-right totalCurrentAssetsBal"><?php echo $currencysymbol; ?><?php echo $setCurrentBal ?></span></strong></a></li>
												</ul>
											</li>
											<?php }
											?>
											<?php 
											if(count($fixedAssets) > 0)
											{
											?>
											<li class="fixedAssets"><a class="text-gray">Fixed assets</a>
												<ul class="fixedAssetUL">
												<?php 
													
													for($index = 0; $index < count($fixedAssets); $index++)
													{
													?>
													<li><a><span class="text-gray"><?php echo $fixedAssets[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $fixedAssets[$index]['balance'] ?></span></a></li>
													<?php $setFixedBal = ($setFixedBal)+($fixedAssets[$index]['balance']); }
													?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Fixed assets</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setFixedBal ?></span></strong></a></li>
												</ul>
											</li>
											<?php }
										
											if(count($non) > 0)
											{
											?>
											<li class="nonAssets"><a class="text-gray">Non Current assets</a>
												<ul class="nonAssetsUL">
													<?php 
													
													for($index = 0; $index < count($non); $index++)
													{
													?>
													<li><a><span class="text-gray"><?php echo $non[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $non[$index]['balance'] ?></span></a></li>
													<?php $setNonBal = ($setNonBal)+($non[$index]['balance']); }
													?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Current assets</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setNonBal ?></span></strong></a></li>
												</ul>
											</li>
											<?php }
											?>
											<?php $setTotalAssets = ($setCurrentBal)+($setFixedBal)+($setNonBal);
											?>
											
											<li class="border-b border-t"><span class="text-gray"><strong>Total Assets</strong></span><span class="pull-right text-gray totalAssestsData"><?php echo $currencysymbol; ?><?php echo $setTotalAssets; ?></span></a></li>
										</ul>
									</li>
										<?php }
										?>
									
										<!-- Account receivable and payable -->
										<?php 
										if(count($receiveable) > 0 || count($payable) > 0)
										{
											$setRecevableBal = 0;
											$setPaybaleBal = 0;
										?>
										<li class="mainReceivablePayble"><a class="text-gray">Account Receivable And Payable</a>
										<ul class="">
											<?php 
											if(count($receiveable) > 0)
											{
											?>
											<li class="accountReceivable"><a class="text-gray">Account Receivable</a>
												<ul class="accountReceivableUL">
												<?php 
												for($index = 0; $index < count($receiveable); $index++)
												{
												?>
												<li><a><span class="text-gray"><?php echo $receiveable[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $receiveable[$index]['balance'] ?></span></a></li>
												<?php $setRecevableBal = ($setRecevableBal)+($receiveable[$index]['balance']); }
												?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Account Receivable</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setRecevableBal ?></span></strong></a></li>
												</ul>
											</li>
											<?php }
											if(count($payable) > 0)
											{
											?>
												<li class="accountPayable"><a class="text-gray">Account Payable</a>
												<ul class="accountPayableUL">
												<?php 
												for($index = 0; $index < count($payable); $index++)
												{
												?>
												<li><a><span class="text-gray"><?php echo $payable[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $payable[$index]['balance'] ?></span></a></li>
												<?php $setPaybaleBal = ($setPaybaleBal)+($payable[$index]['balance']); }
												?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Account Payable</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setPaybaleBal; ?></span></strong></a></li>
												</ul>
											</li>
											<?php }
											$setFinalBal = ($setRecevableBal)+($setPaybaleBal);
											?>
											<li class="border-b border-t"><span class="text-gray"><strong>Total Receivable And Payable</strong></span><span class="pull-right text-gray totalRecPayDataVa"><?php echo $currencysymbol; ?><?php echo $setFinalBal; ?></span></a></li>
										</ul>
									</li>
										<?php }
										?>
									<!-- Bank and credit card -->
									<?php 
									if(count($bank) > 0 || count($credit) > 0)
									{
										$setBankeBal = 0;
										$setCreditBal = 0;
									?>
									<li class="mainBankCredit"><a class="text-gray">Bank and Credit Card</a>
										<ul class="">
											<?php 
											if(count($bank) > 0)
											{
											?>
											<li class="bankdata"><a class="text-gray">Bank</a>
												<ul class="bankDataUL">
												<?php 
												for($index = 0; $index < count($bank); $index++)
												{
												?>
												<li><a><span class="text-gray"><?php echo $bank[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $bank[$index]['balance'] ?></span></a></li>
												<?php $setBankeBal = ($setBankeBal)+($bank[$index]['balance']); }
												?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Bank</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setBankeBal ?></span></strong></a></li>
												</ul>
											</li>
											<?php }
											?>
											<?php 
											if(count($credit) > 0)
											{
											?>
												<li class="creditData"><a class="text-gray">Credit card</a>
												<ul class="creditDataUL">
												<?php 
												for($index = 0; $index < count($credit); $index++)
												{
												?>
												<li><a><span class="text-gray"><?php echo $credit[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $credit[$index]['balance'] ?></span></a></li>
												<?php $setCreditBal = ($setCreditBal)+($credit[$index]['balance']); }
												?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Credit Card</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setCreditBal; ?></span></strong></a></li>
												</ul>
											</li>
											<?php }
											$setFinalCreditBankBal = ($setBankeBal)+($setCreditBal);
											?>
											<li class="border-b border-t"><span class="text-gray"><strong>Total Bank and Credit Card</strong></span><span class="pull-right text-gray totalBankCreditData"><?php echo $currencysymbol; ?><?php echo $setFinalCreditBankBal; ?></span></a></li>
										</ul>
									</li>
									<?php }
									?>
									<!-- liabltied and equity -->
									<?php 
									if(count($liblties) > 0 || count($equity) > 0)
									{
										$setLibaltiesBal = 0;
										$setEquityBal = 0;
									?>
									<li class="mainlibeq"><a class="text-gray">Liabilities and Equity</a>
										<ul class="">
											<?php 
											if(count($liblties) > 0 )
											{
											?>
												<li class="libaltiesData"><a class="text-gray">Liabilities</a>
												<ul class="libaltiesDataUL">
												<?php 
												for($index = 0; $index < count($liblties); $index++)
												{
												?>
												<li><a><span class="text-gray"><?php echo $liblties[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $liblties[$index]['balance'] ?></span></a></li>
												<?php $setLibaltiesBal = ($setLibaltiesBal)+($liblties[$index]['balance']); }
												?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Liabilities</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setLibaltiesBal; ?></span></strong></a></li>
												</ul>
											</li>
											<?php }
											?>
											<?php 
											if(count($equity) > 0)
											{
											?>
											<li class="equityData"><a class="text-gray">Equity</a>
												<ul class="equityDataUL">
												<?php 
												for($index = 0; $index < count($equity); $index++)
												{
												?>
												<li><a><span class="text-gray"><?php echo $equity[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $equity[$index]['balance'] ?></span></a></li>
												<?php $setEquityBal = ($setEquityBal)+($equity[$index]['balance']); }
												?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Equity</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setEquityBal ?></span></strong></a></li>
												</ul>
											</li>
										<?php }
										$setFinalBal = ($setLibaltiesBal)+($setEquityBal);
											?>
											
											<li class="border-b border-t"><span class="text-gray"><strong>Total Liabilities and Equity</strong></span><span class="pull-right text-gray totalLibaltiesEquity"><?php echo $currencysymbol; ?><?php echo $setFinalBal; ?></span></a></li>
										</ul>
									</li>
									<?php }
									?>
									<!-- expense and other expense -->
									<?php 
									if(count($expense) > 0 || count($otherExpense) > 0) 
									{
										$setExpense = 0;
										$setOtherExpense = 0;
									?>
									<li class="mainexpotherexp"><a class="text-gray">Expense and Other Expense</a>
										<ul class="">
											<?php 
											if(count($expense) > 0)
											{
											?>
											<li class="expenseData"><a class="text-gray">Expense</a>
												<ul class="expenseDataUL">
												<?php 
												for($index = 0; $index < count($expense); $index++)
												{
												?>
												<li><a><span class="text-gray"><?php echo $expense[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $expense[$index]['balance'] ?></span></a></li>
												<?php $setExpense = ($setExpense)+($expense[$index]['balance']); }
												?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Expense</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setExpense; ?></span></strong></a></li>
												</ul>
											</li>
											<?php }
											?>
											<?php 
											if(count($otherExpense) > 0)
											{
											?>
											<li class="otherExpenseData"><a class="text-gray">Other Expense</a>
												<ul class="otherExpenseDataUL">
												<?php 
												for($index = 0; $index < count($otherExpense); $index++)
												{
												?>
												<li><a><span class="text-gray"><?php echo $otherExpense[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $otherExpense[$index]['balance'] ?></span></a></li>
												<?php $setOtherExpense = ($setOtherExpense)+($otherExpense[$index]['balance']); }
												?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Other Expense</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setOtherExpense ?></span></strong></a></li>
												</ul>
											</li>
											<?php } 
											$setFinalExpense = ($setExpense)+($setOtherExpense);
											?>
											<li class="border-b border-t"><span class="text-gray"><strong>Total Expense and Other Expense</strong></span><span class="pull-right text-gray finalExpOthExp"><?php echo $currencysymbol; ?><?php echo $setFinalExpense; ?></span></a></li>
										</ul>
									</li>
									<?php }
									?>
									<!-- undeposited fund -->
									<?php 
									if(count($undeposited) > 0)
									{
										$setUndeposited = 0;
									?>
									<li class="mainUndepositedFund"><a class="text-gray">Undeposited Fund</a>
										<ul class="undepositedfundUl">
										<?php 
										for($index = 0; $index < count($undeposited); $index++)
										{
										?>
										<li><a><span class="text-gray"><?php echo $undeposited[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $undeposited[$index]['balance'] ?></span></a></li>
										<?php $setUndeposited = ($setUndeposited)+($undeposited[$index]['balance']); }
										?>
											<li class="border-b border-t"><span class="text-gray"><strong>Total Undeposited Fund</strong></span><span class="pull-right text-gray totalUndepositedFundData"><?php echo $currencysymbol; ?><?php echo $setUndeposited; ?></span></a></li>
										</ul>
									</li>
									<?php 
									}
									?>
									<!-- income and other income -->
									<?php 
									if(count($income) > 0 || count($otherincome) > 0)
									{
										$setIncomeData = 0;
										$setOtherIncomeData = 0;
									?>
									<li class="mainIncomeOtherincome"><a class="text-gray">Income and Other Income</a>
										<ul class="">
											<?php 
											if(count($income) > 0)
											{
											?>
											<li class="incomeData"><a class="text-gray">Income</a>
												<ul class="incomeDataUL">
												<?php 
										for($index = 0; $index < count($income); $index++)
										{
										?>
										<li><a><span class="text-gray"><?php echo $income[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $income[$index]['balance'] ?></span></a></li>
										<?php $setIncomeData = ($setIncomeData)+($income[$index]['balance']); }
										?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Income</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setIncomeData; ?></span></strong></a></li>
												</ul>
											</li>
											<?php 
											}
											?>
											<?php 
											if(count($otherincome) > 0)
											{
											?>
											<li class="otheincomeData"><a class="text-gray">Other Income</a>
												<ul class="otheincomeDataUL">
												<?php 
										for($index = 0; $index < count($otherincome); $index++)
										{
										?>
										<li><a><span class="text-gray"><?php echo $otherincome[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $otherincome[$index]['balance'] ?></span></a></li>
										<?php $setOtherIncomeData = ($setOtherIncomeData)+($otherincome[$index]['balance']); }
										?>
												<li class="border-b"><a class="text-gray"><strong><span>Total Other Income</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $setOtherIncomeData; ?></span></strong></a></li>
												</ul>
											</li>
											<?php } 
											$setFinalIncome = ($setIncomeData)+($setOtherIncomeData);
											?>
											<li class="border-b border-t"><span class="text-gray"><strong>Total Income and Other Income</strong></span><span class="pull-right text-gray finalIncomeOtherIncome"><?php echo $currencysymbol; ?><?php echo $setFinalIncome; ?></span></a></li>
										</ul>
									</li>
									<?php }
									?>
									<!-- other data -->
									<?php 
									if(count($other) > 0)
									{
										$setOtherData = 0;
									?>
									<li class="mainOtherData"><a class="text-gray">Other</a>
										<ul class="">
											<li><a class="text-gray">Other</a>
												<ul class="OtherDataUl">
												<?php 
										for($index = 0; $index < count($other); $index++)
										{
										?>
										<li><a><span class="text-gray"><?php echo $other[$index]['name'] ?></span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $other[$index]['balance'] ?></span></a></li>
										<?php $setOtherData = ($setOtherData)+($other[$index]['balance']); }
										?>
												<li class="border-b border-t"><a class="text-gray"><strong><span>Total Other</span><span class="pull-right finalSetOtherData"><?php echo $currencysymbol; ?><?php echo $setOtherData; ?></span></strong></a></li>
												</ul>
											</li>
										</ul>
									</li>
								<?php	}
									?>
									
									<!-- <li><a class="text-gray">Assets</a>
										<ul class="">
											<li><a class="text-gray">Current Assets</a>
												<ul class="">
												<li><a><span class="text-gray">Undeposit Funds</span><span class="pull-right text-gray undepositFundsData"><?php echo $currencysymbol; ?><?php echo $totalRemainingCredit; ?></span></a></li>
												<li><a class="text-gray">Account Received</a>
														<ul class="">
														<li><a class="text-gray"><span class="">Account Received</span><span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $totalCreditedAmount; ?></span></a></li>
															<li class="text_black border-t border-b">
															<a class="text-gray">
															<strong><span>Total Accounts Recieved</span>
															<span class="pull-right totalAmountCreditData"><?php echo $currencysymbol; ?><?php echo $totalCreditedAmount; ?></span>
															</strong>
															</a>
															</li>
														</ul>
													</li>
													<li><a class="text-gray">Account Receivable (Debtors)</a>
														<ul class="">
															<li><a class="text-gray"><span class="">Account Receivable (Debtors)</span><span class="pull-right totalRemainigCredit"><?php echo $currencysymbol; ?><?php echo $currencysymbol; ?><?php echo $totalRemainingCredit; ?></span></a></li>
															<li class="text_black border-t border-b">
															<a class="text-gray">
															<strong><span>Total Accounts receivable (Debtors)</span>
															<span class="pull-right totalRemainigCredit"><?php echo $currencysymbol; ?><?php echo $totalRemainingCredit; ?></span>
															</strong>
															</a>
															</li>
														</ul>
													</li>
													<li class=" border-b">
													<a class="text-gray">
													<strong>
													<span>Total Current Assets</span>
													<span class="pull-right totalAccountRecievableData"><?php echo $currencysymbol; ?><?php echo $getTotalAccountRecievable ?></span></strong></a></li>
												</ul>
											</li>
										</ul>
									</li>
									<li class="border-b"> <a class="text-gray"><strong><span>Total Assets</span><span class="pull-right totalAccountRecievableData"><?php echo $currencysymbol; ?><?php echo $getTotalAccountRecievable ?></span></strong></a></li>
									<li class="border-b"></li>
									<li><a class="text-gray">Liabilities and Eduitiy</a>
										<ul>




										<li><a class="text-gray">Account Paid</a>
										<ul>
											<li><a class="text-gray"><span class="">Accounts Paid  </span><span class="pull-right accountPaidData"><?php echo $currencysymbol; ?><?php echo $finalPaidDebitAmount; ?></span></a></li>
											<li class="border-b border-t"><a class="text-gray"><strong><span class="">Total Accounts Paid </span><span class="pull-right accountPaidData"><?php echo $currencysymbol; ?><?php echo $finalPaidDebitAmount; ?></span></strong></a></li>
										</ul>
									</li>
									<li><a class="text-gray">Account Payable</a>
										<ul>
											<li><a class="text-gray"><span class="">Accounts Payable (Creditors)</span><span class="pull-right remainigVebitAmountData"><?php echo $currencysymbol; ?><?php echo $remainingDebitAmount; ?></span></a></li>
											<li class="border-b border-t"><a class="text-gray"><strong><span class="">Total Accounts Payable </span><span class="pull-right remainigVebitAmountData"><?php echo $currencysymbol; ?><?php echo $remainingDebitAmount; ?></span></strong></a></li>
										</ul>
									</li>
									<li><a class="text-gray"><strong><span class="">Total Current Liabilities </span><span class="pull-right currentLibalityData"><?php echo $currencysymbol; ?><?php echo $getTotalCurrentLibalties; ?></span></strong></a></li></ul>
									</li>
									<li><a class="text-gray">Equtity</a>
									<ul>
									<li><a class="text-gray">Retanied Earnings</a></li>
									<li><a class="text-gray"><span class="">Profit</span><span class="pull-right totalProfitData"><?php echo $currencysymbol; ?><?php echo $getProfitData ?></span></a></li>
									<li class="border-t border-b"><a class="text-gray"><strong><span class="">Total Equity</span><span class="pull-right totalProfitData"><?php echo $currencysymbol; ?><?php echo $getProfitData ?></span></strong></a></li>
									</ul>
									</li>
									<li class="border-b"><a class="text-gray"><strong><span class="">Total Liabilities and Equity</span><span class="pull-right totalAccountRecievableData"><?php echo $currencysymbol; ?><?php echo $getTotalAccountRecievable; ?></span></strong></a></li>
									<li class="border-b"></li> -->
									</ul>
									</div>
									</div>
								</div>
							</div>
							<div class="col-md-2"></div>
                      </div>
                      </div>
                    </div>
                  </div>
 <?php $this->load->view('Template/footer.php') ?>
 <script type="text/javascript">
		$(document).ready(function(){
			var setBaseData = "<?php echo base_url(); ?>";
			$(document).on('change','.commonChangeClassData',function(){
				$('.setErrorMessage').html('');
				var fromdate = $('.dateFromText').val();
				var toDate = $('.dateToText').val();
				var setPDFUrl = setBaseData+"Accounts/balancesheetpdf/"+fromdate+"/"+toDate;
				$('.setPdfUrl').attr('href',setPDFUrl);
				jQuery.ajax({
                type: "POST",
                url: setBaseData+"Accounts/updateBalanceSheeReport",
                data: {"fromdate": fromdate,"toDate": toDate},
                success: function(data) {
					var getFinalData = $.parseJSON(data);
					if(getFinalData.message == "Success")
					{
						console.log(getFinalData);
						var setCurrencyData = '<?php echo $currencysymbol; ?>';
						//  from and to date 
						// $('.dateFromText').val('').val(getFinalData.from);
						// $('.dateToText').val('').val(getFinalData.to);
						$('.setDateRange').text(getFinalData.from+" - "+getFinalData.to);
						// set pdf url
						// asset data
						if(getFinalData.currentAssets.length <= 0 && getFinalData.fixedAssets.length <= 0 && getFinalData.non.length <= 0)
						{
							$('.mainAssetsData').hide();
						}
						else
						{
							$('.mainAssetsData').show();
							var currentAssetsBal = 0;
							var fixedAssetsBal = 0;
							var nonAssetsBal = 0;
							// current assets
							if(getFinalData.currentAssets.length > 0)
							{
								$('.currentAssets').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.currentAssets.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.currentAssets[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.currentAssets[index].balance+'</span></a></li>';
									currentAssetsBal = parseFloat(currentAssetsBal)+parseFloat(getFinalData.currentAssets[index].balance);
								}
								setHtml += '<li class="border-b totalCurrentAssets"><a class="text-gray"><strong><span>Total Current assets</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+currentAssetsBal+'</span></strong></a></li>';
								$('.currentAssetUl').html('').html(setHtml);
							}
							else
							{
								$('.currentAssets').hide();
							}
							// end current assets
							// fixed assets
							if(getFinalData.fixedAssets.length > 0)
							{
								$('.fixedAssets').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.fixedAssets.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.fixedAssets[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.fixedAssets[index].balance+'</span></a></li>';
									fixedAssetsBal = parseFloat(fixedAssetsBal)+parseFloat(getFinalData.fixedAssets[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Fixed assets</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+fixedAssetsBal+'</span></strong></a></li>';
								$('.fixedAssetUL').html('').html(setHtml);
							}
							else
							{
								$('.fixedAssets').hide();
							}
							// end fixed assets
							// non current assets
							if(getFinalData.non.length > 0)
							{
								$('.nonAssets').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.non.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.non[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.non[index].balance+'</span></a></li>';
									nonAssetsBal = parseFloat(nonAssetsBal)+parseFloat(getFinalData.non[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Non Current assets</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+nonAssetsBal+'</span></strong></a></li>';
								$('.nonAssetsUL').html('').html(setHtml);
							}
							else
							{
								$('.nonAssets').hide();
							}
							// end non current assets
							var totalAssetsValue = parseFloat(currentAssetsBal)+parseFloat(fixedAssetsBal)+parseFloat(nonAssetsBal);
							totalAssetsValue = setCurrencyData+' '+totalAssetsValue;
							$('.totalAssestsData').text('').text(totalAssetsValue);
						}
						// end asset data
						// start account receivable and payable
						if(getFinalData.receiveable.length <= 0 && getFinalData.payable.length <= 0)
						{
							$('.mainReceivablePayble').hide();
						}
						else
						{
							$('.mainReceivablePayble').show();
							var receivableBal = 0;
							var payableBal = 0;
							// account Receivable
							if(getFinalData.receiveable.length > 0)
							{
								$('.accountReceivable').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.receiveable.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.receiveable[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.receiveable[index].balance+'</span></a></li>';
									receivableBal = parseFloat(receivableBal)+parseFloat(getFinalData.receiveable[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Receivable</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+receivableBal+'</span></strong></a></li>';
								$('.accountReceivableUL').html('').html(setHtml);
							}
							else
							{
								$('.accountReceivable').hide();
							}
							// account payable
							if(getFinalData.payable.length > 0)
							{
								$('.accountPayable').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.payable.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.payable[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.payable[index].balance+'</span></a></li>';
									payableBal = parseFloat(payableBal)+parseFloat(getFinalData.payable[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Payable</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+payableBal+'</span></strong></a></li>';
								$('.accountPayableUL').html('').html(setHtml);
							}
							else
							{
								$('.accountPayable').hide();
							}

							var totalRecPayValue = parseFloat(receivableBal)+parseFloat(payableBal);
							totalRecPayValue = setCurrencyData+' '+totalRecPayValue;
							$('.totalRecPayDataVa').text('').text(totalRecPayValue);
						}
						// end account receivable and payable
						// start bank and credit card
						if(getFinalData.bank.length <= 0 && getFinalData.credit.length <= 0)
						{
							$('.mainBankCredit').hide();
						}
						else
						{
							$('.mainBankCredit').show();
							var bankBal = 0;
							var creditBal = 0;
							// start bank here
							if(getFinalData.bank.length > 0)
							{
								$('.bankdata').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.bank.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.bank[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.bank[index].balance+'</span></a></li>';
									bankBal = parseFloat(bankBal)+parseFloat(getFinalData.bank[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Bank</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+bankBal+'</span></strong></a></li>';
								$('.bankDataUL').html('').html(setHtml);
							}
							else
							{
								$('.bankdata').hide();
							}
							// end bank here
							// credit data
							if(getFinalData.credit.length > 0)
							{
								$('.creditData').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.credit.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.credit[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.credit[index].balance+'</span></a></li>';
									creditBal = parseFloat(creditBal)+parseFloat(getFinalData.credit[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Credit</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+creditBal+'</span></strong></a></li>';
								$('.creditDataUL').html('').html(setHtml);
							}
							else
							{
								$('.creditData').hide();
							}
							// end credit data
							var totalbankcreditValue = parseFloat(creditBal)+parseFloat(bankBal);
							totalbankcreditValue = setCurrencyData+' '+totalbankcreditValue;
							$('.totalBankCreditData').text('').text(totalbankcreditValue);
						}
						// end bank and credit card
						// start libalties and equity
						if(getFinalData.liblties.length <= 0 && getFinalData.equity.length <= 0)
						{
							$('.mainlibeq').hide();
						}
						else
						{
							$('.mainlibeq').show();
							var libaltiesBal = 0;
							var equityBal = 0;
							// Start liablties data
							if(getFinalData.liblties.length > 0)
							{
								$('.libaltiesData').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.liblties.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.liblties[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.liblties[index].balance+'</span></a></li>';
									libaltiesBal = parseFloat(libaltiesBal)+parseFloat(getFinalData.liblties[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Liabilities</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+libaltiesBal+'</span></strong></a></li>';
								$('.libaltiesDataUL').html('').html(setHtml);
							}
							else
							{
								$('.libaltiesData').hide();
							}
							// Start equity data
							if(getFinalData.equity.length > 0)
							{
								$('.equityData').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.equity.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.equity[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.equity[index].balance+'</span></a></li>';
									equityBal = parseFloat(equityBal)+parseFloat(getFinalData.equity[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Equity</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+equityBal+'</span></strong></a></li>';
								$('.equityDataUL').html('').html(setHtml);
							}
							else
							{
								$('.equityData').hide();
							}
							var totallibeqValue = parseFloat(equityBal)+parseFloat(libaltiesBal);
							totallibeqValue = setCurrencyData+' '+totallibeqValue;
							$('.totalLibaltiesEquity').text('').text(totallibeqValue);
						}
						// end libalties and equity
						// start expense and other expense
						if(getFinalData.expense.length <= 0 && getFinalData.otherExpense.length <= 0)
						{
							$('.mainexpotherexp').hide();
						}
						else
						{
							$('.mainexpotherexp').show();
							var expenseBal = 0;
							var otherExpenseBal = 0;
							// start expense
							if(getFinalData.expense.length > 0)
							{
								$('.expenseData').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.expense.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.expense[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.expense[index].balance+'</span></a></li>';
									expenseBal = parseFloat(expenseBal)+parseFloat(getFinalData.expense[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Expense</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+expenseBal+'</span></strong></a></li>';
								$('.expenseDataUL').html('').html(setHtml);
							}
							else
							{
								$('.expenseData').hide();
							}
							// other expense
							if(getFinalData.otherExpense.length > 0)
							{
								$('.otherExpenseData').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.otherExpense.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.otherExpense[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.otherExpense[index].balance+'</span></a></li>';
									otherExpenseBal = parseFloat(otherExpenseBal)+parseFloat(getFinalData.otherExpense[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Other Expense</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+otherExpenseBal+'</span></strong></a></li>';
								$('.otherExpenseDataUL').html('').html(setHtml);
							}
							else
							{
								$('.otherExpenseData').hide();
							}
							var totalexpotherexpValue = parseFloat(otherExpenseBal)+parseFloat(expenseBal);
							totalexpotherexpValue = setCurrencyData+' '+totalexpotherexpValue;
							$('.finalExpOthExp').text('').text(totalexpotherexpValue);
						}
						// end expense and other expense
						// undeposioted fund
						if(getFinalData.undeposited.length <= 0)
						{
							$('.mainUndepositedFund').hide();
						}
						else
						{
							$('.mainUndepositedFund').show();
							var undepositedBal = 0;
							// undeposited fund
							if(getFinalData.undeposited.length > 0)
							{
								$('.mainUndepositedFund').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.undeposited.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.undeposited[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.undeposited[index].balance+'</span></a></li>';
									undepositedBal = parseFloat(undepositedBal)+parseFloat(getFinalData.undeposited[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Undeposited Fund</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+undepositedBal+'</span></strong></a></li>';
								$('.undepositedfundUl').html('').html(setHtml);
							}
							else
							{
								$('.mainUndepositedFund').hide();
							}
							var totalundepositedValue = parseFloat(undepositedBal);
							totalundepositedValue = setCurrencyData+' '+totalundepositedValue;
							$('.totalUndepositedFundData').text('').text(totalundepositedValue);
							totalUndepositedFundData
						}
						// end undeposited fund
						// start income and other income
						if(getFinalData.income.length <= 0 && getFinalData.otherincome.length <= 0)
						{
							$('.mainIncomeOtherincome').hide();
						}
						else
						{
							$('.mainIncomeOtherincome').show();
							var incomeBal = 0;
							var otherIncomeBal = 0;
							// start income
							if(getFinalData.income.length > 0)
							{
								$('.incomeData').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.income.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.income[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.income[index].balance+'</span></a></li>';
									incomeBal = parseFloat(incomeBal)+parseFloat(getFinalData.income[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Income</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+incomeBal+'</span></strong></a></li>';
								$('.incomeDataUL').html('').html(setHtml);
							}
							else
							{
								$('.incomeData').hide();
							}
							// start other income data
							if(getFinalData.otherincome.length > 0)
							{
								$('.otheincomeData').show();
								var setHtml = "";
								for(var index = 0; index < getFinalData.otherincome.length; index++)
								{
									setHtml +=  '<li><a><span class="text-gray">'+getFinalData.otherincome[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.otherincome[index].balance+'</span></a></li>';
									otherIncomeBal = parseFloat(otherIncomeBal)+parseFloat(getFinalData.otherincome[index].balance);
								}
								setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Other Income</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+otherIncomeBal+'</span></strong></a></li>';
								$('.otheincomeDataUL').html('').html(setHtml);
							}
							else
							{
								$('.otheincomeData').hide();
							}
							var totalincomeOtherIncomeValue = parseFloat(otherIncomeBal)+parseFloat(incomeBal);
							totalincomeOtherIncomeValue = setCurrencyData+' '+totalincomeOtherIncomeValue;
							$('.finalIncomeOtherIncome').text('').text(totalincomeOtherIncomeValue);
						}
						// end income and other income
						// start other expense
						if(getFinalData.other.length <= 0)
						{
							$('.mainOtherData').hide();
						}
						else
						{
							$('.mainOtherData').show();
							var otherBal = 0;
							var setHtml = "";
							for(var index = 0; index < getFinalData.other.length; index++)
							{
								setHtml +=  '<li><a><span class="text-gray">'+getFinalData.other[index].name+'</span><span class="pull-right text-gray undepositFundsData">'+setCurrencyData+''+getFinalData.other[index].balance+'</span></a></li>';
								otherBal = parseFloat(otherBal)+parseFloat(getFinalData.other[index].balance);
							}
							setHtml += '<li class="border-b"><a class="text-gray"><strong><span>Total Other Income</span><span class="pull-right totalCurrentAssetsBal">'+setCurrencyData+''+otherBal+'</span></strong></a></li>';
							$('.OtherDataUl').html('').html(setHtml);
							var totalOtherDataValue = parseFloat(otherBal);
							totalOtherDataValue = setCurrencyData+' '+totalOtherDataValue;
							$('.finalSetOtherData').text('').text(totalOtherDataValue);
						}
						// end other expense
					}
					else if(getFinalData.message == "validation")
					{
						$('.setErrorMessage').html('').html('<div class="alert alert-danger">'+getFinalData.data+'</div>');
					}
					else
					{
						$('.setErrorMessage').html('').html('<div class="alert alert-danger">Something went wrong. Please try again</div>');
					}
                }
                });
			})

		})
		</script>
<script type="text/javascript">
	$('.tree .icon').click( function() {
  $(this).parent().toggleClass('expanded').
  closest('li').find('ul:first').
  toggleClass('show-effect');
});
</script>