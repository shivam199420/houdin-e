<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<style type="text/css">
    .py-85 {
    padding-top: 50px !important;
    padding-bottom: 0px !important;
}
.order-detail-section {
    padding: 20px 10px;
    box-shadow: 0px 0px 4px 0px #cecece;
    margin-bottom: 20px;
}
.panel-default {
    border-color: #ddd;
}
.panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.panel-default>.panel-heading {
    color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;
}
.panel-heading {
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
}
.panel-body {
    padding: 15px;
}
.panel-title {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 16px;
    color: inherit;
}
.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}
.table > tbody > tr > .no-line {
    border-top: none;
}
hr {
    margin: 0px 0;
    padding: 0px;
    border-bottom: 1px solid #eceff8;
    border-top: 0px;
}
</style>


<section>
        <div class="container py-85 py-tn-50">
           <div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="section-title text-center mb-50">
                            <h2 class="title">Order Detail</h2>
                            <div class="title-border mx-auto m-b-55 m-b-tn-35"></div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="port-body">
            <div class="container">
            <div class="order-detail-section">
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-6">
                    <address>
                    <strong>Order Id:</strong>#<?php echo $orders->houdinv_order_id ;?><br>
                    <strong>Order Status:</strong><?php echo $orders->houdinv_order_confirmation_status ;?><br>
                    <strong>Order Date:</strong><?php echo date("Y-m-d ,h:i:s",$orders->houdinv_order_created_at) ;?><br><br>


                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>

                        <span><form id="cancel_form" method="post" action="https://tech.hawkscode.in/houdin-e/Shop/Orders" style="display: none;">
                          <?php
                      $array_return = array("Delivered");
                      if( in_array($orders->houdinv_order_confirmation_status,$array_return))
                      {
                       ?>
                         <form id="return_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
                            <input type="text" name="returnOrderId" value="<?php echo $this->uri->segment("3"); ?>"/>
                          <input type="text" value="returnOrder" name="returnOrder"/>
                           </form>
                      <button class="btn btn-default return_button">Return Order</button>
                      <?php
                        }

                      $array_cancel = array("unbilled","Not Delivered","billed","assigned");
                      if( in_array($orders->houdinv_order_confirmation_status,$array_cancel))
                      {
                       ?>

                           <form id="cancel_form" method="post" action="<?php echo base_url(); ?>Orders" style="display: none;">
                            <input type="text" name="cancelOrderId" value="<?php echo $this->uri->segment("3"); ?>"/>
                          <input type="text" value="cancelOrder" name="cancelOrder"/>
                           </form>
                      <div class="single-pro-cart">
  <a href="javascript:;" class="Add_to_cart_button cancel_button" data-variant="0" data-cart="36" title="">Cancel</a>
                      </div>
                      <?php
                      } ?>


                    </span>
                    </address>
                </div>
            </div>


        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Confirmed Products</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <hr><table class="table table-condensed">
                            <thead>
                                <tr>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th>Tax</th>
                                <th>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total_product_gross = 0;
                            foreach($orders->product_all as $detail)
                            { ?>
                            <tr>
                            <td> <?php echo $detail->title ; ?></td>
                            <td><?php echo $detail->allDetail->product_Count ; ?></td>
                            <td><?php echo $detail->allDetail->product_actual_price; ?></td>
                            <td>0</td>
                            <td><?php echo $detail->allDetail->total_product_paid_Amount; ?></td>
                           </tr>
                            <?php
                            $total_product_gross = $total_product_gross + $detail->allDetail->total_product_paid_Amount;
                            } ?>
                            </tbody>
                            <tbody>


                                <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-right"><strong>Gross Amount</strong></td>
                                    <td class="thick-line text-right"> <?php echo $total_product_gross;?></td>
                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-right"><strong>Additional Discount</strong></td>
                                    <td class="no-line text-right"><?php echo  $orders->houdinv_orders_discount; ?></td>
                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-right"><strong>Delivery Charges</strong></td>
                                    <td class="no-line text-right"><?php echo $orders->houdinv_delivery_charge; ?></td>
                                </tr>

                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-right"><strong>Round Off</strong></td>
                                    <td class="no-line text-right"><?php echo $orders->houdinv_orders_total_Amount; ?></td>
                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-right"><strong>Net Payable</strong></td>
                                    <td class="no-line text-right"><?php echo $orders->houdinv_orders_total_Amount; ?></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
            </div>
        </div>
    </section>

<?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
?>
