<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Workorder extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->auth_users->is_logged_in();
           $this->load->model('Customermodel');
            $this->load->model('Workmodel');
            $this->load->model('posmodel');
            $this->validation = array(       
                  array("field"=>"customer_name",
                        "label"=>"customer name",
                        "rules"=>"required"),
                  
                  array("field"=>"customer_phone",
                        "label"=>"customer phone",
                        "rules"=>"required"),
                        
                  array("field"=>"customer_quantity",
                        "label"=>"quantity",
                        "rules"=>"required|integer"),
                        
                  array("field"=>"Customer_pickup_type",
                        "label"=>"pickup type",
                        "rules"=>"required"), 
                        
                  array("field"=>"customer_address",
                        "label"=>"address",
                        "rules"=>"required"),
                        
                  array("field"=>"customer_payment_type",
                        "label"=>"payment type",
                        "rules"=>"required"),
                        
                  array("field"=>"customer_total_amount_paid",
                        "label"=>"total amount",
                        "rules"=>"required|numeric"), 
                        
                  array("field"=>"Customer_Net_payble_amount",
                        "label"=>"Net payble amount",
                        "rules"=>"required|numeric"), 
                        
                  array("field"=>"customer_discount",
                        "label"=>"discount",
                        "rules"=>"numeric"), 
                        
                  array("field"=>"order_delivery_date",
                        "label"=>"order delivery date",
                        "rules"=>"required"), 
                        
                  array("field"=>"order_delivery_time",
                        "label"=>"order delivery time",
                        "rules"=>"required")                                                   
                  );
    }
    public function index()
    {
            $gh =$this->input->post('savedata');
            if($gh)
            {
                  $this->form_validation->set_rules($this->validation); 
                  if($this->form_validation->run() != true)
                  {
                        $this->session->set_flashdata('message_name',validation_errors() );
                        redirect(base_url().'Workorder', 'refresh');      
                  }
                  $date = strtotime(date("Y-m-d"));
                  if($this->input->post('customer_discount'))
                  {
                        $setOrderDiscount = $this->input->post('customer_discount');
                  }
                  else
                  {
                        $setOrderDiscount = 0;
                  }
                  if($this->input->post('paymentStatus') == 1)
                  {
                        $setRemaingAmount = 0;
                        $setAmountPaid = $this->input->post('Customer_Net_payble_amount');
                  }
                  else
                  {
                        $setRemaingAmount = $this->input->post('Customer_Net_payble_amount');
                        $setAmountPaid = 0;
                  }
                  // get user id data
                  if($this->session->userdata('vendorRole') == 0)
                  {
                        $masterDB = $this->load->database('master', TRUE);
                        // get user id
                        $getLoginUserId = $masterDB->select('houdin_vendor_auth_vendor_id')->from('houdin_vendor_auth')->where('houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))->get()->result();
                        $setUserId = $getLoginUserId[0]->houdin_vendor_auth_vendor_id;
                        $setUserRole = $this->session->userdata('vendorRole');
                  }
                  else
                  {
                        $getUserId = $this->db->select('staff_id')->from('houdinv_staff_management')->where('houdinv_staff_auth_token',$this->session->userdata('vendorAuth'))->get()->result();
                        $setUserId = $getUserId[0]->staff_id;
                        $setUserRole = $this->session->userdata('vendorRole');
                  }
                  // fetch delivery charges
                  if($this->input->post('Customer_pickup_type') == 'deliver')
                  {
                        $fetchPosSetting = fetchPosSetting();
                        if($fetchPosSetting[0]->free_delivery == 1)
                        {
                              if($fetchPosSetting[0]->free_delivery_val >= $this->input->post('Customer_Net_payble_amount'))
                              {
                                    $setDeliveryCharge = 0;            
                              }
                              else
                              {
                                    $setDeliveryCharge = $fetchPosSetting[0]->delivery_charges;
                              }
                        }
                        else
                        {
                              $setDeliveryCharge = $fetchPosSetting[0]->delivery_charges;
                        }
                        $setConfirmationStatus = 'billed';
                        $setDeliveryStatus = 0;
                  }
                  else
                  {
                        $setConfirmationStatus = 'Delivered';
                        $setDeliveryCharge = 0;
                        $setDeliveryStatus = 1;
                  }
                  if($this->input->post('customer_payment_type') == 'card')
                  {
                        $setTransactionType = 'online';
                  }
                  else
                  {
                        $setTransactionType = 'cash';
                  }
                  $setdata = strtotime(date('Y-m-d'));
                  $array = array("houdinv_workorder_customer_name"=>$this->input->post('registeredCustomerId'),
                        "houdinv_workorder_customer_phone"=>$this->input->post('customer_phone'),
                        "houdinv_workorder_product"=>$this->input->post('productIdData'),
                        "houdinv_workorder_variant"=>$this->input->post('variantIdData'),
                        "houdinv_workorder_quantity"=>$this->input->post('customer_quantity'),
                        "houdinv_workorder_coupon_code"=>$this->input->post('customer_coupon'),
                        "houdinv_workorder_coupon_discount"=>$this->input->post('customer_discount'),
                        "houdinv_workorder_pickup"=>$this->input->post('Customer_pickup_type'),
                        "houdinv_workorder_address"=>$this->input->post('customer_address'),
                        "houdinv_workorder_note"=>$this->input->post('customer_note'),
                        "houdinv_workorder_payment"=>$this->input->post('customer_payment_type'),
                        "houdinv_workorder_work_detail"=>$this->input->post('customer_work_completion_detail'),
                        "houdinv_workorder_paid_amount"=>$this->input->post('customer_total_amount_paid'),
                        "houdinv_workorder_net_pay"=>$this->input->post('Customer_Net_payble_amount'),
                        "date"=>$date,
                        "update_date"=>$date,
                        "houdinv_workorder_deliver_date"=>$this->input->post('order_delivery_date'),
                        "houdinv_workorder_payment_status"=>$this->input->post('paymentStatus'),
                        "houdinv_workorder_deliver_time"=>$this->input->post('order_delivery_time')); 
                        // create order array
                        // set prouduct array
                        $getActualPrice = $this->input->post('customer_total_amount_paid')/$this->input->post('customer_quantity');
                        $setProductArrayData[] = array("product_id"=>$this->input->post('productIdData'),'variant_id'=>$this->input->post('variantIdData'),
                        'product_Count'=>$this->input->post('customer_quantity'),'product_actual_price'=>$getActualPrice,'total_product_paid_Amount'=>$this->input->post('Customer_Net_payble_amount'));
                        
                        $orderArrayData = array('houdinv_order_user_id'=>$this->input->post('customer_name'),
                        'houdinv_order_product_detail'=>json_encode($setProductArrayData),
                        'houdinv_confirm_order_product_detail'=>json_encode($setProductArrayData),
                        'houdinv_order_confirm_outlet'=>$this->input->post('outletData'),
                        'houdinv_orders_confirm_by'=>$setUserId,
                        'houdinv_orders_confirm_by_role'=>$setUserRole,
                        'houdinv_order_type'=>'Work Order',
                        'houdinv_order_delivery_type'=>$this->input->post('Customer_pickup_type'),
                        'houdinv_order_payment_method'=>$this->input->post('customer_payment_type'),
                        'houdinv_payment_status'=>$this->input->post('paymentStatus'),
                        'houdinv_orders_discount'=>$setOrderDiscount,
                        'houdinv_orders_discount_detail_id'=>$this->input->post('couponIdData'),
                        'houdinv_orders_total_Amount'=>$this->input->post('Customer_Net_payble_amount'),
                        'houdinv_orders_total_paid'=>$setAmountPaid,
                        'houdinv_orders_total_remaining'=>$setRemaingAmount,
                        'houdinv_order_comments'=>$this->input->post('customer_note'),
                        'houdinv_delivery_charge'=>$setDeliveryCharge,
                        'houdinv_order_confirmation_status'=>$setConfirmationStatus,
                        'houdinv_orders_delivery_status'=>$setDeliveryStatus,
                        'houdinv_order_delivery_address_id'=>$this->input->post('customer_address'),
                        'houdinv_order_billing_address_id'=>$this->input->post('customer_address'),
                        'houdinv_order_created_at'=>$setdata);
                        if($this->input->post('paymentStatus') == 1)
                        {
                              $setTransactionId = 'TXN'.rand(99999999,10000000);
                              $setTransactionArray = array('houdinv_transaction_transaction_id'=>$setTransactionId,
                              'houdinv_transaction_type'=>'credit',
                              'houdinv_transaction_method'=>$setTransactionType,
                              'houdinv_transaction_from'=>'cash',
                              'houdinv_transaction_for'=>'Work Order',
                              'houdinv_transaction_amount'=>$this->input->post('Customer_Net_payble_amount'),
                              'houdinv_transaction_date'=>date('Y-m-d'),
                              'houdinv_transaction_status'=>'success'
                              );
                        }
                        else
                        {
                              $setTransactionArray = array();
                        }
                  $order =  $this->Workmodel->AddWorkOrder($array,$orderArrayData,$setTransactionArray); 
                  if($order['message'] == 'yes')
                  {
                        $this->session->set_flashdata('success',"WorkOrder Added Successfully");
                        redirect(base_url().'Calendar', 'refresh');      
                  } 
                  else if($order['message'] == 'trans')
                  {
                        $this->session->set_flashdata('message_name',"WorkOrder Added Successfully. Something went wrong during transaction add");
                        redirect(base_url().'Workorder', 'refresh');     
                  }          
                  else if($order['message'] == 'order')
                  {
                        $this->session->set_flashdata('message_name',"WorkOrder Added Successfully. Something went wrong during order add");
                        redirect(base_url().'Workorder', 'refresh');     
                  }  
                  else if($order['message'] == 'ordertrans')
                  {
                        $this->session->set_flashdata('message_name',"WorkOrder Added Successfully. Something went wrong during order and transaction add");
                        redirect(base_url().'Workorder', 'refresh');     
                  } 
                  else
                  {
                        $this->session->set_flashdata('message_name',"Something went wrong. Please try again");
                        redirect(base_url().'Workorder', 'refresh');     
                  }  
                         
            }
            $data['getCustomerData'] = $this->Customermodel->fetchCustomerRecord();
            $data['getProductData'] = $this->Workmodel->fetchPosData();
            $data['getOutlet'] = $this->Workmodel->fetchOutletData();
            $this->load->view('workorder',$data);  
    }

public function saveworkorder(){
      $setPendingAmountArray = array();
      $getUserType = $this->input->post('customerType');
      $getDeliveryType = $this->input->post('deliveryType');
       
      $this->form_validation->set_rules('paymentStatus','payment status','required|in_list[1,0]');
      if($this->form_validation->run() == true)
      {
          // setMain product array
          $setData = strtotime(date('Y-m-d'));
          $setProductArray = array();
          $setOtherUserData = array();
          $setTransactionArray = array();
          $getProductId = $this->input->post('inputProdcutId');
          $getVariantId = $this->input->post('inputVariantId');
          $getQuantity = $this->input->post('inputQuantity');
          $getActualPrice = $this->input->post('inputActualPrice');
          $getDiscountData = $this->input->post('inputDiscountedPrice');
          $getTotalPrice = $this->input->post('inputTotalPrice');   
          $getSpecialPrice = $this->input->post('specialPrice');
          $gettax = $this->input->post('inputtotaltax');
          // 'saleprice'=>$data['saleprice'][$index],'totaltax'=>$data['totalTax'][$index]
          for($index = 0; $index < count($getQuantity); $index++)
          {
              $this->posmodel->productStockmanage( array("product_id"=>$getProductId[$index],'variant_id'=>$getVariantId[$index],'product_Count'=>$getQuantity[$index]));
         
              $setProductArray[] = array("product_id"=>$getProductId[$index],'variant_id'=>$getVariantId[$index],'product_Count'=>$getQuantity[$index],
              'product_actual_price'=>$getActualPrice[$index],'total_product_paid_Amount'=>$getTotalPrice[$index],
              'discount'=>$getDiscountData[$index],'saleprice'=>$getActualPrice[$index],'totaltax'=>$gettax[$index]);
         
           
          } 
          if($getUserType == 'RC')
          {
              $setUserId = $this->input->post('registeredCustomerId');
          }
          else
          {
              $setOtherUserData = array('name'=>$this->input->post('setNonRegisteredCustomerName'),'contact'=>$this->input->post('setNonRegisteredCustomerContact'),'address'=>$this->input->post('setNonRegisteredCustomerAddress'));
              $setUserId = 0;
          }
          $fetchPosSetting = fetchPosSetting();
          if($getDeliveryType == 'HD')
          {
              $setDeliveryType = 'deliver';
              $setOrderStatus = 'billed';
              $setDeliveryStatus = 0;
              if($fetchPosSetting[0]->delivery_charges)
              {
                  if($fetchPosSetting[0]->free_delivery == 1)
                  {
                      $getFreeDelivery = $fetchPosSetting[0]->free_delivery_val;
                      $getNetPayable = $this->input->post('finalAmountData')-$this->input->post('finalDiscountData');
                      if($getNetPayable < $getFreeDelivery)
                      {
                          $setDeliveryCharges = $fetchPosSetting[0]->delivery_charges;
                      }
                      else
                      {
                          $setDeliveryCharges = 0;
                      }
                  }
                  else
                  {
                      $setDeliveryCharges = $fetchPosSetting[0]->delivery_charges;
                  }
              }
              else
              {
                  $setDeliveryCharges = 0;
              }
          }
          else
          {
              $setDeliveryType = 'pick_up';
              $setOrderStatus = 'Delivered';
              $setDeliveryStatus = 1;
              $setDeliveryCharges = 0;
          }
          if($this->input->post('couponCodeId'))
          {
              $setCouponCode = $this->input->post('couponCodeId');
          }
          else
          {
              $setCouponCode = 0;
          }
          if($this->input->post('setRegisteredCustomerAddress'))
          {
              $setAddressId = $this->input->post('setRegisteredCustomerAddress');
          }
          else
          {
              $setAddressId = 0;
          }
          // additional adjustment
          if($this->input->post('paymentStatus') == 1)
          {
              if($this->input->post('pendingAmountData')>0)
              {
                  $getPendingAmount = $this->input->post('pendingAmountData');
                  $totalPaidAmount = $this->input->post('netPayableAmount');
                  $setPendingAmountArray = array('pendingAmount'=>$getPendingAmount,'userId'=>$this->input->post('registeredCustomerId'));
              }
              else
              {
                  $getPendingAmount = 0;
                  $totalPaidAmount = $this->input->post('netPayableAmount');
              }
              if($this->input->post('paymentOption') == 'cash' || $this->input->post('paymentOption') == 'cheque')
              {
                  $setTransactionMethod = 'cash';
              }
              else
              {
                  $setTransactionMethod = 'online';
              }
              $setTransactionArray = array('houdinv_transaction_type'=>'credit','houdinv_transaction_method'=>$setTransactionMethod,
              'houdinv_transaction_from'=>'pos','houdinv_transaction_for'=>'order','houdinv_transaction_amount'=>$this->input->post('netPayableAmount'));
          }
          else
          {
              $getPendingAmount = $this->input->post('netPayableAmount');
              $totalPaidAmount = 0;
          }
          $getUserId = fetchoutletDetailsData();
          $setOrderInsertArray = array('houdinv_order_user_id'=>$setUserId,'houdinv_order_product_detail'=>json_encode($setProductArray),
          'houdinv_confirm_order_product_detail'=>json_encode($setProductArray),
          'houdinv_order_confirm_outlet'=>$this->session->userdata('vendorOutlet'),
          'houdinv_orders_confirm_by'=>$getUserId['userId'],
          'houdinv_orders_confirm_by_role'=>$this->session->userdata('vendorRole'),
          'houdinv_order_type'=>'Store',
          'houdinv_order_delivery_type'=>$setDeliveryType,
          'houdinv_order_payment_method'=>$this->input->post('paymentOption'),
          'houdinv_payment_status'=>$this->input->post('paymentStatus'),
          'houdinv_orders_discount'=>$this->input->post('finalDiscountData'),
          'houdinv_orders_discount_detail_id'=>$setCouponCode,
          'houdinv_orders_total_Amount'=>$this->input->post('finalAmountData'),
          'houdinv_order_comments'=>$this->input->post('customerNote'),
          'houdinv_orders_deliverydate'=>date('Y-m-d'),
          'houdinv_order_confirmation_status'=>$setOrderStatus,'houdinv_orders_delivery_status'=>$setDeliveryStatus,
          'houdinv_orders_deliverydate'=>date('Y-m-d'),
          'houdinv_order_delivery_address_id'=>$setAddressId,
          'houdinv_order_billing_address_id'=>$setAddressId,
          'houdinv_order_created_at'=>$setData,'houdinv_delivery_charge'=>$setDeliveryCharges,'houdinv_orders_total_paid'=>$totalPaidAmount,
          'houdinv_orders_total_remaining'=>$getPendingAmount);


          $setdata = strtotime(date('Y-m-d'));
          $workorderdata = array("houdinv_workorder_customer_name"=>$this->input->post('registeredCustomerId'),
                        "houdinv_workorder_customer_phone"=>$this->input->post('customer_phone'),
                      /*  "houdinv_workorder_product"=>$this->input->post('productIdData'),
                        "houdinv_workorder_variant"=>$this->input->post('variantIdData'),
                        "houdinv_workorder_quantity"=>$this->input->post('customer_quantity'),
                        "houdinv_workorder_coupon_code"=>$this->input->post('customer_coupon'),
                        "houdinv_workorder_coupon_discount"=>$this->input->post('customer_discount'),*/
                        "houdinv_workorder_pickup"=>$this->input->post('Customer_pickup_type'),
                        "houdinv_workorder_address"=>$this->input->post('customer_address'),
                        "houdinv_workorder_note"=>$this->input->post('customerNote'),
                        "houdinv_workorder_payment"=>$this->input->post('paymentOption'),
                        "houdinv_workorder_work_detail"=>$this->input->post('customer_work_completion_detail'),
                        "houdinv_workorder_paid_amount"=>$this->input->post('netPayableAmount'),
                        "houdinv_workorder_net_pay"=>$this->input->post('netPayableAmount'),
                        "date"=>date('Y-m-d'),
                        "update_date"=>date('Y-m-d'),
                        "houdinv_workorder_deliver_date"=>$this->input->post('order_delivery_date'),
                        "houdinv_workorder_payment_status"=>$this->input->post('paymentStatus'),
                        "houdinv_workorder_deliver_time"=>$this->input->post('order_delivery_time')); 
          $order = $this->Workmodel->setOrderData($workorderdata,$setOrderInsertArray,$setOtherUserData,$setPendingAmountArray,$setTransactionArray);
           
         // $order =  $this->Workmodel->AddWorkOrder($array,$orderArrayData,$setTransactionArray); 
          if($order['message'] == 'yes')
          {
                $this->session->set_flashdata('success',"WorkOrder Added Successfully");
                redirect(base_url().'Calendar', 'refresh');      
          } 
          else if($order['message'] == 'trans')
          {
                $this->session->set_flashdata('message_name',"WorkOrder Added Successfully. Something went wrong during transaction add");
                redirect(base_url().'Workorder', 'refresh');     
          }          
          else if($order['message'] == 'order')
          {
                $this->session->set_flashdata('message_name',"WorkOrder Added Successfully. Something went wrong during order add");
                redirect(base_url().'Workorder', 'refresh');     
          }  
          else if($order['message'] == 'ordertrans')
          {
                $this->session->set_flashdata('message_name',"WorkOrder Added Successfully. Something went wrong during order and transaction add");
                redirect(base_url().'Workorder', 'refresh');     
          } 
          else
          {
                $this->session->set_flashdata('message_name',"Something went wrong. Please try again");
                redirect(base_url().'Workorder', 'refresh');     
          }  


} else
{
    $this->session->set_flashdata('message_name',validation_errors());
    $this->load->helper('url');
    redirect(base_url().'Workorder', 'refresh');     
}

}


    public function checkCoupon()
    {
      $val =$this->input->post('code');
      if($val)
      {
            $data =  $this->Workmodel->matchCoupon($val);
            if($data)
            {
                  $array = array("msg"=>"yes","percent"=>$data->houdinv_coupons_discount_precentage,'id'=>$data->houdinv_coupons_id);
            }
            else
            {
                  $array = array("msg"=>"no");
            }
      }
      else
      {
            $array = array("msg"=>"no");
      }
      echo json_encode($array);
    }
}
