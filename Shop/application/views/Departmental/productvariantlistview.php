<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header");
if(count($variantData) <= 0)
{
    redirect(base_url()."home/".$this->session->userdata('shopName'), 'refresh');
}
$price_all = json_decode($productData['0']->houdin_products_price,true);
if($price_all['discount'] != 0 && $price_all['discount'] != "")
{
    $setDiscountedPrice = $variantData[0]->houdin_products_variants_prices-$price_all['discount'];
    $setOriginalPrice = $variantData[0]->houdin_products_variants_prices;
}
else
{
    $setDiscountedPrice = 0;
    $setOriginalPrice = $variantData[0]->houdin_products_variants_prices;
}
$overall_rating = $reviews_sum->houdinv_product_review_rating;
$total_rating = count($reviews);
$final_rating = $overall_rating/$total_rating;
    ?>
    <?php
    $getCurrency = getShopCurrency();
    $currencysymbol=$getCurrency[0]->houdin_users_currency;
    ?>
    <style>
    .checked {
    color: orange;
}

</style>
       <!-- Page Title -->
       <section>
        <div class="container py-85 py-tn-50">
            <div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-12 ">
                            <h2 class="title text-center">Product List</h2>
                             <div class="title-border mx-auto m-b-tn-35 text-center"></div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </section>

    <!-- End Page Title -->

    <!-- Product Detail -->
    <section>
    <div class="row">
        <div class="container">
        <div class="col-sm-12">
        <?php
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>
        </div>
        <div class="container p-b-90 p-t-100">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-detail">
                        <div class="shop-product p-t-25">
                        <div class="slide100-01" id="slide100-01">
                                <div class="main-wrap-pic">
                                    <div class="main-frame" style="position:unset !important">
                                        <div class="wrap-main-pic">
                                            <div class="main-pic">
                                                <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $variantData[0]->houdin_products_variants_image;?>" alt="image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="wrap-thumb-100 d-flex justify-content-between p-t-10">
                                    <div class="wrap-arrow-slide-100 s-full ab-t-l trans-04">
                                        <span class="my-arrow back prev-slide-100">
                                            <img src="<?php echo base_url() ?>Extra/departmental/images/icon/pro-left-black.png" alt="Prev">
                                        </span>
                                        <span class="my-arrow next next-slide-100">
                                            <img src="<?php echo base_url() ?>Extra/departmental/images/icon/pro-right-black.png" alt="Next">
                                        </span>
                                    </div>
                                    <?php
                                    // for($index = 0; $index < count($images_all); $index++)
                                    // {
                                    //     if($images_all[$index])
                                    //     {
                                    //         $setNumberData = $index+1;
                                            ?>
                                             <div class="thum-100">
                                                <div class="sub-frame sub-<?php //echo $setNumberData ?>">
                                                    <div class="wrap-main-pic">
                                                        <div class="main-pic">
                                                            <img src="<?php //echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php //echo $images_all[$index]; ?>" alt="image">
                                                        </div>
                                                    </div>
                                                    <div class="btn-sub-frame"></div>
                                                </div>
                                            </div>
                                            <?php //} }
                                            ?>
                                </div> -->
                            </div>
                            <div class="product-body">
                                <h3 class="name"><?php echo $variantData[0]->houdin_products_variants_title; ?></h3>
                                <span class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo number_format((float)$variantData[0]->houdinv_products_variants_final_price, 1, '.', ''); ?></span>
                                <span class="price"><strike>(<?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo number_format((float)$price_all['sale_price'], 1, '.', '');  ?>)</strike></span>
                                <!-- <p class="price">$35.00</p> -->
                                <p class="description"><?php echo $productData[0]->houdin_products_short_desc; ?></p>
                                <div class="product-button">
                                    <div class="quantity">

                                        <input value="1" min="1" data-max="<?php echo $variantData[0]->houdinv_products_variants_total_stocks ?>" name="qttybutton" class="cart-plus-minus-box" type="number">

                                    </div>
                                    <a href="javascript:;" data-variant="<?php echo $variantData[0]->houdin_products_variants_id ?>"  data-cart="0" class="add-to-cart acc_btn Add_to_cart_button">Add to cart</a>
                                    <a href="javascript:;" data-variant="<?php echo $variantData[0]->houdin_products_variants_id ?>"  data-cart="0" class="add-to-wishlist acc_btn btn_icn Add_to_whishlist_button"></a>
                                </div>
                                <div class="product-available">
                                    <span>Available :</span>
                                    <?php
                                    if($variantData[0]->houdinv_products_variants_total_stocks > 0)
                                    {
                                        echo '<a href="javascript:;">In stock</a>';
                                    }
                                    else
                                    {
                                        echo '<a href="javascript:;" class="text-danger">Out of stock</a>';
                                    }
                                    ?>
                                </div>
                                <?php
                                if($variantData[0]->houdin_products_variants_sku)
                                {
                                ?>
                                <div class="product-sku">
                                    <span class="text-black">SKU: </span>
                                    <?php echo $variantData[0]->houdin_products_variants_sku; ?>
                                </div>
                                <?php }
                                ?>

                                <div class="product-rating" style="top:0px !important;" data-star="<?php if($final_rating) { echo $final_rating; } else { echo 0; } ?>"> <?php if($total_rating){ echo "(".$total_rating.")"; }?></div>
                            </div>
                        </div>
                        <div class="au-tabs">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#description-tab" class="active show">Description</a>
                                </li>

                                <li>
                                    <a data-toggle="tab" href="#review-tab">review (<?php echo $total_rating; ?>)</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="description-tab" class="tab-pane active">
                                <?php echo $productData[0]->houdin_products_desc; ?>
                                </div>

                                <div id="review-tab" class="tab-pane">
                                    <h5 class="title">REVIEWS</h5>

                                    <?php
                                    if(!$final_rating > 0)
                                    {
                                        ?>
                                        <div class="col-sm-6 pull-left">
                                         <p>There are no reviews yet.</p>
                                    <p class="text-bigger">Be the first to review</p>
                                    </div>
                                    <?php }
                                    else
                                    {
                                    ?>
                                     <div class="review-section-single col-sm-6 pull-left" style="overflow-y:scroll;height:330px">
                                     <?php
                                     foreach($reviews as $user_Review)
                                     {
                                         ?>
                                       <div class="review-tab m-b-10">
                                            <div class="review-name-title font600"><?php echo $user_Review->houdinv_product_review_user_name ?></div>
                                            <div class="star">
                                                <div class="form-group m-b-0">
                                                    <div class="strat-rating">
                                                    <?php for($i=1 ; $i<6 ;$i++)
                                                    {
                                                      if($i <= $user_Review->houdinv_product_review_rating)
                                                      {
                                                      ?>
                                                    <span data-rating="1" style="font-size: 16px;" class="fa fa-star checked"></span>
                                                        <?php }
                                                        else
                                                        {
                                                        ?>
                                                        <span data-rating="1" style="font-size: 16px;" class="fa fa-star"></span>
                                                        <?php } } ?>

                                                    <span class="text-date"><?php echo $user_Review->houdinv_product_review_created_at ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-description"><?php echo  $user_Review->houdinv_product_review_message; ?></div>
                                        </div>
                                     <?php } ?>



                                    </div>
                                    <?php }
                                    ?>
                                    <div class="comment-rating col-sm-6">
                                        <div class="">
                                        <?php echo form_open(base_url( 'Productlistview/variant/'.$this->uri->segment('3').'' ), array( 'id' => 'rtng_form',"class"=>"checkout_form", 'method'=>'post' ));?>
                                        <div class="form-group">

                                        <div class="strat-rating">
                                            <span data-rating="1" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="2" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="3" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="4" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="5" style="font-size: 22px;" class="fa fa-star"></span>
                                            <input type="hidden"  name="rating" class="rating_data" value="0" />
                                            <input type="hidden"  name="product_id" class="product" value="0" />
                                            <input type="hidden"  name="variant_id" class="" value="<?php echo $this->uri->segment('3'); ?>" />
                                        </div>
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation form-control" type="text" name="user_name" placeholder="Your Name" />
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation email_validation form-control" type="text" name="user_email" placeholder="Your Email" />
                                        </div>
                                        <div class="form-group">

                                        <textarea  class="required_validation_for_review form-control" name="user_message" rows="3" placeholder="Write a review"></textarea>
                                        </div>
                                        <input class="btn border-btn submit_form_review" type="submit" name="review_submit" value="Add Review" />
                                        <?php echo form_close(); ?>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Product Detail -->
    <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer"); ?>
    <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('keyup','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
            if(parseInt($(this).val()) <= 0)
            {
                $(this).val('1');
            }
        })
        $(document).on('change','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
            if(parseInt($(this).val()) <= 0)
            {
                $(this).val('1');
            }
        })
    })
    </script>
