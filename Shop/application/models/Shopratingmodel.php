<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopratingmodel extends CI_Model{
    function __construct()
    {
    }
    public function fetchUserinfo($userid,$shop)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shop);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getUserInfo = $getDynamicDB->select('houdinv_user_id,houdinv_user_name,houdinv_user_email')->from('houdinv_users')->where('houdinv_user_id',$userid)->get()->result();
        return $getUserInfo;
    }
    public function addProductRating($data,$shop)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shop);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getReview = $getDynamicDB->insert('houdinv_product_reviews',$data);
        if($getReview)
        {
            return array('message'=>'Review addedd successfully');
        }
        else
        {
            return array('message'=>'Something went wrong. Please try again');
        }
    }
    public function fetchProductRating($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        if($data['productId'] != 0 && $data['productId'] != "")
        {
            // get overall rating
            $getOverallRating = $getDynamicDB->select('SUM(houdinv_product_review_rating) AS totalRating,Count(houdinv_product_review_id) as totalReview ')->from('houdinv_product_reviews')->where('houdinv_product_review_product_id',$data['productId'])->get()->result();
            if($getOverallRating[0]->totalReview)
            {
                $setTotalVotes = $getOverallRating[0]->totalReview;
                $finalRating = $getOverallRating[0]->totalRating/$setTotalVotes;
            }
            else
            {
                $setTotalVotes = 0;
                $finalRating = 0;
            }
            // end here
            $getProductReviewData = $getDynamicDB->select('*')->from('houdinv_product_reviews')->where('houdinv_product_review_product_id',$data['productId'])
            ->order_by('houdinv_product_review_id','DESC')->limit('2')->get()->result();
            if(count($getProductReviewData) > 0)
            {
                $setReviewArray = array();
                foreach($getProductReviewData as $getProductReviewDataList)
                {   
                    $getDate = date('d-m-Y',strtotime($getProductReviewDataList->houdinv_product_review_created_at));
                    $setReviewArray[] = array('name'=>$getProductReviewDataList->houdinv_product_review_user_name,'rating'=>$getProductReviewDataList->houdinv_product_review_rating,'message'=>$getProductReviewDataList->houdinv_product_review_message,'date'=>$getDate);
                }   
                return array('message'=>'Yes','data'=>$setReviewArray,'totalVotes'=>$setTotalVotes,'overallrating'=>number_format((float)$finalRating, 1, '.', ''));
            }
            else
            {
                return array('message'=>'No data');
            }
            
        }
        else
        {
            // get overall rating
            $getOverallRating = $getDynamicDB->select('SUM(houdinv_product_review_rating) AS totalRating,Count(houdinv_product_review_id) AS totalReview ')->from('houdinv_product_reviews')->where('houdinv_product_reviews_variant_id',$data['variantId'])->get()->result();
            if($getOverallRating[0]->totalReview)
            {
                $setTotalVotes = $getOverallRating[0]->totalReview;
                $finalRating = $getOverallRating[0]->totalRating/$setTotalVotes;
            }
            else
            {
                $setTotalVotes = 0;
                $finalRating = 0;
            }
            // end here
            $getProductReviewData = $getDynamicDB->select('*')->from('houdinv_product_reviews')->where('houdinv_product_reviews_variant_id',$data['variantId'])
            ->order_by('houdinv_product_review_id','DESC')->limit('2')->get()->result();
            if(count($getProductReviewData) > 0)
            {
                $setReviewArray = array();
                foreach($getProductReviewData as $getProductReviewDataList)
                {   
                    $getDate = date('d-m-Y',strtotime($getProductReviewDataList->houdinv_product_review_created_at));
                    $setReviewArray[] = array('name'=>$getProductReviewDataList->houdinv_product_review_user_name,'rating'=>$getProductReviewDataList->houdinv_product_review_rating,'message'=>$getProductReviewDataList->houdinv_product_review_message,'date'=>$getDate);
                }   
                return array('message'=>'Yes','data'=>$setReviewArray,'totalVotes'=>$setTotalVotes,'overallrating'=>number_format((float)$finalRating, 1, '.', ''));
            }
            else
            {
                return array('message'=>'No data');
            }
        }
    }
    public function fetchProductTotalRating($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        if($data['productId'] != 0 && $data['productId'] != "")
        {
            $getProductReviewData = $getDynamicDB->select('*')->from('houdinv_product_reviews')->where('houdinv_product_review_product_id',$data['productId'])
            ->order_by('houdinv_product_review_id','DESC')->get()->result();
            if(count($getProductReviewData) > 0)
            {
                $setReviewArray = array();
                foreach($getProductReviewData as $getProductReviewDataList)
                {   
                    $getDate = date('d-m-Y',strtotime($getProductReviewDataList->houdinv_product_review_created_at));
                    $setReviewArray[] = array('name'=>$getProductReviewDataList->houdinv_product_review_user_name,'rating'=>$getProductReviewDataList->houdinv_product_review_rating,'message'=>$getProductReviewDataList->houdinv_product_review_message,'date'=>$getDate);
                }   
                return array('message'=>'Yes','data'=>$setReviewArray);
            }
            else
            {
                return array('message'=>'No data');
            }
            
        }
        else
        {
            $getProductReviewData = $getDynamicDB->select('*')->from('houdinv_product_reviews')->where('houdinv_product_reviews_variant_id',$data['variantId'])
            ->order_by('houdinv_product_review_id','DESC')->get()->result();
            if(count($getProductReviewData) > 0)
            {
                $setReviewArray = array();
                foreach($getProductReviewData as $getProductReviewDataList)
                {   
                    $getDate = date('d-m-Y',strtotime($getProductReviewDataList->houdinv_product_review_created_at));
                    $setReviewArray[] = array('name'=>$getProductReviewDataList->houdinv_product_review_user_name,'rating'=>$getProductReviewDataList->houdinv_product_review_rating,'message'=>$getProductReviewDataList->houdinv_product_review_message,'date'=>$getDate);
                }   
                return array('message'=>'Yes','data'=>$setReviewArray);
            }
            else
            {
                return array('message'=>'No data');
            }
        }
    }
}
?>
