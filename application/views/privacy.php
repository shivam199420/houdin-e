<?php  $this->load->view('Template/header');?> 

<header class="header bg-gray header-page-top">
  <div class="header-page-top-inner">
	  <div class="container">
		<h1 class="display-4">Privacy Policy</h1> 
  </div>
	</div>
</header>


  <section class="section py-8">
        <div class="container">
          <div class="row">
            <div class="col-md-12">

              <h4>General Information</h4>
              <p>Houdine.com (hereinafter referred to as the "Site" or "Houdin-e") provides its customers an online store by use of its on-demand hosted (web based) e-commerce software, accounting management, inventory management and retail management. We take your privacy seriously and it is for the same reason We have developed this Privacy Policy. This Privacy Policy governs Houdin-e and specifies the manner in which We handle your personal information. We assure You that We will maintain and use this information responsibly. Please familiarize yourself with Our privacy practices.</p>
              <p>This Privacy Policy is effective upon acceptance for new users as amended from time to time. While using the facilities or services provided by Houdin-e, or through any other website connected/ linked through this website by Houdin-e, You shall be bound by this Privacy Policy.</p>
              <p>For purposes of this Privacy Policy, the term "You" or "Your" shall refer to the "User (Store Owner)" or the "End User (Customer of the Store)" and the term "We" or "Our" shall refer to "Houdin-e". If You are using the Site or Services on behalf of Your company (or another entity), then "You" means Your company (or such other entity), its officers, members, agents, successors and assigns.</p>
             

              <br>
              <h4>About The Company</h4>
              <p>Houdin-e is owned and managed by Warrdel Solutions LLP a company incorporated in Jaipur (India). This Privacy Policy describes how we handle your personal information on the website and other storage devices and platforms related to the website. This Privacy Policy is applicable to you:</p>
              <ul>
                <li>When you accept the Terms of Service during the process for registering an account with Houdine.com or</li>
                <li>When you use the Site, or</li>
                <li> When you visit the Site, or</li>
                <li>When you grant hyperlink to this website, or</li>
                <li>When you have access to this Site, or</li>
                <li>When access is facilitated for you to this website,</li>
                <li>You expressly consent to our collection, storage, use and disclosure of your personal identification information as described in this Privacy Policy.</li>
              </ul>

              <br>
              <h4>General Statements</h4>
              <p>We will not use your personal data or information which you have provided or we have collected for :</p>
              <ul>
                <li>Marketing other products and services than the ones which we intend to provide through Houdin-e website or affiliate website, or</li>
                <li> Any third party products and services without having given you the opportunity to opt out of such marketing opportunities.</li>
                <li>We are committed to protecting your right to privacy. We collect information about our users in order to help us continually improve our services.</li>
                <li>We also use the information you provide us to grade your ratings, feedback on your activity, past history, credentials, etc.</li>
                <li>
                  We have the right to verify the Information provided by you in reasonable circumstances as required.
                </li>
                <li>We prefer to keep your personal information accurate and up to date. For this, we provide you with the opportunity to update or modify your personal information by logging into your account.</li>
              </ul>

              <br>
              <h4>To Whom Does The Policy Apply</h4>
               <ul>
                <li>This Policy applies to the store owners, the buyers and other third parties who visit the site or link to our site.</li>
                <li> This Policy restricts the use of this website by minors and those who do not have capacity to enter into a valid commercial contract.</li>
                <li> Our services are not available to children and any person under the age of 18 should not submit any personal information to us.</li>
                <li> If you are less than 18 years and you wish to contract, you are required to contract through your parents or legal guardian.</li>
                
              </ul>

              <br>
              <h4>Relation With Third Parties</h4>
               <ul>
                <li>We do not disclose the information given to us by you (store owners or the buyers as the case may be) unless we have obtained consent from you.</li>
                <li> Once consent is communicated we may combine your information with the information on other users and use it to improve and personalise our services, content and advertising.</li>
                <li> We may use your personal information to pass on information in accordance with your interests and preferences. On receiving the first communication of this nature, you will be given the opportunity to opt-out of future communications of similar nature by clicking on unsubscribe link or by following unsubscribe instructions described within the communication. If you do not want to receive email or other communications from us, you can also contact us at contactus@Houdine.com</li>
                <li>  If you do not wish to receive marketing communications from or participate in our ad-customization programs, please communicate your intention to Unsubscribe@Houdin-e.in</li>
                <li>However, as part of your registration for the Services, you may receive certain communications from Houdin-e, such as administrative announcements and customer service messages regarding the Services, and you cannot opt out of receiving such communications. If you disclose your information to others, whether they are buyers or other store owners on our site(s) or other sites throughout the internet, different rules may apply to their use or disclosure of the information you disclose to them. Houdin-e.in does not control the privacy policies of third parties, and you will be subject to the privacy policies of such third parties where applicable.</li>
                <li>We advise you to be diligent before you disclose any of your personal information to others. Houdin-e does not want to act as a medium to facilitate the transfer of personal information, and thus you are strongly discouraged from providing your personal information to the third parties through Houdin-e website.</li>
              </ul>


              <br>
              <h4>Kind Of Information We Collect</h4>
              <ul>
                <li> Users of our website are devoid of any obligation to provide any information other than the one's required to be conveyed at various stages inclusive of the time of registration of an account with our website.</li>
                <li>After the required information is conveyed to us the user is no longer anonymous to Houdin-e. If, thereafter, you choose to provide us with personal information, your consent to transfer and store the information on our server located in Chicago, IL (USA) is implied.</li>
              </ul>
              <br>
              <p>We may collect and store information and it includes the following: Personal Information that you provide to us or disclose in connection with the site or services include the following :</p>
              <ul>
                <li>
                Your name, address, e - mail address, telephone number and any other physical contact information,
              </li>
              <li>Your credit card information,</li>
              <li>Communications and other messages of correspondence between Houdin-e and you. Transactional information based on your activities on the sites (such as selling, item and content you generate or that relates to your account);</li>
              <li>Feedback, correspondence through our sites, and correspondence sent to us;</li>
              <li>Other information from your interaction with our sites, services, content and advertising, including computer and connection information, statistics on page views, traffic to and from the sites, ad data, IP address, geographical location, browser type, referral source, length of visit and the number of pages viewed (web log information);</li>
              <li>Any other additional information we may ask you to submit to authenticate yourself or if we believe you are violating site policies including information about your ID or bill to verify your address, or to answer additional questions online to help verify your identity; If the information provided by you cannot be verified, we may ask you to send us additional information, such as your driver's licence, credit card statement, or passport number or other information confirming your address or to answer additional questions online to help verify your information).</li>
            </ul>
              <br>
              <h4>Usage Of Collected Information</h4>
              <ul>
                <li> We use the information to help us understand more about how our website is used, to improve our site, and to send you information about us and our services which we think may be of interest to you, both electronically and otherwise.</li>
                <li>In order to achieve these we use your information to: Perform website administration;</li>
                <li>Improve your browsing experience by personalising the website;</li>
                <li>Enable your use of the services available on the website;</li>
                <li>Send you general (non-marketing) commercial communications;</li>
                <li>Send statements and invoices to you, and collect payments from you ;</li>
                <li>Send you email notifications which you have specifically requested;</li>
                <li> Deliver our newsletter and other communications relating to our business or businesses of third parties which may be of interest to you, based on the consent which you have given through e-mail or other source.</li>
                <li>Provide third parties with statistical information about our users, but this information will not be used to identify any individual user;</li>
                <li>Deal with enquiries and complaints made by or about you relating to the website; and</li>
                <li>Where you submit personal information for publication on our website, we will verify that you are a subscriber and we will publish and deliver the publication to you and also use that information in accordance with the license you grant to us.</li>
                <li>We or our partners and affiliates will send you direct emails relating to information about our services unless there is a contrary intent from your side towards such receipt.</li>
                <li>As a result of the whole process of buying and selling on the Site, you will obtain the email address and/or shipping address of your customers. By entering into our Merchant User Agreement, you agree that, with respect to other users' personal information that you obtain through Houdin-e or through a Houdin-e related communication or Houdin-e facilitated transaction, Houdin-e hereby grants to you a license to use such information only for Houdin-e related communications that are not unsolicited commercial messages.</li>
                <li>Furthermore you are not licensed to add the name of your customer, to your mail list (email or physical mail) without their express consent regarding the same.</li>
              </ul>
              <br>
              <h4>Information Regarding Stores</h4>
              <p>The Stores accessed through the Site are independently owned & operated by third parties. We do not own or operate any Stores. Thus, if you link to, browse or transact business with any Stores, we are not responsible for the privacy practices or the content of such Stores, including, without limitation, such Store's use of any of your Personal Information. If you visit any Stores, we encourage you to review and become familiar with the Store's privacy policy and any terms of use.

We will not without your express consent provide your personal information to any of the third parties for the purpose of direct marketing.</p>
              <ul>
                <li> <strong>For advertisement - </strong>For the purpose of marketing and promoting our services we gather information of all user accounts and disclose such information in a non-personally identifiable manner to advertisers and other third parties, while doing so we do not disclose to these entities any information that could be used to identify you personally, especially your name, email address, password, credit card number, and bank account number are never disclosed to third party advertisers.</li>
                <li> <strong>Matters relating to our Website - </strong>This website does not protect you against the identification information which you yourself would have provided by associating your name with your User ID. Although our pages with personal information are coded with robot exclusion headers, individuals and corporations may illegally attempt to automatically collect your email address from the Site. We would also keep you informed of any suspicious activity or policy violations on the Site once they come to our notice.</li>
                <li><strong>External Service Providers -</strong> We do not disclose your personal information to external service providers unless you provide your explicit consent. To prevent our disclosure of your personally identifiable information to an external service provider, you can decline such consent or simply not use their services. Because we do not control the privacy practices of these third parties, you should evaluate their practices before deciding to use their services</li>
                <li><strong>Internal Service Providers (ISP) - </strong>Unless you have explicitly agreed or given your prior permission to the ISP for additional use we may use third parties that we refer to as internal service providers to facilitate or outsource one or more elements of our services (e.g., search, identity verification, discussion boards and surveys) and therefore we may provide some of your personally identifiable information directly to them subject to confidentiality agreements with us and other legal restrictions that prohibit the use of the your information we provide them. At instances where we require a survey to be undertaken and have authorized an ISP to do the same, you will be notified of the involvement of theISP, and all information disclosures you make will be strictly optional. If you provide additional information to an ISP directly, then their use of your information is governed by their applicable privacy policy as has been mentioned above earlier.</li>
                <li><strong>Change in Corporate Entities - </strong>You agree that we may share personally identifiable information about you with our parent, affiliates, subsidiaries and joint ventures that are committed to serving your online trading needs and related services, throughout the world. The privacy practices followed by us are governed by this Privacy Policy document and we and our parent, affiliates, subsidiaries and joint ventures use these policies to the extent allowed by applicable law. In the event of merger of all the entities with another separate entity or in the event of acquisition of this company by another company you should expect and agree that Houdin-e may share some or all of your information in order to continue to provide you the user services. You will be served a notice of such event (to the extent it occurs) and we will require that the new combined entity follow the practices disclosed in this Privacy Policy.</li>
                <li><strong>Legal Compliance - </strong>We shall disclose your information where necessary to cooperate with law enforcement inquiries, in order to investigate, prevent, or take action regarding illegal activities, violations of our terms of service as well as to enforce laws and third party rights, such as intellectual property rights and laws relating to fraud., or as otherwise required by law or if there is a valid court order in this regard. We will, in furtherance of such needs adhere to the requests made, in relation to criminal investigation or alleged illegal activity, or other activity that may expose us or you to legal liability, (and you authorise us to) disclose personal information such as name, address, telephone number, email address, User ID history, about you as we, in our sole discretion, believe necessary or appropriate.</li>
                <li><strong>Other Circumstances -</strong> Such information would be also disclosed to enforce or apply the terms of this Agreement when It is understood that You have expressly violated the Terms stipulated under this Agreement , or To protect the rights, property, or safety of Houdin-e, our users, or others, whether during or after the term of your use of the Service.<br/>

This Privacy Policy relates only to our use of the information you provide us and your use of Houdin-e website and does not extend to your use of the Internet other than the website of Houdin-e.</li>
               
              </ul>
              <br>
              <h4>Security Measures</h4>
              <ul>
                <li>  The website confers on its users an inbuilt risk of disclosure at their own cost.</li>
                <li>The website does not provide an absolute guarantee against any or all technological malice's the personally identifiable information of a person would be subject to.</li>
                <li>We undertake to take reasonable technical and organisational precautions to prevent the loss, misuse or alteration of your personal information.</li>
                <li> Various risk factors involved relate to, the disclosure of your personal identification information to third parties who may unlawfully intercept or access transmissions or private communications, or even be lead from the act of abuse or misuse of your information by the third parties of the information that they collect from the Site.</li>
              </ul>
              <br>
              <h4>Use Of Cookies</h4>
              <ul>
                <li>  <strong>Cookies -</strong> These are objects placed in a computer which enables the web server to identify and track the web browser. A cookie consists of information sent by a web server to a web browser, and is stored by the browser. The information is then sent back to the server each time the browser requests a page from the server. We use cookies in the website and there shall be provision in your computer itself to disable cookies.</li>
                <li><strong>Disabling or enabling cookies -</strong> Though cookies and other aids are created to provide better access to the internet including this Site you have the ability to accept or decline cookies by modifying the settings in your computer through your browser. However, it is brought to your notice that disabling of cookies does not guarantee easy access of this Site.</li>
                <li><strong>Use of web beacons - </strong>Some of our web pages may contain electronic images known as web beacons (sometimes known as clear gifs) that allow us to count users who have visited these pages. Web beacons collect only limited information which includes a cookie number, time and date of a page view, and a description of the page on which the web beacon resides. We may also carry web beacons placed by thirdparty advertisers. These beacons do not carry any personally identifiable information and are only used to track the effectiveness of a particular campaign.</li>
                
              </ul>
              <br>
              <h4>Use Of Cookies</h4>
             <p>Log files allow us to record visitors' use of the site. We may analyze log file information (who has visited where in our website) from all our visitors.<br/>We use this information to make changes to the layout of the website and to manage the information in it. Log files do not contain any personal information and they are not used to identify any individual patterns of use of the site.</p>
             <h4>Changes In Policy</h4>
             <p>We may need to amend the Privacy Policy from time to time. Any such amendment will take effect as soon as it is posted on the website.<br/>Please let us know if the personal information which we hold about you needs to be corrected or updated and for that please keep yourself posted of our website on a day to day basis.</p>
             <br>
              <h4>Contact</h4>
             <p>If you have any questions about this privacy policy or our treatment of your personal data, please write to us by email to support@Houdin-e.in or by post to S-7, 2nd Floor, Pinnacle Business Park, Mahakali Caves Road, Andheri East, Mumbai - 400093.</p>
             <br/>
             <h4>Awareness Of Grievance Redressal</h4>
             <p>At Houdin-e, the users' experience is what we deliberate and focus on. This is why we listen and take the time to know our users and take their concerns seriously. Going skin-deep to analyze and scrutinize, how you would feel from the time you step in to Houdin-e till the time you're done, helps us evolve and enhance our services. If a user has found our experience simple and incredible, we know we are doing something worthy. However, when a user is concerned or has complaints, we do everything we can to fix it and make it right</p>
            </div>
          </div>
        </div>
      </section>



  	

<?php  $this->load->view('Template/footer');?>