<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/storesetting') ?>
<style type="text/css">
  @media screen and (min-width: 768px){
.dropdown.dropdown-lg .dropdown-menu {
    min-width: 390px!important;
}
.checkbox{
  padding-left: 10px;
}
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 10px;
}
}
.zoom img{
    padding: 50px;
    transition-duration: 5s;
    margin: 0 auto;
}
img {
    vertical-align: middle;
    height: 40px;
    width: auto;
}
.zoom:hover {
    -ms-transform: scale(3.5); /* IE 9 */
    -webkit-transform: scale(3.5); /* Safari 3-8 */
    transform: scale(3.5); 
    margin-left: 90px;
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
 
          <!--START Page-Title -->
            <div class="row">
         
                <div class="col-md-8">
                   <h4 class="page-title">Testimonial</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li><a href="<?php echo base_url(); ?>storesetting/Storesettinglanding">Store setting</a></li>
                  <li class="active">Testimonial</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                  
            </div>
           <!--END Page-Title --> 
            <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
            
     <?php 
   
   

     ?>
        <div class="row m-t-20">
        <div class="col-sm-12">
        <div class="card-box">
         <div class="pull-left"><h3 style="margin-top: 0px;">Testimonial List</h3></div>
        <div class="pull-right"><a data-toggle="modal" class="btn btn-default" data-target="#con-close-modal" href="#" title="Add testimonial"><i class="fa fa-plus"></i></a></div>
        <div class="clearfix"></div>
             <?php if(!is_array($testimonials_data)){?>
        <div class="">You have not added any testimonial yet. <a data-toggle="modal" data-target="#con-close-modal" href="#">Add a testimonial right now!</a> </div> <?php } ?>
       
         
  
        </div>
        </div>
        </div>

           <?php if(is_array($testimonials_data)){ ?>
         <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">
              
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                   
                    <th>Customer</th>
                    <th>Name</th>
                    <th>Feedback</th>
                    <th>Publish</th>
                    <th>Action</th>
                  </tr>
                  
                </thead>
                <tbody>
               <?php 
                 foreach ($testimonials_data as $key => $value) {
                   # code...
                $publish_val=$value->testimonials_publish;
                if($publish_val=='1'){$publish="true";}else{$publish="false";}
               ?>

                  <tr>
                    
                    <td class="test_image">
                    <?php if($value->testimonials_image){?>
                    <img src="<?php echo base_url(); ?>upload/testimonials/<?=$value->testimonials_image?>">
                    <?php }else{?><img src="<?php echo base_url(); ?>upload/testimonials/defult.jpg"><?php } ?>
                     </td>
                     <td class="test_name"><?=$value->testimonials_name;?></td>
                    <td class="test_feedback"><?=$value->testimonials_feedback;?></td> 
                    <td><?=$publish?></td> 
                    <td>
                    <input type="hidden" name="test_publish" class="test_publish" value="<?=$value->testimonials_publish;?>">
                    <input type="hidden" name="update_testi" class="update_testi" value="<?=$value->id;?>">
                    
                    <button class="btn btn-success waves-effect waves-light edit_tastimo">Edit</button> 
                    <button class="btn btn-danger waves-effect waves-light delete_tastimo">Delete</button></td>
                  </tr>
                  <?php }?>
                 
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <?php } ?>
     



   


        <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title custom_title"><b>Add Testimonial</b></h4> 
                    </div> 
                     <?php echo form_open(base_url('Storesetting/storetestimonials_frm'), array( 'id' => 'storetestimonials_frm', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
                     <div class="ajax_Loginresponse_result">
                    <div class="modal-body"> 
                
                   <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label" name="testimonials_names">Name</label> 
                                    <input type="text" class="form-control" id="field-3" name="testimonials_name" placeholder=""> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Image</label> 
                                     <input type="file" require="true" class="filestyle" data-buttonname="btn-white" name="testimonials_image">
                                </div> 
                            </div> 
                        </div> 
                            <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Feedback</label> 
                                    <textarea type="text" class="form-control" name="testimonials_feedback" rows="5" placeholder=""></textarea> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row">
                              <div class="col-md-6 checkbox checkbox-custom">
                                <input id="Publish_check" type="checkbox" value="1" name="testimonials_checkbox">
                                <label>Publish</label>
                              </div>
                             </div>
                      
                    </div> 

                 </div> 
                    
                    <div class="modal-footer">                         
                        <button type="submit" class="btn btn-primary waves-effect waves-light" name="testimonials_save">Save</button> 
                    </div> 
                      <?php echo form_close(); ?>
                </div> 
            </div>
        </div><!-- /.modal -->

        <!--Delete-->

  <div id="delete_order" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
            <?php echo form_open(base_url('Storesetting/storetestimonials_delete'), array( 'id' => 'storetestimonials_delete', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete testimonial</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this testimonial ?</b></h4>
          </div>
          </div>
          </div>
          <input type="hidden" name="delete_tastimoneal" id="delete_tastimoneal">

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Delete">

          </div>
         

          </div>
             <?php echo form_close(); ?>
          </div>

          </div>

          <!-- END Delete -->

 <!--START Edit-->
 <div id="edit" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
          <?php echo form_open(base_url('Storesetting/storetestimonials_edit'), array( 'id' => 'storetestimonials_edit', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Edit Testimonial</h4>

          </div>
          <div class="edit_module_data">
          <div class="modal-body">

          <div class="row">
          <div class="col-md-12">

          <div class="card-box" style="padding: 0px 20px!important">
             
                                    <div class="row p-l-0">
                                        <div class="col-md-12 p-l-0 form-horizontal">
                                          <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input class="form-control" id="testimonial_edit_name" placeholder="" type="text" name="testimonials_name" value="testimonials_name">
                                            </div>
                                            <div class="form-group">
                                            <label class="control-label">Image</label>
                                             <input type="file" require class="filestyle" name="testimonials_image" data-buttonname="btn-white">
                                             <br/>
                                             <div id="imageset"></div>
                                            </div>
                                            <div class="form-group">
                                            <label class="control-label">Feedback</label>
                                            <textarea class="form-control" name="testimonials_feedback" id="testimonial_edit_feedback" value="Hello Good Shop" rows="2"></textarea>
                                            </div>
                                         <div class="col-md-6 checkbox checkbox-custom m-b-10">
                                           <input id="tastimoneal_publish_check" type="checkbox" name="testimonials_checkbox" value="1">
                                           <label>Publish</label>
                                        </div>
                                       
                                        </div>                             
                           
                                    </div>
                                            
                                  
                                  </div>
          </div>
          </div>
          </div>
</div>

          <input type="hidden" name="update_tastimoneal" id="update_tastimoneal">

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="" value="Edit">

          </div>

          </div>
           <?php echo form_close(); ?>
          </div>

          </div>

          <!-- End Edit-->

<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
    
    $('#storetestimonials_frm').submit(function(e){
    e.preventDefault(); 
 

  if($('#Publish_check').is(':checked')){
    status=true; 
  }else{
     status=''; 
  } 

    urls='<?=base_url();?>'+'Storesetting/storetestimonials_frm?publish_check='+status;
          var dataString = $(this).serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".ajax_Loginresponse_result").html(data);
        }else{

        window.location.reload();
        }
        }
        });  
    });  
    
     $(".delete_tastimo").click(function(){ 
     
       update_id=$(this).siblings('.update_testi').val();
       $('#delete_tastimoneal').val(update_id);              
      $('#delete_order').modal('show');

       });



$(".edit_tastimo").click(function(){ 
     
       update_id=$(this).siblings('.update_testi').val();
       test_image=$(this).parents('td').siblings('.test_image').html();     
       test_name=$(this).parents('td').siblings('.test_name').text();
         
       test_feedback=$(this).parents('td').siblings('.test_feedback').text();

       test_publish=$(this).siblings('.test_publish').val();

       $('#update_tastimoneal').val(update_id);
        $('#imageset').html(test_image);
         $('#testimonial_edit_name').val(test_name);
          $('#testimonial_edit_feedback').val(test_feedback);

          if(test_publish==1){
           $('#tastimoneal_publish_check').prop('checked', true);           
          }else{
          $('#tastimoneal_publish_check').prop('checked', false);
          }
      $('#edit').modal('show');

       });

    



 $('#storetestimonials_edit').submit(function(e){
    e.preventDefault(); 
 
 

    urls='<?=base_url();?>'+'Storesetting/storetestimonials_edit';
          var dataString = $(this).serialize();
     
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".edit_module_data").html(data);
        }else{

        window.location.reload();
        }
        }
        });  
    });  












  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
