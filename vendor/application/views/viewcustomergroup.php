        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">

          <!--START Page-Title -->
            <div class="row">
         
                <div class="col-md-8">
                   <h4 class="page-title">View Customer Group</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Customer</a></li>
                  <li class="active">View Customer Group</li>
                  </ol>
                  </div>
               
            </div>
           <!--END Page-Title --> 

        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
        <div class="row m-t-20">
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <i class="md md-attach-money text-primary"></i>
        <h2 class="m-0 text-dark counter font-600"><?php if($totalCount) { echo $totalCount; } else { echo 0; } ?></h2>
        <div class="text-muted m-t-5">Total customer</div>
        </div>
        </div>  
        </div>

        <div class="row">
        <div class="col-md-12">

        <div class="card-box table-responsive">
        <div class="btn-group pull-right m-t-10 m-b-20">

        <button type="button" class="btn btn-default m-r-5" title="Add customer " data-toggle="modal" data-target="#addCustomerToGroup"><i class="fa fa-plus"></i></button>
        <button type="button" class="btn btn-default m-r-5 setSupplierMultiBtn deleteMultipleroupCustomer" title="Delete customer" style="display:none;"><i class="fa fa-trash-o"></i></button>
        </div>
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th><input type="checkbox" class="masterSupplierCheck"></th>
        <th>Customer name</th>
        <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        foreach($GroupCustomerList as $GroupCustomerListData)
        {
        ?>    
        <tr>
        <td><input type="checkbox" class="childSupplierCheck" data-id="<?php echo $GroupCustomerListData->houdinv_users_group_users_list_id ?>"></td>
        <td><?php echo $GroupCustomerListData->houdinv_user_name ?></td>
        <td>
        <button data-id="<?php echo $GroupCustomerListData->houdinv_users_group_users_list_id ?>" class="btn btn-success waves-effect waves-light singleDeleteBtnData">Delete</button>
        </td>
        </tr>
        <?php }
        ?>
        </tbody>
        </table>
        <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
        </div>
        </div>
        </div>


        </div>
        </div>
        </div>

        <!-- START Add -->
        <div id="addCustomerToGroup" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Customer/viewcustomergroup/'.$this->uri->segment('3').'' ), array( 'id' => 'addCustomerGroupData', 'method'=>'post' ));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <label for="field-7" class="control-label">Choose customer </label>
        <select multiple="" class="form-control select2 required_validation_for_add_customer_group" name="customerIddata[]">
        <option>Choose Customer</option>
        <?php 
        foreach($customerList as $customerListData)
        {
        ?>    
        <option value="<?php echo $customerListData->houdinv_user_id ?>"><?php echo $customerListData->houdinv_user_name ?></option>
        <?php }
        ?>
        </select>


        </div>

        </div>
        </div>
        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="addCustomerGroup" value="Add Customer">

        </div>

        </div>
        <?php echo form_close(); ?>
        </div>

        </div>
        <!--END Add -->
        <!--Delete-->

        <div id="deleteCustomerFromGroup" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Customer/viewcustomergroup/'.$this->uri->segment('3').'' ), array( 'method'=>'post' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleteCustomerGroupId" name="deleteCustomerGroupId"/>
        <h4><b>Do you really want to Delete this group ?</b></h4>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="deleteGroupCustomerData" value="Delete">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        <?php $this->load->view('Template/footer.php') ?>
        <script>
        $(document).ready(function(){
        $(".select2").select2();

        });
        </script>
        <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addCustomerGroupData',function(){
			var check_required_field='';
			$(this).find(".required_validation_for_add_customer_group").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
	});
	</script>