<?php $this->load->view('Template/header') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <div class="btn-group pull-right m-t-15">
                                    <a href="<?php echo base_url(); ?>package/addpackage" class="btn btn-default btn-sm m-l-5"> <i class="fa fa-plus m-r-5"></i>Add Package</a>
                                </div>
                                <h4 class="page-title">Package Management</h4>
                            </div>
                        </div>
                        <div class="row">
                            <?php 
                            if($this->session->flashdata('error'))
                            {
                                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                            }
                            if($this->session->flashdata('success'))
                            {
                                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                            }
                            ?>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="card-box table-responsive">
                              <table class="table table-striped table-bordered table_shop_custom">
                                <thead>
                                  <tr>
                                    <th>Package Name</th>
                                    <th>Plugins</th>
                                    <th>Price</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    foreach($packageLsit as $packageLsitData)
                                    {
                                    ?>
                                    <tr>
                                    <td><?php echo $packageLsitData->houdin_package_name ?></td>
                                    <td><?php 
                                     echo $packageLsitData->houdin_package_product." products,".$packageLsitData->houdin_package_staff_accounts." staff accounts,".
                                     $packageLsitData->houdin_package_stock_location." stock location".$packageLsitData->houdin_package_app_access." app access".
                                     $packageLsitData->houdin_package_order_pm." order pm".$packageLsitData->houdin_package_sms." free sms".$packageLsitData->houdin_package_email."..."
                                    ?></td>
                                    <td>
                                    <?php 
                                    if($packageLsitData->houdin_package_status == 'active')
                                    {
                                        $setText = "Active";
                                        $setClass = "success";
                                    }
                                    else
                                    {
                                        $setText = "Deactive";
                                        $setClass = "warning";
                                    }
                                    ?>    
                                    <button type="button" class="btn btn-<?php echo $setClass; ?> btn btn-xs"><?php echo $setText; ?></button></td>
                                    <td><?php echo $packageLsitData->houdin_package_price ?></td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>package/editpackage/<?php echo $packageLsitData->houdin_package_id ?>" class="btn btn-warning btn btn-sm"><i class="fa fa-pencil m-r-5"></i>Edit</a>
                                        <button type="button" data-id="<?php echo $packageLsitData->houdin_package_id ?>" class="btn btn-warning btn btn-sm deletePackageBtn"><i class="fa fa-trash m-r-5"></i>Delete</button>
                                    </td>
                                  </tr>    
                                    <?php }
                                    ?>
                                </tbody>
                              </table>
                              <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
                            </div>
                          </div>
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->
<div id="deletePackageModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
<div class="modal-dialog">
<?php echo form_open(base_url( 'Package' ));?>
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
<h4 class="modal-title">Delete Entry</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-12">
    <input type="hidden" class="deletePackageId" name="deletePackageId"/>
<h4><b>Do you really want to delete this entry ?</b></h4>
</div>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
<input type="submit" class="btn btn-info" name="deletePackageBtn" value="Delete">
</div>
</div>
<?php echo form_close();?>
</div>
</div>
<?php $this->load->view('Template/footer.php') ?>
