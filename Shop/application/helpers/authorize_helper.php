<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function getOrderInfo($orderId)
{
    $CI =& get_instance();
    $getDatabase = switchDynamicDatabase();
    $getDb = $CI->load->database($getDatabase,true);
    // get order info
    $getOrderData = $getDb->select('houdinv_order_user_id,houdinv_orders_total_Amount,houdinv_order_delivery_address_id')->from('houdinv_orders')->where('houdinv_order_id',$orderId)->get()->result();
    // get user info
    $getUserData = $getDb->select('*')->from('houdinv_users')->where('houdinv_user_id',$getOrderData[0]->houdinv_order_user_id)->get()->result();
    // get address infon
    $getAddressData = $getDb->select('*')->from('houdinv_user_address')->where('houdinv_user_address_id',$getOrderData[0]->houdinv_order_delivery_address_id)->get()->result();
    return array('order'=>$getOrderData,'user'=>$getUserData,'address'=>$getAddressData);
}
function getOrderInfoApp($orderId,$shopName)
{
    $CI =& get_instance();
    $CI->load->helper('dynamicdatabaseapp_helper');
    $getAppDB = switchDynamicDatabaseApp($shopName);
    $getDb = $CI->load->database($getAppDB,true); 
    // get order info
    $getOrderData = $getDb->select('houdinv_order_user_id,houdinv_orders_total_Amount,houdinv_order_delivery_address_id')->from('houdinv_orders')->where('houdinv_order_id',$orderId)->get()->result();
    // get user info
    $getUserData = $getDb->select('*')->from('houdinv_users')->where('houdinv_user_id',$getOrderData[0]->houdinv_order_user_id)->get()->result();
    // get address infon
    $getAddressData = $getDb->select('*')->from('houdinv_user_address')->where('houdinv_user_address_id',$getOrderData[0]->houdinv_order_delivery_address_id)->get()->result();
    return array('order'=>$getOrderData,'user'=>$getUserData,'address'=>$getAddressData);
}
function updateTransaction($orderArray,$orderId,$transaction)
{
    $CI =& get_instance();
    $getDatabase = switchDynamicDatabase();
    $getDb = $CI->load->database($getDatabase,true);

    if(count($orderArray) > 0)
    {
        $getDb->where('houdinv_order_id',$orderId);
        $getOrderStatus = $getDb->update('houdinv_orders',$orderArray);
        if($getOrderStatus)
        {
            // update accounts data
            $getUndepositedAccount = $getDb->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
            if(count($getUndepositedAccount) > 0)
            {
                $getCustomerId = $getDb->select('houdinv_order_user_id')->from('houdinv_orders')->where('houdinv_order_id',$transaction['houdinv_transaction_for_id'])->get()->result();
                if(count($getCustomerId) > 0)
                {
                    $getCustomerId = $getCustomerId;
                }
                else
                {
                    $getCustomerId = "";
                }
                $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                'houdinv_accounts_balance_sheet_ref_type'=>'Online Transaction',
                'houdinv_accounts_balance_sheet_pay_account'=>$getCustomerId[0]->houdinv_order_user_id,
                'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                'houdinv_accounts_balance_sheet_account'=>0,
                'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                'houdinv_accounts_balance_sheet__increase'=>$transaction['houdinv_transaction_amount'],
                'houdinv_accounts_balance_sheet_decrease'=>0,
                'houdinv_accounts_balance_sheet_deposit'=>0,
                'houdinv_accounts_balance_sheet_final_balance'=>0,
                'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                $getDb->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                // get amount
                $getTotalAmount = $getDb->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                if(count($getTotalAmount) > 0)
                {
                    $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                }
                else
                {
                    $setFinalAmount = 0;
                }
                $getDb->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
            }
            // end here
            $getInsert = $getDb->insert('houdinv_transaction',$transaction);
            if($getInsert)
            {
                return array('message'=>'Order placed successfully');
            }
            else
            {
                return array('message'=>'Order placed successfully');
            }
        }
        else
        {
            $getInsert = $getDb->insert('houdinv_transaction',$transaction);
            if($getInsert)
            {
                return array('message'=>'Order placed successfully, transaction is not recorded so please contact to our customer support with transaction id: '.$transaction['houdinv_transaction_transaction_id']);
            }
            else
            {
                return array('message'=>'Order placed successfully, transaction is not recorded so please contact to our customer support with transaction id: '.$transaction['houdinv_transaction_transaction_id']);
            }
        }
    }
    else
    {
        $getInsert = $getDb->insert('houdinv_transaction',$transaction);
        if($getInsert)
        {
            return array('Order placed successfully, something went wrong during transaction.');
        }
        else
        {
            return array('Order placed successfully, something went wrong during transaction.');
        }
    }
}