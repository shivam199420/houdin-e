<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style>
#revenueChart,#shopChart {
  width: 100%;
  height: 500px;
}

.amcharts-export-menu-top-right {
  top: 10px;
  right: 0;
}
</style>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <h4 class="page-title">Dashboard</h4>

                            </div>
                        </div>


                        <div class="row">
                           
                        <div class="col-md-6 col-lg-4">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Earning.png">
                              <h2 class="m-0 text-dark font-600"><b class="">&#8377;<?php if($totalTransaction) { echo $totalTransaction; } else { echo 0; } ?></b></h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Earning</div>
                          </div>
                      </div>

                      <div class="col-md-6 col-lg-4">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Credit.png">
                              <h2 class="m-0 text-dark font-600"><b class="">&#8377;<?php if($totalTransactionCredit) { echo $totalTransactionCredit; } else { echo 0; } ?></b></h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Credit</div>
                          </div>
                      </div>

                      <div class="col-md-6 col-lg-4">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Debit.png">
                              <h2 class="m-0 text-dark font-600"><b class="">&#8377;<?php if($totalTransactionDebit) { echo $totalTransactionDebit; } else { echo 0; } ?></b></h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Debit</div>
                          </div>
                      </div>

                      <div class="col-md-6 col-lg-4">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Profit.png">
                              <h2 class="m-0 text-dark font-600"><b class="">&#8377;<?php echo $totalProfit ?></b></h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Profit</div>
                          </div>
                      </div>

                       <div class="col-md-6 col-lg-4">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Loss.png">
                              <h2 class="m-0 text-dark font-600"><b class="">&#8377;<?php echo $totalLoss ?></b></h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Loss</div>
                          </div>
                      </div>
                      <div class="col-md-6 col-lg-4">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_User.png">
                              <h2 class="m-0 text-dark font-600"><b class=""><?php if($totalUser) { echo $totalUser; } else { echo 0; } ?></b></h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Users</div>
                          </div>
                      </div>
                       </div>
                        
                      <div class="row">
                      <div class="col-md-12 m-t-30">
                        <h4>Shops</h4>
                        <div id="shopData" style="min-width: 100%; height: 400px; margin: 0 auto"></div>
                      </div>
                    </div>
                    </div> <!-- container -->

                </div> <!-- content -->
        <?php $this->load->view('Template/footer.php') ?>
        <script type="text/javascript">

Highcharts.chart('shopData', {
  title: {
    text: 'Basic Statics'
  },
  xAxis: {
    categories: ['Total Shops', 'Total Credit', 'Total Debit', 'Total Subadmins']
  },
  // labels: {
  //   items: [{
  //     html: 'Basic Statics',
  //     style: {
  //       left: '50px',
  //       top: '18px',
  //       color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
  //     }
  //   }]
  // },
  series: [{
    type: 'column',
    name: 'Total Shops',
    data: [<?php if($totalUser) { echo $totalUser; } else { echo 0; } ?>]
  }, {
    type: 'column',
    name: 'Total Credit',
    data: [<?php if($totalTransactionCredit) { echo $totalTransactionCredit; } else { echo 0; } ?>]
  }, {
    type: 'column',
    name: 'Total Debit',
    data: [<?php if($totalTransactionDebit) { echo $totalTransactionDebit; } else { echo 0; } ?>]
  },  
  // {
  //   type: 'pie',
  //   name: 'Total consumption',
  //   data: [{
  //     name: 'Jane',
  //     y: 13,
  //     color: Highcharts.getOptions().colors[0] // Jane's color
  //   }, {
  //     name: 'John',
  //     y: 23,
  //     color: Highcharts.getOptions().colors[1] // John's color
  //   }, {
  //     name: 'Joe',
  //     y: 19,
  //     color: Highcharts.getOptions().colors[2] // Joe's color
  //   }],
  //   center: [100, 80],
  //   size: 100,
  //   showInLegend: false,
  //   dataLabels: {
  //     enabled: false
  //   }
  // }
  ]
});
        </script>