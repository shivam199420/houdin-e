<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function checkShopUrl()
{
    $getServerUrl = $_SERVER['HTTP_HOST'];
    $current_url = $_SERVER['REQUEST_URI'];
    // $getServerUrl = explode('.', $getServerUrl); 
    $getServerUrl = explode('/', $current_url); 
    $setBase = 'http://3.22.189.222/houdin-e/';
    // check subdomain
    $CI =& get_instance();
    // 
    if(count($getServerUrl) > 2) { 
        $masterDB = $CI->load->database('master', TRUE);
        $getSubdominData = $masterDB->select('houdin_user_shop_id')->from('houdin_user_shops')->where('domain_name',end($getServerUrl))->get()->result();
        if(count($getSubdominData) > 0)
        {
            return array('message'=>'yes','shop'=>end($getServerUrl));
        }
        else
        {
            return array('message'=>'no','baseUrl'=>$setBase);       
        }
    } else{ 
     return array('message'=>'no','baseUrl'=>$setBase);
    } 
}
