<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header') ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 
<?php
$getData = stroeSetting($this->session->userdata('shopName'));
$cartFunction = $getData[0]->receieve_order_out;
?>
<style type="text/css">
	.title{    position: absolute;
    top: 40%;
    text-align: center;
    left: 0;
    right: 0;
    color: #fff;
    opacity: 0;}
    .over .banner-item:hover + .title {

    opacity: 1;
}
</style>
        <!-- ================== START MAIN SECTION ================== -->
        <div class="main" role="main">
        <!-- Start banner section -->
        <?php
        if(count($sliderListData) > 0)
        {
        ?>
        <div class="banner-section">
        <!-- Swiper -->
        <div class="swiper-container">
        <div class="swiper-wrapper">
        <?php
        $i = 1;
        foreach($sliderListData as $sliderListDataList)
        {
          ?>
        <div class="swiper-slide banner-slider-<?php echo $i; ?>" style="background: url(<?php echo $this->session->userdata('vendorURL') ?>upload/Slider/<?php echo $sliderListDataList->houdinv_custom_slider_image ?>) no-repeat center center / cover !important;">
        <div class="slider-info">
        <div class="container">
        <div class="row">
        <div class="col-lg-6 text-sm-left text-center">
        <span><?php echo $sliderListDataList->houdinv_custom_slider_head ?></span>
        <h2><?php echo $sliderListDataList->houdinv_custom_slider_sub_heading ?></h2>
        <p></p>
        </div>
        </div>
        </div>
        </div>
        </div>
        <?php $i++; } ?>
        </div>
        <!-- Add Arrows -->
        <div class="swiper-arrows">
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        </div>
        </div>
        </div>
        <?php } ?>
        <?php
        if($this->session->flashdata('message_name'))
        {
                echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
        }
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
    ?>
        <!-- Start latest catefgory button -->
        <?php
        if(count($categoryData) > 0 && $customHome[0]->houdinv_custom_home_data_category)
        {
        ?>

        <div class="masonry-banner">
            <div class="container">
            <div class="row">
        <div class="col-lg-6 offset-lg-3">
        <div class="section-title text-center mb-4">
        <h3 class="mt-0"><?php echo $customHome[0]->houdinv_custom_home_data_category; ?></h3>
        </div>
        </div>
        </div>
                <div class="row">
                <?php
                foreach($categoryData as $categoryDataList)
                {
                if($categoryDataList->houdinv_category_thumb)
                {
                $setImageData = $this->session->userdata('vendorURL')."images/category/".$categoryDataList->houdinv_category_thumb;
                }
                else
                {
                $setImageData = base_url()."Extra/noPhotoFound.png";
                }
                ?>
                    <div class="col-lg-3 col-sm-6 over">
                    <a href="<?php echo base_url() ?>Productlist/<?php echo $categoryDataList->houdinv_category_id ?>">
                        <div class="banner-item padding-bottom-xs-30 ">
                            <img src="<?php echo $setImageData; ?>" alt="" class="img-fluid">
                        </div>

                        <h3 class="title"><?php echo $categoryDataList->houdinv_category_name ?></h3>
                        </a>
                    </div>

        <?php } ?>

                </div>
            </div>
        </div>
        <?php } ?>
        <!-- end latest category button -->
        <!-- Start service section -->

        <!-- Start all products -->
        <?php
        if($customHome[0]->houdinv_custom_home_data_latest_product && count($latestProducts) > 0)
        {
        ?>
        <div class="all-product">
        <div class="container">
        <div class="row">
        <div class="col-lg-6 offset-lg-3">
        <div class="section-title text-center mb-4">
        <h3 class="mt-0"><?php echo $customHome[0]->houdinv_custom_home_data_latest_product; ?></h3>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-lg-12">
        <div class="owl-carousel all-product-list owl-theme">
        <?php
        // print_r($latestProducts);
        foreach($latestProducts as $latestProductsList)
        {
          $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
          $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
        ?>
        <div class="item">
        <div class="single-item">
        <?php 
    if($getProductImage[0])
    {
        $setLatestData = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
    }
    else
    {
        $setLatestData = base_url()."Extra/noPhotoFound.png";
    }
    ?>
        <img src="<?php echo $setLatestData; ?>" alt="" class="img-fluid">
        <div class="add-product">
        <ul>
        <?php

        if($latestProductsList->houdinv_products_total_stocks <= 0)
        {
          if($cartFunction == 1)
          {
            ?>
          <li><a href="javascript:;" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" class="Add_to_cart_button"><i class="fa fa-shopping-basket"></i></a></li>
         <?php  }
        }
        else
        {
          ?>
          <li><a href="javascript:;" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" class="Add_to_cart_button"><i class="fa fa-shopping-basket"></i></a></li>
        <?php }
        ?>

        <li><a href="javascript:;" class="Add_to_whishlist_button"  data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-heart"></i></a></li>
        <li><a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-eye"></i></a></li>
        </ul>
        </div>
        <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?>
        <div class="discount-tag">
        <span>Out of stock</span>
        </div>
        <?php } ?>
        <?php
        if($latestProductsList->houdinv_products_main_quotation == 1)
        {
        ?>
                <div class="">
                <span class="model_qute_show" data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal" style="position: absolute;
    display: inline-block;
    background: #000;
    color: #fff;
    z-index: 99;
    top: 15px;
    padding: 3px 12px;
    border-radius: 2px;
    cursor: pointer;
    right: 0;">Ask Quotation</span>
                </div>
        <?php
        }
        ?>

        </div>
        <div class="price-info">
        <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><?php echo substr($latestProductsList->houdin_products_title,0, 25).".." ; ?></a>
        <div class="clearfix mt-2">

        <h5 class="pull-right m-0"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?> <?php  } ?></h5>
        </div>
        </div>
        </div>
        <?php } ?>
        </div>
        </div>
        </div>
        </div>
        </div>
        <?php } ?>

        <?php
        if($customHome[0]->houdinv_custom_home_data_featured_product)
        {
         ?>
        <!-- Start all products -->
        <div class="all-product">
        <div class="container">
        <div class="row">
        <div class="col-lg-6 offset-lg-3">
        <div class="section-title text-center mb-4">
        <h3 class="mt-0"><?php echo $customHome[0]->houdinv_custom_home_data_featured_product; ?></h3>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-lg-12">
        <div class="owl-carousel all-product-list owl-theme">

        <?php
        foreach($featuredProduct as $latestProductsList)
        {
          // print_r($latestProductsList);
          $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
          $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
        ?>
        <div class="item">
        <div class="single-item">
        <?php 
      if($getProductImage[0])
      {
          $setFeaturedData = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
      }
      else
      {
          $setFeaturedData = base_url()."Extra/noPhotoFound.png";
      }
      ?>
        <img src="<?php echo $setFeaturedData ?>" alt="" class="img-fluid">
        <div class="add-product">
        <ul>
        <?php

        if($latestProductsList->houdinv_products_total_stocks <= 0)
        {
          if($cartFunction == 1)
          {
            ?>
            <li><a href="javascript:;" class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-shopping-basket"></i></a></li>
         <?php  }
        }
        else
        {
          ?>
          <li><a href="javascript:;" class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-shopping-basket"></i></a></li>
        <?php }
        ?>
        <li><a href="javascript:;" class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-heart"></i></a></li>
        <li><a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-eye"></i></a></li>
        </ul>
        </div>
        <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?>
        <div class="discount-tag">
        <span>Out of stock</span>
        </div>
        <?php } ?>
        <?php
        if($latestProductsList->houdinv_products_main_quotation == 1)
        {
        ?>
        <div class="model_qute_show" data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal" style="position: absolute;display: inline-block;background: #000;color: #fff;z-index: 99;top: 15px;padding: 3px 12px;border-radius: 2px;cursor: pointer;right: 0;">Ask Quotation
        </div>
        <?php
        }
        ?>
        </div>
        <div class="price-info">
        <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><?php echo substr($latestProductsList->houdin_products_title,0, 25).".." ; ?></a>
        <div class="clearfix mt-2">
        <h5 class="pull-right m-0"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?> <?php  } ?></span></h5>
        </div>
        </div>
        </div>
        <?php } ?>
        </div>
        </div>
        </div>
        </div>
        </div>
        <?php } ?>
        <!-- Start banner -->


        <!-- Start testimonial -->
        <?php if(count($storeTestimonial) > 0 && $customHome[0]->houdinv_custom_home_data_testimonial)
        {
        ?>
        <div class="testimonial">
        <div class="container">
        <div class="row">
        <div class="col-lg-6 offset-lg-3">
        <div class="section-title text-center mb-4">
        <h3 class="mt-0 text-warning"><?php echo $customHome[0]->houdinv_custom_home_data_testimonial; ?></h3>
        </div>
        <div class="clearfix"></div>
        </div>
        </div>

        <div class="row">
        <div class="col-lg-6 offset-lg-3">
        <div class="owl-carousel testimonial-list owl-theme">
        <?php
        foreach($storeTestimonial as $storeTestimonialData)
        {
        ?>
        <div class="item">
        <div class="testimonial-item d-flex">
        <div class="testi-img">
        <img src="<?php if($storeTestimonialData->testimonials_image) { ?> <?php echo $this->session->userdata('vendorURL')."upload/testimonials/".$storeTestimonialData->testimonials_image; ?> <?php  } else { ?> <?php echo base_url() ?>Extra/apparels/img/testimonial/1.jpg <?php } ?>" alt="" class="img-fluid">
        </div>
        <div class="testimonial-info">
        <h4 class="m-0"><?php echo $storeTestimonialData->testimonials_name ?></h4>
        <p class="m-0"> <?php echo $storeTestimonialData->testimonials_feedback ?></p>
        </div>
        </div>
        </div>
        <?php } ?>

        </div>
        </div>
        </div>
        </div>
        </div>
        <?php } ?>
        <!-- Start letest news -->

        <!-- ================== END MAIN SECTION ================== -->
        <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view(''.$getShopName.'/Template/footer') ?>
