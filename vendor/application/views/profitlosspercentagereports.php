<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/accountsidebar.php'); 
$getCurrency = getVendorCurrency();
if($getCurrency[0]->houdin_users_currency=="USD")
{
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="AUD"){
	$currencysymbol= "$";
}else if($getCurrency[0]->houdin_users_currency=="Euro"){
	$currencysymbol= "£";
}else if($getCurrency[0]->houdin_users_currency=="Pound"){
	$currencysymbol= "€";
}else if($getCurrency[0]->houdin_users_currency=="INR"){
	$currencysymbol= "₹";
}
?>
<style type="text/css">
	.heading_content ul{list-style: none;    padding-left: 0px;}
	.heading_content ul li a{    padding: 7px;
    display: block;}



	.border-t{border-top: 1px solid #ddd;}
	.border-b{border-bottom: 1px solid #ddd;}
	.text-gray{color: #797979;}

/*17-09-2018*/
table.table tr:nth-child(2) td {
    border-top: 0;
}
.sub-part {
    padding-left: 5px;
}
table.table tr:nth-child(5) td, table.table tr:nth-child(6) td {
    border-top: 0;
}
.table-responsive tr td:first-child, .table-responsive tr th:first-child {
    text-align: left;
}
.table-responsive tr td, .table-responsive tr th {
    text-align: right;
}
.table thead tr:nth-child(2) th:first-child {
    text-align: right;

    border-right: 1px dotted;
     border-bottom: 0;
}
.table thead tr:nth-child(2) th:nth-child(2){border-bottom: 0;}
table.table thead tr:first-child th:nth-child(2) {
    text-align: center;

}
thead {
    border-top: 1px solid #f4f8fb;
}

table.table thead tr:first-child th:nth-child(2) {
    text-align: center;
    border-left: 1px dotted;
        border-bottom: 0;
}
.table-responsive tr:last-child td, .table-responsive tr:nth-last-child(2) td {
    padding: 0;
    line-height: 1px;
    border-color: #000;

}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">

                <div class="col-md-12">
                   <h4 class="page-title">Profit & Loss Percentage</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url() ?>">Home</a></li>
                  <li class="active">Profit & Loss Percentage</li>
                  </ol>
                  </div>
            </div>
            <div class="row m-t-20">
			<div class="errorMessage"></div>
           				<div class="col-sm-12">


                            					<!-- <div class="col-md-4">
												<div class="form-group m-r-10">
												<label for="exampleInputEmail2">Report Period</label>
													<select class="form-control"><optgroup>All Dates</optgroup><option>This Fiancial year to date</option><option>All Dates</option><option>Custom</option><option>Today</option><option>This Week</option><option>Week-to-Date</option></select>
												</div>
												</div> -->
												<div class="col-md-4">
												<div class="form-group m-r-10">
												<label for="exampleInputEmail2">From</label>
													<input type="text" value="<?php echo $from ?>" class="form-control date_picker dateFromText commonChangeClassData" id="" placeholder="">
												</div>
												</div>
												<div class="col-md-4">
												<div class="form-group m-r-10">
													<label for="exampleInputEmail2">To</label>
													<input type="text" value="<?php echo $to ?>" class="form-control date_picker dateToText commonChangeClassData" id="" >
												</div>
												</div>

                            			</div>
                      </div>
                      <div class="m-t-40 ">
                      <div class="col-md-12 bg-white">
                      <div class="col-md-2"></div>
                      <div class="col-lg-8 m-t-30">
								<div class="panel panel-default" style="border: 1px solid #ddd;">
									<div class="panel-heading pull-left" style="width: 100%;">
										<h3 class="panel-title pull-left">Profit & Loss Percentage</h3>
										<div class="icon-right pull-right">
										<a href="<?php echo base_url(); ?>Accounts/profitlosspercentagedatareport/<?php echo $from ?>/<?php echo $to ?>" class="btn btn-default setPdfUrl"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>

										</div>
									</div>
									<div class="panel-body">
									<div class="heading_sheet text-center">
									<?php $getVendorInfo = fetchvendorinfo(); ?>
									<h4><?php echo $getVendorInfo['name'] ?></h4>
									<h5><strong>Profit and Loss % of Total Income </strong></h5>
									<p class="setDateDataValue"><?php echo $from." - ".$to; ?></p>
									</div>
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr><td rowspan="2" style="width: 50%;">&nbsp;</td><th colspan="2" class="text-center">TOTAL</th></tr>
												<tr><th class="setDateDataValue"><?php echo $from." - ".$to; ?></th><th>% OF INCOME
</th></tr>
											</thead>
											<tbody>
											<?php
											if($credit > $debit)
											{
												$setSign = "+";
											}
											else
											{
												$setSign = "-";
											}
											?>
										    <tr><td colspan="3">Income</td></tr>
												<tr>
												<td>


														<div class="sub-part">Service</div>


												</td>
												<td class="setCreditAmount"><?php echo $currencysymbol; ?><?php if($credit) { echo $credit; } else { echo $credit; } ?></td>
												<td>&nbsp;</td>
												</tr>
													<tr>
												<th>
												Total Income
												</th>
												<th class="setCreditAmount"><?php echo $currencysymbol; ?><?php if($credit) { echo $credit; } else { echo $credit; } ?></th>
												<th>100%</th>
												</tr>
												<tr><td>GROSS PROFIT</td>
													<th class="setCreditAmount"><?php echo $currencysymbol; ?><?php if($credit) { echo $credit; } else { echo $credit; } ?></th>
													<th>100%</th>

												</tr>
												<tr>
												<td>

											Expenses


												</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr>
												<tr>
												<td>


													<div class="sub-part">Depreciation Expense</div>

												</td>
												<td class="setDebitAmount"><?php echo $currencysymbol; ?><?php if($debit) { echo $debit; } else { echo 0; } ?></td>
												<td class="setDebitPercentage"><?php $getPercentage = ($credit/$debit)*100;
												echo $setSign."".$getPercentage."%";
												?></td>

												</tr>
													<tr>
												<th>
												Total Expenses
												</th>
												<th class="setDebitAmount"><?php echo $currencysymbol; ?><?php if($debit) { echo $debit; } else { echo 0; } ?></th>
												<th class="setDebitPercentage"><?php $getPercentage = ($credit/$debit)*100;
												echo $setSign."".$getPercentage."%";
												?></th>
												</tr>
												<tr>
												<td>
												 PROFIT
												</td>
												<th class="setFinalAmountData">
												<?php echo $currencysymbol; ?><?php
												$getFinalAmount = $credit-$debit;
												if($getFinalAmount)
												{
													echo $getFinalAmount;
												}
												else
												{
													echo 0;
												}
												?>
												</th>
												<th class="finalPercentageData">
												<?php
												 if($setSign == '+') { $getFinalProfitPercentage = $getPercentage-100; } else { $getFinalProfitPercentage = 100-$getPercentage; }
												?>
												<?php echo $getFinalProfitPercentage; ?>%</th>
												</tr>
												<tr>
												<td colspan="3">&nbsp;</td>

												</tr>
												<tr>
												<td colspan="3">&nbsp;</td>

												</tr>
											</tbody>
										</table>
									</div>

									</div>
								</div>
							</div>
							<div class="col-md-2"></div>
                      </div>
                      </div>
                    </div>
                  </div>
 <?php $this->load->view('Template/footer.php') ?>
 <script type="text/javascript">
		$(document).ready(function(){
			var setBaseData = "<?php echo base_url(); ?>";
			$(document).on('change','.commonChangeClassData',function(){
				var setSign = "";
				$('.errorMessage').html('');
				var fromdate = $('.dateFromText').val();
				var toDate = $('.dateToText').val();
				var setPDFUrl = setBaseData+"Accounts/profitlosspercentagedatareport/"+fromdate+"/"+toDate;
				$('.setPdfUrl').attr('href',setPDFUrl);
				jQuery.ajax({
                type: "POST",
                url: setBaseData+"Accounts/updateProfitlossspercentagedetailReport",
                data: {"fromdate": fromdate,"toDate": toDate},
                success: function(data) {
					var getFinalData = $.parseJSON(data);
					if(getFinalData.message == 'success')
					{
						if(parseFloat(getFinalData.credit) > parseFloat(getFinalData.debit))
						{
							setSign = "+";
						}
						else
						{
							setSign = "-";
						}
						var credit = getFinalData.credit;
						if(credit)
						{
							credit = credit;
						}
						else
						{
							credit = 0;
						}
						var debit = getFinalData.debit;
						if(debit)
						{
							debit = debit;
						}
						else
						{
							debit = 0;
						}
						// calculate debit percentage
						var getDebitPercentage = (parseFloat(credit)/parseFloat(debit))*100;
						var finalAmount = parseFloat(credit)-parseFloat(debit);
						if(setSign == "+")
						{
							var finalPercenatgeData = getDebitPercentage-100;
						}
						else
						{
							var finalPercenatgeData = 100-getDebitPercentage;
						}

						$('.setCreditAmount').text('').text(credit);
						$('.setDebitAmount').text('').text(debit);
						$('.setDebitPercentage').text('').text(setSign+""+getDebitPercentage+"%");
						$('.setFinalAmountData').text('').text(finalAmount);
						$('.finalPercentageData').text('').text(finalPercenatgeData);
						$('.setDateDataValue').text('').text(getFinalData.from+" - "+getFinalData.to);
					}
					else if(getFinalData.message == 'validation')
					{
						$('.errorMessage').html('').html('<div class="alert alert-danger">'+getFinalData.data+'</div>');
					}
					else
					{
						$('.errorMessage').html('').html('<div class="alert alert-danger">Something went wrong. Please try again</div>');
					}
                }
                });
			})

		})
		</script>
<script type="text/javascript">
	$('.tree .icon').click( function() {
  $(this).parent().toggleClass('expanded').
  closest('li').find('ul:first').
  toggleClass('show-effect');
});
</script>