<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Orders extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopuserordermodel');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
        $this->from = fromemail();
        $this->perPage = 12;
    }
    public function index()
    {
        // cancel order request
        if($this->input->post('cancelOrder'))
        {
            $this->form_validation->set_rules('cancelOrderId','text','required');
            if($this->form_validation->run() == true)
            {
                $getOrderStatus = $this->Shopuserordermodel->cancelOrderRequest($this->input->post('cancelOrderId'));
                if($getOrderStatus['message'] == 'yes')
                {
                    if($getOrderStatus['userId'] != "")
                    {
                         // send email to admin user
                        $url = 'https://api.sendgrid.com/';
                        $user = 'anuankita';
                        $pass = 'india@123';
                        $json_string = array(
                        'to' => array(
                            $getOrderStatus['businessId']
                        ),
                        'category' => 'test_category'
                        );
                        $htm = '
                        <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                        <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                        <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                        <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                        <img src="'.base_url().'assets/images/main-logo.png" style="max-width: 53%;width:100%"/></td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Hello Vendor,</strong></td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Customer Raise a cancellation request against order number: '.$this->input->post('cancelOrderId').'</td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                        <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                        <table width="100%" ><tr >
                        <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e 2018--All right reserverd </td></tr>
                        <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
                        </tr></table>
                        </div>
                        </div></td></tr></table>';
                        $params = array(
                            'api_user'  => $user,
                            'api_key'   => $pass,
                            'x-smtpapi' => json_encode($json_string),
                            'to'        => $getOrderStatus['businessId'],
                            'fromname'  => 'Customer Request',
                            'subject'   => 'Cancel Order Request',
                            'html'      => $htm,
                            'from'      => $getOrderStatus['userId'],
                        );
                        $request =  $url.'api/mail.send.json';
                        $session = curl_init($request);
                        curl_setopt ($session, CURLOPT_POST, true);
                        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                        curl_setopt($session, CURLOPT_HEADER, false);
                        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($session);
                        curl_close($session);
                        $getResponse = json_decode($response);
                        if($getResponse->message == 'success')
                        {
                            $getRemaining = $getOrderStatus['count']-1;
                            updateSuceessEmailCount($getRemaining,$this->session->userdata('shopName'));
                            $this->session->set_flashdata('success','Cancel request raise successfully');
                            redirect(base_url()."Orders", 'refresh'); 
                        }
                        else
                        {
                            updateErrorEmailLog($this->session->userdata('shopName'));
                            $this->session->set_flashdata('success','Cancel request raise successfully');
                            redirect(base_url()."Orders", 'refresh');
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Cancel request raise successfully');
                        redirect(base_url()."Orders", 'refresh'); 
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url()."Orders", 'refresh'); 
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url()."Orders", 'refresh'); 
            }
        }
        
        
         if($this->input->post('returnOrder'))
        {
            $this->form_validation->set_rules('returnOrderId','text','required');
            if($this->form_validation->run() == true)
            {
                $getOrderStatus = $this->Shopuserordermodel->returnOrderRequest($this->input->post('returnOrderId'));
                if($getOrderStatus['message'] == 'yes')
                {
                    if($getOrderStatus['userId'] != "")
                    {
                         // send email to admin user
                        $url = 'https://api.sendgrid.com/';
                        $user = 'anuankita';
                        $pass = 'india@123';
                        $json_string = array(
                        'to' => array(
                            $getOrderStatus['businessId']
                        ),
                        'category' => 'test_category'
                        );
                        $htm = '
                        <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                        <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                        <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                        <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                        <img src="'.base_url().'assets/images/main-logo.png" style="max-width: 53%;width:100%"/></td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Hello Vendor,</strong></td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Customer Raise a return request against order number: '.$this->input->post('returnOrderId').'</td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                        <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                        <table width="100%" ><tr >
                        <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e 2018--All right reserverd </td></tr>
                        <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
                        </tr></table>
                        </div>
                        </div></td></tr></table>';
                        $params = array(
                            'api_user'  => $user,
                            'api_key'   => $pass,
                            'x-smtpapi' => json_encode($json_string),
                            'to'        => $getOrderStatus['businessId'],
                            'fromname'  => 'Customer Request',
                            'subject'   => 'Cancel Order Request',
                            'html'      => $htm,
                            'from'      => $getOrderStatus['userId'],
                        );
                        $request =  $url.'api/mail.send.json';
                        $session = curl_init($request);
                        curl_setopt ($session, CURLOPT_POST, true);
                        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                        curl_setopt($session, CURLOPT_HEADER, false);
                        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($session);
                        curl_close($session);
                        $getResponse = json_decode($response);
                        if($getResponse->message == 'success')
                        {
                            $getRemaining = $getOrderStatus['count']-1;
                            updateSuceessEmailCount($getRemaining,$this->session->userdata('shopName'));
                            $this->session->set_flashdata('success','return request raise successfully');
                            redirect(base_url()."Orders", 'refresh'); 
                        }
                        else
                        {
                            updateErrorEmailLog($this->session->userdata('shopName'));
                            $this->session->set_flashdata('success','return request raise successfully');
                            redirect(base_url()."Orders", 'refresh');
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','return request raise successfully');
                        redirect(base_url()."Orders", 'refresh'); 
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url()."Orders", 'refresh'); 
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url()."Orders", 'refresh'); 
            }
        }
        
        $setOrderArray = array('shopName'=>$this->session->userdata('shopName'),'userid'=>$this->session->userdata('userAuth'));
        $resultArray = $this->Shopuserordermodel->fetchUserOrders($setOrderArray); 
        $getShopName = getfolderName($this->session->userdata('shopName'));   
        $this->load->view("".$getShopName."/order",$resultArray);
    }
    
    
    public function quatationAdd()
    {
        
         if($this->input->post('quote_submit'))
        {
            $this->form_validation->set_rules('name','name','required');
            $this->form_validation->set_rules('phone','phone','required');
            $this->form_validation->set_rules('email','email','required|valid_email');
            $this->form_validation->set_rules('count','product count','required|integer');
            $this->form_validation->set_rules('comment','description','required');
             $this->form_validation->set_rules('product_id','product id','required');
        if($this->form_validation->run() != true)
            {
            $this->session->set_flashdata('message_name',validation_errors());
          
                redirect($_SERVER['HTTP_REFERER'], 'refresh');      
                
            }
            $date = strtotime(date("Y-m-d, h:i:s"));
            $quete_array = array("houdinv_shop_ask_quotation_name"=>$this->input->post("name"),
                                 "houdinv_shop_ask_quotation_email"=>$this->input->post("email"),
                                 "houdinv_shop_ask_quotation_comment"=>$this->input->post("comment"),
                                 "houdinv_shop_ask_quotation_phone"=>$this->input->post("phone"),
                                 "houdinv_shop_ask_quotation_product_id"=>$this->input->post("product_id"),
                                 "houdinv_shop_ask_quotation_product_count"=>$this->input->post("count"),
                                 "houdinv_shop_ask_quotation_created_date"=>$date,
                                 "houdinv_shop_ask_quotation_update_date"=>$date
                                 );
                                 
             $response = $this->Shopuserordermodel->SaveQuatation($quete_array);  
             if($response)
             {
                //  get email count
                $getEmailCount = fetchEmailCount($this->session->userdata('shopName'));
                $getUseEmail = getShopDetails($this->session->userdata('shopName'));
                if($getEmailCount[0]->houdinv_emailsms_stats_total_credit > 0)
                {
                    // send email to admin user
                    $url = 'https://api.sendgrid.com/';
                    $user = 'anuankita';
                    $pass = 'india@123';
                    $json_string = array(
                    'to' => array(
                        $getUseEmail[0]->houdinv_shop_communication_email,
                    ),
                    'category' => 'test_category'
                    );
                    $htm = '
                    <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                    <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                    <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                    <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                    <img src="'.base_url().'assets/images/main-logo.png" style="max-width: 53%;width:100%"/></td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong>Name</strong></td>
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->input->post("name").'</td>
                    </tr>
                     
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong>Email</strong></td>
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->input->post("email").'</td>
                    </tr>

                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong>Contact</strong></td>
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->input->post("phone").'</td>
                    </tr>

                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong>Quotation</strong></td>
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->input->post("count").'</td>
                    </tr>

                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong>Description</strong></td>
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->input->post("comment").'</td>
                    </tr>

                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                    <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                    <table width="100%" ><tr >
                    <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e 2018--All right reserverd </td></tr>
                    <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
                    </tr></table>
                    </div>
                    </div></td></tr></table>';
                    $params = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'x-smtpapi' => json_encode($json_string),
                        'to'        => $getUseEmail[0]->houdinv_shop_communication_email,
                        'fromname'  => 'Quotation',
                        'subject'   => 'Quotation',
                        'html'      => $htm,
                        'from'      => $this->from,
                    );
                    $request =  $url.'api/mail.send.json';
                    $session = curl_init($request);
                    curl_setopt ($session, CURLOPT_POST, true);
                    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                    curl_setopt($session, CURLOPT_HEADER, false);
                    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($session);
                    curl_close($session);
                    $getResponse = json_decode($response);
                    if($getResponse->message == 'success')
                    {
                        $getRemaining = $getEmailCount[0]->houdinv_emailsms_stats_remaining_credits-1;
                        updateSuceessEmailCount($getRemaining,$this->session->userdata('shopName'));
                        $this->session->set_flashdata('success',"Quatation placed successfully");
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');  
                    }
                    else
                    {
                        updateErrorEmailLog($this->session->userdata('shopName'));
                        $this->session->set_flashdata('success',"Quatation placed successfully");
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');   
                    }
                }
                else
                {   
                    $this->session->set_flashdata('success',"Quatation placed successfully");
                    redirect($_SERVER['HTTP_REFERER'], 'refresh');     
                }
             }  
             else
             {
              $this->session->set_flashdata('message_name',"Somthing went wrong ! try again");
          
                redirect($_SERVER['HTTP_REFERER'], 'refresh');    
             }                
        
        }   
        
    }
    public function orderdetail()
    {
         $id   = $this->uri->segment("3");
         
         if($this->input->post("Cancel"))
         {
         $re =   $this->Shopuserordermodel->OrderCacelRequest($this->input->post("Cancel"),$this->session->userdata('userAuth'));
           if($re)
           {
            $this->session->set_flashdata('success','Cancel request raise successfully');
            redirect(base_url()."Orders/orderdetail", 'refresh');
           }
         }
         
       $data['orders'] = $this->Shopuserordermodel->GetOrdeDetail($id,$this->session->userdata('userAuth'));  
       $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/orderdetail',$data);
        
    }
}    