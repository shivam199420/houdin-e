<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header") ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<!-- Page Title -->
<section>
        <div class="container py-85 py-tn-50">
            <div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-12 ">
                            <h2 class="title text-center">Your Cart</h2>
                             <div class="title-border mx-auto  text-center"></div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </section>

    <!-- End Page Title -->
      <!-- Table -->
      <?php
                if(count($AllCart)<1)
                {
                ?>
                <section>
        <div class="container p-t-100 p-b-30 py-tn-30">
            <div class="row m-t-20">

        <div class="col-sm-12 text-center">
            <h1>Your cart is empty. <a href="<?php echo base_url() ?>home/">Continue Shopping</a></h1>
                </div>
                </div>
                </section>
                <?php }
                else
                {
                ?>
<section>
        <div class="container p-t-100 p-b-30 py-tn-30">
            <div class="row m-t-20">

        <div class="col-sm-12">
        <?php
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }

         if($this->session->flashdata('message_name'))
        {
                echo '<div class="alert alert-danger">'.implode("</br>",json_Decode($this->session->flashdata('message_name'))).'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>

                <div class="col-md-12 cart_table_area">
                <div class="table-responsive">
                    <table class="table-shop cart_prdct_table table-bordered">
                        <thead>
                            <tr>
                            <th class="cpt_no">No.</th>
                            <th class="cpt_img">image</th>
                            <th class="cpt_pn">product name</th>
                            <th class="cpt_q">quantity</th>
                            <th class="cpt_p">price</th>
                            <th class="cpt_t">total</th>
                            <th class="cpt_r">remove</th>
                            </tr>
                        </thead>
                        <?php echo form_open(base_url( 'Cart/UpdateCart' ), array( 'id' => 'UpdateCart', 'method'=>'post' ));?>
                        <tbody>

                        <?php
                        $i=1;
                      $final_price =0;
                        foreach($AllCart as $thisItem)
                        {
                          if($thisItem['stock'] <= 0)
													{
														$setStockData = '(Out of stock)';
													}
						  $count = $thisItem['count'];
						  $main_price = $thisItem['productPrice'];
                          $total_price = $main_price*$count;
                          $final_price =$final_price+$total_price;

                        ?>
									<tr class="tr_row">
										<td><span class="cp_no"><?php echo  $i; ?></span></td>
										<td><a href="javascript:;" class="cp_img"><img style="height: 80px; width: 80px;" src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $thisItem['productImage']; ?>" alt="" /></a></td>
										<td><a href="<?php if($thisItem['productId'] != 0) { echo base_url()."Productlistview/".$thisItem['productId'];  } else { echo base_url()."Productlistview/variant/".$thisItem['productId']; }  ?>" class="cp_title"><?php echo $thisItem['productName']; ?><span style="color:red"><?php echo $setStockData; ?></span></a></td>
										<td>
											<div class="cp_quntty">
                                            	<input name="product[<?php echo  $i-1; ?>][product_id]" value="<?php echo $thisItem['cartId']; ?>" size="2" type="hidden">
												<input name="product[<?php echo  $i-1; ?>][quantity]" value="<?php echo $thisItem['count']; ?>" size="2" type="number">
											</div>
										</td>
										<td><p class="cp_price"><?php echo $main_price; ?></p></td>
										<td><p class="cpp_total"><?php echo $total_price; ?></p></td>
										<td><a class="btn btn-default cp_cart_remove" data-replace="<?php echo $total_price; ?>" data-id="<?php echo $thisItem['cartId']; ?>" dat><i class="fa fa-trash text-danger"></i></a></td>
									</tr>
					<?php
                        $i++;
                        } ?>
                        </tbody>
                        <?php echo form_close(); ?>


                    </table>
                    </div>
                                    <div class="table-button m-t-20 update_custom_btn">
                                    <div class="col-md-12">
                                    <div class="row">
                                    <div class="col-md-8">
                                    <div class="row">
                                    <div class="col-md-5">
                                     <a href="javascript:;" class="update_cart ">update shopping cart </a>
                                    </div>
                                    <div class="col-md-5">
                                       <a href="<?php echo base_url() ?>Checkout" class="">process to checkout</a>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="col-md-4">
                                    <a href="<?php echo base_url() ?>home/<?php echo $this->session->userdata('shopName') ?>" class=" background_black pull-right">continue shopping</a>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Table -->

    <section>
        <div class="container p-b-100">
            <div class="row">
               <div class="col-md-6"></div>
                <div class="col-md-6 p-l-lg-30 p-l-xl-30 m-t-30">
                    <div class="shop-total">
                        <div class="shop-total-heading">
                            <h3 class="title">cart total</h3>
                            <div class="title-border-3 m-b-30"></div>
                        </div>
                        <div class="shop-total-body">
                            <p class="sub-total">Subtotal
                                <span class="sub-total-span set-sub-total-remove"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span>
                            </p>
                            <p class="total">Grandtotal
                                <span class="sub-total-span "><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span>
                            </p>
                            <!-- <form>
                                <div class="form-group au-form m-b-0">
                                    <button type="submit">Apply coupon</button>
                                </div>
                            </form> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Wishlist -->
                <?php }
                ?>

   <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer") ?>
