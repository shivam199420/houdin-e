<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopproductlistviewmodel extends CI_Model{
    function __construct() 
    {
         $this->getAppDB = switchDynamicDatabase($data['shop']);

    $this->getDynamicDB = $this->load->database($this->getAppDB,true);
    }
    

    public function ShowWhishlistData($id)
    {
         
        $main_arrra = $this->getDynamicDB->select("*")->from("houdinv_users_whishlist") 
    ->join("houdinv_products","houdinv_users_whishlist.houdinv_users_whishlist_item_id=houdinv_products.houdin_products_id")
   
    ->where("houdinv_users_whishlist.houdinv_users_whishlist_user_id",$id)->get()->result();


    $data12 = $this->getDynamicDB->select("*")
    ->from("houdinv_users_whishlist") 
    ->join("houdinv_products_variants","houdinv_users_whishlist.houdinv_users_whishlist_item_variant_id=houdinv_products_variants.houdin_products_variants_id")
   
    ->where("houdinv_users_whishlist.houdinv_users_whishlist_user_id",$id)->get()->result();
  

 

    return array('wishproducts'=>$main_arrra,'wishvarint'=>$data12);
    }


    
    public function GetSingleProductList($id)
    {
      $data =  $this->getDynamicDB->select("*")
       ->from("houdinv_products")
       ->where("houdin_products_id",$id)->get()->row(); 
      
    $variant =   $this->getDynamicDB->select("*")->from("houdinv_products_variants")
        ->where("houdin_products_variants_product_id",$data->houdin_products_id)
        ->get()->result();
     
     $reviews =  $this->getDynamicDB->select("*")->from("houdinv_product_reviews")
        ->where("houdinv_product_review_product_id",$data->houdin_products_id)
        ->get()->result();  
      
       $reviews_sum =  $this->getDynamicDB->select_sum("houdinv_product_review_rating")->from("houdinv_product_reviews")
        ->where("houdinv_product_review_product_id",$data->houdin_products_id)
        ->get()->row();    
        // get wishlist 
         $getUserWishlist = $this->getDynamicDB->select('houdinv_users_whishlist_id')->from('houdinv_users_whishlist')->where('houdinv_users_whishlist_item_id',$id)->where('houdinv_users_whishlist_user_id',$this->session->userdata('userAuth'))->get()->result();
         if(count($getUserWishlist) > 0)
         {
             $setWishlistStatus = 'yes';
         }
         else
         {
            $setWishlistStatus = 'no';
         }
        //  get cart
        $getUserCart = $this->getDynamicDB->select('houdinv_users_cart_id')->from('houdinv_users_cart')->where('houdinv_users_cart_item_id',$id)->where('houdinv_users_cart_user_id',$this->session->userdata('userAuth'))->get()->result();
        if(count($getUserCart) > 0)
         {
             $setcartStatus = 'yes';
         }
         else
         {
            $setcartStatus = 'no';
         }
       return array("main_product"=>$data,"related"=>$variant,"reviews"=>$reviews,"reviews_sum"=>$reviews_sum,'setWishlistStatus'=>$setWishlistStatus,'setcartStatus'=>$setcartStatus);
    } 
    
    public function SaveReview($review)
    {
        
      $this->getDynamicDB->insert("houdinv_product_reviews",$review);
     return  $this->getDynamicDB->insert_id();
        
        
    }
    public function getVariantData($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);

        $getVariantData = $this->getDynamicDB->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data)->get()->result();
        $getProductData = $this->getDynamicDB->select('*')->from('houdinv_products')->where('houdin_products_id',$getVariantData[0]->houdin_products_variants_product_id)->get()->result();
        // get review
        $reviews =  $this->getDynamicDB->select("*")->from("houdinv_product_reviews")->where("houdinv_product_reviews_variant_id",$data)->get()->result();  
        // review sum
        $reviews_sum =  $this->getDynamicDB->select_sum("houdinv_product_review_rating")->from("houdinv_product_reviews")->where("houdinv_product_reviews_variant_id",$data)->get()->row(); 
        // get related peroduct
        $variant =   $this->getDynamicDB->select("*")->from("houdinv_products_variants")->where("houdin_products_variants_product_id",$getVariantData[0]->houdin_products_variants_product_id)->get()->result();
        // get wishlist 
        $getUserWishlist = $this->getDynamicDB->select('houdinv_users_whishlist_id')->from('houdinv_users_whishlist')->where('houdinv_users_whishlist_item_variant_id',$data)->where('houdinv_users_whishlist_user_id',$this->session->userdata('userAuth'))->get()->result();
        if(count($getUserWishlist) > 0)
        {
            $setWishlistStatus = 'yes';
        }
        else
        {
           $setWishlistStatus = 'no';
        }
       //  get cart
       $getUserCart = $this->getDynamicDB->select('houdinv_users_cart_id')->from('houdinv_users_cart')->where('houdinv_users_cart_item_variant_id',$data)->where('houdinv_users_cart_user_id',$this->session->userdata('userAuth'))->get()->result();
       if(count($getUserCart) > 0)
        {
            $setcartStatus = 'yes';
        }
        else
        {
           $setcartStatus = 'no';
        }
        return array('variantData'=>$getVariantData,'productData'=>$getProductData,"reviews"=>$reviews,"reviews_sum"=>$reviews_sum,"relateData"=>$variant,'setWishlistStatus'=>$setWishlistStatus,'setcartStatus'=>$setcartStatus);
    }
   
} 
?>