<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function createDynamicDatabase($getDbName)
{
    $CI =& get_instance();
	// $active_group = $config_app;
	$query_builder = TRUE;
	$config_app['dsn'] = '';
	$config_app['hostname'] = 'localhost';
	$config_app['username'] = 'root';
	$config_app['password'] = 'houdine123';
	$config_app['database'] = $getDbName;
	$config_app['dbdriver'] = 'mysqli';
	$config_app['dbprefix'] = '';
	$config_app['pconnect'] = FALSE;
	$config_app['db_debug'] = (ENVIRONMENT !== 'production');
	$config_app['cache_on'] = FALSE;
	$config_app['cachedir'] = '';
	$config_app['char_set'] = 'utf8';
	$config_app['dbcollat'] = 'utf8_general_ci';
	$config_app['swap_pre'] = '';
	$config_app['encrypt'] = FALSE;
	$config_app['compress'] = FALSE;
	$config_app['autoinit'] = TRUE;
	$config_app['stricton'] = FALSE;
	$config_app['failover'] = array();
	$config_app['save_queries'] = TRUE;
    return $config_app;
}
?>