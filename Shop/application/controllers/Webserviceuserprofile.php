<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Webserviceuserprofile extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopuserprofile');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function fetchuserprofile()
    {
        $this->form_validation->set_rules('userId','text','required');
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('id'=>$this->input->post('userId'),'shop'=>$this->input->post('shopName'));
            $getUserProfile = $this->Shopuserprofile->fetchUserProfileApp($setArray);
            print_r(json_encode($getUserProfile));
        }
        else
        {
            $resultArray = array('messgae'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function updateuserprofile()
    {
        $this->form_validation->set_rules('userId','text','required');
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('userName','text','required');
        if($this->form_validation->run() == true)
        {
            $setdata = strtotime(date('Y-m-d h:i:s'));
            $setUpdateArray = array('houdinv_user_name'=>$this->input->post('userName'),'houdinv_users_gender'=>$this->input->post('userGender'),'houdinv_users_dob'=>$this->input->post('userDob'),'houdinv_user_updated_at'=>$setdata);
            $setExtraDataArray = array('id'=>$this->input->post('userId'),'shopname'=>$this->input->post('shopName'));
            $getUpdateStatus = $this->Shopuserprofile->updateUserProfileData($setUpdateArray,$setExtraDataArray);
            if($getUpdateStatus['message'] == 'yes')
            {
                if(!empty($_FILES['name']['name']))
                {
                    $newFileName = rand(100000,999999)."".$_FILES['name']['name'];
                    $config['upload_path'] = "Extra/Uploads/userprofile/";
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $config['max_size'] = '100000000';
                    $config['file_name'] = $newFileName;
                    $this->load->library('upload',$config);
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('name'))
                    {
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                        $uploadImageArray = array('image'=>$picture,'id'=>$this->input->post('userId'),'shopname'=>$this->input->post('shopName'));
                        $getImageUploadStatus = $this->Shopuserprofile->updateUserProfileImageData($uploadImageArray);
                        if($getImageUploadStatus['message'] == 'yes')
                        {
                            $resultArray = array('message'=>'Profile updated successfully');
                            print_r(json_encode($resultArray));
                        }
                        else
                        {
                            $resultArray = array('message'=>'Something went wrong during image upload. Please try again');
                            print_r(json_encode($resultArray));
                        }
                    }
                    else
                    {
                        $error = array('error' => $this->upload->display_errors());
                        $resultArray = array('message'=>$error);
                        print_r(json_encode($resultArray));
                    }
                }
                else
                {
                    $resultArray = array('message'=>'Profile updated successfully');
                    print_r(json_encode($resultArray));
                }
            }
            else
            {
                $resultArray = array('messgae'=>'Something went wrong. Please try again');
                print_r(json_encode($resultArray)); 
            }
        }
        else
        {
            $resultArray = array('messgae'=>'All fileds are mandatory');
            print_r(json_encode($resultArray));
        }
    }
}
