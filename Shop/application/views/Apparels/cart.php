 <?php
 $getShopName = getfolderName($this->session->userdata('shopName'));
 $this->load->view(''.$getShopName.'/Template/header') ?>
 <?php
 $getCurrency = getShopCurrency();
 $currencysymbol=$getCurrency[0]->houdin_users_currency;
 ?>

		<!-- Page item Area -->
		<div id="page_item_area">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 text-left">
						<h3>Shop Details</h3>
					</div>

					<!--<div class="col-sm-6 text-right">
						<ul class="p_items">
							<li><a href="#">home</a></li>
							<li><a href="#">category</a></li>
							<li><span>Cart</span></li>
						</ul>
					</div>	-->
				</div>
			</div>
		</div>

        <div class="container">
         <div class="row">
        <div class="col-sm-12">
        <?php
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }

         if($this->session->flashdata('message_name'))
        {
                echo '<div class="alert alert-danger">'.implode("</br>",json_Decode($this->session->flashdata('message_name'))).'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>
        </div>



		<!-- Cart Page -->
		<div class="cart_page_area">
			<div class="container">

                      <?php   if(count($AllCart)<1)
     {

        ?>

     <div class="row">
                    <div class="col-md-12">
                        <div class="title" style="text-align: center;">
                            <h3>Your cart is empty.</h3>
                        </div>

                        </div>
                        </div>
        <?php
     }
     else
     {
     ?>


				<div class="row">
					<div class="col-sm-12">
						<div class="cart_table_area table-responsive">
							<table class="table cart_prdct_table text-center">
								<thead>
									<tr>
										<th class="cpt_no">No.</th>
										<th class="cpt_img">image</th>
										<th class="cpt_pn">product name</th>
										<th class="cpt_q">quantity</th>
										<th class="cpt_p">price</th>
										<th class="cpt_t">total</th>
										<th class="cpt_r">remove</th>
									</tr>
								</thead>
								<tbody>
    <?php echo form_open(base_url( 'Cart/UpdateCart' ), array( 'id' => 'UpdateCart', 'method'=>'post' ));?>

        <?php
                        $i=1;
                      $final_price =0;
                        foreach($AllCart as $thisItem)
                        {
													if($thisItem['stock'] <= 0)
													{
														$setStockData = '(Out of stock)';
													}
						  $count = $thisItem['count'];
						  $main_price = $thisItem['productPrice'];
                          $total_price = $main_price*$count;
                          $final_price =$final_price+$total_price;

                        ?>
									<tr class="tr_row">
										<td><span class="cp_no"><?php echo  $i; ?></span></td>
										<td><a href="javascript:;" class="cp_img"><img style="max-width: 40%;" src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $thisItem['productImage']; ?>" alt="" /></a></td>
										<td ><a href="<?php if($thisItem['productId'] != 0) { echo base_url()."Productlistview/".$thisItem['productId'];  } else { echo base_url()."Productlistview/variant/".$thisItem['productId']; }  ?>" class="cp_title"><?php echo $thisItem['productName']; ?><span style="color:red"><?php echo $setStockData; ?></span></a></td>
                      <td class="cart-qty">
                          <div class=" cp_quntty">
                              <input name="product[<?php echo  $i-1; ?>][product_id]" value="<?php echo $thisItem['cartId']; ?>" size="2" type="hidden">
                              <input name="product[<?php echo  $i-1; ?>][quantity]" value="<?php echo $thisItem['count']; ?>" size="2" type="number">
                          </div>
                      </td>
										<td><p class="cp_price"><?php echo $main_price; ?></p></td>
										<td><p class="cpp_total"><?php echo $total_price; ?></p></td>
										<td><a class="btn btn-default cp_cart_remove" data-replace="<?php echo $total_price; ?>" data-id="<?php echo $thisItem['cartId']; ?>" dat><i class="fa fa-trash"></i></a></td>
									</tr>
					<?php
                        $i++;
                        } ?>
						  <?php echo form_close(); ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-8 col-xs-12 cart-actions cart-button-cuppon">
						<div class="row">
							<div class="col-sm-7">
								<div class="cart-action">
									<a href="<?php echo base_url() ?>home/category1" class="btn border-btn">continiue shopping</a>


									<a href="javascript:void(0)" class="btn border-btn update_cart">update shopping bag</a>

								</div>
							</div>


						</div>
					</div>

					<div class="col-md-4 col-xs-12 cart-checkout-process text-right">
						<div class="wrap">
							<p><span>Subtotal</span><span class="mcart_subtotal_class" id="mcart_subtotal_class"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span></p>
							<h4><span>Grand total</span><span class="mcart_subtotal_class"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span></h4>
							<a href="<?php echo base_url() ?>Checkout" class="btn border-btn">process to checkout</a>
						</div>
					</div>

				</div>
                         <?php
     }

     ?>
			</div>
		</div>

		<?php
		$getShopName = getfolderName($this->session->userdata('shopName'));
		$this->load->view(''.$getShopName.'/Template/footer') ?>
