    <?php $this->load->view('Template/header') ?>
    <?php $this->load->view('Template/sidebar') ?>
    <div class="content-page">
    <!-- Start content -->
    <div class="content">
    <div class="container">
    <!--START Page-Title -->
    <div class="row">
    
    <div class="col-md-8">
    <h4 class="page-title">Add Purchase</h4>
    <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>">Home</a></li>
    <li><a href="<?php echo base_url(); ?>purchase">Purchase</a></li>

    <li class="active">Add</li>
    </ol>  
    </div>
   
    </div>
    <div class="row setErrorMessage" style="display:none">
    
    </div>
    <!--END Page-Title --> 
    <div class="row">
    <div class="col-sm-12">
    <?php 
    if($this->session->flashdata('success'))
    {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    else if($this->session->flashdata('error'))
    {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
    }
    ?>
    </div>
    </div>
    <div class="row m-t-20">
    <div class="col-lg-12">
    <div class="card-box">
    <form id="sendSupplierData">
    <input type="hidden" class="setToken" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
    <div class="form-group">
    <label for="userName">Supplier</label>
    <select class="form-control select2 checkSupplierProduct" name="supplierId">
    <option value="">Choose Supplier</option>
    <?php 
    foreach($supplierList as $supplierListData)
    {
    ?>  
    <option value="<?php echo $supplierListData->houdinv_supplier_id ?>"><?php echo $supplierListData->houdinv_supplier_contact_person_name ?></option>
    <?php }
    ?>
    </select>
    </div>
    </form>
    </div>
    </div>
    </div>
    <?php echo form_open(base_url( 'Purchase/add' ), array( 'id' => 'addPurchaseDetails', 'method'=>'post' ));?>
    <div class="row">
    <div class="col-md-12">
    <div class="card-box table-responsive">
    <table class="table table-striped table-bordered table_shop_custom">
    <thead>
    <tr>
    <input type="hidden" class="supplierIdData" name="supplierIdData"/>
    <!-- <input type="hidden" class="setSupplierMultiBtn" name="setProductPurchase"/> -->
    <th><input type="checkbox" class="masterSupplierProductCheck"></th>
    <th>Product name</th>
    <th>Quantity</th>
    </tr>
    </thead>
    <tbody class="setTableData">
     
    </tbody>
    </table>
    <div class="form-group">
    <label>Credit Period</label>
    <select class="form-control required_validation_for_prchase_customer changeCreditPeriod" name="creditPeriod">
    <option value="">Choose Credit Period</option>
    <option value="day">Day</option>
    <option value="week">Week</option>
    <option value="month">Month</option>
    </select>
    </div>
    <div class="form-group showPeriodTime" style="display:none">
    <label class="setPeriodLabel">Period Time</label>
    <input type="text" class="form-control required_validation_for_prchase_customer name_validation number_validation" name="periodTime"/>
    </div>
    <div class="form-group">
    <label>Expected Delivery Date</label>
    <input type="text" class="form-control required_validation_for_prchase_customer date_picker name_validation" name="deliveryDate"/>
    </div>
    <div class="pull-right">
    <input type="submit" class="btn btn-success" name="addPurchaseOrder" value="Create Purchase Order"/>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <?php echo form_close(); ?>
    <?php $this->load->view('Template/footer') ?>
    <script>
    $(document).ready(function(){
    $(".select2").select2();
    // add purchase data
    $(document).on('change','.checkSupplierProduct',function(){
      var supplierId = $(this).val();
      $('.setTableData').html('');
      $('.supplierIdData').val('');
      $('.setErrorMessage').html('');
      $('.setSupplierMultiBtn').attr('data-id','');
      $('.masterSupplierCheck').prop('checked',false);
      $('.childSupplierCheck').prop('checked',false);
      var formData = $('#sendSupplierData').serialize();
      $('.checkSupplierProduct').attr('disabled',true);
      $('.setajaxFormData').attr('disabled',true);
      jQuery.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "Purchase/fetchSupplierPorduct",
          data: formData,
          success: function(data) {
            $('.checkSupplierProduct').attr('disabled',false);
            $('.setajaxFormData').attr('disabled',false);
            if($.trim(data) == 'no')
            {
              $('.setErrorMessage').show();
              $('.setErrorMessage').html('<div class="alert alert-danger">Something went wrong. Please try again</div>');
            }
            else
            {
              $('.supplierIdData').val(supplierId);
              $('.setTableData').html(data);
              setValidationData();
            }
          }
          }); 
    })
    function setValidationData()
    {
      console.log('call');
      $(".number_validation").keydown(function (e) {
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
      (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
      (e.keyCode >= 35 && e.keyCode <= 40)) {
      return;
      }
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
      }
      });
      // Name validation
      $(document).on('keydown', '.name_validation', function(e) {
      if (e.which === 32 &&  e.target.selectionStart === 0) {return false;}  });
    }
		$(document).on('submit','#addPurchaseDetails',function(){
      var setData = 0;
      var checkData = 0;
			var check_required_field='';
			$(".required_validation_for_prchase_customer").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
      else 
      {
        // return true;
        $('.childSupplierProductCheck').each(function(){
        if($(this).prop('checked') == true)
        {
          var getValue = $(this).parents('.parentRowClass').find('.setProductQuantity').val();
          if($.trim(getValue) == "")
          {
            setData++;
          }
          checkData++;
        }
        });
        if(checkData == 0)
        {
          $('.setErrorMessage').show();
          $('.setErrorMessage').html('');
          $('.setErrorMessage').html('<div class="alert alert-danger">Please choose atleast one product to create purchase order.</div>');
          return false;
        }
        else
        {
          $('.setErrorMessage').hide();
          $('.setErrorMessage').html('');
        }
        if(setData == 0)
        {
          return true;
        }
        else
        {
          $('.setErrorMessage').show();
          $('.setErrorMessage').html('');
          $('.setErrorMessage').html('<div class="alert alert-danger">Please fill quantity of all selected product.</div>');
          return false;
        }
      }

		});
    });
    </script>
    
