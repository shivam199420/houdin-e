﻿<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
 
            
            <!--START Page-Title -->
            <div class="row">
          
                <div class="col-md-8">
                   <h4 class="page-title">Edit Customer</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Customer Management</a></li>
                  <li><a href="<?php echo base_url(); ?>Customer/privilegedcustomer">Privilege Customer</a></li>
                  <li class="active">Manage Inventory</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
             
            </div>
           <!--END Page-Title -->
            
        <div class="row m-t-20">
        <div class="col-sm-12">
        <div class="card-box">
       
        <form method="">
        <div class="row">
        <div class="col-md-12 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 control-label">Customer Name</label>
        <div class="col-md-10">
        <input type="text" value="" class="form-control" name="" placeholder="Customer Name">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Mobile Number</label>
        <div class="col-md-10">
        <input type="text" value="" class="form-control" name="" placeholder="Mobile Number">
        </div>
        </div>
         <div class="form-group">
        <label class="col-md-2 control-label">Alternate Number</label>
        <div class="col-md-10">
        <input type="text" value="" class="form-control" name="" placeholder="Alternate Number">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Address</label>
        <div class="col-md-10">
        <input type="text" value="" class="form-control" name="" placeholder="Address">
        </div>
        </div>

        
        </div>
        <div class="col-md-12 form-horizontal">
          <div class="form-group">
        <label class="col-md-2 control-label">Email Address</label>
        <div class="col-md-10">
        <input type="text" value="" class="form-control" name="" placeholder="Email Address">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">City</label>
        <div class="col-md-10">
        <input type="text" value="" class="form-control" name="" placeholder="City">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">State</label>
        <div class="col-md-10">
        <input type="text" value="" class="form-control" name="" placeholder="State">
        </div>
        </div>
          <div class="form-group">
          <label class="col-md-2 control-label">Country</label>
          <div class="col-md-10">
          <select class="form-control">
              <option>Country</option>
          <option>India</option>
          <option>Usa</option>
          <option>Uk</option>
        </select>
          </div>
          </div>
           
          
        </div>
        <div class="col-md-12 ">
        <input type="submit" class="btn btn-default pull-right m-r-10" name="customSearchDriver" value="Submit">
        <button type="reset" class="btn btn-info pull-right m-r-10">Reset</button>
        </div>
        </div>
        </form>
        </div>
        </div>
        </div>

                                       <div id="con-close-modal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Add Inventory</h4> 
                                                </div> 
                                                <div class="modal-body"> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label">Choose Status</label> 
                                                                <select class="form-control">
                                                                    <option>Receive</option>
                                                                    <option>Return</option>
                                                                    
                                                                </select>
                                                            </div> 
                                                        </div> 
                                                     
                                                    </div> 
                                                    <!--<div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-3" class="control-label">Note</label> 
                                                                <input type="text" class="form-control" id="field-3" placeholder="Quantity"> 
                                                            </div> 
                                                        </div> 
                                                    </div> -->
                                                  
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group no-margin"> 
                                                                <label for="field-7" class="control-label">Note</label> 
                                                                <textarea class="form-control autogrow" id="field-7" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                    <button type="button" class="btn btn-info waves-effect waves-light">Save changes</button> 
                                                </div> 
                                            </div> 
                                        </div>
                                    </div><!-- /.modal -->
                              
      </div>
    </div>
  </div>
<!--Delete-->

  

<!--change Status-->


        

<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
