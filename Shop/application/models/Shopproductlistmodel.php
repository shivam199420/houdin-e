<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopproductlistmodel extends CI_Model{
    function __construct()
    {
     $this->getAppDB = switchDynamicDatabase($data['shop']);

    $this->getDynamicDB = $this->load->database($this->getAppDB,true);
    }
    public function getProductList($data)
    {
        $settingData = stroeSetting($this->session->userdata('shopName'));
        $this->getDynamicDB->select('*')->from('houdinv_products');
        if($data['categoryId']) { $this->getDynamicDB->where('houdin_products_category',$data['categoryId']); }
        if($data['subcategory']) { $this->getDynamicDB->where('houdin_products_sub_category_level_1',$data['subcategory']); }
        if($data['subsubcategory']) { $this->getDynamicDB->where('houdin_products_sub_category_level_2',$data['subsubcategory']); }
        if($data['setSort'] == 'name') { $this->getDynamicDB->order_by('houdin_products_title','ASC'); }
        else if($data['setSort'] == 'date') { $this->getDynamicDB->order_by('houdin_products_updated_date','DESC'); }
        else if($data['setSort'] == 'pricehl') { $this->getDynamicDB->order_by('houdin_products_final_price','DESC'); }
        else if($data['setSort'] == 'pricelh') { $this->getDynamicDB->order_by('houdin_products_final_price','ASC'); }
        else { $this->getDynamicDB->order_by('houdin_products_id','DESC'); }

        if($settingData[0]->display_out_stock == 0)
        {
            $this->getDynamicDB->where('houdinv_products_total_stocks > ',0);
        }
        $getProductList = $this->getDynamicDB->where('houdin_products_status','1')->limit('20')->get()->result();
        return array('productList'=>$getProductList);
    }
    public function fetchAjaxProduct($data)
    {
        $settingData = stroeSetting($this->session->userdata('shopName'));
        $this->getDynamicDB->select('*')->from('houdinv_products');
        if($data['categoryId']) { $this->getDynamicDB->where('houdin_products_category',$data['categoryId']); }
        if($data['subcategory']) { $this->getDynamicDB->where('houdin_products_sub_category_level_1',$data['subcategory']); }
        if($data['subsubcategory']) { $this->getDynamicDB->where('houdin_products_sub_category_level_2',$data['subsubcategory']); }
        if($data['sortBy'] == 'name') { $this->getDynamicDB->order_by('houdin_products_title','ASC'); }
        else if($data['sortBy'] == 'date') { $this->getDynamicDB->order_by('houdin_products_updated_date','DESC'); }
        else if($data['sortBy'] == 'pricehl') { $this->getDynamicDB->order_by('houdin_products_final_price','DESC'); }
        else if($data['sortBy'] == 'pricelh') { $this->getDynamicDB->order_by('houdin_products_final_price','ASC'); }
        else { $this->getDynamicDB->order_by('houdin_products_id','DESC'); }
        if($settingData[0]->display_out_stock == 0)
        {
            $this->getDynamicDB->where('houdinv_products_total_stocks > ',0);
        }
        $getProductList = $this->getDynamicDB->where('houdin_products_status','1')->limit('12',$data['lastid'])->get()->result();
        return $getProductList;
    }
    public function getProductTotalCount($data)
    {
        $this->getDynamicDB->select('COUNT(houdin_products_id) AS totalProduct')->from('houdinv_products');
        if($data['categoryId']) { $this->getDynamicDB->where('houdin_products_category',$data['categoryId']); }
        if($data['subcategory']) { $this->getDynamicDB->where('houdin_products_sub_category_level_1',$data['subcategory']); }
        if($data['subsubcategory']) { $this->getDynamicDB->where('houdin_products_sub_category_level_2',$data['subsubcategory']); }
        $getProductCount = $this->getDynamicDB->where('houdin_products_status','1')->get()->result();
        return $getProductCount[0]->totalProduct;
    }
    public function fetchProductListModelApp($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopname']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        // fetch vendor setting
       $getVendorStoreSetting =  $getDynamicDB->select('display_out_stock')->from('houdinv_storesetting')->where('id',1)->get()->result();
        $getDynamicDB->select('houdinv_products.*')->from('houdinv_products');
        if($data['category']) { $getCategory = $data['category'];  $getDynamicDB->where("FIND_IN_SET($getCategory, houdinv_products.houdin_products_category) !=", 0); }
        if($data['subCategory']) { $getSubCategory = $data['subCategory'];  $getDynamicDB->or_where("FIND_IN_SET($getSubCategory, houdinv_products.houdin_products_sub_category_level_1) !=", 0); }
        if($data['subSubCategory']) { $getSubSubCategory = $data['subSubCategory'];  $getDynamicDB->where("FIND_IN_SET($getSubSubCategory, houdinv_products.houdin_products_sub_category_level_2) !=", 0); }
        if($data['filter']) { $getExplodeData = explode('-',$data['filter']); $getDynamicDB->where('houdin_products_final_price >=',$getExplodeData[0]); $getDynamicDB->where('houdin_products_final_price <=',$getExplodeData[1]); }
        if($data['sort'] == 'name'){ $getDynamicDB->order_by('houdin_products_title','DESC'); }
        else if($data['sort'] == 'date'){ $getDynamicDB->order_by('houdin_products_updated_date','DESC'); }
        else if($data['sort'] == 'hl'){ $getDynamicDB->order_by('houdin_products_final_price','DESC'); }
        else if($data['sort'] == 'lh'){ $getDynamicDB->order_by('houdin_products_final_price','ASC'); }
        else { $getDynamicDB->order_by('houdin_products_id','DESC');    }
        $getDynamicDB->limit('20',$data['lastLimit']);
        $getDynamicDB->group_start();
        $getDynamicDB->where('houdinv_products_show_on','Both');
        $getDynamicDB->or_where('houdinv_products_show_on','App');
        $getDynamicDB->group_end();
        $getPorductData = $getDynamicDB->get()->result();
        $getNextLimit = count($getPorductData)+$data['lastLimit'];
        if(count($getPorductData) > 0)
        {
            foreach($getPorductData as $getPorductDataList)
            {
                
                // set image
                $getImage = json_decode($getPorductDataList->houdinv_products_main_images,true);
                if($getImage[0])
                {
                    $setProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$getImage[0];
                }
                else
                {
                    $getShop = shopurl();
                    $setProductImage = "".$getShop."Extra/noimage.jpg";
                }
                // set price
                $getPrice = json_decode($getPorductDataList->houdin_products_price,true);
                if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
                {
                    $getOriginalPrice = $getPrice['sale_price'];
                    $getDiscountPrice = $getPorductDataList->houdin_products_final_price;
                }
                else
                {
                    $getOriginalPrice = $getPorductDataList->houdin_products_final_price;
                    $getDiscountPrice = 0;
                }
                // set quantity
                $getQuantity = $getPorductDataList->houdinv_products_total_stocks;
                if($getVendorStoreSetting[0]->display_out_stock == 1)
                {
                    if($getQuantity > 0)
                    {
                        $setProductArray[] = array('productId'=>$getPorductDataList->houdin_products_id,'inventoryId'=>0,'productImage'=>$setProductImage,'price'=>$getOriginalPrice,'discountPrice'=>$getDiscountPrice,'productName'=>$getPorductDataList->houdin_products_title,'stock'=>'yes','quotation'=>$getPorductDataList->houdinv_products_main_quotation);
                    }
                    else
                    {
                        $setProductArray[] = array('productId'=>$getPorductDataList->houdin_products_id,'inventoryId'=>0,'productImage'=>$setProductImage,'price'=>$getOriginalPrice,'discountPrice'=>$getDiscountPrice,'productName'=>$getPorductDataList->houdin_products_title,'stock'=>'no','quotation'=>$getPorductDataList->houdinv_products_main_quotation);
                    }
                }
                else
                {
                    if($getQuantity > 0)
                    {
                        $setProductArray[] = array('productId'=>$getPorductDataList->houdin_products_id,'inventoryId'=>0,'productImage'=>$setProductImage,'price'=>$getOriginalPrice,'discountPrice'=>$getDiscountPrice,'productName'=>$getPorductDataList->houdin_products_title,'stock'=>'yes','quotation'=>$getPorductDataList->houdinv_products_main_quotation);
                    }
                }
            }
            if(count($setProductArray) > 0)
            {
                return array('message'=>'Success','data'=>$setProductArray,'nextLimit'=>$getNextLimit);
            }
            else
            {
                return array('message'=>'No data');
            }
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function fetchProductListViewModelApp($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopname']);
        $getDynamicDB = $this->load->database($getAppDB,true);
        if($data['productId'] != 0)
        {
            $getProductData = $getDynamicDB->select('*')->from('houdinv_products')->where('houdin_products_id',$data['productId'])->where('houdin_products_status','1')->get()->result();
            if(count($getProductData) > 0)
            {
                // get rating and total vote
                $getProductRating = $getDynamicDB->select('COUNT(houdinv_product_review_id) AS totalReview, SUM(houdinv_product_review_rating) AS ratingSum')->from('houdinv_product_reviews')->where('houdinv_product_review_product_id',$data['productId'])->get()->result();
                if($getProductRating[0]->totalReview)
                {
                    $setTotalVotes = $getProductRating[0]->totalReview;
                    $getRating = $getProductRating[0]->ratingSum/$setTotalVotes;
                    $finalRating = number_format((float)$getRating, 2, '.', '');
                }
                else
                {
                    $setTotalVotes = 0;
                    $finalRating = 0;
                }
                // get cart and wishlist status
                $getCartStatus = $getDynamicDB->select('houdinv_users_cart_id')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$data['userId'])->where('houdinv_users_cart_item_id',$data['productId'])->get()->result();
                if(count($getCartStatus) > 0)
                {
                    $setCartStatus = 'yes';
                }
                else
                {
                    $setCartStatus = 'no';
                }
                $getWishlistStatus = $getDynamicDB->select('houdinv_users_whishlist_id')->from('houdinv_users_whishlist')->where('houdinv_users_whishlist_user_id',$data['userId'])->where('houdinv_users_whishlist_item_id',$data['productId'])->get()->result();
                if(count($getWishlistStatus) > 0)
                {
                    $setWishlistStatus = 'yes';
                }
                else
                {
                    $setWishlistStatus = 'no';
                }
                // get Images Data
                $getImages = json_decode($getProductData[0]->houdinv_products_main_images,true);
                if(count($getImages) > 0)
                {
                    for($image = 0; $image < count($getImages); $image++)
                    {
                        if($getImages[0] != "")
                        {
                            if($getImages[$image] != "")
                            {
                                $setImageArray[] = $this->session->userdata('vendorURL')."upload/productImage/".$getImages[$image];
                            }
                        }
                        else
                        {
                            $getShop = shopurl();
                            $setImageArray = array();
                            $setImageArray[] = "".$getShop."Extra/noimage.jpg";
                        }
                    }
                }
                else
                {
                    $getShop = shopurl();
                    $setImageArray[] = "".$getShop."Extra/noimage.jpg";
                }
                // set price
                $getPrice = json_decode($getProductData[0]->houdin_products_price,true);
                if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
                {
                    $getOriginalPrice = $getPrice['sale_price'];
                    $getDiscountPrice = $getProductData[0]->houdin_products_final_price;
                }
                else
                {
                    $getOriginalPrice = $getProductData[0]->houdin_products_final_price;
                    $getDiscountPrice = 0;
                }
                // fetch cart button setting
                $getoutofStock = $getDynamicDB->select('receieve_order_out')->from('houdinv_storesetting')->where('id',1)->get()->result();
                if($getoutofStock == 0)
                {
                    if($getProductData[0]->houdinv_products_total_stocks > 0)
                    {
                        $setCart = 'yes';
                    }
                    else
                    {
                        $setCart = 'no';
                    }
                }
                else
                {
                    $setCart = 'yes';
                }
                $shortDescription = trim(preg_replace('/\s\s+/', ' ', $getProductData[0]->houdin_products_short_desc));
                $shortDescription = str_replace("\n","",$shortDescription);
                $mainDescription = trim(preg_replace('/\s\s+/', ' ', $getProductData[0]->houdin_products_desc));
                $mainDescription = str_replace("\n","",$mainDescription);
                $setProductArray = array('productName'=>$getProductData[0]->houdin_products_title,'shortDescription'=>strip_tags($shortDescription),
                'productImage'=>$setImageArray,'productPrice'=>$getOriginalPrice,'productDiscountPrice'=>$getDiscountPrice,'productSKU'=>$getProductData[0]->houdin_products_main_sku,
                'minorder'=>$getProductData[0]->houdinv_products_main_minimum_order,'cartShow'=>$setCart,'description'=>strip_tags($mainDescription),
                'totalVotes'=>$setTotalVotes,'finalRating'=>$finalRating,'userCartStatus'=>$setCartStatus,'userWishlistStatus'=>$setWishlistStatus
                );
                return array('messgae'=>'Success','data'=>$setProductArray);
            }
            else
            {
                return array('message'=>'No data');
            }
        }
        else
        {
            $getMainVariantData = $getDynamicDB->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data['productVariantId'])->get()->result();
            // get main product data
            $getVariantProduct = $getDynamicDB->select('*')->from('houdinv_products')->where('houdin_products_id',$getMainVariantData[0]->houdin_products_variants_product_id)->get()->result();
            // get variant image
            $setImageArray[] = $this->session->userdata('vendorURL')."upload/productImage/".$getMainVariantData[0]->houdin_products_variants_image;
            // get rating and total vote
            $getProductRating = $getDynamicDB->select('COUNT(houdinv_product_review_id) AS totalReview, SUM(houdinv_product_review_rating) AS ratingSum')->from('houdinv_product_reviews')->where('houdinv_product_reviews_variant_id',$data['productVariantId'])->get()->result();
            if($getProductRating[0]->totalReview)
            {
                $setTotalVotes = $getProductRating[0]->totalReview;
                $getRating = $getProductRating[0]->ratingSum/$setTotalVotes;
                $finalRating = number_format((float)$getRating, 2, '.', '');
            }
            else
            {
                $setTotalVotes = 0;
                $finalRating = 0;
            } 
             // get cart and wishlist status
             $getCartStatus = $getDynamicDB->select('houdinv_users_cart_id')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$data['userId'])->where('houdinv_users_cart_item_variant_id',$data['productVariantId'])->get()->result();
             if(count($getCartStatus) > 0)
             {
                 $setCartStatus = 'yes';
             }
             else
             {
                 $setCartStatus = 'no';
             }
             $getWishlistStatus = $getDynamicDB->select('houdinv_users_whishlist_id')->from('houdinv_users_whishlist')->where('houdinv_users_whishlist_user_id',$data['userId'])->where('houdinv_users_whishlist_item_variant_id',$data['productVariantId'])->get()->result();
             if(count($getWishlistStatus) > 0)
             {
                 $setWishlistStatus = 'yes';
             }
             else
             {
                 $setWishlistStatus = 'no';
             }
            // set price
             $getPrice = json_decode($getVariantProduct[0]->houdin_products_price,true);
             if($getPrice['discount'] != 0 && $getPrice['discount'] != "")
             {
                 $getOriginalPrice = $getPrice['sale_price'];
                 $getDiscountPrice = $getMainVariantData[0]->houdinv_products_variants_final_price;
             }
             else
             {
                 $getOriginalPrice = $getMainVariantData[0]->houdinv_products_variants_final_price;
                 $getDiscountPrice = 0;
             }
              // fetch cart button setting
              $getoutofStock = $getDynamicDB->select('receieve_order_out')->from('houdinv_storesetting')->where('id',1)->get()->result();
              if($getoutofStock == 0)
              {
                  if($getProductData[0]->houdinv_products_total_stocks > 0)
                  {
                      $setCart = 'yes';
                  }
                  else
                  {
                      $setCart = 'no';
                  }
              }
              else
              {
                  $setCart = 'yes';
              }
              $shortDescription = trim(preg_replace('/\s\s+/', ' ', $getVariantProduct[0]->houdin_products_short_desc));
              $shortDescription = str_replace("\n","",$shortDescription);
            $mainDescription = trim(preg_replace('/\s\s+/', ' ', $getVariantProduct[0]->houdin_products_desc));
            $mainDescription = str_replace("\n","",$mainDescription);
              $setProductArray = array('productName'=>$getMainVariantData[0]->houdin_products_variants_title,
              'shortDescription'=>strip_tags($shortDescription),
              'productImage'=>$setImageArray,'productPrice'=>$getOriginalPrice,'productDiscountPrice'=>$getDiscountPrice,
              'productSKU'=>$getMainVariantData[0]->houdin_products_variants_sku,
              'minorder'=>$getVariantProduct[0]->houdinv_products_main_minimum_order,
              'cartShow'=>$setCart,'description'=>strip_tags($mainDescription),'totalVotes'=>$setTotalVotes,'finalRating'=>$finalRating,
              'userCartStatus'=>$setCartStatus,'userWishlistStatus'=>$setWishlistStatus
              );
              return array('messgae'=>'Success','data'=>$setProductArray);
        }
    }
    public function fetchRelatedProducts($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);
        // Get product data
        $getMainProductData = $getDynamicDB->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$data['productId'])->get()->result();
        $getPriceData = json_decode($getMainProductData,true);
        // get variant data
        $getRelatedProductData = $getDynamicDB->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_product_id',$data['productId'])->get()->result();
        if(count($getRelatedProductData) > 0)
        {
            foreach($getRelatedProductData as $getRelatedProductDataList)
            {
                $getPricedata = json_decode($getRelatedProductDataList->houdinv_products_variants_new_price,true);
                //$getProduct price data
                if($getPriceData['discount'] != "" && $getPriceData['discount'] != 0)
                {
                    $getOriginalPrice = $getPricedata['sale_price'];
                    $getDiscountPrice = $getRelatedProductDataList->houdinv_products_variants_final_price;
                }
                else
                {
                    $getOriginalPrice = $getRelatedProductDataList->houdinv_products_variants_final_price;
                    $getDiscountPrice = 0;
                }
                // set image
                if($getRelatedProductDataList->houdin_products_variants_image)
                {
                    $setImageData = $this->session->userdata('vendorURL')."upload/productImage/".$getRelatedProductDataList->houdin_products_variants_image;
                }
                else
                {
                    $setImageData = "";
                }
                $setRelatedProductArray[] = array('productId'=>0,'variantyId'=>$getRelatedProductDataList->houdin_products_variants_id,
                'productName'=>$getRelatedProductDataList->houdin_products_variants_title,'price'=>$getOriginalPrice,'discountedPrice'=>$getDiscountPrice,'image'=>$setImageData);
            }
            if(count($setRelatedProductArray) > 0)
            {
                return array('message'=>'Success','data'=>$setRelatedProductArray);
            }
            else
            {
                return array('message'=>'No data');
            }
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function checkProductQuantity($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        if($data['productId'] != 0)
        {
            $getProductList = $getDynamicDB->select('houdinv_products_total_stocks,houdin_products_title')->from('houdinv_products')->where('houdin_products_id',$data['productId'])->get()->result();
            if(count($getProductList) > 0)
            {
                return array('message'=>'Success','quantity'=>$getProductList[0]->houdinv_products_total_stocks,'title'=>$getProductList[0]->houdin_products_title);
            }
            else
            {
                return array('message'=>'Something went wrong. Please try again');
            }
        }
        else
        {
            $getVariantList = $getDynamicDB->select('houdinv_products_variants_total_stocks,houdin_products_variants_title')->from('houdinv_products_variants')->where('houdin_products_variants_id',$data['variantId'])->get()->result();
            if(count($getVariantList) > 0)
            {
                return array('message'=>'Success','quantity'=>$getVariantList[0]->houdinv_products_variants_total_stocks,'title'=>$getVariantList[0]->houdin_products_variants_title);
            }
            else
            {
                return array('message'=>'Something went wrong. Please try again');
            }
        }
    }
    public function fethcCartSetting()
    {
        $getCartSetting = $this->getDynamicDB->select('display_out_stock,receieve_order_out')->from('houdinv_storesetting')->where('id','1')->get()->result();
        return $getCartSetting;
    }
}

?>
