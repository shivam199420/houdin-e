<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Webserviceregister extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopusermodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function index()
    {
        $getPassword = $this->input->post('userPass');
        $getConfirmPass = $this->input->post('userRetypePass');
        if($getPassword == $getConfirmPass)
        {
            $this->form_validation->set_rules('userFirstName','First Name','required');
            $this->form_validation->set_rules('userLastName','Last Name','required');
            $this->form_validation->set_rules('userEmail','Email','required|valid_email');
            $this->form_validation->set_rules('userContact','Contact Number','required');
            $this->form_validation->set_rules('userPass','Password','required');
            $this->form_validation->set_rules('userRetypePass','Confirm Password','required');
            $this->form_validation->set_rules('shopName','text','required');
            if($this->form_validation->run() == true)
            {
                $letters1='abcdefghijklmnopqrstuvwxyz'; 
                $string1=''; 
                for($x=0; $x<3; ++$x)
                {  
                    $string1.=$letters1[rand(0,25)].rand(0,9); 
                }
                $saltdata = password_hash($string1,PASSWORD_DEFAULT);
                $pass = crypt($getPassword,$saltdata);
                $setUserNameData = $this->input->post('userFirstName')." ".$this->input->post('userLastName');
                $setData = strtotime(date('Y-m-d H:i:s'));
                $setInsertArray = array('houdinv_user_name'=>$setUserNameData,'houdinv_user_email'=>$this->input->post('userEmail'),'houdinv_user_password'=>$pass,'houdin_user_password_salt'=>$saltdata,'houdinv_user_contact'=>$this->input->post('userContact'),'houdinv_user_is_verified'=>'1','houdinv_user_is_active'=>'active','houdinv_user_created_at'=>$setData);
                $extraArray = array('shopName'=>$this->input->post('shopName'));
                $getUserRegistrationStatus = $this->Shopusermodel->addUserDataApp($setInsertArray,$extraArray);
                if($getUserRegistrationStatus['message'] == 'yes')
                {
                    $resultArray = array('message'=>'You are successfully register with us');
                    print_r(json_encode($resultArray));  
                }
                else if($getUserRegistrationStatus['message'] == 'email')
                {
                    $resultArray = array('message'=>'Email address is already exist');
                    print_r(json_encode($resultArray));  
                }
                else
                {
                    $resultArray = array('message'=>'Something went wrong. Please try again');
                    print_r(json_encode($resultArray));  
                }
                 
            }
            else
            {
                $resultArray = array('message'=>'All fields are mandatory');
                print_r(json_encode($resultArray));  
            }
        }
        else
        {
            $resultArray = array('message'=>'Passwords are not matched');
            print_r(json_encode($resultArray));
        }
    }
    public function checkUserAuth()
    {
        $this->form_validation->set_rules('userPass','Password','required');
        $this->form_validation->set_rules('userEmail','Email','required|valid_email');
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $setAuthArray = array('email'=>$this->input->post('userEmail'),'password'=>$this->input->post('userPass'),'shopName'=>$this->input->post('shopName'),'deviceId'=>$this->input->post('deviceId'));
            $getAuthData = $this->Shopusermodel->checkUserAuthApp($setAuthArray);
            print_r(json_encode($getAuthData));  
        }
        else
        {
            $resultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultArray));  
        }
    }
    public function checkUserForgot()
    {
        $this->form_validation->set_rules('emailForgot','Email','required');
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $getForgotStatus = $this->Shopusermodel->forgotPassApp($this->input->post('emailForgot'),$this->input->post('shopName'));
            if($getForgotStatus['message'] == 'no')
            {
                $resultArray = array('message'=>'Something went wrong. Please try again');
                print_r(json_encode($resultArray));
            }
            else if($getForgotStatus['message'] == 'credit')
            {
                $resultArray = array('message'=>'Something went wrong. Please contact to support');
                print_r(json_encode($resultArray));
            }
            else if($getForgotStatus['message'] == 'email')
            {
                $resultArray = array('message'=>'Please enter correct email address');
                print_r(json_encode($resultArray));
            }
            else if($getForgotStatus['message'] == 'yes')
            {
                // send email to admin user
                $url = 'https://api.sendgrid.com/';
                $user = 'anuankita';
                $pass = 'india@123';
                $json_string = array(
                'to' => array(
                    $this->input->post('emailForgot')
                ),
                'category' => 'test_category'
                );
                $htm = '
                <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                <img src="'.base_url().'assets/images/main-logo.png" style="max-width: 53%;width:100%"/></td></tr>
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Your pin is:</strong></td></tr>
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$getForgotStatus['token'].'</td></tr>
                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                <table width="100%" ><tr >
                <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e 2018--All right reserverd </td></tr>
                <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
                </tr></table>
                </div>
                </div></td></tr></table>';
                $params = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'x-smtpapi' => json_encode($json_string),
                    'to'        => $this->input->post('emailForgot'),
                    'fromname'  => 'Houdin-e',
                    'subject'   => 'Recover Your Passoword',
                    'html'      => $htm,
                    'from'      => 'hawkscodeteam@gmail.com',
                );
                $request =  $url.'api/mail.send.json';
                $session = curl_init($request);
                curl_setopt ($session, CURLOPT_POST, true);
                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                curl_setopt($session, CURLOPT_HEADER, false);
                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($session);
                curl_close($session);
                $getResponse = json_decode($response);
                if($getResponse->message == 'success')
                {
                    $resultArray = array('messgae'=>'success','token'=>$getForgotStatus['token']);
                    print_r(json_encode($resultArray));
                }
                else
                {
                    $resultArray = array('message'=>'Something went wrong. Please try again');
                    print_r(json_encode($resultArray));
                }
            }
            else
            {
                $resultArray = array('message'=>'Something went wrong. Please try again');
                print_r(json_encode($resultArray));
            }
        }
        else
        {
            $resultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
    public function updateforgot()
    {
        $getPass = $this->input->post('pass');
        $getConfirmPass = $this->input->post('confirmpass');
        if($getPass == $getConfirmPass)
        {
            $this->form_validation->set_rules('pin','text','required|exact_length[4]');
            $this->form_validation->set_rules('pass','text','required');
            $this->form_validation->set_rules('confirmpass','text','required');
            $this->form_validation->set_rules('shopName','text','required');
            if($this->form_validation->run() == true)
            {
                $setForgotArray = array('pin'=>$this->input->post('pin'),'pass'=>$getPass,'shopName'=>$this->input->post('shopName'));
                $getForgotStatus = $this->Shopusermodel->updateForgotApp($setForgotArray);
                print_r(json_encode($getForgotStatus));
            }
            else
            {
                $resultArray = array('message'=>'All fields are mandatory');
                print_r(json_encode($resultArray));
            }
        }
        else
        {
            $resultArray = array('message'=>'Passwords are not matched');
            print_r(json_encode($resultArray));
        }
    }
    public function changePass()
    {
        $this->form_validation->set_rules('currentPass','text','required');
        $this->form_validation->set_rules('newpass','text','required');
        $this->form_validation->set_rules('newconfirmpass','text','required');
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('userId','text','required');
        if($this->form_validation->run() == true)
        {
            $getNewPass = $this->input->post('newpass');
            $getNewConfirmPass = $this->input->post('newconfirmpass');
            if($getNewPass == $getNewConfirmPass)
            {
                $setArray = array('oldpass'=>$this->input->post('currentPass'),'newpass'=>$getNewPass,'shopName'=>$this->input->post('shopName'),'id'=>$this->input->post('userId'));
                $getPassData = $this->Shopusermodel->updatePasswordApp($setArray);
                print_r(json_encode($getPassData));    
            }
            else
            {
                $resultArray = array('message'=>'Password are not matched');
                print_r(json_encode($resultArray));
            }
        }
        else
        {
            $resultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
}
