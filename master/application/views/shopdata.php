        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">
    <!-- Page-Title -->
        <div class="row">
        <div class="col-sm-12 m-b-15">
        <div class="btn-group pull-right m-t-15">


        </div>
        <h4 class="page-title">Shop Management</h4>

        </div>
        </div>



        <div class="row">
         <div class="col-md-6 col-lg-4">
              <div class="widget-panel widget-style-2 bg-light">
                  <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Shop.png">
                  <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($totalShops) { echo $totalShops; } else { echo 0; }  ?></b></h2>
                  <div class="text-muted m-t-5 custom_width_sm">Total Shop</div>
                  <div class="clearfix"></div>
              </div>
        </div>
        <div class="col-md-6 col-lg-4">
              <div class="widget-panel widget-style-2 bg-light">
                  <img src="<?php echo base_url(); ?>assets/images/allpageicon/Active_Shop.png">
                  <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($totalActiveShops) { echo $totalActiveShops; } else { echo 0; }  ?></b></h2>
                  <div class="text-muted m-t-5 custom_width_sm">Total Active Shop</div>
                  <div class="clearfix"></div>
              </div>
        </div>

        <div class="col-md-6 col-lg-4">
              <div class="widget-panel widget-style-2 bg-light">
                  <img src="<?php echo base_url(); ?>assets/images/allpageicon/Deactive_Shop.png">
                  <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($totalDeactiveShops) { echo $totalDeactiveShops; } else { echo 0; }  ?></b></h2>
                  <div class="text-muted m-t-5 custom_width_sm">Total Deactive Shop</div>
                  <div class="clearfix"></div>
              </div>
        </div>

        </div>
        <!-- col -->

        <div class="row">
        <div class="col-sm-12">
        <div class="card-box">
        <h4 class="m-t-0 header-title text-center" style="font-size: 22px;margin-bottom: 29px;    color: #f9a02d!important;"><b>    Advanced Search Box</b></h4>
        <?php echo form_open(base_url( 'Shop' ), array( 'method' => 'post'));?>
        <div class="row">
        <div class="col-md-6 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 control-label">Shop Name</label>
        <div class="col-md-10">
        <input type="text" value="" class="form-control name_validation" name="searchShopName" placeholder="Shop Name">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Email</label>
        <div class="col-md-10">
        <input type="text" value="" class="form-control name_validation email_validation" name="searchShopEmail" placeholder="Email">
        </div>
        </div>
        </div>
        <div class="col-md-6 form-horizontal">
        <div class="form-group">
        <label class="col-md-2 control-label">Phone No</label>
        <div class="col-md-10">
        <input type="text" class="form-control name_validation" name="searchShopPhone" placeholder="Phone No"/>
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-2 control-label">Status</label>
        <div class="col-md-10">
        <select class="form-control" name="searchShopStatus">
        <option value="">Select Status</option>
        <option value="active">Active</option>
        <option value="deactive">Deactive</option>
        <option value="block">Block</option>
        </select>
        </div>
        </div>
        </div>
        <div class="col-md-12 ">
        <input type="submit" class="btn btn-default pull-right m-r-10" name="shopSearchButton" value="Submit">
        <button type="reset" class="btn btn-info pull-right m-r-10">Reset</button>
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('error'))
        {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        else if($this->session->flashdata('success'))
        {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="card-box table-responsive">
         <div class="btn-group pull-right m-t-10 m-b-20">
         <a href="<?php echo base_url() ?>Shop/add" class="btn btn-default m-r-5" title="Add Shop"><i class="fa fa-plus"></i></a>
                                     


                                  </div>

        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th>Shop Name</th>
        <th>Shop Owner</th>
        <th>Shop Owner Email</th>
        <th>Shop Package</th>
        <th>Shop Status</th>
        <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        if($this->session->flashdata('shopSearch'))
        {
            $shopListValue = $this->session->flashdata('shopSearch');
            foreach($shopListValue as $shopListData)
            {
            ?>   
            <tr>
            <td><?php echo $shopListData->houdin_user_shop_shop_name ?></td>
            <td><?php echo $shopListData->houdin_user_name ?></td>
            <td><?php echo $shopListData->houdin_user_email ?></td>
            <td><?php 
            $future = strtotime($shopListData->houdin_users_package_expiry); //Future date.
            $timefromdb = strtotime(date('Y-m-d')); //source time
            $timeleft = $future-$timefromdb;
            $daysleft = round((($timeleft/24)/60)/60); 
            if($daysleft > 0)
            {
                $daysleft = $daysleft;
                $setColor = 'green';
                $setText = 'In process';
            }
            else
            {
                $daysleft = 0;
                $setColor = 'red';
                $setText = 'Expired';
            }
            if($shopListData->houdin_users_package_id == 0) { echo 'Trial'; } else { echo $shopListData->houdin_package_name; } ?><br/>Expiry Date:<?php echo date('d-m-Y',strtotime($shopListData->houdin_users_package_expiry))."(".$daysleft." days left)<br/>
            <label style='color:".$setColor."'>".$setText."</label>"; ?><br/><label style='color:<?php echo $setAccountColor; ?>!important'>Account Type:<?php echo $setAccountText ?></label>
            
            </td>
            <td>
            <?php 
            if($shopListData->houdin_user_shop_active_status == 'active')
            {
            $setTextData = 'Active';
            $setClassdata = 'success';
            }
            else if($shopListData->houdin_user_shop_active_status == 'deactive')
            {
            $setTextData = 'Deactive';
            $setClassdata = 'warning';
            }
            else if($shopListData->houdin_user_shop_active_status == 'block')
            {
            $setTextData = 'Block';
            $setClassdata = 'danger';
            }
            ?>
            <button type="button" class="btn btn-<?php echo $setClassdata ?> btn btn-xs"><?php echo $setTextData ?></button></td>
            <td>
            <a href="<?php echo base_url() ?>Shop/view/<?php echo $shopListData->houdin_user_shop_id ?>" class="btn btn-warning btn btn-sm"><i class="fa fa-eye-slash m-r-5"></i>View</a>
            <!-- <button type="button" data-id="<?php //echo $shopListData->houdin_user_shop_id ?>" class="btn btn-warning btn btn-sm deleteShopBtn"><i class="fa fa-trash m-r-5"></i>Delete</button> -->
            <button type="button" data-status="<?php echo $shopListData->houdin_user_shop_active_status; ?>" data-id="<?php echo $shopListData->houdin_user_shop_id ?>" class="btn btn-warning btn btn-sm changeShopStatusBtn"><i class="fa fa-toggle-on m-r-5"></i>Change Status</button>
            <button type="button" data-email="<?php echo $shopListData->houdin_user_email ?>" class="btn btn-warning btn btn-sm sendShopUserEmailBtn"><i class="fa fa-envelope-open m-r-5"></i>Send Email</button>
            <button type="button" class="btn btn-warning btn btn-sm upgradePackageData" data-price="<?php  if($shopListData->houdin_package_price) { echo $shopListData->houdin_package_price; } else { echo 0; } ?>" data-id="<?php echo $shopListData->houdin_user_shop_user_id ?>" data-expiry="<?php echo $shopListData->houdin_users_package_expiry ?>" data-package="<?php echo $shopListData->houdin_users_package_id ?>">Upgrade Package</button>
            <button type="button" data-id="<?php echo $shopListData->houdin_user_shop_user_id ?>" data-email="<?php echo $shopListData->houdin_user_email ?>" class="btn btn-warning btn btn-sm editShopLoginDetail"><i class="fa fa-toggle-on m-r-5"></i>Edit Shop Detail</button>
            <button type="button" data-id="<?php echo $shopListData->houdin_user_shop_id ?>" class="btn btn-danger btn btn-sm ResetAccountdatabase"><i class="fa fa-refresh m-r-5"></i>Reset Account</button>
            
        
        </td>
            </tr> 
        <?php }
        }
        else
        {
            foreach($shopList as $shopListData)
            {
            ?>   
            <tr>
            <td><?php echo $shopListData->houdin_user_shop_shop_name ?></td>
            <td><?php echo $shopListData->houdin_user_name ?></td>
            <td><?php echo $shopListData->houdin_user_email ?></td>
            <td><?php 
            $future = strtotime($shopListData->houdin_users_package_expiry); //Future date.
            $timefromdb = strtotime(date('Y-m-d')); //source time
            $timeleft = $future-$timefromdb;
            $daysleft = round((($timeleft/24)/60)/60); 
            if($daysleft > 0)
            {
                $daysleft = $daysleft;
                $setColor = 'green';
                $setText = 'In process';
            }
            else
            {
                $daysleft = 0;
                $setColor = 'red';
                $setText = 'Expired';
            }
            if($shopListData->houdin_user_account_type == 'testing')
            {
                $setAccountColor = 'red';
                $setAccountText = 'Testing';
            }
            else
            {
                $setAccountColor = 'green';
                $setAccountText = 'Live';
            }
            if($shopListData->houdin_users_package_id == 0) { echo 'Trial'; } else { echo $shopListData->houdin_package_name; } ?><br/>Expiry Date:<?php echo $shopListData->houdin_users_package_expiry."(".$daysleft." days left)<br/>
            <label style='color:".$setColor."!important'>".$setText."</label>"; ?><br/>
            <label style='color:<?php echo $setAccountColor; ?>!important'>Account Type:<?php echo $setAccountText ?></label>
            </td>
            <td>
            <?php 
            if($shopListData->houdin_user_shop_active_status == 'active')
            {
            $setTextData = 'Active';
            $setClassdata = 'success';
            }
            else if($shopListData->houdin_user_shop_active_status == 'deactive')
            {
            $setTextData = 'Deactive';
            $setClassdata = 'warning';
            }
            else if($shopListData->houdin_user_shop_active_status == 'block')
            {
            $setTextData = 'Block';
            $setClassdata = 'danger';
            }
            ?>
            <button type="button" class="btn btn-<?php echo $setClassdata ?> btn btn-xs"><?php echo $setTextData ?></button></td>
            <td>
            <a href="<?php echo base_url() ?>Shop/view/<?php echo $shopListData->houdin_user_shop_id ?>" class="btn btn-warning btn btn-sm"><i class="fa fa-eye-slash m-r-5"></i>View</a>
            
            <!-- <button type="button" data-id="<?php //echo $shopListData->houdin_user_shop_id ?>" class="btn btn-warning btn btn-sm deleteShopBtn"><i class="fa fa-trash m-r-5"></i>Delete</button> -->
            <button type="button" data-status="<?php echo $shopListData->houdin_user_shop_active_status; ?>" data-id="<?php echo $shopListData->houdin_user_shop_id ?>" class="btn btn-warning btn btn-sm changeShopStatusBtn"><i class="fa fa-toggle-on m-r-5"></i>Change Status</button>
            <button type="button" data-email="<?php echo $shopListData->houdin_user_email ?>" class="btn btn-warning btn btn-sm sendShopUserEmailBtn"><i class="fa fa-envelope-open m-r-5"></i>Send Email</button>
            <button type="button" class="btn btn-warning btn btn-sm upgradePackageData" data-price="<?php if($shopListData->houdin_package_price) { echo $shopListData->houdin_package_price; } else { echo 0; } ?>" data-id="<?php echo $shopListData->houdin_user_shop_user_id ?>" data-expiry="<?php echo $shopListData->houdin_users_package_expiry ?>" data-package="<?php echo $shopListData->houdin_users_package_id ?>">Upgrade Package</button>

             <button type="button" data-id="<?php echo $shopListData->houdin_user_shop_user_id ?>" data-email="<?php echo $shopListData->houdin_user_email ?>" class="btn btn-warning btn btn-sm editShopLoginDetail"><i class="fa fa-pencil m-r-5"></i>Edit Shop Detail</button>

             <button type="button" data-status="<?php echo $shopListData->houdin_user_shop_active_status; ?>" data-id="<?php echo $shopListData->houdin_user_shop_id ?>" class="btn btn-danger btn btn-sm ResetAccountdatabase"><i class="fa fa-refresh m-r-5"></i>Reset Account</button>
            </td>
            </tr> 
        <?php  } }
            ?>   
        
        </tbody>
        </table>
        <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
        </div>
        </div>
        </div>


        </div> <!-- container -->

        </div> <!-- content -->
        <div id="deleteShopModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >

        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Shop' ), array( 'method' => 'post'));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

        <h4 class="modal-title">Delete Entry</h4>

        </div>

        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <input type="hidden" class="deleteShopId" name="deleteShopId"/>
        <h4><b>Do you really want to delete this entry ?</b></h4>
        </div>
        </div>
        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info" name="deleteShopData" value="Delete">

        </div>

        </div>
        <?php echo form_close(); ?>
        </div>

        </div>


        <div id="changeShopStatusModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Shop' ), array( 'method' => 'post','id'=>'changeStatuform'));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

        <h4 class="modal-title">Change Status</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">

        <div class="form-group no-margin">
        <input type="hidden" class="chnageShopStatusId" name="chnageShopStatusId"/>
        <label for="field-7" class="control-label">Change Status</label>
        <select class="form-control required_validation_for_change_status changeShopStatus" name="changeShopStatus">
        <option value="">Choose Status</option>
        <option value="active">Active</option>
        <option value="deactive">Deactive</option>
        <option value="block">Block</option>
        </select>                                                                                                                
        </div>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="updateShopStatus" value="Update Status">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>


<!-- Start resert account module --> 

<div id="ResetAccountdatabaseModule" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

<div class="modal-dialog">
<?php echo form_open(base_url( 'Shop/resetaccount' ), array( 'method' => 'post','id'=>'resetaccount'));?>
<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

<h4 class="modal-title">Reset Account </h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-12">

<div class="form-group no-margin">
<input type="hidden" class="resetaccountId" id="resetaccountId" name="resetaccountId"/>
<label for="field-7" class="control-label"> Are you sure you want to reset this Account</label>
                                                                                                                
</div>
</div>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
<input type="submit" class="btn btn-info" name="ResetAccount" value="Reset Now">
</div>
</div>
<?php echo form_close(); ?>
</div>
</div>

<!-- end Restart Moduele -->









         <div id="editShopModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Shop/updateshopcredentials' ), array( 'method' => 'post','id'=>'updateLoginCred'));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

        <h4 class="modal-title">Edit Shop</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">

          <div class="form-group no-margin">
         <label for="field-7" class="control-label">User Email</label>
        <input type="text" class="form-control required_validation_for_change_status name_validation email_validation setuemail" autocomplete="off" name="uemail"/>
                                                                                                               
        </div>
        <input type="hidden" class="setuiddata" name="uiddata"/>
          <div class="form-group no-margin">
         <label for="field-7" class="control-label">User Password</label>
        <input type="password" class="form-control name_validation" autocomplete="off" name="upass"/>
                           
        </div>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="" value="Update">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>



        <div id="sendShopUserEmail" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Shop' ), array( 'method' => 'post','id'=>'sendShopEmail'));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

        <h4 class="modal-title">Send Email</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">

        <div class="form-group no-margin">
            <input type="hidden" class="shopUserEmail" name="shopUserEmail"/>
        <label for="field-7" class="control-label">Subject</label>
        <input type="text" class="form-control required_validation_for_send_email name_validation" name="subject" placeholder="Subject" />

        </div>

        </div>
        </div>
        <div class="row">
        <div class="col-md-12">

        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Message</label>
        <textarea class="form-control required_validation_for_send_email name_validation" name="userMessage" rows="6"></textarea>

        </div>

        </div>
        </div>
        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info" name="sendShopEmail" value="Send Email">

        </div>

        </div>
        <?php echo form_close(); ?>
        </div>

        </div>

        <div id="upgradePackageModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <?php echo form_open(base_url( 'Shop' ), array( 'method' => 'post','id'=>'upgradePackageForm'));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

        <h4 class="modal-title">Upgrade Package</h4>
        </div>
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">

        <div class="form-group no-margin">
        <input type="hidden" class="upgardePackageId" name="upgardePackageId"/>
        <input type="hidden" class="packagePrice" name="packagePrice"/>
        <label for="field-7" class="control-label">Choose Package</label>
        <select class="form-control choosePackage" name="choosePackage">
        <option value="">Choose Package</option>
        <option data-price="0" value="0">Trial</option>
        <?php 
        foreach($packageData as $packageDataList)
        {
        ?>
        <option data-price="<?php echo $packageDataList->houdin_package_price ?>" value="<?php echo $packageDataList->houdin_package_id ?>"><?php echo $packageDataList->houdin_package_name ?></option>
        <?php }
        ?>
        </select>                                                                                                                
        </div>

        <div class="form-group no-margin">
        <label for="field-7" class="control-label">Expiry Date</label>
        <input type="text" class="form-control date_picker expiryDate" name="expiryDate"/>                                                                                                              
        </div>

        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-info" name="updateShopPackage" value="Update Package">
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        <?php $this->load->view('Template/footer.php') ?>
        <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('submit','#changeStatuform,#upgradePackageForm,#updateLoginCred',function(){
            var check_required_field='';
            $(this).find(".required_validation_for_change_status").each(function(){
                var val22 = $(this).val();
                if (!val22){
                    check_required_field =$(this).size();
                    $(this).css("border-color","#ccc");
                    $(this).css("border-color","red");
                }
                $(this).on('keypress change',function(){
                    $(this).css("border-color","#ccc");
                });
            });
            if(check_required_field)
            {
                return false;
            }
            else {
                return true;
            }
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('submit','#sendShopEmail',function(){
            var check_required_field='';
            $(".required_validation_for_send_email").each(function(){
                var val22 = $(this).val();
                if (!val22){
                    check_required_field =$(this).size();
                    $(this).css("border-color","#ccc");
                    $(this).css("border-color","red");
                }
                $(this).on('keypress change',function(){
                    $(this).css("border-color","#ccc");
                });
            });
            if(check_required_field)
            {
                return false;
            }
            else {
                return true;
            }
        });
        $(document).on('click','.upgradePackageData',function(){
            $('.upgardePackageId').val($(this).attr('data-id'));
            $('.choosePackage').val($(this).attr('data-package'));
            $('.expiryDate').val($(this).attr('data-expiry'));
            $('.packagePrice').val($(this).attr('data-price'));
            $('#upgradePackageModal').modal('show');
        });
        $(document).on('change','.choosePackage',function(){
            var getPrice = $(this).children('option:selected').attr('data-price');
            $('.packagePrice').val(getPrice);
        });

        $(document).on('click','.ResetAccountdatabase',function(){
            $('#resetaccountId').val($(this).attr('data-id'));
             
            $('#ResetAccountdatabaseModule').modal('show');
        });


    });
    </script>
