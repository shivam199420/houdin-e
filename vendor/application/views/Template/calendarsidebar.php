<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
<?php
$getDatabaseData = switchDynamicDatabase();
    $this->db = $this->load->database($getDatabaseData,true);
  if($this->session->userdata('vendorRole')==1)
    {
$sidebar_permission =  $this->db->select("*")->from("houdinv_staff_management_sub")
    ->join("houdinv_staff_management","houdinv_staff_management.staff_id=houdinv_staff_management_sub.staff_id","right outer")
    ->where("houdinv_staff_management.houdinv_staff_auth_token",$this->session->userdata('vendorAuth'))
    ->get()->row();
    }
    else
    {
        $sidebar_permission = '';
    }
    
  //  print_R($sidebar_permission);
?>
        <div id="sidebar-menu">
            <ul>
            <?php if(!$sidebar_permission || ($sidebar_permission->calendar) !=''){ 
        ?>
               <li class="has_sub" >
                    <a href="<?php echo base_url(); ?>Calendar" class="waves-effect"><i class="fa fa-calendar"></i> <span> Calendar </span></a>
                </li>
       <?php  
         }
          ?></ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
