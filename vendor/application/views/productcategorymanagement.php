<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">
           
                <div class="col-md-8">
                   <h4 class="page-title">Catagory Management</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>Category/Categorylanding">Category</a></li>
                  <li class="active">Catagory Management</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                
            </div>
           <!--END Page-Title --> 

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                  <div class="btn-group pull-right m-t-10 "> 
                      <button type="button" class="btn btn-default m-r-5" title="Add Category" data-toggle="modal" data-target="#add_catagory"><i class="fa fa-plus"></i></button>
                      
                  </div>

                </div>
            </div>
<?php
                                    
            if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
   // print_r($all_data);
    ?>
        <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">

              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>

                    <th>Category Name</th>
                    <th style="width: 25%;">Thumbnail</th>
                    <th>Status</th>
                    <th>Action</th>

                  </tr>
                </thead>
                <tbody>
                <?php foreach($category as $val)
                {?>
                  <tr>


                    <td><?php echo $val['main']['houdinv_category_name']; ?></td>
                    <td style="width: 25%;">
                    <?php 
                      if ($val['main']['houdinv_category_thumb'] && $val['main']['houdinv_category_thumb'] != 'Others.png') { ?>
                      <img class="custom_img_detail" src="<?php echo base_url();?>images/category/<?php echo $val['main']['houdinv_category_thumb']; ?>"/> <?php } else { ?> <img class="custom_img_detail" src="<?php echo base_url() ?>images/no.png"/> <?php  }  ?></td>
                    <td><button type="button" class="btn btn-success btn btn-xs"><?php echo $val['main']['houdinv_category_status']; ?></button></td>
                    <td>
                    <?php 
                    if ($val['main']['houdinv_category_thumb'] != 'Others.png') {
                    ?>
                      <button type="button" class="btn btn-info btn-sm m-r-10 edit_popup" data-toggle="modal" data-target="#edit_catagory">Edit</button>
                     <input type="hidden" id="edit_all_data" data-id="<?php echo $val['main']['houdinv_category_id']; ?>" 
                     name="<?php echo $val['main']['houdinv_category_name']; ?>" 
                     data-status="<?php echo $val['main']['houdinv_category_status']; ?>" 
                     data-des="<?php echo $val['main']['houdinv_category_description']; ?>" 
                     data-sub='<?php echo json_encode($val['sub']); ?>'/>
                      <button type="button" class="btn btn-info btn-sm m-r-10 delete_popup" data-toggle="modal" data-target="#delete_catagory">Delete</button>
                    <?php } ?>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--Delete-->

  <div id="delete_catagory" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
      <?php echo form_open(base_url( 'Category/categorymanagement' ),  array( 'method' => 'post', 'id'=>"delete_form" ));?>
 
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Delete Category</h4>

          </div>

          <div class="modal-body">
          <div class="row">
          <div class="col-md-12">
          <h4><b>Do you really want to Delete this category ?</b></h4>
          </div>
          </div>
          </div>

          <div class="modal-footer">
<input type="hidden" id="delete_input" name="delete_input">
          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

          <input type="submit" class="btn btn-info" name="delete" value="Delete">

          </div>

          </div>
           <?php echo form_close();?>
          </div>

          </div>

<!--change Status-->
<div id="edit_catagory" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
<?php echo form_open(base_url( 'Category/categorymanagement' ),  array( 'method' => 'post', 'id'=>"Edit_form","enctype"=>"multipart/form-data" ));?>
 
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Edit Category</h4>

        </div>

        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <label for="field-7" class="control-label">Category Name</label>
        <input type="text" class="form-control required_validation_for_category" name="category_name" id="edit_name" placeholder="Catagory Name" />
        <input type="hidden" name="edit_id" id="edit_id"/>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <label for="field-7" class="control-label">Category Thumbnail</label>
        <input type="file" class="form-control" name="category_thumb"/>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <label for="field-7" class="control-label">Category Description</label>
      <textarea class="form-control required_validation_for_category" rows="4" id="edit_desc" name="category_desc"></textarea>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <label for="field-7" class="control-label">Category Status</label>
        <select class="form-control required_validation_for_category" name="category_status" id="edit_status">
          <option value="">Choose Status</option>
          <option value="active">Active</option>
          <option value="deactive">Dective</option>
          <option value="block">Block</option>
        </select>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="form-group">
        <button type="button" class="btn btn-info" id="edit-sub"><i class="fa fa-plus"></i>&nbsp;Add Sub Category</button>
 <div class="edit_new">
 
 </div>
 
        </div>
        </div>
        </div>


        </div>

        <div class="modal-footer">
       <input type="hidden" name="delete_values"  class="delete_subcategories" />
       <input type="hidden" name="delete_values_sub"  class="delete_subcategories_sub" />
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="edit_Form" value="Submit">

        </div>

        </div>
            <?php echo form_close();?>
        </div>

        </div>
        
        
        
        <div id="add_catagory" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                <div class="modal-dialog">
                
                <?php echo form_open(base_url().'Category/categorymanagement',array("id"=>'categoryForm',"enctype"=>"multipart/form-data"))?>
                <div class="modal-content">

                <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h4 class="modal-title">Add Category</h4>

                </div>

                <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                <label for="field-7" class="control-label">Category Name</label>
                <input type="text" name="category_name" class="form-control required_validation_for_category" placeholder="Catagory Name" />
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                <label for="field-7" class="control-label">Category Thumbnail</label>
                <input type="file" name="category_thumb" class="form-control" />
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                <label for="field-7" class="control-label">Category Description</label>
              <textarea class="form-control required_validation_for_category" name="category_desc" rows="4"></textarea>
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                <label for="field-7" class="control-label">Category Status</label>
                <select class="form-control required_validation_for_category" name="category_status">
                  <option value="">Choose Status</option>
                  <option value="active">Active</option>
                  <option value="deactive">Dective</option>
                  <option value="block">Block</option>
                </select>
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                <button type="button" class="btn btn-info" id="add-sub" >Add Sub Category</button>
      
        <!--<div class="row RowMain">
          <div class="col-md-12 m-b-10 insidethis">
             
              <div class="RowNo1">
             <div class="form-group no-margin">
             <label for="field-7" class="control-label">Add Sub Category</label>
             <span>
             <i class="fa fa-remove removebuttonMain1" style="font-size:12px;color:red;float:right"></i>
              <i class="fa fa-plus AddbuttonMain1" style="font-size:12px;color:#3dad3d;float:right;margin-right: 5px;margin-top: 1px;"></i> 
              </span>
             </div>
              <input type="text" value="sub13" name="sub[]" class="form-control required_validation_for_category" placeholder="Add Sub Category">
              </div>
            
             <div class="col-md-10 col-md-offset-1 m-t-10 subchilds">
             <div class="this_row">
             <div class="RowNo2">
               <div class="form-group no-margin"><label for="field-7" class="control-label">Add Sub Category</label>
               <span>
               <i class="fa fa-remove removebuttonMain2" style="font-size:12px;color:red;float:right"></i>
               <i class="fa fa-plus AddbuttonMain2" style="font-size:12px;color:#3dad3d;float:right;margin-right: 5px;margin-top: 1px;"></i> 
               
               </span>
             </div>
             <input type="text" value="sub13" name="sub[]" class="form-control required_validation_for_category" placeholder="Add Sub Category">
                 </div>
                 
              <div class="col-md-10 col-md-offset-1 m-t-10 subchildsmore">
              
              <div class="RowNo3">
               <div class="form-group no-margin">
               <label for="field-7" class="control-label">Add Sub Category</label>
               <span>
                <i class="fa fa-remove removebuttonMain3" style="font-size:12px;color:red;float:right"></i>
               
               </span>
             </div>
             <input type="text" value="sub13" name="sub[]" class="form-control required_validation_for_category" placeholder="Add Sub Category">
             </div>
             
             </div>
             
             </div>
           </div>
         </div>
        
        </div> -->
                </div>
                </div>
                </div>

                
                <div class="add-new"></div>
                </div>

                <div class="modal-footer">

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
 
                <input type="submit" class="btn btn-info " name="add_Form" value="Submit">

                </div>

                </div>
               <?php echo form_close(); ?>
                </div>

                </div>
<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom/page-js/product-category/add-category.js"></script>