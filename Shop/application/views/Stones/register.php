<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName.'/Template/header'); ?>

    <!-- ================== END HEADER SECTION ================== -->
    <!-- ================== START MAIN SECTION ================== -->
    <!-- ================== START MAIN SECTION ================== -->
    <div class="main" role="main">
        <!-- Start page location -->

        <div class="page-location">
            <div class="page-location-layer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-location-title">
                                <h3>Register</h3>
                            </div>
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Register</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="">

        <div class="col-sm-12">
        <?php
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>

        </div>

        <!-- Start user arae -->
        <div class="user-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="login-area">
                              <?php echo form_open(base_url( 'Register/checkUserAuth' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
                              <div class="form-group">
                              <h4>Login Your Account</h4>
                                  </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email"  name="userEmail" class="required_validation_for_user name_validation email_validation form-control" placeholder="Enter Email Address"/>
                                  </div>

                                <div class="form-group">
                                    <label>Password</label>
                                      <input type="password" name="userPass" class="required_validation_for_user name_validation form-control" placeholder="Enter Password"/></div>

                                <p class="forgot_password">
                                <a href="javascript:;" title="Recover your forgotten password" rel="" data-toggle="modal" data-target="#myModal">Forgot your password?</a>
                                </p>

                                <input type="submit" class="btn btn-primary acc_btn" value="Login"/>
                          <?php echo form_close(); ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="login-area">
                                  <?php echo form_open(base_url( 'Register/addShopUsers' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
                                  <div class="form-group">
                            <h4>  Don’t have an Account? Register now </h4>
                              </div>
                                <div class="form-group">
                                    <label>Firstname</label>
                                      <input type="text" class="required_validation_for_user name_validation form-control" name="userFirstName" placeholder="Enter Firstname" />
                                </div>
                                <div class="form-group">
                                    <label>Lastname</label>
                                    <input type="text" class="required_validation_for_user name_validation form-control" name="userLastName" placeholder="Enter Lastname"/>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="required_validation_for_user name_validation email_validation form-control" name="userEmail" placeholder="Enter Emailaddress"/>
                                </div>
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <input type="text" class="required_validation_for_user name_validation form-control" name="userContact" placeholder="Enter Contact Number"/>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="required_validation_for_user name_validation opass form-control" name="userPass" placeholder="Enter password"/>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="required_validation_for_user name_validation cpass form-control" name="userRetypePass" placeholder="Conform password" />
                                </div>
                                <input type="submit" class="btn btn-primary acc_btn setDisableData" type="submit" value="Create an account"/>
                          <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
    <?php echo form_open(base_url( 'Register/checkUserForgot' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
    <div class="modal-content">
    <div class="modal-header">
    <h4 class="modal-title">Forgot your password</h4>
    <button type="button" class="close" data-dismiss="modal" style="float: right;">&times;</button>
    </div>
    <div class="modal-body">
    <div class="input-area">
    <input placeholder="Please enter your Email address" name="emailForgot" class="required_validation_for_user name_validation email_validation form-control" type="text" style="width:100%;">
    </div>
    </div>
    <div class="modal-footer">
    <input type="submit" class="btn porduct-place-order-btn acc_btn" style="float: left" value="Submit"/>
    </div>
    </div>
    <?php echo form_close(); ?>
    </div>
    </div>
    <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName.'/Template/footer'); ?>
    <!-- CLient side form validation -->
    <script type="text/javascript">
    $(document).ready(function(){
            $(document).on('submit','#addShopUsers',function(e){

                    var check_required_field='';
                    $(this).find(".required_validation_for_user").each(function(){

                            var val22 = $(this).val();

                            if (!val22){
                                    check_required_field ='error';
                                    $(this).css("border-color","#ccc");
                                    $(this).css("border-color","red");
                            }
                            $(this).on('keypress change',function(){
                                    $(this).css("border-color","#ccc");
                            });
                    });
                    if(check_required_field)
                    {
                          e.preventDefault();
                            return false;
                    }
                    else {

                            return true;
                    }
            });
    });
            </script>
