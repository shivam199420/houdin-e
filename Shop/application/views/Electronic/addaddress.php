<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
  <div id="content">

    <!-- Linking -->
    <div class="linking">
      <div class="container">
        <ol class="breadcrumb">
          <li class="active">New-address</li>
        </ol>
      </div>
    </div>

    <!-- Blog -->
    <section class="login-sec padding-top-30 padding-bottom-100">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <!-- Login Your Account -->
            <h5>New Address</h5>
            <hr>
            <!-- FORM -->
            <div class="row">
                <div class="col-sm-12">
                <?php
                if($this->session->flashdata('error'))
                {
                    echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
                }
                else if($this->session->flashdata('success'))
                {
                    echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
                }
                ?>
                </div>
            </div>
            <?php echo form_open(base_url( 'Address/adduseraddress' ), array( 'id' => 'addaddress', 'method'=>'post' ));?>
              <ul class="row text">
                <li class="col-sm-12">
                  <label>Name *
                  <input name="username" placeholder="Name" class="form-control required_validation_for_address name_validation" type="text">
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Contact *
                  <input name="userphone" placeholder="Phone number" pattern="^[-+]?\d+$"  class="phone_telephone form-control required_validation_for_address name_validation" type="text">
                  </label>
                </li>
                <li class="col-sm-12 form-group">

                <label for="address">Address:</label>
                <textarea rows="3" name="usermainaddress" placeholder="Street address. Apartment, suite, unit etc. (optional)" class="form-control required_validation_for_address name_validation"></textarea>
                </li>
                <li class="col-sm-12">
                  <label>Town/City *
                  <input name="usercity" placeholder="Town/City*" class="form-control required_validation_for_address name_validation" type="text">
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Post code/Zip *
                  <input name="userpincode" placeholder="Post code / Zip" class="form-control required_validation_for_address name_validation" type="text">
                  </label>
                </li>
                <li class="col-sm-12 form-group">
                        <label> City * </label>
                        <select class="form-control required_validation_for_address" name="usercountry">
                        <option value="">Choose Country</option>
                        <option value="<?php echo $countryList[0]->houdin_user_shop_country ?>"><?php echo $countryList[0]->houdin_country_name ?></option>
                        </select>
                </li>
                <li class="col-sm-12 text-left">
                <input type="submit" class="btn-round" style="background: #0088cc !important;" value="Add Address"/>
                </li>
              </ul>
            <?php echo form_close() ?>
          </div>
        </div>
      </div>
    </section>
  </div>
   <!-- End Content -->
   <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>

  <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#addaddress',function(){
                var check_required_field='';
                $(this).find(".required_validation_for_address").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>
