<?php
class Posmodel extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
    }
    public function fetchPosData()
    {
        // get customer info
        $getCustomerData = $this->db->select('houdinv_user_name,houdinv_user_id,houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_is_active','active')->get()->result();
        // get counrty list
        $getMasterDB = $this->load->database('master',true);
        $getCountryData = $getMasterDB->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id,houdin_users.houdin_user_country,houdin_countries.houdin_country_name')
        ->from('houdin_vendor_auth')->where('houdin_vendor_auth.houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))
        ->join('houdin_users','houdin_users.houdin_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id','left outer')
        ->join('houdin_countries','houdin_countries.houdin_country_id = houdin_users.houdin_user_country')->get()->result();
        $setCountryArray = array('id'=>$getCountryData[0]->houdin_user_country,'country'=>$getCountryData[0]->houdin_country_name);
        if($this->session->userdata('vendorRole') == 0)
        {
            // get product data
            $getProducts = $this->db->select('*')->from('houdinv_products')->where('houdin_products_status','1')->where('houdinv_products_total_stocks >','0')->get()->result();
            foreach($getProducts as $getProductsList)
            {
                

                $getPriceData = json_decode($getProductsList->houdin_products_price,true);
                if($getPriceData['tax_price'])
                {
                    $getTax = $this->db->select('houdinv_tax_percenatge')->from('houdinv_taxes')->where('houdinv_tax_id',$getPriceData['tax_price'])->get()->result();
                    $setTax = round($getProductsList->houdin_products_final_price*$getTax[0]->houdinv_tax_percenatge)/100;
                }
                else
                {
                    $setTax = 0;
                }
                if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                {
                    $setFinalPrice = $getProductsList->houdin_products_final_price;
                }
                else
                {
                    $setFinalPrice = $getProductsList->houdin_products_final_price;
                }
                //$setPorudtcArray[] = array('productName'=>$getProductsList->houdin_products_title,'produtcId'=>$getProductsList->houdin_products_id,'variantId'=>0,
               // 'productPrice'=>$setFinalPrice,'stock'=>$getProductsList->houdinv_products_total_stocks,'tax_price'=>$getTax[0]->houdinv_tax_percenatge,'cp'=>$getPriceData['cost_price'],'mrp'=>$getPriceData['sale_price'],'tax_pricedata'=>$setTax);
            }
            // get variant data
            $getVariantData = $this->db->select('*')->from('houdinv_products_variants')->where('houdinv_products_variants_total_stocks >','0')->get()->result();
            foreach($getVariantData as $getVariantDataList)
            { 
                $mainproducnt_Id=$getVariantDataList->houdin_products_variants_product_id;
                $GetmanProducts = $this->db->select('*')->from('houdinv_products')->where('houdin_products_id',$mainproducnt_Id)->get()->row();
                      
                // fetch variant parent
                $getProducts = $this->db->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getVariantDataList->houdin_products_variants_product_id)->get()->result();
                $getPriceData = json_decode($getProducts[0]->houdin_products_price,true);
                if($getPriceData['tax_price'])
                {
                    $getTax = $this->db->select('houdinv_tax_percenatge')->from('houdinv_taxes')->where('houdinv_tax_id',$getPriceData['tax_price'])->get()->result();
                    $setTax = round($getVariantDataList->houdinv_products_variants_final_price*$getTax[0]->houdinv_tax_percenatge)/100;
                }
                else
                {
                    $setTax = 0;
                }
                if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                {
                    $setFinalPrice = $getVariantDataList->houdinv_products_variants_final_price;
                }
                else
                {
                    $setFinalPrice = $getVariantDataList->houdinv_products_variants_final_price;
                }
                $getVariantPrice = json_decode($getVariantDataList->houdinv_products_variants_new_price,true);
                
                $setPorudtcArray[] = array('productName'=>$GetmanProducts->houdin_products_title.' - '.$getVariantDataList->houdin_products_variants_title,'produtcId'=>0,'variantId'=>$getVariantDataList->houdin_products_variants_id,
                'productPrice'=>$setFinalPrice,'stock'=>$getVariantDataList->houdinv_products_variants_total_stocks,'tax_price'=>$getTax[0]->houdinv_tax_percenatge,'cp'=>$getVariantPrice['cost_price'],'mrp'=>$getVariantPrice['sale_price'],'tax_pricedata'=>$setTax);
            }   
        }
        else
        {

        }
        return array('productData'=>$setPorudtcArray,'customerList'=>$getCustomerData,'countryData'=>$setCountryArray);
    }
    public function IsCouponExist($data)
    {
        $getCouponsData = $this->db->select("*")->from("houdinv_coupons")->where("houdinv_coupons_code",strtolower($data['code']))->where('houdinv_coupons_status','active')->get()->row();
        $date = strtotime(date("Y-m-d")); 
        if($getCouponsData)
        {
            if(strtotime($getCouponsData->houdinv_coupons_valid_to)>=$date && strtotime($getCouponsData->houdinv_coupons_valid_from)<=$date)
            {
                if($data['totalAmount'] < $getCouponsData->houdinv_coupons_order_amount)
                {
                    $array = array("message"=>"Minimum amount for this coupon is ".$getCouponsData->houdinv_coupons_order_amount."");      
                    return $array;
                }
                else
                {
                    // check user coupon access
                    $getCouponUsage = $this->db->select('houdinv_user_use_Coupon_used_Count')->from('houdinv_user_use_Coupon')->where('houdinv_user_use_Coupon_user_id',$data['customerId'])->where('houdinv_user_use_Coupon_coupon_id',$getCouponsData->houdinv_coupons_id)->get()->result();
                    if($getCouponUsage[0]->houdinv_user_use_Coupon_used_Count < $getCouponsData->houdinv_coupons_limit)
                    {
                        // Check user group
                        $user_group = $this->userGroup($data['customerId'],$data['shopName']); 
                        $dataCustomerGroupCheck = $this->db->select("*")->from("houdinv_coupons_procus")->where('houdinv_coupons_procus_coupon_id',$getCouponsData->houdinv_coupons_id)
                        ->where('houdinv_coupons_procus_user_id',$data['customerId'])->or_where("houdinv_coupons_procus_user_group",$user_group)->get()->row();
                        $date = strtotime(date("Y-m-d"));     
                        if($dataCustomerGroupCheck)
                        {
                            $array = array('message'=>'Success','discount'=>$getCouponsData->houdinv_coupons_discount_precentage,'couponId'=>$getCouponsData->houdinv_coupons_id);
                            return $array;
                        }
                        else
                        {
                            $user_product = $this->ShowCartData1($data['customerId'],$data['shopName']);
                            $dataProductCheck = $this->db->select("*")->from("houdinv_coupons_procus")->where('houdinv_coupons_procus_coupon_id',$getCouponsData->houdinv_coupons_id)
                                    ->where_in('houdinv_coupons_procus_product_id',$user_product['item'])
                                    ->get()->row();
                            if($dataProductCheck)
                            {
                                $array = array('message'=>'Success','discount'=>$getCouponsData->houdinv_coupons_discount_precentage,'couponId'=>$getCouponsData->houdinv_coupons_id);
                                return $array;
                            }
                            else
                            {
                                $array = array('message'=>'Coupon code is not valid');
                                return $array;
                            } 
                        }
                    }
                    else
                    {
                        $array = array('message'=>'Your coupon usage limit is excced');
                        return $array;
                    }
                }
            }
           else
           {
                $array = array("message"=>"Coupon code is expired");  
                return $array;
           }
        }
        else
        {
           $array = array('message'=>'Coupon code is not valid');
           return $array;
        } 
    }
    public function userGroup($id,$shop)
    {
        $data = $this->db->select("*")->from("houdinv_users_group_users_list")->where('houdinv_users_group_users_id',$id)->get()->row();
        return $data->houdinv_users_group__user_group_id;
    }
    public function ShowCartData1($data,$shop)
    {
        $data = $this->db->select("*")->from("houdinv_users_cart") ->join("houdinv_products","houdinv_users_cart.houdinv_users_cart_item_id=houdinv_products.houdin_products_id")
                ->where("houdinv_users_cart.houdinv_users_cart_user_id",$id)->get()->result();
        $cat1 = array();
        foreach($data as $val)
        {
            $item[] =  $val->houdinv_users_cart_item_id;
            $cat = explode(",",$val->houdin_products_category);
            array_push($cat1,$cat);
        }
        $array = array("item"=>$item,"cat"=>$cat);
        return $array;
    }
    public function fetchUserAddressApp($data)
    {
        $getUserAddress = $this->db->select('*')->from('houdinv_user_address')->where('houdinv_user_address_user_id',$data['id'])->get()->result();
        if(count($getUserAddress) > 0)
        {
            foreach($getUserAddress as $getUserAddressList)
            {
                $setAddressArray[] = array('addressId'=>$getUserAddressList->houdinv_user_address_id,'name'=>$getUserAddressList->houdinv_user_address_name,
                'phone'=>$getUserAddressList->houdinv_user_address_phone,'mainAddress'=>$getUserAddressList->houdinv_user_address_user_address,'city'=>$getUserAddressList->houdinv_user_address_city,
                'zip'=>$getUserAddressList->houdinv_user_address_zip);
            }
            return array('message'=>'Success','data'=>$setAddressArray);
        }
        else
        {
            return array('message'=>'No address found. Please add new address');
        }
    }
    public function addUserAddressDataApp($data,$shop)
    {
        $insertStatus = $this->db->insert('houdinv_user_address',$data);
        if($insertStatus == 1)
        {
            return array('message'=>'yes');
        }
    }
    public function getCustomerPriv($data)
    {
        $getPriv = $this->db->select('houdinv_users_privilage')->from('houdinv_users')->where('houdinv_user_id',$data)->get()->result();
        if(count($getPriv) > 0)
        {
            return array('message'=>'Success','dataValuePriv'=>$getPriv[0]->houdinv_users_privilage);
        }
        else
        {
            return array('message'=>'Something went wrong. Please try again');
        }
    }

    public function productStockmanage($stock)
    {  
$product_id=$stock['product_id'];
$product_Count=$stock['product_Count'];
if($stock['product_id']!='0'){
    $getProductData = $this->db->select('houdin_products_title,houdinv_products_total_stocks,houdinv_products_main_images')
    ->from('houdinv_products')->where('houdin_products_id',$product_id)->get()->row(); 
		
   $products_total_stocks=$getProductData->houdinv_products_total_stocks;
   $newstockprodunct= $products_total_stocks-$product_Count;
 
   $data=array('houdinv_products_total_stocks'=>$newstockprodunct); 
   $this->db->where('houdin_products_id',$product_id);
   $this->db->update('houdinv_products',$data);                 

}else if($stock['variant_id']!='0'){
    $variant_id=$stock['variant_id'];
    $getVariantData = $this->db->select('houdin_products_variants_title,houdinv_products_variants_total_stocks,houdin_products_variants_image')->from('houdinv_products_variants')->where('houdin_products_variants_id',$variant_id)->get()->row(); 
    $variants_total_stocks=$getVariantData->houdinv_products_variants_total_stocks;
    $newstockvariants=$variants_total_stocks-$product_Count;

 $data=array('houdinv_products_variants_total_stocks'=>$newstockvariants); 
$this->db->where('houdin_products_variants_id',$variant_id);
$this->db->update('houdinv_products_variants',$data);
   
}



 
return true;
     }

    public function setOrderData($order,$extra,$pending,$transaction)
    {         
        $this->db->insert('houdinv_orders',$order);
        $getOrderId = $this->db->insert_id();
        if($getOrderId)
        {
             // update account receivable(debtor) in case of order is COD
             if($order['houdinv_order_payment_method'] != 'card' && $order['houdinv_order_payment_method'] != 'cheque')
             {
                 if($order['houdinv_payment_status'] == '1')
                 {
                    $getAccountData = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id',16)->where('houdinv_accounts_detail_type_id',170)->get()->result();
                    if(count($getAccountData) > 0)
                    {
                        $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getAccountData[0]->houdinv_accounts_id,
                        'houdinv_accounts_balance_sheet_ref_type'=>'Undeposited Fund',
                        'houdinv_accounts_balance_sheet_pay_account'=>$order['houdinv_order_user_id'],
                        'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                        'houdinv_accounts_balance_sheet_account'=>0,
                        'houdinv_accounts_balance_sheet_order_id'=>$getOrderId,
                        'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                        'houdinv_accounts_balance_sheet__increase'=>$order['houdinv_orders_total_paid'],
                        'houdinv_accounts_balance_sheet_decrease'=>0,
                        'houdinv_accounts_balance_sheet_deposit'=>0,
                        'houdinv_accounts_balance_sheet_final_balance'=>0,
                        'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                        $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                        // get amount
                        $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                        ->where('houdinv_accounts_balance_sheet_account_id',$getAccountData[0]->houdinv_accounts_id)->get()->result();
                        if(count($getTotalAmount) > 0)
                        {
                            $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                        }
                        else
                        {
                            $setFinalAmount = 0;
                        }
                        $this->db->where('houdinv_accounts_id',$getAccountData[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                    }
                 }
                 else
                 {
                    $getAccountData = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id',1)->where('houdinv_accounts_detail_type_id','1')->get()->result();
                    if(count($getAccountData) > 0)
                    {
                        $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getAccountData[0]->houdinv_accounts_id,
                        'houdinv_accounts_balance_sheet_ref_type'=>'Pending Amount',
                        'houdinv_accounts_balance_sheet_pay_account'=>$order['houdinv_order_user_id'],
                        'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                        'houdinv_accounts_balance_sheet_account'=>0,
                        'houdinv_accounts_balance_sheet_order_id'=>$getOrderId,
                        'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                        'houdinv_accounts_balance_sheet__increase'=>$order['houdinv_orders_total_remaining'],
                        'houdinv_accounts_balance_sheet_decrease'=>0,
                        'houdinv_accounts_balance_sheet_deposit'=>0,
                        'houdinv_accounts_balance_sheet_final_balance'=>0,
                        'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                        $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                        // get amount
                        $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                        ->where('houdinv_accounts_balance_sheet_account_id',$getAccountData[0]->houdinv_accounts_id)->get()->result();
                        if(count($getTotalAmount) > 0)
                        {
                            $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                        }
                        else
                        {
                            $setFinalAmount = 0;
                        }
                        $this->db->where('houdinv_accounts_id',$getAccountData[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                    }
                 }
             }
             else
             {
                // update accounts data
                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
                if(count($getUndepositedAccount) > 0)
                {
                    $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                    'houdinv_accounts_balance_sheet_ref_type'=>'Online Transaction',
                    'houdinv_accounts_balance_sheet_pay_account'=>$order['houdinv_order_user_id'],
                    'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                    'houdinv_accounts_balance_sheet_account'=>0,
                    'houdinv_accounts_balance_sheet_order_id'=>$getOrderId,
                    'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                    'houdinv_accounts_balance_sheet__increase'=>$order['houdinv_orders_total_paid'],
                    'houdinv_accounts_balance_sheet_decrease'=>0,
                    'houdinv_accounts_balance_sheet_deposit'=>0,
                    'houdinv_accounts_balance_sheet_final_balance'=>0,
                    'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                    $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                    // get amount
                    $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                    ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                    if(count($getTotalAmount) > 0)
                    {
                        $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                    }
                    else
                    {
                        $setFinalAmount = 0;
                    }
                    $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                }
                // end here
             }
             // end here
            if(count($extra) > 0)
            {
                $getAddress = $extra['address'];
                $getAddressValueData = explode('^',$getAddress);
                $setInsertArray = array('houdinv_order_users_order_id'=>$getOrderId,'houdinv_order_users_name'=>$extra['name'],'houdinv_order_users_contact'=>$extra['contact'],'houdinv_order_users_main_address'=>$getAddressValueData[0],
                'houdinv_order_users_city'=>$getAddressValueData[1],'houdinv_order_users_zip'=>$getAddressValueData[2]);
                $getInsertData = $this->db->insert('houdinv_order_users',$setInsertArray);
                if($getInsertData)
                {
                    // add undeposited fund
                            if($order['houdinv_order_payment_method'] == "cash")
                            {
                                $getUndepositedAccount = $this->db->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','16')->get()->result();    
                                if(count($getUndepositedAccount) > 0)
                                {
                                    $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                                    'houdinv_accounts_balance_sheet_ref_type'=>'Undeposited Fund',
                                    'houdinv_accounts_balance_sheet_pay_account'=>$order['houdinv_order_user_id'],
                                    'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                                    'houdinv_accounts_balance_sheet_account'=>0,
                                    'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                                    'houdinv_accounts_balance_sheet__increase'=>$order['houdinv_orders_total_paid'],
                                    'houdinv_accounts_balance_sheet_decrease'=>0,
                                    'houdinv_accounts_balance_sheet_deposit'=>0,
                                    'houdinv_accounts_balance_sheet_final_balance'=>0,
                                    'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                                    $this->db->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                                    // get amount
                                    $getTotalAmount = $this->db->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                                    ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                                    if(count($getTotalAmount) > 0)
                                    {
                                        $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                                    }
                                    else
                                    {
                                        $setFinalAmount = 0;
                                    }
                                    $this->db->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                                }
                            }
                    return array('message'=>'yes','orderId'=>$getOrderId);
                }
                else
                {
                    return array('message'=>'user','orderId'=>$getOrderId);
                }
            }
            else
            {
                if(count($pending) > 0)
                {
                    $getPendingAmount = $this->db->select('houdinv_users_pending_amount')->from('houdinv_users')->where('houdinv_user_id',$pending['userId'])->get()->result();
                    if(count($getPendingAmount) > 0)
                    {
                        $newPendingAmount = $getPendingAmount[0]->houdinv_users_pending_amount+$pending['pendingAmount'];
                    }
                    else
                    {
                        $newPendingAmount = $pending['pendingAmount'];
                    }
                    $this->db->where('houdinv_user_id',$pending['userId']);
                    $this->db->update('houdinv_users',array('houdinv_users_pending_amount'=>$newPendingAmount));
                }
                if(count($transaction) > 0)
                {
                    $setTransactionId = "TXN".rand(99999999,10000000);
                    $setTrasanctionArray = array('houdinv_transaction_transaction_id'=>$setTransactionId,'houdinv_transaction_type'=>'credit',
                    'houdinv_transaction_method'=>$transaction['houdinv_transaction_method'],'houdinv_transaction_from'=>'pos',
                    'houdinv_transaction_for'=>'order','houdinv_transaction_for_id'=>$getOrderId,'houdinv_transaction_amount'=>$transaction['houdinv_transaction_amount'],
                    'houdinv_transaction_date'=>date('Y-m-d'),'houdinv_transaction_status'=>'success');
                    $this->db->insert('houdinv_transaction',$setTrasanctionArray);
                }
                return array('message'=>'yes','orderId'=>$getOrderId);    
            }
        }
        else
        {
            return array('message'=>'no');
        }
    }

    public function getCustomer($data)
    {
        $get_customer = $this->db->select('*')->from('houdinv_users')->like('houdinv_user_contact',$data['houdinv_user_contact'])
        ->or_like('houdinv_user_name',$data['houdinv_user_contact'])
        ->or_like('houdinv_user_email',$data['houdinv_user_contact'])
        ->or_like('houdinv_user_id',$data['houdinv_user_contact'])
        ->get()->result();
        if(count($get_customer) > 0)
        {
            return array('message'=>'Success','data'=>$get_customer);
        }
        else
        {
            return array('message'=>'Customer not found');
        }
    }

    public function addCustomerData($data)
    {
        $get_customer = $this->db->select('*')->from('houdinv_users')->where('houdinv_user_contact',$data['houdinv_user_contact'])->get()->result();
        if (count($get_customer)) {
            $return = ['status' => '402','message' => 'Mobile number is already exist'];
        } else {
            $this->db->insert('houdinv_users',$data);
            $get_customer = $this->db->insert_id();
            $return = ['status' => '200','message' => 'Customer added successfully','data' => $get_customer];
        }
        return $return;
    }
}
