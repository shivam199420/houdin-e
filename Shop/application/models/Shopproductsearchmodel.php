<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopproductsearchmodel extends CI_Model{
    function __construct()
    {
    }
    public function searchProduct($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopname']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        // fetch product and variants
        $setProductListData = array();
        $setProductCategoryData = array();
        $getKeyword = $data['keyword'];
        $getStoreSetting = stroeSetting($data['shopname']);
        $getProductList = $getDynamicDB->select('*')->from('houdinv_products')->where('houdin_products_status','1')->where("houdin_products_title LIKE '%$getKeyword%'")->get()->result();
        if(count($getProductList) > 0)
        {
            foreach($getProductList as $getProductListData)
            {
                // get price
                $getPriceData = json_decode($getProductListData->houdin_products_price,true);
                // get product image
                $getImageData = json_decode($getProductListData->houdinv_products_main_images,true);
                $setImage = $this->session->userdata('vendorURL')."upload/productImage/".$getImageData[0];
                if($getStoreSetting[0]->receieve_order_out == 1)
                {
                    $setShowcart = 'yes';
                }
                else
                {
                    if($getProductListData->houdinv_products_total_stocks > 0)
                    {
                        $setShowcart = 'yes';
                    }
                    else
                    {
                        $setShowcart = 'no';
                    }
                }

                $setProductListData[] = array('productId'=>$getProductListData->houdin_products_id,'variantId'=>0,'productName'=>$getProductListData->houdin_products_title,'price'=>$getPriceData['price'],'productImage'=>$setImage,'productStock'=>$getProductListData->houdinv_products_total_stocks,'cartShow'=>$setShowcart);
            }
        }
        $getVariantList = $getDynamicDB->select('*')->from('houdinv_products_variants')->where("houdin_products_variants_title LIKE '%$getKeyword%'")->get()->result();
        if(count($getVariantList) > 0)
        {
            foreach($getVariantList as $getVariantListData)
            {
                // get price
                $getPriceData = $getVariantListData->houdin_products_variants_prices;
                // get product image
                $setImage = $this->session->userdata('vendorURL')."upload/productImage/".$getVariantListData->houdin_products_variants_image;
                if($getStoreSetting[0]->receieve_order_out == 1)
                {
                    $setShowcart = 'yes';
                }
                else
                {
                    if($getVariantListData->houdinv_products_variants_total_stocks > 0)
                    {
                        $setShowcart = 'yes';
                    }
                    else
                    {
                        $setShowcart = 'no';
                    }
                }
                $setProductListData[] = array('productId'=>0,'variantId'=>$getVariantListData->houdin_products_variants_id,'productName'=>$getVariantListData->houdin_products_variants_title,'price'=>$getPriceData,'productImage'=>$setImage,'productStock'=>$getVariantListData->houdinv_products_variants_total_stocks,'cartShow'=>$setShowcart);
            }
        }
        // fetch category sub category and sub sub category
        $getProductCategory = $getDynamicDB->select('*')->from('houdinv_categories')->where('houdinv_category_status','active')->where("houdinv_category_name LIKE '%$getKeyword%'")->get()->result();
        if(count($getProductCategory) > 0)
        {
            foreach($getProductCategory as $getProductCategoryList)
            {
                $setProductCategoryData[] = array('categoryId'=>$getProductCategoryList->houdinv_category_id,'subCategoryId'=>'','subSubCategoryId'=>'','categoyrName'=>$getProductCategoryList->houdinv_category_name);
            }
        }
        $getProductSubCategory = $getDynamicDB->select('*')->from('houdinv_sub_categories_one')->where("houdinv_sub_category_one_name LIKE '%$getKeyword%'")->get()->result();
        if(count($getProductSubCategory) > 0)
        {
            foreach($getProductSubCategory as $getProductSubCategoryList)
            {
                $setProductCategoryData[] = array('categoryId'=>$getProductSubCategoryList->houdinv_sub_category_one_main_id,'subCategoryId'=>$getProductSubCategoryList->houdinv_sub_category_one_id,'subSubCategoryId'=>'','categoyrName'=>$getProductSubCategoryList->houdinv_sub_category_one_name);
            }
        }
        $getProductSubSubCategory = $getDynamicDB->select('*')->from('houdinv_sub_categories_two')->where("houdinv_sub_category_two_name LIKE '%$getKeyword%'")->get()->result();
        if(count($getProductSubSubCategory) > 0)
        {
            foreach($getProductSubSubCategory as $getProductSubSubCategoryList)
            {
                $setProductCategoryData[] = array('categoryId'=>$getProductSubSubCategoryList->houdinv_sub_category_two_main_id,'subCategoryId'=>$getProductSubSubCategoryList->houdinv_sub_category_two_sub1_id,'subSubCategoryId'=>$getProductSubSubCategoryList->houdinv_sub_category_two_id,'categoyrName'=>$getProductSubSubCategoryList->houdinv_sub_category_two_name);
            }
        }
        // show cart status
        $getCartShowData = $getDynamicDB->select('add_to_cart_button')->from('houdinv_storesetting')->where('id','1')->get()->result();
        if(count($getCartShowData) > 0)
        {
            if($getCartShowData[0]->add_to_cart_button == 1)
            {
                $setCartShow = 'yes';
            }
            else
            {
                $setCartShow = 'no';
            }

        }
        else
        {
            $setCartShow = 'no';
        }
        return array('message'=>'Sucess','productList'=>$setProductListData,'categoryList'=>$setProductCategoryData,'cartShow'=>$setCartShow);
    }
}
?>
