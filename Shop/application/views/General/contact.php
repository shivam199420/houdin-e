<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">Contact Us</h2>
            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <div id="contact-area" class="contact-area ptb-120 gray-bg">
        <div class="container">
        <?php 
		if($this->session->flashdata('error'))
		{
			echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
		}
		else if($this->session->flashdata('success'))
		{
			echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
		}
		?>
            <div class="section-title text-center mb-70">
                <h2>GET IN TOUCH <i class="fa fa-phone"></i></h2>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-offset-2 col-lg-8 col-sm-12">
                    <div class="row">
                        <div class="col-md-5 col-lg-5 col-sm-5">
                            <div class="contact-info-area">
                                <ul>
                                    <li>
                                        <div class="contact-icon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="contact-address">
                                            <h5>Phone</h5>
                                            <span><?php echo $shopInfo[0]->houdinv_shop_contact_info ?></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="contact-icon">
                                            <i class="fa fa-envelope-o"></i>
                                        </div>
                                        <div class="contact-address">
                                            <h5>Email</h5>
                                            <span><a href="mailto:<?php echo $shopInfo[0]->houdinv_shop_customer_care_email ?>"><?php echo $shopInfo[0]->houdinv_shop_customer_care_email ?><a/></span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="contact-icon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="contact-address">
                                            <h5>Our Location</h5>
                                            <span><?php echo $shopInfo[0]->houdinv_shop_address ?></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-7 col-lg-7 col-sm-7">
                            <div class="sent-message">
                            <?php echo form_open(base_url( 'Contact/addcontact' ), array( 'id' => 'contact_form_data' ,'method'=>'post' ));?> 
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="main-input mb-10">
                                            <input type="text" name="userName" class="form-control required_validation_for_shop_contact name_validation" placeholder="Name*" />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="main-input mb-10">
                                            <input type="text" name="userEmail" class="form-control required_validation_for_shop_contact email_validation name_validation" placeholder="Email*" />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="main-input mb-10">
                                            <input type="text" name="userEmailSub" class="form-control required_validation_for_shop_contact name_validation" placeholder="Subject" />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="text-leave2 mb-20">
                                            <textarea name="userEmailBody" rows="5" class="form-control required_validation_for_shop_contact name_validation" placeholder="Message"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                        <input class="submit ripple-btn" type="submit" value="Send Message"/>
                                        </div>
                                    </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="contact-area-all">
        <div id="Chaffer"></div>
    </div> -->
    <?php 
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer') 
  ?>

  <script type="text/javascript">
		$(document).ready(function(){
			$(document).on('submit','#contact_form_data',function(){
				var check_required_field='';
				$(this).find(".required_validation_for_shop_contact").each(function(){
					var val22 = $(this).val();
					if (!val22){
						check_required_field =$(this).size();
						$(this).css("border-color","#ccc");
						$(this).css("border-color","red");
					}
					$(this).on('keypress change',function(){
						$(this).css("border-color","#ccc");
					});
				});
				if(check_required_field)
				{
					return false;
				}
				else {
					return true;
				}
			});
		});
		</script>	