<?php 
        $getShopName = getfolderName($this->session->userdata('shopName')); 
        $this->load->view("".$getShopName."/Template/header"); ?>


        <!-- Page item Area -->
        <div id="page_item_area">
        <div class="container">
        <div class="row">
        <div class="col-sm-6 text-left">
        <h3>Products</h3>
        </div>		
        </div>
        </div>
        </div>


        <!-- Shop Product Area -->
        <div class="shop_page_area">
        <div class="container">
        <div class="shop_bar_tp fix">
        <div class="row">
        <div class="col-sm-6 col-xs-12 short_by_area">
        <div class="short_by_inner">
        <label>short by:</label>
        <select class="sort-select">
        <option>Name Descending</option>
        <option>Date Descending</option>
        <option>Price Descending</option>
        </select>
        </div>
        </div>

        
        </div>
        </div>	
		        <div class="row">
        <div class="container">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        
         if($this->session->flashdata('message_name'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('message_name').'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>
        </div>
        <div class="shop_details text-center">
        <div class="row">	
            <?php 
            foreach($productList as $productListData)
            {
          
              //  $getProductImage = json_decode($productListData->houdinv_products_main_images,true);
              //  $getProductPrice = json_decode($productListData->houdin_products_price,true);
            ?>
            <div class="col-md-3 col-sm-6">
            <div class="single_product">
            <div class="product_image">
            <img src="<?php echo $productListData['productImage']; ?>" alt="" style="width: 250px!important; height: 300px!important;"/>
            <?php if($productListData['productStock'] <= 0) { ?><div class="new_badge">Out of Stock</div><?php } ?>
            <div class="box-content">
            <a href="#" class="Add_to_whishlist_button" data-cart="<?php echo $productListData['poductId']; ?>" ><i class="fa fa-heart-o"></i></a>
            <?php 
            if($productListData['productStock'] <= 0)
            { 
            if($shopsetting[0]->receieve_order_out == 1)
            {
                ?>
                <a class="Add_to_cart_button" data-cart="<?php echo $productListData['poductId']; ?>"><i class="fa fa-cart-plus"></i></a>
            <?php  }
            }
            else
            {
            ?>
                <a class="Add_to_cart_button" data-cart="<?php echo $productListData['poductId']; ?>"><i class="fa fa-cart-plus"></i></a>
            <?php }
            ?>
            
            <a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData['poductId']; ?>"><i class="fa fa-search"></i></a>
            </div>										
            </div>

            <div class="product_btm_text">
            <h4><a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData['poductId']; ?>"><?php echo substr($productListData['productName'],0, 25).".." ; ?></a></h4>
            <span class="price"><?php echo $productListData['price']; ?> <?php   ?></span>
            </div>
            </div>								
            </div>
            <?php }
            ?>								

        </div>
        
                <div class="row">
        <?php 
        foreach($categoryList as $categoryDataList)
        {
          if($categoryDataList['houdinv_category_thumb'])
          {
            $setImageData = $this->session->userdata('vendorURL')."images/category/".$categoryDataList->houdinv_category_thumb;
          }
          else
          {
            $setImageData = base_url()."Extra/apparels/img/promo/1.jpg";
          }
        ?>  
        <div class="col-xs-12 col-lg-3 col-md-3 col-sm-3">	
        <a href="<?php echo base_url() ?>Productlist/<?php echo $categoryDataList['categoryId']; ?>">
        <div class="single_promo">
        <img class="image_w_h" src="<?php echo $setImageData; ?>" alt="">
        <div class="box-content">
        <h3 class="title"><?php echo $categoryDataList['categoyrName']; ?></h3>
        <span class="post"><?php echo date("Y"); ?> Collection</span>
        </div>
        </div>
        </a>						
        </div>
        <?php }
        ?>
        <!--  End Col -->						
 

        <!--  End Col -->					

        </div>	
        
        
        
        </div>

        <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>	
        </div>
        </div>
        <?php 
        $getShopName = getfolderName($this->session->userdata('shopName')); 
        $this->load->view("".$getShopName."/Template/footer"); ?>