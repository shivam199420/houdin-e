<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
 
 <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

              <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">GST Information</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                   <li><a href="<?php echo base_url(); ?>tax/Taxlanding">Tax</a></li>
                  <li class="active">GST Information</li>
                  </ol>
                  </div>
             
                  
               
            </div>
           <!--END Page-Title -->  
            <div class="row m-t-20">
              <div class="col-lg-12">
                <div class="card-box">                          
                  <form action="#" data-parsley-validate novalidate>
                    <div class="form-group">
                      <label for="userName">Gstin value</label>
                      <input type="text" name="nick" parsley-trigger="change" required placeholder="" class="form-control" id="userName">
                    </div>
                     <div class="form-group">
                      <label for="userName">Home State/Region/Province</label>
                     <select class="form-control">
                      <option>Assam</option>
                      <option>Bihar</option>
                      
                    </select>
                   
                    </div>
                    <div class="form-group m-b-0">
                      <button class="btn btn-primary waves-effect waves-light" type="Save">
                        Save
                      </button>
                      
                    </div>
                    <hr>
                    
                   <h4>All product prices are :</h4>
                  <div class="form-group">
                      <select class="form-control">
                      <option>Tax inclusive</option>
                      </select>
                   </div>

                  </form>
                </div>
              </div>
            </div>                          
      </div>
    </div>
  </div>


<?php $this->load->view('Template/footer.php') ?>

<script>
$(document).ready(function(){

    $(".select2").select2();
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
