<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shopuserordermodel extends CI_Model{
    function __construct()
    {

    }
    public function fetchUserWishlist($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shop']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getWishlistData = $getDynamicDB->select('*')->from('houdinv_users_whishlist')->where('houdinv_users_whishlist_user_id',$data['id'])->get()->result();
        if(count($getWishlistData) > 0)
        {
            // $getStoreSetting = $getDynamicDB->select('display_out_stock')->from('houdinv_storesetting')->where('id','1')->get()->result();
            foreach($getWishlistData as $getWishlistDataList)
            {
                if($getWishlistDataList->houdinv_users_whishlist_item_id != 0 && $getWishlistDataList->houdinv_users_whishlist_item_id != "")
                {
                    // fetch product list
                    $getProductList = $getDynamicDB->select('houdin_products_id,houdin_products_title,houdin_products_price,houdinv_products_main_images')->from('houdinv_products')->where('houdin_products_id',$getWishlistDataList->houdinv_users_whishlist_item_id)->get()->result();
                    $getProductPrice = json_decode($getProductList[0]->houdin_products_price,true);
                    $gteProductImage = json_decode($getProductList[0]->houdinv_products_main_images,true);
                    $setProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$gteProductImage[0];
                    if($getProductPrice['discount'] && $getProductPrice['discount'] != 0)
                    {
                        $setOriginalPrice = $getProductPrice['price'];
                        $setDiscountPrice = $getProductPrice['price']-$getProductPrice['discount'];
                    }
                    else
                    {
                        $setOriginalPrice = $getProductPrice['price'];
                        $setDiscountPrice = 0;
                    }
                    $setWishlistArray[] = array('wishlistId'=>$getWishlistDataList->houdinv_users_whishlist_id,'productId'=>$getWishlistDataList->houdinv_users_whishlist_item_id,'variantId'=>0,'productTitle'=>$getProductList[0]->houdin_products_title,'productPrice'=>$setOriginalPrice,'productDiscountPrice'=>$setDiscountPrice,'productImage'=>$setProductImage);
                }
                else
                {
                    // get variant list
                    $getVariantListData = $getDynamicDB->select('houdin_products_variants_id,houdin_products_variants_title,houdin_products_variants_prices,houdin_products_variants_product_id,houdin_products_variants_image')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getWishlistDataList->houdinv_users_whishlist_item_variant_id)->get()->result();
                    // get product list
                    $getProductPriceData = $getDynamicDB->select('houdin_products_price,houdinv_products_main_images')->from('houdinv_products')->where('houdin_products_id',$getVariantListData->houdin_products_variants_product_id)->get()->result();
                    $getProductPrice = json_decode($getProductPriceData[0]->houdin_products_price,true);
                    $getProdctImage = json_decode($getProductPriceData[0]->houdinv_products_main_images,true);
                    if($getProductPrice['discount'] && $getProductPrice['discount'] != 0)
                    {
                        $setOriginalPrice = $getVariantListData[0]->houdin_products_variants_prices;
                        $setDiscountPrice = $getVariantListData[0]->houdin_products_variants_prices-$getProductPrice['discount'];
                    }
                    else
                    {
                        $setOriginalPrice = $getVariantListData[0]->houdin_products_variants_prices;
                        $setDiscountPrice = 0;
                    }
                    // set image
                    if($getVariantListData[0]->houdin_products_variants_image)
                    {
                        $setImage = $this->session->userdata('vendorURL')."upload/productImage/".$getVariantListData[0]->houdin_products_variants_image;
                    }
                    else
                    {
                        $setImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProdctImage[0];
                    }
                    $setWishlistArray[] = array('wishlistId'=>$getWishlistDataList->houdinv_users_whishlist_id,'productId'=>0,'variantId'=>$getVariantListData[0]->houdin_products_variants_id,'productTitle'=>$getVariantListData[0]->houdin_products_variants_title,'productPrice'=>$setOriginalPrice,'productDiscountPrice'=>$setDiscountPrice,'productImage'=>$setImage);
                }
            }
            return array('message'=>'Success','wishlist'=>$setWishlistArray);
        }
        else
        {
            return array('messgae'=>'No data');
        }
    }
    public function fetchUserCoupons($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getCouponsData = $getDynamicDB->select('*')->from('houdinv_coupons')->where('houdinv_coupons_status','active')->where('houdinv_coupons_valid_to > CURDATE()')->get()->result();
        if(count($getCouponsData) > 0)
        {
            foreach($getCouponsData as $getCouponsDataList)
            {
                $setExpiry = date('d-m-Y',strtotime($getCouponsDataList->houdinv_coupons_valid_to));
                $setCouponsArray[] = array('couponName'=>$getCouponsDataList->houdinv_coupons_name,'couponExpiry'=>$setExpiry,'minorder'=>$getCouponsDataList->houdinv_coupons_order_amount,'couponCode'=>$getCouponsDataList->houdinv_coupons_code,'couponDiscount'=>$getCouponsDataList->houdinv_coupons_discount_precentage);
            }
            return array('message'=>'success','data'=>$setCouponsArray);
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function fetchUserCartData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shop']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getCartData = $getDynamicDB->select('*')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$data['id'])->get()->result();
        if(count($getCartData) > 0)
        {
            $getStoreSetting = stroeSetting($data['shop']);
            $getTotalPrice = 0;
            $getTotalDiscount = 0;
            $payablePrice = 0;
            $getAdditionalCharges = 0;
            foreach($getCartData as $getCartDataList)
            {
                $getCartQuantity = $getCartDataList->houdinv_users_cart_item_count;
                if($getCartDataList->houdinv_users_cart_item_id != 0 && $getCartDataList->houdinv_users_cart_item_id != "")
                {
                    // fetch product data
                    $getPorductsData = $getDynamicDB->select('*')->from('houdinv_products')->where('houdin_products_id',$getCartDataList->houdinv_users_cart_item_id)->get()->result();
                    $getPriceData = json_decode($getPorductsData[0]->houdin_products_price,true);
                    $getProductImage = json_decode($getPorductsData[0]->houdinv_products_main_images,true);
                    if($getProductImage[0])
                    {
                        $setProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                    }
                    else
                    {
                        $getShop = shopurl();
                        $setProductImage = "".$getShop."Extra/noimage.jpg";
                    }
                    if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                    {
                        $setProductPrice = $getPriceData['sale_price'];
                        $productDiscount = $getPorductsData[0]->houdin_products_final_price;
                        $getDiscount = $setProductPrice-$productDiscount;
                        $getTotalPrice = $getTotalPrice+($getPriceData['sale_price']*$getCartQuantity);
                        $getTotalDiscount = $getTotalDiscount+($getDiscount*$getCartQuantity);
                        $payablePrice = $payablePrice+($productDiscount*$getCartQuantity);
                        $getAdditionalCharges = $getAdditionalCharges+($getPriceData['price']-$productDiscount);
                    }
                    else
                    {
                        $setProductPrice = $getPorductsData[0]->houdin_products_final_price;
                        $productDiscount = 0;
                        $getDiscount = $getPriceData['sale_price']-$setProductPrice;
                        $getTotalPrice = $getTotalPrice+($getPriceData['sale_price']*$getCartQuantity);
                        $getTotalDiscount = $getTotalDiscount+($getDiscount*$getCartQuantity);
                        $payablePrice = $payablePrice+($setProductPrice*$getCartQuantity);
                        $getAdditionalCharges = $getAdditionalCharges+($getPriceData['price']-$setProductPrice);
                    }
                    if($getStoreSetting[0]->receieve_order_out == 1)
                    {
                        $setShowcart = 'yes';
                    }
                    else
                    {
                        if($getPorductsData[0]->houdinv_products_total_stocks > 0)
                        {
                            $setShowcart = 'yes';
                        }
                        else
                        {
                            $setShowcart = 'no';
                        }
                    }
                    $setproductArray[] = array('cartId'=>$getCartDataList->houdinv_users_cart_id,'productName'=>$getPorductsData[0]->houdin_products_title,
                    'productId'=>$getCartDataList->houdinv_users_cart_item_id,'variantId'=>$getCartDataList->houdinv_users_cart_item_variant_id,
                    'productImage'=>$setProductImage,'quantity'=>$getCartQuantity,'price'=>$setProductPrice,'discountPrice'=>$productDiscount,'cartShow'=>$setShowcart);
                }
                else
                {
                    // fetch variant data
                    $getVariantData = $getDynamicDB->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getCartDataList->houdinv_users_cart_item_variant_id)->get()->result();
                    // fetch main productData
                    $getVariantProductData = $getDynamicDB->select('*')->from('houdinv_products')->where('houdin_products_id',$getVariantData[0]->houdin_products_variants_product_id)->get()->result();
                    // get variant image
                    if($getVariantData[0]->houdin_products_variants_image)
                    {
                        $setProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$getVariantData[0]->houdin_products_variants_image;
                    }
                    else
                    {
                        $getProductImage = json_decode($getVariantProductData[0]->houdinv_products_main_images,true);
                        if($getProductImage[0])
                        {
                            $setProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                        }
                        else
                        {
                            $getShop = shopurl();
                            $setProductImage = "".$getShop."Extra/noimage.jpg";
                        }
                    }
                    // get product price
                    $getPriceData = json_decode($getVariantProductData[0]->houdin_products_price,true);
                    $getVariantPrice = json_decode($getVariantData[0]->houdinv_products_variants_new_price,true);
                    if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                    {
                        $setProductPrice = $getVariantPrice['sale_price'];
                        $setProductPrice = $getVariantData[0]->houdinv_products_variants_final_price;
                        $getDiscount = $setProductPrice-$setProductPrice;
                        $getTotalPrice = $getTotalPrice+($getVariantPrice['sale_price']*$getCartQuantity);
                        $getTotalDiscount = $getTotalDiscount+($getDiscount*$getCartQuantity);
                        $payablePrice = $payablePrice+($productDiscount*$getCartQuantity);
                        $getAdditionalCharges = $getAdditionalCharges+($getVariantPrice['price']-$setProductPrice);
                    }
                    else
                    {
                        $setProductPrice = $getVariantData[0]->houdinv_products_variants_final_price;
                        $productDiscount = 0;
                        $getDiscount = $getVariantPrice['sale_price']-$setProductPrice;
                        $getTotalPrice = $getTotalPrice+($getVariantPrice['sale_price']*$getCartQuantity);
                        $getTotalDiscount = $getTotalDiscount+($getDiscount*$getCartQuantity);
                        $payablePrice = $payablePrice+($setProductPrice*$getCartQuantity);
                        $getAdditionalCharges = $getAdditionalCharges+($getVariantPrice['price']-$setProductPrice);
                    }
                    if($getStoreSetting[0]->receieve_order_out == 1)
                    {
                        $setShowcart = 'yes';
                    }
                    else
                    {
                        if($getVariantData[0]->houdinv_products_variants_total_stocks > 0)
                        {
                            $setShowcart = 'yes';
                        }
                        else
                        {
                            $setShowcart = 'no';
                        }
                    }
                    $setproductArray[] = array('cartId'=>$getCartDataList->houdinv_users_cart_id,'productName'=>$getVariantData[0]->houdin_products_variants_title,
                    'productId'=>0,'variantId'=>$getVariantData[0]->houdin_products_variants_id,
                    'productImage'=>$setProductImage,'quantity'=>$getCartQuantity,'price'=>$setProductPrice,'discountPrice'=>$productDiscount,'cartShow'=>$setShowcart);
                }
            }
            // set checkout and delivery charges
            $getCheckoutData = $getDynamicDB->select('minimum_order_amount,delivery_charges,free_delivery_val')->from('houdinv_possetting')->where('id',1)->get()->result();
            if(count($getCheckoutData) > 0)
            {
                if($getCheckoutData[0]->minimum_order_amount)
                {
                    if($getCheckoutData[0]->minimum_order_amount <= $payablePrice)
                    {
                        $setCheckoutStatus = 'yes';
                    }
                    else
                    {
                        $setCheckoutStatus = 'no';
                    }
                    $setMinimumOrder = $getCheckoutData[0]->minimum_order_amount;
                }
                if($getCheckoutData[0]->free_delivery_val <= $payablePrice)
                {
                    $setDeliveryChrages = $getCheckoutData[0]->delivery_charges;
                }
                else
                {
                    $setDeliveryChrages = 0;
                }
            }
            else
            {
                $setDeliveryChrages = 0;
                $setMinimumOrder = 0;
                $setCheckoutStatus = 'yes';
            }
            $finalPayableAmount = $payablePrice;
            return array('message'=>'Success','cartdata'=>$setproductArray,'totalamount'=>$getTotalPrice,'totalDiscount'=>$getTotalDiscount,'totalPayableAmount'=>$finalPayableAmount,'checkoutStatus'=>$setCheckoutStatus,'minorder'=>$setMinimumOrder,'deliveryCharges'=>$setDeliveryChrages,'tax'=>$getAdditionalCharges);
        }
        else
        {
            return array('messgae'=>'No data');
        }
    }
    public function firstCheckOut($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);
        // fetch user address
        $setAddressArray = array();
        $setproductArray = array();
        $getUserAddress = $getDynamicDB->select('*')->from('houdinv_user_address')->where('houdinv_user_address_user_id',$data['id'])->get()->result();
        if(count($getUserAddress) > 0)
        {
            foreach($getUserAddress as $getUserAddressList)
            {
                $mainAddress = str_replace(',',' ',$getUserAddressList->houdinv_user_address_user_address);
                $setAddressArray[] = array('addressId'=>$getUserAddressList->houdinv_user_address_id,'name'=>$getUserAddressList->houdinv_user_address_name,'contact'=>$getUserAddressList->houdinv_user_address_phone,'mainAddress'=>$mainAddress,'city'=>$getUserAddressList->houdinv_user_address_city,'zip'=>$getUserAddressList->houdinv_user_address_zip);
            }
        }
        else
        {
            $setAddressArray = array();
        }
        // fethc product list
        $getCartData = $getDynamicDB->select('*')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$data['id'])->get()->result();
        if(count($getCartData) > 0)
        {
            $getTotalPrice = 0;
            $getTotalDiscount = 0;
            $payablePrice = 0;
            foreach($getCartData as $getCartDataList)
            {
                $getCartQuantity = $getCartDataList->houdinv_users_cart_item_count;

                if($getCartDataList->houdinv_users_cart_item_id != 0 && $getCartDataList->houdinv_users_cart_item_id != "")
                {
                    // fetch product data
                    $getPorductsData = $getDynamicDB->select('*')->from('houdinv_products')->where('houdin_products_id',$getCartDataList->houdinv_users_cart_item_id)->get()->result();
                    $getPriceData = json_decode($getPorductsData[0]->houdin_products_price,true);
                    $getProductImage = json_decode($getPorductsData[0]->houdinv_products_main_images,true);
                    $setProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                    if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                    {
                        $setProductPrice = $getPriceData['price'];
                        $productDiscount = $getPriceData['price']-$getPriceData['discount'];
                        $getTotalPrice = $getTotalPrice+($getPriceData['price']*$getCartQuantity);
                        $getTotalDiscount = $getTotalDiscount+($getPriceData['discount']*$getCartQuantity);
                        $payablePrice = $payablePrice+($productDiscount*$getCartQuantity);
                    }
                    else
                    {
                        $setProductPrice = $getPriceData['price'];
                        $productDiscount = 0;
                        $getTotalPrice = $getTotalPrice+($getPriceData['price']*$getCartQuantity);
                        $getTotalDiscount = $getTotalDiscount+($productDiscount*$getCartQuantity);
                        $payablePrice = $payablePrice+($setProductPrice*$getCartQuantity);
                    }
                    $setproductArray[] = array('productId'=>$getCartDataList->houdinv_users_cart_item_id,'variantId'=>0,'productName'=>$getPorductsData[0]->houdin_products_title,'productImage'=>$setProductImage,'quantity'=>$getCartQuantity,'price'=>$setProductPrice,'discountPrice'=>$productDiscount);
                }
                else
                {
                    // fetch variant data
                    $getVariantData = $getDynamicDB->select('*')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getCartDataList->houdinv_users_cart_item_variant_id)->get()->result();
                    // fetch main productData
                    $getVariantProductData = $getDynamicDB->select('*')->from('houdinv_products')->where('houdin_products_id',$getVariantData[0]->houdin_products_variants_product_id)->get()->result();
                    // get variant image
                    if($getVariantData[0]->houdin_products_variants_image)
                    {
                        $setProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$getVariantData[0]->houdin_products_variants_image;
                    }
                    else
                    {
                        $getProductImage = json_decode($getVariantProductData[0]->houdinv_products_main_images,true);
                        $setProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                    }
                    // get product price
                    $getPriceData = json_decode($getVariantProductData[0]->houdin_products_price,true);
                    if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                    {
                        $setProductPrice = $getVariantData[0]->houdin_products_variants_prices;
                        $productDiscount = $getVariantData[0]->houdin_products_variants_prices-$getPriceData['discount'];
                        $getTotalPrice = $getTotalPrice+($getVariantData[0]->houdin_products_variants_prices*$getCartQuantity);
                        $getTotalDiscount = $getTotalDiscount+($getPriceData['discount']*$getCartQuantity);
                        $payablePrice = $payablePrice+($productDiscount*$getCartQuantity);
                    }
                    else
                    {
                        $setProductPrice = $getVariantData[0]->houdin_products_variants_prices;
                        $productDiscount = 0;
                        $getTotalPrice = $getTotalPrice+($getVariantData[0]->houdin_products_variants_prices*$getCartQuantity);
                        $getTotalDiscount = $getTotalDiscount+($productDiscount*$getCartQuantity);
                        $payablePrice = $payablePrice+($setProductPrice*$getCartQuantity);
                    }
                    $setproductArray[] = array('productId'=>0,'variantId'=>$getCartDataList->houdinv_users_cart_item_variant_id,'productName'=>$getVariantData[0]->houdin_products_variants_title,'productImage'=>$setProductImage,'quantity'=>$getCartQuantity,'price'=>$setProductPrice,'discountPrice'=>$productDiscount);
                }
            }
        }
        else
        {
            $setproductArray = array();
        }
        // set checkout and delivery charges
        $getCheckoutData = $getDynamicDB->select('minimum_order_amount,delivery_charges,free_delivery_val')->from('houdinv_possetting')->where('id',1)->get()->result();
        if(count($getCheckoutData) > 0)
        {
            if($getCheckoutData[0]->minimum_order_amount)
            {
                if($getCheckoutData[0]->minimum_order_amount <= $payablePrice)
                {
                    $setCheckoutStatus = 'yes';
                }
                else
                {
                    $setCheckoutStatus = 'no';
                }
                $setMinimumOrder = $getCheckoutData[0]->minimum_order_amount;
            }
            // if($getCheckoutData[0]->free_delivery_val <= $payablePrice)
            // {
            //     $setDeliveryChrages = $getCheckoutData[0]->delivery_charges;
            // }
            // else
            // {
            //     $setDeliveryChrages = 0;
            // }
        }
        else
        {
            $setDeliveryChrages = 0;
            $setMinimumOrder = 0;
            $setCheckoutStatus = 'yes';
        }
        $finalPayableAmount = $payablePrice;
        // $finalPayableAmount = $payablePrice+$setDeliveryChrages;
        return array('message'=>'Success','addressData'=>$setAddressArray,'productArray'=>$setproductArray,'totalamount'=>$getTotalPrice,'totalDiscount'=>$getTotalDiscount,'totalPayableAmount'=>$finalPayableAmount,'minorder'=>$setMinimumOrder,'checkoutStatus'=>$setCheckoutStatus);
    }
    public function fetchDeliveryCharge($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getCheckoutData = $getDynamicDB->select('minimum_order_amount,delivery_charges,free_delivery_val')->from('houdinv_possetting')->where('id',1)->get()->result();
        if($getCheckoutData[0]->free_delivery_val <= $data['amount'])
        {
            $setDeliveryChrages = $getCheckoutData[0]->delivery_charges;
        }
        else
        {
            $setDeliveryChrages = 0;
        }
        $setAmount = $data['amount']+$setDeliveryChrages;
        return array('initialAmount'=>$data['amount'],'deliveryCharge'=>$setDeliveryChrages,'finalAmount'=>$setAmount);
    }
    public function finalCheckOutData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $setData = strtotime(date('Y-m-d h:i:s'));
        $getCartItem = $getDynamicDB->select('houdinv_users_cart.*')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$data['id'])->get()->result();
        $getTotalPayableAmount = 0;
        foreach($getCartItem as $getCartItemList)
        {
            if($getCartItemList->houdinv_users_cart_item_id != 0 && $getCartItemList->houdinv_users_cart_item_id != "")
            {
                // Fetch product data
                $getProdcutDetails = $getDynamicDB->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getCartItemList->houdinv_users_cart_item_id)->get()->result();
                $getPriceData = json_decode($getProdcutDetails[0]->houdin_products_price,true);
                if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                {
                    $getActualPrice = $getPriceData['price'];
                    $getAfterDiscountPrice = $getPriceData['price']-$getPriceData['discount'];
                    $getTotalPaidPrice = $getAfterDiscountPrice*$getCartItemList->houdinv_users_cart_item_count;
                    $getTotalPayableAmount = $getTotalPayableAmount+$getTotalPaidPrice;
                }
                else
                {
                    $getActualPrice = $getPriceData['price'];
                    $getTotalPaidPrice = $getActualPrice*$getCartItemList->houdinv_users_cart_item_count;
                    $getTotalPayableAmount = $getTotalPayableAmount+$getTotalPaidPrice;
                }
                $setProducctArray[] = array('product_id'=>$getCartItemList->houdinv_users_cart_item_id,'variant_id'=>0,'product_Count'=>$getCartItemList->houdinv_users_cart_item_count,'product_actual_price'=>$getActualPrice,'total_product_paid_Amount'=>$getTotalPaidPrice);
            }
            else
            {
                // Fetch variant data
                $getVariantDetails = $getDynamicDB->select('houdin_products_variants_prices,houdin_products_variants_product_id')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getCartItemList->houdinv_users_cart_item_variant_id)->get()->result();
                // Get product details
                $getVariantProdcutDetails = $getDynamicDB->select('houdin_products_price')->from('houdinv_products')->where('houdin_products_id',$getVariantDetails[0]->houdin_products_variants_product_id)->get()->result();
                $getPriceData = json_decode($getVariantProdcutDetails[0]->houdin_products_price,true);
                if($getPriceData['discount'] != 0 && $getPriceData['discount'] != "")
                {
                    $getActualPrice = $getVariantDetails[0]->houdin_products_variants_prices;
                    $getAfterDiscountPrice = $getVariantDetails[0]->houdin_products_variants_prices-$getPriceData['discount'];
                    $getTotalPaidPrice = $getAfterDiscountPrice*$getCartItemList->houdinv_users_cart_item_count;
                    $getTotalPayableAmount = $getTotalPayableAmount+$getTotalPaidPrice;
                }
                else
                {
                    $getActualPrice = $getVariantDetails[0]->houdin_products_variants_prices;
                    $getTotalPaidPrice = $getActualPrice*$getCartItemList->houdinv_users_cart_item_count;
                    $getTotalPayableAmount = $getTotalPayableAmount+$getTotalPaidPrice;
                }
                $setProducctArray[] = array('product_id'=>0,'variant_id'=>$getCartItemList->houdinv_users_cart_item_variant_id,'product_Count'=>$getCartItemList->houdinv_users_cart_item_count,'product_actual_price'=>$getActualPrice,'total_product_paid_Amount'=>$getTotalPaidPrice);
            }
        }
        // apply coupon data
        if($data['couponId'] != "" && $data['discount'] != "")
        {
            $getTotalDiscountApplied = ($getTotalPayableAmount*$data['discount'])/100;
            $getTotalPayableAmount = $getTotalPayableAmount-$getTotalDiscountApplied;
            $setData = strtotime(date('Y-m-d h:i:s'));
            $getCouponUsage = $getDynamicDB->select('*')->from('houdinv_user_use_Coupon')->where('houdinv_user_use_Coupon_user_id',$data['id'])->where('houdinv_user_use_Coupon_coupon_id',$data['couponId'])->get()->result();
            if(count($getCouponUsage) > 0)
            {
                $houdinv_user_use_Coupon_used_Count = $getCouponUsage->houdinv_user_use_Coupon_used_Count+1;
                $houdinv_user_use_Coupon_discount = $getCouponUsage->houdinv_user_use_Coupon_discount+$data['discount'];
                $setUpdateArray = array('houdinv_user_use_Coupon_used_Count'=>$houdinv_user_use_Coupon_used_Count,'houdinv_user_use_Coupon_discount'=>$houdinv_user_use_Coupon_discount,
                'houdinv_user_use_Coupon_update_date'=>$setData
                );
                $getDynamicDB->where('houdinv_user_use_Coupon_user_id',$data['id']);
                $getDynamicDB->where('houdinv_user_use_Coupon_coupon_id',$data['couponId']);
                $getDynamicDB->update('houdinv_user_use_Coupon',$setUpdateArray);
            }
            else
            {
                $setInsetArray = array('houdinv_user_use_Coupon_user_id'=>$data['id'],'houdinv_user_use_Coupon_used_Count'=>'1',
                'houdinv_user_use_Coupon_coupon_id'=>$data['couponId'],'houdinv_user_use_Coupon_discount'=>$data['discount'],'houdinv_user_use_Coupon_add_date'=>$setData);
                $getDynamicDB->insert('houdinv_user_use_Coupon',$setInsetArray);
            }
            $setCouponIdData = $data['couponId'];
        }
        else
        {
            $setCouponIdData = 0;
        }
        // fetch delivery charges
        $getDeliveryCharges = $getDynamicDB->select('delivery_charges,free_delivery_val')->from('houdinv_possetting')->where('id',1)->get()->result();
        if(count($getDeliveryCharges) > 0)
        {
            if($getDeliveryCharges[0]->free_delivery_val != 0 && $getDeliveryCharges[0]->free_delivery_val != "")
            {
                if($getDeliveryCharges[0]->free_delivery_val <= $getTotalPayableAmount)
                {
                    $setDeliveryCharge = 0;
                }
                else
                {
                    $setDeliveryCharge = $getDeliveryCharges[0]->delivery_charges;
                }
            }
            else
            {
                $setDeliveryCharge = 0;
            }
        }
        else
        {
            $setDeliveryCharge = 0;
        }
        $setFinalPaidAmount = $setDeliveryCharge+$getTotalPayableAmount;

        $setCheckoutArray = array('houdinv_order_user_id'=>$data['id'],'houdinv_order_product_detail'=>json_encode($setProducctArray),
        'houdinv_order_type'=>'App','houdinv_order_delivery_type'=>$data['deliveryType'],'houdinv_order_payment_method'=>$data['paymentMethod'],'houdinv_payment_status'=>0,
        'houdinv_orders_discount'=>$setCouponIdData,'houdinv_orders_total_Amount'=>$setFinalPaidAmount,'houdinv_orders_total_paid'=>0,'houdinv_orders_total_remaining'=>$setFinalPaidAmount,
        'houdinv_order_comments'=>$data['customerComment'],
        'houdinv_delivery_charge'=>$setDeliveryCharge,'houdinv_order_confirmation_status'=>'unbilled','houdinv_orders_delivery_status'=>0,'houdinv_orders_delivery_status'=>$data['shippingAdd'],
        'houdinv_order_billing_address_id'=>$data['billingAdd'],'houdinv_order_created_at'=>$setData
        );
        $getDynamicDB->insert('houdinv_orders',$setCheckoutArray);
        $getOrderId = $getDynamicDB->insert_id();
        if($getOrderId)
        {
             // update account receivable(debtor) in case of order is COD
             if($setCheckoutArray['houdinv_order_payment_method'] != 'payumoney' && $setCheckoutArray['houdinv_order_payment_method'] != 'auth')
             {
                $getAccountData = $getDynamicDB->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id',1)->where('houdinv_accounts_detail_type_id','1')->get()->result();
                if(count($getAccountData) > 0)
                {
                    $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getAccountData[0]->houdinv_accounts_id,
                    'houdinv_accounts_balance_sheet_ref_type'=>'Pending Amount',
                    'houdinv_accounts_balance_sheet_pay_account'=>$data['id'],
                    'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                    'houdinv_accounts_balance_sheet_account'=>0,
                    'houdinv_accounts_balance_sheet_order_id'=>$getOrderId,
                    'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                    'houdinv_accounts_balance_sheet__increase'=>$setCheckoutArray['houdinv_orders_total_remaining'],
                    'houdinv_accounts_balance_sheet_decrease'=>0,
                    'houdinv_accounts_balance_sheet_deposit'=>0,
                    'houdinv_accounts_balance_sheet_final_balance'=>0,
                    'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                    $getDynamicDB->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                    // get amount
                    $getTotalAmount = $getDynamicDB->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                    ->where('houdinv_accounts_balance_sheet_account_id',$getAccountData[0]->houdinv_accounts_id)->get()->result();
                    if(count($getTotalAmount) > 0)
                    {
                        $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                    }
                    else
                    {
                        $setFinalAmount = 0;
                    }
                    $getDynamicDB->where('houdinv_accounts_id',$getAccountData[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                }
            }
            // end here
            $getDynamicDB->where('houdinv_users_cart_user_id',$data['id']);
            $getDynamicDB->delete('houdinv_users_cart');
            // fetch sms data
            $getUserPhone = $getDynamicDB->select('houdinv_user_contact')->from('houdinv_users')->where('houdinv_user_id',$data['id'])->get()->result();
            if(count($getUserPhone) > 0)
            {
                $getSMSCount = $getDynamicDB->select('houdinv_emailsms_stats_remaining_credits,houdinv_emailsms_stats_id')->from('houdinv_emailsms_stats')->where('houdinv_emailsms_stats_type','sms')->get()->result();
                if(count($getSMSCount) > 0)
                {
                    if($getSMSCount[0]->houdinv_emailsms_stats_remaining_credits > 0)
                    {
                        $id = "ACabde828d2f3aa46ee410a87a5c9dca4b";
                        $token = "6bf55a8df94b143d20c2dc6882c2cb48";
                        $url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
                        $from = "+14124447340";
                        $to = $getUserPhone[0]->houdinv_user_contact; // twilio trial verified number
                        $body = "Your order has been placed. Our team will confirm your order ASAP";
                        $data = array (
                            'From' => $from,
                            'To' => $getUserPhone[0]->houdinv_user_contact,
                            'Body' => $body,
                        );
                        $post = http_build_query($data);
                        $x = curl_init($url );
                        curl_setopt($x, CURLOPT_POST, true);
                        curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
                        curl_setopt($x, CURLOPT_POSTFIELDS, $post);
                        $y = curl_exec($x);
                        $getResponse = json_decode($y);
                        if($getResponse->error)
                        {
                            $setData = strtotime(date('Y-m-d h:i:s'));
                            $setInsertArray = array('houdinv_sms_log_credit_used'=>'0','houdinv_sms_log_status'=>'0','houdinv_sms_log_created_at'=>$setData);
                            $getDynamicDB->insert('houdinv_sms_log',$setInsertArray);
                        }
                        else
                        {
                            $setData = strtotime(date('Y-m-d h:i:s'));
                            $getRemainingSMS = $getSMSCount[0]->houdinv_emailsms_stats_remaining_credits-1;
                            $setUpdatedArray = array('houdinv_emailsms_stats_remaining_credits'=>$getRemainingSMS);
                            $getDynamicDB->where('houdinv_emailsms_stats_id',$getSMSCount[0]->houdinv_emailsms_stats_id);
                            $getDynamicDB->update('houdinv_emailsms_stats',$setUpdatedArray);
                            $setInsertArray = array('houdinv_sms_log_credit_used'=>'1','houdinv_sms_log_status'=>'1','houdinv_sms_log_created_at'=>$setData);
                            $getDynamicDB->insert('houdinv_sms_log',$setInsertArray);
                        }
                    }
                }
            }

            return array('message'=>'Success','orderid'=>$getOrderId);
        }
        else
        {
            return array('message'=>'Something went wrong. Please try again');
        }
    }
    public function IsCouponExist($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getCouponsData = $getDynamicDB->select("*")->from("houdinv_coupons")->where("houdinv_coupons_code",strtolower($data['code']))->where('houdinv_coupons_status','active')->get()->row();
        $date = strtotime(date("Y-m-d"));
        if($getCouponsData)
        {
            if(strtotime($getCouponsData->houdinv_coupons_valid_to)>=$date && strtotime($getCouponsData->houdinv_coupons_valid_from)<=$date)
            {
                if($data['totalAmount'] < $getCouponsData->houdinv_coupons_order_amount)
                {
                    $array = array("message"=>"Minimum amount for this coupon is ".$getCouponsData->houdinv_coupons_order_amount."");
                    return $array;
                }
                else
                {
                    // check user coupon access
                    $getCouponUsage = $getDynamicDB->select('houdinv_user_use_Coupon_used_Count')->from('houdinv_user_use_Coupon')->where('houdinv_user_use_Coupon_user_id',$data['customerId'])->where('houdinv_user_use_Coupon_coupon_id',$getCouponsData->houdinv_coupons_id)->get()->result();
                    if($getCouponUsage[0]->houdinv_user_use_Coupon_used_Count < $getCouponsData->houdinv_coupons_limit)
                    {
                        // Check user group
                        $user_group = $this->userGroup($data['customerId'],$data['shopName']);
                        $dataCustomerGroupCheck = $getDynamicDB->select("*")->from("houdinv_coupons_procus")->where('houdinv_coupons_procus_coupon_id',$getCouponsData->houdinv_coupons_id)
                        ->where('houdinv_coupons_procus_user_id',$data['customerId'])->or_where("houdinv_coupons_procus_user_group",$user_group)->get()->row();
                        $date = strtotime(date("Y-m-d"));
                        if($dataCustomerGroupCheck)
                        {
                            $array = array('message'=>'Success','discount'=>$getCouponsData->houdinv_coupons_discount_precentage,'couponId'=>$getCouponsData->houdinv_coupons_id);
                            return $array;
                        }
                        else
                        {
                            $user_product = $this->ShowCartData1($data['customerId'],$data['shopName']);
                            $dataProductCheck = $getDynamicDB->select("*")->from("houdinv_coupons_procus")->where('houdinv_coupons_procus_coupon_id',$getCouponsData->houdinv_coupons_id)
                                    ->where_in('houdinv_coupons_procus_product_id',$user_product['item'])
                                    ->get()->row();
                            if($dataProductCheck)
                            {
                                $array = array('message'=>'Success','discount'=>$getCouponsData->houdinv_coupons_discount_precentage,'couponId'=>$getCouponsData->houdinv_coupons_id);
                                return $array;
                            }
                            else
                            {
                                $array = array('message'=>'Coupon code is not valid');
                                return $array;
                            }
                        }
                    }
                    else
                    {
                        $array = array('message'=>'Your coupon usage limit is excced');
                        return $array;
                    }
                }
            }
           else
           {
                $array = array("message"=>"Coupon code is expired");
                return $array;
           }
        }
        else
        {
           $array = array('message'=>'Coupon code is not valid');
           return $array;
        }
    }
    public function userGroup($id,$shop)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shop);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $data = $getDynamicDB->select("*")->from("houdinv_users_group_users_list")->where('houdinv_users_group_users_id',$id)->get()->row();
        return $data->houdinv_users_group__user_group_id;
    }
    public function ShowCartData1($data,$shop)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shop);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $data = $getDynamicDB->select("*")->from("houdinv_users_cart") ->join("houdinv_products","houdinv_users_cart.houdinv_users_cart_item_id=houdinv_products.houdin_products_id")
                ->where("houdinv_users_cart.houdinv_users_cart_user_id",$id)->get()->result();
        $cat1 = array();
        foreach($data as $val)
        {
            $item[] =  $val->houdinv_users_cart_item_id;
            $cat = explode(",",$val->houdin_products_category);
            array_push($cat1,$cat);
        }
        $array = array("item"=>$item,"cat"=>$cat);
        return $array;
    }
    public function removeProductWishlistData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getDynamicDB->where('houdinv_users_whishlist_id',$data['wishlistId']);
        $getDeleteStatus = $getDynamicDB->delete('houdinv_users_whishlist');
        if($getDeleteStatus == 1)
        {
            $getUserId = $getDynamicDB->select('houdinv_users_whishlist_user_id')->from('houdinv_users_whishlist')->where('houdinv_users_whishlist_id',$data['wishlistId'])->get()->result();
            $getCartCount = $getDynamicDB->select('COUNT(houdinv_users_cart_id) as cartCount')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$getUserId[0]->houdinv_users_whishlist_user_id)->get()->result();
            return array('message'=>'Success','cartCount'=>$getCartCount[0]->cartCount);
        }
        else
        {
            return array('message'=>'Something went wrong. Please try again');
        }
    }
    public function removeProductcartData($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);
        $getUserId = $getDynamicDB->select('houdinv_users_cart_user_id')->from('houdinv_users_cart')->where('houdinv_users_cart_id',$data['cartid'])->get()->result();
        $getCartCount = $getDynamicDB->select('COUNT(houdinv_users_cart_id) as cartCount')->from('houdinv_users_cart')->where('houdinv_users_cart_user_id',$getUserId[0]->houdinv_users_cart_user_id)->get()->result();
        $getDynamicDB->where('houdinv_users_cart_id',$data['cartid']);
        $getDeleteStatus = $getDynamicDB->delete('houdinv_users_cart');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'Success','cartcount'=>$getCartCount[0]->cartCount-1);
        }
        else
        {
            return array('message'=>'Something went wrong. Please try again');
        }
    }
    public function fetchUserOrders($data)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($data['shopName']);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getUserOrders = $getDynamicDB->select('*')->from('houdinv_orders')->where('houdinv_order_user_id',$data['userid'])->order_by('houdinv_order_id','DESC')->get()->result();
        $setOrderDataArray = array();
        if(count($getUserOrders) > 0)
        {
            foreach($getUserOrders as $getUserOrdersList)
            {
                $getProducts = json_decode($getUserOrdersList->houdinv_order_product_detail,true);
                // get product detail
                if($getProducts[0]['variant_id'] == 0)
                {
                    $getProductList = $getDynamicDB->select('houdin_products_title,houdinv_products_main_images')->from('houdinv_products')->where('houdin_products_id',$getProducts[0]['product_id'])->get()->result();
                    // set product image
                    $getProductImage = json_decode($getProductList[0]->houdinv_products_main_images,true);
                    $setImageData = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                    $setProductTitle = $getProductList[0]->houdin_products_title;
                }
                else
                {
                    $getVariantList = $getDynamicDB->select('houdin_products_variants_title,houdin_products_variants_image')->from('houdinv_products_variants')->where('houdin_products_variants_id',$getProducts[0]['variant_id'])->get()->result();
                    $setImageData = $this->session->userdata('vendorURL')."upload/productImage/".$getVariantList[0]->houdin_products_variants_image;
                    $setProductTitle = $getVariantList[0]->houdin_products_variants_title;
                }
                if($getUserOrdersList->houdinv_orders_deliverydate)
                {
                    $setDeliveryDate = date('d-m-Y',strtotime($getUserOrdersList->houdinv_orders_deliverydate));
                }
                else
                {
                    $setDeliveryDate = "";
                }
                $setOrderDataArray[] = array('orderId'=>$getUserOrdersList->houdinv_order_id,'productName'=>$setProductTitle,'productimage'=>$setImageData,'deliverystatus'=>$getUserOrdersList->houdinv_order_confirmation_status,'deliverydate'=>$setDeliveryDate);
            }
            if(count($setOrderDataArray) > 0)
            {
                $orderSetting = orderSetting($data['shopName']);
                return array('message'=>'Success','data'=>$setOrderDataArray,'orderSetting'=>$orderSetting);
            }
            else
            {
                return array('message'=>'No data');
            }
        }
        else
        {
            return array('message'=>'No data');
        }
    }
    public function cancelOrderRequest($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);


        $getDynamicWebDb->where('houdinv_order_id',$data);
        $getDeleteStatus = $getDynamicWebDb->update('houdinv_orders',array('houdinv_order_confirmation_status'=>'cancel request'));
        if($getDeleteStatus == 1)
        {
            $getEMailCount = fetchEmailCount($this->session->userdata('shopName'));
            if($getEMailCount[0]->houdinv_emailsms_stats_remaining_credits > 0)
            {
                $getUserId = $getDynamicWebDb->select('houdinv_user_email')->from('houdinv_users')->where('houdinv_user_id',$this->session->userdata('userAuth'))->get()->result();
                $getBusinessId = $getDynamicWebDb->select('houdinv_shop_order_email')->from('houdinv_shop_detail')->where('houdinv_shop_id',1)->get()->result();
            }
            return array('message'=>'yes','userId'=>$getUserId[0]->houdinv_user_email,'count'=>$getEMailCount[0]->houdinv_emailsms_stats_remaining_credits,'businessId'=>$getBusinessId[0]->houdinv_shop_order_email);
        }
        else
        {
            return array('message'=>'no');
        }
    }



        public function returnOrderRequest($data)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);


        $getDynamicWebDb->where('houdinv_order_id',$data);
        $getDeleteStatus = $getDynamicWebDb->update('houdinv_orders',array('houdinv_order_confirmation_status'=>'return request'));
        if($getDeleteStatus == 1)
        {
            $getEMailCount = fetchEmailCount($this->session->userdata('shopName'));
            if($getEMailCount[0]->houdinv_emailsms_stats_remaining_credits > 0)
            {
                $getUserId = $getDynamicWebDb->select('houdinv_user_email')->from('houdinv_users')->where('houdinv_user_id',$this->session->userdata('userAuth'))->get()->result();
                $getBusinessId = $getDynamicWebDb->select('houdinv_shop_order_email')->from('houdinv_shop_detail')->where('houdinv_shop_id',1)->get()->result();
            }
            return array('message'=>'yes','userId'=>$getUserId[0]->houdinv_user_email,'count'=>$getEMailCount[0]->houdinv_emailsms_stats_remaining_credits,'businessId'=>$getBusinessId[0]->houdinv_shop_order_email);
        }
        else
        {
            return array('message'=>'no');
        }
    }


    public function SaveQuatation($array)
    {


        if($array)
        {
         $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);
          $getDynamicWebDb->insert("houdinv_shop_ask_quotation",$array);
          return $getDynamicWebDb->insert_id();

        }
    }

    public function GetOrdeDetail($order,$user)
    {
        $getWebDB = switchDynamicDatabase();
        $getDynamicWebDb = $this->load->database($getWebDB,true);
   $orderDetail =   $getDynamicWebDb->select("*")->from("houdinv_orders")
     ->where("houdinv_order_id",$order)->where("houdinv_order_user_id",$user)
     ->get()->row();

     $product = json_decode($orderDetail->houdinv_order_product_detail);
     $all_product = array();
     $productDetail = array();
    // print_R($product);
     foreach($product as $detail)
     {


            if($detail->product_id > 0)
            {
              //main

                $productDetail =   $getDynamicWebDb->select("houdin_products_id as Product_id,houdin_products_title as title")->from("houdinv_products")
                 ->where("houdin_products_id",$detail->product_id)
                 ->get()->row();
                     if($productDetail)
                     {
                        $productDetail->link = "Productlistview/".$detail->product_id;
                        $productDetail->allDetail = $detail;

                     }
            }
            else if($detail->product_id == 0)
            {

                // variant
                $productDetail =   $getDynamicWebDb->select("houdin_products_variants_id as Product_id,houdin_products_variants_title as title")->from("houdinv_products_variants")
                 ->where("houdin_products_variants_id",$detail->variant_id)
                 ->get()->row();

                      if($productDetail)
                     {
                        $productDetail->link = "Productlistview/variant/".$detail->variant_id;
                            $productDetail->allDetail = $detail;
                     }

            }

            $all_product[] = $productDetail;
     }
      $orderDetail->product_all = $all_product;

      return $orderDetail;
    }
    public function updateOrderTransaction($order,$transaction,$shop,$id)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shop);
        $getDynamicDB = $this->load->database($getAppDB,true);
        // update accounts data
        if($transaction['houdinv_transaction_status'] == 'success')
        {
        $getUndepositedAccount = $getDynamicDB->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
        if(count($getUndepositedAccount) > 0)
        {
            $getCustomerId = $getDynamicDB->select('houdinv_order_user_id')->from('houdinv_orders')->where('houdinv_order_id',$id)->get()->result();
            if(count($getCustomerId) > 0)
            {
                $getCustomerId = $getCustomerId;
            }
            else
            {
                $getCustomerId = "";
            }
            $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
            'houdinv_accounts_balance_sheet_ref_type'=>'Online Transaction',
            'houdinv_accounts_balance_sheet_pay_account'=>$getCustomerId[0]->houdinv_order_user_id,
            'houdinv_accounts_balance_sheet_payee_type'=>'customer',
            'houdinv_accounts_balance_sheet_account'=>0,
            'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
            'houdinv_accounts_balance_sheet__increase'=>$transaction['houdinv_transaction_amount'],
            'houdinv_accounts_balance_sheet_decrease'=>0,
            'houdinv_accounts_balance_sheet_deposit'=>0,
            'houdinv_accounts_balance_sheet_final_balance'=>0,
            'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
            $getDynamicDB->insert('houdinv_accounts_balance_sheet',$setInsertArray);
            // get amount
            $getTotalAmount = $getDynamicDB->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
            ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
            if(count($getTotalAmount) > 0)
            {
                $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
            }
            else
            {
                $setFinalAmount = 0;
            }
            $getDynamicDB->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
        }
        }
        // end here
        if(count($order) > 0)
        {
            $getDynamicDB->where('houdinv_order_id',$id);
            $getOrderStatus = $getDynamicDB->update('houdinv_orders',$order);
            if($getOrderStatus)
            {
                $getTransaction = $getDynamicDB->insert('houdinv_transaction',$transaction);
                if($getTransaction)
                {
                    return array('message'=>'Your order has been successfully placed');
                }
                else
                {
                    return array('message'=>'Your order has been successfully placed');
                }
            }
            else
            {
                $getTransaction = $getDynamicDB->insert('houdinv_transaction',$transaction);
                if($getTransaction)
                {
                    return array('message'=>'Your order status is not updated. Please contact to our customer care with order id '.$id.' and transaction id '.$transaction['houdinv_transaction_transaction_id'].'');
                }
                else
                {
                    return array('message'=>'Somthing went wrong. Please try again');
                }
            }
        }
        else
        {
            $getTransaction = $getDynamicDB->insert('houdinv_transaction',$transaction);
            if($getTransaction)
            {
                return array('message'=>'Your order has been successfully placed. Transaction fail');
            }
            else
            {
                return array('message'=>'Your order has been successfully placed. Transaction fail');
            }
        }
    }
    public function getTrackingDetails($shop,$order)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shop);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getTrackingDetails = $getDynamicDB->select('houdinv_order_confirmation_status,houdinv_orders_deliverydate')->from('houdinv_orders')->where('houdinv_order_id',$order)->get()->result();
        if($getTrackingDetails[0]->houdinv_order_confirmation_status == 'unbilled')
        {
            $setCurrentStatus = "Not Confirm";
            $setStatus = 0;
        }
        else if($getTrackingDetails[0]->houdinv_order_confirmation_status == 'billed')
        {
            $setCurrentStatus = "Order Confirm";
            $setStatus = 1;
        }
        else if($getTrackingDetails[0]->houdinv_order_confirmation_status == 'assigned')
        {
            $setCurrentStatus = "Delivery Service Assigned";
            $setStatus = 2;
        }   
        else if($getTrackingDetails[0]->houdinv_order_confirmation_status == 'order pickup')
        {
            $setCurrentStatus = "Order picked up";
            $setStatus = 2;
        }   
        else if($getTrackingDetails[0]->houdinv_order_confirmation_status == 'Delivered')
        {
            $setCurrentStatus = "Order delivered";
            $setStatus = 3;
        } 
        else if($getTrackingDetails[0]->houdinv_order_confirmation_status == 'Not Delivered')
        {
            $setCurrentStatus = "Order not delivered";
            $setStatus = 2;
        }     
        else if($getTrackingDetails[0]->houdinv_order_confirmation_status == 'cancel')
        {
            $setCurrentStatus = "Order cancel";
            $setStatus = 2;
        }
        else if($getTrackingDetails[0]->houdinv_order_confirmation_status == 'return')
        {
            $setCurrentStatus = "Order return";
            $setStatus = 2;
        }     
        else if($getTrackingDetails[0]->houdinv_order_confirmation_status == 'return request')
        {
            $setCurrentStatus = "Request for return";
            $setStatus = 2;
        }     
        else if($getTrackingDetails[0]->houdinv_order_confirmation_status == 'cancel request')
        {
            $setCurrentStatus = "Request for cancel";
            $setStatus = 2;
        }   

        
        $setArray = array('currentStatus'=>$setCurrentStatus,'orderStatus'=>$setStatus,'deliveryDate'=>$getTrackingDetails[0]->houdinv_orders_deliverydate);
        return $setArray;
    }
    public function getOrderDetails($shop,$order)
    {
        $this->load->helper('dynamicdatabaseapp_helper');
        $getAppDB = switchDynamicDatabaseApp($shop);
        $getDynamicDB = $this->load->database($getAppDB,true);

        $getOrderDetails = $getDynamicDB->select('houdinv_orders.*')->from('houdinv_orders')->where('houdinv_orders.houdinv_order_id',$order)->get()->result();
        // set order status
        if($getOrderDetails[0]->houdinv_order_confirmation_status == 'unbilled')
        {
            $setCurrentStatus = "Not Confirm";
        }
        else if($getOrderDetails[0]->houdinv_order_confirmation_status == 'billed')
        {
            $setCurrentStatus = "Order Confirm";
        }
        else if($getOrderDetails[0]->houdinv_order_confirmation_status == 'assigned')
        {
            $setCurrentStatus = "Delivery Service Assigned";
        }   
        else if($getOrderDetails[0]->houdinv_order_confirmation_status == 'order pickup')
        {
            $setCurrentStatus = "Order picked up";
        }   
        else if($getOrderDetails[0]->houdinv_order_confirmation_status == 'Delivered')
        {
            $setCurrentStatus = "Order delivered";
        } 
        else if($getOrderDetails[0]->houdinv_order_confirmation_status == 'Not Delivered')
        {
            $setCurrentStatus = "Order not delivered";
        }     
        else if($getOrderDetails[0]->houdinv_order_confirmation_status == 'cancel')
        {
            $setCurrentStatus = "Order cancel";
        }
        else if($getOrderDetails[0]->houdinv_order_confirmation_status == 'return')
        {
            $setCurrentStatus = "Order return";
        }     
        else if($getOrderDetails[0]->houdinv_order_confirmation_status == 'return request')
        {
            $setCurrentStatus = "Request for return";
        }     
        else if($getOrderDetails[0]->houdinv_order_confirmation_status == 'cancel request')
        {
            $setCurrentStatus = "Request for cancel";
        }   
        // set billing and shipping address
        $getBillingAddress = $getOrderDetails[0]->houdinv_order_billing_address_id;
        $getShippingAddress = $getOrderDetails[0]->houdinv_order_delivery_address_id;
        // get billing address
        $getBillingAddres = $getDynamicDB->select('*')->from('houdinv_user_address')->where('houdinv_user_address_id',$getBillingAddress)->get()->result();
        $setBillingAddressArray = array('address'=>$getBillingAddres[0]->houdinv_user_address_user_address,'city'=>$getBillingAddres[0]->houdinv_user_address_city,'zip'=>$getBillingAddres[0]->houdinv_user_address_zip);
        if($getBillingAddress == $getShippingAddress)
        {
            $setShippingAddressArray = array('address'=>$getBillingAddres[0]->houdinv_user_address_user_address,'city'=>$getBillingAddres[0]->houdinv_user_address_city,'zip'=>$getBillingAddres[0]->houdinv_user_address_zip);
        }
        else
        {
            $getBillingAddres = $getDynamicDB->select('*')->from('houdinv_user_address')->where('houdinv_user_address_id',$getShippingAddress)->get()->result();
            $setShippingAddressArray = array('address'=>$getBillingAddres[0]->houdinv_user_address_user_address,'city'=>$getBillingAddres[0]->houdinv_user_address_city,'zip'=>$getBillingAddres[0]->houdinv_user_address_zip);
        }
        // order product details
        $setProductArray = array();
        $setItemTotal = 0;
        if($getOrderDetails[0]->houdinv_confirm_order_product_detail)
        {
            $setOrderData = json_decode($getOrderDetails[0]->houdinv_confirm_order_product_detail,true);
            for($index = 0; $index < count($setOrderData); $index++)
            {
                if($setOrderData[$index]['product_id'] != 0 && $setOrderData[$index]['product_id'] != "")
                {
                    $getProductDetails = $getDynamicDB->select('houdin_products_title,houdinv_products_main_images')->from('houdinv_products')->where('houdin_products_id',$setOrderData[$index]['product_id'])->get()->result();
                    $getProductName = $getProductDetails[0]->houdin_products_title;
                    $imageData = json_decode($getProductDetails[0]->houdinv_products_main_images,true);
                    $getImage = "".$this->session->userdata('vendorURL')."/upload/productImage/".$imageData[0];
                }
                else
                {
                    $getProductDetails = $getDynamicDB->select('houdin_products_variants_title,houdin_products_variants_image')->from('houdinv_products_variants')->where('houdin_products_variants_id',$setOrderData[$index]['variant_id'])->get()->result();
                    $getProductName = $getProductDetails[0]->houdin_products_variants_title;
                    $getImage = "".$this->session->userdata('vendorURL')."/upload/productImage/".$getProductDetails[0]->houdin_products_variants_image;
                }
                $setItemTotal = $setItemTotal+$setOrderData[$index]['product_actual_price'];
                $setProductArray[] = array('productName'=>$getProductName,'productImage'=>$getImage,'productPrice'=>$setOrderData[$index]['product_actual_price'],'productQuantity'=>$setOrderData[$index]['product_Count']);
            }
        }
        else
        {
            $setOrderData = json_decode($getOrderDetails[0]->houdinv_order_product_detail,true);
            for($index = 0; $index < count($setOrderData); $index++)
            {
                if($setOrderData[$index]['product_id'] != 0 && $setOrderData[$index]['product_id'] != "")
                {
                    $getProductDetails = $getDynamicDB->select('houdin_products_title,houdinv_products_main_images')->from('houdinv_products')->where('houdin_products_id',$setOrderData[$index]['product_id'])->get()->result();
                    $getProductName = 
                    $getProductDetails[0]->houdin_products_title;
                    $imageData = json_decode($getProductDetails[0]->houdinv_products_main_images,true);
                    $getImage = "".$this->session->userdata('vendorURL')."/upload/productImage/".$imageData[0];
                }
                else
                {
                    $getProductDetails = $getDynamicDB->select('houdin_products_variants_title,houdin_products_variants_image')->from('houdinv_products_variants')->where('houdin_products_variants_id',$setOrderData[$index]['variant_id'])->get()->result();
                    $getProductName = $getProductDetails[0]->houdin_products_variants_title;
                    $getImage = "".$this->session->userdata('vendorURL')."/upload/productImage/".$getProductDetails[0]->houdin_products_variants_image;
                }
                $setItemTotal = $setItemTotal+$setOrderData[$index]['product_actual_price'];
                $setProductArray[] = array('productName'=>$getProductName,'productImage'=>$getImage,'productPrice'=>$setOrderData[$index]['product_actual_price'],'productQuantity'=>$setOrderData[$index]['product_Count']);
            }
        }
        $getOrderDate = date('d-m-Y',$getOrderDetails[0]->houdinv_order_created_at);
        if($getOrderDetails[0]->houdinv_delivery_charge)
        {
            $setDeliveryCharge = $getOrderDetails[0]->houdinv_delivery_charge;
        }
        else
        {
            $setDeliveryCharge = 0;
        }
        if($getOrderDetails[0]->houdinv_orders_discount)
        {
            $setOrderDiscount = $getOrderDetails[0]->houdinv_orders_discount;
        }
        else
        {
            $setOrderDiscount = 0;
        }
        if($getOrderDetails[0]->houdinv_order_payment_method == 'cod')
        {
            $setPaymentMethod = "Cash on delivery";
        }
        else
        {
            $setPaymentMethod = "Card";
        }
        $setOrderDetail = array('orderDate'=>$getOrderDate,
        'orderNumber'=>$getOrderDetails[0]->houdinv_order_id,
        'orderTotal'=>$getOrderDetails[0]->houdinv_orders_total_Amount,
        'productDetail'=>$setProductArray,
        'orderStatus'=>$setCurrentStatus,
        'orderDeliveryDate'=>$getOrderDetails[0]->houdinv_orders_deliverydate,
        'orderPaymentMethod'=>$setPaymentMethod,
        'billingAddress'=>$setBillingAddressArray,
        'shippingAddress'=>$setShippingAddressArray,
        'ItemPrice'=>$setItemTotal,
        'DeliveryCharge'=>$setDeliveryCharge,
        'Discount'=>$setOrderDiscount,
        'totalAmount'=>$getOrderDetails[0]->houdinv_orders_total_Amount,
        'totalPaidAmount'=>$getOrderDetails[0]->houdinv_orders_total_paid,
        'totalRemainingAmount'=>$getOrderDetails[0]->houdinv_orders_total_remaining
    );
    return $setOrderDetail;
    }
}
?>
