<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style type="text/css">
  .checkbox {
    padding-left: 0px;
}
@media (max-width:767px){
  .checkbox {
    padding-left: 20px;
}
}
</style> 
 <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
 
           <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Tax Items</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>tax/Taxlanding">Tax</a></li>
                  <li class="active">Tax Items</li>
                  </ol>
                  </div>
             
            </div>
           <!--END Page-Title --> 
            <div class="row m-t-20">
              <div class="col-lg-12">
                <div class="card-box">
                    <form action="#" data-parsley-validate novalidate>
                    <h4>Add a new tax item</h4>
                    <div class="form-group">
                      <label for="userName">Tax Type (Enter the tax type like VAT, CST etc.)</label>
                      <input type="text" name="nick" parsley-trigger="change" required placeholder="" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                      <label for="userName">Tax Rate (Enter the applicable tax percentage rate without % sign.)</label>
                      <input type="text" name="nick" parsley-trigger="change" required placeholder="" class="form-control" id="userName">
                    </div>

                    <div class="checkbox checkbox-custom m-b-10">
                 <input id="checkbox11" type="checkbox" checked="">
                 <label>Is this tax applicable to shipping charges too?</label>
              </div>

              <div class="checkbox checkbox-custom m-b-10">
                 <input id="checkbox11" type="checkbox" >
                 <label>Will Region tax rule/Subdivision tax rule be applicable on this item?</label>
              </div>
                     
                    <div class="form-group m-b-0">
                      <button class="btn btn-primary waves-effect waves-light" type="Save">
                        Save
                      </button>
                      
                    </div>
                   
                  </form>
                </div>
              </div> 
            </div>           
      </div>
    </div>
  </div>

<?php $this->load->view('Template/footer.php') ?>

<script>
$(document).ready(function(){

    $(".select2").select2();
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
