<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');


$images_all = json_decode($product['main_product']->houdinv_products_main_images);
$price_all = json_decode($product['main_product']->houdin_products_price,true);
$final_price = $product['main_product']->houdin_products_final_price;
$overall_rating = $product['reviews_sum']->houdinv_product_review_rating;
$total_rating = count($product['reviews']);
$final_rating = $overall_rating/$total_rating;
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
    <!-- ================== END HEADER SECTION ================== -->
    <!-- ================== START MAIN SECTION ================== -->
    <div class="main" role="main">
        <!-- Start page location -->
        <div class="page-location">
            <div class="page-location-layer">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-location-title">
                                <h3>Products</h3>
                            </div>
                            <!-- <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Shop page</li>
                                </ol>
                            </nav> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
        <div class="filter-list">
                            <div class="row">
                              <?php
                              if($this->session->flashdata('error'))
                              {
                                      echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                              }
                              if($this->session->flashdata('success'))
                              {
                                      echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                              }
                              ?>
                              <?php
                              $count_product=count($productList);
                              if($count_product==0)
                              {?>
                                <div class="container"><div class="col-sm-12 text-center"><h3>Products are not available</h3></div></div>
                              <?php } else { ?>
                                <div class="col-md-12 col-12">
                                    <ul class="Sort-type d-flex justify-content-md-end justify-content-center">
                                        <li class="sort-title">Sort by : </li>
                                        <li class="Sort-by-show">
                                          <span style="float: right;"><?php echo form_open(base_url( 'Productlist/'.$this->uri->segment('2').'/'.$this->uri->segment('3').'/'.$this->uri->segment('4')), array( 'id' => 'Productlist', 'method'=>'get' ));?>
                                          <select class="sort-select" name="sort">

                                            <option value="">select</option>
                                            <option value="name" <?php if($_REQUEST['sort'] =="name") { echo "selected"; }?> >Name Descending</option>
                                            <option value="date" <?php if($_REQUEST['sort'] =="date") { echo "selected"; }?>>Date Descending</option>
                                            <option value="price" <?php if($_REQUEST['sort'] =="pricehl") { echo "selected"; }?>>Price High to Low</option>
                                            <option value="price" <?php if($_REQUEST['sort'] =="pricelh") { echo "selected"; }?>>Price Low to High</option>

                                          </select>
                                          <?php echo form_close(); ?></span>
                                        </li>

                                        <li><label class="line31 float-left">Showing: <?php echo $count; ?></label></li>
                                    </ul>
                                </div>
                              <?php } ?>
                            </div>
                        </div>
                      </div>
        <div class="shop-products">
            <div class="container">
                <div class="row appendProductList">






                  <?php
                  foreach($productList as $productListData)
                  {
                      $getProductImage = json_decode($productListData->houdinv_products_main_images,true);
                      $getProductPrice = json_decode($productListData->houdin_products_price,true);
                  ?>
                    <div class="col-lg-3 col-md-6">
                        <div class="item">
                            <div class="single-item">
                            <?php 
                            if($getProductImage[0])
                            {
                                $getMainImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                            }
                            else
                            {
                                $getMainImage = base_url()."Extra/noPhotoFound.png";
                            }
                            ?>
                                <img src="<?php echo $getMainImage; ?>" alt="" class="img-fluid" style="width: 100%;">
                                <?php if($productListData->houdinv_products_total_stocks <= 0) { ?>
                                <!--<div class="new_badge">Out of Stock</div>-->
                                <?php } ?>
                                 <?php
                                if($productListData->houdinv_products_main_quotation == 1)
                                {
                                ?>
                                <div class="model_qute_show ask-question" data-id="<?php echo $productListData->houdin_products_id ?>"  data-toggle="modal" >Ask Quotation
                                </div>
                                <?php
                                }
                                ?>
                                  <div class="add-product">

                                    <ul>
                                        <li>
                                          <?php
                                          if($productListData->houdinv_products_total_stocks <= 0)
                                          {
                                          if($storeSetting[0]->receieve_order_out == 1)
                                          {
                                              ?>
                                          <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-shopping-basket"></i></a>
                                        <?php  }
                                        }
                                        else
                                        {
                                        ?>
                                        <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-shopping-basket"></i></a>

                                      <?php }
                                      ?>
                                        </li>
                                        <li>
                                          <a href="javascript:;" class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-heart"></i></a>
                                        </li>
                                        <li><a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-eye"></i></a></li>
                                    </ul>
                                </div>
                                <!-- <div class="discount-tag">
                                    <span>40% <span>off</span></span>
                                </div> -->
                            </div>
                            <div class="price-info">
                                <a href="#"><?php echo substr($productListData->houdin_products_title,0, 25).".." ; ?></a>
                                <div class="clearfix mt-2">
                                    <!-- <ul class="d-flex pull-left">
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul> -->

                                    <h5 class="pull-right m-0"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $productListData->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <del><?php echo $getProductPrice['sale_price']; ?></del>&nbsp;<?php echo $productListData->houdin_products_final_price; ?> <?php  } ?></small></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                  <?php } ?>






                </div>
            </div>
        </div>


        <div class="load-more">
        <div class="container">
            <div class="pages pages2 text-center">
              <?php
              if($count > 0)
              {
                  if($count >= 12)
                  {
                      $remain = $count-12;
                  }
                  else
                  {
                      $remain = 0;
                  }
              ?>


              <?php
              if($remain > 0)
              {
              ?>
              <div class=""><button type="button" data-remain="<?php echo $remain ?>" data-last="12" class="btn btn-sm  acc_btn setLoadMoredata btn-block load-more" style="margin-top: 10px;padding: 4px;border-radius: 4px;margin-bottom: 25px;">Load More&nbsp;<i class="fa fa-spinner fa-spin setSpinnerData" style="display:none;"></i></button></div>
              <?php }
              ?>

              <?php }
              ?>
            </div>
            </div>
        </div>



        <!-- Start business logo -->

    </div>
    <!-- ================== END MAIN SECTION ================== -->
    <!-- ================== START FOOTER SECTION ================== -->
    <!-- Footer Part Start -->
    <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view("".$getShopName."/Template/footer"); ?>

    <script>
    $('.sort-select').on('change',function()
    {
      document.getElementById('Productlist').submit();
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){

        $(document).on('click','.setLoadMoredata',function(){

            $(this).prop('disabled',true);
            $('.setSpinnerData').show();
            var getCategory = '<?php echo $this->uri->segment('2') ?>';
            var getSubCategory = '<?php echo  $this->uri->segment('3') ?>';
            var getSubSubCategory = '<?php echo $this->uri->segment('4') ?>';
            var sortBy = '<?php echo $_REQUEST['sort'] ?>';
            var dataLast = $(this).attr('data-last');
            var dataRemain = $(this).attr('data-remain');
            $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>Ajaxcontroller/fetchProductPaginationData",
            data: {"dataLast": dataLast,"dataRemain":dataRemain,"getCategory":getCategory,"getSubCategory":getSubCategory,"getSubSubCategory":getSubSubCategory,"sortBy":sortBy},
            success: function(datas) {
                $('.setSpinnerData').hide();
                $('.setLoadMoredata').prop('disabled',false);
                var setData = jQuery.parseJSON(datas);

                   var grid = "";
                for(var i=0; i < setData.main.length ; i++)
                {
                    var $productListData = setData.main[i];
                    var textsplit = $productListData.houdin_products_title;
                    var $setCartDesign = "";
                      var $setStockBadge = "";
                      var $settingData = setData.settingData;
                        if($productListData.houdinv_products_total_stocks <= 0)
                            {
                                if($settingData[0].receieve_order_out == 1)
                                {

                                    $setCartDesign = '<a class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><i class="fa fa-shopping-basket"></i></a>';
                                }
                            }
                            else
                            {
                                $setCartDesign = '<a class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><i class="fa fa-shopping-basket"></i></a>';
                            }
                            if($productListData.houdinv_products_total_stocks <= 0)
                            {
                                $setStockBadge = '<div class="new_badge">Out of Stock</div>';
                            }

                      var   $getProductImage = jQuery.parseJSON($productListData.houdinv_products_main_images);
                       var     $getProductPrice = jQuery.parseJSON($productListData.houdin_products_price);
                          console.log($getProductPrice);
                            if($getProductPrice.discount != 0 && $getProductPrice.discount  != "")
                            {
                               var $DiscountedPrice = $getProductPrice.price-$getProductPrice.discount;
                               var $originalPrice = $getProductPrice.price;
                            }
                            else
                            {
                              var  $DiscountedPrice = 0;
                              var  $originalPrice = $getProductPrice.price;
                            }
                            if($DiscountedPrice == 0)
                            {
                              var  $setPrice = $originalPrice;
                            }
                            else
                            {
                               var $setPrice = "<del>"+$originalPrice+"</del>&nbsp;"+$DiscountedPrice+"";
                            }
                            if($getProductImage[0])
                            {
                                var getMainImage = '<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/'+$getProductImage[0]+'';
                            }
                            else
                            {
                                var getMainImage = '<?php echo base_url() ?>Extra/noPhotoFound.png';
                            }
grid+='<div class="col-lg-3 col-md-6"><div class="item"><div class="single-item"><img src="'+getMainImage+'" alt="" class="img-fluid"><div class="add-product"><ul><li>'+$setCartDesign+'</li><li><a href="javascript:;" class="Add_to_whishlist_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><i class="fa fa-heart"></i></a></li>                    <li><a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'"><i class="fa fa-eye"></i></a></li></ul></div></div><div class="price-info"><a href="#">'+textsplit.substring(0,25)+'</a><div class="clearfix mt-2"><h5 class="pull-right m-0"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;'+$setPrice+'</h5></div></div></div></div>';

                }
                $('.appendProductList').append(grid);


                $('.setLoadMoredata').attr('data-last',setData.last);

            }
            });
        });
    });
    </script>

    <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('keyup','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        })
        $(document).on('change','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        });
    })
    </script>
