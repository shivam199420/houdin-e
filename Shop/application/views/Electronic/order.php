<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 
<style type="text/css">
        ol.progtrckr {
    margin: 0;
    padding: 0;
        padding-bottom: 5%;
    list-style-type none;
}

ol.progtrckr li {
    display: inline-block;
    text-align: center;
    line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
ol.progtrckr[data-progtrckr-steps="5"] li { width: %; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
}
ol.progtrckr li.progtrckr-todo {
    color: silver;
    border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
    content: "\00a0\00a0";
}
ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 50%;
    line-height: 1em;
}
ol.progtrckr li.progtrckr-done:before {
    content: "\2713";
    color: white;
    background-color: yellowgreen;
    height: 2.2em;
    width: 2.2em;
    line-height: 2.2em;
    border: none;
    border-radius: 2.2em;
}
ol.progtrckr li.progtrckr-todo:before {
    content: "\039F";
    color: silver;
    background-color: white;
    font-size: 2.2em;
    bottom: -1.2em;
}
@media(max-width: 767px){

  ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
    width: 100%;
}
ol.progtrckr li.progtrckr-todo {
    color: silver;
    border-bottom: 4px solid silver;
    width: 100%;
}
ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 48%;
    line-height: 1em;
}
}
</style>
  <!-- Content -->
  <div id="content">
  <!-- Linking -->
    <div class="linking">
      <div class="container">
        <ol class="breadcrumb">
          <li class="active">Order</li>
        </ol>
      </div>
    </div>
    <!-- Ship Process -->
    <section class="padding-top-30">
      <div class="container">
        <div class="row">
			<?php
			if($this->session->flashdata('success'))
			{
				echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
			}
			if($this->session->flashdata('error'))
			{
				echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
			}
            ?>
            </div></div></section>
    <?php
    if($message=='No data')
    {
    ?>
    <section class="padding-top-30 padding-bottom-100">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <!-- Login Your Account -->
            <h5>Order</h5>
            <hr>
            <p>You have no order. Please <a href="<?php echo base_url(); ?>" style="text-decoration: underline!important;">check here</a> to order now.</p>
          </div>
        </div>
      </div>
    </section>
    <?php
    }
    else
    {
    ?>
     <!-- Shopping Cart -->
     <section class="shopping-cart padding-bottom-60">
      <div class="container">
      <div class="table-responsive">
        <table class="table">
          <thead>
          <tr>
            <th class="cpt_no">Order Id</th>
            <th class="cpt_img">Image</th>
            <th>Status</th>
            <th class="cpt_r">View</th>
            <th class="cpt_r">Action</th>
            </tr>
          </thead>
          <tbody>
          <?php
          $i=1;
          foreach($data as $orderData)
          {
          ?>
            <!-- Item Cart -->
            <tr>
            <td class="text-center padding-top-60">#<?php echo $orderData['orderId']; ?></td>
              <td>
                  <div class="media">
                  <div class="media-left">   <img class="" style="height:100px;" src="<?php echo $orderData['productimage'];?>" alt="" >   </div>
                  <div class="media-body">
                    <p><?php echo $orderData['productName'];?></p>
                  </div>
                </div></td>
                <td>

                <ol class="progtrckr" data-progtrckr-steps="5">
                                            <li class="progtrckr-done">Unbilled</li>
                                            <li class="<?php if($orderData->deliverystatus != 'unbilled') { echo "progtrckr-done"; } else { echo "progtrckr-todo"; } ?>">Billed</li>
                                            <li class="<?php if($orderData->deliverystatus != 'unbilled' && $orderData->deliverystatus != 'billed' && $orderData->deliverystatus == 'assigned') { echo "progtrckr-done"; } else { echo "progtrckr-todo"; } ?>progtrckr-done">Assigned</li>
                                            <li class="<?php if($orderData->deliverystatus == 'Delivered') { echo  "progtrckr-done"; } else { echo "progtrckr-todo"; } ?>progtrckr-todo">Delivered</li>
                                            </ol>
                </td>
              <td class="text-center padding-top-60"><a href="<?php echo base_url(); ?>Orders/orderdetail/<?php echo $orderData['orderId']; ?>"><button class="btn-round acc_btn" type="submit" id="acc_Create">Details</button></a></td>
              <td class="text-center padding-top-60"><?php
            if($orderSetting[0]->houdinv_shop_order_configuration_cancellation == 1)
            {
            if($orderData['deliverystatus'] != 'Delivered' && $orderData['deliverystatus'] != 'return request' && $orderData['deliverystatus'] != 'cancel' && $orderData['deliverystatus'] != 'return' && $orderData['deliverystatus'] != 'cancel request' && $orderData['deliverystatus'] != 'order pickup' && $orderData['deliverystatus'] != 'unbilled')
            {
            ?>
            <button class="btn-round acc_btn cancelOrderBtn" data-id="<?php echo $orderData['orderId'] ?>" type="button" id="acc_Create">Cancel</button>
            <?php } }
            else if($orderSetting[0]->houdinv_shop_order_configuration_return != "" && $orderSetting[0]->houdinv_shop_order_configuration_return != 0)
            {
                if($orderData['deliverystatus'] == 'Delivered')
            {
                $getDeliveryDate = $orderData['deliverydate'];
                $getTodayDate = date('d-m-Y');
                if(strtotime($getTodayDate) > strtotime($getDeliveryDate))
                {
                    $datetime1 = new DateTime($getTodayDate);
                    $datetime2 = new DateTime($getDeliveryDate);
                    $interval = $datetime1->diff($datetime2);
                    if($interval > 0 && $interval <= $orderSetting[0]->houdinv_shop_order_configuration_return)
                    {
            ?>
            <button class="btn-round acc_btn" type="submit" id="acc_Create">Return</button>
            <?php } }
                }
            } ?></td>
            </tr>
            <?php
            }
            ?>
          </tbody>
        </table>
        </div>
      </div>
    </section>
    <?php
    }
    ?>

  </div>
  <!-- End Content -->
  <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>
