<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Resetdatabase extends CI_Model{
    function __construct() {
        $this->userTbl = '';
    }

public function fetchShopDetailDB($shop_id){

  $this->db->select('*')->from('houdin_user_shops')->where('houdin_user_shop_id',$shop_id);
 return $getShopData = $this->db->get()->row();
   
}



    public function Resetdatabase_data($setDbName)
    {
       
            $this->load->helper('createdb_helper');
                $getNewDatabase = createDynamicDatabase($setDbName);
                $getDB = $this->load->database($getNewDatabase, true);
                try {
                    $getDB->query("SET FOREIGN_KEY_CHECKS=0");
                     
          $getDB->query("DROP TABLE `cities`, `houdinv_accounts`, `houdinv_accounts_balance_sheet`, `houdinv_account_type`, `houdinv_block_users`, `houdinv_campaignemail`, `houdinv_campaignemail_user`, `houdinv_campaignpush`, `houdinv_campaignpush_user`, `houdinv_campaignsms`, `houdinv_campaignsms_user`, `houdinv_categories`, `houdinv_checkout_address_data`, `houdinv_countries`, `houdinv_coupons`, `houdinv_coupons_procus`, `houdinv_customersetting`, `houdinv_custom_home_data`, `houdinv_custom_links`, `houdinv_custom_slider`, `houdinv_deliveries`, `houdinv_delivery_tracks`, `houdinv_delivery_users`, `houdinv_detail_type`, `houdinv_emailsms_payment`, `houdinv_emailsms_stats`, `houdinv_email_log`, `houdinv_email_Template`, `houdinv_extra_expence`, `houdinv_extra_expence_item`, `houdinv_forgot_password`, `houdinv_google_analytics`, `houdinv_inventories`, `houdinv_inventorysetting`, `houdinv_inventory_purchase`, `houdinv_inventory_tracks`, `houdinv_invoicesetting`, `houdinv_navigation_store_pages`, `houdinv_noninventory_products`, `houdinv_offline_payment_settings`, `houdinv_online_payment_settings`, `houdinv_orders`, `houdinv_order_users`, `houdinv_payment_gateway`, `houdinv_possetting`, `houdinv_products`, `houdinv_products_variants`, `houdinv_productType`, `houdinv_product_reviews`, `houdinv_product_types`, `houdinv_purchase_products`, `houdinv_purchase_products_inward`, `houdinv_push_Template`, `houdinv_shipping_charges`, `houdinv_shipping_credentials`, `houdinv_shipping_data`, `houdinv_shipping_ruels`, `houdinv_shipping_warehouse`, `houdinv_shop_applogo`, `houdinv_shop_ask_quotation`, `houdinv_shop_configurations`, `houdinv_shop_detail`, `houdinv_shop_logo`, `houdinv_shop_order_configuration`, `houdinv_shop_query`, `houdinv_skusetting`, `houdinv_sms_log`, `houdinv_sms_template`, `houdinv_social_links`, `houdinv_staff_management`, `houdinv_staff_management_sub`, `houdinv_states`, `houdinv_stock_transfer_log`, `houdinv_storediscount`, `houdinv_storepolicies`, `houdinv_storesetting`, `houdinv_sub_categories_one`, `houdinv_sub_categories_two`, `houdinv_suppliers`, `houdinv_supplier_products`, `houdinv_taxes`, `houdinv_taxsetting`, `houdinv_templates`, `houdinv_template_pages`, `houdinv_testimonials`, `houdinv_transaction`, `houdinv_users`, `houdinv_users_cart`, `houdinv_users_forgot`, `houdinv_users_group`, `houdinv_users_group_users_list`, `houdinv_users_whishlist`, `houdinv_user_address`, `houdinv_user_addresses`, `houdinv_user_bank_details`, `houdinv_user_payments`, `houdinv_user_tag`, `houdinv_user_use_Coupon`, `houdinv_user_verifications`, `houdinv_viewmembership`, `houdinv_visitors`, `houdinv_vouchers`, `houdinv_workorder`");

 
          $getDB->query("SET FOREIGN_KEY_CHECKS=1");
  
    
                $getDB->query("CREATE TABLE cities (city_id int(11) NOT NULL,city_name varchar(30) COLLATE utf8_unicode_ci NOT NULL,state_id int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
               // $getDB->query("CREATE TABLE houdinv_coupons (houdinv_coupons_id int(11) NOT NULL,houdinv_coupons_name varchar(50) NOT NULL,houdinv_coupons_tagline varchar(50) NOT NULL,houdinv_coupons_valid_from date NOT NULL,houdinv_coupons_valid_to date NOT NULL,houdinv_coupons_order_amount int(11) NOT NULL,houdinv_coupons_code varchar(50) NOT NULL,houdinv_coupons_discount_precentage varchar(20) NOT NULL,houdinv_coupons_limit int(11) NOT NULL,houdinv_coupons_payment_method enum('cod','online payment','both') NOT NULL,houdinv_coupons_status enum('active','deactive') NOT NULL,houdinv_coupons_applicable enum('app','both') NOT NULL,houdinv_coupons_created_at varchar(30) NOT NULL,houdinv_coupons_modified_at varchar(30) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1");
                
                $getDB->query("CREATE TABLE  houdinv_block_users  (houdinv_block_user_id  int(10) UNSIGNED NOT NULL,houdinv_block_user_email  varchar(150) DEFAULT NULL,houdinv_block_user_reason  text,houdinv_block_user_created_at  timestamp NULL DEFAULT NULL,houdinv_block_user_updated_at  timestamp NULL DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1");
    
    //  Table structure for table   houdinv_campaignemail  

                $getDB->query("CREATE TABLE   houdinv_campaignemail   (
          campaignsms_id   int(11) NOT NULL,
          campaign_name   varchar(200) NOT NULL,
          campaign_status   int(11) NOT NULL,
          campaign_text   longblob NOT NULL,
          campaign_date   varchar(200) DEFAULT NULL,
          date_time   int(200) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Table structure for table   houdinv_campaignemail_user  

                $getDB->query("CREATE TABLE   houdinv_campaignemail_user   (
          campaignsms_user_id   int(11) NOT NULL,
          campaignsms_id   int(11) NOT NULL,
          campaignsms_users_id   int(11) NOT NULL,
          campaignsms_group_id   int(11) NOT NULL DEFAULT '0'
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Table structure for table   houdinv_campaignpush  

                $getDB->query("CREATE TABLE   houdinv_campaignpush   (
          campaignsms_id   int(11) NOT NULL,
          campaign_name   varchar(200) NOT NULL,
          campaign_status   int(11) NOT NULL,
          campaign_text   longblob NOT NULL,
          campaign_date   varchar(200) DEFAULT NULL,
          date_time   int(200) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Table structure for table   houdinv_campaignpush_user  

                $getDB->query("CREATE TABLE   houdinv_campaignpush_user   (
          campaignsms_user_id   int(11) NOT NULL,
          campaignsms_id   int(11) NOT NULL,
          campaignsms_users_id   int(11) NOT NULL,
          campaignsms_group_id   int(11) NOT NULL DEFAULT '0'
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

      //  Table structure for table   houdinv_campaignsms  

                $getDB->query("CREATE TABLE   houdinv_campaignsms   (
          campaignsms_id   int(11) NOT NULL,
          campaign_name   varchar(200) NOT NULL,
          campaign_status   int(11) NOT NULL,
          campaign_text   longblob NOT NULL,
          campaign_date   varchar(200) DEFAULT NULL,
          date_time   int(200) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
        
    //  Table structure for table   houdinv_campaignsms_user  

                $getDB->query("CREATE TABLE   houdinv_campaignsms_user   (
          campaignsms_user_id   int(11) NOT NULL,
          campaignsms_id   int(11) NOT NULL,
          campaignsms_users_id   int(11) NOT NULL,
          campaignsms_group_id   int(11) NOT NULL DEFAULT '0'
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Table structure for table   houdinv_categories  

                $getDB->query("CREATE TABLE   houdinv_categories   (
          houdinv_category_id   int(11) NOT NULL,
          houdinv_category_name   varchar(100) NOT NULL,
          houdinv_category_thumb   varchar(255) NOT NULL,
          houdinv_category_description   longtext NOT NULL,
          houdinv_category_subcategory   longtext NOT NULL,
          houdinv_category_status   enum('active','deactive','block','','') NOT NULL,
          houdinv_category_created_date   varchar(100) NOT NULL,
          houdinv_category_updated_date   varchar(100) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    

    //  Table structure for table   houdinv_checkout_address_data  

                $getDB->query("CREATE TABLE   houdinv_checkout_address_data   (
          houdinv_data_id   int(11) NOT NULL,
          houdinv_data_first_name   varchar(100) NOT NULL,
          houdinv_data_last_name   varchar(100) NOT NULL,
          houdinv_data_company_name   varchar(100) NOT NULL,
          houdinv_data_email   varchar(100) NOT NULL,
          houdinv_data_phone_no   varchar(100) NOT NULL,
          houdinv_data_country   varchar(100) NOT NULL,
          houdinv_data_address   text NOT NULL,
          houdinv_data_zip   varchar(12) NOT NULL,
          houdinv_data_city   varchar(100) NOT NULL,
          houdinv_data_user_id   int(11) NOT NULL,
          houdinv_data_order_note   text NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

   // table structure for houdinv_accounts
   $getDB->query("CREATE TABLE `houdinv_accounts` (
    `houdinv_accounts_id` int(11) NOT NULL,
    `houdinv_accounts_account_type_id` int(11) NOT NULL,
    `houdinv_accounts_detail_type_id` int(11) NOT NULL,
    `houdinv_accounts_name` varchar(100) NOT NULL,
    `houdinv_accounts_description` text NOT NULL,
    `houdinv_accounts_parent_account` int(11) NOT NULL DEFAULT '0',
    `houdinv_accounts_default_tax_code` varchar(255) NOT NULL,
    `houdinv_accounts_balance` varchar(255) NOT NULL,
    `houdinv_accounts_depreciation` varchar(255) NOT NULL,
    `houdinv_accounts_final_balance` float NOT NULL,
    `houdinv_accounts_date_add` int(11) NOT NULL,
    `houdinv_accounts_date_update` int(11) NOT NULL,
    `houdinv_accounts_delete_status` enum('no','yes') NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
   
   $getDB->query(" INSERT INTO `houdinv_accounts` (`houdinv_accounts_id`, `houdinv_accounts_account_type_id`, `houdinv_accounts_detail_type_id`, `houdinv_accounts_name`, `houdinv_accounts_description`, `houdinv_accounts_parent_account`, `houdinv_accounts_default_tax_code`, `houdinv_accounts_balance`, `houdinv_accounts_depreciation`, `houdinv_accounts_final_balance`, `houdinv_accounts_date_add`, `houdinv_accounts_date_update`, `houdinv_accounts_delete_status`) VALUES
   (1, 3, 21, 'Current', '', 0, '', '', '', 0, 1542067200, 1542067200, 'no'),
   (2, 16, 170, 'Undeposited fund', '', 0, '', '', '', 0, 1542067200, 1542067200, 'no'),
   (3, 1, 1, 'Accounts receivable (Debtors)', 'Accounts receivable (Debtors)', 0, '', '', '', 0, 1542067200, 1542067200, 'no'),
   (4, 2, 7, 'Inventory', 'Inventory', 0, '', '', '', 0, 1542067200, 1542067200, 'no'),
   (5, 14, 152, 'Supplies & materials', 'Supplies & materials', 0, '', '', '', 0, 1542067200, 1542067200, 'no'),
   (6, 11, 109, 'Sales of product income', 'Sales of product income', 0, '', '', '', 0, 1542067200, 1542067200, 'no'),
   (7, 6, 52, 'Accounts payable (Creditors)', 'Accounts payable (Creditors)', 0, '', '', '', 0, 1542067200, 1542067200, 'no')");

$getDB->query("ALTER TABLE `houdinv_accounts`
ADD PRIMARY KEY (`houdinv_accounts_id`)");

$getDB->query("ALTER TABLE `houdinv_accounts`
MODIFY `houdinv_accounts_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");


//    table structure for  balance sheet table

$getDB->query("CREATE TABLE `houdinv_accounts_balance_sheet` (
  `houdinv_accounts_balance_sheet_id` int(11) NOT NULL,
  `houdinv_accounts_balance_sheet_account_id` int(11) NOT NULL,
  `houdinv_accounts_balance_sheet_reference` varchar(255) NOT NULL,
  `houdinv_accounts_balance_sheet_ref_type` varchar(100) NOT NULL,
  `houdinv_accounts_balance_sheet_pay_account` varchar(255) NOT NULL,
  `houdinv_accounts_balance_sheet_payee_type` varchar(30) DEFAULT NULL,
  `houdinv_accounts_balance_sheet_account` int(11) NOT NULL,
  `houdinv_accounts_balance_sheet_order_id` int(11) NOT NULL DEFAULT '0',
  `houdinv_accounts_balance_sheet_product_desc` varchar(255) DEFAULT NULL,
  `houdinv_accounts_balance_sheet_purchase_id` int(11) NOT NULL DEFAULT '0',
  `houdinv_accounts_balance_sheet_concile` varchar(2) NOT NULL,
  `houdinv_accounts_balance_sheet_created_at` varchar(50) NOT NULL,
  `houdinv_accounts_balance_sheet_memo` varchar(255) NOT NULL,
  `houdinv_accounts_balance_sheet_decrease` float NOT NULL,
  `houdinv_accounts_balance_sheet__increase` float NOT NULL,
  `houdinv_accounts_balance_sheet_payment` float NOT NULL,
  `houdinv_accounts_balance_sheet_deposit` float NOT NULL,
  `houdinv_accounts_balance_sheet_transaction_added_status` varchar(10) NOT NULL,
  `houdinv_accounts_balance_sheet_tax` varchar(255) NOT NULL,
  `houdinv_accounts_balance_sheet_final_balance` float DEFAULT NULL,
  `houdinv_accounts_balance_sheet_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8");


$getDB->query("ALTER TABLE `houdinv_accounts_balance_sheet`
  ADD PRIMARY KEY (`houdinv_accounts_balance_sheet_id`);");
  
$getDB->query("ALTER TABLE `houdinv_accounts_balance_sheet`
  MODIFY `houdinv_accounts_balance_sheet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;");

// table structure for account type
$getDB->query("CREATE TABLE `houdinv_account_type` (
    `account_type_id` int(11) NOT NULL,
    `account_type_name` varchar(255) NOT NULL,
    `detail_type` varchar(255) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

$getDB->query("INSERT INTO `houdinv_account_type` (`account_type_id`, `account_type_name`, `detail_type`) VALUES
(1, 'Accounts receivable (Debtors)', '1'),
(2, 'Current assets', '2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17'),
(3, 'Bank', '18,19,20,21,22,23,24,25'),
(4, 'Fixed assets', '26,27,28,29,30,31,32,33,34,35,36,37'),
(5, 'Non-current assets', '38,39,40,41,42,43,44,45,46,47,48,49,50,51'),
(6, 'Accounts payable (Creditors)', '52,53,54'),
(7, 'Credit Card', '55'),
(8, 'Current liabilities', '56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72'),
(9, 'Non-current liabilities', '73,74,75,76,77,78,79,80,81,82,83'),
(10, 'Equity', '84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102'),
(11, 'Income', '103,104,105,106,107,108,109,110,111'),
(12, 'Other income', '112,113,114,115,116,117,118,119'),
(13, 'Cost of Goods Sold', '119,120,121,122,123,124'),
(14, 'Expenses', '125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157'),
(15, 'Other Expenses', '158,159,160,161,162,163,164,165,166,167,168,169'),
(16, 'Undeposited Fund', '170')");

$getDB->query("ALTER TABLE `houdinv_account_type`
ADD PRIMARY KEY (`account_type_id`)");

$getDB->query("ALTER TABLE `houdinv_account_type`
MODIFY `account_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17");

// table structure for detail type
$getDB->query("CREATE TABLE `houdinv_detail_type` (
    `houdinv_detail_type_id` int(11) NOT NULL,
    `houdinv_detail_type_name` varchar(100) NOT NULL,
    `houdinv_detail_type_description` text NOT NULL,
    `houdinv_detail_type_balance_box` int(11) NOT NULL,
    `houdinv_detail_type_depreciation` int(11) NOT NULL,
    `houdinv_detail_type_report` enum('register','report') NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");


$getDB->query("INSERT INTO `houdinv_detail_type` (`houdinv_detail_type_id`, `houdinv_detail_type_name`, `houdinv_detail_type_description`, `houdinv_detail_type_balance_box`, `houdinv_detail_type_depreciation`, `houdinv_detail_type_report`) VALUES
(1, 'Accounts receivable (Debtors)', '<b>Accounts receivable (Debtors)</b> tracks money that customers owe you for products or services, and payments customers make. <p>QuickBooks Online Plus automatically creates one Accounts receivable (Debtors) account for you. Most businesses need only one.</p><p>Each customer has a register, which functions like a Accounts receivable (Debtors) account for each customer.</p>', 0, 0, 'register'),
(2, 'Allowance for bad debts', 'Use <b>Allowance for bad debts</b> to estimate the part of Accounts Receivable (Debtors) that you think you might not collect. <p>Use this <b>only</b> if you are keeping your books on the accrual basis.</p>', 1, 0, 'register'),
(3, 'Assets available for sale', 'Use <b>Assets available for sale</b> to track assets that are available for sale that are not expected to be held for a long period of time.', 1, 0, 'register'),
(4, 'Balance with Government Authorities', 'Use <b>Balance with Government Authorities</b> to track the amount of taxes paid on input services/purchases, which offset taxes collected on sales (for example, CENVAT Credit Receivable)', 1, 0, 'register'),
(5, 'Development costs', 'Use <b>Development costs</b> to track amounts you deposit or set aside to arrange for financing, such as an SBA loan, or for deposits in anticipation of the purchase of property or other assets.  <p>When the deposit is refunded, or the purchase takes place, remove the amount from this account.</p>', 1, 0, 'register'),
(6, 'Employee cash advances', 'Use <b>Employee cash advances</b> to track employee wages and salary you issue to an employee early, or other non-salary money given to employees. <p>If you make a loan to an employee, use the Current asset account type called Loans to others, instead.</p>', 1, 0, 'register'),
(7, 'Inventory', 'Use <b>Inventory</b> to track the cost of goods your business purchases for resale.<p></p><p>When the goods are sold, assign the sale to a <b>Cost of goods sold</b> account.</p>', 1, 0, 'register'),
(8, 'Investments - Other', 'Use <b>Investments - Other</b> to track the value of investments not covered by other investment account types. Examples include publicly-traded shares, coins, or gold.', 1, 0, 'register'),
(9, 'Loans to officers', 'If you operate your business as a Corporation, use <b>Loans to officers</b> to track money loaned to officers of your business.', 1, 0, 'register'),
(10, 'Loans to others', 'Use <b>Loans to others</b> to track money your business loans to other people or businesses. <p>This type of account is also referred to as Notes Receivable. </p><p>For early salary payments to employees, use Employee cash advances, instead.</p>', 1, 0, 'register'),
(11, 'Loans to Shareholders', 'If you operate your business as a Corporation, use <b>Loans to Shareholders</b> to track money your business loans to its shareholders.', 1, 0, 'register'),
(12, 'Other current assets', 'Use <b>Other current assets</b> for current assets not covered by the other types. Current assets are likely to be converted to cash or used up in a year.', 1, 0, 'register'),
(13, 'Prepaid expenses', 'Use <b>Prepaid expenses</b> to track payments for expenses that you won’t recognise until your next accounting period.<p>When you recognise the expense, make a journal entry to transfer money from this account to the expense account.</p>', 1, 0, 'register'),
(14, 'Retainage', 'Use <b>Retainage</b> if your customers regularly hold back a portion of a contract amount until you have completed a project. <p>This type of account is often used in the construction industry, and only if you record income on an accrual basis.</p>', 1, 0, 'register'),
(15, 'Short Term Investments in Related Parties', 'Use <b>Short Term Investments in Related Parties</b> to track investments held in subsidiaries, associates, joint ventures and other related entities maturing within 12 months.', 1, 0, 'register'),
(16, 'Short Term Loans and Advances to related parties', 'Use <b>Short Term Loans and Advances to related parties</b> to track loans and advance given to related parties for a period not more than twelve months from the balance sheet date.', 1, 0, 'register'),
(17, 'Undeposited funds', 'Use <b>Undeposited funds</b> for cash or cheques from sales that haven’t been deposited yet.<p>For petty cash, use Cash on hand, instead.</p>', 1, 0, 'register'),
(18, 'Cash and Cash Equivalents', 'Use <b>Cash and Cash Equivalents</b> to track cash or assets that can be converted into cash immediately. For example, marketable securities and Treasury bills.', 1, 0, 'register'),
(19, 'Cash on hand', 'Use a <b>Cash on hand</b> account to track cash your company keeps for occasional expenses, also called petty cash. <p>To track cash from sales that have not been deposited yet, use a pre-created account called <b>Undeposited funds</b>, instead.</p>', 1, 0, 'register'),
(20, 'Client trust accounts', 'Use <b>Client trust accounts</b> for money held by you for the benefit of someone else. <p>For example, client trust accounts are often used by lawyers to keep track of expense money their customers have given them. </p><p>Often, to keep the amount in a client trust account from looking like it’s yours, the amount is offset in a \"contra\" liability account (a Current Liability).</p>', 1, 0, 'register'),
(21, 'Current', 'Use <b>Current</b> accounts to track all your chequing activity, including debit card transactions.<p>Each current account your company has at a bank or other financial institution should have its own Current type account in QuickBooks Online Plus.</p>', 1, 0, 'register'),
(22, 'Money market', 'Use <b>Money market</b> to track amounts in money market accounts. <p>For investments, see <b>Current Assets</b>, instead.</p>', 1, 0, 'register'),
(23, 'Other Earmarked Bank Accounts', 'Use <b>Other Earmarked Bank Accounts</b> to keep track of funds set aside for a specific purpose. For example, funds set aside to be able to pay a specific liability in the future.', 1, 0, 'register'),
(24, 'Rents held in trus', 'Use <b>Rents held in trust</b> to track deposits and rent held on behalf of the property owners. <p>Typically only property managers use this type of account.</p>', 1, 0, 'register'),
(25, 'Savings', 'Use <b>Savings</b> accounts to track your savings and CD activity. <p>Each savings account your company has at a bank or other financial institution should have its own Savings type account. </p><p>For investments, see <b>Current Assets</b>, instead.</p>', 1, 0, 'register'),
(26, 'Accumulated depreciation', 'Use <b>Accumulated depreciation</b> to track how much you depreciate a fixed asset (a physical asset you do not expect to convert to cash during one year of normal operations).', 1, 0, 'register'),
(27, 'Accumulated depletion', 'Use <b>Accumulated depletion</b> to track how much you deplete a natural resource.', 1, 0, 'register'),
(28, 'Buildings', 'Use <b>Buildings</b> to track the cost of structures you own and use for your business. If you have a business in your home, consult your accountant.<p>Use a <b>Land</b> account for the land portion of any real property you own, splitting the cost of the property between land and building in a logical method. A common method is to mimic the land-to-building ratio on the property tax statement.</p>', 1, 1, 'register'),
(29, 'Capital Work-in-Progress', 'Use <b>Capital Work-in-Progress</b> to track the amount spent to create an asset that is not yet complete as of the date of the balance sheet (Work Under Process). For example, Asset Under Construction.', 1, 1, 'register'),
(30, 'Depletable assets', 'Use <b>Depletable assets</b> to track natural resources, such as timberlands, oil wells, and mineral deposits.', 1, 1, 'register'),
(31, 'Furniture and fixtures', 'Use <b>Furniture and fixtures</b> to track any furniture and fixtures your business owns and uses, like a dental chair or sales booth.', 1, 1, 'register'),
(32, 'Intangible Assets Under Developmen', 'Use <b>Intangible Assets Under Development</b> to track the amount spent to generate an intangible asset that is not yet complete as of the date of the balance sheet (Work Under Process). For example, Asset Under Construction.', 1, 1, 'register'),
(33, 'Land', 'Use <b>Land</b> to track assets that are not easily convertible to cash or not expected to become cash within the next year. For example, leasehold improvements', 1, 1, 'register'),
(34, 'Leasehold improvements', 'Use <b>Leasehold improvements</b> to track improvements to a leased asset that increases the asset’s value. For example, if you carpet a leased office space and are not reimbursed, that’s a leasehold improvement.', 1, 1, 'register'),
(35, 'Machinery and equipment', 'Use <b>Machinery and equipment</b> to track computer hardware, as well as any other non-furniture fixtures or devices owned and used for your business.<p>This includes equipment that you ride, like tractors and lawn mowers. Cars and trucks, however, should be tracked with <b>Vehicle accounts</b>, instead.</p>', 1, 1, 'register'),
(36, 'Other fixed asset', 'Use <b>Other fixed asset</b> for fixed assets that are not covered by other asset types. <p>Fixed assets are physical property that you use in your business and that you do not expect to convert to cash or be used up during one year of normal operations.</p>', 1, 1, 'register'),
(37, 'Vehicles', 'Use <b>Vehicles</b> to track the value of vehicles your business owns and uses for business. This includes off-road vehicles, air planes, helicopters, and boats.<p>If you use a vehicle for both business and personal use, consult your accountant to see how you should track its value.</p>', 1, 1, 'register'),
(38, 'Accumulated amortisation of non-current assets', 'Use <b>Accumulated amortisation of non-current assets</b> to track how much you’ve amortised an asset whose type is <b>Non-Current Asset</b>.<p></p>', 1, 0, 'register'),
(39, 'Assets held for sale', 'Use <b>Assets held for sale</b> to track assets of a company that are available for sale that are not expected to be held for a long period of time.', 1, 0, 'register'),
(40, 'Deferred Tax', 'Use <b>Deferred Tax</b> to record the future tax asset from timing differences between book profits from the Companies Act and taxable profits from the Income Tax Act.', 1, 0, 'register'),
(41, 'Goodwill', 'Use <b>Goodwill</b> only if you have acquired another company. It represents the intangible assets of the acquired company which gave it an advantage, such as favourable government relations, business name, outstanding credit ratings, location, superior management, customer lists, product quality, or good labour relations.<p></p>', 1, 0, 'register'),
(42, 'Intangible assets', 'Use <b>Intangible assets</b> to track intangible assets that you plan to amortise. Examples include franchises, customer lists, copyrights, and patents.<p></p>', 1, 0, 'register'),
(43, 'Lease buyout', 'Use <b>Lease buyout</b> to track lease payments to be applied toward the purchase of a leased asset. <p>You don’t track the leased asset itself until you purchase it.</p>', 1, 0, 'register'),
(44, 'Licences', 'Use <b>Licences</b> to track non-professional licences for permission to engage in an activity, like selling alcohol or radio broadcasting.<p>For fees associated with professional licences granted to individuals, use a <b>Legal and professional fees</b> expense account, instead.</p>', 1, 0, 'register'),
(45, 'Long Term Loans and Advances to Related Parties', 'Use <b>Long Term Loans and Advances to Related Parties</b> to track loans and advances given to related parties for more than twelve months from the balance sheet date.', 1, 0, 'register'),
(46, 'Long-term investments in related entities', 'Use <b>Long Term Investments</b> to track investments held in the subsidiaries, associates, joint ventures and other related entities that will mature later than 12 months from the balance sheet date.', 1, 0, 'register'),
(47, 'Organisational costs', 'Use <b>Organisational costs</b> to track costs incurred when forming a partnership or corporation.<p>The costs include the legal and accounting costs necessary to organise the company, facilitate the filings of the legal documents, and other paperwork.</p>', 1, 0, 'register'),
(48, 'Other Long Term Investments', 'Use <b>Other Long Term Investments</b> to track investments that you expect to hold for more than twelve months and that are not classified in other account types.', 1, 0, 'register'),
(49, 'Other Long Term Loans and Advances', 'Use <b>Other Long Term Loans and Advances</b> to track loans and advances given to related parties for more than twelve months from the balance sheet date, that are not classified in other account types.', 1, 0, 'register'),
(50, 'Other long-term assets', 'Use <b>Other long-term assets</b> to track assets not covered by other types.<p>Long-term assets are expected to provide value for more than one year.</p>', 1, 0, 'register'),
(51, 'Security deposits', 'Use <b>Security deposits</b> to track funds you’ve paid to cover any potential costs incurred by damage, loss, or theft. <p>The funds should be returned to you at the end of the contract.</p><p>If you accept down payments, advance payments, security deposits, or other kinds of deposits, use an <b>Other current liabilities</b> account with the detail type Other current liabilities.</p>', 1, 0, 'register'),
(52, 'Accounts payable (Creditors)', '<b>Accounts payable (Creditors)</b> tracks amounts you owe to your suppliers.<p>QuickBooks Online Plus automatically creates one Accounts Payable (Creditors) account for you. Most businesses need only one.</p>', 0, 0, 'register'),
(53, 'MSME Payables', 'Account to hold principal amount and the interest due remaining unpaid to any business suppliers that are MSME (Micro, Small and Medium Enterprise)', 0, 0, 'register'),
(54, 'Non - MSME Payables', 'This account to hold principal amount and the interest due remaining unpaid to any business suppliers that are not an MSME (Micro, Small and Medium Enterprise)', 0, 0, 'register'),
(55, 'Credit card', '<b>Credit card</b> accounts track the balance due on your business credit cards. <p>Create one <b>Credit card</b> account for each credit card account your business uses.</p>', 0, 0, 'register'),
(56, 'Accrued Liabilities', 'Use <b>Accrued Liabilities</b> to track expenses that a business has incurred but has not yet paid. For example, pensions for companies that contribute to a pension fund for their employees for their retirement.', 1, 0, 'register'),
(57, 'Client Trust accounts - liabilities', 'Use <b>Client Trust accounts - liabilities</b> to offset <b>Client Trust accounts</b> in assets. <p>Amounts in these accounts are held by your business on behalf of others.  They do not belong to your business, so should not appear to be yours on your balance sheet. This \"contra\" account takes care of that, as long as the two balances match.</p>', 1, 0, 'register'),
(58, 'Current liabilities', 'Use <b>Current liabilities</b> to track liabilities due within the next twelve months that do not fit the Current liability account types.', 1, 0, 'register'),
(59, 'Current portion of obligations under finance leases', 'Use <b>Current portion of obligations under finance leases</b> to track the value of lease payments due within the next 12 months.', 1, 0, 'register'),
(60, 'Dividends payable', 'Use <b>Dividends payable</b> to track dividends that are owed to shareholders but have not yet been paid.', 1, 0, 'register'),
(61, 'Duties and Taxes', 'Use <b>Duties and Taxes</b> to keep track of various duties and taxes payable by the company. However, it does not include Income Tax (for example, VAT Payable, Service Tax Payable).', 1, 0, 'register'),
(62, 'Income tax payable', 'Use <b>Income tax payable</b> to track monies that are due to pay the company’s income tax liabilties.', 1, 0, 'register'),
(63, 'Income tax payable', 'Use <b>Income tax payable</b> to track monies that are due to pay the company’s income tax liabilties.', 1, 0, 'register'),
(64, 'Line of credit', 'Use <b>Line of credit</b> to track the balance due on any lines of credit your business has.  Each line of credit your business has should have its own <b>Line of credit</b> account.', 1, 0, 'register'),
(65, 'Loan payable', 'Use <b>Loan payable</b> to track loans your business owes which are payable within the next twelve months. <p>For longer-term loans, use the Long-term liability called <b>Notes payable</b>, instead.</p>', 1, 0, 'register'),
(66, 'Other current liabilities', 'Use <b>Other current liabilities</b> to track monies owed by the company and due within one year.', 1, 0, 'register'),
(67, 'Payroll liabilities', 'Use <b>Payroll liabilities</b> to keep track of tax amounts that you owe to government agencies as a result of paying wages. This includes taxes withheld, health care premiums, employment insurance, government pensions, etc. When you forward the money to the government agency, deduct the amount from the balance of this account.', 1, 0, 'register'),
(68, 'Prepaid expenses payable', 'Use <b>Prepaid expenses payable</b> to track items such as property taxes that are due, but not yet deductible as an expense because the period they cover has not yet passed.', 1, 0, 'register'),
(69, 'Rents in trust - liability', 'Use <b>Rents in trust - liability</b> to offset the <b>Rents in trust</b> amount in assets.  <p>Amounts in these accounts are held by your business on behalf of others.  They do not belong to your business, so should not appear to be yours on your balance sheet.  This \"contra\" account takes care of that, as long as the two balances match.</p>', 1, 0, 'register'),
(70, 'Short Term Borrowings from Related Parties', 'Use Short Term Borrowings to track loans and other obligations payable to related parties and maturing within 12 months of the balance sheet date.', 1, 0, 'register'),
(71, 'Short Term Provisions for Employee Benefits', 'Use this account type to track all types of benefits and compensation payable to employees, within 12 months of the balance sheet date.', 1, 0, 'register'),
(72, 'Short-term provisions', 'Use <b>Short-term provisions</b> to track current liabilities that have not yet been realized.', 1, 0, 'register'),
(73, 'Accrued holiday payable', 'Use <b>Accrued holiday payable</b> to track holiday earned but that has not been paid out to employees.', 1, 0, 'register'),
(74, 'Accrued Non-current liabilities', 'Use <b>Accrued Non-current liabilities</b> to track expenses that a business has incurred but has not yet paid. For example, pensions for companies that contribute to a pension fund for their employees for their retirement.', 1, 0, 'register'),
(75, 'Deferred Tax Liabilitie', 'Use <b>Deferred Tax Liabilities</b> to record future tax liability from timing differences between book profits from the Companies Act and taxable profits from the Income Tax Act.', 1, 0, 'register'),
(76, 'Liabilities related to assets held for sale', 'Use <b>Liabilities related to assets held for sale</b> to track any liabilities that are directly related to assets being sold or written off.', 1, 0, 'register'),
(77, 'Long Term Borrowings from Related Parties', 'Use <b>Long Term Borrowings</b> to track loans and other obligations payable to related parties and maturing more than one year later than the balance sheet date.', 1, 0, 'register'),
(78, 'Long Term Provisions for Employee Benefits', 'Use this account type to track all types of benefits and compensation payable to the employees, but which are not classified as Short Term Provision for Employees benefits.', 1, 0, 'register'),
(79, 'Long-term debt', 'Use <b>Long-term debt</b> to track loans and obligations with a maturity of longer than one year.  For example, mortgages.', 1, 0, 'register'),
(80, 'Notes payable', 'Use <b>Notes payable</b> to track the amounts your business owes in long-term (over twelve months) loans. <p>For shorter loans, use the Current liability account type called <b>Loan payable</b>, instead.</p>', 1, 0, 'register'),
(81, 'Other Long Term Provisions', 'Use <b>Other Long Term Provisions</b> to track the provisions for more than twelve months that do not classify under other account types.', 1, 0, 'register'),
(82, 'Other non-current liabilities', 'Use <b>Other non-current liabilities</b> to track liabilities due in more than twelve months that don’t fit the other Non-Current liability account types.', 1, 0, 'register'),
(83, 'Shareholder notes payable', 'Use <b>Shareholder notes payable</b> to track long-term loan balances your business owes its shareholders.', 1, 0, 'register'),
(84, 'Accumulated adjustment', 'Some corporations use this account to track adjustments to owner’s equity that are not attributable to net income.', 1, 0, 'register'),
(85, 'Capital Reserves', 'Use <b>Capital Reserves</b> to track the reserves created when the company accumulated capital profits, not including any profits from revenue.', 1, 0, 'register'),
(86, 'Dividend disbursed', 'Use <b>Dividend disbursed</b> to track a payment given to its shareholders out of the company’s retained earnings.', 1, 0, 'register'),
(87, 'Equity in earnings of subsidiaries', 'Use <b>Equity in earnings of subsidiaries</b> to track the original investment in shares of subsidiaries plus the share of earnings or losses from the operations of the subsidiary.', 1, 0, 'register'),
(88, 'Funds', 'Use <b>Funds</b> to track any portion of a reserve that is earmarked for a specific investment outside the business, such as a Sinking Fund.', 1, 0, 'register'),
(89, 'Money Received Against Share Warrants', 'Use <b>Money Received Against Share Warrants</b> to track money received when share warrants were issued. A share warrant is a document that entitles its holder to buy shares at a certain price in the future.', 1, 0, 'register'),
(90, 'Opening balance equity', 'QuickBooks Online Plus creates this account the first time you enter an opening balance for a balance sheet account. <p>As you enter opening balances, QuickBooks Online Plus records the amounts in <b>Opening balance equity</b>. This ensures that you have a correct balance sheet for your company, even before you’ve finished entering all your company’s assets and liabilities.</p>', 1, 0, 'register'),
(91, 'Ordinary shares', 'Corporations use <b>Ordinary shares</b> to track its ordinary shares in the hands of shareholders. The amount in this account should be the stated (or par) value of the stock.', 1, 0, 'register'),
(92, 'Other Free Reserves', 'Use <b>Other Free Reserves</b> to track reserves not covered by other free reserve types. <br>Free Reserves are reserves created from profits. You can use them to declare dividends, but not to repay any future liability.', 1, 0, 'register'),
(93, 'Other comprehensive income', 'Use <b>Other comprehensive income</b> to track the increases or decreases in income from various businesses that is not yet absorbed by the company.', 1, 0, 'register'),
(94, 'Owner’s equity', 'Corporations use <b>Owner’s equity</b> to show the cumulative net income or loss of their business as of the beginning of the financial year.', 1, 0, 'register'),
(95, 'Paid-in capital or surplus', 'Corporations use <b>Paid-in capital</b> to track amounts received from shareholders in exchange for shares that are over and above the shares’ stated (or par) value.', 1, 0, 'register'),
(96, 'Partner Distributions', 'Partnerships use <b>Partner distributions</b> to track amounts distributed by the partnership to its partners during the year.<p>Don’t use this for regular payments to partners for interest or service. For regular payments, use a <b>Guaranteed payments</b> account (a Expense account in Payroll expenses), instead.</p>', 1, 0, 'register'),
(97, 'Partner’s equity', 'Partnerships use <b>Partner’s equity</b> to show the income remaining in the partnership for each partner as of the end of the prior year.', 1, 0, 'register'),
(98, 'Preferred shares', 'Corporations use this account to track its preferred shares in the hands of shareholders. The amount in this account should be the stated (or par) value of the shares.', 1, 0, 'register'),
(99, 'Retained earnings', 'QuickBooks Online Plus adds this account when you create your company. <p><b>Retained earnings</b> tracks net income from previous financial years.</p><p>QuickBooks Online Plus automatically transfers your profit (or loss) to <b>Retained earnings</b> at the end of each financial year.</p>', 1, 0, 'register'),
(100, 'Share Application Money Pending Allotment', 'Use <b>Share Application Money Pending Allotment</b> to track the application amounts the company receives for shares that still need to be allotted to the investors.', 1, 0, 'register'),
(101, 'Share capital', 'Use <b>Share capital</b> to track the funds raised by issuing shares.', 1, 0, 'register'),
(102, 'Treasury shares', 'Corporations use <b>Treasury shares</b> to track amounts paid by the corporation to buy its own shares back from shareholders.<p></p>', 1, 0, 'register'),
(103, 'Discounts/refunds given', 'Use <b>Discounts/refunds given</b> to track discounts you give to customers.<p>This account typically has a negative balance so it offsets other income.</p><p>For discounts from suppliers, use an expense account, instead.</p>', 0, 0, 'report'),
(104, 'Non-profit income', 'Use <b>Non-profit income</b> to track money coming in if you are a non-profit organisation.', 0, 0, 'report'),
(105, 'Other primary income', 'Use <b>Other primary income</b> to track income from normal business operations that doesn’t fall into another Income type.', 0, 0, 'report'),
(106, 'Revenue - General', 'Use <b>Revenue - General</b> to track income from normal business operations that do not fit under any other category.', 0, 0, 'register'),
(107, 'Sales - retail', 'Use <b>Sales - retail</b> to track sales of goods/services that have a mark-up cost to consumers.', 0, 0, 'register'),
(108, 'Sales - wholesale', 'Use <b>Sales - wholesale</b> to track the sale of goods in quantity for resale purposes.', 0, 0, 'register'),
(109, 'Sales of product income', 'Use <b>Sales of product income</b> to track income from selling products.<p>This can include all kinds of products, like crops and livestock, rental fees, performances, and food served.</p>', 0, 0, 'report'),
(110, 'Service/fee income', 'Use <b>Service/fee income</b> to track income from services you perform or ordinary usage fees you charge.<p>For fees customers pay you for late payments or other uncommon situations, use an Other Income account type called <b>Other miscellaneous income</b>, instead.', 0, 0, 'report'),
(111, 'Unapplied Cash Payment Income', '<b>Unapplied Cash Payment Income</b> reports the <b>Cash Basis</b> income from customers payments you’ve received but not applied to invoices or charges. In general, you would never use this directly on a purchase or sale transaction.', 0, 0, 'report'),
(112, 'Dividend income', 'Use <b>Dividend income</b> to track taxable dividends from investments.', 0, 0, 'report'),
(113, 'Interest earned', 'Use <b>Interest earned</b> to track interest from bank or savings accounts, investments, or interest payments to you on loans your business made.', 0, 0, 'report'),
(114, 'Loss on disposal of assets', 'Use <b>Loss on disposal of assets</b> to track losses realised on the disposal of assets.', 0, 0, 'register'),
(115, 'Other investment income', 'Use <b>Other investment income</b> to track other types of investment income that isn’t from dividends or interest.', 0, 0, 'report'),
(116, 'Other miscellaneous income', 'Use <b>Other miscellaneous income</b> to track income that isn’t from normal business operations, and doesn’t fall into another Other Income type.', 0, 0, 'report'),
(117, 'Other operating income', 'Use <b>Other operating income</b> to track income from activities other than normal business operations.  For example, Investment interest, foreign exchange gains, and rent income.', 0, 0, 'register'),
(118, 'Tax-exempt interest', 'Use <b>Tax-exempt interest</b> to record interest that isn’t taxable, such as interest on money in tax-exempt retirement accounts, or interest from tax-exempt bonds.', 0, 0, 'report'),
(119, 'Unrealised loss on securities, net of tax', 'Use <b>Unrealised loss on securities, net of tax</b> to track losses on securities that have occurred but are yet been realised through a transaction. For example, shares whose value has fallen but that are still being held.', 0, 0, 'report'),
(120, 'Equipment rental - COS', 'Use <b>Equipment rental - COS</b> to track the cost of renting equipment to produce products or services.<p>If you purchase equipment, use a Fixed Asset account type called <b>Machinery and equipment</b>.</p>', 0, 0, 'report'),
(121, 'Freight and delivery - COS', 'Use <b>Freight and delivery - COS</b> to track the cost of shipping/delivery of obtaining raw materials and producing finished goods for resale.', 0, 0, 'report'),
(122, 'Other costs of sales - COS', 'Use <b>Other costs of sales - COS</b> to track costs related to services or sales that you provide that don’t fall into another Cost of Sales type.', 0, 0, 'report'),
(123, 'Salaries and Wages', 'Use <b>Salaries and Wages</b> to track the cost of paying employees to produce products or supply services.<p>It includes all employment costs, including food and transportation, if applicable.', 0, 0, 'report'),
(124, 'Supplies and materials - COS', 'Use <b>Supplies and materials - COS</b> to track the cost of raw goods and parts used or consumed when producing a product or providing a service.', 0, 0, 'report'),
(125, 'Advertising/promotional', 'Use <b>Advertising/promotional</b> to track money spent promoting your company. <p>You may want different accounts of this type to track different promotional efforts (Yellow Pages, newspaper, radio, flyers, events, and so on).</p><p>If the promotion effort is a meal, use <b>Promotional meals</b> instead.</p>', 0, 0, 'report'),
(126, 'Amortisation expense', 'Use <b>Amortisation expense</b> to track writing off of assets (such as intangible assets or investments) over the projected life of the assets.', 0, 0, 'report'),
(127, 'Auto', 'Use <b>Auto</b> to track costs associated with vehicles.<p>You may want different accounts of this type to track fuel, repairs, and maintenance.</p><p>If your business owns a car or truck, you may want to track its value as a Fixed Asset, in addition to tracking its expenses.</p>', 0, 0, 'report'),
(128, 'Bad debt', 'Use <b>Bad debt</b> to track debt you have written off.', 0, 0, 'report'),
(129, 'Bank charges', 'Use <b>Bank charges</b> for any fees you pay to financial institutions.', 0, 0, 'report'),
(130, 'Borrowing Costs', 'Use <b>Borrowing Costs</b> to track the cost to procure funds. This includes supplementary costs that relate to borrowings.', 0, 0, 'report'),
(131, 'Charitable contributions', 'Use <b>Charitable contributions</b> to track gifts to charity.', 0, 0, 'report'),
(132, 'Commissions and fees', 'Use <b>Commissions and fees</b> to track amounts paid to agents (such as brokers) in order for them to execute a trade.', 0, 0, 'report'),
(133, 'Cost of labour', 'Use <b>Cost of labour</b> to track the cost of paying employees to produce products or supply services.<p>It includes all employment costs, including food and transportation, if applicable.</p><p>This account is also available as a Cost of Sales (COS) account.</p>', 0, 0, 'report'),
(134, 'Dues and subscriptions', 'Use <b>Dues and subscriptions</b> to track dues and subscriptions related to running your business.<p>You may want different accounts of this type for professional dues, fees for licences that can’t be transferred, magazines, newspapers, industry publications, or service subscriptions.</p>', 0, 0, 'report'),
(135, 'Equipment rental', 'Use <b>Equipment rental</b> to track the cost of renting equipment to produce products or services.<p>This account is also available as a Cost of Sales account. </p><p>If you purchase equipment, use a Fixed asset account type called <b>Machinery and equipment</b>.</p>', 0, 0, 'report'),
(136, 'Finance costs', 'Use <b>Finance costs</b> to track the costs of obtaining loans or credit.<p>Examples of finance costs would be credit card fees, interest and mortgage costs.</p>', 0, 0, 'report'),
(137, 'Income tax expense', 'Use <b>Income tax expense</b> to track income taxes that the company has paid to meet their tax obligations.', 0, 0, 'report'),
(138, 'Insurance', 'Use <b>Insurance</b> to track insurance payments.<p>You may want different accounts of this type for different types of insurance (auto, general liability, and so on)', 0, 0, 'report'),
(139, 'Interest paid', 'Use <b>Interest paid</b> for all types of interest you pay, including mortgage interest, finance charges on credit cards, or interest on loans.', 0, 0, 'report'),
(140, 'Legal and professional fees', 'Use <b>Legal and professional fees</b> to track money to pay to professionals to help you run your business.<p>You may want different accounts of this type for payments to your accountant, attorney, or other consultants.</p>', 0, 0, 'report'),
(141, 'Loss on discontinued operations, net of tax', 'Use <b>Loss on discontinued operations, net of tax</b> to track the loss realised when a part of the business ceases to operate or when a product line is discontinued.', 0, 0, 'report'),
(142, 'Management compensation', 'Use <b>Management compensation</b> to track remuneration paid to Management, Executives and non-Executives.  For example, salary, fees, and benefits.', 0, 0, 'report'),
(143, 'Meals and entertainment', 'Use <b>Meals and entertainment</b> to track how much you spend on dining with your employees to promote morale. <p>If you dine with a customer to promote your business, use a <b>Promotional meals</b> account, instead.</p><p>Be sure to include who you ate with and the purpose of the meal when you enter the transaction.</p>', 0, 0, 'report'),
(144, 'Office/general administrative expenses', 'Use <b>Office/general administrative expenses</b> to track all types of general or office-related expenses.', 0, 0, 'report'),
(145, 'Other miscellaneous service cost', 'Use <b>Other miscellaneous service cost</b> to track costs related to providing services that don’t fall into another Expense type.<p>This account is also available as a Cost of Sales (COS) account. <span class=\"’instructions’\"></span></p>', 0, 0, 'report'),
(146, 'Other selling expenses', 'Use <b>Other selling expenses</b> to track selling expenses incurred that do not fall under any other category.', 0, 0, 'report'),
(147, 'Payroll expenses', 'Use <b>Payroll expenses</b> to track payroll expenses. You may want different accounts of this type for things like: <ul> <li>Compensation of officers</li> <li>Guaranteed payments</li> <li>Workers compensation</li> <li>Salaries and wages</li> <li>Payroll taxes</li> </ul>', 0, 0, 'report'),
(148, 'Rent or lease of buildings', 'Use <b>Rent or lease of buildings</b> to track rent payments you make.', 0, 0, 'report'),
(149, 'Repair and maintenance', 'Use <b>Repair and maintenance</b> to track any repairs and periodic maintenance fees. <p>You may want different accounts of this type to track different types repair &amp; maintenance expenses (auto, equipment, landscape, and so on).</p>', 0, 0, 'report'),
(150, 'Shipping and delivery expense', 'Use <b>Shipping and delivery expense</b> to track the cost of shipping and delivery of goods to customers.', 0, 0, 'report'),
(151, 'Shipping, Freight and Delivery', 'Use <b>Shipping, Freight and Delivery</b> to track the cost of shipping products to customers or distributors.<p>You might use this type of account for incidental shipping expenses, and the COS type of <b>Shipping, freight &amp; delivery</b> account for direct costs.</p><p>This account is also available as a Cost of Sales (COS) account.</p>', 0, 0, 'report'),
(152, 'Supplies & materials', 'Use <b>Supplies &amp; materials</b> to track the cost of raw goods and parts used or consumed when producing a product or providing a service.<p>This account is also available as a Cost of Sales account.</p>', 0, 0, 'report'),
(153, 'Taxes paid', 'Use <b>Taxes paid</b> to track taxes you pay.<p>You may want different accounts of this type for payments to different tax agencies.</p>', 0, 0, 'report'),
(154, 'Travel expenses - general and admin expenses', 'Use <b>Travel expenses - general and admin expenses</b> to track travelling costs incurred that are <b>not</b> directly related to the revenue-generating operation of the company.  For example, flight tickets and hotel costs when performing job interviews.', 0, 0, 'report'),
(155, 'Travel expenses - selling expense', 'Use <b>Travel expenses - selling expense</b> to track travelling costs incurred that are directly related to the revenue-generating operation of the company.  For example, flight tickets and hotel costs when selling products and services.', 0, 0, 'report'),
(156, 'Unapplied Cash Bill Payment Expense', '<b>Unapplied Cash Bill Payment Expense</b> reports the <b>Cash Basis</b> expense from supplier payment cheques you’ve sent but not yet applied to supplier bills. In general, you would never use this directly on a purchase or sale transaction.', 0, 0, 'report'),
(157, 'Utilities', 'Use <b>Utilities</b> to track utility payments.<p>You may want different accounts of this type to track different types of utility payments (gas and electric, telephone, water, and so on).</p>', 0, 0, 'report'),
(158, 'Amortisation', 'Use <b>Amortisation</b> to track amortisation of intangible assets.<p>Amortisation is spreading the cost of an intangible asset over its useful life, like depreciation of fixed assets.</p><p>You may want an amortisation account for each intangible asset you have.</p>', 0, 0, 'report'),
(159, 'Deferred Tax Expense', 'Use <b>Deferred Tax Expense</b> to track the difference between the tax liability from book profits and the tax liability from taxable profits. The variation is from timing differences between book profits and taxable profits, which may become reversed in a later period.', 0, 0, 'report'),
(160, 'Depletion', 'Use <b>Depletion</b> to track the reduction in value of an asset used in the extraction of natural resources like quarries, mines, and so on, where the quantity of the material of asset reduces with extraction', 0, 0, 'report'),
(161, 'Depreciation', 'Use <b>Depreciation</b> to track how much you depreciate fixed assets. <p>You may want a depreciation account for each fixed asset you have.</p>', 0, 0, 'report'),
(162, 'Exceptional Items', 'Use <b>Exceptional Items</b> to record income/expenditure items that are connected to the business activities, but are of exceptional nature and are observed to have a substantial impact on the performance and net results of the company.', 0, 0, 'report'),
(163, 'Exchange Gain or Loss', 'Use <b>Exchange Gain or Loss</b> to track gains or losses that occur as a result of exchange rate fluctuations.', 0, 0, 'report'),
(164, 'Extraordinary Items', 'Use <b>Extraordinary Items</b> to record income/expenditure items from non-recurring transactions that are not connected to the routine course of business activities.', 0, 0, 'report'),
(165, 'Income Tax', 'Use <b>Income Tax</b> to track the amount of tax liability payable on the profits earned for the period (based on The Income Tax Act).', 0, 0, 'report'),
(166, 'MAT Credit', 'Use <b>MAT Credit</b> to record the amount of Minimum Alternate Tax (MAT) credit used to pay the taxes on the current year’s income.', 0, 0, 'report'),
(167, 'Other expense', 'Use <b>Other expense</b> to track unusual or infrequent expenses that don’t fall into another Other Expense type.', 0, 0, 'report'),
(168, 'Penalties and settlements', 'Use <b>Penalties and settlements</b> to track money you pay for violating laws or regulations, settling lawsuits, or other penalties.', 0, 0, 'report'),
(169, 'Prior Period Items', 'Use <b>Prior Period Items</b> to record income/expenditure items from prior accounting periods that were omitted or erroneously recorded in the prior period.', 0, 0, 'report'),
(170, 'Undeposited fund', '<b>Undeposited fund</b>', 0, 0, 'register')");


$getDB->query("ALTER TABLE `houdinv_detail_type`
ADD PRIMARY KEY (`houdinv_detail_type_id`)");

$getDB->query("ALTER TABLE `houdinv_detail_type`
MODIFY `houdinv_detail_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171");

// table structure for  account type.



 

    //  Table structure for table  houdinv_countries   

                $getDB->query("CREATE TABLE   houdinv_countries   (
          country_id   int(11) NOT NULL,
          country_name   varchar(30) CHARACTER SET utf8 NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

    //  Dumping data for table   houdinv_countries     

                $getDB->query("INSERT INTO   houdinv_countries   (  country_id  ,   country_name  ) VALUES
    (1, 'Aruba'),
    (2, 'Afghanistan'),
    (3, 'Angola'),
    (4, 'Anguilla'),
    (5, 'Albania'),
    (6, 'Andorra'),
    (7, 'Netherlands Antilles'),
    (8, 'United Arab Emirates'),
    (9, 'Argentina'),
    (10, 'Armenia'),
    (11, 'American Samoa'),
    (12, 'Antarctica'),
    (13, 'French Southern territories'),
    (14, 'Antigua and Barbuda'),
    (15, 'Australia'),
    (16, 'Austria'),
    (17, 'Azerbaijan'),
    (18, 'Burundi'),
    (19, 'Belgium'),
    (20, 'Benin'),
    (21, 'Burkina Faso'),
    (22, 'Bangladesh'),
    (23, 'Bulgaria'),
    (24, 'Bahrain'),
    (25, 'Bahamas'),
    (26, 'Bosnia and Herzegovina'),
    (27, 'Belarus'),
    (28, 'Belize'),
    (29, 'Bermuda'),
    (30, 'Bolivia'),
    (31, 'Brazil'),
    (32, 'Barbados'),
    (33, 'Brunei'),
    (34, 'Bhutan'),
    (35, 'Bouvet Island'),
    (36, 'Botswana'),
    (37, 'Central African Republic'),
    (38, 'Canada'),
    (39, 'Cocos (Keeling) Islands'),
    (40, 'Switzerland'),
    (41, 'Chile'),
    (42, 'China'),
    (43, 'CÃ´te dâ€™Ivoire'),
    (44, 'Cameroon'),
    (45, 'Congo, The Democratic Republic'),
    (46, 'Congo'),
    (47, 'Cook Islands'),
    (48, 'Colombia'),
    (49, 'Comoros'),
    (50, 'Cape Verde'),
    (51, 'Costa Rica'),
    (52, 'Cuba'),
    (53, 'Christmas Island'),
    (54, 'Cayman Islands'),
    (55, 'Cyprus'),
    (56, 'Czech Republic'),
    (57, 'Germany'),
    (58, 'Djibouti'),
    (59, 'Dominica'),
    (60, 'Denmark'),
    (61, 'Dominican Republic'),
    (62, 'Algeria'),
    (63, 'Ecuador'),
    (64, 'Egypt'),
    (65, 'Eritrea'),
    (66, 'Western Sahara'),
    (67, 'Spain'),
    (68, 'Estonia'),
    (69, 'Ethiopia'),
    (70, 'Finland'),
    (71, 'Fiji Islands'),
    (72, 'Falkland Islands'),
    (73, 'France'),
    (74, 'Faroe Islands'),
    (75, 'Micronesia, Federated States o'),
    (76, 'Gabon'),
    (77, 'United Kingdom'),
    (78, 'Georgia'),
    (79, 'Ghana'),
    (80, 'Gibraltar'),
    (81, 'Guinea'),
    (82, 'Guadeloupe'),
    (83, 'Gambia'),
    (84, 'Guinea-Bissau'),
    (85, 'Equatorial Guinea'),
    (86, 'Greece'),
    (87, 'Grenada'),
    (88, 'Greenland'),
    (89, 'Guatemala'),
    (90, 'French Guiana'),
    (91, 'Guam'),
    (92, 'Guyana'),
    (93, 'Hong Kong'),
    (94, 'Heard Island and McDonald Isla'),
    (95, 'Honduras'),
    (96, 'Croatia'),
    (97, 'Haiti'),
    (98, 'Hungary'),
    (99, 'Indonesia'),
    (100, 'India'),
    (101, 'British Indian Ocean Territory'),
    (102, 'Ireland'),
    (103, 'Iran'),
    (104, 'Iraq'),
    (105, 'Iceland'),
    (106, 'Israel'),
    (107, 'Italy'),
    (108, 'Jamaica'),
    (109, 'Jordan'),
    (110, 'Japan'),
    (111, 'Kazakstan'),
    (112, 'Kenya'),
    (113, 'Kyrgyzstan'),
    (114, 'Cambodia'),
    (115, 'Kiribati'),
    (116, 'Saint Kitts and Nevis'),
    (117, 'South Korea'),
    (118, 'Kuwait'),
    (119, 'Laos'),
    (120, 'Lebanon'),
    (121, 'Liberia'),
    (122, 'Libyan Arab Jamahiriya'),
    (123, 'Saint Lucia'),
    (124, 'Liechtenstein'),
    (125, 'Sri Lanka'),
    (126, 'Lesotho'),
    (127, 'Lithuania'),
    (128, 'Luxembourg'),
    (129, 'Latvia'),
    (130, 'Macao'),
    (131, 'Morocco'),
    (132, 'Monaco'),
    (133, 'Moldova'),
    (134, 'Madagascar'),
    (135, 'Maldives'),
    (136, 'Mexico'),
    (137, 'Marshall Islands'),
    (138, 'Macedonia'),
    (139, 'Mali'),
    (140, 'Malta'),
    (141, 'Myanmar'),
    (142, 'Mongolia'),
    (143, 'Northern Mariana Islands'),
    (144, 'Mozambique'),
    (145, 'Mauritania'),
    (146, 'Montserrat'),
    (147, 'Martinique'),
    (148, 'Mauritius'),
    (149, 'Malawi'),
    (150, 'Malaysia'),
    (151, 'Mayotte'),
    (152, 'Namibia'),
    (153, 'New Caledonia'),
    (154, 'Niger'),
    (155, 'Norfolk Island'),
    (156, 'Nigeria'),
    (157, 'Nicaragua'),
    (158, 'Niue'),
    (159, 'Netherlands'),
    (160, 'Norway'),
    (161, 'Nepal'),
    (162, 'Nauru'),
    (163, 'New Zealand'),
    (164, 'Oman'),
    (165, 'Pakistan'),
    (166, 'Panama'),
    (167, 'Pitcairn'),
    (168, 'Peru'),
    (169, 'Philippines'),
    (170, 'Palau'),
    (171, 'Papua New Guinea'),
    (172, 'Poland'),
    (173, 'Puerto Rico'),
    (174, 'North Korea'),
    (175, 'Portugal'),
    (176, 'Paraguay'),
    (177, 'Palestine'),
    (178, 'French Polynesia'),
    (179, 'Qatar'),
    (180, 'RÃ©union'),
    (181, 'Romania'),
    (182, 'Russian Federation'),
    (183, 'Rwanda'),
    (184, 'Saudi Arabia'),
    (185, 'Sudan'),
    (186, 'Senegal'),
    (187, 'Singapore'),
    (188, 'South Georgia and the South Sa'),
    (189, 'Saint Helena'),
    (190, 'Svalbard and Jan Mayen'),
    (191, 'Solomon Islands'),
    (192, 'Sierra Leone'),
    (193, 'El Salvador'),
    (194, 'San Marino'),
    (195, 'Somalia'),
    (196, 'Saint Pierre and Miquelon'),
    (197, 'Sao Tome and Principe'),
    (198, 'Suriname'),
    (199, 'Slovakia'),
    (200, 'Slovenia'),
    (201, 'Sweden'),
    (202, 'Swaziland'),
    (203, 'Seychelles'),
    (204, 'Syria'),
    (205, 'Turks and Caicos Islands'),
    (206, 'Chad'),
    (207, 'Togo'),
    (208, 'Thailand'),
    (209, 'Tajikistan'),
    (210, 'Tokelau'),
    (211, 'Turkmenistan'),
    (212, 'East Timor'),
    (213, 'Tonga'),
    (214, 'Trinidad and Tobago'),
    (215, 'Tunisia'),
    (216, 'Turkey'),
    (217, 'Tuvalu'),
    (218, 'Taiwan'),
    (219, 'Tanzania'),
    (220, 'Uganda'),
    (221, 'Ukraine'),
    (222, 'United States Minor Outlying I'),
    (223, 'Uruguay'),
    (224, 'United States'),
    (225, 'Uzbekistan'),
    (226, 'Holy See (Vatican City State)'),
    (227, 'Saint Vincent and the Grenadin'),
    (228, 'Venezuela'),
    (229, 'Virgin Islands, British'),
    (230, 'Virgin Islands, U.S.'),
    (231, 'Vietnam'),
    (232, 'Vanuatu'),
    (233, 'Wallis and Futuna'),
    (234, 'Samoa'),
    (235, 'Yemen'),
    (236, 'Yugoslavia'),
    (237, 'South Africa'),
    (238, 'Zambia'),
    (239, 'Zimbabwe')");

    //  Table structure for table  houdinv_coupons   

                $getDB->query("CREATE TABLE   houdinv_coupons   (
          houdinv_coupons_id   int(11) NOT NULL,
          houdinv_coupons_name   varchar(50) NOT NULL,
          houdinv_coupons_tagline   varchar(50) NOT NULL,
          houdinv_coupons_valid_from   date NOT NULL,
          houdinv_coupons_valid_to   date NOT NULL,
          houdinv_coupons_order_amount   int(11) NOT NULL,
          houdinv_coupons_code   varchar(50) NOT NULL,
          houdinv_coupons_discount_precentage   varchar(20) NOT NULL,
          houdinv_coupons_limit   int(11) NOT NULL,
          houdinv_coupons_payment_method   enum('cod','online payment','both') NOT NULL,
          houdinv_coupons_status   enum('active','deactive') NOT NULL,
          houdinv_coupons_applicable   enum('app','both') NOT NULL,
          houdinv_coupons_created_at   varchar(30) NOT NULL,
          houdinv_coupons_modified_at   varchar(30) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 

    //  Table structure for table  houdinv_coupons_procus   

                $getDB->query("CREATE TABLE   houdinv_coupons_procus   (
          houdinv_coupons_procus_id   int(11) NOT NULL,
          houdinv_coupons_procus_coupon_id   int(11) NOT NULL,
          houdinv_coupons_procus_type   enum('product','customer') NOT NULL,
          houdinv_coupons_procus_user_id   int(11) DEFAULT NULL,
          houdinv_coupons_procus_product_id   int(11) DEFAULT NULL,
          houdinv_coupons_procus_user_group   int(11) NOT NULL DEFAULT '0',
          houdinv_coupons_procus_produdct_category   int(11) NOT NULL DEFAULT '0'
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
      
      
    //  Table structure for table   houdinv_customersetting      

                $getDB->query("CREATE TABLE houdinv_customersetting(id   int(11) NOT NULL,pay_panding_amount   varchar(250) NOT NULL,pay_panding_period   varchar(250) NOT NULL,          allow_pay_panding   int(11) DEFAULT '0',date_time   varchar(250) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //   Dumping data for table   houdinv_customersetting

     //  Table structure for table   houdinv_custom_home_data      

                $getDB->query("CREATE TABLE   houdinv_custom_home_data   (
          houdinv_custom_home_data_id   int(11) NOT NULL,
          houdinv_custom_home_data_category   varchar(100) DEFAULT NULL,
          houdinv_custom_home_data_latest_product   varchar(100) DEFAULT NULL,
          houdinv_custom_home_data_featured_product   varchar(100) DEFAULT NULL,
          houdinv_custom_home_data_testimonial   varchar(100) DEFAULT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    //  Table structure for table   houdinv_custom_links  

                $getDB->query("CREATE TABLE   houdinv_custom_links   (
          houdinv_custom_links_id   int(11) NOT NULL,
          houdinv_custom_links_title   varchar(30) NOT NULL,
          houdinv_custom_links_url   varchar(100) NOT NULL,
          houdinv_custom_links_target   enum('current','new') NOT NULL,
          houdinv_custom_links_created_at   varchar(30) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

// Table structure for table   houdinv_custom_slider  

                $getDB->query("CREATE TABLE   houdinv_custom_slider   (
      houdinv_custom_slider_id   int(11) NOT NULL,
      houdinv_custom_slider_image   text NOT NULL,
      houdinv_custom_slider_head   varchar(30) NOT NULL,
      houdinv_custom_slider_sub_heading   varchar(50) NOT NULL,
      houdinv_custom_slider_created_at   varchar(30) DEFAULT NULL,
      houdinv_custom_slider_modified_at   varchar(30) DEFAULT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

// Table structure for table   houdinv_deliveries  

                $getDB->query("CREATE TABLE   houdinv_deliveries   (
      houdinv_delivery_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_order_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_by   enum('Delivery Boy','Delivery Service') NOT NULL,
      houdinv_delivery_boy_id   int(11) NOT NULL DEFAULT '0',
      houdinv_delivery_courier_id   tinytext,
      houdinv_deliveries_courier_name   varchar(100) DEFAULT NULL,
      houdinv_deliveries_tracking_id   int(11) NOT NULL DEFAULT '0',
      houdinv_deliveries_invoice_url   tinytext NOT NULL,
      houdinv_delivery_created_at   varchar(30) DEFAULT NULL,
      houdinv_delivery_updated_at   varchar(30) DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

// Table structure for table   houdinv_delivery_tracks   

                $getDB->query("CREATE TABLE   houdinv_delivery_tracks   (
      houdinv_delivery_track_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_track_order_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_track_delivery_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_track_updated_by   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_track_date   datetime NOT NULL,
      houdinv_delivery_track_status   tinyint(4) NOT NULL,
      houdinv_delivery_track_comment   text NOT NULL,
      houdinv_delivery_track_created_at   timestamp NULL DEFAULT NULL,
      houdinv_delivery_track_updated_at   timestamp NULL DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

  // Table structure for table   houdinv_delivery_users   

                $getDB->query("CREATE TABLE   houdinv_delivery_users   (
      houdinv_delivery_user_id   int(10) UNSIGNED NOT NULL,
      houdinv_delivery_user_name   varchar(150) DEFAULT NULL,
      houdinv_delivery_user_email   varchar(150) DEFAULT NULL,
      houdinv_delivery_user_contact   varchar(20) DEFAULT NULL,
      houdinv_delivery_user_attributes   text,
      houdinv_delivery_user_address   varchar(350) DEFAULT NULL,
      houdinv_delivery_user_active_status   tinyint(4) DEFAULT '0',
      houdinv_delivery_user_created_at   timestamp NULL DEFAULT NULL,
      houdinv_delivery_user_updated_at   timestamp NULL DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

  // Table structure for table   houdinv_emailsms_payment   

                $getDB->query("CREATE TABLE   houdinv_emailsms_payment   (
      houdinv_emailsms_payment_id   int(11) NOT NULL,
      houdinv_emailsms_payment_type   enum('sms','email') NOT NULL,
      houdinv_emailsms_payment_total_purchase   int(11) NOT NULL,
      houdinv_emailsms_payment_amount   float NOT NULL,
      houdinv_emailsms_payment_status   enum('active','deactive') NOT NULL,
      houdinv_emailsms_payment_created_at   varchar(30) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

  // Table structure for table   houdinv_emailsms_stats   

                $getDB->query("CREATE TABLE   houdinv_emailsms_stats   (
      houdinv_emailsms_stats_id   int(11) NOT NULL,
      houdinv_emailsms_stats_type   enum('sms','email') NOT NULL,
      houdinv_emailsms_stats_total_credit   int(11) NOT NULL,
      houdinv_emailsms_stats_remaining_credits   int(11) NOT NULL,
      date_time   varchar(250) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

    // Table structure for table   houdinv_email_log   

                $getDB->query("CREATE TABLE   houdinv_email_log   (
          houdinv_email_log_id   int(11) NOT NULL,
          houdinv_email_log_credit_used   int(11) NOT NULL,
          houdinv_email_log_status   int(1) NOT NULL,
          houdinv_email_log_created_at   varchar(30) DEFAULT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_email_Template   

                $getDB->query("CREATE TABLE   houdinv_email_Template   (
              houdinv_email_template_id   int(11) NOT NULL,
              houdinv_email_template_type   varchar(50) NOT NULL,
              houdinv_email_template_subject   varchar(255) NOT NULL,
              houdinv_email_template_message   text NOT NULL,
              houdinv_email_template_created_date   varchar(100) NOT NULL,
              houdinv_email_template_updated_date   varchar(100) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_extra_expence   

                $getDB->query("CREATE TABLE   houdinv_extra_expence   (
              houdinv_extra_expence_id   int(11) NOT NULL,
              houdinv_extra_expence_payee   int(11) NOT NULL,
              houdinv_extra_expence_payee_type   enum('customer','staff','supplier') NOT NULL,
              houdinv_extra_expence_payment_method   enum('cash','card','cheque') NOT NULL,
              houdinv_extra_expence_subtotal   float NOT NULL,
              houdinv_extra_expence_tax   float NOT NULL,
              houdinv_extra_expence_total_amount   float NOT NULL,
              houdinv_extra_expence_payment_status   int(1) NOT NULL,
              houdinv_extra_expence_transaction_type   enum('credit','debit') NOT NULL,
              houdinv_extra_expence_refrence_number   varchar(50) NOT NULL,
              houdinv_extra_expence_payment_date   date NOT NULL,
              `houdinv_extra_expence_type` enum('products','payment') NOT NULL,
              houdinv_extra_expence_created_at   varchar(30) DEFAULT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_extra_expence_item   

                $getDB->query("CREATE TABLE   houdinv_extra_expence_item   (
              houdinv_extra_expence_item_id   int(11) NOT NULL,
              houdinv_extra_expence_item_item_id   int(11) NOT NULL,
              houdinv_extra_expence_item_type   enum('inv','ninv') NOT NULL,
              houdinv_extra_expence_item_quantity   float NOT NULL,
              houdinv_extra_expence_item_rate   float NOT NULL,
              houdinv_extra_expence_item_tax   float NOT NULL,
              houdinv_extra_expence_item_total_amount   float NOT NULL,
              houdinv_extra_expence_id   int(11) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_forgot_password   

                $getDB->query("CREATE TABLE   houdinv_forgot_password   (
              houdinv_forgot_password_email   varchar(150) NOT NULL,
              houdinv_forgot_password_token   varchar(50) NOT NULL,
              houdinv_forgot_password_created_at   timestamp NULL DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_google_analytics   

                $getDB->query("CREATE TABLE   houdinv_google_analytics   (
              id   int(11) NOT NULL,
              analytics_number   varchar(250) DEFAULT NULL,
              date_time   varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_inventories   

                $getDB->query("CREATE TABLE   houdinv_inventories   (
              houdinv_inventory_id   int(10) UNSIGNED NOT NULL,
              houdinv_inventory_supplier_id   int(10) UNSIGNED NOT NULL,
              houdinv_inventory_supplier_product_id   int(10) UNSIGNED NOT NULL,
              houdinv_inventory_quantity   decimal(10,0) DEFAULT NULL,
              houdinv_inventory_quantity_left   decimal(10,0) NOT NULL,
              houdinv_inventory_created_at   timestamp NULL DEFAULT NULL,
              houdinv_inventory_updated_at   timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

          // Table structure for table   houdinv_inventorysetting   

                $getDB->query("CREATE TABLE   houdinv_inventorysetting   (
              id   int(11) NOT NULL,
              inventorysetting_number   int(200) NOT NULL,
              Universal_rl   int(20) DEFAULT NULL,
              date_time   varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

            // Table structure for table   houdinv_inventory_purchase   

                $getDB->query("CREATE TABLE   houdinv_inventory_purchase   (
                  houdinv_inventory_purchase_id   int(11) NOT NULL,
                  houdinv_inventory_purchase_credit   enum('one day','one week','one month') NOT NULL,
                  `houdinv_inventory_purchase_time` int(11) NOT NULL DEFAULT '0',
                  houdinv_inventory_purchase_supplier_id   varchar(200) NOT NULL,
                  houdinv_inventory_purchase_delivery   varchar(30) NOT NULL,
                  houdinv_inventory_purchase_status   enum('in process','accpeted','recieve','return') NOT NULL,
                  houdinv_inventory_purchase_recieved_by   int(11) DEFAULT '0',
                  houdinv_inventory_purchase_created_at   varchar(30) DEFAULT NULL,
                  houdinv_inventory_purchase_modified_at   varchar(30) DEFAULT NULL
              ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
              
        // Table structure for table   houdinv_inventory_tracks   

                $getDB->query("CREATE TABLE   houdinv_inventory_tracks   (
                  houdinv_inventory_track_id   int(10) UNSIGNED NOT NULL,
                  houdinv_inventory_track_product_id   int(10) UNSIGNED NOT NULL,
                  houdinv_inventory_track_supplier_id   int(10) UNSIGNED NOT NULL,
                  houdinv_inventory_track_product_qty   decimal(10,0) NOT NULL,
                  houdinv_inventory_track_product_cost   float NOT NULL,
                  houdinv_inventory_track_created_at   timestamp NULL DEFAULT NULL,
                  houdinv_inventory_track_updated_at   timestamp NULL DEFAULT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

            // Table structure for table   houdinv_invoicesetting   

                $getDB->query("CREATE TABLE   houdinv_invoicesetting   (
                  id   int(250) NOT NULL,
                  show_task_breakup   int(11) DEFAULT NULL,
                  total_saving_invoice   int(11) DEFAULT NULL,
                  invoice_number_receipt   int(11) DEFAULT NULL,
                  order_number_receipt   int(11) DEFAULT NULL,
                  product_wise   int(11) DEFAULT NULL,
                  invoice_num_start   varchar(11) DEFAULT NULL,
                  date_time   varchar(200) NOT NULL
              ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

// Table structure for table   houdinv_navigation_store_pages  

        $getDB->query("CREATE TABLE houdinv_navigation_store_pages (
            houdinv_navigation_store_pages_id int(11) NOT NULL,
            houdinv_navigation_store_pages_name varchar(30) NOT NULL,
            houdinv_navigation_store_pages_category_id int(11) NOT NULL DEFAULT '0',
            houdinv_navigation_store_pages_category_name varchar(100) DEFAULT NULL,
            houdinv_navigation_store_pages_custom_link_id int(11) DEFAULT '0',
            houdinv_navigation_store_pages_custom_link_name varchar(30) DEFAULT NULL
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
            // Table structure for table   houdinv_noninventory_products   

                $getDB->query("CREATE TABLE   houdinv_noninventory_products   (
                  houdinv_noninventory_products_id   int(11) NOT NULL,
                  houdinv_noninventory_products_name   varchar(100) NOT NULL,
                  houdinv_noninventory_products_desc   text NOT NULL,
                  houdinv_noninventory_products_price   float NOT NULL,
                  houdinv_noninventory_products_created_at   varchar(30) DEFAULT NULL
              ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
            // Table structure for table   houdinv_offline_payment_settings   

                $getDB->query("CREATE TABLE   houdinv_offline_payment_settings   (
                  houdinv_offline_payment_setting_id   int(11) UNSIGNED NOT NULL,
                  houdinv_offline_payment_setting_user_id   int(10) UNSIGNED NOT NULL,
                  houdinv_offline_payment_setting_holder_name   varchar(150) DEFAULT NULL,
                  houdinv_offline_payment_setting_holder_number   varchar(150) DEFAULT NULL,
                  houdinv_offline_payment_setting_country   varchar(150) DEFAULT NULL,
                  houdinv_offline_payment_setting_bank_name   varchar(200) DEFAULT NULL,
                  houdinv_offline_payment_setting_iban   varchar(50) DEFAULT NULL,
                  houdinv_offline_payment_setting_shift_code   varchar(50) DEFAULT NULL,
                  houdinv_offline_payment_setting_additional_information   text,
                  houdinv_offline_payment_setting_created_at   timestamp NULL DEFAULT NULL,
                  houdinv_offline_payment_setting_updated_at   timestamp NULL DEFAULT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

            // Table structure for table   houdinv_online_payment_settings   

                $getDB->query("CREATE TABLE   houdinv_online_payment_settings   (
                  houdinv_online_payment_setting_id   int(11) NOT NULL,
                  houdinv_online_payment_setting_gateway_name   varchar(150) NOT NULL,
                  houdinv_online_payment_setting_client_id   varchar(150) NOT NULL,
                  houdinv_online_payment_setting_secret_id   varchar(150) NOT NULL,
                  houdinv_online_payment_setting_mode   varchar(20) NOT NULL,
                  houdinv_online_payment_setting_active_status   tinyint(4) NOT NULL DEFAULT '0',
                  houdinv_online_payment_setting_created_at   timestamp NULL DEFAULT NULL,
                  houdinv_online_payment_setting_updated_at   timestamp NULL DEFAULT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1");
                
            // Table structure for table   houdinv_orders   

                $getDB->query("CREATE TABLE   houdinv_orders   (
                  houdinv_order_id   int(10) UNSIGNED NOT NULL,
                  houdinv_order_user_id   int(10) UNSIGNED NOT NULL DEFAULT '0',
                  houdinv_order_product_detail   text NOT NULL,
                  houdinv_confirm_order_product_detail   text,
                  houdinv_order_confirm_outlet   int(11) NOT NULL DEFAULT '0',
                  houdinv_orders_confirm_by   int(11) NOT NULL DEFAULT '0',
                  houdinv_orders_confirm_by_role   int(1) NOT NULL DEFAULT '0',
                  houdinv_order_type   enum('App','Website','Store','Work Order') DEFAULT NULL,
                  houdinv_order_delivery_type   varchar(100) NOT NULL,
                  houdinv_order_payment_method   varchar(100) NOT NULL,
                  houdinv_payment_status   int(11) NOT NULL DEFAULT '0',
                  houdinv_orders_refund_status   int(1) NOT NULL DEFAULT '0',
                  houdinv_orders_discount   float NOT NULL,
                  houdinv_orders_discount_detail_id   int(11) NOT NULL,
                  houdinv_orders_total_Amount   float NOT NULL,
                  houdinv_orders_total_paid   float NOT NULL DEFAULT '0',
                  houdinv_orders_total_remaining   float NOT NULL DEFAULT '0',
                  houdinv_order_comments   tinytext,
                  houdinv_delivery_charge   int(11) DEFAULT '0',
                  houdinv_order_confirmation_status   enum('unbilled','Delivered','Not Delivered','cancel','return','return request','cancel request','order pickup','billed','assigned') NOT NULL,
                  houdinv_orders_delivery_status   int(11) NOT NULL,
                  houdinv_orders_deliverydate   date DEFAULT NULL,
                  houdinv_order_delivery_address_id   int(10) UNSIGNED NOT NULL,
                  houdinv_order_billing_address_id   int(11) NOT NULL,
                  houdinv_order_created_at   varchar(100) DEFAULT NULL,
                  houdinv_order_updated_at   varchar(100) DEFAULT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1");
                // Table structure for table   houdinv_order_users   

                $getDB->query("CREATE TABLE   houdinv_order_users   (
                      houdinv_order_users_id   int(11) NOT NULL,
                      houdinv_order_users_order_id   int(11) NOT NULL,
                      houdinv_order_users_name   varchar(50) NOT NULL,
                      houdinv_order_users_contact   varchar(20) NOT NULL,
                      houdinv_order_users_main_address   varchar(255) DEFAULT NULL,
                      houdinv_order_users_city   varchar(50) DEFAULT NULL,
                      houdinv_order_users_zip   varchar(20) DEFAULT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Table structure for table   houdinv_payment_gateway   

                $getDB->query("CREATE TABLE houdinv_payment_gateway (
                    houdinv_payment_gateway_id int(11) NOT NULL,
                    houdinv_payment_gateway_type enum('payu','auth') NOT NULL,
                    houdinv_payment_gateway_merchnat_key varchar(100) NOT NULL,
                    houdinv_payment_gateway_merchant_salt varchar(100) NOT NULL,
                    houdinv_payment_gateway_status enum('active','deactive') NOT NULL,
                    houdinv_payment_gateway_created_at varchar(30) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_payment_gateway

                $getDB->query("INSERT INTO houdinv_payment_gateway (houdinv_payment_gateway_id, houdinv_payment_gateway_type, houdinv_payment_gateway_merchnat_key, houdinv_payment_gateway_merchant_salt, houdinv_payment_gateway_status, houdinv_payment_gateway_created_at) VALUES
                (1, 'payu', 'MBVIetjo', 'U3YohhxRvu', 'active', '1535068800'),
                (2, 'auth', '6Qq6Bfk2P', '8B533TWkc5Y4ug46', 'active', '1536192000')");

                // Table structure for table   houdinv_possetting   

                $getDB->query("CREATE TABLE houdinv_possetting (
                    id int(111) NOT NULL,
                    minimum_order_amount varchar(250) NOT NULL,
                    delivery_charges varchar(250) NOT NULL,
                    free_delivery int(250) DEFAULT NULL,
                    free_delivery_val varchar(250) DEFAULT NULL,
                    bill_out_stock int(250) DEFAULT NULL,
                    inventory_status int(250) DEFAULT NULL,
                    net_weight_content int(250) DEFAULT NULL,
                    adjustment_during int(250) DEFAULT NULL,
                    allow_sp_desc int(250) DEFAULT NULL,
                    serial_number int(250) DEFAULT NULL,
                    billing_custom_product int(250) DEFAULT NULL,
                    mobile_verify int(250) DEFAULT NULL,
                    accept_payment int(250) DEFAULT NULL,
                    same_barcode int(250) DEFAULT NULL,
                    same_mrp int(250) DEFAULT NULL,
                    fifo int(250) DEFAULT NULL,
                    a_b int(11) DEFAULT NULL,
                    date_time varchar(250) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Table structure for table   houdinv_products   

                $getDB->query("CREATE TABLE `houdinv_products` (
                    `houdin_products_id` int(11) NOT NULL,
                    `houdin_products_category` varchar(255) NOT NULL,
                    `houdin_products_sub_category_level_1` varchar(255) NOT NULL,
                    `houdin_products_sub_category_level_2` varchar(255) NOT NULL,
                    `houdin_products_title` varchar(255) NOT NULL,
                    `houdin_products_short_desc` text NOT NULL,
                    `houdin_products_desc` longblob NOT NULL,
                    `houdin_products_pag_title` varchar(100) NOT NULL,
                    `houdin_product_type` int(11) NOT NULL,
                    `houdin_product_type_attr_value` varchar(255) NOT NULL,
                    `houdinv_products_show_on` varchar(100) NOT NULL,
                    `houdin_products_barcode_image` varchar(100) NOT NULL,
                    `houdin_products_suppliers` varchar(255) NOT NULL,
                    `houdin_products_warehouse` varchar(255) NOT NULL,
                    `houdin_products_price` varchar(150) NOT NULL,
                    `houdin_products_final_price` int(11) DEFAULT '0',
                    `houdin_products_main_sku` varchar(100) NOT NULL,
                    `houdinv_products_main_stock` varchar(255) NOT NULL,
                    `houdinv_products_total_stocks` int(11) DEFAULT '0',
                    `houdinv_products_main_minimum_order` int(11) NOT NULL DEFAULT '0',
                    `houdinv_products_main_sort_order` int(11) NOT NULL DEFAULT '0',
                    `houdinv_products_main_shipping` text NOT NULL,
                    `houdinv_products_main_featured` int(11) NOT NULL DEFAULT '0',
                    `houdinv_products_main_quotation` int(11) NOT NULL DEFAULT '0',
                    `houdinv_products_main_payment` text NOT NULL,
                    `houdinv_products_main_images` longtext NOT NULL,
                    `houdinv_products_sell_as` enum('packaged','loose','both') NOT NULL,
                    `houdinv_products_unit` varchar(50) NOT NULL,
                    `houdinv_products_batch_number` varchar(100) NOT NULL,
                    `houdinv_products_expiry` date DEFAULT NULL,
                    `houdin_products_status` int(11) NOT NULL DEFAULT '0',
                    `houdinv_products_main_other_variant` text NOT NULL,
                    `houdin_products_created_date` varchar(255) NOT NULL,
                    `houdin_products_updated_date` varchar(255) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
                // Table structure for table   houdinv_possetting   

                $getDB->query("CREATE TABLE `houdinv_products_variants` (
                    `houdin_products_variants_id` int(11) NOT NULL,
                    `houdin_products_variants_new` varchar(255) NOT NULL,
                    `houdinv_products_variants_name` varchar(70) NOT NULL,
                    `houdinv_products_variants_barcode` varchar(100) NOT NULL,
                    `houdinv_products_variants_new_price` varchar(200) NOT NULL,
                    `houdin_products_variants_title` varchar(255) NOT NULL,
                    `houdin_products_variants_prices` varchar(255) NOT NULL,
                    `houdinv_products_variants_final_price` float DEFAULT '0',
                    `houdin_products_variants_sku` varchar(100) NOT NULL,
                    `houdin_products_variants_stock` text NOT NULL,
                    `houdinv_products_variants_total_stocks` int(11) NOT NULL DEFAULT '0',
                    `houdin_products_variants_image` longtext DEFAULT NULL,
                    `houdin_products_variants_default` int(1) NOT NULL,
                    `houdin_products_variants_product_id` int(11) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
                // Table structure for table   houdinv_productType   

                $getDB->query("CREATE TABLE houdinv_productType (
                    houdinv_productType_id int(11) NOT NULL,
                    houdinv_productType_image varchar(255) NOT NULL,
                    houdinv_productType_name varchar(150) NOT NULL,
                    houdinv_productType_description longtext NOT NULL,
                    houdinv_productType_subattr longblob NOT NULL,
                    houdinv_productType_status enum('active','deactive','block','') NOT NULL,
                    houdinv_productType_created_date varchar(255) NOT NULL,
                    houdinv_productType_update_date varchar(255) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
                // Table structure for table   houdinv_product_reviews   

                $getDB->query("CREATE TABLE houdinv_product_reviews (
                    houdinv_product_review_id int(10) UNSIGNED NOT NULL,
                    houdinv_product_review_user_id int(10) DEFAULT '0',
                    houdinv_product_review_user_email varchar(255) NOT NULL,
                    houdinv_product_review_user_name varchar(255) NOT NULL,
                    houdinv_product_review_product_id int(10) DEFAULT '0',
                    houdinv_product_reviews_variant_id int(11) DEFAULT '0',
                    houdinv_product_review_message text NOT NULL,
                    houdinv_product_review_rating int(11) DEFAULT NULL,
                    houdinv_product_review_created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");
                // Table structure for table   houdinv_product_types   

                $getDB->query("CREATE TABLE houdinv_product_types (
                    houdinv_product_type_id int(10) UNSIGNED NOT NULL,
                    houdinv_product_type_name varchar(150) NOT NULL,
                    houdinv_product_type_description tinytext,
                    houdinv_product_type_created_at timestamp NULL DEFAULT NULL,
                    houdinv_product_type_updated_at timestamp NULL DEFAULT NULL
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

                  // Table structure for table   houdinv_purchase_products   

                $getDB->query("CREATE TABLE houdinv_purchase_products (
                    houdinv_purchase_products_id int(11) NOT NULL,
                    houdinv_purchase_products_purchase_id int(11) NOT NULL,
                    houdinv_purchase_products_product_id int(11) NOT NULL,
                    houdinv_purchase_products_product_variant_id int(11) NOT NULL,
                    houdinv_purchase_products_quantity int(11) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
                // Table structure for table   houdinv_purchase_products_inward   

                $getDB->query("CREATE TABLE houdinv_purchase_products_inward (
                    houdinv_purchase_products_inward_id int(11) NOT NULL,
                    houdinv_purchase_products_inward_purchase_product_id int(11) NOT NULL,
                    houdinv_purchase_products_inward_variant_id int(11) NOT NULL DEFAULT '0',
                    houdinv_purchase_products_inward_purchase_id int(11) NOT NULL DEFAULT '0',
                    houdinv_purchase_products_inward_quantity_recieved int(11) NOT NULL,
                    houdinv_purchase_products_inward_quantity_return int(11) DEFAULT '0',
                    houdinv_purchase_products_inward_sale_price int(11) NOT NULL,
                    houdinv_purchase_products_inward_cost_price int(11) NOT NULL,
                    houdinv_purchase_products_inward_retail_price int(11) NOT NULL,
                    houdinv_purchase_products_inward_total_amount int(11) NOT NULL,
                    houdinv_purchase_products_inward_payment_status enum('paid','not paid') NOT NULL,
                    houdinv_purchase_products_inward_amount_paid int(11) NOT NULL,
                    houdinv_purchase_products_inward_outlet_id int(11) NOT NULL,
                    houdinv_purchase_products_inward_craeted_at varchar(30) NOT NULL,
                    houdinv_purchase_products_inward_modified_at varchar(30) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Table structure for table   houdinv_push_Template   

                $getDB->query("CREATE TABLE houdinv_push_Template (
                    houdinv_push_template_id int(11) NOT NULL,
                    houdinv_push_template_type varchar(50) NOT NULL,
                    houdinv_push_template_subject varchar(255) NOT NULL,
                    houdinv_push_template_message text NOT NULL,
                    houdinv_push_template_created_date varchar(100) NOT NULL,
                    houdinv_push_template_updated_date varchar(100) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Table structure for table   houdinv_shipping_charges   

                $getDB->query("CREATE TABLE houdinv_shipping_charges (
                    houdinv_shipping_charge_id int(11) NOT NULL,
                    houdinv_shipping_charge_country varchar(150) DEFAULT NULL,
                    houdinv_shipping_charge_cost float DEFAULT NULL,
                    houdinv_shipping_charge_created_at timestamp NULL DEFAULT NULL,
                    houdinv_shipping_charge_updated_at timestamp NULL DEFAULT NULL
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

                  // Table structure for table   houdinv_shipping_credentials   

                $getDB->query("CREATE TABLE houdinv_shipping_credentials (
                    houdinv_shipping_credentials_id int(11) NOT NULL,
                    houdinv_shipping_credentials_key tinytext NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1 ");

                // Dumping data for table houdinv_shipping_credentials

                $getDB->query("INSERT INTO houdinv_shipping_credentials (houdinv_shipping_credentials_id, houdinv_shipping_credentials_key) VALUES
                (1, '26593907-0a55-4b55-a719-12bf280e6062')");

                // Table structure for table   houdinv_shipping_data   

                $getDB->query("CREATE TABLE houdinv_shipping_data (
                    houdinv_shipping_data_id int(11) NOT NULL,
                    houdinv_shipping_data_first_name varchar(100) NOT NULL,
                    houdinv_shipping_data_last_name varchar(100) NOT NULL,
                    houdinv_shipping_data_company_name varchar(100) NOT NULL,
                    houdinv_shipping_data_email varchar(100) NOT NULL,
                    houdinv_shipping_data_phone_no varchar(100) NOT NULL,
                    houdinv_shipping_data_country varchar(100) NOT NULL,
                    houdinv_shipping_data_address text NOT NULL,
                    houdinv_shipping_data_zip varchar(12) NOT NULL,
                    houdinv_shipping_data_city varchar(100) NOT NULL,
                    houdinv_shipping_data_user_id int(11) NOT NULL,
                    houdinv_shipping_data_order_note text NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1"); 

                $getDB->query("CREATE TABLE houdinv_shipping_ruels (
                    id int(250) NOT NULL,
                    shipping_min_value varchar(250) NOT NULL,
                    shipping_charge varchar(250) NOT NULL,
                    shiping_active int(250) DEFAULT '0',
                    date_time varchar(250) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                 
                // Table structure for table   houdinv_shipping_warehouse   

                $getDB->query("CREATE TABLE houdinv_shipping_warehouse (
                    id int(250) NOT NULL,
                    w_name varchar(250) NOT NULL,
                    w_pickup_name varchar(250) NOT NULL,
                    w_pickup_phone varchar(250) NOT NULL,
                    w_warehouse int(11) NOT NULL,
                    w_pickup_country varchar(250) DEFAULT NULL,
                    w_pickup_state varchar(250) DEFAULT NULL,
                    w_pickup_city varchar(250) DEFAULT NULL,
                    w_pickup_address varchar(250) DEFAULT NULL,
                    w_pickup_zip varchar(250) DEFAULT NULL,
                    w_date_time varchar(250) DEFAULT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
                // Table structure for table   houdinv_shop_applogo   

                $getDB->query("CREATE TABLE houdinv_shop_applogo (
                    applogo_id int(11) NOT NULL,
                    applogo varchar(250) NOT NULL,
                    splashscreen varchar(250) NOT NULL,
                    applogo_clear int(11) NOT NULL DEFAULT '0',
                    splashscreen_clear int(11) NOT NULL DEFAULT '0',
                    date_time varchar(200) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Table structure for table   houdinv_shop_ask_quotation   

                $getDB->query("CREATE TABLE houdinv_shop_ask_quotation (
                    houdinv_shop_ask_quotation_id int(11) NOT NULL,
                    houdinv_shop_ask_quotation_name varchar(100) NOT NULL,
                    houdinv_shop_ask_quotation_email varchar(100) NOT NULL,
                    houdinv_shop_ask_quotation_comment text NOT NULL,
                    houdinv_shop_ask_quotation_phone varchar(50) NOT NULL,
                    houdinv_shop_ask_quotation_product_id int(11) NOT NULL,
                    houdinv_shop_ask_quotation_product_count int(11) NOT NULL,
                    houdinv_shop_ask_quotation_created_date varchar(100) NOT NULL,
                    houdinv_shop_ask_quotation_update_date varchar(100) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1"); 

                // Table structure for table   houdinv_shop_configurations   

                $getDB->query("CREATE TABLE houdinv_shop_configurations (
                    houdinv_shop_configuration_google_analytics varchar(150) DEFAULT NULL,
                    houdinv_shop_configuration_bing_analytics varchar(150) DEFAULT NULL,
                    houdinv_shop_configuration_product_review tinyint(4) DEFAULT '0',
                    houdinv_shop_configuration_category_direction varchar(10) DEFAULT NULL,
                    houdinv_shop_configuration_shop_logo varchar(150) DEFAULT NULL,
                    houdinv_shop_configuration_product_limit tinyint(4) DEFAULT '4',
                    houdinv_shop_configuration_tag_line varchar(200) DEFAULT NULL,
                    houdinv_shop_configuration_ad_active tinyint(4) DEFAULT '0',
                    houdinv_shop_configuration_currency int(11) DEFAULT NULL,
                    houdinv_shop_configuration_language int(11) DEFAULT NULL,
                    houdinv_shop_configuration_created_at timestamp NULL DEFAULT NULL,
                    houdinv_shop_configuration_updated_at timestamp NULL DEFAULT NULL
                  ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

                // Table structure for table   houdinv_shop_detail   

                $getDB->query("CREATE TABLE houdinv_shop_detail (
                    houdinv_shop_id int(11) NOT NULL,
                    houdinv_shop_title varchar(255) NOT NULL,
                    houdinv_shop_business_name varchar(255) NOT NULL,
                    houdinv_shop_address varchar(255) NOT NULL,
                    houdinv_shop_contact_info varchar(255) NOT NULL,
                    `profile_image` varchar(255) DEFAULT NULL,
                    houdinv_shop_communication_email varchar(100) NOT NULL,
                    houdinv_shop_order_email varchar(100) NOT NULL,
                    houdinv_shop_customer_care_email varchar(100) NOT NULL,
                    houdinv_shop_pan varchar(150) NOT NULL,
                    houdinv_shop_tin varchar(100) NOT NULL,
                    houdinv_shop_cst varchar(100) NOT NULL,
                    houdinv_shop_vat varchar(100) NOT NULL,
                    houdinv_shop_custom_domain varchar(255) NOT NULL,
                    houdinv_shop_ssl varchar(255) NOT NULL,
                    houdinv_shop_cretaed_date varchar(100) NOT NULL,
                    houdinv_shop_updated_date varchar(100) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
                // Table structure for table   houdinv_shop_logo   

                $getDB->query("CREATE TABLE houdinv_shop_logo (
                    id int(200) NOT NULL,
                    shop_logo varchar(200) NOT NULL,
                    shop_favicon varchar(200) NOT NULL,
                    update_date varchar(250) NOT NULL,
                    clear int(250) NOT NULL,
                    clear_favicon int(20) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 

                // Table structure for table   houdinv_shop_order_configuration   

                $getDB->query("CREATE TABLE houdinv_shop_order_configuration (
                    houdinv_shop_order_configuration_id int(11) NOT NULL,
                    houdinv_shop_order_configuration_cancellation varchar(100) DEFAULT NULL,
                    houdinv_shop_order_configuration_return varchar(100) DEFAULT NULL,
                    houdinv_shop_order_configuration_replace varchar(100) DEFAULT NULL,
                    houdinv_shop_order_configuration_created_date varchar(30) DEFAULT NULL,
                    houdinv_shop_order_configuration_modified_date varchar(30) DEFAULT NULL,
                    houdinv_shop_order_configuration_sms_email int(11) NOT NULL,
                    houdinv_shop_order_configuration_printing int(11) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 

                // Table structure for table   houdinv_shop_query   

                $getDB->query("CREATE TABLE houdinv_shop_query (
                    houdinv_shop_query_id int(11) NOT NULL,
                    houdinv_shop_query_name varchar(30) NOT NULL,
                    houdinv_shop_query_email varchar(30) NOT NULL,
                    houdinv_shop_query_subject varchar(100) NOT NULL,
                    houdinv_shop_query_message text NOT NULL,
                    houdinv_shop_query_created_at timestamp NULL DEFAULT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Table structure for table   houdinv_skusetting   

                $getDB->query("CREATE TABLE houdinv_skusetting (
                    id int(11) NOT NULL,
                    skusetting_number varchar(250) NOT NULL,
                    date_time varchar(250) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_skusetting  

                // $getDB->query("INSERT INTO houdinv_skusetting (id, skusetting_number, date_time) VALUES
                // (1, 'Hawks-_', '1531407249')"); 

                 // Table structure for table   houdinv_houdinv_sms_logskusetting   

                $getDB->query("CREATE TABLE houdinv_sms_log (
                    houdinv_sms_log_id int(11) NOT NULL,
                    houdinv_sms_log_credit_used int(11) NOT NULL,
                    houdinv_sms_log_status int(1) NOT NULL,
                    houdinv_sms_log_created_at varchar(30) NOT NULL
                  ) ENGINE=MyISAM DEFAULT CHARSET=latin1");

                // Dumping data for table houdinv_sms_log  

        // Table structure for table   houdinv_sms_template   

                $getDB->query("CREATE TABLE houdinv_sms_template (
            houdinv_sms_template_id int(11) NOT NULL,
            houdinv_sms_template_type varchar(50) NOT NULL,
            houdinv_sms_template_subject varchar(255) NOT NULL,
            houdinv_sms_template_message text NOT NULL,
            houdinv_sms_template_created_date varchar(100) NOT NULL,
            houdinv_sms_template_updated_date varchar(100) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
          // Table structure for table   houdinv_social_links   

                $getDB->query("CREATE TABLE houdinv_social_links (
            social_id int(200) NOT NULL,
            twitter_url varchar(250) NOT NULL,
            facebook_url varchar(250) NOT NULL,
            google_url varchar(250) NOT NULL,
            youtube_url varchar(250) NOT NULL,
            instagram_url varchar(250) NOT NULL,
            pinterest_url varchar(250) NOT NULL,
            linkedin_url varchar(250) NOT NULL,
            date_time varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
        // Table structure for table   houdinv_staff_management   

                $getDB->query("CREATE TABLE houdinv_staff_management (
            staff_id int(11) NOT NULL,
            staff_name varchar(250) NOT NULL,
            staff_contact_number varchar(250) NOT NULL,
            staff_department varchar(250) NOT NULL,
            staff_status int(11) NOT NULL,
            staff_warehouse int(11) NOT NULL,
            staff_email varchar(250) NOT NULL,
            staff_password varchar(250) DEFAULT NULL,
            staff_password_salt varchar(150) DEFAULT NULL,
            staff_alternat_contact varchar(250) DEFAULT NULL,
            staff_address longtext NOT NULL,
            password_send varchar(250) DEFAULT NULL,
            houdinv_staff_auth_token varchar(255) NOT NULL,
            houdinv_staff_auth_url_token varchar(255) NOT NULL,
            create_date varchar(200) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table   houdinv_staff_management_sub   

                $getDB->query("CREATE TABLE houdinv_staff_management_sub (
            id int(11) NOT NULL,
            staff_id int(11) NOT NULL,
            dashboard int(11) DEFAULT NULL,
            `order` int(11) DEFAULT NULL,
            cataogry int(11) DEFAULT NULL,
            product int(11) DEFAULT NULL,
            setting int(11) DEFAULT NULL,
            staff_members int(11) DEFAULT NULL,
            inventory int(11) DEFAULT NULL,
            delivery int(11) DEFAULT NULL,
            discount int(11) DEFAULT NULL,
            supplier_management int(11) DEFAULT NULL,
            templates int(11) DEFAULT NULL,
            purchase int(11) DEFAULT NULL,
            coupons int(11) DEFAULT NULL,
            gift_voucher int(11) DEFAULT NULL,
            calendar int(11) DEFAULT NULL,
            POS int(11) DEFAULT NULL,
            store_setting int(11) DEFAULT NULL,
            tax int(11) DEFAULT NULL,
            shipping int(11) DEFAULT NULL,
            campaign int(11) DEFAULT NULL,
            customer_management int(11) DEFAULT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table   houdinv_states   

                $getDB->query("CREATE TABLE houdinv_states (
            state_id int(11) NOT NULL,
            state_name varchar(30) COLLATE utf8_unicode_ci NOT NULL,
            country_id int(11) NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

        // Dumping data for table houdinv_states

                $getDB->query("INSERT INTO houdinv_states (state_id, state_name, country_id) VALUES
        (240, 'â€“', 1),
        (241, 'Balkh', 2),
        (242, 'Herat', 2),
        (243, 'Kabol', 2),
        (244, 'Qandahar', 2),
        (245, 'Benguela', 3),
        (246, 'Huambo', 3),
        (247, 'Luanda', 3),
        (248, 'Namibe', 3),
        (249, 'â€“', 4),
        (250, 'Tirana', 5),
        (251, 'Andorra la Vella', 6),
        (252, 'CuraÃ§ao', 7),
        (253, 'Abu Dhabi', 8),
        (254, 'Ajman', 8),
        (255, 'Dubai', 8),
        (256, 'Sharja', 8),
        (257, 'Buenos Aires', 9),
        (258, 'Catamarca', 9),
        (259, 'CÃ³rdoba', 9),
        (260, 'Chaco', 9),
        (261, 'Chubut', 9),
        (262, 'Corrientes', 9),
        (263, 'Distrito Federal', 9),
        (264, 'Entre Rios', 9),
        (265, 'Formosa', 9),
        (266, 'Jujuy', 9),
        (267, 'La Rioja', 9),
        (268, 'Mendoza', 9),
        (269, 'Misiones', 9),
        (270, 'NeuquÃ©n', 9),
        (271, 'Salta', 9),
        (272, 'San Juan', 9),
        (273, 'San Luis', 9),
        (274, 'Santa FÃ©', 9),
        (275, 'Santiago del Estero', 9),
        (276, 'TucumÃ¡n', 9),
        (277, 'Lori', 10),
        (278, 'Yerevan', 10),
        (279, 'Å irak', 10),
        (280, 'Tutuila', 11),
        (281, 'St John', 14),
        (282, 'Capital Region', 15),
        (283, 'New South Wales', 15),
        (284, 'Queensland', 15),
        (285, 'South Australia', 15),
        (286, 'Tasmania', 15),
        (287, 'Victoria', 15),
        (288, 'West Australia', 15),
        (289, 'KÃ¤rnten', 16),
        (290, 'North Austria', 16),
        (291, 'Salzburg', 16),
        (292, 'Steiermark', 16),
        (293, 'Tiroli', 16),
        (294, 'Wien', 16),
        (295, 'Baki', 17),
        (296, 'GÃ¤ncÃ¤', 17),
        (297, 'MingÃ¤Ã§evir', 17),
        (298, 'Sumqayit', 17),
        (299, 'Bujumbura', 18),
        (300, 'Antwerpen', 19),
        (301, 'Bryssel', 19),
        (302, 'East Flanderi', 19),
        (303, 'Hainaut', 19),
        (304, 'LiÃ¨ge', 19),
        (305, 'Namur', 19),
        (306, 'West Flanderi', 19),
        (307, 'Atacora', 20),
        (308, 'Atlantique', 20),
        (309, 'Borgou', 20),
        (310, 'OuÃ©mÃ©', 20),
        (311, 'BoulkiemdÃ©', 21),
        (312, 'Houet', 21),
        (313, 'Kadiogo', 21),
        (314, 'Barisal', 22),
        (315, 'Chittagong', 22),
        (316, 'Dhaka', 22),
        (317, 'Khulna', 22),
        (318, 'Rajshahi', 22),
        (319, 'Sylhet', 22),
        (320, 'Burgas', 23),
        (321, 'Grad Sofija', 23),
        (322, 'Haskovo', 23),
        (323, 'Lovec', 23),
        (324, 'Plovdiv', 23),
        (325, 'Ruse', 23),
        (326, 'Varna', 23),
        (327, 'al-Manama', 24),
        (328, 'New Providence', 25),
        (329, 'Federaatio', 26),
        (330, 'Republika Srpska', 26),
        (331, 'Brest', 27),
        (332, 'Gomel', 27),
        (333, 'Grodno', 27),
        (334, 'Horad Minsk', 27),
        (335, 'Minsk', 27),
        (336, 'Mogiljov', 27),
        (337, 'Vitebsk', 27),
        (338, 'Belize City', 28),
        (339, 'Cayo', 28),
        (340, 'Hamilton', 29),
        (341, 'Saint GeorgeÂ´s', 29),
        (342, 'Chuquisaca', 30),
        (343, 'Cochabamba', 30),
        (344, 'La Paz', 30),
        (345, 'Oruro', 30),
        (346, 'PotosÃ­', 30),
        (347, 'Santa Cruz', 30),
        (348, 'Tarija', 30),
        (349, 'Acre', 31),
        (350, 'Alagoas', 31),
        (351, 'AmapÃ¡', 31),
        (352, 'Amazonas', 31),
        (353, 'Bahia', 31),
        (354, 'CearÃ¡', 31),
        (355, 'Distrito Federal', 31),
        (356, 'EspÃ­rito Santo', 31),
        (357, 'GoiÃ¡s', 31),
        (358, 'MaranhÃ£o', 31),
        (359, 'Mato Grosso', 31),
        (360, 'Mato Grosso do Sul', 31),
        (361, 'Minas Gerais', 31),
        (362, 'ParaÃ­ba', 31),
        (363, 'ParanÃ¡', 31),
        (364, 'ParÃ¡', 31),
        (365, 'Pernambuco', 31),
        (366, 'PiauÃ­', 31),
        (367, 'Rio de Janeiro', 31),
        (368, 'Rio Grande do Norte', 31),
        (369, 'Rio Grande do Sul', 31),
        (370, 'RondÃ´nia', 31),
        (371, 'Roraima', 31),
        (372, 'Santa Catarina', 31),
        (373, 'SÃ£o Paulo', 31),
        (374, 'Sergipe', 31),
        (375, 'Tocantins', 31),
        (376, 'St Michael', 32),
        (377, 'Brunei and Muara', 33),
        (378, 'Thimphu', 34),
        (379, 'Francistown', 36),
        (380, 'Gaborone', 36),
        (381, 'Bangui', 37),
        (382, 'Alberta', 38),
        (383, 'British Colombia', 38),
        (384, 'Manitoba', 38),
        (385, 'Newfoundland', 38),
        (386, 'Nova Scotia', 38),
        (387, 'Ontario', 38),
        (388, 'QuÃ©bec', 38),
        (389, 'Saskatchewan', 38),
        (390, 'Home Island', 39),
        (391, 'West Island', 39),
        (392, 'Basel-Stadt', 40),
        (393, 'Bern', 40),
        (394, 'Geneve', 40),
        (395, 'Vaud', 40),
        (396, 'ZÃ¼rich', 40),
        (397, 'Antofagasta', 41),
        (398, 'Atacama', 41),
        (399, 'BÃ­obÃ­o', 41),
        (400, 'Coquimbo', 41),
        (401, 'La AraucanÃ­a', 41),
        (402, 'Los Lagos', 41),
        (403, 'Magallanes', 41),
        (404, 'Maule', 41),
        (405, 'OÂ´Higgins', 41),
        (406, 'Santiago', 41),
        (407, 'TarapacÃ¡', 41),
        (408, 'ValparaÃ­so', 41),
        (409, 'Anhui', 42),
        (410, 'Chongqing', 42),
        (411, 'Fujian', 42),
        (412, 'Gansu', 42),
        (413, 'Guangdong', 42),
        (414, 'Guangxi', 42),
        (415, 'Guizhou', 42),
        (416, 'Hainan', 42),
        (417, 'Hebei', 42),
        (418, 'Heilongjiang', 42),
        (419, 'Henan', 42),
        (420, 'Hubei', 42),
        (421, 'Hunan', 42),
        (422, 'Inner Mongolia', 42),
        (423, 'Jiangsu', 42),
        (424, 'Jiangxi', 42),
        (425, 'Jilin', 42),
        (426, 'Liaoning', 42),
        (427, 'Ningxia', 42),
        (428, 'Peking', 42),
        (429, 'Qinghai', 42),
        (430, 'Shaanxi', 42),
        (431, 'Shandong', 42),
        (432, 'Shanghai', 42),
        (433, 'Shanxi', 42),
        (434, 'Sichuan', 42),
        (435, 'Tianjin', 42),
        (436, 'Tibet', 42),
        (437, 'Xinxiang', 42),
        (438, 'Yunnan', 42),
        (439, 'Zhejiang', 42),
        (440, 'Abidjan', 43),
        (441, 'BouakÃ©', 43),
        (442, 'Daloa', 43),
        (443, 'Korhogo', 43),
        (444, 'Yamoussoukro', 43),
        (445, 'Centre', 44),
        (446, 'ExtrÃªme-Nord', 44),
        (447, 'Littoral', 44),
        (448, 'Nord', 44),
        (449, 'Nord-Ouest', 44),
        (450, 'Ouest', 44),
        (451, 'Bandundu', 0),
        (452, 'Bas-ZaÃ¯re', 0),
        (453, 'East Kasai', 0),
        (454, 'Equateur', 0),
        (455, 'Haute-ZaÃ¯re', 0),
        (456, 'Kinshasa', 0),
        (457, 'North Kivu', 0),
        (458, 'Shaba', 0),
        (459, 'South Kivu', 0),
        (460, 'West Kasai', 0),
        (461, 'Brazzaville', 46),
        (462, 'Kouilou', 46),
        (463, 'Rarotonga', 47),
        (464, 'Antioquia', 48),
        (465, 'AtlÃ¡ntico', 48),
        (466, 'BolÃ­var', 48),
        (467, 'BoyacÃ¡', 48),
        (468, 'Caldas', 48),
        (469, 'CaquetÃ¡', 48),
        (470, 'Cauca', 48),
        (471, 'CÃ³rdoba', 48),
        (472, 'Cesar', 48),
        (473, 'Cundinamarca', 48),
        (474, 'Huila', 48),
        (475, 'La Guajira', 48),
        (476, 'Magdalena', 48),
        (477, 'Meta', 48),
        (478, 'NariÃ±o', 48),
        (479, 'Norte de Santander', 48),
        (480, 'QuindÃ­o', 48),
        (481, 'Risaralda', 48),
        (482, 'SantafÃ© de BogotÃ¡', 48),
        (483, 'Santander', 48),
        (484, 'Sucre', 48),
        (485, 'Tolima', 48),
        (486, 'Valle', 48),
        (487, 'Njazidja', 49),
        (488, 'SÃ£o Tiago', 50),
        (489, 'San JosÃ©', 51),
        (490, 'CamagÃ¼ey', 52),
        (491, 'Ciego de Ãvila', 52),
        (492, 'Cienfuegos', 52),
        (493, 'Granma', 52),
        (494, 'GuantÃ¡namo', 52),
        (495, 'HolguÃ­n', 52),
        (496, 'La Habana', 52),
        (497, 'Las Tunas', 52),
        (498, 'Matanzas', 52),
        (499, 'Pinar del RÃ­o', 52),
        (500, 'Sancti-SpÃ­ritus', 52),
        (501, 'Santiago de Cuba', 52),
        (502, 'Villa Clara', 52),
        (503, 'â€“', 53),
        (504, 'Grand Cayman', 54),
        (505, 'Limassol', 55),
        (506, 'Nicosia', 55),
        (507, 'HlavnÃ­ mesto Praha', 56),
        (508, 'JiznÃ­ Cechy', 56),
        (509, 'JiznÃ­ Morava', 56),
        (510, 'SevernÃ­ Cechy', 56),
        (511, 'SevernÃ­ Morava', 56),
        (512, 'VÃ½chodnÃ­ Cechy', 56),
        (513, 'ZapadnÃ­ Cechy', 56),
        (514, 'Anhalt Sachsen', 57),
        (515, 'Baden-WÃ¼rttemberg', 57),
        (516, 'Baijeri', 57),
        (517, 'Berliini', 57),
        (518, 'Brandenburg', 57),
        (519, 'Bremen', 57),
        (520, 'Hamburg', 57),
        (521, 'Hessen', 57),
        (522, 'Mecklenburg-Vorpomme', 57),
        (523, 'Niedersachsen', 57),
        (524, 'Nordrhein-Westfalen', 57),
        (525, 'Rheinland-Pfalz', 57),
        (526, 'Saarland', 57),
        (527, 'Saksi', 57),
        (528, 'Schleswig-Holstein', 57),
        (529, 'ThÃ¼ringen', 57),
        (530, 'Djibouti', 58),
        (531, 'St George', 59),
        (532, 'Ã…rhus', 60),
        (533, 'Frederiksberg', 60),
        (534, 'Fyn', 60),
        (535, 'KÃ¸benhavn', 60),
        (536, 'Nordjylland', 60),
        (537, 'Distrito Nacional', 61),
        (538, 'Duarte', 61),
        (539, 'La Romana', 61),
        (540, 'Puerto Plata', 61),
        (541, 'San Pedro de MacorÃ­', 61),
        (542, 'Santiago', 61),
        (543, 'Alger', 62),
        (544, 'Annaba', 62),
        (545, 'Batna', 62),
        (546, 'BÃ©char', 62),
        (547, 'BÃ©jaÃ¯a', 62),
        (548, 'Biskra', 62),
        (549, 'Blida', 62),
        (550, 'Chlef', 62),
        (551, 'Constantine', 62),
        (552, 'GhardaÃ¯a', 62),
        (553, 'Mostaganem', 62),
        (554, 'Oran', 62),
        (555, 'SÃ©tif', 62),
        (556, 'Sidi Bel AbbÃ¨s', 62),
        (557, 'Skikda', 62),
        (558, 'TÃ©bessa', 62),
        (559, 'Tiaret', 62),
        (560, 'Tlemcen', 62),
        (561, 'Azuay', 63),
        (562, 'Chimborazo', 63),
        (563, 'El Oro', 63),
        (564, 'Esmeraldas', 63),
        (565, 'Guayas', 63),
        (566, 'Imbabura', 63),
        (567, 'Loja', 63),
        (568, 'Los RÃ­os', 63),
        (569, 'ManabÃ­', 63),
        (570, 'Pichincha', 63),
        (571, 'Tungurahua', 63),
        (572, 'al-Buhayra', 64),
        (573, 'al-Daqahliya', 64),
        (574, 'al-Faiyum', 64),
        (575, 'al-Gharbiya', 64),
        (576, 'al-Minufiya', 64),
        (577, 'al-Minya', 64),
        (578, 'al-Qalyubiya', 64),
        (579, 'al-Sharqiya', 64),
        (580, 'Aleksandria', 64),
        (581, 'Assuan', 64),
        (582, 'Asyut', 64),
        (583, 'Bani Suwayf', 64),
        (584, 'Giza', 64),
        (585, 'Ismailia', 64),
        (586, 'Kafr al-Shaykh', 64),
        (587, 'Kairo', 64),
        (588, 'Luxor', 64),
        (589, 'Port Said', 64),
        (590, 'Qina', 64),
        (591, 'Sawhaj', 64),
        (592, 'Shamal Sina', 64),
        (593, 'Suez', 64),
        (594, 'Maekel', 65),
        (595, 'El-AaiÃºn', 66),
        (596, 'Andalusia', 67),
        (597, 'Aragonia', 67),
        (598, 'Asturia', 67),
        (599, 'Balears', 67),
        (600, 'Baskimaa', 67),
        (601, 'Canary Islands', 67),
        (602, 'Cantabria', 67),
        (603, 'Castilla and LeÃ³n', 67),
        (604, 'Extremadura', 67),
        (605, 'Galicia', 67),
        (606, 'Kastilia-La Mancha', 67),
        (607, 'Katalonia', 67),
        (608, 'La Rioja', 67),
        (609, 'Madrid', 67),
        (610, 'Murcia', 67),
        (611, 'Navarra', 67),
        (612, 'Valencia', 67),
        (613, 'Harjumaa', 68),
        (614, 'Tartumaa', 68),
        (615, 'Addis Abeba', 69),
        (616, 'Amhara', 69),
        (617, 'Dire Dawa', 69),
        (618, 'Oromia', 69),
        (619, 'Tigray', 69),
        (620, 'Newmaa', 70),
        (621, 'PÃ¤ijÃ¤t-HÃ¤me', 70),
        (622, 'Pirkanmaa', 70),
        (623, 'Pohjois-Pohjanmaa', 70),
        (624, 'Varsinais-Suomi', 70),
        (625, 'Central', 71),
        (626, 'East Falkland', 72),
        (627, 'Alsace', 73),
        (628, 'Aquitaine', 73),
        (629, 'Auvergne', 73),
        (630, 'ÃŽle-de-France', 73),
        (631, 'Basse-Normandie', 73),
        (632, 'Bourgogne', 73),
        (633, 'Bretagne', 73),
        (634, 'Centre', 73),
        (635, 'Champagne-Ardenne', 73),
        (636, 'Franche-ComtÃ©', 73),
        (637, 'Haute-Normandie', 73),
        (638, 'Languedoc-Roussillon', 73),
        (639, 'Limousin', 73),
        (640, 'Lorraine', 73),
        (641, 'Midi-PyrÃ©nÃ©es', 73),
        (642, 'Nord-Pas-de-Calais', 73),
        (643, 'Pays de la Loire', 73),
        (644, 'Picardie', 73),
        (645, 'Provence-Alpes-CÃ´te', 73),
        (646, 'RhÃ´ne-Alpes', 73),
        (647, 'Streymoyar', 74),
        (648, 'Chuuk', 0),
        (649, 'Pohnpei', 0),
        (650, 'Estuaire', 76),
        (651, 'â€“', 77),
        (652, 'England', 77),
        (653, 'Jersey', 77),
        (654, 'North Ireland', 77),
        (655, 'Scotland', 77),
        (656, 'Wales', 77),
        (657, 'Abhasia [Aphazeti]', 78),
        (658, 'Adzaria [AtÅ¡ara]', 78),
        (659, 'Imereti', 78),
        (660, 'Kvemo Kartli', 78),
        (661, 'Tbilisi', 78),
        (662, 'Ashanti', 79),
        (663, 'Greater Accra', 79),
        (664, 'Northern', 79),
        (665, 'Western', 79),
        (666, 'â€“', 80),
        (667, 'Conakry', 81),
        (668, 'Basse-Terre', 82),
        (669, 'Grande-Terre', 82),
        (670, 'Banjul', 83),
        (671, 'Kombo St Mary', 83),
        (672, 'Bissau', 84),
        (673, 'Bioko', 85),
        (674, 'Attika', 86),
        (675, 'Central Macedonia', 86),
        (676, 'Crete', 86),
        (677, 'Thessalia', 86),
        (678, 'West Greece', 86),
        (679, 'St George', 87),
        (680, 'Kitaa', 88),
        (681, 'Guatemala', 89),
        (682, 'Quetzaltenango', 89),
        (683, 'Cayenne', 90),
        (684, 'â€“', 91),
        (685, 'Georgetown', 92),
        (686, 'Hongkong', 93),
        (687, 'Kowloon and New Kowl', 93),
        (688, 'AtlÃ¡ntida', 95),
        (689, 'CortÃ©s', 95),
        (690, 'Distrito Central', 95),
        (691, 'Grad Zagreb', 96),
        (692, 'Osijek-Baranja', 96),
        (693, 'Primorje-Gorski Kota', 96),
        (694, 'Split-Dalmatia', 96),
        (695, 'Nord', 97),
        (696, 'Ouest', 97),
        (697, 'Baranya', 98),
        (698, 'BÃ¡cs-Kiskun', 98),
        (699, 'Borsod-AbaÃºj-ZemplÃ', 98),
        (700, 'Budapest', 98),
        (701, 'CsongrÃ¡d', 98),
        (702, 'FejÃ©r', 98),
        (703, 'GyÃ¶r-Moson-Sopron', 98),
        (704, 'HajdÃº-Bihar', 98),
        (705, 'Szabolcs-SzatmÃ¡r-Be', 98),
        (706, 'Aceh', 99),
        (707, 'Bali', 99),
        (708, 'Bengkulu', 99),
        (709, 'Central Java', 99),
        (710, 'East Java', 99),
        (711, 'Jakarta Raya', 99),
        (712, 'Jambi', 99),
        (713, 'Kalimantan Barat', 99),
        (714, 'Kalimantan Selatan', 99),
        (715, 'Kalimantan Tengah', 99),
        (716, 'Kalimantan Timur', 99),
        (717, 'Lampung', 99),
        (718, 'Molukit', 99),
        (719, 'Nusa Tenggara Barat', 99),
        (720, 'Nusa Tenggara Timur', 99),
        (721, 'Riau', 99),
        (722, 'Sulawesi Selatan', 99),
        (723, 'Sulawesi Tengah', 99),
        (724, 'Sulawesi Tenggara', 99),
        (725, 'Sulawesi Utara', 99),
        (726, 'Sumatera Barat', 99),
        (727, 'Sumatera Selatan', 99),
        (728, 'Sumatera Utara', 99),
        (729, 'West Irian', 99),
        (730, 'West Java', 99),
        (731, 'Yogyakarta', 99),
        (732, 'Andhra Pradesh', 100),
        (733, 'Assam', 100),
        (734, 'Bihar', 100),
        (735, 'Chandigarh', 100),
        (736, 'Chhatisgarh', 100),
        (737, 'Delhi', 100),
        (738, 'Gujarat', 100),
        (739, 'Haryana', 100),
        (740, 'Jammu and Kashmir', 100),
        (741, 'Jharkhand', 100),
        (742, 'Karnataka', 100),
        (743, 'Kerala', 100),
        (744, 'Madhya Pradesh', 100),
        (745, 'Maharashtra', 100),
        (746, 'Manipur', 100),
        (747, 'Meghalaya', 100),
        (748, 'Mizoram', 100),
        (749, 'Orissa', 100),
        (750, 'Pondicherry', 100),
        (751, 'Punjab', 100),
        (752, 'Rajasthan', 100),
        (753, 'Tamil Nadu', 100),
        (754, 'Tripura', 100),
        (755, 'Uttar Pradesh', 100),
        (756, 'Uttaranchal', 100),
        (757, 'West Bengal', 100),
        (758, 'Leinster', 102),
        (759, 'Munster', 102),
        (760, 'Ardebil', 103),
        (761, 'Bushehr', 103),
        (762, 'Chaharmahal va Bakht', 103),
        (763, 'East Azerbaidzan', 103),
        (764, 'Esfahan', 103),
        (765, 'Fars', 103),
        (766, 'Gilan', 103),
        (767, 'Golestan', 103),
        (768, 'Hamadan', 103),
        (769, 'Hormozgan', 103),
        (770, 'Ilam', 103),
        (771, 'Kerman', 103),
        (772, 'Kermanshah', 103),
        (773, 'Khorasan', 103),
        (774, 'Khuzestan', 103),
        (775, 'Kordestan', 103),
        (776, 'Lorestan', 103),
        (777, 'Markazi', 103),
        (778, 'Mazandaran', 103),
        (779, 'Qazvin', 103),
        (780, 'Qom', 103),
        (781, 'Semnan', 103),
        (782, 'Sistan va Baluchesta', 103),
        (783, 'Teheran', 103),
        (784, 'West Azerbaidzan', 103),
        (785, 'Yazd', 103),
        (786, 'Zanjan', 103),
        (787, 'al-Anbar', 104),
        (788, 'al-Najaf', 104),
        (789, 'al-Qadisiya', 104),
        (790, 'al-Sulaymaniya', 104),
        (791, 'al-Tamim', 104),
        (792, 'Babil', 104),
        (793, 'Baghdad', 104),
        (794, 'Basra', 104),
        (795, 'DhiQar', 104),
        (796, 'Diyala', 104),
        (797, 'Irbil', 104),
        (798, 'Karbala', 104),
        (799, 'Maysan', 104),
        (800, 'Ninawa', 104),
        (801, 'Wasit', 104),
        (802, 'HÃ¶fuÃ°borgarsvÃ¦Ã°i', 105),
        (803, 'Ha Darom', 106),
        (804, 'Ha Merkaz', 106),
        (805, 'Haifa', 106),
        (806, 'Jerusalem', 106),
        (807, 'Tel Aviv', 106),
        (808, 'Abruzzit', 107),
        (809, 'Apulia', 107),
        (810, 'Calabria', 107),
        (811, 'Campania', 107),
        (812, 'Emilia-Romagna', 107),
        (813, 'Friuli-Venezia Giuli', 107),
        (814, 'Latium', 107),
        (815, 'Liguria', 107),
        (816, 'Lombardia', 107),
        (817, 'Marche', 107),
        (818, 'Piemonte', 107),
        (819, 'Sardinia', 107),
        (820, 'Sisilia', 107),
        (821, 'Toscana', 107),
        (822, 'Trentino-Alto Adige', 107),
        (823, 'Umbria', 107),
        (824, 'Veneto', 107),
        (825, 'St. Andrew', 108),
        (826, 'St. Catherine', 108),
        (827, 'al-Zarqa', 109),
        (828, 'Amman', 109),
        (829, 'Irbid', 109),
        (830, 'Aichi', 110),
        (831, 'Akita', 110),
        (832, 'Aomori', 110),
        (833, 'Chiba', 110),
        (834, 'Ehime', 110),
        (835, 'Fukui', 110),
        (836, 'Fukuoka', 110),
        (837, 'Fukushima', 110),
        (838, 'Gifu', 110),
        (839, 'Gumma', 110),
        (840, 'Hiroshima', 110),
        (841, 'Hokkaido', 110),
        (842, 'Hyogo', 110),
        (843, 'Ibaragi', 110),
        (844, 'Ishikawa', 110),
        (845, 'Iwate', 110),
        (846, 'Kagawa', 110),
        (847, 'Kagoshima', 110),
        (848, 'Kanagawa', 110),
        (849, 'Kochi', 110),
        (850, 'Kumamoto', 110),
        (851, 'Kyoto', 110),
        (852, 'Mie', 110),
        (853, 'Miyagi', 110),
        (854, 'Miyazaki', 110),
        (855, 'Nagano', 110),
        (856, 'Nagasaki', 110),
        (857, 'Nara', 110),
        (858, 'Niigata', 110),
        (859, 'Oita', 110),
        (860, 'Okayama', 110),
        (861, 'Okinawa', 110),
        (862, 'Osaka', 110),
        (863, 'Saga', 110),
        (864, 'Saitama', 110),
        (865, 'Shiga', 110),
        (866, 'Shimane', 110),
        (867, 'Shizuoka', 110),
        (868, 'Tochigi', 110),
        (869, 'Tokushima', 110),
        (870, 'Tokyo-to', 110),
        (871, 'Tottori', 110),
        (872, 'Toyama', 110),
        (873, 'Wakayama', 110),
        (874, 'Yamagata', 110),
        (875, 'Yamaguchi', 110),
        (876, 'Yamanashi', 110),
        (877, 'Almaty', 111),
        (878, 'Almaty Qalasy', 111),
        (879, 'AqtÃ¶be', 111),
        (880, 'Astana', 111),
        (881, 'Atyrau', 111),
        (882, 'East Kazakstan', 111),
        (883, 'Mangghystau', 111),
        (884, 'North Kazakstan', 111),
        (885, 'Pavlodar', 111),
        (886, 'Qaraghandy', 111),
        (887, 'Qostanay', 111),
        (888, 'Qyzylorda', 111),
        (889, 'South Kazakstan', 111),
        (890, 'Taraz', 111),
        (891, 'West Kazakstan', 111),
        (892, 'Central', 112),
        (893, 'Coast', 112),
        (894, 'Eastern', 112),
        (895, 'Nairobi', 112),
        (896, 'Nyanza', 112),
        (897, 'Rift Valley', 112),
        (898, 'Bishkek shaary', 113),
        (899, 'Osh', 113),
        (900, 'Battambang', 114),
        (901, 'Phnom Penh', 114),
        (902, 'Siem Reap', 114),
        (903, 'South Tarawa', 115),
        (904, 'St George Basseterre', 116),
        (905, 'Cheju', 117),
        (906, 'Chollabuk', 117),
        (907, 'Chollanam', 117),
        (908, 'Chungchongbuk', 117),
        (909, 'Chungchongnam', 117),
        (910, 'Inchon', 117),
        (911, 'Kang-won', 117),
        (912, 'Kwangju', 117),
        (913, 'Kyonggi', 117),
        (914, 'Kyongsangbuk', 117),
        (915, 'Kyongsangnam', 117),
        (916, 'Pusan', 117),
        (917, 'Seoul', 117),
        (918, 'Taegu', 117),
        (919, 'Taejon', 117),
        (920, 'al-Asima', 118),
        (921, 'Hawalli', 118),
        (922, 'Savannakhet', 119),
        (923, 'Viangchan', 119),
        (924, 'al-Shamal', 120),
        (925, 'Beirut', 120),
        (926, 'Montserrado', 121),
        (927, 'al-Zawiya', 122),
        (928, 'Bengasi', 122),
        (929, 'Misrata', 122),
        (930, 'Tripoli', 122),
        (931, 'Castries', 123),
        (932, 'Schaan', 124),
        (933, 'Vaduz', 124),
        (934, 'Central', 125),
        (935, 'Northern', 125),
        (936, 'Western', 125),
        (937, 'Maseru', 126),
        (938, 'Kaunas', 127),
        (939, 'Klaipeda', 127),
        (940, 'Panevezys', 127),
        (941, 'Vilna', 127),
        (942, 'Å iauliai', 127),
        (943, 'Luxembourg', 128),
        (944, 'Daugavpils', 129),
        (945, 'Liepaja', 129),
        (946, 'Riika', 129),
        (947, 'Macau', 130),
        (948, 'Casablanca', 131),
        (949, 'Chaouia-Ouardigha', 131),
        (950, 'Doukkala-Abda', 131),
        (951, 'FÃ¨s-Boulemane', 131),
        (952, 'Gharb-Chrarda-BÃ©ni', 131),
        (953, 'Marrakech-Tensift-Al', 131),
        (954, 'MeknÃ¨s-Tafilalet', 131),
        (955, 'Oriental', 131),
        (956, 'Rabat-SalÃ©-Zammour-', 131),
        (957, 'Souss Massa-DraÃ¢', 131),
        (958, 'Tadla-Azilal', 131),
        (959, 'Tanger-TÃ©touan', 131),
        (960, 'Taza-Al Hoceima-Taou', 131),
        (961, 'â€“', 132),
        (962, 'Balti', 133),
        (963, 'Bender (TÃ®ghina)', 133),
        (964, 'Chisinau', 133),
        (965, 'Dnjestria', 133),
        (966, 'Antananarivo', 134),
        (967, 'Fianarantsoa', 134),
        (968, 'Mahajanga', 134),
        (969, 'Toamasina', 134),
        (970, 'Maale', 135),
        (971, 'Aguascalientes', 136),
        (972, 'Baja California', 136),
        (973, 'Baja California Sur', 136),
        (974, 'Campeche', 136),
        (975, 'Chiapas', 136),
        (976, 'Chihuahua', 136),
        (977, 'Coahuila de Zaragoza', 136),
        (978, 'Colima', 136),
        (979, 'Distrito Federal', 136),
        (980, 'Durango', 136),
        (981, 'Guanajuato', 136),
        (982, 'Guerrero', 136),
        (983, 'Hidalgo', 136),
        (984, 'Jalisco', 136),
        (985, 'MÃ©xico', 136),
        (986, 'MichoacÃ¡n de Ocampo', 136),
        (987, 'Morelos', 136),
        (988, 'Nayarit', 136),
        (989, 'Nuevo LeÃ³n', 136),
        (990, 'Oaxaca', 136),
        (991, 'Puebla', 136),
        (992, 'QuerÃ©taro', 136),
        (993, 'QuerÃ©taro de Arteag', 136),
        (994, 'Quintana Roo', 136),
        (995, 'San Luis PotosÃ­', 136),
        (996, 'Sinaloa', 136),
        (997, 'Sonora', 136),
        (998, 'Tabasco', 136),
        (999, 'Tamaulipas', 136),
        (1000, 'Veracruz', 136),
        (1001, 'Veracruz-Llave', 136),
        (1002, 'YucatÃ¡n', 136),
        (1003, 'Zacatecas', 136),
        (1004, 'Majuro', 137),
        (1005, 'Skopje', 138),
        (1006, 'Bamako', 139),
        (1007, 'Inner Harbour', 140),
        (1008, 'Outer Harbour', 140),
        (1009, 'Irrawaddy [Ayeyarwad', 141),
        (1010, 'Magwe [Magway]', 141),
        (1011, 'Mandalay', 141),
        (1012, 'Mon', 141),
        (1013, 'Pegu [Bago]', 141),
        (1014, 'Rakhine', 141),
        (1015, 'Rangoon [Yangon]', 141),
        (1016, 'Sagaing', 141),
        (1017, 'Shan', 141),
        (1018, 'Tenasserim [Tanintha', 141),
        (1019, 'Ulaanbaatar', 142),
        (1020, 'Saipan', 143),
        (1021, 'Gaza', 144),
        (1022, 'Inhambane', 144),
        (1023, 'Manica', 144),
        (1024, 'Maputo', 144),
        (1025, 'Nampula', 144),
        (1026, 'Sofala', 144),
        (1027, 'Tete', 144),
        (1028, 'ZambÃ©zia', 144),
        (1029, 'Dakhlet NouÃ¢dhibou', 145),
        (1030, 'Nouakchott', 145),
        (1031, 'Plymouth', 146),
        (1032, 'Fort-de-France', 147),
        (1033, 'Plaines Wilhelms', 148),
        (1034, 'Port-Louis', 148),
        (1035, 'Blantyre', 149),
        (1036, 'Lilongwe', 149),
        (1037, 'Johor', 150),
        (1038, 'Kedah', 150),
        (1039, 'Kelantan', 150),
        (1040, 'Negeri Sembilan', 150),
        (1041, 'Pahang', 150),
        (1042, 'Perak', 150),
        (1043, 'Pulau Pinang', 150),
        (1044, 'Sabah', 150),
        (1045, 'Sarawak', 150),
        (1046, 'Selangor', 150),
        (1047, 'Terengganu', 150),
        (1048, 'Wilayah Persekutuan', 150),
        (1049, 'Mamoutzou', 151),
        (1050, 'Khomas', 152),
        (1051, 'â€“', 153),
        (1052, 'Maradi', 154),
        (1053, 'Niamey', 154),
        (1054, 'Zinder', 154),
        (1055, 'â€“', 155),
        (1056, 'Anambra & Enugu & Eb', 156),
        (1057, 'Bauchi & Gombe', 156),
        (1058, 'Benue', 156),
        (1059, 'Borno & Yobe', 156),
        (1060, 'Cross River', 156),
        (1061, 'Edo & Delta', 156),
        (1062, 'Federal Capital Dist', 156),
        (1063, 'Imo & Abia', 156),
        (1064, 'Kaduna', 156),
        (1065, 'Kano & Jigawa', 156),
        (1066, 'Katsina', 156),
        (1067, 'Kwara & Kogi', 156),
        (1068, 'Lagos', 156),
        (1069, 'Niger', 156),
        (1070, 'Ogun', 156),
        (1071, 'Ondo & Ekiti', 156),
        (1072, 'Oyo & Osun', 156),
        (1073, 'Plateau & Nassarawa', 156),
        (1074, 'Rivers & Bayelsa', 156),
        (1075, 'Sokoto & Kebbi & Zam', 156),
        (1076, 'Chinandega', 157),
        (1077, 'LeÃ³n', 157),
        (1078, 'Managua', 157),
        (1079, 'Masaya', 157),
        (1080, 'â€“', 158),
        (1081, 'Drenthe', 159),
        (1082, 'Flevoland', 159),
        (1083, 'Gelderland', 159),
        (1084, 'Groningen', 159),
        (1085, 'Limburg', 159),
        (1086, 'Noord-Brabant', 159),
        (1087, 'Noord-Holland', 159),
        (1088, 'Overijssel', 159),
        (1089, 'Utrecht', 159),
        (1090, 'Zuid-Holland', 159),
        (1091, 'Akershus', 160),
        (1092, 'Hordaland', 160),
        (1093, 'Oslo', 160),
        (1094, 'Rogaland', 160),
        (1095, 'SÃ¸r-TrÃ¸ndelag', 160),
        (1096, 'Central', 161),
        (1097, 'Eastern', 161),
        (1098, 'Western', 161),
        (1099, 'â€“', 162),
        (1100, 'Auckland', 163),
        (1101, 'Canterbury', 163),
        (1102, 'Dunedin', 163),
        (1103, 'Hamilton', 163),
        (1104, 'Wellington', 163),
        (1105, 'al-Batina', 164),
        (1106, 'Masqat', 164),
        (1107, 'Zufar', 164),
        (1108, 'Baluchistan', 165),
        (1109, 'Islamabad', 165),
        (1110, 'Nothwest Border Prov', 165),
        (1111, 'Punjab', 165),
        (1112, 'Sind', 165),
        (1113, 'Sindh', 165),
        (1114, 'PanamÃ¡', 166),
        (1115, 'San Miguelito', 166),
        (1116, 'â€“', 167),
        (1117, 'Ancash', 168),
        (1118, 'Arequipa', 168),
        (1119, 'Ayacucho', 168),
        (1120, 'Cajamarca', 168),
        (1121, 'Callao', 168),
        (1122, 'Cusco', 168),
        (1123, 'Huanuco', 168),
        (1124, 'Ica', 168),
        (1125, 'JunÃ­n', 168),
        (1126, 'La Libertad', 168),
        (1127, 'Lambayeque', 168),
        (1128, 'Lima', 168),
        (1129, 'Loreto', 168),
        (1130, 'Piura', 168),
        (1131, 'Puno', 168),
        (1132, 'Tacna', 168),
        (1133, 'Ucayali', 168),
        (1134, 'ARMM', 169),
        (1135, 'Bicol', 169),
        (1136, 'Cagayan Valley', 169),
        (1137, 'CAR', 169),
        (1138, 'Caraga', 169),
        (1139, 'Central Luzon', 169),
        (1140, 'Central Mindanao', 169),
        (1141, 'Central Visayas', 169),
        (1142, 'Eastern Visayas', 169),
        (1143, 'Ilocos', 169),
        (1144, 'National Capital Reg', 169),
        (1145, 'Northern Mindanao', 169),
        (1146, 'Southern Mindanao', 169),
        (1147, 'Southern Tagalog', 169),
        (1148, 'Western Mindanao', 169),
        (1149, 'Western Visayas', 169),
        (1150, 'Koror', 170),
        (1151, 'National Capital Dis', 171),
        (1152, 'Dolnoslaskie', 172),
        (1153, 'Kujawsko-Pomorskie', 172),
        (1154, 'Lodzkie', 172),
        (1155, 'Lubelskie', 172),
        (1156, 'Lubuskie', 172),
        (1157, 'Malopolskie', 172),
        (1158, 'Mazowieckie', 172),
        (1159, 'Opolskie', 172),
        (1160, 'Podkarpackie', 172),
        (1161, 'Podlaskie', 172),
        (1162, 'Pomorskie', 172),
        (1163, 'Slaskie', 172),
        (1164, 'Swietokrzyskie', 172),
        (1165, 'Warminsko-Mazurskie', 172),
        (1166, 'Wielkopolskie', 172),
        (1167, 'Zachodnio-Pomorskie', 172),
        (1168, 'Arecibo', 173),
        (1169, 'BayamÃ³n', 173),
        (1170, 'Caguas', 173),
        (1171, 'Carolina', 173),
        (1172, 'Guaynabo', 173),
        (1173, 'MayagÃ¼ez', 173),
        (1174, 'Ponce', 173),
        (1175, 'San Juan', 173),
        (1176, 'Toa Baja', 173),
        (1177, 'Chagang', 174),
        (1178, 'Hamgyong N', 174),
        (1179, 'Hamgyong P', 174),
        (1180, 'Hwanghae N', 174),
        (1181, 'Hwanghae P', 174),
        (1182, 'Kaesong-si', 174),
        (1183, 'Kangwon', 174),
        (1184, 'Nampo-si', 174),
        (1185, 'Pyongan N', 174),
        (1186, 'Pyongan P', 174),
        (1187, 'Pyongyang-si', 174),
        (1188, 'Yanggang', 174),
        (1189, 'Braga', 175),
        (1190, 'CoÃ­mbra', 175),
        (1191, 'Lisboa', 175),
        (1192, 'Porto', 175),
        (1193, 'Alto ParanÃ¡', 176),
        (1194, 'AsunciÃ³n', 176),
        (1195, 'Central', 176),
        (1196, 'Gaza', 177),
        (1197, 'Hebron', 177),
        (1198, 'Khan Yunis', 177),
        (1199, 'Nablus', 177),
        (1200, 'North Gaza', 177),
        (1201, 'Rafah', 177),
        (1202, 'Tahiti', 178),
        (1203, 'Doha', 179),
        (1204, 'Saint-Denis', 180),
        (1205, 'Arad', 181),
        (1206, 'Arges', 181),
        (1207, 'Bacau', 181),
        (1208, 'Bihor', 181),
        (1209, 'Botosani', 181),
        (1210, 'Braila', 181),
        (1211, 'Brasov', 181),
        (1212, 'Bukarest', 181),
        (1213, 'Buzau', 181),
        (1214, 'Caras-Severin', 181),
        (1215, 'Cluj', 181),
        (1216, 'Constanta', 181),
        (1217, 'DÃ¢mbovita', 181),
        (1218, 'Dolj', 181),
        (1219, 'Galati', 181),
        (1220, 'Gorj', 181),
        (1221, 'Iasi', 181),
        (1222, 'Maramures', 181),
        (1223, 'Mehedinti', 181),
        (1224, 'Mures', 181),
        (1225, 'Neamt', 181),
        (1226, 'Prahova', 181),
        (1227, 'Satu Mare', 181),
        (1228, 'Sibiu', 181),
        (1229, 'Suceava', 181),
        (1230, 'Timis', 181),
        (1231, 'Tulcea', 181),
        (1232, 'VÃ¢lcea', 181),
        (1233, 'Vrancea', 181),
        (1234, 'Adygea', 182),
        (1235, 'Altai', 182),
        (1236, 'Amur', 182),
        (1237, 'Arkangeli', 182),
        (1238, 'Astrahan', 182),
        (1239, 'BaÅ¡kortostan', 182),
        (1240, 'Belgorod', 182),
        (1241, 'Brjansk', 182),
        (1242, 'Burjatia', 182),
        (1243, 'Dagestan', 182),
        (1244, 'Habarovsk', 182),
        (1245, 'Hakassia', 182),
        (1246, 'Hanti-Mansia', 182),
        (1247, 'Irkutsk', 182),
        (1248, 'Ivanovo', 182),
        (1249, 'Jaroslavl', 182),
        (1250, 'Kabardi-Balkaria', 182),
        (1251, 'Kaliningrad', 182),
        (1252, 'Kalmykia', 182),
        (1253, 'Kaluga', 182),
        (1254, 'KamtÅ¡atka', 182),
        (1255, 'KaratÅ¡ai-TÅ¡erkessi', 182),
        (1256, 'Karjala', 182),
        (1257, 'Kemerovo', 182),
        (1258, 'Kirov', 182),
        (1259, 'Komi', 182),
        (1260, 'Kostroma', 182),
        (1261, 'Krasnodar', 182),
        (1262, 'Krasnojarsk', 182),
        (1263, 'Kurgan', 182),
        (1264, 'Kursk', 182),
        (1265, 'Lipetsk', 182),
        (1266, 'Magadan', 182),
        (1267, 'Marinmaa', 182),
        (1268, 'Mordva', 182),
        (1269, 'Moscow (City)', 182),
        (1270, 'Moskova', 182),
        (1271, 'Murmansk', 182),
        (1272, 'Nizni Novgorod', 182),
        (1273, 'North Ossetia-Alania', 182),
        (1274, 'Novgorod', 182),
        (1275, 'Novosibirsk', 182),
        (1276, 'Omsk', 182),
        (1277, 'Orenburg', 182),
        (1278, 'Orjol', 182),
        (1279, 'Penza', 182),
        (1280, 'Perm', 182),
        (1281, 'Pietari', 182),
        (1282, 'Pihkova', 182),
        (1283, 'Primorje', 182),
        (1284, 'Rjazan', 182),
        (1285, 'Rostov-na-Donu', 182),
        (1286, 'Saha (Jakutia)', 182),
        (1287, 'Sahalin', 182),
        (1288, 'Samara', 182),
        (1289, 'Saratov', 182),
        (1290, 'Smolensk', 182),
        (1291, 'Stavropol', 182),
        (1292, 'Sverdlovsk', 182),
        (1293, 'Tambov', 182),
        (1294, 'Tatarstan', 182),
        (1295, 'Tjumen', 182),
        (1296, 'Tomsk', 182),
        (1297, 'Tula', 182),
        (1298, 'Tver', 182),
        (1299, 'Tyva', 182),
        (1300, 'TÅ¡eljabinsk', 182),
        (1301, 'TÅ¡etÅ¡enia', 182),
        (1302, 'TÅ¡ita', 182),
        (1303, 'TÅ¡uvassia', 182),
        (1304, 'Udmurtia', 182),
        (1305, 'Uljanovsk', 182),
        (1306, 'Vladimir', 182),
        (1307, 'Volgograd', 182),
        (1308, 'Vologda', 182),
        (1309, 'Voronez', 182),
        (1310, 'Yamalin Nenetsia', 182),
        (1311, 'Kigali', 183),
        (1312, 'al-Khudud al-Samaliy', 184),
        (1313, 'al-Qasim', 184),
        (1314, 'al-Sharqiya', 184),
        (1315, 'Asir', 184),
        (1316, 'Hail', 184),
        (1317, 'Medina', 184),
        (1318, 'Mekka', 184),
        (1319, 'Najran', 184),
        (1320, 'Qasim', 184),
        (1321, 'Riad', 184),
        (1322, 'Riyadh', 184),
        (1323, 'Tabuk', 184),
        (1324, 'al-Bahr al-Abyad', 185),
        (1325, 'al-Bahr al-Ahmar', 185),
        (1326, 'al-Jazira', 185),
        (1327, 'al-Qadarif', 185),
        (1328, 'Bahr al-Jabal', 185),
        (1329, 'Darfur al-Janubiya', 185),
        (1330, 'Darfur al-Shamaliya', 185),
        (1331, 'Kassala', 185),
        (1332, 'Khartum', 185),
        (1333, 'Kurdufan al-Shamaliy', 185),
        (1334, 'Cap-Vert', 186),
        (1335, 'Diourbel', 186),
        (1336, 'Kaolack', 186),
        (1337, 'Saint-Louis', 186),
        (1338, 'ThiÃ¨s', 186),
        (1339, 'Ziguinchor', 186),
        (1340, 'â€“', 187),
        (1341, 'Saint Helena', 189),
        (1342, 'LÃ¤nsimaa', 190),
        (1343, 'Honiara', 191),
        (1344, 'Western', 192),
        (1345, 'La Libertad', 193),
        (1346, 'San Miguel', 193),
        (1347, 'San Salvador', 193),
        (1348, 'Santa Ana', 193),
        (1349, 'San Marino', 194),
        (1350, 'Serravalle/Dogano', 194),
        (1351, 'Banaadir', 195),
        (1352, 'Jubbada Hoose', 195),
        (1353, 'Woqooyi Galbeed', 195),
        (1354, 'Saint-Pierre', 196),
        (1355, 'Aqua Grande', 197),
        (1356, 'Paramaribo', 198),
        (1357, 'Bratislava', 199),
        (1358, 'VÃ½chodnÃ© Slovensko', 199),
        (1359, 'Osrednjeslovenska', 200),
        (1360, 'Podravska', 200),
        (1361, 'Ã–rebros lÃ¤n', 201),
        (1362, 'East GÃ¶tanmaan lÃ¤n', 201),
        (1363, 'GÃ¤vleborgs lÃ¤n', 201),
        (1364, 'JÃ¶nkÃ¶pings lÃ¤n', 201),
        (1365, 'Lisboa', 201),
        (1366, 'SkÃ¥ne lÃ¤n', 201),
        (1367, 'Uppsala lÃ¤n', 201),
        (1368, 'VÃ¤sterbottens lÃ¤n', 201),
        (1369, 'VÃ¤sternorrlands lÃ¤', 201),
        (1370, 'VÃ¤stmanlands lÃ¤n', 201),
        (1371, 'West GÃ¶tanmaan lÃ¤n', 201),
        (1372, 'Hhohho', 202),
        (1373, 'MahÃ©', 203),
        (1374, 'al-Hasaka', 204),
        (1375, 'al-Raqqa', 204),
        (1376, 'Aleppo', 204),
        (1377, 'Damascus', 204),
        (1378, 'Damaskos', 204),
        (1379, 'Dayr al-Zawr', 204),
        (1380, 'Hama', 204),
        (1381, 'Hims', 204),
        (1382, 'Idlib', 204),
        (1383, 'Latakia', 204),
        (1384, 'Grand Turk', 205),
        (1385, 'Chari-Baguirmi', 206),
        (1386, 'Logone Occidental', 206),
        (1387, 'Maritime', 207),
        (1388, 'Bangkok', 208),
        (1389, 'Chiang Mai', 208),
        (1390, 'Khon Kaen', 208),
        (1391, 'Nakhon Pathom', 208),
        (1392, 'Nakhon Ratchasima', 208),
        (1393, 'Nakhon Sawan', 208),
        (1394, 'Nonthaburi', 208),
        (1395, 'Songkhla', 208),
        (1396, 'Ubon Ratchathani', 208),
        (1397, 'Udon Thani', 208),
        (1398, 'Karotegin', 209),
        (1399, 'Khujand', 209),
        (1400, 'Fakaofo', 210),
        (1401, 'Ahal', 211),
        (1402, 'Dashhowuz', 211),
        (1403, 'Lebap', 211),
        (1404, 'Mary', 211),
        (1405, 'Dili', 212),
        (1406, 'Tongatapu', 213),
        (1407, 'Caroni', 214),
        (1408, 'Port-of-Spain', 214),
        (1409, 'Ariana', 215),
        (1410, 'Biserta', 215),
        (1411, 'GabÃ¨s', 215),
        (1412, 'Kairouan', 215),
        (1413, 'Sfax', 215),
        (1414, 'Sousse', 215),
        (1415, 'Tunis', 215),
        (1416, 'Adana', 216),
        (1417, 'Adiyaman', 216),
        (1418, 'Afyon', 216),
        (1419, 'Aksaray', 216),
        (1420, 'Ankara', 216),
        (1421, 'Antalya', 216),
        (1422, 'Aydin', 216),
        (1423, 'Ã‡orum', 216),
        (1424, 'Balikesir', 216),
        (1425, 'Batman', 216),
        (1426, 'Bursa', 216),
        (1427, 'Denizli', 216),
        (1428, 'Diyarbakir', 216),
        (1429, 'Edirne', 216),
        (1430, 'ElÃ¢zig', 216),
        (1431, 'Erzincan', 216),
        (1432, 'Erzurum', 216),
        (1433, 'Eskisehir', 216),
        (1434, 'Gaziantep', 216),
        (1435, 'Hatay', 216),
        (1436, 'IÃ§el', 216),
        (1437, 'Isparta', 216),
        (1438, 'Istanbul', 216),
        (1439, 'Izmir', 216),
        (1440, 'Kahramanmaras', 216),
        (1441, 'KarabÃ¼k', 216),
        (1442, 'Karaman', 216),
        (1443, 'Kars', 216),
        (1444, 'Kayseri', 216),
        (1445, 'KÃ¼tahya', 216),
        (1446, 'Kilis', 216),
        (1447, 'Kirikkale', 216),
        (1448, 'Kocaeli', 216),
        (1449, 'Konya', 216),
        (1450, 'Malatya', 216),
        (1451, 'Manisa', 216),
        (1452, 'Mardin', 216),
        (1453, 'Ordu', 216),
        (1454, 'Osmaniye', 216),
        (1455, 'Sakarya', 216),
        (1456, 'Samsun', 216),
        (1457, 'Sanliurfa', 216),
        (1458, 'Siirt', 216),
        (1459, 'Sivas', 216),
        (1460, 'Tekirdag', 216),
        (1461, 'Tokat', 216),
        (1462, 'Trabzon', 216),
        (1463, 'Usak', 216),
        (1464, 'Van', 216),
        (1465, 'Zonguldak', 216),
        (1466, 'Funafuti', 217),
        (1468, 'Changhwa', 218),
        (1469, 'Chiayi', 218),
        (1470, 'Hsinchu', 218),
        (1471, 'Hualien', 218),
        (1472, 'Ilan', 218),
        (1473, 'Kaohsiung', 218),
        (1474, 'Keelung', 218),
        (1475, 'Miaoli', 218),
        (1476, 'Nantou', 218),
        (1477, 'Pingtung', 218),
        (1478, 'Taichung', 218),
        (1479, 'Tainan', 218),
        (1480, 'Taipei', 218),
        (1481, 'Taitung', 218),
        (1482, 'Taoyuan', 218),
        (1483, 'YÃ¼nlin', 218),
        (1484, 'Arusha', 219),
        (1485, 'Dar es Salaam', 219),
        (1486, 'Dodoma', 219),
        (1487, 'Kilimanjaro', 219),
        (1488, 'Mbeya', 219),
        (1489, 'Morogoro', 219),
        (1490, 'Mwanza', 219),
        (1491, 'Tabora', 219),
        (1492, 'Tanga', 219),
        (1493, 'Zanzibar West', 219),
        (1494, 'Central', 220),
        (1495, 'Dnipropetrovsk', 221),
        (1496, 'Donetsk', 221),
        (1497, 'Harkova', 221),
        (1498, 'Herson', 221),
        (1499, 'Hmelnytskyi', 221),
        (1500, 'Ivano-Frankivsk', 221),
        (1501, 'Kiova', 221),
        (1502, 'Kirovograd', 221),
        (1503, 'Krim', 221),
        (1504, 'Lugansk', 221),
        (1505, 'Lviv', 221),
        (1506, 'Mykolajiv', 221),
        (1507, 'Odesa', 221),
        (1508, 'Pultava', 221),
        (1509, 'Rivne', 221),
        (1510, 'Sumy', 221),
        (1511, 'Taka-Karpatia', 221),
        (1512, 'Ternopil', 221),
        (1513, 'TÅ¡erkasy', 221),
        (1514, 'TÅ¡ernigiv', 221),
        (1515, 'TÅ¡ernivtsi', 221),
        (1516, 'Vinnytsja', 221),
        (1517, 'Volynia', 221),
        (1518, 'Zaporizzja', 221),
        (1519, 'Zytomyr', 221),
        (1520, 'Montevideo', 223),
        (1521, 'Alabama', 224),
        (1522, 'Alaska', 224),
        (1523, 'Arizona', 224),
        (1524, 'Arkansas', 224),
        (1525, 'California', 224),
        (1526, 'Colorado', 224),
        (1527, 'Connecticut', 224),
        (1528, 'District of Columbia', 224),
        (1529, 'Florida', 224),
        (1530, 'Georgia', 224),
        (1531, 'Hawaii', 224),
        (1532, 'Idaho', 224),
        (1533, 'Illinois', 224),
        (1534, 'Indiana', 224),
        (1535, 'Iowa', 224),
        (1536, 'Kansas', 224),
        (1537, 'Kentucky', 224),
        (1538, 'Louisiana', 224),
        (1539, 'Maryland', 224),
        (1540, 'Massachusetts', 224),
        (1541, 'Michigan', 224),
        (1542, 'Minnesota', 224),
        (1543, 'Mississippi', 224),
        (1544, 'Missouri', 224),
        (1545, 'Montana', 224),
        (1546, 'Nebraska', 224),
        (1547, 'Nevada', 224),
        (1548, 'New Hampshire', 224),
        (1549, 'New Jersey', 224),
        (1550, 'New Mexico', 224),
        (1551, 'New York', 224),
        (1552, 'North Carolina', 224),
        (1553, 'Ohio', 224),
        (1554, 'Oklahoma', 224),
        (1555, 'Oregon', 224),
        (1556, 'Pennsylvania', 224),
        (1557, 'Rhode Island', 224),
        (1558, 'South Carolina', 224),
        (1559, 'South Dakota', 224),
        (1560, 'Tennessee', 224),
        (1561, 'Texas', 224),
        (1562, 'Utah', 224),
        (1563, 'Virginia', 224),
        (1564, 'Washington', 224),
        (1565, 'Wisconsin', 224),
        (1566, 'Andijon', 225),
        (1567, 'Buhoro', 225),
        (1568, 'Cizah', 225),
        (1569, 'Fargona', 225),
        (1570, 'Karakalpakistan', 225),
        (1571, 'Khorazm', 225),
        (1572, 'Namangan', 225),
        (1573, 'Navoi', 225),
        (1574, 'Qashqadaryo', 225),
        (1575, 'Samarkand', 225),
        (1576, 'Surkhondaryo', 225),
        (1577, 'Toskent', 225),
        (1578, 'Toskent Shahri', 225),
        (1579, 'â€“', 226),
        (1580, 'St George', 0),
        (1582, 'AnzoÃ¡tegui', 228),
        (1583, 'Apure', 228),
        (1584, 'Aragua', 228),
        (1585, 'Barinas', 228),
        (1586, 'BolÃ­var', 228),
        (1587, 'Carabobo', 228),
        (1588, 'Distrito Federal', 228),
        (1589, 'FalcÃ³n', 228),
        (1590, 'GuÃ¡rico', 228),
        (1591, 'Lara', 228),
        (1592, 'MÃ©rida', 228),
        (1593, 'Miranda', 228),
        (1594, 'Monagas', 228),
        (1595, 'Portuguesa', 228),
        (1596, 'Sucre', 228),
        (1597, 'TÃ¡chira', 228),
        (1598, 'Trujillo', 228),
        (1599, 'Yaracuy', 228),
        (1600, 'Zulia', 228),
        (1601, 'Tortola', 229),
        (1602, 'St Thomas', 230),
        (1603, 'An Giang', 231),
        (1604, 'Ba Ria-Vung Tau', 231),
        (1605, 'Bac Thai', 231),
        (1606, 'Binh Dinh', 231),
        (1607, 'Binh Thuan', 231),
        (1608, 'Can Tho', 231),
        (1609, 'Dac Lac', 231),
        (1610, 'Dong Nai', 231),
        (1611, 'Haiphong', 231),
        (1612, 'Hanoi', 231),
        (1613, 'Ho Chi Minh City', 231),
        (1614, 'Khanh Hoa', 231),
        (1615, 'Kien Giang', 231),
        (1616, 'Lam Dong', 231),
        (1617, 'Nam Ha', 231),
        (1618, 'Nghe An', 231),
        (1619, 'Quang Binh', 231),
        (1620, 'Quang Nam-Da Nang', 231),
        (1621, 'Quang Ninh', 231),
        (1622, 'Thua Thien-Hue', 231),
        (1623, 'Tien Giang', 231),
        (1624, 'Shefa', 232),
        (1625, 'Wallis', 233),
        (1626, 'Upolu', 234),
        (1627, 'Aden', 235),
        (1628, 'Hadramawt', 235),
        (1629, 'Hodeida', 235),
        (1630, 'Ibb', 235),
        (1631, 'Sanaa', 235),
        (1632, 'Taizz', 235),
        (1633, 'Central Serbia', 236),
        (1634, 'Kosovo and Metohija', 236),
        (1635, 'Montenegro', 236),
        (1636, 'Vojvodina', 236),
        (1637, 'Eastern Cape', 237),
        (1638, 'Free State', 237),
        (1639, 'Gauteng', 237),
        (1640, 'KwaZulu-Natal', 237),
        (1641, 'Mpumalanga', 237),
        (1642, 'North West', 237),
        (1643, 'Northern Cape', 237),
        (1644, 'Western Cape', 237),
        (1645, 'Central', 238),
        (1646, 'Copperbelt', 238),
        (1647, 'Lusaka', 238),
        (1648, 'Bulawayo', 239),
        (1649, 'Harare', 239),
        (1650, 'Manicaland', 239),
        (1651, 'Midlands', 239)");

        // Table structure for table   houdinv_stock_transfer_log   

                $getDB->query("CREATE TABLE houdinv_stock_transfer_log (
            houdinv_stock_transfer_log_id int(11) NOT NULL,
            houdinv_stock_transfer_log_warehouse_from int(11) NOT NULL,
            houdinv_stock_transfer_log_warehouse_to int(11) NOT NULL,
            houdinv_stock_transfer_log_productdetails text NOT NULL,
            houdinv_stock_transfer_log_created_at varchar(30) NOT NULL
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1"); 

        // Table structure for table   houdinv_storediscount   

                $getDB->query("CREATE TABLE houdinv_storediscount (
            discount_id int(11) NOT NULL,
            product_cat_id varchar(200) NOT NULL,
            product_id varchar(250) NOT NULL,
            customer_id varchar(250) NOT NULL,
            discount_name varchar(200) NOT NULL,
            discount_type int(11) NOT NULL,
            discount_amount varchar(250) NOT NULL,
            discount_start varchar(250) NOT NULL,
            discount_end varchar(250) NOT NULL,
            discount_status int(11) NOT NULL DEFAULT '0',
            createdate varchar(250) NOT NULL,
            updatedate varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table   houdinv_storepolicies   

                $getDB->query("CREATE TABLE houdinv_storepolicies (
            id int(250) NOT NULL,
            about longblob NOT NULL,
            faq longblob NOT NULL,
            Privacy_Policy longblob NOT NULL,
            Terms_Conditions longblob NOT NULL,
            Cancellation_Refund_Policy longblob NOT NULL,
            Shipping_Delivery_Policy longblob NOT NULL,
            Disclaimer_Policy longblob NOT NULL,
            date_time varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table   houdinv_storesetting   

                $getDB->query("CREATE TABLE houdinv_storesetting (
            id int(220) NOT NULL,
            display_out_stock int(250) DEFAULT NULL,
            receieve_order_out int(250) DEFAULT NULL,
            custom_html_footer int(250) DEFAULT NULL,
            add_to_cart_button int(11) DEFAULT NULL,
            email_mandatory int(11) DEFAULT NULL,
            verify_mobile_numer int(11) DEFAULT NULL,
            otp_ int(11) DEFAULT NULL,
            otp_setting varchar(200) DEFAULT NULL,
            date_time varchar(200) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table   houdinv_sub_categories_one   

                $getDB->query("CREATE TABLE houdinv_sub_categories_one (
            houdinv_sub_category_one_id int(11) NOT NULL,
            houdinv_sub_category_one_name varchar(100) NOT NULL,
            houdinv_sub_category_one_main_id int(11) NOT NULL,
            houdinv_sub_category_one_created_date varchar(100) NOT NULL,
            houdinv_sub_category_one_updated_date varchar(100) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table   houdinv_sub_categories_two   

                $getDB->query("CREATE TABLE houdinv_sub_categories_two (
            houdinv_sub_category_two_id int(11) NOT NULL,
            houdinv_sub_category_two_name varchar(100) NOT NULL,
            houdinv_sub_category_two_sub1_id int(11) NOT NULL,
            houdinv_sub_category_two_main_id int(11) NOT NULL,
            houdinv_sub_category_two_created_date varchar(100) NOT NULL,
            houdinv_sub_category_two_updated_date varchar(100) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table   houdinv_suppliers   

                $getDB->query("CREATE TABLE houdinv_suppliers (
            houdinv_supplier_id int(10) UNSIGNED NOT NULL,
            houdinv_supplier_comapany_name varchar(150) DEFAULT NULL,
            houdinv_supplier_contact_person_name varchar(150) NOT NULL,
            houdinv_supplier_email varchar(150) NOT NULL,
            houdinv_supplier_contact varchar(20) NOT NULL,
            houdinv_supplier_landline varchar(20) DEFAULT NULL,
            houdinv_supplier_address varchar(500) NOT NULL,
            houdinv_supplier_active_status enum('active','deactive') NOT NULL,
            houdinv_supplier_created_at varchar(30) NOT NULL,
            houdinv_supplier_updated_at varchar(30) NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1");
 
        // Table structure for table   houdinv_supplier_products   

                $getDB->query("CREATE TABLE houdinv_supplier_products (
            houdinv_supplier_products_id int(11) NOT NULL,
            houdinv_supplier_products_supplier_id int(11) NOT NULL,
            houdinv_supplier_products_product_id int(11) NOT NULL,
            houdinv_supplier_products_created_at varchar(30) NOT NULL,
            houdinv_supplier_products_updated_at varchar(30) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
        // Table structure for table   houdinv_taxes   

                $getDB->query("CREATE TABLE `houdinv_taxes` (
                    `houdinv_tax_id` int(11) NOT NULL,
                    `houdinv_tax_name` varchar(150) NOT NULL,
                    `houdinv_tax_percenatge` float NOT NULL,
                    `houdinv_tax_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
                  )ENGINE=InnoDB DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_taxsetting   

                $getDB->query("CREATE TABLE houdinv_taxsetting (
            id int(11) NOT NULL,
            taxsetting_number varchar(250) NOT NULL,
            date_time varchar(250) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table   houdinv_templates   

                $getDB->query("CREATE TABLE houdinv_templates (
            houdinv_template_id int(11) NOT NULL,
            houdinv_template_header_image text,
            houdinv_template_header_text tinytext,
            houdinv_template_header_sub_text text,
            houdinv_template_footer tinytext,
            houdinv_template_copyright varchar(200) DEFAULT NULL,
            houdinv_template_active_status tinyint(4) DEFAULT '0',
            houdinv_template_meta text,
            houdinv_template_file varchar(300) DEFAULT NULL COMMENT 'Location of template folder',
            houdinv_template_created_at timestamp NULL DEFAULT NULL,
            houdinv_template_updated_at timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_template_pages   

                $getDB->query("CREATE TABLE houdinv_template_pages (
            houdinv_template_page_id int(11) NOT NULL,
            houdinv_template_page_name varchar(200) NOT NULL,
            houdinv_template_page_slug varchar(250) NOT NULL,
            houdinv_template_page_body text NOT NULL,
            houdinv_template_page_meta text,
            houdinv_template_page_created_at timestamp NULL DEFAULT NULL,
            houdinv_template_page_updated_at timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

        // Table structure for table   houdinv_testimonials   

                $getDB->query("CREATE TABLE houdinv_testimonials (
            id int(250) NOT NULL,
            testimonials_name varchar(250) DEFAULT NULL,
            testimonials_image varchar(250) DEFAULT NULL,
            testimonials_feedback longblob,
            testimonials_publish int(10) DEFAULT '0',
            date_time varchar(250) DEFAULT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table   houdinv_transaction   

                $getDB->query("CREATE TABLE houdinv_transaction (
            houdinv_transaction_id int(11) NOT NULL,
            houdinv_transaction_transaction_id varchar(155) DEFAULT NULL,
            houdinv_transaction_type enum('credit','debit') NOT NULL,
            houdinv_transaction_method enum('online','cash') NOT NULL,
            houdinv_transaction_from enum('payumoney','cash','pos','authorize','checque','card') NOT NULL,
            houdinv_transaction_for enum('order','sms package','email package','inventory purchase','Work Order','order refund','expense') NOT NULL,
            houdinv_transaction_for_id int(11) DEFAULT '0',
            houdinv_transaction_amount int(11) DEFAULT '0',
            houdinv_transaction_date date DEFAULT NULL,
            houdinv_transaction_status enum('success','fail') NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
        // Table structure for table   houdinv_users   

                $getDB->query("CREATE TABLE houdinv_users (
            houdinv_user_id int(10) UNSIGNED NOT NULL,
            houdinv_user_name varchar(150) NOT NULL,
            houdinv_user_email varchar(150) NOT NULL,
            houdinv_user_password varchar(150) NOT NULL,
            houdin_user_password_salt varchar(100) NOT NULL,
            houdinv_user_contact varchar(150) NOT NULL,
            houdinv_users_profile varchar(100) NOT NULL,
            houdinv_users_gender enum('no','male','female') NOT NULL,
            houdinv_users_dob date DEFAULT NULL,
            houdinv_users_device_id text,
            houdinv_users_pending_amount int(11) NOT NULL DEFAULT '0',
            houdinv_users_privilage float NOT NULL DEFAULT '0',
            houdinv_user_is_verified tinyint(4) NOT NULL DEFAULT '0',
            houdinv_user_is_active enum('deactive','active') NOT NULL,
            houdinv_user_created_at varchar(30) DEFAULT NULL,
            houdinv_user_updated_at varchar(30) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1");
        // Table structure for table   houdinv_users_cart   

                $getDB->query("CREATE TABLE houdinv_users_cart (
            houdinv_users_cart_id int(11) NOT NULL,
            houdinv_users_cart_user_id int(11) NOT NULL,
            houdinv_users_cart_item_id int(11) DEFAULT '0',
            houdinv_users_cart_item_variant_id int(11) DEFAULT '0',
            houdinv_users_cart_item_count int(11) NOT NULL,
            houdinv_users_cart_add_date varchar(100) NOT NULL,
            houdinv_users_cart_update_date varchar(100) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table   houdinv_users_forgot   

                $getDB->query("CREATE TABLE houdinv_users_forgot (
            houdinv_users_forgot_id int(11) NOT NULL,
            houdinv_users_forgot_user_id int(11) NOT NULL,
            houdinv_users_forgot_token int(4) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table houdinv_users_group  

                $getDB->query("CREATE TABLE houdinv_users_group (
            houdinv_users_group_id int(11) NOT NULL,
            houdinv_users_group_name varchar(50) NOT NULL,
            houdinv_users_group_status enum('active','deactive') NOT NULL,
            houdinv_users_group_created_at varchar(30) NOT NULL,
            houdinv_users_group_modified_at varchar(30) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
        // Table structure for table houdinv_users_group_users_list  

                $getDB->query("CREATE TABLE houdinv_users_group_users_list (
            houdinv_users_group_users_list_id int(11) NOT NULL,
            houdinv_users_group__user_group_id int(11) NOT NULL,
            houdinv_users_group_users_id int(11) NOT NULL,
            houdinv_users_group_users_list_created_at varchar(30) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 

       // Table structure for table houdinv_users_whishlist  

                $getDB->query("CREATE TABLE houdinv_users_whishlist (
        houdinv_users_whishlist_id int(11) NOT NULL,
        houdinv_users_whishlist_user_id int(11) NOT NULL,
        houdinv_users_whishlist_item_id int(11) DEFAULT '0',
        houdinv_users_whishlist_item_variant_id int(11) DEFAULT '0',
        houdinv_users_whishlist_final_price varchar(100) NOT NULL,
        houdinv_users_whishlist_add_date varchar(100) NOT NULL,
        houdinv_users_whishlist_update_date varchar(100) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 

    // Table structure for table houdinv_user_address  

                $getDB->query("CREATE TABLE houdinv_user_address (
        houdinv_user_address_id int(11) NOT NULL,
        houdinv_user_address_user_id int(11) NOT NULL,
        houdinv_user_address_name varchar(30) NOT NULL,
        houdinv_user_address_phone varchar(20) NOT NULL,
        houdinv_user_address_user_address varchar(100) NOT NULL,
        houdinv_user_address_city varchar(30) NOT NULL,
        houdinv_user_address_zip varchar(10) NOT NULL,
        houdinv_user_address_country int(11) NOT NULL,
        houdinv_user_address_created_at varchar(30) DEFAULT NULL,
        houdinv_user_address_modified_at varchar(30) DEFAULT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
       
    // Table structure for table houdinv_user_addresses  

                $getDB->query("CREATE TABLE houdinv_user_addresses (
        houdinv_user_address_id int(10) UNSIGNED NOT NULL,
        houdinv_user_address_user_id int(10) UNSIGNED NOT NULL,
        houdinv_user_address_contact_no varchar(15) DEFAULT NULL,
        houdinv_user_address_street_line1 varchar(200) NOT NULL,
        houdinv_user_address_landmark varchar(200) DEFAULT NULL,
        houdinv_user_address_pincode decimal(10,0) NOT NULL,
        houdinv_user_address_city varchar(150) NOT NULL,
        houdinv_user_address_state varchar(150) NOT NULL,
        houdinv_user_address_country varchar(150) NOT NULL,
        houdinv_user_address_created_at varchar(30) DEFAULT NULL,
        houdinv_user_address_updated_at varchar(30) DEFAULT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1");
 

    // Table structure for table houdinv_user_bank_details  

                $getDB->query("CREATE TABLE houdinv_user_bank_details (
        houdinv_user_bank_detail_id int(10) UNSIGNED NOT NULL,
        houdinv_user_bank_detail_user_id int(10) UNSIGNED NOT NULL,
        houdinv_user_bank_detail_holder_name varchar(150) NOT NULL,
        houdinv_user_bank_detail_holder_number varchar(150) NOT NULL,
        houdinv_user_bank_detail_bank_name varchar(200) NOT NULL,
        houdinv_user_bank_detail_swift_no varchar(150) DEFAULT NULL,
        houdinv_user_bank_detail_iban_no varchar(150) DEFAULT NULL,
        houdinv_user_bank_detail_country varchar(150) NOT NULL,
        houdinv_user_bank_detail_created_at timestamp NULL DEFAULT NULL,
        houdinv_user_bank_detail_updated_at timestamp NULL DEFAULT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Bank details of user for refund'");

    // Table structure for table houdinv_user_payments

                $getDB->query("CREATE TABLE houdinv_user_payments (
        houdinv_user_payment_id int(10) UNSIGNED NOT NULL,
        houdinv_user_payment_user_id int(10) UNSIGNED NOT NULL,
        houdinv_user_payment_product_id int(10) UNSIGNED NOT NULL,
        houdinv_user_payment_coupon_id int(10) UNSIGNED NOT NULL,
        houdinv_user_payment_cost float NOT NULL,
        houdinv_user_payment_currency varchar(50) NOT NULL,
        houdinv_user_payment_transcaction_id varchar(150) NOT NULL,
        houdinv_user_payment_status tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 For Unsuccess, 1 For Success, 2 For cancelled, 2 For refund',
        houdinv_user_payment_mode enum('COD','Online','Store') DEFAULT NULL,
        houdinv_user_payment_time datetime NOT NULL,
        houdinv_user_payment_created_at timestamp NULL DEFAULT NULL,
        houdinv_user_payment_updated_at timestamp NULL DEFAULT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

    // Table structure for table houdinv_user_tag

                $getDB->query("CREATE TABLE houdinv_user_tag (
        houdinv_user_tag_id int(11) NOT NULL,
        houdinv_user_tag_user_id int(11) NOT NULL,
        houdinv_user_tag_name varchar(100) NOT NULL,
        houdinv_user_tag_created_date varchar(30) NOT NULL,
        houdinv_user_tag_modified_at varchar(30) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 

    // Table structure for table houdinv_user_use_Coupon

                $getDB->query("CREATE TABLE houdinv_user_use_Coupon (
        houdinv_user_use_Coupon_id int(11) NOT NULL,
        houdinv_user_use_Coupon_user_id int(11) NOT NULL,
        houdinv_user_use_Coupon_used_Count int(11) NOT NULL,
        houdinv_user_use_Coupon_coupon_id int(11) NOT NULL,
        houdinv_user_use_Coupon_discount int(11) NOT NULL,
        houdinv_user_use_Coupon_add_date varchar(100) DEFAULT NULL,
        houdinv_user_use_Coupon_update_date varchar(100) DEFAULT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 

    // Table structure for table houdinv_user_verifications

                $getDB->query("CREATE TABLE houdinv_user_verifications (
        houdinv_user_verification_email varchar(150) NOT NULL,
        houdinv_user_verification_token varchar(150) NOT NULL,
        houdinv_user_verification_created_at timestamp NULL DEFAULT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

    // Table structure for table houdinv_viewmembership

                $getDB->query("CREATE TABLE houdinv_viewmembership (
        id int(11) NOT NULL,
        customer_ids varchar(250) NOT NULL,
        code varchar(250) NOT NULL,
        DiscountPercentage varchar(250) NOT NULL,
        status int(1) DEFAULT '0',
        ExpiryDate varchar(250) NOT NULL,
        createdate varchar(250) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 

    // Table structure for table houdinv_visitors

                $getDB->query("CREATE TABLE houdinv_visitors (
        visitors_id int(11) NOT NULL,
        counts int(200) NOT NULL,
        visitors_date varchar(250) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
    // Table structure for table houdinv_vouchers

                $getDB->query("CREATE TABLE houdinv_vouchers (
        houdinv_vouchers_id int(11) NOT NULL,
        houdinv_vouchers_name varchar(50) NOT NULL,
        houdinv_vouchers_valid_from date NOT NULL,
        houdinv_vouchers_valid_to date NOT NULL,
        houdinv_vouchers_code varchar(6) NOT NULL,
        houdinv_vouchers_discount int(11) NOT NULL,
        houdinv_vouchers_status enum('active','deactive') NOT NULL,
        houdinv_vouchers_created_at varchar(30) DEFAULT NULL,
        houdinv_vouchers_modified_at varchar(30) DEFAULT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 

    // Table structure for table houdinv_workorder

                $getDB->query("CREATE TABLE houdinv_workorder (
        houdinv_workorder_id int(11) NOT NULL,
        houdinv_workorder_order_id int(11) NOT NULL DEFAULT '0',
        houdinv_workorder_customer_name varchar(150) NOT NULL,
        houdinv_workorder_customer_phone varchar(100) NOT NULL,
        houdinv_workorder_product int(11) NOT NULL DEFAULT '0',
        houdinv_workorder_variant int(11) NOT NULL DEFAULT '0',
        houdinv_workorder_quantity int(11) NOT NULL,
        houdinv_workorder_coupon_code varchar(20) NOT NULL,
        houdinv_workorder_coupon_discount int(11) NOT NULL,
        houdinv_workorder_pickup varchar(50) NOT NULL,
        houdinv_workorder_address text NOT NULL,
        houdinv_workorder_note text NOT NULL,
        houdinv_workorder_payment varchar(50) NOT NULL,
        houdinv_workorder_work_detail text NOT NULL,
        houdinv_workorder_paid_amount varchar(50) NOT NULL,
        houdinv_workorder_net_pay varchar(50) NOT NULL,
        houdinv_workorder_deliver_time varchar(100) NOT NULL,
        houdinv_workorder_deliver_date varchar(100) NOT NULL,
        houdinv_workorder_payment_status int(1) NOT NULL DEFAULT '0',
        date varchar(255) NOT NULL,
        update_date varchar(100) NOT NULL
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1");
 
// Indexes for dumped tables

    // Indexes for table cities

                $getDB->query("ALTER TABLE cities ADD PRIMARY KEY (city_id)");

    // Indexes for table houdinv_block_users

                $getDB->query("ALTER TABLE houdinv_block_users
    ADD PRIMARY KEY (houdinv_block_user_id)");

    // Indexes for table houdinv_campaignemail

                $getDB->query("ALTER TABLE houdinv_campaignemail
    ADD PRIMARY KEY (campaignsms_id)");

                $getDB->query("ALTER TABLE houdinv_campaignemail_user
ADD PRIMARY KEY (campaignsms_user_id)");

//
// Indexes for table houdinv_campaignpush
//
                $getDB->query("ALTER TABLE houdinv_campaignpush
ADD PRIMARY KEY (campaignsms_id)");

//
// Indexes for table houdinv_campaignpush_user
//
                $getDB->query("ALTER TABLE houdinv_campaignpush_user
ADD PRIMARY KEY (campaignsms_user_id)");

//
// Indexes for table houdinv_campaignsms
//
                $getDB->query("ALTER TABLE houdinv_campaignsms
ADD PRIMARY KEY (campaignsms_id)");

//
// Indexes for table houdinv_campaignsms_user
//
                $getDB->query("ALTER TABLE houdinv_campaignsms_user
ADD PRIMARY KEY (campaignsms_user_id)");

//
// Indexes for table houdinv_categories
//
                $getDB->query("ALTER TABLE houdinv_categories
ADD PRIMARY KEY (houdinv_category_id)");

//
// Indexes for table houdinv_checkout_address_data
//
                $getDB->query("ALTER TABLE houdinv_checkout_address_data
ADD PRIMARY KEY (houdinv_data_id)");

//
// Indexes for table houdinv_countries
//
                $getDB->query("ALTER TABLE houdinv_countries
ADD PRIMARY KEY (country_id)");

//
// Indexes for table houdinv_coupons
//
                $getDB->query("ALTER TABLE houdinv_coupons
ADD PRIMARY KEY (houdinv_coupons_id)");

//
// Indexes for table houdinv_coupons_procus
//
                $getDB->query("ALTER TABLE houdinv_coupons_procus
ADD PRIMARY KEY (houdinv_coupons_procus_id)");

//
// Indexes for table houdinv_customersetting
//
                $getDB->query("ALTER TABLE houdinv_customersetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_custom_home_data
//
                $getDB->query("ALTER TABLE houdinv_custom_home_data
ADD PRIMARY KEY (houdinv_custom_home_data_id)");

//
// Indexes for table houdinv_custom_links
//
                $getDB->query("ALTER TABLE houdinv_custom_links
ADD PRIMARY KEY (houdinv_custom_links_id)");

//
// Indexes for table houdinv_custom_slider
//
                $getDB->query("ALTER TABLE houdinv_custom_slider
ADD PRIMARY KEY (houdinv_custom_slider_id)");

//
// Indexes for table houdinv_deliveries
//
                $getDB->query("ALTER TABLE houdinv_deliveries
ADD PRIMARY KEY (houdinv_delivery_id)");

//
// Indexes for table houdinv_delivery_tracks
//
                $getDB->query("ALTER TABLE houdinv_delivery_tracks
ADD PRIMARY KEY (houdinv_delivery_track_id)");

//
// Indexes for table houdinv_delivery_users
//
                $getDB->query("ALTER TABLE houdinv_delivery_users
ADD PRIMARY KEY (houdinv_delivery_user_id)");

//
// Indexes for table houdinv_emailsms_payment
//
                $getDB->query("ALTER TABLE houdinv_emailsms_payment
ADD PRIMARY KEY (houdinv_emailsms_payment_id)");

//
// Indexes for table houdinv_emailsms_stats
//
                $getDB->query("ALTER TABLE houdinv_emailsms_stats
ADD PRIMARY KEY (houdinv_emailsms_stats_id)");

//
// Indexes for table houdinv_email_log
//
                $getDB->query("ALTER TABLE houdinv_email_log
ADD PRIMARY KEY (houdinv_email_log_id)");

//
// Indexes for table houdinv_email_Template
//
                $getDB->query("ALTER TABLE houdinv_email_Template
ADD PRIMARY KEY (houdinv_email_template_id)");

//
// Indexes for table houdinv_extra_expence
//
                $getDB->query("ALTER TABLE houdinv_extra_expence
ADD PRIMARY KEY (houdinv_extra_expence_id)");

//
// Indexes for table houdinv_extra_expence_item
//
                $getDB->query("ALTER TABLE houdinv_extra_expence_item
ADD PRIMARY KEY (houdinv_extra_expence_item_id)");

//
// Indexes for table houdinv_google_analytics
//
                $getDB->query("ALTER TABLE houdinv_google_analytics
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_inventories
//
                $getDB->query("ALTER TABLE houdinv_inventories
ADD PRIMARY KEY (houdinv_inventory_id)");

//
// Indexes for table houdinv_inventorysetting
//
                $getDB->query("ALTER TABLE houdinv_inventorysetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_inventory_purchase
//
                $getDB->query("ALTER TABLE houdinv_inventory_purchase
ADD PRIMARY KEY (houdinv_inventory_purchase_id)");

//
// Indexes for table houdinv_inventory_tracks
//
                $getDB->query("ALTER TABLE houdinv_inventory_tracks
ADD PRIMARY KEY (houdinv_inventory_track_id)");

//
// Indexes for table houdinv_invoicesetting
//
                $getDB->query("ALTER TABLE houdinv_invoicesetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_navigation_store_pages
//
                $getDB->query("ALTER TABLE houdinv_navigation_store_pages
ADD PRIMARY KEY (houdinv_navigation_store_pages_id)");

//
// Indexes for table houdinv_noninventory_products
//
                $getDB->query("ALTER TABLE houdinv_noninventory_products
ADD PRIMARY KEY (houdinv_noninventory_products_id)");

//
// Indexes for table houdinv_offline_payment_settings
//
                $getDB->query("ALTER TABLE houdinv_offline_payment_settings
ADD PRIMARY KEY (houdinv_offline_payment_setting_id)");

//
// Indexes for table houdinv_online_payment_settings
//
                $getDB->query("ALTER TABLE houdinv_online_payment_settings
ADD PRIMARY KEY (houdinv_online_payment_setting_id)");

//
// Indexes for table houdinv_orders
//
                $getDB->query("ALTER TABLE houdinv_orders
ADD PRIMARY KEY (houdinv_order_id)");

//
// Indexes for table houdinv_order_users
//
                $getDB->query("ALTER TABLE houdinv_order_users
ADD PRIMARY KEY (houdinv_order_users_id)");

//
// Indexes for table houdinv_payment_gateway
//
                $getDB->query("ALTER TABLE houdinv_payment_gateway
ADD PRIMARY KEY (houdinv_payment_gateway_id)");

//
// Indexes for table houdinv_possetting
//
                $getDB->query("ALTER TABLE houdinv_possetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_products
//
                $getDB->query("ALTER TABLE houdinv_products
ADD PRIMARY KEY (houdin_products_id)");

//
// Indexes for table houdinv_products_variants
//
                $getDB->query("ALTER TABLE houdinv_products_variants
ADD PRIMARY KEY (houdin_products_variants_id)");

//
// Indexes for table houdinv_productType
//
                $getDB->query("ALTER TABLE houdinv_productType
ADD PRIMARY KEY (houdinv_productType_id)");

//
// Indexes for table houdinv_product_reviews
//
                $getDB->query("ALTER TABLE houdinv_product_reviews
ADD PRIMARY KEY (houdinv_product_review_id)");

//
// Indexes for table houdinv_product_types
//
                $getDB->query("ALTER TABLE houdinv_product_types
ADD PRIMARY KEY (houdinv_product_type_id)");

//
// Indexes for table houdinv_purchase_products
//
                $getDB->query("ALTER TABLE houdinv_purchase_products
ADD PRIMARY KEY (houdinv_purchase_products_id)");

//
// Indexes for table houdinv_purchase_products_inward
//
                $getDB->query("ALTER TABLE houdinv_purchase_products_inward
ADD PRIMARY KEY (houdinv_purchase_products_inward_id)");

//
// Indexes for table houdinv_push_Template
//
                $getDB->query("ALTER TABLE houdinv_push_Template
ADD PRIMARY KEY (houdinv_push_template_id)");

//
// Indexes for table houdinv_shipping_charges
//
                $getDB->query("ALTER TABLE houdinv_shipping_charges
ADD PRIMARY KEY (houdinv_shipping_charge_id)");

//
// Indexes for table houdinv_shipping_credentials
//
                $getDB->query("ALTER TABLE houdinv_shipping_credentials
ADD PRIMARY KEY (houdinv_shipping_credentials_id)");

//
// Indexes for table houdinv_shipping_data
//
                $getDB->query("ALTER TABLE houdinv_shipping_data
ADD PRIMARY KEY (houdinv_shipping_data_id)");

//
// Indexes for table houdinv_shipping_ruels
//
                $getDB->query("ALTER TABLE houdinv_shipping_ruels
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_shipping_warehouse
//
                $getDB->query("ALTER TABLE houdinv_shipping_warehouse
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_shop_applogo
//
                $getDB->query("ALTER TABLE houdinv_shop_applogo
ADD PRIMARY KEY (applogo_id)");

//
// Indexes for table houdinv_shop_ask_quotation
//
                $getDB->query("ALTER TABLE houdinv_shop_ask_quotation
ADD PRIMARY KEY (houdinv_shop_ask_quotation_id)");

//
// Indexes for table houdinv_shop_detail
//
                $getDB->query("ALTER TABLE houdinv_shop_detail
ADD PRIMARY KEY (houdinv_shop_id)");

//
// Indexes for table houdinv_shop_logo
//
                $getDB->query("ALTER TABLE houdinv_shop_logo
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_shop_order_configuration
//
                $getDB->query("ALTER TABLE houdinv_shop_order_configuration
ADD PRIMARY KEY (houdinv_shop_order_configuration_id)");

//
// Indexes for table houdinv_shop_query
//
                $getDB->query("ALTER TABLE houdinv_shop_query
ADD PRIMARY KEY (houdinv_shop_query_id)");

//
// Indexes for table houdinv_skusetting
//
                $getDB->query("ALTER TABLE houdinv_skusetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_sms_log
//
                $getDB->query("ALTER TABLE houdinv_sms_log
ADD PRIMARY KEY (houdinv_sms_log_id)");

//
// Indexes for table houdinv_sms_template
//
                $getDB->query("ALTER TABLE houdinv_sms_template
ADD PRIMARY KEY (houdinv_sms_template_id)");

//
// Indexes for table houdinv_social_links
//
                $getDB->query("ALTER TABLE houdinv_social_links
ADD PRIMARY KEY (social_id)");

//
// Indexes for table houdinv_staff_management
//
                $getDB->query("ALTER TABLE houdinv_staff_management
ADD PRIMARY KEY (staff_id)");

//
// Indexes for table houdinv_staff_management_sub
//
                $getDB->query("ALTER TABLE houdinv_staff_management_sub
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_states
//
                $getDB->query("ALTER TABLE houdinv_states
ADD PRIMARY KEY (state_id)");

//
// Indexes for table houdinv_stock_transfer_log
//
                $getDB->query("ALTER TABLE houdinv_stock_transfer_log
ADD PRIMARY KEY (houdinv_stock_transfer_log_id)");

//
// Indexes for table houdinv_storediscount
//
                $getDB->query("ALTER TABLE houdinv_storediscount
ADD PRIMARY KEY (discount_id)");

//
// Indexes for table houdinv_storepolicies
//
                $getDB->query("ALTER TABLE houdinv_storepolicies
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_storesetting
//
                $getDB->query("ALTER TABLE houdinv_storesetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_sub_categories_one
//
                $getDB->query("ALTER TABLE houdinv_sub_categories_one
ADD PRIMARY KEY (houdinv_sub_category_one_id)");

//
// Indexes for table houdinv_sub_categories_two
//
                $getDB->query("ALTER TABLE houdinv_sub_categories_two
ADD PRIMARY KEY (houdinv_sub_category_two_id)");

//
// Indexes for table houdinv_suppliers
//
                $getDB->query("ALTER TABLE houdinv_suppliers
ADD PRIMARY KEY (houdinv_supplier_id)");

//
// Indexes for table houdinv_supplier_products
//
                $getDB->query("ALTER TABLE houdinv_supplier_products
ADD PRIMARY KEY (houdinv_supplier_products_id)");

//
// Indexes for table houdinv_taxes
//
                $getDB->query("ALTER TABLE houdinv_taxes
ADD PRIMARY KEY (houdinv_tax_id)");

//
// Indexes for table houdinv_taxsetting
//
                $getDB->query("ALTER TABLE houdinv_taxsetting
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_templates
//
                $getDB->query("ALTER TABLE houdinv_templates
ADD PRIMARY KEY (houdinv_template_id)");

//
// Indexes for table houdinv_template_pages
//
                $getDB->query("ALTER TABLE houdinv_template_pages
ADD PRIMARY KEY (houdinv_template_page_id)");

//
// Indexes for table houdinv_testimonials
//
                $getDB->query("ALTER TABLE houdinv_testimonials
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_transaction
//
                $getDB->query("ALTER TABLE houdinv_transaction
ADD PRIMARY KEY (houdinv_transaction_id)");

//
// Indexes for table houdinv_users
//
                $getDB->query("ALTER TABLE houdinv_users
ADD PRIMARY KEY (houdinv_user_id)");

//
// Indexes for table houdinv_users_cart
//
                $getDB->query("ALTER TABLE houdinv_users_cart
ADD PRIMARY KEY (houdinv_users_cart_id)");

//
// Indexes for table houdinv_users_forgot
//
                $getDB->query("ALTER TABLE houdinv_users_forgot
ADD PRIMARY KEY (houdinv_users_forgot_id)");

//
// Indexes for table houdinv_users_group
//
                $getDB->query("ALTER TABLE houdinv_users_group
ADD PRIMARY KEY (houdinv_users_group_id)");

//
// Indexes for table houdinv_users_group_users_list
//
                $getDB->query("ALTER TABLE houdinv_users_group_users_list
ADD PRIMARY KEY (houdinv_users_group_users_list_id)");

//
// Indexes for table houdinv_users_whishlist
//
                $getDB->query("ALTER TABLE houdinv_users_whishlist
ADD PRIMARY KEY (houdinv_users_whishlist_id)");

//
// Indexes for table houdinv_user_address
//
                $getDB->query("ALTER TABLE houdinv_user_address
ADD PRIMARY KEY (houdinv_user_address_id)");

//
// Indexes for table houdinv_user_addresses
//
                $getDB->query("ALTER TABLE houdinv_user_addresses
ADD PRIMARY KEY (houdinv_user_address_id)");

//
// Indexes for table houdinv_user_bank_details
//
                $getDB->query("ALTER TABLE houdinv_user_bank_details
ADD PRIMARY KEY (houdinv_user_bank_detail_id)");

//
// Indexes for table houdinv_user_payments
//
                $getDB->query("ALTER TABLE houdinv_user_payments
ADD PRIMARY KEY (houdinv_user_payment_id)");

//
// Indexes for table houdinv_user_tag
//
                $getDB->query("ALTER TABLE houdinv_user_tag
ADD PRIMARY KEY (houdinv_user_tag_id)");

//
// Indexes for table houdinv_user_use_Coupon
//
                $getDB->query("ALTER TABLE houdinv_user_use_Coupon
ADD PRIMARY KEY (houdinv_user_use_Coupon_id)");

//
// Indexes for table houdinv_viewmembership
//
                $getDB->query("ALTER TABLE houdinv_viewmembership
ADD PRIMARY KEY (id)");

//
// Indexes for table houdinv_visitors
//
                $getDB->query("ALTER TABLE houdinv_visitors
ADD PRIMARY KEY (visitors_id)");

//
// Indexes for table houdinv_vouchers
//
                $getDB->query("ALTER TABLE houdinv_vouchers
ADD PRIMARY KEY (houdinv_vouchers_id)");

//
// Indexes for table houdinv_workorder
//
                $getDB->query("ALTER TABLE houdinv_workorder
ADD PRIMARY KEY (houdinv_workorder_id)");

//
// AUTO_INCREMENT for dumped tables
//

//
// AUTO_INCREMENT for table cities
//
                $getDB->query("ALTER TABLE cities
MODIFY city_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6178");
//
// AUTO_INCREMENT for table houdinv_block_users
//
                $getDB->query("ALTER TABLE houdinv_block_users
MODIFY houdinv_block_user_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignemail
//
                $getDB->query("ALTER TABLE houdinv_campaignemail
MODIFY campaignsms_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignemail_user
//
                $getDB->query("ALTER TABLE houdinv_campaignemail_user
MODIFY campaignsms_user_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignpush
//
                $getDB->query("ALTER TABLE houdinv_campaignpush
MODIFY campaignsms_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignpush_user
//
                $getDB->query("ALTER TABLE houdinv_campaignpush_user
MODIFY campaignsms_user_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignsms
//
                $getDB->query("ALTER TABLE houdinv_campaignsms
MODIFY campaignsms_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_campaignsms_user
//
                $getDB->query("ALTER TABLE houdinv_campaignsms_user
MODIFY campaignsms_user_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_categories
//
                $getDB->query("ALTER TABLE houdinv_categories
MODIFY houdinv_category_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9");
//
// AUTO_INCREMENT for table houdinv_checkout_address_data
//
                $getDB->query("ALTER TABLE houdinv_checkout_address_data
MODIFY houdinv_data_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12");
//
// AUTO_INCREMENT for table houdinv_countries
//
                $getDB->query("ALTER TABLE houdinv_countries
MODIFY country_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240");
//
// AUTO_INCREMENT for table houdinv_coupons
//
                $getDB->query("ALTER TABLE houdinv_coupons
MODIFY houdinv_coupons_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");
//
// AUTO_INCREMENT for table houdinv_coupons_procus
//
                $getDB->query("ALTER TABLE houdinv_coupons_procus
MODIFY houdinv_coupons_procus_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27");
//
// AUTO_INCREMENT for table houdinv_customersetting
//
                $getDB->query("ALTER TABLE houdinv_customersetting
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");
//
// AUTO_INCREMENT for table houdinv_custom_home_data
//
                $getDB->query("ALTER TABLE houdinv_custom_home_data
MODIFY houdinv_custom_home_data_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12");
//
// AUTO_INCREMENT for table houdinv_custom_links
//
                $getDB->query("ALTER TABLE houdinv_custom_links
MODIFY houdinv_custom_links_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8");
//
// AUTO_INCREMENT for table houdinv_custom_slider
//
                $getDB->query("ALTER TABLE houdinv_custom_slider
MODIFY houdinv_custom_slider_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5");
//
// AUTO_INCREMENT for table houdinv_deliveries
//
                $getDB->query("ALTER TABLE houdinv_deliveries
MODIFY houdinv_delivery_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11");
//
// AUTO_INCREMENT for table houdinv_delivery_tracks
//
                $getDB->query("ALTER TABLE houdinv_delivery_tracks
MODIFY houdinv_delivery_track_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_delivery_users
//
                $getDB->query("ALTER TABLE houdinv_delivery_users
MODIFY houdinv_delivery_user_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_emailsms_payment
//
                $getDB->query("ALTER TABLE houdinv_emailsms_payment
MODIFY houdinv_emailsms_payment_id int(11) NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_emailsms_stats
//
                $getDB->query("ALTER TABLE houdinv_emailsms_stats
MODIFY houdinv_emailsms_stats_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");
//
// AUTO_INCREMENT for table houdinv_email_log
//
                $getDB->query("ALTER TABLE houdinv_email_log
MODIFY houdinv_email_log_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202");
//
// AUTO_INCREMENT for table houdinv_email_Template
//
                $getDB->query("ALTER TABLE houdinv_email_Template
MODIFY houdinv_email_template_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6");
//
// AUTO_INCREMENT for table houdinv_extra_expence
//
                $getDB->query("ALTER TABLE houdinv_extra_expence
MODIFY houdinv_extra_expence_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");
//
// AUTO_INCREMENT for table houdinv_extra_expence_item
//
                $getDB->query("ALTER TABLE houdinv_extra_expence_item
MODIFY houdinv_extra_expence_item_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7");
//
// AUTO_INCREMENT for table houdinv_google_analytics
//
                $getDB->query("ALTER TABLE houdinv_google_analytics
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");
//
// AUTO_INCREMENT for table houdinv_inventories
//
                $getDB->query("ALTER TABLE houdinv_inventories
MODIFY houdinv_inventory_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_inventorysetting
//
                $getDB->query("ALTER TABLE houdinv_inventorysetting
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");
//
// AUTO_INCREMENT for table houdinv_inventory_purchase
//
                $getDB->query("ALTER TABLE houdinv_inventory_purchase
MODIFY houdinv_inventory_purchase_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");
//
// AUTO_INCREMENT for table houdinv_inventory_tracks
//
                $getDB->query("ALTER TABLE houdinv_inventory_tracks
MODIFY houdinv_inventory_track_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");
//
// AUTO_INCREMENT for table houdinv_invoicesetting
//
                $getDB->query("ALTER TABLE houdinv_invoicesetting
MODIFY id int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");
//
// AUTO_INCREMENT for table houdinv_navigation_store_pages
//
                $getDB->query("ALTER TABLE houdinv_navigation_store_pages
MODIFY houdinv_navigation_store_pages_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71");
//
// AUTO_INCREMENT for table houdinv_noninventory_products
//
                $getDB->query("ALTER TABLE houdinv_noninventory_products
MODIFY houdinv_noninventory_products_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7");
//
// AUTO_INCREMENT for table houdinv_offline_payment_settings
//
                $getDB->query("ALTER TABLE houdinv_offline_payment_settings
MODIFY houdinv_offline_payment_setting_id int(11) UNSIGNED NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_online_payment_settings

                $getDB->query("ALTER TABLE houdinv_online_payment_settings
MODIFY houdinv_online_payment_setting_id int(11) NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_orders

                $getDB->query("ALTER TABLE houdinv_orders
MODIFY houdinv_order_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=512");

// AUTO_INCREMENT for table houdinv_order_users

                $getDB->query("ALTER TABLE houdinv_order_users
MODIFY houdinv_order_users_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_payment_gateway

                $getDB->query("ALTER TABLE houdinv_payment_gateway
MODIFY houdinv_payment_gateway_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_possetting

                $getDB->query("ALTER TABLE houdinv_possetting
MODIFY id int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_products

                $getDB->query("ALTER TABLE houdinv_products
MODIFY houdin_products_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38");

// AUTO_INCREMENT for table houdinv_products_variants

                $getDB->query("ALTER TABLE houdinv_products_variants
MODIFY houdin_products_variants_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27");

// AUTO_INCREMENT for table houdinv_productType

                $getDB->query("ALTER TABLE houdinv_productType
MODIFY houdinv_productType_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");

// AUTO_INCREMENT for table houdinv_product_reviews

                $getDB->query("ALTER TABLE houdinv_product_reviews
MODIFY houdinv_product_review_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66");

// AUTO_INCREMENT for table houdinv_product_types

                $getDB->query("ALTER TABLE houdinv_product_types
MODIFY houdinv_product_type_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_purchase_products

                $getDB->query("ALTER TABLE houdinv_purchase_products
MODIFY houdinv_purchase_products_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8");

// AUTO_INCREMENT for table houdinv_purchase_products_inward

                $getDB->query("ALTER TABLE houdinv_purchase_products_inward
MODIFY houdinv_purchase_products_inward_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7");

// AUTO_INCREMENT for table houdinv_push_Template

                $getDB->query("ALTER TABLE houdinv_push_Template
MODIFY houdinv_push_template_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5");

// AUTO_INCREMENT for table houdinv_shipping_charges

                $getDB->query("ALTER TABLE houdinv_shipping_charges
MODIFY houdinv_shipping_charge_id int(11) NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_shipping_credentials

                $getDB->query("ALTER TABLE houdinv_shipping_credentials
MODIFY houdinv_shipping_credentials_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_shipping_data

                $getDB->query("ALTER TABLE houdinv_shipping_data
MODIFY houdinv_shipping_data_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");

// AUTO_INCREMENT for table houdinv_shipping_ruels

                $getDB->query("ALTER TABLE houdinv_shipping_ruels
MODIFY id int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_shipping_warehouse

                $getDB->query("ALTER TABLE houdinv_shipping_warehouse
MODIFY id int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_shop_applogo

                $getDB->query("ALTER TABLE houdinv_shop_applogo
MODIFY applogo_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_shop_ask_quotation

                $getDB->query("ALTER TABLE houdinv_shop_ask_quotation
MODIFY houdinv_shop_ask_quotation_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51");

// AUTO_INCREMENT for table houdinv_shop_detail

                $getDB->query("ALTER TABLE houdinv_shop_detail
MODIFY houdinv_shop_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_shop_logo

                $getDB->query("ALTER TABLE houdinv_shop_logo
MODIFY id int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_shop_order_configuration

                $getDB->query("ALTER TABLE houdinv_shop_order_configuration
MODIFY houdinv_shop_order_configuration_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_shop_query

                $getDB->query("ALTER TABLE houdinv_shop_query
MODIFY houdinv_shop_query_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16");

// AUTO_INCREMENT for table houdinv_skusetting

                $getDB->query("ALTER TABLE houdinv_skusetting
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_sms_log

                $getDB->query("ALTER TABLE houdinv_sms_log
MODIFY houdinv_sms_log_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=368");

// AUTO_INCREMENT for table houdinv_sms_template

                $getDB->query("ALTER TABLE houdinv_sms_template
MODIFY houdinv_sms_template_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5");

// AUTO_INCREMENT for table houdinv_social_links

                $getDB->query("ALTER TABLE houdinv_social_links
MODIFY social_id int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_staff_management

                $getDB->query("ALTER TABLE houdinv_staff_management
MODIFY staff_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10");

// AUTO_INCREMENT for table houdinv_staff_management_sub

                $getDB->query("ALTER TABLE houdinv_staff_management_sub
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9");

// AUTO_INCREMENT for table houdinv_states

                $getDB->query("ALTER TABLE houdinv_states
MODIFY state_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1652");

// AUTO_INCREMENT for table houdinv_stock_transfer_log

                $getDB->query("ALTER TABLE houdinv_stock_transfer_log
MODIFY houdinv_stock_transfer_log_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

// AUTO_INCREMENT for table houdinv_storediscount

                $getDB->query("ALTER TABLE houdinv_storediscount
MODIFY discount_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13");

// AUTO_INCREMENT for table houdinv_storepolicies

                $getDB->query("ALTER TABLE houdinv_storepolicies
MODIFY id int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_storesetting

                $getDB->query("ALTER TABLE houdinv_storesetting
MODIFY id int(220) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_sub_categories_one

                $getDB->query("ALTER TABLE houdinv_sub_categories_one
MODIFY houdinv_sub_category_one_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6");

// AUTO_INCREMENT for table houdinv_sub_categories_two

                $getDB->query("ALTER TABLE houdinv_sub_categories_two
MODIFY houdinv_sub_category_two_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12");

// AUTO_INCREMENT for table houdinv_suppliers

                $getDB->query("ALTER TABLE houdinv_suppliers
MODIFY houdinv_supplier_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8");

// AUTO_INCREMENT for table houdinv_supplier_products

                $getDB->query("ALTER TABLE houdinv_supplier_products
MODIFY houdinv_supplier_products_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38");

// AUTO_INCREMENT for table houdinv_taxes

                $getDB->query("ALTER TABLE houdinv_taxes
MODIFY houdinv_tax_id int(11) NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_taxsetting

                $getDB->query("ALTER TABLE houdinv_taxsetting
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2");

// AUTO_INCREMENT for table houdinv_templates

                $getDB->query("ALTER TABLE houdinv_templates
MODIFY houdinv_template_id int(11) NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_template_pages

                $getDB->query("ALTER TABLE houdinv_template_pages
MODIFY houdinv_template_page_id int(11) NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_testimonials

                $getDB->query("ALTER TABLE houdinv_testimonials
MODIFY id int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6");

// AUTO_INCREMENT for table houdinv_transaction

                $getDB->query("ALTER TABLE houdinv_transaction
MODIFY houdinv_transaction_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90");

// AUTO_INCREMENT for table houdinv_users

                $getDB->query("ALTER TABLE houdinv_users
MODIFY houdinv_user_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82");

// AUTO_INCREMENT for table houdinv_users_cart

                $getDB->query("ALTER TABLE houdinv_users_cart
MODIFY houdinv_users_cart_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=317");

// AUTO_INCREMENT for table houdinv_users_forgot

                $getDB->query("ALTER TABLE houdinv_users_forgot
MODIFY houdinv_users_forgot_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93");

// AUTO_INCREMENT for table houdinv_users_group

                $getDB->query("ALTER TABLE houdinv_users_group
MODIFY houdinv_users_group_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");

// AUTO_INCREMENT for table houdinv_users_group_users_list

                $getDB->query("ALTER TABLE houdinv_users_group_users_list
MODIFY houdinv_users_group_users_list_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10");

// AUTO_INCREMENT for table houdinv_users_whishlist

                $getDB->query("ALTER TABLE houdinv_users_whishlist
MODIFY houdinv_users_whishlist_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=365");

// AUTO_INCREMENT for table houdinv_user_address

                $getDB->query("ALTER TABLE houdinv_user_address
MODIFY houdinv_user_address_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86");

// AUTO_INCREMENT for table houdinv_user_addresses

                $getDB->query("ALTER TABLE houdinv_user_addresses
MODIFY houdinv_user_address_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8");

// AUTO_INCREMENT for table houdinv_user_bank_details

                $getDB->query("ALTER TABLE houdinv_user_bank_details
MODIFY houdinv_user_bank_detail_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT");

// AUTO_INCREMENT for table houdinv_user_tag

                $getDB->query("ALTER TABLE houdinv_user_tag
MODIFY houdinv_user_tag_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6");

// AUTO_INCREMENT for table houdinv_user_use_Coupon

                $getDB->query("ALTER TABLE houdinv_user_use_Coupon
MODIFY houdinv_user_use_Coupon_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5");

// AUTO_INCREMENT for table houdinv_viewmembership

                $getDB->query("ALTER TABLE houdinv_viewmembership
MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4");

// AUTO_INCREMENT for table houdinv_visitors

                $getDB->query("ALTER TABLE houdinv_visitors
MODIFY visitors_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33");

// AUTO_INCREMENT for table houdinv_vouchers

                $getDB->query("ALTER TABLE houdinv_vouchers
MODIFY houdinv_vouchers_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5");

// AUTO_INCREMENT for table houdinv_workorder

                $getDB->query("ALTER TABLE houdinv_workorder
MODIFY houdinv_workorder_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3");

return true;
                }catch (Exception $e) {
                    return false;
                }


            
       

        return true;
        
    }




}