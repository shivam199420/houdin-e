<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
           <!--START Page-Title -->
            <div class="row">
            
                <div class="col-md-8">
                   <h4 class="page-title">Email Logs</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                 
                  <li class="active">Email Logs</li>
                  </ol>
                  </div>
                 
            </div>
           <!--END Page-Title -->
             
                    <div class="row">
                          <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>

                    <th>Email Used</th>
                    <th>Date</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach($emaillog as $emaillogList)
                  {                
                    ?>
                  <tr>
                    <td><?php echo $emaillogList->houdinv_email_log_credit_used ?></td>
                    <td><?php echo date('d-m-Y',$emaillogList->houdinv_email_log_created_at) ?></td>
                    <td>
                      <?php 
                      if($emaillogList->houdinv_email_log_status == 1)
                      {
                        $setclass = 'success';
                        $settext = 'Success';
                      }
                      else
                      {
                        $setclass = 'danger';
                        $settext = 'Failure';
                      }
                      ?>
                      <button class="btn btn-xs btn-<?php echo $setclass ?>"><?php echo $settext ?></button>
                    </td>
                    
                  </tr>
                  <?php }
                  ?>
                </tbody>
              </table>
              <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
                      </div>
                    </div>
                  </div>
                <?php $this->load->view('Template/footer.php') ?>
