<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Coupons extends CI_Controller 
{
    
    function __construct()
    {
    	parent::__construct();
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        $this->load->model('Couponsmodel');
        
    	$this->load->helper('url');
    	$this->load->library('pagination');
        $this->perPage = 100;
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->Validconfigs = array(
        array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
        ),
        array(
                'field' => 'discount',
                'label' => 'Discount',
                'rules' => 'required'
        ),
        array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
              ),
        array(
                'field' => 'limit',
                'label' => 'Limit',
                'rules' => 'required'
              ),
        array(
                'field' => 'type',
                'label' => 'Type',
                'rules' => 'required'
              ),                            
        array(
                'field' => 'date_From',
                'label' => 'Valid from date',
                'rules' => 'required'
              ) ,
          array(
            'field' => 'date_to',
            'label' => 'Valid to date',
            'rules' => 'required|callback_compareDate'
          )           
            );
        
        
        }
    public function index()
    {
                if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        if($this->input->post('Add'))
            {
                $this->form_validation->set_rules($this->Validconfigs);
                if($this->form_validation->run() != true)
                {
                       $this->session->set_flashdata('message_name',validation_errors() );
                      redirect(base_url().'Coupons', 'refresh');    
                }
                $date = strtotime(date("Y-m-d, h:i:s"));
                
                $array = array("houdin_coupon_code"=>$this->input->post('name'),"houdin_coupon_discount"=>$this->input->post('discount'),"houdin_coupon_limit"=>$this->input->post('limit'),
                          "houdin_coupon_type"=>$this->input->post('type'),"houdin_coupon_valid_from"=>strtotime($this->input->post('date_From')),"houdin_coupon_valid_to"=>strtotime($this->input->post('date_to')),
                          "houdin_coupon_created_at"=>$date,"houdin_coupon_updated_at"=>$date,"houdin_coupon_status"=>$this->input->post('status'));
                

                $data = $this->Couponsmodel->CouponAdd($array);
                if($data)
                {
                        $this->session->set_flashdata('success',"Coupon added successfully");
                        redirect(base_url().'Coupons', 'refresh');                 
                }    
            }
            // edit
      if($this->input->post('Edit'))
        {
            $this->form_validation->set_rules($this->Validconfigs);            
            if($this->form_validation->run() != true)
            {
                   $this->session->set_flashdata('message_name',validation_errors() );
                  redirect(base_url().'Coupons', 'refresh');    
            }
            $date = strtotime(date("Y-m-d, h:i:s"));
            $array = array("houdin_coupon_code"=>$this->input->post('name'),"houdin_coupon_discount"=>$this->input->post('discount'),"houdin_coupon_limit"=>$this->input->post('limit'),
                          "houdin_coupon_type"=>$this->input->post('type'),"houdin_coupon_valid_from"=>strtotime($this->input->post('date_From')),"houdin_coupon_valid_to"=>strtotime($this->input->post('date_to')),
                          "houdin_coupon_updated_at"=>$date,"houdin_coupon_status"=>$this->input->post('status'));
                

            
            
            $data = $this->Couponsmodel->CouponEdit($array,$this->input->post('Edit_id'));
            if($data)
            {
                $this->session->set_flashdata('success',"Coupon updated successfully");
                redirect(base_url().'Coupons', 'refresh');                 
            }
        }   
        if($this->input->post('delete'))
        {
            if($this->input->post('delete_input'))
            {
                $data = $this->Couponsmodel->CouponDelete($this->input->post('delete_input'));
                if($data)
                {
                    $this->session->set_flashdata('success',"Coupon deleted successfully");
                    redirect(base_url().'Coupons', 'refresh');                 
                }  
            }
        }
        // fetch all category data
        $AllCoupon= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->Couponsmodel->getAllCoupon($conditions);
        //pagination config
        $config['base_url']    = base_url().'Coupons/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset; 
        $conditions['limit'] = $this->perPage;
        $AllCoupon['Coupon'] = $this->Couponsmodel->getAllCoupon($conditions);
        $AllCoupon['ActiveCoupon'] = $this->Couponsmodel->CouponActive();
        $AllCoupon['ExpireCoupon'] = $this->Couponsmodel->CouponExpired();
        $AllCoupon['totalRec'] = $totalRec;
        
        $this->load->view('Coupons',$AllCoupon);
    }
    
    function compareDate() {
  $startDate = strtotime($_POST['date_From']);
  $endDate = strtotime($_POST['date_to']);

  if ($endDate >= $startDate)
    return True;
  else {
    $this->form_validation->set_message('compareDate', 'valid to date should be greater than Contract Start Date.');
    return False;
  }
}
}
