<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Authentication');
		$this->load->helper('url');
        $this->load->library('session');
		$this->load->helper('passwordhash');  //password encryption helper
        $this->load->helper('form');
        $this->load->library('form_validation');
	}

	public function index()
	{
        if($this->session->userdata('userAuth'))
           {
            redirect(base_url().'dashboard', 'refresh');
           }
		$this->load->view('login');
	}
    
    public function submit() // login form submit request
    {
            $config = array(
        array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email'
        ),
        array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required',
                'errors' => array(
                        'required' => 'You must provide a %s.',
                ),
        ),
        array(
                'field' => 'cpassword',
                'label' => 'Password Confirmation',
                'rules' => 'required|matches[password]'
        )
            );
            
            $this->form_validation->set_rules($config);
            
            if($this->form_validation->run() != true)
            {
                           $this->session->set_flashdata('message_name',validation_errors() );
                          redirect(base_url().'Login', 'refresh');    
            }
            
            

             $data = $this->Authentication->AdminLogin($this->input->post()); // check user detail
             
             if($data)
             {
                if($data->houdin_admin_user_status=='active')
                {

                
                    $msg = $this->SendSms($data->houdin_admin_user_email);   // store random token in db
                    if($msg)
                    {
                        

                    }
                    
                }
                else
                {
                    if($data->houdin_admin_user_status=='deactive')
                    {
                      $this->session->set_flashdata('message_name', 'Your account is suspended right now by admin');
                      
                    }
                    else if($data->houdin_admin_user_status=='block')
                    {
                        $this->session->set_flashdata('message_name', 'Your account is blocked  by admin');
                        
                    }  
                    else
                    {
                        $this->session->set_flashdata('message_name', 'An unknown error encounter');
                        
                    }
                    
                    redirect(base_url().'Login', 'refresh');
                }
                
             }
             else
             {
                
              $this->session->set_flashdata('message_name', 'Please enter correct credentials');
              redirect(base_url().'Login', 'refresh');
                
             }
    


    }

	
	public function accountverification()
	{
	   if(!$this->session->userdata('VarifyId'))   // email of user 
       {
        redirect(base_url().'Login', 'refresh');
       }

       if($this->input->post('varify'))
       {

            $config = array(
        array(
                'field' => 'token',
                'label' => 'Token',
                'rules' => 'required|numeric|min_length[3]|max_length[5]'
              )
            );

            $this->form_validation->set_rules($config);
            
            if($this->form_validation->run() != true)
            {
                           $this->session->set_flashdata('message_name',validation_errors() );
                          redirect(base_url().'Login/accountverification', 'refresh');    
            }


              //match  user token 
              
              $varification = $this->Authentication->VarifyTokenByDb($this->input->post('token'),$this->session->userdata('VarifyId'));
              if($varification)
              {
                
                $SaveLoginDetail = $this->Authentication->SaveAllLoginDeatil($this->session->userdata('VarifyId'));
                if($SaveLoginDetail)
                {
                $this->session->set_userdata('userAuth',$SaveLoginDetail['loginAuth']);
                $this->session->set_userdata('role',$SaveLoginDetail['userrole']);
                $this->session->set_userdata('department',$SaveLoginDetail['department']);
                $this->session->unset_userdata('VarifyId');
                redirect(base_url().'Dashboard', 'refresh');
                
                }
              }
              else
              {
                    $this->session->set_flashdata('message_name', 'Token mismatch');
                    redirect(base_url().'Login/accountverification', 'refresh');                
              }
              
       }
		$this->load->view('accountverification');
	}
    
    public function SendSms($para=false)
    {
        
        if(!$para)
        {
            $para = $this->session->userdata('VarifyId');
        }
        
         $userPhone = $this->Authentication->AdminDetail($para);
         if($userPhone)
         {
             $getTwillio = twillio();
                    $randomString = rand(1000,99999);
                    $id = $getTwillio[0]->houdin_twilio_sid;
                    $token = $getTwillio[0]->houdin_twilio_token;
                    $url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
                    $from = $getTwillio[0]->houdin_twilio_number;
    
                    $body = "Your houdin-e verification PIN is: ".$randomString."";
                    $data1 = array (
                        'From' => $from,
                        'To' => $userPhone->houdin_admin_user_phone,
                        'Body' => $body,
                    );
                   
                    $post = http_build_query($data1);
                    $x = curl_init($url );
                    curl_setopt($x, CURLOPT_POST, true);
                    curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
                    curl_setopt($x, CURLOPT_POSTFIELDS, $post);
                    $y = curl_exec($x);
                 //   print_r($y);
                 //   exit;
              /*      if (strpos($y, 'PIN') == false) 
                    {
                        $this->session->set_flashdata('message_name', 'Server Error occure, ! try again');
                        redirect(base_url().'Login', 'refresh');
                    }
                    else
                    { */
                    $tokenpara = array("houdin_second_auth_email"=>$para,"houdin_second_auth_token"=>$randomString);
                    $TokenSave = $this->Authentication->SaveToken($tokenpara);  
                        $this->session->set_userdata('VarifyId', $para); // session to check user email on varification page
                        redirect(base_url().'Login/accountverification', 'refresh');
                   // }
            }
    }
    
    
       //logout function
 public function logout()
 {
   
           $remove = $this->Authentication->RemoveAuthToken($this->session->userdata('userAuth'));
              $this->session->unset_userdata('userAuth');
              $this->session->unset_userdata('role');
                $this->session->unset_userdata('department');
              redirect(base_url().'Login', 'refresh');
 }
}
