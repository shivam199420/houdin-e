<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png">

        <title>Houdin-e - Shop Management</title>

		    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/vendor.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/intlTelInput.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/daterangepicker.css" type="text/css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/plugins/summernote/summernote.css" rel="stylesheet" />
        
        <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
<style type="text/css">
     .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, .navbar-default .navbar-nav>.active>a:hover {
    color: #fff !important;
    background-color: #5079ad !important;
} 

li.custom_li_header {
    padding: 3px 0px;
}
ul.list-unstyled.custom_ul_header {
    padding-left: 23px;
}
@media (max-width:767px){
  .hidden-xs {
    display: none!important;
}
}

.navbar-nav li.dropdown:hover > .dropdown-menu{
  display: block;
}
.navbar-nav li.dropdown .dropdown-menu .dropdown-menu{
left: 100%;
top: 0;
}
 .highcharts-credits
 {
     display:none !important;
 }
 .skiptranslate{display:none;}
#goog-gt-tt
{
    display:none !important;
}
</style>
    </head>


    <body class="fixed-left">
    
    <?php
     $this->load->helper('database');
     $getDatabaseData = switchDynamicDatabase();
     $this->db = $this->load->database($getDatabaseData,true);
    $getDynamicDb = $this->load->database('master',true);
    $resultAdminLogoData = $getDynamicDb->select('houdin_admin_logo_image')->from('houdin_admin_logo')->where('houdin_admin_logo_id','1')->get()->result();
    $getLogo = fetchvedorsitelogo();
    $setDynamicImageData = $getLogo;
    // $setDynamicImageData = "https://tech.hawkscode.in/houdin-e/master/uploads/logo/".$resultAdminLogoData[0]->houdin_admin_logo_image;
    $setDynamicImageData2=base_url()."assets/images/favicon_W.png"
    ?>
<?php
$shopNameData = shopNameData();
 
  if($this->session->userdata('vendorRole')==1)
    {
    $header_permission =  $this->db->select("*")->from("houdinv_staff_management_sub")
    ->join("houdinv_staff_management","houdinv_staff_management.staff_id=houdinv_staff_management_sub.staff_id","right outer")
    ->where("houdinv_staff_management.houdinv_staff_auth_token",$this->session->userdata('vendorAuth'))
    ->get()->row();
    }
    else
    {
        $header_permission = '';
    }
    // warehouse get
 $ware =    $this->db->select("*")->from("houdinv_shipping_warehouse")->get()->result();

    
  //  print_R($sidebar_permission);
?>
        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">
               <div class="topbar-left">
                    <div class="text-center">
                        <a href="<?php echo base_url(); ?>" class="logo"><!--<i class="icon-magnet icon-c-logo"></i>-->
                <img class="icon-c-logo" src="<?php echo $setDynamicImageData2; ?>"/>
                
                <!-- <img class="main-c-logo" src="<?php echo $setDynamicImageData; ?>" style="height:30px"/>  -->
                        <span><img class="main-c-logo" src="<?php echo base_url() ?>assets/images/Logo_F_B.png" style="height:30px"/> 
                        <!--Ub<i class="md md-album"></i>ld--></span></a>
                    </div>
                </div>

              

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect waves-light" style="color:black">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>
                            <ul class="nav navbar-nav navbar-right pull-right">
                            <!-- <li><div id="google_translate_element"></div></li> -->
                             <li class=" hidden-xs <?php if($this->router->fetch_class()=="dashboard"){echo 'active';}?>"><a href="<?php echo base_url(); ?>dashboard" class="waves-effect"> Home</a></li>
                            
                     
        <?php if(!$header_permission || ($header_permission->POS) !=''){ 
        ?> 
        <?php 
                    $getUserPlan = getVendorplan();
                    if($getUserPlan[0]->houdin_package_pos == 'yes')
                    {
                    ?>
                               <li class="dropdown top-menu-item-xs hidden-xs <?php if($this->router->fetch_class()=="POS" || $this->router->fetch_class()=="Workorder"){echo 'active';}?>">
                                    <a href="" class=" dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">POS <i class="fa fa-angle-down m-r-10 text-custom" style="color: #000;font-weight:600"></i></a>
                                    <ul class="dropdown-menu" >
                                       <li><a href="<?php echo base_url();?>POS">POS</a></li>
                        <li><a href="<?php echo base_url();?>Workorder">Workorder</a></li>
                                    </ul>
                                </li>
                    <?php } ?>


                              
            <?php  
         }
          ?>                   
                             
        <?php if(!$header_permission || ($header_permission->staff_members) !=''){ 
        ?>       
                              <li class="hidden-xs <?php if($this->router->fetch_class()=="staff"){echo 'active';}?>"><a href="<?php echo base_url(); ?>staff" class="waves-effect">Staff</a></li>
              <?php  
         }
          ?>                  
           <?php 
                    $getUserPlan = getVendorplan();
                    if($getUserPlan[0]->houdin_package_accounting == 'yes')
                    {
                    ?>
         <li class="dropdown top-menu-item-xs hidden-xs <?php if($this->router->fetch_class()=="Accounts" || $this->router->fetch_class()=="transaction"){echo 'active';}?>">
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light <?php if($this->router->fetch_class()=="accounts"){echo 'active';}?>" data-toggle="dropdown" aria-expanded="true">Accounting <i class="fa fa-angle-down m-r-10 text-custom" style="color: #000;font-weight:600"></i></a>
                                    <ul class="dropdown-menu"  >
                                     
                                    <li><a href="<?php echo base_url(); ?>Accounts">Dashboard</a></li>
                                    <li><a href="<?php echo base_url(); ?>Accounts/accounting">Accounting</a></li>
                                    <li><a href="<?php echo base_url(); ?>transaction">Transaction</a></li>
                                    <li><a href="<?php echo base_url(); ?>Accounts/expence">Expense</a></li>
                                    <li><a href="<?php echo base_url(); ?>Accounts/reports">Basic Reports</a></li>
                                     <li><a href="<?php echo base_url(); ?>Accounts/taxreport">Tax Report</a></li>
                                    <li><a href="<?php echo base_url(); ?>Accounts/balancesheet">Balance Sheet</a></li>
                                    <li><a href="<?php echo base_url(); ?>Accounts/customreports">Custom Summary Reports</a></li>
                                    <li><a href="<?php echo base_url(); ?>Accounts/profitlossreports">Profit & Loss Peport/A/C</a></li>
                                    <li><a href="<?php echo base_url(); ?>Accounts/profitlosspercentagereports">Profit Loss Percentage</a></li>
                                    
                                    </ul>
                                </li>
                    <?php } ?>
                    <?php if(!$header_permission || ($header_permission->store_setting) !=''){ 
        ?>     
                <li class="dropdown top-menu-item-xs hidden-xs <?php if($this->router->fetch_class()=="storesetting"){echo 'active';}?>">
                    <a href="<?php echo base_url(); ?>storesetting/Storesettinglanding" class="waves-effect dropdown-toggle profile waves-effect waves-light " data-toggle="dropdown" aria-expanded="true"> Store Setting  <i class="fa fa-angle-down m-r-10 text-custom" style="color: #000;font-weight:600"></i></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url();?>storesetting/logo">Logo</a></li>
                        <li><a href="<?php echo base_url();?>storesetting/favicon">Favicon</a></li>
                        <li><a href="<?php echo base_url();?>storesetting/navigation">Navigation</a></li>
                        <!-- <li><a href="<?php //echo base_url();?>storesetting/storeinfo">Store Info</a></li> -->
                        <li><a href="<?php echo base_url();?>storesetting/storepolicies">Store Policies</a></li>
                        <li><a href="<?php echo base_url();?>storesetting/storetheme">Store Theme</a></li>
                        <li><a href="<?php echo base_url();?>storesetting/edittheme">Edit Theme</a></li>
                        <li><a href="<?php echo base_url();?>storesetting/sociallinks">Social Links</a></li>
                        <li><a href="<?php echo base_url();?>storesetting/storetestimonials">Store Testimonials</a></li>
                        <!-- <li><a href="<?php //echo base_url();?>storesetting/custompages">Custom Page</a></li> -->


                    </ul>
                </li>

                
                <?php  
         }
          ?>
          <li class="dropdown top-menu-item-xs hidden-xs <?php if($this->router->fetch_class()=="Appcms"){echo 'active';}?>">
                    <a href="<?php echo base_url(); ?>Appcms/AppCmsLanding" class="waves-effect dropdown-toggle profile waves-effect waves-light " data-toggle="dropdown" aria-expanded="true">App CMS <i class="fa fa-angle-down m-r-10 text-custom" style="color: #000;font-weight:600"></i></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url();?>Appcms/logo">App Logo</a></li>
                        <li><a href="<?php echo base_url();?>Appcms/splashscreen">Splash Logo</a></li>


                    </ul>
                </li>
                <?php 
               // print_r($this->session->userdata("vendorOutlet"));
                $WAREHOUSE= $this->session->userdata("WAREHOUSE");

                
                ?>
                <?php if($this->session->userdata("vendorOutlet")==0){?>
           <li class="dropdown top-menu-item-xs hidden-xs"> 
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                        <?php if(!empty($WAREHOUSE->w_name)){?>
                                          <?=$WAREHOUSE->w_name;?>
                        <?php }else{ echo 'Outlet';} ?>
                                        <i class="fa fa-angle-down m-r-10 text-custom" style="color: #000;font-weight:600"></i></a>
                                 
                                        
                                        <ul class="dropdown-menu" >
                                       
                                       
        <?php 
        
        if(!empty($WAREHOUSE->w_name)){
          echo '<li><a href="'.base_url().'outletchoose/index/">All Outlet </a></li>';
        }
        foreach($ware as $house){  
            
            if($WAREHOUSE->id!=$house->id){

            ?>

    <li><a href="<?=base_url();?>outletchoose/index/<?php echo $house->id; ?>"><?php echo $house->w_name; ?></a></li>                                  
        
        <?php } }  ?>
                                    </ul>
            
                                </li>
                                <?php }else{ ?>
                                    <li class="dropdown top-menu-item-xs hidden-xs"><a href="#" class="waves-effect"> 
                                         
                                    <?php if(!empty($WAREHOUSE->w_name)){?>
                                          <?=$WAREHOUSE->w_name;?>
                        <?php }else{ echo 'Outlet';} ?></a></li>
                           
                                <?php } ?>
                                          <?php if(!$sidebar_permission || ($sidebar_permission->setting) !=''){ 
        ?>  
         <li class="dropdown top-menu-item-xs hidden-xs">
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-cogs" style="font-size: 17px !important;"></i> <i class="fa fa-angle-down m-r-10 text-custom" style="color: #000;font-weight:600"></i></a>
                                    <ul class="dropdown-menu"  >
                                      <li class="dropdown dropdown-submenu"><a href="" class="dropdown-toggle" data-toggle="dropdown">General<i class="fa fa-caret-left pull-right pd-5"></i></a>



                                      
                            <ul class="dropdown-menu dropdown-menu-custom">
                              <li class="custom_li_header"><a href="<?php echo base_url(); ?>Setting/compnaydetails"><i class="fa fa-angle-double-right"></i> Company Details</a></li>
                              <li class="custom_li_header"><a href="<?php echo base_url(); ?>Setting/ordersetting"><i class="fa fa-angle-double-right"></i> Orders Setting</a></li>
                              <li class="custom_li_header"><a href="<?php echo base_url(); ?>Setting/possetting"><i class="fa fa-angle-double-right"></i> POS Setting</a></li>
                              <li class="custom_li_header"><a href="<?php echo base_url(); ?>Setting/customersetting"><i class="fa fa-angle-double-right"></i> Customer Setting</a></li>
                              <li class="custom_li_header"><a href="<?php echo base_url(); ?>Setting/inventorysetting"><i class="fa fa-angle-double-right"></i> Inventory Setting</a></li>
                                <li class="custom_li_header"><a href="<?php echo base_url(); ?>Setting/taxsetting"><i class="fa fa-angle-double-right"></i> Tax Setting</a></li>
                                  <!-- <li class="custom_li_header"><a href="<?php //echo base_url(); ?>Setting/skusetting"><i class="fa fa-angle-double-right"></i> SKU Setting</a></li> -->
                              <li class="custom_li_header"><a href="<?php echo base_url(); ?>Setting/invoicesetting"><i class="fa fa-angle-double-right"></i> Invoice Setting</a></li>
                              <!--<li><a href="#">Supplier Setting</a></li>-->
                              <li class="custom_li_header"><a href="<?php echo base_url(); ?>Setting/storesetting"><i class="fa fa-angle-double-right"></i> Store Setting</a></li>
                              <!--<li><a href="#">Online Store Setting</a></li>-->
                            </ul>
                        </li> 
                          <li><a href="<?php echo base_url(); ?>setting/resetaccountsetting"> <span> Reset Account</span></a></li>
                        <li><a href="<?php echo base_url(); ?>setting/smsemail">SMS & Email</a></li>
                        <!--<li><a href="#">Tax</a></li>-->
                          <!-- <li><a href="<?php //echo base_url(); ?>setting/analytics">Analytics Key</a></li> -->
                                    </ul>
                                </li>
                                 <?php  
         }  
        //  get user profile
        $getProfileImage = $this->db->select('profile_image')->from('houdinv_shop_detail')->get()->row();
        
        if(!empty($getProfileImage->profile_image))
        {
            $setProfileImage = base_url()."upload/vendorprofile/".$getProfileImage->profile_image;
        }
        else
        {
            $setProfileImage = base_url()."assets/images/users/avatar-30.png";
        }
          ?>
                                <li class="dropdown top-menu-item-xs">
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="<?php echo $setProfileImage; ?>" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                       <!--<li><a href="javascript:void(0)"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>-->
                                        <li><a href="<?php echo base_url() ?>Login/logoutVendor"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                                        <?php 
                                        if($this->session->userdata('shop') == 'hawksind_houdinvendordb')
                                        {
                                        ?>
                                        <li><a href="<?php echo base_url() ?>Resetdb"><i class="ti-power-off m-r-10 text-danger"></i> Reset DB</a></li>
                                        <?php }
                                        // get package info
                                        $getPackageData = $getDynamicDb->select('houdin_vendor_auth.houdin_vendor_auth_vendor_id,houdin_users.houdin_users_package_id,houdin_users.houdin_users_package_expiry,houdin_packages.houdin_package_name')
                                        ->from('houdin_vendor_auth')->where('houdin_vendor_auth_token',$this->session->userdata('vendorAuth'))
                                        ->join('houdin_users','houdin_users.houdin_user_id = houdin_vendor_auth.houdin_vendor_auth_vendor_id','left outer')
                                        ->join('houdin_packages','houdin_packages.houdin_package_id = houdin_users.houdin_users_package_id','left outer')->get()->result();
                                        $future = strtotime($getPackageData[0]->houdin_users_package_expiry); //Future date.
                                        $timefromdb = strtotime(date('Y-m-d')); //source time
                                        $timeleft = $future-$timefromdb;
                                        $daysleft = round((($timeleft/24)/60)/60); 
                                        if($daysleft > 0)
                                        {
                                            $daysleft = $daysleft;
                                        }
                                        else
                                        {
                                            $daysleft = 0;
                                        }
                                        ?>
                                        <li class="divider"></li>
                                        <li><a href="javascript:;">Package: <?php if($getPackageData[0]->houdin_package_name) { echo $getPackageData[0]->houdin_package_name; } else { echo "Trial"; } ?></a></li>
                                        <li><a href="javascript:;">Expiry: <?php echo date('d-m-Y',strtotime($getPackageData[0]->houdin_users_package_expiry)) ?>(<?php echo $daysleft ?>) </a></li>
                                        <li><a href="http://3.22.189.222/houdin-e/Shop/" target="_blank">Your Shop </a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
