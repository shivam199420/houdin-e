<?php
class Foregtmodel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }  
    public function sendForgetTokenData($data)
    {
        // check user email and set forget data
        $this->db->select('houdin_admin_user_email')->from('houdin_admin_users')->where('houdin_admin_user_email',$data);
        $getEmailData = $this->db->get()->result();
        if(count($getEmailData) > 0)
        {
            // insert in to forget table
            $getDateTime = strtotime(date('Y-m-d h:i:s'));
            $getTokenData = rand(99999,10000);
            $insertArray = array('houdin_forgot_password_email'=>$getEmailData[0]->houdin_admin_user_email,'houdin_forgot_password_token'=>$getTokenData,'houdin_forgot_password_created_at'=>$getDateTime);
            $getinsertStatus = $this->db->insert('houdin_forgot_password',$insertArray);
            if($getinsertStatus)
            {
                return array('message'=>'success','emaildata'=>$getEmailData[0]->houdin_admin_user_email,'tokendata'=>$getTokenData);
            }
            else
            {
                return array('message'=>'Something went wrong. Please try again');
            }
        }
        else
        {
            return array('message'=>'no');
        }
    }  
    // update admin password data
    public function updateAdminPassword($data)
    {
        $this->db->select('houdin_forgot_password_email')->from('houdin_forgot_password')->where('houdin_forgot_password_token',$data['pin']);
        $getForgetData = $this->db->get()->result();
        if(count($getForgetData) > 0)
        {
            // call helper function
            $passworddata = getHashedPassword($data['password']);
            $getDateData = strtotime(date('Y-m-d h:i:s'));
            $updateArrayData = array('houdin_admin_user_password'=>$passworddata,'houdin_admin_user_updated_at'=>$getDateData);
            $this->db->where('houdin_admin_user_email',$getForgetData[0]->houdin_forgot_password_email);
            $getUpdateData = $this->db->update('houdin_admin_users',$updateArrayData);
            $this->db->where('houdin_forgot_password_email',$getForgetData[0]->houdin_forgot_password_email);
            $this->db->delete('houdin_forgot_password');
            if($getUpdateData == 1)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }
        }
        else
        {
            return array('message'=>'pin');
        }
    }
}
