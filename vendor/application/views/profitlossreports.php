		<?php $this->load->view('Template/header') ?>
		<?php $this->load->view('Template/accountsidebar.php'); 
		$getCurrency = getVendorCurrency();
		if($getCurrency[0]->houdin_users_currency=="USD")
		{
			$currencysymbol= "$";
		}else if($getCurrency[0]->houdin_users_currency=="AUD"){
			$currencysymbol= "$";
		}else if($getCurrency[0]->houdin_users_currency=="Euro"){
			$currencysymbol= "£";
		}else if($getCurrency[0]->houdin_users_currency=="Pound"){
			$currencysymbol= "€";
		}else if($getCurrency[0]->houdin_users_currency=="INR"){
			$currencysymbol= "₹";
		}
		?>
		<style type="text/css">
		.heading_content ul{list-style: none;    padding-left: 0px;}

		display: block;}
		.heading_content>ul>li>ul>li> a{ padding-left: 20px;}
		.heading_content>ul>li>ul>li>ul>li>ul>li a{ padding-left: 40px;}
		.heading_content>ul>li>ul>li>ul>li> a{ padding-left: 30px;}
		.p-l-40{padding-left: 40px;}
		.border-t{border-top: 1px solid #ddd;}
		.border-b{border-bottom: 1px solid #ddd;}
		.text-gray{color: #797979;}
		.heading_content_table ul{list-style: none;}
		.table_custom_profit > thead > tr > th, .table_custom_profit > tbody > tr > th, .table_custom_profit > tfoot > tr > th, .table_custom_profit > thead > tr > td, .table_custom_profit > tbody > tr > td, .table_custom_profit > tfoot > tr > td, .table > thead > tr > th, .table-bordered {
		border-top: 0px solid #ebeff2 !important;
		padding: 6px !important;
		}
			ul.tree, .tree li {
    list-style: none;
    margin:0;
    padding:0;
    cursor: pointer;
}

.tree ul {
  display:none;
}

.tree > li {
  display:block;
  background:#f4f8fb;
  margin-bottom:2px;
}

.tree span {
  display:block;
  padding:10px 12px;

}

.icon {
  display:inline-block;
}

.tree .hasChildren > .expanded {
  background:#f4f8fb;
}

.tree .hasChildren > .expanded a {
  color:#464646;
}

.icon:before {
  content:"+";
  display:inline-block;
  min-width:20px;
  text-align:center;
}
.tree .icon.expanded:before {
  content:"-";
}

.show-effect {
  display:block!important;
}
		</style>
		<div class="content-page">
		<!-- Start content -->
		<div class="content">
		<div class="container">
		<!--START Page-Title -->
		<div class="row">

		<div class="col-md-12">
		<h4 class="page-title">Profit & Loss</h4>
		<ol class="breadcrumb">
		<li><a href="<?php echo base_url() ?>">Home</a></li>
		<li><a href="<?php echo base_url(); ?>Accounts">Accounts</a></li>
		<li class="active">Profit & Loss</li>
		</ol>
		</div>
		</div>
		<div class="row m-t-20">
		<div class="setErrorMessage"></div>
		<div class="col-sm-12">


		<!-- <div class="col-md-4">
		<div class="form-group m-r-10">
		<label for="exampleInputEmail2">Report Period</label>
		<select class="form-control"><optgroup>All Dates</optgroup><option>This Fiancial year to date</option><option>All Dates</option><option>Custom</option><option>Today</option><option>This Week</option><option>Week-to-Date</option></select>
		</div>
		</div> -->
		<div class="col-md-4">
		<div class="form-group m-r-10">
		<label for="exampleInputEmail2">From</label>
		<input type="text" value="<?php echo $from; ?>" class="form-control date_picker dateFromText commonChangeClassData"/>
		</div>
		</div>
		<div class="col-md-4">
		<div class="form-group m-r-10">
		<label for="exampleInputEmail2">To</label>
		<input type="text" value="<?php echo $to; ?>" class="form-control date_picker dateToText commonChangeClassData"/>
		</div>
		</div>
		<div class="col-sm-4"></div>
		</div>
		</div>
		<div class="m-t-40 ">
		<div class="col-md-12 bg-white">
		<!-- <div class="col-md-1"></div> -->
		<div class="col-lg-12 m-t-30">
		<div class="panel panel-default" style="border: 1px solid #ddd;">
		<div class="panel-heading pull-left" style="width: 100%;">
		<h3 class="panel-title pull-left">Profit & Loss</h3>
		<div class="icon-right pull-right">
		<!-- 	<a href="<?php echo base_url(); ?>Accounts/profitlossreport/<?php echo $from ?>/<?php echo $to ?>" class="btn btn-default setPdfUrl"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a> -->

		</div>
		</div>
		<div class="panel-body">
     	<!-- <ul class="tree">

	  
	<li class="tree__item hasChildren" style="margin-top: 40px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Assets</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Fixed asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Non Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>


		</ul>

	</li>
	 
	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Account Receivable and Payable</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>
 
	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Bank and Credit card</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>

 
	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Liabltied and Equity</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>
 
	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Expense and Other expense</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>
 
	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Undeposited fund</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>
 
	<li class="tree__item hasChildren" style="margin-top: 10px;">
		<span>
	        <div class="icon"></div>
			<a href="#">Income and Other income</a>
		</span>

		<ul>
			<li>
				<span>
			        <div class="icon"></div>
					<a href="#">Current asset</a>
				</span>

				<ul>
					<li>
						<span><a href="">Link</a></span>
					</li>
				</ul>
			</li>

			<li>
				<span><a href="#">Fixed asset</a></span>
			</li>

			<li>
				<span><a href="#">Non Current asset</a></span>
			</li>

		</ul>

	</li>
 
	
	<li class="tree__item hasChildren">

		<span>
	        <div class="icon"></div>
			<a href="#">Other Data</a>
		</span>

		<ul>
			<li>
				<span><a href="#">Acting</a></span>
			</li>

			<li>
				<span><a href="#">Biomechanics</a></span>
			</li>

			<li>
				<span><a href="#">Improvisation</a></span>
			</li>

		</ul>

	</li>
 
</ul> -->

		<div class="heading_sheet text-center">
		<?php $getVendorInfo = fetchvendorinfo(); ?>
		<h4><?php echo $getVendorInfo['name']; ?></h4>
		<h5><strong>Custom Summary Report</strong></h5>
		<p class="setDateRange"> <?php echo $from; ?> - <?php echo $to ?></p>
		</div>

		<div class="table-responsive m-t-20">

		<table class="table table_custom_profit">
		<thead>
		<tr class="border-t border-b" style="padding: 10px !important;">
		<th>Date</th>
		<th>Transaction Type</th>
		<th>No.</th>
		<th>Name</th>
		<th>Memo</th>
		<th>Amount</th>
		<th>Balance</th>
		</tr>
		</thead>
		<!-- Assets Data  -->
		<?php 
		$netIncome = 0;
		if(count($currentAssets) > 0 || count($fixedAssets) > 0 || count($non) > 0)
		{
			$calculateFinaldata = 0;
			$setToatlCurrentAssets = 0;
		?>
		<!-- current assets -->
		<tbody class="assetData">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li><a class="text-gray">Assets</a>
		<ul class="">
		<li class="currentAsset"><?php if(count($currentAssets) > 0) {  ?><a class="text-gray">Current assets</a> <?php } ?>
		</li>
		</ul>
		</li>
		</ul>
		</div>
		</td>

		</tr>
		</tbody>
		<?php if(count($currentAssets) > 0) { $setFinalBal = 0; ?>
		<tbody class="creditData assetData currentAsset currentAssetBody">
		<?php 
		for($index = 0; $index < count($currentAssets); $index++)
		{
			for($column = 0; $column < count($currentAssets[$index]['name']); $column++)
			{
				$setData = $currentAssets[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Current Assets</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php } ?>
		<!-- end of current assets -->
		<!-- start of fixed assets -->
		<?php 
		if(count($fixedAssets) > 0)
		{  $setFinalBal = 0;
		?>
		<tbody class="assetData fixedAsset">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li>
		
		<ul class="">
		<li><a class="text-gray">Fixed assets</a>
		</li>
		</ul>
		</li>
		</ul>

		</div>
		</td>

		</tr>
		</tbody>

		<tbody class="creditData assetData fixedAsset fixedAssetBody">
		<?php 
		$setFinalBal = 0;
		$setToatlCurrentAssets = 0;
		for($index = 0; $index < count($fixedAssets); $index++)
		{
			for($column = 0; $column < count($fixedAssets[$index]['name']); $column++)
			{
				$setData = $fixedAssets[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Fixed Assets</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php } ?>
		<!-- end of fixed assets -->
		<!-- non current assest -->
		<?php if(count($non) > 0) { $setFinalBal = 0; ?>
		<tbody class="assetData nonCurrentAsset">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li>
		
		<ul class="">
		<li><a class="text-gray">Non Current assets</a>
		</li>
		</ul>
		</li>
		</ul>

		</div>
		</td>

		</tr>
		</tbody>

		<tbody class="creditData assetData nonCurrentAsset nonCurrentAssetBody">
		<?php 
		for($index = 0; $index < count($non); $index++)
		{
			for($column = 0; $column < count($non[$index]['name']); $column++)
			{
				$setData = $non[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Non Current Assets</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php }?>
		<tbody class="assetData">
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Assets</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome assetFinalAmount"><?php echo $currencysymbol." ".$calculateFinaldata ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php $netIncome = $netIncome+$calculateFinaldata; ?>
		<!-- end non current assets -->
		<?php }
		?>	
		<!-- end main assets -->

		<!-- account receivable and payable start -->
		<!-- Account Receivable -->
		<?php 
		if(count($receiveable) > 0 || count($payable) > 0)
		{
			$calculateFinaldata = 0;
			$setToatlCurrentAssets = 0;
		?>
		<tbody class="receivabalepaybale">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li><a class="text-gray">Account Receivable And Payable</a>
		<ul class="">
		<li class="accountReceivable"><?php if(count($receiveable) > 0) {  ?><a class="text-gray">Account Receivable</a> <?php } ?>
		</li>
		</ul>
		</li>
		</ul>
		</div>
		</td>

		</tr>
		</tbody>
		<?php if(count($receiveable) > 0) { $setFinalBal = 0; ?>
		<tbody class="creditData receivabalepaybale accountReceivable accountReceivableBody">
		<?php 
		for($index = 0; $index < count($receiveable); $index++)
		{
			for($column = 0; $column < count($receiveable[$index]['name']); $column++)
			{
				$setData = $receiveable[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Account Receivable</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php } ?>
		<!-- end account receivable -->
		<!-- start account payable -->
		<?php if(count($payable) > 0)  
		{ $setFinalBal = 0;
		?>
		<tbody class="receivabalepaybale accountPayable">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li>
		
		<ul class="">
		<li><a class="text-gray">Account Payable</a>
		</li>
		</ul>
		</li>
		</ul>

		</div>
		</td>

		</tr>
		</tbody>

		<tbody class="creditData receivabalepaybale accountPayable accountPayableBody">
		<?php 
		for($index = 0; $index < count($payable); $index++)
		{
			for($column = 0; $column < count($payable[$index]['name']); $column++)
			{
				$setData = $payable[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Account Payable</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
			<?php } ?>
			<tbody class="receivabalepaybale">
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Account Receivable & Payable</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome receivablepayableFinalAmount"><?php echo $currencysymbol." ".$calculateFinaldata ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
			<!-- end account payable -->
			<?php $netIncome = $netIncome+$calculateFinaldata; ?>
		<?php } ?>
		<!-- account receivable and payable end -->

		<!-- start bank and credit card -->
			<?php if(count($bank) > 0 || count($credit) > 0) {  
				$calculateFinaldata = 0;
				$setToatlCurrentAssets = 0;
			?>
		<tbody class="bankcredit">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li><a class="text-gray">Bank and Credit Card</a>
		<ul class="">
		<li class="bankAccount"><?php if(count($bank) > 0) {  ?><a class="text-gray">Bank</a> <?php } ?>
		</li>
		</ul>
		</li>
		</ul>
		</div>
		</td>

		</tr>
		</tbody>
		<?php if(count($bank) > 0) { $setFinalBal = 0; ?>
		<tbody class="creditData bankcredit bankAccount bankAccountBody">
		<?php 
		for($index = 0; $index < count($bank); $index++)
		{
			for($column = 0; $column < count($bank[$index]['name']); $column++)
			{
				$setData = $bank[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Bank</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
			<?php } ?>
			<!-- start credit card -->
			<?php if(count($credit) > 0) { $setFinalBal = 0;  ?>
			<tbody class="bankcredit creditAccount">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li>
		
		<ul class="">
		<li><a class="text-gray">Credit Card</a>
		</li>
		</ul>
		</li>
		</ul>

		</div>
		</td>

		</tr>
		</tbody>

		<tbody class="creditData bankcredit creditAccount creditAccountBody">
		<?php 
		for($index = 0; $index < count($credit); $index++)
		{
			for($column = 0; $column < count($credit[$index]['name']); $column++)
			{
				$setData = $credit[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Credit card</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
			<?php } ?>
		<!-- end credit card -->
		<tbody class="bankcredit">
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Bank & Credit Card</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome setFinalPaymentCreditBank"><?php echo $currencysymbol." ".$calculateFinaldata ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php $netIncome = $netIncome+$calculateFinaldata; ?>
			<?php } ?>
		<!-- end bank and credit card -->

		<!-- start Liabilities and Equity  -->
		<?php if(count($liblties) > 0 || count($equity) > 0) { 
			$calculateFinaldata = 0;
			$setToatlCurrentAssets = 0;
			?>
		<tbody class="libaltiesEquity">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li><a class="text-gray">Liabilities and Equity</a>
		<ul class="">
		<li class="libaltiesAccount"><?php if(count($liblties) > 0) {  ?><a class="text-gray">Liabilities</a> <?php } ?>
		</li>
		</ul>
		</li>
		</ul>
		</div>
		</td>

		</tr>
		</tbody>
		<?php if(count($liblties) > 0) { $setFinalBal = 0; ?>
		<tbody class="creditData libaltiesEquity libaltiesAccount libaltiesAccountBody">
		<?php 
		for($index = 0; $index < count($liblties); $index++)
		{
			for($column = 0; $column < count($liblties[$index]['name']); $column++)
			{
				$setData = $liblties[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Liabilities</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php } ?>
		
		<!-- start Equity -->
		<?php if(count($equity) > 0) { $setFinalBal = 0;  ?>
		<tbody class="libaltiesEquity equityAccount">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li>
		
		<ul class="">
		<li><a class="text-gray">Equity</a>
		</li>
		</ul>
		</li>
		</ul>

		</div>
		</td>

		</tr>
		</tbody>

		<tbody class="creditData libaltiesEquity equityAccount equityAccountBody">
		<?php 
		for($index = 0; $index < count($equity); $index++)
		{
			for($column = 0; $column < count($equity[$index]['name']); $column++)
			{
				$setData = $equity[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Equity</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
			<?php } ?>
			<tbody class="libaltiesEquity">
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Liabilities & Equity</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome setfinallibaltiesequity"><?php echo $currencysymbol." ".$calculateFinaldata ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php $netIncome = $netIncome+$calculateFinaldata; ?>
		<!-- end Equity -->
			<?php } ?>
		<!-- end Liabilities and Equity  -->
				
		<!-- start expense and other expense -->
		<?php if(count($expense) > 0 || count($otherExpense) > 0) { 
			$calculateFinaldata = 0;
			$setToatlCurrentAssets = 0;
			?>
		<tbody class="expenseotherexpnse">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li><a class="text-gray">Expense and Other Expense</a>
		<ul class="">
		<li class="expenseAccount"><?php if(count($expense) > 0) {  ?><a class="text-gray">Expense</a> <?php } ?>
		</li>
		</ul>
		</li>
		</ul>
		</div>
		</td>

		</tr>
		</tbody>
		<?php if(count($expense) > 0) { $setFinalBal = 0; ?>
		<tbody class="creditData expenseotherexpnse expenseAccount expenseAccountBody">
		<?php 
		for($index = 0; $index < count($expense); $index++)
		{
			for($column = 0; $column < count($expense[$index]['name']); $column++)
			{
				$setData = $expense[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Expense</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php } ?>
		<!-- start other expense -->
		<?php if(count($otherExpense) > 0) { $setFinalBal = 0; ?>
		<tbody class="expenseotherexpnse otherexpense">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li>
		
		<ul class="">
		<li><a class="text-gray">Other Expense</a>
		</li>
		</ul>
		</li>
		</ul>

		</div>
		</td>

		</tr>
		</tbody>

		<tbody class="creditData expenseotherexpnse otherexpense otherexpenseBody">
		<?php 
		for($index = 0; $index < count($otherExpense); $index++)
		{
			for($column = 0; $column < count($otherExpense[$index]['name']); $column++)
			{
				$setData = $otherExpense[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Other Expense</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
			<?php } ?>
			<tbody class="expenseotherexpnse">
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Expense and Other Expense</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome netIncomeExpenseother"><?php echo $currencysymbol." ".$calculateFinaldata ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php $netIncome = $netIncome+$calculateFinaldata; ?>
		<!-- End other expense -->
		<?php } ?>
		<!-- end expense and other expense -->

		<!-- start undeposited fund -->
		<?php if(count($undeposited) > 0) { 
			$calculateFinaldata = 0;
			$setToatlCurrentAssets = 0;
			?>
		<tbody class="undepositedfund">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li><a class="text-gray">Ubdeposited Fund</a>
		<ul class="">
		<li><?php if(count($undeposited) > 0) {  ?><a class="text-gray">Undeposited Fund</a> <?php } ?>
		</li>
		</ul>
		</li>
		</ul>
		</div>
		</td>

		</tr>
		</tbody>
		<?php if(count($undeposited) > 0) { $setFinalBal = 0; ?>
		<tbody class="creditData undepositedfund undepositedfundBody">
		<?php 
		for($index = 0; $index < count($undeposited); $index++)
		{
			for($column = 0; $column < count($undeposited[$index]['name']); $column++)
			{
				$setData = $undeposited[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Undeposited Fund</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php } ?>
		<tbody class="undepositedfund">
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Undeposited Fund</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome undepositedfundincome"><?php echo $currencysymbol." ".$calculateFinaldata ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php $netIncome = $netIncome+$calculateFinaldata; ?>
			<?php } ?>
		<!-- End undeposited fund -->

		<!-- start income and other income -->
		<?php if(count($income) > 0 || count($otherincome) > 0) { 
			$calculateFinaldata = 0;
			$setToatlCurrentAssets = 0;
			?>
		<tbody class="incomeotherincome">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li><a class="text-gray">Income and Other Income</a>
		<ul class="">
		<li class="incomeAccount"><?php if(count($income) > 0) {  ?><a class="text-gray">Income</a> <?php } ?>
		</li>
		</ul>
		</li>
		</ul>
		</div>
		</td>

		</tr>
		</tbody>
		<?php if(count($income) > 0) { $setFinalBal = 0; ?>
		<tbody class="creditData incomeotherincome incomeAccount incomeAccountBody">
		<?php 
		for($index = 0; $index < count($income); $index++)
		{
			for($column = 0; $column < count($income[$index]['name']); $column++)
			{
				$setData = $income[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Income</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php } ?>
		<!-- start other income -->
		<?php if(count($otherincome) > 0) { $setFinalBal = 0; ?>
		<tbody class="incomeotherincome otherincomeaccount">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li>
		
		<ul class="">
		<li><a class="text-gray">Other Income</a>
		</li>
		</ul>
		</li>
		</ul>

		</div>
		</td>

		</tr>
		</tbody>

		<tbody class="creditData incomeotherincome otherincomeaccount otherincomeaccountBody">
		<?php 
		for($index = 0; $index < count($otherincome); $index++)
		{
			for($column = 0; $column < count($otherincome[$index]['name']); $column++)
			{
				$setData = $otherincome[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Other Income</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
			<?php } ?>
			<tbody class="incomeotherincome">
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Income & Other Income</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome setfinalIncomeOther"><?php echo $currencysymbol." ".$calculateFinaldata ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php $netIncome = $netIncome+$calculateFinaldata; ?>
		<!-- end other income -->
		<?php } ?>

		<!-- end income and other income -->

		<!-- strat other data -->
		<?php if(count($other) > 0) { 
			$calculateFinaldata = 0;			
			$setToatlCurrentAssets = 0;
			?>
		<tbody class="otherdataAccount">
		<tr>
		<td>
		<div class="heading_content">
		<ul class="">
		<li><a class="text-gray">Other</a>
		<ul class="">
		<li><?php if(count($other) > 0) {  ?><a class="text-gray">Other</a> <?php } ?>
		</li>
		</ul>
		</li>
		</ul>
		</div>
		</td>

		</tr>
		</tbody>
		<?php if(count($other) > 0) { $setFinalBal = 0; ?>
		<tbody class="creditData otherdataAccount otherdataAccountBody">
		<?php 
		for($index = 0; $index < count($other); $index++)
		{
			for($column = 0; $column < count($other[$index]['name']); $column++)
			{
				$setData = $other[$index]['name'][$column];
				if($setData->houdinv_accounts_balance_sheet__increase)
				{
					$setFinalBal = $setFinalBal+$setData->houdinv_accounts_balance_sheet__increase;
					$setIncome = $setData->houdinv_accounts_balance_sheet__increase;
				}
				else
				{
					$setFinalBal = $setFinalBal-$setData->houdinv_accounts_balance_sheet_decrease;
					$setIncome = "-".$setData->houdinv_accounts_balance_sheet_decrease;
				}
			?>
			<tr>
		<td>
		<div class="heading_content_table">
		<ul>
		<li><a class="text-gray"><?php echo date('d-m-Y',strtotime($setData->houdinv_accounts_balance_sheet_date)) ?></a></li>
		</ul>
		</div></td>
		<td><?php echo $setData->houdinv_accounts_balance_sheet_ref_type ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_reference) { echo $setData->houdinv_accounts_balance_sheet_reference; } else { echo  "--"; } ?></td>
		<td><?php if($setData->houdinv_user_name) { echo $setData->houdinv_user_name; } else { echo "--"; } ?></td>
		<td><?php if($setData->houdinv_accounts_balance_sheet_memo) { echo $setData->houdinv_accounts_balance_sheet_memo; } else { echo "--"; } ?></td>
		<td><?php echo $currencysymbol." ".$setIncome; ?></td>
		<td><?php echo $currencysymbol." ".$setFinalBal;	 ?></td>
		</tr>
			<?php } } $calculateFinaldata = $calculateFinaldata+$setFinalBal; ?>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Other</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome"><?php echo $currencysymbol." ".$setFinalBal ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php } ?>
		<tbody class="otherdataAccount">
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Total for Other</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome finalOtherAccount"><?php echo $currencysymbol." ".$calculateFinaldata ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		<?php $netIncome = $netIncome+$calculateFinaldata; ?>
			<?php } ?>
		<!-- End other data -->
			<!-- net income -->
			<tbody>
		<tr class="border-b border-t">
		<td colspan="6">
		<div class="heading_content">
		<ul>
		<li><a class="text-gray"><strong>Net Income</strong></a></li>
		</ul>
		</div>
		</td>
		<td><strong class="netIncome setfinalIncomeData"><?php echo $currencysymbol." ".$netIncome ?></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		</table>

		</div>

		</div>
		</div>
		</div>
		<div class="col-md-1"></div>
		</div>
		</div>
		</div>
		</div>
		<?php $this->load->view('Template/footer') ?>
		<script type="text/javascript">
		$(document).ready(function(){
			var setBaseData = "<?php echo base_url(); ?>";
			$(document).on('change','.commonChangeClassData',function(){
				$('.setErrorMessage').html('');
				var fromdate = $('.dateFromText').val();
				var toDate = $('.dateToText').val();
				var setPDFUrl = setBaseData+"Accounts/profitlossreport/"+fromdate+"/"+toDate;
				$('.setPdfUrl').attr('href',setPDFUrl);
				jQuery.ajax({
                type: "POST",
                url: setBaseData+"Accounts/updateProfitlosssdetailReport",
                data: {"fromdate": fromdate,"toDate": toDate},
                success: function(data) {
					var getFinalData = $.parseJSON(data);
					 if(getFinalData.message == 'Success')
					 {
						 var rangeDate = getFinalData.from+" - "+getFinalData.to;
						 $('.setDateRange').text('').text(rangeDate);
						 console.log(getFinalData);
						 var currencysymbol = '<?php echo $currencysymbol ?>';
						 var netIncome = 0;
						//  start asset data
						 if(getFinalData.currentAssets.length > 0 || getFinalData.fixedAssets.length > 0 || getFinalData.non.length > 0)
						 {
							var calculateFinaldata = 0;
							$('.assetData').show();
							//Start current assets
							if(getFinalData.currentAssets.length > 0)
							{
								var setFinalBal = 0;
								$('.currentAsset').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.currentAssets.length; index++)
								{
									var getMainIndex = getFinalData.currentAssets[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Current Assets</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.currentAssetBody').html('').html(setHtmldata);
							}
							else
							{
								$('.currentAsset').hide();
							}
							// end current asset
							// start fixed asset
							console.log('shivam');
							console.log(getFinalData.fixedAssets.length);
							if(getFinalData.fixedAssets.length > 0)
							{
								var setFinalBal = 0;
								$('.fixedAsset').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.fixedAssets.length; index++)
								{
									var getMainIndex = getFinalData.fixedAssets[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Fixed Assets</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.fixedAssetBody').html('').html(setHtmldata);
							}
							else
							{
								$('.fixedAsset').hide();
							}
							// end fixed asset
							// start non current asset
							if(getFinalData.non.length > 0)
							{
								var setFinalBal = 0;
								$('.nonCurrentAsset').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.non.length; index++)
								{
									var getMainIndex = getFinalData.non[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Non Current Assets</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.nonCurrentAssetBody').html('').html(setHtmldata);
							}
							else
							{
								$('.nonCurrentAsset').hide();
							}
							// end non current asset
							var finalAmount = currencysymbol+''+calculateFinaldata;
							$('.assetFinalAmount').text('').text(finalAmount);
							netIncome = netIncome+parseFloat(calculateFinaldata);
							// final calculation start
							// end final calculation start
						 }
						 else
						 {
							$('.assetData').hide();
						 }
						//  end assets data

						// */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//*/*/*/*/*/*/ */
						// start account receivable and payable
						if(getFinalData.receiveable.length > 0 || getFinalData.payable.length > 0)
						{
							var calculateFinaldata = 0;
							$('.receivabalepaybale').show();
							//Start account Receivable
							if(getFinalData.receiveable.length > 0)
							{
								var setFinalBal = 0;
								$('.accountReceivable').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.receiveable.length; index++)
								{
									var getMainIndex = getFinalData.receiveable[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Receivable Account</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.accountReceivableBody').html('').html(setHtmldata);
							}
							else
							{
								$('.accountReceivable').hide();
							}
							// end Account Receivable

							// start Account Payable
							if(getFinalData.payable.length > 0)
							{
								var setFinalBal = 0;
								$('.accountPayable').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.payable.length; index++)
								{
									var getMainIndex = getFinalData.payable[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Payable Account</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.accountPayableBody').html('').html(setHtmldata);
							}
							else
							{
								$('.accountPayable').hide();
							}
							// end Account Payable
							var finalAmount = currencysymbol+''+calculateFinaldata;
							$('.receivablepayableFinalAmount').text('').text(finalAmount);
							netIncome = netIncome+parseFloat(calculateFinaldata);
							// final calculation start
							// end final calculation start
						 }
						 else
						 {
							$('.receivabalepaybale').hide();
						 }
						// end account receivable and payable
						// */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//*/*/*/*/*/*/ */
						// start bank and credit
						if(getFinalData.bank.length > 0 || getFinalData.credit.length > 0)
						{
							var calculateFinaldata = 0;
							$('.bankcredit').show();
							//Start bank account
							if(getFinalData.bank.length > 0)
							{
								var setFinalBal = 0;
								$('.bankAccount').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.bank.length; index++)
								{
									var getMainIndex = getFinalData.bank[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Bank Account</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.bankAccountBody').html('').html(setHtmldata);
							}
							else
							{
								$('.bankAccount').hide();
							}
							// end bank account

							// start credit account
							if(getFinalData.credit.length > 0)
							{
								var setFinalBal = 0;
								$('.creditAccount').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.credit.length; index++)
								{
									var getMainIndex = getFinalData.credit[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Credit Card</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.creditAccountBody').html('').html(setHtmldata);
							}
							else
							{
								$('.creditAccount').hide();
							}
							// end Credit Account
							var finalAmount = currencysymbol+''+calculateFinaldata;
							$('.setFinalPaymentCreditBank').text('').text(finalAmount);
							netIncome = netIncome+parseFloat(calculateFinaldata);
							// final calculation start
							// end final calculation start
						 }
						 else
						 {
							$('.bankcredit').hide();
						 }

						// end bank and credit
						// */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//*/*/*/*/*/*/ */
						// start libalties and equity
						if(getFinalData.liblties.length > 0 || getFinalData.equity.length > 0)
						{
							var calculateFinaldata = 0;
							$('.libaltiesEquity').show();
							//Start libalties account
							if(getFinalData.liblties.length > 0)
							{
								var setFinalBal = 0;
								$('.libaltiesAccount').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.liblties.length; index++)
								{
									var getMainIndex = getFinalData.liblties[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Libalties Account</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.libaltiesAccountBody').html('').html(setHtmldata);
							}
							else
							{
								$('.libaltiesAccount').hide();
							}
							// end libalties account

							// start equity account
							if(getFinalData.equity.length > 0)
							{
								var setFinalBal = 0;
								$('.equityAccount').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.credit.length; index++)
								{
									var getMainIndex = getFinalData.credit[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Equity Account</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.equityAccountBody').html('').html(setHtmldata);
							}
							else
							{
								$('.equityAccount').hide();
							}
							// end Equity Account
							var finalAmount = currencysymbol+''+calculateFinaldata;
							$('.setfinallibaltiesequity').text('').text(finalAmount);
							netIncome = netIncome+parseFloat(calculateFinaldata);
							// final calculation start
							// end final calculation start
						 }
						 else
						 {
							$('.libaltiesEquity').hide();
						 }
						// end libalties and equity
						// */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//*/*/*/*/*/*/ */
						// start expense and other expense
						if(getFinalData.expense.length > 0 || getFinalData.otherExpense.length > 0)
						{
							var calculateFinaldata = 0;
							$('.expenseotherexpnse').show();
							//Start expense account
							if(getFinalData.expense.length > 0)
							{
								var setFinalBal = 0;
								$('.expenseAccount').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.expense.length; index++)
								{
									var getMainIndex = getFinalData.expense[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Expense</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.expenseAccountBody').html('').html(setHtmldata);
							}
							else
							{
								$('.expenseAccount').hide();
							}
							// end expense account

							// start other expense account
							if(getFinalData.otherExpense.length > 0)
							{
								var setFinalBal = 0;
								$('.otherexpense').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.credit.length; index++)
								{
									var getMainIndex = getFinalData.credit[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Other Expense</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.otherexpenseBody').html('').html(setHtmldata);
							}
							else
							{
								$('.otherexpense').hide();
							}
							// end other expense Account
							var finalAmount = currencysymbol+''+calculateFinaldata;
							$('.netIncomeExpenseother').text('').text(finalAmount);
							netIncome = netIncome+parseFloat(calculateFinaldata);
							// final calculation start
							// end final calculation start
						 }
						 else
						 {
							$('.expenseotherexpnse').hide();
						 }
						// end expense and other expense
						// */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//*/*/*/*/*/*/ */
						// start undeposited fund
						if(getFinalData.undeposited.length > 0)
						{
							var calculateFinaldata = 0;
							$('.undepositedfund').show();
							//Start expense account
							if(getFinalData.undeposited.length > 0)
							{
								var setFinalBal = 0;
								$('.undepositedfund').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.undeposited.length; index++)
								{
									var getMainIndex = getFinalData.undeposited[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Undeposited Fund</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.undepositedfundBody').html('').html(setHtmldata);
							}
							else
							{
								$('.undepositedfund').hide();
							}
							// end expense account
							var finalAmount = currencysymbol+''+calculateFinaldata;
							$('.undepositedfundincome').text('').text(finalAmount);
							netIncome = netIncome+parseFloat(calculateFinaldata);
							// final calculation start
							// end final calculation start
						 }
						 else
						 {
							$('.undepositedfund').hide();
						 }
						// end undeposited fund
						// */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//*/*/*/*/*/*/ */
						// start income and other income
						if(getFinalData.income.length > 0 || getFinalData.otherincome.length > 0)
						{
							var calculateFinaldata = 0;
							$('.incomeotherincome').show();
							//Start income account
							if(getFinalData.income.length > 0)
							{
								var setFinalBal = 0;
								$('.incomeAccount').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.income.length; index++)
								{
									var getMainIndex = getFinalData.income[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Income Account</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.incomeAccountBody').html('').html(setHtmldata);
							}
							else
							{
								$('.incomeAccount').hide();
							}
							// end income account

							// start other income account
							if(getFinalData.otherincome.length > 0)
							{
								var setFinalBal = 0;
								$('.otherincomeaccount').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.otherincome.length; index++)
								{
									var getMainIndex = getFinalData.otherincome[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Other Income Account</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.otherincomeaccountBody').html('').html(setHtmldata);
							}
							else
							{
								$('.otherincomeaccount').hide();
							}
							// end other income Account
							var finalAmount = currencysymbol+''+calculateFinaldata;
							$('.setfinalIncomeOther').text('').text(finalAmount);
							netIncome = netIncome+parseFloat(calculateFinaldata);
							// final calculation start
							// end final calculation start
						 }
						 else
						 {
							$('.incomeotherincome').hide();
						 }

						// end income and other income
						// */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//*/*/*/*/*/*/ */
						// start other account
						if(getFinalData.other.length > 0)
						{
							var calculateFinaldata = 0;
							$('.otherdataAccount').show();
							//Start other account
							if(getFinalData.other.length > 0)
							{
								var setFinalBal = 0;
								$('.otherdataAccount').show();
								var setHtmldata = '';
								for(var index = 0; index < getFinalData.other.length; index++)
								{
									var getMainIndex = getFinalData.other[index].name;
									for(var column = 0; column < getMainIndex.length; column++)
									{
										// balance sheet ref
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_reference'])
										{
											var setRefrence = getMainIndex[column]['houdinv_accounts_balance_sheet_reference'];
										}
										else
										{
											var setRefrence = '--';
										}
										// set user name
										if(getMainIndex[column]['houdinv_user_name'])
										{
											var setUsername = getMainIndex[column]['houdinv_user_name'];
										}
										else
										{
											var setUsername = '';
										}
										// set memo
										if(getMainIndex[column]['houdinv_accounts_balance_sheet_memo'])
										{
											var setMemo = getMainIndex[column]['houdinv_accounts_balance_sheet_memo'];
										}
										else
										{
											var setMemo = '';
										}
										// set income and final bal
										if(getMainIndex[column]['houdinv_accounts_balance_sheet__increase'])
										{
											setFinalBal = setFinalBal+parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet__increase']);
											var setIncome = getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										else
										{
											setFinalBal = setFinalBal-parseFloat(getMainIndex[column]['houdinv_accounts_balance_sheet_decrease']);
											var setIncome = "-"+getMainIndex[column]['houdinv_accounts_balance_sheet__increase'];
										}
										setHtmldata +='<tr>'
										+'<td><div class="heading_content_table"><ul>'
										+'<li><a class="text-gray">'+getMainIndex[column]['houdinv_accounts_balance_sheet_date']+'</a></li></ul></div></td>'
										+'<td>'+getMainIndex[column]['houdinv_accounts_balance_sheet_ref_type']+'</td>'
										+'<td>'+setRefrence+'</td>'
										+'<td>'+setUsername+'</td>'
										+'<td>'+setMemo+'</td>'
										+'<td>'+currencysymbol+' '+setIncome+'</td>'
										+'<td>'+currencysymbol+' '+setFinalBal+'</td>'
										+'</tr>'
									}
								}
								calculateFinaldata = calculateFinaldata+parseFloat(setFinalBal); 
								setHtmldata +='<tr class="border-b border-t">'
								+'<td colspan="6"><div class="heading_content"><ul><li><a class="text-gray"><strong>Total for Other Account</strong></a></li></ul></div></td>'
								+'<td><strong class="netIncome">'+currencysymbol+' '+setFinalBal+'</strong></td>'
								+'<td>&nbsp;</td>'
								+'</tr>'
								$('.otherdataAccountBody').html('').html(setHtmldata);
							}
							else
							{
								$('.otherdataAccount').hide();
							}
							// end other account
							var finalAmount = currencysymbol+''+calculateFinaldata;
							$('.finalOtherAccount').text('').text(finalAmount);
							netIncome = netIncome+parseFloat(calculateFinaldata);
							// final calculation start
							// end final calculation start
						 }
						 else
						 {
							$('.otherdataAccount').hide();
						 }

						// end other account
						// */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*//*/*/*/*/*/*/ */
						// set final account
						var setString = currencysymbol+''+netIncome;
						$('.setfinalIncomeData').text('').text(setString);
					 }
					 else if(getFinalData.message == 'validation')
					 {
						 $('.setErrorMessage').html('').html('<div class="alert alert-danger">'+getFinalData.data+'</div>');
					 }
					 else
					 {
						$('.setErrorMessage').html('').html('<div class="alert alert-danger">Something went wrong. Please try again</div>');
					 }
                }
                });
			})

		})
		</script>
<script type="text/javascript">
	$('.tree .icon').click( function() {
  $(this).parent().toggleClass('expanded').
  closest('li').find('ul:first').
  toggleClass('show-effect');
});
</script>