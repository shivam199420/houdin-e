        <?php $this->load->view('Template/header') ?>
        <?php $this->load->view('Template/possidebar') ?>
        <style type="text/css">
        .radio.radio-inline {
        margin-top: 0;
        margin-left: 0px;
        }
        </style> 
        <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container"> 
        <!--START Page-Title -->
        <div class="row">

        <div class="col-md-8">
        <h4 class="page-title">POS</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <!--<li><a href="javascript:void(0);">POS</a></li>-->
        <li class="active">POS</li>
        </ol>
        </div>

        </div>
        <!--END Page-Title --> 

        <div class="row" style="margin-right: 10px">
        <div class="hidden-print">
        <div class="pull-right">
        <a href="<?php echo base_url();?>POS/printinvoice" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i> Print</a>
        <a href="#" class="btn btn-primary waves-effect waves-light">Save</a>
        </div>
        </div>
        </div>


        <div class="row m-t-20">
        <div class="col-md-12"> 
        <div class="card-box row"> 
        <div class="col-md-8">
        <div class="row m-b-15">
        <div class="">
        <div class="form-group">
        <label class="col-md-3 control-label">Delivery Option</label>
        <div class="col-md-9">
        <div class="radio radio-inline" style="padding-left: 0px!important">
        <input type="radio" id="inlineRadio1" value="option1" name="radioInline" checked="">
        <label for="inlineRadio1"> Home Delivery </label>
        </div>

        <div class="radio radio-inline">
        <input type="radio" id="inlineRadio2" value="option2" name="radioInline">
        <label for="inlineRadio2"> Pick up by customer </label>
        </div>
        </div>
        </div>

        <!--<div class="form-group">

        <div class="col-md-9 m-t-10">
        <input type="text" value="" class="form-control" name="" placeholder="Address">
        </div>
        </div>-->

        </div>
        </div>
        <form method="">
        <div class="row form-horizontal">


        <div class="form-group">
        <label class="col-md-3 control-label">Coupon Disc</label>
        <div class="col-md-9">
        <input type="text" value="" class="form-control" name="" placeholder="">
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-3 control-label">Customer Name</label>
        <div class="col-md-9">
        <input type="text" value="" class="form-control" name="" placeholder="">
        </div>


        </div>

        <div class="form-group">
        <label class="col-md-3 control-label">Contact No.</label>
        <div class="col-md-9">
        <input type="text" value="" class="form-control" name="" placeholder="">
        </div>
        </div>


        </div>
        </form>
        <div class="row">
        <div class="col-md-12">
        <div class="table-responsive">
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr> 
        <th style="width: 30%;">Product Detail</th>
        <th>MRP</th>
        <th>SP</th>
        <th>Qty</th>
        <th>Total</th>
        </tr>
        </thead>
        <tbody class="setRowData">
        <tr class="mainParentRow">
        <td>
            <div class="form-group" style="margin-bottom: 0px!important;">
            <select class="form-control setproductData select1">
            <option value="">Choose Product</option>
            <?php 
                foreach($productData as $productDataList)
                {
            ?>
                <option data-price="<?php echo $productDataList['productPrice'] ?>" data-stock="<?php echo $productDataList['stock'] ?>" data-variant="<?php echo $productDataList['variantId'] ?>" data-product="<?php echo $productDataList['produtcId'] ?>"><?php echo $productDataList['productName'] ?></option>
            <?php 
            }
            ?>
        </select>
        </div>
    </td>
        <td class="setMainPrice"></td>
        <td><input type="text" value="" class="form-control specialPrice number_validation" name="" placeholder=""></td>
        <td><input type="text" value="" class="form-control getQuantity number_validation" name="" placeholder=""></td>
        <td class="setFinalPrice">0</td>
        </tr>
        </tbody>
        </table>

        </div>
        </div>
        </div>
        <div class="form-group"> 
        <label for="field-1" class="control-label">Note</label>
        <textarea class="form-control"></textarea> 

        </div> 

        </div>
        <div class="col-md-4 m-t-10">
        <form method="">
        <div class="row form-horizontal"> 
        <div class="text-left text_black">
        <div class="col-md-5 m-t-15"><b>Total:</b></div><div class="col-md-7 m-t-15">2930.00</div>
        <div class="col-md-5 m-t-15"><b>Coupon:</b></div><div class="col-md-7 m-t-15">2930.00</div>
        <div class="col-md-5 m-t-15"><b>Net Payable:</b></div><div class="col-md-7 m-t-15">2930.00</div>

        </div>


        </div>

        <div class="m-t-10">

        <p class="m-b-15" style="color:#000"><strong>Payment Options</strong></p>
        <div class="radio radio-inline" style="display: contents!important">
        <input type="radio" id="inlineRadio1" value="option1" name="radioInline" checked="">
        <label for="inlineRadio1"> Cash </label>
        </div>
        <div class="radio radio-inline" style="display: contents!important">
        <input type="radio" id="inlineRadio2" value="option2" name="radioInline">
        <label for="inlineRadio2"> Card </label>
        </div>
        <div class="radio radio-inline" style="display: contents!important">
        <input type="radio" id="inlineRadio2" value="option2" name="radioInline">
        <label for="inlineRadio2"> Cheque </label>
        </div>
        <div class="radio radio-inline" style="display: contents!important">
        <input type="radio" id="inlineRadio2" value="option2" name="radioInline">
        <label for="inlineRadio2"> Wallet </label>
        </div>
        </div>
        <div class="form-group m-t-15">
        <label>Additional adjustment</label>
        <input class="checked" type="checkbox" name="">
        </div>  
        <div class="form-group">
        <input type="text" value="" class="form-control" name="" placeholder="Amount paid or amount pending">
        </div>
        </form>


        </div>
        </div>
        </div>
        </div>  
        <?php $this->load->view('Template/footer') ?>
        <script type="text/javascript">
        $(document).ready(function(){
            $(".select1").select2();
            // Select serach function 
            // set product data
            $(document).on('change','.setproductData',function(){
                $(this).parents('.mainParentRow').find('.setMainPrice').text('').text($(this).children('option:selected').attr('data-price'));
                $(this).parents('.mainParentRow').find('.specialPrice').val('').val($(this).children('option:selected').attr('data-price'));
                $(this).parents('.mainParentRow').find('.getQuantity').attr('data-max',$(this).children('option:selected').attr('data-stock'));
                // Call final price function
                var quantity = $(this).parents('.mainParentRow').find('.getQuantity').val();
                var getSpecialPriceData = $(this).parents('.mainParentRow').find('.specialPrice').val();
                var dataSet = $(this);
                finalPrice(quantity,getSpecialPriceData,dataSet); 
            });
            // get quantity
            $(document).on('keyup','.getQuantity',function(e){
                var getSpecialPriceData = $(this).parents('.mainParentRow').find('.specialPrice').val();
                if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
                {
                    var quantity = $(this).attr('data-max');
                    $(this).val($(this).attr('data-max'));
                }
                else
                {
                    var quantity = $(this).val();
                }
                var dataSet = $(this);
                finalPrice(quantity,getSpecialPriceData,dataSet); // Call final price function
                // check enter is pressed or not on quantity
                if(e.which == 13){
                    var setText = 'add';
                    var dataset = $(this);
                    sethtmldata(setText)
                }
            });
            // get special price
            $(document).on('keyup','.specialPrice',function(){
                var getSpecialPriceData = $(this).val();
                var quantity = $(this).parents('.mainParentRow').find('.getQuantity').val();
                var dataSet = $(this);
                finalPrice(quantity,getSpecialPriceData,dataSet);// Call final price function
            })
            // final price function 
            function finalPrice(quantity,specialprice,dataSet)
            {
                console.log(quantity+"::"+specialprice+"::"+dataSet);
                if(specialprice)
                {
                    if(quantity)
                    {
                        var setQuantity = quantity;
                    }
                    else
                    {
                        setQuantity = 0;
                        dataSet.val(setQuantity);
                    }
                    var setFinalPrice = (parseFloat(specialprice)*parseFloat(setQuantity))
                    dataSet.parents('.mainParentRow').find('.setFinalPrice').text('').text(setFinalPrice);
                    dataSet.parents('.mainParentRow').find('.specialPrice').css('border','1px solid #d3d3d3');
                }
                else
                {
                    dataSet.val(0);
                    dataSet.parents('.mainParentRow').find('.specialPrice').css('border','1px solid red');
                    dataSet.parents('.mainParentRow').find('.setFinalPrice').text('0');
                }
            }
            // add html or remove html
            function sethtmldata(setText,dataset)
            {
                if(setText == 'add')
                {
                    var countRows = ($('.mainParentRow').length)+1;
                    alert(countRows);
                    $('.setRowData').append('<tr class="mainParentRow"><td><div class="form-group" style="margin-bottom: 0px!important;"><select class="form-control setproductData select'+countRows+'><option value="">Choose Product</option><?php foreach($productData as $productDataList){?><option data-price="<?php echo $productDataList['productPrice'] ?>" data-stock="<?php echo $productDataList['stock'] ?>" data-variant="<?php echo $productDataList['variantId'] ?>" data-product="<?php echo $productDataList['produtcId'] ?>"><?php echo $productDataList['productName'] ?></option><?php } ?></select></div></td><td class="setMainPrice"></td><td><input type="text" value="" class="form-control specialPrice number_validation" name="" placeholder=""></td><td><input type="text" value="" class="form-control getQuantity number_validation" name="" placeholder=""></td><td class="setFinalPrice">0</td></tr>');
                    $(".select"+countRows+"").select2();
                }
            }
        })
        </script>

