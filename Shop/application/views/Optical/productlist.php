<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view("".$getShopName."/Template/header"); ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>

   <!-- About Banner Start -->

   <section id="about">

       <div class="container">

           <div class="row">

               <div class="about-heading text-center">

                   <h2>Products</h2>

               </div>

           </div>

       </div>

   </section>

   <!-- About Banner End -->



   <!-- Product-item start -->

   <section id="product-grid-sidebar">

       <div class="container">

           <div class="row">

               <div class="col-md-12">
                 <?php
                 if($this->session->flashdata('error'))
                 {
                         echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                 }
                 if($this->session->flashdata('success'))
                 {
                         echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                 }
                 ?>
                  <div class="pro-grid-sidebar col-md-12">
                    <?php
                    $count_product=count($productList);
                    if($count_product==0)
                    {?>
                      <div class="container"><div class="col-sm-12 text-center"><h3>Products are not available</h3></div></div>
                    <?php } else { ?>

                       <div class="col-md-12 col-sm-12">

                           <div class="sort text-right">

                             <span style="float: right;"><?php echo form_open(base_url( 'Productlist/'.$this->uri->segment('2').'/'.$this->uri->segment('3').'/'.$this->uri->segment('4')), array( 'id' => 'Productlist', 'method'=>'get' ));?>
                               <label>Sort by</label>
                               <select class="sort-select" name="sort">

                                 <option value="">select</option>
                                 <option value="name" <?php if($_REQUEST['sort'] =="name") { echo "selected"; }?> >Name Ascending</option>
                                 <option value="date" <?php if($_REQUEST['sort'] =="date") { echo "selected"; }?>>Date Descending</option>
                                 <option value="price" <?php if($_REQUEST['sort'] =="pricehl") { echo "selected"; }?>>Price High to Low</option>
                                 <option value="price" <?php if($_REQUEST['sort'] =="pricelh") { echo "selected"; }?>>Price Low to High</option>

                               </select>
                               <?php echo form_close(); ?></span>
                                <label class="line31 float-left">Showing: <?php echo $count; ?></label>


                           </div>

                       </div>
                     <?php } ?>

                   </div>

                   <div class="latest-main">

                       <div class="product-grid-item appendProductList">

                          <!-- Start product list  -->
                          <?php
                          foreach($productList as $productListData)
                          {
                              $getProductImage = json_decode($productListData->houdinv_products_main_images,true);
                              $getProductPrice = json_decode($productListData->houdin_products_price,true);
                          ?>
                           <div class="gallery_product col-md-3 col-sm-3">

                                <div class="grid-product">
                                  <div class="image bg-lightblue">

                                    <a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>">
                                    <?php 
                                  if($getProductImage[0])
                                  {
                                      $mainImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                                  }
                                  else
                                  {
                                      $mainImage = base_url()."Extra/noPhotoFound.png";
                                  }
                                  ?>
                                      <img src="<?php echo $mainImage; ?>" alt="featured-product-img" class="img-responsive"></a>
                                      <?php if($productListData->houdinv_products_total_stocks <= 0) { ?>
                                      <!--<div class="new_badge">Out of Stock</div>-->
                                      <?php } ?>
                                       <?php
                                      if($productListData->houdinv_products_main_quotation == 1)
                                      {
                                      ?>
                                      <div class="model_qute_show ask-question" data-id="<?php echo $productListData->houdin_products_id ?>"  data-toggle="modal" >Ask Quotation
                                      </div>
                                      <?php
                                      }
                                      ?>
                                    <div class="overlay2 text-center">

                                        <a href="javascript:;" class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-heart"></i></a>

                                        <a href="<?php echo base_url() ?>Productlistview/<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-info"></i></a>
                                        <?php
                                        if($productListData->houdinv_products_total_stocks <= 0)
                                        {
                                        if($storeSetting[0]->receieve_order_out == 1)
                                        {
                                            ?>
                                            <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-shopping-cart"></i></a>
                                        <?php  }
                                        }
                                        else
                                        {
                                        ?>
                                            <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-shopping-cart"></i></a>
                                        <?php }
                                        ?>
                                        <!-- <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $productListData->houdin_products_id ?>"><i class="fa fa-shopping-cart"></i></a> -->

                                    </div>

                                </div>
                                </div>

                                <div class="feat-details feat-details2">

                                    <p><?php echo substr($productListData->houdin_products_title,0, 25).".." ; ?></p><span><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $productListData->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $productListData->houdin_products_final_price; ?> <?php  } ?></span>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                          <?php } ?>
                       </div>

                   </div>

                   <div class="clearfix"></div>

                   <div class="load-more">

                       <div class="pages pages2 text-center">
                         <?php
                         if($count > 0)
                         {
                             if($count >= 12)
                             {
                                 $remain = $count-12;
                             }
                             else
                             {
                                 $remain = 0;
                             }
                         ?>
                         <div class="row">

                         <?php
                         if($remain > 0)
                         {
                         ?>
                         <div class="col-sm-12"><button type="button" data-remain="<?php echo $remain ?>" data-last="12" class="btn btn-sm  acc_btn setLoadMoredata load-more" style="width:100%;margin-top: 0px;padding: 4px;border-radius: 4px;">Load More&nbsp;<i class="fa fa-spinner fa-spin setSpinnerData" style="display:none;"></i></button></div>
                         <?php }
                         ?>
                         </div>
                         <?php }
                         ?>
                       </div>

                   </div>

               </div>

           </div>

       </div>

   </section>

  <!-- Product-item end -->


   <!-- Footer Part Start -->
   <?php
   $getShopName = getfolderName($this->session->userdata('shopName'));
   $this->load->view("".$getShopName."/Template/footer"); ?>

   <script>
   $('.sort-select').on('change',function()
   {
     document.getElementById('Productlist').submit();
   });
   </script>
   <script type="text/javascript">
   $(document).ready(function(){

       $(document).on('click','.setLoadMoredata',function(){

           $(this).prop('disabled',true);
           $('.setSpinnerData').show();
           var getCategory = '<?php echo $this->uri->segment('2') ?>';
           var getSubCategory = '<?php echo  $this->uri->segment('3') ?>';
           var getSubSubCategory = '<?php echo $this->uri->segment('4') ?>';
           var sortBy = '<?php echo $_REQUEST['sort'] ?>';
           var dataLast = $(this).attr('data-last');
           var dataRemain = $(this).attr('data-remain');
           $.ajax({
           type: "POST",
           url: "<?php echo base_url() ?>Ajaxcontroller/fetchProductPaginationData",
           data: {"dataLast": dataLast,"dataRemain":dataRemain,"getCategory":getCategory,"getSubCategory":getSubCategory,"getSubSubCategory":getSubSubCategory,"sortBy":sortBy},
           success: function(datas) {
               $('.setSpinnerData').hide();
               $('.setLoadMoredata').prop('disabled',false);
               var setData = jQuery.parseJSON(datas);

                  var grid = "";
               for(var i=0; i < setData.main.length ; i++)
               {
                   var $productListData = setData.main[i];
                   var textsplit = $productListData.houdin_products_title;
                   var $setCartDesign = "";
                     var $setStockBadge = "";
                     var $settingData = setData.settingData;
                       if($productListData.houdinv_products_total_stocks <= 0)
                           {
                               if($settingData[0].receieve_order_out == 1)
                               {

                                   $setCartDesign = '<a class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><i class="fa fa-shopping-cart"></i></a>';
                               }
                           }
                           else
                           {
                               $setCartDesign = '<a class="Add_to_cart_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><i class="fa fa-shopping-cart"></i></a>';
                           }
                           if($productListData.houdinv_products_total_stocks <= 0)
                           {
                               $setStockBadge = '<div class="new_badge">Out of Stock</div>';
                           }

                     var   $getProductImage = jQuery.parseJSON($productListData.houdinv_products_main_images);
                      var     $getProductPrice = jQuery.parseJSON($productListData.houdin_products_price);
                         console.log($getProductPrice);
                           if($getProductPrice.discount != 0 && $getProductPrice.discount  != "")
                           {
                              var $DiscountedPrice = $getProductPrice.price-$getProductPrice.discount;
                              var $originalPrice = $getProductPrice.price;
                           }
                           else
                           {
                             var  $DiscountedPrice = 0;
                             var  $originalPrice = $getProductPrice.price;
                           }
                           if($DiscountedPrice == 0)
                           {
                             var  $setPrice = $originalPrice;
                           }
                           else
                           {
                              var $setPrice = "<strike>"+$originalPrice+"</strike>&nbsp;"+$DiscountedPrice+"";
                           }
                            if($getProductImage[0])
                            {
                                var mainImage = '<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/'.$getProductImage[0].'';
                            }
                            else
                            {
                              var mainImage = '<?php base_url() ?>Extra/noPhotoFound.png';
                            }

                           // grid+=' <div class="gallery_product col-md-3 col-sm-3"><div class="featured-product"><a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'">'
                           //          +'<img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/'+$getProductImage[0]+'" alt="featured-product-img" class="img-responsive"></a>'+$setStockBadge+''
                           //          +'<div class="overlay2 text-center"><a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'"><i class="fa fa-info"></i></a>'+$setCartDesign+''
                           //          +'</div></div></div><div class="feat-details feat-details2"><p>'+textsplit.substring(0,25)+'</p><span><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;'+$setPrice+'</span><div class="clearfix"></div></div></div>';

                 grid+=' <div class="gallery_product col-md-3 col-sm-3"><div class="grid-product"><div class="image bg-lightblue"><a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'">'
                             +'<img src="'+mainImage+'" alt="featured-product-img" class="img-responsive"></a>'+$setStockBadge+'<div class="overlay2 text-center">'
                               +'<a href="javascript:;" class="Add_to_whishlist_button" data-variant="0" data-cart="'+$productListData.houdin_products_id+'"><i class="fa fa-heart"></i></a><a href="<?php echo base_url() ?>Productlistview/'+$productListData.houdin_products_id+'"><i class="fa fa-info"></i></a>'+$setCartDesign+''
                               +'</div></div></div><div class="feat-details feat-details2"><p>'+textsplit.substring(0,25)+'</p><span><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;'+$setPrice+'</span><div class="clearfix"></div></div></div>';
               }
               $('.appendProductList').append(grid);


               $('.setLoadMoredata').attr('data-last',setData.last);

           }
           });
       });
   });
   </script>
