<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
$images_all = json_decode($product['main_product']->houdinv_products_main_images);
$price_all = json_decode($product['main_product']->houdin_products_price,true);
$final_price = $product['main_product']->houdin_products_final_price;
$overall_rating = $product['reviews_sum']->houdinv_product_review_rating;
$total_rating = count($product['reviews']);
$final_rating = $overall_rating/$total_rating;
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
  <style>
    .checked {
    color: orange;
}

</style>

         <!-- Linking -->
         <div class="linking">
            <div class="container">
               <ol class="breadcrumb">
                  <li class="active">Product Details</li>
               </ol>
            </div>
         </div>
         <!-- Content -->
         <div id="content">
            <!-- Products -->
            <section class="padding-top-40 padding-bottom-60">
               <div class="container">
                  <div class="row">
                     <!-- Products -->
                     <div class="col-md-12">
                        <div class="product-detail">
                           <div class="product">
                              <div class="row">
                                 <!-- Slider Thumb -->
                                 <div class="col-xs-5">
                                    <article class="slider-item on-nav">
                                              <div id="slider" class="flexslider">
                                                  <?php

                                                  ?>
                                                <ul class="slides">
                                                    <?php
                                                    for($index = 0; $index < count($images_all); $index++)
                                                    {
                                                        if($images_all[$index])
                                                        {
                                                    ?>
                                                     <li>
                                                     
                                                    <img src="<?php echo $this->session->userdata('vendorUrl') ?>/upload/productImage/<?php echo $images_all[$index] ?>" alt="">
                                                  </li>
                                                    <?php } }
                                                    ?>
                                                  <!-- items mirrored twice, total of 12 -->
                                                </ul>
                                              </div>
                                              <div id="carousel" class="flexslider">
                                                <ul class="slides">
                                                <?php
                                                    for($indexthumb = 0; $indexthumb < count($images_all); $indexthumb++)
                                                    {
                                                        if($images_all[$indexthumb])
                                                        {
                                                    ?>
                                                     <li>
                                                    <img src="<?php echo $this->session->userdata('vendorUrl') ?>/upload/productImage/<?php echo $images_all[$indexthumb] ?>" alt="">
                                                  </li>
                                                    <?php } }
                                                    ?>

                                                </ul>
                                              </div>
                                    </article>
                                 </div>
                                 <!-- Item Content -->
                                 <div class="col-xs-7">
                                    <h5><?php echo $product['main_product']->houdin_products_title; ?></h5>
                                    <p class="rev">
                                    <?php
                                    for($i=1 ; $i<6 ;$i++)
                                    {
                                        if($i <= $final_rating)
                                        {
                                            $posi = strpos($final_rating,".");
                                            if($posi>=0)
                                            {
                                                $exp =  explode(".",$final_rating);
                                                if($exp[0]==$i)
                                                {
                                    ?>
                                    <i class="fa fa-star-half-o"></i>
                                    <?php
                                    }
                                    else
                                    {
                                        echo '<i class="fa fa-star"></i>';
                                    }
                                    }
                                    else
                                    {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <?php
                                    }
                                }
                                else
                                {
                                    ?>
                                    <i class="fa fa-star-o"></i>
                                    <?php } } ?>
                                    <span class="margin-left-10"><?php echo $total_rating; ?> Review(s)</span>
                                    </p>
                                    <div class="row">
                                       <div class="col-sm-6"><span class="price"><span class="new"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $final_price; ?></span>
                                        <span class="old"><strike>(<?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $price_all['sale_price'];  ?>)</strike></span></div>
                                       <div class="col-sm-6">
                                           <?php
                                           if($product['main_product']->houdinv_products_total_stocks > 0)
                                           {
                                               $settext = 'In Stock';
                                               $setClass = 'text-green';
                                           }
                                           else
                                           {
                                                $settext = 'Out Of Stock';
                                                $setClass = 'text-danger';
                                           }
                                           ?>
                                          <p>Availability: <span class="in-stock <?php echo $setClass ?>"><?php echo $settext ?></span></p>
                                       
                                        <?php if(!empty($product['related'])){?> 
                                        <select style="margin-bottom: 36px;width: 50%;" title="Pick a number" class="form-control variantsselect">
                                        <option>Select variants </option>
                                        <?php 
                                        foreach($product['related'] as $related){?>
                                        <option value="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>"><?php echo $related->houdinv_products_variants_name; ?></option>
                                        <?php } ?>
                                        </select>
                                        <?php  }?>
                                       
                                       
                                        </div>
                                    </div>
                                    <!-- List Details -->
                                    <p><?php echo $product['main_product']->houdin_products_short_desc; ?></p>

                                    <!-- Compare Wishlist -->
                                    <ul class="cmp-list">

                                      <?php
                                      if($product['setWishlistStatus'] == 'yes')
                                      {
                                        ?>
                                        <li><a class="btn-default acc_btn btn_icn " data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>"><i class="fa fa-heart"></i> Already in Wishlist</a></li>
                                      <?php }
                                      else {
                                        ?>
                                        <li><a class="btn-default acc_btn btn_icn Add_to_whishlist_button" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>"><i class="fa fa-heart"></i> Add to Wishlist</a></li>
                                      <?php }
                                      ?>
                                    </ul>
                                    <!-- Quinty -->
                                    <div class="quinty">
                                       <input value="1" min="1" data-max="<?php echo $product['main_product']->houdinv_products_total_stocks ?>" name="qttybutton" class="cart-plus-minus-box setCartQunatity" type="number">
                                    </div>
                                    <a href="#." class="btn-round"><i class="icon-basket-loaded margin-right-5"></i> Add to Cart</a>
                                 </div>
                              </div>
                           </div>
                           <!-- Details Tab Section-->
                           <div class="item-tabs-sec">
                              <!-- Nav tabs -->
                              <ul class="nav" role="tablist">
                                 <li role="presentation" class="active"><a href="#pro-detil"  role="tab" data-toggle="tab">Product Details</a></li>
                                 <li role="presentation"><a href="#cus-rev"  role="tab" data-toggle="tab">Customer Reviews</a></li>

                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                 <div role="tabpanel" class="tab-pane fade in active" id="pro-detil">
                                 <?php echo $product['main_product']->houdin_products_desc; ?>
                                 </div>


                                  <div role="tabpanel" class="tab-pane fade" id="cus-rev">
                                    <div id="" class="">
                                      <h5 class="title">REVIEWS</h5>
<div class="col-sm-6" style="overflow-y: scroll;height: 330px;">
                                      <?php
                                    if(!$final_rating > 0)
                                    {
                                        ?>
                                        
                                         <p>There are no reviews yet.</p>
                                    <p class="text-bigger">Be the first to review</p>
                                   
                                    <?php }
                                    else
                                    {
                                      ?>
                                      
                                      <?php
                                      foreach($product['reviews'] as $user_Review)
                                      {
                            
                                    ?>

                                    <div class=" m-b-10">
                                          <div class=""><?php echo $user_Review->houdinv_product_review_user_name ?></div>


                                          <div class="strat-rating">
                                          <?php for($i=1 ; $i<6 ;$i++)
                                          {
                                          if($i <= $user_Review->houdinv_product_review_rating)
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star checked"></span>
                                          <?php }
                                          else
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star"></span>
                                          <?php } } ?>

                                            <span class="text-date"><?php echo $user_Review->houdinv_product_review_created_at ?></span>
                                          </div>


                                          <div class=""><?php echo  $user_Review->houdinv_product_review_message; ?></div>
                                        </div>
                                      
                                    <?php }
                                     } ?>
</div>



                                      <!-- end left section -->
                                      <div class="col-sm-6">
                                        <div class="comment-rating">
                                        <?php echo form_open(base_url( 'Productlistview' ), array( 'id' => 'rtng_form',"class"=>"checkout_form", 'method'=>'post' ));?>
                                        <div class="form-group">

                                        <div class="strat-rating">
                                            <span data-rating="1" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="2" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="3" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="4" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="5" style="font-size: 22px;" class="fa fa-star"></span>
                                            <input type="hidden"  name="rating" class="rating_data" value="0" />
                                            <input type="hidden"  name="product_id" class="product" value="<?php echo $product_id; ?>" />
                                            <input type="hidden"  name="variant_id" class="" value="0" />
                                        </div>
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation form-control" type="text" name="user_name" placeholder="Your Name" />
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation email_validation form-control" type="text" name="user_email" placeholder="Your Email" />
                                        </div>
                                        <div class="form-group">

                                        <textarea  class="required_validation_for_review form-control" name="user_message" rows="3" placeholder="Write a review"></textarea>
                                        </div>
                                        <input class="btn-round submit_form_review" type="submit" name="review_submit" value="Add Review" />
                                        <?php echo form_close(); ?>
                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                 <!-- start -->

                                 <!-- end -->
                              </div>
                           </div>
                        </div>
                        <!-- Related Products -->
                        <?php
                        if(count($product['related']) > 0)
                        {
                        ?>
                        <section class="padding-top-30 padding-bottom-0">
                           <!-- heading -->
                           <div class="heading">
                              <h2>Related Products</h2>
                              <hr>
                           </div>
                           <!-- Items Slider -->
                           <div class="item-slide-4 with-nav">
                              <!-- Product -->
                                <?php
                                foreach($product['related'] as $related)
                                {
                                ?>
                              <div class="product">
                                 <article>
                                    <img class="img-responsive" src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $related->houdin_products_variants_image; ?>" alt="" >
                                    <!-- Content -->
                                     <a href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>" class="tittle"><?php echo $related->houdin_products_variants_title ?></a>
                                    <!-- Reviews -->
                                   <br/>
                                    <div class="price"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $related->houdinv_products_variants_final_price; ?> </div>
<?php
if($product['setcartStatus'] == 'yes')
{
  ?>
  <a  href="javascript:;" class="cart-btn" data-variant="<?php echo $related->houdin_products_variants_id ?>"  data-cart="0"><i class="icon-basket-loaded" style="color:red;"></i></a>
<?php }
else {
  ?>
  <a  href="javascript:;" class="Add_to_cart_button cart-btn" data-variant="<?php echo $related->houdin_products_variants_id ?>"  data-cart="0"><i class="icon-basket-loaded"></i></a>
<?php }
?>

                                 </article>
                              </div>
                                <?php } ?>
                               </div>
                        </section>
                        <?php } ?>
                     </div>
                  </div>
               </div>
            </section>

         </div>
         <!-- End Content -->
         <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>
 <script src="<?php echo base_url() ?>Extra/electronic/js/vendors/jquery.nouislider.min.js"></script>
      <script>
        //  jQuery(document).ready(function($) {



        //    //  Price Filter ( noUiSlider Plugin)

        //      $("#price-range").noUiSlider({

        //      range: {

        //        'min': [ 0 ],

        //        'max': [ 1000 ]

        //      },

        //      start: [40, 940],

        //          connect:true,

        //          serialization:{

        //              lower: [

        //          $.Link({

        //            target: $("#price-min")

        //          })

        //        ],

        //        upper: [

        //          $.Link({

        //            target: $("#price-max")

        //          })

        //        ],

        //        format: {

        //        // Set formatting

        //          decimals: 2,

        //          prefix: '$'

        //        }

        //          }

        //    })

        //  })



      </script>
  <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('keyup','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        })
        $(document).on('change','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        })
    })
    </script>
