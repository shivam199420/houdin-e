<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/settingsidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

           <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Customer Setting</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li><a href="javascript:void(0);">General</a></li>
                  <li class="active">Customer Setting</li>
                  </ol>
                  </div>
                
               
            </div>
           <!--END Page-Title -->  
         <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
         <?php  
           $customersetting_data=$customersetting[0];

           if($customersetting_data->allow_pay_panding=='1'){
             $selected='checked';
           }else{
             $selected='';

           }
         ?>
        <?php echo form_open(base_url('Setting/customersetting_save'), array( 'id' => 'customersetting_save', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
                 

            <div class="row m-t-20">
        <div class="col-sm-12">
            <div class="card-box">
          <div class="col-md-12">
            <div class="form-group">
            <input type="checkbox" value="1" <?=$selected;?> name="allow_pay_panding">&nbsp;Allow customer to pay pending payment online
          </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
            <input type="checkbox" id="enable_custome_edit" value="1" name="enable_custome_edit" >&nbsp;Enable customer credit
          </div>
          </div>
          <div class="row">
          <div class="ajax_response_result">
          <div class="col-md-6 form-horizontal">
        <div class="form-group">
        <label class="col-md-3 control-label">Amount</label>
        <div class="col-md-9">
        <input type="text" value="<?=$customersetting_data->pay_panding_amount?>"   id="enable_amount" class="form-control required_validation_for_add_custo" disabled="disabled" name="pay_panding_amount" placeholder="Amount">
        </div>
        </div>
      </div>
      <div class="col-md-6 form-horizontal">
    <div class="form-group">
    <label class="col-md-3 control-label">Period</label>
    <div class="col-md-9">
    <input type="text" id="enable_period" value="<?=$customersetting_data->pay_panding_period?>" class="form-control required_validation_for_add_custo" disabled="disabled" name="pay_panding_period" placeholder="Period">
    </div>
    </div>
  </div>
  </div>
         <input type="hidden" name="update_id" value="<?=$customersetting_data->id;?>">
        <input type="submit" class="btn btn-default pull-right m-r-10" id="enable_save" name="" disabled="disabled" value="Submit">
        <button type="reset" id="enable_custome_reset" class="btn btn-info pull-right m-r-10"  disabled="disabled">Reset</button>

        </div>

        </div>
      </div>
    </div>

      <?php echo form_close(); ?>

  </div>
</div>

<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">
  
$(document).ready(function(){

 $("#enable_custome_edit").click(function () {
                if ($(this).is(":checked")) {
                    $("#enable_amount").removeAttr("disabled");
                    $("#enable_amount").focus();
                    $("#enable_period").removeAttr("disabled");
                    $("#enable_save").removeAttr("disabled");
                     $("#enable_custome_reset").removeAttr("disabled");

                } else {
                    $("#enable_amount").attr("disabled", "disabled");
                    $("#enable_period").attr("disabled", "disabled");
                    $("#enable_save").attr("disabled", "disabled");
                    $("#enable_custome_reset").attr("disabled", "disabled");


                }
            });
 $("#enable_custome_reset").click(function () { 
  
 
$('.required_validation_for_add_custo').each(function() {
  
        $(this).val('');
         
    });
 });


 $('#customersetting_save').submit(function(e){
    e.preventDefault(); 
 

var check_required_field='';
      $(this).find(".required_validation_for_add_custo").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {
        return false;
      }else{
      


    urls='<?=base_url();?>'+'Setting/customersetting_save';
           
        $.ajax({
        url:urls,
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success:function (data) {
        
        if(data){
        $(".ajax_response_result").html(data);
        }else{
        window.location.reload();
        }
        }
        }); } 
    });  


 }); 

</script>