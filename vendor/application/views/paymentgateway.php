<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style type="text/css">
  /* @media screen and (min-width: 768px){
.dropdown.dropdown-lg .dropdown-menu {
    min-width: 390px!important;
}
.checkbox{
  padding-left: 10px;
}
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 10px;
}
}
@media(max-width:767px){
  .custom_table_respo td{ 
       display: block;
       width: 50%;
       float: left;
  }
}
input[type=checkbox], input[type=radio] {
    margin-top: 1px\9!important;
    vertical-align: middle!important;
    height: 20px!important;
    padding: 0px!important;
    width: 20px!important;
    margin: 0!important;
    border-radius: 0!important;
    background: 0 0!important;
    position: relative!important;
} */
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
     <!--START Page-Title -->
            <div class="row">
           
                <div class="col-md-8">
                   <h4 class="page-title">Payment Gateway</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  
                  <li class="active">Payment Gateway</li>
                  </ol>
                  </div>
                 
            </div>
           <!--END Page-Title --> 
          
          <div class="row">
          <div class="col-sm-12">
          <?php 
          if($this->session->flashdata('success'))
          {
              echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
          }
          if($this->session->flashdata('error'))
          {
              echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
          }
          ?>
          </div>
          </div>

                                      <div class="row m-t-20">
        <div class="col-sm-12">
        <?php
        ?>
        <div class="col-sm-3">
        <?php 
        if(count($getPaymentGatewayPauy) > 0)
        {
        ?>
        <img class="payuBtn" data-status="<?php echo $getPaymentGatewayPauy[0]->houdinv_payment_gateway_status ?>"  data-salt="<?php echo $getPaymentGatewayPauy[0]->houdinv_payment_gateway_merchant_salt ?>" data-merchant="<?php echo $getPaymentGatewayPauy[0]->houdinv_payment_gateway_merchnat_key ?>" data-id="<?php echo $getPaymentGatewayPauy[0]->houdinv_payment_gateway_id ?>" style="width:100%;    border: 1px solid red;border-radius: 8px;" src="<?php echo base_url(); ?>assets/images/payu.jpeg">
        <?php }
        else
        {
        ?>
        <img class="payuBtn" style="width:100%;" src="<?php echo base_url(); ?>assets/images/payu.jpeg">
        <?php }
        ?>
        </div>
        <div class="col-sm-3">
        <?php 
        if(count($getPaymentGatewayAuth) > 0)
        {
        ?>
        <img class="authorizednet" data-status="<?php echo $getPaymentGatewayAuth[0]->houdinv_payment_gateway_status ?>" data-transaction="<?php echo $getPaymentGatewayAuth[0]->houdinv_payment_gateway_merchant_salt ?>" data-login="<?php echo $getPaymentGatewayAuth[0]->houdinv_payment_gateway_merchnat_key ?>" data-id="<?php echo $getPaymentGatewayAuth[0]->houdinv_payment_gateway_id ?>" style="width:100%;border: 1px solid red;border-radius: 8px;" src="<?php echo base_url(); ?>assets/images/authorizenet.png">
        <?php }
        else
        {
        ?>
        <img class="authorizednet" style="width:100%;" src="<?php echo base_url(); ?>assets/images/authorizenet.png">
        <?php }
        ?>
        </div>
        </div>
       
        </div>
         
        </div>

        <div id="Payumodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
            <?php echo form_open(base_url( 'Paymentgateway' ), array( 'id' => 'addPaymentGatewayForm', 'method'=>'post' ));?>
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title custom_title"><b>PayUmoney</b></h4> 
                    </div> 
                    <div class="modal-body"> 
                            <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Merchant Key</label> 
                                    <input type="text" name="merchantKey" class="merchantKey form-control required_validation_for_payment_gateway name_validation" placeholder="Merchant Key"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                <input type="hidden" class="payumoneyId" name="payumoneyId"/>
                                    <label for="field-3" class="control-label">Merchant Salt</label> 
                                    <input type="text" name="merchantSalt" class="merchantSalt form-control required_validation_for_payment_gateway name_validation" placeholder="Merhcnat Salt"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Status</label> 
                                    <select class="form-control required_validation_for_payment_gateway payuStatus" name="payuStatus">
                                    <option value="">Choose Status</option>
                                    <option value="active">Active</option>
                                    <option value="deactive">Deactive</option>
                                    </select>
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div class="modal-footer"> 
                    <input type="submit" name="payumoneyAdd" class="btn btn-info" value="Add"/>
                    </div> 
                </div> 
                <?php echo form_close(); ?>
            </div>
        </div><!-- /.modal -->

        <div id="authorizedModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
            <?php echo form_open(base_url( 'Paymentgateway' ), array( 'id' => 'addAuthorizedPaymentGatewayForm', 'method'=>'post' ));?>
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title custom_title"><b>Authorized.Net</b></h4> 
                    </div> 
                    <div class="modal-body"> 
                            <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Login Id</label> 
                                    <input type="text" name="loginId" class="loginId form-control required_validation_for_payment_gateway name_validation" placeholder="Login Id"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                <input type="hidden" class="authorizedid" name="authorizedid"/>
                                    <label for="field-3" class="control-label">Transaction Key</label> 
                                    <input type="text" name="transactionKey" class="transactionKey form-control required_validation_for_payment_gateway name_validation" placeholder="Transaction Key"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Status</label> 
                                    <select class="form-control required_validation_for_payment_gateway authorizedStatus" name="authorizedStatus">
                                    <option value="">Choose Status</option>
                                    <option value="active">Active</option>
                                    <option value="deactive">Deactive</option>
                                    </select>
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div class="modal-footer"> 
                    <input type="submit" name="authorizedAdd" class="btn btn-info" value="Add"/>
                    </div> 
                </div> 
                <?php echo form_close(); ?>
                            </div>
        </div><!-- /.modal -->

<?php $this->load->view('Template/footer.php') ?>
 
<script type="text/javascript">
$(document).ready(function(){
    $(document).on('click','.payuBtn',function(){
        if($(this).attr('data-id'))
        {
            $('.payumoneyId').val($(this).attr('data-id'));
            $('.merchantKey').val($(this).attr('data-merchant'));
            $('.merchantSalt').val($(this).attr('data-salt'));
            $('.payuStatus').val($(this).attr('data-status'));
        }
        $('#Payumodal').modal('show');
    });
});
</script>
<!-- CLient side form validation -->
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#addPaymentGatewayForm,#addAuthorizedPaymentGatewayForm',function(){
			var check_required_field='';
			$(this).find(".required_validation_for_payment_gateway").each(function(){
				var val22 = $(this).val();
				if (!val22){
					check_required_field =$(this).size();
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
				return true;
			}
		});
        $(document).on('click','.authorizednet',function(){
            $('.loginId').val($(this).attr('data-login'));
            $('.authorizedid').val($(this).attr('data-id'));
            $('.transactionKey').val($(this).attr('data-transaction'));
            $('.authorizedStatus').val($(this).attr('data-status'));
            $('#authorizedModal').modal('show');
        })
	});
	</script>
