<?php

class Authentication extends CI_Model

{

    function __construct()

    {

        parent::__construct();
        $this->adminAuth = 'houdin_user_auth';
        $this->adminAuthsecond = 'houdin_admin_user_second_auth';
        $this->adminLog = 'houdin_user_logs';
        $this->adminUsers = 'houdin_admin_users';

    }



    public function AdminLogin($para=false)

    {

        $adminPassword = $para['password'];

        $resultEmail =  $this->db->select('*')

                        ->from($this->adminUsers)

                        ->where('houdin_admin_user_email',$para['email'])

                        ->get()->row();

        

        if(count($resultEmail) > 0)

        {
        if(verifyHashedPassword($adminPassword, $resultEmail->houdin_admin_user_password))
    	  {
    	  return $resultEmail;
    	  }
    	  else
    	  {
             return false;
    	  }


        }

    }   
    
    public function SaveToken($para=false)
    {
         
        $existEmail =  $this->db->select('*')

                        ->from($this->adminAuthsecond)

                        ->where('houdin_second_auth_email',$para['houdin_second_auth_email'])

                        ->get()->row();   
        if($existEmail)   // if user email already have an entry in the auth table
        {
            $this->db->where('houdin_second_auth_email',$para['houdin_second_auth_email']);
            $this->db->update($this->adminAuthsecond,$para);
            return $para['houdin_second_auth_email'];
        }
        else
        {
            
            $this->db->insert($this->adminAuthsecond,$para);
            return $para['houdin_second_auth_email'];
            
        }     
        
    } 
    
    
    public function VarifyTokenByDb($token,$user)
    {
                $TokenMatch =  $this->db->select('*')

                        ->from($this->adminAuthsecond)

                        ->where('houdin_second_auth_email',$user)

                        ->get()->row(); 
                 if($TokenMatch)
                 {
                    if($TokenMatch->houdin_second_auth_token==$token)
                    {
                        return $TokenMatch;
                        
                    }
                 }       
                        
        
    }
    
    public function SaveAllLoginDeatil($userData)
    {
        $ip=$_SERVER['REMOTE_ADDR'];
        $browser =  $this->get_browser_name($_SERVER['HTTP_USER_AGENT']);
        $country =  $this->get_country_name($ip);
        $date = strtotime(date("Y-m-d, h:i:s"));
        // user time update in user auth table
        $para = array('houdin_admin_user_updated_at'=>$date);
        $this->db->where('houdin_admin_user_email',$userData);
        $this->db->update($this->adminUsers,$para);
        
        $resultUserId =  $this->db->select('*')

                ->from($this->adminUsers)

                ->where('houdin_admin_user_email',$userData)

                ->get()->row();
                $resultUserId =get_object_vars($resultUserId);
        
        
        // user log table insert
        $logData = array("houdin_user_log_userId"=>$resultUserId['houdin_admin_user_id'],"houdin_user_log_browser"=>$browser,
                        "houdin_user_log_geo_location"=>$country,"houdin_user_log_created_at"=>$date,
                        "houdin_user_log_ip_address"=>$ip);
        
        $this->db->insert($this->adminLog,$logData);
        
        //
        
        $letters1='abcdefghijklmnopqrstuvwxyz'; 
        $string1=''; 
        for($x=0; $x<3; ++$x)
        {  
            $string1.=$letters1[rand(0,25)].rand(0,9); 
        }
        $final_auth = md5($string1);
        
        $AuthData = array("houdin_user_auth_auth_token"=>$final_auth,"houdin_user_auth_url_token"=>$final_auth,
        "houdin_user_auth_user_id"=>$resultUserId['houdin_admin_user_id'],
        "houdin_user_auth_updated_at"=>$date);
        
        $AlredyAuthData =  $this->db->select('*')

        ->from($this->adminAuth)

        ->where('houdin_user_auth_user_id',$resultUserId['houdin_admin_user_id'])

        ->get()->row(); 
                 if($AlredyAuthData)
                 {
                    
                     $this->db->where('houdin_user_auth_user_id',$resultUserId['houdin_admin_user_id']);
                     $this->db->update($this->adminAuth,$AuthData);
                     
                 }
                 else
                 {
                    $AuthData["houdin_user_auth_created_at"]=$date;

                     $this->db->insert($this->adminAuth,$AuthData);
                 }
                 
                 $this->db->delete($this->adminAuthsecond, array('houdin_second_auth_email' => $userData)); 
                 return array('loginAuth'=>$final_auth,'userrole'=>$resultUserId['houdin_admin_user_role_id'],'department'=>$resultUserId['houdin_admin_user_department']);
                 
        
        //
        
    }
    
    public  function get_browser_name($user_agent)
    {
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
    elseif (strpos($user_agent, 'Edge')) return 'Edge';
    elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
    elseif (strpos($user_agent, 'Safari')) return 'Safari';
    elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
    
    return 'Other';
    }
    
    public function get_country_name($ip)
    {
          $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://www.geoplugin.net/json.gp?ip=".$ip);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $ip_data_in = curl_exec($ch); // string
            curl_close($ch);
        
            $ip_data = json_decode($ip_data_in,true);
            $ip_data = str_replace('&quot;', '"', $ip_data); // for PHP 5.2 see stackoverflow.com/questions/3110487/
        
            if($ip_data && $ip_data['geoplugin_countryName'] != null) {
               $country = $ip_data['geoplugin_countryName'];
            }

        else
        {
            $country='';
        }
        return $country;
        
    }
    
    public function RemoveAuthToken($token)
    {
        $AuthData = array("houdin_user_auth_auth_token"=>'',"houdin_user_auth_url_token"=>'');
         
         $this->db->where('houdin_user_auth_url_token',$token);
         $this->db->update($this->adminAuth,$AuthData);
    }
    
    public function AdminDetail($email)
    {
                $resultData =  $this->db->select('*')

                        ->from($this->adminUsers)

                        ->where('houdin_admin_user_email',$email)

                        ->get()->row();
                        
                 return   $resultData;    
    }

// Usage:


    
    
     

}

