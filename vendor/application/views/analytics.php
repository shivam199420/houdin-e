<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/settingsidebar.php') ?>
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        
                        <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Analytics Key</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li class="active">Analytics Key</li>
                  </ol>
                  </div>
            
            </div>
           <!--END Page-Title -->  
           <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12 m-b-15">
                                <h4 class="page-title">Analytics Key</h4>

                            </div>
                        </div>

  <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>

        <?php 
 
         $analyticss=$analytics[0];
    ?> 

           <?php echo form_open(base_url('Setting/analytics'), array( 'id' => 'analyticsadd', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
        <div class="row">
          <div class="col-md-12">
            <div class="card-box row">
              <div class="form-group">
                <label>Analytics Key</label>
                <input type="text" class="form-control required_validation_for_add_custo" name="analytics_number" value="<?=$analyticss->analytics_number;?>" placeholder="Enter Analytics Key">

                <div class="errorMessage"><?php echo form_error('analytics_number'); ?></div>
              </div>

              <div class="col-md-12 ">
        <input type="submit" class="btn btn-default pull-right m-r-10" name="" value="Submit">
           <button type="reset" name="reset" id="google_anlyas_reset" class="btn btn-info pull-right m-r-10">Reset</button>
        </div>
            </div>
          </div>
        </div>
         <?=form_close(); ?>


                    </div> <!-- container -->

                </div> <!-- content -->

<?php $this->load->view('Template/footer.php') ?>

<script type="text/javascript">
  
$(document).ready(function(){


 $("#google_anlyas_reset").click(function () { 
  
 
$('.required_validation_for_add_custo').each(function() {
  
        $(this).val('');
         
    });
 });

 
 $('#free_delivery').change(function() {
        if(this.checked) {
         
         
 $('.free_delivery_html').html('<input type="text" value="" placeholder="Order Amount Crossed" name="free_delivery_val" class="form-control required_validation_for_add_custo">');
        }else{
          
              $('.free_delivery_html').html('');
        }
                
    });


 $('#analyticsadd').submit(function(e){
    
  

var check_required_field='';
      $(this).find(".required_validation_for_add_custo").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {
        e.preventDefault(); 
        return false;
      }else{      
       return true;

      }  
    });  


 }); 

</script>
