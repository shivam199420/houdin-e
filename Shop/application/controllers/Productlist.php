<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Productlist extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopproductlistmodel');
         $this->load->model('Shopsettingmodel');
         $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
        $this->perPage = 12;
    }
    public function index()
    {
        if($_REQUEST['sort'])
        {
            $setSort = $_REQUEST['sort'];
        }
        $getCategoryId = $this->uri->segment('2');
        $getSubCategoyrId = $this->uri->segment('3');
        $getSubSubCategoyrId = $this->uri->segment('4');
        $setArrayData = array('categoryId'=>$getCategoryId,'subcategory'=>$getSubCategoyrId,'subsubcategory'=>$getSubSubCategoyrId,'setSort'=>$setSort);
        $getList = $this->Shopproductlistmodel->getProductList($setArrayData);
        $getList['count'] = $this->Shopproductlistmodel->getProductTotalCount($setArrayData);
        $getList['storeSetting'] = stroeSetting($this->session->userdata('shopName'));
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view($getShopName.'/productlist',$getList);
    }
    public function fetchProductPaginationData()
    {
        $getLast = $this->input->post('dataLast');
        $getRemain = $this->input->post('dataRemain');
        echo $getLast."::".$getRemain;
        $getCategoryId = $this->uri->segment('2');
        $getSubCategoyrId = $this->uri->segment('3');
        $getSubSubCategoyrId = $this->uri->segment('4');
        $setArrayData = array('categoryId'=>$getCategoryId,'subcategory'=>$getSubCategoyrId,'subsubcategory'=>$getSubSubCategoyrId);
    }
}
