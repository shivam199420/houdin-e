<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/settingsidebar.php') ?>

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
           <!--START Page-Title -->
            <div class="row">
            
                <div class="col-md-8">
                   <h4 class="page-title">Invoice Setting</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li><a href="javascript:void(0);">General</a></li>
                  <li class="active">Invoice Setting</li>
                  </ol>
                  </div>
                  <div class="col-md-4">
                  </div>
                  
               
            </div>
              <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>

           <?php 
 
         $invoicesettings=$invoicesetting[0];

         if($invoicesettings->show_task_breakup=='1'){
    $show_task_breakup='checked';
  }else{
    $show_task_breakup='';
  }
  if($invoicesettings->total_saving_invoice=='1'){
    $total_saving_invoice='checked';
  }else{
    $total_saving_invoice='';
  }
  if($invoicesettings->invoice_number_receipt=='1'){
    $invoice_number_receipt='checked';
  }else{
    $invoice_number_receipt='';
  }
  if($invoicesettings->order_number_receipt=='1'){
    $order_number_receipt='checked';
  }else{
    $order_number_receipt='';
  }
  if($invoicesettings->product_wise=='1'){
    $product_wise='checked';
  }else{
    $product_wise='';
  } 
    ?> 

           <?php echo form_open(base_url('Setting/invoicesetting'), array( 'id' => 'invoicesetting', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
           <!--END Page-Title -->  
            <div class="row m-t-20">
              <div class="col-md-12">
                <div class="card-box row">
                  <div class="col-md-12">
            <div class="form-group">
          <label>Printer Setting</label>
          </div>
          </div>

  <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?=$show_task_breakup;?> name="show_task_breakup">&nbsp;Show Tax Breakup
</div>
</div>
<div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?=$total_saving_invoice;?> name="total_saving_invoice">&nbsp;Show Total Saving On Invoice
</div>
</div>
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?php //$invoice_number_receipt;?> name="invoice_number_receipt">&nbsp;Print Invoice Number On Receipt
</div>
</div> -->
<div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?=$order_number_receipt;?> name="order_number_receipt">&nbsp;Print Order Number On Receipt
</div>
</div>
<!-- <div class="col-md-12">
<div class="form-group">
<input type="checkbox" value="1" <?php //$product_wise;?> name="product_wise">&nbsp;Show Product-Wise Tax Breakup
</div>
</div> -->
<!-- <div class="col-md-12">
<div class="form-group">
<label>Configuring Invoice Numbering Pattern(Sales  Invoice & Purchase Invoice)</label>
<input type="text" class="form-control" name="invoice_num_start" value="<?php //$invoicesettings->invoice_num_start?>">
</div>
</div> -->
<div class="col-md-12">
  <div class="form-group">
      <a href="<?=base_url();?>Setting/previewinvoice" class="btn btn-default">Preview</a>
    <button type="submit" class="btn btn-info">Submit</button>
  </div>

</div>
 </div>
              </div>
            </div>
         <?=form_close(); ?>









          </div>
        </div>
      </div>
    <?php $this->load->view('Template/footer.php') ?>
