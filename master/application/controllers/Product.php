<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends CI_Controller 
{
    public function productattribute()
    {
        $this->load->view('productattribute');
    }
    public function producttype()
    {
        $this->load->view('producttype');
    }
    public function addproducttype()
    {
        $this->load->view('addproducttype');
    }
    public function editproducttype()
    {
        $this->load->view('editproducttype');
    }
}
