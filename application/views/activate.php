<?php  $this->load->view('Template/header');?> 
<style>
.setFormData
{
  display: contents !important;
}
</style>
<header class="header bg-gray header-page-top">
  <div class="header-page-top-inner">
	  <div class="container">
		<h1 class="display-4">Your Account Setup</h1> 
  </div>
	</div>
</header>

		 
	   <section class="section  bg-img-bottom" >
        <div class="container text-center">
		<div class="col-md-8 mx-auto">	
			<ul class="register_active_step_box">
				<li><a class="register_active_step_box_btn current_step_active"><span>1</span>OTP Verification</a></li>
				<li><a class="register_active_step_box_btn"><span>2</span> Basic Information</a></li>
				 
			</ul>
			</div>
			<div class="clearfix"></div>
      <?php 
      if($this->session->flashdata('error'))
      {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
      }
      if($this->session->flashdata('success'))
      {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
      }
      ?>
          <h5>Hi <?php echo $this->session->userdata('temPname') ?>, Just one more step to go...</h5>
			<br/>
			<p class="lead">An OTP has been sent on your mobile (<?php echo $this->session->userdata('tempPhone') ?>). <br/> Please verify your account by entering it here</p>

          <br> <br/>

          <!-- <form action="<?php //echo base_url(); ?>/home/complete" method="post"> -->
          <?php echo form_open(base_url( 'Register/activate' ), array( 'id' => 'regitserSecondForm', 'method'=>'post'));?>
            <div class="form-row">
              <div class="col-md-4 ml-auto mb-4 mb-md-0 pr-0">
                <div class="input-group input-group-lg">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                  </div>
                  <input type="text" name="otp" maxlength="5" class="form-control required_validation_for_register_second_form name_validation number_validation" placeholder="Enter OTP">
                </div>
              </div>

              <div class="col-md-2 mr-auto">
              <input type="submit" name="submitOtpData" class="btn btn-block btn-lg btn-primary" value="Submit"/>
              </div>
            </div>
          <?php echo form_close(); ?>

           <br/>

          <div class="col-md-8 mx-auto text-center">
          <?php echo form_open(base_url( 'Register/activate' ), array( 'method'=>'post','class'=>'setFormData btn btn-link'));?>
          <i class="fa fa-envelope"></i><input type="submit" name="resendOtpForms" class="btn btn-link" style="padding:0px !important" value="Resend OTP Via SMS"/>
          <?php echo form_close(); ?>
          <!-- <input type="submit" value="Resend OTP Via SMS" class="btn btn-link"/> -->
            <!-- <a class="btn btn-link" href="javascript:;"><i class="fa fa-envelope"></i> Resend OTP Via SMS </a> -->
            <!-- <a class="btn btn-link" href="javascript:;"><i class="fa fa-phone"></i> Resend OTP Via Call </a> -->
			  <a class="btn btn-link" href="javascript:;" data-toggle="modal" data-target="#user-change-mobile-accountsetup"><i class="fa fa-pencil"></i> Change mobile numbers? </a>            
          </div>

        </div>
      </section>


		 <!-- user forgot -->
    <div class="modal fade" id="user-change-mobile-accountsetup" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">

          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <?php echo form_open(base_url( 'Register/activate' ), array( 'id' => 'changeMobileNumber', 'method'=>'post'));?>
              <h4 class="mb-5">Change mobile numbers</h4>
                
              <div class="form-group">
                <input placeholder="Please Enter Mobile Number" class="form-control Internationphonecode required_validation_for_change_mobile_number name_validation" name="userAnotherPhone" type="text">
              </div>
              <input type="submit" class="btn btn-lg btn-block btn-primary" name="updateMobileNumber" value="Update"/>
            <?php echo form_close(); ?>
          </div>

        </div>
      </div>
    </div>
 
<?php  $this->load->view('Template/footer');?>
 <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#regitserSecondForm',function(){
			var check_required_field='';
			$(".required_validation_for_register_second_form").each(function(){
				var val22 = $(this).val();
				if (!val22){
          // check_required_field =$(this).size();
          check_required_field =$(this).length;
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
         return true;
			}
		});
	});
	</script>
  <script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#changeMobileNumber',function(){
			var check_required_field='';
			$(".required_validation_for_change_mobile_number").each(function(){
				var val22 = $(this).val();
				if (!val22){
          // check_required_field =$(this).size();
          check_required_field =$(this).length;
					$(this).css("border-color","#ccc");
					$(this).css("border-color","red");
				}
				$(this).on('keypress change',function(){
					$(this).css("border-color","#ccc");
				});
			});
			if(check_required_field)
			{
				return false;
			}
			else {
         return true;
			}
		});
	});
	</script>