<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Changepassword extends CI_Controller
{
    function __construct()
	{ 
        parent::__construct();
        if($this->session->userdata('userAuth') == "")
        {
            redirect(base_url()."register", 'refresh');
        }
        $this->load->model('Shopuserchangepass');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/changepassword');
    }     
    public function updatepass()
    {
        $this->form_validation->set_rules('currentPass','Current Password','required');
        $this->form_validation->set_rules('newpass','New Password','required');
        $this->form_validation->set_rules('newconfirmpass','Confirm Password','required');
        if($this->form_validation->run() == true)
        {
            $getNewPass = $this->input->post('newpass');
            $getNewConfirmPass = $this->input->post('newconfirmpass');
            if($getNewPass == $getNewConfirmPass)
            {
                $setArray = array('oldpass'=>$this->input->post('currentPass'),'newpass'=>$getNewPass);
                $getPassData = $this->Shopuserchangepass->updatePassword($setArray);
                if($getPassData['message'] == 'no')
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url()."Changepassword", 'refresh'); 
                }
                else if($getPassData['message'] == 'oldpass')
                {
                    $this->session->set_flashdata('error','Please enter correct old password');
                    redirect(base_url()."Changepassword", 'refresh');
                }
                else if($getPassData['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Password updated successfully');
                    redirect(base_url()."Changepassword", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url()."Changepassword", 'refresh'); 
                }
            }
            else
            {
                $this->session->set_flashdata('error','Password are not matched');
                redirect(base_url()."Changepassword", 'refresh');    
            }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."Changepassword", 'refresh');
        }
    }
}
