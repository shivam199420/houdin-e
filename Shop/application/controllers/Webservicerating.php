<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Webservicerating extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopratingmodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function addproductrating()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('userId','text','required');
        $this->form_validation->set_rules('rating','text','required');
        $this->form_validation->set_rules('comment','text','required');
        if($this->form_validation->run() == true)
        {
            $userInfo = $this->Shopratingmodel->fetchUserinfo($this->input->post('userId'),$this->input->post('shopName'));
            if(count($userInfo) > 0)
            {
                $setArrayData = array('houdinv_product_review_user_id'=>$userInfo[0]->houdinv_user_id,'houdinv_product_review_user_email'=>$userInfo[0]->houdinv_user_email,
                'houdinv_product_review_user_name'=>$userInfo[0]->houdinv_user_name,'houdinv_product_review_product_id'=>$this->input->post('productId'),'houdinv_product_reviews_variant_id'=>$this->input->post('varinatId'),
                'houdinv_product_review_message'=>$this->input->post('comment'),'houdinv_product_review_rating'=>$this->input->post('rating'),'houdinv_product_review_created_at'=>date('Y-m-d h:i:s'));
                $setResultArray = $this->Shopratingmodel->addProductRating($setArrayData,$this->input->post('shopName'));
                print_r(json_encode($setResultArray));    
            }
            else
            {
                $setResultArray = array('message'=>'Something went wrong. Please try again');
                print_r(json_encode($setResultArray));    
            }
        }
        else
        {
            $setResultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($setResultArray));
        }
    }
    // fetch rating on product page
    public function fetchProductrating()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('shopName'=>$this->input->post('shopName'),'productId'=>$this->input->post('productId'),'variantId'=>$this->input->post('variantId'));
            $getReview = $this->Shopratingmodel->fetchProductRating($setArray);
            print_r(json_encode($getReview));
        }
        else
        {
            $setResultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($setResultArray));
        }
    }
    // fetch total rating on product page
    public function fetchTotalrating()
    {
        $this->form_validation->set_rules('shopName','text','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('shopName'=>$this->input->post('shopName'),'productId'=>$this->input->post('productId'),'variantId'=>$this->input->post('variantId'));
            $getReview = $this->Shopratingmodel->fetchProductTotalRating($setArray);
            print_r(json_encode($getReview));
        }
        else
        {
            $setResultArray = array('message'=>'All fields are mandatory');
            print_r(json_encode($setResultArray));
        }
    }
}
