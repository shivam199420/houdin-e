<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function payumoney()
{
    $CI =& get_instance();
    $getPayumoneydata = $CI->db->select('*')->from('houdin_payumoney')->where('houdin_payumoney','1')->get()->result();
    return $getPayumoneydata;
}
function getUserDetails($id)
{
    $CI =& get_instance();
    $getUserInfo = $CI->db->select('houdin_user_name,houdin_user_email,houdin_user_contact')->from('houdin_users')->where('houdin_user_id',$id)->get()->result();
    return $getUserInfo;
}
function gettwillioDetails()
{
    $CI =& get_instance();
    $getTwillioDetails = $CI->db->select('*')->from('houdin_twilio')->get()->result();
    return $getTwillioDetails;
}