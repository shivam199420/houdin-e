<?php  $this->load->view('Template/header');?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- <link href="<?php echo base_url(); ?>assets/css/index.css" rel="stylesheet">-->
    <link href="<?php echo base_url(); ?>assets/css/mfb.css" rel="stylesheet">


     
    <!--  <section class="section text-center" style="background-image: linear-gradient(135deg, #f9f7ff 0%, #fff 50%, #f6f3ff 100%);">
        <div class="container">
 
          <div class="row">
            <div class="col-md-6 mx-auto">
              <p><img src="<?php echo base_url(); ?>assets/img/icon/rocket2.png" alt="join us"></p>
              <br>
              <h3 class="mb-6"><strong>Join over 2,400 companies that trust us.</strong></h3>
              <p class="lead text-muted">Try it yourself for a while. Request a refund if you didn't find it useful and awesome as we advertised.</p>
              <br>
              <a class="btn btn-xl btn-round btn-info px-7" href="">Get a license</a>
            </div>
          </div>

        </div>
      </section>-->


      
      <section class="section py-8">
        <div class="container">
       
           <div class="row"> 

            <div class="col-12 col-md-6 text-center " >
              <h1 class="text_black">Access your CRM & Marketing tools anytime, anywhere</h1>
            </div>
        
         <div class="col-12 col-md-6 pb-70 align-self-center">
          
          <p class="lead-2 custom_lead_p text_black">Interactively maintain vertical portals through competitive strategic theme areas. Collaboratively reintermediate cross-platform data rather than state of the art value. Interactively maintain vertical portals through competitive strategic theme areas. Collaboratively reintermediate cross-platform data rather than state of the art value.</p>

         <br>

        <a class="btn btn-xl btn-round btn-second w-200 mr-3 px-6 d-none d-md-inline-block" href="" data-scrollto="section-intro">Get Started</a>
      


        </div>
          </div>
    

        </div>
      </section>
<div class="clearfix"></div>
      <section class="section py-8"> 
        <div class="container">
          <div class="row"> 
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <div class="custom_all_circle">
    <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_left" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
        
        </a>
        <ul class="mfb-component__list" data-left>
          <li>
            <a href="https://github.com/nobitagit/material-floating-button/" data-mfb-label="View on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-github"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
        </ul>
      </li>
      </ul>

      <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_right" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
       
        </a>
        <ul class="mfb-component__list" data-right>
          <li>
            <a href="https://github.com/nobitagit/material-floating-button/" data-mfb-label="View on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-github"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
        </ul>
      </li>
      </ul>

      <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_bottom" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
       
        </a>
        <ul class="mfb-component__list" data-bottom>
          <li>
            <a href="https://github.com/nobitagit/material-floating-button/" data-mfb-label="View on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-github"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/nobitagit" data-mfb-label="Follow me on Github" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-octocat"></i>
            </a>
          </li>

          <li>
            <a href="http://twitter.com/share?text=Check this material floating button component!&url=http://nobitagit.github.io/material-floating-button/&hashtags=material,design,button,css"
               data-mfb-label="Share on Twitter" class="mfb-component__button--child">
              <i class="mfb-component__child-icon ion-social-twitter"></i>
            </a>
          </li>
        </ul>
      </li>
      </ul>
     
      <ul id="menu" class="mfb-component--br mfb-zoomin custom_position_center" data-mfb-toggle="hover" >
      <li class="mfb-component__wrap">
		  <a href="#" class="mfb-component__button--main custom_circle" style=""><span>houdin-e</span></a>
         
      </li>
      </ul>
    </div>
            </div>
          </div>
        </div> 
      </section>    
      <div class="clearfix"></div> 



    <section class="section text-white py-8" style="background-image:linear-gradient(120deg, #244669 0%, #5079ad 100%);background-image:linear-gradient(120deg, #244669 0%, #5079ad 100%);">
        <div class="container">

          <div class="row"> 

            <div class="col-12 col-md-6 align-self-end text-center " >
              <img src="<?php echo base_url(); ?>assets/img/18.png"/>
            </div>
        
         <div class="col-12 col-md-6 pb-70 align-self-center">
          <h2>Access your CRM & Marketing tools anytime, anywhere</h2>
          <p class="lead-2">Interactively maintain vertical portals through competitive strategic theme areas. Collaboratively reintermediate cross-platform data rather than state of the art value.</p>

         <br><br> 

        <a class="btn btn-xl btn-round btn_theme_orange w-200 mr-3 px-6 d-none d-md-inline-block" href="" data-scrollto="section-intro">Start Now</a>
        <a class="btn btn-xl btn-round btn_theme_white w-200 px-6" href="" data-scrollto="section-intro" >Learn More</a>


        </div>
          </div>

        </div>
      </section>





    <section class="section py-8">
        <div class="container">
          <header class="section-header"> 
            <h2>Choose your perfect plan</h2>
             
          </header> 


          <div class="row gap-y text-center">

            <div class="col-md-4">
              <div class="pricing-1">
                <p class="plan-name">SILVER</p>
                <br>
                <h2 class="price"> <span class="price-unit">$</span>
                  <span data-bind-radio="pricing-1">29</span>
          </h2>
               <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">Yearly</span>
                </p>

                <div class="text-muted">
        <p class="mb-1"><i class="ti-check text-three mr-2"></i> Your eCommerce Store</p>
        <p class="mb-1"><i class="ti-check text-three mr-2"></i> 50+ beautiful themes</p>
        <p class="mb-1"><i class="ti-check text-three mr-2"></i> Automated Logistics</p>
        <p class="mb-1"><i class="ti-check text-three mr-2"></i>  Coupon code promotions</p>
        <p class="mb-1"><i class="ti-check text-three mr-2"></i>  PIN/ZIP code avaliablity check</p>
        <p class="mb-1"><i class="ti-check text-three mr-2"></i> SMS & Email Notifications/p> 
                </div>

                <br>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="">Get started</a></p>
              </div>
            </div>


            <div class="col-md-4">
              <div class="pricing-1 popular">
                <p class="plan-name">Gold</p>
                <br>
                <h2 class="price text-second">
                  <span class="price-unit">$</span>
                  <span data-bind-radio="pricing-1">39</span>
                </h2>
                <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">monthly</span>
                </p>

                <div class="text-muted">
          
                 <p class="mb-1"><i class="ti-check text-second mr-2"></i> Enabled International Payments</p>
          <p class="mb-1"><i class="ti-check text-second mr-2"></i> 50+ beautiful themes</p>
          <p class="mb-1"><i class="ti-check text-second mr-2"></i> 25+ premium themes</p>
          <p class="mb-1"><i class="ti-check text-second mr-2"></i>Payment enabled Facebook store</p>
          <p class="mb-1"><i class="ti-check text-second mr-2"></i> Wholesale pricing. Wishlist</p>
          <p class="mb-1"><i class="ti-check text-second mr-2"></i> 30-Day Onboarding support</p>
          <p class="mb-1"><i class="ti-check text-second mr-2"></i>  Superfast website (CDN).</p>
          <p class="mb-1"><i class="ti-check text-second mr-2"></i> CRM, Marketing tools</p>
                  
                </div>

                <br>
                <p class="text-center py-3"><a class="btn btn-second btn-round" href="">Get started</a></p>
              </div>
            </div>


            <div class="col-md-4">
              <div class="pricing-1">
                <p class="plan-name">PLATINUM</p>
                <br>
                <h2 class="price">
                  <span class="price-unit">$</span>
                  <span data-bind-radio="pricing-1">69</span>
                </h2>
                <p class="small text-lighter">
                  Billed
                  <span data-bind-radio="pricing-1">monthly</span>
                </p>

                <div class="text-muted">
                  <p class="mb-1"><i class="ti-check text-three mr-2"></i> Gold Plan with iOS app and Android app</p>
        <p class="mb-1"><i class="ti-check text-three mr-2"></i> Personal eCommerce consultant</p>
        <p class="mb-1"><i class="ti-check text-three mr-2"></i>Store setup cost included</p>
        <p class="mb-1"><i class="ti-check text-three mr-2"></i>  Coupon code promotions</p>
        <p class="mb-1"><i class="ti-check text-three mr-2"></i>   Digital marketing</p>
        <p class="mb-1"><i class="ti-check text-three mr-2"></i>  Integrated SSL</p> 
                </div>

                <br>
                <p class="text-center py-3"><a class="btn btn_theme_light btn-round" href="">Get started</a></p>
              </div>
            </div>

          </div>


        </div>
      </section>




     
     
      <!--
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      | CTA
      |‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒
      
      <section class="section text-white py-8">
        <canvas class="overlay opacity-95" data-granim="#667eea,#764ba2,#00cdac,#3cba92"></canvas>

        <div class="container">
          <header class="section-header">
            <small><strong>Own it</strong></small>
            <h2 class="display-3 fw-400">Get It <span class="mark-underline">Now</span></h2>
            <hr>
            <p class="lead-2">If you have made your decision to purchase this template, go ahead and press on the following button to get a license in less than a minute.</p>
          </header>

          <p class="text-center">
            <a class="btn btn-xl btn-round btn-light w-250" href="">Purchase for <span class="pl-2">$19</span></a><br>
            <small><a class="text-white opacity-80" href="">or purchase an Extended License</a></small>
          </p>
        </div>
      </section>!-->

<?php  $this->load->view('Template/footer');?>