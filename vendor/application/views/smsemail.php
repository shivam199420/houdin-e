<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/settingsidebar.php') ?>
<style>
.button_box
{
  background: #4f8fb6;
border-radius: 3px;
text-align: center;
padding: 12px;
}
.button_box h4
{
  font-size: 17px;
color: #fff;

}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!--START Page-Title -->
            <div class="row">
          
                <div class="col-md-8">
                   <h4 class="page-title">SMS & Email</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Setting</a></li>
                  <li class="active">SMS & Email</li>
                  </ol>
                  </div>
                 
            </div>
           <!--END Page-Title -->  
    <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>
          <div class="row m-t-20">
            <div class="col-md-12">
              <div class="col-md-3">
                <div class="button_box" data-toggle="modal" data-target="#buy_sms">
                  <h4><span class="m-r-5"><i class="fa fa-wpforms"></i></span>Buy sms credit</h4>
                </div>
              </div>
              <div class="col-md-3">
                <div class="button_box" data-toggle="modal" data-target="#buy_email">
                  <h4><span class="m-r-5"><i class="fa fa-envelope-open"></i></span>Buy email credit</h4>
                </div>
              </div>
              <div class="col-md-3">
                <a href="<?php echo base_url(); ?>Setting/smslogs">
                <div class="button_box">
                  <h4><span class="m-r-5"><i class="fa fa-window-restore"></i></span>View sms log</h4>
                </div>
              </a>
              </div>
              <div class="col-md-3">
                <a href="<?php echo base_url(); ?>Setting/emaillogs">
                <div class="button_box">
                  <h4><span class="m-r-5"><i class="fa fa-sticky-note-o"></i></span>View email log</h4>
                </div>
              </a>
              </div>
            </div>
          </div>
            <div class="row m-t-20">
	                    <div class="col-lg-4 col-sm-6">
	                        <div class="widget-panel widget-style-2 bg-white">
	                            <img src="<?php echo base_url(); ?>assets/images/allpageicon/sms-credit.png">
	                            <h2 class="m-0 text-dark counter font-600"><?=$my_sms_remaing[0]->houdinv_emailsms_stats_remaining_credits?></h2>
	                            <div class="text-muted m-t-5">Total Remaining SMS Credit </div>
	                        </div>
	                    </div>
	                    <div class="col-lg-4 col-sm-6">
	                        <div class="widget-panel widget-style-2 bg-white">
	                            <img src="<?php echo base_url(); ?>assets/images/allpageicon/email-credit.png">
	                            <h2 class="m-0 text-dark counter font-600"><?=$my_email_remaing[0]->houdinv_emailsms_stats_remaining_credits?></h2>
	                            <div class="text-muted m-t-5">Total Remaining Email Credit </div>
	                        </div>
	                    </div>

	                  </div>
                      </div>
                    </div>
                  </div>





                  <div id="buy_sms" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >

        <div class="modal-dialog">
         <?php echo form_open(base_url('Setting/buysmsadd'), array( 'id' => 'buysmsadd', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

       <h4 class="modal-title">Buy SMS Credit</h4>

        </div>

        <div class="modal-body">


        <div class="row">
        <div class="col-md-12">

        <div class="form-group">

        <label for="field-7" class="control-label">Choose Package</label>

        <select class="form-control required_validation_for_add_custo" name="sms_pakage">
        <option value="">Choose Type</option>
        <?php 
    foreach ($sms_pakege as $key => $sms_pakeges) { ?>
        <option value="<?=$sms_pakeges->houdin_sms_package_id?>"><?=$sms_pakeges->houdin_sms_package_name?> ( <?=$sms_pakeges->houdin_sms_package_sms_count?> SMS) ( <?=$sms_pakeges->houdin_sms_package_price?> price)</option>
    <?php } ?>
        </select>

        </div>




        </div>
        </div>

        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="submit">

        </div>

        </div><?php echo form_close(); ?>

        </div>

        </div>
        <div id="buy_email" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >

<div class="modal-dialog">
 <?php echo form_open(base_url('Setting/buyemailadd'), array( 'id' => 'buyemailadd', 'method'=>'POST','enctype'=> 'multipart/form-data' ));?>
<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

<h4 class="modal-title">Buy Email Credit</h4>

</div>

<div class="modal-body">


<div class="row">
<div class="col-md-12">

<div class="form-group">

<label for="field-7" class="control-label">Choose Package</label>

<select class="form-control required_validation_for_add_custo" name="email_pakeges">
<option value="">Choose Type</option>
 <?php 
    foreach ($email_pakege as $key => $email_pakeges) {
       
        ?>
        <option value="<?=$email_pakeges->houdin_email_package_id?>"><?=$email_pakeges->houdin_email_package_name?> ( <?=$email_pakeges->houdin_email_package_email_count?> SMS) ( <?=$email_pakeges->houdin_email_package_price?> price)</option>
    <?php } ?>
</select>

</div>




</div>
</div>

</div>

<div class="modal-footer">

<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

<input type="submit" class="btn btn-info " name="" value="Submit">

</div>

</div><?php echo form_close(); ?>

</div>

</div>
<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">  
$(document).ready(function(){
 $('#buyemailadd').submit(function(e){
var check_required_field='';
      $(this).find(".required_validation_for_add_custo").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {  e.preventDefault();  
        return false;
      }else{ return true;} 
    });  
$('#buysmsadd').submit(function(e){
var check_required_field='';
      $(this).find(".required_validation_for_add_custo").each(function(){
        var val22 = $(this).val();
        
        if (!val22){
          check_required_field =$(this).size();
          $(this).css("border-color","#ccc");
          $(this).css("border-color","red");
        }
        $(this).on('keypress change',function(){
          $(this).css("border-color","#ccc");
        });
      });
      // alert(check_required_field);
      if(check_required_field)
      {  e.preventDefault();  
        return false;
      }else{ return true;} 
    });  


 }); 

</script>

