<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header'); ?>

<!-- Page item Area -->
<div id="page_item_area">
  <div class="page-location">
      <div class="page-location-layer">
          <div class="container">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="page-location-title">
                          <h3>Change Password</h3>
                      </div>
                      <nav>
                          <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                              <li class="breadcrumb-item active" aria-current="page">Change Password</li>
                          </ol>
                      </nav>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>


<!-- Login Page -->
<div class="user-area">
<div class="container">
<div class="row">
<div class="col-md-2 col-sm-2"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
<div class="create_account_area">
<?php
if($this->session->flashdata('error'))
{
    echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
}
else if($this->session->flashdata('success'))
{
    echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
}
?>
<div class="caa_form_area">
<div class="caa_form_group">
<?php echo form_open(base_url( 'Changepassword/updatepass' ), array( 'id' => 'changepass', 'method'=>'post' ));?>
<div class="login-area">
<div class="form-group">
<label>Current Password</label>
<input type="password" name="currentPass" class="required_validation_for_change_pass name_validation form-control" />
</div>
<div class="form-group">
<label>New Password</label>
<input type="password" name="newpass" class="required_validation_for_change_pass name_validation opass form-control" />
</div>
<div class="form-group">
<label>Confirm Password</label>
<input type="password" name="newconfirmpass" class="required_validation_for_change_pass name_validation cpass form-control" />
</div>
<div class="form-group">
    <input type="submit" class="btn porduct-place-order-btn acc_btn text-center setDisableData" value="update Password"/>
</div>
</div>
<?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/footer'); ?>

<script type="text/javascript">
$(document).ready(function(){
    $(document).on('submit','#changepass',function(){
        var check_required_field='';
        $(this).find(".required_validation_for_change_pass").each(function(){
            var val22 = $(this).val();
            if (!val22){
                check_required_field ='error';
                $(this).css("border-color","#ccc");
                $(this).css("border-color","red");
            }
            $(this).on('keypress change',function(){
                $(this).css("border-color","#ccc");
            });
        });
        if(check_required_field)
        {
            return false;
        }
        else {
            return true;
        }
    });
});
</script>
