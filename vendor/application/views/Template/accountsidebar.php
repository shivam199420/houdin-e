<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
<?php
$getDatabaseData = switchDynamicDatabase();
$this->db = $this->load->database($getDatabaseData,true);
  if($this->session->userdata('vendorRole')==1)
    {
$sidebar_permission =  $this->db->select("*")->from("houdinv_staff_management_sub")
    ->join("houdinv_staff_management","houdinv_staff_management.staff_id=houdinv_staff_management_sub.staff_id","right outer")
    ->where("houdinv_staff_management.houdinv_staff_auth_token",$this->session->userdata('vendorAuth'))
    ->get()->row();
    }
    else
    {
        $sidebar_permission = '';
    }
    
  //  print_R($sidebar_permission);
?>
        <div id="sidebar-menu">
            <ul>
            <li class="has_sub">
                    <a href="javascript:;" class="waves-effect <?php if($this->router->fetch_class()=="accounts"){echo 'active';}?>"><i class="fa fa-th-list"></i> <span> Accounting </span><span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url(); ?>Accounts">Dashboard</a></li>
                        <li><a href="<?php echo base_url(); ?>Accounts/accounting">Accounting</a></li>
                        <li><a href="<?php echo base_url(); ?>transaction">Transaction</a></li>
                        <li><a href="<?php echo base_url(); ?>Accounts/expence">Expense</a></li>
                        <li><a href="<?php echo base_url(); ?>Accounts/reports">Basic Reports</a></li>
                        <li><a href="<?php echo base_url(); ?>Accounts/taxreport">Tax Report</a></li>
                        <li><a href="<?php echo base_url(); ?>Accounts/balancesheet">Balance Sheet</a></li>
                        <li><a href="<?php echo base_url(); ?>Accounts/customreports">Custom Summary Reports</a></li>
                        <li><a href="<?php echo base_url(); ?>Accounts/profitlossreports">Profit & Loss Peport/A/C</a></li>
                        
                        <li><a href="<?php echo base_url(); ?>Accounts/profitlosspercentagereports">Profit Loss Percentage</a></li>
                    </ul>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
