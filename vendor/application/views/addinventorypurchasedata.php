<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
             <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">Add Inventory Purchase</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">Inventory</a></li>
                  <li><a href="<?php echo base_url(); ?>inventory/addinventory">Add Inventory</a></li>
                 
                  <li class="active">Add Inventory Purchase</li> 
                  </ol>  
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->
                
            </div>
           <!--END Page-Title --> 

          
            
             <div class="row m-t-20">
              <div class="col-lg-12">
                <div class="card-box">
                   <form action="#" data-parsley-validate novalidate>
                     <div class="form-group">
                      <label for="userName">Supplier</label>
                     <select class="form-control select2">
                        <option>Select</option>
                        <optgroup label="Alaskan/Hawaiian Time Zone">
                            <option value="AK">Alaska</option>
                            <option value="HI">Hawaii</option>
                        </optgroup>
                      </select>
                     </div>
                   </form>
                </div>
              </div>
              
              
            </div>
                 
        <div class="row">
          <div class="col-md-12">

            <div class="card-box table-responsive">
             
              <table class="table table-striped table-bordered table_shop_custom">
                <thead>
                  <tr>
                    <th><input type="checkbox"></th>
                    <th>Product name</th>
                    <th>Quantity</th>
                  </tr>
                 
                </thead>
                <tbody>
                  <tr>
                    <td><input type="checkbox"></td>
                    <td>[Sample] Electronics</td>
                    <td>123654</td> 
                  </tr>
                  <tr>
                    <td><input type="checkbox"></td>
                    <td>[Sample] Electronics</td>
                    <td>123654</td> 
                  </tr>
                  <tr>
                    <td><input type="checkbox"></td>
                    <td>[Sample] Electronics</td>
                    <td>123654</td> 
                  </tr>
                </tbody>
              </table>
              <div class="pull-right"><button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#">Create purchase order</button>
              <button class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#">Create purchase order & Send Email</button></div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>


<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){

    $(".select2").select2();
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});
</script>
