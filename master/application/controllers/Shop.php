<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Shop extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        if($this->session->userdata('role') != 0)
        {
            $this->load->helper('access');
            $getData = access($this->session->userdata('department'));
            if(!in_array('shop',$getData))
            {
                $this->load->helper('url');
                redirect(base_url()."dashboard", 'refresh');
            }
        }
        
        $this->load->model('Shopmodel');
        $this->load->model('Resetdatabase');
		$this->load->library('pagination');
        $this->perPage = 100;
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form = fromemail();
        $this->vendor = getvendorurl();
	}   
    public function index()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // delete Shop Data
        if($this->input->post('deleteShopData'))
        {
            $this->form_validation->set_rules('deleteShopId','text','required');
            if($this->form_validation->run() == true)
            {
                $getDeleteStatus = $this->Shopmodelp->deleteShopData($this->input->post('deleteShopId'));
                if($getDeleteStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Shop and shop user deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."shop", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong.Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."shop", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong.Please try again');
                $this->load->helper('url');
                redirect(base_url()."shop", 'refresh');
            }
        }
        // update shop status
        if($this->input->post('updateShopStatus'))
        {
            $this->form_validation->set_rules('chnageShopStatusId','text','required');
            $this->form_validation->set_rules('changeShopStatus','text','required|in_list[active,deactive,block]');
            if($this->form_validation->run() == true)
            {
                $setUpdateArray = array('id'=>$this->input->post('chnageShopStatusId'),'shopStatus'=>$this->input->post('changeShopStatus'));
                $getshopStatus = $this->Shopmodel->updateShopStatusData($setUpdateArray);
                if($getshopStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Status updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."shop", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."shop", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fileds are mandatory');
                $this->load->helper('url');
                redirect(base_url()."shop", 'refresh');
            }
        }
        // send email to shop user
        if($this->input->post('sendShopEmail'))
        {
            $getsendgrid = sendgrid();
            $this->form_validation->set_rules('shopUserEmail','text','required|valid_email');
            $this->form_validation->set_rules('subject','text','required');
            $this->form_validation->set_rules('userMessage','text','required');
            if($this->form_validation->run() == true)
            {
                $url = 'https://api.sendgrid.com/';
                $user = $getsendgrid[0]->houdin_sendgrid_username;
                $pass = $getsendgrid[0]->houdin_sendgrid_password;
                $json_string = array(
                'to' => array(
                    $getForgetUpdate['emaildata']
                ),
                'category' => 'test_category'
                );
                $htm = $this->input->post('userMessage');
                $params = array(
                    'api_user'  => $user,
                    'api_key'   => $pass,
                    'x-smtpapi' => json_encode($json_string),
                    'to'        => $this->input->post('shopUserEmail'),
                    'fromname'  => 'Houdin-e',
                    'subject'   => $this->input->post('subject'),
                    'html'      => $htm,
                    'from'      => $this->form,
                );
                $request =  $url.'api/mail.send.json';
                $session = curl_init($request);
                curl_setopt ($session, CURLOPT_POST, true);
                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                curl_setopt($session, CURLOPT_HEADER, false);
                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($session);
                curl_close($session);
                $getResponse = json_decode($response);
                if($getResponse->message == 'success')
                {
                    $this->session->set_flashdata('success','Email send successfully');
                    $this->load->helper('url');
                    redirect(base_url()."shop", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."shop", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','All fileds are mandatory');
                $this->load->helper('url');
                redirect(base_url()."shop", 'refresh');
            }
        }
        // advanve search data
        if($this->input->post('shopSearchButton'))
        {
            $setSearchArray = array('name'=>$this->input->post('searchShopName'),'email'=>$this->input->post('searchShopEmail'),'phone'=>$this->input->post('searchShopPhone'),'status'=>$this->input->post('searchShopStatus'));
            $getDataValue = $this->Shopmodel->advanceSearchData($setSearchArray);
            $this->session->set_flashdata('shopSearch',$getDataValue['searchList']);
            $this->load->helper('url');
            redirect(base_url()."shop", 'refresh');
        }
        // update shop package
        if($this->input->post('updateShopPackage'))
        {
            $this->form_validation->set_rules('upgardePackageId','text','required');
            if($this->input->post('choosePackage') != 0)
            {
                $this->form_validation->set_rules('packagePrice','text','required');
            }
            $this->form_validation->set_rules('choosePackage','text','required');
            $this->form_validation->set_rules('expiryDate','text','required');
            if($this->form_validation->run() == true)
            {
                $setPackageData = $this->input->post('choosePackage');
                $setUpdateArray = array('houdin_users_package_id'=>$setPackageData,'houdin_users_package_expiry'=>$this->input->post('expiryDate'));
                if($setPackageData != 0)
                {
                    $setTransactionId = 'TXN'.rand(99999999,10000000);
                    $setTransactionArray = array(
                        'houdin_admin_transaction_transaction_id'=>$setTransactionId,
                        'houdin_admin_transaction_from'=>'cash',
                        'houdin_admin_transaction_type'=>'credit',
                        'houdin_admin_transaction_for'=>'package',
                        'houdin_admin_transaction_for_id'=>$this->input->post('upgardePackageId'),
                        'houdin_admin_transaction_amount'=>$this->input->post('packagePrice'),
                        'houdin_admin_transaction_status'=>'success',
                        'houdin_admin_transaction_date'=>date('Y-m-d'));
                }
                else
                {
                    $setTransactionArray = array();
                }
                $getsetData = $this->Shopmodel->updateShopPackage($setUpdateArray,$setTransactionArray,$this->input->post('upgardePackageId'));
                if($getsetData['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Package updated successfully');
                    $this->load->helper('url');
                    redirect(base_url()."shop", 'refresh');
                }
                else if($getsetData['message'] == 'tarns')
                {
                    $this->session->set_flashdata('error','Package updated successfully.Transaction is not updated');
                    $this->load->helper('url');
                    redirect(base_url()."shop", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."shop", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."shop", 'refresh');
            }
        }
        $getTotalCount = $this->Shopmodel->getTotalShopCount();
        $config['base_url']    = base_url().'shop/';
		$config['uri_segment'] = 2;
		$config['total_rows']  = $getTotalCount['totalRows'];
		$config['per_page']    = $this->perPage;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
		$config['cur_tag_close'] = '</a></li>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['next_tag_open'] = '<li class="pg-next">';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li class="pg-prev">';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(2);
		$offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getShopDetailsData = $this->Shopmodel->fetchShopDetailsData($setArray);
        $this->load->view('shopdata',$getShopDetailsData);
    }


public function resetaccount(){
if($this->input->post('resetaccountId')){
$dbnameget =$this->Resetdatabase->fetchShopDetailDB($this->input->post('resetaccountId'));
if($dbnameget->houdin_user_shop_db_name){
    


    $Resetdatabase =$this->Resetdatabase->Resetdatabase_data($dbnameget->houdin_user_shop_db_name); 

if($Resetdatabase){

    $this->session->set_flashdata('success','Reset Account successfully update');
    $this->load->helper('url');
    redirect(base_url()."shop", 'refresh');
}else{
    $this->session->set_flashdata('error','Something went wrong. Please try again');
    $this->load->helper('url');
    redirect(base_url()."shop", 'refresh');
}
}else{
   
    $this->session->set_flashdata('error','Something went wrong. Please try again');
$this->load->helper('url');
redirect(base_url()."shop", 'refresh');
}

 
}else{

$this->session->set_flashdata('error','Something went wrong. Please try again');
$this->load->helper('url');
redirect(base_url()."shop", 'refresh');
}

}


    public function updateshopcredentials()
    {
        $getUserEmail = $this->input->post('uemail');
        $getUserPass = $this->input->post('upass');
        $this->form_validation->set_rules('uemail','email','required');
        $this->form_validation->set_rules('uiddata','id','required');
        if($this->form_validation->run() == true)
        {
            $getUserEmail = $this->input->post('uemail');
            $getUserPass = $this->input->post('upass');
            $getResponse = $this->Shopmodel->updateshopcredentialsData(array('email'=>$getUserEmail,'pass'=>$getUserPass,'id'=>$this->input->post('uiddata')));
            $this->session->set_flashdata($getResponse['response'],$getResponse['message']);
            redirect(base_url()."Shop",'refresh');
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."Shop",'refresh');
        }
    }
    public function add()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        // create shop
        if($this->input->post('addShopData'))
        {
            $this->form_validation->set_rules('userName','user name','required');
            $this->form_validation->set_rules('userContact','user contact','required');
            $this->form_validation->set_rules('stroeName','store name','required|is_unique[houdin_user_shops.houdin_user_shop_shop_name]');
            $this->form_validation->set_rules('useremail','email','required|valid_email|is_unique[houdin_users.houdin_user_email]');
            $this->form_validation->set_rules('address','address','required');
            $this->form_validation->set_rules('city','city','required');
            $this->form_validation->set_rules('country','country','required');
            $this->form_validation->set_rules('sellingStatus','already selling','required|in_list[yes,no]');
            $this->form_validation->set_rules('businessCategory','business category','required');
            $this->form_validation->set_rules('language','langugae','required');
            $this->form_validation->set_rules('currency','currency','required');
            
            if($this->form_validation->run() == true)
            {
                $letters1='abcdefghijklmnopqrstuvwxyz'; 
                $string1=''; 
                for($x=0; $x<3; ++$x)
                {  
                    $string1.=$letters1[rand(0,25)].rand(0,9); 
                }
                $getShopPassword = $string1;
                $saltdata = password_hash($string1,PASSWORD_DEFAULT);
                $pass = crypt($getShopPassword,$saltdata);
                $setdata = strtotime(date('Y-m-d H:i:s'));
                if($this->input->post('plan') == 0)
                {
                    $setExpiryDate = $getExpiryDate = date('Y-m-d', strtotime('+20 days', strtotime(date('Y-m-d'))));
                }
                else
                {
                    $setExpiryDate = $getExpiryDate = date('Y-m-d', strtotime('+365 days', strtotime(date('Y-m-d'))));
                }
                // user array
                $setUserArray = array('houdin_user_name'=>$this->input->post('userName'),
                'houdin_user_email'=>$this->input->post('useremail'),
                'houdin_user_password'=>$pass,
                'houdin_user_password_salt'=>$saltdata,
                'houdin_users_package_id'=>$this->input->post('plan'),
                'houdin_users_package_expiry'=>$setExpiryDate,
                'houdin_user_contact'=>$this->input->post('userContact'),
                'houdin_user_address'=>$this->input->post('address'),
                'houdin_user_city'=>$this->input->post('city'),
                'houdin_user_country'=>$this->input->post('country'),
                'houdin_users_currency'=>$this->input->post('currency'),
                'houdin_user_registration_status'=>'1',
                'houdin_user_is_verified'=>'1',
                'houdin_user_is_active'=>'active',
                'houdin_user_created_at'=>$setdata);
                // shop array
                $setShopArray = array(
                    'houdin_user_shop_shop_name'=>$this->input->post('stroeName'),
                    'houdin_user_shop_country'=>$this->input->post('country'),
                    'houdin_user_shops_category'=>$this->input->post('businessCategory'),
                    'houdin_user_shops_language'=>$this->input->post('language'),
                    'houdin_user_shop_active_status'=>'active',
                    'houdin_user_shop_created_at'=>$setdata,'accounttype'=>$this->input->post('accounttype'));
                    $getShopStatus = $this->Shopmodel->addUserdata($setUserArray,$setShopArray);
                    if($getShopStatus['message'] == 'yes')
                    {
                        $getsendgrid = sendgrid();
                        $getLogo = logo();
                        // send email data
                        $url = 'https://api.sendgrid.com/';
                        $user = $getsendgrid[0]->houdin_sendgrid_username;
                        $pass = $getsendgrid[0]->houdin_sendgrid_password;
                        $json_string = array(
                        'to' => array(
                            $this->input->post('useremail')
                        ),
                        'category' => 'test_category'
                        );
                        $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                    <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                                    <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                                    <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                                        <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                            <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                                            <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                                            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                            <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                                                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Hello '.$this->input->post('userName').' </strong></td></tr>
                                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Your shop details are:</strong></td></tr>
                                                    
                                                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Login URL:</td>
                                                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->vendor.'</td>
                                                    </tr>
                                                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Username:</td>
                                                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$this->input->post('useremail').'</td>
                                                    </tr>
                                                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Password:</td>
                                                    <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$getShopPassword.'</td>
                                                    </tr>
                                                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing houdin-e.</td></tr></table></td></tr></table>
                                <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                                <table width="100%" ><tr >
                                <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;Houdin-e '.date('Y').'--All right reserverd </td></tr>
                                <tr><td style=""><img src="'.base_url().'assets/images/Icon1.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/Icon2.png" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;<img src="'.base_url().'assets/images/icon3.png" style="max-height: 25px;height:100%;"/></td>
                                </tr></table>
                                </div>
                                </div></td></tr></table>';
                        $params = array(
                            'api_user'  => $user,
                            'api_key'   => $pass,
                            'x-smtpapi' => json_encode($json_string),
                            'to'        => $this->input->post('useremail'),
                            'fromname'  => 'Houdin-e',
                            'subject'   => 'Shop Details',
                            'html'      => $htm,
                            'from'      => $this->form,
                        );
                        $request =  $url.'api/mail.send.json';
                        $session = curl_init($request);
                        curl_setopt ($session, CURLOPT_POST, true);
                        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                        curl_setopt($session, CURLOPT_HEADER, false);
                        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($session);
                        curl_close($session);
                        $getResponse = json_decode($response);
                        if($getResponse->message == 'success')
                        {
                            $this->session->set_flashdata('success','Shop addedd successfully');
                            $this->load->helper('url');
                            redirect(base_url()."Shop", 'refresh');
                        }
                        else
                        {
                            $this->session->set_flashdata('success','Shop addedd successfully.Email not sent');
                            $this->load->helper('url');
                            redirect(base_url()."Shop", 'refresh');
                        }
                    }
                    else if($getshopStatus['message'] == 'shop')
                    {
                        $this->session->set_flashdata('error','User addedd successfully. Something went wrong during shop creation');
                        $this->load->helper('url');
                        redirect(base_url()."Shop/add", 'refresh');
                    }
                    else if($getshopStatus['message'] == 'phone')
                    {
                        $this->session->set_flashdata('error','Contact number is already exist. Please try again with different number.');
                        $this->load->helper('url');
                        redirect(base_url()."Shop/add", 'refresh');
                    }
                    else if($getshopStatus['message'] == 'email')
                    {
                        $this->session->set_flashdata('error','Email address is already exist. Please try again with different number.');
                        $this->load->helper('url');
                        redirect(base_url()."Shop/add", 'refresh');
                    }
                    else
                    {
                        $this->session->set_flashdata('error','Something went wrong. Please try again');
                        $this->load->helper('url');
                        redirect(base_url()."Shop/add", 'refresh');
                    }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."Shop/add", 'refresh');
            }
        }
        $getShopBasicDetails = $this->Shopmodel->getShopDataValue();
        $this->load->view('addshop',$getShopBasicDetails);
    }
    public function view()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        $getShopId = $this->uri->segment('3');
        if(!$getShopId)
        {
            $this->load->helper('url');
            redirect(base_url()."Shop", 'refresh');
        }
        $getShopBasicInfo = $this->Shopmodel->fetchShopInfo($getShopId);
        $this->load->view('shopview',$getShopBasicInfo);
    }
    // generate shop csv
    public function generateCsvdata()
    {
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        $getCsvData = $this->Shopmodel->fetchShopCSVData();
        if(count($getCsvData) > 0)
        { 
            $delimiter = ","; 
            $filename = "shops_" . date('Y-m-d') . ".csv";
            $f = fopen('php://memory', 'w');
            $fields = array('Shop id', 'Shop Name', 'Shop Owner Name', 'Shop Owner Email', 'Shop Email', 'Shop Contact','Shop Website','Shop Status','Shop Created At');
            fputcsv($f, $fields, $delimiter);
            foreach($getCsvData as $getShopCsvData){
                $getCreatedDate = date('d-m-Y',$getShopCsvData->houdin_user_shop_created_at);
                if($getShopCsvData->houdin_user_shop_email != "")
                {
                    $setShopEmail = $getShopCsvData->houdin_user_shop_email;
                }
                else
                {
                    $setShopEmail = "No Shop Email";
                }
                if($getShopCsvData->houdin_user_shop_phone != "")
                {
                    $setShopPhone = $getShopCsvData->houdin_user_shop_phone;
                }
                else
                {
                    $setShopPhone = "NO Shop Phone";
                }
                if($getShopCsvData->houdin_user_shop_website != "")
                {
                    $setShopWebsite = $getShopCsvData->houdin_user_shop_website;
                }
                else
                {
                    $setShopWebsite = "No Shop Website";
                }
                $lineData = array($getShopCsvData->houdin_user_shop_id,$getShopCsvData->houdin_user_shop_shop_name,$getShopCsvData->houdin_user_name,$getShopCsvData->houdin_user_email,$setShopEmail,$setShopPhone,$setShopWebsite,$getShopCsvData->houdin_user_shop_active_status,$getCreatedDate);
                fputcsv($f, $lineData, $delimiter);
            }
            fseek($f, 0);
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            fpassthru($f);
        }
    }
}
