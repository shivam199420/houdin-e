$(document).ready(function() {
    "use strict";


    // Start cart list
    var hide = $('.hide');
    var listBtn = $('.cart-list');
    var cartList = $('.cart-wrapper');

    listBtn.on('click', function() {
        cartList.toggleClass('hide');
    });

    // slicknav
    $('ul#navigation').slicknav({
        prependTo: ".responsive-menu-wrap"
    });

    // Initialize Swiper
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        speed: 700,
        autoplay: {
            delay: 3000,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });


    // all products
    $('.all-product-list').owlCarousel({
        loop: true,
        dots: false,
        margin: 15,
        autoplay: true,
        autoplayTimeout: 3000,
        smartSpeed: 900,
        nav: true,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: { items: 1 },
            600: { items: 2 },
            1000: { items: 4 }
        }
    });

    // new arrival
    $('.new-arrival').owlCarousel({
        loop: true,
        dots: false,
        margin: 15,
        autoplay: true,
        autoplayTimeout: 3000,
        smartSpeed: 900,
        nav: true,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: { items: 1 },
            600: { items: 2 },
            1000: { items: 2 }
        }
    });

    // new arrival
    $('.gym-new-arrival').owlCarousel({
        loop: true,
        dots: false,
        margin: 15,
        autoplay: true,
        autoplayTimeout: 3000,
        smartSpeed: 900,
        nav: true,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: { items: 1 },
            600: { items: 2 },
            1000: { items: 3 }
        }
    });


    // Discount products
    $('.discount-products').owlCarousel({
        loop: true,
        dots: false,
        margin: 15,
        autoplay: true,
        autoplayTimeout: 3000,
        smartSpeed: 900,
        nav: true,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: { items: 1 },
            600: { items: 2 },
            1000: { items: 2 }
        }
    });

    // testimonial
    $('.testimonial-list').owlCarousel({
        loop: true,
        dots: false,
        margin: 15,
        autoplay: true,
        autoplayTimeout: 3000,
        smartSpeed: 1000,
        nav: true,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: { items: 1 },
            600: { items: 1 },
            1000: { items: 1 }
        }
    });

    // Flex slider

    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails"
    });


    // Start color demo
    $('#color1').on('click', function(){
        $('link').removeAttr('href');
        $('link').attr('href', 'css/colors/color1.css');
    });

    $(window).on('load', function() {
        /*-- preloader ---*/
        $('#popup-subscribe').modal('show');
    });



































});