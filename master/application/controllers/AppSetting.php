<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AppSetting extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        if($this->session->userdata('role') != 0)
        {
            $this->load->helper('access');
            $getData = access($this->session->userdata('department'));
            if(!in_array('setting',$getData))
            {
                $this->load->helper('url');
                redirect(base_url()."dashboard", 'refresh');
            }
        }
        $this->load->model('Categorymodel');
        $this->load->helper('url');
        $this->load->helper('setting');
		$this->load->library('pagination');
        $this->perPage = 100;
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->Validconfigs = array(
        array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
        ),
        array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
              )
            );
        $get_user = getUserId($this->session->userdata('userAuth'));
        $user_id = $get_user[0]->houdin_user_auth_user_id;
    }
/**
 * businesscategory()
 * @purpose: to manager app business category
 * @created by: Shivam Singh Sengar
 * @created at: 08 Dec 2019
 * @param NONE
 * @return ARRAY ( mixed )
 */
public function businesscategory()
{
    if($this->session->userdata('userAuth') == "") 
    {
        $this->load->helper('url');
        redirect(base_url(), 'refresh');
    }
    if($this->input->post('Add'))
    {
        $this->form_validation->set_rules($this->Validconfigs);
        if($this->form_validation->run() != true)
        {
            $this->session->set_flashdata('message_name',validation_errors() );
            redirect(base_url().'AppSetting/businesscategory?', 'refresh');    
        }
        $date = strtotime(date("Y-m-d, h:i:s"));
        $array = array("name"=>$this->input->post('name'),"description"=>$this->input->post('description'),
        "status"=>$this->input->post('status'),"created_at"=>$date,
        "updated_at"=>$date,'created_by' => $user_id);
        $data = $this->Categorymodel->appCategoryAdd($array);
        if($data)
        {
            $this->session->set_flashdata('success',"App Category added successfully");
            redirect(base_url().'AppSetting/businesscategory?', 'refresh');                 
        }
    }     
    if($this->input->post('Edit'))
    {
        $this->form_validation->set_rules($this->Validconfigs);
        if($this->form_validation->run() != true)
        {
            $this->session->set_flashdata('message_name',validation_errors() );
            redirect(base_url().'AppSetting/businesscategory?', 'refresh');    
        }
        $date = strtotime(date("Y-m-d, h:i:s"));
        $array = array("name"=>$this->input->post('name'),"description"=>$this->input->post('description'),
        "status"=>$this->input->post('status'),
        "updated_at"=>$date,"updated_by" => $user_id);
        $data = $this->Categorymodel->appCategoryEdit($array,$this->input->post('Edit_id'));
        if($data)
        {
            $this->session->set_flashdata('success',"App Category updated successfully");
            redirect(base_url().'AppSetting/businesscategory?', 'refresh');                 
        }
    }   
    if($this->input->post('delete'))
    {
        if($this->input->post('delete_input'))
        {
            $data = $this->Categorymodel->appCategoryDelete($this->input->post('delete_input'));
            if($data)
            {
                $this->session->set_flashdata('success',"App Category deleted successfully");
                redirect(base_url().'AppSetting/businesscategory?', 'refresh');                 
            }  
        }
    }
    // fetch all category data
    $Allcategory= array();
    //get rows count
    $conditions['returnType'] = 'count';
    $totalRec = $this->Categorymodel->getAllAppCategory($conditions);
    //pagination config
    $config['base_url']    = base_url().'AppSetting/businesscategory/';
    $config['uri_segment'] = 3;
    $config['total_rows']  = $totalRec;
    $config['per_page']    = $this->perPage;
    //styling
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
    $config['cur_tag_close'] = '</a></li>';
    $config['next_link'] = 'Next';
    $config['prev_link'] = 'Prev';
    $config['next_tag_open'] = '<li class="pg-next">';
    $config['next_tag_close'] = '</li>';
    $config['prev_tag_open'] = '<li class="pg-prev">';
    $config['prev_tag_close'] = '</li>';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    //initialize pagination library
    $this->pagination->initialize($config);
    //define offset
    $page = $this->uri->segment(3);
    $offset = !$page?0:$page;
    //get rows
    $conditions['returnType'] = '';
    $conditions['start'] = $offset;
    $conditions['limit'] = $this->perPage;
    $Allcategory['category'] = $this->Categorymodel->getAllAppCategory($conditions);
    $this->load->view('app_business_category',$Allcategory);
}
}
