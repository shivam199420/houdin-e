        <?php 
        $getShopName = getfolderName($this->session->userdata('shopName')); 
        $this->load->view($getShopName.'/Template/header'); ?>
        <!-- Page item Area -->
        <div id="page_item_area">
        <div class="container">
        <div class="row">
        <div class="col-sm-6 text-left">
        <h3>Account</h3>
        </div>			
        </div>
        </div>
        </div>
        <div class="row">
        <div class="container">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>
        </div>
        <!-- Login Page -->
        <div class="login_page_area">
        <div class="container">
        <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_open(base_url( 'Register/addShopUsers' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
        <div class="create_account_area">
        <h2 class="caa_heading">Create an account</h2>
        <div class="caa_form_area">
        <p>Please enter your email address to create an account.</p>
        <div class="caa_form_group">
        <div class="caf_form">
        <label>Firstname</label>
        <div class="input-area">
                <input type="text" class="required_validation_for_user name_validation characterValidation" name="userFirstName" placeholder="Enter Firstname" />
        </div>
        </div>
        <div class="caf_form">
        <label>Lastname</label>
        <div class="input-area">
                <input type="text" class="required_validation_for_user name_validation characterValidation" name="userLastName" placeholder="Enter Lastname"/>
        </div>
        </div>
        <div class="caf_form">
        <label>Email address</label>
        <div class="input-area">
                <input type="email" class="required_validation_for_user name_validation email_validation" name="userEmail" placeholder="Enter Emailaddress"/>
        </div>
        </div>
        <div class="caf_form">
        <label>Contact Number</label>
        <div class="input-area">
                <input type="text" class="required_validation_for_user number_validation name_validation" name="userContact" placeholder="Enter Contact Number"/>
        </div>
        </div>
        <div class="caf_form">
        <label>Password</label>
        <div class="input-area">
                <input type="password" class="required_validation_for_user name_validation opass" name="userPass" placeholder="Enter password"/>
        </div>
        </div>
        <div class="caf_form">
        <label>Conform password</label>
        <div class="input-area">
                <input type="password" class="required_validation_for_user name_validation cpass" name="userRetypePass" placeholder="Conform password" />
        </div>
        </div>
         
        <input type="submit" class="btn btn-default acc_btn setDisableData" type="submit" value="Create an account"/>
        </div>
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_open(base_url( 'Register/checkUserAuth' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
        <div class="create_account_area">
        <h2 class="caa_heading">Already registered?</h2>
        <div class="caa_form_area">
        <div class="caa_form_group">
        <div class="login_email">
        <label>Email address</label>
        <div class="input-area">
                <input type="email"  name="userEmail" class="required_validation_for_user name_validation email_validation" placeholder="Enter Email Address"/></div>
        </div>
        <div class="login_password">
        <label>Password</label>
        <div class="input-area">
                <input type="password" name="userPass" class="required_validation_for_user name_validation" placeholder="Enter Password"/></div>
        <p class="forgot_password">
        <a href="javascript:;" title="Recover your forgotten password" rel="" data-toggle="modal" data-target="#myModal">Forgot your password?</a>
        </p>
        <input type="submit" class="btn btn-default acc_btn" value="Login"/>
        </div>
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        </div>
        </div>		
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <?php echo form_open(base_url( 'Register/checkUserForgot' ), array( 'id' => 'addShopUsers', 'method'=>'post' ));?>
        <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Forgot your password</h4>
        <button type="button" class="close" data-dismiss="modal" style="float: right;">&times;</button>
        </div>
        <div class="modal-body">
        <div class="input-area">
        <input placeholder="Please enter your Email address" name="emailForgot" class="required_validation_for_user name_validation email_validation" type="text" style="width:100%;">
        </div>
        </div>
        <div class="modal-footer">
        <input type="submit" class="btn btn-default acc_btn" style="float: left" value="Submit"/>
        </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        <!-- modal end -->
        <?php 
        $getShopName = getfolderName($this->session->userdata('shopName')); 
        $this->load->view($getShopName.'/Template/footer'); ?>
        <!-- CLient side form validation -->
        <script type="text/javascript">
        $(document).ready(function(){
                $(document).on('submit','#addShopUsers',function(){
                        var check_required_field='';
                        $(this).find(".required_validation_for_user").each(function(){
                                var val22 = $(this).val();
                                if (!val22){
                                        check_required_field =$(this).size();
                                        $(this).css("border-color","#ccc");
                                        $(this).css("border-color","red");
                                }
                                $(this).on('keypress change',function(){
                                        $(this).css("border-color","#ccc");
                                });
                        });
                        if(check_required_field)
                        {
                                return false;
                        }
                        else {
                                return true;
                        }
                });
        });
                </script>