<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header") ?>
<style>
    .setDataValue
    {
        display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -moz-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    background-color: #eef1f8;
    border-top: 5px solid #e4c7a2;
    min-height: 55px;
    line-height: 1.6;
    padding: 5px 30px;
    margin-bottom: 30px;
    min-height: 175px;
    }
</style>
<section>
        <div class="container ">
            <div class="port-title py-85  py-tn-50">
                <div class="">
                    <div class="row justify-content-center">
                        <div class="col-md-8 text-center">
                            <h2 class="title">My Address</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container  py-tn-50">
            <div class="row">
            <div class="container">
            
            <div class="col-sm-12">
            <?php 
            if(count($userAddress) > 0)
            {
            ?>
            <a href="<?php echo base_url() ?>Address/add" class="process-button acc_btn text-center pull-right m-b-20">Add New Address</a>
            <?php } ?>
            </div>
            </div>
            </div>
            <div class="port-body">
            <?php 
        if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger' style='width:100%;'>".$this->session->flashdata('error')."</div>";
        }
        else if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success' style='width:100%;'>".$this->session->flashdata('success')."</div>";
        }
        ?>
            <?php 
            if(!count($userAddress) > 0)
            {
            ?>
                <div class="contact-form">
                    <div class="messages" id="status"></div>
                        <div class="col-md-12">
                            <div class="form-group au-form text-center">
                            <a href="<?php echo base_url() ?>Address/add" class="process-button acc_btn text-center" style="padding:10px">Add New Address</a>
                            </div>
                        </div>
                </div>
            <?php }
            else
            {
            ?>
            <!-- adress data -->
            <?php 
            foreach($userAddress as $userAddressList)
            {
             ?>
            <div class="col-sm-6 pull-left">
            <div class="setDataValue edit-adress-page" style="display:block !important">
              <div class="adress-top-dep"><div class="adress-top-left-dep col-md-8"><h3><?php echo $userAddressList->houdinv_user_address_name."(".$userAddressList->houdinv_user_address_phone.")" ?></h3></div>
                <div class="adress-top-right-dep">
                <a href="<?php echo base_url() ?>Address/edit/<?php echo $userAddressList->houdinv_user_address_id ?>" style="font-size: 15px;position: absolute;top: 5px;right: 49px;" class="text-success"><i class="fa fa-pencil"></i></a>
                &nbsp;&nbsp;
                <a href="<?php echo base_url() ?>Address/deleteAddress/<?php echo $userAddressList->houdinv_user_address_id ?>" class="text-danger" style="font-size: 15px;position: absolute;top: 5px;right: 29px;"><i class="fa fa-trash-o"></i></a>
           </div></div>
               
                <address><?php echo $userAddressList->houdinv_user_address_user_address ?>,<br>
                <?php echo $userAddressList->houdinv_user_address_city ?>,<br>
                <?php echo $userAddressList->houdinv_user_address_zip ?>
                </address>
                </div>
                </div>
             
            <?php }
            ?>
           <?php  } ?>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <?php 
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer") ?>