<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header") ?>
<style>
 
    .adress-top-dep{background: #ee7460;
        padding: 10px 0px;
    width: 100%;
    float: left;}
    .adress-top-left-dep h3{    margin-bottom: 0px;
    color: #fff;
    font-size: 17px;}
</style>
<div class="page-wrap">
   <div class="ps-section--hero"><img src="<?php echo base_url(); ?>Extra/Uploads/02.jpg" alt="">

        <div class="ps-section__content text-center">

          <h3 class="ps-section__title">My Address</h3>

          <div class="ps-breadcrumb">

            <ol class="breadcrumb">

              <li><a href="<?php echo base_url(); ?>">Home</a></li>

              <li class="active">Address</li>

            </ol>

          </div>

        </div>

      </div>
    <section>
        <div class="container  py-tn-50">
            <div class="row">
            <div class="container">
            
            <div class="col-sm-12">
            <?php 
            if(count($userAddress) > 0)
            {
            ?>
            <a href="<?php echo base_url() ?>Address/add" class="process-button acc_btn text-center pull-right m-b-20 border-btn">Add New Address</a>
            <?php } ?>
            </div>
            </div>
            </div>
            <div class="port-body">
            <?php 
        if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger' style='width:100%;'>".$this->session->flashdata('error')."</div>";
        }
        else if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success' style='width:100%;'>".$this->session->flashdata('success')."</div>";
        }
        ?>
            <?php 
            if(!count($userAddress) > 0)
            {
            ?>
                <div class="contact-form">
                    <div class="messages" id="status"></div>
                        <div class="col-md-12">
                            <div class="form-group au-form text-center">
                            <a href="<?php echo base_url() ?>Address/add" class="process-button acc_btn text-center border-btn" style="padding:10px">Add New Address</a>
                            </div>
                        </div>
                </div>
            <?php }
            else
            {
            ?>
            <!-- adress data -->
            <?php 
            foreach($userAddress as $userAddressList)
            {
             ?>
            <div class="col-sm-6 col-xs-12 pull-left">
            <div class="setDataValue edit-adress-page" style="display:block !important">
              <div class="adress-top-dep">
              <div class="adress-top-left-dep col-md-8 col-xs-8">
              <h3><?php echo $userAddressList->houdinv_user_address_name."(".$userAddressList->houdinv_user_address_phone.")" ?></h3>
              </div>
                <div class="adress-top-right-dep col-md-4 col-xs-4">
                <a href="<?php echo base_url() ?>Address/edit/<?php echo $userAddressList->houdinv_user_address_id ?>" class="text-success pull-right address-icon"><i class="fa fa-pencil"></i></a>
                &nbsp;&nbsp;
                <a href="<?php echo base_url() ?>Address/deleteAddress/<?php echo $userAddressList->houdinv_user_address_id ?>" class="text-danger pull-right address-icon" ><i class="fa fa-trash-o"></i></a>
           </div></div>
               
                <address class="back_address"><?php echo $userAddressList->houdinv_user_address_user_address ?>,<br>
                <?php echo $userAddressList->houdinv_user_address_city ?>,<br>
                <?php echo $userAddressList->houdinv_user_address_zip ?>
                </address>
                </div>
                </div>
             
            <?php }
            ?>
           <?php  } ?>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    </div>
    <?php 
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer") ?>