<?php
class SettingModel extends CI_Model
{
    function __construct()
    {  
        parent::__construct();
    }
    public function fetchMasterLogo()
    {
        $this->db->select('*')->from('houdin_admin_logo');
        $getLogoData = $this->db->get()->result();
        return array('masterLogo'=>$getLogoData);
    } 
    public function addAdminImageData($data)
    {
        $setDate = strtotime(date('Y-m-d h:i:s'));
        $setInsertData = array('houdin_admin_logo_image'=>$data['imageData'],'houdin_admin_logo_created_at'=>$setDate);
        $getInsertStatus = $this->db->insert('houdin_admin_logo',$setInsertData);
        if($getInsertStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function updateAdminImageData($data)
    {
        $setDate = strtotime(date('Y-m-d h:i:s'));
        $setUpdateData = array('houdin_admin_logo_image'=>$data['imageData'],'houdin_admin_logo_updated_at'=>$setDate);
        $this->db->where('houdin_admin_logo_id','1');
        $getUpdateStatus = $this->db->update('houdin_admin_logo',$setUpdateData);
        if($getUpdateStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // insert langauge data
    public function insertLanguageData($data)
    {
        $setDate = strtotime(date('Y-m-d h:i:s'));
        // check selected language exist or not
        $this->db->select('houdin_language_id')->from('houdin_languages')->where('houdin_language_name',$data['language']);
        $getLanguagedata = $this->db->get()->result();
        if(count($getLanguagedata) > 0)
        {
            return array('message'=>'exist');
        }
        else
        {
            $setInsertArray = array('houdin_language_name'=>$data['language'],'houdin_language_status'=>$data['status'],'houdin_language_created_at'=>$setDate,'houdin_language_name_value'=>$data['languageName']);
            $getInsertStatusdata = $this->db->insert('houdin_languages',$setInsertArray);
            if($getInsertStatusdata == 1)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }   
        }
    }
    // count language data
    public function countLanguageListdata()
    {
        $this->db->select('COUNT(houdin_language_id) AS countData')->from('houdin_languages');
        $getdata = $this->db->get()->result();
        return array('totalRows'=>$getdata[0]->countData);
    }
    // fetch language data
    public function getLanguageData($data)
    {
        $this->db->select('*')->from('houdin_languages');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getLanguageData = $this->db->get();
        $resultLanguageData = ($getLanguageData->num_rows() > 0)?$getLanguageData->result():FALSE;
        return array('languageList'=>$resultLanguageData);
    }
    // delete language data
    public function deleteLanguageData($data)
    {
        $this->db->where('houdin_language_id',$data);
        $getDeleteStatus = $this->db->delete('houdin_languages');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        { 
            return array('message'=>'no');
        }
    }
    // update language value
    public function updateLanguageDataValue($data)
    {

        $setDateValue = strtotime(date('Y-m-d h:i:s'));
        // check selected language exist or not
        $this->db->select('houdin_language_id')->from('houdin_languages')->where('houdin_language_name',$data['language']);
        $getLanguagedata = $this->db->get()->result();
        if(count($getLanguagedata) > 0)
        {
            return array('message'=>'exist');
        }
        else
        {
            $setUpdateArrayData = array('houdin_language_name'=>$data['houdin_language_name'],'houdin_language_name_value'=>$data['houdin_language_name_value'],'houdin_language_status'=>$data['houdin_language_status'],'houdin_language_updated_at'=>$setDateValue);
            $this->db->where('houdin_language_id',$data['editLanguageId']);
            $getUpdateStatus = $this->db->update('houdin_languages',$setUpdateArrayData);
            if($getUpdateStatus == 1)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }
        }
    }
    // fetch template data
    public function fetchTemplateData($data)
    {
        $this->db->select('*')->from('houdin_business_categories')->where('houdin_business_categories_status','active');
        $getTemplateBusinessCategoryData = $this->db->get()->result();
        // get template list
        $this->db->select('houdin_templates.*,houdin_business_categories.houdin_business_category_name')->from('houdin_templates')->join('houdin_business_categories','houdin_business_categories.houdin_business_category_id = houdin_templates.houdin_template_category');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getTemplatedata = $this->db->get();
        $resultTemplateData = ($getTemplatedata->num_rows() > 0)?$getTemplatedata->result():FALSE;
        // count total template
        $this->db->select('COUNT(houdin_template_id) AS totalTemplate')->from('houdin_templates');
        $getTotalTemplateData = $this->db->get()->result();
        // count total active template
        $this->db->select('COUNT(houdin_template_id) AS totalActiveTemplate')->from('houdin_templates')->where('houdin_template_status','active');
        $getTotalActiveTemplateData = $this->db->get()->result();
        // count total deactivate template
        $this->db->select('COUNT(houdin_template_id) AS totalDeactiveTemplate')->from('houdin_templates')->where('houdin_template_status','deactive');
        $getTotalDeactivateTemplateData = $this->db->get()->result();
        return array('businessCaegory'=>$getTemplateBusinessCategoryData,'templateList'=>$resultTemplateData,'totalTemplate'=>$getTotalTemplateData[0]->totalTemplate,'activeTemplate'=>$getTotalActiveTemplateData[0]->totalActiveTemplate,'deactivateTemplate'=>$getTotalDeactivateTemplateData[0]->totalDeactiveTemplate);
    }
    // count template data
    public function countTemplateListdata()
    {
        $this->db->select('COUNT(houdin_template_id) AS templateCount')->from('houdin_templates');
        $getTemplateCount = $this->db->get()->result();
        return array('totalRows'=>$getTemplateCount[0]->templateCount);
    }
    // add template data
    public function insertTemplateData($data)
    {
        $setDateData = strtotime(date('Y-m-d h:i:s'));
        $setInsertArray = array('houdin_template_file'=>$data['directory'],'houdin_template_name'=>$data['templateName'],'houdin_template_category'=>$data['templateCategory'],'houdin_template_status'=>$data['status'],'houdin_template_created_at'=>$setDateData,'houdin_template_description'=>$data['templateDescription'],'houdin_template_thumbnail'=>$data['templateThumbnail']);
        $getInsertDataStatus = $this->db->insert('houdin_templates',$setInsertArray);
        if($getInsertDataStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // delete template data
    public function deletetemplateData($data)
    {
        $this->db->where('houdin_template_id',$data);
        $getDeleteStatus = $this->db->delete('houdin_templates');
        if($getDeleteStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // update tempalte data
    public function updateTemplatedata($data)
    {
        $setdate = strtotime(date('Y-m-d h:i:s'));
        $setUpadetdArray = array('houdin_template_file'=>$data['directory'],'houdin_template_name'=>$data['name'],'houdin_template_category'=>$data['category'],'houdin_template_description'=>$data['description'],'houdin_template_status'=>$data['status'],'houdin_template_updated_at'=>$setdate);
        $this->db->where('houdin_template_id',$data['id']);
        $getUpdateStatus = $this->db->update('houdin_templates',$setUpadetdArray);
        if($getUpdateStatus == 1)
        {
            return array('message'=>'yes','id'=>$data['id']);
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // update template image
    public function updateTemplaetImage($data)
    {
        $updatedArrayData = array('houdin_template_thumbnail'=>$data['imageData']);
        $this->db->where('houdin_template_id',$data['id']);
        $getUpdateStatus = $this->db->update('houdin_templates',$updatedArrayData);
        if($getUpdateStatus == 1)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    // update twilio credentials
    public function setTwilioCredentials($data)
    {
        $setData = strtotime(date('Y-m-d h:i:s'));
        if($data['id'] == "")
        {
            $setInsertArray = array('houdin_twilio_sid'=>$data['houdin_twilio_sid'],'houdin_twilio_token'=>$data['houdin_twilio_token'],'houdin_twilio_number'=>$data['houdin_twilio_number'],'houdin_twilio_created_at'=>$setData);
            $getInsertData = $this->db->insert('houdin_twilio',$setInsertArray);
            if($getInsertData == 1)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }
        }
        else
        {
            $setUpdateArray = array('houdin_twilio_sid'=>$data['houdin_twilio_sid'],'houdin_twilio_token'=>$data['houdin_twilio_token'],'houdin_twilio_number'=>$data['houdin_twilio_number'],'houdin_twilio_modified_at'=>$setData);
            $this->db->where('houdin_twilio_id',$data['id']);
            $getUpdateData = $this->db->update('houdin_twilio',$setUpdateArray);
            if($getUpdateData == 1)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }
        }
    }
    // fetch twillio details
    public function fetchTwilio()
    {
        $this->db->select('*')->from('houdin_twilio');
        $getTwilioData = $this->db->get()->result();
        return array('twilioList'=>$getTwilioData);
    }
    public function updateEmailCredentials($data,$id)
    {
        if($id)
        {
            $this->db->where('houdin_sendgrid_id',$id);
            $getData = $this->db->update('houdin_sendgrid',$data);
            return $getData;
        }
        else
        {
            $getData = $this->db->insert('houdin_sendgrid',$data);
            return $getData;
        }
    }
    public function getEmailCredentials()
    {
        $getCredentials = $this->db->select('*')->from('houdin_sendgrid')->where('houdin_sendgrid_id',1)->get()->result();
        return array('data'=>$getCredentials);
    }
    public function updatePayumoney($data,$id)
    {
        if($id)
        {
            $this->db->where('houdin_payumoney',$id);
            $getUpdateStatus = $this->db->update('houdin_payumoney',$data);
            return $getUpdateStatus;
        }
        else
        {
            $getInsertStatus = $this->db->insert('houdin_payumoney',$data);
            return $getInsertStatus;
        }
    }
    public function fetchPayumoney()
    {
        $getPayuMoney = $this->db->select('*')->from('houdin_payumoney')->where('houdin_payumoney','1')->get()->result();
        return array('payu'=>$getPayuMoney);
    }
}
