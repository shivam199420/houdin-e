        <!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon_icon.png">
        <title>Houdin - Welcome to Houdin Admin Panel</title>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/login_custom.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
        <style>
        .account-pages {
            background: url(../vendor/assets/images/updated_login_banner.jpg);
            position: absolute;
            height: 100%;
            width: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }
        .card-box{
        background-color: #ffffff69!important;
        }
        .text-dark {
        color: #ffffff !important;
        }
        .modal-dialog {
    width: 417px!important;
    margin: 30px auto;
}
        </style>
        </head>
        <body>
        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
        <div class=" card-box card_box_custom">
        <div class="panel-heading">
        <h3 class="text-center"> <img src="<?php echo base_url(); ?>assets/images/Logo_F_W.png" class="logo_head"/></h3>
        </div>
        <div class="panel-body">
                    <?php
            if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    ?>
        <!--<form class="form-horizontal" action="<?php echo base_url();?>Login/submit" method="post" id="login_form">-->
       
       <?php echo form_open(base_url( 'Login/submit' ),  array( 'method' => 'post', 'class' => 'form-horizontal','id'=>"login_form" ));?>
        <div class="form-group ">
        <div class="col-xs-12">
        <input class="form-control required_validation_for_register required_validation_email" type="text"  name="email"  placeholder="Email Address" >
       <!-- <p class="messagesd"></p>-->
        </div>
        </div>
        <div class="form-group">
        <div class="col-xs-12">
        <input class="form-control required_validation_for_register password" type="password" name="password"  placeholder="Password" >
        </div>
        </div>
        <div class="form-group">
        <div class="col-xs-12">
        <input class="form-control required_validation_for_register confirm_password" type="password" name="cpassword" placeholder="Confirm Password" >
        </div>
        </div>
        <div class="form-group text-center m-t-40">
        <div class="col-xs-12">
        <button class="btn btn_theme_dark text-uppercase waves-effect waves-light" name="login" type="submit">Log In</button>
        </div>
        </div>
        <div class="form-group m-t-30 m-b-0">
        <div class="col-sm-12">
        <a href="<?php echo base_url(); ?>forget" class="text-dark"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
        </div>
        </div>
        <?php echo form_close();?>
        </div>
        </div>
        </div>
        <!--script-->
        <script>
        var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>
        
        <script type="text/javascript">
    /*==============Form Validation===========================*/
        $(document).ready(function(){

        $(document).on('submit','#login_form',function(c){
                
             //  c.preventDefault();
                 var rep_image_val='';
                 $(".required_validation_for_register").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                        
                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
                
                $('.required_validation_for_register').on('keyup',function()
                                {
                                    
                                        $(this).css("border-color","#ccc");
                                });
                
                
                 $('.required_validation_email').on('blur',function()
                                {
                                    if(!validateEmail($(this).val()))
                                       {
                                        
                                        $(this).css("border-color","red");
                                        $('.messagesd').text('please fill correct email  address');
                                        }
                                        else
                                        {
                                           $(this).css("border-color","#ccc"); 
                                           $('.messagesd').text('');
                                        }
                                });
                
                   function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
  }





               var current_value = $('.confirm_password').val();
                var originalValue = $('.password').val();


                if(current_value != originalValue)
                {
                $('.confirm_password').css('border-bottom','1px solid red');
                        $('.confirm_password').val('');
                       // $('.confirm_password').attr('placeholder','Set password and Confirm password must match');
                        rep_image_val = 'error form';
                }
                else
                {
                        $(this).css('border-bottom','1px solid #d3d3d3');
                        $('.confirm_password').attr('placeholder','Confirm Password');
                }

                
                if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                else
                {
                }

        });
        });
      </script>
        </body>
        </html>
