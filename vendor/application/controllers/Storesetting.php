<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Storesetting extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('Storesettingmodel');
        $this->load->library('form_validation');
         $this->load->helper('url');
         $this->auth_users->is_logged_in(); 
    }
    
 public function Storesettinglanding()
    {
        $this->load->view('storesettinglandingpage');
    }
    
    public function logo()
    {

    	 $getlogo['storelogos'] = $this->Storesettingmodel->fetchstorelogoRecord('logo');

        $this->load->view('storelogo',$getlogo);
        
        
    }
    

    public function do_upload(){

          
          if($_REQUEST['clear_check']){
                
        $insert_data= array('clear' =>1,'update_date'=>strtotime(date('Y-m-d')));

            $file_id = $this->Storesettingmodel->insert_logo($insert_data);
            if($file_id)
            {          
             return  $this->session->set_flashdata('success','Logo Successfully Add');
            }else{                
              return  $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            }
          	return;
          }
       $File_name='logo-'.strtotime(date('Y-m-d'));

        $config['upload_path'] = APPPATH . '../upload/logo'; 
        $config['file_name'] = $File_name;
        $config['overwrite'] = TRUE;
        $config["allowed_types"] = 'jpg|jpeg|png';
        // $config["max_size"] = 1024;
        // $config["max_width"] = 190;
        // $config["max_height"] = 47;
        // $config['$min_width'] = 100;
        // $config['min_height'] = 20;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if(!$this->upload->do_upload('image_logo')) { 

          //$this->data['error'] = $this->upload->display_errors();
          $this->session->set_flashdata('error',$this->upload->display_errors());
        } else {
         $dataimage_return = $this->upload->data();  
         $insert_data= array('shop_logo' =>$dataimage_return['file_name'],'clear' =>0,'update_date'=>strtotime(date('Y-m-d')));

            $file_id = $this->Storesettingmodel->insert_logo($insert_data);
            if($file_id)
            {          
               $this->session->set_flashdata('success','Logo Successfully Add');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            }


                                               
        }  
    


     }
    
    public function favicon()
    {

    	 $getlogo['storelogos'] = $this->Storesettingmodel->fetchstorelogoRecord('favicon');

        $this->load->view('storefavicon',$getlogo);
      
    }

public function do_upload_favicon(){

          
          if($_REQUEST['clear_check']){
                
        $insert_data= array('clear_favicon' =>1,'update_date'=>strtotime(date('Y-m-d')));

            $file_id = $this->Storesettingmodel->insert_logo($insert_data);
            if($file_id)
            {          
             return  $this->session->set_flashdata('success','Logo Successfully Add');
            }else{                
              return  $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            }
          	return;
          }
       $File_name='favicon-'.strtotime(date('Y-m-d'));

        $config['upload_path'] = APPPATH . '../upload/logo'; 
        $config['file_name'] = $File_name;
        $config['overwrite'] = TRUE;
        $config["allowed_types"] = 'jpg|jpeg|png|ico';
        // $config["max_size"] = 1024;
        // $config["max_width"] = 32;
        // $config["max_height"] = 32;
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if(!$this->upload->do_upload('favicon_image')) { 

          //$this->data['error'] = $this->upload->display_errors();
          $this->session->set_flashdata('error',$this->upload->display_errors());
        } else {
         $dataimage_return = $this->upload->data();
          
         $insert_data= array('shop_favicon' =>$dataimage_return['file_name'],'clear_favicon' =>0,'update_date'=>strtotime(date('Y-m-d')));

            $file_id = $this->Storesettingmodel->insert_logo($insert_data);
            if($file_id)
            {          
               $this->session->set_flashdata('success','Logo Successfully Add');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            }


                                               
        }  
    


     }

    public function navigation()
    {
        $getNavigationData  = $this->Storesettingmodel->fetchNavigationData();
        $this->load->view('storenavigation',$getNavigationData);
    }
    public function storeinfo()
    {
        $this->load->view('storeinfo');
    }
    public function storepolicies()
    {

    $getPolicy['Policy_data'] = $this->Storesettingmodel->fetchstorePolicyRecord();
        $this->load->view('storepolicies', $getPolicy);
    }
  public function storepolicies_frm(){
    $postData_policies = $this->input->post();   
     $Privacy_Policy=$postData_policies['Privacy_Policy'];
     $Terms_Conditions=$postData_policies['Terms_Conditions'];
     $Cancellation_Refund_Policy=$postData_policies['Cancellation_Refund_Policy'];
     $Shipping_Delivery_Policy=$postData_policies['Shipping_Delivery_Policy'];
     $Disclaimer_Policy=$postData_policies['Disclaimer_Policy'];
     $about = $postData_policies['about'];
     $faq = $postData_policies['FAQ'];
       $insert_data= array('Privacy_Policy' =>$Privacy_Policy,'Terms_Conditions' =>$Terms_Conditions,'Cancellation_Refund_Policy'=>$Cancellation_Refund_Policy,'Shipping_Delivery_Policy'=>$Shipping_Delivery_Policy,'Disclaimer_Policy'=>$Disclaimer_Policy,'date_time'=>strtotime(date('Y-m-d')),'about'=>$about,'faq'=>$faq);
            $file_id = $this->Storesettingmodel->save_policy($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Policy Successfully Update');             
             
             redirect("Storesetting/storepolicies", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            }
  }

    public function storetheme()
    {
        $getThemeData = $this->Storesettingmodel->fetchThemeData();
        $this->load->view('storetheme',$getThemeData);
    }


    public function edittheme()
    {
        $getThemeData = $this->Storesettingmodel->fetchCustomHomeData();
        $this->load->view('edittheme',$getThemeData);
    }
    public function sociallinks()
    { $getsocials['socials_data'] = $this->Storesettingmodel->fetchstoreSocialRecord();
        $this->load->view('sociallinks',$getsocials);
    }

    public function storesocial_frm(){
    $postData_policies = $this->input->post();   
     $twitter_url=$postData_policies['twitter_url'];
     $facebook_url=$postData_policies['facebook_url'];
     $google_url=$postData_policies['google_url'];
     $youtube_url=$postData_policies['youtube_url'];
     $instagram_url=$postData_policies['instagram_url'];
      $pinterest_url=$postData_policies['pinterest_url'];
       $linkedin_url=$postData_policies['linkedin_url']; 

       $insert_data= array('twitter_url' =>$twitter_url,'facebook_url' =>$facebook_url,'google_url'=>$google_url,'youtube_url'=>$youtube_url,'instagram_url'=>$instagram_url,'pinterest_url'=>$pinterest_url,'linkedin_url'=>$linkedin_url,'date_time'=>strtotime(date('Y-m-d')));
            $file_id = $this->Storesettingmodel->save_sociallink($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Social Successfully Update');             
             
             redirect("Storesetting/sociallinks", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the Social, please try again.');
            }
  }
    public function storetestimonials()
    {
    	 $gettestimonials['testimonials_data'] = $this->Storesettingmodel->fetchstoreTestimonialsRecord();
        $this->load->view('storetestimonials',$gettestimonials);
    }

public function storetestimonials_frm(){

	  //is_unique check the unique email value in users table
         $this->form_validation->set_rules('testimonials_name', 'Name', 'trim|required');
         $this->form_validation->set_rules('testimonials_feedback', 'Feedback', 'trim|required');
         //$this->form_validation->set_rules('testimonials_image', 'Image', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){ 
        $reg_pup['model_type']="add_new"; 

        $this->load->view('models_',$reg_pup);            
        }else{       	

       $File_name='testimonials-'.strtotime(date('Y-m-d'));

        $config['upload_path'] = APPPATH . '../upload/testimonials'; 
        $config['file_name'] = $File_name;
        $config['overwrite'] = TRUE;
        $config["allowed_types"] = 'jpg|jpeg|png';
        $config["max_size"] = 1024;
        //$config["max_width"] = 1500;
       // $config["max_height"] = 1000;       
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('testimonials_image')) {  $this->session->set_flashdata('error',$this->upload->display_errors());  }else{

        	 $dataimage_return = $this->upload->data();
        	$file_name= $dataimage_return['file_name'];
        }
          $postData_testimonials = $this->input->post(); 

          
                  $testimonials_name= $postData_testimonials['testimonials_name'];
                  $testimonials_feedback= $postData_testimonials['testimonials_feedback'];
                  $testimonials_checkbox= $postData_testimonials['testimonials_checkbox'];

           
         	  $insert_data= array('testimonials_name' =>$testimonials_name,'testimonials_image' =>$file_name,'testimonials_feedback'=>$testimonials_feedback,'testimonials_publish'=>$testimonials_checkbox,'date_time'=>strtotime(date('Y-m-d')));
            $file_id = $this->Storesettingmodel->save_testimonials($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Testimonials Successfully Save');             
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            }
                       
         }
}

  public function storetestimonials_delete()
{
	if($this->input->post('delete_tastimoneal')){

      $file_id = $this->Storesettingmodel->delete_testimonials($this->input->post('delete_tastimoneal'));
            if($file_id)
            {$this->session->set_flashdata('success','Testimonials Successfully Delete');             
             
           redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            }

	}
	# code...
}


public function storetestimonials_edit(){

	  //is_unique check the unique email value in users table
         $this->form_validation->set_rules('testimonials_name', 'Name', 'trim|required');
         $this->form_validation->set_rules('testimonials_feedback', 'Feedback', 'trim|required');
         //$this->form_validation->set_rules('testimonials_image', 'Image', 'trim|required');
       
        if ($this->form_validation->run() == FALSE){          
                 $reg_pup['model_type']="edit";         
                $this->load->view('models_',$reg_pup);               
        }else{  
        	

       $File_name='testimonials-'.strtotime(date('Y-m-d'));

        $config['upload_path'] = APPPATH . '../upload/testimonials'; 
        $config['file_name'] = $File_name;
        $config['overwrite'] = TRUE;
        $config["allowed_types"] = 'jpg|jpeg|png';
        $config["max_size"] = 1024;
        //$config["max_width"] = 1500;
       // $config["max_height"] = 1000;       
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('testimonials_image')) {  $this->session->set_flashdata('error',$this->upload->display_errors());  }else{

        	 $dataimage_return = $this->upload->data();
        	$file_name= $dataimage_return['file_name'];
        }
          $postData_testimonials = $this->input->post(); 

          
                  $testimonials_name= $postData_testimonials['testimonials_name'];
                  $testimonials_feedback= $postData_testimonials['testimonials_feedback'];
                  $testimonials_checkbox= $postData_testimonials['testimonials_checkbox'];
                  $update_id= $postData_testimonials['update_tastimoneal'];

           if($file_name){
         	  $insert_data= array('id'=> $update_id,'testimonials_name' =>$testimonials_name,'testimonials_image' =>$file_name,'testimonials_feedback'=>$testimonials_feedback,'testimonials_publish'=>$testimonials_checkbox,'date_time'=>strtotime(date('Y-m-d')));
           }else{

           	$insert_data= array('id'=> $update_id,'testimonials_name' =>$testimonials_name,'testimonials_feedback'=>$testimonials_feedback,'testimonials_publish'=>$testimonials_checkbox,'date_time'=>strtotime(date('Y-m-d')));
           }

                

            $file_id = $this->Storesettingmodel->edit_testimonials($insert_data);
            if($file_id)
            {$this->session->set_flashdata('success','Testimonials Successfully Update');             
             
             //redirect("Storesetting/storetestimonials", 'refresh');
            }else{                
                $this->session->set_flashdata('error','Something went wrong when saving the file, please try again.');
            }
                       
         }
}


    public function custompages()
    {
        $this->load->view('custompages');
    }
    public function updateStaticNavigation()
    {
        $this->form_validation->set_rules('navData','text','required');
        if($this->form_validation->run() == true)
        {
            $getMessage = $this->Storesettingmodel->addStoreNavigationPage($this->input->post('navData'));
            if($getMessage['message'] == 'yes')
            {
                $getData = $this->Storesettingmodel->fetchMenuData();
                echo $getData;
            }
            else
            {
                echo 'no';
            }
        }
        else
        {
            echo 'nonav';
        }
    }
    public function updateCustomHome()
    {
        $setInsertArray = array('houdinv_custom_home_data_category'=>$this->input->post('categoryHeading'),'houdinv_custom_home_data_latest_product'=>$this->input->post('productHeading'),
        'houdinv_custom_home_data_featured_product'=>$this->input->post('featuredProductHeading'),'houdinv_custom_home_data_testimonial'=>$this->input->post('testimonialHeading'));
        $getValue = $this->Storesettingmodel->updateCustomHomeData($setInsertArray);
        if($getValue['message'] == 'yes'){
            $this->session->set_flashdata('success','Custom home updated successfully');
            redirect(base_url()."Storesetting/edittheme", 'refresh');
        }else{
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(base_url()."Storesetting/edittheme", 'refresh');
        }
    }
    public function updateSlider()
    {
        $this->form_validation->set_rules('headingData','Heading','required');
        $this->form_validation->set_rules('subheadingData','Sub Heading','required');
        if($this->form_validation->run() == true)
        {
            if(!empty($_FILES['name']['name']))
            {
                $newFileName = "slider_".rand(100000,999999)."".$_FILES['name']['name'];
                $config['upload_path'] = "upload/Slider/";
                $config['allowed_types'] = 'jpg|jpeg|png';
                //$config['max_size'] = '5120';
                $config['file_name'] = $newFileName;
                // $config["max_width"] = 1680;
                // $config["max_height"] = 1050;
                // $config['$min_width'] = 770;
                // $config['min_height'] = 513;
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('name'))
                {
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                    $getHeading = $this->input->post('headingData');
                    $getSubHeading = $this->input->post('subheadingData');
                    $getId = $this->input->post('sliderId');
                    $setSliderArray = array('imageData'=>$picture,'heading'=>$getHeading,'subheading'=>$getSubHeading,'id'=>$getId);
                    $getSliderStatus = $this->Storesettingmodel->updateSliderImage($setSliderArray);
                    if($getSliderStatus['message'] == 'yes')
                    {
                        $this->session->set_flashdata('success','Slider Image updated successfully');
                        redirect(base_url()."Storesetting/edittheme", 'refresh');
                    }
                    else
                    {
                        $this->session->set_flashdata('error','Something went wrong. Please try again');
                        redirect(base_url()."Storesetting/edittheme", 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error',$this->upload->display_errors());
                    redirect(base_url()."Storesetting/edittheme", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Please choose image');
                redirect(base_url()."Storesetting/edittheme", 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."Storesetting/edittheme", 'refresh');
        }
    }
    public function deleteSliderImage()
    {
        $this->form_validation->set_rules('deleteSliderImageId','text','required|integer');
        if($this->form_validation->run() == true)
        {
            $getDeleteStatus = $this->Storesettingmodel->deleteSliderImage($this->input->post('deleteSliderImageId'));
            if($getDeleteStatus['message'] == 'yes')
            {
                $this->session->set_flashdata('success','Slider image deleted successfully');
                redirect(base_url()."Storesetting/edittheme", 'refresh');
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url()."Storesetting/edittheme", 'refresh');    
            }
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(base_url()."Storesetting/edittheme", 'refresh');
        }
    }
    public function deleteNavigation()
    {
        $this->form_validation->set_rules('deleteNavigationId','text','required|integer');
        if($this->form_validation->run() == true)
        {
            $getDeleteStatus = $this->Storesettingmodel->deleteSliderNav($this->input->post('deleteNavigationId'));
            if($getDeleteStatus['message'] == 'yes')
            {
                $this->session->set_flashdata('success','Navigation deleted successfully');
                redirect(base_url()."Storesetting/navigation", 'refresh');
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(base_url()."Storesetting/navigation", 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(base_url()."Storesetting/navigation", 'refresh');
        }
    }
    public function updateCategoryNavigation()
    {
        $this->form_validation->set_rules('navDataId','text','required');
        $this->form_validation->set_rules('navDataName','text','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('id'=>$this->input->post('navDataId'),'name'=>$this->input->post('navDataName'));
            $getInsertStatus = $this->Storesettingmodel->addCategoryNav($setArray);
            if($getInsertStatus['message'] == 'yes')
            {
                $fetchData = $this->Storesettingmodel->fetchMenuData();
                echo $fetchData;
            }
            else
            {
                echo 'no';    
            }
        }   
        else
        {
            echo 'nonav';
        }
    }
    public function updateCustomLinks()
    {
        $this->form_validation->set_rules('title','Title','required');
        $this->form_validation->set_rules('url','URl','required|valid_url');
        $this->form_validation->set_rules('target','Target','required|in_list[current,new]');
        if($this->form_validation->run() == true)
        {
            $setData = strtotime(date('Y-m-d'));
            $setinserArray = array('houdinv_custom_links_title'=>$this->input->post('title'),'houdinv_custom_links_url'=>$this->input->post('url'),'houdinv_custom_links_target'=>$this->input->post('target'),'houdinv_custom_links_created_at'=>$setData);
            $getcustomLink = $this->Storesettingmodel->addcustomLinksData($setinserArray);
            if($getcustomLink['messgae'] == 'yes')
            {
                $getData = $this->Storesettingmodel->fetchMenuData();
                echo $getData;
            }
            else
            {
                echo 'no';
            }
        }
        else
        {
            echo 'all';
        }
    }
    public function deleteCustomLinkNavigation()
    {
        $this->form_validation->set_rules('deleteCustomMenuNavigationId','text','required');
        if($this->form_validation->run() == true)
        {
            $getDeleteStatus = $this->Storesettingmodel->deleteCustomMenuNavigation($this->input->post('deleteCustomMenuNavigationId'));
            if($getDeleteStatus['messgae'] == 'yes')
            {
                $this->session->set_flashdata('success','Navigation deleted successfully');
                redirect(base_url()."Storesetting/navigation", 'refresh');
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url()."Storesetting/navigation", 'refresh');    
            }
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(base_url()."Storesetting/navigation", 'refresh');
        }
    }
}
