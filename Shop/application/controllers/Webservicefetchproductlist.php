<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Webservicefetchproductlist extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->load->model('Shopproductlistmodel');
        // $getSubDomain = checkShopUrl();
        // if($getSubDomain['message'] == 'no')
        // {
        //     redirect($getSubDomain['baseUrl'], 'refresh');
        // }
        // else
        // {
        //     $this->setShopName = $getSubDomain['shop'];
        // }
    }
    public function index()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('productCategory','text','required');
        $this->form_validation->set_rules('lastLimit','text','required');
        if($this->form_validation->run() == true)
        {
            $setArray = array('shopname'=>$this->input->post('shopName'),'category'=>$this->input->post('productCategory'),'subCategory'=>$this->input->post('productSubCategory'),'subSubCategory'=>$this->input->post('productSubSubCategory'),'sort'=>$this->input->post('sort'),'filter'=>$this->input->post('filter'),'lastLimit'=>$this->input->post('lastLimit'));
            $getProductArray = $this->Shopproductlistmodel->fetchProductListModelApp($setArray);
            print_r(json_encode($getProductArray));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function checkquantityData()
    {
        $this->form_validation->set_rules('shopName','text','required');
        $this->form_validation->set_rules('productId','text','required');
        $this->form_validation->set_rules('variantId','text','required');
        if($this->form_validation->run() == true)
        {
            $setQuantityArray = array('shopName'=>$this->input->post('shopName'),'productId'=>$this->input->post('productId'),'variantid'=>$this->input->post('variantId'));
            $getQuantityData = $this->Shopproductlistmodel->checkProductQuantity($setQuantityArray);
            print_r(json_encode($getQuantityData));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
}

