<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Templatemodel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
             }
             
             
    public function FetchAllSmsTemplate($data)
    {
       $sms_data  = $this->db->select("*")->from("houdinv_sms_template")
       ->where("houdinv_sms_template_type",$data)
                    ->get()->row();  
        return  $sms_data;
    
    }    
    
     public function FetchAllEmailTemplate($data)
    {
       $sms_data  = $this->db->select("*")->from("houdinv_email_Template")
       ->where("houdinv_email_template_type",$data)
                    ->get()->row();  
        return  $sms_data;
    
    }
    
     public function FetchAllPushTemplate($data)
    {
       $sms_data  = $this->db->select("*")->from("houdinv_push_Template")
       ->where("houdinv_push_template_type",$data)
                    ->get()->row();  
        return  $sms_data;
    
    }     
             
   public function SmsTemplateSave($data)
   {
       $already_data  = $this->db->select("*")->from("houdinv_sms_template")
        ->where("houdinv_sms_template_type",$data['houdinv_sms_template_type'])
        ->get()->result();
        if(count($already_data))
        {
            $this->db->where("houdinv_sms_template_type",$data['houdinv_sms_template_type'])
            ->update("houdinv_sms_template",$data);
        }
        else
        {
            $data['houdinv_sms_template_created_date']=$data['houdinv_sms_template_updated_date'];
         $this->db->insert("houdinv_sms_template",$data);   
        }
        
        return true;
    
   }    
   
   
      public function EmailTemplateSave($data)
   {
       $already_data  = $this->db->select("*")->from("houdinv_email_Template")
        ->where("houdinv_email_template_type",$data['houdinv_email_template_type'])
        ->get()->result();
        if(count($already_data))
        {
            $this->db->where("houdinv_email_template_type",$data['houdinv_email_template_type'])
            ->update("houdinv_email_Template",$data);
        }
        else
        {
            $data['houdinv_email_template_created_date']=$data['houdinv_email_template_updated_date'];
         $this->db->insert("houdinv_email_Template",$data);   
        }
        
        return true;
    
   }    
   
         public function PushTemplateSave($data)
   {
       $already_data  = $this->db->select("*")->from("houdinv_push_Template")
        ->where("houdinv_push_template_type",$data['houdinv_push_template_type'])
        ->get()->result();
        if(count($already_data))
        {
            $this->db->where("houdinv_push_template_type",$data['houdinv_push_template_type'])
            ->update("houdinv_push_Template",$data);
        }
        else
        {
            $data['houdinv_push_template_created_date']=$data['houdinv_push_template_updated_date'];
         $this->db->insert("houdinv_push_Template",$data);   
        }
        
        return true;
    
   }     
             
             
             }