        <?php $this->load->view('Template/header') ?>
        <?php $this->load->view('Template/sidebar') ?>
        <style type="text/css">
        .widget-style-2 i {
        background: rgba(244, 248, 251, 0.6) !important;
        font-size: 48px;
        padding: 30px 8px;
        }
        </style>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">
         <!--START Page-Title -->
            <div class="row">
                
                <div class="col-md-8">
                   <h4 class="page-title">View Supplier</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>Supplier">Supplier</a></li>
                  <li class="active">View Supplier</li>
                  </ol>
                  </div>
                 
            </div>
           <!--END Page-Title --> 

        <!-- Page-Title -->
        <div class="row">
        <div class="col-sm-12">
        <h4 class="page-title">View Supplier</h4>
        </div>
        </div>

        <div class="row m-t-20">
        <div class="col-sm-3">


        <div class="card-box no-pad no-margin"> 

        <form method="">
        <form method=""> 
        <div class="row">
        <div class="col-md-12 form-horizontal pad-10">
        <h3 class="custom_h3">Personal Info</h3>
        <div class="form-group m-b-0">
        <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Company Name : </label>
        <div class="col-md-8 col-xs-8 col-sm-8">
        <p class="line-height-form-desktop"><?php echo $supplierInfo[0]->houdinv_supplier_comapany_name ?></p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Contact Person : </label>
        <div class="col-md-8 col-xs-8 col-sm-8">
        <p class="line-height-form-desktop"><?php echo $supplierInfo[0]->houdinv_supplier_contact_person_name ?></p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Email : </label>
        <div class="col-md-8 col-xs-8 col-sm-8">
        <p class="line-height-form-desktop"><?php echo $supplierInfo[0]->houdinv_supplier_email ?></p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Contact : </label>
        <div class="col-md-8 col-xs-8 col-sm-8">
        <p class="line-height-form-desktop"><?php echo $supplierInfo[0]->houdinv_supplier_contact ?></p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Landline : </label>
        <div class="col-md-8 col-xs-8 col-sm-8">
        <p class="line-height-form-desktop"><?php echo $supplierInfo[0]->houdinv_supplier_landline ?></p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Address : </label>
        <div class="col-md-8 col-xs-8 col-sm-8">
        <p class="line-height-form-desktop"><?php echo $supplierInfo[0]->houdinv_supplier_address ?></p>
        </div>
        </div>

        <div class="form-group m-b-0">
        <label class="col-md-4 col-xs-4 col-sm-4 control-label pad-left-20">Status : </label>
        <div class="col-md-8 col-xs-8 col-sm-8">
        <p class="line-height-form-desktop"><?php if($supplierInfo[0]->houdinv_supplier_active_status == 'active') { $setText="Active";$setClass="success"; }
        else if($supplierInfo[0]->houdinv_supplier_active_status == 'deactive') { $setText="Deactive";$setClass="warning"; }  ?>
        <button type="button" class="btn btn-<?php echo $setClass; ?> btn-xs"><?php echo $setText; ?></button>
        </p>
        </div>
        </div>
        </div>

        </div>
        </form>
        </form>
        </div>

        </div>

        <div class="col-md-9">
        <div class="card-box table-responsive no-pad no-margin">
        <div class="row"> 
        <div class="col-lg-12"> 
        <ul class="nav nav-tabs navtab-bg nav-justified"> 
        <li class="active"> 
        <a href="#home1" data-toggle="tab" aria-expanded="false"> 
        <span class="visible-xs"><i class="fa fa-home"></i></span> 
        <span class="hidden-xs">Previous order details</span> 
        </a> 
        </li> 
        <li class=""> 
        <a href="#profile1" data-toggle="tab" aria-expanded="true"> 
        <span class="visible-xs"><i class="fa fa-user"></i></span> 
        <span class="hidden-xs">Upcoming order details</span> 
        </a> 
        </li> 
        <li class=""> 
        <a href="#messages1" data-toggle="tab" aria-expanded="false"> 
        <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> 
        <span class="hidden-xs">Account status</span> 
        </a> 
        </li> 
        <li class=""> 
        <a href="#tagged" data-toggle="tab" aria-expanded="false"> 
        <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> 
        <span class="hidden-xs">Tagged product </span> 
        </a> 
        </li> 
        </ul> 
        <div class="tab-content"> 
        <div class="tab-pane active" id="home1"> 
        <div class="row m-t-20">
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <i class="md md-attach-money text-primary"></i>
        <h2 class="m-0 text-dark counter font-600">50568</h2>
        <div class="text-muted m-t-5">Total order</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <i class="md md-add-shopping-cart text-pink"></i>
        <h2 class="m-0 text-dark counter font-600">1256</h2>
        <div class="text-muted m-t-5">Total Completed order </div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <i class="md md-store-mall-directory text-info"></i>
        <h2 class="m-0 text-dark counter font-600">18</h2>
        <div class="text-muted m-t-5">Total cancelled order</div>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="container">
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th><input type="checkbox"></th>
        <th>Order ID</th>
        <th>Product name</th>
        <th>Delivery date</th>
        <th>Order</th>
        <th>Payment Status</th>
        <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td><input type="checkbox"></td>
        <td>H124568</td>
        <td>Electronic</td> 
        <td>DD/MM/YYYY</td>
        <td>Cash</td>
        <td>Pending</td>
        <td><button type="button" class="btn btn-success btn btn-xs">Invoice</button></td>
        </tr>
        </tbody>
        </table>
        </div>
        </div>
        </div> 
        <div class="tab-pane" id="profile1"> 
        <div class="row m-t-20">
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <i class="md md-attach-money text-primary"></i>
        <h2 class="m-0 text-dark counter font-600">50568</h2>
        <div class="text-muted m-t-5">Total upcoming order</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <i class="md md-add-shopping-cart text-pink"></i>
        <h2 class="m-0 text-dark counter font-600">1256</h2>
        <div class="text-muted m-t-5">Total Completed order </div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <i class="md md-store-mall-directory text-info"></i>
        <h2 class="m-0 text-dark counter font-600">18</h2>
        <div class="text-muted m-t-5">Total cancelled order</div>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="container">
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th><input type="checkbox"></th>
        <th>Order ID</th>
        <th>Product name</th>
        <th>Expected Delivery date</th>
        <th>Order Status</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td><input type="checkbox"></td>
        <td>H124568</td>
        <td>Electronic</td> 
        <td>DD/MM/YYYY</td>
        <td>Pending</td>
        </tr>
        </tbody>
        </table>
        </div> 
        </div>
        </div> 
        <div class="tab-pane" id="messages1"> 
        <div class="row m-t-20">
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <i class="md md-attach-money text-primary"></i>
        <h2 class="m-0 text-dark counter font-600">50568</h2>
        <div class="text-muted m-t-5">Total paid for order </div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <i class="md md-add-shopping-cart text-pink"></i>
        <h2 class="m-0 text-dark counter font-600">1256</h2>
        <div class="text-muted m-t-5">Total unpaid amount </div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <i class="md md-store-mall-directory text-info"></i>
        <h2 class="m-0 text-dark counter font-600">18</h2>
        <div class="text-muted m-t-5">Total cancelled order</div>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="container">
        <img src="../assets/images/revenue1.jpg">
        </div>
        </div>
        </div> 
        <div class="tab-pane" id="tagged"> 
        <div class="row m-t-20">
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <i class="md md-attach-money text-primary"></i>
        <h2 class="m-0 text-dark counter font-600">50568</h2>
        <div class="text-muted m-t-5">Total paid for order </div>
        </div>
        </div>
        
        </div>
     
        </div> 
        </div> 
        </div> 
        </div>
        <!-- end row --> 
        </div>
        </div>
        </div>
        <?php $this->load->view('Template/footer') ?>
        <script>
        var resizefunc = [];
        </script>
