<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        if($this->session->userdata('userAuth') == "")
        {
            redirect(base_url()."register", 'refresh');
        }
        $this->load->model('Shopuserprofile');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $this->load->view('Apparels/profile');
    }
}
