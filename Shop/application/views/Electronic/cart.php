  <?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>

  <!-- Content -->
  <div id="content">
      <div class="linking">
      <div class="container">
        <ol class="breadcrumb">
          <li class="active">Shopping-cart</li>
        </ol>
      </div>
    </div>


    <!-- Shopping Cart -->
    <section class="shopping-cart padding-bottom-60">
      <div class="container">
      <?php
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }

         if($this->session->flashdata('message_name'))
        {
                echo '<div class="alert alert-danger">'.implode("</br>",json_Decode($this->session->flashdata('message_name'))).'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        <?php
        if(count($AllCart)<1)
        {
        ?>
        <div class="col-sm-12 text-center"><h3>Your cart is empty</h3></div>
        <?php }
        else
        {
        ?>
        <div class="cart_table_area">
        <div class="table-responsive">
        <table class="table cart_prdct_table">
          <thead>
            <tr>
            <tr>
            <th class="cpt_img">Product</th>
            <th class="cpt_q">quantity</th>
            <th class="cpt_p">price</th>
            <th class="cpt_t">total</th>
            <th class="cpt_r">remove</th>
            </tr>
            </tr>
          </thead>
          <?php echo form_open(base_url( 'Cart/UpdateCart' ), array( 'id' => 'UpdateCart', 'method'=>'post' ));?>
          <tbody>

          <?php
            $i=1;
            $final_price =0;
            foreach($AllCart as $thisItem)
            {
              if($thisItem['stock'] <= 0)
              {
                $setStockData = '(Out of stock)';
              }
                $count = $thisItem['count'];
                $main_price = $thisItem['productPrice'];
                $total_price = $main_price*$count;
                $final_price =$final_price+$total_price;
        ?>
            <tr class="tr_row">
              <td><div class="media">
                  <div class="media-left">   <a href="<?php if($thisItem['productId'] != 0) { echo base_url()."Productlistview/".$thisItem['productId'];  } else { echo base_url()."Productlistview/variant/".$thisItem['productId']; }  ?>"><img class="img-responsive" src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $thisItem['productImage']; ?>" alt="" > </a>  </div>
                  <div class="media-body">
                    <p><?php echo $thisItem['productName']; ?><span style="color:red"><?php echo $setStockData; ?></span></p>
                  </div>
                </div></td>
                <td class="text-center">
                <div class="quinty padding-top-20 cp_quntty">
                <input name="product[<?php echo  $i-1; ?>][product_id]" value="<?php echo $thisItem['cartId']; ?>" size="2" type="hidden">
                <input name="product[<?php echo  $i-1; ?>][quantity]" value="<?php echo $thisItem['count']; ?>" size="2" type="number">
                </div></td>
              <td class="text-center padding-top-60"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $main_price; ?></td>
              <td class="text-center padding-top-60"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $total_price; ?></td>
              <td class="text-center padding-top-60"><a href="javascript:;"  data-replace="<?php echo $total_price; ?>" dat data-id="<?php echo $thisItem['cartId']; ?>" class="remove cp_cart_remove"><i class="fa fa-close"></i></a></td>
            </tr>
            <?php $i++; } ?>
          </tbody>
          <?php echo form_close(); ?>
        </table>
        </div>
            </div>

          <!-- Grand total -->
          <div class="g-totel">
            <h5>Grand total: <span class="set-grand-total"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span></h5>
          </div>
        </div>

        <!-- Button -->
        <div class="pro-btn"> <a href="<?php echo base_url() ?>home/<?php echo $this->session->userdata('shopName') ?>" class="btn-round btn-light">Continue Shopping</a>
        <a href="<?php echo base_url() ?>Checkout" class="btn-round">Proceed to checkout</a>
        <a href="javascript:;" class="btn-round update_cart">Update Shopping Cart</a> </div>
        <?php }
        ?>
      </div>
    </section>
  </div>
  <!-- End Content -->
  <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>
