<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12 m-b-15">

                                <h4 class="page-title">Add Product Type</h4>

                            </div>
                        </div>

                        <div class="row">
  <div class="col-sm-12">
  <div class="card-box">

  <form method="post">
  <div class="row">
  <div class="col-md-12 form-horizontal">
  <div class="form-group">
  <label class="col-md-2 control-label">Business Category</label>
  <div class="col-md-8">
  <select class="form-control">
    <option>1</option>
    <option>2</option>
    <option>3</option>
  </select>
  </div>
  </div>
  <div class="form-group">
  <label class="col-md-2 control-label">Product Type Name</label>
  <div class="col-md-8">
  <input type="text" class="form-control" value="" name="" placeholder="Product Type Name">
  </div>
  </div>
  <div class="form-group">
  <label class="col-md-2 control-label" for="example-email">Product Description</label>
  <div class="col-md-8">
  <textarea class="form-control"></textarea>
  </div>
  </div>
  <div class="form-group">
  <label class="col-md-2 control-label">Status</label>
  <div class="col-md-8">
  <select class="form-control" name="status">
  <option value="">Select Status</option>
  <option value="0">Deactive</option>
  <option value="1">Active</option>
  <option value="2">Block</option>
  </select>
  </div>
  </div>
  <div class="form-group">
  <label class="col-md-2 control-label">Choose Attribute</label>
  <div class="col-md-8">


                                  <label class="checkbox-inline text-black">
                                  <input type="checkbox" name="" class=" " value="">One
                                  </label>
                                  <label class="checkbox-inline text-black">
                                  <input type="checkbox" name="" class="" value="">Two
                                  </label>
                                  <label class="checkbox-inline text-black">
                                  <input type="checkbox" name="" class="" value="">Three
                                  </label>
                                  <label class="checkbox-inline text-black">
                                  <input type="checkbox" name="" class="" value="">Fourth
                                  </label>


  </div>
  </div>
  </div>

  <div class="col-md-12 text-center">
  <input type="submit" class="btn btn-default  m-r-10" name="searchCustomer" value="Submit">

  </div>
  </div>
  </form>
  </div>
  </div>
  </div>



                    </div> <!-- container -->

                </div> <!-- content -->


  <?php $this->load->view('Template/footer.php') ?>
