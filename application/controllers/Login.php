<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	function __construct() 
    {
        parent::__construct();
        $this->load->library('form_validation'); 
        $this->load->model('Shoploginmodel');
	}
	public function loginCheckData()
	{
		$this->form_validation->set_rules('username','text','required|valid_email');
		$this->form_validation->set_rules('password','text','required');
		if($this->form_validation->run() == true)
		{
			$setArrayData = array('username'=>$this->input->post('username'),'pass'=>$this->input->post('password'));
			$getDataSetData = $this->Shoploginmodel->checkUserLoginData($setArrayData);
			if($getDataSetData['message'] == 'email')
			{
				echo 'Please enter correct email address';
			}
			else if($getDataSetData['message'] == 'pass')
			{
				echo 'Please enter correct password';
			}
			else if($getDataSetData['message'] == 'support')
			{
				echo 'You are not active from admin side. Please contact to support';
			}
			else if($getDataSetData['message'] == 'no')
			{
				echo 'Something went wrong. Please try again';
			}
			else if($getDataSetData['message'] == 'package')
			{
				echo 'Your package is expired. Please upgrade your package';
			}
			else if($getDataSetData['message'] == 'success')
			{
				setcookie('tempAuth', $getDataSetData['authData'], time() + (86400 * 30), "/"); // 86400 = 1 day
				// $this->session->set_userdata('vendorAuth',);
				echo 'success';
			}
		}
		else
		{
			echo "mandatory";
		}
	}
	public function index()
	{
		$this->load->view('loginshop');
	}
}
