<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header") ?>

    <section>
        <div class="container py-85 py-tn-50">
            <div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8 text-center">
                            <h2 class="title">Forgot Password</h2>
                            <div class="title-border mx-auto m-b-55 m-b-tn-35"></div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php
                if($this->session->flashdata('error'))
                {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                }
                if($this->session->flashdata('success'))
                {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                }
                ?>
                </div>
            </div>
            <div class="port-body">
                <div class="contact-form">
                    <?php echo form_open(base_url( 'Forgot/updatePass' ), array( 'id' => 'forgotPass', 'method'=>'post' ));?>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <input type="text" name="forgotPin" class="required_validation_for_user_forgot name_validation number_validation" maxlength="4" />
                                </div>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <input type="password" name="forgotPass" class="required_validation_for_user_forgot name_validation opass" />
                                </div>
                            </div>
            </div>
                            <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <input type="password" name="forgotConPass" class="required_validation_for_user_forgot name_validation cpass"  />
                                </div>
                            </div>
            </div>
            <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group au-form">
                                <input type="submit" class="mx-auto acc_btn text-center setDisableData" value="Reset Password"/>
                                </div>
                            </div>
            </div>
                           
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            

        </div>


    </section>
    <?php 
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer") ?>


    <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#forgotPass',function(){
                var check_required_field='';
                $(this).find(".required_validation_for_user_forgot").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field =$(this).size();
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>