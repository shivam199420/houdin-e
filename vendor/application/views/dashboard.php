<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>

<script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/exporting.js"></script>
 
<style type="text/css">
  @media screen and (max-width: 767px){
.table-responsive>.table>tbody>tr>td, .table-responsive>.table>tbody>tr>th, .table-responsive>.table>tfoot>tr>td, .table-responsive>.table>tfoot>tr>th, .table-responsive>.table>thead>tr>td, .table-responsive>.table>thead>tr>th {
    white-space: normal!important;
}
}

.highcharts-credits{
    display:none;
}
li span.round-tabs.five{border:0px !important;}
li span.round-tabs.five svg{
    position:absolute;
    top:0px;
    left:0px;
}
</style>
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                              <div class="col-md-8">
                                <h4 class="page-title">Dashboard</h4>
                                <p class="text-muted page-title-alt">Welcome to Houdin-e Vendor Panel !</p>
                            </div>
                        </div>
                        <!--tabs-->
                        <section>
         
            <div class="row">
            <div class="col-sm-12">
                <div class="board">

                    <div class="card-box">
                    <!--<div class="col-md-12 form-horizontal hidden-md hidden-lg">
                        <ul class="custom_ul_hiddenxs">
                        <li class="custom_li_hiddenxs"><i class="fa fa-check"></i> <a href="">Business Info</a></li>
                        <li class="custom_li_hiddenxs"><i class="fa fa-check"></i> <a href="">Campaign</a></li>
                        <li class="custom_li_hiddenxs"><i class="fa fa-check"></i> <a href="">Customer</a></li>
                        <li class="custom_li_hiddenxs"><i class="fa fa-check"></i> <a href="">Inventory</a></li>
                        <li class="custom_li_hiddenxs"><i class="fa fa-check"></i> <a href="">Payment Gateway</a></li>
                        <li class="custom_li_hiddenxs"><i class="fa fa-check"></i> <a href="">Promotion</a></li>
                        <li class="custom_li_hiddenxs"><i class="fa fa-check"></i> <a href="">Social Slider Testimonials</a></li>
                        

                        </ul>
                                      
                    </div>-->
    <?php 
     //print_r($Shipping);
     if($setting_business['href']!='#'){
        $b_business_hrf= 'href="'.base_url().$setting_business['href'].'"';
     }
    
     if($setting_store['href']!='#'){
        $setting_store_hrf= 'href="'.base_url().$setting_store['href'].'"';
     }
     if($Shipping['href']!='#'){
        $Shipping_hrf= 'href="'.base_url().$Shipping['href'].'"';
     }
     if($Categories['href']!='#'){
        $Categories_hrf= 'href="'.base_url().$Categories['href'].'"';
     }
     if($Product['href']!='#'){
        $Product_hrf= 'href="'.base_url().$Product['href'].'"';
     }
     if($Staff['href']!='#'){
        $Staff_hrf= 'href="'.base_url().$Staff['href'].'"';
     }
     if($Supplier['href']!='#'){
        $Supplier_hrf= 'href="'.base_url().$Supplier['href'].'"';
     }
    ?>
                      <div class="row">
                      <div class="hidden-xs">
                        <div class="col-md-11">
                         
                    <ul class="nav nav-tabs1" >
                    <div class="liner"></div>
                     <li class="<?=$setting_business['class'];?>">
                     <a <?=$b_business_hrf;?>  title="Business Info">
                      <span class="round-tabs five " data-padding=0 data-thickness=3 data-radius="22" data-donutty data-min=0 data-max=9 data-value=<?=$setting_business['val'];?> data-circle=true data-color="#FF5722">
                              <i class="fa fa-check-circle"></i>
                      </span>
                     
                      <p class="text_p"> <?php //echo $this->session->userdata('vendorRole'); ?>
                      Business Info</p>
                  </a></li>

                  <li class="<?=$setting_store['class'];?>">
                     <a <?=$setting_store_hrf;?>  title="Store Info">
                      <span class="round-tabs five " data-padding=0 data-thickness=3 data-radius="22" data-donutty data-min=0 data-max=9 data-value=<?=$setting_store['val'];?> data-circle=true data-color="#FF5722">
                              <i class="fa fa-check-circle"></i>
                      </span>
                     <p class="text_p">Store Info</p>
           </a>
                 </li>
                 <li class="<?=$Supplier['class'];?>">
                 <a <?=$Supplier_hrf;?> title="Supplier">
                     <span class="round-tabs five "  data-padding=0 data-thickness=3 data-radius="22" data-donutty data-min=0 data-max=1 data-value=<?=$Supplier['val'];?> data-circle=true data-color="#FF5722">
                          <i class="fa fa-check-circle"></i>
                     </span>
                     <p class="text_p">Supplier</p>
                    </a>
                     </li>

                    <li class="<?=$Shipping['class'];?>">
                     <a <?=$Shipping_hrf;?>  title="Shipping Info">
                      <span class="round-tabs five " data-padding=0 data-thickness=3 data-radius="22" data-donutty data-min=0 data-max=2 data-value=<?=$Shipping['val'];?> data-circle=true data-color="#FF5722">
                              <i class="fa fa-check-circle"></i>
                      </span>
                         <p class="text_p">Shipping Info</p>
                     </a></li>

                    <li class="<?=$Categories['class'];?>">
                     <a <?=$Categories_hrf;?>  title="Categories">
                   
                         <span class="round-tabs five" data-padding=0 data-thickness=3 data-radius="22" data-donutty data-min=0 data-max=2 data-value=<?=$Categories['val'];?> data-circle=true data-color="#FF5722">
                              <i class="fa fa-check-circle"></i>
                         </span><p class="text_p">Categories</p> </a>
                     </li>
                     <li class="<?=$Product['class'];?>">
                     <a <?=$Product_hrf;?>  title="Products">
                         <span class="round-tabs five" data-padding=0 data-thickness=3 data-radius="22" data-donutty data-min=0 data-max=1 data-value=<?=$Product['val'];?>  data-circle=true data-color="#FF5722">
                              <i class="fa fa-check-circle"></i>
                         </span><p class="text_p">Products</p> </a>
                     </li>
                     <li class="<?=$Staff['class'];?>">
                     <a <?=$Staff_hrf;?>  title="Staff Management">
                         <span class="round-tabs five"  data-padding=0 data-thickness=3 data-radius="22" data-donutty data-min=0 data-max=1 data-value=<?=$Staff['val'];?> data-circle=true data-color="#FF5722">
                              <i class="fa fa-check-circle"></i>
                         </span><p class="text_p">Staff Management</p> </a>
                     </li>


                   </ul></div></div>

             <?php 
             $total_max_data=25;

           $total_file=$Staff['val']+$Product['val']+$Categories['val']+$Shipping['val']+$Supplier['val']+$setting_store['val']+$setting_business['val'];

          $new_width = ($total_file * 100) / $total_max_data;
             ?>

                   <div class="col-md-1">	<div class="widget-chart text-center">
                               <input class="knob" data-width="60" data-height="60" data-linecap=round data-fgColor="#FF5722" value="<?=$new_width;?>%" data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15"/>

                         </div></div>
                 </div></div>



</div>
</div>
</div>

</section>
<div class="row">
<div class="col-sm-12">

    <ul class="nav nav-tabs tabs">
        <li class="active tab">
            <a href="#home-2" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs"><i class="fa fa-home"></i></span>
                <span class="hidden-xs">Visits</span>
            </a>
        </li>
        <li class="tab">
            <a href="#profile-2" data-toggle="tab" aria-expanded="false">
                <span class="visible-xs"><i class="fa fa-user"></i></span>
                <span class="hidden-xs">Orders</span>
            </a>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="home-2">
          <div class="text-center">
          

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
          </div>
          <div id="morris-bar-stacked"  ></div>
        </div>
        <div class="tab-pane" id="profile-2">
          <div class="text-center">
          <div id="orderchart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>

                <div id="morris-area-with-dotted"></div>
        </div>

    </div>


</div>

              <div class="row m-t-20">
              <div class="col-sm-12 m-t-20">
                      <div class="col-lg-3 col-sm-6">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Order.png">
                              <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($totalOrderAmount) { echo number_format((float)$totalOrderAmount, 2, '.', ''); } else { echo 0; } ?></b></h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Order Amount</div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-sm-6">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Completed_orders.png">
                              <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($totalPaidAmount) { echo number_format((float)$totalPaidAmount, 2, '.', ''); } else { echo 0; } ?></b></h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Paid Amount</div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-sm-6">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Pending_Order.png">
                              <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($totalRemainigAmount) { echo number_format((float)$totalRemainigAmount, 2, '.', ''); } else { echo 0; } ?></b></h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Remaining Amount</div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-sm-6">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Pending_Order.png">
                              <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($totalTransaction) { echo number_format((float)$totalTransaction, 2, '.', ''); } else { echo 0; } ?></b> </h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Transaction</div>
                          </div>
                      </div>
                     </div>
                    </div>

                        <div class="row">
                     <div class="col-sm-12">
                            <div style="float:right;margin-right: 10px">
                              <button type="button" class="btn btn-info click_show m-b-20">Show More</button>
                            </div>
                            </div>
                          </div>
                          <div class="row hide_div">
                          <div class="col-sm-12">
                            <div class="col-lg-3 col-sm-6">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Order.png">
                              <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($totalTransactionCredit) { echo number_format((float)$totalTransactionCredit, 2, '.', ''); } else { echo 0; } ?></b></h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Credit</div>
                          </div>
                      </div>
                           <div class="col-lg-3 col-sm-6">
                          <div class="widget-panel widget-style-2 bg-light">
                              <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Order.png">
                              <h2 class="m-0 text-dark counter font-600"><b class="counter"><?php if($totalTransactionDebit) { echo number_format((float)$totalTransactionDebit, 2, '.', ''); } else { echo 0; } ?></b></h2>
                              <div class="text-muted m-t-5 custom_width_sm">Total Debit</div>
                          </div>
                      </div>
                      

                            <div style="float:right;">
                              <button type="button" class="btn btn-info click_hide m-b-20">Hide</button>
                            </div>
                            </div>
                        </div>


                        <!-- end row -->


                        <div class="row">

                        <div class="col-sm-12">
                          <div class="col-lg-6">
                            <div class="card-box">
                                    <a href="#" class="pull-right btn btn-default btn-sm waves-effect waves-light">View All</a>
                              <h4 class="text-dark header-title m-t-0">New Customer</h4>
                              <p class="text-muted m-b-30 font-13">
                              Latest New Customer List.
                        </p>
                         
                              <div class="table-responsive">
                                        <table class="table table-actions-bar m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Email </th>
                                                    <th>Contact</th>
                                                     
                                                </tr>
                                            </thead>
                                            <tbody>
 
                                            <?php 
                                            foreach($customerList as $customerLists){

                                               // print_r($customerLists->houdinv_user_name);  
                                            
                                            ?>

                                                <tr>
                                                    <td><?=$customerLists->houdinv_user_name;?></td>
                                                    <td><?=$customerLists->houdinv_user_email;?></td>

                                                    <td><?=$customerLists->houdinv_user_contact;?></td>                                                     
                                                    
                                                </tr> 
                                            <?php } ?>                                               

                                            </tbody>
                                        </table>
                                    </div>

                            </div>
                          </div>
                            <!-- col -->

                        	<div class="col-lg-6">
                        		<div class="card-box">
                                    <a href="#" class="pull-right btn btn-default btn-sm waves-effect waves-light">View All</a>
                        			<h4 class="text-dark header-title m-t-0">New Orders</h4>
                        			<p class="text-muted m-b-30 font-13">
                                    Latest New Order List
									</p>

                        			<div class="table-responsive">
                                        <table class="table table-actions-bar m-b-0">
                                            <thead>
                                                <tr>
                                                    <th>Order ID</th>
                                                    <th>Ordered On</th>
                                                    <th>Order Mode(Web,App)</th>
                                                    <th style="min-width: 80px;">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                            <?php 
                                            foreach($latest_order as $latest_orders){

                                              //print_r($latest_orders);  
                                            
                                            ?>

                                                <tr>
                                                    <td><?=$latest_orders->houdinv_order_id;?></td>
                                                    <td><?php echo date("Y-m-d, h:i:s",$latest_orders->houdinv_order_created_at); ?></td>

                                                    <td><?=$latest_orders->houdinv_order_type;?></td>        
                                                    <td><?=$latest_orders->houdinv_orders_total_Amount;?></td>                                                
                                                    
                                                </tr> 
                                            <?php } ?>   

                                                 

                                            </tbody>
                                        </table>
                                    </div>

                        		</div>
                        	</div>
                          </div>
                        	<!-- end col -->



                        </div>

 


                        </div>
                        </div>
                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->
<?php 

$counts='';
$visitors_date='';
foreach($visitors as $visitor)
{
   
$counts.=$visitor['count'].',';
$visitors_date.="'". $visitor['dates']."',";
} 
 

$counts_order='';
$order_date='';
foreach($orders_chart as $order_chart)
{
   
$counts_order.=$order_chart['count'].',';
$order_date.="'". $order_chart['dates']."',";
} 
 
?>


<?php $this->load->view('Template/footer.php') ?>
<script>
$(document).ready(function(){
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});


Highcharts.chart('container', {
    chart: {
        type: 'area'
    },
    title: {
        text: 'Latest visitors '
    },
    subtitle: {
      //  text: 'Source: Wikipedia.org'
    },
    xAxis: {
        categories: [<?=$visitors_date;?>],
        tickmarkPlacement: 'on',
        title: {
            enabled: false
        }
    },
    yAxis: {
        title: {
           // text: 'Billions'
        },
        labels: {
           /* formatter: function () {
                return this.value / 1000;
            }*/
        }
    },
    tooltip: {
        split: true,
        valueSuffix: ' visitors'
    },
    plotOptions: {
        area: {
            stacking: 'normal',
            lineColor: '#666666',
            lineWidth: 1,
            marker: {
                lineWidth: 1,
                lineColor: '#666666'
            }
        }
    },
    series: [{
        name: 'Visits',
        data: [<?=$counts?>]
    
    }]
});
 



Highcharts.chart('orderchart', {
    chart: {
        type: 'area'
    },
    title: {
        text: 'Latest Orders '
    },
    subtitle: {
      //  text: 'Source: Wikipedia.org'
    },
    xAxis: {
        categories: [<?=$order_date;?>],
        tickmarkPlacement: 'on',
        title: {
            enabled: false
        }
    },
    yAxis: {
        title: {
           // text: 'Billions'
        },
        labels: {
           /* formatter: function () {
                return this.value / 1000;
            }*/
        }
    },
    tooltip: {
        split: true,
        valueSuffix: ' Orders'
    },
    plotOptions: {
        area: {
            stacking: 'normal',
            lineColor: '#5fbeaa',
            lineWidth: 1,
            marker: {
                lineWidth: 1,
                lineColor: '#5fbeaa'
            }
        }
    },
    series: [{
        name: 'Orders',
        color: '#5fbeaa',
        data: [<?=$counts_order;?>]
    
    }]
});
</script>
<script src="<?=base_url();?>assets/js/donutty-jquery.js"></script>