<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view($getShopName."/Template/header");
if(count($product) <= 0)
{
    redirect(base_url()."home/".$this->session->userdata('shopName'), 'refresh');
}
$images_all = json_decode($product['main_product']->houdinv_products_main_images);
$price_all = json_decode($product['main_product']->houdin_products_price,true);
$final_price = $product['main_product']->houdin_products_final_price;
$overall_rating = $product['reviews_sum']->houdinv_product_review_rating;
$total_rating = count($product['reviews']);
$final_rating = $overall_rating/$total_rating;
    ?>
    <?php
    $getCurrency = getShopCurrency();
    $currencysymbol=$getCurrency[0]->houdin_users_currency;
    ?>
    <style>
    .checked {
    color: orange;
}
.ir {
  text-indent: 100%;
  white-space: nowrap;
  overflow: hidden;
}

/**
 * Gallery Styles
 * 1. Enable fluid images
 */
.gallery {
  overflow: hidden;
}

.gallery__hero {
  overflow: hidden;
  position: relative;
  padding: 2em;
  margin: 0 0 0.3333333333em;
  background: #fff;
  text-align:center;
  width: 500px !important;
}
.is-zoomed .gallery__hero {
  cursor: move;
}
.is-zoomed .gallery__hero img {
  max-width: none;
  position: absolute;
  z-index: 0;
  top: -50%;
  left: -50%;
}

.gallery__hero-enlarge {
  position: absolute;
  right: 0.5em;
  bottom: 0.5em;
  z-index: 1;
  width: 30px;
  height: 30px;
  opacity: 0.5;
  background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMCIgaGVpZ2h0PSIzMCIgdmlld0JveD0iNS4wIC0xMC4wIDEwMC4wIDEzNS4wIiBmaWxsPSIjMzRCZjQ5Ij48cGF0aCBkPSJNOTMuNTkzIDg2LjgxNkw3Ny4wNDUgNzAuMjY4YzUuNDEzLTYuODczIDguNjQyLTE1LjUyNiA4LjY0Mi0yNC45MTRDODUuNjg3IDIzLjEwNCA2Ny41OTMgNSA0NS4zNDMgNVM1IDIzLjEwNCA1IDQ1LjM1NGMwIDIyLjI0IDE4LjA5NCA0MC4zNDMgNDAuMzQzIDQwLjM0MyA5LjQgMCAxOC4wNjItMy4yNCAyNC45MjQtOC42NTNsMTYuNTUgMTYuNTZjLjkzNy45MjcgMi4xNjIgMS4zOTYgMy4zODggMS4zOTYgMS4yMjUgMCAyLjQ1LS40NyAzLjM5LTEuMzk2IDEuODc0LTEuODc1IDEuODc0LTQuOTEyLS4wMDItNi43ODh6bS00OC4yNS0xMC43MWMtMTYuOTU0IDAtMzAuNzUzLTEzLjc5OC0zMC43NTMtMzAuNzUyIDAtMTYuOTY0IDEzLjgtMzAuNzY0IDMwLjc1My0zMC43NjQgMTYuOTY0IDAgMzAuNzUzIDEzLjggMzAuNzUzIDMwLjc2NCAwIDE2Ljk1NC0xMy43ODggMzAuNzUzLTMwLjc1MyAzMC43NTN6TTYzLjAzMiA0NS4zNTRjMCAyLjM0NC0xLjkwNyA0LjI2Mi00LjI2MiA0LjI2MmgtOS4xNjR2OS4xNjRjMCAyLjM0NC0xLjkwNyA0LjI2Mi00LjI2MiA0LjI2Mi0yLjM1NSAwLTQuMjYyLTEuOTE4LTQuMjYyLTQuMjYydi05LjE2NGgtOS4xNjRjLTIuMzU1IDAtNC4yNjItMS45MTgtNC4yNjItNC4yNjIgMC0yLjM1NSAxLjkwNy00LjI2MiA0LjI2Mi00LjI2Mmg5LjE2NHYtOS4xNzVjMC0yLjM0NCAxLjkwNy00LjI2MiA0LjI2Mi00LjI2MiAyLjM1NSAwIDQuMjYyIDEuOTE4IDQuMjYyIDQuMjYydjkuMTc1aDkuMTY0YzIuMzU1IDAgNC4yNjIgMS45MDcgNC4yNjIgNC4yNjJ6Ii8+PC9zdmc+);
  background-repeat: no-repeat;
  transition: opacity 0.3s cubic-bezier(0.455, 0.03, 0.515, 0.955);
}
.gallery__hero-enlarge:hover {
  opacity: 1;
}
.is-zoomed .gallery__hero-enlarge {
  background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMCIgaGVpZ2h0PSIzMCIgdmlld0JveD0iNS4wIC0xMC4wIDEwMC4wIDEzNS4wIiBmaWxsPSIjMzRCZjQ5Ij48cGF0aCBkPSJNOTMuNTkzIDg2LjgxNkw3Ny4wNDUgNzAuMjY4YzUuNDEzLTYuODczIDguNjQyLTE1LjUyNiA4LjY0Mi0yNC45MTRDODUuNjg3IDIzLjEwNCA2Ny41OTMgNSA0NS4zNDMgNVM1IDIzLjEwNCA1IDQ1LjM1NGMwIDIyLjI0IDE4LjA5NCA0MC4zNDMgNDAuMzQzIDQwLjM0MyA5LjQgMCAxOC4wNjItMy4yNCAyNC45MjQtOC42NTNsMTYuNTUgMTYuNTZjLjkzNy45MjcgMi4xNjIgMS4zOTYgMy4zODggMS4zOTYgMS4yMjUgMCAyLjQ1LS40NyAzLjM5LTEuMzk2IDEuODc0LTEuODc1IDEuODc0LTQuOTEyLS4wMDItNi43ODh6TTE0LjU5IDQ1LjM1NGMwLTE2Ljk2NCAxMy44LTMwLjc2NCAzMC43NTMtMzAuNzY0IDE2Ljk2NCAwIDMwLjc1MyAxMy44IDMwLjc1MyAzMC43NjQgMCAxNi45NTQtMTMuNzkgMzAuNzUzLTMwLjc1MyAzMC43NTMtMTYuOTUzIDAtMzAuNzUzLTEzLjgtMzAuNzUzLTMwLjc1M3pNNTguNzcyIDQ5LjYxSDMxLjkyYy0yLjM1NSAwLTQuMjYzLTEuOTA3LTQuMjYzLTQuMjZzMS45MDgtNC4yNjMgNC4yNjItNC4yNjNINTguNzdjMi4zNTQgMCA0LjI2MiAxLjkwOCA0LjI2MiA0LjI2MnMtMS45MSA0LjI2LTQuMjYyIDQuMjZ6Ii8+PC9zdmc+);
}

.gallery__thumbs {
  /*text-align: center;*/
  background: #fff;
}
.gallery__thumbs a {
  display: inline-block;
  
  padding: 0.5em;
  opacity: 0.75;
  transition: opacity 0.3s cubic-bezier(0.455, 0.03, 0.515, 0.955);
}
.gallery__thumbs a:hover {
  opacity: 1;
}
.gallery__thumbs a.is-active {
  opacity: 0.2;
}
.sizes{
    display:inline-flex;
}
.size-variants{
    margin-right: 10px;
    border: 1px solid #a0a0a0;
    border-radius: 50%;
    width: 45px;
    height: 45px;
    padding: 6px 8px;
    line-height: 30px;
    text-align: center;
}

.size-active{
    margin-right: 10px;
    border: 1px solid #c73f57;
    border-radius: 50%;
    width: 45px;
    height: 45px;
    padding: 6px 8px;
    line-height: 30px;
    text-align: center;
    color: #c73f57;
}


#radios label {
	cursor: pointer;
	position: relative;
}

/*#radios label + label {*/
/*	margin-left: 15px;*/
/*}*/

input[type="radio"] {
opacity: 0;
    position: absolute;
        height: 34px;
       width: 55px;
    margin-top: -6px;
}

input[type="radio"] + span {
        color: #333333;
    /*border-radius: 50%;*/
    padding: 7px 17px;
    transition: all 0.4s;
    -webkit-transition: all 0.4s;
    border:1px solid #333;
}

input[type="radio"]:checked + span {
  color: #ffffff;
    border: 1px solid #333333;
    background-color: #333;
}

input[type="radio"]:focus + span {
	color: #d9527d;
}
.material-iconsP{
        border-radius: 50%;
    width: 45px;
    height: 45px;
    line-height: 42px;
    text-align: center;
}
.thumpImage{
    height: 100px;
    width: 100px;
}
/* ================ TOOLTIPS ================= */



/* =============================================
* CENTERING, CONTAINER STYLING ETC || IGNORE
=============================================== */



#radios {
	/*text-align: center;*/
	margin 0 auto;
}
.img-main {
      
    min-height: 300px; 
    max-height: 300px;
    overflow: hidden;
    max-width: 100%;
}

</style>
       <!-- Page Title -->
       <section>
        <div class="container py-85 py-tn-50">
            <div class="port-title">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-12 ">
                            <h2 class="title text-center">Product List</h2>
                             <div class="title-border mx-auto m-b-tn-35 text-center"></div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </section>

    <!-- End Page Title -->

    <!-- Product Detail -->
    <section>
    <div class="row">
        <div class="container">
        <div class="col-sm-12">
        <?php
        if($this->session->flashdata('error'))
        {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        if($this->session->flashdata('success'))
        {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>
        </div>
        <div class="container p-b-90 p-t-100">
            <div class="row">
                <div class="col-md-12">
                 
                    <div class="product-detail">
                        <div class="shop-product p-t-25">
                        <div class="slide100-01" id="slide100sliderproduct">
                                <!--<div class="main-wrap-pic">-->
                                <!--    <div class="main-frame">-->
                                <!--        <div class="wrap-main-pic">-->
                                <!--            <div class="main-pic">-->
                                <!--                <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $images_all[0]; ?>" alt="image">-->
                                <!--            </div>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
 <!-- Gallery -->
  <div id="js-gallery" class="gallery">

    <!--Gallery Hero-->
    <div class="gallery__hero">
      <!--<a href="" class="gallery__hero-enlarge ir" data-gallery="zoom">Zoom</a>-->

      <img class="img-main variantsimageR" src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $images_all[0]; ?>">
    </div>
    <!--Gallery Hero-->

    <!--Gallery Thumbs-->
    <div class="gallery__thumbs">
        
        <?php
                                    $setImageData = "";
                                    for($index = 0; $index < count($images_all); $index++)
                                    {
                                        $setImageData = "";
                                        if($images_all[$index])
                                        {
                                            $setNumberData = $index+1;
                                            if($index=='0')
                                            {
                                                $setImageData ="is-active";
                                            }
                                             
                                            ?>
        
        <a href="<?=$this->session->userdata('vendorURL')."upload/productImage/". $images_all[$index];?>" data-gallery="thumb" class="<?=$setImageData;?>  thumpImage">
          <img class="thumpImage" src="<?=$this->session->userdata('vendorURL')."upload/productImage/". $images_all[$index];?>">
        </a>
         <?php } }     ?>
        
        <!--<a href="https://public-619e3.firebaseapp.com/Product-Gallery/products/normal/product-01_view-02.jpg" data-gallery="thumb">-->
        <!--  <img src="https://public-619e3.firebaseapp.com/Product-Gallery/products/thumb/product-01_view-02.jpg">-->
        <!--</a>-->
        <!--<a href="https://public-619e3.firebaseapp.com/Product-Gallery/products/normal/product-01_view-03.jpg" data-gallery="thumb">-->
        <!--  <img src="https://public-619e3.firebaseapp.com/Product-Gallery/products/thumb/product-01_view-03.jpg">-->
        <!--</a>-->
    </div>
    <!--Gallery Thumbs-->

  </div><!--.gallery-->
  <!-- Gallery -->
                               
                            </div>
                            
                            <div class="product-body">
                                <h3 class="name"><?php echo $product['main_product']->houdin_products_title; ?></h3>
                                <span class="price variantspricesR">
                                <?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo number_format((float)$final_price, 1, '.', ''); ?></span>
                                <span class="price"><strike>(<?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo number_format((float)$price_all['sale_price'], 1, '.', '');  ?>)</strike></span>

                                <!-- <p class="price">$35.00</p> -->
                                <p class="description"><?php echo $product['main_product']->houdin_products_short_desc; ?></p>
                               
                                <?php

   //print_r($product['related']); 

                                if(!empty($product['related'])){
                                    ?>
                                    <div id="radios">
                                        <?php foreach($product['related'] as $mkey=>$related)
                             { 
                                // print_r($related);
                                         if($related->houdin_products_variants_image)
                                                {
                                                    $setVariantIMageData = $this->session->userdata('vendorURL')."upload/productImage/".$related->houdin_products_variants_image;
                                                }
                                                else
                                                {
                                                    $setVariantIMageData = base_url()."Extra/noPhotoFound.png";
                                                }
                                                ?>
                           
		<label for="<?php echo $related->houdinv_products_variants_name; ?>" class="material-icons">
			<input type="radio" require="true" <?php if($mkey==0){echo "checked";}?>  data-image="<?=$setVariantIMageData;?>" data-price="<?=$related->houdinv_products_variants_final_price;?>" class="variantsradionbutton" name="houdin_products_variants" id="<?php echo $related->houdinv_products_variants_name; ?>" value="<?php echo $related->houdin_products_variants_id ?>" />
			<span><?php echo $related->houdinv_products_variants_name; ?></span>
		</label>
		
		<?php } ?>
		 
	</div>
	<p>&nbsp;</p>
       <?php  }?>   
                                 <div class="product-button">
                                    <div class="quantity">

                                        <input value="1" min="1" data-max="<?php echo $product['main_product']->houdinv_products_total_stocks ?>" name="qttybutton" class="cart-plus-minus-box number_validation" type="number">

                                    </div>

                                    <?php
                                    if($product['setcartStatus'] == 'yes')
                                    {
                                      ?>
                                      <a href="javascript:;" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" class="add-to-cart acc_btn">Already in cart</a>
                                    <?php }
                                    else {
                                      ?>
                                      <a href="javascript:;" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" class="add-to-cart acc_btn Add_to_cart_button">Add to cart</a>
                                    <?php }
                                    ?>

                                    <?php
                                    if($product['setWishlistStatus'] == 'yes')
                                    {
                                      ?>
                                      <a href="javascript:;" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" class="add-to-wishlist acc_btn btn_icn" style="color:red;"></a>
                                    <?php }
                                    else {
                                      ?>
                                      <a href="javascript:;" data-variant="0"  data-cart="<?php echo $product['main_product']->houdin_products_id ?>" class="add-to-wishlist acc_btn btn_icn Add_to_whishlist_button"></a>
                                    <?php }
                                    ?>

                                </div>
                                <div class="product-available">
                                    <span>Available :</span>
                                    <?php
                                    if($product['main_product']->houdinv_products_total_stocks > 0)
                                    {
                                        echo '<a href="javascript:;">In stock</a>';
                                    }
                                    else
                                    {
                                        echo '<a href="javascript:;" class="text-danger">Out of stock</a>';
                                    }
                                    ?>
                                </div>
                                <?php
                                if($product['main_product']->houdin_products_main_sku)
                                {
                                ?>
                                <div class="product-sku">
                                    <span class="text-black">SKU: </span>
                                    <?php echo $product['main_product']->houdin_products_main_sku ?>
                                </div>
                                <?php }
                                ?>

                                <div class="product-rating" style="top:0px !important;" data-star="<?php if($final_rating) { echo $final_rating; } else { echo 0; } ?>"> <?php if($total_rating){ echo "(".$total_rating.")"; }?> </div>
                            </div>
                        </div>
                        <div class="au-tabs">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#description-tab" class="active show">Description</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#additional-tab">additional information </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#review-tab">review (<?php echo $total_rating; ?>)</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="description-tab" class="tab-pane active">
                                <?php echo $product['main_product']->houdin_products_desc; ?>
                                </div>
                                <div id="additional-tab" class="tab-pane">
                                    <table class="product-additionnal">
                                        <?php
                                        $getAdditionalInfo = json_decode($product['main_product']->houdinv_products_main_shipping,true);
                                        ?>
                                        <tr>
                                            <th>Weight</th>
                                            <td><?php if($getAdditionalInfo['weight']) { echo $getAdditionalInfo['weight']; } else { echo 'N/A'; } ?></td>
                                        </tr>
                                        <tr>
                                            <th>Dimensions</th>
                                            <td><?php if($getAdditionalInfo['length']) { echo $getAdditionalInfo['length']."*".$getAdditionalInfo['height']."*".$getAdditionalInfo['breath']; } else  { echo 'N/A'; } ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="review-tab" class="tab-pane">
                                    <h5 class="title">REVIEWS</h5>




                                    <?php
                                    if(!$final_rating > 0)
                                    {
                                        ?>
                                        <div class="col-sm-6 pull-left">
                                         <p>There are no reviews yet.</p>
                                    <p class="text-bigger">Be the first to review “Cloud Wall Clock”</p>
                                    </div>
                                    <?php }
                                    else
                                    {
                                    ?>
                                    <div class="review-section-single col-sm-6 pull-left" style="overflow-y: scroll;height: 330px;">
                                        <?php
                                        foreach($product['reviews'] as $user_Review)
                                        {
                                        ?>
                                        <div class="review-tab m-b-10">
                                            <div class="review-name-title font600"><?php echo $user_Review->houdinv_product_review_user_name ?></div>
                                            <div class="star">
                                                <div class="form-group m-b-0">
                                                    <div class="strat-rating">
                                                    <?php for($i=1 ; $i<6 ;$i++)
                                                    {
                                                    if($i <= $user_Review->houdinv_product_review_rating)
                                                    {
                                                    ?>
                                                    <span data-rating="1" style="font-size: 16px;" class="fa fa-star checked"></span>
                                                    <?php }
                                                    else
                                                    {
                                                    ?>
                                                    <span data-rating="1" style="font-size: 16px;" class="fa fa-star"></span>
                                                    <?php } } ?>

                                                    <span class="text-date"><?php echo $user_Review->houdinv_product_review_created_at ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-description"><?php echo  $user_Review->houdinv_product_review_message; ?></div>
                                        </div>
                                        <?php }
                                        ?>
                                    </div>
                                    <?php }
                                    ?>
                                    <div class="comment-rating col-sm-6">
                                        <div class="">
                                        <?php echo form_open(base_url( 'Productlistview' ), array( 'id' => 'rtng_form',"class"=>"checkout_form", 'method'=>'post' ));?>
                                        <div class="form-group">

                                        <div class="strat-rating">
                                            <span data-rating="1" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="2" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="3" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="4" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="5" style="font-size: 22px;" class="fa fa-star"></span>
                                            <input type="hidden"  name="rating" class="rating_data" value="0" />
                                            <input type="hidden"  name="product_id" class="product" value="<?php echo $product_id; ?>" />
                                            <input type="hidden"  name="variant_id" class="" value="0" />
                                        </div>
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation form-control" type="text" name="user_name" placeholder="Your Name" />
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation email_validation form-control" type="text" name="user_email" placeholder="Your Email" />
                                        </div>
                                        <div class="form-group">

                                        <textarea  class="required_validation_for_review form-control" name="user_message" rows="3" placeholder="Write a review"></textarea>
                                        </div>
                                        <input class="btn border-btn submit_form_review" type="submit" name="review_submit" value="Add Review" />
                                        <?php echo form_close(); ?>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php
                        if(count($product['related']) > 0)
                        {
                        ?>
                        <div class="port-title justify-content-center text-center">
                            <h2 class="title">Related Products</h2>
                            <div class="title-border mx-auto m-b-70"></div>
                        </div>
                        <div class="related-products">
                            <div class="owl-carousel row" data-responsive='{"0":{"items":"1"},"576":{"items":"1"},"768":{"items":"2"}, "992":{"items":"3"} }'>
                            <?php
                            foreach($product['related'] as $related)
                            {
                            ?>
                             <div class="col-md-12">
                                    <div class="grid-product">
                                        <div class="image bg-lightblue">
                                            <a href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>">
                                            <?php 
                                                if($related->houdin_products_variants_image)
                                                {
                                                    $setVariantIMageData = $this->session->userdata('vendorURL')."upload/productImage/".$related->houdin_products_variants_image;
                                                }
                                                else
                                                {
                                                    $setVariantIMageData = base_url()."Extra/noPhotoFound.png";
                                                }
                                                ?>
                                                <img src="<?php echo $setVariantIMageData; ?>" alt="Chair">
                                            </a>
                                            <div class="addcart">
                                                <a href="javascript:;" class="Add_to_cart_button" data-variant="<?php echo $related->houdin_products_variants_id ?>"  data-cart="0">Add to cart</a>
                                            </div>
                                        </div>
                                        <a href="<?php echo base_url() ?>Productlistview/variant/<?php echo $related->houdin_products_variants_id ?>" class="name"><?php echo $related->houdin_products_variants_title; ?></a>
                                        <div class="price"> <?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $related->houdinv_products_variants_final_price; ?></div>
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Product Detail -->
    <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view($getShopName."/Template/footer"); ?>
         <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('keyup','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
            if(parseInt($(this).val()) <= 0)
            {
                $(this).val('1');
            }
        })
        $(document).on('change','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
            if(parseInt($(this).val()) <= 0)
            {
                $(this).val('1');
            }
        });


        $('.variantsselect').on('change', function() {
            var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
});


$('.variantsradionbutton').on('change', function() {
    
    if($('[type="radio"]:checked').val()){
    var varintscart=$('[type="radio"]:checked').val();
     var varintscart1=$(this).val();
     var varintsImage=$(this).attr("data-image");
     var varintsprice=$(this).attr("data-price");
    // alert(varintsImage); alert(varintsprice);
    
     $('.variantspricesR').html(varintsprice);
     $('.variantsimageR').attr('src',varintsImage);
    $('.Add_to_cart_button').attr('data-variant',varintscart1);
     $('.Add_to_whishlist_button').attr('data-variant',varintscart1);
     
     $('.Add_to_cart_button').attr('data-cart','0');
     $('.Add_to_whishlist_button').attr('data-cart','0');
    }
});



    });
    </script>
    <?php
                                    $setImageData = "";
                                    for($index = 0; $index < count($images_all); $index++)
                                    {
                                        if($images_all[$index])
                                        {
                                            $setNumberData = $index+1;
                                            if($setImageData)
                                            {
                                                $setImageData = $setImageData.",'".$this->session->userdata('vendorURL')."upload/productImage/". $images_all[$index]."'";
                                            }
                                            else
                                            {
                                                $setImageData = "'".$this->session->userdata('vendorURL')."upload/productImage/". $images_all[$index]."'";
                                            }
                                            ?>
                                           
                                            <?php } } ?>
    
    <script>
     
        !function(e){
            jQuery("#slide100sliderproduct").slide100({
                autoPlay:"false",
                timeAuto:3e3,
                deLay:400,
                linkIMG:[<?php  echo $setImageData;?>],
               linkThumb:[<?php  echo $setImageData;?>]
                
            });
            
        }();




        
    </script>
    <script>
        var App = (function () {

  //=== Use Strict ===//
  'use strict';

  //=== Private Variables ===//
  var gallery = $('#js-gallery');

  //=== Gallery Object ===//
  var Gallery = {
    zoom: function(imgContainer, img) {
      var containerHeight = imgContainer.outerHeight(),
      src = img.attr('src');
      
      if ( src.indexOf('/products/normal/') != -1 ) {
        // Set height of container
        imgContainer.css( "height", containerHeight );
        
        // Switch hero image src with large version
        img.attr('src', src.replace('/products/normal/', '/products/zoom/') );
        
        // Add zoomed class to gallery container
        gallery.addClass('is-zoomed');
        
        // Enable image to be draggable
        img.draggable({
          drag: function( event, ui ) {
            ui.position.left = Math.min( 100, ui.position.left );
            ui.position.top = Math.min( 100, ui.position.top );
          }
        });
      } else {
        // Ensure height of container fits image
        imgContainer.css( "height", "auto" );
        
        // Switch hero image src with normal version
        img.attr('src', src.replace('/products/zoom/', '/products/normal/') );
        
        // Remove zoomed class to gallery container
        gallery.removeClass('is-zoomed');
      }
    },
    switch: function(trigger, imgContainer) {
      var src = trigger.attr('href'),
      thumbs = trigger.siblings(),
			img = trigger.parent().prev().children();
      
      // Add active class to thumb
      trigger.addClass('is-active');
      
      // Remove active class from thumbs
      thumbs.each(function() {
        if( $(this).hasClass('is-active') ) {
          $(this).removeClass('is-active');
        }
      });

      // Reset container if in zoom state
      if ( gallery.hasClass('is-zoomed') ) {
        gallery.removeClass('is-zoomed');
        imgContainer.css( "height", "auto" );
      }

      // Switch image source
      img.attr('src', src);
    }
  };

  //=== Public Methods ===//
  function init() {

    // Listen for clicks on anchors within gallery
    gallery.delegate('a', 'click', function(event) {
      var trigger = $(this);
      var triggerData = trigger.data("gallery");

      if ( triggerData === 'zoom') {
        var imgContainer = trigger.parent(),
        img = trigger.siblings();
        Gallery.zoom(imgContainer, img);
      } else if ( triggerData === 'thumb') {
        var imgContainer = trigger.parent().siblings();
        Gallery.switch(trigger, imgContainer);
      } else {
        return;
      }

      event.preventDefault();
    });

  }

  //=== Make Methods Public ===//
  return {
    init: init
  };

})();

App.init();
        
    </script>
