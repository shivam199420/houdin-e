<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function sendgrid()
{
    $CI =& get_instance();
    $getSendgrid = $CI->db->select('*')->from('houdin_sendgrid')->where('houdin_sendgrid_id','1')->get()->result();
    return $getSendgrid;
}
function logo()
{
    $CI =& get_instance();
    $getLogodata = $CI->db->select('houdin_admin_logo_image')->from('houdin_admin_logo')->where('houdin_admin_logo_id','1')->get()->result();
    $setImage = base_url()."uploads/logo/".$getLogodata[0]->houdin_admin_logo_image;
    return $setImage;
}

