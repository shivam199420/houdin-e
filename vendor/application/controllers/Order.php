<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        $this->auth_users->is_logged_in();
        $this->load->helper('form');
        $this->load->library('Pdf');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Ordermodel');
        $this->perPage = 100;
	}
      public function Orderlanding()
    { 	
        $this->load->view('orderlandingpage');
    }
    public function unbilled()
    {
        // Custom Pagination
        $getTotalCount = $this->Ordermodel->getUnbilledOrderCount();
        $config['base_url']    = base_url().'order/unbilled/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getUnbilledStatus = $this->Ordermodel->getUnbilledOrderDetails($setArray);
        $this->load->view('unbilled',$getUnbilledStatus);
    }
    
    public function allorders()
    {
        $getSmsEmail = smsemailcredentials();
        // update order status
        if($this->input->post('updateOrderStatusBtn'))
        {
            $this->form_validation->set_rules('updateOrderStatusDataValue','status','required');
            if($this->form_validation->run() == true)
            {
                $setupdateStatus = array('id'=>$this->input->post('updateOrderStatusId'),'status'=>$this->input->post('updateOrderStatusDataValue'));
                $getStatus = $this->Ordermodel->updateOrderStatusData($setupdateStatus);
                if($getStatus['message'] == 'yes')
                {
                     // send notification
                     $getDeviceId = updategetUserDevice($this->input->post('updateOrderStatusId'));
                   
                     if($getDeviceId[0]->houdinv_users_device_id)
                     {
                         define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                         $to=$getDeviceId[0]->houdinv_users_device_id;
                         $message= $getMessage;
                         $msg = array(
                         'title'     => '',
                         'body'=> 'Your order status has been changed to '.$this->input->post('updateOrderStatusDataValue').'',
                         'sound' => 'mySound'/*Default sound*/
                         );
                         $fields = array
                         (
                             'to'            => $to,
                             'notification'  => $msg,
                             'priority'      =>'high',
                         );
                         $headers = array
                         (
                             'Authorization: key=' . API_ACCESS_KEY,
                             'Content-Type: application/json'
                         );
                         $ch = curl_init();
                         curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                         curl_setopt( $ch,CURLOPT_POST, true );
                         curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                         curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                         curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                         curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                         $result = curl_exec($ch );
                         curl_close( $ch );
                         shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                         $jsonDecodeData = json_decode($result,true);
                     } 
                    if($this->input->post('updateOrderStatusDataValue') == 'Delivered')
                    {
                        $getRemainingEmail = getRemainingEmail();
                        if($getRemainingEmail['remaining'] > 0)
                        {
                            $getTemaplateData = getcompleteordertemapte();
                            if(count($getTemaplateData) > 0)
                            {
                                $setSubject = $getTemaplateData[0]->houdinv_email_template_subject;
                                $getMessage = $getTemaplateData[0]->houdinv_email_template_message;
                                $getFindString = strpos($getMessage,"{user_name}");
                                if($getFindString)
                                {
                                    $fetchUserinfo = $this->Ordermodel->fetchUserInfoData($this->input->post('updateOrderStatusId'));
                                    $setMessage = str_replace('{user_name}',$fetchUserinfo[0]->houdinv_user_name,$getMessage);
                                    $getFindNextString = strpos($setMessage,"{order_id}");
                                    if($getFindNextString)
                                    {
                                        $setMessage = str_replace('{order_id}',$this->input->post('updateOrderStatusId'),$setMessage);
                                    }
                                    else
                                    {
                                        $setMessage = $setMessage;
                                    }
                                }
                                else
                                {
                                    $setMessage = $getFindString;
                                }
                                // put validation
                                $getLogo = fetchvedorsitelogo();
                                $getInfo = fetchvendorinfo();
                                $getSocial = fetchsocialurl(); 
                                if(count($getSocial) > 0)
                                {
                                    $setSocialString = "";
                                    if($getSocial[0]->facebook_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                    }
                                    if($getSocial[0]->twitter_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                    }
                                    if($getSocial[0]->youtube_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                                    }
                                }
                                // send email to admin user
                                $url = 'https://api.sendgrid.com/';
                                $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                                $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                                $json_string = array(
                                'to' => array(
                                    $fetchUserinfo[0]->houdinv_user_email
                                ),
                                'category' => 'test_category'
                                );
                                $htm = '
                                <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                                <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                                <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                                <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$setMessage.'</td></tr>
                                
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">
                                Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
                                <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                                <table width="100%" ><tr >
                                <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
                                <tr><td style="">
                               '.$setSocialString.'</td>
                                </tr></table>
                                </div>
                                </div></td></tr></table>';
                                $params = array(
                                    'api_user'  => $user,
                                    'api_key'   => $pass,
                                    'x-smtpapi' => json_encode($json_string),
                                    'to'        => $fetchUserinfo[0]->houdinv_user_email,
                                    'fromname'  => $getInfo['name'],
                                    'subject'   => $setSubject,
                                    'html'      => $htm,
                                    'from'      => $getInfo['email'],
                                );
                                $request =  $url.'api/mail.send.json';
                                $session = curl_init($request);
                                curl_setopt ($session, CURLOPT_POST, true);
                                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                                curl_setopt($session, CURLOPT_HEADER, false);
                                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                                $response = curl_exec($session);
                                curl_close($session);
                                $getResponse = json_decode($response);
                                
                                if($getResponse->message == 'success')
                                {
                                    updateSuceessEmailCount($getRemainingEmail['remaining']-1);
                                    $this->session->set_flashdata('success','Order status updated successfully1');
                                    redirect(base_url().'Order/allorders', 'refresh'); 
                                }
                                else
                                {
                                    updateErrorEmailLog();
                                    $this->session->set_flashdata('success','Order status updated successfully2');
                                    redirect(base_url().'Order/allorders', 'refresh');
                                }
                            }
                            else
                            {
                                $this->session->set_flashdata('success','Order status updated successfully');
                                redirect(base_url().'Order/allorders', 'refresh'); 
                            }
                        }
                        else
                        {
                            $this->session->set_flashdata('success','Order status updated successfully. Please upgrade your email package');
                            redirect(base_url().'Order/allorders', 'refresh');  
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Order status updated successfully');
                        redirect(base_url().'Order/allorders', 'refresh');     
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Order/allorders', 'refresh');  
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url().'Order/allorders', 'refresh');  
            }
        }
        // Delete order
        if($this->input->post('deleteOrderBtnSet'))
        {
            $this->form_validation->set_rules('deleteOrderId','text','required|integer');
            if($this->form_validation->run() == true)
            {
                $getStatus = $this->Ordermodel->deleteOrderData($this->input->post('deleteOrderId'));
                if($getStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Order deleted successfully');
                    redirect(base_url().'Order/allorders', 'refresh');  
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Order/allorders', 'refresh');  
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url().'Order/allorders', 'refresh');
            }
        }
        // tag delivery boy
        if($this->input->post('tagDeliveryBoys'))
        {
            $this->form_validation->set_rules('deliveryType','delivery type','required');
            $this->form_validation->set_rules('deliveryBoyId','delivery service','required');
            if($this->form_validation->run() == true)
            {
                $setDeliveryArray = array('type'=>$this->input->post('deliveryType'),'id'=>$this->input->post('deliveryBoyId'),'orderId'=>$this->input->post('deliveryOrderId'));
                $getStatus = $this->Ordermodel->tagDeliveryBoyData($setDeliveryArray);
                if($getStatus['message'] == 'yes')
                {
                    if($this->input->post('deliveryboyEmail'))
                    {
                        $getEmailStats = $this->Ordermodel->getEmailStatus();
                        if($getEmailStats['message'] == 'success')
                        {
                            // put validation
                            $getLogo = fetchvedorsitelogo();
                            $getInfo = fetchvendorinfo();
                            $getSocial = fetchsocialurl(); 
                            if(count($getSocial) > 0)
                            {
                                $setSocialString = "";
                                if($getSocial[0]->facebook_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                }
                                if($getSocial[0]->twitter_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                }
                                if($getSocial[0]->youtube_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                                }
                            }
                            $url = 'https://api.sendgrid.com/';
                            $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                            $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                            $json_string = array(
                            'to' => array(
                                $this->input->post('deliveryboyEmail')
                            ),
                            'category' => 'test_category'
                            );
                            
                            
                            $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
          <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
            <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
              <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                  <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                      <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                      <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                          <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Hello</strong></td></tr>
                            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                              <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">You got one delivery request (orderId: #'.$this->input->post('deliveryOrderId').')</td></tr>
                              <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">
                                Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
          <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
          <table width="100%" ><tr >
          <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
          <tr><td style="">
          '.$setSocialString.'
         </td>
          </tr></table>
          </div> 
          </div></td></tr></table>';
                            
                            
                            $params = array(
                                'api_user'  => $user,
                                'api_key'   => $pass,
                                'x-smtpapi' => json_encode($json_string),
                                'to'        => $this->input->post('deliveryboyEmail'),
                                'fromname'  => $getInfo['name'],
                                'subject'   => 'Delivery Tag Notification',
                                'html'      => $htm,
                                'from'      => $getInfo['email'],
                            );
                            $request =  $url.'api/mail.send.json';
                            $session = curl_init($request);
                            curl_setopt ($session, CURLOPT_POST, true);
                            curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                            curl_setopt($session, CURLOPT_HEADER, false);
                            curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                            $response = curl_exec($session);
                            curl_close($session);
                            $getResponse = json_decode($response);
                            // send notification
                            $getDeviceId = updategetUserDevice($this->input->post('deliveryOrderId'));
                            if($getDeviceId[0]->houdinv_users_device_id)
                            {
                                define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                                $to=$getDeviceId[0]->houdinv_users_device_id;
                                $message= $getMessage;
                                $msg = array(
                                'title'     => '',
                                'body'=> 'Delivery boy assigned to your Order',
                                'sound' => 'mySound'/*Default sound*/
                                );
                                $fields = array
                                (
                                    'to'            => $to,
                                    'notification'  => $msg,
                                    'priority'      =>'high',
                                );
                                $headers = array
                                (
                                    'Authorization: key=' . API_ACCESS_KEY,
                                    'Content-Type: application/json'
                                );
                                $ch = curl_init();
                                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                                curl_setopt( $ch,CURLOPT_POST, true );
                                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch );
                                curl_close( $ch );
                                shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                                $jsonDecodeData = json_decode($result,true);
                            }
                            if($getResponse->message == 'success')
                            {
                                $getRemainingCredit = $getEmailStats['remaining']-1;
                                $getId = $getEmailStats['id'];
                                $setArray = array('houdinv_emailsms_stats_remaining_credits'=>$getRemainingCredit);
                                $this->Ordermodel->updateCreditsData($setArray,$getId);
                                $this->session->set_flashdata('success','Delivery service assigned successfully');
                                redirect(base_url().'Order/allorders', 'refresh');
                            }
                        }
                    }
                    $this->session->set_flashdata('success','Delivery service assigned successfully');
                    redirect(base_url().'Order/allorders', 'refresh'); 
                }
                else if($getStatus['message'] == 'shipper')
                {
                    $this->session->set_flashdata('error','Shipper account not found');
                    redirect(base_url().'Order/allorders', 'refresh');
                }
                else if($getStatus['message'] == 'no')
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Order/allorders', 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error',$getStatus['message']);
                    redirect(base_url().'Order/allorders', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url().'Order/allorders', 'refresh');
            }
        }
        // Custom Pagination
        $All= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->Ordermodel->FetchOrderData($conditions);
        //pagination config
        $config['base_url']    = base_url().'Order/allorders';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset; 
        $conditions['limit'] = $this->perPage;
        $data['all'] =  $this->Ordermodel->FetchOrderData($conditions);
        $data['Count'] =  $this->Ordermodel->allOrderLanding();
        $this->load->view('allorders',$data);
    }
    public function vieworder()
    {
        $getSmsEmail = smsemailcredentials();
        // update user comment
        if($this->input->post('updateUserOrderComment'))
        {
            $setUpdateComment = array('orderId'=>$this->uri->segment('3'),'comment'=>$this->input->post('setUserComment'));
            $getUpdateStatus = $this->Ordermodel->updateUserOrderComment($setUpdateComment);
            if($getUpdateStatus['message'] == 'yes')
            {
                $this->session->set_flashdata('success','User Comment updated successfully');
                redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try agagin');
                redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
            }
        }
        // update delivery date
        if($this->input->post('updateDeliveryDate'))
        {
            $setUpdateDeliveryDate = array('orderId'=>$this->uri->segment('3'),'deliveryDate'=>$this->input->post('updateDeliveryDateData'));
            $getUpdateStatus = $this->Ordermodel->updatedDeliveryDate($setUpdateDeliveryDate);
            if($getUpdateStatus['message'] == 'yes')
            {
                // send notification
                $getDeviceId = updategetUserDevice($this->uri->segment('3'));
                if($getDeviceId[0]->houdinv_users_device_id)
                {
                    define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                    $to=$getDeviceId[0]->houdinv_users_device_id;
                    $message= $getMessage;
                    $msg = array(
                    'title'     => '',
                    'body'=> 'Order delivery date is updated to '.$this->input->post('updateDeliveryDateData').'',
                    'sound' => 'mySound'/*Default sound*/
                    );
                    $fields = array
                    (
                        'to'            => $to,
                        'notification'  => $msg,
                        'priority'      =>'high',
                    );
                    $headers = array
                    (
                        'Authorization: key=' . API_ACCESS_KEY,
                        'Content-Type: application/json'
                    );
                    $ch = curl_init();
                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt( $ch,CURLOPT_POST, true );
                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                    $result = curl_exec($ch );
                    curl_close( $ch );
                    shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                    $jsonDecodeData = json_decode($result,true);
                }
                $this->session->set_flashdata('success','Delivery date updated successfully');
                redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try agagin');
                redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
            }
        }
        // return order
        if($this->input->post('returnOrderData'))
        {
            $this->form_validation->set_rules('retrunOrderId','text','required');
            if($this->form_validation->run() == true)
            {
                $getStatus = $this->Ordermodel->updateReturnOrderStatus($this->input->post('retrunOrderId'));
                if($getStatus)
                {
                    $getEmailData = getRemainingEmail();
                    if($getEmailData['remaining'] > 0)
                    {
                        $getCustomerDetail = $this->Ordermodel->fetchCustomerDetail($this->input->post('retrunOrderId'));
                        // put validation
                        $getLogo = fetchvedorsitelogo();
                        $getInfo = fetchvendorinfo();
                        $getSocial = fetchsocialurl(); 
                        if(count($getSocial) > 0)
                        {
                            $setSocialString = "";
                            if($getSocial[0]->facebook_url)
                            {
                                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                            }
                            if($getSocial[0]->twitter_url)
                            {
                                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                            }
                            if($getSocial[0]->youtube_url)
                            {
                                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                            }
                        }
                        $url = 'https://api.sendgrid.com/';
                        $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                        $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                        $json_string = array(
                        'to' => array(
                            $getCustomerDetail[0]->houdinv_user_email
                        ),
                        'category' => 'test_category'
                        );
                        $htm = ' <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                        <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                        <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                        <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                        <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong>Hello '. $getCustomerDetail[0]->houdinv_user_name.'</strong></td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Your return request against order number '.$this->input->post('retrunOrderId').' has been suucessfully placed</td></tr>
                         
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">
                        Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
                        <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                        <table width="100%" ><tr >
                        <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">
                        &copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
                        <tr><td style="">
                        '.$setSocialString.'</td>
                        </tr></table>
                        </div>
                        </div></td></tr></table>';
                        $params = array(
                            'api_user'  => $user,
                            'api_key'   => $pass,
                            'x-smtpapi' => json_encode($json_string),
                            'to'        => $getCustomerDetail[0]->houdinv_user_email,
                            'fromname'  => $getInfo['name'],
                            'subject'   => 'Order Return',
                            'html'      => $htm,
                            'from'      => $getInfo['email'],
                        );
                        $request =  $url.'api/mail.send.json';
                        $session = curl_init($request);
                        curl_setopt ($session, CURLOPT_POST, true);
                        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                        curl_setopt($session, CURLOPT_HEADER, false);
                        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($session);
                        curl_close($session);
                        $getResponse = json_decode($response);
                        // send notification
                        $getDeviceId = updategetUserDevice($this->input->post('retrunOrderId'));
                        if($getDeviceId[0]->houdinv_users_device_id)
                        {
                            define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                            $to=$getDeviceId[0]->houdinv_users_device_id;
                            $message= $getMessage;
                            $msg = array(
                            'title'     => '',
                            'body'=> 'Your order return request is accepted',
                            'sound' => 'mySound'/*Default sound*/
                            );
                            $fields = array
                            (
                                'to'            => $to,
                                'notification'  => $msg,
                                'priority'      =>'high',
                            );
                            $headers = array
                            (
                                'Authorization: key=' . API_ACCESS_KEY,
                                'Content-Type: application/json'
                            );
                            $ch = curl_init();
                            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                            curl_setopt( $ch,CURLOPT_POST, true );
                            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            $result = curl_exec($ch );
                            curl_close( $ch );
                            shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                            $jsonDecodeData = json_decode($result,true);
                        }
                        if($getResponse->message == 'success')
                        {
                            $getSuccessdata = updateSuceessEmailCount($getEmailData['remaining']-1);
                            $this->session->set_flashdata('success','Order status updatd successfully');
                            redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                        }
                        else
                        {
                            $getFailuredata = updateErrorEmailLog();
                            $this->session->set_flashdata('success','Order status updatd successfully. updated email is not sent');
                            redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Order status updatd successfully. Please update your email package');
                        redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                    }
                    
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try agagin');
                    redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try agagin');
                redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
            }
        }
        // refund order amount
        if($this->input->post('refundOrderData'))
        {
            $this->form_validation->set_rules('refundOrderId','text','required');
            $this->form_validation->set_rules('refundOrderAmount','text','required');
            if($this->form_validation->run() == true)
            {   
                $setArrayData = array('id'=>$this->input->post('refundOrderId'),'amount'=>$this->input->post('refundOrderAmount'));
                $getRefundStatus = $this->Ordermodel->updateRefundAmountdata($setArrayData);
                if($getRefundStatus['message'] == 'yes')
                {
                    $getEmailData = getRemainingEmail();
                    if($getEmailData['remaining'] > 0)
                    {
                        $getCustomerDetail = $this->Ordermodel->fetchCustomerDetail($this->input->post('cancelOrderId'));
                        // put validation
                        $getLogo = fetchvedorsitelogo();
                        $getInfo = fetchvendorinfo();
                        $getSocial = fetchsocialurl(); 
                        if(count($getSocial) > 0)
                        {
                            $setSocialString = "";
                            if($getSocial[0]->facebook_url)
                            {
                                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                            }
                            if($getSocial[0]->twitter_url)
                            {
                                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                            }
                            if($getSocial[0]->youtube_url)
                            {
                                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                            }
                        }
                        $url = 'https://api.sendgrid.com/';
                        $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                        $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                        $json_string = array(
                        'to' => array(
                            $getCustomerDetail[0]->houdinv_user_email
                        ),
                        'category' => 'test_category'
                        );
                        $htm = ' <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                        <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                        <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                        <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                        <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong>Hello '. $getCustomerDetail[0]->houdinv_user_name.'</strong></td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Your amount '.$this->input->post('refundOrderAmount').' is refunded successfully against order id '.$this->input->post('refundOrderId').'</td></tr>
                         
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">
                        Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
                        <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                        <table width="100%" ><tr >
                        <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
                        <tr><td style="">
                       '.$setSocialString.'</td>
                        </tr></table>
                        </div>
                        </div></td></tr></table>';
                        $params = array(
                            'api_user'  => $user,
                            'api_key'   => $pass,
                            'x-smtpapi' => json_encode($json_string),
                            'to'        => $getCustomerDetail[0]->houdinv_user_email,
                            'fromname'  => $getInfo['name'],
                            'subject'   => 'Order Return',
                            'html'      => $htm,
                            'from'      => $getInfo['email'],
                        );
                        $request =  $url.'api/mail.send.json';
                        $session = curl_init($request);
                        curl_setopt ($session, CURLOPT_POST, true);
                        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                        curl_setopt($session, CURLOPT_HEADER, false);
                        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($session);
                        curl_close($session);
                        $getResponse = json_decode($response);
                        // send notification
                        $getDeviceId = updategetUserDevice($this->input->post('refundOrderId'));
                        if($getDeviceId[0]->houdinv_users_device_id)
                        {
                            define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                            $to=$getDeviceId[0]->houdinv_users_device_id;
                            $message= $getMessage;
                            $msg = array(
                            'title'     => '',
                            'body'=> 'Amount is refunded in your account against order id '.$this->input->post('refundOrderId').'',
                            'sound' => 'mySound'/*Default sound*/
                            );
                            $fields = array
                            (
                                'to'            => $to,
                                'notification'  => $msg,
                                'priority'      =>'high',
                            );
                            $headers = array
                            (
                                'Authorization: key=' . API_ACCESS_KEY,
                                'Content-Type: application/json'
                            );
                            $ch = curl_init();
                            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                            curl_setopt( $ch,CURLOPT_POST, true );
                            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            $result = curl_exec($ch );
                            curl_close( $ch );
                            shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                            $jsonDecodeData = json_decode($result,true);
                        }
                        if($getResponse->message == 'success')
                        {
                            $getSuccessdata = updateSuceessEmailCount($getEmailData['remaining']-1);
                            $this->session->set_flashdata('success','Refund status updated successfully.');
                            redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                        }
                        else
                        {
                            $getFailuredata = updateErrorEmailLog();
                            $this->session->set_flashdata('success','Refund status updated successfully.. updated email is not sent');
                            redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Refund status updated successfully.Please update your email packge');
                        redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                    }
                }
                else if($getRefundStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('error','Refund status updated successfully. Transaction is not updated.');
                    redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try agagin');
                    redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try agagin');
                redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
            }
        }
        // cancel order
        if($this->input->post('cancelOrderData'))
        {
            $this->form_validation->set_rules('cancelOrderId','text','required');
            if($this->form_validation->run() == true)
            {
                $getStatus = $this->Ordermodel->updateCancelOrderStatus($this->input->post('cancelOrderId'));
                if($getStatus)
                {
                    $getEmailData = getRemainingEmail();
                    if($getEmailData['remaining'] > 0)
                    {
                        $getCustomerDetail = $this->Ordermodel->fetchCustomerDetail($this->input->post('cancelOrderId'));
                        // put validation
                        $getLogo = fetchvedorsitelogo();
                        $getInfo = fetchvendorinfo();
                        $getSocial = fetchsocialurl(); 
                        if(count($getSocial) > 0)
                        {
                            $setSocialString = "";
                            if($getSocial[0]->facebook_url)
                            {
                                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                            }
                            if($getSocial[0]->twitter_url)
                            {
                                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                            }
                            if($getSocial[0]->youtube_url)
                            {
                                $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                            }
                        }
                        $url = 'https://api.sendgrid.com/';
                        $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                        $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                        $json_string = array(
                        'to' => array(
                            $getCustomerDetail[0]->houdinv_user_email
                        ),
                        'category' => 'test_category'
                        );
                        $htm = ' <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                        <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                        <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                        <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                        <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong>Hello '. $getCustomerDetail[0]->houdinv_user_name.'</strong></td></tr>
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">Your order number '.$this->input->post('cancelOrderId').' has been suucessfully cancelled</td></tr>
                         
                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
                        <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                        <table width="100%" ><tr >
                        <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
                        <tr><td style="">
                       '.$setSocialString.'</td>
                        </tr></table>
                        </div>
                        </div></td></tr></table>';
                        $params = array(
                            'api_user'  => $user,
                            'api_key'   => $pass,
                            'x-smtpapi' => json_encode($json_string),
                            'to'        => $getCustomerDetail[0]->houdinv_user_email,
                            'fromname'  => $getInfo['name'],
                            'subject'   => 'Order Return',
                            'html'      => $htm,
                            'from'      => $getInfo['email'],
                        );
                        $request =  $url.'api/mail.send.json';
                        $session = curl_init($request);
                        curl_setopt ($session, CURLOPT_POST, true);
                        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                        curl_setopt($session, CURLOPT_HEADER, false);
                        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($session);
                        curl_close($session);
                        $getResponse = json_decode($response);
                        // send notification
                        $getDeviceId = updategetUserDevice($this->input->post('cancelOrderId'));
                        if($getDeviceId[0]->houdinv_users_device_id)
                        {
                            define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                            $to=$getDeviceId[0]->houdinv_users_device_id;
                            $message= $getMessage;
                            $msg = array(
                            'title'     => '',
                            'body'=> 'Your order has been successfully canceled',
                            'sound' => 'mySound'/*Default sound*/
                            );
                            $fields = array
                            (
                                'to'            => $to,
                                'notification'  => $msg,
                                'priority'      =>'high',
                            );
                            $headers = array
                            (
                                'Authorization: key=' . API_ACCESS_KEY,
                                'Content-Type: application/json'
                            );
                            $ch = curl_init();
                            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                            curl_setopt( $ch,CURLOPT_POST, true );
                            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            $result = curl_exec($ch );
                            curl_close( $ch );
                            shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                            $jsonDecodeData = json_decode($result,true);
                        }
                        if($getResponse->message == 'success')
                        {
                            $getSuccessdata = updateSuceessEmailCount($getEmailData['remaining']-1);
                            $this->session->set_flashdata('success','Order status updatd successfully');
                            redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                        }
                        else
                        {
                            $getFailuredata = updateErrorEmailLog();
                            $this->session->set_flashdata('success','Order status updatd successfully. updated email is not sent');
                            redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Order status updatd successfully. Please update your email package');
                        redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                    }
                    
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try agagin');
                    redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try agagin');
                redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
            }
        }
        // send invoice on email
        if($this->input->post('sendCustomerInvoice'))
        {
            $this->form_validation->set_rules('customerEmailData','text','required');
            if($this->form_validation->run() == true)
            {
                $getEmailData = getRemainingEmail();
                if($getEmailData['remaining'] > 0)
                {
                    $getOrderDetail = $this->Ordermodel->fetchInvoiceDetailsData($this->uri->segment('3'));

                    $getMainArrayData = $getOrderDetail['maindata'][0]['main'];
                    $getMainUserData = $getOrderDetail['maindata'][0]['user'][0];
                    if($getMainUserData->houdinv_order_users_name)
                    {
                        $getUserName = $getMainUserData->houdinv_order_users_name;
                    }
                    else
                    {
                        $getUserName = $getMainUserData->houdinv_user_name;
                    }
                    if($getMainUserData->houdinv_order_users_contact)
                    {
                        $getUserPhone = $getMainUserData->houdinv_order_users_contact;
                    }
                    else
                    {
                        $getUserPhone = $getMainUserData->houdinv_user_contact;
                    }
                    if($getMainUserData->houdinv_user_email)
                    {
                        $getUserEmail = $getMainArrayData->houdinv_user_email;
                    }
                    else
                    {
                        $getUserEmail = "";
                    }
            
                    $getOrderId = $getMainArrayData->houdinv_order_id;
                    $getOrderStatus = $getMainArrayData->houdinv_order_confirmation_status;
                    $getSalesChannel = $getMainArrayData->houdinv_order_type;
                    $getOrderDate = date('d-m-Y',$getMainArrayData->houdinv_order_created_at);
                    if(count($getOrderDetail['extraAddress']) > 0)
                    {
                        $setDeliveryAddress = $getOrderDetail['extraAddress'][0]->houdinv_order_users_main_address.", ".$getOrderDetail['extraAddress'][0]->houdinv_order_users_city.", ".$getOrderDetail['extraAddress'][0]->houdinv_order_users_zip;
                        $setBillingAddress = $getOrderDetail['extraAddress'][0]->houdinv_order_users_main_address.", ".$getOrderDetail['extraAddress'][0]->houdinv_order_users_city.", ".$getOrderDetail['extraAddress'][0]->houdinv_order_users_zip;
                    }
                    else
                    {
                        $setDeliveryAddress = $getOrderDetail['deliveryAddress'][0]->houdinv_user_address_user_address.", ".$getOrderDetail['deliveryAddress'][0]->houdinv_user_address_city.", ".$getOrderDetail['deliveryAddress'][0]->houdinv_user_address_zip;
                        $setBillingAddress = $getOrderDetail['billingAddress'][0]->houdinv_user_address_user_address.", ".$getOrderDetail['billingAddress'][0]->houdinv_user_address_city.", ".$getOrderDetail['billingAddress'][0]->houdinv_user_address_zip;
                    }
                    
            
                    $getProductDetailsData = $getOrderDetail['productDetail'];
                    $setTextData = "";
                    $setSubTotal = 0;
                    for($index = 0; $index < count($getProductDetailsData); $index++)
                    {
                        $setSubTotal = $setSubTotal+$getProductDetailsData[$index]['total'];
                        if($setTextData)
                        {
                            $setTextData = $setTextData.'<tr>
                            <td style="border-bottom:1px solid #000;font-size:12px;">#'.$getOrderId.'</td>
                            <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['productName'].'</td>
                            <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['quantity'].'</td>
                            <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['actualPrice'].'</td>
                            <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['total'].'</td>
                            </tr>';
                        }
                        else
                        {
                            $setTextData = '<tr>
                            <td style="border-bottom:1px solid #000;font-size:12px;">#'.$getOrderId.'</td>
                            <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['productName'].'</td>
                            <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['quantity'].'</td>
                            <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['actualPrice'].'</td>
                            <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['total'].'</td>
                            </tr>';
                        }
                    }
                    if($getMainArrayData->houdinv_orders_deliverydate)
                    {
                        $setDeliveyDate = '<p style="font-size:12px;text-align:right;">Delivery Date : '.date('d-m-Y',strtotime($getMainArrayData->houdinv_orders_deliverydate)).'</p>';
                    }
            
                    $getDeliveryCharges = $getMainArrayData->houdinv_delivery_charge;
                    $getDiscountCharge = $getMainArrayData->houdinv_orders_discount;
                    $getNetPayable = $getMainArrayData->houdinv_orders_total_Amount;
                    $getInvoiceSetting = invoiceSetting();
                    if($getInvoiceSetting[0]->total_saving_invoice == 1)
                    {
                        $setDiscount = ' <tr>
                        <th colspan="4" style="text-align:right;">Discount</th>
                        <th>'.$getDiscountCharge.'</th>	
                    </tr>';
                    }
                    
                    if($getInvoiceSetting[0]->order_number_receipt == 1)
                    {
                        $setOrderId = '   <p>Order ID : #'.$getOrderId.'</p>';
                    }
                    // put validation
                    $getLogo = fetchvedorsitelogo();
                    $getInfo = fetchvendorinfo();
                    $getSocial = fetchsocialurl(); 
                    if(count($getSocial) > 0)
                    {
                        $setSocialString = "";
                        if($getSocial[0]->facebook_url)
                        {
                            $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                        }
                        if($getSocial[0]->twitter_url)
                        {
                            $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                        }
                        if($getSocial[0]->youtube_url)
                        {
                            $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                        }
                    }
                    $url = 'https://api.sendgrid.com/';    
                    $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                    $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                    $json_string = array(
                    'to' => array(
                        $this->input->post('customerEmailData')
                    ),
                    'category' => 'test_category'
                    );
                    $htm = '<!doctype html>
                    <html>
                    <head>
                        <title></title>
                    </head>
                    
                    <body style="margin:50px">
                        
                    
                    
                    <table width="100%">
                    <th colspan="5" bgcolor="#D9D9D9">
                        
                    <img src="'.$getLogo.'" style="text-align:center;">	
                    </th>
                    
                    
                    <tr>
                        <th colspan="3"style="text-align:left;"><h2> Customer Detail</h2></th>
                        
                        <th colspan="2" style="text-align:right;"><h2>Order Detail</h2></th>
                    </tr>
                    
                    <tr>
                        <td colspan="3" style="text-align:left; font-size:100%;">  
                    
                            <p>'.$getUserName.'</p>
                    <p>Mobile : '.$getUserPhone.'</p>
                    <p>Email : '.$getUserEmail.'</p>
                    <p>Delivery Address : '.$setDeliveryAddress.'</p>
                    <p>Biilling Address : '.$setBillingAddress.'</p>
                           
                        </td>
                    <td  colspan="2" style="text-align:right; font-size:100%;">
                            
                    '.$setOrderId.'
                    <p>Order Status : '.$getOrderStatus.'</p>
                    <p>Order Date : '.$getOrderDate.'</p>
                    '.$setDeliveyDate.'
                    
                    </td>
                    
                    </tr>
                    
                    <tr style="text-align:center;">
                        <th >Order  Id</th>
                        <th >Order Name </th>
                        <th >QTY</th>
                        <th > Price</th>
                        <th>Total price</th>
                    
                    
                    </tr>
                    
                    
                    '.$setTextData.'
                    <tr>
                        <th colspan="4"style="text-align:right;"> Delivery Charges</th>
                        <th>'.$getDeliveryCharges.'</th>	
                    </tr>
                   '.$setDiscount.'
                    <tr>
                        <th colspan="4" style="text-align:right;">Net Payable</th>
                        <th>'.$getNetPayable.'</th>	
                    </tr>
                    
                    
                    <tr>
                    <th colspan="5"  bgcolor="#33333" style="text-align:center; color:white;">
                        <p> ©'.$getInfo['name'].' '.date('Y').'--All right reserverd</p>
                      '.$setSocialString.'
                        
                        
                    </th>
                    </tr>
                    </table>
                    </body>
                    </html>';
                    $params = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'x-smtpapi' => json_encode($json_string),
                        'to'        => $this->input->post('customerEmailData'),
                        'fromname'  => $getInfo['name'],
                        'subject'   => 'Order Invoice',
                        'html'      => $htm,
                        'from'      => $getInfo['email'],
                    );
                    $request =  $url.'api/mail.send.json';
                    $session = curl_init($request);
                    curl_setopt ($session, CURLOPT_POST, true);
                    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                    curl_setopt($session, CURLOPT_HEADER, false);
                    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($session);
                    curl_close($session);
                    $getResponse = json_decode($response);
                    if($getResponse->message == 'success')
                    {
                        $getSuccessdata = updateSuceessEmailCount($getEmailData['remaining']-1);
                        $this->session->set_flashdata('success','Email sent successfully');
                        redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                    }
                    else
                    {
                        $getFailuredata = updateErrorEmailLog();
                        $this->session->set_flashdata('error','Something went wrong. Please try agagin');
                        redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','You dont have enough email in your account');
                    redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try agagin');
                redirect(base_url().'Order/vieworder/'.$this->uri->segment('3').'', 'refresh');
            }
        }
        $page = (int)$this->uri->segment(3);
        if(!$page)
        {
            redirect(base_url().'Order/allorders', 'refresh');      
        }
        $data = $this->Ordermodel->orderDetail($page);
        $this->load->view('vieworderdata',$data);
    }
    public function deliverypending()
    {
        $getSmsEmail = smsemailcredentials();
         // update order status
         if($this->input->post('updateOrderStatusBtn'))
         {
             $this->form_validation->set_rules('updateOrderStatusDataValue','status','required');
             if($this->form_validation->run() == true)
             {
                 $setupdateStatus = array('id'=>$this->input->post('updateOrderStatusId'),'status'=>$this->input->post('updateOrderStatusDataValue'));
                 $getStatus = $this->Ordermodel->updateOrderStatusData($setupdateStatus);
                 if($getStatus['message'] == 'yes')
                 {
                    if($this->input->post('updateOrderStatusDataValue') == 'Delivered')
                    {
                        $getRemainingEmail = getRemainingEmail();
                        if($getRemainingEmail['remaining'] > 0)
                        {
                            $getTemaplateData = getcompleteordertemapte();
                            if(count($getTemaplateData) > 0)
                            {
                                $setSubject = $getTemaplateData[0]->houdinv_email_template_subject;
                                $getMessage = $getTemaplateData[0]->houdinv_email_template_message;
                                $getFindString = strpos($getMessage,"{user_name}");
                                if($getFindString)
                                {
                                    $fetchUserinfo = $this->Ordermodel->fetchUserInfoData($this->input->post('updateOrderStatusId'));
                                    $setMessage = str_replace('{user_name}',$fetchUserinfo[0]->houdinv_user_name,$getMessage);
                                    $getFindNextString = strpos($setMessage,"{order_id}");
                                    if($getFindNextString)
                                    {
                                        $setMessage = str_replace('{order_id}',$this->input->post('updateOrderStatusId'),$setMessage);
                                    }
                                    else
                                    {
                                        $setMessage = $setMessage;
                                    }
                                }
                                else
                                {
                                    $setMessage = $getFindString;
                                }
                                // put validation
                                $getLogo = fetchvedorsitelogo();
                                $getInfo = fetchvendorinfo();
                                $getSocial = fetchsocialurl(); 
                                if(count($getSocial) > 0) 
                                {
                                    $setSocialString = "";
                                    if($getSocial[0]->facebook_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                    }
                                    if($getSocial[0]->twitter_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                    }
                                    if($getSocial[0]->youtube_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                                    }
                                }
                                // send email to admin user
                                $url = 'https://api.sendgrid.com/';
                                $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                                $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                                $json_string = array(
                                'to' => array(
                                    $fetchUserinfo[0]->houdinv_user_email
                                ),
                                'category' => 'test_category'
                                );
                                $htm = '
                                <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                                <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                                <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                                <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$setMessage.'</td></tr>
                                
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
                                <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                                <table width="100%" ><tr >
                                <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">
                                &copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
                                <tr><td style="">
                                '.$setSocialString.'</td>
                                </tr></table>
                                </div>
                                </div></td></tr></table>';
                                $params = array(
                                    'api_user'  => $user,
                                    'api_key'   => $pass,
                                    'x-smtpapi' => json_encode($json_string),
                                    'to'        => $fetchUserinfo[0]->houdinv_user_email,
                                    'fromname'  => $getInfo['name'],
                                    'subject'   => $setSubject,
                                    'html'      => $htm,
                                    'from'      => $getInfo['email'],
                                );
                                $request =  $url.'api/mail.send.json';
                                $session = curl_init($request);
                                curl_setopt ($session, CURLOPT_POST, true);
                                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                                curl_setopt($session, CURLOPT_HEADER, false);
                                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                                $response = curl_exec($session);
                                curl_close($session);
                                $getResponse = json_decode($response);
                                 // send notification
                                $getDeviceId = updategetUserDevice($this->input->post('updateOrderStatusId'));
                                if($getDeviceId[0]->houdinv_users_device_id)
                                {
                                    define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                                    $to=$getDeviceId[0]->houdinv_users_device_id;
                                    $message= $getMessage;
                                    $msg = array(
                                    'title'     => '',
                                    'body'=> 'Your order has been updated to '.$this->input->post('updateOrderStatusDataValue').'',
                                    'sound' => 'mySound'/*Default sound*/
                                    );
                                    $fields = array
                                    (
                                        'to'            => $to,
                                        'notification'  => $msg,
                                        'priority'      =>'high',
                                    );
                                    $headers = array
                                    (
                                        'Authorization: key=' . API_ACCESS_KEY,
                                        'Content-Type: application/json'
                                    );
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    curl_close( $ch );
                                    shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                                    $jsonDecodeData = json_decode($result,true);
                                }
                                if($getResponse->message == 'success')
                                {
                                    updateSuceessEmailCount($getRemainingEmail['remaining']-1);
                                    $this->session->set_flashdata('success','Order status updated successfully');
                                    redirect(base_url().'Order/deliverypending', 'refresh');  
                                }
                                else
                                {
                                    updateErrorEmailLog();
                                    $this->session->set_flashdata('success','Order status updated successfully');
                                    redirect(base_url().'Order/deliverypending', 'refresh');  
                                }
                            }
                            else
                            {
                                $this->session->set_flashdata('success','Order status updated successfully');
                                redirect(base_url().'Order/deliverypending', 'refresh');  
                            }
                        }
                        else
                        {
                            $this->session->set_flashdata('success','Order status updated successfully');
                            redirect(base_url().'Order/deliverypending', 'refresh');  
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Order status updated successfully');
                        redirect(base_url().'Order/deliverypending', 'refresh');    
                    } 
                 }
                 else
                 {
                     $this->session->set_flashdata('error','Something went wrong. Please try again');
                     redirect(base_url().'Order/deliverypending', 'refresh');  
                 }
             }
             else
             {
                 $this->session->set_flashdata('error',validation_errors());
                 redirect(base_url().'Order/deliverypending', 'refresh');  
             }
         }
         // Delete order
         if($this->input->post('deleteOrderBtnSet'))
         {
             $this->form_validation->set_rules('deleteOrderId','text','required|integer');
             if($this->form_validation->run() == true)
             {
                 $getStatus = $this->Ordermodel->deleteOrderData($this->input->post('deleteOrderId'));
                 if($getStatus['message'] == 'yes')
                 {
                     $this->session->set_flashdata('success','Order deleted successfully');
                     redirect(base_url().'Order/deliverypending', 'refresh');  
                 }
                 else
                 {
                     $this->session->set_flashdata('error','Something went wrong. Please try again');
                     redirect(base_url().'Order/deliverypending', 'refresh');  
                 }
             }
             else
             {
                 $this->session->set_flashdata('error','Something went wrong. Please try again');
                 redirect(base_url().'Order/deliverypending', 'refresh');
             }
         }
        $All= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->Ordermodel->FetchOrderDeliverPendingData($conditions);
        //pagination config
        $config['base_url']    = base_url().'Order/deliverypending';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset; 
        $conditions['limit'] = $this->perPage;
        $data['all'] =  $this->Ordermodel->FetchOrderDeliverPendingData($conditions);
        $this->load->view('deliverypendingorder',$data);
    }
    public function paymentpending()
    {
        $getSmsEmail = smsemailcredentials();
        // update order status
        if($this->input->post('updateOrderStatusBtn'))
        {
            $this->form_validation->set_rules('updateOrderStatusDataValue','status','required');
            if($this->form_validation->run() == true)
            {
                $setupdateStatus = array('id'=>$this->input->post('updateOrderStatusId'),'status'=>$this->input->post('updateOrderStatusDataValue'));
                $getStatus = $this->Ordermodel->updateOrderStatusData($setupdateStatus);
                if($getStatus['message'] == 'yes')
                {
                    if($this->input->post('updateOrderStatusDataValue') == 'Delivered')
                    {
                        $getRemainingEmail = getRemainingEmail();
                        if($getRemainingEmail['remaining'] > 0)
                        {
                            $getTemaplateData = getcompleteordertemapte();
                            if(count($getTemaplateData) > 0)
                            {
                                $setSubject = $getTemaplateData[0]->houdinv_email_template_subject;
                                $getMessage = $getTemaplateData[0]->houdinv_email_template_message;
                                $getFindString = strpos($getMessage,"{user_name}");
                                if($getFindString)
                                {
                                    $fetchUserinfo = $this->Ordermodel->fetchUserInfoData($this->input->post('updateOrderStatusId'));
                                    $setMessage = str_replace('{user_name}',$fetchUserinfo[0]->houdinv_user_name,$getMessage);
                                    $getFindNextString = strpos($setMessage,"{order_id}");
                                    if($getFindNextString)
                                    {
                                        $setMessage = str_replace('{order_id}',$this->input->post('updateOrderStatusId'),$setMessage);
                                    }
                                    else
                                    {
                                        $setMessage = $setMessage;
                                    }
                                }
                                else
                                {
                                    $setMessage = $getFindString;
                                }
                                // send email to admin user
                                // put validation
                                $getLogo = fetchvedorsitelogo();
                                $getInfo = fetchvendorinfo();
                                $getSocial = fetchsocialurl(); 
                                if(count($getSocial) > 0) 
                                {
                                    $setSocialString = "";
                                    if($getSocial[0]->facebook_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                    }
                                    if($getSocial[0]->twitter_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                    }
                                    if($getSocial[0]->youtube_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                                    }
                                }
                                $url = 'https://api.sendgrid.com/';
                                $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                                $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                                $json_string = array(
                                'to' => array(
                                    $fetchUserinfo[0]->houdinv_user_email
                                ),
                                'category' => 'test_category'
                                );
                                $htm = '
                                <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                                <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                                <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                                <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$setMessage.'</td></tr>
                                
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
                                <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                                <table width="100%" ><tr >
                                <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
                                <tr><td style="">
                                '.$setSocialString.'</td>
                                </tr></table>
                                </div>
                                </div></td></tr></table>';
                                $params = array(
                                    'api_user'  => $user,
                                    'api_key'   => $pass,
                                    'x-smtpapi' => json_encode($json_string),
                                    'to'        => $fetchUserinfo[0]->houdinv_user_email,
                                    'fromname'  => $getInfo['name'],
                                    'subject'   => $setSubject,
                                    'html'      => $htm,
                                    'from'      => $getInfo['email'],
                                );
                                $request =  $url.'api/mail.send.json';
                                $session = curl_init($request);
                                curl_setopt ($session, CURLOPT_POST, true);
                                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                                curl_setopt($session, CURLOPT_HEADER, false);
                                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                                $response = curl_exec($session);
                                curl_close($session);
                                $getResponse = json_decode($response);
                                // send notification
                                $getDeviceId = updategetUserDevice($this->input->post('updateOrderStatusId'));
                                if($getDeviceId[0]->houdinv_users_device_id)
                                {
                                    define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                                    $to=$getDeviceId[0]->houdinv_users_device_id;
                                    $message= $getMessage;
                                    $msg = array(
                                    'title'     => '',
                                    'body'=> 'Your order has been updated to '.$this->input->post('updateOrderStatusDataValue').'',
                                    'sound' => 'mySound'/*Default sound*/
                                    );
                                    $fields = array
                                    (
                                        'to'            => $to,
                                        'notification'  => $msg,
                                        'priority'      =>'high',
                                    );
                                    $headers = array
                                    (
                                        'Authorization: key=' . API_ACCESS_KEY,
                                        'Content-Type: application/json'
                                    );
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    curl_close( $ch );
                                    shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                                    $jsonDecodeData = json_decode($result,true);
                                }
                                if($getResponse->message == 'success')
                                {
                                    updateSuceessEmailCount($getRemainingEmail['remaining']-1);
                                    $this->session->set_flashdata('success','Order status updated successfully');
                                    redirect(base_url().'Order/paymentpending', 'refresh');   
                                }
                                else
                                {
                                    updateErrorEmailLog();
                                    $this->session->set_flashdata('success','Order status updated successfully');
                                    redirect(base_url().'Order/paymentpending', 'refresh');  
                                }
                            }
                            else
                            {
                                $this->session->set_flashdata('success','Order status updated successfully');
                                redirect(base_url().'Order/paymentpending', 'refresh');  
                            }
                        }
                        else
                        {
                            $this->session->set_flashdata('success','Order status updated successfully');
                            redirect(base_url().'Order/paymentpending', 'refresh');  
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Order status updated successfully');
                        redirect(base_url().'Order/paymentpending', 'refresh');  
                    } 
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Order/paymentpending', 'refresh');  
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url().'Order/paymentpending', 'refresh');  
            }
        }
        // Delete order
        if($this->input->post('deleteOrderBtnSet'))
        {
            $this->form_validation->set_rules('deleteOrderId','text','required|integer');
            if($this->form_validation->run() == true)
            {
                $getStatus = $this->Ordermodel->deleteOrderData($this->input->post('deleteOrderId'));
                if($getStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Order deleted successfully');
                    redirect(base_url().'Order/paymentpending', 'refresh');  
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Order/paymentpending', 'refresh');  
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url().'Order/paymentpending', 'refresh');
            }
        }
        // tag delivery boy
        if($this->input->post('tagDeliveryBoys'))
        {
            $this->form_validation->set_rules('deliveryType','delivery type','required');
            $this->form_validation->set_rules('deliveryBoyId','delivery service','required');
            if($this->form_validation->run() == true)
            {
                $setDeliveryArray = array('type'=>$this->input->post('deliveryType'),'id'=>$this->input->post('deliveryBoyId'),'orderId'=>$this->input->post('deliveryOrderId'));
                $getStatus = $this->Ordermodel->tagDeliveryBoyData($setDeliveryArray);
                if($getStatus['message'] == 'yes')
                {
                    if($this->input->post('deliveryboyEmail'))
                    {
                        $getEmailStats = $this->Ordermodel->getEmailStatus();
                        if($getEmailStats['message'] == 'success')
                        {
                            // put validation
                            $getLogo = fetchvedorsitelogo();
                            $getInfo = fetchvendorinfo();
                            $getSocial = fetchsocialurl(); 
                            if(count($getSocial) > 0)
                            {
                                $setSocialString = "";
                                if($getSocial[0]->facebook_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                }
                                if($getSocial[0]->twitter_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                }
                                if($getSocial[0]->youtube_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                                }
                            }
                            $url = 'https://api.sendgrid.com/';
                            $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                            $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                            $json_string = array(
                            'to' => array(
                                $this->input->post('deliveryboyEmail')
                            ),
                            'category' => 'test_category'
                            );
                            
                            
                            $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
          <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
            <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
              <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                  <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                      <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                      <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                          <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Hello</strong></td></tr>
                            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                              <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">You got one delivery request (orderId: #'.$this->input->post('deliveryOrderId').')</td></tr>
                              <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">
                                Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
          <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
          <table width="100%" ><tr >
          <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
          <tr><td style="">
          '.$setSocialString.'
         </td>
          </tr></table>
          </div> 
          </div></td></tr></table>';
                            
                            
                            $params = array(
                                'api_user'  => $user,
                                'api_key'   => $pass,
                                'x-smtpapi' => json_encode($json_string),
                                'to'        => $this->input->post('deliveryboyEmail'),
                                'fromname'  => $getInfo['name'],
                                'subject'   => 'Delivery Tag Notification',
                                'html'      => $htm,
                                'from'      => $getInfo['email'],
                            );
                            $request =  $url.'api/mail.send.json';
                            $session = curl_init($request);
                            curl_setopt ($session, CURLOPT_POST, true);
                            curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                            curl_setopt($session, CURLOPT_HEADER, false);
                            curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                            $response = curl_exec($session);
                            curl_close($session);
                            $getResponse = json_decode($response);
                            // send notification
                            $getDeviceId = updategetUserDevice($this->input->post('deliveryOrderId'));
                            if($getDeviceId[0]->houdinv_users_device_id)
                            {
                                define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                                $to=$getDeviceId[0]->houdinv_users_device_id;
                                $message= $getMessage;
                                $msg = array(
                                'title'     => '',
                                'body'=> 'Delivery service is assigned to your order '.$this->input->post('deliveryOrderId').'',
                                'sound' => 'mySound'/*Default sound*/
                                );
                                $fields = array
                                (
                                    'to'            => $to,
                                    'notification'  => $msg,
                                    'priority'      =>'high',
                                );
                                $headers = array
                                (
                                    'Authorization: key=' . API_ACCESS_KEY,
                                    'Content-Type: application/json'
                                );
                                $ch = curl_init();
                                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                                curl_setopt( $ch,CURLOPT_POST, true );
                                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch );
                                curl_close( $ch );
                                shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                                $jsonDecodeData = json_decode($result,true);
                            }
                            if($getResponse->message == 'success')
                            {
                                $getRemainingCredit = $getEmailStats['remaining']-1;
                                $getId = $getEmailStats['id'];
                                $setArray = array('houdinv_emailsms_stats_remaining_credits'=>$getRemainingCredit);
                                $this->Ordermodel->updateCreditsData($setArray,$getId);
                                $this->session->set_flashdata('success','Delivery service assigned successfully');
                                redirect(base_url().'Order/paymentpending', 'refresh');
                            }
                        }
                    }
                    $this->session->set_flashdata('success','Delivery service assigned successfully');
                    redirect(base_url().'Order/paymentpending', 'refresh'); 
                }
                else if($getStatus['message'] == 'shipper')
                {
                    $this->session->set_flashdata('error','Shipper account not found');
                    redirect(base_url().'Order/paymentpending', 'refresh');
                }
                else if($getStatus['message'] == 'no')
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Order/paymentpending', 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error',$getStatus['message']);
                    redirect(base_url().'Order/paymentpending', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url().'Order/paymentpending', 'refresh');
            }
        }
        // // Custom Pagination
        $getTotalCount = $this->Ordermodel->getPaymentPendingOrderCount();
        $config['base_url']    = base_url().'order/paymentpending/';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $getTotalCount['totalRows'];
        $config['per_page']    = $this->perPage;
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        // End Here
        $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $data['all'] = $this->Ordermodel->getPaymentPendingOrderDetails($setArray);
        $this->load->view('paymentpending',$data);
    }
    public function deliverycompleted()
    {
        // Delete order
        if($this->input->post('deleteOrderBtnSet'))
        {
            $this->form_validation->set_rules('deleteOrderId','text','required|integer');
            if($this->form_validation->run() == true)
            {
                $getStatus = $this->Ordermodel->deleteOrderData($this->input->post('deleteOrderId'));
                if($getStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Order deleted successfully');
                    redirect(base_url().'Order/deliverycompleted', 'refresh');  
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Order/deliverycompleted', 'refresh');  
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url().'Order/deliverycompleted', 'refresh');
            }
        }
        $All= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->Ordermodel->FetchOrderCompleteData($conditions);
        //pagination config
        $config['base_url']    = base_url().'Order/deliverycompleted';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset; 
        $conditions['limit'] = $this->perPage;
        $data['all'] =  $this->Ordermodel->FetchOrderCompleteData($conditions);
        $this->load->view('deliverycompletedorder',$data);
    }
    public function deliverypickup()
    {
        $getSmsEmail = smsemailcredentials();
        // update order status
        if($this->input->post('updateOrderStatusBtn'))
        {
            $this->form_validation->set_rules('updateOrderStatusDataValue','status','required');
            if($this->form_validation->run() == true)
            {
                $setupdateStatus = array('id'=>$this->input->post('updateOrderStatusId'),'status'=>$this->input->post('updateOrderStatusDataValue'));
                $getStatus = $this->Ordermodel->updateOrderStatusData($setupdateStatus);
                if($getStatus['message'] == 'yes')
                {
                    if($this->input->post('updateOrderStatusDataValue') == 'Delivered')
                    {
                        $getRemainingEmail = getRemainingEmail();
                        if($getRemainingEmail['remaining'] > 0)
                        {
                            $getTemaplateData = getcompleteordertemapte();
                            if(count($getTemaplateData) > 0)
                            {
                                $setSubject = $getTemaplateData[0]->houdinv_email_template_subject;
                                $getMessage = $getTemaplateData[0]->houdinv_email_template_message;
                                $getFindString = strpos($getMessage,"{user_name}");
                                if($getFindString)
                                {
                                    $fetchUserinfo = $this->Ordermodel->fetchUserInfoData($this->input->post('updateOrderStatusId'));
                                    $setMessage = str_replace('{user_name}',$fetchUserinfo[0]->houdinv_user_name,$getMessage);
                                    $getFindNextString = strpos($setMessage,"{order_id}");
                                    if($getFindNextString)
                                    {
                                        $setMessage = str_replace('{order_id}',$this->input->post('updateOrderStatusId'),$setMessage);
                                    }
                                    else
                                    {
                                        $setMessage = $setMessage;
                                    }
                                }
                                else
                                {
                                    $setMessage = $getFindString;
                                }
                                // put validation
                                $getLogo = fetchvedorsitelogo();
                                $getInfo = fetchvendorinfo();
                                $getSocial = fetchsocialurl(); 
                                if(count($getSocial) > 0) 
                                {
                                    $setSocialString = "";
                                    if($getSocial[0]->facebook_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                    }
                                    if($getSocial[0]->twitter_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                    }
                                    if($getSocial[0]->youtube_url)
                                    {
                                        $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                                    }
                                }
                                // send email to admin user
                                $url = 'https://api.sendgrid.com/';
                                $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                                $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                                $json_string = array(
                                'to' => array(
                                    $fetchUserinfo[0]->houdinv_user_email
                                ),
                                'category' => 'test_category'
                                );
                                $htm = '
                                <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                                <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                                <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                                <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">'.$setMessage.'</td></tr>
                                
                                <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
                                <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
                                <table width="100%" ><tr >
                                <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
                                <tr><td style="">
                                '.$setSocialString.'</td>
                                </tr></table>
                                </div>
                                </div></td></tr></table>';
                                $params = array(
                                    'api_user'  => $user,
                                    'api_key'   => $pass,
                                    'x-smtpapi' => json_encode($json_string),
                                    'to'        => $fetchUserinfo[0]->houdinv_user_email,
                                    'fromname'  => $getInfo['name'],
                                    'subject'   => $setSubject,
                                    'html'      => $htm,
                                    'from'      => $getInfo['email'],
                                );
                                $request =  $url.'api/mail.send.json';
                                $session = curl_init($request);
                                curl_setopt ($session, CURLOPT_POST, true);
                                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                                curl_setopt($session, CURLOPT_HEADER, false);
                                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                                $response = curl_exec($session);
                                curl_close($session);
                                $getResponse = json_decode($response);
                                // send notification
                                $getDeviceId = updategetUserDevice($this->input->post('updateOrderStatusId'));
                                if($getDeviceId[0]->houdinv_users_device_id)
                                {
                                    define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                                    $to=$getDeviceId[0]->houdinv_users_device_id;
                                    $message= $getMessage;
                                    $msg = array(
                                    'title'     => '',
                                    'body'=> 'Your order status is updated',
                                    'sound' => 'mySound'/*Default sound*/
                                    );
                                    $fields = array
                                    (
                                        'to'            => $to,
                                        'notification'  => $msg,
                                        'priority'      =>'high',
                                    );
                                    $headers = array
                                    (
                                        'Authorization: key=' . API_ACCESS_KEY,
                                        'Content-Type: application/json'
                                    );
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    curl_close( $ch );
                                    shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                                    $jsonDecodeData = json_decode($result,true);
                                }
                                if($getResponse->message == 'success')
                                {
                                    updateSuceessEmailCount($getRemainingEmail['remaining']-1);
                                    $this->session->set_flashdata('success','Order status updated successfully');
                                    redirect(base_url().'Order/deliverypickup', 'refresh');   
                                }
                                else
                                {
                                    updateErrorEmailLog();
                                    $this->session->set_flashdata('success','Order status updated successfully');
                                    redirect(base_url().'Order/deliverypickup', 'refresh');  
                                }
                            }
                            else
                            {
                                $this->session->set_flashdata('success','Order status updated successfully');
                                redirect(base_url().'Order/deliverypickup', 'refresh');   
                            }
                        }
                        else
                        {
                            $this->session->set_flashdata('success','Order status updated successfully');
                            redirect(base_url().'Order/deliverypickup', 'refresh');  
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Order status updated successfully');
                        redirect(base_url().'Order/deliverypickup', 'refresh');  
                    } 
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Order/deliverypickup', 'refresh');  
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url().'Order/deliverypickup', 'refresh');  
            }
        }
        // Delete order
        if($this->input->post('deleteOrderBtnSet'))
        {
            $this->form_validation->set_rules('deleteOrderId','text','required|integer');
            if($this->form_validation->run() == true)
            {
                $getStatus = $this->Ordermodel->deleteOrderData($this->input->post('deleteOrderId'));
                if($getStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Order deleted successfully');
                    redirect(base_url().'Order/deliverypickup', 'refresh');  
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Order/deliverypickup', 'refresh');  
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url().'Order/deliverypickup', 'refresh');
            }
        }
        // tag delivery boy
        if($this->input->post('tagDeliveryBoys'))
        {
            $this->form_validation->set_rules('deliveryType','delivery type','required');
            $this->form_validation->set_rules('deliveryBoyId','delivery service','required');
            if($this->form_validation->run() == true)
            {
                $setDeliveryArray = array('type'=>$this->input->post('deliveryType'),'id'=>$this->input->post('deliveryBoyId'),'orderId'=>$this->input->post('deliveryOrderId'));
                $getStatus = $this->Ordermodel->tagDeliveryBoyData($setDeliveryArray);
                if($getStatus['message'] == 'yes')
                {
                    if($this->input->post('deliveryboyEmail'))
                    {
                        $getEmailStats = $this->Ordermodel->getEmailStatus();
                        if($getEmailStats['message'] == 'success')
                        {
                            // put validation
                            $getLogo = fetchvedorsitelogo();
                            $getInfo = fetchvendorinfo();
                            $getSocial = fetchsocialurl(); 
                            if(count($getSocial) > 0)
                            {
                                $setSocialString = "";
                                if($getSocial[0]->facebook_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                }
                                if($getSocial[0]->twitter_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                }
                                if($getSocial[0]->youtube_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                                }
                            }
                            $url = 'https://api.sendgrid.com/';
                            $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                            $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                            $json_string = array(
                            'to' => array(
                                $this->input->post('deliveryboyEmail')
                            ),
                            'category' => 'test_category'
                            );
                            
                            
                            $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
          <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
            <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
              <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                  <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                      <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                      <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                          <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Hello</strong></td></tr>
                            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                              <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">You got one delivery request (orderId: #'.$this->input->post('deliveryOrderId').')</td></tr>
                              <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">
                                Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
          <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
          <table width="100%" ><tr >
          <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
          <tr><td style="">
          '.$setSocialString.'
         </td>
          </tr></table>
          </div> 
          </div></td></tr></table>';
                            
                            
                            $params = array(
                                'api_user'  => $user,
                                'api_key'   => $pass,
                                'x-smtpapi' => json_encode($json_string),
                                'to'        => $this->input->post('deliveryboyEmail'),
                                'fromname'  => $getInfo['name'],
                                'subject'   => 'Delivery Tag Notification',
                                'html'      => $htm,
                                'from'      => $getInfo['email'],
                            );
                            $request =  $url.'api/mail.send.json';
                            $session = curl_init($request);
                            curl_setopt ($session, CURLOPT_POST, true);
                            curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                            curl_setopt($session, CURLOPT_HEADER, false);
                            curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                            $response = curl_exec($session);
                            curl_close($session);
                            $getResponse = json_decode($response);
                            // send notification
                            $getDeviceId = updategetUserDevice($this->input->post('deliveryOrderId'));
                            if($getDeviceId[0]->houdinv_users_device_id)
                            {
                                define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                                $to=$getDeviceId[0]->houdinv_users_device_id;
                                $message= $getMessage;
                                $msg = array(
                                'title'     => '',
                                'body'=> 'Delivery service is assigned to your order '.$this->input->post('deliveryOrderId').'',
                                'sound' => 'mySound'/*Default sound*/
                                );
                                $fields = array
                                (
                                    'to'            => $to,
                                    'notification'  => $msg,
                                    'priority'      =>'high',
                                );
                                $headers = array
                                (
                                    'Authorization: key=' . API_ACCESS_KEY,
                                    'Content-Type: application/json'
                                );
                                $ch = curl_init();
                                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                                curl_setopt( $ch,CURLOPT_POST, true );
                                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch );
                                curl_close( $ch );
                                shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                                $jsonDecodeData = json_decode($result,true);
                            }
                            if($getResponse->message == 'success')
                            {
                                $getRemainingCredit = $getEmailStats['remaining']-1;
                                $getId = $getEmailStats['id'];
                                $setArray = array('houdinv_emailsms_stats_remaining_credits'=>$getRemainingCredit);
                                $this->Ordermodel->updateCreditsData($setArray,$getId);
                                $this->session->set_flashdata('success','Delivery service assigned successfully');
                                redirect(base_url().'Order/deliverypickup', 'refresh');
                            }
                        }
                    }
                    $this->session->set_flashdata('success','Delivery service assigned successfully');
                    redirect(base_url().'Order/deliverypickup', 'refresh'); 
                }
                else if($getStatus['message'] == 'shipper')
                {
                    $this->session->set_flashdata('error','Shipper account not found');
                    redirect(base_url().'Order/deliverypickup', 'refresh');
                }
                else if($getStatus['message'] == 'no')
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Order/deliverypickup', 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error',$getStatus['message']);
                    redirect(base_url().'Order/deliverypickup', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url().'Order/deliverypickup', 'refresh');
            }
        }
        $All= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->Ordermodel->FetchOrderPickupData($conditions);
        //pagination config
        $config['base_url']    = base_url().'Order/allorders';
        $config['uri_segment'] = 3;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        $page = $this->uri->segment(3);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset; 
        $conditions['limit'] = $this->perPage;
        $data['all'] =  $this->Ordermodel->FetchOrderPickupData($conditions);
        $this->load->view('deliverypickuporder',$data);
    }
    public function getPaymentStatus()
    {
        $this->form_validation->set_rules('getOrderId','text','required');
        if($this->form_validation->run() == true)
        {
            $getOrder = $this->Ordermodel->fetchRemainingPayment($this->input->post('getOrderId'));
            print_r(json_encode($getOrder));
        }
        else
        {
            $resultArray = array('message'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function updatePaymentStatus()
    {
        $getPageName = $this->input->post('paymentOrderPage');
        $this->form_validation->set_rules('AmountPaid','text','required');
        if($this->form_validation->run() == true)
        {
            if($this->input->post('paymentMode') == 'cash')
            {
                $setPaymentMode = 'cash';
            }
            else if($this->input->post('paymentMode') == 'card')
            {
                $setPaymentMode = 'card';
            }
            else if($this->input->post('paymentMode') == 'cheque')
            {
                $setPaymentMode = 'checque';
            }
            else
            {
                $setPaymentMode = 'cash';
            }
            // set order update page
            $setData = strtotime(date('Y-m-d'));
            $setOrderUpdatedArray = array('houdinv_orders_total_paid'=>$this->input->post('AmountPaid'),'houdinv_orders_total_remaining'=>$this->input->post('newRemainingAmount'),'houdinv_payment_status'=>$this->input->post('paymentStatus'),'houdinv_order_updated_at'=>$setData);
            // setTransaction
            $setTransaction = "TXN".rand(99999999,10000000);
            $setTransactionArray = array('houdinv_transaction_transaction_id'=>$setTransaction,'houdinv_transaction_type'=>'credit','houdinv_transaction_method'=>$setPaymentMode,'houdinv_transaction_from'=>$setPaymentMode,'houdinv_transaction_for'=>'order','houdinv_transaction_for_id'=>$this->input->post('paymentOrderId'),'houdinv_transaction_amount'=>$this->input->post('AmountPaid'),'houdinv_transaction_date'=>date('Y-m-d'),'houdinv_transaction_status'=>'success');
            // set customer array
            if($this->input->post('newRemainingAmount') > 0)
            {
                $getFinalPendingAmount = $this->input->post('newRemainingAmount')+$this->input->post('totalPendingAmount');
                $setCustomerArray = array('houdinv_users_pending_amount'=>$getFinalPendingAmount);
            }
            else
            {
                $setCustomerArray = array();
            }
            $getPaymentStatus = $this->Ordermodel->updatePaymentStatus($setOrderUpdatedArray,$setTransactionArray,$setCustomerArray,$this->input->post('paymentOrderId'),$this->input->post('customerId'),$this->input->post('paymentMode'));
            if($getPaymentStatus['message'] == 'yes')
            {
                $this->session->set_flashdata('success','Payment updated successfully');
                $this->load->helper('url');
                if($getPageName == 'allorder'){ redirect(base_url()."order/allorders", 'refresh'); }
                else if($getPageName == 'pending') { redirect(base_url()."order/deliverypending", 'refresh'); }
                else if($getPageName == 'completed') { redirect(base_url()."order/deliverycompleted", 'refresh'); }
                else if($getPageName == 'pickup') { redirect(base_url()."order/deliverypickup", 'refresh'); }
                else if($getPageName == 'pending') { redirect(base_url()."order/paymentpending", 'refresh'); }
             }
            else if($getPaymentStatus['message'] == 'cus')
            {
                $this->session->set_flashdata('error','Payment updated successfully, Something went wrong during customer updation');
                $this->load->helper('url');
                if($getPageName == 'allorder'){ redirect(base_url()."order/allorders", 'refresh'); }
                else if($getPageName == 'pending') { redirect(base_url()."order/deliverypending", 'refresh'); }
                else if($getPageName == 'completed') { redirect(base_url()."order/deliverycompleted", 'refresh'); }
                else if($getPageName == 'pickup') { redirect(base_url()."order/deliverypickup", 'refresh'); }
                else if($getPageName == 'pending') { redirect(base_url()."order/paymentpending", 'refresh'); }
            }
            else if($getPaymentStatus['message'] == 'transaction')
            {
                $this->session->set_flashdata('error','Payment updated successfully, Something went wrong during transaction updation');
                $this->load->helper('url');
                if($getPageName == 'allorder'){ redirect(base_url()."order/allorders", 'refresh'); }
                else if($getPageName == 'pending') { redirect(base_url()."order/deliverypending", 'refresh'); }
                else if($getPageName == 'completed') { redirect(base_url()."order/deliverycompleted", 'refresh'); }
                else if($getPageName == 'pickup') { redirect(base_url()."order/deliverypickup", 'refresh'); }
                else if($getPageName == 'pending') { redirect(base_url()."order/paymentpending", 'refresh'); }
            }
            else if($getPaymentStatus['message'] == 'custransaction')
            {
                $this->session->set_flashdata('error','Payment updated successfully, Something went wrong during transaction and customer updation');
                $this->load->helper('url');
                if($getPageName == 'allorder'){ redirect(base_url()."order/allorders", 'refresh'); }
                else if($getPageName == 'pending') { redirect(base_url()."order/deliverypending", 'refresh'); }
                else if($getPageName == 'completed') { redirect(base_url()."order/deliverycompleted", 'refresh'); }
                else if($getPageName == 'pickup') { redirect(base_url()."order/deliverypickup", 'refresh'); }
                else if($getPageName == 'pending') { redirect(base_url()."order/paymentpending", 'refresh'); }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                if($getPageName == 'allorder'){ redirect(base_url()."order/allorders", 'refresh'); }
                else if($getPageName == 'pending') { redirect(base_url()."order/deliverypending", 'refresh'); }
                else if($getPageName == 'completed') { redirect(base_url()."order/deliverycompleted", 'refresh'); }
                else if($getPageName == 'pickup') { redirect(base_url()."order/deliverypickup", 'refresh'); }
                else if($getPageName == 'pending') { redirect(base_url()."order/paymentpending", 'refresh'); }
            }
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            $this->load->helper('url');
            if($getPageName == 'allorder'){ redirect(base_url()."order/allorders", 'refresh'); }
            else if($getPageName == 'pending') { redirect(base_url()."order/deliverypending", 'refresh'); }
            else if($getPageName == 'completed') { redirect(base_url()."order/deliverycompleted", 'refresh'); }
            else if($getPageName == 'pickup') { redirect(base_url()."order/deliverypickup", 'refresh'); }
            else if($getPageName == 'pending') { redirect(base_url()."order/paymentpending", 'refresh'); }
        }
    }
    public function deliveryquotation()
    {
        $getSmsEmail = smsemailcredentials();
        // delete quotation
        if($this->input->post('deleteQuotation'))
        {
            $this->form_validation->set_rules('deleteQuotationId','text','required|integer');
            if($this->form_validation->run() == true)
            {
                $getStatus = $this->Ordermodel->deleteOrderQuotation($this->input->post('deleteQuotationId'));
                if($getStatus['message'] == 'yes')
                { 
                    $this->session->set_flashdata('success','Quotation deleted successfully');
                    $this->load->helper('url');
                    redirect(base_url()."order/deliveryquotation", 'refresh'); 
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."order/deliveryquotation", 'refresh'); 
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."order/deliveryquotation", 'refresh'); 
            }
        }
        // send email to user
        if($this->input->post('sendEmailtoUser'))
        {
            $this->form_validation->set_rules('emailQuotationData','text','required|valid_email');
            $this->form_validation->set_rules('emailSubject','text','required');
            $this->form_validation->set_rules('emailBodyData','text','required');
            if($this->form_validation->run() == true)
            {   
                $getRemainingCount = getRemainingEmail();
                if($getRemainingCount['remaining'] > 0)
                {
                    // put validation
                    $getInfo = fetchvendorinfo();
                    // send email to admin user
                    $url = 'https://api.sendgrid.com/';
                    $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                    $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                    $json_string = array(
                    'to' => array(
                        $this->input->post('emailQuotationData')
                    ),
                    'category' => 'test_category'
                    );
                    $htm = $this->input->post('emailBodyData');
                    $params = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'x-smtpapi' => json_encode($json_string),
                        'to'        => $this->input->post('emailQuotationData'),
                        'fromname'  => $getInfo['name'],
                        'subject'   => $this->input->post('emailSubject'),
                        'html'      => $htm,
                        'from'      => $getInfo['email'],
                    );
                    $request =  $url.'api/mail.send.json';
                    $session = curl_init($request);
                    curl_setopt ($session, CURLOPT_POST, true);
                    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                    curl_setopt($session, CURLOPT_HEADER, false);
                    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($session);
                    curl_close($session);
                    $getResponse = json_decode($response);
                    if($getResponse->message == 'success')
                    {
                        $getRemaining = $getRemainingCount['remaining']-1;
                        updateSuceessEmailCount($getRemaining);
                        $this->session->set_flashdata('success','Email sent successsfully');
                        redirect(base_url()."Order/deliveryquotation", 'refresh'); 
                    }
                    else
                    {
                        updateErrorEmailLog();
                        $this->session->set_flashdata('error','Something went wrong. Please try again');
                        redirect(base_url()."Order/deliveryquotation", 'refresh'); 
                    }
                }
                else
                {
                    $this->session->set_flashdata('error','Please upgrade your email package');
                    $this->load->helper('url');
                    redirect(base_url()."order/deliveryquotation", 'refresh'); 
                }
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                $this->load->helper('url');
                redirect(base_url()."order/deliveryquotation", 'refresh'); 
            }
        }
        // Custom Pagination
         $getTotalCount = $this->Ordermodel->getQuotationOrderCount();
         $config['base_url']    = base_url().'order/deliveryquotation/';
         $config['uri_segment'] = 3;
         $config['total_rows']  = $getTotalCount['totalRows'];
         $config['per_page']    = $this->perPage;
         $config['num_tag_open'] = '<li>';
         $config['num_tag_close'] = '</li>';
         $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
         $config['cur_tag_close'] = '</a></li>';
         $config['next_link'] = 'Next';
         $config['prev_link'] = 'Prev';
         $config['next_tag_open'] = '<li class="pg-next">';
         $config['next_tag_close'] = '</li>';
         $config['prev_tag_open'] = '<li class="pg-prev">';
         $config['prev_tag_close'] = '</li>';
         $config['first_tag_open'] = '<li>';
         $config['first_tag_close'] = '</li>';
         $config['last_tag_open'] = '<li>';
         $config['last_tag_close'] = '</li>';
         $this->pagination->initialize($config);
         $page = $this->uri->segment(3);
         $offset = !$page?0:$page;
         // End Here
         $setArray = array('start'=>$offset,'limit'=>$this->perPage);
        $getQuotationOrder = $this->Ordermodel->fetchDeliveryQuotationData($setArray);
        $this->load->view('deliveryquotationorder',$getQuotationOrder);
    }
    public function printinvoicestatement()
    {
        $getOrderId = $this->uri->segment('3');
        if(!$getOrderId)
        {
            $this->load->helper('url');
            redirect(base_url()."order/allorders/", 'refresh');
        }
        $getOrderDetail = $this->Ordermodel->fetchInvoiceDetailsData($getOrderId);
        $this->load->view('printinvoicestatementorder',$getOrderDetail);
    }
    public function billedconfirmation()
    {
        $getOrderId = $this->uri->segment('3');
        if(!$getOrderId)
        {
            $this->load->helper('url');
            redirect(base_url()."order/unbilled", 'refresh');
        }
        // confirm bill data
        if($this->input->post('confirmBilledData'))
        {
            $this->form_validation->set_rules('productCount[]','confirmed product count','required');
            $this->form_validation->set_rules('paymentStatus','payment status','required');
            // $this->form_validation->set_rules('deliveryDate','delivery date','required');
            // $this->form_validation->set_rules('outletIdData','outlet','required|integer');
            if($this->form_validation->run() == true)
            {
                $setInsertArray = array('productId'=>$this->input->post('productId'),'variantId'=>$this->input->post('variantId'),'actualPrice'=>$this->input->post('actualPrice'),
                'totalPaid'=>$this->input->post('totalPaid'),'confirmCount'=>$this->input->post('productCount'),'paymentStatus'=>$this->input->post('paymentStatus'),
                'deliveryDate'=>$this->input->post('deliveryDate'),'outletId'=>$this->input->post('outletIdData'),'saleprice'=>$this->input->post('saleprice'),'totalTax'=>$this->input->post('totaltax'));
                $getBilledStatus = $this->Ordermodel->updateConfirmOrder($setInsertArray);
                if($getBilledStatus['message'] == 'yes')
                {
                    // send SMS to customer

                    // send notification
                    $getDeviceId = updategetUserDevice($this->input->post('outletIdData'));
                    if($getDeviceId[0]->houdinv_users_device_id)
                    {
                        define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                        $to=$getDeviceId[0]->houdinv_users_device_id;
                        $message= $getMessage;
                        $msg = array(
                        'title'     => '',
                        'body'=> 'Your order status has been confirmed',
                        'sound' => 'mySound'/*Default sound*/
                        );
                        $fields = array
                        (
                            'to'            => $to,
                            'notification'  => $msg,
                            'priority'      =>'high',
                        );
                        $headers = array
                        (
                            'Authorization: key=' . API_ACCESS_KEY,
                            'Content-Type: application/json'
                        );
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $result = curl_exec($ch );
                        curl_close( $ch );
                        shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                        $jsonDecodeData = json_decode($result,true);
                    }
                    $this->session->set_flashdata('success','Order status updated successfully');
                    $this->session->set_flashdata('deliveryStatus','1');
                    $this->load->helper('url');
                    redirect(base_url()."order/billedconfirmation/".$this->uri->segment('3')."", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    $this->load->helper('url');
                    redirect(base_url()."order/billedconfirmation/".$this->uri->segment('3')."", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                $this->load->helper('url');
                redirect(base_url()."order/billedconfirmation/".$this->uri->segment('3')."", 'refresh');
            }
        }
        // tag delivery service
        // tag delivery boy
        if($this->input->post('tagDeliveryBoys'))
        {
            $this->form_validation->set_rules('deliveryType','delivery type','required');
            // $this->form_validation->set_rules('deliveryBoyId','delivery service','required');
            if($this->form_validation->run() == true)
            {
                $setDeliveryArray = array('type'=>$this->input->post('deliveryType'),'id'=>$this->input->post('deliveryBoyId'),'orderId'=>$this->input->post('deliveryOrderId'));
                $getStatus = $this->Ordermodel->tagDeliveryBoyData($setDeliveryArray);
                if($getStatus['message'] == 'yes')
                {
                    if($this->input->post('deliveryboyEmail'))
                    {
                        $getEmailStats = $this->Ordermodel->getEmailStatus();
                        if($getEmailStats['message'] == 'success')
                        {
                            // put validation
                            $getLogo = fetchvedorsitelogo();
                            $getInfo = fetchvendorinfo();
                            $getSocial = fetchsocialurl(); 
                            if(count($getSocial) > 0)
                            {
                                $setSocialString = "";
                                if($getSocial[0]->facebook_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->facebook_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon1.png" alt="Facebook" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                }
                                if($getSocial[0]->twitter_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->twitter_url.'" target="_blank"><img src="'.base_url().'assets/images/Icon2.png" alt="Twitter" style="max-height: 25px;height:100%;"/>&nbsp;&nbsp;</a>';
                                }
                                if($getSocial[0]->youtube_url)
                                {
                                    $setSocialString = $setSocialString.'<a href="'.$getSocial[0]->youtube_url.'" target="_blank"><img src="'.base_url().'assets/images/icon3.png" alt="youtubt" style="max-height: 25px;height:100%;"/></a>';
                                }
                            }
                            $url = 'https://api.sendgrid.com/';
                            $user = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_username;
                            $pass = $getSmsEmail['sendgrid'][0]->houdin_sendgrid_password;
                            $json_string = array(
                            'to' => array(
                                $this->input->post('deliveryboyEmail')
                            ),
                            'category' => 'test_category'
                            );
                            
                            
                            $htm = '<table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">
          <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
            <td class="container"  style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; width:100%; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
              <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff">
                  <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #9e9e9e4a;    border-bottom: 3px solid #000; margin: 0; padding: 20px;" align="center"  valign="top">
                      <img src="'.$getLogo.'" style="max-width: 53%;width:100%"/></td></tr>
                    <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                      <td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                          <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                            <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top"><strong style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">Hello</strong></td></tr>
                            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                              <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">You got one delivery request (orderId: #'.$this->input->post('deliveryOrderId').')</td></tr>
                              <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top">
                                Thanks for choosing '.$getInfo['name'].'.</td></tr></table></td></tr></table>
          <div  style="text-align: center; font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box;background: #000; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 10px 0px;">
          <table width="100%" ><tr >
          <td  ><a href="#" style="  font-size: 12px; color: #fff; text-decoration: none; margin: 0;">&copy;'.$getInfo['name'].' '.date('Y').'--All right reserverd </td></tr>
          <tr><td style="">
          '.$setSocialString.'
         </td>
          </tr></table>
          </div> 
          </div></td></tr></table>';
                            
                            
                            $params = array(
                                'api_user'  => $user,
                                'api_key'   => $pass,
                                'x-smtpapi' => json_encode($json_string),
                                'to'        => $this->input->post('deliveryboyEmail'),
                                'fromname'  => $getInfo['name'],
                                'subject'   => 'Delivery Tag Notification',
                                'html'      => $htm,
                                'from'      => $getInfo['email'],
                            );
                            $request =  $url.'api/mail.send.json';
                            $session = curl_init($request);
                            curl_setopt ($session, CURLOPT_POST, true);
                            curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                            curl_setopt($session, CURLOPT_HEADER, false);
                            curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                            $response = curl_exec($session);
                            curl_close($session);
                            $getResponse = json_decode($response);
                            // send notification
                            $getDeviceId = updategetUserDevice($this->input->post('deliveryOrderId'));
                            if($getDeviceId[0]->houdinv_users_device_id)
                            {
                                define( 'API_ACCESS_KEY', 'AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ');
                                $to=$getDeviceId[0]->houdinv_users_device_id;
                                $message= $getMessage;
                                $msg = array(
                                'title'     => '',
                                'body'=> 'Delivery boy assigned to your Order',
                                'sound' => 'mySound'/*Default sound*/
                                );
                                $fields = array
                                (
                                    'to'            => $to,
                                    'notification'  => $msg,
                                    'priority'      =>'high',
                                );
                                $headers = array
                                (
                                    'Authorization: key=' . API_ACCESS_KEY,
                                    'Content-Type: application/json'
                                );
                                $ch = curl_init();
                                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                                curl_setopt( $ch,CURLOPT_POST, true );
                                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch );
                                curl_close( $ch );
                                shell_exec('curl -X POST --header "Authorization: key=<AAAAvRhvW78:APA91bEKmhPGUDkwk1Om5lEeTf1gGcWGAYR3SwZ_AfNR2UjetcNuXXPdiauFR7om2Nu-7fXc9O_VxCuupzCY-LTOmDA7QXBWcamq4RoEdXc18lhZUJFuy7h5RtKgRHDzFIRxCiC2-KMgRDvCk1lweBHvkTTLWiDynQ>" --header "Content-Type: application/json" https://fcm.googleapis.com/fcm/send -d "{\"to\":\"'.$to.'\",\"priority\":\"high\",\"notification\":{\"body\": \"'.stripslashes($message).'\"}}"');
                                $jsonDecodeData = json_decode($result,true);
                            }
                            if($getResponse->message == 'success')
                            {
                                $getRemainingCredit = $getEmailStats['remaining']-1;
                                $getId = $getEmailStats['id'];
                                $setArray = array('houdinv_emailsms_stats_remaining_credits'=>$getRemainingCredit);
                                $this->Ordermodel->updateCreditsData($setArray,$getId);
                                $this->session->set_flashdata('success','Delivery service assigned successfully');
                                redirect(base_url().'Order/allorders', 'refresh');
                            }
                        }
                    }
                    $this->session->set_flashdata('success','Delivery service assigned successfully');
                    redirect(base_url().'Order/deliverypending', 'refresh'); 
                }
                else if($getStatus['message'] == 'shipper')
                {
                    $this->session->set_flashdata('error','Shipper account not found');
                    redirect(base_url().'Order/unbilled', 'refresh');
                }
                else if($getStatus['message'] == 'no')
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Order/unbilled', 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error',$getStatus['message']);
                    redirect(base_url().'Order/unbilled', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url().'Order/unbilled', 'refresh');
            }
        }
        $getUnbilledData = $this->Ordermodel->fetchUnbilledData($getOrderId);
        $this->load->view('billedconfirmation',$getUnbilledData);
    }
    // get delivery boys details
    public function getDeliveryBoysDetails()
    {
        $this->form_validation->set_rules('type','text','required');
        if($this->form_validation->run() == true)
        {
            $getType = $this->input->post('type');
            if($getType == 2)
            {
                $getStaffData = $this->Ordermodel->getDeliveryBoysData();
                if($getStaffData['message'] == 'yes')
                {
                    echo $getStaffData['setData'];
                }
                else if($getStaffData['message'] == 'no')
                {
                    echo 'no staff';
                } 
                else
                {
                    echo 'no';
                }
            }
            else if($getType == 1)
            {
                $getKey = getpostemanKey();
                $url = 'https://sandbox-api.postmen.com/v3/shipper-accounts';
                $method = 'GET';
                $headers = array(
	                "content-type: application/json",
	                "postmen-api-key: ".$getKey[0]->houdinv_shipping_credentials_key.""
	            );
	            $curl = curl_init();	
                curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_HTTPHEADER => $headers,
	            ));
	            $response = curl_exec($curl);
	            $err = curl_error($curl);
	            curl_close($curl);
	            if ($err) {
	    	        echo 'no';
	            } else {
                    $getCourierService = json_decode($response,true);
                    if($getCourierService['meta']['message'] == 'OK')
                    {
                        $setCourierService = "";
                        if(count($getCourierService['data']['shipper_accounts']) > 0)
                        {   
                            for($index = 0; $index < count($getCourierService['data']['shipper_accounts']); $index++)
                            {
                                $setMainArray = $getCourierService['data']['shipper_accounts'][$index];
                                if($setMainArray['status'] == 'enabled')
                                {
                                    if($setCourierService)
                                    {
                                        
                                        $setCourierService = $setCourierService.'<option value="'.$setMainArray['id'].'">'.$setMainArray['slug'].'</option>';
                                    }
                                    else
                                    {
                                        $setCourierService = '<option value="">Choose Courier Service</option><option value="'.$setMainArray['id'].'">'.$setMainArray['slug'].'</option>';
                                    }
                                }
                            }
                            echo $setCourierService;
                        }
                        else
                        {
                            echo 'no staff';    
                        }
                    }
                    else
                    {
                        echo 'no staff';
                    }
	            }
            }
        }
        else
        {
            echo 'no';
        }
    }
    public function orderinvoicepdf()
    {
        $getOrderId = $this->uri->segment('3');
        if(!$getOrderId)
        {
            $this->load->helper('url');
            redirect(base_url()."order/allorders/", 'refresh');
        }
        // get invoice setting
        $getInvoiceSetting = invoiceSetting();
        
        $getOrderDetail = $this->Ordermodel->fetchInvoiceDetailsData($getOrderId);
        $getMainArrayData = $getOrderDetail['maindata'][0]['main'];
        $getMainUserData = $getOrderDetail['maindata'][0]['user'][0];
        if($getMainUserData->houdinv_order_users_name)
        {
            $getUserName = $getMainUserData->houdinv_order_users_name;
        }
        else
        {
            $getUserName = $getMainUserData->houdinv_user_name;
        }
        if($getMainUserData->houdinv_order_users_contact)
        {
            $getUserPhone = $getMainUserData->houdinv_order_users_contact;
        }
        else
        {
            $getUserPhone = $getMainUserData->houdinv_user_contact;
        }
        if($getMainUserData->houdinv_user_email)
        {
            $getUserEmail = $getMainArrayData->houdinv_user_email;
        }
        else
        {
            $getUserEmail = "";
        }

        $getOrderId = $getMainArrayData->houdinv_order_id;
        $getOrderStatus = $getMainArrayData->houdinv_order_confirmation_status;
        $getSalesChannel = $getMainArrayData->houdinv_order_type;
        $getOrderDate = date('d-m-Y',$getMainArrayData->houdinv_order_created_at);
        if(count($getOrderDetail['extraAddress']) > 0)
        {
            $setDeliveryAddress = $getOrderDetail['extraAddress'][0]->houdinv_order_users_main_address.", ".$getOrderDetail['extraAddress'][0]->houdinv_order_users_city.", ".$getOrderDetail['extraAddress'][0]->houdinv_order_users_zip;
            $setBillingAddress = $getOrderDetail['extraAddress'][0]->houdinv_order_users_main_address.", ".$getOrderDetail['extraAddress'][0]->houdinv_order_users_city.", ".$getOrderDetail['extraAddress'][0]->houdinv_order_users_zip;
        }
        else
        {
            $setDeliveryAddress = $getOrderDetail['deliveryAddress'][0]->houdinv_user_address_user_address.", ".$getOrderDetail['deliveryAddress'][0]->houdinv_user_address_city.", ".$getOrderDetail['deliveryAddress'][0]->houdinv_user_address_zip;
            $setBillingAddress = $getOrderDetail['billingAddress'][0]->houdinv_user_address_user_address.", ".$getOrderDetail['billingAddress'][0]->houdinv_user_address_city.", ".$getOrderDetail['billingAddress'][0]->houdinv_user_address_zip;
        }
        

        $getProductDetailsData = $getOrderDetail['productDetail'];
        $setTextData = "";
        $setSubTotal = 0;
        $setGrossTotal = 0;
        $setTaxAmount = 0;
        for($index = 0; $index < count($getProductDetailsData); $index++)
        {
            $setSubTotal = $setSubTotal+$getProductDetailsData[$index]['total'];
            $setGrossTotal = $setGrossTotal+($getProductDetailsData[$index]['saleprice']*$getProductDetailsData[$index]['quantity']);
            $setTaxAmount = $setTaxAmount+$getProductDetailsData[$index]['totaltax'];
            if($setTextData)
            {
                $setTextData = $setTextData.'<tr>
                <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['productName'].'</td>
                <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['saleprice'].'</td>
                <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['totaltax'].'</td>
                <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['quantity'].'</td>
                <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['total'].'</td>
                </tr>';
            }
            else
            {
                $setTextData = '<tr>
                <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['productName'].'</td>
                <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['saleprice'].'</td>
                <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['totaltax'].'</td>
                <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['quantity'].'</td>
                <td style="border-bottom:1px solid #000;font-size:12px;">'.$getProductDetailsData[$index]['total'].'</td>
                </tr>';
            }
        }
        if($getMainArrayData->houdinv_orders_deliverydate)
        {
            $setDeliveyDate = '<p style="font-size:12px;text-align:right;">Delivery Date : '.date('d-m-Y',strtotime($getMainArrayData->houdinv_orders_deliverydate)).'</p>';
        }
        $getDeliveryCharges = $getMainArrayData->houdinv_delivery_charge;
        $getDiscountCharge = $getMainArrayData->houdinv_orders_discount;
        $getNetPayable = $getMainArrayData->houdinv_orders_total_Amount;

        if($getInvoiceSetting[0]->total_saving_invoice == 1)
        {
            $setDiscount = ' <tr>
            <td colspan="4" style="text-align:right;border-bottom:1px solid #000;font-size:12px;">Discount</td>
            <td style="border-bottom:1px solid #000;font-size:12px;">'.$getDiscountCharge.'</td>
            </tr>';
        }
        
        if($getInvoiceSetting[0]->order_number_receipt == 1)
        {
            $setOrderId = ' <p style="font-size:12px;text-align:right;">Order ID : #'.$getOrderId.'</p>';
        }
        $getLogo = fetchvedorsitelogo();
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Order Invoice');
    $pdf->SetSubject('Houdin-e - Order Invoice');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = <<<EOD
<table style="width:100%;">
<tr style="text-align:center;"><td><img src="$getLogo" width="180px"></td></tr>
</table>
<br/>
<br/>
<table style="width:100%;">
<tr>
<td>
<h5>Customer Details</h5>
<p style="font-size:12px;">Name : $getUserName </p>
<p style="font-size:12px;">Mobile : $getUserPhone</p>
<p style="font-size:12px;">Email : $getUserEmail</p>


<p style="font-size:12px;">DeliveryAddress : $setDeliveryAddress</p>
<p style="font-size:12px;">Address : $setBillingAddress</p>
</td>
<td>
<h5 style="text-align:right;">Order Details</h5>
$setOrderId
<p style="font-size:12px;text-align:right;">Order Status : $getOrderStatus</p>
<p style="font-size:12px;text-align:right;">Sales channel : $getSalesChannel</p>
<p style="font-size:12px;text-align:right;">Order Date : $getOrderDate</p>
$setDeliveyDate
</td>
</tr>
</table>
<br/>
<br/>
<table style="width:100%;" cellpadding="10" >
<tr>
<td style="border-bottom:1px solid #000;font-size:12px;font-weight:bold;">Product Name</td>
<td style="border-bottom:1px solid #000;font-size:12px;font-weight:bold;">Price</td>
<td style="border-bottom:1px solid #000;font-size:12px;font-weight:bold;">Tax</td>
<td style="border-bottom:1px solid #000;font-size:12px;font-weight:bold;">QTY</td>
<td style="border-bottom:1px solid #000;font-size:12px;font-weight:bold;">Total Price</td>
</tr>
$setTextData
<tr>
<td colspan="4" style="text-align:right;font-size:12px;">Sub Total</td>
<td style="font-size:12px;">$setGrossTotal</td>
</tr>
<tr>
<td colspan="4" style="text-align:right;font-size:12px;">Tax</td>
<td style="font-size:12px;">$setTaxAmount</td>
</tr>
<tr>
<td colspan="4" style="text-align:right;font-size:12px;">Delivery Charges</td>
<td style="font-size:12px;">$getDeliveryCharges</td>
</tr>
$setDiscount
<tr>
<td colspan="4" style="text-align:right;font-size:12px;">Net payable</td>
<td style="font-size:12px;">$getNetPayable</td>
</tr>
</table>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_invoice.pdf', 'I');
    }
    public function orderlistpdf()
    {
        $getData = $this->uri->segment('3');
        if($getData == 'pending')
        {
            $getOrderdata = $this->Ordermodel->getPendingOrdersPdf();
        }
        else if($getData == 'pickup')
        {
            $getOrderdata = $this->Ordermodel->getPickupOrdersPdf();
        }
        else if($getData == 'paymentpending')
        {
            $getOrderdata = $this->Ordermodel->getPaymentPendingOrdersPdf();
        }
        else if($getData == 'unbilled')
        {
            $getOrderdata = $this->Ordermodel->getUnbilledOrdersPdf();
        }
        else
        {
            $getOrderdata = $this->Ordermodel->getAllOrdersPdf();   
        }
        $setHtmlData = "";
        foreach($getOrderdata as $allOrderData)
        {
            if($allOrderData['main']->houdinv_order_confirmation_status == 'unbilled' || $allOrderData['main']->houdinv_order_confirmation_status == 'cancel' || $allOrderData['main']->houdinv_order_confirmation_status == 'return' || $allOrderData['main']->houdinv_order_confirmation_status == 'cancel request')
            {
                $setStatusColor = 'red !important';
            }
            else if($allOrderData['main']->houdinv_order_confirmation_status == 'Processing' || $allOrderData['main']->houdinv_order_confirmation_status == 'order pickup' || $allOrderData['main']->houdinv_order_confirmation_status == 'Processed' || $allOrderData['main']->houdinv_order_confirmation_status == 'billed')
            {
                $setStatusColor = 'yellow !important';
            }
            else
            {
                $setStatusColor = 'green !important';
            }
            if($allOrderData['main']->houdinv_order_delivery_type == 'deliver')
            {
                $setDeliveryText = "Home Delivery";
            }
            else
            {
                $setDeliveryText = "Customer Pickup";
            }
            if($allOrderData['main']->houdinv_orders_deliverydate) 
            {
                $setDeliveryDate = date('d-m-Y',strtotime($allOrderData['main']->houdinv_orders_deliverydate)); 
            }
            else
            {
                $setDeliveryDate = "";
            }  
            
            if($allOrderData['main']->houdinv_payment_status == 0) 
            { 
                $setPaymentStatus = 'Not Paid'; $setColor='red !important'; 
            } 
            else 
            { 
                $setPaymentStatus = 'Paid'; $setColor='green !important'; 
            }
            
            $setToatalAmount = $allOrderData['main']->houdinv_orders_total_Amount;
            
            if($allOrderData['user'][0]->houdinv_user_name)
            {
                $setName = $allOrderData['user'][0]->houdinv_user_name;
            }
            else
            {
                $setName = $allOrderData['user'][0]->houdinv_order_users_name;
            }
            if($allOrderData['user'][0]->houdinv_user_email)
            {
                $setEmail = $allOrderData['user'][0]->houdinv_user_email;
            }
            else
            {
                $setEmail = "-";
            }
            if($allOrderData['user'][0]->houdinv_user_contact)
            {
                $setContact = $allOrderData['user'][0]->houdinv_user_contact;
            }
            else
            {
                $setContact = $allOrderData['user'][0]->houdinv_order_users_contact;
            }
            if($setHtmlData)
            {
                $setHtmlData = $setHtmlData.'
                <tr>
                <td style="font-size:12px;">#'.$allOrderData['main']->houdinv_order_id.'</td>
                <td style="font-size:12px;"><label style="font-weight:normal;color:'.$setStatusColor.'">'.$allOrderData['main']->houdinv_order_confirmation_status.'</label><br/>'.$setDeliveryText.'</td>
                <td style="font-size:12px;">'.$allOrderData['main']->houdinv_order_type.'</td>
                <td style="font-size:12px;"><label style="font-weight:normal !important;color:'.$setStatusColor.'">'.$allOrderData['main']->houdinv_order_confirmation_status.'</label> / <label style="font-weight:normal !important;color:'.$setColor.'">'.$setPaymentStatus.'</label></td>
                <td style="font-size:12px;">'.$setName.'( '.$setEmail.' , '.$setContact.')<br/>&#x20b9;&nbsp;<strong>'.$setToatalAmount.'</strong></td>
                </tr>
                ';
            }
            else
            {
                $setHtmlData = '
                <tr>
                <td style="font-size:12px;">#'.$allOrderData['main']->houdinv_order_id.'</td>
                <td style="font-size:12px;"><label style="font-weight:normal;color:'.$setStatusColor.'">'.$allOrderData['main']->houdinv_order_confirmation_status.'</label><br/>'.$setDeliveryText.'</td>
                <td style="font-size:12px;">'.$allOrderData['main']->houdinv_order_type.'</td>
                <td style="font-size:12px;"><label style="font-weight:normal !important;color:'.$setStatusColor.'">'.$allOrderData['main']->houdinv_order_confirmation_status.'</label> / <label style="font-weight:normal !important;color:'.$setColor.'">'.$setPaymentStatus.'</label></td>
                <td style="font-size:12px;">'.$setName.'( '.$setEmail.' , '.$setContact.')<br/>&#x20b9;&nbsp;<strong>'.$setToatalAmount.'</strong></td>
                </tr>
                ';
            }
        }
        $getLogo = fetchvedorsitelogo();

    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Order List');
    $pdf->SetSubject('Houdin-e - Order List');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = <<<EOD
<table style="width:100%;">
<tr style="text-align:center;"><td><img src="$getLogo" width="180px"></td></tr>
</table>
<br/>
<br/>

<table style="width:100%;" cellpadding="10" border="1">
<tr>
<td style="font-size:12px;font-weight:bold;">Order ID</td>
<td style="font-size:12px;font-weight:bold;">Delivery Details</td>
<td style="font-size:12px;font-weight:bold;">Sales Channel</td>
<td style="font-size:12px;font-weight:bold;">Order / Payment Status</td>
<td style="font-size:12px;font-weight:bold;">Customer</td>
</tr>
$setHtmlData
</table>
<br/>
<br/>
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_order_list.pdf', 'I');
    }
    public function completedorderlistpdf()
    {
        $getOrderdata = $this->Ordermodel->getComplatedOrdersPdf();
        $setHtmlData = "";
        foreach($getOrderdata as $allOrderData)
        {
            if($allOrderData['main']->houdinv_order_confirmation_status == 'unbilled' || $allOrderData['main']->houdinv_order_confirmation_status == 'cancel' || $allOrderData['main']->houdinv_order_confirmation_status == 'return' || $allOrderData['main']->houdinv_order_confirmation_status == 'cancel request')
            {
                $setStatusColor = 'red !important';
            }
            else if($allOrderData['main']->houdinv_order_confirmation_status == 'Processing' || $allOrderData['main']->houdinv_order_confirmation_status == 'order pickup' || $allOrderData['main']->houdinv_order_confirmation_status == 'Processed' || $allOrderData['main']->houdinv_order_confirmation_status == 'billed')
            {
                $setStatusColor = 'yellow !important';
            }
            else
            {
                $setStatusColor = 'green !important';
            }
            if($allOrderData['main']->houdinv_order_delivery_type == 'deliver')
            {
                $setDeliveryText = "Home Delivery";
            }
            else
            {
                $setDeliveryText = "Customer Pickup";
            }
            if($allOrderData['main']->houdinv_orders_deliverydate) 
            {
                $setDeliveryDate = date('d-m-Y',strtotime($allOrderData['main']->houdinv_orders_deliverydate)); 
            }
            else
            {
                $setDeliveryDate = "";
            }  
            
            if($allOrderData['main']->houdinv_payment_status == 0) 
            { 
                $setPaymentStatus = 'Not Paid'; $setColor='red !important'; 
            } 
            else 
            { 
                $setPaymentStatus = 'Paid'; $setColor='green !important'; 
            }
            
            $setToatalAmount = $allOrderData['main']->houdinv_orders_total_Amount;
            
            if($allOrderData['user'][0]->houdinv_user_name)
            {
                $setName = $allOrderData['user'][0]->houdinv_user_name;
            }
            else
            {
                $setName = $allOrderData['user'][0]->houdinv_order_users_name;
            }
            if($allOrderData['user'][0]->houdinv_user_email)
            {
                $setEmail = $allOrderData['user'][0]->houdinv_user_email;
            }
            else
            {
                $setEmail = "-";
            }
            if($allOrderData['user'][0]->houdinv_user_contact)
            {
                $setContact = $allOrderData['user'][0]->houdinv_user_contact;
            }
            else
            {
                $setContact = $allOrderData['user'][0]->houdinv_order_users_contact;
            }
            if($setHtmlData)
            {
                $setHtmlData = $setHtmlData.'
                <tr>
                <td style="font-size:12px;">#'.$allOrderData['main']->houdinv_order_id.'</td>
                <td style="font-size:12px;"><label style="font-weight:normal;color:'.$setStatusColor.'">'.$allOrderData['main']->houdinv_order_confirmation_status.'</label><br/>'.$setDeliveryText.'</td>
                <td style="font-size:12px;">'.$allOrderData['main']->houdinv_order_type.'</td>
                <td style="font-size:12px;"><label style="font-weight:normal !important;color:'.$setStatusColor.'">'.$allOrderData['main']->houdinv_order_confirmation_status.'</label> / <label style="font-weight:normal !important;color:'.$setColor.'">'.$setPaymentStatus.'</label></td>
                <td style="font-size:12px;">'.$setName.'( '.$setEmail.' , '.$setContact.')<br/>&#x20b9;&nbsp;<strong>'.$setToatalAmount.'</strong></td>
                </tr>
                ';
            }
            else
            {
                $setHtmlData = '
                <tr>
                <td style="font-size:12px;">#'.$allOrderData['main']->houdinv_order_id.'</td>
                <td style="font-size:12px;"><label style="font-weight:normal;color:'.$setStatusColor.'">'.$allOrderData['main']->houdinv_order_confirmation_status.'</label><br/>'.$setDeliveryText.'</td>
                <td style="font-size:12px;">'.$allOrderData['main']->houdinv_order_type.'</td>
                <td style="font-size:12px;"><label style="font-weight:normal !important;color:'.$setStatusColor.'">'.$allOrderData['main']->houdinv_order_confirmation_status.'</label> / <label style="font-weight:normal !important;color:'.$setColor.'">'.$setPaymentStatus.'</label></td>
                <td style="font-size:12px;">'.$setName.'( '.$setEmail.' , '.$setContact.')<br/>&#x20b9;&nbsp;<strong>'.$setToatalAmount.'</strong></td>
                </tr>
                ';
            }
        }
        $getLogo = fetchvedorsitelogo();

    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Houdin-e');
    $pdf->SetTitle('Houdin-e - Order List');
    $pdf->SetSubject('Houdin-e - Order List');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setFooterData(array(0,64,0), array(0,64,128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true); 
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
$html = <<<EOD
<table style="width:100%;">
<tr style="text-align:center;"><td><img src="$getLogo" width="180px"></td></tr>
</table>
<br/>
<br/>

<table style="width:100%;" cellpadding="10" border="1">
<tr>
<td style="font-size:12px;font-weight:bold;">Order ID</td>
<td style="font-size:12px;font-weight:bold;">Delivery Details</td>
<td style="font-size:12px;font-weight:bold;">Sales Channel</td>
<td style="font-size:12px;font-weight:bold;">Order / Payment Status</td>
<td style="font-size:12px;font-weight:bold;">Customer</td>
</tr>
$setHtmlData
</table>
<br/>
<br/>     
EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('houdin-e_order_list.pdf', 'I');
    }
}
  