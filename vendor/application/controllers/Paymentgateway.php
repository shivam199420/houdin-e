<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Paymentgateway extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->auth_users->is_logged_in();
        $this->load->model('paymentgatewaymodel');

    }
    public function index()
    {
        // add payumoney
        if($this->input->post('payumoneyAdd'))
        {
            $this->form_validation->set_rules('merchantKey','merchant key','required');
            $this->form_validation->set_rules('merchantSalt','merchant salt','required');
            $this->form_validation->set_rules('payuStatus','status','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {   
                $getPayuMoneyId = $this->input->post('payumoneyId');
                if($getPayuMoneyId)
                {
                    $setUpdatedArray = array('houdinv_payment_gateway_merchnat_key'=>$this->input->post('merchantKey'),'houdinv_payment_gateway_merchant_salt'=>$this->input->post('merchantSalt'),'houdinv_payment_gateway_status'=>$this->input->post('payuStatus'));
                    $getPayuStatus = $this->paymentgatewaymodel->updatePayumoney($setUpdatedArray,$getPayuMoneyId);
                    if($getPayuStatus['message'] == 'yes')
                    {
                        $this->session->set_flashdata('success','Payumoney addedd successfully');
                        redirect(base_url().'Paymentgateway', 'refresh'); 
                    }
                    else
                    {
                        $this->session->set_flashdata('success','Something went wrong. Please try again');
                        redirect(base_url().'Paymentgateway', 'refresh'); 
                    }
                }
                else
                {
                    $setData = strtotime(date('Y-m-d'));
                    $setInsertArray = array('houdinv_payment_gateway_type'=>'payu','houdinv_payment_gateway_merchnat_key'=>$this->input->post('merchantKey'),'houdinv_payment_gateway_merchant_salt'=>$this->input->post('merchantSalt'),'houdinv_payment_gateway_created_at'=>$setData,'houdinv_payment_gateway_status'=>$this->input->post('payuStatus'));
                    $getPayuStatus = $this->paymentgatewaymodel->insertPayumoney($setInsertArray);
                    if($getPayuStatus['message'] == 'yes')
                    {
                        $this->session->set_flashdata('success','Payumoney addedd successfully');
                        redirect(base_url().'Paymentgateway', 'refresh'); 
                    }
                    else
                    {
                        $this->session->set_flashdata('error','Something went wrong. Please try again');
                        redirect(base_url().'Paymentgateway', 'refresh'); 
                    }
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url().'Paymentgateway', 'refresh'); 
            }
        }
        // add authorized.net
        if($this->input->post('authorizedAdd'))
        {
            $this->form_validation->set_rules('loginId','text','required');
            $this->form_validation->set_rules('transactionKey','text','required');
            $this->form_validation->set_rules('authorizedStatus','text','required|in_list[active,deactive]');
            if($this->form_validation->run() == true)
            {
                $getId = $this->input->post('authorizedid');
                if($getId)
                {
                    $setArrayData = array('houdinv_payment_gateway_type'=>"auth",'houdinv_payment_gateway_merchnat_key'=>$this->input->post('loginId'),
                    'houdinv_payment_gateway_merchant_salt'=>$this->input->post('transactionKey'),'houdinv_payment_gateway_status'=>$this->input->post('authorizedStatus'));
                }
                else
                {
                    $setArrayData = array('houdinv_payment_gateway_type'=>"auth",'houdinv_payment_gateway_merchnat_key'=>$this->input->post('loginId'),
                    'houdinv_payment_gateway_merchant_salt'=>$this->input->post('transactionKey'),'houdinv_payment_gateway_status'=>$this->input->post('authorizedStatus')
                    ,'houdinv_payment_gateway_created_at'=>strtotime(date('Y-m-d')));
                }
                $getPaymentStatus = $this->paymentgatewaymodel->updateAuthorized($setArrayData,$getId);
                if($getPaymentStatus)
                {
                    $this->session->set_flashdata('success','Authorized.Net addedd successfully');
                    redirect(base_url().'Paymentgateway', 'refresh'); 
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url().'Paymentgateway', 'refresh'); 
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url().'Paymentgateway', 'refresh'); 
            }
        }
        $getPaymentGatewayData = $this->paymentgatewaymodel->getPaymentGateway();
        $this->load->view('paymentgateway',$getPaymentGatewayData);
    }
}
