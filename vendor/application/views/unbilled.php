        <?php 
        $this->load->view('Template/header');
        $this->load->view('Template/sidebar');
        $getCurrency = getVendorCurrency();
        if($getCurrency[0]->houdin_users_currency=="USD")
        {
            $currencysymbol= "$";
        }else if($getCurrency[0]->houdin_users_currency=="AUD"){
            $currencysymbol= "$";
        }else if($getCurrency[0]->houdin_users_currency=="Euro"){
            $currencysymbol= "£";
        }else if($getCurrency[0]->houdin_users_currency=="Pound"){
            $currencysymbol= "€";
        }else if($getCurrency[0]->houdin_users_currency=="INR"){
            $currencysymbol= "₹";
        }
        ?>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">

        <!--START Page-Title -->
        <div class="row">
        
        <div class="col-md-8">
        <h4 class="page-title">unbilled Orders</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>order/Orderlanding">Order</a></li>
        <li class="active">Unbilled Order</li>
        </ol>
        </div>
        <!-- <div class="col-md-4">
        <form role="search" class="navbar-left app-search pull-left custom_search_all">
        <input type="text" placeholder="Search..." class="form-control">
        <a href=""><i class="fa fa-search"></i></a>
        </form></div> -->

        
        </div>
        <!--END Page-Title --> 
        <div class="row m-t-20">
        <div class="col-sm-12">
        <?php 
        
        if($this->session->flashdata('error'))
        {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        else if($this->session->flashdata('success'))
        {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        ?>
        </div>
        </div>

        <div class="row">
        <div class="col-md-12">

        <div class="card-box table-responsive"> 
        <div class="btn-group pull-right m-t-10 m-b-20">
        <a href="<?php echo base_url() ?>order/orderlistpdf/unbilled" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
        </div>
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <!-- <th>Order Id</th> -->
        <th>Delivery Details</th>
        <th>Sales Channel</th>
        <th>Order Date</th>
        <th>Customer</th>
        <!-- <th>Status</th> -->
        <th>Order Value</th>
        <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        foreach($unbilledData as $unbilledDataList)
        {
        ?>
        <tr>
        
        <td><?php 
        if($unbilledDataList->houdinv_order_delivery_type == 'deliver')
            {
                $setDeliveryText = "Home Delivery";
            }
            else
            {
                $setDeliveryText = "Customer Pickup";
            } 
            if($unbilledDataList->houdinv_orders_deliverydate != "" && $unbilledDataList->houdinv_orders_deliverydate != '0000-00-00')
            {
                $setDeliveryDate = $unbilledDataList->houdinv_orders_deliverydate;
            }
            else
            {
                $setDeliveryDate = "";
            }
             echo $setDeliveryText."<br/>".$setDeliveryDate ?></td>
        <td><?php echo $unbilledDataList->houdinv_order_type ?></td>
        <td><?php echo  date('d-m-Y',$unbilledDataList->houdinv_order_created_at).'<br/><a href="'.base_url().'Order/vieworder/'.$unbilledDataList->houdinv_order_id.'">#'.$unbilledDataList->houdinv_order_id.'<a/>' ?></td>
        <td><?php echo $unbilledDataList->houdinv_user_name ?> <br/> (<?php echo $unbilledDataList->houdinv_user_contact ?>,<?php echo $unbilledDataList->houdinv_user_email ?>)</td>
        <td><?php echo $currencysymbol." ".$unbilledDataList->houdinv_orders_total_Amount ?></td>
        
        <td><a href="<?php echo base_url() ?>order/billedconfirmation/<?php echo $unbilledDataList->houdinv_order_id ?>" class="btn btn-primary">Bill It</a></td>
        </tr>
        <?php }
        ?>
        </tbody>
        </table>
        <ul class="pagination pull-right"> <?php  echo $this->pagination->create_links(); ?> </ul>
        </div>
        </div>
        </div>

        </div>
        </div>
        </div>
        <?php 
        $this->load->view('Template/footer');
        ?>