<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');

$price_all = json_decode($productData['0']->houdin_products_price,true);
if($price_all['discount'] != 0 && $price_all['discount'] != "")
{
    $setDiscountedPrice = $variantData[0]->houdin_products_variants_prices-$price_all['discount'];
    $setOriginalPrice = $variantData[0]->houdin_products_variants_prices;
}
else
{
    $setDiscountedPrice = 0;
    $setOriginalPrice = $variantData[0]->houdin_products_variants_prices;
}
$overall_rating = $reviews_sum->houdinv_product_review_rating;
$total_rating = count($reviews);
$final_rating = $overall_rating/$total_rating;
$getImageData = json_decode($productData[0]->houdinv_products_main_images,true);
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 
    <style>
    .checked {
    color: orange;
}

</style>
         <!-- Linking -->
         <div class="linking">
            <div class="container">
               <ol class="breadcrumb">
                  <li class="active">Product Details</li>
               </ol>
            </div>
         </div>
         <!-- Content -->
         <div id="content">
            <!-- Products -->
            <section class="padding-top-40 padding-bottom-60">
               <div class="container">
                  <div class="row">
                  <?php
                    if($this->session->flashdata('error'))
                    {
                            echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                    }
                    if($this->session->flashdata('success'))
                    {
                            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                    }
                    ?>
                     <!-- Products -->
                     <div class="col-md-12">

                        <div class="product-detail">
                           <div class="product">
                              <div class="row">
                                 <!-- Slider Thumb -->
                                 <div class="col-xs-5">
                                    <article class="slider-item on-nav">
                                              <div id="slider" class="flexslider">

                                                <ul class="slides">

                                                     <li>
                                                    <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $variantData[0]->houdin_products_variants_image;?>" alt="">
                                                  </li>
                                                  <?php
                                                  for($index = 0; $index < count($getImageData); $index++)
                                                  {
                                                      if($getImageData[$index])
                                                      {
                                                    ?>
                                                     <li>
                                                    <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $getImageData[$index]; ?>" alt="">
                                                  </li>
                                                  <?php } }
                                                  ?>


                                                </ul>
                                              </div>
                                              <div id="carousel" class="flexslider">
                                                <ul class="slides">

                                                     <li>
                                                    <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $variantData[0]->houdin_products_variants_image;?>" alt="">
                                                  </li>
                                                  <?php
                                                  for($indexthumb = 0; $indexthumb < count($getImageData); $indexthumb++)
                                                  {
                                                      if($getImageData[$indexthumb])
                                                      {
                                                    ?>
                                                     <li>
                                                    <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/productImage/<?php echo $getImageData[$indexthumb]; ?>" alt="">
                                                  </li>
                                                  <?php } }
                                                  ?>


                                                </ul>
                                              </div>
                                    </article>
                                 </div>
                                 <!-- Item Content -->
                                 <div class="col-xs-7">
                                    <h5><?php echo $variantData[0]->houdin_products_variants_title; ?></h5>
                                    <p class="rev">
                                    <?php
                                    for($i=1 ; $i<6 ;$i++)
                                    {
                                        if($i <= $final_rating)
                                        {
                                            $posi = strpos($final_rating,".");
                                            if($posi>=0)
                                            {
                                                $exp =  explode(".",$final_rating);
                                                if($exp[0]==$i)
                                                {
                                    ?>
                                    <i class="fa fa-star-half-o"></i>
                                    <?php
                                    }
                                    else
                                    {
                                        echo '<i class="fa fa-star"></i>';
                                    }
                                    }
                                    else
                                    {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <?php
                                    }
                                }
                                else
                                { ?>
                                <i class="fa fa-star-o"></i>  <?php } } ?>
                                    <span class="margin-left-10"><?php echo $total_rating; ?> Review(s)</span>
                                    </p>
                                    <div class="row">
                                       <div class="col-sm-6">
                                       <?php
                                        if($setDiscountedPrice != 0)
                                        {
                                        ?>
                                        <span class="new"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $variantData[0]->houdinv_products_variants_final_price; ?></span>
                                        <span class="old"><strike>(<?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $price_all['sale_price'];  ?>)</strike></span>
                                        <?php }
                                        else
                                        {
                                        ?>
                                        <span class="new">(<?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?><?php echo $setOriginalPrice;  ?>)</span>
                                        <?php }
                                        ?></div>
                                       <div class="col-sm-6">
                                           <?php
                                           if($variantData[0]->houdinv_products_variants_total_stocks > 0)
                                           {
                                               $settext = 'In Stock';
                                               $setClass = 'text-green';
                                           }
                                           else
                                           {
                                                $settext = 'Out Of Stock';
                                                $setClass = 'text-danger';
                                           }
                                           ?>
                                          <p>Availability: <span class="in-stock <?php echo $setClass ?>"><?php echo $settext ?></span></p>
                                       </div>
                                    </div>
                                    <!-- List Details -->
                                    <p><?php echo $productData[0]->houdin_products_short_desc; ?></p>

                                    <!-- Compare Wishlist -->
                                    <ul class="cmp-list">


                                       <li><a class="btn-round acc_btn btn_icn Add_to_whishlist_button" data-variant="<?php echo $variantData[0]->houdin_products_variants_id ?>"  data-cart="0"><i class="fa fa-heart"></i> Add to Wishlist</a></li>
                                    </ul>
                                    <!-- Quinty -->
                                    <div class="quinty">
                                       <input value="1" min="1" data-max="<?php echo $variantData[0]->houdinv_products_variants_total_stocks ?>" name="qttybutton" class="cart-plus-minus-box setCartQunatity" type="number">
                                    </div>

                                    <a class="btn-round acc_btn Add_to_cart_button" data-variant="<?php echo $variantData[0]->houdin_products_variants_id ?>"  data-cart="0"><i class="icon-basket-loaded margin-right-5"></i> Add to Cart</a>
                                 </div>
                              </div>
                           </div>
                           <!-- Details Tab Section-->
                           <div class="item-tabs-sec">
                              <!-- Nav tabs -->
                              <ul class="nav" role="tablist">
                                 <li role="presentation" class="active"><a href="#pro-detil"  role="tab" data-toggle="tab">Product Details</a></li>
                                 <li role="presentation"><a href="#cus-rev"  role="tab" data-toggle="tab">Customer Reviews</a></li>

                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                 <div role="tabpanel" class="tab-pane fade in active" id="pro-detil">
                                 <?php echo $productData[0]->houdin_products_desc; ?>
                                 </div>

                                 <div role="tabpanel" class="tab-pane fade" id="cus-rev">

                                   <div id="" class="">
                                      <h5 class="title">REVIEWS</h5>

                                      <?php

                                    if(!$final_rating > 0)
                                    {
                                        ?>
                                        <div class="col-sm-6 pull-left">
                                         <p>There are no reviews yet.</p>
                                    <p class="text-bigger">Be the first to review </p>
                                    </div>
                                    <?php }
                                    else
                                    {
                                    ?>
                                      <div class="col-sm-6 pull-left" style="overflow-y: scroll;height: 330px;">
                                      <?php
                                      foreach($reviews as $user_Review)
                                      {
                                    ?>

                                    <div class=" ">
                                          <div class=""><?php echo $user_Review->houdinv_product_review_user_name ?></div>


                                          <div class="strat-rating">
                                          <?php for($i=1 ; $i<6 ;$i++)
                                          {
                                          if($i <= $user_Review->houdinv_product_review_rating)
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star checked"></span>
                                          <?php }
                                          else
                                          {
                                          ?>
                                          <span data-rating="1" style="font-size: 16px;" class="fa fa-star checked"></span>
                                          <?php } } ?>

                                            <span class="text-date"><?php echo $user_Review->houdinv_product_review_created_at ?></span>
                                          </div>


                                          <div class=""><?php echo  $user_Review->houdinv_product_review_message; ?></div>
                                        </div>

                                    <?php } ?>  </div>  <?php  } ?>


                                      <div class="col-sm-6">
                                        <div class="comment-rating">
                                        <?php echo form_open(base_url( 'Productlistview' ), array( 'id' => 'rtng_form',"class"=>"checkout_form", 'method'=>'post' ));?>
                                        <div class="form-group">

                                        <div class="strat-rating">
                                            <span data-rating="1" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="2" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="3" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="4" style="font-size: 22px;" class="fa fa-star "></span>
                                            <span data-rating="5" style="font-size: 22px;" class="fa fa-star"></span>
                                            <input type="hidden"  name="rating" class="rating_data" value="0" />
                                            <input type="hidden"  name="product_id" class="product" value="0" />
                                            <input type="hidden"  name="variant_id" class="" value="<?php echo $this->uri->segment('3'); ?>" />
                                        </div>
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation form-control" type="text" name="user_name" placeholder="Your Name" />
                                        </div>
                                        <div class="form-group">

                                        <input  class="required_validation_for_review name_validation email_validation form-control" type="text" name="user_email" placeholder="Your Email" />
                                        </div>
                                        <div class="form-group">

                                        <textarea  class="required_validation_for_review form-control" name="user_message" rows="3" placeholder="Write a review"></textarea>
                                        </div>
                                        <input class="btn-round submit_form_review" type="submit" name="review_submit" value="Add Review" />
                                        <?php echo form_close(); ?>
                                        </div>
                                      </div>

                                    </div>



                                 </div>
                                 <!-- start -->

                                 <!-- end -->
                              </div>
                           </div>
                        </div>


         </div>
         <div class="clearfix"></div>
         <!-- End Content -->
         <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>

   <!-- <script src="<?php echo base_url() ?>Extra/electronic/js/vendors/jquery.nouislider.min.js"></script>  -->
      <script>
        //  jQuery(document).ready(function($) {



        //    //  Price Filter ( noUiSlider Plugin)

        //      $("#price-range").noUiSlider({

        //      range: {

        //        'min': [ 0 ],

        //        'max': [ 1000 ]

        //      },

        //      start: [40, 940],

        //          connect:true,

        //          serialization:{

        //              lower: [

        //          $.Link({

        //            target: $("#price-min")

        //          })

        //        ],

        //        upper: [

        //          $.Link({

        //            target: $("#price-max")

        //          })

        //        ],

        //        format: {

        //        // Set formatting

        //          decimals: 2,

        //          prefix: '$'

        //        }

        //          }

        //    })

        //  })



      </script>
      <script type="text/javascript">
    $(document).ready(function(){
        $(document).on('keyup','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        })
        $(document).on('change','.setCartQunatity',function(){
            if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        })
    })
    </script>
