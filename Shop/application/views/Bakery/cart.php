<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view("".$getShopName."/Template/header"); ?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
<style type="text/css">
ol.progtrckr {
  margin: 0;
  padding: 0;
  padding-bottom: 5%;
  list-style-type none;
}

ol.progtrckr li {
  display: inline-block;
  text-align: center;
  line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
ol.progtrckr[data-progtrckr-steps="5"] li { width: %; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

ol.progtrckr li.progtrckr-done {
  color: black;
  border-bottom: 4px solid yellowgreen;
}
ol.progtrckr li.progtrckr-todo {
  color: silver;
  border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
  content: "\00a0\00a0";
}
ol.progtrckr li:before {
  position: relative;
  bottom: -2.5em;
  float: left;
  left: 50%;
  line-height: 1em;
}
ol.progtrckr li.progtrckr-done:before {
  content: "\2713";
  color: white;
  background-color: yellowgreen;
  height: 2.2em;
  width: 2.2em;
  line-height: 2.2em;
  border: none;
  border-radius: 2.2em;
}
ol.progtrckr li.progtrckr-todo:before {
  content: "\039F";
  color: silver;
  background-color: white;
  font-size: 2.2em;
  bottom: -1.2em;
}
@media(max-width: 767px){

  ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
    width: 100%;
  }
  ol.progtrckr li.progtrckr-todo {
    color: silver;
    border-bottom: 4px solid silver;
    width: 100%;
  }
  ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 48%;
    line-height: 1em;
  }
}
.ps-btn i, button.ps-btn i{margin-left: 0px !important;}
</style>

<div id="back2top"><i class="fa fa-angle-up"></i></div>

<div class="loader"></div>

<div class="page-wrap">

  <div class="ps-section ps-section--order-form  pb-80">

    <div class="">

        <div class="ps-section--hero"><img src="<?php echo base_url(); ?>Extra/Uploads/02.jpg" alt="">

        <div class="ps-section__content text-center">

          <h3 class="ps-section__title">My Cart</h3>

          <div class="ps-breadcrumb">

            <ol class="breadcrumb">

              <li><a href="<?php echo base_url(); ?>">Home</a></li>

              <li class="active">Cart</li>

            </ol>

          </div>

        </div>

      </div>
      <?php if($message=='No data')
      {
        ?>
        <div class="ps-section__content">

          <div class="row">

            <div class="col-lg-3 col-md-3"></div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

              <h3 class="text-center">Making People Happy </h3>

              <hr>

              <p class="text-center">You have no order. Please <a href="<?php echo base_url(); ?>" style="text-decoration: underline !important;"> check here </a> to order now.</p>


            </div>

          </div>

        </div>
      <?php   }else{   ?>

        <div class="ps-section--cart pt-100 pb-100">

          <div class="container">

            <div class="ps-cart-listing">

              <p class="hidden-lg"><i>Slide right to view</i></p>

              <div class="table-responsive">

                <table class="table">

                  <thead>

                    <tr>

                      <th class="cpt_no">No.</th>
                      <th class="cpt_img">image</th>
                      <th class="cpt_pn">product name</th>
                      <th class="cpt_q">quantity</th>
                      <th class="cpt_p">price</th>
                      <th class="cpt_t">total</th>
                      <th class="cpt_r">remove</th>

                    </tr>

                  </thead>
<?php echo form_open(base_url( 'Cart/UpdateCart' ), array( 'id' => 'UpdateCart', 'method'=>'post' ));?>
                  <tbody>
                    <?php
                    $i=1;
                  $final_price =0;
                    foreach($AllCart as $thisItem)
                    {
                      if($thisItem['stock'] <= 0)
                      {
                        $setStockData = '(Out of stock)';
                      }
          $count = $thisItem['count'];
          $main_price = $thisItem['productPrice'];
                      $total_price = $main_price*$count;
                      $final_price =$final_price+$total_price;
                      ?>
                      <tr>

                          <td><span class="total-row"><?php echo  $i; ?></span></td>

                        <td>

                          <div class="ps-product--cart"><img src="<?php echo $this->session->userdata('vendorURL') ?>/upload/productImage/<?php echo $thisItem['productImage']; ?>" alt=""></div>

                        </td>

                        <td>
                          <div class=""><a href="<?php if($thisItem['productId'] != 0) { echo base_url()."Productlistview/".$thisItem['productId'];  } else { echo base_url()."Productlistview/variant/".$thisItem['productId']; }  ?>" class="cp_title"><?php echo $thisItem['productName']; ?><span style="color:red"><?php echo $setStockData; ?></span></a></div>
                        </td>

                        <td>

                          <input name="product[<?php echo  $i-1; ?>][product_id]" value="<?php echo $thisItem['cartId']; ?>" size="2" type="hidden">
                          <input name="product[<?php echo  $i-1; ?>][quantity]" value="<?php echo $thisItem['count']; ?>" size="2" min="1" type="number">

                        </td>
                        <td><p class="cp_price"><?php echo $main_price; ?></p></td>
                        <td><p class="cpp_total"><?php echo $total_price; ?></p></td>
                        <td><a class="ps-btn ps-btn--sm ps-btn--fullwidth cp_cart_remove" data-replace="<?php echo $total_price; ?>" data-id="<?php echo $thisItem['cartId']; ?>" dat><i class="fa fa-trash"></i></a></td>

                      </tr>
                      <?php $i++; } ?>
<?php echo form_close(); ?>
                    </tbody>

                  </table>

                </div>

                <div class="ps-cart__process">

                  <div class="row">

                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 ">

                      <div class="form-group form-group--icon ps-cart__promotion">

                          <a href="<?php echo base_url() ?>home/category5" class="ps-cart__shopping">Continiue Shopping</a>

                      </div>

                      <div class="form-groupform-order">

                        <a href="javascript:void(0)" class="ps-cart__shopping update_cart">Update Shopping Bag</a>

                      </div>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">

                      <div class="ps-cart__total">

                        <p>Total Price: <span><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?> <?php echo $final_price; ?></span></p>

                        <a href="<?php echo base_url() ?>Checkout" class="ps-btn ps-btn--sm ps-btn--fullwidth">Process to checkout</a>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>

        <?php } ?>

      </div>

    </div>


    <!-- cancel ordwer -->
    <div id="cancelOrderModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <form method="post">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" style="margin:0px; padding:0px;" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Cancel Order</h4>
            </div>
            <div class="modal-body">
              <input type="hidden" class="cancelOrderId" name="cancelOrderId" />
              <h3>Do you really want to raise cancel order request.</h3>
            </div>
            <div class="modal-footer">
              <input type="submit" name="cancelOrder" class="btn btn-danger" value="Cancel"/>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </form>
      </div>
    </div>

    <!-- Shopping Cart End -->

    <?php
    $getShopName = getfolderName($this->session->userdata('shopName'));
    $this->load->view("".$getShopName."/Template/footer"); ?>

    <script type="text/javascript">
    $(document).on('click','.cancelOrderBtn',function(){
      $('.cancelOrderId').val($(this).attr('data-id'));
      $('#cancelOrderModal').modal('show');
    })
    </script>
