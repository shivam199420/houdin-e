<?php 
if(!$this->session->userdata('authorizedOrderData'))
{
    $this->session->set_flashdata('success',"Your order is successfully placed but something went wrong with transaction.");
    redirect(base_url().'Orders', 'refresh');
}
?>
<link rel="stylesheet" href="<?php echo base_url() ?>Extra/apparels/css/bootstrap.min.css" />	
<!-- Modal -->
<div id="getCardDetailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form id="cardData" method="post" action="<?php echo base_url() ?>Checkout/setAuthorizedPaymentData">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Authorized.Net</h4> -->
      </div>
      <div class="modal-body">
      <div class="row">
      <div class="container">
        <div class="form-group col-sm-12">
        <input type="hidden" name="orderId" value="<?php echo $this->session->userdata('authorizedOrderData') ?>"/>
        <label>Card Number</label>
        <input type="text" class="form-control required_validation_for_card number_validation name_validation" name="cardNumber"/>
        </div>
        
        <div class="form-group col-sm-6" style="float:left">
        <label>Expiry Month</label>
        <select class="form-control required_validation_for_card" name="expiryMonth">
        <option value="">Choose Month</option>
        <?php 
        for($i = 1; $i <=12;$i++)
        {
        ?>
        <option value="<?php echo $i; ?>"><?php echo $i ?></option>
        <?php }
        ?>
        </select>
        </div>
        <div class="form-group col-sm-6" style="float:left">
        <label>Expiry Year</label>
        <select class="form-control required_validation_for_card" name="expiryYear">
        <option value="">Choose Year</option>
        <?php 
        $getValue = date('Y');
        $range = $getValue+20;
        for($i = $getValue; $i <=$range;$i++)
        {
        ?>
        <option value="<?php echo $i; ?>"><?php echo $i ?></option>
        <?php }
        ?>
        </select>
        </div>
        <div class="clearfix"></div>
        <div class="form-group col-sm-12">
        <label>Security Pin</label>
        <input type="text" maxlength="3" class="form-control number_validation required_validation_for_card name_validation" name="securitypin"/>
        </div>
        <div class="form-group col-sm-12">
        <label>Card Holder Name</label>
        <input type="text" class="form-control required_validation_for_card name_validation" name="name"/>
        </div>
        </div>
      </div>
      <div class="modal-footer">
      <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
        <input type="submit" class="btn btn-primary" name="Pay" value="Pay"/>
      </div>
      
    </div>
    </form>
  </div>


<script src="<?php echo base_url() ?>Extra/apparels/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url() ?>Extra/apparels/js/bootstrap.min.js"></script>
        <!-- CLient side form validation -->
        <script type="text/javascript">
        $(document).ready(function(){
            $('#getCardDetailModal').modal({
                show: true,
                keyboard: false,
                backdrop: 'static'
            });
           
                $(document).on('submit','#cardData',function(e){
                        var check_required_field='';
                        $(this).find(".required_validation_for_card").each(function(){
                                var val22 = $(this).val();
                                if (!val22){
                                        check_required_field =$(this).size();
                                        $(this).css("border-color","#ccc");
                                        $(this).css("border-color","red");
                                }
                                $(this).on('keypress change',function(){
                                        $(this).css("border-color","#ccc");
                                });
                        });
                        if(check_required_field)
                        {
                                return false;
                        }
                        else {
                            return true;
                        }
                });
                $(".number_validation").keydown(function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
    (e.keyCode >= 35 && e.keyCode <= 40)) {
    return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
    }
    });
    $(document).on('keydown', '.name_validation', function(e) {
    if (e.which === 32 &&  e.target.selectionStart === 0) {return false;}  });
              
        });
                </script>
                