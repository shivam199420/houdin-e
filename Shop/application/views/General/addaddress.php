<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
    <!-- breadcrumbs start -->
    <div class="breadcrumbs-area breadcrumb-bg ptb-100">
        <div class="container">
            <div class="breadcrumbs text-center">
                <h2 class="breadcrumb-title">Add New Address</h2>

            </div>
        </div>
    </div>
    <!-- breadcrumbs area end -->
    <!-- login area end -->
    <div class="login-area ptb-100">
        <div class="container">
            <div class="row">
            <?php
                if($this->session->flashdata('error'))
                {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
                }
                if($this->session->flashdata('success'))
                {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
                }
                ?>
                <div class="col-md-6 col-md-offset-3">
                    <div class="login-content">
                        <div class="login-title">
                            <h4>Add New Address</h4>
                        </div>
                        <div class="login-form">
                        <?php echo form_open(base_url( 'Address/adduseraddress' ), array( 'id' => 'addaddress', 'method'=>'post' ));?>
                        <input name="username" placeholder="Your Name" class=" required_validation_for_address name_validation" type="text">
                        <input name="userphone" placeholder="Your Phone number" pattern="^[-+]?\d+$"  class="phone_telephone required_validation_for_address name_validation" type="text">
                        <textarea rows="3" name="usermainaddress" placeholder="Street address. Apartment, suite, unit etc. (optional)" class=" required_validation_for_address name_validation"></textarea>
                        <input name="usercity" placeholder="Town/City*" class=" required_validation_for_address name_validation" type="text">
                        <input name="userpincode" placeholder="Post code / Zip" class=" required_validation_for_address name_validation" type="text">
                        <select class=" required_validation_for_address" name="usercountry">
                                <option value="">Choose Country</option>
                                <option value="<?php echo $countryList[0]->houdin_user_shop_country ?>"><?php echo $countryList[0]->houdin_country_name ?></option>
                                </select>
                                <div class="button-remember">
                                <!-- <input type="submit" class="mx-auto acc_btn text-center acc_btn" value="Add Address"/> -->
                                    <button class="login-btn" type="submit">Add Address</button>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- login area end -->
    <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>

   <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('submit','#addaddress',function(){
                var check_required_field=0
                $(this).find(".required_validation_for_address").each(function(){
                    var val22 = $(this).val();
                    if (!val22){
                        check_required_field++;
                        $(this).css("border-color","#ccc");
                        $(this).css("border-color","red");
                    }
                    $(this).on('keypress change',function(){
                        $(this).css("border-color","#ccc");
                    });
                });
                if(check_required_field != 0)
                {
                    return false;
                }
                else {
                    return true;
                }
            });
        });
        </script>
