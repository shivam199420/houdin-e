<?php

class Csvmodel extends CI_Model {
    
    function __construct() {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
        
    }
    
    function get_addressbook() {     
        $query = $this->db->get('houdinv_users');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
    
    function insert_csv($data) {
        $this->db->insert('houdinv_users', $data);
    }

    function get_email_check($email){

        $this->db->select('*')->from('houdinv_users'); 
        $this->db->where("houdinv_user_email", $email); 
         
        $getvisitorsdata = $this->db->get();    
         if($getvisitorsdata->num_rows() > 0){
            return false;
         }else{
            return true;
         }

    }

    function insert_product_csv($data) {
        $this->db->insert('houdinv_products', $data);
    }
}
/*END OF FILE*/
