<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header') ?>
<?php
// $this->load->helper('setting');
$getData = stroeSetting($this->session->userdata('shopName'));
$cartFunction = $getData[0]->receieve_order_out;
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?>
        <!-- Banner Part Start -->
        <?php
        if(count($sliderListData) > 0)
        {
        ?>
        <section id="banner">

        <i class="fa fa-chevron-left prv-arrow"></i>

        <i class="fa fa-chevron-right nxt-arrow"></i>

        <div class="banner-slider">
          <?php
          foreach($sliderListData as $sliderListDataList)
          {
            ?>

        <div class="banner-img" style="background: url(<?php echo $this->session->userdata('vendorURL') ?>upload/Slider/<?php echo $sliderListDataList->houdinv_custom_slider_image ?>) no-repeat center; background-size: cover">

        <div class="container">

        <div class="row">

        <div class="col-md-5">

        <div class="banner-content">

        <h1><?php echo $sliderListDataList->houdinv_custom_slider_head ?></h1>

        <h2><?php echo $sliderListDataList->houdinv_custom_slider_sub_heading ?></h2>

        <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting stry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p> -->

        </div>

        </div>

        </div>

        </div>

        </div>

      <?php }
      ?>
        </div>

        </section>
      <?php }
      ?>

        <!-- Banner Part End -->
        <?php

                    if($this->session->flashdata('message_name'))
                    {
                              echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
                    ?>


            <?php }
            if($this->session->flashdata('success'))
            {
                    echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
            }
           // print_r($all_data);
            ?>
        <!-- Newest Part Start -->

        <section id="newest">

        <div class="container">


        <div class="row">

          <div class="heading text-center">

          <h2><?php echo $customHome[0]->houdinv_custom_home_data_category; ?></h2>

          </div>

          <div class="featured-filter">

        <div class="newest-main">

          <?php
          foreach($categoryData as $categoryDataList)
          {
            if($categoryDataList->houdinv_category_thumb)
            {
              $setImageData = $this->session->userdata('vendorURL')."images/category/".$categoryDataList->houdinv_category_thumb;
            }
            else
            {
              $setImageData = base_url()."Extra/noPhotoFound.png";
            }
          ?>

        <div class="col-md-3 col-sm-3 col-xs-12">
       <div class="grid-product">
              <div class="image bg-lightblue custom">

        <img src="<?php echo $setImageData; ?>" alt="newest1" class="img-responsive">

        <div class="overlay1 text-center">
        <h2><?php echo $categoryDataList->houdinv_category_name ?></h2>

        <h3><?php echo date("Y"); ?> Collection</h3>

        <a href="<?php echo base_url() ?>Productlist/<?php echo $categoryDataList->houdinv_category_id ?>">View now</a>

         </div>
         </div>
        </div>
        </div>

      <?php }
      ?>

        </div>

        </div>

        </div>

        </div>

        </section>

        <!-- Newest Part End -->



        <!-- Featured Part Start -->
        <?php
        if($customHome[0]->houdinv_custom_home_data_latest_product && count($latestProducts) > 0)
        {
        ?>
        <section id="featured">

        <div class="container">

        <div class="row">

        <div class="featured-main">

        <div class="heading text-center">

        <h2><?php echo $customHome[0]->houdinv_custom_home_data_latest_product; ?></h2>

        </div>

        <div class="featured-filter">

          <?php
          // print_r($latestProducts);
          foreach($latestProducts as $latestProductsList)
          {
            $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
            $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
          ?>



        <div class="gallery_product col-md-3 col-sm-4 filter men">

         <div class="grid-product">
              <div class="image bg-lightblue">
              <?php 
            if($getProductImage[0])
            {
                $latestProductImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
            }
            else
            {
                $latestProductImage = base_url()."Extra/noPhotoFound.png";
            }
            ?>
        <img src="<?php echo $latestProductImage; ?>" alt="featured-product-img" class="img-responsive">
        <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?><div class="new_badge out-of-stock">Out of Stock</div> <?php } ?>

<?php
if($latestProductsList->houdinv_products_main_quotation == 1)
{
?>
    <div class="new_badge ask-question model_qute_show quotation_span" data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal">Ask Quotation
</div>
<?php
}
?>
        <div class="overlay2 text-center">

        <a class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-heart"></i></a>



        <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-info"></i></a>

        <?php

        if($latestProductsList->houdinv_products_total_stocks <= 0)
        {
          if($cartFunction == 1)
          {
            ?>
            <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-shopping-cart"></i></a>
          <?php  }
        }
        else
        {
          ?>
          <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-shopping-cart"></i></a>
        <?php }
        ?>

        </div>

        </div>
        </div>

        <div class="feat-details">

        <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><p><?php echo substr($latestProductsList->houdin_products_title,0, 25).".." ; ?></p></a>
        <span><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?><?php  } ?></span>

        <div class="clearfix"></div>

        </div>

        </div>

      <?php }
      ?>


        </div>

        </div>

        </div>

        </div>

        </section>
      <?php }
      ?>

        <!-- Featured Part End -->

        <!-- Latest Part Start -->
        <?php
        if($customHome[0]->houdinv_custom_home_data_featured_product)
        {
         ?>
        <section id="latest">

        <i class="fa fa-chevron-left prv-arrow2"></i>

        <i class="fa fa-chevron-right nxt-arrow2"></i>

        <div class="container">

        <div class="row">

        <div class="latest-main">

        <div class="heading text-center">

        <h2><?php echo $customHome[0]->houdinv_custom_home_data_featured_product; ?></h2>

        </div>

        <div class="latest-item">

          <?php
          foreach($featuredProduct as $latestProductsList)
          {
            // print_r($latestProductsList);
            $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
            $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
          ?>

        <div class="gallery_product col-md-3">

          <div class="grid-product">
              <div class="image bg-lightblue">
              <?php 
            if($getProductImage[0])
            {
                $feturedProduct = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
            }
            else
            {
                $feturedProduct = base_url()."Extra/noPhotoFound.png";
            }
            ?>
          <img src="<?php echo $feturedProduct; ?>" alt="featured-product-img" class="img-responsive">
          <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?><div class="out-of-stock">Out of Stock</div><?php } ?>

  <?php
  if($latestProductsList->houdinv_products_main_quotation == 1)
  {
  ?>
      <div class="new_badge ask-question model_qute_show quotation_span" data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal">Ask Quotation
  </div>
  <?php
  }
  ?>
          <div class="overlay2 text-center">

          <a class="Add_to_whishlist_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-heart"></i></a>




          <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-info"></i></a>

          <?php

          if($latestProductsList->houdinv_products_total_stocks <= 0)
          {
            if($cartFunction == 1)
            {
              ?>
              <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-shopping-cart"></i></a>
            <?php  }
          }
          else
          {
            ?>
            <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>"><i class="fa fa-shopping-cart"></i></a>
          <?php }
          ?>

          </div>

          </div>
          </div>

        <div class="feat-details">

        <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><p><?php echo substr($latestProductsList->houdin_products_title,0, 25).".." ; ?></p></a>
        <span><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?><?php  } ?></span>

        <div class="clearfix"></div>

        </div>


        </div>
      <?php }
      ?>



        </div>

        </div>

        </div>

        </div>

        </section>

      <?php }
      ?>
        <!-- Latest Part End -->



        <!-- Testimonial Part Start -->
        <?php if(count($storeTestimonial) > 0 && $customHome[0]->houdinv_custom_home_data_testimonial)
        {
        ?>
        <section  id="testimonial">

        <div class="heading3 text-center">

        <h2><?php echo $customHome[0]->houdinv_custom_home_data_testimonial; ?></h2>

        </div>

        <div class="testimonial-bg">

        <i class="fa fa-chevron-left prv-arrow3"></i>

        <i class="fa fa-chevron-right nxt-arrow3"></i>

        <div class="container">

        <div class="row">

        <div id="testimonial-slider" class="testimonial-main">


          <?php
          foreach($storeTestimonial as $storeTestimonialData)
          {
          ?>

        <div class="col-md-6">

        <div class="testimonial-item">

        <div class="col-md-3 test-img">

        <img src="<?php if($storeTestimonialData->testimonials_image) { ?> <?php echo $this->session->userdata('vendorURL')."upload/testimonials/".$storeTestimonialData->testimonials_image; ?> <?php  } else { ?> <?php echo base_url() ?>Extra/apparels/img/testimonial/1.jpg <?php } ?>" class="img-responsive" alt="">
        <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?>
        <!--<div class="new_badge">Out of Stock</div>-->
        <?php } ?>

<?php
if($latestProductsList->houdinv_products_main_quotation == 1)
{
?>
    <div class="new_badge ask-question model_qute_show quotation_span" data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal">Ask Quotation
</div>
<?php
}
?>
        </div>

        <div class="col-md-9 test-details">

        <h2><?php echo $storeTestimonialData->testimonials_name ?></h2>

        <!-- <h3>Sketch Artist</h3> -->

        <p>  <?php echo $storeTestimonialData->testimonials_feedback ?></p>

        <h4><?php echo $storeTestimonialData->testimonials_name ?></h4>

        </div>

        </div>

        </div>

      <?php }
      ?>
        </div>

        </div>

        </div>

        </div>

        </section>
        <?php  } ?>
                <!-- End STestimonials Area -->
        <?php
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/Template/footer') ?>
