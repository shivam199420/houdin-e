<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style>
.thunbnail_image
{
  height: 68px;
width: 68px;
float: right;
margin-top: -11px;

}
.btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0; 
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
      }
.btn-circle.btn-lg {
    width: 186px;
    height: 44px;
    padding: 10px 13px;
    font-size: 16px;
    line-height: 1.33;
    border-radius: 8px;
}
.note-editable{
  min-height: 180px!important;
  height: auto!important;
}
.add-descrption {
    float: right;
    right: 47px;
    display: inline-block;
    position: absolute;
    cursor: pointer;
}
.d-none {
    display: none !important;
}
</style>
<div class="content-page">
<?php
//print_r($All_one_var);
$one = get_object_vars($one);
$price = json_decode($one['houdin_products_price'],true);
$images = json_decode($one['houdinv_products_main_images'],true);
$shipping = json_decode($one['houdinv_products_main_shipping'],true);
$payment = json_decode($one['houdinv_products_main_payment'],true);
$outlet_cost = json_decode($one['houdinv_products_main_stock'],true);
?>
    <!-- Start content -->
    <div class="content">
        <div class="container">
           <!--START Page-Title -->
            <div class="row">
           
                <div class="col-md-8">
                   <h4 class="page-title">Product</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="<?php echo base_url(); ?>product">Product</a></li>
                 
                  <li class="active">Edit Product</li>
                  </ol>
                  </div>
              
            </div>
           <!--END Page-Title -->
<?php
                                    
            if($this->session->flashdata('message_name'))
            {
                      echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
            ?>


    <?php }
    if($this->session->flashdata('success'))
    {
            echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
   // print_r($all_data);
    ?>
<?php echo form_open(base_url().'Product/editproduct/'.$one['houdin_products_id'],array("id"=>"Addproduct","enctype"=>"multipart/form-data")); ?>
 
        <div class="row">
        <div class="col-sm-8 m-t-20">
        <div class="card-box">
          <!-- product title -->
          <div class="form-group">
            <label>Title</label>
            <input type="text" id="titles"  value="<?php echo $one['houdin_products_title']; ?>" class="form-control required_validation_for_product" name="title" placeholder="Please enter product title" />
           <input type="hidden" value="<?php echo $one['houdin_products_id']; ?>"  name="ids"/>
          </div>
          <!-- prodct category -->
          <div class="form-group">
                      <label for="userName">Choose a Category</label>
                      
                     <select class="form-control select2"  name="category_name[]" multiple="">
                     
                        <option value="">Select Category</option>
                       <!-- <optgroup label="Alaskan/Hawaiian Time Zone">-->
                          <?php 
                       foreach($category as $values){?>
                            <option value="<?php echo $values['main']->houdinv_category_id; ?>^^sub0" <?php if(in_array($values['main']->houdinv_category_id,explode(",",$one['houdin_products_category']))){ echo "selected"; } ?>><?php echo $values['main']->houdinv_category_name; ?></option>
                            
                             <?php
                             
                           //  print_r($category['sub']);
                              foreach($values['sub'] as $sub1){?>
                           
                            <option style="margin-left:10px;" value="<?php echo $sub1['main']->houdinv_sub_category_one_id; ?>^^sub1" <?php if(in_array($sub1['main']->houdinv_sub_category_one_id,explode(",",$one['houdin_products_sub_category_level_1']))){ echo "selected"; } ?>>  - <?php echo $sub1['main']->houdinv_sub_category_one_name; ?></option>
                                
                              <?php
                             
                           //  print_r($category['sub']);
                              foreach($sub1['sub'] as $sub2){?>
                           
                            <option style="margin-left:20px;" value="<?php echo $sub2->houdinv_sub_category_two_id; ?>^^sub2" <?php if(in_array($sub2->houdinv_sub_category_two_id,explode(",",$one['houdin_products_sub_category_level_2']))){ echo "selected"; } ?>>   -- <?php echo $sub2->houdinv_sub_category_two_name; ?></option>
                                
                           
                           
                           
                             <?php } } } ?>
                        <!--</optgroup>-->
                      </select>
                     </div>
                     <!-- product type -->
                     <div class="form-group">
            <label>Product type</label>
            <select class="form-control required_validation_for_product " id="type_change" name="product_type">
            <option value="">Select Product Type</option>
            <?php 
            foreach($type as $values){?>
            <option value="<?php echo $values->houdinv_productType_id; ?>" <?php if($one['houdin_product_type']==$values->houdinv_productType_id){ echo "selected"; } ?>  data-sub ='<?php echo ($values->houdinv_productType_subattr); ?>' ><?php echo $values->houdinv_productType_name; ?></option>
            <?php } ?>
            </select>
          </div>
          <div class="form-group Add_attribut">
<?php 
$type_attr = json_Decode($one['houdin_product_type_attr_value']);
$attr_is =0;
foreach($type_attr as $tpeData)
 {
?>
<label class="col-md-3"><?php echo $tpeData->main; ?></label>

<input type="hidden" class="form-control" value="<?php echo $tpeData->main; ?>" name="Attr[<?php echo $attr_is; ?>][main]" placeholder="Enter value"/>
<input type="text" class="form-control" value="<?php echo $tpeData->value; ?>" name="Attr[<?php echo $attr_is; ?>][value]" placeholder="Enter value"/>

<?php } ?>
 </div>  
<!-- description -->
          <div class="form-group">
            <label>Description<span class="add-descrption"><i class="fa fa-plus-circle text-success show_description"></i><i class="fa fa-minus-circle d-none text-danger hide_description"></i></span></label>
          <textarea style="min-height: auto;"  class="form-control descption_box d-none" rows="2" id="editor1" placeholder="Enter product description" name="short_desc"><?php echo $one['houdin_products_short_desc']; ?></textarea>
          </div> 
               <!-- sell product as -->
          <div class="form-group">
            <label>Sell Product as *</label>
            
           <div > <input class="form-control radio_valid" type="radio" name="sell_as" <?php if($one['houdinv_products_sell_as'] == 'Packaged') { ?> checked="checked" <?php  } ?>   style="float:left;" value="Packaged"/><span style="margin-left:2%">Packaged</span></div>
           <div > <input class="form-control radio_valid" type="radio" name="sell_as" <?php if($one['houdinv_products_sell_as'] == 'Loose') { ?> checked="checked" <?php  } ?> style="float:left;" value="Loose"/><span style="margin-left:2%">Loose</span></div>
            <div ><input class="form-control radio_valid" type="radio" name="sell_as" <?php if($one['houdinv_products_sell_as'] == 'Both') { ?> checked="checked" <?php  } ?> style="float:left;" value="Both"/><span style="margin-left:2%">Both</span></div>
       
       </div>

           <!-- product fetaures -->
           <div class="form-group">
           <label>Product Features </label><div class="clearfix"></div>
                      <div class="col-md-4">
                        <div class="checkbox checkbox-inline" style="padding-left: 0px!important;margin-left: 0px!important">
                    <input type="checkbox" <?php if($one['houdinv_products_main_featured']==1){ echo "checked"; } ?> id="inlineRadio1" value="1" name="product_feature" >
                    <label for="inlineRadio1">  Featured </label>
                    </div>
                    </div>
                    
                       <div class="col-md-4">
                        <div class="checkbox checkbox-inline" style="padding-left: 0px!important;margin-left: 0px!important">
                    <input type="checkbox" <?php if($one['houdinv_products_main_quotation']==1){ echo "checked"; } ?> id="inlineRadio5" value="1" name="product_quotation" >
                    <label for="inlineRadio5">  Quotation </label>
                    </div>
                    </div>
                     <div class="col-md-4">
                    <div class="checkbox checkbox-inline" style="padding-left: 0px!important;margin-left: 0px!important">
                        <input type="checkbox" <?php if($one['houdin_products_status']==1){ echo "checked"; } ?> id="inlineRadio2"  name="product_status">
                        <label for="inlineRadio2"> Active </label>
                                </div>
                                </div>
                      </div> 
                      <div class="form-group">
            <label>Product Units *</label>
              <select class="form-control required_validation_for_product" name="productUnit">
                <option value="">Choose Product Unit</option>
                <option <?php if($one['houdinv_products_unit'] == 'BAG') { ?> selected="selected" <?php } ?> value="BAG">BAGS</option>
                <option <?php if($one['houdinv_products_unit'] == 'BALE') { ?> selected="selected" <?php } ?> value="BALE">BALE</option>
                <option <?php if($one['houdinv_products_unit'] == 'BUNDLES') { ?> selected="selected" <?php } ?> value="BUNDLES">BUNDLES</option>
                <option <?php if($one['houdinv_products_unit'] == 'BUCKLES') { ?> selected="selected" <?php } ?> value="BUCKLES">BUCKLES</option>
                <option <?php if($one['houdinv_products_unit'] == 'BILLIONS OF UNITS') { ?> selected="selected" <?php } ?> value="BILLIONS OF UNITS">BILLIONS OF UNITS</option>
                <option <?php if($one['houdinv_products_unit'] == 'BOX') { ?> selected="selected" <?php } ?> value="BOX">BOX</option>
                <option <?php if($one['houdinv_products_unit'] == 'BOTTLES') { ?> selected="selected" <?php } ?> value="BOTTLES">BOTTLES</option>
                <option <?php if($one['houdinv_products_unit'] == 'BUNCHES') { ?> selected="selected" <?php } ?> value="BUNCHES">BUNCHES</option>
                <option <?php if($one['houdinv_products_unit'] == 'CANS') { ?> selected="selected" <?php } ?> value="CANS">CANS</option>
                <option <?php if($one['houdinv_products_unit'] == 'CUBIC METER') { ?> selected="selected" <?php } ?> value="CUBIC METER">CUBIC METER</option>
                <option <?php if($one['houdinv_products_unit'] == 'CUBIC CENTIMETER') { ?> selected="selected" <?php } ?> value="CUBIC CENTIMETER">CUBIC CENTIMETER</option>
                <option <?php if($one['houdinv_products_unit'] == 'CENTIMETER') { ?> selected="selected" <?php } ?> value="CENTIMETER">CENTIMETER</option>
                <option <?php if($one['houdinv_products_unit'] == 'CARTONS') { ?> selected="selected" <?php } ?> value="CARTONS">CARTONS</option>
                <option <?php if($one['houdinv_products_unit'] == 'DOZEN') { ?> selected="selected" <?php } ?> value="DOZEN">DOZEN</option>
                <option <?php if($one['houdinv_products_unit'] == 'DRUM') { ?> selected="selected" <?php } ?> value="DRUM">DRUM</option>
                <option <?php if($one['houdinv_products_unit'] == 'GREAT GROSS') { ?> selected="selected" <?php } ?> value="GREAT GROSS">GREAT GROSS</option>
                <option <?php if($one['houdinv_products_unit'] == 'GRAMS') { ?> selected="selected" <?php } ?> value="GRAMS">GRAMS</option>
                <option <?php if($one['houdinv_products_unit'] == 'GROSS') { ?> selected="selected" <?php } ?> value="GROSS">GROSS</option>
                <option <?php if($one['houdinv_products_unit'] == 'GROSS YARDS') { ?> selected="selected" <?php } ?> value="GROSS YARDS">GROSS YARDS</option>
                <option <?php if($one['houdinv_products_unit'] == 'KILOGRAMS') { ?> selected="selected" <?php } ?> value="KILOGRAMS">KILOGRAMS</option>
                <option <?php if($one['houdinv_products_unit'] == 'KILOLITER') { ?> selected="selected" <?php } ?> value="KILOLITER">KILOLITER</option>
                <option <?php if($one['houdinv_products_unit'] == 'KILOMETRE') { ?> selected="selected" <?php } ?> value="KILOMETRE">KILOMETRE</option>
                <option <?php if($one['houdinv_products_unit'] == 'MILLILITRE') { ?> selected="selected" <?php } ?> value="MILLILITRE">MILLILITRE</option>
                <option <?php if($one['houdinv_products_unit'] == 'METERS') { ?> selected="selected" <?php } ?> value="METERS">METERS</option>
                <option <?php if($one['houdinv_products_unit'] == 'NUMBERS') { ?> selected="selected" <?php } ?> value="NUMBERS">NUMBERS</option>
                <option <?php if($one['houdinv_products_unit'] == 'PACKS') { ?> selected="selected" <?php } ?> value="PACKS">PACKS</option>
                <option <?php if($one['houdinv_products_unit'] == 'PIECES') { ?> selected="selected" <?php } ?> value="PIECES">PIECES</option>
                <option <?php if($one['houdinv_products_unit'] == 'PAIRS') { ?> selected="selected" <?php } ?> value="PAIRS">PAIRS</option>
                <option <?php if($one['houdinv_products_unit'] == 'QUINTAL') { ?> selected="selected" <?php } ?> value="QUINTAL">QUINTAL</option>
                <option <?php if($one['houdinv_products_unit'] == 'ROLLS') { ?> selected="selected" <?php } ?> value="ROLLS">ROLLS</option>
                <option <?php if($one['houdinv_products_unit'] == 'SETS') { ?> selected="selected" <?php } ?> value="SETS">SETS</option>
                <option <?php if($one['houdinv_products_unit'] == 'SQUARE FEET') { ?> selected="selected" <?php } ?> value="SQUARE FEET">SQUARE FEET</option>
                <option <?php if($one['houdinv_products_unit'] == 'SQUARE METERS') { ?> selected="selected" <?php } ?> value="SQUARE METERS">SQUARE METERS</option>
                <option <?php if($one['houdinv_products_unit'] == 'SQUARE YARDS') { ?> selected="selected" <?php } ?> value="SQUARE YARDS">SQUARE YARDS</option>
                <option <?php if($one['houdinv_products_unit'] == 'TABLETS') { ?> selected="selected" <?php } ?> value="TABLETS">TABLETS</option>
                <option <?php if($one['houdinv_products_unit'] == 'TEN GROSS') { ?> selected="selected" <?php } ?> value="TEN GROSS">TEN GROSS</option>
                <option <?php if($one['houdinv_products_unit'] == 'THOUSANDS') { ?> selected="selected" <?php } ?> value="THOUSANDS">THOUSANDS</option>
                <option <?php if($one['houdinv_products_unit'] == 'TONNES') { ?> selected="selected" <?php } ?> value="TONNES">TONNES</option>
                <option <?php if($one['houdinv_products_unit'] == 'TUBES') { ?> selected="selected" <?php } ?> value="TUBES">TUBES</option>
                <option <?php if($one['houdinv_products_unit'] == 'US GALLONS') { ?> selected="selected" <?php } ?> value="US GALLONS">US GALLONS</option>
                <option <?php if($one['houdinv_products_unit'] == 'UNITS') { ?> selected="selected" <?php } ?> value="UNITS">UNITS</option>
                <option <?php if($one['houdinv_products_unit'] == 'YARDS') { ?> selected="selected" <?php } ?> value="YARDS">YARDS</option>
                <option <?php if($one['houdinv_products_unit'] == 'OTHERS') { ?> selected="selected" <?php } ?> value="OTHERS">OTHERS</option>
              </select>
          </div> 
                      <!-- show product on -->
                      <div class="form-group">
            <label>Show product on *</label>
            
           <div > <input class="form-control radio_valid" type="radio" name="show_on"  <?php if($one['houdinv_products_show_on']=='App'){ echo "checked"; } ?> style="float:left;" value="App"/><span style="margin-left:2%">App</span></div>
           <div > <input class="form-control radio_valid" type="radio" name="show_on" style="float:left;" <?php if($one['houdinv_products_show_on']=='Website'){ echo "checked"; } ?> value="Website"/><span style="margin-left:2%">Website</span></div>
            <div ><input class="form-control radio_valid" type="radio" name="show_on" style="float:left;" <?php if($one['houdinv_products_show_on']=='Both'){ echo "checked"; } ?> value="Both"/><span style="margin-left:2%">Both</span></div>
       
       </div>

           <!-- <div class="form-group">
            <label>Page Title</label>
            <input type="text" class="form-control" value="<?php echo $one['houdin_products_pag_title']; ?>" name="page_title" placeholder="Please enter page title displayed at tab of browser" />
          </div> -->
        
                 
        
            
        
        
 <!--START ADD variants SECTION -->      
 <!-- <div class="row">
             <div class="col-md-12">
              <h4 class="page-title">Variants</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">
                  <p>Does this product have variations like color, size etc</p>
                   <button type="button" class="btn btn-info" data-toggle="modal" data-target="#delete_product">Add Variants</button> 
                </div>
              </div> 
        </div> -->



</div>
</div>
        <div class="col-md-4 m-t-20 ">
          <div class="card-box pull-left" style="width: 100%;">
            <h4>Images</h4>
           <div class="images_preview_row">
            <div class="col-md-8">
            <div class="form-group">
              <lable>Image 1</lable>
              <!--first_image  -->
              <input type="file" class="form-control images_class"  name="images[0]" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />
            </div>
          </div>
            <div class="col-md-4" >
              <img  class="thunbnail_image images_preview" src="<?php if($images[0]){ echo base_url()."upload/productImage/".$images[0]; } else { ?><?php echo base_url() ?>assets/images/users/avatar-1.jpg <?php } ?>"/>
            </div>
            </div>
            <!--image 2 -->
            <div class="images_preview_row">
            <div class="col-md-8">
            <div class="form-group">
              <lable>Image 2</lable>
              <input type="file" class="form-control images_class"   name="images[1]" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />
            </div>
          </div>
          <div class="col-md-4" >
            <img src="<?php if($images[1]){ echo base_url()."upload/productImage/".$images[1]; } else { ?><?php echo base_url() ?>assets/images/users/avatar-1.jpg <?php } ?>" class="thunbnail_image images_preview"/>
          </div>
          </div>
          <!-- image 3 -->
          <div class="images_preview_row">
          <div class="col-md-8">
          <div class="form-group">
            <lable>Image 3</lable>
            <input type="file" class="form-control images_class"  name="images[2]" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />
          </div>
        </div>
        <div class="col-md-4" >
          <img src="<?php if($images[2]){ echo base_url()."upload/productImage/".$images[2]; } else { ?><?php echo base_url() ?>assets/images/users/avatar-1.jpg <?php } ?>" class="thunbnail_image images_preview"/>
        </div>
        </div>
        <!-- image 4-->
        
        <div class="images_preview_row">
        <div class="col-md-8">
        <div class="form-group">
          <lable>Image 4</lable>
          <input type="file" class="form-control images_class"   name="images[3]" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />
        </div>
      </div>
      <div class="col-md-4" >
        <img src="<?php if($images[3]){ echo base_url()."upload/productImage/".$images[3]; } else { ?><?php echo base_url() ?>assets/images/users/avatar-1.jpg <?php } ?>" class="thunbnail_image images_preview"/>
      </div>
      </div>
      
      <!---- image 5 -->
      <div class="images_preview_row">
      <div class="col-md-8">
      <div class="form-group">
        <lable>Image 5</lable>
        <input type="file" class="form-control images_class"   name="images[4]" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />
      </div>
    </div>
    <div class="col-md-4" >
      <img src="<?php if($images[4]){ echo base_url()."upload/productImage/".$images[4]; } else { ?><?php echo base_url() ?>assets/images/users/avatar-1.jpg <?php } ?>" class="thunbnail_image images_preview"/>
    </div>
    </div>
    
    <!-- image 6 -->
    <div class="images_preview_row">
    <div class="col-md-8">
    <div class="form-group" >
      <lable>Image 6</lable>
      <input type="file" class="form-control images_class"   name="images[5]" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />
    </div>
  </div>
  <div class="col-md-4" >
    <img src="<?php if($images[5]){ echo base_url()."upload/productImage/".$images[5]; } else { ?><?php echo base_url() ?>images/users/avatar-1.jpg <?php } ?>" class="thunbnail_image images_preview"/>
  </div>
  </div>
  
  <!---image 7 -->
  <div class="images_preview_row">
  <div class="col-md-8">
  <div class="form-group">
    <lable>Image 7</lable>
    <input type="file" class="form-control images_class"   name="images[6]" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />
  </div>
</div>
<div class="col-md-4" >
  <img src="<?php if($images[6]){ echo base_url()."upload/productImage/".$images[6]; } else { ?><?php echo base_url() ?>assets/images/users/avatar-1.jpg <?php } ?>" class="thunbnail_image images_preview"/>
</div>
</div>

<!--- image 8 -->
<div class="col-md-8">
<div class="form-group">
  <lable>Image 8</lable>
  <input type="file" class="form-control images_class"   name="images[7]" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />
</div>
</div>
<div class="col-md-4" >
<img src="<?php if($images[7]){ echo base_url()."upload/productImage/".$images[7]; } else { ?><?php echo base_url() ?>assets/images/users/avatar-1.jpg <?php } ?>" class="thunbnail_image images_preview"/>
</div>
<div class="images_preview_row">
<div class="col-md-8">
<div class="form-group">
  <lable>Image 10</lable>
  <input type="file" class="form-control images_class"   name="images[8]" style="border:0px solid !important;padding: 7px 0px 0px 0px;" />
</div>
</div>
<div class="col-md-4" >
<img src="<?php if($images[8]){ echo base_url()."upload/productImage/".$images[8]; } else { ?><?php echo base_url() ?>assets/images/users/avatar-1.jpg <?php } ?>" class="thunbnail_image images_preview"/>
</div>
</div>
          </div>
        </div>
        
        







        
        <!-- add variants-->
        <div class="row">
             <div class="col-md-12"> 
             
       <div class="combinations ng-scope">

<?php  
if($All_one_var)
{
  $i=1;
  $count = count($All_one_var);
  foreach($All_one_var as $each)  
  {
    // print_r($each);
    $all_var_stock = array();
    $new_variant = json_decode($each->houdin_products_variants_new);
    $new_variant_stock = json_decode($each->houdin_products_variants_stock);
    foreach($new_variant_stock as $sty)
    {
      $shipping_ware =  $this->Shippingmodel->fetchwarehouseRecord_edit($sty->main);
      $all_var_stock[] = array("id"=>$sty->main,"val"=>$sty->value,"name"=>$shipping_ware[0]->w_name." outlet quantity");  
    }
    // if(($each->houdin_products_variants_default)==1)
    // {
    //   $read = "readonly";
    //   $checking = "checked";
    // }    
    // else
    // {
    //   $read = "";   
    // }
    if($i==1) {
    ?>
    <div class="card-box has_cusm" style="width: 100%;"> 
    <h4 class="clearfix"><b class="pull-left">Other Variants</b>
    <!-- <small class="btn btn-link pull-right" data-target="#add_options_modal" data-toggle="modal" >Add variant</small> -->
    </h4>
    <table id="variants" class="table table-stripped">
    <thead class="variant_table_is">
    <tr class="tr_head">
    <th>Variants</th>
    <th>Title</th>
    <th>Qty.</th>
    <th>MRP</th>
    <th>CP</th>
    <th>Margin</th>
    <th>SP</th>
    <th>Bardcode</th>
    <th>Image</th>
    <th>Delete</th>
    </tr>
    </thead>
    <tbody class="variant_tbody">
    <?php  } ?>
    <tr class="tr_class_var">
    <!-- <td></td> -->
    <!-- <td style="width: 10%;">
    <?php 
    // $n=0;
    // foreach($new_variant as $new_v)
    // { 
    //   print_r($new_v);
    ?>
    <input <?php //echo $read; ?> type="text"  name="new_value[<?php //echo $indexing; ?>][one<?php //echo $n; ?>]" value="<?php //echo $new_v->variant_value; ?>" class="form-control required_validation_for_product" >
    <?php
    //$n++; }
    ?> 
    </td> -->
    <?php 
    $getStockData = json_decode($each->houdin_products_variants_stock,true);
    $getQuantity = $getStockData[0]['value'];
    $getPricedata = json_decode($each->houdinv_products_variants_new_price,true); 
    ?>
    <td><?php echo $each->houdinv_products_variants_name; ?><input type="hidden" name="variantData[]" value="<?php echo $each->houdinv_products_variants_name; ?>"/><input type="hidden" name="already_id[]" value="<?php echo $each->houdin_products_variants_id; ?>"/></td>
    <td><input  type="text" value="<?php echo $each->houdin_products_variants_title; ?>"  name="new_title[]" class="form-control title_new required_validation_for_product" ></td>
    <td><input  type="text"  name="new_qty[]"  value="<?php echo $getQuantity; ?>" class="form-control required_validation_for_product number_validation" ></td>
    <td><input  type="text" value="<?php echo $getPricedata['sale_price']; ?>" name="new_price[]" class="form-control price_new required_validation_for_product number_validation" ></td>
    <td><input  type="text"  name="new_cp[]"  value="<?php echo $getPricedata['cost_price']; ?>"  class="form-control required_validation_for_product number_validation" ></td>
    <td><input  type="text"  name="new_margin[]"  value="<?php echo $getPricedata['margin']; ?>" class="form-control required_validation_for_product number_validation" ></td>
    <td><input  type="text"  name="new_sp[]"  value="<?php echo $getPricedata['price']; ?>" class="form-control required_validation_for_product number_validation" ></td>
    <td><input type="hidden"  name="barcode_data[]"  value="<?php echo $each->houdinv_products_variants_barcode; ?>" class="form-control barcode_input" placeholder="Enter Bar Code"/><a  class="btn btn-default btn-xs m-r-5 barcode_generate"><i class="fa fa-barcode"></i>Generate Barcode</a><img class="barcode_images" src="<?php echo base_url() ?>images/barcode/<?php echo $each->houdinv_products_variants_barcode; ?>" /><p style="color:red" class="message_text"></p></td>
    <td><input type="file" name="new_image[]"/><img style="height:80px;" src="<?php echo base_url() ?>upload/productImage/<?php echo $each->houdin_products_variants_image; ?>"/></td>
    <td class="delete-variant text-danger" data-id="<?php echo $each->houdin_products_variants_id; ?>"><i class="fa fa-trash"></i>
    </td>
    </tr>
    <?php  if($i==$count) { 
      ?>
      </tbody></table></div> 
      <?php  } ?> 
      <?php  $i++;
  }           
}
?>             
</div>
</div>
</div>
       <!--END ADD variants SECTION --> 
         <!--START ADD Produtc information -->      
         <div class="row">
             <div class="col-md-12">
              <h4 class="page-title">Product Infomration</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">
                <div class="form-group">
            <label>Barcode *</label>
            
            <?php 
            
            if($one['houdin_products_barcode_image'])
            {
                
               $imagess = base_url()."images/barcode/" .$one['houdin_products_barcode_image'];
           $namess = $one['houdin_products_barcode_image'];
            }
            else
            {
              $imagess = '';
           $namess = '';  
            }
            ?>
            
          <input type="hidden"  name="barcode"  value="<?php echo $namess; ?>" class="form-control barcode_input" name="barcode" placeholder="Enter Bar Code"/>
          <a  class="btn btn-default btn-xs m-r-5 barcode_generate">
          <i class="fa fa-barcode"></i>
          Generate Barcode
          </a>
          
          
          <img class="barcode_images" src="<?php echo $imagess; ?>" />
      <p style="color:red" class="message_text"></p>
        </div>
        <div class="form-group">
        <label>WareHouse *</label>
        
        <select class="form-control ware_house required_validation_for_product select2 select_input_requred" multiple="" name="product_warehouse[]">
        <option value="">Choose one</option>
        <?php 
       $all_warehouse_Selected =  explode(",",$one['houdin_products_warehouse']);
        foreach($WareHouse as $house)
        {
          ?>
        <option  <?php if(in_array($house->id,$all_warehouse_Selected)){echo "selected";} ?> data-name="<?php echo $house->w_name; ?>" value="<?php echo $house->id; ?>" ><?php echo $house->w_name; ?></option>
        
        <?php } ?>
        </select>
       
        </div> <div class="form-group Add_cost_quantity">
         <?php 
$attr_sd =0;
foreach($outlet_cost as $tpeData)
 {
  $name_data =   $this->db->select("w_name")
    ->from("houdinv_shipping_warehouse")
    ->where("id",$tpeData['main'])->get()->row();
  
?>
<div class="qty_val" data-val="<?php echo $tpeData['main']; ?> ">
<!--<label class="col-md-8"><?php //echo $name_data->w_name; ?>  outlet quantity </label>-->

<input type="hidden" class="form-control" value="<?php echo $tpeData['main']; ?>" name="quanti[<?php echo $attr_sd; ?>][main]" placeholder="Enter value"/>
<input type="hidden" min="1" pattern="[0-9]" class="form-control required_validation_for_product" value="<?php echo $tpeData['value']; ?>" name="quanti[<?php echo $attr_sd; ?>][value]" placeholder="Enter value"/>
</div>
<?php 
$attr_sd++;
} ?>
</div>
               </div>
             
                     
                        </div>
                    </div>
                </div>
       <!--end ADD Produtc information -->  
        <!--START ADD Inventory SECTION -->      
        <div class="row">
             <div class="col-md-12">
              <h4 class="page-title">Inventory</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">
                <div class="form-group">
                        <label>Low Stock Alert</label>
                        <input type="text"  onkeyup="checkINt(this);" value="<?php echo $one['houdinv_products_main_sort_order']; ?>" class="form-control required_validation_for_product" name="order_sort" placeholder="1" />
                      </div>
                      <div class="form-group">
                        <label>Batch/Lot no </label>
                        <input type="text" class="form-control" value="<?php echo $one['houdinv_products_batch_number']; ?>" name="batchNumber" placeholder="Enter Batch/Lot no" />
                      </div>

                      <div class="form-group">
                        <label>Product Expiry </label>
                        <input type="text" class="form-control date_picker" value="<?php if($one['houdinv_products_expiry'] != '0000-00-00') { echo $one['houdinv_products_expiry'];  } ?>" name="expiry" placeholder="Product Expiry Date" />
                      </div>
                    </div>
                </div>
              </div> 
        <!-- </div> -->
       <!--END ADD Inventory SECTION --> 
        <!--START ADD Pricing SECTION -->      
        <div class="row">
             <div class="col-md-12">
              <h4 class="page-title">Pricing</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">
                        <div class="col-md-6">
                          <div class="form-group">
                        <label>MRP</label>
                        <input type="text" id="pricess"  class="form-control required_validation_for_product" onkeyup="checkDec(this);"  value="<?php echo $price['sale_price']; ?>" name="Main_price" placeholder="Enter product price here, only numbers" />
                      </div>                     
                        </div>

                        <div class="col-md-6" >
                         <div class="input-group">
                        <span class="input-group-addon">
                        <input type="checkbox">
                      </span>
                      <input type="text" class="form-control "  onkeyup="checkDec(this);" name="discount_price" value="<?php echo $price['discount']; ?>" placeholder="Enter discounted price here, only numbers">
                      </div>
                      </div>
                      <div class="clearfix"></div>
                      
                      <div class="col-md-6">
                      <div class="form-group">
                        <label>Sale price</label>
                        <input type="text"  onkeyup="checkDec(this);" value="<?php echo $price['price']; ?>" class="form-control" name="sale_price" placeholder="Enter product sale price" />
                      </div>
                       </div>
                      
                       <div class="col-md-6">
                      <div class="form-group">
                        <label>Sales Tax&nbsp;&nbsp;<input type="checkbox" name="sales_tax_check"/></label>
                        <select class="form-control" name="sales_tax">
                        <option value="">Choose Sale Tax</option>
                        <?php 
                        foreach($taxData as $taxDataList)
                        {
                        ?>
                        <option <?php if($price['tax_price'] == $taxDataList->houdinv_tax_id) { ?> selected="selected" <?php  } ?> value="<?php echo $taxDataList->houdinv_tax_id ?>"><?php echo $taxDataList->houdinv_tax_name ?></option>
                        <?php }
                        ?>
                        </select>
                      </div>
                       </div>
                       <div class="clearfix"></div>

                       <div class="col-md-6">
                          <div class="form-group">
                        <label>Cost Price</label>
                        <input type="text" onkeyup="checkDec(this);" id="costP" value="<?php echo $price['cost_price']; ?>" class="form-control" name="cost_price" placeholder="Enter product cost price" />
                      </div>
                       </div>

                       <div class="col-md-6">
                      <div class="form-group">
                        <label>Sales Tax&nbsp;&nbsp;<input type="checkbox" name="sales_tax_check"/></label>
                        <select class="form-control" name="sales_tax">
                        <option value="">Choose Sale Tax</option>
                        <?php 
                        foreach($taxData as $taxDataList)
                        {
                        ?>
                        <option <?php if($price['tax_price'] == $taxDataList->houdinv_tax_id) { ?> selected="selected" <?php  } ?> value="<?php echo $taxDataList->houdinv_tax_id ?>"><?php echo $taxDataList->houdinv_tax_name ?></option>
                        <?php }
                        ?>
                        </select>
                      </div>
                       </div>
                       <div class="clearfix"></div>
                     
                        </div>
                      
                    </div>
                 
                 
                </div>
               
        
        
                <div class="row">
             <div class="col-md-12">
              <h4 class="page-title">Supplier</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">

                        <div class="col-md-12">
                          <div class="form-group">
                        <label>Supplier</label>
         <select class="form-control select2 select_input_requred" name="MainSuplier[]" multiple="">
            <option value="">Choose Supplier</option>
            <?php 
         
            foreach($supplier as $customerListData)
            {
             ?> 
           
            <option  <?php if(in_array($customerListData->houdinv_supplier_id,$supplierAdd)) { echo "selected"; } ?> value="<?php echo $customerListData->houdinv_supplier_id ?>"><?php echo $customerListData->houdinv_supplier_contact_person_name; ?></option> 
           <?php 
           }
            ?>
            </select> 
            
              </div>
                     
                        </div>

    
                    </div>
                 
                 
                </div>
              </div> 
        
        
       <!--END ADD Pricing SECTION -->   
       
        

       <!--START ADD Shipping SECTION -->      
        <div class="row">
             <div class="col-md-12">
              <h4 class="page-title">Shipping</h4> <br>
                <div class="card-box" style="float:left;width: 100%;">

                        <div class="col-md-6">
                          <div class="form-group">
                        <label>Length</label>
                        <input type="text" value="<?php echo $shipping['length']; ?>" onkeyup="checkDec(this);" class="form-control" name="length" placeholder="Enter product length in cm here, only numbers" />
                      </div>
                      <div class="form-group">
                        <label>Height</label>
                        <input type="text" value="<?php echo $shipping['height']; ?>" onkeyup="checkDec(this);" class="form-control" name="height" placeholder="Enter product height in cm here, only numbers" />
                      </div>
                        </div>

                        <div class="col-md-6">
                         <div class="form-group">
                        <label>Breath</label>
                        <input type="text" value="<?php echo $shipping['breath']; ?>" onkeyup="checkDec(this);" class="form-control" name="breath" placeholder="Enter product breadth in cm here, only numbers" />
                      </div>
                      <div class="form-group">
                        <label>Weight</label>
                        <input type="text" value="<?php echo $shipping['weight']; ?>" onkeyup="checkDec(this);" class="form-control" name="weight" placeholder="Enter product weight here (in gm) only numbers" />
                      </div>
                        </div>

                    </div>
                 
                </div>
              </div> 
        
       <!--END ADD Shipping SECTION -->   
       
      
 
        
      
        
            
  <div class="col-md-12">
          <button type="submit" name="formSubmit" value="submit" class="btn btn-info">Save</button>
        </div>
<?php echo form_close(); ?>
      </div>
      

<?php echo form_open(base_url().'Product/editproduct/'.$one['houdin_products_id'],array("id"=>"AddId","enctype"=>"multipart/form-data")); ?>
 <input type="hidden" name="delet_id"  id="delet_id"/>
    
 <?php echo form_close(); ?> 
    </div>
  </div>
<!--Delete-->

<!--START Option-->

  <div id="delete_product" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

          <div class="modal-dialog">
       
          <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

          <h4 class="modal-title">Option</h4>

          </div>

          <div class="modal-body">



          <div class="row">
          <div class="col-md-12">
          <table class="append_table table houdin_addvariants_box" >
               <?php  
                $ij =0;
                if($All_one_var)
                {
                    
                    
                    $i1=1;
                    $count1 = count($All_one_var);
                   foreach($All_one_var as $each)  
                   {
                    $new_variant1 = json_decode($each->houdin_products_variants_new);

             $n=0;
             foreach($new_variant1 as $new_v1)
           {
            
            
            if(!in_array(($new_v1->variant_name),$ary))
            {
                $ary[]=$new_v1->variant_name;
          
            ?>
                      <tr class="table_tr houdin_addvariants_box_row"> 
                        <td><div class="input-group">
                      <input type="text" class="form-control save_option_name_text" data-name="<?php echo  $new_v->variant_name ;?>" value="<?php echo  $new_v->variant_name ;?>" placeholder="option name like color, size, size" max-length="100" required="">
                      <span class="input-group-btn">
                        <button style="margin:0px!important;" class="btn btn-success save_option_name" type="submit"><i class="fa fa-check"></i></button>
                      </span>
                    </div></td>
                   
                    <td class="delete_option_append"><div class="input-group">
                      <input type="text" class="form-control option_val_text" placeholder="option values" max-length="100" required="">
                      <span class="input-group-btn">
                        <button style="margin:0px!important;" class="btn btn-success save_option_val" type="submit"><i class="fa fa-check"></i></button>
                      </span>
                    </div>
                    <?php 
                    
               foreach($All_one_var as $each1)  
                   {
                    $new_variant11 = json_decode($each1->houdin_products_variants_new);

                         $n1=0;
                         foreach($new_variant11 as $new_v11)
                       {
                       if(($new_v11->variant_name)==$new_v1->variant_name) 
                       {
                        $vb = $new_v11->variant_value;
                    
                    ?>
                    <div class="btn-group div_group added_option_btn" data-name=" <?php echo $vb; ?>">
                    <button type="button" class="btn btn-primary btn-sm"><span class="default-text"> <?php echo $vb; ?></span>&nbsp;&nbsp;<span class="remove_option_value"><i class="fa fa-trash"></i></span></button>
                  </div>
                  
                  <?php } } } ?>
                   
                    </td> 
                    <td class="remove_option" style="padding-left: 10px"><span class="input-group-btn">
                        <button style="margin:0px!important;" class="btn btn-success" type="submit"><i class="fa fa-trash"></i></button>
                      </span></td>
                      </tr>

                     <?php } } }  ?>
                      <?php } ?>

                    </table>
                    <div class="row">
                      <div class="col-md-12">
                          <input type="button" class="btn btn-success add_option" name="" value="Add Option">
                      </div>
                    </div>
          </div>
          </div>
          </div>

          <div class="modal-footer">

          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

         

          </div>

          </div>
          
          </div>

          </div>
  <!--END Option-->
  
  <div class="modal fade" id="add_options_modal" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title">Add Variant Options</h4>
        </div>
            <div class="modal-body">
  <?php  
         
                if($All_one_var)
                {
                  $ary1 =array();
                   foreach($All_one_var as $each2)  
                   {
                    $new_variant2 = json_decode($each2->houdin_products_variants_new);

             foreach($new_variant2 as $new_v2)
           {
            
           if(!in_array(($new_v2->variant_name),$ary1))
            {
                $ary1[]=$new_v2->variant_name;
            ?>
 <div class="variant-option-group">
              <div class="form-group">
                <label class="ng-binding"><?php echo $new_v2->variant_name; ?></label>
                
                <input type="text"  class="form-control  texthj" required="">
              </div>
            </div><!-- end ngRepeat: option in getNoOfOptions(noOfOptions) -->
            
            <?php
            } 
            }
            }
            }
            ?>
            
            <div id="variants_loading"></div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-info pull-right add_nm" type="submit">Save variant</button>
          </div>
        
      </div>
    </div>
  </div>



<!--- add stock popup-->
  <div class="modal fade" id="add_stock_data" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title">Add stock Options</h4>
        </div>
            <form class="modal-body" id="addstockvariantsform">
                  <div class="Add_cost_quantity_of_variant">


                  </div>
                
                
          </form>
           
           
         
        
      </div>
    </div>
  </div>

<?php $this->load->view('Template/footer.php') ?>
        <script type="text/javascript">
                  function checkDec(el){
 var ex = /^[0-9]+\.?[0-9]*$/;
 if(ex.test(el.value)==false){
   el.value = el.value.substring(0,el.value.length - 1);
  }
 }
 
 
                   function checkINt(el){
 var ex = /^[0-9]*$/;
 if(ex.test(el.value)==false){
   el.value = el.value.substring(0,el.value.length - 1);
  }
 }
    /*==============Form Validation===========================*/
        $(document).ready(function(){
            
            
             $(document).on('change','#type_change',function(c){
                        jQuery(".Add_attribut").empty(''); 
           var ids = $(this).val();
                        
                     var option_array =  $(this).find('option[value="'+ids+'"]').attr("data-sub");
                     // console.log(option_array);

            // $('.checkboxClass').find('.input_price').prop("disabled",true);
  var htm='';
  var attr_is=0;
          $.each($.parseJSON(option_array), function(idx,obj) {
//console.log(obj);
htm+='<label class="col-md-3">'+obj+'</label>';
//htm+='<div class="form-group col-md-9">';
htm+='<input type="hidden" class="form-control" value="'+obj+'" name="Attr['+attr_is+'][main]" placeholder="Enter value"/>';
htm+='<input type="text" class="form-control required_validation_for_product" name="Attr['+attr_is+'][value]" placeholder="Enter value"/>';
//htm+='</div>';

attr_is++;

     }); 
     
     
     $('.Add_attribut').append(htm);
     
     
     
     });      
      
         // warehouse quantity
   
          $(document).on('change','.ware_house',function(c){
                     
              var $this = $(this);         
           var idsss = $(this).val();
           
           
     var htm='';
  var attr_new=0;   
  
 var make =''; 
var ghtml ='';
var count_new = 0;
 var already_array = [];  
 var html_array = [];
  var html_array1 = [];
  $(".qty_val").each(function()
     { 
         
        if($(this).attr('data-val'))
            {
             // console.log($(this).html()); 
               var ghtml ='';
               var variantshtml = ''
               
               ghtml+='<div class="qty_val" data-val="'+$(this).attr('data-val')+'">';  
              ghtml+=$(this).html();
               ghtml+='</div>';
               
            /*  var attr_val = $('.qty_val_var').attr('data-val');
               variantshtml+='<div class="qty_val_var" data-val="'+attr_val+'">';  
              variantshtml+=$('.qty_val_var').html();
               variantshtml+='</div>'; */
               
               
               var hj = $(this).attr('data-val');
               already_array.push(hj.trim());
               html_array.push(ghtml);
             //  html_array1.push(variantshtml);
              attr_new++; 
            }
            else
            {
                 console.log("none");   
            }
                
    
                  });
    
      $ty=0;     
   $.each(idsss, function(idx,obj) {
       
       var values =  $this.find('option[value="'+obj+'"]').attr("data-name");
    //    console.log(already_array);
  //   console.log(obj);
 //  var idx = already_array.indexOf(obj);
 var check =  $.inArray(obj,already_array);
  // console.log(check);
     
if (check < 0) { 
  //  console.log("this");
  htm+='<div class="qty_val" data-val="">';   
 htm+='<label class="col-md-8">'+values+' outlet quantity </label>';

htm+='<input type="hidden" class="form-control" value="'+obj+'" name="quanti['+attr_new+'][main]" />';
htm+='<input type="number" class="form-control required_validation_for_product" name="quanti['+attr_new+'][value]" min="1" pattern="[0-9]" value="1" placeholder="Enter value"/>';
 htm+='</div>'; 
 
 /*
  make+='<div class="qty_val_var" data-val="">';   
 make+='<label class="col-md-8">'+values+' outlet quantity </label>';

make+='<input type="hidden" class="form-control" value="'+obj+'" name="quanti_var['+attr_new+'][main]" />';
make+='<input type="number" class="form-control required_validation_for_product" name="quanti_var['+attr_new+'][value]" placeholder="Enter value"/>';
 make+='</div>';  */
    
    attr_new++;
    }   
    else
    {
        
       
     htm+=html_array[check]; 
   //  make+=html_array1[check];  
  //  attr_new++;
    $ty++;
    
    } 
        }); 
      jQuery(".Add_cost_quantity").empty('');                  
    $('.Add_cost_quantity').append(htm); 
              calculatevariantData();
    });
            
            
 function calculatevariantData(){                   
    $(".tr_class_var").each(function(){ 
        var checkval1 = $('.ware_house').val();
        var $that = $(this);
        var stock_var =[]; 
        var array_temp =[];  
        var already_Array  =[];
        var productwarehouse_data=[];
          $(".qty_val").each(function(index)
                {
           var label = $(this).find('label').text();
           var id_var =  $(this).find('input[type="hidden"]').val();
           var values_var =  $(this).find('input[type="number"]').val();  
             productwarehouse_data.push({'id':id_var,'val':values_var,'name':label}); 
          });
        if(productwarehouse_data.length>0){
         if($that.find('.stock_var_Class').val())
            {
                var productwarehouse_dataAlready = $that.find('.stock_var_Class').val();
                productwarehouse_dataAlready = JSON.parse(productwarehouse_dataAlready);
                if(productwarehouse_dataAlready.length>0){
                  $.each(productwarehouse_dataAlready, function(idx,obj) {
                      var alreadyid=obj.id;
                       var alreadyid_val=obj.val;
                          for (var i = 0; i < productwarehouse_data.length; i++) {                   
                            if(productwarehouse_data[i].id==alreadyid){ 
                                productwarehouse_data[i].val = alreadyid_val;
                            }
                        }    
                  });
                    
                    
                    
                    
             
                    $that.find('.stock_var_Class').val(JSON.stringify(productwarehouse_data));
                 
                }
                else{
                     
                    $that.find('.stock_var_Class').val(JSON.stringify(productwarehouse_data));
                }
            }
            else{
                 
                $that.find('.stock_var_Class').val(JSON.stringify(productwarehouse_data));
            }
        }
        else{
        $that.find('.stock_var_Class').val('');
        }
        
        // console.log(productwarehouse_data);
     
       });
     }  
     
     //calculatevariantData();
    
    // });
      
       
        var currentvariantData;
       $(document).on("click",'.add_stock_btn',function()
       {
         
       var count1='';
      
       // var checkval = $('.ware_house').val();
        currentvariantData=$(this).siblings('.stock_var_Class');
      var checkval = $(this).siblings('.stock_var_Class').val();
          
        var make='';
           if(checkval){
           
         $.each(JSON.parse(checkval), function(idx,obj) {       
        
             make+='<div class="qty_val_var" data-val="">';   
             make+='<label class="col-md-8">'+obj.name+'</label>';
            
           // make+='<input type="hidden" class="form-control" value="'+obj.id+'" data-warehouse="" name="quanti_var['+count1+'][main]" />';
            make+='<input type="number" min="0" pattern= "[0-9]" class="form-control" id="addvariantsdatafieldqty'+obj.id+'" data-id="'+obj.id+'" name="quanti_var['+count1+'][value]"  value="'+obj.val+'" placeholder="Enter value" required=""/>';
             make+='</div>';  
           
            count1++;
             });
            make+='<button class="btn btn-info add_stock" type="submit">Save stock</button>';
           // console.log(make); 
           
          }
           else{
            make="<p>Variants data not available  </p>"
           }
        $('.Add_cost_quantity_of_variant').empty(''); 
      $('.Add_cost_quantity_of_variant').append(make);  
       
             
       });
            
            $('form#addstockvariantsform').submit(function(e){
                e.preventDefault(); 
                var variantdataupdate =currentvariantData.val();
                console.log(variantdataupdate);
                variantdataupdate = JSON.parse(variantdataupdate)
                for (var i = 0; i < variantdataupdate.length; i++) {
                     
                    // console.log(variantdataupdate[i].id+'-'+variantdataupdate[i].val);
                    var updatespecficfiledvariant = $(this).find('#addvariantsdatafieldqty'+variantdataupdate[i].id).val();
                     
                    if(updatespecficfiledvariant){
                        
                            variantdataupdate[i].val = updatespecficfiledvariant;                           
                            
                    }
                 
                }
                currentvariantData.val(JSON.stringify(variantdataupdate));
                $(this).find('.Add_cost_quantity_of_variant').html('');
                console.log(JSON.stringify(variantdataupdate));
                $('#add_stock_data').modal('hide');
            });
            

     $(document).on('click','.barcode_generate',function()
     {
        var $that = $(this);
       var dataString = "flag=fetchmediaaudio";
              console.log($that.siblings('input').attr('name'));
      
        jQuery.ajax({
          type: "GET",
          url: "<?php echo base_url(); ?>" + "Product/set_barcode",
        
        
          processData:false,
          cacheData:false,
          success: function(data) {
            
            console.log(data);
           
           
           $that.siblings('input').val(data);
           
            $that.siblings('.barcode_images').attr("src","<?php echo base_url();?>images/barcode/"+data);
           
             $that.siblings('.message_text').text('');
            }
            });
            
        
     });
     
     
     $(document).on("click",".add_nm",function()
     {
     
      
        var mn =0;
       var nv =  $(this).parents(".modal-content").find("input").val();
     
     
   var option_add3 = '<tr class="tr_class_var">'
              +'<td><input  type="text" name="new_value['+indexing+'][one'+mn+']"  value="'+nv+'" class="form-control" ></td>'
           +'<td><input  type="text"  onkeyup="checkINt(this);" value="'+$("#sku").val()+'" name="new_sku[]" class="form-control sku_new required_validation_for_product" ></td>'
            +'<td><input  type="text" value="'+$("#titles").val()+'" name="new_title[]" class="form-control title_new required_validation_for_product" ></td>'
            +'<td><input  type="text"  onkeyup="checkDec(this);" value="'+$("#pricess").val()+'" name="new_price[]" class="form-control price_new required_validation_for_product" ></td>'
            +'<td><input type="file" name="new_image[]"/></td>'
            +'<td><button type="button" data-target="#add_stock_data" data-toggle="modal"  class="btn btn-info add_stock_btn">Add stock</button><input type="hidden" class="stock_var_Class" name="new_stock[]"/></td>'
           +'<td class="delete-variant text-danger"><i class="fa fa-trash"></i></td>'
          +'</tr>';
          
           $(".variant_tbody").append(option_add3);
           indexing++;
           mn++;
           });
           

        $(document).on('submit','#Addproduct',function(c){
           
            
                               var rep_image_val='';
                 $(this).find(".required_validation_for_product").each(function()
               {
                       
                        var val22 = jQuery(this).val();
                        
                        if (!val22)
                        {
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                        
                                
                        }
                });
                

              $(this).find(".barcode_input").each(function()
               {
                          
                        var val56 = jQuery(this).val();
                              
            
           
                        if (!val56)
                        {
                            
                                rep_image_val = 'error form';
                                $(this).css("border-color","red");
                              
                                $(this).siblings('.message_text').text('Please generate  barcode');
                        
                                
                        }
                });
                
                
                    if(!jQuery('.radio_valid:checked').val())
                {
                   rep_image_val = 'error form';
                                $(this).css("border-color","red");  
                }
                
                
           $(this).find(".first_image").each(function()
                {
                console.log("gf");   
                var val_image = jQuery(this).val(); 
                
              if (!val_image)
                {
                  var hass =   $(this).parents(".images_preview_row").find(".images_preview").attr("src");
                      console.log( $(this).parents(".images_preview_row").find(".images_preview")); 
                       if(!hass)
                       {
                        rep_image_val = 'error form';
                        $(this).css("border-color","red");
                        $(this).css("border-width","1px");
                        }
                      }  
                  });
                  $('.required_validation_for_product,.validated_numeric,.validated_integer').on('keyup blur',function()
                                {
                                $(this).css("border-color","#ccc");
                                        $(this).siblings('.message_text').text('');
                                });
                                
                               
                        
                
   
                  
                                
                                
                            if(rep_image_val)
                {
                        c.preventDefault();
                        return false;
                }
                
             
                // else if($('.checkif:checked').length==0)
                // {
                //     alert("Please select any payment method");
                //     c.preventDefault();
                //         return false;
                // }
    
                
            
          });
          
                $(".first_image").on("change",function()
          {
                $(this).css("border-color","ccc");
                $(this).css("border-width","0px");
          });
          
          // add variant option 
          
          var op  = 1;
             
          $(document).on("click",".add_option",function()
          { 
            
             
            var htm ="";
            htm +='<tr class="table_tr addvariants_row houdin_addvariants_box_row">' 
                +'<td><div class="input-group">'
              +'<input type="text" class="form-control save_option_name_text" placeholder="option name like color, size, size" max-length="100" required="">'
              +'<span class="input-group-btn">'
                +'<button style="margin:0px!important;" class="btn btn-success save_option_name" type="submit"><i class="fa fa-check"></i></button>'
              +'</span>'
            +'</div></td>'
            +'<td class="delete_option_append"><div class="input-group">'
              +'<input type="text" class="form-control option_val_text" placeholder="option values" max-length="100" required="">'
              +'<span class="input-group-btn">'
                +'<button style="margin:0px!important;" class="btn btn-success save_option_val" type="submit"><i class="fa fa-check"></i></button>'
              +'</span>'
            +'</div></td>' 
            +'<td class="remove_option" style="padding-left: 10px"><span class="input-group-btn">'
                +'<button style="margin:0px!important;" class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>'
              +'</span></td>'
              +'</tr>';
              
            $(".append_table").append(htm);
             op++;
             if(op==3)
            {
               $(".add_option").css("display","none"); 
            }
            
            
            
          });
          
          
                    
          $(document).on("click",".remove_option",function()
          {
            
            
          
              
            $(this).parents(".table_tr").remove();
             op--;
             if(op!=3)
            {
               $(".add_option").css("display","block"); 
            }
            
               houdin_newaddvariants_box();
           
            
          });
          
          var cusm = 0;
           var indexing = "<?php echo $indexing; ?>";
            var hudinVariantdata;
            function houdin_newaddvariants_box(){
            
                    hudinVariantdata=[];
                    $('.houdin_addvariants_box .houdin_addvariants_box_row').each(function(){
                    var $optiontitle = $(this).find('.save_option_name_text').attr('data-name');
                    if($optiontitle){
                        var added_option_value=[];
                         $(this).find('.added_option_btn').each(function(){
                            added_option_value.push($(this).attr('data-name'));
                        });  
                        // hudinVariantdata[$optiontitle]=added_option_value;                   
                     
                        var variantsOptionRow=[];
                        variantsOptionRow[$optiontitle]=added_option_value;
                          
                        hudinVariantdata.push(variantsOptionRow);
                         
                        // console.log(hudinVariantdata,);
                     }
                        else{
                            alert('Option now available');
                        }
                    });
                    //console.log(hudinVariantdata);
            
            }houdin_newaddvariants_box();
          
          $(document).on("click",".save_option_val",function()
          { 
            var optionRowTitle = $(this).parents('.houdin_addvariants_box_row').find('.save_option_name_text').attr('data-name');
            var optionValue = $(this).parents('.houdin_addvariants_box_row').find('.option_val_text').val();
             if(optionRowTitle){
                 if(optionValue){ 
                    // alert(hudinVariantdata.length);
                 var checkalreadymatchoptionvalue=true;
                    if(hudinVariantdata.length>0){ 
                        $.each(hudinVariantdata,function(count,optionrow){
                        // console.log(optionrow);  
                         var currentRowData=optionrow[optionRowTitle]; 
                        
                         if(currentRowData){ 
                         $.each(currentRowData,function(index,obj){
                            if(obj==optionValue)
                            {
                                checkalreadymatchoptionvalue=false;  
                            }
                         });
                        } 
                        });
                    }
                     if(checkalreadymatchoptionvalue){                      
                         alert('new option value insert');
                           var options = '<div class="btn-group div_group added_option_btn" data-name="'+optionValue+'">'
                            +'<button type="button" class="btn btn-primary btn-sm"><span class="default-text">'+optionValue+'</span>&nbsp;&nbsp;<span class="remove_option_value"><i class="fa fa-trash"></i></span></button>';
                            +'</div>';
                         $(this).parents('.houdin_addvariants_box_row').find('.delete_option_append').append(options);
                     }
                     else{
                        alert('Already have same '+optionRowTitle+ ' option value');    
                     }
                 // alert(optionValue);
                 }
                 else{
                    alert("Please write option value ");
                 }
             }
              else{
                alert("Please write option name  first ");
              }
              
              
              
            houdin_newaddvariants_box()
            calculatevariantData();
             console.log(hudinVariantdata);
              $('#data').val(hudinVariantdata);
            if(hudinVariantdata.length>0)
            {
                var vaiantsOptiontitle='';
                var indexing=0;
                var ind=0;
                $.each(hudinVariantdata,function(index,data){
                    var titlecOption = Object.getOwnPropertyNames(data)[1];
                    console.log(titlecOption);
                    vaiantsOptiontitle+='<th data-col="'+titlecOption+'">'+titlecOption+'</th>';
                    if(data.length)
                    {
                    
                    }
                });
                console.log(vaiantsOptiontitle);
             var option_add ='<div class="card-box has_cusm" style="width: 100%;">' 
                            +'<h4 class="clearfix"><b class="pull-left">Other Variants</b>'
                            +'<small class="btn btn-link pull-right">Edit Options</small>'
                            +'</h4>'
                            +'<table id="variants" class="table table-stripped">'
                            +'<thead class="variant_table_is">'
                            +'<tr class="tr_head">'
                            +'<th>Default</th>'
                            +vaiantsOptiontitle 
                            +'<th>SKU</th>'
                            +'<th>Title</th>'
                            +'<th>Price</th>'
                            +'<th>Image</th>'
                            +'<th>Stock</th>'                           
                            +'<th>Delete</th>'
                            +'</tr>'
                            +'</thead>'
                            +'<tbody class="variant_tbody">'
                            +'<tr class="tr_class_var">'
                            +'<td><input type="radio"  name="default_variant" value="radio['+indexing+']" class="variant_option" checked></td>'
                            +vaiantsOptiontitle
                            //+'<td><input readonly type="text"  name="new_value['+indexing+'][one'+ind+']" value="'+optionValue+'" class="form-control required_validation_for_product" ></td>'
                            +'<td><input  type="text" readonly  onkeyup="checkINt(this);" value="'+$("#sku").val()+'"  name="new_sku[]" class="form-control sku_new required_validation_for_product" ></td>'
                            +'<td><input  type="text" readonly value="title"  name="new_title[]" class="form-control title_new required_validation_for_product" ></td>'
                            +'<td><input  type="text"  readonly onkeyup="checkDec(this);" value="'+$("#pricess").val()+'" name="new_price[]" class="form-control price_new required_validation_for_product" ></td>'
                            +'<td><input type="file" name="new_image[]"/></td>'
                            +'<td><button type="button" data-target="#add_stock_data" data-toggle="modal"  class="btn btn-info add_stock_btn">Add stock</button><input type="hidden" class="stock_var_Class" name="new_stock[]"/></td>'
                            +'<td class="delete-variant text-danger"><i class="fa fa-trash"></i></td>'
                            +'</tr>'
                            +'</tbody></table></div>';
                            
                 $(".combinations").html(option_add);        
                    
            }
              else{
              
              }
                /* 
           var has_val = $( ".combinations" ).children("div").hasClass( "has_cusm" );
            var ind = 0
            var name = $(this).parents(".table_tr").find(".save_option_name_text").val();
            var valur = $(this).parents(".table_tr").find(".option_val_text").val();
            if(name && valur)
            {
                
                
            
                if(!has_val)
                {

                
                  
             var option_add = '<div class="card-box has_cusm" style="width: 100%;">' 
                                +'<h4 class="clearfix"><b class="pull-left">Other Variants</b>'
                               +'<small class="btn btn-link pull-right">Edit Options</small>'
                                +'</h4>'
                              +'<table id="variants" class="table table-stripped">'
                              +'<thead class="variant_table_is">'
                              +'<tr class="tr_head">'
                              +'<th>Default</th>'
                              +'<th>'+name+'<input type="hidden" value="'+name+'" name="new_var_name[]"></th>'
                              +'<th>SKU</th>'
                              +'<th>Title</th>'
                              +'<th>Price</th>'
                              +'<th>Image</th>'
                              +'<th>Stock</th>'
                              +'<th>Delete</th>'
                              +'</tr>'
                              +'</thead><tbody class="variant_tbody">'
                              +'<tr class="tr_class_var">'
                              +'<td>'
              +'<input type="radio"  name="default_variant" value="radio['+indexing+']" class="variant_option" checked>'
            +'</td>'
             +'<td><input readonly type="text"  name="new_value['+indexing+'][one'+ind+']" value="'+valur+'" class="form-control required_validation_for_product" ></td>'
           +'<td><input  type="text" readonly  onkeyup="checkINt(this);" value="'+$("#sku").val()+'"  name="new_sku[]" class="form-control sku_new required_validation_for_product" ></td>'
            +'<td><input  type="text" readonly value="'+$("#titles").val()+'"  name="new_title[]" class="form-control title_new required_validation_for_product" ></td>'
            +'<td><input  type="text"  readonly onkeyup="checkDec(this);" value="'+$("#pricess").val()+'" name="new_price[]" class="form-control price_new required_validation_for_product" ></td>'
           +'<td><input type="file" name="new_image[]"/></td>'
            +'<td><button type="button" data-target="#add_stock_data" data-toggle="modal"  class="btn btn-info add_stock_btn">Add stock</button><input type="hidden" class="stock_var_Class" name="new_stock[]"/></td>'
           +'<td class="delete-variant text-danger"><i class="fa fa-trash"></i></td>'
          +'</tr></tbody></table></div>';
          
          $(".combinations").append(option_add);
            
              cusm++;      
             }
             else
             {
                
               var option_add2 = '<tr class="tr_class_var">'
                              +'<td>'
              +'<input type="radio"  name="default_variant" value="radio['+indexing+']" class="variant_option">'
            +'</td>'
             +'<td><input  type="text" name="new_value['+indexing+'][one'+ind+']"  value="'+valur+'" class="form-control" ></td>'
           +'<td><input  type="text"  onkeyup="checkINt(this);" value="'+$("#sku").val()+'" name="new_sku[]" class="form-control sku_new required_validation_for_product" ></td>'
            +'<td><input  type="text" value="'+$("#titles").val()+'" name="new_title[]" class="form-control title_new required_validation_for_product" ></td>'
            +'<td><input  type="text"  onkeyup="checkDec(this);" value="'+$("#pricess").val()+'" name="new_price[]" class="form-control price_new required_validation_for_product" ></td>'
           +'<td><input type="file" name="new_image[]"/></td>'
            +'<td><button type="button" data-target="#add_stock_data" data-toggle="modal"  class="btn btn-info add_stock_btn">Add stock</button><input type="hidden" class="stock_var_Class" name="new_stock[]"/></td>'
           +'<td class="delete-variant text-danger"><i class="fa fa-trash"></i></td>'
          +'</tr>';
          
           $(".variant_tbody").append(option_add2);
             }
             indexing++;
             ind++;
               $(this).parents(".table_tr").find(".option_val_text").val('');     
                
            
            }
            else
            {
                alert("please select option name and value first");
            }
            */
            
           
              
              
          });
             
              $(document).on("click",".remove_option_value",function()
              {
                $(this).parents(".div_group").remove();
                 houdin_newaddvariants_box();
              });
          

                $(document).on('change','.variant_option', function() {

                    $(".tr_class_var").find("input").prop( "readonly", false );

                   alert($(this).parents(".tr_class_var").find(".sku_new").val());
                     $(this).parents(".tr_class_var").find(".sku_new").val($("#sku").val());
                     $(this).parents(".tr_class_var").find(".title_new").val($("#titles").val());
                     $(this).parents(".tr_class_var").find(".price_new").val($("#pricess").val());
                    // $(this).parents(".tr_class_var").find(".stock_new").val($("#stock").val());
                         $(this).parents(".tr_class_var").find("input[type='text'],input[type='number']").prop( "readonly", true );

                 //  alert($('input[name=radioName]:checked', '#myForm').val()); 
                });
          
    $(document).on("click",".save_option_name",function()
          {
            
             var name1 = $(this).parents(".table_tr").find(".save_option_name_text").val();
            if(!name1)
            {
                alert("please fill the name first");
            }
            else
            {
                 
                 var checkalreadymatchoptionvalue=true;
                    if(hudinVariantdata.length>0){ 
                        $.each(hudinVariantdata,function(count,optionrow){
                             if(optionrow[name1]){
                                checkalreadymatchoptionvalue=false;
                             }
                        });
                    }
                 
                     if(!checkalreadymatchoptionvalue){
                         alert('Already ' + name1 + ' option available');
                     }
                    else{               
                        $(this).parents(".table_tr").find(".save_option_name_text").attr('data-name',name1);
                        alert(name1 +" option name saved");
                        houdin_newaddvariants_box();
                    }
            }
            
            });
          
          
      $(document).on("click",".delete-variant",function()
      {
        $(this).parents(".tr_class_var").remove();
        $("#delet_id").val($(this).attr("data-id"));
        $("#AddId").submit();

        
      });    
          
          });
          
          $('.images_class').on('change',function()
          {
            console.log($(this)[0].files[0]);
            var file1    = $(this)[0];
                var file    = file1.files[0];
                  var reader  = new FileReader();
                
                  reader.onloadend = function () {
              
                   
                    $(file1).parents('.images_preview_row').find('.images_preview').attr('src',reader.result);
                  
                  }
                
                  if (file) {
                    reader.readAsDataURL(file);
                  } else {
                    $(this).parents('.images_preview_row').find('.images_preview').attr('src','');
                  
                  }
            

            
          });
          
function previewFile(div) {
    var x = document.getElementById(div);
  var preview = x.querySelector('img');
  var file    = document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
 }
          </script>

          <script>
$(document).ready(function(){

    $(".select2").select2();
  $(".click_show").click(function(){
      $(".hide_div").show();
      $(".click_show").hide();
  });
  $(".click_hide").click(function(){
      $(".hide_div").hide();
      $(".click_show").show();
  });
});


function arrayRemove(arr, value) {

   return arr.filter(function(ele){
       return ele != value;
   });

}
</script>

    <script>
  tinymce.init({
    selector: '#mytextarea'
  });
  </script>
  <script>
 $(document).ready(function(){
        $(".select2").select2();

        });
        // js for description box
$(document).on('click','.show_description',function (){
    $('.descption_box').removeClass('d-none');
    $(this).addClass('d-none');
    $('.hide_description').removeClass('d-none');
    $('.descption_box').addClass('d-none');
    CKEDITOR.replace( 'editor1' );
});
$(document).on('click','.hide_description',function (){
    $('.descption_box').removeClass('d-none');
    $(this).addClass('d-none');
    $('.show_description').removeClass('d-none');
    CKEDITOR.instances['editor1'].destroy();
    $('.descption_box').addClass('d-none');
});
        </script>
        <script type="text/javascript">
    // CKEDITOR.replace( 'editor1' );
    CKEDITOR.replace( 'editor2' );
</script>
<input type="hidden" id="data"/>