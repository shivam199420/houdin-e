        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <style type="text/css">
        .zoom img{

        transition-duration: 5s;
        margin: 0 auto;
        }
        img {
        vertical-align: middle;
        height: 40px;
        width: auto; 
        } 

        .zoom {
        transition: all 1s;
        -ms-transition: all 1s;
        -webkit-transition: all 1s;
        margin-top: 0px;
        padding-top: 0px;

        }
        .zoom:hover {
        -ms-transform: scale(2); /* IE 9 */
        -webkit-transform: scale(2); /* Safari 3-8 */
        transform: scale(2); 
        margin-left: 40px;
        }
        </style>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">
        <!--START Page-Title -->
        <div class="row">
  
        <div class="col-md-8">
        <h4 class="page-title">Low Inventory</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url(); ?>inventory/Inventorylanding">Inventory</a></li>
        <li class="active">Low Inventory</li>
        </ol>
        </div>
        <div class="col-md-4">
        </div>

        </div>
        <!--END Page-Title -->  

        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }


        ?>
        </div>
        </div>


        <div class="row m-t-20">
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Total_Products.png">
        <h2 class="m-0 text-dark counter font-600"><?=$totalproduct;?></h2>
        <div class="text-muted m-t-5">Total product</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Out_of_Stock.png">
        <h2 class="m-0 text-dark counter font-600"><?=$outofstock;?></h2>
        <div class="text-muted m-t-5">Total out of stock product</div>
        </div>
        </div>
        <div class="col-lg-4 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
        <img src="<?php echo base_url(); ?>assets/images/allpageicon/Low_Stock.png">
        <h2 class="m-0 text-dark counter font-600"><?=$lowstock;?></h2>
        <div class="text-muted m-t-5">Total low stock product</div>
        </div>
        </div>
        <!--<div style="float:right;margin-right:10px">
        <button type="button" class="btn btn-info m-b-20">Generate PDF</button>
        <button type="button" class="btn btn-success m-b-20">Generate CSV</button>

        </div>-->

        </div>



        <div class="row">
        <div class="col-md-12">

        <div class="card-box table-responsive">
        <div class="btn-group pull-right m-t-10 m-b-20">                 
        <a href="<?php echo base_url(); ?>inventory/reorderproduct" class="btn btn-default m-r-5" title="Reorder"><i class="fa fa-reorder"></i></a>
        <a href="<?php echo base_url(); ?>inventory/generatepdflowinventory" class="btn btn-default m-r-5" title="Export To PDF"><i class="fa fa-file-pdf-o"></i></a>
        </div>
        <table id="lowinventory"  style="width:100%" class="table table-striped table-bordered table_shop_custom display">
        <thead>
        <tr>                    
        <th style="width: 15%">Order</th>
        <th>Item Name</th>
        <th>Stock</th>


        </tr>

        </thead>
        <tbody>

        </tbody>
        </table>
        </div>
        </div>
        </div>


        </div>
        </div>
        </div>
        <!--Delete-->

        <div id="delete_order" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <form method="post">
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Delete Order</h4>

        </div>

        <div class="modal-body">



        <div class="row">
        <div class="col-md-12">
        <h4><b>Do you really want to Delete this order ?</b></h4>
        </div>
        </div>
        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info" name="" value="Delete">

        </div>

        </div>
        </form>
        </div>

        </div>

        <!--change Status-->
        <div id="Change-status" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
        <form method="post">
        <div class="modal-content">

        <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h4 class="modal-title">Change Status</h4>

        </div>

        <div class="modal-body">



        <div class="row">
        <div class="col-md-12">

        <div class="form-group no-margin">

        <label for="field-7" class="control-label">Change Status</label>

        <select class="form-control " name=""><option value="">Choose Status</option><option value="1">Active</option><option value="0">Deactive</option><option value="2">Block</option></select>

        </div>

        </div>
        </div>





        </div>

        <div class="modal-footer">

        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-info " name="" value="Update Status">

        </div>

        </div>
        </form>
        </div>

        </div>

        <?php $this->load->view('Template/footer.php') ?>
        <script>
        $(document).ready(function() {
        $('#lowinventory').DataTable( {
        "ajax": "<?php echo base_url(); ?>inventory/lowinventory_api"
        } );
        } );

        </script>
