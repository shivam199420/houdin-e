<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Adminlogs extends CI_Controller 
{
        function __construct()
    {
        parent::__construct();
        if($this->session->userdata('userAuth') == "") 
        {
            $this->load->helper('url');
            redirect(base_url(), 'refresh');
        }
        if($this->session->userdata('role') != 0)
        {
            $this->load->helper('access');
            $getData = access($this->session->userdata('department'));
            if(!in_array('log',$getData))
            {
                $this->load->helper('url');
                redirect(base_url()."dashboard", 'refresh');
            }
        }
        $this->load->model('AdminLog');
		$this->load->helper('url');
		$this->load->library('pagination');
        $this->perPage = 10;
        
        
       } 
    public function index()
    {

         $Alllog= array();
        //get rows count
        $conditions['returnType'] = 'count';
        $totalRec = $this->AdminLog->getAllLogs($conditions);
        //pagination config
        $config['base_url']    = base_url().'Adminlogs/';
        $config['uri_segment'] = 2;
        $config['total_rows']  = $totalRec;
        $config['per_page']    = 1;
        //styling
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //initialize pagination library
        $this->pagination->initialize($config);
        //define offset
        echo $page = $this->uri->segment(2);
        $offset = !$page?0:$page;
        //get rows
        $conditions['returnType'] = '';
        $conditions['start'] = $offset; 
        $conditions['limit'] = $this->perPage;
        $Alllog['log'] = $this->AdminLog->getAllLogs($conditions);
        
      //  print_r($Alllog);
        
        $this->load->view('adminlogs',$Alllog);
    }
}
