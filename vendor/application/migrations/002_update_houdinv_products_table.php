<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_houdinv_products_table extends CI_Migration {
    
    /**
     * construct function
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }
    
    public function up()
    {
        $fields = array(
            'houdin_products_category' => [
                    'name' => 'houdin_products_category',
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => true,
                    'default' => null
            ],
            'houdin_products_sub_category_level_1' => [
                'name' => 'houdin_products_sub_category_level_1',
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => true,
                'default' => null
        ],
        'houdin_products_sub_category_level_2' => [
            'name' => 'houdin_products_sub_category_level_2',
            'type' => 'VARCHAR',
            'constraint' => '255',
            'null' => true,
            'default' => null
        ],
        'houdin_products_short_desc' => [
            'name' => 'houdin_products_short_desc',
            'type' => 'text',
            'null' => true,
            'default' => null
        ],
        'houdin_products_desc' => [
            'name' => 'houdin_products_desc',
            'type' => 'text',
            'null' => true,
            'default' => null
        ],
        'houdin_product_type' => [
            'name' => 'houdin_product_type',
            'type' => 'INT',
            'constraint' => '11',
            'null' => true,
            'default' => null
        ],
        'houdin_product_type_attr_value' => [
            'name' => 'houdin_product_type_attr_value',
            'type' => 'VARCHAR',
            'constraint' => '255',
            'null' => true,
            'default' => null
        ],
        'houdinv_products_unit' => [
            'name' => 'houdinv_products_unit',
            'type' => 'VARCHAR',
            'constraint' => '50',
            'null' => true,
            'default' => null
        ],
        'houdinv_products_batch_number' => [
            'name' => 'houdinv_products_batch_number',
            'type' => 'VARCHAR',
            'constraint' => '100',
            'null' => true,
            'default' => null
        ],
        'houdin_products_barcode_image' => [
            'name' => 'houdin_products_barcode_image',
            'type' => 'VARCHAR',
            'constraint' => '100',
            'null' => true,
            'default' => null
        ],
    );
    $this->dbforge->modify_column('houdinv_products', $fields);
    }

    public function down()
    {
    }
}