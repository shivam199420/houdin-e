<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!--START Page-Title -->
            <div class="row">
             
                <div class="col-md-8">
                   <h4 class="page-title">App Store Setting</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a href="javascript:void(0);">App CMS</a></li>
                  <li class="active">App Store Setting</li>
                  </ol>
                  </div>
                  <!-- <div class="col-md-4">
                  <form role="search" class="navbar-left app-search pull-left custom_search_all">
                           <input type="text" placeholder="Search..." class="form-control">
                           <a href=""><i class="fa fa-search"></i></a>
                      </form></div> -->

            </div>
           <!--END Page-Title -->
<form action="" id="ShopInfoForm" method="post" accept-charset="utf-8">

        <div class="row">
        <div class="col-sm-12 m-t-20">
        <div class="card-box">
          <div class="form-group">
            <label>Shop Title</label>
            <input type="text" class="form-control " value="" name="" placeholder="My Store">
          </div>
          <div class="form-group">
            <label>Business Name</label>
          <input type="text" class="form-control " value="" name="" placeholder="My Store">
          </div>
          <div class="form-group">
            <label>Shop Address</label>
          <input type="text" value="" class="form-control " name="" placeholder="Default Shop Address">
          <p>If you have a physical store apart from thisvirtual store then add your shop address here</p>
          </div>
          <div class="form-group">
            <label>Shop Contact Info</label>
          <input type="text" value="" class="form-control" name="" placeholder="+91234567890">
          <p>Share your Store's contact information here</p>
          </div>
          <div class="form-group">
            <label>Shop communication email</label>
          <input type="text" class="form-control " value="" name="" placeholder="Please enter shop communication email address">
          <p style="color: red;" class="messagesd"></p>
          <p>Take the professional communication to a next levelwith your company's business email id. Example- hello@xyz.com</p>
          </div>

          <div class="form-group">
            <label>Shop order email</label>
          <input type="text" class="form-control " value="" name="" placeholder="Please enter shop order email address">
          <p style="color: red;" class="messagesd"></p>
          <p>Give instant solutions to customer's regarding orders and delivery with your shoporder email id. E.g. orders@xyz.com</p>
          </div>
          <div class="form-group">
            <label>Shop customer care email</label>
          <input type="text" class="form-control " value="" name="" placeholder="My Store">
         <p style="color: red;" class="messagesd"></p>
          <p>Help your customers regarding any product query by providing your customersupport email id here. E.g. support@xyz.com</p>
          </div>
          <div class="form-group">
            <label>Shop PAN</label>
          <input type="text" class="form-control" value="" name="">
          <p>Write your physical store's PAN number here (Optional)</p>
          </div>

          <div class="form-group">
            <label>Shop TIN</label>
            <input type="text" name="" value="" class="form-control">
          </div>
          <div class="form-group">
            <label>Shop CST</label>
            <input type="text" name="" value="" class="form-control">
          </div>
          <div class="form-group">
            <label>Shop VAT</label>
            <input type="text" name="" value="" class="form-control">
          </div>
          <div class="form-group">
            <label>Custom domain</label>
            <input type="text" name="" value="" class="form-control">
          </div>
          <div class="form-group">

            <input type="checkbox" value="" name="" checked="">&nbsp;SSL
          </div>


        </div>
        <!--pricing-->

        <!--inventory-->

      </div>

        <div class="col-md-12">
          <button type="submit" value="" name="" class="btn btn-info">Save</button>
        </div>
        </div>
         </form>
      </div>
    </div>
  </div>






























<?php $this->load->view('Template/footer.php') ?>
