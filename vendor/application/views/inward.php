        <?php $this->load->view('Template/header.php') ?>
        <?php $this->load->view('Template/sidebar.php') ?>
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container">
         <!--START Page-Title -->
        <div class="row">
       
        <div class="col-md-8">
        <h4 class="page-title">Inward</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
       
        <li><a href="<?php echo base_url(); ?>purchase">Purchase</a></li>
        <li class="active">Inward</li>
        </ol>
        </div>
        
        </div>
        <div class="row">
        <div class="col-sm-12">
        <?php 
        if($this->session->flashdata('success'))
        {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        }
        else if($this->session->flashdata('error'))
        {
        echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
        }
        ?>
        </div>
        </div>                  
        <div class="row m-t-20">
        <div class="col-md-12">
        <div class="card-box table-responsive">
        <div class="alert alert-danger showErrorData" style="display:none">Please Fill all the checked row</div>
        <?php echo form_open(base_url( 'Purchase/addInwardata/'.$this->uri->segment('3').'' ), array( 'id' => 'updateinwardData', 'method'=>'post' ));?>
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr>
        <th><input type="checkbox" class="masterPurcahseCheck"></th>
        <th>Product Name</th>
        <th>Quantity ordered</th>
        <th>Quantity received</th>
        <th>SP</th>
        <th>CP</th>
        <th>MRP</th>
        <th>Total Payment</th>
        <th>Payment</th>
        <th>Payment Method</th>
        <th>Paid Amount</th>
        <th>Outlet</th>
        </tr>
        </thead>
        <tbody>
            <input type="hidden" name="purchaseId" value="<?php echo $this->uri->segment('3'); ?>"/>
            <?php 
            $finalTransactionAmount = 0;
            foreach($transactionAmount as $transactionAmountData)
            {
                $finalTransactionAmount = $finalTransactionAmount+$transactionAmountData->houdinv_transaction_amount;
            } 
            ?>
            <input type="hidden" name="transactionAmount" value="<?php echo $finalTransactionAmount; ?>"/>
        <?php 
        foreach($purcahseProduct as $purcahseProductList)
        {
            $getChildArray = $purcahseProductList['child'][0];
        ?>
        <tr class="parentRowData">
        <td><input data-variant="<?php echo $purcahseProductList['main']['variantId'] ?>" data-product="<?php echo $purcahseProductList['main']['productId'] ?>" type="checkbox" <?php if(count($getChildArray) > 0) { ?> checked="checked" <?php  } ?> class="childPurchaseCheck"></td>
        <td><?php echo $purcahseProductList['main']['productName'] ?></td>
        <td><?php echo $purcahseProductList['main']['quantity'] ?></td>
        <input type="hidden" name="purchaseProductId[]" value="<?php echo $purcahseProductList['main']['purchaseProductId'] ?>" />
        <input type="hidden" class="totalOrderedQuantity getTotalQuantity" value="<?php echo $purcahseProductList['main']['quantity'] ?>" name="totalOrderedQuantity[]"/>
        <input type="hidden" class="mainProductId" <?php if(count($getChildArray) > 0) { ?> value="<?php echo $getChildArray->houdinv_purchase_products_inward_purchase_product_id ?>" <?php  } ?> name="mainProductId[]"/>
        <input type="hidden" class="mainVariantId" <?php if(count($getChildArray) > 0) { ?> value="<?php echo $getChildArray->houdinv_purchase_products_inward_variant_id ?>" <?php  } ?> name="mainVariantId[]"/>
        <td><input type="text" name="quantityRecieved[]" data-max="<?php echo $purcahseProductList['main']['quantity'] ?>" <?php if(!count($getChildArray) > 0) { ?> disabled="disabled" <?php } ?> <?php if(count($getChildArray) > 0) { ?> value="<?php echo $getChildArray->houdinv_purchase_products_inward_quantity_recieved ?>" <?php } ?> class="form-control subClassData insertDataValue getQuantityRecieved name_validation number_validation quantityRecieved"/></td>
        <td><input type="text" name="salePrice[]" class="form-control subClassData insertDataValue name_validation number_validation" <?php if(!count($getChildArray) > 0) { ?> disabled="disabled" <?php } ?> <?php if(count($getChildArray) > 0) { ?> value="<?php echo $getChildArray->houdinv_purchase_products_inward_sale_price ?>" <?php } ?> /></td>
        <td><input type="text" name="costPrice[]" class="form-control subClassData getCostPriceData insertDataValue name_validation number_validation" <?php if(!count($getChildArray) > 0) { ?> disabled="disabled" <?php } ?> <?php if(count($getChildArray) > 0) { ?> value="<?php echo $getChildArray->houdinv_purchase_products_inward_cost_price ?>" <?php } ?> /></td>
        <td><input type="text" name="mainprice[]" class="form-control subClassData insertDataValue name_validation number_validation" <?php if(!count($getChildArray) > 0) { ?> disabled="disabled" <?php } ?> <?php if(count($getChildArray) > 0) { ?> value="<?php echo $getChildArray->houdinv_purchase_products_inward_retail_price ?>" <?php } ?> /></td>
        <td><input type="text" name="totalAmount[]" class="form-control setTotalPriceData" <?php if(count($getChildArray) > 0) { ?> value="<?php echo $getChildArray->houdinv_purchase_products_inward_total_amount ?>" <?php } ?> readonly="readonly" /></td>
        <td>
        <select name="paymentStatus[]" class="form-control subClassData insertDataValue" <?php if(!count($getChildArray) > 0) { ?> disabled="disabled" <?php } ?>>
        <option value="">Choose Payment</option>
        <option <?php if($getChildArray->houdinv_purchase_products_inward_payment_status == 'paid') { ?> selected="selected" <?php } ?> value="paid">Paid</option>
        <option <?php if($getChildArray->houdinv_purchase_products_inward_payment_status == 'not paid') { ?> selected="selected" <?php } ?> value="not paid">Not Paid</option>
        </select> 
        </td>
        <td>
        <select class="form-control subClassData insertDataValue" name="paymentMode[]">
            <option value="">Payment Mode</option>
            <option value="cash">Cash</option>
            <option value="checque">Checque</option>
            <option value="card">Card</option>
        </select>
    </td>
        <td><input type="text" name="amountPaid[]" class="form-control subClassData insertDataValue name_validation number_validation" <?php if(!count($getChildArray) > 0) { ?> disabled="disabled" <?php } ?> <?php if(count($getChildArray) > 0) { ?> value="<?php echo $getChildArray->houdinv_purchase_products_inward_amount_paid ?>" <?php } ?>/></td>
        <td>
        <select name="outletId[]" class="form-control subClassData insertDataValue name_validation" <?php if(!count($getChildArray) > 0) { ?> disabled="disabled" <?php } ?>>
        <option value="">Choose Outlet</option>
        <?php 
        foreach($outletList as $outletListData)
        {
        ?>
        <option <?php if($getChildArray->houdinv_purchase_products_inward_outlet_id == $outletListData->id) { ?> selected="selected" <?php  } ?> value="<?php echo $outletListData->id ?>"><?php echo $outletListData->w_name ?></option>
        <?php }
        ?>
        </select> 
        </td>
        </tr>    
        <?php }
        ?>
        </tbody>
        </table>
        <input type="submit" <?php if(!count($getChildArray) > 0) { ?> disabled="disabled" <?php } ?> class="btn btn-success pull-right subClassBtnData"value="Submit">
        <?php echo form_close(); ?>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <?php $this->load->view('Template/footer.php') ?>
       <script type="text/javascript">
       $(document).ready(function(){
        //    Master checkbox
           $(document).on('click','.masterPurcahseCheck',function(){
               if($(this).prop('checked') == true)
               {
                   $('.subClassBtnData').prop('disabled',false);
                   $('.subClassData').prop('disabled',false);
                   $('.childPurchaseCheck').prop('checked',true);
               }
               else
               {
                   $('.insertDataValue').val('');
                    $('.subClassData').prop('disabled',true);
                    $('.subClassBtnData').prop('disabled',true);
                    $('.childPurchaseCheck').prop('checked',false);
               }
            //    Each of child checkbox
            $('.childPurchaseCheck').each(function(){
                if($(this).prop('checked') == true)
                {   
                    $(this).parents('.parentRowData').find('.mainProductId').val($(this).attr('data-product'));
                    $(this).parents('.parentRowData').find('.mainVariantId').val($(this).attr('data-variant'));
                }
                else
                {
                    $(this).parents('.parentRowData').find('.mainProductId').val('');
                    $(this).parents('.parentRowData').find('.mainVariantId').val('');
                }
            })
           });
        //    Child checkbox
           $(document).on('click','.childPurchaseCheck',function(){
               var checkData = 0;
               var setdata = 0;
               if($(this).prop('checked') == true)
               {
                   $(this).parents('.parentRowData').find('.subClassData').prop('disabled',false);
               }
               else
               {
                    $(this).parents('.parentRowData').find('.subClassData').prop('disabled',true).val('');
               }
               $('.childPurchaseCheck').each(function(){
                   if($(this).prop('checked') == true)
                   {
                       $(this).parents('.parentRowData').find('.mainProductId').val($(this).attr('data-product'));
                       $(this).parents('.parentRowData').find('.mainVariantId').val($(this).attr('data-variant'));
                        setdata++;
                   }
                   else
                   {
                        $(this).parents('.parentRowData').find('.mainProductId').val('');
                        $(this).parents('.parentRowData').find('.mainVariantId').val('');
                        checkData++;
                   }
               });
               if(checkData == 0)
               {
                    $('.masterPurcahseCheck').prop('checked',true);
                    $('.subClassData').prop('disabled',true);
                    $('.subClassBtnData').prop('disabled',true);
               }
               else
               {
                $('.masterPurcahseCheck').prop('checked',false);
               }
               if(setdata != 0)
               {
                $('.subClassBtnData').prop('disabled',false);
               }
               else
               {
                $('.subClassBtnData').prop('disabled',true);
               }
           });
        //    quantity recieved validation
        $(document).on('keyup','.quantityRecieved',function(){
            if($(this).val() > parseInt($(this).attr('data-max')))
            {
                $(this).val($(this).attr('data-max'));
            }
        });
        // form submit validation
        $(document).on('submit','#updateinwardData',function(){
            var requiredData = 0;
            $('.childPurchaseCheck').each(function(){
                if($(this).prop('checked') == true)
                {
                    $(this).parents('.parentRowData').find('.subClassData').each(function(){
                        if($(this).val() == "")
                        {
                            requiredData++;
                        }
                    })
                }
            })
            if(requiredData == 0)
            {
                $('.showErrorData').hide();
                return true;
            }
            else
            {
                $('.showErrorData').show();
                return false;
            }
        });
        // set total amount paid data
        $(document).on('keyup','.getCostPriceData',function(){
            var getTotalQuantity = $(this).parents('.parentRowData').find('.getQuantityRecieved').val();
            if(getTotalQuantity)
            {
                var setToatalPricepaid = getTotalQuantity*$(this).val();
                $(this).parents('.parentRowData').find('.setTotalPriceData').val('');
                $(this).parents('.parentRowData').find('.setTotalPriceData').val(setToatalPricepaid);
            }
        });
        $(document).on('keyup','.getQuantityRecieved',function(){
            var getTotalCostPrice = $(this).parents('.parentRowData').find('.getCostPriceData').val();
            if(getTotalCostPrice)
            {
                var setToatalPricepaid = getTotalCostPrice*$(this).val();
                $(this).parents('.parentRowData').find('.setTotalPriceData').val('');
                $(this).parents('.parentRowData').find('.setTotalPriceData').val(setToatalPricepaid);
            }
        })
       });
       </script>

