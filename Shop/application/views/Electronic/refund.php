<?php 
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- Linking -->
    <div class="linking">
      <div class="container">
        <ol class="breadcrumb">
          <li class="active">Refund</li>
        </ol>
      </div>
    </div>
    
    <!-- About Sec -->
    <section class="about-sec padding-bottom-60">
      <div class="container"> 
        
      <?php echo $refund[0]->Cancellation_Refund_Policy ?>
      </div>
    </section>
    
     
  </div>
  <!-- End Content --> 
  <?php 
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer') 
  ?>