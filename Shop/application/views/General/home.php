<?php
$getShopName = getfolderName($this->session->userdata('shopName'));
$this->load->view(''.$getShopName.'/Template/header');
?>
<?php
$getCurrency = getShopCurrency();
$currencysymbol=$getCurrency[0]->houdin_users_currency;
?> 
<?php
$getData = stroeSetting($this->session->userdata('shopName'));
$cartFunction = $getData[0]->receieve_order_out;
?>
<style>
.f-right {
     float: left;
}
    .quotation
    {
        background: #333 none repeat scroll 0 0;
    color: #fff;
    font-size: 13px;
    font-weight: 500;
    /*right: -24px;*/
    margin: 0;
    padding: 1px 30px;
    position: absolute;
    text-align: left;
    text-transform: uppercase;
    top: 48px;
    /*transform: rotate(45deg);*/
    z-index: 99;
    }
    /*testimonial*/
        section {
    padding-top: 100px;
    padding-bottom: 100px;
}

.quote {
    color: rgba(249, 246, 246, 0.1);
    text-align: center;
    margin-bottom: 30px;
}

/*-------------------------------*/
/*    Carousel Fade Transition   */
/*-------------------------------*/

#fade-quote-carousel.carousel {
  padding-bottom: 60px;
}
#fade-quote-carousel.carousel .carousel-inner .item {
  opacity: 0;
  -webkit-transition-property: opacity;
      -ms-transition-property: opacity;
          transition-property: opacity;
}
#fade-quote-carousel.carousel .carousel-inner .active {
  opacity: 1;
  -webkit-transition-property: opacity;
      -ms-transition-property: opacity;
          transition-property: opacity;
}
#fade-quote-carousel.carousel .carousel-indicators {
  bottom: 10px;
}
#fade-quote-carousel.carousel .carousel-indicators > li {
  background-color: #333333;
  border: none;
}
#fade-quote-carousel blockquote {
    text-align: center;
    border: none;
}
#fade-quote-carousel .profile-circle {
    width: 100px;
    height: 100px;
    margin: 0 auto;
    border-radius: 100px;
}
    /*end here*/
.testomonial_bg{
        background-color: #232222!important;
        padding-top: 70px!important;
        padding-bottom: 60px!important;
}
.white{
    color: white!important;
}
.carousel-indicators .active {
    margin: 0;
    width: 12px;
    height: 12px;
    background-color: #fff!important;
}
</style>
    <!-- slider start -->
    <?php
    if(count($sliderListData) > 0)
    {
    ?>
    <div class="slider-area">
        <div class="bend niceties preview-2">
            <div id="ensign-nivoslider" class="slides">
            <?php
            $i = 1;
            foreach($sliderListData as $sliderListDataList)
            {
            ?>
            <img src="<?php echo $this->session->userdata('vendorURL') ?>upload/Slider/<?php echo $sliderListDataList->houdinv_custom_slider_image ?>" alt="" title="#slider-direction-<?php  echo $i; ?>" />
            <?php $i++; } ?>
            </div>
            <!-- direction 1 -->
            <?php
            $i = 1;
            foreach($sliderListData as $sliderListDataList)
            {
            ?>
            <div id="slider-direction-1" class="t-cn slider-direction">
                <div class="container">
                    <div class="slider-content t-lfl s-tb slider-1">
                        <div class="title-container s-tb-c title-compress">
                            <h1 class="title1"> <?php echo $sliderListDataList->houdinv_custom_slider_head ?></h1>
                            <h2 class="title2"><?php echo $sliderListDataList->houdinv_custom_slider_sub_heading ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            <?php $i++; } ?>
        </div>
    </div>
    <?php } ?>
    <?php
    if($this->session->flashdata('message_name'))
    {
        echo '<div class="alert alert-danger bg-blue-text">'.$this->session->flashdata('message_name').'</div>';
    ?>
    <?php }
    if($this->session->flashdata('success'))
    {
        echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
    }
    ?>
    <!-- latest category -->
    <?php
    if(count($categoryData) > 0 && $customHome[0]->houdinv_custom_home_data_category)
    {
    ?>
    <div class="banner-style-3 pt-100">
        <div class="container">
        <div class="section-title text-center mb-50">
                <h2><?php echo $customHome[0]->houdinv_custom_home_data_category; ?></h2>
            </div>
            <div class="row">

                    <?php
                    foreach($categoryData as $categoryDataList)
                    {
                    if($categoryDataList->houdinv_category_thumb)
                    {
                        $setImageData = $this->session->userdata('vendorURL')."images/category/".$categoryDataList->houdinv_category_thumb;
                    }
                    else
                    {
                        $setImageData = base_url()."Extra/noPhotoFound.png";
                    }
                    ?>
                        <div class="col-md-3 col-sm-3">
                            <div class="banner-style-3-img mb-30">
                                <img src="<?php echo $setImageData; ?>" alt="" style="width: 100%;height: 360px;">
                                <div class="banner-style-3-dec">
                                    <a href="<?php echo base_url() ?>Productlist/<?php echo $categoryDataList->houdinv_category_id ?>"><?php echo $categoryDataList->houdinv_category_name ?></a>
                                </div>
                            </div>

                        </div>
                <?php } ?>
                   </div>
        </div>
    </div>
    <?php } ?>
    <!-- End here -->
    <!-- latest collection -->
    <?php
    if($customHome[0]->houdinv_custom_home_data_latest_product && count($latestProducts) > 0)
    {
    ?>
    <div class="portfolio-area ptb-100">
        <div class="container">
            <div class="section-title text-center mb-50">
                <h2><?php echo $customHome[0]->houdinv_custom_home_data_latest_product; ?> <i class="fa fa-shopping-cart"></i></h2>
            </div>
            <div class="row portfolio-style-2">
                <div class="grid">
                <?php
                    foreach($latestProducts as $latestProductsList)
                    {
                      $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
                      $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
                    ?>
                    <div class="col-md-3 col-sm-6 col-xs-12 grid-item mix1 mb-50">
                        <div class="single-shop">
                            <div class="shop-img">
                                <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>">
                                <?php 
                                if($getProductImage[0])
                                {
                                    $setLatestImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                                }
                                else
                                {
                                    $setLatestImage = base_url()."Extra/noPhotoFound.png";
                                }
                                ?>
                                <img src="<?php echo $setLatestImage; ?>" alt=""  /></a>
                                <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?>
                                <div class="price-up-down">
                                    <span class="sale-new">Out of stock</span>
                                </div>
                                <?php  } ?>
                                <?php
                                    if($latestProductsList->houdinv_products_main_quotation == 1)
                                    {
                                    ?>
                                    <div class="price-up-down">
                                    <span class="model_qute_show quotation_span quotation" data-id="<?php echo $latestProductsList->houdin_products_id ?>"  data-toggle="modal">Ask Quotation</span>
                                </div>
                                    <?php } ?>
                                <div class="button-group">
                                <?php
                                if($latestProductsList->houdinv_products_total_stocks <= 0)
                                {
                                if($cartFunction == 1)
                                {
                                    ?>
                                    <div class="addcart">
                                <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" href="Javascript:;">
                                        <i class="pe-7s-cart"></i>
                                    </a>
                                    </div>
                                <?php  }
                                }
                                else
                                {
                                ?>
                                <div class="addcart">
                                <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" href="Javascript:;">
                                        <i class="pe-7s-cart"></i>
                                    </a>
                                    </div>
                                <?php }
                                ?>

                                    <a class="Add_to_whishlist_button wishlist" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>" title="Wishlist">
                                        <i class="pe-7s-like"></i>
                                    </a>
                                    <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>" title="Quick View">
                                        <i class="pe-7s-look"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="shop-text-all">
                                <div class="title-color fix">
                                    <div class="shop-title f-left">
                                        <h3><a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><?php echo substr($latestProductsList->houdin_products_title,0, 20).".." ; ?></a></h3>
                                    </div>
                                    <br/>
                                    <div class="price f-right">
                                        <span class="new"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?> <?php  } ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>
    <?php } ?>

    <!-- featured products -->
    <?php
    if($customHome[0]->houdinv_custom_home_data_featured_product)
    {
    ?>
    <div class="special-offer ptb-100">
        <div class="container">
            <div class="section-title text-center mb-50">
                <h2><?php echo $customHome[0]->houdinv_custom_home_data_featured_product; ?> <i class="fa fa-shopping-cart"></i></h2>
            </div>
            <div class="row">
                <div class="special-slider-active owl-carousel">
                    <?php
                    foreach($featuredProduct as $latestProductsList)
                    {
                    // print_r($latestProductsList);
                    $getProductImage = json_decode($latestProductsList->houdinv_products_main_images,true);
                    $getProductPrice = json_decode($latestProductsList->houdin_products_price,true);
                    ?>
                    <div class="single-special-slider">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="single-shop">
                                <div class="shop-img">
                                    <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>">
                                    <?php 
                                    if($getProductImage[0])
                                    {
                                        $setFeaturedImage = $this->session->userdata('vendorURL')."upload/productImage/".$getProductImage[0];
                                    }
                                    else
                                    {
                                        $setFeaturedImage = base_url()."Extra/noPhotoFound.png";
                                    }
                                    ?>
                                    <img src="<?php echo $setFeaturedImage; ?>" alt="" /></a>
                                    <?php if($latestProductsList->houdinv_products_total_stocks <= 0) { ?>
                                    <div class="price-up-down">
                                        <span class="sale-new">Out of stock</span>
                                    </div>
                                    <?php } ?>
                                    <?php
                                    if($latestProductsList->houdinv_products_main_quotation == 1)
                                    {
                                    ?>
                                    <div class="price-up-down">
                                        <span class="quotation">Ask Quotation</span>
                                    </div>
                                    <?php } ?>
                                    <div class="button-group">
                                    <?php
                                    if($latestProductsList->houdinv_products_total_stocks <= 0)
                                    {
                                    if($cartFunction == 1)
                                    {
                                        ?>
                                       <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>">
                                            <i class="pe-7s-cart"></i>
                                        </a>
                                    <?php  }
                                    }
                                    else
                                    {
                                    ?>
                                    <a class="Add_to_cart_button" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>">
                                            <i class="pe-7s-cart"></i>
                                        </a>

                                    <?php }
                                    ?>

                                        <a class="Add_to_whishlist_button wishlist" data-variant="0" data-cart="<?php echo $latestProductsList->houdin_products_id ?>">
                                            <i class="pe-7s-like"></i>
                                        </a>
                                        <a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>" title="Quick View">
                                            <i class="pe-7s-look"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="shop-text-all">
                                    <div class="title-color fix">
                                        <div class="shop-title f-left">
                                            <h3><a href="<?php echo base_url() ?>Productlistview/<?php echo $latestProductsList->houdin_products_id ?>"><?php echo substr($latestProductsList->houdin_products_title,0, 20).".." ; ?></a></h3>
                                        </div>
                                        <br/>
                                        <div class="price f-right">
                                            <span class="new"><?php
if($currencysymbol=="USD")
{
  echo "$";
}else if($currencysymbol=="AUD"){
  echo "$";
}else if($currencysymbol=="Euro"){
  echo "£";
}else if($currencysymbol=="Pound"){
  echo "€";
}else if($currencysymbol=="INR"){
  echo "₹";
}
?>&nbsp;<?php if($getProductPrice['discount'] == 0 || $getProductPrice['discount'] == "") { echo $latestProductsList->houdin_products_final_price; } else { $getFinalPrice = $getProductPrice['price']-$getProductPrice['discount']; ?> <strike><?php echo $getProductPrice['sale_price']; ?></strike>&nbsp;<?php echo $latestProductsList->houdin_products_final_price; ?> <?php  } ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>

    <!-- testimonial -->
        <section id="carousel" class="testomonial_bg">
    <div class="container">
        <div class="row">
            <div class="section-title text-center">
                <h2 class="white"><span class="ion-minus"></span>TESTIMONIAL<span class="ion-minus"></span></h2>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
                <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                  <!-- Carousel indicators -->
                  <ol class="carousel-indicators">
                    <?php
                    $i=0;
                    foreach($storeTestimonial as $storeTestimonialData)
                    {
                    ?>
                    <li data-target="#fade-quote-carousel" data-slide-to="<?php echo $i;?>" <?php if($i==1){ ?> class="active" <?php } ?>></li>
                    <?php $i++; } ?>
                  </ol>
                  <!-- Carousel items -->
                  <div class="carousel-inner">

                    <?php for($j=0;$j<$i;$j++){?>
                    <div class="item <?php if($j==0){echo "active";}?>">
                        <div class="profile-circle" style="background-color: rgba(0,0,0,.2);">
                        <img src="<?php if($storeTestimonial[$j]->testimonials_image) { ?> <?php echo $this->session->userdata('vendorURL')."upload/testimonials/".$storeTestimonial[$j]->testimonials_image; ?> <?php  } else { ?> <?php echo base_url() ?>Extra/apparels/img/testimonial/1.jpg <?php } ?>" class="img-circle profile-circle" alt="">
                        </div>
                        <blockquote>
                            <p class="white"><?php echo $storeTestimonial[$j]->testimonials_feedback;?></p>
                        </blockquote>
                    </div>
                  <?php } ?>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <!-- end  testimonial -->
    <?php  } ?>
    <!-- special-offer area end -->
    <?php
  $getShopName = getfolderName($this->session->userdata('shopName'));
  $this->load->view(''.$getShopName.'/Template/footer')
  ?>
