<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon_icon.png">
<title>Houdin - Shop Management</title>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/daterangepicker.css" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/shop_custom.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/intlTelInput.css">
<!-- Am chart css -->
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
<style>
#mceu_31
{
    display:none !important;
}
</style>
</head>
<body class="fixed-left">
<?php
$getLogo = logo();
$setDynamicImageData=base_url()."assets/images/Logo_W.png";
$setDynamicImageData2=base_url()."assets/images/favicon_W.png"
?>

  <div id="wrapper">
            <div class="topbar">
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="<?php echo base_url() ?>" class="logo"><!--<i class="icon-magnet icon-c-logo"></i>-->
                <img class="icon-c-logo" src="<?php echo $setDynamicImageData2;  ?>"/>
                        <span><img class="main-c-logo" src="<?php if($getLogo) { echo $getLogo; } else { echo $setDynamicImageData; } ?>" style="height: 46px;margin-top: 8px;"/> 
                        <!--Ub<i class="md md-album"></i>ld--></span></a>
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect waves-light">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>



                            <ul class="nav navbar-nav navbar-right pull-right">

                                <li class="dropdown top-menu-item-xs">
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="<?php echo base_url(); ?>assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url();?>Login/logout"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
