    <?php $this->load->view('Template/header') ?>
    <?php $this->load->view('Template/possidebar') ?>
        <style type="text/css">
        .radio.radio-inline {
        margin-top: 0;
        margin-left: 0px;
        }
        label.btn-primary.active{
            background-color: #964460 !important;  
            border: 1px solid #964460 !important;  
        }
        </style> 
        <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <div class="content-page">
        <!-- Start content -->
        <div class="content">
        <div class="container"> 
        <!--START Page-Title -->
        <div class="row">

        <div class="col-md-8">
        <h4 class="page-title">POS</h4>
        <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <!--<li><a href="javascript:void(0);">POS</a></li>-->
        <li class="active">POS</li>
        </ol>
        </div>

        </div>
        <!--END Page-Title --> 
        <?php echo form_open(base_url( 'POS/addOrderData' ), array( 'id' => 'submitMainForm', 'method'=>'post' ));?>
        <div class="row" style="margin-right: 10px">
        <div class="hidden-print">
        <div class="pull-right">
        <!-- <a href="<?php //echo base_url();?>POS/printinvoice" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i> Print</a> -->
        <input type="button" name="setPosDataBtn" class="btn btn-primary submitMainFormData generateOrderBtn" disabled="disabled" value="Generate Order">
        </div>
        </div>
        </div>
        <div class="row m-t-20">
        <div class="col-md-12"> 
        <?php
        if($this->session->flashdata('error'))
        {
            echo "<div class='alert alert-danger'>".$this->session->flashdata('error')."</div>";
        }
        if($this->session->flashdata('success'))
        {
            echo "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>";
        }
        ?>
        <div class="card-box row"> 
        <div class="form-group">
        <label class="pull-right" style="font-weight:bold"> <?php if($possetting[0]->minimum_order_amount && $possetting[0]->minimum_order_amount != 0) { ?> Minimum Order Amount: <?php echo $possetting[0]->minimum_order_amount; } ?></label>
        </div>
        <div class="col-md-8">
        <div class="alert alert-danger setErrorMessage" style="display:none;"></div>
        <div class="alert alert-success setSucccessMessage" style="display:none;"></div>
        <div class="row m-b-15">
        <div class="">
       
        <div class="form-group">
        <label class="col-md-3 control-label">Delivery Option</label>
        <div class="col-md-9">
        <div class="radio radio-inline" style="padding-left: 0px!important">
        <input type="radio" tabindex="1" class="deliveryTypeData" id="inlineRadio1" name="deliveryType" value="HD">
        <label for="inlineRadio1"> Home Delivery </label>
        </div>

        <div class="radio radio-inline">
        <input type="radio" tabindex="2" class="deliveryTypeData" id="inlineRadio2" name="deliveryType" value="PC" checked="">
        <label for="inlineRadio2"> Pick up by customer </label>
        </div>
        </div>
        </div>

        <!--<div class="form-group">

        <div class="col-md-9 m-t-10">
        <input type="text" value="" class="form-control" name="" placeholder="Address">
        </div>
        </div>-->

        </div>
        </div>
        
        <div class="row form-horizontal">


        <div class="form-group">
        <label class="col-md-3 control-label">Coupon Disc</label>
        <div class="col-md-6">
        <input type="hidden" class="couponCodeId" name="couponCodeId"/>
        <input type="text" value="" tabindex="3" class="form-control couponCodeData" name="couponCodeData" placeholder="Coupon Code">
        </div>
        <div class="col-md-3">
        <button type="button" tabindex="4" class="btn btn-default applyCouponCodeData">Apply</button>
        </div>
        </div>
        <div class="form-group">
        <label class="col-md-3 control-label">Customer Type</label>
        <div class="col-md-4">
        <input type="radio" tabindex="5" id="inlineRadio3" class="checkCustomerTypeData" value="RC" name="customerType" checked="">&nbsp;&nbsp;Registered
        </div>
        <div class="col-md-1"></div>
    	<div class="col-md-4">
        <input type="radio" id="inlineRadio4" class="checkCustomerTypeData"  tabindex="6" value="NRC" name="customerType">&nbsp;&nbsp;Not Registered
        </div>
         
        </div>
        <!-- Registered customer -->
        <div class="registerdCustomerData">
        <div class="form-group">
        <label class="col-md-3 control-label">Customer Name</label>
        <div class="col-md-9">
        <select class="form-control select1 selectCustomerData requiredValidationForCustomerName" tabindex="7" name="registeredCustomerId"> 
        <option value="">Choose Customer</option>
        <?php 
        foreach($customerList as $customerListData)
        {
        ?>
        <option value="<?php echo $customerListData->houdinv_user_id ?>" data-contact="<?php echo $customerListData->houdinv_user_contact ?>"><?php echo $customerListData->houdinv_user_name."(".$customerListData->houdinv_user_contact.")"  ?></option>
        <?php }
        ?>
        </select>
        </div>


        </div>

        <div class="form-group">
        <label class="col-md-3 control-label">Contact No.</label>
        <div class="col-md-9">
        <input type="text" value="" tabindex="8" readonly="readonly" class="form-control setCustomerContact requiredValidationForCustomerName" name="" placeholder="">
        </div>
        </div>
        <div class="form-group setAddressData" style="display:none">
        <label class="col-md-3 control-label">Choose Address</label>
        <div class="col-md-9">
        <select class="form-control setRegisteredCustomerAddress requiredValidationForRegisteredAddress" tabindex="9" name="setRegisteredCustomerAddress">
        
        </select>
        </div>
        </div>
        <div class="form-group setAddressData" style="display:none">
        <div class="col-sm-12"><label class="pull-right addRegistredUserAddress" style="cursor:pointer">Add Another Address</lable></div>
        </div>

        </div>
        <!-- Non registred customer -->
        <div class="nonRegisterdCustomerData" style="display:none;"> 
        <div class="form-group">
        <label class="col-md-3 control-label">Customer Name</label>
        <div class="col-md-9">
        <input type="text" tabindex="10" class="form-control requiredValidationForNonRegisteredUser" name="setNonRegisteredCustomerName" placeholder="Customer Name">
        </div>


        </div>

        <div class="form-group">
        <input type="hidden" class="setNonRegisteredCustomerAddress" name="setNonRegisteredCustomerAddress"/>
        <label class="col-md-3 control-label">Contact No.</label>
        <div class="col-md-9">
        <input type="text" tabindex="11" class="form-control requiredValidationForNonRegisteredUser" name="setNonRegisteredCustomerContact" placeholder="Customer Contact">
        </div>
        </div>
        <div class="form-group setAddressData" style="display:none">
        <label class="col-md-3 control-label">Choose Address</label>
        <div class="col-md-9">
        <input type="text" tabindex="12" class="form-control setAddressDataValue requiredValidationForNonRegisteredAddress" readonly="readonly"/>
        </div>
        </div>
        <div class="form-group setAddressData"  style="display:none">
        <div class="col-sm-12"><label class="pull-right addNonRegistredUserAddress" style="cursor:pointer">Add Address</lable></div>
        </div>
        </div>
        <!-- End here -->
        </div>
        <div class="row">
        <div class="col-md-12">
        <div class="table-responsive">
        <table class="table table-striped table-bordered table_shop_custom">
        <thead>
        <tr> 
         <th style="width:40%">Product Detail</th>
         <th style="width:8%">MRP</th>
         <th style="width:8%">CP</th>
        <th style="width:8%">SP</th>
        <th style="width:8%">Dis.P.</th>
        <th style="width:8%">Tax</th>
        <th style="width:8%">Qty</th>
        <th style="width:8%">Total</th>
        </tr>
        </thead>
        <tbody class="setRowData">
        <tr class="mainParentRow">
        <input type="hidden" class="inputProdcutId" name="inputProdcutId[]"/>
        <input type="hidden" class="inputVariantId" name="inputVariantId[]"/>
        <input type="hidden" class="inputQuantity" name="inputQuantity[]"/>
        <input type="hidden" class="inputActualPrice" name="inputActualPrice[]"/>
        <input type="hidden" class="inputDiscountedPrice" name="inputDiscountedPrice[]"/>
        <input type="hidden" class="inputTotalPrice" name="inputTotalPrice[]"/>
        <input type="hidden" class="inputtotaltax" name="inputtotaltax[]"/>
        <td>
            <div class="form-group" style="margin-bottom: 0px!important;">
            <select class="form-control setproductData select1" tabindex="14">
            <option value="">Choose Product</option>
            <?php 
                foreach($productData as $productDataList)
                {
            ?>
                <option data-price="<?php echo $productDataList['productPrice'] ?>" data-stock="<?php echo $productDataList['stock'] ?>" 
                data-variant="<?php echo $productDataList['variantId'] ?>" 
                data-product="<?php echo $productDataList['produtcId'] ?>" 
                data-tax="<?php echo $productDataList['tax_price'] ?>" data-cp="<?php echo $productDataList['cp'] ?>" data-taxprice="<?php echo $productDataList['tax_pricedata']; ?>" data-mrp="<?php echo $productDataList['mrp'] ?>"><?php echo $productDataList['productName'] ?></option>
            <?php 
            }
            ?>
        </select>
        </div>
    </td>
    <td class="mrp"></td>
    <td class="cp"></td>
        <td class="setMainPrice"></td>
        <td><input type="text" value="" class="form-control specialPrice number_validation" name="" placeholder=""></td>
        <td class="tax"></td>
        <td><input type="text" value="" class="form-control getQuantity number_validation" name="" placeholder=""></td>
        <td class="setFinalPrice">0</td>
        </tr>
        </tbody>
        </table>

        </div>
        </div>
        </div>
        <div class="form-group"> 
        <label for="field-1" class="control-label">Note</label>
        <textarea class="form-control" rows="4" name="customerNote"></textarea> 

        </div> 

        </div>
        <div class="col-md-4 m-t-10">
        <form method="">
        <div class="row form-horizontal"> 
        <div class="text-left text_black"> 
        <div class="col-md-5 m-t-15"><b>Total:<input type="hidden" class="finalAmountData" name="finalAmountData"/></b></div><div class="col-md-7 m-t-15 setFinalTotal">0</div>
        <div class="col-md-5 m-t-15"><b>Coupon:<input type="hidden" class="finalDiscountData" name="finalDiscountData"/></b></div><div class="col-md-7 m-t-15 setCouponDiscount">0</div>
        <div class="col-md-5 m-t-15"><b>Net Payable:<input type="hidden" class="netPayableAmount" name="netPayableAmount"/></b></div><div class="col-md-7 m-t-15 setNetPayable">0</div>

        </div>


        </div>

 
        <div class="m-t-10 btn-group" data-toggle="buttons">

        <p class="m-b-15" style="color:#000"><strong>Payment Options</strong></p>
        
        <label class="btn btn-primary ">
        <input type="radio" id="inlineRadio1" style="display:none" value="cash" name="paymentOption" checked=""> Cash
        </label>

        

         <label class="btn btn-primary ">
         <input type="radio" id="inlineRadio2" style="display:none" value="card" name="paymentOption"> Card
        </label>
        
      
        
        <label class="btn btn-primary ">
        <input type="radio" id="inlineRadio2" style="display:none" value="cheque" name="paymentOption"> Cheque
        </label>
 
        
        </div>
        
        <div class="form-group m-t-15">
        <label>Payment Status</label>
        <select class="form-control checkPaymentStatus" name="paymentStatus">
        <option value="">Choose Payment Status</option>
        <option value="1">Paid</option>
        <option value="0">Not Paid</option>
        </select>
        </div>  
        <div class="form-group m-t-15">
        <label>Delivery Date</label>
        <input type="text" class="form-control date_picker" name="deliveryDateData" value= "<?php echo date('Y-m-d'); ?>"/>
        </div>  
        <div class="form-group m-t-15 additionalAdjustmentCheck" style="display:none">
        <label>Additional adjustment</label>
        <input class="checked additionalAdjustmentCheckboxData" name="additioanlAdjustment" type="checkbox" value="adjustment">
        </div>  
        <div class="form-group additionalAdjustmentText" style="display:none">
        <input type="text" value="" class="form-control setPrivMaxValue" name="pendingAmountData" placeholder="Amount paid or amount pending">
        </div>
        </form>


        </div>
        </div>
        </div>
        </div>  
        <?php echo form_close(); ?>
        <!-- add registred user address modal -->
        <div id="addRegistredUserAddressModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
            <?php echo form_open(base_url( 'POS/addUserAddress' ), array( 'id' => 'addPosForm', 'method'=>'post' ));?>
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title custom_title"><b>Add Address</b></h4> 
                    </div> 
                    <div class="modal-body"> 
                            <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                <input type="hidden" class="addAddressCustomerId" name="addAddressCustomerId"/>
                                    <label for="field-3" class="control-label">Name</label> 
                                    <input type="text" name="userName" class=" form-control required_validation_for_pos name_validation" placeholder="User Name"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Contact</label> 
                                    <input type="text" name="userPhone" class=" form-control required_validation_for_pos name_validation" placeholder="Contact"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Address</label> 
                                    <textarea class="form-control required_validation_for_pos name_validation" rows="5" name="userMainAddress"> </textarea>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">City</label> 
                                    <input type="text" name="userCity" class=" form-control required_validation_for_pos name_validation" placeholder="City"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Zip</label> 
                                    <input type="text" name="userZip" class=" form-control required_validation_for_pos name_validation" placeholder="Zip"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Country</label> 
                                    <select class="form-control required_validation_for_pos" name="userCountry">
                                    <option value="">Choose Country</option>
                                    <option value="<?php echo $countryData['id'] ?>"><?php echo $countryData['country'] ?></option>
                                    </select>
                                </div> 
                            </div> 
                        </div> 
                         
                    </div> 
                    <div class="modal-footer"> 
                    <input type="submit" name="payumoneyAdd" class="btn btn-info addRegisteredUserBtnData" value="Add"/>
                    </div> 
                </div> 
                <?php echo form_close(); ?>
            </div>
        </div><!-- /.modal -->


         <!-- add non registred user address modal -->
         <div id="addNonRegistredUserAddressModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog"> 
            <?php echo form_open(base_url( 'POS/addUserAddress' ), array( 'id' => 'addNonRegisteredForm', 'method'=>'post' ));?>
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title custom_title"><b>Add Address</b></h4> 
                    </div> 
                    <div class="modal-body"> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Address</label> 
                                    <textarea class="form-control required_validation_for_pos_non name_validation nonUserMainAddress" rows="5" name="nonUserMainAddress"> </textarea>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">City</label> 
                                    <input type="text" name="nonUserCity" class=" form-control nonUserCity required_validation_for_pos_non name_validation" placeholder="City"> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group"> 
                                    <label for="field-3" class="control-label">Zip</label> 
                                    <input type="text" name="nonUserZip" class=" form-control nonUserZip required_validation_for_pos_non name_validation" placeholder="Zip"> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div class="modal-footer"> 
                    <input type="submit" class="btn btn-info addNonRegisteredUserBtnData" value="Add"/>
                    </div> 
                </div> 
                <?php echo form_close(); ?>
            </div>
        </div><!-- /.modal -->
        <?php $this->load->view('Template/footer') ?>
    <script type="text/javascript">
    $(".select1").select2({
    minimumInputLength: 2
});
// Select serach function 
// set product data
$(document).on('change','.setproductData',function(){
    $(this).parents('.mainParentRow').find('.setMainPrice').text('').text($(this).children('option:selected').attr('data-price'));
    $(this).parents('.mainParentRow').find('.specialPrice').val('').val($(this).children('option:selected').attr('data-price'));
    $(this).parents('.mainParentRow').find('.getQuantity').attr('data-max',$(this).children('option:selected').attr('data-stock'));
    
    $(this).parents('.mainParentRow').find('.mrp').text('').text($(this).children('option:selected').attr('data-mrp'));
    $(this).parents('.mainParentRow').find('.cp').text('').text($(this).children('option:selected').attr('data-cp'));
    $(this).parents('.mainParentRow').find('.tax').text('').text($(this).children('option:selected').attr('data-tax'));
    // Call final price function
    var quantity = $(this).parents('.mainParentRow').find('.getQuantity').val();
    var getSpecialPriceData = $(this).parents('.mainParentRow').find('.specialPrice').val();
    var dataSet = $(this);
    if(quantity && getSpecialPriceData)
    {
        finalPrice(quantity,getSpecialPriceData,dataSet); 
    }
    setProductArrayData(dataSet);
});
// get quantity
$(document).on('keyup','.getQuantity',function(e){
    var getSpecialPriceData = $(this).parents('.mainParentRow').find('.specialPrice').val();
    if(parseInt($(this).val()) > parseInt($(this).attr('data-max')))
    {
        var quantity = $(this).attr('data-max');
        $(this).val($(this).attr('data-max'));
    }
    else
    {
        var quantity = $(this).val();
    }
    var dataSet = $(this);
    finalPrice(quantity,getSpecialPriceData,dataSet); // Call final price function
    setProductArrayData(dataSet);
    // check enter is pressed or not on quantity
    if(e.which == 13){
        var getEmptyValue = emptyValueCheck()  // call empty valur check function
        if(getEmptyValue == 0)
        {
            var setText = 'add';
            var dataset = $(this);
            sethtmldata(setText)  // call html append function
        }
    }
});
// get coupon code ajax
$(document).on('click','.applyCouponCodeData',function(){
    var getCouponCode = $('.couponCodeData').val();
    var getNetPayable = $('.setNetPayable').text();
    couponCodeAjax(getCouponCode,1,getNetPayable);  // check coupon code ajax
})
// get special price
$(document).on('keyup','.specialPrice',function(){
    var getSpecialPriceData = $(this).val();
    var quantity = $(this).parents('.mainParentRow').find('.getQuantity').val();
    var dataSet = $(this);
    finalPrice(quantity,getSpecialPriceData,dataSet);// Call final price function
    setProductArrayData(dataSet);
});
// set customer type
$(document).on('click','.checkCustomerTypeData',function(){
    // call to check customer type
    checkCustomerTypeData();
    // call to check delivery type
    setDeliveryType();
});

// get customer contact
$(document).on('change','.selectCustomerData',function(){
    $('.setCustomerContact').val($(this).children('option:selected').attr('data-contact'));
    fetchCustomerAddress($(this).val());
    checkCustomerPriv($(this).val())
});
// set delivery type
$(document).on('click','.deliveryTypeData',function(){
    // call delivery type function
    setDeliveryType(); 
    // call to check customer type
    checkCustomerTypeData();
});
// add registred user address
$(document).on('click','.addRegistredUserAddress',function(){
    if($('.selectCustomerData').val())
    {
        $('.addAddressCustomerId').val($('.selectCustomerData').val());
        $('#addRegistredUserAddressModal').modal('show');
        $('.setErrorMessage').hide();
    }
    else
    {
        $('.setErrorMessage').show();
        $('.setErrorMessage').text('').text('Please choose customer');
    }   
});
// client side validaion
$(document).on('submit','#addPosForm',function(e){
    $('.setErrorMessage').hide();
    $('.setSucccessMessage').hide();
var check_required_field='';
$(this).find(".required_validation_for_pos").each(function(){
    var val22 = $(this).val();
    if (!val22){
        check_required_field =$(this).size();
        $(this).css("border-color","#ccc");
        $(this).css("border-color","red");
    }
    $(this).on('keypress change',function(){
        $(this).css("border-color","#ccc");
    });
});
if(check_required_field)
{
    return false;
}
else {
    e.preventDefault();
    $('.addRegisteredUserBtnData').prop('disabled',true);
    var formValue = $('#addPosForm').serializeArray();
    jQuery.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>POS/addRegisteredUserAddress",
    data: formValue,
    success: function(data) {
        var getJsonData = jQuery.parseJSON(data);
        if(getJsonData.messgae == 'Address is addedd successfully')
        {
            $('#addRegistredUserAddressModal').modal('hide');
            $('.setSucccessMessage').show()
            $('.setSucccessMessage').text('').text(getJsonData.messgae);
            // call custoemr address function
            fetchCustomerAddress($('.selectCustomerData').val());
        }
        else
        {
            $('#addRegistredUserAddressModal').modal('hide');
            $('.setErrorMessage').show();
            $('.setErrorMessage').text('').text(getJsonData.messgae);
        }
    }
    }); 
    
}
});
// add address for non registered users
$(document).on('click','.addNonRegistredUserAddress',function(){
$('#addNonRegistredUserAddressModal').modal('show');
});
// edit current address of non registered user
$(document).on('click','.setAddressDataValue',function(){
$('.nonUserMainAddress').text($(this).attr('data-address'));
$('.nonUserCity').val($(this).attr('data-city'));
$('.nonUserZip').val($(this).attr('data-zip'));
$('#addNonRegistredUserAddressModal').modal('show');
})
// set validation for non registred user
$(document).on('submit','#addNonRegisteredForm',function(e){
    $('.setErrorMessage').hide();
    $('.setSucccessMessage').hide();
    $('.addNonRegisteredUserBtnData').prop('disabled',true);
var check_required_field='';
$(this).find(".required_validation_for_pos_non").each(function(){
    var val22 = $(this).val();
    if (!val22){
        check_required_field =$(this).size();
        $(this).css("border-color","#ccc");
        $(this).css("border-color","red");
    }
    $(this).on('keypress change',function(){
        $(this).css("border-color","#ccc");
    });
});
if(check_required_field)
{
    $('.addNonRegisteredUserBtnData').prop('disabled',false);
    return false;
}
else 
{
    e.preventDefault();
    $('.addNonRegisteredUserBtnData').prop('disabled',false);
    var formValue = $('#addNonRegisteredForm').serializeArray();
    var getMainAddress = formValue[1].value;
    var getCity = formValue[2].value;
    var getZip = formValue[3].value;
    $('.setAddressDataValue').val('').val(getMainAddress+", "+getCity+", "+getZip);
    $('.setAddressDataValue').attr('data-address',getMainAddress);
    $('.setAddressDataValue').attr('data-city',getCity);
    $('.setAddressDataValue').attr('data-zip',getZip);
    $('#addNonRegistredUserAddressModal').modal('hide');
    var setAddressString = getMainAddress+"^"+getCity+"^"+getZip;
    $('.setNonRegisteredCustomerAddress').val(setAddressString);
}
});
// set additionla adjustment
$(document).on('click','.additionalAdjustmentCheckboxData',function(){
$('.setPrivMaxValue').val('');
if($(this).prop('checked') == true)
{
    $('.setPrivMaxValue').val($('.setPrivMaxValue').attr('data-max'));
    $('.additionalAdjustmentText').show();
}    
else
{
    $('.setPrivMaxValue').val('');
    $('.additionalAdjustmentText').hide();
}
setFinalPriceValue();
});
// set adjustment on price
$(document).on('keyup','.setPrivMaxValue',function(){
var getAdjustMentValue = $(this).val();
setPendingAmountData(getAdjustMentValue);
})
// final price function 
function finalPrice(quantity,specialprice,dataSet)
{
    if(specialprice)
    {
        if(quantity)
        {
            var setQuantity = quantity;
        }
        else
        {
            setQuantity = 0;
            dataSet.val(setQuantity);
        }
        var setFinalPrice = (parseFloat(specialprice)*parseFloat(setQuantity))
        dataSet.parents('.mainParentRow').find('.setFinalPrice').text('').text(setFinalPrice);
        dataSet.parents('.mainParentRow').find('.specialPrice').css('border','1px solid #d3d3d3');
    }
    else
    {
        
        dataSet.val(dataSet.parents('.mainParentRow').find('.setMainPrice').text());
        dataSet.parents('.mainParentRow').find('.specialPrice').css('border','1px solid red');
        dataSet.parents('.mainParentRow').find('.setFinalPrice').text('0');
    }
    setFinalPriceValue(); // set final value
}
// add html or remove html
function sethtmldata(setText,dataset)
{
    if(setText == 'add')
    {
        var countRows = ($('.mainParentRow').length)+1;
        $('.setRowData').append('<tr ddd class="mainParentRow"><input type="hidden" class="inputProdcutId" name="inputProdcutId[]"/><input type="hidden" class="inputDiscountedPrice" name="inputDiscountedPrice[]"/><input type="hidden" class="inputVariantId" name="inputVariantId[]"/><input type="hidden" class="inputQuantity" name="inputQuantity[]"/><input type="hidden" class="inputActualPrice" name="inputActualPrice[]"/><input type="hidden" class="inputtotaltax" name="inputtotaltax[]"/><input type="hidden" class="inputTotalPrice" name="inputTotalPrice[]"/><td><div class="form-group" style="margin-bottom: 0px!important;"><select class="form-control setproductData select_auto'+countRows+'"><option value="">Choose Product</option><?php foreach($productData as $productDataList){?><option data-tax="<?php echo $productDataList['tax_price'] ?>"data-cp="<?php echo $productDataList['cp'] ?>" data-mrp="<?php echo $productDataList['mrp'] ?>" data-price="<?php echo $productDataList['productPrice'] ?>" data-stock="<?php echo $productDataList['stock'] ?>" data-variant="<?php echo $productDataList['variantId'] ?>" data-product="<?php echo $productDataList['produtcId'] ?>"><?php echo $productDataList['productName'] ?></option><?php } ?></select></div></td><td class="mrp"></td><td class="cp"></td><td class="setMainPrice"></td><td><input type="text" value="" class="form-control specialPrice number_validation" name="" placeholder=""></td><td class="tax"></td><td><input type="text" value="" class="form-control getQuantity number_validation" name="" placeholder=""></td><td class="setFinalPrice">0</td></tr>');
        $(".select_auto"+countRows+"").select2({
            minimumInputLength: 2
        });
    }
}
// calculate final price value
function setFinalPriceValue()
{
    // each function to calculate total
    var setTotalData = 0;
    var minimumorder = '<?php echo $possetting[0]->minimum_order_amount; ?>';
    if(minimumorder)
    {
        minimumorder = minimumorder;    
    }
    else
    {
        minimumorder = 0;    
    }
    $('.setFinalPrice').each(function(){
        setTotalData = setTotalData+parseFloat($(this).text());
    });
    // set subtotal
    $('.setFinalTotal').text('').text(setTotalData);
    var getDiscount = $('.setCouponDiscount').text();
    var getFinalPayable = setTotalData-parseFloat(getDiscount);
    $('.setNetPayable').text(getFinalPayable);
    if(parseFloat(getFinalPayable) < parseFloat(minimumorder))
    {
        $('.generateOrderBtn').prop('disabled',false);
    }
    else
    {
        $('.generateOrderBtn').prop('disabled',false);
    }
    // update coupon code data
    var getCouponCode = $('.couponCodeData').val();
    var getCustomer = $('.selectCustomerData').val();
    var getNetPayable = $('.setFinalTotal').text();
    if(getCouponCode && getCustomer && getNetPayable)
    {
        couponCodeAjax(getCouponCode,getCustomer,getNetPayable);
    }
    var getAdjustMentValue = $('.setPrivMaxValue').val();
    if(getAdjustMentValue && getAdjustMentValue > 0)
    {
        setPendingAmountData(getAdjustMentValue)
    }
    // set amount
    $('.finalAmountData').val($('.setFinalTotal').text());
    $('.finalDiscountData').val($('.setCouponDiscount').text());
    $('.netPayableAmount').val($('.setNetPayable').text());
}
// check all empty value
function emptyValueCheck()
{
    // product select 
    var checkData = 0;
    $('.setproductData').each(function(){
        if($(this).val() == '')
        {
            $(this).css('border','1px solid red');
            checkData++;
        }
        else
        {
            $(this).css('border','1px solid #d3d3d3');
        }
    });
    // check special price value
    $('.specialPrice').each(function(){
        if($(this).val() == 0 || $(this).val() == "")
        {
            $(this).css('border','1px solid red');
            checkData++;
        }
        else
        {
            $(this).css('border','1px solid #d3d3d3');
        }
    });
    // check quantity
    $('.getQuantity').each(function(){
        if($(this).val() == 0 || $(this).val() == "")
        {
            $(this).css('border','1px solid red');
            checkData++;
        }
        else
        {
            $(this).css('border','1px solid #d3d3d3');
        }
    });
     return checkData;
}
// check coupon ajax data
function couponCodeAjax(coupon,customerId,getNetPayable)
{
    $('.setErrorMessage').hide();
    $('.setSucccessMessage').hide();
    $('.applyCouponCodeData').prop('disabled',true);
    jQuery.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>POS/getCouponValidity",
    data: {"coupon": coupon,'customerId':customerId,'getNetPayable':getNetPayable},
    success: function(data) {
       var getJsonData = jQuery.parseJSON(data);
       var getMinimumOrder = '<?php echo $possetting[0]->minimum_order_amount ?>';
       if(getMinimumOrder)
       {
           getMinimumOrder = getMinimumOrder;
       }
       else
       {
           getMinimumOrder = 0;
       }
       if(getJsonData.message == 'Success')
       {
           $('.setSucccessMessage').show().text('Coupon code applied Succefully')
           var getSubTotal = $('.setFinalTotal').text();
           var couponDiscount = getJsonData.discount;
           var appliedCouponId = getJsonData.couponId;
           $('.couponCodeId').val(appliedCouponId);
           if(couponDiscount > 0)
           {
               var setNetDiscount = (parseFloat(getSubTotal)*parseFloat(couponDiscount))/100;
               var setNetPayable = parseFloat(getSubTotal)-setNetDiscount;
           }
           else
           {
               var setNetDiscount = 0;
               var setNetPayable = getSubTotal;
           }
           $('.setCouponDiscount').text(setNetDiscount);
           $('.setNetPayable').text(setNetPayable);
           if(parseFloat(setNetPayable) < parseFloat(getMinimumOrder))
            {
                $('.generateOrderBtn').prop('disabled',true);
            }
            else
            {
                $('.generateOrderBtn').prop('disabled',false);
            }
           $('.applyCouponCodeData').prop('disabled',false);
           var getAdjustMentValue = $('.setPrivMaxValue').val();
           if(getAdjustMentValue && getAdjustMentValue > 0)
           {
               setPendingAmountData(getAdjustMentValue)
            }
            // set amount
            $('.finalAmountData').val($('.setFinalTotal').text());
            $('.finalDiscountData').val($('.setCouponDiscount').text());
            $('.netPayableAmount').val($('.setNetPayable').text());
       }
       else
       {
            $('.applyCouponCodeData').prop('disabled',false);
            $('.setErrorMessage').show().text(getJsonData.message);
       }
    }
    }); 
}
// fetch customer registred address
function fetchCustomerAddress(customerId)
{
    var getDeliveryType = setDeliveryType();
    if(getDeliveryType == 'HD')
    {
        $('.setErrorMessage').hide();
        $('.setSucccessMessage').hide();
        jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>POS/getCustomerAddress",
        data: {'customerId':customerId },
        success: function(data) {
            var getJsonData = jQuery.parseJSON(data);
            if(getJsonData.message == 'Success')
            {
                var setCustomerAddress = '<option value="">Choose Address</option>';
                $.each(getJsonData.data,function(){
                    setCustomerAddress += '<option value="'+this.addressId+'">'+this.mainAddress+', '+this.city+', '+this.zip+'</option>'
                });
                $('.setRegisteredCustomerAddress').html(setCustomerAddress);
            }
            else
            {
                $('.setErrorMessage').show().text(getJsonData.message);
            }
        }
        }); 
    }
}
// check deliveryType Data
function setDeliveryType()
{
    var setPickupValue= "";
    $('.deliveryTypeData').each(function(){
        if($(this).prop('checked') == true)
        {
            if($(this).val() == 'HD')
            {
                $('.setAddressData').show();
                setPickupValue = 'HD';
            }
            else
            {
                $('.setAddressData').hide();
                setPickupValue = 'PC';
            }
        }
    });
    return setPickupValue;
}
// check customer type function
function checkCustomerTypeData()
{
    $('.additionalAdjustmentCheck').hide();
    $('.selectCustomerData').val('');
    $('.setCustomerContact').val('');
    $('.checkCustomerTypeData').each(function(){
    if($(this).prop('checked') == true)
    {
        var getValue = $(this).val();
        if(getValue == 'RC')
        {
            $('.registerdCustomerData').show();
            $('.nonRegisterdCustomerData').hide();
        }
        else
        {
            $('.registerdCustomerData').hide();
            $('.nonRegisterdCustomerData').show();
        }
    }
})
}
function checkCustomerPriv(customerId)
{
    jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>POS/getCustomerPrivilage",
        data: {'customerId':customerId },
        success: function(data) {
            var getData = jQuery.parseJSON(data);
            if(getData.message == 'Success')
            {
                if(getData.dataValuePriv > 0)
                {
                    $('.additionalAdjustmentCheck').show();
                    $('.setPrivMaxValue').attr('data-max',getData.dataValuePriv)
                }
                else
                {
                    $('.additionalAdjustmentCheck').hide();
                    $('.setPrivMaxValue').attr('data-max','')
                }
            }
        }
        }); 
}
function setPendingAmountData(priceData)
{
    $('.setErrorMessage').hide();
    if(parseFloat($('.setPrivMaxValue').attr('data-max')) < parseFloat(priceData))
    {
        $('.setPrivMaxValue').val($('.setPrivMaxValue').attr('data-max'));
    }
    var getTotalAmount = $('.setFinalTotal').text();
    var getDiscount = $('.setCouponDiscount').text();
    var getFianlNetPayable = (parseFloat(getTotalAmount)-parseFloat(getDiscount));
    var additionalAdjustment = $('.setPrivMaxValue').val();
    var getMinimumOrder = '<?php echo $possetting[0]->minimum_order_amount ?>';
    if(getMinimumOrder)
    {
        getMinimumOrder = getMinimumOrder;
    }
    else
    {
        getMinimumOrder = 0;
    }
    if(getFianlNetPayable && getFianlNetPayable > 0 && additionalAdjustment && additionalAdjustment > 0)
    {
        if(getFianlNetPayable > additionalAdjustment)
        {
            var setNewNetPayable = getFianlNetPayable - additionalAdjustment;
        }
        else
        {
            var setNewNetPayable = additionalAdjustment - getFianlNetPayable;
        }
        $('.setNetPayable').text('').text(setNewNetPayable);
        if(parseFloat(setNewNetPayable) < parseFloat(getMinimumOrder))
        {
            $('.generateOrderBtn').prop('disabled',true);
        }
        else
        {
            $('.generateOrderBtn').prop('disabled',false);
        }
    }
    else
    {
        $('.setPrivMaxValue').val('1');
    }
}
// set product value
function setProductArrayData(dataset)
{
    var getProductId = dataset.parents('.mainParentRow').find('.setproductData').children('option:selected').attr('data-product');
    var getVariantId = dataset.parents('.mainParentRow').find('.setproductData').children('option:selected').attr('data-variant');
    var actualPrice = dataset.parents('.mainParentRow').find('.specialPrice').val();
    var quantityData = dataset.parents('.mainParentRow').find('.getQuantity').val();
    var finalPriceData = dataset.parents('.mainParentRow').find('.setFinalPrice').text();
    var productInitialPrice = dataset.parents('.mainParentRow').find('.setMainPrice').text();
    var productTax = dataset.parents('.mainParentRow').find('.setproductData').children('option:selected').attr('data-taxprice');
    if(productTax)
    {
        var setTaxdata = parseFloat(productTax)*quantityData;
    }
    else
    {
        var setTaxdata = 0;
    }
    var findDiscountData = parseFloat(productInitialPrice)-parseFloat(actualPrice);
    dataset.parents('.mainParentRow').find('.inputProdcutId').val(getProductId);
    dataset.parents('.mainParentRow').find('.inputVariantId').val(getVariantId);
    dataset.parents('.mainParentRow').find('.inputQuantity').val(quantityData);
    dataset.parents('.mainParentRow').find('.inputActualPrice').val(actualPrice);
    dataset.parents('.mainParentRow').find('.inputTotalPrice').val(finalPriceData);
    dataset.parents('.mainParentRow').find('.inputDiscountedPrice').val(findDiscountData);
    dataset.parents('.mainParentRow').find('.inputtotaltax').val(setTaxdata);
}
$(document).on('click','.submitMainFormData',function(){
    var setData = 0;

    $('.checkCustomerTypeData').each(function(){
        if($(this).prop('checked') == true)
        {
            if($(this).val() == 'RC')
            {
                $('.requiredValidationForCustomerName').each(function(){
                    if($(this).val() == "")
                    {
                        setData++;
                        $(this).css('border','1px solid red');
                    }
                    else
                    {
                        $(this).css('border','1px solid #d3d3d3');
                    }
                });
                $('.deliveryTypeData').each(function(){
                if($(this).prop('checked') == true)
                {
                    if($(this).val() == 'HD')
                    {
                        if($('.checkCustomerTypeData').val() == 'RC')
                        {
                            if($('.requiredValidationForRegisteredAddress').val() == "")
                            {
                                setData++;
                                $('.requiredValidationForRegisteredAddress').css('border','1px solid red');
                            }
                            else
                            {
                                $('.requiredValidationForRegisteredAddress').css('border','1px solid #d3d3d3');
                            }
                        }
                        else
                        {
                            if($('.requiredValidationForNonRegisteredAddress').val() == "")
                            {
                                setData++;
                                $('.requiredValidationForNonRegisteredAddress').css('border','1px solid red');
                            }
                            else
                            {
                                $('.requiredValidationForNonRegisteredAddress').css('border','1px solid #d3d3d3');
                            }
                        }
                    }
                    else
                    {
                        // if($('.requiredValidationForNonRegisteredAddress').val() == "")
                        // {
                        //     alert('inf');
                        //     setData++;
                        //     $('.requiredValidationForNonRegisteredAddress').css('border','1px solid red');
                        // }
                        // else
                        // {
                        //     $('.requiredValidationForNonRegisteredAddress').css('border','1px solid #d3d3d3');
                        // }
                    }
                }
            })
            }
            else
            {
                $('.requiredValidationForNonRegisteredUser').each(function(){
                    if($(this).val() == "")
                    {
                        setData++;
                        $(this).css('border','1px solid red');
                    }
                    else
                    {
                        $(this).css('border','1px solid #d3d3d3');
                    }
                });
            }            
        }
    });
    var getValueData = emptyValueCheck();
    // setData = getValueData;
    if($('.checkPaymentStatus').val() == "")
    {
        setData++;
        $('.checkPaymentStatus').css('border','1px solid red');
    }
    else
    {
        $('.checkPaymentStatus').css('border','1px solid #d3d3d3');
    }
    if(setData == 0 && getValueData == 0)
    {
        generateOrderData();
    }
    else
    {
        return false;
    }
});
function generateOrderData()
{
    $('#submitMainForm').submit();
}
    </script>
