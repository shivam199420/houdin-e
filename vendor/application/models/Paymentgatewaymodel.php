<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Paymentgatewaymodel extends CI_Model{
    function __construct() 
    {
        parent::__construct();
        $this->load->helper('database');
        $getDatabaseData = switchDynamicDatabase();
        $this->db = $this->load->database($getDatabaseData,true);
    }
    public function insertPayumoney($data)
    {
        $getPayment = $this->db->insert('houdinv_payment_gateway',$data);
        if($getPayment == 1)
        {
            return array('message'=>'yes');
        } 
        else
        {
            return array('message'=>'no');
        }
    }
    public function updatePayumoney($data,$id) 
    {
        $this->db->where('houdinv_payment_gateway_id',$id);
        $getPayuUpdate = $this->db->update('houdinv_payment_gateway',$data);
        if($getPayuUpdate)
        {
            return array('message'=>'yes');
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function getPaymentGateway()
    {
        $getPaymentGatewayPauy = $this->db->select('*')->from('houdinv_payment_gateway')->where('houdinv_payment_gateway_type','payu')->get()->result();
        // get authorized.net
        $getPaymentGatewayAuth = $this->db->select('*')->from('houdinv_payment_gateway')->where('houdinv_payment_gateway_type','auth')->get()->result();
        return array('getPaymentGatewayPauy'=>$getPaymentGatewayPauy,'getPaymentGatewayAuth'=>$getPaymentGatewayAuth);
    }
    public function updateAuthorized($data,$id)
    {
        if($id)
        {
            $this->db->where('houdinv_payment_gateway_id',$id);
            $getStatus = $this->db->update('houdinv_payment_gateway',$data);
        }
        else
        {
            $getStatus = $this->db->insert('houdinv_payment_gateway',$data);
        }
        return $getStatus;
    }
}
