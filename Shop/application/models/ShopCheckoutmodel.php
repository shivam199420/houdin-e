<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ShopCheckoutmodel extends CI_Model{
    function __construct()
    {
        $this->getAppDB = switchDynamicDatabase($data['shop']);

        $this->getDynamicDB = $this->load->database($this->getAppDB,true);
    }


    public function checkout($billing_data,$shipping_data,$data,$user_coupon)
    {
       $coupon_use_id = 0;
        if($user_coupon['data'])
                {
                   $coupon = $this->getDynamicDB->select("*")->from("houdinv_user_use_Coupon")
                   ->where("houdinv_user_use_Coupon_user_id",$user_coupon['data']['houdinv_user_use_Coupon_user_id'])
                   ->where("houdinv_user_use_Coupon_coupon_id",$user_coupon['data']['houdinv_user_use_Coupon_coupon_id'])
                   ->get()->row();
                    if($coupon)
                    {

                     $count =  $coupon->houdinv_user_use_Coupon_used_Count;
                     $discount = $coupon->houdinv_user_use_Coupon_discount;
                     $count_row_id= $coupon->houdinv_user_use_Coupon_id;
                     $final_count_array =  array("houdinv_user_use_Coupon_discount"=>($user_coupon['data']['houdinv_user_use_Coupon_discount'])+$discount,
                            "houdinv_user_use_Coupon_update_date"=>$date,
                            "houdinv_user_use_Coupon_used_Count"=>$count+1
                            );
                      $this->getDynamicDB->where("houdinv_user_use_Coupon_id",$count_row_id);
                      $this->getDynamicDB->update("houdinv_user_use_Coupon",$final_count_array);
                     $coupon_use_id = $count_row_id;
                    }
                    else
                    {

                      $this->getDynamicDB->insert("houdinv_user_use_Coupon",$user_coupon['data']);
                   $coupon_use_id = $this->getDynamicDB->insert_id();;
                    }
                }



       if($billing_data)
       {
        if(!$billing_data['billing_data_old'])
        {
              $billing =   $this->getDynamicDB->insert("houdinv_checkout_address_data",$billing_data);
              $billing_id = $this->getDynamicDB->insert_id();
        }
        else
        {
              $billing_id = $billing_data['billing_data_old'];
        }

             if($billing_id)
             {
                    if($shipping_data['billing_data_only'])
                    {
                        $shipping_id = $billing_id;
                    }
                    else
                    {

                      if(!$shipping_data['shipping_data_old'])
                        {
                           $shipping = $this->getDynamicDB->insert("houdinv_checkout_address_data",$shipping_data);
                            $shipping_id = $this->getDynamicDB->insert_id();

                       }
                        else
                        {
                           $shipping_id =$shipping_data['shipping_data_old'];

                        }
                    }

                   if($shipping_id)
                   {
                        $data['houdinv_order_delivery_address_id']=$shipping_id;
                        $data['houdinv_order_billing_address_id']=$billing_id;
                        $data['houdinv_orders_discount_detail_id'] =$coupon_use_id;
                       $this->getDynamicDB->insert("houdinv_orders",$data);
                        $order_id = $this->getDynamicDB->insert_id();

                        if($order_id)
                        {
                            $this->getDynamicDB->delete("houdinv_users_cart",array("houdinv_users_cart_user_id"=>$data['houdinv_order_user_id']));
                            // update account receivable(debtor) in case of order is COD
                            if($data['houdinv_order_payment_method'] != 'payumoney' && $data['houdinv_order_payment_method'] != 'auth')
                            {
                                // $getAccountData = $this->getDynamicDB->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id',1)->where('houdinv_accounts_detail_type_id','1')->get()->result();
                                // if(count($getAccountData) > 0)
                                // {
                                //     $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getAccountData[0]->houdinv_accounts_id,
                                //     'houdinv_accounts_balance_sheet_ref_type'=>'Pending Amount',
                                //     'houdinv_accounts_balance_sheet_pay_account'=>$data['houdinv_order_user_id'],
                                //     'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                                //     'houdinv_accounts_balance_sheet_account'=>0,
                                //     'houdinv_accounts_balance_sheet_order_id'=>$order_id,
                                //     'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                                //     'houdinv_accounts_balance_sheet__increase'=>$data['houdinv_orders_total_remaining'],
                                //     'houdinv_accounts_balance_sheet_decrease'=>0,
                                //     'houdinv_accounts_balance_sheet_deposit'=>0,
                                //     'houdinv_accounts_balance_sheet_final_balance'=>0,
                                //     'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                                //     $this->getDynamicDB->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                                //     // get amount
                                //     $getTotalAmount = $this->getDynamicDB->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                                //     ->where('houdinv_accounts_balance_sheet_account_id',$getAccountData[0]->houdinv_accounts_id)->get()->result();
                                //     if(count($getTotalAmount) > 0)
                                //     {
                                //         $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                                //     }
                                //     else
                                //     {
                                //         $setFinalAmount = 0;
                                //     }
                                //     $this->getDynamicDB->where('houdinv_accounts_id',$getAccountData[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
                                // }
                            }
                            // end here
                        return $order_id;

                        }


                   }
             }

        }


    }

    public function GetprodcutId_varints($varints_id){
      return  $data =   $this->getDynamicDB->select("houdin_products_variants_product_id as p_id")
        ->from("houdinv_products_variants")
        ->where('houdin_products_variants_id',$varints_id)
        ->get()->row();


 }
    public function shippingCharge($min_price)
    {
    $data =   $this->getDynamicDB->select("*")
      ->from("houdinv_shipping_ruels")
      ->get()->row();


      if($data)
      {
          if($data->shipping_min_value <=$min_price)
          {
            return 0;
          }
          else
          {
            return $data->shipping_charge;
          }
      }
      else
      {
        return 0;
      }
    }

   public  function AddressData($user)
   {

       $data =   $this->getDynamicDB->select("*")
             ->from("houdinv_user_address")
            ->where('houdinv_user_address_user_id',$this->session->userdata('userAuth'))
             ->get()->result();



        return $data;

   }


   public function IsCouponExist($code)
    {

           $data =   $this->getDynamicDB->select("*")->from("houdinv_coupons")
             ->where("houdinv_coupons_code",strtolower($code))
             ->where("houdinv_coupons_payment_method","both")
             ->where("houdinv_coupons_status","active")->get()->row();

            $date = strtotime(date("Y-m-d"));

             if($data)
             {

                if(strtotime($data->houdinv_coupons_valid_to)>=$date && strtotime($data->houdinv_coupons_valid_from)<=$date)
                {
                 $array = array("code"=>200 ,"msg"=>$data->houdinv_coupons_id,
                 "cost"=>$data->houdinv_coupons_discount_precentage,"min_order"=>$data->houdinv_coupons_order_amount);
                }
                else
                {
                 $array = array("code"=>400 ,"msg"=>"Coupon code expire or not available right now");
                }



             }
             else
             {
                $array = array("code"=>400 ,"msg"=>"no such coupon code");
             }

       return $array;

    }

   public function userGroup($id)
   {

     $data =   $this->getDynamicDB->select("*")->from("houdinv_users_group_users_list")
            ->where('houdinv_users_group_users_id',$id)
             ->get()->row();
             return $data->houdinv_users_group__user_group_id;
   }

    public function CouponValidForUsergroup($coupon_id,$userid)
    {
       $user_group = $this->userGroup($userid);

           $data =   $this->getDynamicDB->select("*")->from("houdinv_coupons_procus")
           ->where('houdinv_coupons_procus_coupon_id',$coupon_id)
            ->where('houdinv_coupons_procus_user_id',$userid)
            ->or_where("houdinv_coupons_procus_user_group",$user_group)
             ->get()->row();

            $date = strtotime(date("Y-m-d"));

             if($data)
             {

                 $array = array("code"=>200 ,"msg"=>"yes");

             }
             else
             {
                $array = array("code"=>400 ,"msg"=>"not Valid for you");
             }

       return $array;

    }


    public function ShowCartData1($id)
    {

    $data = $this->getDynamicDB->select("*")->from("houdinv_users_cart")
    ->join("houdinv_products","houdinv_users_cart.houdinv_users_cart_item_id=houdinv_products.houdin_products_id")
    ->where("houdinv_users_cart.houdinv_users_cart_user_id",$id)->get()->result();

    $cat1 = array();
        foreach($data as $val)
        {
          $item[] =  $val->houdinv_users_cart_item_id;
          $cat = explode(",",$val->houdin_products_category);
          array_push($cat1,$cat);
        }
        $array = array("item"=>$item,"cat"=>$cat);
    return $array;
    }


   public function CouponValidForProduct($coupon_id,$userid)
    {
       $user_product = $this->ShowCartData1($userid);
    //   print_R($user_product);

           $data =   $this->getDynamicDB->select("*")->from("houdinv_coupons_procus")
           ->where('houdinv_coupons_procus_coupon_id',$coupon_id)
            ->where_in('houdinv_coupons_procus_product_id',$user_product['item'])
          //  ->or_where_in("houdinv_coupons_procus_produdct_category",$user_product['cat'])
             ->get()->row();



             if($data)
             {

                 $array = array("code"=>200 ,"valid_for"=>"product",
                 "total"=>$data->houdinv_orders_total_Amount,"msg"=>$data->houdinv_coupons_procus_product_id);

             }
             else
             {
                $array = array("code"=>400 ,"msg"=>"not Valid for you");
             }

       return $array;

    }

     public function totalcart($userId)
    {
       $data = $this->getDynamicDB->select("*")->from("houdinv_users_cart")
    ->join("houdinv_products","houdinv_users_cart.houdinv_users_cart_item_id=houdinv_products.houdin_products_id")
    ->where("houdinv_users_cart.houdinv_users_cart_user_id",$userId)->get()->result();


                $final_price =0;
                        foreach($data as $thisItem)
                        {

                          $count = $thisItem->houdinv_users_cart_item_count;
                          $price = json_DEcode($thisItem->houdin_products_price,true);
                          $main_price = $price['sale_price']-$price['discount'];
                          $total_price = $main_price*$count;
                          $final_price =$final_price+$total_price;
                        }
    $shipping =$this->shippingCharge($final_price);
      $final_price = $final_price + $shipping;
   return  $final_price;
    }

    public function minAmount($coupon_id,$userId)
    {

        $total_cart = $this->totalcart($userId);
      //  print_R($total_cart);

        $data =   $this->getDynamicDB->select("*")->from("houdinv_coupons")
             ->where("houdinv_coupons_id",$coupon_id)->get()->row();

        if($data->houdinv_coupons_order_amount > $total_cart)
        {
          $array = array("code"=>400 ,"msg"=>" min order amount will be ".$data->houdinv_coupons_order_amount);

        }
        else
        {
              $array = array("code"=>200,"coupons_limit"=>$data->houdinv_coupons_limit);

        }

        return $array;
    }


    public function Already_use($coupon_id,$userId,$coupon_limit)
    {
     $data = $this->getDynamicDB->select("*")->from("houdinv_user_use_Coupon")
             ->where("houdinv_user_use_Coupon_user_id",$userId)
             ->where("houdinv_user_use_Coupon_coupon_id",$coupon_id)
             ->get()->row();

        if($data)
        {
            if($data->houdinv_user_use_Coupon_used_Count>=$coupon_limit)
            {
                $array = array("code"=>400 ,"msg"=>"You already used". $coupon_limit ."limit of this coupon");
            }
            else
            {
               $array = array("code"=>200);
            }


        }
        else
        {
         $array = array("code"=>200);

        }

        return $array;

    }
    public function updateSuccessTransaction($data)
    {
        $getInsertStatus = $this->getDynamicDB->insert('houdinv_transaction',$data);
        if($getInsertStatus)
        {
            // update accounts data
            $getUndepositedAccount = $this->getDynamicDB->select('houdinv_accounts_id')->from('houdinv_accounts')->where('houdinv_accounts_account_type_id','3')->where('houdinv_accounts_detail_type_id','21')->get()->result();    
            if(count($getUndepositedAccount) > 0)
            {
                $getCustomerId = $this->getDynamicDB->select('houdinv_order_user_id')->from('houdinv_orders')->where('houdinv_order_id',$data['houdinv_transaction_for_id'])->get()->result();
                if(count($getCustomerId) > 0)
                {
                    $getCustomerId = $getCustomerId;
                }
                else
                {
                    $getCustomerId = "";
                }
                $setInsertArray = array('houdinv_accounts_balance_sheet_account_id'=>$getUndepositedAccount[0]->houdinv_accounts_id,
                'houdinv_accounts_balance_sheet_ref_type'=>$data['houdinv_transaction_for_id'],
                'houdinv_accounts_balance_sheet_pay_account'=>$getCustomerId[0]->houdinv_order_user_id,
                'houdinv_accounts_balance_sheet_payee_type'=>'customer',
                'houdinv_accounts_balance_sheet_account'=>0,
                'houdinv_accounts_balance_sheet_created_at'=>strtotime(date('Y-m-d')),
                'houdinv_accounts_balance_sheet__increase'=>$data['houdinv_transaction_amount'],
                'houdinv_accounts_balance_sheet_order_id'=>$data['houdinv_transaction_for_id'],
                'houdinv_accounts_balance_sheet_decrease'=>0,
                'houdinv_accounts_balance_sheet_memo'=>'Online Transaction',
                'houdinv_accounts_balance_sheet_deposit'=>0,
                'houdinv_accounts_balance_sheet_final_balance'=>0,
                'houdinv_accounts_balance_sheet_date'=>date('Y-m-d'));
                $this->getDynamicDB->insert('houdinv_accounts_balance_sheet',$setInsertArray);
                // get amount
                $getTotalAmount = $this->getDynamicDB->select('SUM(houdinv_accounts_balance_sheet__increase) AS profitAmount, SUM(houdinv_accounts_balance_sheet_decrease) AS lossAmount')->from('houdinv_accounts_balance_sheet')
                ->where('houdinv_accounts_balance_sheet_account_id',$getUndepositedAccount[0]->houdinv_accounts_id)->get()->result();
                if(count($getTotalAmount) > 0)
                {
                    $setFinalAmount = $getTotalAmount[0]->profitAmount-$getTotalAmount[0]->lossAmount;
                }
                else
                {
                    $setFinalAmount = 0;
                }
                $this->getDynamicDB->where('houdinv_accounts_id',$getUndepositedAccount[0]->houdinv_accounts_id)->update('houdinv_accounts',array('houdinv_accounts_final_balance'=>$setFinalAmount));
            }
            // end here
            $getTransactionAmount = $data['houdinv_transaction_amount'];
            // get order details =
            $getOrderAmount = $this->getDynamicDB->select('houdinv_orders_total_Amount')->from('houdinv_orders')->where('houdinv_order_id',$data['houdinv_transaction_for_id'])->get()->result();
            $getRemainingAmount = $getOrderAmount[0]->houdinv_orders_total_Amount-$getTransactionAmount;
            $setUpdateArray = array('houdinv_payment_status'=>"1",'houdinv_orders_total_paid'=>$getTransactionAmount,'houdinv_orders_total_remaining'=>$getRemainingAmount);
            $this->getDynamicDB->where('houdinv_order_id',$data['houdinv_transaction_for_id']);
            $getStatus = $this->getDynamicDB->update('houdinv_orders',$setUpdateArray);
            if($getStatus)
            {
                return array('message'=>'yes');
            }
            else
            {
                return array('message'=>'no');
            }
        }
        else
        {
            return array('message'=>'no');
        }
    }
    public function updateFailureTransaction($data)
    {
        $getInsertStatus = $this->getDynamicDB->insert('houdinv_transaction',$data);
        if($getInsertStatus)
        {
            return array('message'=>'no');
        }
        else
        {
            return array('message'=>'no');
        }
    }

    }
