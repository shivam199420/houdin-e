<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Google OAuth Library for CodeIgniter 3.x
 *
 * Library for Google+ login. It helps the user to login with their Google account
 * in CodeIgniter application.
 *
 * This library requires the Google API PHP client and it should be placed in third_party folder.
 *
 * It also requires google configuration file and it should be placed in the config directory.
 *
 * @package     CodeIgniter
 * @category    Libraries
 * @author      CodexWorld
 * @license     http://www.codexworld.com/license/
 * @link        http://www.codexworld.com
 * @version     2.0
 */
class Facebook{


    public function __construct(){

        $CI =& get_instance();
        require_once APPPATH .'libraries/php 5 sdk/vendor/autoload.php';

    }

    public function loginURL() {
        return $this->client->createAuthUrl();
    }

    public function getAuthenticate() {
        return $this->client->authenticate();
    }

    public function getAccessToken() {
        return $this->client->getAccessToken();
    }

    public function setAccessToken() {
        return $this->client->setAccessToken();
    }

    public function revokeToken() {
        return $this->client->revokeToken();
    }

    public function getUserInfo() {
        return $this->oauth2->userinfo->get();
    }

}
