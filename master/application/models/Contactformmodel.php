<?php
class Contactformmodel extends CI_Model
{
  function __construct()
    {
        parent::__construct();
    }
    public function countQueryListdata()
    {
        $getData = $this->db->select('COUNT(houdin_admin_contact_id) AS queryCount')->from('houdin_admin_contact')->get()->result();
        return $getData[0]->queryCount;
    }
    public function getQueryList($data)
    {
        $this->db->select('*')->from('houdin_admin_contact');
        if($data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit'],$data['start']);
        }
        elseif(!$data['start'] && $data['limit'])
        {
            $this->db->limit($data['limit']);
        }  
        $getQueryData = $this->db->get();
        $resultQueryData = ($getQueryData->num_rows() > 0)?$getQueryData->result():FALSE;
        return array('queryList'=>$resultQueryData);
    }
}