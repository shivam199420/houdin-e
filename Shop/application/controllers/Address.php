<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Address extends CI_Controller
{
    function __construct()
	{
        parent::__construct();
        if($this->session->userdata('userAuth') == "")
        {
            redirect(base_url()."register", 'refresh');
        }
        $this->load->model('Shopuseraddress');
        $getSubDomain = checkShopUrl();
        if($getSubDomain['message'] == 'no')
        {
            redirect($getSubDomain['baseUrl'], 'refresh');
        }
        else
        {
            $this->setShopName = $getSubDomain['shop'];
        }
    }
    public function index()
    {
        $getUserAddress = $this->Shopuseraddress->fetchUserAddress();
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/address',$getUserAddress);
    }
    public function add()
    {
        $getAddressData = $this->Shopuseraddress->fetchAddressData();
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/addaddress',$getAddressData);
    }
    public function adduseraddress()
    {
        $this->form_validation->set_rules('username','Name','required');
        $this->form_validation->set_rules('userphone','Phone','required');
        $this->form_validation->set_rules('usermainaddress','Address','required');
        $this->form_validation->set_rules('usercity','City / Town','required');
        $this->form_validation->set_rules('userpincode','Pin Code','required');
        $this->form_validation->set_rules('usercountry','Country','required');
        if($this->form_validation->run() == true)
        {
            $setData = strtotime(date('Y-m-d h:i:s'));
            $setInsertArray = array('houdinv_user_address_user_id'=>$this->session->userdata('userAuth'),'houdinv_user_address_name'=>$this->input->post('username'),
            'houdinv_user_address_phone'=>$this->input->post('userphone'),'houdinv_user_address_user_address'=>$this->input->post('usermainaddress'),'houdinv_user_address_city'=>$this->input->post('usercity'),
            'houdinv_user_address_zip'=>$this->input->post('userpincode'),'houdinv_user_address_country'=>$this->input->post('usercountry'),'houdinv_user_address_created_at'=>$setData
        );
            $getInsertStatus = $this->Shopuseraddress->addUserAddressData($setInsertArray);
            if($getInsertStatus['message'] == 'yes')
            {
                $this->session->set_flashdata('success','Address addedd successfully');
                redirect(base_url()."Address", 'refresh');
            }
            else
            {
                $this->session->set_flashdata('error','Something went wrong. Please try again');
                redirect(base_url()."Address/add", 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('error',validation_errors());
            redirect(base_url()."Address/add", 'refresh');
        }
    }
    public function fetchUserCountry()
    { 
        $this->form_validation->set_rules('shopName','Country','required');
        if($this->form_validation->run() == true)
        {
            $getCountryList = $this->Shopuseraddress->fetchUserCounrtyApp($this->input->post('shopName'));
            print_r(json_encode($getCountryList));
        }
        else
        {
            $resultArray = array('messgae'=>'Something went wrong. Please try again');
            print_r(json_encode($resultArray));
        }
    }
    public function adduseraddressapp()
    {
        $this->form_validation->set_rules('username','Name','required');
        $this->form_validation->set_rules('userphone','Phone','required');
        $this->form_validation->set_rules('usermainaddress','Address','required');
        $this->form_validation->set_rules('usercity','City / Town','required');
        $this->form_validation->set_rules('userpincode','Pin Code','required');
        $this->form_validation->set_rules('usercountry','Country','required');
        $this->form_validation->set_rules('userid','Country','required');
        $this->form_validation->set_rules('shopName','Country','required');
        if($this->form_validation->run() == true)
        {
            $setData = strtotime(date('Y-m-d h:i:s'));
            $setInsertArray = array('houdinv_user_address_user_id'=>$this->input->post('userid'),'houdinv_user_address_name'=>$this->input->post('username'),
            'houdinv_user_address_phone'=>$this->input->post('userphone'),'houdinv_user_address_user_address'=>$this->input->post('usermainaddress'),'houdinv_user_address_city'=>$this->input->post('usercity'),
            'houdinv_user_address_zip'=>$this->input->post('userpincode'),'houdinv_user_address_country'=>$this->input->post('usercountry'),'houdinv_user_address_created_at'=>$setData
            );
            $setExtraData = array('shopName'=>$this->input->post('shopName'));
            $getInsertStatus = $this->Shopuseraddress->addUserAddressDataApp($setInsertArray,$setExtraData);
            if($getInsertStatus['message'] == 'yes')
            {
                $resultArray = array('messgae'=>'Address is addedd successfully');
                print_r(json_encode($resultArray));
            }
            else
            {
                $resultArray = array('messgae'=>'Something went wrong. Please try again');
                print_r(json_encode($resultArray));
            }
        }
        else
        {
            $resultArray = array('messgae'=>'All fields are mandatory');
            print_r(json_encode($resultArray));
        }
    }
    public function edit()
    {
        if(!$this->uri->segment('3'))
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(base_url()."Address", 'refresh');
        }
        // edit address
        if($this->input->post('editAddress'))
        {
            $this->form_validation->set_rules('username','Name','required');
            $this->form_validation->set_rules('userphone','Phone','required');
            $this->form_validation->set_rules('usermainaddress','Address','required');
            $this->form_validation->set_rules('usercity','City / Town','required');
            $this->form_validation->set_rules('userpincode','Pin Code','required');
            $this->form_validation->set_rules('usercountry','Country','required');
            if($this->form_validation->run() == true)
            {
                $setData = strtotime(date('Y-m-d h:i:s'));
                $setUpdateArray = array('houdinv_user_address_user_id'=>$this->session->userdata('userAuth'),'houdinv_user_address_name'=>$this->input->post('username'),
                'houdinv_user_address_phone'=>$this->input->post('userphone'),'houdinv_user_address_user_address'=>$this->input->post('usermainaddress'),'houdinv_user_address_city'=>$this->input->post('usercity'),
                'houdinv_user_address_zip'=>$this->input->post('userpincode'),'houdinv_user_address_country'=>$this->input->post('usercountry'),'houdinv_user_address_modified_at'=>$setData);
                $getUpdateStatus = $this->Shopuseraddress->updateUserAddress($setUpdateArray);
                if($getUpdateStatus['message'] == 'yes')
                {
                    $this->session->set_flashdata('success','Address updated successfully');
                    redirect(base_url()."Address", 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('error','Something went wrong. Please try again');
                    redirect(base_url()."Address/edit/".$this->uri->segment('3')."", 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url()."Address/edit/".$this->uri->segment('3')."", 'refresh');
            }
        }
        $getAddressData = $this->Shopuseraddress->fetchAddressData();
        $getAddressData['address'] = $this->Shopuseraddress->getUserAddressUpdate($this->uri->segment('3'));
        $getShopName = getfolderName($this->session->userdata('shopName'));
        $this->load->view(''.$getShopName.'/editaddress',$getAddressData);
    }  
    public function deleteAddress()
    {
        if(!$this->uri->segment('3'))
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(base_url()."Address", 'refresh');
        }
        $getDeleteStatus = $this->Shopuseraddress->deleteUserAddress($this->uri->segment('3'));
        if($getDeleteStatus['message'] == 'yes')
        {
            $this->session->set_flashdata('success','Address deleted successfully');
            redirect(base_url()."Address", 'refresh');
        }
        else
        {
            $this->session->set_flashdata('error','Something went wrong. Please try again');
            redirect(base_url()."Address", 'refresh');
        }
    } 
}
