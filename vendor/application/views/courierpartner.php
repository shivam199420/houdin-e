<?php $this->load->view('Template/header.php') ?>
<?php $this->load->view('Template/sidebar.php') ?>
<style type="text/css">
  @media screen and (min-width: 768px){
.dropdown.dropdown-lg .dropdown-menu {
    min-width: 390px!important;
}
.checkbox{
  padding-left: 10px;
}
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 10px;
}
}
@media(max-width:767px){
  .custom_table_respo td{ 
       display: block;
       width: 50%;
       float: left;
  }
  .custom_payment {
    padding: 15px 5px!important;
    height: 46px!important;
    margin-top: 3px;
}
}
.custom_payment {
    padding: 15px 5px;
    height: 52px;
    margin-top: 2px;
}
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
 
           <!--START Page-Title -->
            <div class="row">
           
                <div class="col-md-8">
                   <h4 class="page-title">Courier Partner</h4>
                   <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                 <li><a href="<?php echo base_url(); ?>shipping/Shippinglanding">Shipping</a></li>
                  <li class="active">Courier Partner</li>
                  </ol>
                  </div>
                 
            </div>
           <!--END Page-Title -->  
            <div class="row">
            <div class="col-sm-12">
            <?php 
            if($this->session->flashdata('success'))
            {
                echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
            }
            if($this->session->flashdata('error'))
            {
                echo '<div class="alert alert-danger">'.$this->session->flashdata('error').'</div>';
            }
            ?>
            </div>
            </div>

                                      <div class="row m-t-20">
        <div class="col-sm-12">
        <div class="card-box">
        <?php echo form_open(base_url( 'Shipping/partner' ), array( 'id'=>'updatePostmenForm','method'=>'post' ));?>
        <div class="form-group">
        <label>Postmen Api Key</label>
        <input type="hidden" value="<?php echo $data[0]->houdinv_shipping_credentials_id ?>" name="postmenapikeyid"/>
        <input type="text" value="<?php echo $data[0]->houdinv_shipping_credentials_key ?>" class="form-control required_validation_for_add_single_tag name_validation" name="postmenapikey" placeholder="Enter postmen key" />
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-primary" name="updateApiKey" value="Update"/>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        </div>

<?php $this->load->view('Template/footer.php') ?>
<script type="text/javascript">
$(document).ready(function(){
    $(document).on('submit','#updatePostmenForm',function(c){
    var rep_image_val='';
    $(this).find(".required_validation_for_add_single_tag").each(function()
    {
        var val22 = jQuery(this).val();
        if (!val22)
        {   
            rep_image_val = 'error form';
            $(this).css("border-color","red");
        }
    });
    $(this).find('.required_validation_for_add_single_tag').on('keyup blur',function()
    {
        $(this).css("border-color","#ccc");
    });
    if(rep_image_val)
    {
        c.preventDefault();
        return false;
    }
    else
    {
        return true;
    }        
    });
})
        </script>
